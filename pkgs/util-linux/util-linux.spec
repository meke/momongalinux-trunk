%global momorel 1

### Header
Summary: A collection of basic system utilities
Name: util-linux
Version: 2.24.2
Release: %{momorel}m%{?dist}
License: GPLv2 and GPLv2+ and GPLv3+ and LGPLv2+ and BSD and Public Domain
Group: System Environment/Base
URL: ftp://ftp.kernel.org/pub/linux/utils/util-linux

%global upstream_version %{version}

%define include_raw 0
%define mtab_symlink 1

### Macros
%define cytune_archs %{ix86} alpha %{arm}
%define compldir %{_datadir}/bash-completion/completions/

### Paths
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

### Dependences
BuildRequires: audit-libs-devel >= 2.0.4
BuildRequires: gettext-devel
BuildRequires: libselinux-devel
BuildRequires: ncurses-devel
BuildRequires: pam-devel
BuildRequires: texinfo
BuildRequires: zlib-devel
BuildRequires: popt-devel
BuildRequires: libutempter-devel

### Sources
Source0: ftp://ftp.kernel.org/pub/linux/utils/util-linux/v2.24/%{name}-%{upstream_version}.tar.xz
NoSource: 0
Source0: ftp://ftp.kernel.org/pub/linux/utils/util-linux/v2.24/util-linux-%{upstream_version}.tar.xz
Source1: util-linux-login.pamd
Source2: util-linux-remote.pamd
Source3: util-linux-chsh-chfn.pamd
Source4: util-linux-60-raw.rules
Source12: util-linux-su.pamd
Source13: util-linux-su-l.pamd
Source14: util-linux-runuser.pamd
Source15: util-linux-runuser-l.pamd

### Obsoletes & Conflicts & Provides
Conflicts: bash-completion < 2.1-2m
# su(1) and runuser(1) merged into util-linux v2.22
Conflicts: coreutils < 8.20
# eject has been merged into util-linux v2.22
Obsoletes: eject <= 2.1.5
Provides: eject = 2.1.6
# sulogin, utmpdump merged into util-linux v2.22
Conflicts: sysvinit-tools < 2.88-4m
# old versions of e2fsprogs contain fsck, uuidgen
Conflicts: e2fsprogs < 1.41.8-5
# rename from util-linux-ng back to util-linux
Obsoletes: util-linux-ng < 2.20.1-2m
Provides: util-linux-ng = %{version}-%{release}

Provides: /bin/dmesg
Provides: /bin/kill
Provides: /bin/more
Provides: /bin/mount
Provides: /bin/umount
Provides: /sbin/blkid
Provides: /sbin/blockdev
Provides: /sbin/findfs
Provides: /sbin/fsck
Provides: /sbin/nologin

Requires(post): coreutils
Requires: pam >= 1.0.90
Requires: audit-libs >= 1.0.6
Requires: libuuid = %{version}-%{release}
Requires: libblkid = %{version}-%{release}
Requires: libmount = %{version}-%{release}

%if %{include_raw}
Requires: udev
%endif

### Ready for upstream?
###
# 151635 - makeing /var/log/lastlog
Patch3: util-linux-ng-2.22-login-lastlog.patch

%description
The util-linux package contains a large variety of low-level system
utilities that are necessary for a Linux system to function. Among
others, Util-linux contains the fdisk configuration tool and the login
program.


%package -n libmount
Summary: Device mounting library
Group: Development/Libraries
License: LGPLv2+
Requires: libblkid = %{version}-%{release}

%description -n libmount
This is the device mounting library, part of util-linux.


%package -n libmount-devel
Summary: Device mounting library
Group: Development/Libraries
License: LGPLv2+
Requires: libmount = %{version}-%{release}
Requires: pkgconfig

%description -n libmount-devel
This is the device mounting development library and headers,
part of util-linux.


%package -n libblkid
Summary: Block device ID library
Group: Development/Libraries
License: LGPLv2+
Requires: libuuid = %{version}-%{release}

%description -n libblkid
This is block device identification library, part of util-linux.


%package -n libblkid-devel
Summary: Block device ID library
Group: Development/Libraries
License: LGPLv2+
Requires: libblkid = %{version}-%{release}
Requires: pkgconfig

%description -n libblkid-devel
This is the block device identification development library and headers,
part of util-linux.


%package -n libuuid
Summary: Universally unique ID library
Group: Development/Libraries
License: BSD

%description -n libuuid
This is the universally unique ID library, part of e2fsprogs.

The libuuid library generates and parses 128-bit universally unique
id's (UUID's).  A UUID is an identifier that is unique across both
space and time, with respect to the space of all UUIDs.  A UUID can
be used for multiple purposes, from tagging objects with an extremely
short lifetime, to reliably identifying very persistent objects
across a network.

See also the "uuid" package, which is a separate implementation.

%package -n libuuid-devel
Summary: Universally unique ID library
Group: Development/Libraries
License: BSD
Requires: libuuid = %{version}-%{release}
Requires: pkgconfig

%description -n libuuid-devel
This is the universally unique ID development library and headers,
part of e2fsprogs.

The libuuid library generates and parses 128-bit universally unique
id's (UUID's).  A UUID is an identifier that is unique across both
space and time, with respect to the space of all UUIDs.  A UUID can
be used for multiple purposes, from tagging objects with an extremely
short lifetime, to reliably identifying very persistent objects
across a network.

See also the "uuid-devel" package, which is a separate implementation.


%package -n uuidd
Summary: Helper daemon to guarantee uniqueness of time-based UUIDs
Group: System Environment/Daemons
Requires: libuuid = %{version}-%{release}
License: GPLv2
Requires(pre): shadow-utils

%description -n uuidd
The uuidd package contains a userspace daemon (uuidd) which guarantees
uniqueness of time-based UUID generation even at very high rates on
SMP systems.

%package -n python3-libmount
Summary: Python bindings for the libmount library
Group: Development/Libraries
Requires: libmount = %{version}-%{release}

%description -n python3-libmount
The libmount-python package contains a module that permits applications
written in the Python programming language to use the interface
supplied by the libmount library to work with mount tables (fstab,
mountinfo, etc) and mount filesystems.

%prep
%setup -n %{name}-%{upstream_version}

%build
unset LINGUAS || :

export CFLAGS="-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 $RPM_OPT_FLAGS"
export SUID_CFLAGS="-fpie"
export SUID_LDFLAGS="-pie -Wl,-z,relro -Wl,-z,now"
%configure \
        --with-systemdsystemunitdir=%{_unitdir} \
        --disable-silent-rules \
        --disable-bfs \
        --disable-pg \
        --enable-socket-activation \
        --enable-chfn-chsh \
        --enable-write \
        --enable-raw \
        --with-python=3 \
        --with-udev \
        --with-selinux \
        --with-audit \
        --with-utempter \
        --disable-makeinstall-chown \
%ifnarch %cytune_archs
        --disable-cytune \
%endif
%ifarch s390 s390x
        --disable-hwclock \
        --disable-fdformat
%endif

# build util-linux
make %{?_smp_mflags}

%check
#to run tests use "--with check"
%if %{?_with_check:1}%{!?_with_check:0}
make check
%endif


%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man{1,6,8,5}
mkdir -p ${RPM_BUILD_ROOT}%{_sbindir}
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/{pam.d,security/console.apps}
mkdir -p ${RPM_BUILD_ROOT}/var/log
touch ${RPM_BUILD_ROOT}/var/log/lastlog
chmod 0644 ${RPM_BUILD_ROOT}/var/log/lastlog

# install util-linux
make install DESTDIR=${RPM_BUILD_ROOT}

# raw
echo '.so man8/raw.8' > $RPM_BUILD_ROOT%{_mandir}/man8/rawdevices.8
{ 
        # see RH bugzilla #216664
        mkdir -p ${RPM_BUILD_ROOT}%{_prefix}/lib/udev/rules.d
        pushd ${RPM_BUILD_ROOT}%{_prefix}/lib/udev/rules.d
        install -m 644 %{SOURCE4} ./60-raw.rules
        popd
}

# sbin -> bin
mv ${RPM_BUILD_ROOT}%{_sbindir}/raw ${RPM_BUILD_ROOT}%{_bindir}/raw

# And a dirs uuidd needs that the makefiles don't create
install -d ${RPM_BUILD_ROOT}/run/uuidd
install -d ${RPM_BUILD_ROOT}/var/lib/libuuid

# libtool junk
rm -rf ${RPM_BUILD_ROOT}%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/python*/site-packages/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/python*/site-packages/*.a

%ifarch %{sparc}
rm -rf ${RPM_BUILD_ROOT}%{_bindir}/sunhostid
cat << E-O-F > ${RPM_BUILD_ROOT}%{_bindir}/sunhostid
#!/bin/sh 
# this should be _bindir/sunhostid or somesuch.
# Copyright 1999 Peter Jones, <pjones@redhat.com> .
# GPL and all that good stuff apply.
(
idprom=\`cat /proc/openprom/idprom\`
echo \$idprom|dd bs=1 skip=2 count=2
echo \$idprom|dd bs=1 skip=27 count=6
echo
) 2>/dev/null
E-O-F
chmod 755 ${RPM_BUILD_ROOT}%{_bindir}/sunhostid
%endif

# PAM settings
{
        pushd ${RPM_BUILD_ROOT}%{_sysconfdir}/pam.d
        install -m 644 %{SOURCE1} ./login
        install -m 644 %{SOURCE2} ./remote
        install -m 644 %{SOURCE3} ./chsh
        install -m 644 %{SOURCE3} ./chfn
        install -m 644 %{SOURCE12} ./su
        install -m 644 %{SOURCE13} ./su-l
        install -m 644 %{SOURCE14} ./runuser
        install -m 644 %{SOURCE15} ./runuser-l
        popd
}

%ifnarch s390 s390x
ln -sf hwclock ${RPM_BUILD_ROOT}%{_sbindir}/clock
echo ".so man8/hwclock.8" > ${RPM_BUILD_ROOT}%{_mandir}/man8/clock.8
%endif

# unsupported on SPARCs
%ifarch %{sparc}
for I in /sbin/sfdisk \
        %{_mandir}/man8/sfdisk.8* \
        %doc Documentation/sfdisk.txt \
        /sbin/cfdisk \
        %{_mandir}/man8/cfdisk.8*; do
      
        rm -f $RPM_BUILD_ROOT$I
done
%endif

# we install getopt-*.{bash,tcsh} by %doc directive
chmod 644 misc-utils/getopt-*.{bash,tcsh}
rm -f ${RPM_BUILD_ROOT}%{_datadir}/doc/util-linux/getopt/*
rmdir ${RPM_BUILD_ROOT}%{_datadir}/doc/util-linux/getopt
        
ln -sf /proc/mounts %{buildroot}/etc/mtab

# remove static libs
rm -f $RPM_BUILD_ROOT%{_libdir}/lib{uuid,blkid,mount}.a

# find MO files
%find_lang %name

# the files section supports only one -f option...
mv %{name}.lang %{name}.files

# create list of setarch(8) symlinks
find  $RPM_BUILD_ROOT%{_bindir}/ -regextype posix-egrep -type l \
        -regex ".*(linux32|linux64|s390|s390x|i386|ppc|ppc64|ppc32|sparc|sparc64|sparc32|sparc32bash|mips|mips64|mips32|ia64|x86_64)$" \
        -printf "%{_bindir}/%f\n" >> %{name}.files

find  $RPM_BUILD_ROOT%{_mandir}/man8 -regextype posix-egrep  \
        -regex ".*(linux32|linux64|s390|s390x|i386|ppc|ppc64|ppc32|sparc|sparc64|sparc32|sparc32bash|mips|mips64|mips32|ia64|x86_64)\.8.*" \
        -printf "%{_mandir}/man8/%f*\n" >> %{name}.files

%post   
# only for minimal buildroots without /var/log
[ -d /var/log ] || mkdir -p /var/log
touch /var/log/lastlog
chown root:root /var/log/lastlog
chmod 0644 /var/log/lastlog
# Fix the file context, do not use restorecon
if [ -x /usr/sbin/selinuxenabled ] && /usr/sbin/selinuxenabled; then
        SECXT=$( /usr/sbin/matchpathcon -n /var/log/lastlog 2> /dev/null )
        if [ -n "$SECXT" ]; then
                # Selinux enabled, but without policy? It's true for buildroots
                # without selinux stuff on host machine with enabled selinux.
                # We don't want to use any RPM dependence on selinux policy for
                # matchpathcon(2). SELinux policy should be optional.
                /usr/bin/chcon "$SECXT"  /var/log/lastlog >/dev/null 2>&1 || :
        fi 
fi
if [ ! -L /etc/mtab ]; then
        ln -fs /proc/mounts /etc/mtab
fi

%post -n libblkid
/sbin/ldconfig 

### Move blkid cache to /run
[ -d /run/blkid ] || mkdir -p /run/blkid
for I in /etc/blkid.tab /etc/blkid.tab.old \
         /etc/blkid/blkid.tab /etc/blkid/blkid.tab.old; do

        if [ -f "$I" ]; then
                mv "$I" /run/blkid/ || :
        fi
done

%postun -n libblkid -p /sbin/ldconfig

%post -n libuuid -p /sbin/ldconfig
%postun -n libuuid -p /sbin/ldconfig

%post -n libmount -p /sbin/ldconfig
%postun -n libmount -p /sbin/ldconfig

%pre -n uuidd
getent group uuidd >/dev/null || groupadd -r uuidd
getent passwd uuidd >/dev/null || \
useradd -r -g uuidd -d /var/lib/libuuid -s /sbin/nologin \
    -c "UUID generator helper daemon" uuidd
exit 0

%post -n uuidd
if [ $1 -eq 1 ]; then
        # Package install,
        /bin/systemctl enable uuidd.service >/dev/null 2>&1 || :
        /bin/systemctl start uuidd.service > /dev/null 2>&1 || :
else
        # Package upgrade
        if /bin/systemctl --quiet is-enabled uuidd.service ; then
                /bin/systemctl reenable uuidd.service >/dev/null 2>&1 || :
        fi
fi

%preun -n uuidd
if [ "$1" = 0 ]; then
        /bin/systemctl stop uuidd.service > /dev/null 2>&1 || :
        /bin/systemctl disable uuidd.service > /dev/null 2>&1 || :
fi


%files -f %{name}.files
%defattr(-,root,root)
%doc README NEWS AUTHORS
%doc Documentation/deprecated.txt Documentation/licenses/*
%doc misc-utils/getopt-*.{bash,tcsh}

%config(noreplace)      %{_sysconfdir}/pam.d/chfn
%config(noreplace)      %{_sysconfdir}/pam.d/chsh
%config(noreplace)      %{_sysconfdir}/pam.d/login
%config(noreplace)      %{_sysconfdir}/pam.d/remote
%config(noreplace)      %{_sysconfdir}/pam.d/su
%config(noreplace)      %{_sysconfdir}/pam.d/su-l
%config(noreplace)      %{_sysconfdir}/pam.d/runuser
%config(noreplace)      %{_sysconfdir}/pam.d/runuser-l
%config(noreplace)      %{_prefix}/lib/udev/rules.d/60-raw.rules

%attr(4755,root,root)   %{_bindir}/mount
%attr(4755,root,root)   %{_bindir}/umount
%attr(4755,root,root)   %{_bindir}/su
%attr(755,root,root)    %{_bindir}/login
%attr(4711,root,root)   %{_bindir}/chfn
%attr(4711,root,root)   %{_bindir}/chsh
%attr(2755,root,tty)    %{_bindir}/write

%ghost %attr(0644,root,root) %verify(not md5 size mtime) /var/log/lastlog
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) /etc/mtab

%{_bindir}/cal
%{_bindir}/chrt
%{_bindir}/col
%{_bindir}/colcrt
%{_bindir}/colrm
%{_bindir}/column
%{_bindir}/dmesg
%{_bindir}/eject
%{_bindir}/fallocate    
%{_bindir}/findmnt
%{_bindir}/flock
%{_bindir}/getopt
%{_bindir}/hexdump
%{_bindir}/ionice
%{_bindir}/ipcmk
%{_bindir}/ipcrm
%{_bindir}/ipcs
%{_bindir}/isosize
%{_bindir}/kill
%{_bindir}/last
%{_bindir}/lastb
%{_bindir}/logger
%{_bindir}/look
%{_bindir}/lsblk
%{_bindir}/lscpu
%{_bindir}/lslocks
%{_bindir}/mcookie
%{_bindir}/mesg
%{_bindir}/more
%{_bindir}/mountpoint
%{_bindir}/namei
%{_bindir}/nsenter
%{_bindir}/prlimit
%{_bindir}/raw
%{_bindir}/rename
%{_bindir}/renice
%{_bindir}/rev
%{_bindir}/script
%{_bindir}/scriptreplay
%{_bindir}/setarch
%{_bindir}/setpriv
%{_bindir}/setsid
%{_bindir}/setterm
%{_bindir}/tailf
%{_bindir}/taskset
%{_bindir}/ul
%{_bindir}/unshare
%{_bindir}/utmpdump
%{_bindir}/uuidgen
%{_bindir}/wall
%{_bindir}/wdctl
%{_bindir}/whereis
%{_mandir}/man1/cal.1*
%{_mandir}/man1/chfn.1*
%{_mandir}/man1/chrt.1*
%{_mandir}/man1/chsh.1*
%{_mandir}/man1/col.1*
%{_mandir}/man1/colcrt.1*
%{_mandir}/man1/colrm.1*
%{_mandir}/man1/column.1*
%{_mandir}/man1/dmesg.1*
%{_mandir}/man1/eject.1*
%{_mandir}/man1/fallocate.1*
%{_mandir}/man1/flock.1*
%{_mandir}/man1/getopt.1*
%{_mandir}/man1/hexdump.1*
%{_mandir}/man1/ionice.1*
%{_mandir}/man1/ipcmk.1*
%{_mandir}/man1/ipcrm.1*
%{_mandir}/man1/ipcs.1*
%{_mandir}/man1/kill.1*
%{_mandir}/man1/last.1*
%{_mandir}/man1/lastb.1*
%{_mandir}/man1/logger.1*
%{_mandir}/man1/login.1*
%{_mandir}/man1/look.1*
%{_mandir}/man1/lscpu.1*
%{_mandir}/man1/mcookie.1*
%{_mandir}/man1/mesg.1*
%{_mandir}/man1/more.1*
%{_mandir}/man1/mountpoint.1*
%{_mandir}/man1/namei.1*
%{_mandir}/man1/nsenter.1*
%{_mandir}/man1/prlimit.1*
%{_mandir}/man1/rename.1*
%{_mandir}/man1/renice.1*
%{_mandir}/man1/rev.1*
%{_mandir}/man1/runuser.1*
%{_mandir}/man1/script.1*
%{_mandir}/man1/scriptreplay.1*
%{_mandir}/man1/setpriv.1*
%{_mandir}/man1/setsid.1*
%{_mandir}/man1/setterm.1*
%{_mandir}/man1/su.1*
%{_mandir}/man1/tailf.1*
%{_mandir}/man1/taskset.1*
%{_mandir}/man1/ul.1*
%{_mandir}/man1/unshare.1* 
%{_mandir}/man1/utmpdump.1.*
%{_mandir}/man1/uuidgen.1*
%{_mandir}/man1/wall.1*
%{_mandir}/man1/whereis.1*
%{_mandir}/man1/write.1*
%{_mandir}/man5/fstab.5*
%{_mandir}/man8/addpart.8*
%{_mandir}/man8/agetty.8*
%{_mandir}/man8/blkdiscard.8*
%{_mandir}/man8/blkid.8*
%{_mandir}/man8/blockdev.8*
%{_mandir}/man8/chcpu.8*
%{_mandir}/man8/ctrlaltdel.8*
%{_mandir}/man8/delpart.8*
%{_mandir}/man8/fdisk.8*
%{_mandir}/man8/findfs.8*
%{_mandir}/man8/findmnt.8*
%{_mandir}/man8/fsck.8*
%{_mandir}/man8/fsck.cramfs.8*
%{_mandir}/man8/fsck.minix.8*
%{_mandir}/man8/fsfreeze.8*
%{_mandir}/man8/fstrim.8*
%{_mandir}/man8/isosize.8*
%{_mandir}/man8/ldattach.8*
%{_mandir}/man8/losetup.8*
%{_mandir}/man8/lsblk.8*
%{_mandir}/man8/lslocks.8.*
%{_mandir}/man8/mkfs.8*
%{_mandir}/man8/mkfs.cramfs.8*
%{_mandir}/man8/mkfs.minix.8*
%{_mandir}/man8/mkswap.8*
%{_mandir}/man8/mount.8*
%{_mandir}/man8/nologin.8*
%{_mandir}/man8/partx.8*
%{_mandir}/man8/pivot_root.8*
%{_mandir}/man8/raw.8*
%{_mandir}/man8/rawdevices.8*
%{_mandir}/man8/readprofile.8*
%{_mandir}/man8/resizepart.8*
%{_mandir}/man8/rtcwake.8*
%{_mandir}/man8/setarch.8*
%{_mandir}/man8/sulogin.8.*
%{_mandir}/man8/swaplabel.8*
%{_mandir}/man8/swapoff.8*
%{_mandir}/man8/swapon.8*
%{_mandir}/man8/switch_root.8*
%{_mandir}/man8/umount.8*
%{_mandir}/man8/wdctl.8.*
%{_mandir}/man8/wipefs.8*
%{_sbindir}/addpart
%{_sbindir}/agetty
%{_sbindir}/blkdiscard
%{_sbindir}/blkid
%{_sbindir}/blockdev
%{_sbindir}/chcpu
%{_sbindir}/ctrlaltdel
%{_sbindir}/delpart
%{_sbindir}/fdisk
%{_sbindir}/findfs
%{_sbindir}/fsck
%{_sbindir}/fsck.cramfs
%{_sbindir}/fsck.minix
%{_sbindir}/fsfreeze
%{_sbindir}/fstrim
%{_sbindir}/ldattach
%{_sbindir}/losetup
%{_sbindir}/mkfs
%{_sbindir}/mkfs.cramfs
%{_sbindir}/mkfs.minix
%{_sbindir}/mkswap
%{_sbindir}/nologin
%{_sbindir}/partx
%{_sbindir}/pivot_root
%{_sbindir}/readprofile
%{_sbindir}/resizepart
%{_sbindir}/rtcwake
%{_sbindir}/runuser
%{_sbindir}/sulogin
%{_sbindir}/swaplabel
%{_sbindir}/swapoff
%{_sbindir}/swapon
%{_sbindir}/switch_root
%{_sbindir}/wipefs

%{compldir}/addpart
%{compldir}/blkdiscard
%{compldir}/blkid
%{compldir}/blockdev
%{compldir}/cal
%{compldir}/chcpu
%{compldir}/chfn
%{compldir}/chrt
%{compldir}/chsh
%{compldir}/col
%{compldir}/colcrt
%{compldir}/colrm
%{compldir}/column
%{compldir}/ctrlaltdel
%{compldir}/delpart
%{compldir}/dmesg
%{compldir}/eject
%{compldir}/fallocate
%{compldir}/fdisk
%{compldir}/findmnt
%{compldir}/flock
%{compldir}/fsck
%{compldir}/fsck.cramfs
%{compldir}/fsck.minix
%{compldir}/fsfreeze
%{compldir}/fstrim
%{compldir}/getopt
%{compldir}/hexdump
%{compldir}/ionice
%{compldir}/ipcrm
%{compldir}/ipcs
%{compldir}/isosize
%{compldir}/ldattach
%{compldir}/logger
%{compldir}/look
%{compldir}/losetup
%{compldir}/lsblk
%{compldir}/lscpu 
%{compldir}/lslocks
%{compldir}/mcookie
%{compldir}/mesg
%{compldir}/mkfs
%{compldir}/mkfs.cramfs
%{compldir}/mkfs.minix
%{compldir}/mkswap
%{compldir}/more
%{compldir}/mountpoint
%{compldir}/namei
%{compldir}/nsenter
%{compldir}/partx
%{compldir}/pivot_root
%{compldir}/prlimit
%{compldir}/raw
%{compldir}/readprofile
%{compldir}/rename
%{compldir}/renice
%{compldir}/resizepart
%{compldir}/rev
%{compldir}/rtcwake
%{compldir}/runuser
%{compldir}/script
%{compldir}/scriptreplay
%{compldir}/setarch
%{compldir}/setpriv
%{compldir}/setsid
%{compldir}/setterm
%{compldir}/su
%{compldir}/swaplabel
%{compldir}/swapon
%{compldir}/tailf
%{compldir}/taskset
%{compldir}/ul
%{compldir}/unshare
%{compldir}/utmpdump
%{compldir}/uuidgen
%{compldir}/wall
%{compldir}/wdctl
%{compldir}/whereis
%{compldir}/wipefs
%{compldir}/write

%ifnarch s390 s390x
%{_sbindir}/clock
%{_sbindir}/fdformat
%{_sbindir}/hwclock
%{_mandir}/man8/fdformat.8*
%{_mandir}/man8/hwclock.8*
%{_mandir}/man8/clock.8*
%{compldir}/fdformat
%{compldir}/hwclock
%endif

%ifnarch %{sparc}
%doc Documentation/sfdisk.txt
%{_sbindir}/cfdisk
%{_sbindir}/sfdisk
%{_mandir}/man8/cfdisk.8*
%{_mandir}/man8/sfdisk.8*
%{compldir}/cfdisk
%{compldir}/sfdisk
%endif

%ifarch %{sparc}
%{_bindir}/sunhostid
%endif

%ifarch %cytune_archs
%{_bindir}/cytune
%{_mandir}/man8/cytune.8*
%{compldir}/cytune
%endif


%files -n uuidd
%defattr(-,root,root)
%doc Documentation/licenses/COPYING.GPLv2
%{_mandir}/man8/uuidd.8*
%{_sbindir}/uuidd
%{_unitdir}/*
%dir %attr(2775, uuidd, uuidd) /var/lib/libuuid
%dir %attr(2775, uuidd, uuidd) /run/uuidd
%{compldir}/uuidd


%files -n libmount
%defattr(-,root,root)
%doc libmount/COPYING
%{_libdir}/libmount.so.*

%files -n libmount-devel
%defattr(-,root,root)
%doc libmount/COPYING
%{_libdir}/libmount.so
%{_includedir}/libmount
%{_libdir}/pkgconfig/mount.pc


%files -n libblkid
%defattr(-,root,root)
%doc libblkid/COPYING
%{_libdir}/libblkid.so.*

%files -n libblkid-devel
%defattr(-,root,root)
%doc libblkid/COPYING
%{_libdir}/libblkid.so
%{_includedir}/blkid
%{_mandir}/man3/libblkid.3*
%{_libdir}/pkgconfig/blkid.pc


%files -n libuuid
%defattr(-,root,root)
%doc libuuid/COPYING
%{_libdir}/libuuid.so.*


%files -n libuuid-devel
%defattr(-,root,root)
%doc libuuid/COPYING
%{_libdir}/libuuid.so
%{_includedir}/uuid
%{_mandir}/man3/uuid.3*
%{_mandir}/man3/uuid_clear.3*
%{_mandir}/man3/uuid_compare.3*
%{_mandir}/man3/uuid_copy.3*
%{_mandir}/man3/uuid_generate.3*
%{_mandir}/man3/uuid_generate_random.3*
%{_mandir}/man3/uuid_generate_time.3*
%{_mandir}/man3/uuid_generate_time_safe.3*
%{_mandir}/man3/uuid_is_null.3*
%{_mandir}/man3/uuid_parse.3*
%{_mandir}/man3/uuid_time.3*
%{_mandir}/man3/uuid_unparse.3*
%{_libdir}/pkgconfig/uuid.pc

%files -n python3-libmount
%defattr(-, root, root)
%doc libmount/COPYING
%{_libdir}/python*/site-packages/libmount/*

%changelog
* Wed Apr 30 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.2-1m)
- update 2.24.2

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.1-2m)
- support UserMove env

* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.1-1m)
- update 2.24.1

* Wed Oct 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24-1m)
- update 2.24

* Thu May 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23.1-1m)
- update to 2.23.1

* Fri May 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23-1m)
- update to 2.23

* Tue Nov  1 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-3m)
- modify Obsoletes

* Fri Oct 28 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-2m)
- rename util-linux-ng to util-linux

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.1-1m)
- update 2.20.1

* Sun Sep 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19.1-3m)
- fix BTS #392

* Mon Jun 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.1-2m)
- add any patches

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.1-1m)
- update 2.19.1
-- include /etc/mtab

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18-3m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.18-2m)
- modify spec

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.18-1m)
- update 2.18

* Wed Mar 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.2-1m)
- update 2.17.2

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.1-1m)
- update 2.17.1

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.2-3m)
- rebuild against audit-2.0.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.16.2-2m)
- delete __libtoolize hack

* Sat Dec  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.2-1m)
- update 2.16.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.10-1m)
- sync Fedora
 +* Wed Sep 16 2009 Tomas Mraz <tmraz@redhat.com> - 2.16-10
 +- use password-auth common PAM configuration instead of system-auth and
 +  drop pam_console.so call from the remote PAM config file

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.2-4m)
- fix install-info

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.14.2-3m)
- define __libtoolize (build fix)

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.2-2m)
- remove arch to avoid conflicting with new coreutils

* Sat Mar 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.2-1m)
- update 2.14.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14-2m)
- update Patch0,2 for fuzz=0
- License: GPLv2+

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14-1m)
- update 2.14

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.1-4m)
- Requires: ConsoleKit-libs

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.1-2m)
- %%NoSource -> NoSource

* Thu Jan 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.1-1m)
- update 2.13.1 release

* Wed Jan  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.1-0.999.3m)
- update 2.13.1-rc2

* Mon Dec 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.13.1-0.999.2m)
- add Obsoletes and Provides of setarch

* Mon Dec 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.1-0.999.1m)
- rename package util-linux-ng
- update 2.13.1-rc1

* Sun Nov  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-8m)
- [SECURITY] CVE-2007-5191
- import security patch from Fedora

* Fri Apr 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13-7m)
- re-sync with Fedora, hal-0.5.9 needs

* Sun Feb 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13-6m)
- remove %%{_mandir}/man5/nfs.5* to avoid conflicting with nfs-utils

* Wed Feb 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13-5m)
- add PreReq: policycoreutils

* Sun Dec 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13-4m)
- add BuildRequires: info chkconfig

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13-3m)
- modify %%install to avoid conflicting with shadow-utils

* Sun May 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13-2m)
- revise %%files for rpm-4.4.2

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.13-1m)
- update to 2.13
- sync FC5

* Sun Feb 19 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12r-2m)
- change /var/log/lastlog perms to 0644
  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=167200

* Sun Oct 30 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.12r-1m)
- version 2.12r

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12q-4m)
- enable ia64
- fix typo

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12q-3m)
- enable ppc and other arch.

* Wed Jul 13 2005 Toru Hoshina <t@momonga-linux.org>
- (2.12q-2m)
- enable x86_64. util-linux-2.12q-pagesize.patch is modified a bit.

* Sun May 22 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.12q-1m)
- version up based on Fedora (2.12p-9.3)
- add Patch200 to avoid mismatch between kernel-headers and glibc-kernheaders
- still include 'newgrp'

  * Wed May  4 2005 Jeremy Katz <katzj@redhat.com> - 2.12p-9.3
  - rebuild against new libe2fsprogs (and libblkid) to fix cramfs auto-detection

  * Mon May  2 2005 Karel Zak <kzak@redhat.com> 2.12p-9
  - fix #156597 - look - doesn't work with separators

  * Mon Apr 25 2005 Karel Zak <kzak@redhat.com> 2.12p-8
  - fix #154498 - util-linux login & pam session
  - fix #155293 - man 5 nfs should include vers as a mount option
  - fix #76467 - At boot time, fsck chokes on LVs listed by label in fstab
  - new Source URL
  - added note about ATAPI IDE floppy to fdformat.8
  - fix #145355 - Man pages for fstab and fstab-sync in conflict

  * Tue Apr  5 2005 Karel Zak <kzak@redhat.com> 2.12p-7
  - enable build with libblkid from e2fsprogs-devel
  - remove workaround for duplicated labels

  * Thu Mar 31 2005 Steve Dickson <SteveD@RedHat.com> 2.12p-5
  - Fixed nfs mount to rollback correctly.

  * Fri Mar 25 2005 Karel Zak <kzak@redhat.com> 2.12p-4
  - added /var/log/lastlog to util-linux (#151635)
  - disabled 'newgrp' in util-linux (enabled in shadow-utils) (#149997, #151613)
  - improved mtab lock (#143118)
  - fixed ipcs typo (#151156)
  - implemented mount workaround for duplicated labels (#116300)

  * Fri Feb 25 2005 Steve Dickson <SteveD@RedHat.com> 2.12p-2
  - Changed nfsmount to only use reserve ports when necessary
    (bz# 141773)

  * Thu Dec 23 2004 Elliot Lee <sopwith@redhat.com> 2.12p-1
  - Update to util-linux-2.12p. This changes swap header format
    from - you may need to rerun mkswap if you did a clean install of
    FC3.

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.12i-3m)
- rebuild against libtermcap and ncurses

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12i-2m)
- fix *.pamd for multilib.

* Tue Nov 30 2004 Shingo Akagaki <dora@unya.org>
- (2.12i-1m)
  update to 2.12i

* Sat Sep  4 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.12b-1m)
- add Patch129: util-linux-2.12-partx-kernel2681.patch
- update Patch0: util-linux-2.12b-rhconfig.patch
- remove Patch70: util-linux-2.11z-miscfixes.patch
- remove Patch103: util-linux-2.11r-ownerumount.patch
- remove Patch116: util-linux-2.11n-loginutmp-66950.patch
- remove Patch123: util-linux-2.11y-blkgetsize-81069.patch
- remove Patch124: util-linux-2.11y-umount-75421.patch
- remove Patch127: util-linux-2.11y-mcookie-83345.patch

* Wed Jul 23 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.11z-2m)
- raise >> cfdisk
  
* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.11z-1m)
  update to 2.11z
  
* Fri Mar 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.11y-1m)
- update to 2.11y
- sync with rawhide

* Wed Feb  5 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.11r-5m)
- revise util-linux-2.11r-moremisc.patch(rename to util-linux-2.11r-moremisc.patch)
  fix typo in mount.8
  omit patch to cal.c (see [Momonga-devel.ja:01339])
     thanks to Shingo TAKEDA <stakeda--at--iris.eonet.ne.jp>.

* Fri Nov 29 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11r-4m)
- add version at requires chkconfig for alternatives

* Tue Nov 19 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (2.11r-3m)
- adopt the alternatives system (requries chkconfig, provides pager).

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.11r-2m)
- mount: remove Prereq: /bin/awk /usr/bin/cmp
- mount: add Prereq: gawk diffutils

* Sun Aug 25 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.11r-1m)
- update to 2.11r
- sync with rawhide
- change util-linux-2.11n-setpwrace.patch is merged into util-linux-2.11r-moremisc.patch
- add cryptoloop patch(momonga-util-linux-2.11r.patch)

* Wed Aug  7 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.11n-7m)
- [security] Add Patch116 (util-linux-2.11n-setpwrace.patch) 
  to fix setpwnam race
  Ref. https://rhn.redhat.com/errata/RHSA-2002-132.html

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (2.11n-6k)
- add some patches.

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.11n-4k)
- delete Requires: /etc/pam.d/system-auth
- change Prereq: /sbin/install-info   -> info

* Tue Jan 22 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11n-2k)
- update to 2.11n

* Thu Nov 08 2001 Motonobu Ichimura <famao@kondara.org>
- (2.11l-6k)
- rename cal-li18nux.patch to cal-i18n-0.1.patch and modify it.

* Wed Nov 07 2001 Motonobu Ichimura <famao@kondara.org>
- (2.11l-4k)
- add cal-li18nux.patch

* Tue Oct 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11l-2k)
- update to 2.11, for security fix

* Wed Oct  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11k-4k)
- errase Conflict tag

* Tue Oct  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11k-2k)
- update to 2.11k and merge to rh 2.11f-9

* Fri Sep 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10s-18k)
- add xfs.id.patch for XFS

* Mon Sep 24 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10s-16k)
- add jfs.id.patch for JFS

* Tue Jul 10 2001 Motonobu Ichimura <famao@digitalfactory.co.jp>
- (2.10s-14k)
- fixed pam.d

* Sun May 20 2001 Toru Hoshina <toru@df-usa.com>
- (2.10s-12k)
- force to use /bin/vi instead of /usr/bin/vi :-p

* Fri Apr 27 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10s-10k)
- modified the bug that display 1f for ReiserFS on bsdmode l command

* Sat Apr 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10s-8k)
- add ReiserFS table to bsd disk label

* Sat Apr 21 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.10s-6k)
  fixed compile option to '-O1 -mpowerpc -fsigned-char'.
  include fsck.minix and mkfs.minix.

* Wed Apr 11 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10s-4k)
- fixed specfile for ppc about %files section

* Thu Mar 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10s-2k)
- update to 2.10s
- add reiserfs.id.patch to determine 0x88 reiserfs

* Sun Dec 10 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10r-2k)
- update to 2.10r

* Tue Nov 14 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.10q

* Sat Oct 21 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10p-1k)
- update to 2.10p
- patch32 errased

* Wed Oct  4 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10o-2k)
- update to 2.10o for upper 2.4.0 kernel

* Thu Jul 21 2000 AYUHANA Tomonori <l@kondara.org>
- (2.10m-2k)
- add %patch30,31,32,33 (for nfs-utils)
- rebuild against glibc-2.1.91, X-4.0, rpm-3.0.5

* Thu Mar 02 2000 Shingo Akagaki <dora@kondara.org>
- use static link (more,cfdisk)
- remove unset LINGUAS || :

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-util-linux-20000115

* Sat Dec 25 1999 Tenkou N. Hattori <tnh@kondara.org>
- merge util-linux-2.10c-1

* Sat Dec  4 1999 Tenkou N. Hattori <tnh@kondara.org>
- delete %{_mandir}/ja/man8/halt.8

* Tue Nov 30 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-util-linux-19991115

* Sun Nov 11 1999 Norihito Ohmori <nono@kondara.org>
- be a NoSrc :-P
- fix ja.po

* Wed Oct 27 1999 Bill Nottingham <notting@redhat.com>
- ship hwclock on alpha.

* Tue Oct  5 1999 Bill Nottingham <notting@redhat.com>
- don't ship symlinks to rdev if we don't ship rdev.

* Tue Sep 07 1999 Cristian Gafton <gafton@redhat.com>
- add rawIO support from sct

* Mon Aug 30 1999 Preston Brown <pbrown@redhat.com>
- don't display "new mail" message when the only piece of mail is from IMAP

* Fri Aug 27 1999 Michael K. Johnson <johnsonm@redhat.com>
- kbdrate is now a console program

* Thu Aug 26 1999 Jeff Johnson <jbj@redhat.com>
- hostid is now in sh-utils. On sparc, install hostid as sunhostid (#4581).
- update to 2.9w:
-  Updated mount.8 (Yann Droneaud)
-  Improved makefiles
-  Fixed flaw in fdisk

* Tue Aug 10 1999 Jeff Johnson <jbj@redhat.com>
- tsort is now in textutils.

* Wed Aug  4 1999 Bill Nottingham <notting@redhat.com>
- turn off setuid bit on login. Again. :(

* Tue Aug  3 1999 Peter Jones, <pjones@redhat.com>
- hostid script for sparc (#3803).

* Tue Aug 03 1999 Christian 'Dr. Disk' Hechelmann <drdisk@tc-gruppe.de>
- added locale message catalogs to %file
- added patch for non-root build
- vigr.8 and /usr/lib/getopt  man-page was missing from file list
- /etc/fdprm really is a config file

* Fri Jul 23 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.9v:
-   cfdisk no longer believes the kernel's HDGETGEO
    (and may be able to partition a 2 TB disk)

* Fri Jul 16 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.9u:
-   Czech more.help and messages (Ji?? Pavlovsk?)
-   Japanese messages (Daisuke Yamashita)
-   fdisk fix (Klaus G. Wagner)
-   mount fix (Hirokazu Takahashi)
-   agetty: enable hardware flow control (Thorsten Kranzkowski)
-   minor cfdisk improvements
-   fdisk no longer accepts a default device
-   Makefile fix

* Tue Jul  6 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.9t:
-   national language support for hwclock
-   Japanese messages (both by Daisuke Yamashita)
-   German messages and some misc i18n fixes (Elrond)
-   Czech messages (Ji?? Pavlovsk?)
-   wall fixed for /dev/pts/xx ttys
-   make last and wall use getutent() (Sascha Schumann)
    [Maybe this is bad: last reading all of wtmp may be too slow.
     Revert in case people complain.]
-   documented UUID= and LABEL= in fstab.5
-   added some partition types
-   swapon: warn only if verbose

* Fri Jun 25 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.9s.

* Sat May 29 1999 Jeff Johnson <jbj@redhat.com>
- fix mkswap sets incorrect bits on sparc64 (#3140).

* Thu Apr 15 1999 Jeff Johnson <jbj@redhat.com>
- on sparc64 random ioctls on clock interface cause kernel messages.

* Thu Apr 15 1999 Jeff Johnson <jbj@redhat.com>
- improved raid patch (H.J. Lu).

* Wed Apr 14 1999 Michael K. Johnson <johnsonm@redhat.com>
- added patch for smartraid controllers

* Sat Apr 10 1999 Cristian Gafton <gafton@redhat.com>
- fix logging problems caused by setproctitle and PAM interaction
  (#2045)

* Wed Mar 31 1999 Jeff Johnson <jbj@redhat.com>
- include docs and examples for sfdisk (#1164)

* Mon Mar 29 1999 Matt Wilson <msw@redhat.com>
- rtc is not working properly on alpha, we can't use hwclock yet.

* Fri Mar 26 1999 Cristian Gafton <gafton@redhat.com>
- add patch to make mkswap more 64 bit friendly... Patch from
  eranian@hpl.hp.com (ahem!)

* Thu Mar 25 1999 Jeff Johnson <jbj@redhat.com>
- include sfdisk (#1164)
- fix write (#1784)
- use positive logic in spec file (%ifarch rather than %ifnarch).
- (re)-use 1st matching utmp slot if search by mypid not found.
- update to 2.9o
- lastb wants bad logins in wtmp clone /var/run/btmp (#884)

* Thu Mar 25 1999 Jakub Jelinek <jj@ultra.linux.cz>
- if hwclock is to be compiled on sparc,
  it must actually work. Also, it should obsolete
  clock, otherwise it clashes.
- limit the swap size in mkswap for 2.2.1+ kernels
  by the actual maximum size kernel can handle.
- fix kbdrate on sparc, patch by J. S. Connell
  <ankh@canuck.gen.nz>

* Wed Mar 24 1999 Matt Wilson <msw@redhat.com>
- added pam_console back into pam.d/login

* Tue Mar 23 1999 Matt Wilson <msw@redhat.com>
- updated to 2.9i
- added hwclock for sparcs and alpha

* Mon Mar 22 1999 Erik Troan <ewt@redhat.com>
- added vigr to file list

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 12)

* Thu Mar 18 1999 Cristian Gafton <gafton@redhat.com>
- remove most of the ifnarch arm stuff

* Mon Mar 15 1999 Michael Johnson <johnsonm@redhat.com>
- added pam_console.so to /etc/pam.d/login

* Thu Feb  4 1999 Michael K. Johnson <johnsonm@redhat.com>
- .perms patch to login to make it retain root in parent process
  for pam_close_session to work correctly

* Tue Jan 12 1999 Jeff Johnson <jbj@redhat.com>
- strip fdisk in buildroot correctly (#718)

* Mon Jan 11 1999 Cristian Gafton <gafton@redhat.com>
- have fdisk compiled on sparc and arm

* Mon Jan 11 1999 Erik Troan <ewt@redhat.com>
- added beos partition type to fdisk

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- incorporate fdisk on all arches

* Sat Dec  5 1998 Jeff Johnson <jbj@redhat.com>
- restore PAM functionality at end of login (Bug #201)

* Thu Dec 03 1998 Cristian Gafton <gafton@redhat.com>
- patch top build on the arm without PAM and related utilities, for now.
- build hwclock only on intel

* Wed Nov 18 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to version 2.9

* Thu Oct 29 1998 Bill Nottingham <notting@redhat.com>
- build for Raw Hide (slang-1.2.2)
- patch kbdrate wackiness so it builds with egcs

* Tue Oct 13 1998 Erik Troan <ewt@redhat.com>
- patched more to use termcap

* Mon Oct 12 1998 Erik Troan <ewt@redhat.com>
- added warning about alpha/bsd label starting cylinder

* Mon Sep 21 1998 Erik Troan <ewt@redhat.com>
- use sigsetjmp/siglongjmp in more rather then sig'less versions

* Fri Sep 11 1998 Jeff Johnson <jbj@redhat.com>
- explicit attrs for setuid/setgid programs

* Thu Aug 27 1998 Cristian Gafton <gafton@redhat.com>
- sln is now included in glibc

* Sun Aug 23 1998 Jeff Johnson <jbj@redhat.com>
- add cbm1581 floppy definitions (problem #787)

* Mon Jun 29 1998 Jeff Johnson <jbj@redhat.com>
- remove /etc/nologin at end of shutdown/halt.

* Fri Jun 19 1998 Jeff Johnson <jbj@redhat.com>
- add mount/losetup.

* Thu Jun 18 1998 Jeff Johnson <jbj@redhat.com>
- update to 2.8 with 2.8b clean up. hostid now defunct?

* Mon Jun 01 1998 David S. Miller <davem@dm.cobaltmicro.com>
- "more" now works properly on sparc

* Sat May 02 1998 Jeff Johnson <jbj@redhat.com>
- Fix "fdisk -l" fault on mounted cdrom. (prob #513)

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat Apr 11 1998 Cristian Gafton <gafton@redhat.com>
- manhattan rebuild

* Mon Dec 29 1997 Erik Troan <ewt@redhat.com>
- more didn't suspend properly on glibc
- use proper tc*() calls rather then ioctl's

* Sun Dec 21 1997 Cristian Gafton <gafton@redhat.com>
- fixed a security problem in chfn and chsh accepting too 
  long gecos fields

* Fri Dec 19 1997 Mike Wangsmo <wanger@redhat.com>
- removed "." from default path

* Tue Dec 02 1997 Cristian Gafton <gafton@redhat.com>
- added (again) the vipw patch

* Wed Oct 22 1997 Michael Fulbright <msf@redhat.com>
- minor cleanups for glibc 2.1

* Fri Oct 17 1997 Michael Fulbright <msf@redhat.com>
- added vfat32 filesystem type to list recognized by fdisk

* Fri Oct 10 1997 Erik Troan <ewt@redhat.com>
- don't build clock on the alpha 
- don't install chkdupexe

* Thu Oct 02 1997 Michael K. Johnson <johnsonm@redhat.com>
- Update to new pam standard.
- BuildRoot.

* Thu Sep 25 1997 Cristian Gafton <gafton@redhat.com>
- added rootok and setproctitle patches
- updated pam config files for chfn and chsh

* Tue Sep 02 1997 Erik Troan <ewt@redhat.com>
- updated MCONFIG to automatically determine the architecture
- added glibc header hacks to fdisk code
- rdev is only available on the intel

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
- update to util-linux 2.7, fixed login problems

* Wed Jun 25 1997 Erik Troan <ewt@redhat.com>
- Merged Red Hat changes into main util-linux source, updated package to
  development util-linux (nearly 2.7).

* Tue Apr 22 1997 Michael K. Johnson <johnsonm@redhat.com>
- LOG_AUTH --> LOG_AUTHPRIV in login and shutdown

* Mon Mar 03 1997 Michael K. Johnson <johnsonm@redhat.com>
- Moved to new pam and from pam.conf to pam.d

* Tue Feb 25 1997 Michael K. Johnson <johnsonm@redhat.com>
- pam.patch differentiated between different kinds of bad logins.
  In particular, "user does not exist" and "bad password" were treated
  differently.  This was a minor security hole.
