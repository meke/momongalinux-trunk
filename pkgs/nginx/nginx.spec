%global momorel 3

%define nginx_user      nginx
%define nginx_group     %{nginx_user}
%define nginx_home      %{_localstatedir}/lib/nginx
%define nginx_home_tmp  %{nginx_home}/tmp
%define nginx_logdir    %{_localstatedir}/log/nginx
%define nginx_confdir   %{_sysconfdir}/nginx
%define nginx_datadir   %{_datadir}/nginx
%define nginx_webroot   %{nginx_datadir}/html

Name:           nginx
Version:        1.4.4
Release:        %{momorel}m%{?dist}
Summary:        Robust, small and high performance HTTP and reverse proxy server
Group:          System Environment/Daemons   

# BSD License (two clause)
# http://www.freebsd.org/copyright/freebsd-license.html
License:        Modified BSD
URL:            http://nginx.net/ 
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:      pcre-devel >= 8.31,zlib-devel,openssl-devel >= 1.0.0,perl-devel,perl-ExtUtils-Embed
Requires:           pcre,zlib,openssl
Requires:           perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
# for /usr/sbin/useradd
Requires(pre):      shadow-utils
Requires(post):     chkconfig
# for /sbin/service
Requires(preun):    chkconfig, initscripts
Requires(postun):   initscripts

Source0:    http://nginx.org/download/nginx-%{version}.tar.gz
NoSource:   0
Source1:    %{name}.service
Source2:    %{name}.logrotate
Source3:    virtual.conf
Source4:    ssl.conf
Source5:    %{name}.sysconfig
Source6:    nginx.conf
Source100:  index.html
Source101:  poweredby.png
Source102:  nginx-logo.png
Source103:  50x.html
Source104:  404.html

# removes -Werror in upstream build scripts.  -Werror conflicts with
# -D_FORTIFY_SOURCE=2 causing warnings to turn into errors.
Patch0:     nginx-auto-cc-gcc.patch

%description
Nginx [engine x] is an HTTP(S) server, HTTP(S) reverse proxy and IMAP/POP3
proxy server written by Igor Sysoev.

%prep
%setup -q

%patch0 -p0

%build
# nginx does not utilize a standard configure script.  It has its own
# and the standard configure options cause the nginx configure script
# to error out.  This is is also the reason for the DESTDIR environment
# variable.  The configure script(s) have been patched (Patch1 and
# Patch2) in order to support installing into a build environment.
export DESTDIR=%{buildroot}
./configure \
    --user=%{nginx_user} \
    --group=%{nginx_group} \
    --prefix=%{nginx_datadir} \
    --sbin-path=%{_sbindir}/%{name} \
    --conf-path=%{nginx_confdir}/%{name}.conf \
    --error-log-path=%{nginx_logdir}/error.log \
    --http-log-path=%{nginx_logdir}/access.log \
    --http-client-body-temp-path=%{nginx_home_tmp}/client_body \
    --http-proxy-temp-path=%{nginx_home_tmp}/proxy \
    --http-fastcgi-temp-path=%{nginx_home_tmp}/fastcgi \
    --http-uwsgi-temp-path=%{nginx_home_tmp}/uwsgi \
    --http-scgi-temp-path=%{nginx_home_tmp}/scgi \
    --pid-path=%{_localstatedir}/run/%{name}.pid \
    --lock-path=%{_localstatedir}/lock/subsys/%{name} \
    --with-http_ssl_module \
    --with-http_realip_module \
    --with-http_addition_module \
    --with-http_xslt_module \
    --with-http_image_filter_module \
    --with-http_geoip_module \
    --with-http_sub_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_gzip_static_module \
    --with-http_random_index_module \
    --with-http_secure_link_module \
    --with-http_degradation_module \
    --with-http_stub_status_module \
    --with-http_perl_module \
    --with-mail \
    --with-file-aio \
    --with-mail_ssl_module \
    --with-ipv6 \
    --with-cc-opt="%{optflags} $(pcre-config --cflags)" \
    --with-ld-opt="-Wl,-E" # so the perl module finds its symbols
make %{?_smp_mflags} 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALLDIRS=vendor
find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} \;
find %{buildroot} -type f -empty -exec rm -f {} \;
find %{buildroot} -type f -exec chmod 0644 {} \;
find %{buildroot} -type f -name '*.so' -exec chmod 0755 {} \;
chmod 0755 %{buildroot}%{_sbindir}/nginx
%{__install} -p -D -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service
%{__install} -p -D -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
%{__install} -p -D -m 0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/conf.d
%{__install} -p -m 0644 %{SOURCE3} %{SOURCE4} %{buildroot}%{nginx_confdir}/conf.d
%{__install} -p -m 0644 %{SOURCE6} %{buildroot}%{nginx_confdir}
%{__install} -p -d -m 0755 %{buildroot}%{nginx_home_tmp}
%{__install} -p -d -m 0755 %{buildroot}%{nginx_logdir}
%{__install} -p -d -m 0755 %{buildroot}%{nginx_webroot}
%{__install} -p -m 0644 %{SOURCE100} %{SOURCE101} %{SOURCE102} %{SOURCE103} %{SOURCE104} %{buildroot}%{nginx_webroot}

# convert to UTF-8 all files that give warnings.
for textfile in CHANGES
do
    mv $textfile $textfile.old
    iconv --from-code ISO8859-1 --to-code UTF-8 --output $textfile $textfile.old
    rm -f $textfile.old
done

%clean
rm -rf %{buildroot}

%pre
if [ $1 -eq 1 ]; then
    getent group %{nginx_group} > /dev/null || groupadd -r %{nginx_group}
    getent passwd %{nginx_user} > /dev/null || \
        useradd -r -d %{nginx_home} -g %{nginx_group} \
        -s /sbin/nologin -c "Nginx web server" %{nginx_user}
    exit 0
fi

%post
if [ $1 -eq 1 ]; then
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%triggerun -- nginx < 1:1.0.12-2
if /sbin/chkconfig --level 3 nginx; then
    /bin/systemctl enable nginx.service >/dev/null 2>&1 || :
fi
/sbin/chkconfig --del nginx >/dev/null 2>&1 || :
/bin/systemctl try-restart nginx.service >/dev/null 2>&1 || :

%preun
if [ $1 -eq 0 ]; then
    /bin/systemctl --no-reload disable nginx.service >/dev/null 2>&1 || :
    /bin/systemctl stop nginx.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -gt 1 ]; then
    /bin/systemctl try-restart nginx.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%doc LICENSE CHANGES README
%{nginx_datadir}/
%{_sbindir}/%{name}
%{_mandir}/man3/%{name}.3pm*
%{_unitdir}/%{name}.service
%dir %{nginx_confdir}
%dir %{nginx_confdir}/conf.d
%dir %{nginx_logdir}
%config(noreplace) %{nginx_confdir}/conf.d/*.conf
%config(noreplace) %{nginx_confdir}/win-utf
%config(noreplace) %{nginx_confdir}/%{name}.conf.default
%config(noreplace) %{nginx_confdir}/mime.types.default
%config(noreplace) %{nginx_confdir}/fastcgi.conf
%config(noreplace) %{nginx_confdir}/fastcgi.conf.default
%config(noreplace) %{nginx_confdir}/fastcgi_params
%config(noreplace) %{nginx_confdir}/fastcgi_params.default
%config(noreplace) %{nginx_confdir}/scgi_params
%config(noreplace) %{nginx_confdir}/scgi_params.default
%config(noreplace) %{nginx_confdir}/uwsgi_params
%config(noreplace) %{nginx_confdir}/uwsgi_params.default
%config(noreplace) %{nginx_confdir}/koi-win
%config(noreplace) %{nginx_confdir}/koi-utf
%config(noreplace) %{nginx_confdir}/%{name}.conf
%config(noreplace) %{nginx_confdir}/mime.types
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%dir %{perl_vendorarch}/auto/%{name}
%{perl_vendorarch}/%{name}.pm
%{perl_vendorarch}/auto/%{name}/%{name}.so
%attr(-,%{nginx_user},%{nginx_group}) %dir %{nginx_home}
%attr(-,%{nginx_user},%{nginx_group}) %dir %{nginx_home_tmp}

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-2m)
- rebuild against perl-5.18.2

* Mon Nov 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- [SECURITY] CVE-2013-4547
- update to 1.4.4

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-2m)
- rebuild against perl-5.18.0

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- [SECURITY] CVE-2013-2028
- update to 1.4.1

* Sat Apr 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-2m)
- rebuild against perl-5.16.3

* Sun Nov 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-2m)
- rebuild against perl-5.16.2

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.15-4m)
- rebuild against pcre-8.31

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.15-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.15-2m)
- rebuild against perl-5.16.0

* Fri Apr 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.15-1m)
- [SECURITY] CVE-2012-1180 CVE-2012-2089
- support systemd
- update to 1.0.15

* Wed Mar 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.14-1m)
- [SECURITY] CVE-2012-1180
- Upgrade to 1.0.14

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.11-1m)
- Upgrade to 1.0.11

* Mon Nov 22 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (1.0.10-1m)
- Upgrade to 1.0.10

* Mon Nov 7 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (1.0.8-2m)
- s/Fedora/Momonga Linux/g on index.html

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.8-1m)
- Upgrade to 1.0.8

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-2m)
- rebuild against perl-5.14.2

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.5-1m)
- Upgrade to 1.0.5

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May 1 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.0.0-1m)
- Upgrade to 1.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.67-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.67-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.67-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.67-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.67-1m)
- sync with Fedora 13 (0.7.67-1)

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.64-4m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.64-3m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.64-2m)
- rebuild against openssl-1.0.0

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.64-1m)
- [SECURITY] CVE-2009-3555
- update to 0.7.64

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.63-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.63-1m)
- [SECURITY] CVE-2009-3898
- update to 0.7.63

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.62-1m)
- [SECURITY] CVE-2009-2629 CVE-2009-3896
- update to 0.7.62

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.61-2m)
- rebuild against perl-5.10.1

* Tue Aug 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.61-1m)
- import from Fedora to Momonga

* Sun Aug 02 2009 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.7.61-1
- Update to new stable 0.7.61

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.36-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun May 17 2009 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.36-2
- init script updates from Gena Makhomed
- remove nginx-upstream-fair

* Sat Apr 11 2009 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.36-1
-  update to 0.6.36

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.35-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 19 2009 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.35-2
- rebuild

* Thu Feb 19 2009 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.35-1
- update to 0.6.35

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> - 0.6.34-2
- rebuild with new openssl

* Tue Dec 30 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.34-1
- update to 0.6.34

* Thu Dec  4 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 0.6.33-2
- Fix inclusion of /usr/share/nginx tree => no unowned directories.

* Sun Nov 23 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.33-1
- update to 0.6.33

* Tue Jul 22 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.32-1
- update to 0.6.32
- nginx now supports DESTDIR so removed the patches that enabled it 

* Mon May 26 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.31-3
- init script fixes
- resolve 'listen 80 default' [#447873]

* Mon May 12 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.31-2
- update to 0.6.31

* Sun May 11 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.6.30-2
- upate to new upstream stable branch 0.6
- added 3rd party module nginx-upstream-fair
- added default webpages

* Sun Apr 20 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.5.35-2
- update init script to match recommended guidelines
- add /etc/nginx/conf.d support [#443280]
- use /etc/sysconfig/nginx to determine nginx.conf [#442708]

* Tue Mar 18 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.5.35-3
- add Requires for versioned perl (libperl.so)
- drop silly file Requires

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.5.35-2
- Autorebuild for GCC 4.3

* Sat Jan 19 2008 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.5.35-1
- update to 0.5.35

* Sat Dec 15 2007 Jeremy Hinegardner <jeremy at hinegardner dot org> - 0.5.34-1
- update to 0.5.34

* Wed Dec 05 2007 Release Engineering <rel-eng at fedoraproject dot org> - 0.5.33-2
 - Rebuild for deps

* Sun Nov 11 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.33-1
- update to 0.5.33

* Mon Sep 24 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.32-1
- updated to 0.5.32
- fixed rpmlint UTF-8 complaints.

* Sat Aug 18 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.31-2
- added --with-http_stub_status_module build option.
- added --with-http_sub_module build option.
- added use of pcre-config --cflags

* Fri Aug 17 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.31-1
- Update to 0.5.31
- specify license is BSD

* Sat Aug 11 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.30-2
- Add BuildRequires: perl-devel - fixing rawhide build

* Mon Jul 30 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.30-1
- Update to 0.5.30

* Tue Jul 24 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.29-1
- Update to 0.5.29

* Wed Jul 18 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.28-1
- Update to 0.5.28

* Mon Jul 09 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.27-1
- Update to 0.5.27

* Mon Jun 18 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.26-1
- Update to 0.5.26

* Sat Apr 28 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.19-1
- Update to 0.5.19

* Mon Apr 02 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.17-1
- Update to 0.5.17

* Mon Mar 26 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.16-1
- Update to 0.5.16
- add ownership of /usr/share/nginx/html (#233950)

* Fri Mar 23 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.15-3
- fixed package review bugs (#235222) given by ruben@rubenkerkhof.com

* Thu Mar 22 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.15-2
- fixed package review bugs (#233522) given by kevin@tummy.com

* Thu Mar 22 2007 Jeremy Hinegardner <jeremy@hinegardner.org> - 0.5.15-1
- create patches to assist with building for Fedora
- initial packaging for Fedora
