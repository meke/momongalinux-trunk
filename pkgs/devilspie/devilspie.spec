%global momorel 9

Name: devilspie
Summary: A window matching tool inspired by the Matched Window options in Sawfish
Version: 0.22
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.burtonini.com/blog/computers/devilspie
Source0: http://www.burtonini.com/computing/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: devilspie-0.22-setlocale.patch
Patch1: devilspie-0.22-gtk222.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gob2 >= 2.0.4
BuildRequires: libwnck-devel >= 2.20.0
BuildRequires: libglade-devel
BuildRequires: gtk2-devel

%description
A window-matching utility, inspired by Sawfish's "Matched Windows" option and the lack of the functionality in Metacity. Metacity lacking window matching is not a bad thing -- Metacity is a lean window manager, and window matching does not have to be a window manager task.

Devil's Pie can be configured to detect windows as they are created, and match the window to a set of rules. If the window matches the rules, it can perform a series of actions on that window. For example, one can make all windows created by X-Chat appear on all workspaces, and the main Gkrellm1 window does not appear in the pager or task list.

%prep
%setup -q
%patch0 -p1 -b .locale
%patch1 -p1 -b .gtk222

%build
autoreconf
%configure LIBS="-lX11"
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/devilspie
%makeinstall

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README NEWS TODO
%dir %{_sysconfdir}/devilspie
%{_bindir}/*
%{_mandir}/man*/*

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-9m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-7m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22-6m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-5m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-4m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against rpm-4.6

* Sat May 24 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.22-1m)
- update to 0.22
- add patch0 (from http://d.hatena.ne.jp/khiker/20080519/devilspie)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17.1-3m)
- %%NoSource -> NoSource

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17.1-2m)
- rebuild against libwnck-2.20.0

* Fri Jun 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.17.1-1m)
- import to Momonga

* Mon Sep 26 2005 Lars R. Damerow <lars@pixar.com> 0.13
- update to 0.13

* Fri Sep 16 2005 Lars R. Damerow <lars@pixar.com> 0.11
- update to 0.11
- removed setwintype patch

* Fri Sep 16 2005 Lars R. Damerow <lars@pixar.com> 0.10-2
- patch to fix setwintype crash

* Tue Aug 16 2005 Lars R. Damerow <lars@pixar.com> 0.10-1
- update to 0.10

* Mon Feb 07 2005 Lars R. Damerow <lars@pixar.com> 0.8-2
- update to 0.8
- add devilspie-reference.html

* Fri May  2 2003 Frederic Crozat <fcrozat@mandrakesoft.com>
- Package locale files

* Thu May 01 2003 Ross Burton <ross@burtonini.com>
- Initial spec file based on spec files by Michael Raab
  <mraab@macbyte.info> and Lars R. Damerow <lars@oddment.org>
