%global         momorel 1

Summary:        The Xapian Probabilistic Information Retrieval Library
Name:           xapian-core
Version:        1.2.17
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Databases
URL:            http://www.xapian.org/
Requires:       %{name}-libs = %{version}
Source0:        http://www.oligarchy.co.uk/xapian/%{version}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  zlib-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Xapian is an Open Source Probabilistic Information Retrieval Library. It
offers a highly adaptable toolkit that allows developers to easily add advanced
indexing and search facilities to applications

%package libs
Summary: Xapian search engine libraries
Group: System Environment/Libraries

%description libs
Xapian is an Open Source Probabilistic Information Retrieval framework. It
offers a highly adaptable toolkit that allows developers to easily add advanced
indexing and search facilities to applications. This package provides the
libraries for applications using Xapian functionality

%package devel
Group: Development/Libraries
Summary: Files needed for building packages which use Xapian
Requires: %{name} = %{version}
Requires: %{name}-libs = %{version}

%description devel
Xapian is an Open Source Probabilistic Information Retrieval framework. It
offers a highly adaptable toolkit that allows developers to easily add advanced
indexing and search facilities to applications. This package provides the
files needed for building packages which use Xapian

%prep
%setup -q -n %{name}-%{version}

%build
# FC6 (at least) has a patched libtool which knows not to set rpath for
# /usr/lib64, which upstream libtool fails to do currently.  We can drop
# this "autoreconf --force" and the "BuildRequires:" for the autotools
# once upstream libtool is fixed.  Note: this overwrites INSTALL, but
# that doesn't matter here as we don't package it.
autoreconf --force --install
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
# makeinstall doesn't work properly with libtool built libraries
make DESTDIR=%{buildroot} install
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
# Move the docs to the right place
mv %{buildroot}%{_datadir}/doc/%{name} %{buildroot}%{_datadir}/doc/%{name}-devel-%{version}
# Copy HACKING now, as "%doc HACKING" would overwrite everything
cp HACKING %{buildroot}%{_datadir}/doc/%{name}-devel-%{version}
# Copy the rest while we are in this directory
mkdir -p %{buildroot}%{_datadir}/doc/%{name}-%{version}
cp AUTHORS ChangeLog COPYING NEWS PLATFORMS README %{buildroot}%{_datadir}/doc/%{name}-%{version}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_bindir}/xapian-check
%{_bindir}/xapian-chert-update
%{_bindir}/xapian-compact
%{_bindir}/xapian-inspect
%{_bindir}/xapian-metadata
%{_bindir}/xapian-progsrv
%{_bindir}/xapian-replicate
%{_bindir}/xapian-replicate-server
%{_bindir}/xapian-tcpsrv
%{_bindir}/quest
%{_bindir}/delve
%{_bindir}/copydatabase
%{_bindir}/simpleindex
%{_bindir}/simplesearch
%{_bindir}/simpleexpand
%doc %{_datadir}/doc/%{name}-%{version}
# man pages may be gzipped, hence the trailing wildcard.
%{_mandir}/man1/xapian-check.1*
%{_mandir}/man1/xapian-chert-update.1*
%{_mandir}/man1/xapian-compact.1*
%{_mandir}/man1/xapian-inspect.1*
%{_mandir}/man1/xapian-metadata.1*
%{_mandir}/man1/xapian-progsrv.1*
%{_mandir}/man1/xapian-replicate.1*
%{_mandir}/man1/xapian-replicate-server.1*
%{_mandir}/man1/xapian-tcpsrv.1*
%{_mandir}/man1/quest.1*
%{_mandir}/man1/delve.1*
%{_mandir}/man1/copydatabase.1*

%files libs
%defattr(-, root, root)
%{_libdir}/libxapian.so.*

%files devel
%defattr(-, root, root)
%{_bindir}/xapian-config
%{_includedir}/xapian
%{_includedir}/xapian.h
%{_libdir}/libxapian.so
%{_libdir}/cmake/xapian
%{_datadir}/aclocal/xapian.m4
%doc %{_datadir}/doc/%{name}-devel-%{version}
# man pages may be gzipped, hence the trailing wildcard.
%{_mandir}/man1/xapian-config.1*

%changelog
* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.17-1m)
- update to 1.2.17

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.16-1m)
- update to 1.2.16

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15-1m)
- update to 1.2.15

* Sun Mar 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.14-1m)
- update to 1.2.14

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.13-1m)
- update to 1.2.13

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.10-1m)
- update to 1.2.10

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.9-1m)
- update to 1.2.9

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8

* Fri Aug 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-2m)
- rebuild for new GCC 4.6

* Tue Dec 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-2m)
- rebuild for new GCC 4.5

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Tue Jun 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.21-1m)
- update to 1.0.21

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.20-1m)
- update to 1.0.20

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.19-1m)
- update to 1.0.19

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.18-1m)
- update to 1.0.18

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.17-1m)
- update to 1.0.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.16-1m)
- update to 1.0.16

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.14-1m)
- update to 1.0.14

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.13-1m)
- update to 1.0.13

* Fri May 22 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.12-2m)
- add install option to autoreconf command

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.12-1m)
- update to 1.0.12

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against rpm-4.6

* Fri Jul 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-1m)
- import from Fedora to Momonga for perl-Search-Xapian use xapian-config

* Sun Mar 30 2008 Adel Gadllah <adel.gadllah@gmail.com> 1.0.6-1
- Update to 1.0.6

* Sat Feb 09 2008 Adel Gadllah <adel.gadllah@gmail.com> 1.0.5-2
- Rebuild for gcc-4.3

* Thu Dec 27 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.5-1
- Update to 1.0.5

* Tue Oct 30 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.4-1
- Update to 1.0.4

* Fri Oct 25 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-7
- Fix up multilib patch

* Thu Oct 25 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-6
- Fix multilib conflict in devel package (RH #343471)

* Tue Aug 21 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-5
- Rebuild for BuildID and ppc32 bug

* Wed Aug 08 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-4
- Add disttag

* Wed Aug 08 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-3
- Bump to avoid tag conflict

* Wed Aug 08 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-2
- Add missing files
- Minor cleanups

* Wed Aug 08 2007 Adel Gadllah <adel.gadllah@gmail.com> 1.0.2-1
- Update to 1.0.2
- Fix License tag

* Sat Jun 16 2007 Marco Pesenti Gritti <mpg@redhat.com> 1.0.1-1
- Update to 1.0.1

* Tue May  8 2007 Marco Pesenti Gritti <mpg@redhat.com> 0.9.10-2.2.svn8397
- Initial build
