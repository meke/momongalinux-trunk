%global momorel 7

Summary: A library of gtk+ widgets
Name: gtk+extra
Version: 2.1.2
Release: %{momorel}m%{?dist}
License: LGPLv2
URL: http://gtkextra.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/project/gtkextra/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: gtk+extra-2.1.2-gtk222.patch
Patch1: gtk+extra-2.1.2-make382.patch
Patch2: gtk+extra-2.1.2-configure.patch
Patch3: gtk+extra-2.1.2-marshal.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: gtk-doc
BuildRequires: gtk2-devel
BuildRequires: pkgconfig

%description
A library of dynamically linked gtk+ widgets including:
GtkSheet, GtkPlot, and GtkIconList

%package devel
Summary: Header files and static libraries from gtk+extra
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on gtk+extra.

%prep
%setup -q
%patch0 -p1 -b .gtk222~
%patch1 -p1 -b .make~
%patch2 -p0 -b .configure
%patch3 -p0 -b .marshal

# fix permission
chmod 644 ChangeLog

%build
autoreconf -ivf
%configure CPPFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_libdir}/libgtkextra-x11-2.0.so.*

%files devel
%defattr(-,root,root)
%doc docs/reference docs/tutorial
%doc docs/COPYING docs/HELP docs/README docs/TODO
%doc docs/gtk*.ChangeLog
%{_includedir}/gtkextra-2.0
%{_libdir}/pkgconfig/gtkextra-2.0.pc
%{_libdir}/libgtkextra-x11-2.0.a
%{_libdir}/libgtkextra-x11-2.0.so
%{_datadir}/gtk-doc/html/gtkextra

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-7m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.2-6m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.2-5m)
- add make-3.8.2 patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.2-4m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-3m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-2m)
- full rebuild for mo7 release

* Wed Mar 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.2-1m)
- version 2.1.2
- remove merged item.patch
- License: LGPLv2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-6m)
- apply a patch for gtk+-2.18

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.1.1-5m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-2m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.1-1m)
- initial package for gpsim-0.22.0
