%global         momorel 2
%global         srcver 1.91

Name:           perl-BDB
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Summary:        Asynchronous Berkeley DB access
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/BDB/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/BDB-%{srcver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-common-sense
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  db4-devel
BuildRequires:  libbsd-devel
Requires:       perl-common-sense
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
See the BerkeleyDB documentation (http://www.oracle.com/technology/documentation/berkeley-
db/db/index.html). The BDB API is very similar to the C API (the
translation has been very faithful).

%prep
%setup -q -n BDB-%{srcver}

%build
# perl-BDB doesn't support libdb
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" \
	  INC="`pkg-config libbsd-overlay --cflags` -I/usr/include/db4" \
	  LIBS="-L/%{_libdir}/db4 -ldb"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README
%{perl_vendorarch}/auto/BDB
%{perl_vendorarch}/BDB.*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.91-2m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.91-1m)
- update to 1.91

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-2m)
- rebuild against perl-5.16.0

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-1m)
- update to 1.9

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.89-3m)
- rebuild against perl-5.14.2

* Sun Sep 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.89-2m)
- use db4 instead of libdb4

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.89-1m)
- update to 1.89

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.88-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.88-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.88-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.88-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.88-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.88-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.88-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.88-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.88-1m)
- update to 1.88

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.87-2m)
- rebuild against db-4.8.26

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.87-1m)
- update to 1.87

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.86-1m)
- update to 1.86

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.85-1m)
- update to 1.85

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.84-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.84-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.84-1m)
- update to 1.84

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.83-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.83-1m)
- update to 1.83

* Wed Jan  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.82-1m)
- update to 1.82

* Tue Oct 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.81-1m)
- update to 1.81

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.801-2m)
- rebuild against db4-4.7.25-1m

* Tue Sep 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.801-1m)
- update to 1.801

* Sat Sep 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-1m)
- update to 1.8

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- update to 1.71

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.45-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.43-1m)
- update to 1.43

* Wed Dec 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Sun Dec 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
