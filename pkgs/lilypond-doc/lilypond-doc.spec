%global momorel 1
%global srcrel 1

Summary: HTML documentation for LilyPond
Name: lilypond-doc
Version: 2.18.2
Release: %{momorel}m%{?dist}
License: GPLv3
URL: http://www.lilypond.org/
Group: Applications/Publishing
Source0: http://download.linuxaudio.org/lilypond/binaries/documentation/lilypond-%{version}-%{srcrel}.documentation.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: lilypond >= %{version}
Requires: filesystem
BuildArch: noarch

%description
LilyPond is an automated music engraving system. It formats music
beautifully and automatically, and has a friendly syntax for its input
files.

This package contains the HTML documentation for LilyPond.

%prep
%setup -q -c

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}

# omf
mkdir -p %{buildroot}%{_datadir}/omf/lilypond/%{version}
install -m 644 share/omf/lilypond/%{version}/*.omf %{buildroot}%{_datadir}/omf/lilypond/%{version}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc license/lilypond-doc share/doc/lilypond/html
%{_datadir}/omf/lilypond

%changelog
* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.2-1m)
- version 2.18.2

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16.2-2m)
- update to 2.16.2

* Fri Sep 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.2-1m)
- version 2.14.2
- change License: to GPLv3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.3-2m)
- full rebuild for mo7 release

* Sat Jan  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.3-1m)
- version 2.12.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jan 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.2-1m)
- version 2.12.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.1-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.1-1m)
- version 2.12.1

* Mon Dec 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.0-1m)
- version 2.12.0

* Sun Dec  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.65-1m)
- version 2.11.65

* Thu Nov 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.64-1m)
- version 2.11.64

* Fri Oct 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.63-1m)
- version 2.11.63

* Sat Oct 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.62-1m)
- version 2.11.62

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.61-1m)
- version 2.11.61

* Fri Sep 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.60-1m)
- version 2.11.60

* Mon Sep 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.59-1m)
- version 2.11.59

* Sat Sep 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.58-1m)
- version 2.11.58

* Wed Aug 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.57-1m)
- version 2.11.57

* Sun Aug 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.56-1m)
- version 2.11.56

* Wed Aug  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.55-1m)
- version 2.11.55

* Sat Aug  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.54-1m)
- version 2.11.54

* Wed Jul 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.53-1m)
- version 2.11.53

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.52-1m)
- version 2.11.52

* Fri Jul 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.51-1m)
- version 2.11.51

* Thu Jul  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.50-1m)
- version 2.11.50

* Sat Jun 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.49-1m)
- version 2.11.49

* Mon Jun  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.48-1m)
- version 2.11.48

* Sun Jun  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.47-1m)
- version 2.11.47

* Mon May 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.46-1m)
- version 2.11.46

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.45-1m)
- version 2.11.45

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.43-1m)
- version 2.11.43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.42-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.42-1m)
- version 2.11.42

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.41-1m)
- version 2.11.41

* Tue Feb 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.40-1m)
- version 2.11.40

* Sun Feb 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.39-1m)
- version 2.11.39

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.37-1m)
- version 2.11.37

* Tue Jan  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.36-1m)
- version 2.11.36

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.35-1m)
- version 2.11.35

* Wed Oct  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.34-1m)
- version 2.11.34

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.33-1m)
- version 2.11.33

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.32-1m)
- version 2.11.32

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.30-1m)
- version 2.11.30

* Wed Aug 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.29-1m)
- version 2.11.29

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.28-1m)
- version 2.11.28

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.27-1m)
- version 2.11.27

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.26-1m)
- version 2.11.26

* Tue May 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.25-1m)
- version 2.11.25

* Sat May 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.24-1m)
- version 2.11.24

* Fri May  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.23-1m)
- version 2.11.23

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.22-1m)
- version 2.11.22

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.20-1m)
- initial package for lilypond-2.11.20
- Summary and %%description are imported from Fedora
