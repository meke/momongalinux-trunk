%global momorel 1

Name:		kyotocabinet
Summary:	a straightforward implementation of DBM
Version:	1.2.76
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Libraries
URL:		http://fallabs.com/kyotocabinet/
Source0:	http://fallabs.com/kyotocabinet/pkg/%{name}-%{version}.tar.gz 
NoSource:       0

BuildRequires: zlib-devel, bzip2-devel
BuildRequires: lzo-devel, xz-devel
BuildRequires: sed

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Kyoto Cabinet is a library of routines for managing a database. 
The database is a simple data file containing records, each is 
a pair of a key and a value. Every key and value is serial bytes 
with variable length. Both binary data and character string can 
be used as a key and a value. Each key must be unique within a database. 

%prep
%setup -q

sed -i 's/-march=native//g' configure*

%build
%configure --libexecdir=%{_libdir} --enable-lzo --enable-lzma
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

#mv %{buildroot}/%{_datadir}/doc/kyotocabinet{,-version}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_mandir}/*/*
%{_datadir}/doc/kyotocabinet*
# Todo: split -devel package
%{_includedir}/*
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Aug  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.76-1m)
- update 1.2.76

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.74-1m)
- update 1.2.74

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.72-1m)
- update 1.2.72

* Wed Nov  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.70-2m)
- remove -march=native from flags to enable build

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.70-1m)
- update 1.2.70

* Mon Aug  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.69-1m)
- update 1.2.69

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.34-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.34-1m)
- update 1.2.34

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.19-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.19-1m)
- Initial Commit Momonga Linux

