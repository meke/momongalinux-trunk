%global         momorel 1

Name:           perl-FindBin-libs
Version:        1.9
Epoch:          1
Release:        %{momorel}m%{?dist}
Summary:        FindBin::libs perl module
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/FindBin-libs/
Source0:        http://www.cpan.org/authors/id/L/LE/LEMBARK/FindBin-libs-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl-Cwd
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Locate and 'use lib' directories along the path of $FindBin::Bin to automate locating modules. 
Uses File::Spec and Cwd's abs_path to accomodate multiple O/S and redundant symlinks

%prep
%setup -q -n FindBin-libs-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(v5.12)/d' \
  sed -e '/perl(v5.10)/d'

EOF
%define __perl_requires %{_builddir}/FindBin-libs-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES README
%{perl_vendorlib}/FindBin/libs.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.9-1m)
- rebuild against perl-5.20.0
- update to 1.9

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.8-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.8-2m)
- rebuild against perl-5.18.1

* Thu May 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.8-1m)
- update to 1.8

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.7-2m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.7-1m)
- update to 1.7
- rebuild against perl-5.16.3

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.65.2-1m)
- update to 1.65.2

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.65.1-1m)
- update to 1.65.1

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-1m)
- update to 1.64

* Wed Dec 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60

* Sat Dec 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.59-1m)
- update to 1.59

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-1m)
- update to 1.55

* Tue Nov 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga^linux.org>
- (1.53-1m)
- update to 1.53

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.51-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.51-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-2m)
- rebuild against perl-5.10.1

* Fri Jun 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.37-2m)
- modify BuildRequires

* Mon Jun 01 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (1.37-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
