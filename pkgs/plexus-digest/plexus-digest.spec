%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# If you don't want to build with maven, and use straight ant instead,
# give rpmbuild option '--without maven'

%define with_maven 1
%define without_maven 0

%define parent plexus
%define subname digest

Name:           plexus-digest
Version:        1.1
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus Digest / Hashcode Components
License:        "ASL 2.0"
Group:          Development/Libraries
URL:            http://plexus.codehaus.org/plexus-components/plexus-digest/
Source0:        %{name}-%{version}-src.tar.gz
# svn export http://svn.codehaus.org/plexus/plexus-components/tags/plexus-digest-1.1/ plexus-digest/
# tar czf plexus-digest-1.1-src.tar.gz plexus-digest/

Source2:        %{name}-1.0-build.xml
Source3:        %{name}-components.xml

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  ant >= 0:1.6
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven-surefire-maven-plugin
BuildRequires:  maven-surefire-provider-junit = 2.3
BuildRequires:  maven2-common-poms >= 1.0
BuildRequires:  maven-doxia
BuildRequires:  maven-doxia-sitetools
BuildRequires:  qdox >= 1.5
%endif

BuildRequires:  plexus-maven-plugin
BuildRequires:  plexus-cdc

BuildRequires:  junit

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%description
The Plexus project seeks to create end-to-end developer tools for
writing applications. At the core is the container, which can be
embedded or for a full scale application server. There are many
reusable components for hibernate, form processing, jndi, i18n,
velocity, etc. Plexus also includes an application server which
is like a J2EE application server, without all the baggage.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{name}

%if ! %{with_maven}
cp %{SOURCE2} build.xml
mkdir -p target/classes/META-INF/plexus
cp %{SOURCE3} target/classes/META-INF/plexus/components.xml
%endif


%build
%if %{with_maven}
mkdir external_repo
ln -s %{_javadir} external_repo/JPP

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
        -e \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install javadoc:javadoc
#       -Dmaven.test.failure.ignore=true \

%else
export CLASSPATH=$(build-classpath classworlds \
                   plexus/utils plexus/container-default)
CLASSPATH=$CLASSPATH:target/classes:target/test-classes
ant -Dbuild.sysclasspath=only jar javadoc
%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus
install -pm 644 target/*.jar \
        $RPM_BUILD_ROOT%{_javadir}/%{parent}/%{subname}-%{version}.jar
%add_to_maven_depmap org.codehaus.plexus %{name} 1.0 JPP/%{parent} %{subname}

(cd $RPM_BUILD_ROOT%{_javadir}/%{parent} && for jar in *-%{version}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml \
 $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}.pom

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

cp -pr target/site/apidocs/* \
       $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/

ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/plexus/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-3m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against maven2-2.0.8-2m

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- import from Fedora 13
- with_maven is 0

* Tue Dec 22 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.1-1
- Update to upstream 1.1.

* Tue Dec 22 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.0-10
- Drop not needed depmap.
- Build with maven.

* Fri Aug 21 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.0-9
- Fix License, formatting and comments.

* Sun May 17 2009 Fernando Nasser <fnasser@redhat.com> 0:1.0-8
- Fix license

* Tue Apr 30 2009 Yong Yang <yyang@redhat.com> 1.0-7
- Rebuild with new maven2 2.0.8 built in non-bootstrap mode

* Tue Apr 30 2009 Yong Yang <yyang@redhat.com> 1.0-6
- force to BR plexus-cdc alpha 10
- rebuild without maven

* Tue Apr 30 2009 Yong Yang <yyang@redhat.com> 1.0-5
- Add BRs maven-doxia*, qdox
- Enable jpp-depmap
- Rebuild with new maven2 2.0.8 built in non-bootstrap mode
- ignore test failure

* Tue Mar 17 2009 Yong Yang <yyang@redhat.com> 1.0-4
- rebuild with new maven2 2.0.8 built in bootstrap mode

* Thu Feb 05 2009 Yong Yang <yyang@redhat.com> 1.0-3
- Fix release tag

* Wed Jan 14 2009 Yong Yang <yyang@redhat.com> 1.0-2jpp.1
- Import from dbhole's maven 2.0.8 packages, initial building

* Mon Jan 07 2008 Deepak Bhole <dbhole@redhat.com> 1.0-1jpp.1
- Import from JPackage
- Update per Fedora spec

* Wed Nov 14 2007 Ralph Apel <r.apel @ r-apel.de> - 0:1.0-1jpp
- Initial build
