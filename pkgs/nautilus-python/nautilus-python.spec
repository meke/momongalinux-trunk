%global momorel 1

Name:           nautilus-python
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Python bindings for Nautilus

Group:          Development/Libraries
License:        GPLv2+
URL:            http://www.gnome.org/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  nautilus-devel >= 3.0
BuildRequires:  gnome-python2-devel
BuildRequires:  gnome-vfs2-devel
BuildRequires:  pygtk2-devel
Requires:       nautilus

Obsoletes: eel2

%description
Python bindings for Nautilus


%package devel
Summary:        Python bindings for Nautilus
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

Obsoletes: eel2-devel

%description devel
Python bindings for Nautilus


%prep
%setup -q

%build
%configure --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_libdir}/nautilus/extensions-3.0/python/

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README AUTHORS COPYING NEWS
%{_libdir}/nautilus/extensions-3.0/lib%{name}.*
%dir %{_libdir}/nautilus/extensions-3.0/python/


%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/doc/%{name}
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-4m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-2m)
- full rebuild for mo7 release

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1
- delete patch0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Apr 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1
-- import Patch0 from Fedora 11 (0.5.1-4)
- nautilus-bzr needs nautilus-python

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3-2m)
- rebuild against python-2.6.1

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-1m)
- import from Fedora to Momonga for bzr-gtk
- change BuildRequires: gnome-python2-devel to gnome-python2

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.4.3-5
- Autorebuild for GCC 4.3

* Wed May 02 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.4.3-4
- Added missing folder. Fixes bug #238591.

* Sat Apr 21 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.4.3-3
- Moved example code to devel package.

* Thu Apr 19 2007 Jef Spaleta <jspaleta@gmail.com> - 0.4.3-2
- Package review corrections

* Wed Apr 04 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.4.3-1
- Initial version
