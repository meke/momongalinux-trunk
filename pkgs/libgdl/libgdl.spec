%global momorel 1
Summary:	GNOME docking library
Name:		libgdl
Version:	3.6.0
Release: %{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Libraries
URL:		http://www.gnome.org/
Source0:	http://download.gnome.org/sources/gdl/3.6/gdl-%{version}.tar.xz
NoSource: 0

Requires:	gobject-introspection

BuildRequires:	gettext
BuildRequires:	gobject-introspection-devel
BuildRequires:	gtk3-devel
BuildRequires:	gtk-doc

BuildRequires:	intltool
BuildRequires:	libtool
BuildRequires:	libxml2-devel
BuildRequires:	perl(XML::Parser)

Obsoletes:	gdl
Obsoletes:	gdl-devel

%description
GDL adds dockable widgets to GTK+. The user can rearrange those widgets by drag
and drop and layouts can be saved and loaded. Currently it is used by anjuta,
inkscape, gtranslator and others.

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries

Requires:	gobject-introspection-devel
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains development files for %{name}.

%prep
%setup -q -n gdl-%{version}

%build
%configure \
  --disable-silent-rules \
  --disable-static \
  --enable-introspection=yes

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

%make 

%install
make install INSTALL="%{__install} -p" DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -delete

%find_lang gdl-3

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f gdl-3.lang
%doc AUTHORS
%doc COPYING
%doc MAINTAINERS
%doc NEWS
%doc README
%{_libdir}/%{name}-3.so.*
%{_libdir}/girepository-1.0/Gdl-3.typelib

%files devel
%{_libdir}/%{name}-3.so
%{_libdir}/pkgconfig/gdl-3.0.pc
%{_datadir}/gir-1.0/Gdl-3.gir

%dir %{_datadir}/gtk-doc/
%dir %{_datadir}/gtk-doc/html/
%doc %{_datadir}/gtk-doc/html/gdl-3.0/

%dir %{_includedir}/%{name}-3.0
%{_includedir}/%{name}-3.0/gdl

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- version down 3.6.0
-- 3.6.2 was any API change...

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sat Jul 07 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-3m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.30.0-2m)
- rename to libgdl

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.2-1m)
- update to 2.29.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Wed Nov 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Fri Feb 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.11-3m)
- rebuild against for librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.11-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.11-1m)
- update to 0.7.11

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.6-2m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.6-1m)
- update to 0.7.6

* Sat Jun 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Fri Jan 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Mon Jan 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- update 0.7.0

* Mon Sep 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-4m)
- rebuild against glib-2.12.3 and gtk+-2.10.3
- revise %%install section

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-3m)
- delete libtool library

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-2m)
- revise %%file (conflict)

* Tue May 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue May 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Mon Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.4.0-5m)
- rebuild against for gnome-common

* Mon Nov 29 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.0-4m)
- add patch1(gdl-0.4.0-string_h.patch)

* Wed Apr 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.0-3m)
- add patch0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.4.0-2m)
- rebuild against for libxml2-2.6.8

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.0-1m)
- version 0.4.0

* Fri Jul 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-7m)
- rebuild against for libcoroco-0.3.0

* Sat Apr 12 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (0.3.0-6m)
- rebuild against for librsvg (libcroco).

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-5m)
- rebuild against for XFree86-4.3.0

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.3.0-4m)
  rebuild against openssl 0.9.7a

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-3m)
- fix URL
- add BuildPrereq: libgsf-devel

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-2m)
- fix defattr about files section

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-1m)
- created spec file
