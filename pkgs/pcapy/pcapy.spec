%global momorel 7

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name: pcapy
Version: 0.10.5
Release:  %{momorel}m%{?dist}
Summary: A Python interface to libpcap

Group: Development/Languages
License: "ASL 1.1"
URL: http://oss.coresecurity.com/projects/pcapy.html
Source0: http://oss.coresecurity.com/repo/pcapy-0.10.5.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7, libpcap-devel >= 1.1.1

%description
Pcapy is a Python extension module that interfaces with the libpcap
packet capture library. Pcapy enables python scripts to capture packets
on the network. Pcapy is highly effective when used in conjunction with 
a packet-handling package such as Impacket, which is a collection of 
Python classes for constructing and dissecting network packets.

%prep
%setup -q

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

#fix encodings
sed -i 's/\r//' LICENSE
sed -i 's/\r//' README
sed -i 's/\r//' pcapy.html
iconv -f IBM850 -t UTF8 pcapy.html > pcapy.html.tmp
mv pcapy.html.tmp pcapy.html


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

rm -rf $RPM_BUILD_ROOT/usr/share/doc/pcapy

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE README pcapy.html
%attr(0755,root,root) %dir %{python_sitearch}/pcapy*

%changelog
* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.5-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.5-4m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-3m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.5-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.10.5-4
- Rebuild for Python 2.6

* Fri Feb 08 2008 Jon Ciesla <limb@jcomserv.net> - 0.10.5-3
- GCC 4.3 rebuild.

* Thu Jan 03 2008 Jon Ciesla <limb@jcomserv.net> - 0.10.5-2
- Fixed file listing.

* Thu Nov 29 2007 Jon Ciesla <limb@jcomserv.net> - 0.10.5-1
- create.
