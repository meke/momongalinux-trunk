%global         momorel 1

%global         qtver 4.8.4
%global         kdever 4.10.0

Name:		wicd-kde
Version:	0.3.1
Release:	%{momorel}m%{?dist}
License:	GPLv3+
Source:		http://kde-apps.org/CONTENT/content-files/132366-%{name}-%{version}.tar.gz
NoSource:	0
Source1:	kcm_wicd.desktop
Group:		Applications/Internet
Summary:	A Wicd client built on the KDE Development Platform
URL:		https://projects.kde.org/projects/extragear/network/wicd-kde
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       kdelibs
Requires:	wicd

BuildRequires:	qt-devel >= %{qtver}
BuildRequires:	kdebase-devel >= %{kdever}
BuildRequires:	gettext
BuildRequires:	desktop-file-utils

%description
A Wicd client built on the KDE Development Platform.

%prep

%setup -q -n %{name}  
 
%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ../
popd
make %{?_smp_mflags} -C %{_target_platform} 

%install
rm -rf %{buildroot}
%make_install -C %{_target_platform}

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :

%postun
update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc ChangeLog COPYING TODO
%{_sysconfdir}/dbus-1/system.d/org.kde.wicdclient.scripts.conf
%{_kde4_libdir}/kde4/kcm_wicd.so
%{_kde4_libdir}/kde4/plasma_applet_wicd.so
%{_kde4_libdir}/kde4/plasma_engine_wicd.so
%{_kde4_libexecdir}/wicdclient_scripts_helper
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/plasma/services/wicd.operations
%{_kde4_datadir}/kde4/services/kcm_wicd.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-wicd.desktop
%{_kde4_datadir}/kde4/services/plasma-dataengine-wicd.desktop
%{_datadir}/dbus-1/system-services/org.kde.wicdclient.scripts.service
%{_datadir}/polkit-1/actions/org.kde.wicdclient.scripts.policy

%changelog
* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Mon Dec 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-1m)
- a little modify spec file
- update to 0.2.3

* Sun Sep 04 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.2.2-2m) 
- correct spec file errors

* Sat Sep 03 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.2.2-1m) 
- initial momonga release
- removed desktop-file-install 
- fixed macro error with wicdclient_scripts_helper
- added wicd-kde-pofiles-regex.patch to correct cmake error

* Thu Jun 08 2011 Minh Ngo <nlminhtl@gmail.com> 0.2.2-2
- desktop-file-install for desktop files
- KDE-specific macros
- macro make_install fixed

* Tue Jun 07 2011 Minh Ngo <nlminhtl@gmail.com> 0.2.2-1
- initial build 
