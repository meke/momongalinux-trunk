%global momorel 1

Summary: Example data file for VTK
Name: vtkdata
Version: 6.0.0
Release: %{momorel}m%{?dist}
License: "The Visualization Toolkit (VTK) Copyright"
Group: Development/Libraries
URL: http://www.vtk.org/
Source0: http://www.vtk.org/files/release/6.0/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: vtk-devel = %{version}
Requires: vtk = %{version}
BuildArch: noarch

%description
Example data file for VTK

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}
cd %{buildroot}%{_datadir}
tar -zpxf %{SOURCE0}
mv VTKDATA%{version} %{name}-%{version}

# (Verbosely) fix 0555 permissions
find . -type f -perm 0555 | xargs -r echo chmod 0755 | sh -x
# Remove execute bits from not-scripts
for file in `find . -type f -perm 0755`; do
  head -1 $file | grep '^#!' > /dev/null && continue
  chmod 0644 $file
done

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_datadir}/*

%changelog
* Sat Jan 18 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.0-1m)
- update to 6.0.0

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10.1-1m)
- update to 5.10.1

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10.0-1m)
- update to 5.10.0

* Tue Oct 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.0-1m)
- update to 5.8.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.1-2m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.1-1m)
- update to 5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.6.0-1m)
- update to 5.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.4-2m)
- rebuild against gcc43

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.0.4-1m)
- import to Momonga (based on 5.0.4-7 package in Fedora)

* Sat Feb 23 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.4-7
- Update to 5.0.4.

* Sun Apr 15 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.3-6
- Update to 5.0.3.

* Mon Sep 11 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.2-5
- Update to 5.0.2.

* Wed Jul 19 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-4
- Fix some permissions.

* Thu Jul 13 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-3
- Update to 5.0.1.

* Thu Jun  1 2006 Axel Thimm <Axel.Thimm@ATrpms.net>
- Initial build.

