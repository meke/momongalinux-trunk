%global         momorel 1
%global         qtver 4.7.4
%global         kdever 4.7.3

Summary:        a KDE4 port of QuadKonsole
Name:           quadkonsole4
Version:        0.4.4
Release:        %{momorel}m%{?dist}
License:        GPLv2
Group:          User Interface/X
URL:            http://kb.ccchl.de/quadkonsole4/
Source0:        http://kb.ccchl.de/quadkonsole4/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  cmake
BuildRequires:  desktop-file-utils >= 0.16

%description
QuadKonsole4 is a KDE4 port of QuadKonsole (http://kde-apps.org/content/show.php/QuadKonsole?content=22482).

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/qkremote
%{_kde4_libdir}/kde4/qkremotepart.so
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/qkremote
%{_kde4_appsdir}/qkremotepart
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/qkremote.desktop
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_datadir}/kde4/services/qkremote_part.desktop
%{_kde4_docdir}/HTML/en/%{name}
%{_kde4_datadir}/locale/cs/LC_MESSAGES/%{name}.mo	
%{_kde4_datadir}/locale/de/LC_MESSAGES/%{name}.mo	
%{_kde4_datadir}/locale/sr/LC_MESSAGES/%{name}.mo	
%{_kde4_datadir}/locale/sr@ijekavian/LC_MESSAGES/%{name}.mo	
%{_kde4_datadir}/locale/sr@ijekavianlatin/LC_MESSAGES/%{name}.mo	
%{_kde4_datadir}/locale/sr@latin/LC_MESSAGES/%{name}.mo	
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/*/apps/qkremote.png

%changelog
* Sun Nov 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sun Sep 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Fri Aug 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- update to 0.4

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- update to 0.3

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- initial commit for Momonga Linux
