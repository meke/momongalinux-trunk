%global momorel 2

# -*- RPM-SPEC -*-
%define debug_package %{nil}

Summary: A graphical interface for configuring the boot loader
Name: system-config-boot
Version: 1.0
Release: %{momorel}m%{?dist}
URL: http://fedoraproject.org/wiki/SystemConfig/boot
License: GPLv2
Group: Applications/System 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: %{name}-%{version}.tar.bz2
Requires: pygtk2
Requires: python
Requires: usermode >= 1.36
Requires: usermode-gtk
ExclusiveArch: %{ix86} x86_64
Obsoletes: redhat-config-boot
Provides: redhat-config-boot = %{version}
BuildRequires: python
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: desktop-file-utils
BuildRequires: perl-XML-Parser
BuildRequires: intltool

%description
system-config-boot is a graphical user interface that allows 
the user to change the default boot entry of the system.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

mkdir %{buildroot}%{_datadir}/applications
for i in %{name}.desktop; do \
  desktop-file-install --vendor system --delete-original \
    --dir %{buildroot}%{_datadir}/applications \
    --add-category Settings \
    --remove-category SystemSetup \
    --remove-category Application \
    $i; \
done;

%find_lang %name

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
%attr(0755,root,root) /usr/bin/system-config-boot
%attr(0755,root,root) /usr/sbin/system-config-boot
%dir /usr/share/system-config-boot
%attr(0644,root,root) /usr/share/system-config-boot/*
%attr(0644,root,root) /usr/share/pixmaps/*.png
%attr(0644,root,root) %{_datadir}/applications/*
%attr(0644,root,root) %config(noreplace) /etc/security/console.apps/system-config-boot
%attr(0644,root,root) %config(noreplace) /etc/pam.d/system-config-boot

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-2m)
- rebuild for glib 2.33.2

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-1m)
- update 1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- full rebuild for mo7 release

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
- remove Requires: rhpl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.20-1m)
- update to 0.2.20

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.14-3m)
- rebuild against gcc43

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.14-2m)
- revival pyc pyo

* Tue Feb 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.14-1m)
- update 0.2.14

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.11-4m)
- delete pyc pyo

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.11-3m)
- remove category X-Red-Hat-Base SystemSetup Application

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.11-2m)
- delete duplicated dir

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.11-1m)
- update 0.2.11

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8-2m)
- add System to Categories of desktop file for KDE
- BuildRequires: desktop-file-utils

* Mon Dec  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.2.8-1m)
  update to 0.2.8

* Tue Nov 23 2004 Harald Hoyer <harald@redhat.com> - 0.2.8-1
- s/gtk.mainquit/gtk.main_quit/g

* Wed Sep 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.1.6-2m)
  for python2.3

* Fri Jul 30 2004 Harald Hoyer <harald@redhat.com> - 0.2.7-1
- added build requirements: Perl-XML-Parser desktop-file-utils

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (0.1.6-1m)
- import from Fedora.

* Wed Apr 21 2004 Harald Hoyer <harald@redhat.com> - 0.2.6-1
- translation updates (118804)

* Fri Apr  2 2004 Harald Hoyer <harald@redhat.com> - 0.2.5-1
- translation updates
- renaming of desktop file

* Mon Mar 15 2004 Harald Hoyer <harald@redhat.com> - 0.2.4-1
- translation update

* Thu Nov  6 2003 Harald Hoyer <harald@redhat.de> 0.2.0-1
- changed python version

* Thu Nov 06 2003 Harald Hoyer <harald@redhat.de> 0.1.7-1
- fixed #109266

* Thu Oct 30 2003 Harald Hoyer <harald@redhat.de> 0.1.6-1
- fixed #106796
- added exception handling

* Mon Oct 27 2003 Harald Hoyer <harald@redhat.de> 0.1.5-1
- fixed conf loading (#106796)
- i18n update

* Tue Oct 14 2003 Harald Hoyer <harald@redhat.de> 0.1.4-1
- autofooed and intltoolized r-c-boot

* Wed Oct  8 2003 Harald Hoyer <harald@redhat.de> 0.1.3-1
- added po files

* Wed Oct  2 2003 Harald Hoyer <harald@redhat.de> 0.1.2-1
- fixed desktop file

* Fri Aug 22 2003 Harald Hoyer <harald@redhat.de> 0.1.1-1
- first version

