%global         momorel 1

Summary:        Traces the route taken by packets over an IPv4/IPv6 network
Name:           traceroute
Version:        2.0.19
Release:        %{momorel}m%{?dist}
Group:          Applications/Internet
License:        GPLv2+
URL:            http://traceroute.sourceforge.net
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}/%{name}-%{version}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The traceroute utility displays the route used by IP packets on their
way to a specified network (or Internet) host.  Traceroute displays
the IP number and host name (if possible) of the machines along the
route taken by the packets.  Traceroute is used as a network debugging
tool.  If you're having network connectivity problems, traceroute will
show you where the trouble is coming from along the route.

Install traceroute if you need a tool for diagnosing network connectivity
problems.

%prep
%setup -q

%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS" LDFLAGS=""

%install
rm -rf %{buildroot}

install -d %{buildroot}/bin
install -m755 traceroute/traceroute %{buildroot}/bin
pushd %{buildroot}/bin
ln -s traceroute traceroute6
ln -s traceroute tracert
popd

install -d %{buildroot}%{_mandir}/man8
install -p -m644 traceroute/traceroute.8 %{buildroot}%{_mandir}/man8
pushd %{buildroot}%{_mandir}/man8
ln -s traceroute.8 traceroute6.8
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README TODO CREDITS
/bin/*
%{_mandir}/*/*

%changelog
* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.19-1m)
- update to 2.0.19

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.17-2m)
- rebuild for new GCC 4.6

* Wed Dec 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-2m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.15-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-2m)
- rebuild against gcc43

* Fri Jun  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-1m)
- sync Fedora

* Wed Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org
- (1.0.4-1m)
- new source

* Sun Nov  9 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.4a12-3m)
- use %%{momorel}

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (1.4a12-2k)
- remove traceroute-1.4a5-security.patch, because it's already fixed.
- version up.

* Sun Oct 01 2000 Toyokazu WATABE <toy2@kondara.org>
- add traceroute-1.4a5-security.patch

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.4a5-18).

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Mon Jan 17 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user.

* Fri Jan 14 2000 Bill Nottingham <notting@redhat.com>
- add patch for tracing to really long hostnames

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Thu May 27 1999 Richard Henderson <rth@twiddle.net>
- avoid unaligned traps writing into the output data area.

* Fri May 14 1999 Jeff Johnson <jbj@redhat.com>
- fix segfault when host cannot be reached through if (#2819)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 14)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- strip binaries.

* Sun Feb 07 1999 Preston Brown <pbrown@redhat.com>
- patch added to automatically determine interface to route through

* Fri Jan 22 1999 Jeff Johnson <jbj@redhat.com>
- use %configure
- fix 64 bit problem on alpha (#919)

* Wed Jan 13 1999 Bill Nottingham <notting@redhat.com>
- configure fix for arm

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Dec 16 1997 Cristian Gafton <gafton@redhat.com>
- updated the security patch (ouch!). Without the glibc fix, it could be
  worthless anyway

* Sat Dec 13 1997 Cristian Gafton <gafton@redhat.com>
- added a security patch fix

* Wed Oct 22 1997 Erik Troan <ewt@redhat.com>
- added fix from Christopher Seawood

* Mon Sep 22 1997 Erik Troan <ewt@redhat.com>
- updated to 1.4a5 for security fixes; release 1 is for RH 4.2, release 2
  is against glibc

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
- built against glibc
