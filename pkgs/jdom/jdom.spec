%global momorel 4

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define section free

Name:           jdom
Version:        1.1.1
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Java alternative to DOM and SAX
License:        BSD
URL:            http://www.jdom.org/
Group:          Development/Libraries
Source0:        http://jdom.org/dist/binary/jdom-1.1.1.tar.gz
Source1:        http://repo1.maven.org/maven2/org/jdom/jdom/1.1/jdom-1.1.pom
Patch0:         %{name}-crosslink.patch
Patch1:         %{name}-1.0-OSGiManifest.patch
# Change version number and remove dependencies that don't have POMs yet
Patch2:         %{name}-cleanup-pom.patch
Requires:       xalan-j2 >= 0:2.2.0
BuildRequires:  ant >= 0:1.6
BuildRequires:  xalan-j2 >= 0:2.2.0
BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  java-javadoc
%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
JDOM is, quite simply, a Java representation of an XML document. JDOM
provides a way to represent that document for easy and efficient
reading, manipulation, and writing. It has a straightforward API, is a
lightweight and fast, and is optimized for the Java programmer. It's an
alternative to DOM and SAX, although it integrates well with both DOM
and SAX.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
BuildArch:      noarch
# for /bin/rm and /bin/ln
Requires(post):   coreutils
Requires(postun): coreutils

%description javadoc
Javadoc for %{name}.

%package demo
Summary:        Demos for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{epoch}:%{version}-%{release}
BuildArch:      noarch

%description demo
Demonstrations and samples for %{name}.


%prep
%setup -q -n %{name}
cp %{SOURCE1} .
%patch0 -p0
%patch1 -p0
%patch2 -p0
# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;

%build
export CLASSPATH=$(build-classpath xalan-j2)
sed -e 's|<property name="build.compiler".*||' build.xml > tempf; cp tempf build.xml; rm tempf
ant -Dj2se.apidoc=%{_javadocdir}/java package javadoc-link


%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p build/%{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

# demo
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -pr samples $RPM_BUILD_ROOT%{_datadir}/%{name}

# maven stuff
mkdir -p $RPM_BUILD_ROOT%{_mavenpomdir}
cp jdom-1.1.pom $RPM_BUILD_ROOT%{_mavenpomdir}/JPP-jdom.pom
%add_to_maven_depmap org.jdom %{name} %{version} JPP %{name}


%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT


%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/%{name}
fi

%post
%update_maven_depmap
%if %{gcj_support}
  if [ -x %{_bindir}/rebuild-gcj-db ] 
  then
    %{_bindir}/rebuild-gcj-db
  fi
%endif

%postun
%update_maven_depmap
%if %{gcj_support}
  if [ -x %{_bindir}/rebuild-gcj-db ] 
  then
    %{_bindir}/rebuild-gcj-db
  fi
%endif


%files
%defattr(0644,root,root,0755)
%doc CHANGES.txt COMMITTERS.txt LICENSE.txt README.txt TODO.txt
%{_javadir}/%{name}*.jar
%{_mavendepmapfragdir}/%{name}
%{_mavenpomdir}/*.pom

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(0644,root,root,0755)
%ghost %doc %{_javadocdir}/%{name}
%doc %{_javadocdir}/%{name}-%{version}

%files demo
%defattr(0644,root,root,0755)
%{_datadir}/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-1m)
- sync with Fedora 13 (1.1.1-1)

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-4jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-4jpp.1m)
- import from Fedora

* Fri Aug 04 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-4jpp.1
- Added missing requirements.
- Remove jaxen requirement, since we don't have it in fc yet.
- Merge with fc spec.

* Tue Apr 11 2006 Ralph Apel <r.apel at r-apel.de> - 0:1.0-3jpp
- First JPP-1.7 release
- Drop false xalan dependency

* Tue Oct 11 2005 Ralph Apel <r.apel at r-apel.de> - 0:1.0-2jpp
- Add jaxen to Requires and classpath

* Sat Sep 18 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0-1jpp
- Upgrade to 1.0 final

* Tue Sep 07 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.rc1.1jpp
- Upgrade to 1.0-rc1

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.0-0.b9.4jpp
- Rebuild with ant-1.6.2

* Mon Jul 19 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.b9.3jpp
- Add non-versioned javadoc dir symlink.
- Crosslink with local J2SE javadocs.

* Thu Jan 22 2004 David Walluck <david@anti-microsoft.org> 0:1.0-0.b9.2jpp
- fix URL

* Wed Jan 21 2004 David Walluck <david@anti-microsoft.org> 0:1.0-0.b9.1jpp
- b9
- don't use classic compiler

* Thu Mar 27 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.b8.2jpp
- Adapted to JPackage 1.5.
- Use sed instead of bash 2 extension when symlinking jars during build.

* Wed May 08 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b8.1jpp
- vendor, distribution, group tags

* Sat Jan 19 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b7.6jpp
- versioned dir for javadoc
- requires xalan-j2 >= 2.2.0
- no dependencies for javadoc package
- stricter dependency for demo package
- section macro

* Wed Dec 5 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b7.5jpp
- javadoc into javadoc package

* Wed Nov 21 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.0-0.b7.4jpp
- removed packager tag
- new jpp extension
- added xalan 2.2.D13 support

* Sat Oct 6 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b7.3jpp
- used original tarball

* Sun Sep 30 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b7.2jpp
- first unified release
- s/jPackage/JPackage

* Mon Sep 17 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b7.1mdk
- Requires and BuildRequires xalan-j2
- vendor tag
- packager tag
- s/Copyright/License/
- truncated description to 72 columns in spec
- spec cleanup
- used versioned jar
- added demo package

*  Sat Feb 17 2001 Guillaume Rousse <g.rousse@linux-mandrake.com> 1.0b6-1mdk
- first Mandrake release
