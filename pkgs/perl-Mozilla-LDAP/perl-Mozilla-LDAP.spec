%global momorel 10

%define nspr_name	nspr
%define nss_name	nss
%define mozldap_name	mozldap

Summary: LDAP Perl module that wraps the Mozilla C SDK
Name: perl-Mozilla-LDAP
Version: 1.5.3
Release: %{momorel}m%{?dist}
License: GPLv2+ LGPLv2+ "MPLv1.1"
Group: Development/Libraries
URL: http://www.mozilla.org/directory/perldap.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: perl >= 2:5.8.0
BuildRequires: perl >= 2:5.8.0
BuildRequires: perl-ExtUtils-MakeMaker
BuildRequires: %{nspr_name}-devel >= 4.6
BuildRequires: %{nss_name}-devel >= 3.11
BuildRequires: %{mozldap_name}-devel >= 6.0.7
Source0: ftp://ftp.mozilla.org/pub/mozilla.org/directory/perldap/releases/1.5/perl-mozldap-%{version}.tar.gz
Source1: ftp://ftp.mozilla.org/pub/mozilla.org/directory/perldap/releases/1.5/Makefile.PL.rpm
Patch0: perl-mozldap-1.5.3-mozldap.patch
Patch1: perl-mozldap-1.5.3-test.patch

%description
%{summary}.

%prep
%setup -q -n perl-mozldap-%{version}
%patch0 -p1
%patch1 -p1

# Filter unwanted Provides:
cat << \EOF > %{name}-prov
#!/bin/sh
%{__perl_provides} $* |\
  sed -e '/perl(Mozilla::LDAP::Entry)$/d'
EOF

%define __perl_provides %{_builddir}/perl-mozldap-%{version}/%{name}-prov
chmod +x %{__perl_provides}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Mozilla::LDAP::Entry)/d'
EOF

%define __perl_requires %{_builddir}/perl-mozldap-%{version}/%{name}-req
chmod +x %{__perl_requires}

%build

LDAPPKGNAME=%{mozldap_name} CFLAGS="$RPM_OPT_FLAGS" perl %{SOURCE1} PREFIX=$RPM_BUILD_ROOT%{_prefix} INSTALLDIRS=vendor < /dev/null
make OPTIMIZE="$RPM_OPT_FLAGS" CFLAGS="$RPM_OPT_FLAGS" 
make test

%install
rm -rf $RPM_BUILD_ROOT
eval `perl '-V:installarchlib'`

%makeinstall

# remove files we don't want to package
rm -f `find $RPM_BUILD_ROOT -type f -name perllocal.pod -o -name .packlist`
find $RPM_BUILD_ROOT -name API.bs -a -size 0 -exec rm -f {} \;

# make sure shared lib is correct mode
find $RPM_BUILD_ROOT -name API.so -exec chmod 755 {} \;


# find and run the correct version of brp-compress
if [ -x /usr/lib/rpm/momonga/brp-compress ] ; then
    /usr/lib/rpm/momonga/brp-compress
elif [ -x %{_libdir}/rpm/brp-compress ] ; then
    %{_libdir}/rpm/brp-compress
fi

# make sure files refer to %{_prefix} instead of buildroot/%prefix
find $RPM_BUILD_ROOT%{_prefix} -type f -print | \
	sed "s@^$RPM_BUILD_ROOT@@g" > %{name}-%{version}-%{release}-filelist
if [ "$(cat %{name}-%{version}-%{release}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit 1
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}-%{version}-%{release}-filelist
%defattr(-,root,root,-)
%doc CREDITS ChangeLog README MPL-1.1.txt

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-2m)
- rebuild against perl-5.14.2

* Sun Aug  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-8m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-7m)
- rebuild against perl-5.12.0

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-6m)
- fix for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-3m)
- rebuild against rpm-4.6

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-2m)
- change BuildRequires: perl(ExtUtils::MakeMaker) to perl-ExtUtils-MakeMaker

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.2-1m)
- import from Fedora

* Tue Mar 11 2008 Rich Megginson <rmeggins@redhat.com> - 1.5.2-4.1
- rebuild for perl 5.10

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.5.2-3.1
- Autorebuild for GCC 4.3

* Tue Oct 16 2007 Tom "spot" Callaway <tcallawa@redhat.com> - 1.5.2-2.1
- correct license tag
- add BR: perl(ExtUtils::MakeMaker)

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.5.2-2
- Rebuild for selinux ppc32 issue.

* Fri Jul 27 2007 Rich Megginson <richm@stanfordalumni.org> - 1.5.2-1
- Fix bugzilla 389731 - crash when a bad URL is passed

* Wed Jun 20 2007 Rich Megginson <richm@stanfordalumni.org> - 1.5.1-1
- all files have been GPL/LGPL/MPL tri-licensed

* Wed Jan 10 2007 Rich Megginson <richm@stanfordalumni.org> - 1.5-9
- remove only perl(Mozilla::LDAP::Entry) from Provides, leave in 
- perl(Mozilla::LDAP::Entry) = 1.5

* Wed Jan 10 2007 Rich Megginson <richm@stanfordalumni.org> - 1.5-8
- add perl_requires filter for the Entry module
- add the MPL-1.1.txt file to the DOCs

* Wed Jan 10 2007 Rich Megginson <richm@stanfordalumni.org> - 1.5-7
- Incorporate comments from Fedora Extras review - https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=219869
- Remove all Requires except perl - use autogenerated ones
- Remove ExclusiveArch
- Remove files that don't need to be packaged
- add full URL to sources
- set API.so to mode 755

* Tue Oct 17 2006 Rich Megginson <richm@stanfordalumni.org> - 1.5-6
- look for brp-compress first in /usr/lib then _libdir

* Tue Oct 17 2006 Rich Megginson <richm@stanfordalumni.org> - 1.5-5
- there is no TODO file; use custom Makefile.PL

* Mon Oct 16 2006 Rich Megginson <richm@stanfordalumni.org> - 1.5-4
- use pkg-config --variable=xxx instead of --cflags e.g.

* Mon Oct 16 2006 Rich Megginson <richm@stanfordalumni.org> - 1.5-3
- this is not a noarch package

* Mon Oct 16 2006 Rich Megginson <richm@stanfordalumni.org> - 1.5-2
- Use new mozldap6, dirsec versions of nspr, nss

* Tue Feb  7 2006 Rich Megginson <richm@stanfordalumni.org> - 1.5-1
- Based on the perl-LDAP.spec file

