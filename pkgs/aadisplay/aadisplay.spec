%global momorel 8

Name: aadisplay
Summary: aadisplay is print ASCII Art
Version: 0.2
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/X
%global srcname %{name}-%{version}-1
Source0: http://www.geocities.co.jp/SiliconValley-Oakland/4550/%{srcname}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib1-devel
BuildRequires: gtk+1-devel

%description
display ASCII Art used by elisp-navi2ch.

%prep
%setup -q -n %{srcname}

%build
%configure
#./configure --prefix=%{_prefix}
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
%{makeinstall}

%files
%defattr(-, root, root)
%{_bindir}/*

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-4m)
- rebuild against rpm-4.6

* Mon Jun  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2-3m)
- Requires -> BuildRequires glib2-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc43

* Thu May 25 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- add BuildRequires: gtk+1-devel

* Tue May 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-1m)
- create
