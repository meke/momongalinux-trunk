%global momorel 2

Summary: A library for parsing XML
Name: expat
Version: 2.1.0
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://expat.sourceforge.net/
License: MIT/X
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is expat, the C library for parsing XML, written by James Clark. Expat
is a stream oriented XML parser. This means that you register handlers with
the parser prior to starting the parse. These handlers are called when the
parser discovers the associated structures in the document being parsed. A
start tag is an example of the kind of structures for which you may
register handlers.

%package devel
Summary: Libraries and include files to develop XML applications with expat
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The expat-devel package contains the libraries, include files and documentation
to develop XML applications with expat.

%prep
%setup -q

%build
export CFLAGS="%{optflags} -fPIC"
%configure
%make

%install
rm -rf %{buildroot}

%makeinstall man1dir=%{buildroot}/%{_mandir}/man1

find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "*.pc" -delete

%check
make check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING Changes README
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_mandir}/*/*

%files devel
%defattr(-,root,root,-)
%doc Changes doc examples
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_includedir}/*.h

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-2m)
- support UserMove env

* Mon Mar 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-9m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-8m)
- delete __libtoolize hack

* Sat Dec  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-7m)
- [SECURITY] CVE-2009-3560
- import patches from Rawhide (2.0.1-8)
- add %%check

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-5m)
- [SECURITY] CVE-2009-3720
- import upstream patch (Patch0)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- %%NoSource -> NoSource

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.0-1m)
- update to 2.0.0

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.95.8-2m)
- source: expat-2005-01-28.tar.gz

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.95.8-1m)
- update to 1.95.8
- delete patch

* Fri Oct 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.95.6-3m)
- change License: to MIT/X

* Thu Feb  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.95.6-2m)
- add expat-1.95.6-XML_Status.patch

* Fri Jan 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.95.6-1m)

* Wed Sep 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.95.5-1m)
- minor bugfixes

* Mon Aug  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.95.4-1m)
- major bugfixes

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.95-2k)
- up to 1.95.2

* Tue Oct 24 2000 Jeff Johnson <jbj@redhat.com>
- update to 1.95.1

* Sun Oct  8 2000 Jeff Johnson <jbj@redhat.com>
- Create.
