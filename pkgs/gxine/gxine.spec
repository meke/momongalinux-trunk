%global momorel 2
%global geckover 2.0

Name: gxine
Summary: GTK frontend for the xine multimedia library
Version: 0.5.907
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.xine-project.org/home
Source0: http://dl.sourceforge.net/sourceforge/xine/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.5.906-desktop.patch
Patch1: %{name}-0.5.906-dso.patch
Patch2: %{name}-0.5.906-lirc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(post): gtk2
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): shared-mime-info
Requires: gtk2
Requires: xine-lib
# for dbus support
BuildRequires: dbus-glib-devel
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: gtk2-devel
# for mozplugin
BuildRequires: libXaw-devel
# for XTest support
BuildRequires: libXtst-devel
# for LIRC support
BuildRequires: lirc-devel
BuildRequires: nspr-devel
BuildRequires: xine-lib-devel >= 1.2.4
BuildRequires: xulrunner-devel >= %{geckover}

%description
gxine is a fully-featured free audio/video player for unix-like systems which
uses libxine for audio/video decoding and playback. For more informations on
what formats are supported, please refer to the libxine documentation.
gxine is a gtk based gui for this xine-library alternate to xine-ui.

%package mozplugin
Summary: Mozilla plugin for gxine
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}
Requires: mozilla-filesystem

%description mozplugin
This plugin allows gxine to be embedded in a web browser.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .dso
%patch2 -p1 -b .lirc

%{__sed} -i 's/Name=gxine/Name=GXine Video Player/' %{name}.desktop.in
%{__sed} -i 's/Exec=gxine/Exec=gxine %U/' %{name}.desktop.in

%build
%configure \
	--with-dbus \
	--without-hal \
	--with-logo-format=image \
	--with-browser-plugin \
	--disable-integration-wizard \
	--enable-watchdog \
	--disable-own-playlist-parsers \
	--disable-deprecated \
	CPPFLAGS=-DGLIB_COMPILATION

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Remove .las
%{__rm} %{buildroot}%{_libdir}/%{name}/*.la

# Move Mozilla plugin
mkdir -p %{buildroot}%{_libdir}/mozilla/plugins/
mv %{buildroot}%{_libdir}/%{name}/%{name}plugin.so %{buildroot}%{_libdir}/mozilla/plugins/

# Not supported yet - check bz #213041
%find_lang %{name} --all-name

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
%{_bindir}/update-desktop-database &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
%{_bindir}/update-desktop-database &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%files -f %{name}.lang
%defattr(-, root, root,-)
%doc AUTHORS BUGS COPYING ChangeLog README* TODO
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/gtkrc
%config(noreplace) %{_sysconfdir}/%{name}/keypad.xml
%config(noreplace) %{_sysconfdir}/%{name}/startup
%config(noreplace) %{_sysconfdir}/%{name}/toolbar*.xml
%{_bindir}/%{name}*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/*/*/apps/%{name}.png
%{_mandir}/man1/%{name}*.1*
%lang(de) %{_mandir}/de/man1/%{name}*.1*
%lang(es) %{_mandir}/es/man1/%{name}*.1*
%{_datadir}/pixmaps/%{name}.png

%files mozplugin
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/mozilla/plugins/%{name}plugin.so

%changelog
* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.907-2m)
- rebuild against xine-lib-1.2.4

* Sun Oct 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.907-1m)
- update to 0.5.907

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.906-3m)
- build fix

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.906-2m)
- rebuild against HAL removed env

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.906-1m)
- update 0.5.906
- disable hal support

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.905-7m)
- import build fix patch from Ubuntu

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.905-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.905-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.905-4m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.905-3m)
- import desktop.patch, dso.patch and lirc.patch from Fedora
- sort BR and %%files
- enable lirc support again
- modify %%post and %%postun

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.905-2m)
- rebuild against xulrunner-1.9.2

* Tue Jan  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.905-1m)
- version 0.5.905

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.904-3m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.904-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.904-1m)
- update to 0.5.904
- disable lirc support

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.903-3m)
- rebuild against xulrunner-1.9.1

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.903-2m)
- fix gxine.desktop

* Thu Jun 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.903-1m)
- update to 0.5.903

* Sat Mar 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.11-25m)
- revise BuildReq. and Req. about %%{gecko_version}

* Sat Mar  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-24m)
- rebuild against xulrunner-1.9.0.7-1m

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-23m)
- rebuild against xulrunner-1.9.0.6-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.11-22m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-21m)
- rebuild against xulrunner-1.9.0.5-1m

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-20m)
- rebuild against xulrunner-1.9.0.4-1m

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-19m)
- rebuild against xulrunner-1.9.0.3-1m

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-18m)
- rebuild against xulrunner-1.9.0.2-1m

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-17m)
- rebuild against xulrunner-1.9.0.1-1m

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.11-16m)
- remove Requires: firefox 

* Sun Apr 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-15m)
- rebuild against firefox-3

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.11-14m)
- rebuild against firefox-2.0.0.14

* Fri Apr  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.11-13m)
- import xine-version-check.patch from Fedora (to correct xine-lib's version checking)
#'

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-12m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.5.11-11m)
- rebuild against firefox-2.0.0.13

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.11-10m)
- rebuild against firefox-2.0.0.12

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.11-9m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.11-8m)
- rebuild against firefox-2.0.0.10

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.11-7m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.11-6m)
- rebuild against firefox-2.0.0.8

* Wed Sep 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.11-5m)
- rebuild against firefox-2.0.0.7

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.11-4m)
- add Requires for firefox

* Mon Aug  6 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.5.11-3m)
- rebuild against firefox-2.0.0.6

* Sat Jul 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.11-2m)
- rebuild against firefox-2.0.0.5

* Mon Jun  4 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.5.11-1m)
- import from Fedora

* Wed May 16 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.11-4
- rebuild to include ppc64 arch

* Sun Mar 25 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.11-3
- fix rpmlint warning

* Wed Mar 14 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.11-2
- add patch to keep track of window state (backport from 0.6.0 branch) 

* Thu Feb 01 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.11-1
- Bump to new (bugfix) release
- Removed stream end crash patch - fixed in upstream

* Wed Jan 24 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.10-5
- Stream end crash when modal dialog opened patch: see upstream bz #1643093

* Thu Jan 18 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.10-4
- replace R: firefox with R: %%{_libdir}/mozilla/plugins

* Wed Jan 17 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.10-3
- fix BR: gettext instead of gettext-devel
- add R: firefox to mozplugin subpackage

* Thu Jan 11 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.10-2
- fix url
- fix require in mozplugin subpackage
- install desktop file correctly

* Thu Jan 11 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.10-1
- Update to upstream release 0.5.10
- Remove screensaver patch (it was approved by upstream)

* Thu Dec 28 2006 Martin Sourada <martin.sourada@seznam.cz> - 0.5.9-2
- Rebuild

* Sun Dec 17 2006 Martin Sourada <martin.sourada@seznam.cz> - 0.5.9-1.1
- Add BR dbus-devel

* Sat Dec 16 2006 Michel Salim <michel.salim@gmail.com> - 0.5.9-1
- Update to upstream release 0.5.9
- Drop logo.ogg, gxine now includes a JPEG logo

* Sat Dec 16 2006 Michel Salim <michel.salim@gmail.com> - 0.5.8-4
- Fix gnome-screensaver DBUS call
- Separate mozplugin subpackage (from Martin Sourada)
- Add missing BuildRequires

* Tue Nov  7 2006 Mola Pahnadayan <mola@c100c.com> - 0.5.8-3
- Add libXinerama-devel , lirc-devel >= 0.8.1 in build requires
- Fix find_lang
ambiguous

* Tue Oct 31 2006 Mola Pahnadayan <mola@c100c.com> - 0.5.8-2
- rm non-free logo
- add patch to install free-logo 
- uses %%find_lang

* Tue Oct 31 2006 Mola Pahnadayan <mola@c100c.com> - 0.5.8-1
- Updated to 0.5.8

* Tue Sep 19 2006 Mola Pahnadayan <mola@c100c.com> - 0.5.7-1
- write new spec file
- Updated to release 0.5.7
