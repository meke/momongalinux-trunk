%global momorel 2
%global boost_version 1.55.0

Name: libcmis
Version: 0.3.0
Release: %{momorel}m%{?dist}
Summary: A C++ client library for the CMIS interface

Group: System Environment/Libraries
License: GPL+ or LGPLv2+ or MPLv1.1
URL: http://sourceforge.net/projects/libcmis/
Source: http://dl.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: boost-devel >= %{boost_version}
BuildRequires: libcurl-devel
BuildRequires: libxml2-devel

%description
LibCMIS is a C++ client library for the CMIS interface. This allows C++
applications to connect to any ECM behaving as a CMIS server like
Alfresco, Nuxeo for the open source ones.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package tools
Summary: Command line tool to access CMIS
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}

%description tools
The %{name}-tools package contains a tool for accessing CMIS from the
command line.

%prep
%setup -q


%build
%configure --disable-static --disable-tests --disable-werror --without-man
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_libdir}/*.la
rm -rf %{buildroot}%{_mandir}


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING.* README
%{_libdir}/%{name}-*.so.*


%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}*
%{_libdir}/%{name}-*.so
%{_libdir}/pkgconfig/%{name}-*.pc


%files tools
%defattr(-,root,root,-)
%{_bindir}/cmis-client


%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-2m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.1.0-2m)
- rebuild for boost 1.50.0

* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-1m)
- Initial Commit Momonga Linux

