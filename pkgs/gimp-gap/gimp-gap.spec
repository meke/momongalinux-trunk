%global momorel 11

Summary: The GIMP Animation Package.
Name: gimp-gap
Version: 2.0.2
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPL
URL: http://www.gimp.org/
Source0: ftp://ftp.gimp.org/pub/gimp/plug-ins/v2.0/gap/gimp-gap-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gimp-devel >= 2.2.12-2m
Requires: gimp >= 2.0.0

%description
The GIMP-GAP (GIMP Animation Package) is a collection of Plug-Ins to
extend GIMP 2.0 with capabilities to edit and create animations as
sequences of single frames.

%prep
%setup -q

%build
%configure LIBS="-lm"
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files 
%defattr(-, root, root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_libdir}/gimp/2.0/plug-ins/*
%{_libdir}/gimp-gap-2.0
%{_datadir}/gimp/2.0/scripts/*
%{_datadir}/locale/*/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-9m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-8m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-4m)
- %%NoSource -> NoSource

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-3m)
- %%NoSource -> NoSource

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-2m)
- delete libtool library

* Sun Jun 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.2-1m)
- import to Momonga
- animation-plugin has been separated from gimp since version 2.0

