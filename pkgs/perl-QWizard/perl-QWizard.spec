%global momorel 21

Name:           perl-QWizard
Version:        3.15
Release:        %{momorel}m%{?dist}
Summary:        A very portable graphical question and answer wizard system
License:        GPL+ or Artistic 
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/QWizard/
Source0:	http://www.cpan.org/modules/by-module/QWizard/QWizard-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:	perl
BuildRequires:  perl-ExtUtils-MakeMaker
# These are not auto-detected since they're technically optional
# (they're the best of the alternative choices available on linux)
Requires:       perl-Gtk2
Requires:       perl-Chart

# neither are picked up automagically.
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
The QWizard module allows script authors to concentrate on the
content of the forms they want their users to fill in without
worrying about the display.  It allows "Question Wizard" like
interfaces to be very easily created and the results of the input
easily acted upon.  Scripts written which are entirely based on
QWizard inputs are able to be run from the command line which will
show a Gtk2, Tk window or as a ReadLine interactive session or as a
CGI script without modification.  Script writers do not need to know
which interface is being used to display the resulting form(s) as it
should be transparent to the script itself.

Other wizard interfaces exist for perl, but this one strives very
hard to be both extensible and easy to code with requiring as little
work by script authors as possible.  It is also one of the only ones
that supports both web environments and windowing environments
without code modification required by the script author.

%prep
%setup -q -n QWizard-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

# rpm doc examples shouldn't be executable
chmod a-x examples/*.pl
# not needed perl script that is actually just a pod generator from dist
rm -f %{buildroot}%{perl_vendorlib}/QWizard_Widgets.pl

%{_fixperms} %{buildroot}/*

%check
make test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc examples/
%doc README
%{perl_vendorlib}/auto/QWizard
%{perl_vendorlib}/QWizard*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.15-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.15-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.15-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.15-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-3m)
- rebuild against perl-5.10.1

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-2m)
- fix BuildRequires and duplications

* Tue Feb  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.15-1m)
- import from Fedora to Momonga for dnssec-tools

* Wed Sep 17 2008 Wes Hardaker <wjhns174@hardakers.net> - 3.15-1
- Update to latest upstream bug fixes

* Fri Jul 18 2008 Wes Hardaker <wjhns174@hardakers.net> - 3.14-2
- Version bump for build issues

* Tue Apr 29 2008 Wes Hardaker <wjhns174@hardakers.net> - 3.14-1
- Update to latest upstream for bug fixes and minor new features

* Sat Dec 22 2007 Wes Hardaker <wjhns174@hardakers.net> - 3.13-2
- remove patch now in base

* Fri Dec 21 2007 Wes Hardaker <wjhns174@hardakers.net> - 3.13-1
- Sync with parent 3.13 version

* Wed Dec 19 2007 Wes Hardaker <wjhns174@hardakers.net> - 3.12-2
- Changed Chart requirement to Chart::Lines to pick up proper dependencies

* Sat Dec  1 2007  Wes Hardaker <wjhns174@hardakers.net> - 3.12-1
- Initial version
