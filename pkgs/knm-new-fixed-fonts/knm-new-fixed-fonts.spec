%global momorel 5

%global		priority	69
%global		fontname	knm-new-fixed
%global		fontconf	%{priority}-%{fontname}.conf
%global		catalogue	%{_sysconfdir}/X11/fontpath.d

Name:		%{fontname}-fonts
Version:	1.1
Release:	%{momorel}m%{?dist}

Summary:	12x12 JIS X 0208 Bitmap fonts
Group:		User Interface/X
License:	GPL+

## the following upstream URL is a dead link anymore.
#URL:		http://www.din.or.jp/~storm/fonts/
#Source0:	http://www.din.or.jp/~storm/fonts/knm_new.tar.gz
Source0:	knm_new.tar.gz
Source1:	%{name}-fontconfig.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	mkfontdir fontpackages-devel

Requires:	fontpackages-filesystem
Conflicts:	fonts-japanese <= 0.20061016-11.fc8
Obsoletes:	knm_new <= 1.1-16 knm_new-fonts < 1.1-7

%description
This package provides 12x12 Japanese bitmap fonts for JIS X 0208.
The JIS X 0208 character set contains the most often used Kanji glyphs.


%prep
%setup -q -T -c -a 0

%build

%install
rm -rf $RPM_BUILD_ROOT

install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0755 -d $RPM_BUILD_ROOT%{catalogue}

install -m 0644 -p fonts/*.pcf.gz $RPM_BUILD_ROOT%{_fontdir}/

install -m 0755 -d	$RPM_BUILD_ROOT%{_fontconfig_templatedir}	\
			$RPM_BUILD_ROOT%{_fontconfig_confdir}
install -m 0644 -p	%{SOURCE1}	\
			$RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{fontconf}

ln -s	%{_fontconfig_templatedir}/%{fontconf}	\
	$RPM_BUILD_ROOT%{_fontconfig_confdir}/%{fontconf}

mkfontdir $RPM_BUILD_ROOT%{_fontdir}

# Install catalogue symlink
ln -s -f %{_fontdir} $RPM_BUILD_ROOT%{catalogue}/%{fontname}


%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf} *.pcf.gz

%lang(ja) %doc fonts/readme fonts/changes
%doc fonts/gtkrc.sample
%verify(not md5 size mtime) %{_fontdir}/fonts.dir
%{catalogue}/*


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1-5m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- import from Fedora 13

* Tue May 25 2010 Akira TAGOH <tagoh@redhat.com> - 1.1-13
- Improve the fontconfig config file to match ja as well.

* Mon Apr 19 2010 Akira TAGOH <tagoh@redhat.com> - 1.1-12
- Get rid of compare="contains".

* Mon Apr 19 2010 Akira TAGOH <tagoh@redhat.com> - 1.1-11
- Get rid of binding="same" from the fontconfig config file. (#578027)

* Fri Dec  4 2009 Akira TAGOH <tagoh@redhat.com> - 1.1-10
- revert the previous change and set the priority to 69 to ensure loading
  the rule later according to the result of
  https://bugzilla.redhat.com/show_bug.cgi?id=532237#c6
  (#532237)

* Thu Oct 29 2009 Akira TAGOH <tagoh@redhat.com> - 1.1-9
- change the priority to 65 to make this the less priority.

* Tue Oct 20 2009 Akira TAGOH <tagoh@redhat.com> - 1.1-8
- Improve the spec file etc to pass the package review.

* Tue Oct 13 2009 Akira TAGOH <tagoh@redhat.com> - 1.1-7
- Renaming to satisfy the naming schema in the fontpackages policy.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Mar 26 2009 Akira TAGOH <tagoh@redhat.com> - 1.1-5
- Clean up a spec file.
- Rebuild to correct autoprovides (#491966)

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Oct 30 2007 Akira TAGOH <tagoh@redhat.com> - 1.1-3
- Remove the upstream's dead link from the spec file. (#353941)

* Fri Sep  7 2007 Akira TAGOH <tagoh@redhat.com> - 1.1-2
- clean up

* Thu Aug 30 2007 Akira TAGOH <tagoh@redhat.com> - 1.1-1
- Split up from fonts-japanese.

