%global momorel 2

Summary: GNU Data Access
Name: libgda4
%global realname libgda
Version: 4.2.13
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Databases
URL: http://www.gnome-db.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/4.2/%{realname}-%{version}.tar.xz
NoSource: 0
# backport patch taken from upstream
Patch1:  libgda4-4.2.13-gdadataselect.patch
# backport patch taken from upstream
Patch2:  libgda4-4.2.13-glib-fix.patch
# http://git.gnome.org/browse/libgda/commit/?id=4dd8c531a7042a74e4fbd34c8614c72d67018024
Patch3:   libgda4-4.2.13-introspect.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gobject-introspection-devel >= 1.34.0
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: gamin-devel >= 0.1.10
BuildRequires: db4-devel >= 4.7.25
BuildRequires: gdbm-devel >= 1.8.3
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: postgresql-devel >= 8.3.7
BuildRequires: libxslt-devel >= 1.1.24
BuildRequires: gtk-doc
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: libbonobo-devel >= 2.24.1
BuildRequires: graphviz-devel >= 2.28.0
BuildRequires: sqlite-devel >= 3.6.11
BuildRequires: readline >= 5.2
BuildRequires: unixODBC-devel
BuildRequires: openldap-devel >= 2.4.15
BuildRequires: openssl-devel >= 1.0.0

Obsoletes: gda-postgres gda-odbc gda-mysql

Requires(post): rarian
Requires(postun): rarian
Requires(pre): hicolor-icon-theme gtk2

%description
GNU Data Access is an attempt to provide uniform access to
different kinds of data sources (databases, information
servers, mail spools, etc).
It is a complete architecture that provides all you need to
access your data.

libgda was part of the GNOME-DB project
(http://www.gnome.org/projects/gnome-db), but has been
separated from it to allow non-GNOME applications to be
developed based on it.

%package devel
Summary: GNU Data Access Development
Group: Applications/Databases
Requires: %{name} = %{version}-%{release}
Requires: mysql-devel
Requires: postgresql-devel
Requires: libxslt-devel
Requires: GConf-devel
Requires: libbonobo-devel

%description devel
GNU Data Access is an attempt to provide uniform access to
 different kinds of data sources (databases, information
 servers, mail spools, etc).
 It is a complete architecture that provides all you need to
 access your data.
 .
 libgda was part of the GNOME-DB project
 (http://www.gnome.org/projects/gnome-db), but has been
 separated from it to allow non-GNOME applications to be
 developed based on it.

%prep
%setup -q -n %{realname}-%{version}

%patch1 -p1 -b .gdadataqselect~
%patch2 -p1 -b .glib~
%patch3 -p1 -b .introspectable~

# fix encoding of AUTHORS
iconv --from=ISO-8859-1 --to=UTF-8 AUTHORS > AUTHORS.new && \
touch -r AUTHORS AUTHORS.new && \
mv AUTHORS.new AUTHORS

%ifarch x86_64
find . -name "Makefile.*" | xargs perl -p -i -e "s|/lib/libgda|/lib64/libgda|"
%endif

%build

%configure \
    --disable-static \
    --with-bdb \
    --with-postgres=yes \
    --with-mysql=yes \
    --with-odbc=yes \
    --with-ldap=yes \
    --with-sqlite=yes \
    --without-java \
    --without-goocanvas \
    --enable-system-sqlite \
    --enable-introspection=yes \
    LIBS=-lm
#    --enable-gtk-doc \
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{realname}-4.0

for desktop_file in %{buildroot}%{_datadir}/applications/*.desktop
do
 %{_bindir}/desktop-file-validate $desktop_file
done

# Fix ME... conflict vala
rm -rf %{buildroot}/%{_datadir}/vala-*

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
rarian-sk-update
update-desktop-database -q 2> /dev/null || :
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
/sbin/ldconfig
rarian-sk-update
update-desktop-database -q 2> /dev/null || :
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files -f %{realname}-4.0.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/libgda-?.0/config
%{_sysconfdir}/libgda-?.0/sales_test.db

%{_bindir}/gda-list-config
%{_bindir}/gda-list-server-op
%{_bindir}/gda-sql
%{_bindir}/gda-list-config-?.0
%{_bindir}/gda-list-server-op-?.0
%{_bindir}/gda-sql-?.0
%{_bindir}/gda-test-connection-?.0
%{_bindir}/gda-browser-?.0
%{_bindir}/gda-control-center-?.0
%{_bindir}/gdaui-demo-?.0

%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la
%dir %{_libdir}/libgda-?.0
%dir %{_libdir}/libgda-?.0/providers
%{_libdir}/libgda-?.0/providers/*.so
%{_libdir}/libgda-?.0/providers/*.la
%dir %{_libdir}/libgda-?.0/plugins
%{_libdir}/libgda-?.0/plugins/*.xml
%{_libdir}/libgda-?.0/plugins/*.so
%{_libdir}/libgda-?.0/plugins/*.la
%{_libdir}/girepository-1.0/Gda-4.0.typelib
%{_libdir}/girepository-1.0/Gdaui-4.0.typelib

%{_datadir}/libgda-?.0
%{_datadir}/applications/gda-browser-4.0.desktop
%{_datadir}/applications/gda-control-center-4.0.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/pixmaps/gda-browser-4.0.png
%{_mandir}/man1/gda-sql.1.*
%{_mandir}/man1/gda-sql-?.0.1.*

# python (why install under /usr/bin ?)
#%{_bindir}/gda_trml2html
#%{_bindir}/gda_trml2pdf


%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gtk-doc/html/libgda-?.0
%{_includedir}/libgda-?.0
%{_datadir}/gir-1.0/Gda-4.0.gir
%{_datadir}/gir-1.0/Gdaui-4.0.gir

%{_datadir}/gnome/help/gda-browser
%{_datadir}/gtk-doc/html/gda-browser

%changelog
* Sun Apr 13 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.2.13-2m)
- rebuild against graphviz-2.36.0-1m

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.13-1m)
- update to 4.2.13
- import two bug fix patches from upstream

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.12-2m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.12-1m)
- update to 4.2.12

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.8-2m)
- add LIBS-lm

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.8-1m)
- update to 4.2.8

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.7-1m)
- update to 4.2.7

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.5-5m)
- rebuild against graphviz-2.28.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.5-4m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.5-3m)
- rebuild against mysql-5.5.10

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.5-2m)
- enable to build on x86_64

* Thu Feb 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.5-1m)
- update to 4.2.5

* Sun Feb 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.1.11-1m)
- update to 4.1.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.5-7m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.5-6m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.5-5m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.5-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.5-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.5-1m)
- update to 4.0.5

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-2m)
- rebuild against openssl-0.9.8k

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.99.14-2m)
- rebuild against mysql-5.1.32

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.14-1m)
- update to 3.99.14

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.13-1m)
- update to 3.99.13

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.12-1m)
- update to 3.99.12

* Sun Feb 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.11-1m)
- update to 3.99.11

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.10-2m)
- rename libgda3 to libgda4

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.10-1m)
- update to 3.99.10
-- without-java
-- disable-gtk-doc

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-4m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.4-3m)
- rebuild against db4-4.7.25-1m

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-2m)
- rebuild against openssl-0.9.8h-1m

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.4-1m)
- update 3.0.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.2-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.2-1m)
- update 3.0.2

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.1-5m)
- rebuild against db4-4.6.21

* Sat Jul 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-4m)
- add -maxdepth 1 to del .la

* Fri Jul 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-3m)
- to trunk

* Sat May 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- rename libgda3
- to TUPPA4RI

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-9m)
- rebuild against postgresql-8.2.3

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-8m)
- rebuild against db-4.5

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-7m)
- delete libtool library

* Thu Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-6m)
- rebuild against readline

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-5m)
- depend sqlite3 -> sqlite

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.2.3-4m)
- rebuild against mysql 5.0.22-1m

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-3m)
- rebuild against openldap-2.3.19

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-2m)
- rebuild against openldap-2.3.11

* Thu Jan 12 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.3-1m)
- version up for db4-4.3.29
- use sqlite3 instead of sqlite

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.0-5m)
- rebuild against postgresql-8.1.0

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.2.0-4m)
- rebuild against for MySQL-4.1.14

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-3m)
- rebuild against postgresql-8.0.2.

* Fri Feb  4 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-2m)
- enable x86_64.

* Mon Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0
- remove patch libgda-1.0.3-gtkdoc_fixes.patch
- remove patch libgda-1.0.4-gcc34.patch
- add patch libgda-1.2.0-gcc34.patch

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-5m)
- add Patch1: libgda-1.0.4-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sat Aug 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-4m)
- add BuildPrereq: sqlite-devel

* Sat Aug 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.4-3m)
  revise Source0 URL

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.4-2m)
- rebuild against ncurses 5.3.

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-2m)
- rebuild against for libxml2-2.6.8

* Mon Mar 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.3-1m)
- update to version 1.0.3
- revise URL

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.2-3m)
- rebuild against postgresql-7.4.1

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-2m)
- accept gdbm-1.8.0 or newer

* Fri Dec 12 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.2-1m)
- version up.

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gdbm-1.8.0

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.91.0-2m)
- rebuild against for MySQL-4.0.14-1m

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.91.0-1m)
- version 0.91.0

* Thu Jul 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.90.0-2m)
- add BuildPrereq: scrollkeeper

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.90.0-1m)
- version 0.90.0

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.12.1-2m)
  rebuild against cyrus-sasl2

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.1-1m)
- version 0.12.1

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.0-1m)
- version 0.12.0

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.0-1m)
- version 0.11.0

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.10.0-3m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.0-2m)
- rebuild against for gdbm

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.0-1m)
- version 0.10.0

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-2m)
- rebuild against for postgresql

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-1m)
- version 0.9.0

* Mon Oct 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.199-1m)
- version 0.8.199

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.193-2m)
- remove omf

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.193-1m)
- version 0.8.193

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.192-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.192-2m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.192-1m)
- version 0.8.192

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-14k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-12k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-10k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-8k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-6k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-2k)
- version 0.8.191

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-8k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-6k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Thu May  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-6k)
- remove DB bindings

* Wed May  8 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-4k)
- remove scrollkeeper depends

* Tue May 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-2k)
- version 0.8.190

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-12k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-2k)
- version 0.8.105

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.104-6k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.104-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Thu Mar 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.104-2k)
- version 0.8.104

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-34k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-32k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-30k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.103-28k)
- change schema handring

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.103-26k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-24k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-22k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-20k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-18k)
- change subpackage name to libgda-hoge

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-16k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-14k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-12k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-10k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-8k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-6k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-4k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-2k)
- version 0.8.103
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-18k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-16k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-14k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-12k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-10k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-8k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-4k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-2k)
- version 0.8.102
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.101-4k)
- rebuild against for linc-0.1.15

* Tue Jan  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.99-2k)
- version 0.8.99
- GNOME2 env

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.2.93-4k)
- nigittenu

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (0.2.91-12k)
- rebuild against postgresql 7.1.3-4k.
- add buildprereq.

* Sat Oct 27 2001 Toru Hoshina <t@kondara.org>
- (0.2.91-10k)
- rebuild against postgresql 7.1.3.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (0.2.91-8k)
- rebuild against gettext 0.10.40.

* Sun Oct  7 2001 WATABE Toyokazu <toy2@kondara.org>
- (0.2.91-6k)
- add bonobo-devel as PreRequire package.

* Fri Oct  5 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.2.91-4k)
- your Makefile is broken (translated by nakaya)

* Thu Oct  4 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.2.91-2k)
- version 0.2.91

* Thu Aug 23 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.90-4k)
- add libgda-lemon.c.patch for Alpha building from dora

* Mon Aug 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.2.90

* Thu Jul  5 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.2.10

* Sun May 21 2001 Shingo Akagaki <dora@kondara.org>
- add build prerex: GConf-devel

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Sat Sep 2 2000 Rodrigo Moya <rodrigo@linuxave.net>
- Initial spec imported from old GNOME-DB spec
