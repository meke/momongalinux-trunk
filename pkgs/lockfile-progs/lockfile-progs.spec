%global momorel 3

Name:           lockfile-progs
Version:        0.1.15
Release:        %{momorel}m%{?dist}
Summary:        Command-line programs to safely lock and unlock files and mailboxes

License:        GPLv2
Group:          Applications/System
# debian package, no real upstream source
URL:            http://packages.qa.debian.org/l/lockfile-progs.html
Source0:        http://ftp.de.debian.org/debian/pool/main/l/%{name}/%{name}_%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  liblockfile-devel


%description
lockfile-progs provide a method to lock and unlock mailboxes and  files
safely (via liblockfile).

%prep
%setup -q -n sid
sed -i 's/\(cd bin && ln \)/\1 -sf /' Makefile

%build
make %{_smp_mflags} CFLAGS='%{optflags}'

%check
make check

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
cp -r --preserve=all bin/* %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_mandir}/man1
cp -r --preserve=all man/* %{buildroot}/%{_mandir}/man1/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/lockfile-check
%{_bindir}/lockfile-create
%{_bindir}/lockfile-remove
%{_bindir}/lockfile-touch
%{_bindir}/mail-lock
%{_bindir}/mail-touchlock
%{_bindir}/mail-unlock
%{_mandir}/man1/*.1*
%doc COPYING

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.15-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.15-2m)
- rebuild for new GCC 4.5

* Sun Sep 12 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.15-1m)
- import from Fedora

* Sat Aug 14 2010 Matthias Runge <mrunge@matthias-runge.de> 0.1.15-2
- correct make-invocation, move it to build
- COPYING in as doc
- remove {__-invocations, replace by plain calls

* Fri Aug 13 2010 Matthias Runge <mrunge@matthias-runge.de> 0.1.15-1
- new version from upstream

* Fri Aug 13 2010 Matthias Runge <mrunge@matthias-runge.de> 0.1.13-2
- cleanup

* Wed Apr 28 2010 Matthias Runge <mrunge@matthias-runge.de> 1.3.8-1
- initial spec
