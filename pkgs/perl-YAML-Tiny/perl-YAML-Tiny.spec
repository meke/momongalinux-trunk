%global         momorel 1

Name:           perl-YAML-Tiny
Version:        1.63
Release:        %{momorel}m%{?dist}
Summary:        Read/Write YAML files with as little code as possible
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/YAML-Tiny/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/YAML-Tiny-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-Carp
BuildRequires:  perl-Cwd >= 0.80
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Fcntl
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Getopt-Long
BuildRequires:  perl-IO
BuildRequires:  perl-lib
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple >= 0.98
BuildRequires:  perl-version
Requires:       perl-Carp
Requires:       perl-Data-Dumper
Requires:       perl-Fcntl
Requires:       perl-List-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
YAML::Tiny is a perl class for reading and writing YAML-style files,
written with as little code as possible, reducing load time and
memory overhead.

%prep
%setup -q -n YAML-Tiny-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/YAML/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.63-1m)
- rebuild against perl-5.20.0
- update to 1.63

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.61-1m)
- update to 1.61

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.58-1m)
- update to 1.58
- rebuild against perl-5.18.2

* Fri Sep 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.56-1m)
- update to 1.56

* Fri Sep 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-1m)
- update to 1.55

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-2m)
- rebuild against perl-5.14.2

* Fri Jun 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.50

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.48-2m)
- rebuild for new GCC 4.6

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48

* Thu Dec 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.44-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.43-1m)
- update to 1.43

* Fri Jun 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-2m)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.36-2m)
- rebuild against rpm-4.6

* Thu Jan  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Thu May 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Mon May  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.27-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Tue Mar 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- Specfile autogenerated by cpanspec 1.74 for Momonga Linux.
