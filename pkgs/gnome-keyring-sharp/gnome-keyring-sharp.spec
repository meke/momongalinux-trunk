%global momorel 1
%global svn_rev 133722
%global debug_package %{nil}

Name:           gnome-keyring-sharp
Version:        1.0.1
Release: %{momorel}m%{?dist}
Summary:        Mono implementation of GNOME Keyring

Group:          System Environment/Libraries
License:        MIT
URL:            http://www.mono-project.com/Libraries#Gnome-KeyRing-Sharp
# Tarfile created from svn snapshot
# svn co -r %{svn-rev} \
#   svn://anonsvn.mono-project.com/source/trunk/gnome-keyring-sharp \
#   gnome-keyring-sharp-%{version}
# tar cjf gnome-keyring-sharp-%{version}-r%{svn_rev}.tar.bz2 --exclude=.svn \
#   gnome-keyring-sharp-%{version}
Source0:        gnome-keyring-sharp-%{version}-r%{svn_rev}.tar.bz2
# Patch to directly p/invoke libgnome-keyring instead of using
# deprecated socket interface taken from upstream bug report:
# https://bugzilla.novell.com/show_bug.cgi?id=589166
Patch1:         gnome-keyring-sharp-1.0.1-new-api.diff
Patch2:         gnome-keyring-sharp-1.0.1-monodoc-dir.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Mono only available on these:
ExclusiveArch: %ix86 x86_64 ppc ppc64 ia64 %{arm} sparcv9 alpha s390x

BuildRequires:  autoconf automake libtool
BuildRequires:  mono-devel ndesk-dbus-devel monodoc
BuildRequires:  gtk-sharp2-devel libgnome-keyring-devel

%description
gnome-keyring-sharp is a fully managed implementation of libgnome-keyring.

When the gnome-keyring-daemon is running, you can use this to retrive/store
confidential information such as passwords, notes or network services user
information.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files
for developing applications that use %{name}.

%package        doc
Summary:        Documentation for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
Requires:       monodoc

%description    doc
The %{name}-doc package contains documentation
for %{name}.


%prep
%setup -q
%patch1 -p0 -F 2 -b .new-api
%patch2 -p1 -b .monodoc-dir


%build
autoreconf -f -i
%configure --disable-static
make
# sharing violation when doing parallel build
#%{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
strip %{buildroot}%{_libdir}/libgnome-keyring-sharp-glue.so
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_prefix}/lib/mono/gnome-keyring-sharp-1.0
%{_prefix}/lib/mono/gac/Gnome.Keyring
%{_libdir}/libgnome-keyring-sharp-glue.so

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/%{name}-1.0.pc

%files doc
%defattr(-,root,root,-)
%{_prefix}/lib/monodoc/sources/Gnome.Keyring.*


%changelog
* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-1m)
- reimport from fedora

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-6m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- full rebuild for mo7 release

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- enable to build on x86_64

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- initial build
