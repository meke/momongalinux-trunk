%global momorel 2
%global qtver 4.8.1
%global kdever 4.8.4
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: A multimedia mplayer/gstreamer/libxine frontend for KDE
Name: kmplayer
Version: 0.11.3d
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: Applications/Multimedia
URL: http://kmplayer.kde.org/
Source0: http://kmplayer.kde.org/pkgs/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: mplayer
Requires: oxygen-icons
Requires: oxygen-icons-scalable
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: gettext
BuildRequires: glib2-devel

%description
KMPlayer can play all the audio/video supported by mplayer/libxine/Gstreamer from local
file or url, be embedded inside Konqueror and KHTML and play DVD's.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# conflicts with oxygen-icons-4.2.85
rm -f %{buildroot}%{_kde4_iconsdir}/oxygen/*/apps/%{name}.png

# conflicts with oxygen-icons-scalable-4.2.85
rm -f %{buildroot}%{_kde4_iconsdir}/oxygen/*/apps/%{name}.svgz

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/knpplayer
%{_kde4_bindir}/kphononplayer
%{_kde4_libdir}/kde4/lib%{name}part.so
%{_kde4_libdir}/libkdeinit4_%{name}.so
%{_kde4_libdir}/lib%{name}common.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_configdir}/%{name}rc
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_kde4_iconsdir}/*/*/*/%{name}.svgz
%{_kde4_datadir}/kde4/services/%{name}_part.desktop
%{_kde4_datadir}/doc/HTML/en/%{name}
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.3d-2m)
- rebuild for glib 2.33.2

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3d-1m)
- update to 0.11.3d

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3c-1m)
- update to 0.11.3c

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3b-1m)
- update to 0.11.3b

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3a-1m)
- update to 0.11.3a

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3-1m)
- update to 0.11.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.2c-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.2c-2m)
- rebuild for new GCC 4.5

* Tue Oct  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2c-1m)
- version 0.11.2c

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2b-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.2b-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.2b-3m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2b-2m)
- rebuild against qt-4.6.3-1m

* Sun Apr 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.2b-1m)
- version 0.11.2b

* Wed Mar 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.2a-1m)
- version 0.11.2a

* Sun Mar  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.2-1m)
- version 0.11.2
- License: GPLv2 and LGPLv2+

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.1b-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.1b-1m)
- version 0.11.1b

* Mon May 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.1a-1m)
- version 0.11.1a

* Wed May 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.1-1m)
- update to 0.11.1

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.0a-2m)
- remove kmplayer.svgz
  to avoid conflicting with oxygen-icon-theme-scalable-4.2.2

* Tue Jan 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.0a-1m)
- version 0.11.0a

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-0.20080606.2m)
- rebuild against rpm-4.6

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.0-0.20080606.1m)
- update to 20080606 svn snapshot for KDE 4.1 Beta 1

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.0-0.20080123.4m)
- revise %%{_docdir}/HTML/*/kmplayer/common

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.0-0.20080123.3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.0-0.20080123.2m)
- rebuild against gcc43

* Wed Jan 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.0-0.20080123.1m)
- update to 20080123 svn snapshot for KDE4
- License: GPLv2

* Wed Dec 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0c-1m)
- update to 0.10.0c

* Sat Dec  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0b-1m)
- update to 0.10.0b

* Sat Sep 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0a-1m)
- update to 0.10.0a

* Thu May 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4a-1m)
- initial package for Momonga Linux
