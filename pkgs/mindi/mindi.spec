%global momorel 1
Summary:	A program that creates emergency boot disks/CDs using your kernel, tools and modules.
Name:		mindi
Version:	2.1.7
Release: %{momorel}m%{?dist}
License:	GPL
Group:		Applications/System
Url:		http://www.mondorescue.org
Source0:	ftp://ftp.mondorescue.org/src/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	bzip2 >= 0.9, genisoimage, ncurses, binutils, gawk
Autoreq:	0

%description
Mindi takes your kernel, modules, tools and libraries, and puts them on N
bootable disks (or 1 bootable CD image). You may then boot from the disks/CD
and do system maintenance - e.g. format partitions, backup/restore data,
verify packages, etc.

%prep
%setup -q
%build
%ifarch ia64
make -f Makefile.parted2fdisk
%endif

%install
rm -Rf /usr/local/share/mindi
%{__rm} -rf %{buildroot}
MINDIDIR=%{buildroot}%{_datadir}/mindi
%{__mkdir_p} $MINDIDIR %{buildroot}%{_bindir} %{buildroot}%{_sysconfdir}/mindi
%{__mv} deplist.txt %{buildroot}%{_sysconfdir}/mindi/
%{__cp} -af * $MINDIDIR
%ifarch ia64
	make -f Makefile.parted2fdisk install
	mv %{buildroot}%{_datadir}/mindi/rootfs/bin/busybox-ia64 %{buildroot}%{_datadir}/mindi/rootfs/bin/busybox
	mv %{buildroot}%{_datadir}/mindi/rootfs/sbin/parted2fdisk-ia64 %{buildroot}%{_datadir}/mindi/rootfs/sbin/parted2fdisk
%else
#	mv $MINDIDIR/rootfs/bin/busybox-i386 $MINDIDIR/rootfs/bin/busybox
	%{__ln_s} fdisk $MINDIDIR/rootfs/sbin/parted2fdisk
%endif
rm -f $MINDIDIR/rootfs/bin/busybox-ia64 $MINDIDIR/rootfs/sbin/parted2fdisk-ia64 $MINDIDIR/rootfs/bin/busybox-i386
#
# These are installed twice if not removed here
( cd $MINDIDIR
rm -f CHANGES INSTALL COPYING README TODO
)
cd %{buildroot}%{_bindir}
%{__ln_s} -f %{_datadir}/mindi/mindi .
%{__ln_s} -f %{_datadir}/mindi/parted2fdisk.pl .
%{__ln_s} -f %{_datadir}/mindi/analyze-my-lvm .
chmod +x $MINDIDIR/mindi

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/mindi/deplist.txt
%doc ChangeLog INSTALL COPYING README* TODO NEWS
# %%attr(755,root,root) %%{_datadir}/mindi/mindi
%{_datadir}/mindi
%{_bindir}/analyze-my-lvm
%{_bindir}/mindi
%{_bindir}/parted2fdisk.pl

%changelog
* Fri Oct 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.7-1m)
- update to 2.1.7

* Fri Dec 12 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.04-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.04-5m)
- rebuild against gcc43

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.04-4m)
- good-bye cdrtools and welcome cdrkit

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.04-3m)
- change Source URL

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.04-2m)
- rebuild against libtermcap and ncurses

* Sat Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.04-1m)
- update

* Tue Aug 10 2004 Kimitake Shibata <siva@momonga-linux.org>
- (1.03-2m)
- typo

* Tue Jul 10 2004 Kimitake Shibata <siva@momonga-linux.org>
- (1.03-1m)
- New Version

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.87-4m)
- rebuild against ncurses 5.3.

* Thu Dec  2 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.87-3m)
- add source

* Mon Dec  1 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.87-2m)
- roll Back to ver 0.87

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-0.0.20031122001m)
- s/Copyright:/License:/

* Fri Nov 14 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.87-1m)
- First Relese

