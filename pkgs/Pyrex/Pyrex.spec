%global momorel 5
%{expand: %%define pyver %(python -c 'import sys;print(sys.version[0:3])')}

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A Language for Writing Python Extension Modules
Name: Pyrex
Version: 0.9.9
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Development/Libraries
URL: http://www.cosc.canterbury.ac.nz/~greg/python/Pyrex/
Source0: http://www.cosc.canterbury.ac.nz/greg.ewing/python/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
#Copyright: UNKNOWN
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.7
BuildArchitectures: noarch

%description
Pyrex lets you write code that mixes Python and C data types any way you want, 
and compiles it into a C extension for Python.

%prep
%setup -q

%build
python setup.py build

%install
python setup.py install --root=%{buildroot} --record=INSTALLED_FILES
# /usr/lib/rpm/brp-python-bytecompile will generate *.pyc and *.pyo
sed --in-place -e 's,\.py$,.py*,g' INSTALLED_FILES

%clean
%{__rm} -rf %{buildroot}

%files -f INSTALLED_FILES
%defattr(-,root,root)
%dir %{python_sitelib}/Pyrex
%dir %{python_sitelib}/Pyrex/DistutilsOld
%dir %{python_sitelib}/Pyrex/Unix
%dir %{python_sitelib}/Pyrex/Distutils
%dir %{python_sitelib}/Pyrex/Mac
%dir %{python_sitelib}/Pyrex/Compiler
%dir %{python_sitelib}/Pyrex/Plex

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Thu May  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8.5-4m)
- fix build on x86_64

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.8.5-3m)
- add %%dir

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8.5-3m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8.5-1m)
- update 0.9.8.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.4-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.8.4-2m)
- rebuild agaisst python-2.6.1-1

* Mon Jun 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.4-1m)
- update 0.9.8.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4.1-3m)
- rebuild against gcc43

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.9.4.1-2m)
- rebuild against python-2.5-9m

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4.1-1m)
- update 0.9.4.1

* Thu Jan 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3.1-4m)
- rebuild against python-2.5

* Sun Dec 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.9.3.1-3m)
- no NoSorce

* Wed Nov  2 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.3.1-2m)
- revised spec

* Sun Oct 30 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.3.1-1m)
- update to 0.9.3.1
 
* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.3-2m)
- build against python-2.4.2

