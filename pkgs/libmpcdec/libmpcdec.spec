%global momorel 6

Summary: Musepack audio decoding library
Name: libmpcdec
Version: 1.2.6
Release: %{momorel}m%{?dist}
License: BSD 
Group: System Environment/Libraries
URL: http://www.musepack.net/
Source: http://files.musepack.net/source/libmpcdec-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Musepack is an audio compression format with a strong emphasis on high quality.
It's not lossless, but it is designed for transparency, so that you won't be
able to hear differences between the original wave file and the much smaller
MPC file.
It is based on the MPEG-1 Layer-2 / MP2 algorithms, but has rapidly developed
and vastly improved and is now at an advanced stage in which it contains
heavily optimized and patentless code.

%package devel
Summary: Development files for the Musepack audio decoding library
Group:	 Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q

%build
%configure --disable-static

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

#Unpackaged files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_libdir}/libmpcdec.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/mpcdec
%{_libdir}/libmpcdec.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6-1m)
- version 1.2.6
- clean up spec file

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-2m)
- rebuild against gcc43

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-1m)
- import from fc7 to Momonga

* Tue Aug 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 
- fc6 respin

* Wed Aug 09 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.2.2-3
- fc6 respin

* Sat Apr 01 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.2.2-2
- License: BSD

* Thu Jan 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.2.2-1
- libmpcdec-1.2.2

* Thu Jan 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.1-2
- cleanup

* Fri Jun 17 2005 Mihai Maties <mihai@xcyb.org> 1.1-1
- update to 1.1
- changed license to BSD
- updated the spec to use autotools

* Fri Nov 26 2004 Matthias Saou <http://freshrpms.net/> 1.0.2-1
- Initial RPM release.
- Include the mandatory copy of the LGPL (there is none in the sources...).

