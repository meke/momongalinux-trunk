%global momorel 2

%global srcver %{version}
%global freetypever 2.3.9
%global fontsdir %{_datadir}/fonts
%global config_sample_dir %{_datadir}/config-sample/%{name}

Summary: Font configuration and customization library
Name: fontconfig
Version: 2.11.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://fontconfig.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://fontconfig.org/release/fontconfig-%{srcver}.tar.gz
NoSource: 0
# original Fedora's configuration files
Source1: 25-no-bitmap-fedora.conf
# Momonga configuration files
Source10: dot.fonts.conf
Source11: momonga-bitmaps.conf
Source12: momonga-blacklist.conf
Source13: momonga-generic.conf
Source14: momonga-hinting.conf
Source15: momonga-language-ja.conf
Source16: momonga.local.conf
Source17: momonga-nonlatin.conf
# from opensuse
Patch10: bugzilla-158573-turn-off-hinting-when-embolden.patch
Requires(post): coreutils grep
Requires: freetype >= %{freetypever}
Requires: fontpackages-filesystem
Requires(post): coreutils
#loop Requires(post): fontconfig
# for use fc-cache but loop
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: docbook-utils-pdf
BuildRequires: elinks
BuildRequires: expat-devel >= 2.0.0-2m
BuildRequires: freetype-devel >= %{freetypever}
BuildRequires: gawk
BuildRequires: libtool
BuildRequires: perl
# Hebrew fonts referenced in fonts.conf changed names in fonts-hebrew-0.100
Conflicts: fonts-hebrew < 0.100
# Conflict with pre-modular X fonts, because they moved and we
# reference the new path in %%configure
Conflicts: fonts-xorg-base, fonts-xorg-syriac

%description
Fontconfig is designed to locate fonts within the
system and select them according to requirements specified by
applications.

%package devel
Summary: Font configuration and customization library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: freetype-devel >= %{freetypever}
Requires: pkgconfig

%description devel
The fontconfig-devel package includes the header files,
and developer docs for the fontconfig package.

Install fontconfig-devel if you want to develop programs which
will use fontconfig.

%prep
%setup -q -n %{name}-%{srcver}

%patch10 -p1 -b .bold-quality

autoreconf -fi

%build

# We don't want to rebuild the docs, but we want to install the included ones.
export HASDOCBOOK=no

%configure \
	--with-default-fonts=%{_datadir}/fonts \
	--with-add-fonts=%{_datadir}/X11/fonts/Type1,%{_datadir}/X11/fonts/TTF,/usr/local/share/fonts

%make
make check

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# install Fedora config file
install -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/fonts/fedora-no-bitmap.conf

# install Momonga config files
install -m 0644 %{SOURCE11} %{buildroot}%{_sysconfdir}/fonts/
install -m 0644 %{SOURCE12} %{buildroot}%{_sysconfdir}/fonts/
install -m 0644 %{SOURCE13} %{buildroot}%{_sysconfdir}/fonts/
install -m 0644 %{SOURCE14} %{buildroot}%{_sysconfdir}/fonts/
install -m 0644 %{SOURCE15} %{buildroot}%{_sysconfdir}/fonts/
install -m 0644 %{SOURCE17} %{buildroot}%{_sysconfdir}/fonts/

# tune links in conf.d:
pushd %{buildroot}%{_sysconfdir}/fonts/conf.d
    ln -s ../fedora-no-bitmap.conf 25-fedora-no-bitmap.conf
## reserve...
#    ln -s ../momonga-generic.conf 41-generic.conf
    ln -s ../momonga-language-ja.conf 41-language-ja.conf
    rm -f 50-user.conf
    rm -f 51-local.conf
    ln -s ../momonga-bitmaps.conf 50-momonga-bitmaps.conf
    ln -s ../momonga-hinting.conf 51-momonga-hinting.conf
    ln -s ../momonga-blacklist.conf 52-momonga-blacklist.conf
    ln -s ../conf.avail/51-local.conf 55-local.conf
    ln -s ../conf.avail/50-user.conf 56-user.conf
    rm -f 65-nonlatin.conf
    ln -s ../momonga-nonlatin.conf 65-nonlatin.conf
popd

# install local.conf
install -m 644 %{SOURCE16} %{buildroot}%{_sysconfdir}/fonts/local.conf

# install config sample
mkdir -p %{buildroot}%{config_sample_dir}
install -m 644 %{SOURCE10} %{buildroot}%{config_sample_dir}/

## don't package any files in the cache directory, only the directory itself:
##rm -f %{buildroot}%{_localstatedir}/cache/fontconfig/*

# move installed doc files back to build directory to package themm
# in the right place
mv %{buildroot}%{_docdir}/fontconfig/* .
rm -rf %{buildroot}%{_docdir}/fontconfig

# All font packages depend on this package, so we create
# and own /usr/share/fonts
mkdir -p %{buildroot}%{_datadir}/fonts

# Remove unpackaged files
rm %{buildroot}%{_libdir}/*.la
#rm %{buildroot}%{_libdir}/*.a

mkdir -p %{buildroot}%{fontsdir}/type1

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

umask 0022

# Remove stale caches
rm -f %{_localstatedir}/cache/fontconfig/????????????????????????????????.cache-2
rm -f %{_localstatedir}/cache/fontconfig/stamp

# Force regeneration of all fontconfig cache files
# The check for existance is needed on dual-arch installs (the second
#  copy of fontconfig might install the binary instead of the first)
# The HOME setting is to avoid problems if HOME hasn't been reset
if [ -x %{_bindir}/fc-cache ] && %{_bindir}/fc-cache --version 2>&1 | grep -q %{version} ; then
  HOME=/root %{_bindir}/fc-cache -f
fi

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README
%doc fontconfig-user*
%dir %{_datadir}/fontconfig/conf.avail
%{_datadir}/fontconfig/conf.avail/*.conf
%config %{_sysconfdir}/fonts/conf.d/*.conf
%doc %{_sysconfdir}/fonts/conf.d/README
%config %{_sysconfdir}/fonts/fedora-no-bitmap.conf
%config %{_sysconfdir}/fonts/fonts.conf
%{_datadir}/xml/fontconfig/fonts.dtd
%config(noreplace) %{_sysconfdir}/fonts/local.conf
%config %{_sysconfdir}/fonts/momonga-bitmaps.conf
%config %{_sysconfdir}/fonts/momonga-blacklist.conf
%config %{_sysconfdir}/fonts/momonga-generic.conf
%config %{_sysconfdir}/fonts/momonga-hinting.conf
%config %{_sysconfdir}/fonts/momonga-language-ja.conf
%config %{_sysconfdir}/fonts/momonga-nonlatin.conf
%{_bindir}/fc-cache
%{_bindir}/fc-cat
%{_bindir}/fc-list
%{_bindir}/fc-match
%{_bindir}/fc-pattern
%{_bindir}/fc-query
%{_bindir}/fc-scan
%{_bindir}/fc-validate
%{_libdir}/libfontconfig.so.*
%{config_sample_dir}
%{fontsdir}/type1
%{_mandir}/man1/*.1*
%{_mandir}/man5/*.5*
%dir %{_localstatedir}/cache/fontconfig

%files devel
%defattr(-, root, root)
%doc fontconfig-devel*
%{_includedir}/fontconfig
%{_libdir}/libfontconfig.so
%{_libdir}/pkgconfig/fontconfig.pc
%{_mandir}/man3/*.3*

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linxu.org>
- (2.11.1-2m)
- fix Fontconfig warning for 41-language-ja.conf

* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linxu.org>
- (2.11.1-1m)
- update to 2.11.1

* Mon Jun 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0
- delete patch1

* Fri Feb 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.0-8m)
- system default MigMix font

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.0-5m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-4m)
- add Takao Fonts to configs

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.0-3m)
- fix up Requires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.0-2m)
- use Requires

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.3-1m)
- update to 2.7.3

* Tue Jul 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1
- use Requires(post)

* Sun Jul 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.99-0.20090508.5m)
- add VL Gothic to sans-serif in momonga-nonlatin.conf

* Sat Jul 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.99-0.20090508.4m)
- revise momonga-language-ja.conf and momonga-nonlatin.conf
- http://bugs.freedesktop.org/show_bug.cgi?id=20911

* Sat Jul 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.99-0.20090508.3m)
- change Japanese default font from VL to Meguri

* Tue Jul 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.99-0.20090508.2m)
- change Japanese default font from IPA PGothic to VL PGothic for Qt4

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.99-0.20090508.1m)
- update to 2.6.99.behdad.20090508

* Sun Apr 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.99-0.20090318.1m)
- version 2.6.99.behdad.20090318
- modify momonga-blacklist.conf for full anti-aliasing
- remove momonga-generic.conf
- use autoreconf for libtool-1.5.26
- sort %%files
- waiting for ipa-family

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.0-2m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.0-5m)
- modify configuration files

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.0-4m)
- release directories owned by filesystem

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.0-3m)
- rebuild against gcc43

* Fri Dec 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.0-2m)
- modify %%post to update caches

* Mon Dec 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.0-1m)
- update 2.5.0

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-16m)
- fix momonga-blacklist.conf

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-15m)
- remove no-bitmaps.conf and update momonga-blacklist.conf for Armenian and Thai
- exculde %%{_datadir}/X11/non-ttf-directories to avoid breaking anti ailiasing without no-bitmaps.conf

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-14m)
- set fonts PATH to %%{_datadir}/fonts

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-13m)
- remove "rh_prefer_bitmaps" from momonga-hinting.conf

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-12m)
- modify momonga-hinting.conf (remove "pixelsize")

* Thu Apr 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-11m)
- clean up conf.d/

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-10m)
- move momonga-blacklist.conf to 52 for local.conf and user.conf

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-9m)
- add momonga-blacklist.conf

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-8m)
- add momonga-alias.conf

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-7m)
- re-tune up for Momonga Linux

* Mon Apr  9 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2-6m)
- sync fc 2.4.2-2

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-5m)
- BuildRequires: freetype2-devel -> freetype-devel

* Thu Jan 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-4m)
- import AppleRoman-DynaFont.patch from http://www.kde.gr.jp/~akito/
- remove merged slighthint.patch

* Tue Jan 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-3m)
- revive local.conf

* Mon Jan 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-2m)
- update momonga.fonts.conf
- modify %%files

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-1m)
- update 2.4.2

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-14m)
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.3-13m)
- rebuild against expat-2.0.0-1m

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-12m)
- M+1P+IPAG was added to momonga.fonts.conf. 
-- need anaconda graphical installer

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-11m)
- M+2P+IPAG was added to momonga.fonts.conf. 
-- need anaconda graphical installer

* Tue Jun 27 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.3-10m)
- now IPAPGothic/Mincho is default font

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.3-9m)
- revised momonga.fonts.conf to include directory "/usr/share/X11/fonts/TTF" from truetype-fonts-truetype

* Sat Apr 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.3-8m)
- add ~/.fonts to momonga.fonts.conf
- revise option of configure for new X

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.3-7m)
- update momonga.fonts.conf for new X

* Sun Mar 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.3-6m)
- "%%config fonts.conf" in %%files section to save old fonts.conf as .rpmsave. (Sometimes it is needed to modify fotns.conf, not local.conf.)

* Fri Apr  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.3-5m)
- modify momonga.fonts.conf to use hinting

* Fri Nov 19 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.2.3-4m)
- import patch4
  [Momonga-devel.ja:02906]
  thanks TetsuO.S.

* Sun Nov 14 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.2.3-3m)
- remake patch4
- add patch5

* Sat Nov 13 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.2.3-2m)
- create patch4.
- remove some patches.

* Sat Nov 13 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.2.3-1m)
- update to 2.3.5
- some patch from FC

* Tue Jul  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.1-8m)
- move antialias settings before loading user/local config files
  [Momonga-devel.ja:02613] thanks to Yoshiyuki Doi

* Sun Jun 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.1-7m)
- use sazanami instead of kochi by default

* Thu Apr  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.1-6m)
- revise momonga.fonts.conf for XFree86-4.3.0.2-3m

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.1-5m)
- revised spec for enabling rpm 4.2.

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.1-4m)
- specfile cleanup

* Fri Nov  7 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.2.1-3m)
- append patch1.

* Wed Oct 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-2m)
- change License: MIT to MIT/X for speclint
- fix files devel section warning 'File listed twice'

* Fri Aug 22 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.2.1-1m)
- update to 2.2.1
- patch0 forgery...

* Thu Aug 21 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.0-5m)
- hold /usr/share/fonts/CID, /usr/share/fonts/type1,
  and /usr/share/fonts/truetype.

* Wed Aug 20 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.0-4m)
- config_sample -> config-sample ([Momonga-devel-ja:02027]).

* Thu Aug  7 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.0-3m)
- Obsoletes: XFree86 < 4.3.0.1-3m -> Conflicts: XFree86 < 4.3.0.1-3m

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.0-2m)
- add config-sample.
- modify fonts.conf.
- obsoletes the old XFree86-{libs,devel,tools}.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.0-1m)
- separated from XFree86
- version up.
-* Fri Mar  7 2003 Mike A. Harris <mharris@redhat.com> 2.1.92-1
-- Removed man1/* and added man5/* to main package and man3/* to devel package
-- Added missing defattr(-, root, root) to main package
-- Added HTML and text development documentation to -devel subpackage
-
-* Wed Mar  5 2003 Mike A. Harris <mharris@redhat.com>
-- Added back the configure macro options --disable-docs because otherwise
-  fontconfig installs docs into /usr/share/doc/fontconfig (with no version
-  number) unconditionally, causing RPM to fail the build due to unpackaged
-  files terminate build
-
-* Wed Mar  5 2003 Mike A. Harris <mharris@redhat.com>
-- Removed commented out rpm macro define at top of spec file, replacing it with
-  a simple explanation, since rpm macros are expanded by rpm even in comments
-- Changed /usr/bin to _bindir in BuildRequires lines
-- Cleaned up rpm postinstall script, and made fc-cache use _bindir
-- Reorganized file manifest lists
-
-* Sun Mar  2 2003 Owen Taylor <otaylor@redhat.com>
-- Various improvements from Red Hat spec file
-
-* Sun Mar  2 2003 Mike A. Harris <mharris@redhat.com>
-- Initial changelog entry
