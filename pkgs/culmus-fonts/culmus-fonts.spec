%global momorel 5

%define fontname culmus
%define fontconf 65-%{fontname}

%define common_desc \
The culmus-fonts package contains fonts for the display of\
Hebrew from the Culmus project.


Name:           %{fontname}-fonts
Version:        0.104
Release:        %{momorel}m%{?dist}
Summary:        Fonts for Hebrew from Culmus project

Group:          User Interface/X
License:        GPLv2
URL:            http://culmus.sourceforge.net
Source0:        http://downloads.sourceforge.net/sourceforge/%{fontname}/%{fontname}-%{version}.tar.gz
Source1:        %{fontconf}-aharoni-clm.conf
Source2:        %{fontconf}-caladings-clm.conf
Source3:        %{fontconf}-david-clm.conf
Source4:        %{fontconf}-drugulin-clm.conf
Source5:        %{fontconf}-ellinia-clm.conf
Source6:        %{fontconf}-frank-ruehl-clm.conf
Source7:        %{fontconf}-miriam-clm.conf
Source8:        %{fontconf}-miriam-mono-clm.conf
Source9:        %{fontconf}-nachlieli-clm.conf
Source10:       http://downloads.sourceforge.net/project/culmus/culmus/0.104/%{fontname}-type1-%{version}.tar.gz
Obsoletes:      culmus-fonts < 0.102-1
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel

%description
%common_desc
Meta-package of Culmus fonts which installs various families of culmus project.

%package common
Summary:        Common files of culmus-fonts
Group:          User Interface/X
Requires:       fontpackages-filesystem
Obsoletes:      culmus-fonts < 0.102-1
Obsoletes:      fonts-hebrew
%description common
%common_desc

This package consists of files used by other %{name} packages.


%package -n %{fontname}-fonts-compat
Summary:          Compatibility files of Culmus font families
Group:            User Interface/X
Obsoletes:        %{fontname}-fonts-common < 0.102-5m
Obsoletes:        %{fontname}-aharoni-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-caladings-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-david-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-drugulin-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-ellinia-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-frank-ruehl-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-miriam-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-miriam-mono-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-nachlieli-clm-fonts < 0.102-5m
Obsoletes:        %{fontname}-yehuda-clm-fonts < 0.102-5m
Obsoletes:        culmus-fonts < 0.104-1m

Requires:         %{fontname}-fonts-common = %{version}-%{release}
Requires:         %{fontname}-aharoni-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-caladings-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-david-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-drugulin-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-ellinia-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-frank-ruehl-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-miriam-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-miriam-mono-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-nachlieli-clm-fonts = %{version}-%{release}
Requires:         %{fontname}-yehuda-clm-fonts = %{version}-%{release}

%description -n %{fontname}-fonts-compat
%common_desc
Meta-package for installing all font families of Culmus.
This package only exists to help transition culmus-fonts users to the new
package split. It will be removed after one distribution release cycle, please
do not reference it or depend on it in any way.


%files -n %{fontname}-fonts-compat


%package -n %{fontname}-aharoni-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-aharoni-clm-fonts
%common_desc

%_font_pkg -n aharoni-clm -f %{fontconf}-aharoni-clm.conf AharoniCLM-*.afm AharoniCLM-*.pfa

%package -n %{fontname}-caladings-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-caladings-clm-fonts
%common_desc

%_font_pkg -n caladings-clm -f %{fontconf}-caladings-clm.conf CaladingsCLM.afm CaladingsCLM.pfa

%package -n %{fontname}-david-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-david-clm-fonts
%common_desc

%_font_pkg -n david-clm -f %{fontconf}-david-clm.conf DavidCLM-*.ttf DavidCLM-*.afm DavidCLM-*.pfa

%package -n %{fontname}-drugulin-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-drugulin-clm-fonts
%common_desc

%_font_pkg -n drugulin-clm -f %{fontconf}-drugulin-clm.conf DrugulinCLM-*.afm DrugulinCLM-*.pfa

%package -n %{fontname}-ellinia-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-ellinia-clm-fonts
%common_desc

%_font_pkg -n ellinia-clm -f %{fontconf}-ellinia-clm.conf ElliniaCLM-*.afm ElliniaCLM-*.pfa

%package -n %{fontname}-frank-ruehl-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-frank-ruehl-clm-fonts
%common_desc

%_font_pkg -n frank-ruehl-clm -f %{fontconf}-frank-ruehl-clm.conf FrankRuehlCLM-*.afm FrankRuehlCLM-*.pfa

%package -n %{fontname}-miriam-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-miriam-clm-fonts
%common_desc

%_font_pkg -n miriam-clm -f %{fontconf}-miriam-clm.conf MiriamCLM-*.ttf MiriamCLM-*.afm MiriamCLM-*.pfa

%package -n %{fontname}-miriam-mono-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-miriam-mono-clm-fonts
%common_desc

%_font_pkg -n miriam-mono-clm -f %{fontconf}-miriam-mono-clm.conf MiriamMonoCLM-*.afm MiriamMonoCLM-*.pfa

%package -n %{fontname}-nachlieli-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-nachlieli-clm-fonts
%common_desc

%_font_pkg -n nachlieli-clm -f %{fontconf}-nachlieli-clm.conf NachlieliCLM-*.afm NachlieliCLM-*.pfa

%package -n %{fontname}-yehuda-clm-fonts
Summary:        Fonts for Hebrew from Culmus project
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-yehuda-clm-fonts
%common_desc

%_font_pkg -n yehuda-clm YehudaCLM-*.afm YehudaCLM-*.pfa

%prep
%setup -q -n %{fontname}-%{version}
%setup -c -q -a 10
mv %{fontname}-%{version}/* .
mv %{fontname}-type1-%{version}/* .

%build

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}
install -m 0644 -p *.afm %{buildroot}%{_fontdir}
install -m 0644 -p *.pfa %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-aharoni-clm.conf
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-caladings-clm.conf
install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-david-clm.conf
install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-drugulin-clm.conf
install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-ellinia-clm.conf
install -m 0644 -p %{SOURCE6} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-frank-ruehl-clm.conf
install -m 0644 -p %{SOURCE7} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-miriam-clm.conf
install -m 0644 -p %{SOURCE8} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-miriam-mono-clm.conf
install -m 0644 -p %{SOURCE9} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-nachlieli-clm.conf

for fconf in %{fontconf}-aharoni-clm.conf \
             %{fontconf}-caladings-clm.conf \
             %{fontconf}-david-clm.conf \
             %{fontconf}-drugulin-clm.conf \
             %{fontconf}-ellinia-clm.conf \
             %{fontconf}-frank-ruehl-clm.conf \
             %{fontconf}-miriam-clm.conf \
             %{fontconf}-miriam-mono-clm.conf \
             %{fontconf}-nachlieli-clm.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc CHANGES GNU-GPL LICENSE LICENSE-BITSTREAM 

%dir %{_fontdir}


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.104-5m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.104-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.104-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.104-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.104-1m)
- sync with Fedora 13 (0.104-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.102-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.102-7m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.102-6m)
- rebuild against rpm-4.7.0-7m

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.101-5m)
- fix up Obsoletes

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.101-4m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.101-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.101-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.101-1m)
- import from Fedora

* Mon Oct 15 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.101-4.fc8
- License change

* Thu Oct 11 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.101-3.fc8
- Updated according to the review

* Thu Oct 04 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.101-2.fc8
- Using common spec template for font packages

* Thu Oct 04 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.101-1.fc8
- Font directory and package name corrected and updated the version

* Thu Oct 04 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.100-1.fc8
- Split package from fonts-hebrew to reflect upstream project name
