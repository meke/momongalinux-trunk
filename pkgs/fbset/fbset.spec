%global momorel 20

Summary: Tools for managing a frame buffer's video mode properties.
Name: fbset
Version: 2.1
Release: %{momorel}m%{?dist}
License: GPL+
Group: Applications/System
Source: http://home.tvd.be/cr26864/Linux/fbdev/fbset-%{version}.tar.gz
Patch0: fbset-makefile.patch
Patch1: fbset-fhs.patch
Patch2: fbset-2.1-fixmode.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://home.tvd.be/cr26864/Linux/fbdev/

%description
Fbset is a utility for maintaining frame buffer resolutions.  Fbset
can change the video mode properties of a frame buffer device, and is
usually used to change the current video mode.

Install fbset if you need to manage frame buffer resolutions.

%prep
%setup -q
%patch0 -p1 -b .makefile
%patch1 -p1 -b .fhs
%patch2 -p1 -b .fixmode

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/dev
mkdir -p %{buildroot}/usr/sbin
mkdir -p %{buildroot}%{_mandir}/man5
mkdir -p %{buildroot}%{_mandir}/man8
make install PREFIX=%{buildroot} MANDIR=%{_mandir}
%ifarch sparc sparc64
mkdir -p %{buildroot}/etc
cp etc/fb.modes.ATI %{buildroot}/etc/fb.modes
%endif
chmod 644 %{buildroot}%{_mandir}/man[58]/*

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/sbin/*
%{_mandir}/man[58]/*
%ifarch sparc sparc64
%config /etc/fb.modes
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-18m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-16m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-15m)
- revise for rpm46 (s/Patch/Patch0/)
- License: GPL+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-14m)
- rebuild against gcc43

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1-13m)
- rebuild against new environment.

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.1-12k)
- NoSource statement was removed.

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.1-10k)
- modified fbset-fhs.patch and spec file for compatiblity

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.1-4).

* Tue Feb 15 2000 Bill Nottingham <notting@redhat.com>
- ship fb.modes everywhere

* Fri Feb  4 2000 Bill Nottingham <notting@redhat.com>
- fix man page permissions

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix summary

* Sun Nov 28 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Thu Oct  7 1999 Bill Nottingham <notting@redhat.com>
- update to 2.1
- don't include fb devs.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- include fb devs too (#1515)
- update to 19990118 version.

* Thu Nov  5 1998 Jeff Johnson <jbj@redhat.com>
- import from ultrapenguin 1.1.
- upgrade to 19981104.

* Thu Oct 29 1998 Jakub Jelinek <jj@ultra.linux.cz>
- new package
