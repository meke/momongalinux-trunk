# Generated from hoe-3.0.1.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname hoe

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Hoe is a rake/rubygems helper for project Rakefiles
Name: rubygem-%{gemname}
Version: 3.0.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://www.zenspider.com/projects/hoe.html
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.4
Requires: ruby 
Requires: rubygem(rake) => 0.8
Requires: rubygem(rake) < 1
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.4
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Hoe is a rake/rubygems helper for project Rakefiles. It helps you
manage, maintain, and release your project and includes a dynamic
plug-in system allowing for easy extensibility. Hoe ships with
plug-ins for all your usual project tasks including rdoc generation,
testing, packaging, deployment, and announcement..
See class rdoc for help. Hint: `ri Hoe` or any of the plugins listed
below.
For extra goodness, see: http://seattlerb.rubyforge.org/hoe/Hoe.pdf


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/sow
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update 3.0.1

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.5-1m)
- update 2.12.5

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.3-1m) 
- update 2.12.3

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.2-1m) 
- update 2.12.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.2-1m)
- update 2.6.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-1m)
- update 2.6.1

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.0-1m)
- update 2.5.0

* Fri Jan  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- update 2.4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.3-1m)
- update 2.3.3

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.2-1m)
- update 2.3.2

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-1m)
- update 2.3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.0-1m)
- Initial package for Momonga Linux
