%global momorel 1

Summary: The GNU line editor
Name: ed
Version: 1.10
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Text
URL: http://www.gnu.org/software/ed/
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.lz
NoSource: 0
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: lzip

%description
Ed is a line-oriented text editor, used to create, display, and modify
text files (both interactively and via shell scripts).  For most
purposes, ed has been replaced in normal usage by full-screen editors
(emacs and vi, for example).

Ed was the original UNIX editor, and may be used by some programs.  In
general, however, you probably don't need to install it and you probably
won't use it.

%prep
%setup -q

%build
./configure --exec-prefix=/
%make CFLAGS="%{optflags}"

%install
%makeinstall bindir=%{buildroot}/bin

# delete dir file
rm -f %{buildroot}/%{_datadir}/info/dir

%check
make check

%post
/sbin/install-info %{_infodir}/ed.info %{_infodir}/dir

%preun
if [ $1 = 0 ] ; then
  /sbin/install-info --delete %{_infodir}/ed.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
/bin/*
%{_infodir}/ed.info*
%{_mandir}/man1/*.1*

%changelog
* Fri Feb 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10-1m)
- update 1.10

* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-1m)
- update 1.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.6

* Wed Oct 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Mon Aug 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-1m)
- [SECURITY] CVE-2008-3916
- update to 1.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- rebuild against gcc43

* Wed Feb 27 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-1m)
- update to 0.9
- License: GPLv3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-2m)
- %%NoSource -> NoSource

* Sun May 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-1m)
- upsate to 0.5

* Sat Jan 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-2m)
- delete file info/dir

* Sat Jan 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-1m)
- update to 0.4
- SECURITY FIX CVE-2006-6939

* Tue Feb 22 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-30m)
- use autoconf instead of autoconf-old
- delete patch0, override by patch2
- add patch0, patch1, and patch2 from fc-devel

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.2-29m)
- rebuild against gcc-3.2 with autoconf-2.53

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.2-28k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 Toru Hoshina <t@kondara.org>
- (0.2-26k)
- force autoconf-old.

* Wed Dec 13 2000 AYUHANA Tomonori <l@kondara.org>
- (0.2-24k)
- 0.2-25k backport for 2.0

* Thu Dec  7 2000 AYUHANA Tomonori <l@kondara.org>
- (0.2-25k)
- add ed_0.2-18.1.diff.gz

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Aug 14 2000 Akira Higuchi <a@kondara.org>
- use regex in glibc

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Than Ngo <than@redhat.de>
- fix typo

* Sat Jun 17 2000 Than Ngo <than@redhat.de>
- add %%defattr
- clean up specfile

* Sat May 20 2000 Ngo Than <than@redhat.de>
- rebuild for 7.0
- put man pages and infos in right place

* Wed Apr 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, Distribution, description )

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man pages.

* Tue Mar 23 1999 Jeff Johnson <jbj@redhat.com>
- fix %post syntax error (#1689).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 11)

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Fri Dec 18 1998 Preston Brown <pbrown@redhat.com>
- bumped spec number for initial rh 6.0 build

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Oct 17 1997 Donnie Barnes <djb@redhat.com>
- added install-info support
- added BuildRoot
- correct URL in Source line

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
