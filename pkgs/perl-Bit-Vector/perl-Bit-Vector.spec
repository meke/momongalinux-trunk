%global         momorel 4

Name:           perl-Bit-Vector
Version:        7.3
Release:        %{momorel}m%{?dist}
Summary:        Efficient bit vector, set of integers and "big int" math library
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Bit-Vector/
Source0:        http://www.cpan.org/authors/id/S/ST/STBEY/Bit-Vector-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Carp-Clan >= 5.3
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Storable >= 2.21
Requires:       perl-Carp-Clan >= 5.3
Requires:       perl-Storable >= 2.21
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
OVERLOADED OPERATORS

%prep
%setup -q -n Bit-Vector-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Artistic.txt CHANGES.txt CREDITS.txt GNU_GPL.txt GNU_LGPL.txt INSTALL.txt README.txt
%{perl_vendorarch}/auto/Bit/Vector
%{perl_vendorarch}/Bit/Vector
%{perl_vendorarch}/Bit/Vector.pm
%{perl_vendorarch}/Bit/Vector.pod
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3-2m)
- rebuild against perl-5.18.1

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3-1m)
- update to 7.3

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2-1m)
- update to 7.2

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1-1m)
- update to 7.1

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0-1m)
- update to 7.0

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.9-2m)
- rebuild against perl-5.10.1

* Sat Aug 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-1m)
- update to 6.9

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6-1m)
- update to 6.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.4-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.4-5m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (6.4-4m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4-3m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4-2m)
- rebuilt against perl-5.8.7

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.4-1m)
- update to 6.4

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.3-8m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (6.3-7m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (6.3-6m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3-5m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (6.3-4m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (6.3-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (6.3-2m)
- rebuild against perl-5.8.0

* Mon Sep 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (6.3-1m)
- major feature enhancements

* Sun Sep 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (6.2-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (6.1-5m)
- remove BuildRequires: gcc2.95.3

* Tue May 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (6.1-4k)
- fix a typo to avoid a conflict of Carp::Clan.3*

* Sun May 12 2002 Toru Hoshina <t@Kondara.org>
- (6.1-2k)
- version up.
