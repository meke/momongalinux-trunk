%global momorel 1

%global xfce4ver 4.10.0
%global major 0.5

Name: 		xfce4-xmms-plugin
Version: 	0.5.3
Release:	%{momorel}m%{?dist}
Summary:	XMMS plugin for the Xfce panel


Group: 		User Interface/Desktops
License:  	BSD
URL:		http://goodies.xfce.org/
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel libxml2-devel pkgconfig
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  xmms-devel
Requires: 	xmms xfce4-panel >= %{xfce4ver}

%description 
Control XMMS from the Xfce panel.

%prep 
%setup -q 

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/xfce4/xfce4-xmms-plugin
%{_datadir}/locale/*/*/*


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.3-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-11m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-10m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-7m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-6m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.2-3m)
- add __libtoolize

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-1m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-2m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.1-1m)
- update to 0.3.1

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-11m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-10m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-9m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.1-8m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-7m)
- rebuild against xfce4 4.1.90

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.1-6m)
- rebuild against xfce4-4.0.6

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.1-5m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.1.1-4m)
- rebuild against for libxml2-2.6.8

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (0.1.1-3m)
- revised spec for rpm 4.2.

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-2m)
- rebuild against xfce4 4.0.3

* Wed Dec 10 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.1.1-1m)
- first import
