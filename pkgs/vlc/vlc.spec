%global momorel 2
%global srcname %{name}-%{version}
%global moz_plugindir %{_libdir}/mozilla/plugins
%global geckover 1.9.2

Summary: VLC media player
Name: vlc
Version: 2.1.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.videolan.org/
Source0: http://download.videolan.org/pub/videolan/%{name}/%{version}/%{srcname}.tar.xz
# Source0: ftp://ftp.videolan.org/pub/videolan/testing/%{srcname}/%{srcname}.tar.xz
# See http://nightlies.videolan.org/build/source/
NoSource: 0
# Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-2.0.0-nn-desktop.patch
Patch2: vlc-1.1.12-xulrunner7.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: a52dec-devel
BuildRequires: alsa-lib-devel
BuildRequires: autoconf
BuildRequires: automake17
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: faad2-devel >= 2.7
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: fluidsynth-devel >= 1.1.5
BuildRequires: fribidi-devel
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: libdvbpsi-devel >= 0.2.0
BuildRequires: libdvdnav-devel >= 4.1.2
BuildRequires: libdvdread-devel >= 4.1.2
BuildRequires: libid3tag-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libkate-devel >= 0.3.7-3m
BuildRequires: libmad
BuildRequires: libmodplug-devel >= 0.8.8.4
BuildRequires: libmpcdec-devel >= 1.2.6
BuildRequires: libogg-devel 
BuildRequires: libpostproc >= 0.10.3
BuildRequires: libproxy-devel >= 0.4.4
BuildRequires: libtool
BuildRequires: libva-devel >= 1.0.0
BuildRequires: libvorbis-devel
BuildRequires: lua-devel
BuildRequires: mpeg2dec-devel
BuildRequires: mplayer
BuildRequires: ncurses-devel
# BuildRequires: perl
BuildRequires: pulseaudio-libs-devel
BuildRequires: qt-devel >= 4.7.0
BuildRequires: sed
BuildRequires: speex-devel
BuildRequires: x264-devel >= 0.0.2377
BuildRequires: xulrunner-devel >= %{geckover}
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: xcb-util-keysyms-devel
BuildRequires: libupnp-devel >= 1.6.13
BuildRequires: libmtp-devel >= 1.1.0
BuildRequires: live-devel >= 0.20120126
BuildRequires: mesa-libEGL-devel
BuildRequires: systemd-devel >= 187

%description
VideoLAN is a software project, which produces free software for
video, released under the GNU General Public License.

VLC media player is a free cross-platform media player. It supports a
large number of multimedia formats, without the need for additional
codecs. It can also be used as a streaming server, with extended
features (video on demand, on the fly transcoding, ...)

%package plugin
Summary: VLC Media Player plugin for Mozilla compatible web browsers
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}

%description plugin
This package contains a VLC Media Player plugin for Mozilla compatible
web browsers.

%package devel
Summary: header and static libraries for VideoLAN
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
header and static libraries for Video LAN Client

%prep
%setup -q -n %{srcname}

#%%patch0 -p1 -b .desktop-ja
# %patch1 -p1 -b .nn-desktop
#%%patch2 -p1 -b .xulrunner7~

## fix PLUGIN_PATH path for lib64
# %%{__perl} -pi -e 's|/lib/%%{name}|/%%{_lib}/%%{name}|g' %%{name}-config.in.in configure*
# fix perms issues
# chmod 644 mozilla/control/*
# chmod 644 src/control/log.c
# sed -i 's/\r//'  projects/mozilla/control/*

# ./bootstrap

%build
export CFLAGS="%{optflags} -fPIC `pkg-config libpostproc --cflags` -I%{_includedir}/nspr4/ -I%{_libdir}/live/liveMedia/include/ "
export CXXFLAGS="%{optflags} -fPIC `pkg-config libpostproc --cflags` -I%{_includedir}/nspr4/ -I%{_libdir}/live/liveMedia/include/ "

# altivec compiler flags aren't set properly (0.8.2)
%ifarch ppc ppc64
export CFLAGS="$CFLAGS -maltivec -mabi=altivec"
%endif

%configure \
%ifarch %{ix86}
    --enable-sse \
%endif
    --enable-pulse \
    --enable-alsa \
    --enable-dvdread \
    --enable-dvdnav \
    --enable-v4l \
    --enable-faad \
    --enable-toolame \
    --enable-aa \
    --enable-ncurses \
    --enable-xosd \
    --enable-goom \
    --enable-mozilla \
    --enable-ffmpeg \
    --enable-live555 \
    --disable-pda \
    --disable-goom \
    --enable-libva \
    --enable-libmpeg2

# disabled live555 vlc unsupported current live
#    --enable-live555 \

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall transform='s,x,x,' soliddatadir=%{buildroot}%{_kde4_appsdir}/solid/actions

# install man files
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 doc/*.1 %{buildroot}%{_mandir}/man1

# fix permission
#chmod 755 %{buildroot}%{moz_plugindir}/lib%{name}plugin.so

# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Video \
  --add-category Audio \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# clean up
rm -rf %{buildroot}%{_docdir}/%{name}

rm -rf %{buildroot}/%{_libdir}/*.la
#rm -rf %{buildroot}/%{_libdir}/mozilla/plugins/*.la

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS
%doc doc/*.txt doc/ChangeLog-* doc/skins doc/lirc
%{_bindir}/c%{name}
%{_bindir}/n%{name}
%{_bindir}/q%{name}
%{_bindir}/r%{name}
%{_bindir}/s%{name}
%{_bindir}/%{name}
%{_bindir}/%{name}-wrapper
%{_libdir}/lib%{name}*.so.*
%{_libdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_kde4_appsdir}/solid/actions/%{name}-open*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*.png
%{_datadir}/icons/hicolor/*/apps/%{name}*.xpm
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}-wrapper.1*
%{_datadir}/pixmaps/%{name}.png

%files plugin
%defattr(-,root,root)
#%{moz_plugindir}/lib%{name}plugin.so

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/*%{name}*.pc
%{_libdir}/lib%{name}*.so
#%{_mandir}/man1/%{name}-config.1*

%changelog
* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-2m)
- rebuild against ffmpeg

* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Sun Oct 13 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-2m)
- rebuild against gnutls-3.2.0

* Tue Apr 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-1m)
- [SECURITY] http://www.h-online.com/security/news/item/VLC-2-0-6-fixes-hole-from-January-1840848.html
- update to 2.0.6

* Wed Dec 13 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- [SECURITY] CVE-2012-5470
- update 2.0.4

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.3-2m)
- rebuild against systemd-187

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update 2.0.3

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-1m)
- [SECURITY] CVE-2012-3377
- update 2.0.2

* Wed Jun 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-5m)
- just increase release to enable upgrade

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-4m)
- rebuild against x264-0.0.2200
 
* Thu May 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-3m)
- rebuild against ffmpeg-0.10.3

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-2m)
- rebuild against fluidsynth-1.1.5

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- [SECURITY] CVE-2012-1775 CVE-2012-1776
- update 2.0.1

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.991.2m)
- rebuild against libbbmodplug-0.8.8.4

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.0-0.991.1m)
- rename vlc-1.2 to vlc-2.0
- update 2.0.0-rc1
-- support live555

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-0.4.1m)
- update 1.2.0-pre4

* Wed Dec 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-0.1.20111228.1m)
- update 1.2.0-pre3 20111228

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-0.1.20111218.1m)
- update 1.2.0-pre3 20111218

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-0.1.1m)
- update 1.2.0-pre1

* Mon Dec 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.12-3m)
- fix build failure; add patch for xulrunner-7.0.1

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.12-2m)
- rebuild against x264-2106

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.12-1m)
- [SECURITY] CVE-2011-3333
- update to 1.1.12

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-6m)
- rebuild against libmtp

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-5m)
- rebuild against libva

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.11-4m)
- rebuild against fluidsynth-1.1.4

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-3m)
- rebuild against x264 and ffmpeg

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-2m)
- rebuild against libupnp

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.11-1m)
- [SECURITY] CVE-2011-2587 CVE-2011-2588
- see following pages for more information about SECURITY
- http://www.videolan.org/security/sa1105.html
- http://www.videolan.org/security/sa1106.html
- update to 1.1.11

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.10-3m)
- rebuild against ffmpeg-0.7.0-0.20110705

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.10-2m)
- fix build failure; add BuildRequires

* Tue Jun  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.10-1m)
- update to 1.1.10

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.9-4m)
- rebuild against x264 and ffmpeg

* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.9-3m)
- rebuild against libdvdpsi-0.2.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.9-2m)
- rebuild against libnotify-0.7.2

* Wed Apr 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.9-1m)
- [SECURITY] VideoLAN-SA-1103
- update to 1.1.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-3m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.8-2m)
- rebuild against libva-0.32

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.8-1m)
- [SECURITY] CVE-2010-3275 CVE-2010-3276
- update to 1.1.8

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.7-2m)
- rebuild against x264

* Tue Feb  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.7-1m)
- [SECURITY] VideoLAN-SA-1102
- update to 1.1.7

* Wed Jan 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.6-1m)
- [SECURITY] CVE-2010-3907 (VideoLAN-SA-1007)
- update to 1.1.6

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-5m)
- rebuild against x264

* Fri Dec 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-4m)
- rebuild against x264

* Thu Dec 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-3m)
- adapt vlc-open*.desktop to KDE 4.5.85

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-1m)
- update 1.1.5
- [SECURITY] VideoLAN-SA-1006

* Fri Oct 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4.1-2m)
- rebuild against x264

* Wed Sep 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4.1-1m)
- update to 1.1.4.1
- [SECURITY] CVE-2010-2937

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-2m)
- full rebuild for mo7 release

* Tue Aug 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-2m)
- [SECURITY] CVE-2010-2937
- import the upstream patch

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-2m)
- build with libkate

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-6m)
- rebuild against x264

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-5m)
- rebuild against libproxy-0.4.4

* Mon Jul 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-4m)
- correct symlink to icon

* Sun Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-3m)
- rebuild against libva-0.31.1-1m

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-2m)
- rebuild against qt-4.6.3-1m

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update vlc-1.1.0 release

* Fri Jun 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.999.7m)
- update desktop.patch

* Fri Jun 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.999.6m)
- update vlc-1.1.0-rc4

- * Sun Jun 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- - (1.1.0-0.999.5m)
- - update vlc-1.1.0-rc3

* Fri Jun 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.999.5m)
- rebuild against x264-0.0.1649

* Fri Jun 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.999.4m)
- fix up broken link of icon
- add desktop.patch
- but vlc is still crashing on i686

* Wed Jun  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-0.999.3m)
- update 1.1.0-rc2

* Wed May 26 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-0.999.2m)
- fix version number

* Wed May 26 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-0.999.1m)
- update 1.1.0-rc

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-3m)
- rebuild against x264

* Wed Apr 28 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-2m)
- rebuild against libdvbpsi-0.1.7

* Sun Apr 25 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-1m)
- update 1.0.6
- [SECURITY] http://www.videolan.org/security/sa1003.html
- [SECURITY] CVE-2010-1441 CVE-2010-1442 CVE-2010-1443 CVE-2010-1444 CVE-2010-1445

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-6m)
- rebuild against x264-0.0.1523

* Sat Apr 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-5m)
- BR fribidi-devel, ffmpeg-devel, ncurses-devel

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-4m)
- rebuild against libjpeg-8a

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-3m)
- rebuild against x264-0.0.1471

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-2m)
- rebuild against x264-0.0.1406

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5
-- apply xulrunner192 patch from RPM Fusion (1.0.5-1)

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-3m)
- rebuild against x264-0.0.1376

* Tue Nov 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-2m)
- rebuild against x264-0.0.1332

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sun Oct 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-2m)
- rebuild against x264-0.0.1281

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- [SECURITY] http://www.videolan.org/security/sa0901.html
- update to 1.0.2

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-4m)
- rebuild against x264-0.0.1259-0.20090919

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-3m)
- rebuild against libjpeg-7

* Mon Aug 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-2m)
- rebuild against x264-0.0.1222-0.20090823.1m

* Tue Aug  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- rebuild against x264-0.0.1195-0.20090730.1m

* Sat Aug  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- [SECURITY] CVE-2010-2062
- update to 1.0.1

* Mon Jul 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- enable sse2 on i686
-- now we unsupport x86 processors before pentium 4

* Wed Jul  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.4.4m)
- no src

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-0.4.3m)
- rebuild against ffmpeg-0.5.1-0.20090622

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.4.2m)
- build with --disable-sse on i686

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.4.1m)
- [SECURITY] CVE-2009-1045
- update to 1.0.0-rc4
- rebuild against xulrunner-1.9.1

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.9a-2m)
- rebuild against faad2-2.7

* Tue Apr  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9a-1m)
- update to 0.9.9a

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8a-3m)
- rebuild against x264

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8a-2m)
- rebuild against x264

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8a-2m)
- rebuild against rpm-4.6

* Sat Dec  6 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8a-1m)
- [SECURITY] CVE-2008-5276
- [SECURITY] http://www.videolan.org/security/sa0811.html
- update to 0.9.8a

* Fri Nov  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-1m)
- [SECURITY] CVE-2008-5032 CVE-2008-5036
- [SECURITY] http://www.videolan.org/security/sa0810.html
- update to 0.9.6
- do not ./bootstrap
-- need not replace /lib/vlc

* Fri Nov  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-3m)
- current vlc do not require wxGTK, do require Qt4

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5-2m)
- rebuild against x264 & ffmpeg

* Sun Oct 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1m)
- [SECURITY] CVE-2008-4654 CVE-2008-4686
- [SECURITY] http://www.videolan.org/security/sa0809.html
- update to 0.9.5

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- update 0.9.4

* Sun Oct  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3-1m)
- [SECURITY] CVE-2008-4558
- update 0.9.3

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2-1m)
- update 0.9.2

* Mon Sep  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1-1m)
- update 0.9.1

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6h-3m)
- change BR from libid3tag to libid3tag-devel

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6h-2m)
- rebuild against gnutls-2.4.1

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6h-1m)
- update 0.8.6h

* Thu Jul  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6f-7m)
- [SECURITY] CVE-2008-2430 apply upstream fix

* Mon Jun 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6f-6m)
- update for xulrunner-1.9

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6f-5m)
- rebuild against libmpcdec-1.2.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6f-4m)
- rebuild against libdvdread-4.1.2

* Sat May 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6f-3m)
- build with xulrunner

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6f-2m)
- rebuild against x264

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6f-1m)
- update to 0.8.6f
- rebuild against firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6e-3m)
- rebuild against gcc43

* Wed Mar 26 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6e-2m)
- fix compilation issue with ffmpeg

* Fri Feb 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6e-1m)
- update 0.8.6e

* Sun Feb 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6d-4m)
- buildReq faad2-devel

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6d-3m)
- rebuild against x264-0.0.712-0.20071219

* Wed Dec 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6d-2m)
- rebuild against ffmpeg-0.4.9-0.20071219

* Sun Dec  2 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6d-1m)
- update to 0.8.6d
- License: GPLv2

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6c-5m)
- rebuild against wxGTK-2.8.6

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6c-4m)
- rebuild against libvorbis-1.2.0-1m

* Sun Jul  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6c-3m)
- add %%{_datadir}/pixmaps/vlc.png

* Sun Jul  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6c-2m)
- add a package vlc-plugin based on livna
- see http://pc11.2ch.net/test/read.cgi/linux/1092539027/931, I love 2ch ;)

* Tue Jun 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6c-1m)
- update 0.8.6c

* Wed Apr 25 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.8.6b-1m)
- import to Momonga

* Tue Feb 27 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.8.6a-1m)
- update to 0.8.6a

* Fri May 12 2006 Masayuki SANO <sbfiled_snmsyk@yahoo.co.jp>
- (0.8.5-0.0.1m)
- update to 0.8.5

* Thu Jul 07 2005 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (0.8.2-0.0.1m)
- update to 0.8.2

* Tue Nov 09 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (0.8.0-0.0.1m)
- update to 0.8.0
- now wxWidgets frontend is default and qt,gtk,gnome,kde frontend are removed in upstream
- ffmpeg supprot is disabled because it requires latest preversion of ffmpeg

* Thu Jan  8 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (0.7.0-0.0.2m)
- add kde, qt, gnome front-end

* Thu Jan  8 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (0.7.0-0.0.1m)
- build for Momonga
- --disable-libmpeg2 since it needs libmpeg2 (http://libmpeg2.sf.net/)
