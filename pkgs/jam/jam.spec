%global momorel 9

Name:		jam
Version:	2.5
Release: %{momorel}m%{?dist}
License:	"Distributable"
Group:		Development/Tools
Summary:	Program construction tool, similar to make
URL:		http://public.perforce.com/public/jam/index.html
Source0: ftp://ftp.perforce.com/pub/jam/%{name}-%{version}.tar 
NoSource: 0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	byacc

%description
Jam is a program construction tool, like make. Jam recursively builds target 
files from source files, using dependency information and updating actions 
expressed in the Jambase file, which is written in jam's own interpreted 
language. The default Jambase is compiled into jam and provides a boilerplate 
for common use, relying on a user-provide file "Jamfile" to enumerate actual 
targets and sources. 

%prep
%setup -q

%build

# gcc-4.2 was no used "-Ox" flags ...
# Fix Me 
if [ "`gcc -dumpversion | cut -c 0-3`.x" == "4.2.x" ]; then
export RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed s/\-O[0-9]//g`
fi

make CFLAGS="$RPM_OPT_FLAGS" CCFLAGS="$RPM_OPT_FLAGS" %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m0755 bin.linux*/jam %{buildroot}%{_bindir}
install -m0755 bin.linux*/mkjambase %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,0755)
%doc README RELNOTES *.html
%{_bindir}/jam
%{_bindir}/mkjambase

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-7m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-3m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-2m)
- gcc-4.2 was no used "-Ox" flags ...

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5-1m)
- import to Momonga Linux 3

* Mon Sep 11 2006 Tom "spot" Callaway <tcallawa@redhat.com> 2.5-4
- fix minimal BR from bison to byacc
- rebuild for FC-6

* Tue Feb 28 2006 Tom "spot" Callaway <tcallawa@redhat.com> 2.5-3
- bump for FC5

* Fri Sep  9 2005 Tom "spot" Callaway <tcallawa@redhat.com> 2.5-2
- use smp_mflags
- use name and version in source field

* Fri Aug 19 2005 Tom "spot" Callaway <tcallawa@redhat.com> 2.5-1
- initial package for Fedora Extras
