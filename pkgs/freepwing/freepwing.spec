%global momorel 18

Summary: Free JIS X 4081 Formatter
Name: freepwing
Version: 1.6.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
URL: http://www.sra.co.jp/people/m-kasahr/freepwing/
Source0: ftp://ftp.sra.co.jp/pub/misc/freepwing/freepwing-%{version}.tar.bz2
NoSource: 0
Buildarch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: perl >= 5.8.2, epwutil >= 1.1-10k

%description
FreePWING is a software for making JIS X 4081 format dictionary data.

%prep
%setup -q

%build
%configure \
    --with-pkgdocdir=%{_docdir}/%{name}-%{version} \
    --with-perllibdir=%{perl_vendorlib}
make

%install
rm -rf %{buildroot}
%makeinstall \
    perllibdir=%{buildroot}%{perl_vendorlib} \
    pkgdocdir=%{buildroot}%{_docdir}/%{name}-%{version} 

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%doc README NEWS AUTHORS COPYING ChangeLog doc/*.html doc/*.css catdump/*.txt
%{_bindir}/fpwmake
%{perl_vendorlib}/FreePWING
%{_libexecdir}/freepwing
%{_datadir}/freepwing

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-18m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-17m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-16m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-11m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-3m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- rebuild against perl-5.12.0

* Thu Apr  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.4.4-8m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-6m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.4-5m)
- rebuild against perl-5.10.0-1m

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.4-4m)
- rebuild against perl-5.8.8

* Mon Jun 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.4-3m)
- rebuild against perl-5.8.7

* Sun Feb 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.4-2m)
- apply 'freepwing-1.4.3-sound-050213.patch'

* Mon Jan 10 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.4-1m)
- version up

* Sat Aug 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-7m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.4.3-6m)
- remove Epoch from BuildPrereq

* Mon Mar 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-5m)
- rebuild against perl-5.8.2-5m

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-4m)
- rebuild against perl-5.8.2

* Sat Nov  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-3m)
- rebuild against perl-5.8.1

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-2m)
- rebuild against perl-5.8.0

* Sat Sep 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-1m)

* Tue Apr 23 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.4.2-2k)
- NoSource again
- add URL

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.1-4k)
- NoNoSource..

* Wed Jan 30 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.4.1-2k)

* Sun Sep 16 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.4-2k)

* Sat Jul 07 2001 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.3.1-6k)
- remove freepwing-1.3.1-fpw.patch.

* Tue May 29 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.1-4k)
- catdump is renamed, fpwcatdump is provided.

* Tue Nov 21 2000 Kazuhiko <kazuhiko@kondara.org>
- (1.3.1-1k)

* Thu Jun 30 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2-3k)
- fix stupid mistabe (sitearch >sitelib)

* Thu Jun 30 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2-2k)
- install path of perl library was changed.

* Thu Jun 29 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2-1k)
- added Buildarch

* Sun Apr 02 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- 1st release
