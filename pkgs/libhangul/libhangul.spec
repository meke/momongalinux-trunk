%global momorel 1

Summary: Hangul input library
Name: libhangul
Version: 0.1.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://kldp.net/projects/hangul/
Group: System Environment/Libraries
Source0: http://libhangul.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig

%description
libhangul provides common features for Hangul input method programs.

%package devel
Summary: Header files and static libraries from libhangul
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libhangul.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/hangul
%{_libdir}/libhangul.so.*
%{_datadir}/libhangul/hanja/hanja.txt
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-,root,root)
%{_includedir}/hangul-1.0
%{_libdir}/pkgconfig/libhangul.pc
%{_libdir}/libhangul.a
%{_libdir}/libhangul.so

%changelog
* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- update to 0.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.12-2m)
- rebuild for new GCC 4.6

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.12-1m)
- update to 0.0.12

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.10-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.10-1m)
- version 0.0.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.9-1m)
- version 0.0.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.6-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.6-2m)
- %%NoSource -> NoSource

* Tue Jan  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.6-1m)
- version 0.0.6

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.4-1m)
- initial package for scim-hangul-0.3.1
