%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global phononver 4.7.1

Summary: Audiocd kio slave
Name: audiocd-kio
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.kde.org/areas/multimedia/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{version}
Requires: kde-workspace >= %{version}
Requires: kde-runtime >= %{version}
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: cdparanoia
BuildRequires: cdparanoia-devel
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: flac-devel
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kde-workspace-devel >= %{version}
BuildRequires: libkcddb-devel >= %{version}
BuildRequires: libkcompactdisc-devel >= %{version}
BuildRequires: libtheora-devel
BuildRequires: libvorbis-devel
BuildRequires: phonon-devel >= %{phononver}
BuildRequires: pulseaudio-libs-devel

Obsoletes: kdemultimedia-kio_audiocd < 4.8.97
Provides:  kio_audiocd = %{version}-%{release}

Conflicts: kdemultimedia < 4.8.97

%description
%{summary}.

%package libs
Summary: Runtime libraries for %{name}
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{version}
Requires: libkcddb >= %{version}
Requires: libkcompactdisc >= %{version}
Conflicts: kdemultimedia-libs < 4.8.97

%description libs
%{summary}.

%package devel
Summary:  Development files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: kdelibs-devel
Conflicts: kdemultimedia-devel < 4.8.97

%description devel
%{summary}.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang %{name} --with-kde --all-name

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING COPYING.DOC
%{_kde4_appsdir}/konqsidebartng/virtual_folders/services/audiocd.desktop
%{_kde4_appsdir}/solid/actions/solid_audiocd.desktop
%{_kde4_datadir}/kde4/services/audiocd.desktop
%{_kde4_datadir}/kde4/services/audiocd.protocol
%{_kde4_datadir}/config.kcfg/audiocd*.kcfg
%{_kde4_libdir}/kde4/kcm_audiocd.so
%{_kde4_libdir}/kde4/kio_audiocd.so

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libaudiocdplugins.so.4*
%{_kde4_libdir}/kde4/libaudiocd_encoder_flac.so
%{_kde4_libdir}/kde4/libaudiocd_encoder_lame.so
%{_kde4_libdir}/kde4/libaudiocd_encoder_vorbis.so
%{_kde4_libdir}/kde4/libaudiocd_encoder_wav.so

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/audiocdencoder.h
%{_kde4_libdir}/libaudiocdplugins.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- iinitial build (KDE 4.9 RC2)
