%global momorel 5

Name: xhtml2fo-style-xsl
Version: 20051222
Release: %{momorel}m%{?dist}
Group: Applications/Text

Summary: Antenna House, Inc. XHTML to XSL:FO stylesheets
License: see "Copyright"
URL: http://www.antennahouse.com/XSLsample/XSLsample.htm

Requires(pre): xhtml1-dtds
Requires(pre): xml-common >= 0.6.3
#Requires(post): libxml2
#Requires(postun): libxml2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
Source0: http://www.antennahouse.com/XSLsample/sample-xsl-xhtml2fo.zip
Source1: AntennaHouse-COPYRIGHT

%description
These XSL stylesheets allow you to transform any XHTML document to FO.
With a XSL:FO processor you could create PDF versions of XHTML documents.


%prep
%setup -q -c -n %{name}-%{version} -T -b 0
%__cp %{SOURCE1} .
%build


%install
rm -rf %{buildroot}

mkdir -p %{buildroot}
DESTDIR=%{buildroot}%{_datadir}/sgml/xhtml1/xhtml2fo-stylesheets

mkdir -p $DESTDIR
cp *xsl $DESTDIR/

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc AntennaHouse-COPYRIGHT
%{_datadir}/sgml/xhtml1/xhtml2fo-stylesheets


%post
CATALOG=%{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" \
 "http://www.antennahouse.com/XSLsample/sample-xsl-xhtml2fo/xhtml2fo.xsl" \
 "file:///usr/share/sgml/xhtml1/xhtml2fo-stylesheets/xhtml2fo.xsl" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteURI" \
 "http://www.antennahouse.com/XSLsample/sample-xsl-xhtml2fo/xhtml2fo.xsl" \
 "file:///usr/share/sgml/xhtml1/xhtml2fo-stylesheets/xhtml2fo.xsl" $CATALOG

%postun
# remove entries only on removal of package
if [ "$1" = 0 ]; then
  CATALOG=%{_sysconfdir}/xml/catalog
  %{_bindir}/xmlcatalog --noout --del \
  "file://%{_datadir}/sgml/xhtml1/xhtml2fo-stylesheets/xhtml2fo.xsl" $CATALOG
fi


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20051222-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20051222-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20051222-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20051222-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (20051222-1m)
- Initial commit Momonga Linux

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20051222-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild


* Sun Oct 12 2008 Ismael Olea <ismael@olea.org> 20051222-2
- adding the %{?dist} macro to the spec

* Thu Jan 17 2008 Ismael Olea <ismael@olea.org> 20051222-1
- updating to last version of sample-xsl-xhtml2fo.zip
- fixing spec for contributing to Fedora and rpmlinting with 0.82																																		

* Mon Jan 10 2005 Ismael Olea <ismael@olea.org> 20050106-1
- First version (based on docbook-xsl-stylesheets.spec by Tim Waugh.

