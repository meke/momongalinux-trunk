%global momorel 4

Name: gok
Summary: GNOME On-screen Keyboard (GOK)
Version: 2.30.1
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.30/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch1: gok-0.10.2-launcher.patch

URL: 		http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: at-spi-devel >= 1.26.0
BuildRequires: libbonobo-devel >= 2.24.1
BuildRequires: atk-devel >= 1.26.0
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: libwnck-devel >= 2.26.0
BuildRequires: esound-devel
BuildRequires: libusb-devel >= 0.1.12
BuildRequires: ORBit2-devel >= 2.14.17
BuildRequires: gnome-speech-devel >= 0.4.25
BuildRequires: dbus-glib-devel
Requires(pre,preun): GConf2
Requires(pre,preun): rarian

%description
Welcome to GOK.  Please note that this is an unstable release and that
GOK may crash or behave unexpectedly.  With this in mind we would like
to thank you very much for your interest in GOK and hope very much
that you find it useful.

%package devel
Summary: Libraries/include files for GStreamer plugins.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: at-spi-devel
Requires: libbonobo-devel
Requires: atk-devel
Requires: gtk2-devel
Requires: libwnck-devel
Requires: esound-devel

%description devel
The gok project aims to enable users to control their computer without
having to rely on a standard keyboard or mouse, leveraging GNOME's
built-in accessibility framework.
#'

This package contains the libraries and includes files necessary to develop
applications and plugins for gok.

%prep
%setup -q
%patch1 -p1 -b .launcher~

%build
gtkdocize --copy
autoreconf -vfi

%configure --disable-schemas-install \
    --enable-gtk-doc \
    --enable-libusb-input \
    --enable-xevie
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache --quiet /usr/share/icons/hicolor
fi
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/gok.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gok.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gok.schemas \
	> /dev/null || :
fi

%postun
rarian-sk-update

%files
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/gok.schemas
%{_bindir}/gok
%{_bindir}/create-branching-keyboard
%{_datadir}/gok
%{_datadir}/omf/gok
%{_datadir}/gnome/help/gok
%{_datadir}/locale/*/*/*.mo
%{_datadir}/applications/gok.desktop
%{_datadir}/pixmaps/gok.png
%{_datadir}/icons/hicolor/48x48/apps/gok.png
%{_libdir}/bonobo/servers/*.server
%{_datadir}/sounds/freedesktop/stereo/goksound1.wav
%{_datadir}/sounds/freedesktop/stereo/goksound2.wav

%files devel
%defattr (-, root, root)
%{_libdir}/pkgconfig/gok-1.0.pc
%doc %{_datadir}/gtk-doc/html/gok

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-4m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-2m)
- full rebuild for mo7 release

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.2-1m)
- update to 2.29.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-5m)
- delete __libtoolize hack

* Sat Dec 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.28.1-4m)
- add BuildPrereq: dbus-glib-devel

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.7-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.7-2m)
- %%NoSource -> NoSource

* Mon Oct 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7

* Sat Oct 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.6-1m)
- update to 1.3.6

* Sat Oct 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Sat May 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sat Jan 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-2m)
- remove category Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-2m)
- rebuild against GNOME 2.16

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.0-2m)
- rebuild against expat-2.0.0-1m

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10

* Thu May 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-3m)
- add patch3

* Wed May 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-2m)
- add Patch2 from FC (gok-1.0.5-crash.patch)

* Thu Apr 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8
- remove Patch2 (gok-1.0.7-cspi_accessible_is_a.patch)
- BUG FIX Release

* Sun Apr  9 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.7-2m)
- add Patch2 (gok-1.0.7-cspi_accessible_is_a.patch)

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Mon Nov 21 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.5-1m)
- version 1.0.5
- GNOME 2.12.1 Desktop
- add Patch1 (gok-0.10.2-launcher.patch)

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.11.16-1m)
- version 0.11.16
- GNOME 2.8 Desktop

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.2-2m)
- add Utility to Categories of desktop file for KDE
- BuildPrereq: desktop-file-utils

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.10.2-1m)
- version 0.10.2

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.10.0-1m)
- version 0.10.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.8.4-2m)
- revised spec for enabling rpm 4.2.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.4-1m)
- version 0.8.4

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.3-1m)
- version 0.8.3

* Tue Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.8.1-1m)
- version 0.8.1

* Tue Aug 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.13-1m)
- version 0.7.13

* Fri Jul 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.11-1m)
- version 0.7.11

* Wed Jul 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.10-1m)
- version 0.7.10

* Tue Jul 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.9-1m)
- version 0.7.9

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.7-1m)
- version 0.7.7

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.5-1m)
- version 0.7.5

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.1-1m)
- version 0.7.1
