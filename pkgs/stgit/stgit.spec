%global momorel 6

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           stgit
Version:        0.14.3
Release:        %{momorel}m%{?dist}
Summary:        StGIT provides similar functionality to Quilt on top of GIT

Group:          Development/Tools
License:        GPLv2
URL:            http://www.procode.org/stgit
Source0:        http://homepage.ntlworld.com/cmarinas/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
BuildRequires:  git

Requires:       git

%description
StGIT is a Python application providing similar functionality to Quilt
(i.e. pushing/popping patches to/from a stack) on top of GIT. These
operations are performed using GIT commands and the patches are stored
as GIT commit objects, allowing easy merging of the StGIT patches into
other repositories using standard GIT functionality.

Note that StGIT is not an SCM interface on top of GIT and it expects a
previously initialised GIT repository (unless it is cloned using StGIT
directly). For standard SCM operations, either use plain GIT commands
or the Cogito tool but it is not recommended to mix them with the
StGIT commands.


%prep
%setup -q


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
chmod -x $RPM_BUILD_ROOT%{_datadir}/stgit/contrib/stgbashprompt.sh
 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README

%{_bindir}/stg

# For noarch packages: sitelib
%{python_sitelib}/*
%{_datadir}/stgit


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.3-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.3-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.2-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.2-2m)
- rebuild against python-2.6.1-1m

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.2-1m)
- import from Fedora to Momonga

* Wed Mar 26 2008 James Bowes <jbowes@redhat.com> 0.14.2-1
- Update to 0.14.2

* Wed Dec 12 2007 James Bowes <jbowes@redhat.com> - 0.14.1-1
- Update to 0.14.1

* Thu Aug 23 2007 James Bowes <jbowes@redhat.com> - 0.13-2
- Mark license as GPLv2+

* Sun Aug 05 2007 James Bowes <jbowes@redhat.com> - 0.13-1
- Update to 0.13

* Wed Apr 25 2007 James Bowes <jbowes@redhat.com> - 0.12.1-2
- Use macro for datadir.

* Thu Apr 19 2007 James Bowes <jbowes@redhat.com> - 0.12.1-1
- Update version.
- Don't install the bash prompt shell script as executable.

* Fri Feb 02 2007 James Bowes <jbowes@redhat.com> - 0.12-1
- Initial packaging.
