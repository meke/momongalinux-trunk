%global momorel 2

%define ver 1.2.12

Summary: Simple DirectMedia Layer - Sample Image Loading Library
Name: SDL_image
Version: %{ver}
Release: %{momorel}m%{?dist}
Source0: http://www.libsdl.org/projects/%{name}/release/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://www.libsdl.org/projects/SDL_image/
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf >= 2.52-8k, automake >= 1.5-8k
BuildRequires: SDL-devel >= 1.2.15
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: libtiff-devel >= 4.0.1


%description
This is a simple library to load images of various formats as SDL surfaces.
This library supports BMP, PPM, PCX, GIF, JPEG, PNG, and TIFF formats.

%package devel
Summary: Libraries, includes and more to develop SDL applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: SDL-devel

%description devel
This is a simple library to load images of various formats as SDL surfaces.
This library supports BMP, PPM, PCX, GIF, JPEG, PNG, and TIFF formats.

%prep
%setup -q

%build

%configure \
	--disable-dependency-tracking \
	--enable-tif				\
	--disable-jpg-shared			\
	--disable-png-shared			\
	--disable-tif-shared			\
	--disable-static

%make

%install
rm -rf %{buildroot}
%makeinstall
mkdir -p %{buildroot}%{_bindir}
./libtool --mode=install /usr/bin/install showimage $RPM_BUILD_ROOT%{_bindir}

find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README CHANGES COPYING
%{_bindir}/showimage
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/SDL/*

%changelog
* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.12-2m)
- rebuild against libtiff-4.0.1

* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.12-1m)
- update to 1.2.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-3m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-2m)
- rebuild against libjpeg-8a

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.10-1m)
- update to 1.2.10
- remove unused patches

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-3m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.5-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-4m)
- %%NoSource -> NoSource

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-2m)
- delete libtool library

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.4-1m)
- version up

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-3m)
- build against SDL-1.2.7-4m

* Tue Sep  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.3-2m)
- rebuild against SDL-1.2.6
- don't use %%{version} in Version:
- use %%NoSource macro

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.3-1m)
  update to 1.2.3(rebuild against DirectFB 0.9.18)

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.2-3m)
- reubuild aginst SDL-1.2.4-3m

* Thu May 20 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.2-2k)
- version up.

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (1.2.1-2k)
- version up.

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.0-16k)
- rebuild against libpng-1.2.2

* Fri Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.2.0-14k)
- enable to build

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (1.2.0-12k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Thu Jan 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.0-10k)
- automake autoconf 1.5

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.0-8k)
- nigirisugi

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (1.2.0-6k)
- rebuild against libpng 1.2.0.

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-4k)
- rebuild against SDL 1.2.2.

* Sat Apr 14 2001 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (1.1.0-5k)
- rebuild against audiofile-0.2.1.

* Tue Mar 27 2001 Toru Hoshina <toru@df-usa.com>
- (1.1.0-3k)

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (1.0.10-7k)
- rebuild against audiofile-0.2.0.

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.10-5k)
- added unresolved.resolve.patch

* Mon Oct 30 2000 Toru Hoshina <toru@df-usa.com>
- be kondarized. (1.0.10-1k)

* Wed Jan 19 2000 Sam Lantinga 
- converted to get package information from configure
* Tue Jan 18 2000 Hakan Tandogan <hakan@iconsult.com>
- initial spec file


