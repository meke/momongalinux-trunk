%global momorel 1
%global xfcever 4.10
%global xfce4ver 4.10.0
%global major 1.3

Name:           xfce4-power-manager
Version:        1.3.0
Release:        %{momorel}m%{?dist}
Summary:        Power management for the Xfce desktop environment

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:		xfce4-power-manager-1.3.0-fix-desktop.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  dbus-devel >= 0.60
BuildRequires:  dbus-glib-devel >= 0.70
BuildRequires:  gettext intltool desktop-file-utils
BuildRequires:  libnotify-devel >= 0.7.2
BuildRequires:  xfconf-devel >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  xorg-x11-proto-devel
Requires:       xfce4-panel >= %{xfce4ver}
#tmp Requires:       xfce4-doc
Requires:       polkit

%description
Xfce Power Manager uses the information and facilities provided by HAL to 
display icons and handle user callbacks in an interactive Xfce session.
Xfce Power Preferences allows authorised users to set policy and change 
preferences.


%prep
%setup -q
%patch0 -p1

%build
%configure
make %{?_smp_mflags} V=1


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'
%find_lang %{name}
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/%{name}-settings.desktop

# rm .la files
find %{buildroot}%{_libdir} -type f -name "*.la" -exec rm -f {} ';'

%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/%{name}
%{_bindir}/%{name}-settings
%{_bindir}/xfce4-pm-helper
%{_sbindir}/xfpm-power-backlight-helper
%config %{_sysconfdir}/xdg/autostart/%{name}.desktop
%{_libdir}/xfce4/panel/plugins/libxfce4*.so
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/%{name}-settings.desktop
%{_datadir}/icons/hicolor/*/*/*.*
%{_datadir}/xfce4/panel-plugins/xfce4-battery-plugin.desktop
%{_datadir}/xfce4/panel-plugins/xfce4-brightness-plugin.desktop
%{_datadir}/polkit-1/actions/org.xfce.power.policy
%{_mandir}/man1/%{name}-settings.1.*
%{_mandir}/man1/%{name}.1.*


%changelog
* Thu Jun  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-1m)
- update to version 1.2.0

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-2m)
- remove BR hal

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.10-1m)
- update
- rebuild against xfce4-4.8

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.5-5m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.5-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.5-1m)
- import from Fedora to Momonga

* Tue Mar 02 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.5-1
- Update to 0.8.5
- Make build verbose

* Sat Nov 21 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.4.2-1
- Update to 0.8.4.2

* Mon Nov 02 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.4.1-1
- Update to 0.8.4.1

* Tue Sep 29 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.4-1
- Update to 0.8.4
- Drop xfpm_session_set_client_id patch, fixed upstream

* Wed Sep 09 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.3.1-2
- Fix segfault in xfpm_session_set_client_id

* Sun Aug 09 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.3.1-1
- Update to 0.8.3.1

* Sat Aug 01 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.3-1
- Update to 0.8.3

* Thu Jul 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.2-3
- Patch to include dpmsconst.h instead of dpms.h

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jul 09 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.2-1
- Update to 0.8.2

* Mon Jul 06 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.1.1-1
- Update to 0.8.1.1

* Fri Jul 03 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.1-1
- Update to 0.8.1
- Drop libglade2 requirement

* Wed Jun 10 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-1
- Update to 0.8.0 final
- Update gtk-icon-cache scriptlets

* Wed May 20 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.3.RC2
- Update to 0.8.0RC2

* Tue Apr 28 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.3.RC1
- Update to 0.8.0RC1

* Mon Apr 13 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.2.beta2
- Update to 0.8.0beta2
- Drop xfpm-button-hal.patch, no longer necessary

* Mon Apr 13 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.2.beta1
- Add xfpm-button-hal.patch by Mike Massonnet

* Sun Apr 12 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.1.beta1
- Update to 0.8.0beta1

* Thu Apr 09 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.1.alpha2
- Update to 0.8.0alpha2

* Thu Apr 02 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-0.1.alpha
- Update to 0.8.0alpha 

* Tue Mar 24 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.6.5-1
- Update to 0.6.5
- Remove custom autostart file

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 19 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.6.2-1
- Update to 0.6.2

* Sat Feb  7 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.6.1-1
- Update to 0.6.1
- Include additional desktop file for autostarting the app

* Mon Nov 10 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.6.0-0.1.RC1
- Update to 0.6.0 RC1

* Fri Oct 31 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.6.0-0.1.0.beta1
- Initial Fedora package
