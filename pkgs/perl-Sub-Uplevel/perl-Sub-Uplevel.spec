%global         momorel 9
%global         srcver 0.24

Name:           perl-Sub-Uplevel
Version:        %{srcver}00
Release:        %{momorel}m%{?dist}
Summary:        Apparently run a function in a higher stack frame
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Sub-Uplevel/
Source0:        http://www.cpan.org/authors/id/D/DA/DAGOLDEN/Sub-Uplevel-%{srcver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-constant
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Test-Simple
Requires:       perl-constant
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Like Tcl's uplevel() function, but not quite so dangerous. The idea is
just to fool caller(). All the really naughty bits of Tcl's uplevel()
are avoided.

%prep
%setup -q -n Sub-Uplevel-%{srcver}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json perlcritic.rc README README.PATCHING
%{perl_vendorlib}/Sub/Uplevel.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2400-1m)
- update to 0.24

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-11m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2200-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2200-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2200-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-3m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-1m)
- update to 0.22

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2002-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2002-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2002-2m)
- rebuild against rpm-4.6

* Fri Sep 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2002-1m)
- update to 0.2002

* Wed Sep 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2001-1m)
- update to 0.2001

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1901-1m)
- update to 0.1901

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18-2m)
- rebuild against gcc43

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Wed Sep  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16
- do not use %%NoSource macro
- adjust BuildRequires and BuildRequiresuires and Requires

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.prg>
- (0.14-1m)
- update to 0.14

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-1m)
- spec file was autogenerated
