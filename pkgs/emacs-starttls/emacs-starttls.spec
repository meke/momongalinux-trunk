%global momorel 7
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Simple wrapper program for STARTTLS for Emacs
Name: emacs-starttls
Version: 0.10
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source0: ftp://opaopa.org/pub/elisp/starttls-%{version}.tar.gz
URL: ftp://opaopa.org/pub/elisp/
BuildRequires: emacs >= %{emacsver}
BuildRequires: openssl-devel
Requires: emacs >= %{emacsver}

Obsoletes: elisp-starttls
Provides: elisp-starttls

%description
starttls is a simple wrapper program for STARTTLS for Emacs.

%prep
%setup -q -n starttls-%{version}

%build
%{configure}
%{make}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/starttls
%{e_sitedir}/starttls.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-7m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-6m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.10-5m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-4m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-3m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-2m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-1m)
- initial packaging
