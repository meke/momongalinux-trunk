%global momorel 23
%{!?tetex:%define tetex 1}

Summary: A text formatting package based on SGML
Name: linuxdoc-tools
Obsoletes: sgml-tools
Obsoletes: linuxdoc-sgml
Version: 0.9.56
Release: %{momorel}m%{?dist}
License: "GPL or MIT/X"
Group: Applications/Publishing
Source0: http://ftp.debian.org/debian/pool/main/l/linuxdoc-tools/%{name}_%{version}.tar.gz
Patch2: linuxdoc-tools-0.9.20-lib64.patch
Patch3: linuxdoc-tools-0.9.56-flex.patch
Requires: jade
Url: http://packages.qa.debian.org/l/linuxdoc-tools.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: flex flex-static sgml-common jade groff
Requires: gawk groff
# this should anyway be only a "suggest"
%if %{tetex}
Requires: tetex-latex
%endif
Provides: sgml-tools
Provides: linuxdoc-sgml
Obsoletes: sgml-tools

%description
Linuxdoc-tools is a text formatting suite based on SGML (Standard
Generalized Markup Language), using the LinuxDoc document type.
Linuxdoc-tools allows you to produce LaTeX, HTML, GNU info, LyX, RTF,
plain text (via groff), and other format outputs from a single SGML
source.  Linuxdoc-tools is intended for writing technical software
documentation.

%prep
%setup -q
%patch2 -p1 -b .lib64
%patch3 -p1 -b .flex

%build
%configure --with-installed-nsgmls --with-installed-iso-entities
# Packaging brain-damage
pushd entity-map
  autoconf
  %configure
popd

make OPTIMIZE="$RPM_OPT_FLAGS" %{?_smp_mflags}
perl -pi -e 's,\$main::prefix/share/sgml/iso-entities-8879.1986/iso-entities.cat,/usr/share/sgml/sgml-iso-entities-8879.1986/catalog,' \
           lib/LinuxDocTools.pm

%install
rm -rf %{buildroot}
%makeinstall
eval `perl '-V:installvendorlib'`
mkdir -p $RPM_BUILD_ROOT/$installvendorlib
%makeinstall perl5libdir=$RPM_BUILD_ROOT/$installvendorlib
rm -rf %{buildroot}%{_libdir}/perl5/Text
mv %{buildroot}%{_docdir}/%{name} %{buildroot}%{_docdir}/%{name}-%{version}
perl -pi -e 's,/usr/share/sgml/iso-entities-8879.1986/iso-entities.cat,\$main::prefix/share/sgml/sgml-iso-entities-8879.1986/catalog,' \
           %{buildroot}%{_datadir}/%{name}/LinuxDocTools.pm

# Some files need moving around.
rm -f %{buildroot}%{_datadir}/%{name}/epsf.*
rm -f %{buildroot}%{_datadir}/%{name}/url.sty
install -d %{buildroot}%{_datadir}/texmf/tex/latex/misc
mv %{buildroot}%{_datadir}/%{name}/*.sty \
	%{buildroot}%{_datadir}/texmf/tex/latex/misc

cat > doc/COPYRIGHT <<EOF
(C) International Organization for Standardization 1986
Permission to copy in any form is granted for use with
conforming SGML systems and applications as defined in
ISO 8879, provided this notice is included in all copies.
EOF

%clean
rm -rf %{buildroot}

%post
[ -x %{_bindir}/texhash ] && /usr/bin/env - %{_bindir}/texhash > /dev/null 2>&1
exit 0

%postun
[ -x %{_bindir}/texhash ] && /usr/bin/env - %{_bindir}/texhash > /dev/null 2>&1
exit 0

%files
%defattr (-,root,root)
%doc %{_docdir}/%{name}-%{version}
%{_bindir}/*
%{_datadir}/%{name}
%{_datadir}/entity-map
%{_datadir}/texmf/tex/latex/misc/*.sty
%{_prefix}/lib*/perl5/vendor_perl/*/Text/EntityMap.pm
%{_mandir}/*/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-23m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-22m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-21m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-20m)
- rebuild against perl-5.18.0

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-19m)
- rebuild against perl-5.16.2

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-18m)
- add source (latest version is 0.9.68)

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-17m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-16m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-15m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.56-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.56-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.56-9m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.56-8m)
- add BuildRequires

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.56-7m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.56-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.56-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.9.56-4m)
- rebuild against perl-5.10.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.56-3m)
- use vendor

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.56-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.56-1m)
- update to 0.9.56
-- remove unused patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.21-5m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.21-4m)
- rebuild against perl-5.10.0-1m

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.21-3m)
- rebuild against perl-5.8.8

* Tue Jan 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.21-2m)
- modify install directory

* Wed Jan 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.21-1m)
- update to 0.9.21
- add Patch3: linuxdoc-tools-0.9.21-badif.patch
- add Patch4: linuxdoc-tools-fi.patch

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.20-15m)
- import from FC

* Wed Oct  6 2004 Tim Waugh <twaugh@redhat.com> 0.9.20-14
- Build requires groff (bug #134798).

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Sep 23 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- Allow rpms without a tetex dependency. That allows using sgml things
  for online things without installing the heavy tetex.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Sat Jan  4 2003 Jeff Johnson <jbj@redhat.com> 0.9.20-7
- use internal dep generator.

* Sat Dec 14 2002 Tim Powers <timp@redhat.com> 0.9.20-6
- don't use rpms internal dep generator

* Mon Oct 14 2002 Tim Waugh <twaugh@redhat.com> 0.9.20-5
- Rebuild.

* Wed Sep 11 2002 Than Ngo <than@redhat.com> 0.9.20-4
- Added fix to have lib64 in perl path on 64bit machine

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Apr 22 2002 Tim Waugh <twaugh@redhat.com> 0.9.20-1
- 0.9.20.
- Don't explicitly strip binaries (bug #62563).

* Thu Feb 28 2002 Elliot Lee <sopwith@redhat.com> 0.9.16-4
- Provides: sgml-tools (and linuxdoc-sgml).
- Use _smp_mflags and RPM_OPT_FLAGS.

* Tue Feb 26 2002 Tim Waugh <twaugh@redhat.com> 0.9.16-3
- Rebuild in new environment.

* Wed Jan 30 2002 Tim Waugh <twaugh@redhat.com> 0.9.16-2
- Rebuild to fix bug #59055.

* Mon Jan 28 2002 Tim Waugh <twaugh@redhat.com> 0.9.16-1
- 0.9.16.
- No longer need the libdir patch.

* Wed Jan 09 2002 Tim Powers <timp@redhat.com> 0.9.15-2
- automated rebuild

* Mon Dec  3 2001 Tim Waugh <twaugh@redhat.com> 0.9.15-1
- 0.9.15; incorporates most of the libdir patch.
- The fixsgml2latex patch is no longer required.
- Installed documentation now works.
- Don't ship backup files.
- Put the LaTeX style files in the texmf directory tree.
- Requires: gawk, groff.

* Mon Nov 26 2001 Tim Waugh <twaugh@redhat.com> 0.9.13-1
- Dump sgml-tools in favour of linuxdoc-tools (bug #56710).

* Mon Jun 18 2001 Tim Waugh <twaugh@redhat.com> 1.0.9-12
- Use %%{_tmppath}.
- Build requres flex.

* Wed May 30 2001 Tim Waugh <twaugh@redhat.com> 1.0.9-11
- Don't ship backup files.

* Wed May 30 2001 Tim Waugh <twaugh@redhat.com> 1.0.9-10
- Sync description with specspo.

* Thu Mar  8 2001 Tim Waugh <twaugh@redhat.com> 1.0.9-9
- Create temporary files safely (patch from Debian package).

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 10 2000 Bill Nottingham <notting@redhat.com>
- fix man page for sgml2txt (#10722)

* Sat Jun  3 2000 Bill Nottingham <notting@redhat.com>
- apparently this can't deal with FHS

* Thu Jun  1 2000 Bill Nottingham <notting@redhat.com>
- hey, it's self-hosting now!

* Fri Feb 11 2000 Preston Brown <pbrown@redhat.com>
- add copyright file (#8621)
- fix sgml2latex processing (#4114)
- fix configure tests (#6480)

* Wed Feb  9 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- get rid of "CC=egcs"
- require jade - sgml2html calls nsgmls.

* Fri Jan 28 2000 Bill Nottingham <notting@redhat.com>
- resurrect package in a non-conflicting manner

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Wed Mar 17 1999 Preston Brown <pbrown@redhat.com>
- we aren't going to 2.0.x for 6.0, using 1.0.9 instead (more stable)

* Thu Aug 20 1998 Bill Nottingham <notting@redhat.com>
- updated to 1.0.7

* Tue May 05 1998 Donnie Barnes <djb@redhat.com>
- changed default papersize to letter (from a4...sorry Europeans :-)
  use --papersize=a4 on any sgml2* command to change it or remove the
  patch from this spec file and rebuild.

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- updated to 1.0.6

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Jan 12 1998 Donnie Barnes <djb@redhat.com>
- updated from 0.99 to 1.0.3
- added BuildRoot

* Sat Nov 01 1997 Donnie Barnes <djb@redhat.com>
- fixed man pages

* Mon Oct 20 1997 Donnie Barnes <djb@redhat.com>
- new release - Obsoletes linuxdoc-sgml

