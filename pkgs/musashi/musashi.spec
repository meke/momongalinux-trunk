%global momorel 16
#%define srcrel pre6
%define date 20040214

# default encoding is euc
# if you want to change encoding, used setdocenc.sh
%define docenc 0
# Set this to "1" if you want to use UTF-8
%define utf8 0
# Set this to "1" if you want to use Shift_JIS
%define sjis 0

Summary: MUSASHI provides a set of commands for data mining and data warehousing
Name: musashi
Summary(ja): MUSAHI - Command line data mining and datawarehousing tools
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Databases
Url: http://musashi.sourceforge.ne.jp/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: http://osdn.dl.sourceforge.jp/musashi/8248/%{name}-core-%{version}.tar.gz 
NoSource: 0
Source1: http://osdn.dl.sourceforge.jp/musashi/8249/%{name}-man-%{date}.tar.gz 
NoSource: 1
Source2: http://osdn.dl.sourceforge.jp/musashi/8250/%{name}-module-%{date}.tar.gz 
NoSource: 2
Source3: http://osdn.dl.sourceforge.jp/musashi/8251/%{name}-check-%{date}.tar.gz 
NoSource: 3
Source4: xtmvavg.tar.gz
BuildRequires: libtool, automake, autoconf, libxml2-devel, zlib-devel, gawk
%if %{docenc}
BuildRequires: perl
%endif
Requires: libxml2, bash


%description
MUSASHI is a set of commands that enables us to efficiently execute various types of data manipulations in a flexible manner, mainly aiming at data processing of huge amount of data required for data mining. Data format which MUSASHI can deal with is either an XML table written in XML or plain text file with table structure. Maximum data size which MUSASHI can handle ranges from millions to tens of millions of records. 

%prep
%setup -q -c -a 1 -a 2

pushd %{name}-core-%{version}
%if %{utf8}
perl -pi -e 's,^#(helpEnc=ja-utf8),$1,' setdocenc.sh
perl -pi -e 's,^#(manEnc=ja-utf8),$1,' setdocenc.sh
perl -pi -e 's,^#(xmlEnc=UTF-8),$1,' setdocenc.sh
./setdocenc.sh
%endif

%if %{sjis}
perl -pi -e 's,^#(helpEnc=ja-sjis),$1,' setdocenc.sh
perl -pi -e 's,^#(manEnc=ja-sjis),$1,' setdocenc.sh
perl -pi -e 's,^#(xmlEnc=Shift_JIS),$1,' setdocenc.sh
./setdocenc.sh
%endif
popd

pushd %{name}-core-%{version}
sed -i.fix~ -e 's,^\. mssEnc\.sh,. ./mssEnc.sh,g' configure.in
autoreconf -fi
popd

%build
pushd %{name}-core-%{version}
%configure 
SED=/bin/sed make
popd

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/ja/man1
mkdir -p %{buildroot}%{_datadir}/musashi

pushd %{name}-core-%{version}
SED=/bin/sed %makeinstall
popd

pushd %{name}-man-%{date}
# install man files of en
cp en/*.1 %{buildroot}%{_mandir}/man1/
# install man files of ja
cp ja-euc/*.1 %{buildroot}%{_mandir}/ja/man1
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
  iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done
popd

# install modules' shell script
pushd %{name}-module-%{date}
install ja-euc/*.sh %{buildroot}%{_bindir}

# install xtmvavg.sh from [MUSASHI-users:294]
tar xzvf %{SOURCE4}
install xtmvavg/ja-euc/xtmvavg.sh %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/musashi/xtmvavg
install xtmvavg/ja-euc/test.sh %{buildroot}%{_datadir}/musashi/xtmvavg
install -m 644 xtmvavg/ja-euc/*.xt %{buildroot}%{_datadir}/musashi/xtmvavg
popd

# install check script to /usr/share/musashi
cd %{buildroot}%{_datadir}/musashi
tar xzvf %{SOURCE3}

find %{buildroot} -name "*.la" -delete

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
                                                                                
%post -p /sbin/ldconfig
                                                                                
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc %{name}-core-%{version}/{AUTHORS,COPYING,ChangeLog,README,INSTALL,NEWS}
%{_libdir}/lib*
%{_includedir}/musashi.h
%dir %{_includedir}/musashi
%{_includedir}/musashi/*
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/ja/man1/*
%{_datadir}/musashi/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-16m)
- rebuild for new GCC 4.6

* Wed Feb 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-15m)
- fix build failure

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-13m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-12m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-11m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-9m)
- fix build on x86_64

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-8m)
- add libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-5m)
- %%NoSource -> NoSource

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.3-4m)
- convert ja.man(UTF-8)

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-3m)
- delete libtool library

* Sat May 22 2004 Satoshi MACHINO <machino@vinelinux.org> 1.0.3-2
- build for RH9
- added xtmvavg from MUSASHI-users:294
- installed musashi-check-20040214 in %{_datadir}/musashi
- added %%claen, %%post and %%postun

* Tue Apr 27 2004 Satoshi MACHINO <machino@vinelinux.org> 1.0.3-1
- build for RHL9
