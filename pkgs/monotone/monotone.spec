%global momorel 14
#%global emacsver 23.2

Name: monotone
Version: 1.0
Release: %{momorel}m%{?dist}

Summary: A free, distributed version control system
Group: Development/Tools
License: GPLv2+

URL: http://monotone.ca/
Source0: http://monotone.ca/downloads/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: monotone.init
Source2: monotone.sysconfig
Source3: README.monotone-server
Source4: monotone-server-tmpfiles.conf

Patch0: %{name}-1.0-file_handle.patch
Patch1: monotone-1.0-boost147.patch
Patch2: monotone-1.0-fix-help.patch
Patch3: monotone-1.0-pcre.patch
Patch4: monotone-1.0-boost155.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: zlib-devel
BuildRequires: boost-devel >= 1.55.0
BuildRequires: botan-devel >= 1.6.3
BuildRequires: pcre-devel >= 8.31
BuildRequires: sqlite-devel >= 3.3.8
BuildRequires: lua-devel >= 5.1
BuildRequires: libidn-devel
BuildRequires: glibc-headers => 2.14

# The test suite needs cvs.
BuildRequires: cvs

## We need Emacs to byte-compile the Emacs Lisp code.
##BuildRequires: emacs >= %{emacsver}

Requires(post): info
Requires(preun): info

%description
monotone is a free, distributed version control system.
It provides fully disconnected operation, manages complete
tree versions, keeps its state in a local transactional
database, supports overlapping branches and extensible
metadata, exchanges work over plain network protocols,
performs history-sensitive merging, and delegates trust
functions to client-side RSA certificates.

%package server
Summary: Standalone server setup for monotone
Requires: monotone = %{version}-%{release}
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): initscripts, chkconfig
Requires(postun): initscripts
Group: Development/Tools

%description server
This package provides an easy-to-use standalone server setup for monotone.

%package -n perl-Monotone
Summary: Perl Module for monotone
Requires: monotone = %{version}-%{release}
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Group: Development/Tools

%description -n perl-Monotone
This is a simple Perl module to start a monotone automate sub-process
and then pass commands to it.

%prep
%setup -q

%patch0 -p1 -b .glibc-2.14
%patch1 -p1 -b .boost147~
%patch2 -p1 -b .fixhelp
%patch3 -p1 -b .pcre
%patch4 -p1 -b .boost155

%build
%configure
make %{?_smp_mflags}

%check
# extra/bash_completion will fail if your shell disables bash_completion
rm -rf test/extra/bash_completion
export DISABLE_NETWORK_TESTS=1
make %{?_smp_mflags} check || { cat tester_dir/work/*.log; false; }

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_infodir}/dir
mv %{buildroot}%{_datadir}/doc/%{name} _doc

%find_lang %{name}

#lispdir=%{buildroot}%{_datadir}/emacs/site-lisp
#mkdir -p ${lispdir}
#%{__install} -c -m 0444 contrib/monotone*.el ${lispdir}
#emacs -batch -f batch-byte-compile ${lispdir}

mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_localstatedir}/lib
ln -snf ../bin/mtn %{buildroot}%{_sbindir}/monotone-server
%{__install} -D -m 0555 %{SOURCE1} \
             %{buildroot}%{_initscriptdir}/monotone
%{__install} -D -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/monotone
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/monotone
%{__install} -d -m 0750 %{buildroot}%{_sysconfdir}/monotone/private-keys
%{__install} -d -m 0770 %{buildroot}%{_localstatedir}/lib/monotone
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/run/monotone

install -D -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/tmpfiles.d/monotone.conf

# These do not actually wind up in the package, due to %%ghost.
%{__install} -c -m 0440 /dev/null \
             %{buildroot}%{_sysconfdir}/monotone/passphrase.lua
%{__install} -c -m 0640 /dev/null \
             %{buildroot}%{_sysconfdir}/monotone/read-permissions
%{__install} -c -m 0640 /dev/null \
             %{buildroot}%{_sysconfdir}/monotone/write-permissions
%{__install} -c -m 0644 /dev/null \
             %{buildroot}%{_sysconfdir}/monotone/monotonerc
touch %{buildroot}%{_localstatedir}/lib/monotone/server.mtn

cp %{SOURCE3} .

%{__install} -D -m 0644 -p contrib/Monotone.pm \
             %{buildroot}%{perl_vendorlib}/Monotone.pm

%clean
rm -f README.monotone-server
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ]
then
        /sbin/install-info %{_infodir}/monotone.info %{_infodir}/dir > /dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]
then
        /sbin/install-info --delete %{_infodir}/monotone.info %{_infodir}/dir > /dev/null 2>&1 || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README UPGRADE
%doc _doc/*
%{_bindir}/mtn
%{_bindir}/mtnopt
%{_bindir}/mtn-cleanup
%{_infodir}/monotone.info*
#%{_datadir}/emacs/site-lisp/monotone*.el*
%{_sysconfdir}/bash_completion.d/monotone.bash_completion
%{_mandir}/man1/*.1*
%dir %{_datadir}/monotone
%dir %{_datadir}/monotone/hooks
%{_datadir}/monotone/hooks
%dir %{_datadir}/monotone/scripts
%{_datadir}/monotone/scripts

%files -n perl-Monotone
%defattr(-,root,root,-)
%{perl_vendorlib}/Monotone.pm

%files server
%defattr(-,root,root,-)
%doc README.monotone-server
%{_sbindir}/monotone-server
%{_initscriptdir}/monotone
%config(noreplace) %{_sysconfdir}/tmpfiles.d/monotone.conf
%dir %attr(0755,monotone,monotone) %{_localstatedir}/run/monotone
%config(noreplace) %{_sysconfdir}/sysconfig/monotone
%dir %attr(0755,root,monotone) %{_sysconfdir}/monotone
%dir %attr(0750,root,monotone) %{_sysconfdir}/monotone/private-keys
%attr(0640,root,monotone) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) %{_sysconfdir}/monotone/monotonerc
%attr(0440,root,monotone) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) %{_sysconfdir}/monotone/passphrase.lua
%attr(0640,root,monotone) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) %{_sysconfdir}/monotone/read-permissions
%attr(0640,root,monotone) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) %{_sysconfdir}/monotone/write-permissions
%dir %attr(0770,monotone,monotone) %{_localstatedir}/lib/monotone
%attr(0660,monotone,monotone) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) %{_localstatedir}/lib/monotone/server.mtn

%pre server
# Add "monotone" user per http://fedoraproject.org/wiki/Packaging/UsersAndGroups
getent group monotone > /dev/null || groupadd -r monotone
getent passwd monotone > /dev/null ||
useradd -r -g monotone -r -d %{_localstatedir}/lib/monotone -s /sbin/nologin \
        -c "Monotone Netsync Server" monotone
exit 0

%post server
# Register the monotone service
/sbin/chkconfig --add monotone

%preun server
if [ $1 = 0 ]; then
        /sbin/service monotone stop > /dev/null 2>&1
        /sbin/chkconfig --del monotone
fi

%postun
if [ $1 -gt 1 ]; then
        # Restart the running server: updates its db format when needed.
        /sbin/service monotone condrestart > /dev/null 2>&1 || :
fi


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-14m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-13m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-12m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-11m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-10m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-9m)
- rebuild against perl-5.16.2
o
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-8m)
- rebuild against pcre-8.31

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-7m)
- rebuild against perl-5.16.1

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for boost 1.50.0

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-4m)
- rebuild against perl-5.16.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for boost-1.48.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- rebuild against perl-5.14.2

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-2m)
- add tmpfiles.d file

* Tue Sep  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-1m)
- update to 1.0
-- delete emacs support. it is no longer maintained
- add patch for boost 1.47

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.1-9m)
- rebuild against perl-5.14.1

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.1-8m)
- import file_handle.patch from fedora to enable build with new glibc

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.1-7m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.1-6m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.1-5m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.1-4m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.1-3m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.1-2m)
- rebuild against boost-1.44.0

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.1-1m)
- update to 0.99.1

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.1-1m)
- update to 0.48.1
- add sqlite-3.7.2,patch

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-3m)
- rebuild against perl-5.12.2
-- now, we can not build this package due to sqlite issue

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.48-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-7m)
- rebuild against emacs-23.2

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-6m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-3m)
- rebuild against perl-5.10.1

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.44-2m)
- clean up options of configure

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-1m)
- update to 0.44 based on Fedora 11 (0.44-1)

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.42-4m)
- apply gcc44 patch
-- upstream rev.33022690.. and rev.76e258cb..

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.42-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.42-2m)
- release %%{_sysconfdir}/bash_completion.d
- it's already provided by bash-completion

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.42-1m)
- import from Fedora-devel to Momonga

* Fri Jan  2 2009 Thomas Moschny <thomas.moschny@gmx.de> - 0.42-2
- Pack Monotone.pm (in a subpackage). (#450267)

* Fri Jan  2 2009 Thomas Moschny <thomas.moschny@gmx.de> - 0.42-1
- Updated for 0.42 release.

* Fri Sep 12 2008 Thomas Moschny <thomas.moschny@gmx.de> - 0.41-1
- Updated for 0.41 release.
- Added mtnopt helper.

* Mon Aug 11 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.40-2
- fix license tag

* Mon Apr 14 2008 Roland McGrath <roland@redhat.com> - 0.40-1
- Updated for 0.40 release.
- Tweaked trivia for packaging guidelines nits.

* Mon Feb 25 2008 Roland McGrath <roland@redhat.com> - 0.39-1
- Updated for 0.39 release.

* Sat Dec 22 2007 Roland McGrath <roland@redhat.com> - 0.38-2
- Fix monotone-server user creation. (#426607)
- Moved monotone-server database to /var/lib. (#426608)
- Use monotone@ in server key name. (#426609)

* Fri Dec 21 2007 Roland McGrath <roland@redhat.com> - 0.38-1
- Updated for 0.38 release.

* Sat Oct 27 2007 Roland McGrath <roland@redhat.com> - 0.37-3
- Updated for 0.37 release.
- Prevent destroying old passphrase file with 'service monotone genkey'.
- Put LSB standard comments in monotone.init for monotone-server subpackage.

* Tue Aug 28 2007 Roland McGrath <roland@redhat.com> - 0.36-2
- Clean up %%pre script per packaging guidelines.
- Disable ppc and ppc64 builds temporarily since make check fails. (#259161)

* Fri Aug  3 2007 Roland McGrath <roland@redhat.com> - 0.36-1
- Updated for 0.36 release.

* Fri Jul 06 2007 Florian La Roche <laroche@redhat.com> - 0.35-4
- add more requires

* Wed May 16 2007 Roland McGrath <roland@redhat.com> - 0.35-3
- Fix locale dependency in monotone-server init script. (#213893)

* Wed May  9 2007 Roland McGrath <roland@redhat.com> - 0.35-2
- Updated for 0.35 release.
- Avoid unnecessary "db migrate" in monotone-server init script. (#213893)

* Wed Apr  4 2007 Roland McGrath <roland@redhat.com> - 0.34-1
- Updated for 0.34 release.

* Thu Mar  1 2007 Roland McGrath <roland@redhat.com> - 0.33-1
- Updated for 0.33 release.
- Install monotone.bash_completion file.

* Wed Feb 28 2007 Roland McGrath <roland@redhat.com> - 0.32-1
- Updated for 0.32 release.

* Thu Dec 21 2006 Kevin Fenzi <kevin@tummy.com> - 0.31-2
- Bump and rebuild to fix upgrade path

* Sat Nov 11 2006 Roland McGrath <roland@redhat.com> - 0.31-1
- Updated for 0.31 release.

* Tue Oct 10 2006 Roland McGrath <roland@redhat.com> - 0.30-1
- Updated for 0.30 release.
- Fix service script to work around buggy init.d/functions. (#198761)

* Thu Aug  3 2006 Roland McGrath <roland@redhat.com> - 0.28-2
- Updated for 0.28 release. (#198652)
- Move server PID file into /var/run/monotone subdirectory. (#198761)

* Tue Jul 11 2006 Roland McGrath <roland@redhat.com> - 0.27-1
- Updated for 0.27 release.

* Mon May  8 2006 Roland McGrath <roland@redhat.com> - 0.26-2
- Fix service script genkey subcommand.

* Mon Apr 10 2006 Roland McGrath <roland@redhat.com> - 0.26-1
- Updated for 0.26 release.
  - Major changes; see UPGRADE doc file for details.

* Fri Jan  6 2006 Roland McGrath <roland@redhat.com> - 0.25-2
- Restore testsuite fix for nonroot owner of / in build chroot.

* Thu Jan  5 2006 Roland McGrath <roland@redhat.com> - 0.25-1
- Updated for 0.25 release.

* Sun Dec 11 2005 Roland McGrath <roland@redhat.com> - 0.24-1
- Updated for 0.24 release.

* Mon Oct  3 2005 Roland McGrath <roland@redhat.com> - 0.23-1
- Updated for 0.23 release.

* Mon Aug 22 2005 Roland McGrath <roland@redhat.com> - 0.22-4
- Updated for 0.22 release.
- Added monotone-server package.

* Sun Aug  7 2005 Roland McGrath <roland@redhat.com> - 0.21-3
- Work around non-root build user owning / in mock chroot builds.

* Wed Jul 27 2005 Roland McGrath <roland@redhat.com> - 0.21-2
- Include monotone-nav.el too.
- Add BuildRequires on cvs so the test suite can run.

* Mon Jul 18 2005 Roland McGrath <roland@redhat.com> - 0.21-1
- Updated for 0.21 release.
- Install Emacs support.

* Thu Jul  7 2005 Roland McGrath <roland@redhat.com> - 0.20-0.1
- Updated for 0.20 release.
- Added %%check section.
- Cannot use FC4 native sqlite3, need newer bundled one.

* Mon Apr 18 2005 Jeffrey C. Ollie <jcollie@lt16586.campus.dmacc.edu> - 0.18-0.4
- Modified summary so that it doesn't contain the name

* Thu Apr 14 2005 Jeffrey C. Ollie <jcollie@lt16586.campus.dmacc.edu> - 0.18-0.3
- Modified install-info commands to prevent errors in case of --excludedocs

* Wed Apr 13 2005 Jeffrey C. Ollie <jcollie@lt16586.campus.dmacc.edu> - 0.18-0.2
- Added post and postun scripts to take care of .info file
- Added parallel make flags

* Wed Apr 13 2005 Jeffrey C. Ollie <jcollie@lt16586.campus.dmacc.edu> - 0.18-0.1
- First version.
