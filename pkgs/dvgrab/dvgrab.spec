%global momorel 5

Summary: DV grabber through the FireWire interface
Name: dvgrab
Version: 3.5
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.kinodv.org/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/kino/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: dvgrab-3.4-gcc44.patch
BuildRequires: libavc1394-devel
BuildRequires: libdv-devel
BuildRequires: libiec61883-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libraw1394-devel >= 2.0.2

%description
Dvgrab is a small utility that grabs AVI-2 video from a DV camera using the
IEEE-1394 bus (aka FireWire).

%prep
%setup -q

%patch1 -p1 -b .gcc44~

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/dvgrab
%{_mandir}/man1/dvgrab.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5-3m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-2m)
- rebuild against libjpeg-8a

* Sat Jan 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5-1m)
- version 3.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4-4m)
- rebuild against libjpeg-7

* Sat Aug 08 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-3m)
- add patch for gcc44, generated by gen44patch(v1)

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-2m)
- rebuild against libraw1394-2.0.2

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-1m)
- update to 3.4

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-3m)
- add patch for gcc43, generated by gen43patch(v1)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-2m)
- %%NoSource -> NoSource

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1-1m)
- initial package for kdenlive-0.5
