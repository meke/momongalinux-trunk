%global momorel 1

Summary: Ruby binding for MeCab
Name: ruby-mecab
Version: 0.994
Release: %{momorel}m%{?dist}
License: LGPL 
Group: Development/Libraries
URL: http://mecab.sourceforge.net/
Source0: http://mecab.googlecode.com/files/mecab-ruby-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby-devel >= 1.9.2, mecab-devel >= %{version}
Requires: ruby >= 1.9.2, mecab >= %{version}
Provides: mecab-ruby

%description
Ruby binding for MeCab

%prep
%setup -q -n mecab-ruby-%{version}
%build
ruby extconf.rb
make

%install
%makeinstall DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README test.rb bindings.html
%{ruby_sitearchdir}/MeCab.so

%changelog
* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-1m)
- update to 0.994

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-1m)
- update to 0.993

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99
- rebuild against mecab-0.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-4m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.98-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-4m)
- version down to 0.97 official release

* Tue May 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-0.1.1m)
- update to 0.98pre1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-2m)
- rebuild against gcc43

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96-2m)
- %%NoSource -> NoSource

* Wed Jun 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.95-2m)
- rebuild against ruby-1.8.6-4m

* Sun Apr  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95
- change URI from sourceforge.jp to sourceforge.net

* Wed Aug  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.93-1m)
- update to 0.93 (rebuild against mecab-0.93-1m)

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.92-1m)
- update to 0.92 (with mecab-0.92)

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.91-1m)
- update to 0.91 and rebuild against mecab-0.91-1m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.81-2m)
- /usr/lib/ruby

* Tue Mar 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-1m)
- version up

* Mon Oct  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.78-2m)
- rebuild against gcc-c++-3.4.2

* Wed Aug 18 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.78-1m)
- initial import
