%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           telepathy-sunshine
Version:        0.2.0
Release:        %{momorel}m%{?dist}
Summary:        Gadu-Gadu connection manager for telepathy
Group:          Applications/Communications
License:        GPLv3+
URL:            http://telepathy.freedesktop.org/wiki
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
#remove unnecessary shebangs
Patch0:         %{name}-0.1.8-noshebang.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel >= 2.7
Requires:       dbus
Requires:       pyOpenSSL
Requires:       python-oauth
Requires:       python-telepathy >= 0.15.17
Requires:       python-twisted-web
Requires:       telepathy-filesystem
BuildArch:      noarch

%description
Telepathy-sunshine is a Gadu-Gadu network connection manager. It supports the
GG8 aka Nowe Gadu Gadu features such as UTF-8 encoding and new statuses.

%prep
%setup -q
%patch0 -p1 -b .noshebang

%build
%configure

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING
%{python_sitelib}/sunshine
%{_libexecdir}/%{name}
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.ConnectionManager.sunshine.service
%{_datadir}/telepathy/managers/sunshine.manager 

%changelog
* Mon May 30 2011 NARITAKoichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.8-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.8-1m)
- import from Fedora devel

* Sun May 16 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.8-1
- Updated to 0.1.8

* Sat Apr 03 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.7-1
- Updated to 0.1.7
- Added pyOpenSSL to Requires just to be on the safe side
- python-telepathy is needed in at least version 0.15.17
- Dropped python-twisted-core requirement, it was redundant

* Thu Feb 18 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.6-1
- Updated to 0.1.6
- Dropped libtelepathy requirement, -haze is enough

* Mon Feb 08 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.5-4.20100208git
- Updated to the latest git snapshot
- Added libtelepathy to Requires as a workaround

* Sat Feb 06 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.5-3.20100202git
- Updated to the git snapshot
- Changed license to GPLv3+
- Added python-oauth to Requires

* Fri Jan 29 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.5-2
- Use %%{name} macro in %%files

* Thu Jan 21 2010 Julian Sikorski <belegdol@fedoraproject.org> - 0.1.5-1
- Initial RPM release
