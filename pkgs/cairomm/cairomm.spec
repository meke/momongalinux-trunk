%global momorel 1

Summary: C++ interface to cairo
Name: cairomm
Version: 1.10.0
Release: %{momorel}m%{?dist}
License: LGPL or MPL
Group: System Environment/Libraries
URL: http://cairographics.org
Source0: http://cairographics.org/releases/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cairo-devel >= 1.8.0
BuildRequires: pkgconfig

%description 
This library provides a C++ interface to cairo.

%package devel
Summary: Cairo developmental libraries and header files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
cairomm devel package

%prep
%setup -q

%build
%configure --enable-gtk-doc --enable-shared --enable-static --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig 

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING MAINTAINERS NEWS README
%{_libdir}/libcairomm-1.0.so.* 

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/cairomm-1.0
%{_includedir}/cairomm-1.0/cairomm
%{_libdir}/libcairomm-1.0.a
%{_libdir}/libcairomm-1.0.so
%exclude %{_libdir}/libcairomm-1.0.la
%{_libdir}/cairomm-1.0/*
%{_libdir}/pkgconfig/*
%{_datadir}/devhelp/books/cairomm-1.0
%{_datadir}/doc/cairomm-1.0

%changelog
* Sat Nov 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.0-1m)
- update to 1.10.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.8-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.8-1m)
- update to 1.9.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2-2m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.2-1m)
- update to 1.9.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-3m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.4-2m)
- fix %%files to avoid conflicting

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.4-1m)
- update to 1.8.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-1m)
- update to 1.8.2

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update 1.7.0

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update to 1.6.4

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-2m)
- rebuild against gcc43

* Mon Dec 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Wed Aug 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Fri Jul 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.4

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- initial build
