%global momorel 11

%{!?pyver: %global pyver %(%{__python} -c "import sys ; print sys.version[:3]")}
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: scientific data visualizer
Name: mayavi
Version: 1.5
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Engineering
URL:   http://mayavi.sourceforge.net/
Source: http://dl.sourceforge.net/sourceforge/mayavi/MayaVi-%{version}.tar.gz
NoSource: 0
Source1: mayavi-icon.png
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= %{pyver}, vtk-devel
BuildRequires: desktop-file-utils >= 0.16
Requires: python, vtk-python, vtk-tcl

%description 
 MayaVi is a free, easy to use scientific data visualizer. It is written in Python and uses the amazing Visualization Toolkit (VTK) for the graphics. It provides a GUI written using  Tkinter. MayaVi is free and distributed under the conditions of the  BSD license. It is also cross platform and should run on any platform where both Python and VTK are available (which is almost any *nix, Mac OSX or Windows).

%prep
%setup -q -n MayaVi-%{version}

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --prefix=%{_prefix} --root=%{buildroot} --compile

# add icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
cp %{SOURCE1} %{buildroot}%{_datadir}/pixmaps

## desktop
%__mkdir_p %{buildroot}%{_datadir}/applications
%__cat > mayavi.desktop <<EOF
[Desktop Entry]
Name=Mayavi
GenericName=Mayavi VTK file viewer 
Comment=VTK file viewer
Exec=mayavi
Terminal=false
Type=Application
Icon=mayavi-icon.png
EOF
# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --add-category Engineering \
  --add-category Education \
    ./mayavi.desktop


%clean
rm -rf %{buildroot}

%post 
update-desktop-database 2> /dev/null || :

%files
%defattr(-, root, root)
%doc doc examples
%{_bindir}/*
%{python_sitelib}/*
%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/mayavi-icon.png

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5-11m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-8m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-7m)
- add menu icon

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-6m)
- build fix with desktop-file-utils-0.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-4m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5-3m)
- rebuild agaisst python-2.6.1-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc43

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5-1m)
- import to Momonga

* Wed Apr 26 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.5-0.0.3m)
- add desktop file

* Sat Apr 22 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.5-0.0.2m)
- rebuild against python2.4

* Wed Sep 14 2005 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.5-0.0.1m)
- version 1.5

* Mon Apr 11 2005 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.4-0.0.1m)
- version 1.4 has been released

* Tue Sep 22 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.3-0.0.2m)
- fix path

* Sat Sep 19 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.3-0.0.1m)
- build for Momonga
