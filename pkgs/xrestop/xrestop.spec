%global momorel 7

Summary: X Resource Monitor
Name: xrestop
Version: 0.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
URL: http://www.freedesktop.org/Software/xrestop
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: ncurses-devel libXres-devel libXext-devel libX11-devel
BuildRequires: libXau-devel

%description
A utility to monitor application usage of X resources in the X Server, and
display them in a manner similar to 'top'.  This is a very useful utility
for tracking down application X resource usage leaks.

%prep
%setup -q

%build
%configure
make
# SUBDIRS=

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
#SUBDIRS=

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL NEWS README
%{_bindir}/xrestop
%{_mandir}/man1/xrestop.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-1m)
- import from f7 to Momonga

* Tue Feb 13 2007 Adam Jackson <ajax@redhat.com> 0.4-1
- Update to 0.4

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.2-6.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.2-6.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.2-6.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Dec 01 2005 Karsten Hopp <karsten@redhat.de> 0.2-6
- fix build requirements

* Thu Mar  3 2005 Mike A. Harris <mharris@redhat.com> 0.2-5
- Rebuilt with gcc 4 for FC4.

* Thu Oct 14 2004 Mike A. Harris <mharris@redhat.com> 0.2-4
- Added "BuildRequires: xorg-x11-devel, ncurses-devel" (#125029)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com> 0.2-3
- rebuilt

* Wed Apr 14 2004 Mike A. Harris <mharris@redhat.com> 0.2-2
- Add missing documentation
- Add xrestop-manpage-fix.patch to fix bug (#118038)

* Tue Mar  9 2004 Mike A. Harris <mharris@redhat.com> 0.2-1
- Initial Red Hat RPM package.


