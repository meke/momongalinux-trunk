%global momorel 1

%{!?tcl:%define tcl 1}
%{!?guile:%define guile 1}

Summary: Connects C/C++/Objective C to some high-level programming languages
Name: swig
Version: 2.0.9
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Tools
URL: http://swig.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/swig/swig-%{version}.tar.gz
NoSource: 0
Patch4: swig203-rh706140.patch
Patch9: swig207-setools.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl, python-devel, pcre-devel
%if %{tcl}
BuildRequires: tcl-devel
%endif
%if %{guile}
BuildRequires: guile-devel
%endif
BuildRequires: autoconf, automake, gawk, dos2unix

%description
Simplified Wrapper and Interface Generator (SWIG) is a software
development tool for connecting C, C++ and Objective C programs with a
variety of high-level programming languages.  SWIG is primarily used
with Perl, Python and Tcl/TK, but it has also been extended to Java,
Eiffel and Guile.  SWIG is normally used to create high-level
interpreted programming environments, systems integration, and as a
tool for building user interfaces

%package doc
Summary: Documentation files for SWIG
License: BSD
Group: Development/Tools
BuildArch: noarch

%description doc
This package contains documentation for SWIG and useful examples

%prep
%setup -q -n %{name}-%{version}
%patch4 -p1 -b .rh706140
%patch9 -p1 -b .setools

# as written on https://fedoraproject.org/wiki/Packaging_talk:Perl, section 2
# (specific req/prov filtering). Before you remove this hack make sure you don't
# reintroduce https://bugzilla.redhat.com/show_bug.cgi?id=489421
cat << \EOF > %{name}-prov
#!/bin/sh
%{__perl_provides} `perl -p -e 's|\S+%{_docdir}/%{name}-doc-%{version}\S+||'`
EOF

%define __perl_provides %{_builddir}/%{name}-%{version}/%{name}-prov
chmod +x %{__perl_provides}

cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} `perl -p -e 's|\S+%{_docdir}/%{name}-doc-%{version}\S+||'`
EOF

%define __perl_requires %{_builddir}/%{name}-%{version}/%{name}-req
chmod +x %{__perl_requires}

for all in CHANGES README; do
	iconv -f ISO88591 -t UTF8 < $all > $all.new
	touch -r $all $all.new
	mv -f $all.new $all
done

%build
./autogen.sh
%configure
make %{?_smp_mflags}
#make check

%install
rm -rf %{buildroot}

pushd Examples/
# Remove all arch dependent files in Examples/
find -type f -name 'Makefile.in' | xargs rm -f --

# We don't want to ship files below.
rm -rf test-suite
find -type f -name '*.dsp' | xargs rm -f --
find -type f -name '*.dsw' | xargs rm -f --

# Convert files to UNIX format
for all in `find -type f`; do
        dos2unix -k $all
        chmod -x $all
done
popd

make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_datadir}/swig
%{_mandir}/man1/ccache-swig.1*
%doc ANNOUNCE CHANGES CHANGES.current INSTALL LICENSE LICENSE-GPL
%doc LICENSE-UNIVERSITIES COPYRIGHT README TODO

%files doc
%defattr(-,root,root,-)
%doc Doc Examples LICENSE LICENSE-GPL LICENSE-UNIVERSITIES COPYRIGHT

%changelog
* Mon Mar 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.9-1m)
- update to 2.0.9
- sync fc19

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-2m)
- rebuild against pcre-8.31

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update to 2.0.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.40-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.40-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.40-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.40-2m)
- use BuildRequires

* Thu Jan 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.40-1m)
- update to 1.3.40

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.39-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.39-1m)
- update to 1.3.39

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.35-2m)
- rebuild against rpm-4.6

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.35-1m)
- update 1.3.35

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.33-2m)
- rebuild against gcc43

* Fri Nov 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.33-1m)
- update 1.3.33

* Sat Jun 30 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.31-2m)
- added BuildPrereq: java-devel

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.31-1m)
- update 1.3.31

* Thu Aug 17 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.29-1m)
- version up againt for subversion-1.4

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.24-1m)
- version down

* Wed May 03 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.3.29-1m)
- version up

* Sun Dec 11 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.27-2m)
- remove Patch0: swig-1.3.19-x86_64.patch

* Sun Dec  5 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.3.27-1m)
- new version.
- stop applying dated patches.

* Sun Mar 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.21-2m)
- revised swig-1.3.19-x86_64.patch

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.3.21-1m)
- version up

* Wed Jan 19 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.3.19-6m)
- apply x86_64 patch on x86_64 environment only

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.19-5m)
- enable x86_64.

* Fri Oct  1 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.19-4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Fri Oct  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.3.19-3m)
- lib64

* Sun Apr 27 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.19-2m)
  added /usr/lib/libswig*

* Sun Apr 27 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.19-1m)
  update to 1.3.19

* Fri Jul 19 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.13-1m)
- update to 1.3.13

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gcc 2.96.

* Sat Nov 25 2000 Kenichi Matsubara <m@kondara.org>
- add %clean,%ChangeLog section. :-P

