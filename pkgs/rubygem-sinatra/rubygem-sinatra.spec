# Generated from sinatra-1.3.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname sinatra

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Classy web-development dressed in a DSL
Name: rubygem-%{gemname}
Version: 1.3.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://www.sinatrarb.com/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(rack) => 1.3
Requires: rubygem(rack) < 2
Requires: rubygem(rack) >= 1.3.6
Requires: rubygem(rack-protection) => 1.2
Requires: rubygem(rack-protection) < 2
Requires: rubygem(tilt) => 1.3
Requires: rubygem(tilt) < 2
Requires: rubygem(tilt) >= 1.3.3
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Sinatra is a DSL for quickly creating web applications in Ruby with minimal
effort.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.de.rdoc
%doc %{geminstdir}/README.es.rdoc
%doc %{geminstdir}/README.fr.rdoc
%doc %{geminstdir}/README.hu.rdoc
%doc %{geminstdir}/README.jp.rdoc
%doc %{geminstdir}/README.pt-br.rdoc
%doc %{geminstdir}/README.pt-pt.rdoc
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/README.ru.rdoc
%doc %{geminstdir}/README.zh.rdoc
%doc %{geminstdir}/LICENSE
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.6-1m)
- update 1.2.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-2m)
- add Requires
 
* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-1m)
- Initial package for Momonga Linux
