%global momorel 4

%global fontname google-droid

%global download_root http://android.git.kernel.org/?p=platform/frameworks/base.git;a=blob_plain;f=data/fonts/

%global common_desc \
The Droid typeface family was designed in the fall of 2006 by Ascender's \
Steve Matteson, as a commission from Google to create a set of system fonts \
for its Android platform. The goal was to provide optimal quality and comfort \
on a mobile handset when rendered in application menus, web browsers and for \
other screen text.

Name:    %{fontname}-fonts
# No sane versionning upstream, use the date we did a git dump
Version: 20100917
Release: %{momorel}m%{?dist}
Summary: General-purpose fonts released by Google as part of Android

Group:     User Interface/X
License:   "ASL 2.0"
URL:       http://android.git.kernel.org/?p=platform/frameworks/base.git;a=tree;f=data/fonts
Source1:   %{download_root}DroidSans.ttf
Source2:   %{download_root}DroidSans-Bold.ttf
Source3:   %{download_root}DroidSansJapanese.ttf
Source4:   %{download_root}DroidSansFallback.ttf
Source5:   %{download_root}DroidSansArabic.ttf
Source6:   %{download_root}DroidSansHebrew.ttf
Source7:   %{download_root}DroidSansThai.ttf
Source8:   %{download_root}DroidSansMono.ttf
Source9:   %{download_root}DroidSerif-Regular.ttf
Source10:  %{download_root}DroidSerif-Bold.ttf
Source11:  %{download_root}DroidSerif-Italic.ttf
Source12:  %{download_root}DroidSerif-BoldItalic.ttf

Source20:  %{download_root}NOTICE
Source21:  %{download_root}README.txt
Source31:  %{name}-sans-fontconfig.conf
Source32:  %{name}-sans-mono-fontconfig.conf
Source33:  %{name}-serif-fontconfig.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch
BuildRequires: fontpackages-devel

%description
%common_desc


%package common
Summary:  Common files of the Droid font set
Requires: fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%package -n %{fontname}-sans-fonts
Summary:  A humanist sans serif typeface
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-sans-fonts
%common_desc

Droid Sans is a humanist sans serif typeface designed for user interfaces and
electronic communication.

%_font_pkg -n sans -f ??-%{fontname}-sans.conf DroidSans.ttf DroidSans-Bold.ttf DroidSansJapanese.ttf DroidSansFallback.ttf DroidSansArabic.ttf DroidSansHebrew.ttf DroidSansThai.ttf


%package -n %{fontname}-sans-mono-fonts
Summary:  A humanist monospace sans serif typeface
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-sans-mono-fonts
%common_desc

Droid Sans Mono is a humanist monospace sans serif typeface designed for user
interfaces and electronic communication.

%_font_pkg -n sans-mono -f ??-%{fontname}-sans-mono.conf DroidSansMono.ttf


%package -n %{fontname}-serif-fonts
Summary:  A serif typeface
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-serif-fonts
%common_desc

Droid Serif is a contemporary serif typeface family designed for comfortable
reading on screen. Droid Serif is slightly condensed to maximize the amount of
text displayed on small screens. Vertical stress and open forms contribute to
its readability while its proportion and overall design complement its
companion Droid Sans.

%_font_pkg -n serif -f ??-%{fontname}-serif.conf DroidSerif*ttf


%prep
%setup -q -c -T
install -m 0644 -p %{SOURCE20} notice.txt
install -m 0644 -p %{SOURCE21} readme.txt


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}

install -m 0644 -p  %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} \
                    %{SOURCE6} %{SOURCE7} %{SOURCE8} %{SOURCE9} \
                    %{SOURCE10} %{SOURCE11} %{SOURCE12} \
                    %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE31} \
        %{buildroot}%{_fontconfig_templatedir}/65-%{fontname}-sans.conf
install -m 0644 -p %{SOURCE32} \
        %{buildroot}%{_fontconfig_templatedir}/60-%{fontname}-sans-mono.conf
install -m 0644 -p %{SOURCE33} \
        %{buildroot}%{_fontconfig_templatedir}/59-%{fontname}-serif.conf

for fontconf in 65-%{fontname}-sans.conf \
                60-%{fontname}-sans-mono.conf \
                59-%{fontname}-serif.conf ; do
  ln -s %{_fontconfig_templatedir}/$fontconf \
        %{buildroot}%{_fontconfig_confdir}/$fontconf
done


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc *.txt


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100917-4m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100917-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100917-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20100917-1m)
- update 20100917

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090906-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090906-1m)
- sync with Fedora 13 (20090906-5)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.112-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.112-4m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.112-3m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.112-2m)
- move google-droid-sans.conf from 65 to 66 for Momonga's fontconfig

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.112-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.112-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0.112-5
— prepare for F11 mass rebuild, new rpm and new fontpackages

* Sat Jan 31 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0.112-4
⬨ fix-up fontconfig installation for sans and mono

* Fri Jan 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0.112-3
⁉ Workaround RHEL5 rpmbuild UTF-8 handling bug
- 1.0.112-2
⁍ Convert to new naming guidelines
⁍ Do strange stuff with Sans Fallback (CJK users please check)

* Tue Dec  9 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0.112-1
փ Licensing bit clarified in bug #472635
շ Fedora submission

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0.107-1
Ϫ Initial built using “fontpackages”

