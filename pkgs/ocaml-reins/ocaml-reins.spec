%global momorel 7
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-reins
Version:        0.1a
Release:        %{momorel}m%{?dist}
Summary:        Library of OCaml persistent data structures

Group:          Development/Libraries
License:        "LGPLv2 with exceptions"
URL:            http://ocaml-reins.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/ocaml-reins/ocaml-reins-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:         reins-destdir-debian.patch
Patch1:         reins-install-cmi-debian.patch

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-omake
BuildRequires:  ocaml-ounit-devel >= 1.1.0-1m


%description
Reins is a collection of persistent data structures for OCaml.

In addition to providing a large collection of data structures, the
O'Caml Reins project also includes several features that I hope will
make developing O'Caml applications easier such as a random testing
framework and a collection of "standard" modules.

Current features:

* List data types:
   o Single linked lists (compatible with the standard library type)
   o O(1) catenable lists
   o Acyclic double linked lists
   o Random access lists with O(1) hd/cons/tl and O(log i) lookup/update
     for i'th element
* Double ended queues
* Sets/Maps:
   o AVL
   o Red/Black
   o Big-endian Patricia
   o Splay
* Heaps:
   o Binomial
   o Skew Binomial
* Zipper style cursor interfaces
* Persistent, bi-directional cursor based iterators (currently only
  for lists and sets)
* All standard types hoisted into the module level (Int, Bool, etc...)
* A collection of functor combinators to minimize boilerplate (e.g.,
  constructing compare or to_string functions)
* Quickcheck testing framework
   o Each structure provides a gen function that can generate a random
     instance of itself
* Completely safe code. No -unsafe or references to Obj.*
* Consistent function signatures. For instance, all fold functions take
  the accumulator in the same position.
* All operations use no more than O(log n) stack space (except for a
  few operations on splay trees which currently have O(log n) expected
  time, but O(n) worst case)


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q
%patch0 -p1
%patch1 -p1


%build
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
omake --config
omake doc


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
omake install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING LGPL-2.1
%{_libdir}/ocaml/reins
%if %opt
%exclude %{_libdir}/ocaml/reins/*.a
%exclude %{_libdir}/ocaml/reins/*.cmxa
%exclude %{_libdir}/ocaml/reins/*.cmx
%endif
%exclude %{_libdir}/ocaml/reins/*.mli


%files devel
%defattr(-,root,root,-)
%doc AUTHORS COPYING LGPL-2.1 doc/html
%if %opt
%{_libdir}/ocaml/reins/*.a
%{_libdir}/ocaml/reins/*.cmxa
%{_libdir}/ocaml/reins/*.cmx
%endif
%{_libdir}/ocaml/reins/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1a-7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1a-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1a-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1a-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1a-3m)
- rebuild against ocaml-3.11.2
- rebuild against ocaml-ounit-1.0.3-4m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1a-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1a-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1a-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Nov 26 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1a-3
- Rebuild for OCaml 3.11.0+rc1.

* Wed Sep  3 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1a-2
- Use normal versioning scheme.

* Sat Aug 30 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1-0.1.a
- Initial RPM release.
