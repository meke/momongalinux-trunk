%global momorel 1
%global src_date    20120731
%global htdocs_date 20120731

Summary:	A PostScript font editor
Name:		fontforge
License:	Modified BSD
Group:		Applications/Publishing
Version:	%{src_date}
Release:	%{momorel}m%{?dist}
URL:		http://fontforge.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/fontforge/fontforge_full-%{src_date}-b.tar.bz2
Source1:	http://dl.sourceforge.net/sourceforge/fontforge/fontforge_htdocs-%{htdocs_date}-b.tar.bz2
Source2:	http://khdd.net/fontforge-jman.tar.gz
NoSource:	0
NoSource:	1
Patch1:         fontforge-20090224-pythondl.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXi-devel
BuildRequires:	libjpeg-devel, libtiff-devel, libpng-devel
BuildRequires:	freetype-devel >= 2.3.1-2m
BuildRequires:	python >= 2.7
Provides:	%{_bindir}/fontforge
Obsoletes:	pfaedit

%description
Fontforge allows you to edit outline and bitmap fonts.  You can create
new ones or modify old ones.  It is also a font format converter and
can convert among PostScript (ASCII & binary Type 1, some Type 3s,
some Type 0s), TrueType, OpenType (Type2), CID-keyed and SVG fonts.

%prep
%setup -q -T -b 0 -n %{name}-%{src_date}-b
%{__mkdir} -m 755 docs
%{__tar} zxf %{S:1} -C docs/
%{__tar} zxf %{S:2} -C docs/

%patch1 -p1

%build
cd docs/fontforge-jman
perl -pi -e 's|<IMG SRC=\"|<IMG SRC=\"../|g' *.html
cd -

libtoolize -c -f
aclocal
autoconf
%configure --without-freetype-src --with-multilayer --with-devicetables
%make

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}{%{_bindir},%{_libdir},%{_mandir}/man1,%{_datadir}/%{name}}
%makeinstall

find %{buildroot} -name "*.la" -delete

%post
ldconfig

%postun
ldconfig

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS INSTALL LICENSE README* docs
%{_bindir}/*
%{_includedir}/%{name}
%{_libdir}/lib*
%{_libdir}/pkgconfig/*.pc
%{_datadir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/*
%{_mandir}/man1/*.1*

%changelog
* Sat Aug 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120731-1m)
- update to 20120731-b (src)
- update to 20120731-b (doc)

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110222-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110222-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20110222-1m)
- update to 20110222 (src)
- update to 20110221 (doc)
- import patch1 and patch2 from Fedora devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090923-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090923-6m)
- full rebuild for mo7 release

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090923-5m)
- roll back to 20090923

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20100501-1m)
- update to 20100501 (src)

* Fri Apr 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20100429-1m)
- update to 20100429 (src)
- update to 20100429 (doc)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (20090923-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090923-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090923-2m)
- apply glibc210 patch

* Sat Sep 26 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (20090923-1m)
- update to 20090923 (src)

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090914-1m)
- update to 20090914 (src/doc)

* Fri Jun 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090622-1m)
- update to 20090622 (src)
- update to 20090622 (doc)

* Mon Apr 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090408-1m)
- update to 20090408 (src)
- update to 20090408 (doc)

* Sat Feb 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090224-1m)
- update to 20090224 (src)
- update to 20090224 (doc)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081224-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20081224-2m)
- rebuild against python-2.6.1-2m

* Sun Dec 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20081224-1m)
- update to 20081224 (src)
- update to 20081224 (doc)

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20081215-1m)
- update to 20081215 (src)
- update to 20081215 (doc)

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081115-2m)
- no NoSource: 2
-- can not connect to khdd.net

* Thu Nov 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20081115-1m)
- update to 20081115 (src)
- update to 20081115 (doc)

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080927-1m)
- update to 20080927 (src)
- update to 20080927 (doc)

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080824-1m)
- update to 20080824 (src)
- update to 20080824 (doc)

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080720-1m)
- update to 20080720 (src)
- update to 20080720 (doc)

* Sat Jun 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080607-1m)
- update to 20080607 (src)
- update to 20080607 (doc)

* Tue May  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080429-1m)
- update to 20080429 (src)
- update to 20080429 (doc)

* Sat Apr 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080330-1m)
- update to 20080330 (src)
- update to 20080330 (doc)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20080309-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20080309-1m)
- fix segfaults while compiling lilypond and wine
- update to 20080309 (src)

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20080302-1m)
- update to 20080302 (src)
- update to 20080302 (doc)

* Fri Jan 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080110-1m)
- update to 20080110 (src)
- update to 20080109 (doc)

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20071210-1m)
- update to 20071210 (src)
- update to 20071211 (doc)

* Wed Nov 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20071110-1m)
- update to 20071110 (src)
- update to 20071109 (doc)

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20071002-1m)
- update to 20071002 (src)
- update to 20071002 (doc)

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20070915-1m)
- update to 20070915 (src)
- update to 20070915 (doc)
- do not use %%NoSource macro

* Tue Aug 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20070808-1m)
- update to 20070808 (src)
- update to 20070808 (doc)

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20070723-1m)
- update to 20070723 (src)
- update to 20070723 (doc)

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20070501-1m)
- update to 20070511 (src)
- update to 20070501 (doc)

* Sat Apr  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20070312-1m)
- update to 20070312 (src)
- update to 20070313 (doc)

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20061220-3m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (20061220-2m)
- delete libtool library

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (20061220-1m)
- update to 20061220 (both of src and doc)

* Sat Oct 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (20061025-1m)
- update to 20061025 (src) and 20061014 (doc)

* Sun Sep  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (20060822-1m)
- update to 20060822

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (20060703-1m)
- update to 20060703

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (20060408-2m)
- delete duplicated dir

* Thu Apr 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (20060408-1m)
- update to 20060408

* Sat Feb 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (20060125-1m )
- update to 20060125

* Mon Dec 26 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (20051205-3m)
- add more configure options.

* Fri Dec  9 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (20051205-2m)
- revise image path of japanese manual.

* Fri Dec  9 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (20051205-1m)
- add japanese manual.

* Thu Feb 24 2005 Kazuhiko <kazuhiko@fdiary.net>
- (20050209-1m)
- PfaEdit has changed its name to FontForge
- remove mplus.patch

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (040130-1m)

* Mon Jan 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (040111-1m)

* Thu Dec 18 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (031210-2m)
- add patch0 from http://mplus-fonts.sourceforge.jp/mplus-outline-fonts/

* Tue Dec 16 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (031210-1m)

* Fri Nov  7 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (031029-1m)

* Sat Oct 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (031023-1m)

* Sun Oct  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (031002-1m)

* Sat Jun 28 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (030619-2m)
- now our XFree86 has Xutf8* functions. so removed a patch
- related Xutf8*

* Fri Jun 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (030619-1m)

* Wed Jun  4 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (030518-1m)

* Sat Jan 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (030109-1m)

* Thu Dec 26 2002 Kazuhiko <kazuhiko@fdiary.net>
- (021223-1m)
- remove 'pfaedit-0x2141_is_wavedash.patch' because it was merged

* Fri Dec 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (021211-1m)

* Mon Dec  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (021208-1m)
- docs-021208

* Tue Nov 26 2002 Kazuhiko <kazuhiko@fdiary.net>
- (021125-1m)
- version up

* Sat Nov 16 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (021113-1m)
- version up.

* Mon Oct 28 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (021026-1m)
- ver up.

* Wed Oct 23 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (021021-2m)
- ver up.
- modified patch0
- add patch1

* Sat Oct 19 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (021018-1m)
- ver up.

* Fri Oct 18 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (020910-1m)
- version down...(T_T)
  ("ver. 020911" had some bugs...it was unstable.)

* Tue Oct 15 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (020911-1m)
- first import for momonga project.
