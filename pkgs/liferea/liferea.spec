%global momorel 9

Name:           liferea
Version:        1.6.4
Release: 	%{momorel}m%{?dist}
Summary:        An RSS/RDF feed reader

Group:          Applications/Internet
License:        GPLv2+
URL:            http://liferea.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/liferea/liferea-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0:		liferea-1.6.4-libnotify-0.7.2.patch

BuildRequires:  GConf2-devel
BuildRequires:  NetworkManager-glib-devel >= 0.8.999
BuildRequires:  atk-devel
BuildRequires:  cairo-devel
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
BuildRequires:  glib2-devel
BuildRequires:  glibc-devel
BuildRequires:  gtk2-devel
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  libX11-devel
BuildRequires:  libXcomposite-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXdamage-devel
BuildRequires:  libXext-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXi-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXrender-devel
BuildRequires:  libglade2-devel
BuildRequires:  libnotify-devel >= 0.7.2
BuildRequires:  libpng-devel
BuildRequires:  libsoup-devel
BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel >= 1.1.19
BuildRequires:  lua-devel
BuildRequires:  pango-devel
BuildRequires:  sqlite-devel
BuildRequires:  webkitgtk-devel >= 1.3.4
BuildRequires:  zlib-devel

BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  intltool >= 0.35.0
BuildRequires:	perl-XML-Parser

Requires: 	sqlite

Requires(pre): 	GConf2
Requires(post): GConf2
Requires(preun): GConf2

Obsoletes: liferea-WebKit

%description
Liferea (Linux Feed Reader) is an RSS/RDF feed reader. 
It's intended to be a clone of the Windows-only FeedReader. 
It can be used to maintain a list of subscribed feeds, 
browse through their items, and show their contents.

%prep
%setup -q
%patch0 -p1 -b .libnotify

%build
autoreconf -fi
%configure --enable-nm --enable-libnotify --enable-lua LIBS="-lX11 -lICE"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
# Remove *.in files.
find %{buildroot} -name '*.in' -exec rm -f {} ';'

desktop-file-install --vendor= --delete-original	\
  --dir %{buildroot}%{_datadir}/applications		\
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf %{buildroot}

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/%{name}.schemas >/dev/null || :
fi

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/%{name}.schemas > /dev/null || :
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/%{name}.schemas > /dev/null || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING AUTHORS README ChangeLog
%{_mandir}/man1/%{name}.1*
%{_mandir}/pl/man1/%{name}.1*
%{_sysconfdir}/gconf/schemas/%{name}.schemas
%{_bindir}/%{name}
%{_bindir}/%{name}-add-feed
%{_datadir}/%{name}/
%{_libdir}/liferea/*
%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
%{_datadir}/icons/hicolor/24x24/apps/%{name}.png
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%dir %{_libdir}/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-9m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.4-8m)
- build fix

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.4-7m)
- rebuild against NetworkManager-0.8.999

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.4-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-4m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.4-3m)
- rebuild against webkitgtk-1.3.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-2m)
- fix build

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3
- BuildRequires: NetworkManager-glib-devel >= 0.8

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2
- disable xulrunner support

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.28-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.28-2m)
- rebuild against xulrunner-1.9.1

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.28-1m)
- update to 1.4.28 based on Fedora 11 (1.4.26-2)

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-7m)
- rebuild against WebKit-1.1.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-6m)
- rebuild against rpm-4.6

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-5m)
- rebuild against gnutls-2.4.1

* Wed Jul  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.13-4m)
- rebuild to resolve dependency

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.13-3m)
- revise spec

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.13-2m)
- remove vendor="fedoea" from desktop file

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-1m)
- import from Fedora to Momonga

* Tue May 06 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.4.13-3
- Bump release to fix upgrade path (again).

* Mon Apr 07 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.4.13-2
- Rebuild for N-E-V-R issues.

* Mon Mar 17 2008 Marc Wiriadisastra <marc@mwiriadi.id.au> - 1.4.13-1
- Updated to latest stable version

* Sat Feb 23 2008 Marc Wiriadisastra <marc@mwiriadi.id.au> - 1.4.12-2
- Fixed fedora feed for fedora weekly news

* Wed Feb 20 2008 Marc Wiriadisastra <marc@mwiriadi.id.au> - 1.4.12-1
- new version
- builds with gcc4.3
- added firefox-devel and xulrunner-devel for different fedora's

* Fri Feb  8 2008 Christopher Aillon <caillon@redhat.com> - 1.4.11-2
- Rebuild against newer gecko

* Thu Jan 17 2008 Brian Pepple <bpepple@fedoraproject.org> - 1.4.11-1
- Update to 1.4.11. release fixes news bin crasher. (#429021)

* Wed Dec 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.4.10-1
- Update to 1.4.10.
- Update feed patch.

* Sun Dec  2 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.4.9-1
- Update to 1.4.9.
- Update feed patch.

* Tue Nov 27 2007 Christopher Aillon <caillon@redhat.com> - 1.4.8-2
- Rebuild against newer gecko

* Thu Nov 22 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.4.8-1
- Update to 1.4.8.
- fixes LD_LIBRARY_PATH security bug. CVE-2006-4791

* Thu Nov 15 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.4.7-1
- Update to 1.4.7.
- Drop opml & nm patches. fixed upstream.
- Update fedora feed patch for 1.4.x.
- add BR on sqlite-devel, dbus-devel, dbus-glib-devel, libglade2-devel.
- Don't build gtkhtml2 plugin for now.

* Tue Nov  6 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.23-6
- Rebuild for new gecko libs.

* Wed Oct 31 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.23-5
- Add patch to fix opml security bug: CVE-2007-5751. (#360641)

* Wed Oct 24 2007 Jeremy Katz <katzj@redhat.com> - 1.2.23-4
- Fix build against new NetworkManager

* Tue Oct 23 2007 Jeremy Katz <katzj@redhat.com> - 1.2.23-3
- Don't build against NetworkManager as the nm-glib api seems to have changed

* Tue Oct 23 2007 Jeremy Katz <katzj@redhat.com> - 1.2.23-2
- Rebuild against new firefox

* Sat Sep  8 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.23-1
- Update to 1.2.23.

* Tue Aug 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.22-1
- Update to 1.2.22.

* Thu Aug  9 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.21-2
- Rebuild against new gecko.

* Tue Aug  7 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.21-1
- Update to 1.2.21.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.20-3
- Update license tag.

* Wed Jul 18 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.20-2
- Rebuild for new gecko-libs 1.8.1.5.

* Wed Jul 11 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.20-1
- Update to 1.2.20.

* Tue Jul  3 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.19-2
- Bump.

* Mon Jul  2 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.19-1
- Update to 1.2.19.

* Sun Jul  1 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.18-1
- Update to 1.2.18.

* Sat Jun 16 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.17-1
- Update to 1.2.17.

* Tue Jun  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.16b-1
- Update to 1.2.16b.

* Mon Jun  4 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.16-1
- Update to 1.2.16.

* Fri Jun  1 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.15b-2
- Rebuild for new gecko release.

* Fri May 18 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.15b-1
- Update to 1.2.15b.
- Drop cpu timer patch, fixed upstream.

* Sat May 12 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.10c-2
- Add patch to fix cpu from waking up frequently. (#239945)

* Thu Apr  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.10c-1
- Update to 1.2.10c.
- Update feed patch.

* Thu Mar 29 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.10b-1
- Update to 1.2.10b.

* Wed Mar 28 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.10-2
- Bump.

* Wed Mar 28 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.10-1
- Update to 1.2.10.

* Sat Mar 24 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.9-1
- Update to 1.2.9.

* Wed Mar 14 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.8-1
- Update to 1.2.8.

* Sun Mar  4 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.7-3
- Rebuild against firefox-2.0.0.1.

* Tue Feb 27 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.7-2
- Rebuild against new firefox.

* Wed Feb 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.7-1
- Update to 1.2.7.

* Sun Feb 11 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.6b-1
- Update to 1.2.6b.

* Thu Feb  8 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.6-1
- Update to 1.2.6.

* Mon Feb  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.5-1
- Update to 1.2.5.

* Sun Jan 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.4-1
- Update to 1.2.4.

* Wed Jan 17 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.3-2
- Add min. version of libxslt required.

* Wed Jan 10 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.3-1
- Update to 1.2.3.

* Thu Jan  4 2007 Brian Pepple <bpepple@fedoraproject.org> - 1.2.2-1
- Update to 1.2.2.
- Remove *.in files that shouldn't be installed.

* Sun Dec 24 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.2.1-1
- Update to 1.2.1.

* Sat Dec 23 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.2.0-2
- Rebuild against new firefox.

* Sun Dec 17 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.2.0-1
- Update to 1.2.0.
- Add scriptlets for gtk+ icon cache.
- Add BR for gnutls-devel, libnotify-devel, & NetworkManager-glib-devel.
- Add BR for perl(XML::Parser) & libxslt-devel.
- Drop BR on dbus-devel, since libnotify-devel will pull this in.
- Drop fonts patch, fixed upstream.

* Thu Dec  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.27-1
- Update to 1.0.27.

* Tue Nov 21 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.26-1
- Update to 1.0.26.
- Add patch to use document font name. (#216813)

* Sun Oct 29 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.25-3
- Actually use the correct version for firefox.

* Sun Oct 29 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.25-2
- Rebuild for new firefox.

* Fri Oct 27 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.25-1
- Update to 1.0.25.
- Drop X-Fedora category from desktop file.

* Thu Oct 19 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.24-1
- Update to 1.0.24.

* Thu Oct 12 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.23-3
- Add patch to add Fedora default feeds. (#209301)

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 1.0.23-2
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 18 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.23-1
- Update to 1.0.23.

* Fri Sep 15 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.22-2
- Bump firefox version to 1.5.0.7.

* Thu Aug 31 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.22-1
- Update to 1.0.22.

* Thu Aug 10 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.21-3
- Update to 1.0.21.
- Bump firefox version to 1.5.0.6.

* Mon Aug  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.20-4
- Add requires on specified version of firefox.

* Mon Aug  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.20-3
- Update to 1.0.20. (#199222)

* Mon Jul 31 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.19-3
- Update to 1.0.19.
- Build against firefox instead of mozilla. (#145752)

* Fri Jul 28 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.18-3
- Update to 1.0.18.

* Sat Jul 22 2006 Michael Schwendt <mschwendt[AT]users.sf.net> - 1.0.16-4
- Rebuild because of dbus soname change.

* Sun Jun 25 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.16-3
- Update to 1.0.16.

* Sun Jun 18 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.0.15-3
- Update to 1.0.15.
- Drop doc patch.

* Mon May 29 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.14-3
- Update to 1.0.14.
- Add patch to fix doc build.
- Drop NEWS, since it doesn't contain any useful info.

* Wed May  3 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.11-3
- Update to 1.0.11.

* Sat Apr 22 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.10-3
- Update to 1.0.10,
- Delete origianl desktop file with desktop-file-install call.
- Remove *.la, instead of excluding.

* Wed Apr  5 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.9-3
- Update to 1.0.9.

* Sat Mar 18 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.8-3
- Update to 1.0.8.
- Drop mozilla-lib64 patch, fixed upstream.

* Tue Mar 14 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.7-3
- Add patch to find mozilla on x86_64 (#185243).

* Mon Mar  6 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.7-2
- Update to 1.0.7.
- Fix gconf scriptlets.

* Sat Feb 25 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.6-2
- Update to 1.0.6.

* Thu Feb 16 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.4-4
- Remove unnecessary BR (zlib-devel & libxml2-devel).

* Mon Feb 13 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.4-3
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Feb 10 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.4-2
- Update to 1.0.4.

* Tue Jan 31 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.3-2
- Update to 1.0.3.

* Wed Jan 25 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.2-2
- Update to 1.0.2.

* Mon Jan 16 2006 Brian Pepple <bdpepple@ameritech.net> - 1.0.1-1
- Update to 1.0.1.

* Mon Dec 26 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-4
- Dropp BR for libXdmcp-devel & libXau-devel, replace w/ libX11-devel (#176313).

* Sun Dec 25 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-3
- Add BR for libXdmcp-devel,libXau-devel, & libSM-devel.

* Fri Dec 23 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-2
- Update to 1.0.

* Sun Dec 04 2005 Luke Macken <lmacken@redhat.com> - 1.0-0.3.rc4
- Rebuild against dbus 0.60

* Fri Nov 18 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-0.2.rc4
- Update tp 1.0-RC4.

* Fri Nov  4 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-0.2.rc3
- Update to 1.0-RC3.

* Mon Oct 10 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-0.2.rc2
- Update to 1.0-RC2.

* Tue Oct  4 2005 Brian Pepple <bdpepple@ameritech.net> - 1.0-0.2.rc1
- Update to 1.0-RC1.

* Sun Sep  4 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.7b-2
- Update to 0.9.7b.

* Tue Aug 30 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.7a-2
- Update to 0.9.7a.

* Thu Aug 18 2005 Jeremy Katz <katzj@redhat.com> - 0.9.6-3
- rebuild for devel changes

* Mon Aug 15 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.6-2
- Update to 0.9.6.

* Sun Jul 31 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.5-2
- Update to 0.9.5.

* Fri Jul 22 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.4-2
- Update to 0.9.4.

* Thu Jul  7 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.3-1
- Update to 0.9.3.
- Enable dbus.
- Add dist tag.

* Thu May 12 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.2-1
- Update to 0.9.2
- Don't enable d-bus since old API is used.

* Sat Mar 12 2005 Brian Pepple <bdpepple@ameritech.net> - 0.9.1-1
- Updated to 0.9.1.
- Drop epoch: 0.

* Tue Jan 18 2005 Brian Pepple <bdpepple@gmail.com> - 0:0.9.0-0.1.b
- Updated to 0.9.0b.
- Remove seperate desktop file.

* Fri Jan 14 2005 Brian Pepple <bdpepple@gmail.com> - 0:0.9.0-0.fdr.1
- Updated to 0.9.0.

* Wed Jan 12 2005 Brian Pepple <bdpepple@gmail.com> - 0:0.6.4-0.fdr.1.b
- Updated to 0.6.4b.

* Mon Nov  1 2004 Brian Pepple <bdpepple@ameritech.net> - 0:0.6.1-0.fdr.1
- Updated to 0.6.1.
- Added %exclude *.la
- Removed liferea-0.5.3c-fixes.patch, not needed anymore.
- Removed liferea-0.5.3b-mozillahome.patch, not needed anymore.

* Fri Sep 24 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.5.3-0.fdr.4.c
- Sync with Mozilla 1.7.3 update for FC2.

* Fri Sep 10 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.5.3-0.fdr.3.c
- Add memory leak fix + updates from 0.5.3c
  (tarball would require autoreconf).

* Fri Aug 27 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.5.3-0.fdr.2.b
- Add BR mozilla-devel to build Mozilla plugin.
- Patch liferea script to support FC1/FC2 Mozilla homes.

* Sat Aug 21 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.5.3-0.fdr.1.b
- Update to 0.5.3b (bug-fixes).
- Remove version from libxml2 requirement (2.4.1 would be sufficient).

* Thu Aug 19 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.5.3-0.fdr.1
- Update to 0.5.3.
- Add schemas un-/install in preun/post.
- Update desktop file.

* Wed Aug  4 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.5.2-0.fdr.1.c
- Add BR zlib-devel.
- Update desktop file.
- Adhere to versioning guidelines.

* Wed Aug 04 2004 Daryll Strauss <daryll@daryll.net> - 0:0.5.2c-0.fdr.1
- Updated to 0.5.2c

* Sat May 22 2004 Brian Pepple <bdpepple@ameritech.net> - 0:0.4.9-0.fdr.1
- Updated to 0.4.9
- Removed liferea_browser.patch.  Shouldn't be needed anymore.

* Thu May  6 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.8-0.fdr.1
- Updated to 0.4.8.
- Modified liferea_browser.patch to work with new version.
- Removed liferea.png that fedora.us provided, since package now supplies it's own.

* Fri Apr 30 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.5.d
- Applied browser.patch from Michael Schwendt (#1478)

* Wed Apr 28 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.4.d
- Updated to 0.4.7d.
- Removed patch, since it didn't fix browser problem.

* Fri Apr 23 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.4.c
- Add patch to use 'gnome-open %s' instead of 'mozilla %s'.

* Tue Apr 20 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.3.c
- Updated to 0.4.7c

* Sun Apr 11 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.3.b
- Updated to 0.4.7b
- Removed %post/postun [#1478]

* Sun Apr 11 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.2
- Added liferea-bin to files.

* Fri Apr  2 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.7-0.fdr.1
- Updated to 0.4.7.
- Added %post & %postun
- Added %{_libdir} files

* Tue Mar 30 2004 Brian Pepple <bdpepple@ameritech.net> 0:0.4.6-0.fdr.1.e
- Updated to 0.4.6e
- Added gettext build requirement

* Sun Jan 11 2004 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.4.6-0.fdr.1.b
- Updated to 0.4.6b.

* Sun Nov 16 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.4.4-0.fdr.2
- BuildReq desktop-file-utils.

* Mon Nov 10 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.4.4-0.fdr.1
- Updated to 0.4.4.

* Thu Oct 16 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.4.3-0.fdr.1
- Updated to 0.4.3.

* Mon Oct 13 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.4.1-0.fdr.1
- Updated to 0.4.1.

* Fri Oct 10 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.4.0-0.fdr.1
- Updated to 0.4.0.

* Wed Oct 01 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.3.8-0.fdr.1
- Updated to 0.3.8.

* Tue Sep 23 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.3.7-0.fdr.3
- Correction in %%defattr.
- Improved Summary.
- Source now direct downloadable.

* Sun Sep 21 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.3.7-0.fdr.2
- epoch added to versioned req.

* Thu Sep 18 2003 Phillip Compton <pcompton[AT]proteinmedia.com> 0:0.3.7-0.fdr.1
- Initial Fedora Release.
