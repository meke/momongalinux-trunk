%global    momorel 1
Summary:   GLib wrapper around libusb1
Name:      libgusb
Version:   0.1.6
Release:   %{momorel}m%{?dist}
License:   LGPLv2+
Group: 	   System Environment/Libraries
URL:       https://gitorious.org/gusb/
Source0:   http://people.freedesktop.org/~hughsient/releases/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires: glib2-devel >= 2.16.1
BuildRequires: libtool
BuildRequires: libgudev1-devel
BuildRequires: libusb1-devel

%description
GUsb is a GObject wrapper for libusb1 that makes it easy to do
asynchronous control, bulk and interrupt transfers with proper
cancellation and integration into a mainloop.

%package devel
Summary: Libraries and headers for gusb
Requires: %{name} = %{version}-%{release}

%description devel
GLib headers and libraries for gusb.

%prep
%setup -q

%build
%configure \
        --disable-static \
        --disable-dependency-tracking

%make 

%install
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/libgusb.la

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README AUTHORS NEWS COPYING
%{_libdir}/libgusb.so.?
%{_libdir}/libgusb.so.?.0.*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%defattr(-,root,root,-)
%{_includedir}/gusb-1
%{_libdir}/libgusb.so
%{_libdir}/pkgconfig/gusb.pc
%{_datadir}/gtk-doc/html/gusb
%{_datadir}/gir-1.0/*.gir
%{_datadir}/vala/vapi/*.vapi

%changelog
* Thu May 16 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.6-1m)
- update 0.1.6

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-1m)
- import from fedora

