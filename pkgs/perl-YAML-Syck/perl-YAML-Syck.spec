%global         momorel 4

Name:           perl-YAML-Syck
Version:        1.27
Release:        %{momorel}m%{?dist}
Summary:        Fast, lightweight YAML loader and dumper
License:        MIT
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/YAML-Syck/
Source0:        http://www.cpan.org/authors/id/T/TO/TODDR/YAML-Syck-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.6.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Scalar-Util
Requires:       perl-Scalar-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides a Perl interface to the libsyck data serialization
library. It exports the Dump and Load functions for converting Perl data
structures to YAML strings, and the other way around.

%prep
%setup -q -n YAML-Syck-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COMPATIBILITY COPYING README
%{perl_vendorarch}/auto/YAML/Syck
%{perl_vendorarch}/JSON/Syck.pm
%{perl_vendorarch}/YAML/Dumper
%{perl_vendorarch}/YAML/Loader
%{perl_vendorarch}/YAML/Loader
%{perl_vendorarch}/YAML/Syck.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-2m)
- rebuild against perl-5.16.3

* Mon Mar 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Wed Dec  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-2m)
- rebuild against perl-5.16.2

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Sun Nov  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-2m)
- rebuild against perl-5.12.2

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Mon Jun  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sun May 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Mon May 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08
- Specfile re-generated by cpanspec 1.78.

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.07-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-2m)
- rebuild against perl-5.10.1

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-2m)
- rebuild against rpm-4.6

* Mon Jun  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.04-2m)
- rebuild against gcc43

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Sat Jan 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Mon Dec 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Tue Oct 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Mon Sep  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97
- do not use %%NoSource macro

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Sat Aug  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Tue Jul 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.85-2m)
- use vendor

* Sun Apr 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Mon Apr  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Sun Jan 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Fri Jan 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Sat Sep 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.67-2m)
- BuildRequire perl-YAML

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.67-1m)
- spec file was autogenerated
