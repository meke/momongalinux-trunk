%global momorel 8

Name: gst-plugins-farsight
Version: 0.12.11
Release: %{momorel}m%{?dist}
Summary: Gstreamer plugins for farsight
URL: http://farsight.freedesktop.org/wiki/
Group: System Environment/Base
License: GPLv2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://www.freedesktop.org/software/farstream/releases/obsolete/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires: pkgconfig
BuildRequires: gstreamer-devel
BuildRequires: GConf2-devel

%description
This is a set of plugins for Gstreamer that will be used by Farsight for
Audio/Video conferencing.

%prep
%setup -q

%build
%configure --disable-jrtplib --disable-mimic --disable-jingle-p2p CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README
%{_libdir}/gstreamer-0.10/*.so
%{_libdir}/gstreamer-0.10/*.la

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.11-8m)
- change Source0 URI, but obsoleted by upstream...

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.11-7m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.11-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.11-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.11-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.11-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.11-1m)
- update to 0.12.11

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.10-1m)
- initial build
