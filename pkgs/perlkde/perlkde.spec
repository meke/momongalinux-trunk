%global momorel 2
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global akonadiver 1.12.1
%global qimageblitzver 0.0.6
%global sopranover 2.9.4
%global qscintillaver 2.8
%global phononver 4.7.1
%global smokekderel 1m
%global perlver 5.18.1

Name: perlkde
Summary: Perl KDE 4 bindings
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: LGPLv2
URL: https://projects.kde.org/projects/kde/kdebindings/perl/perlkde
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource :0
Patch0: %{name}-4.12.97-CMakeLists.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: perlqt-devel >= %{version}
BuildRequires: perl-devel >= %{perlver}
BuildRequires: smokegen-devel >= %{version}
BuildRequires: smokeqt-devel >= %{version}
BuildRequires: smokekde-devel >= %{version}-%{smokekderel}
Requires: perlqt >= %{version}
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Conflicts: kdebindings < 4.7.0

%description
Perl KDE 4 bindings.

%prep
%setup -q
%patch0 -p1 -b .revert

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
   -DCUSTOM_PERL_SITE_ARCH_DIR:STRING=%{perl_vendorarch} \
   ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_kde4_libdir}/kde4/kperlpluginfactory.so
%{perl_vendorarch}/*.pm
%{perl_vendorarch}/auto/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-2m)
- rebuild against perl-5.20.0

* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-2m)
- rebuild against perl-5.18.2

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-2m)
- rebuild against perl-5.18.1

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-2m)
- rebuild against perl-5.18.0

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-2m)
- rebuild against perl-5.16.3

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-2m)
- rebuild against perl-5.16.2

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-2m)
- rebuild against perl-5.16.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-2m)
- rebuild against perl-5.16.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-2m)
- rebuild against perl-5.14.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.0-2m)
- build with smokekde-4.7.0-2m

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- initial build for Momonga Linux
