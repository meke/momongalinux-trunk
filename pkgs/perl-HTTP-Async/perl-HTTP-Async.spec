%global         momorel 1

Name:           perl-HTTP-Async
Version:        0.26
Release:        %{momorel}m%{?dist}
Summary:        Process multiple HTTP requests in parallel without blocking
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTTP-Async/
Source0:        http://www.cpan.org/authors/id/K/KA/KAORU/HTTP-Async-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTTP-Message
BuildRequires:  perl-HTTP-Server-Simple
BuildRequires:  perl-IO
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Net-HTTP
BuildRequires:  perl-Test-HTTP-Server-Simple
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Time-HiRes
BuildRequires:  perl-URI
Requires:       perl-Data-Dumper
Requires:       perl-HTTP-Message
Requires:       perl-HTTP-Server-Simple
Requires:       perl-IO
Requires:       perl-libwww-perl
Requires:       perl-Net-HTTP
Requires:       perl-Test-HTTP-Server-Simple
Requires:       perl-Test-Simple
Requires:       perl-Time-HiRes
Requires:       perl-URI
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Although using the conventional LWP::UserAgent is fast and easy it does
have some drawbacks - the code execution blocks until the request has been
completed and it is only possible to process one request at a time.
HTTP::Async attempts to address these limitations.

%prep
%setup -q -n HTTP-Async-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README.md TODO
%{perl_vendorlib}/HTTP/Async*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- rebuild against perl-5.20.0
- update to 0.26

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.18.1

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Tue May 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.18.0

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.16.3

* Wed Nov 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.09-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09-2m)
- rebuild against gcc43

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
