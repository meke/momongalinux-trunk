%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0
%global minor 0.1

Name:           tumbler
Version:        0.1.30
Release:        %{momorel}m%{?dist}
Summary:        D-Bus service for applications to request thumbnails

License:        GPLv2+ and LGPLv2+
Group:          Applications/System
URL:            http://git.xfce.org/xfce/tumbler/
#VCS git:git://git.xfce.org/xfce/tumbler
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{minor}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRequires:  dbus-glib-devel
BuildRequires:  freetype-devel
BuildRequires:  gettext
BuildRequires:  gtk2-devel >= 2.10.0
BuildRequires:  intltool
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  poppler-glib-devel
# extra thumbnailers
BuildRequires:  gstreamer-devel
BuildRequires: libgsf-devel
BuildRequires: libopenraw-gnome-devel


%description
Tumbler is a D-Bus service for applications to request thumbnails for various
URI schemes and MIME types. It is an implementation of the thumbnail
management D-Bus specification described on
http://live.gnome.org/ThumbnailerSpec written in an object-oriented fashion

Additional thumbnailers can be found in the tumbler-extras package


%package devel
Summary:       Development files for %{name}
Group:         Development/Libraries
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description devel
This package contains libraries and header files for developing applications 
that use %{name}.

%prep
%setup -q


%build
%configure --disable-static

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

# Remove rpaths.
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags} V=1


%install
make install INSTALL="%{__install} -p" DESTDIR=%{buildroot}

find %{buildroot} -type f -name "*.la" -delete

%find_lang %{name}


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_datadir}/dbus-1/services/org.xfce.Tumbler.*.service
%{_libdir}/libtumbler-*.so.*
%{_sysconfdir}/xdg/tumbler/tumbler.rc
%dir %{_libdir}/%{name}-*
%{_libdir}/%{name}-*/tumblerd
%{_libdir}/tumbler-*/plugins/


%files devel
%defattr(-,root,root,-)
%{_libdir}/libtumbler-*.so
%{_libdir}/pkgconfig/%{name}-1.pc

%doc %{_datadir}/gtk-doc/

%dir %{_includedir}/%{name}-1
%{_includedir}/%{name}-1/tumbler

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.30-1m)
- update to 0.1.30

* Fri Aug 30 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.29-1m)
- update to 0.1.29

* Fri Feb 15 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.27-1m)
- update to 0.1.27

* Thu Sep  6 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.25-1m)
- import from fedora
