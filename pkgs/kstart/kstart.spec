%global momorel 1

Name: kstart
Version: 4.1
Release: %{momorel}m%{?dist}
Summary: Daemon version of kinit for Kerberos v5
License: MIT
Group: Applications/System
URL: http://www.eyrie.org/~eagle/software/kstart/
Source0: http://archives.eyrie.org/software/kerberos/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: krb5-devel

%description

k5start is a modified version of kinit which can use keytabs to authenticate, 
can run as a daemon and wake up periodically to refresh a ticket, and can run 
single commands with its own authentication credentials and refresh those 
credentials until the command exits. 

%prep
%setup -q

%build
%configure --disable-k4start --enable-setpag --enable-reduced-depends

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc NEWS README
%{_bindir}/k5start
%{_bindir}/krenew
%{_mandir}/man1/k5start.1*
%{_mandir}/man1/krenew.1*

%changelog
* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-1m)
- update to 4.1

* Sun Oct 30 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.16-1m)
- import from Fedora

* Tue Feb 08 2011 Simon Wilkinson <simon@sxw.org.uk> - 3.16
- Upgrade to 3.16
- Enable support for setpag

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.11-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.11-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Apr 13 2008 Simon Wilkinson <simon@sxw.org.uk> 3.11-1
- Update to 3.11
- Fix license tag, as per approval requirement

* Sat Jan 27 2007 Simon Wilkinson <simon@sxw.org.uk> 3.10-1
- Initial revision for Fedora

