%global momorel 6
%global with_evo 0
%global with_gda3 0

Summary: project managment tool for the Gnome
Name: planner
Version: 0.14.6
Release: %{momorel}m%{?dist}
License: GPLv2+ or LGPLv2+
Group: Applications/Engineering
URL: http://www.imendio.com/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.14/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: planner-0.14.4-gtk-doc-1.13.patch
Patch1: planner-0.14.4-automake-1.11.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: rarian
%if %{with_gda3}
BuildRequires: libgda3-devel
%else
BuildRequires: libgda-devel >= 1.2.3-33m
%endif
BuildRequires: glib2-devel >= 2.20.1
BuildRequires: gtk2-devel >= 2.16.1
BuildRequires: libgnomecanvas-devel >= 2.26.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: libgnomeprintui22-devel >= 2.18.4
BuildRequires: gnome-vfs2-devel >= 2.24.1
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: libxslt-devel >= 1.1.24
BuildRequires: pygobject2-devel
BuildRequires: pygtk2-devel >= 2.24
%if 0%{?with_evo}
BuildRequires: evolution-data-server-devel >= 3.5
BuildRequires: evolution-devel >= 3.5
%endif

%description
Planner is a project managment tool for the Gnome desktop, for planning, 
scheduling and tracking projects. 

Planner was originally developed and maintained by the guys at Imendio, but
is now maintained by the Planner community.  We wish to extend our thanks 
to Imendio for all their hard work and dedication to this an many other 
open source projects.  For more information on Imendio, please visit 
http://www.imendio.com

%package devel
Summary: Libraries and include files for developing with Eel.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package provides the necessary development libraries and include
files to allow you to develop with Eel.

%prep
%setup -q
#%%patch0 -p1 -b .gtk-doc
#%%patch1 -p1 -b .automake

%build
%if 0%{?with_evo}
%global evo_flags --enable-eds --enable-eds-backend
%else
%global evo_flags --disable-eds --disable-eds-backend
%endif

#gtkdocize --copy --docdir docs/libplanner
#autoreconf -vfi
%configure \
    --disable-schemas-install \
    --enable-gtk-doc \
    --enable-python \
    --enable-python-plugin \
    --enable-simple-priority-scheduling \
    %evo_flags \
    --disable-update-mimedb \
%if %{with_gda3}
    --with-database=gda3
%else
    --with-database=gda
%endif

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

# schemas
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/planner.schemas \
    > /dev/null ||:

# update mime database
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null

# update icon
gtk-update-icon-cache -q -t -f /usr/share/icons/hicolor

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/planner.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/planner.schemas \
	> /dev/null || :
fi

%postun
/sbin/ldconfig

# update mime database
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null

# update icon
gtk-update-icon-cache -q -f -t /usr/share/icons/hicolor

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README
%{_sysconfdir}/gconf/schemas/planner.schemas
%{_bindir}/planner
%{_libdir}/libplanner-1.so.*
%exclude %{_libdir}/libplanner-1.la
%{_libdir}/planner
%{_libdir}/python*/site-packages/*
%{_datadir}/applications/planner.desktop
%{_datadir}/doc/planner
%{_datadir}/gnome/help/planner
%{_datadir}/icons/hicolor/48x48/mimetypes/gnome-mime-application-x-planner.png
%{_datadir}/locale/*/*/*
%{_datadir}/man/man1/planner.1.*
%{_datadir}/mime/packages/planner.xml
%{_datadir}/omf/planner
%{_datadir}/pixmaps/gnome-planner.png
%{_datadir}/planner

%files devel
%defattr(-, root, root)
%{_includedir}/planner-1.0
%{_libdir}/libplanner-1.so
%{_libdir}/pkgconfig/libplanner-1.pc
%{_datadir}/gtk-doc/html/libplanner

%changelog
* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.6-6m)
- specify version and release of libgda

* Sat Jul 13 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6-5m)
- add workaround for BTS #487

* Wed Jul  3 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6-4m)
- switch to use libgda3-devel
- revise spec

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6-3m)
- rebuild for pygobject2-devel
- temporary disable evolution support

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6-2m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.6-1m)
- update to 0.14.6

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.5-3m)
- add BuildRequires: pygobject228-devel

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.5-2m)
- rebuild against evolution-data-server-3.1.91

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.5-1m)
- update to 0.14.5

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.4-13m)
- rebuild against evolution-data-server-3.0.1

* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.4-12m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.4-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.4-10m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.4-9m)
- rebuild against evolution-2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.4-8m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.4-7m)
- rebuild against evolution-2.30.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.4-6m)
- use BuildRequires

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.4-5m)
- --enable-gtk-doc

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.4-4m)
- delete __libtoolize hack

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.4-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Apr 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.4-1m)
- update to 0.14.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.3-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.3-4m)
- rebuild agaisst python-2.6.1-1m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.3-3m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+ or LGPLv2+

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-2m)
- rebuild against evolution-data-server-2.24.0
- disable database

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.2-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.2-5m)
- %%NoSource -> NoSource

* Mon Sep 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-4m)
- modify patch0 (for evolution-2.12.0)
- disable evolution

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-3m)
- add intltoolize (intltool-0.36.0)

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-2m)
- add -maxdepth 1 to del .la

* Sun May 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-1m)
- initial build

