%global momorel 2
%global src_name netcdf_handler

Summary:         NetCDF 3 data handler for the OPeNDAP Data server
Name:            dap-netcdf_handler
Version:         3.9.2
Release:         %{momorel}m%{?dist}
License:         LGPL
Group:           System Environment/Daemons 
URL:             http://www.opendap.org/
Source0:         http://www.opendap.org/pub/source/%{src_name}-%{version}.tar.gz
NoSOurce:        0
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:   bes-devel >= 3.9.0
BuildRequires:   libdap-devel >= 3.11.0
BuildRequires:   netcdf-devel >= 4.1.2

%description
This is the netcdf data handler for our data server. It reads netcdf 3
files and returns DAP responses that are compatible with DAP2 and the
dap-server software.

%prep 
%setup -q -n netcdf_handler-%{version}

%build
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install INSTALL="install -p"
find %{buildroot} -name '*.la' -exec rm -f {} \;

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING COPYRIGHT NEWS README
%config(noreplace) %{_sysconfdir}/bes/modules/nc.conf
%{_libdir}/bes/libnc_module.so
%{_datadir}/hyrax/data/nc/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.2-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.2-1m)
- rebuild againsr bes-3.9.0
- rebuild against libdap-3.11.0
- rebuild against netcdf-4.1.2
- update to 3.9.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.1-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.1-1m)
- rebuild against libdap-3.10.3 and bes-3.8.4 and netcdf-4.1.1
- update to 3.9.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.9-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Feb 15 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.9-4m)
- fix BuildRequires:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.9-3m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.9-2m)
- rebuild against libdap-3.8.2
- rebuild against bes-3.6.2

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.9-1m)
- update to 3.7.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.3-3m)
- rebuild against gcc43

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.3-2m)
- rebuild for new libdap

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.7.3-1m)
- import from Fedora

* Tue Oct 31 2006 Patrice Dumas <pertusus@free.fr> 3.7.3-3
- rebuild for new libcurl soname (indirect dependency through libdap)

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 3.7.3-2
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Wed Sep 20 2006 Patrice Dumas <pertusus at free.fr> 3.7.3-1
- update to 3.7.3

* Wed Sep  6 2006 Patrice Dumas <pertusus at free.fr> 3.7.2-1
- update to 3.7.2

* Sat Jul 22 2006 Patrice Dumas <pertusus at free.fr> 3.7.1-1
- update to 3.7.1

* Fri Mar  3 2006 Patrice Dumas <pertusus at free.fr> - 3.6.0-1
- new release
- remove upstreamed patch

* Mon Feb 20 2006 Patrice Dumas <pertusus at free.fr> - 3.5.2-2
- add a patch for netcdf detection in lib64

* Wed Feb  1 2006 Patrice Dumas <pertusus at free.fr> - 3.5.2-1
- re-add netcdf-devel

* Wed Nov 16 2005 James Gallagher <jgallagher@opendap.org> 3.5.1-1
- Removed netcdf-devel from BuildRequires. it does, unless you install 
- netcdf some other way.

* Thu Sep 21 2005 James Gallagher <jgallagher@opendap.org> 3.5.0-1
- initial release
