%global momorel 3

%global py_ver %(python -c 'import sys;print(sys.version[0:3])')
%{!?tcl_version: %global tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %global tcl_sitearch %{_libdir}/tcl%{tcl_version}}

Summary: Graph Visualization Tools
Name: graphviz
Version: 2.36.0
Release: %{momorel}m%{?dist}
Group: Applications/Engineering
License: CPL
URL: http://www.graphviz.org/
Source0: http://www.graphviz.org/pub/%{name}/ARCHIVE/%{name}-%{version}.tar.gz 
NoSource: 0

%{?include_specopt}
%{?!do_test: %global do_test 1}

BuildRequires: w3m
BuildRequires: flex, bison, expat-devel, curl-devel
BuildRequires: tcl-devel, tk-devel
BuildRequires: ruby-devel >= 1.9.2-0.992.9m, php-devel, python-devel >= 2.7 , guile-devel, perl
BuildRequires: freetype-devel >= 2.5.3-2m, fontconfig-devel >= 2.2.3-14m
BuildRequires: lasi-devel > 1.1.0-9m
BuildRequires: ghostscript-devel >= 9.05
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: gd-devel >= 2.0.34
BuildRequires: autoconf, automake, libtool >= 2.4
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXaw-devel
BuildRequires: libXext-devel, libXmu-devel, libXpm-devel, libXt-devel
#BuildRequires: ming-devel >= 0.4, swig
%if %{do_test}
BuildRequires: ksh, awk
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A collection of tools and tcl packages for the manipulation and layout
of graphs (as in nodes and edges, not as in barcharts).

%package demo
Summary: Demo graphs for graphviz
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}

%description demo
This package provides some example graphs for graphviz.

%package tcltk
Summary: TclTk extension tools for graphviz
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}

%description tcltk
TckTk extensions for %{name}

%package perl
Group:		Applications/Multimedia
Summary:	Perl extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description perl
Perl extensions for %{name}.

%package php
Group:		Applications/Multimedia
Summary:	PHP extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description php
PHP extensions for %{name}.

%package python
Group:		Applications/Multimedia
Summary:	Python extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description python
Python extensions for %{name}.

%package ruby
Group:		Applications/Multimedia
Summary:	Ruby extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description ruby
Ruby extensions for %{name}.

%package guile
Group:		Applications/Multimedia
Summary:	Guile extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description guile
Guile extensions for %{name}.

%if 0
%package sharp
Group:	Applications/Multimedia
Summary:	C# extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description sharp
C# extensions for %{name}.
%endif

%if 0
%package io
Group:		Applications/Multimedia
Summary:	Io extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description io
IO extensions for %{name}.
%endif

%if 0
%package java
Group:		Applications/Multimedia
Summary:	Java extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description java
Java extensions for %{name}.
%endif

%if 0
%package lua
Group:		Applications/Multimedia
Summary:	Lua extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description lua
Lua extensions for %{name}.
%endif

%if 0
%package ocaml
Group:		Applications/Multimedia
Summary:	Ocaml extension for %{name}
Requires:	%{name} = %{version}-%{release}

%description ocaml
Ocaml extensions for %{name}.
%endif

%package devel
Summary: Development tools for graphviz
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The graphviz-devel package contains the header files and man3 pages
necessary for developing programs using the graphviz libraries.

%package doc
Group: Documentation
Summary: PDF and HTML documents for graphviz
Requires: %{name} = %{version}-%{release}

%description doc
The graphviz-doc package provides some additional PDF and HTML
documentation for graphviz.

%prep
%setup -q

%build
# build fix
export CXXFLAGS="%{optflags} -fpermissive"

%define _ruby_inc %(ruby -rrbconfig -e "puts RbConfig::CONFIG['rubyhdrdir']")
%define _ruby_arch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['arch']")
%define ruby_vendorarch %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["vendorarchdir"] ')
CPPFLAGS="-I%{_ruby_inc}/%{_ruby_arch} -I%{_ruby_inc}"
export CPPFLAGS

%configure \
  --enable-static \
  --enable-shared \
  --disable-sharp \
  --disable-java \
  --disable-lua \
  --disable-ocaml \
  --disable-io \
  --enable-perl \
  --enable-php \
  --enable-python \
  --enable-ruby \
  --disable-ming \
  --with-x \
  --with-extraincludedir=%{_includedir}/tk-private/unix \
  --without-gts
# --with-dynagraph

%make

%install
[ "%{buildroot}" = '/' ] || %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'

%if ! 0
rm -rf %{buildroot}%{_libdir}/%{name}/{java,lua,sharp,io,ocaml}
%endif

# misc
chmod -x %{buildroot}%{_datadir}/%{name}/lefty/*
cp -a %{buildroot}%{_datadir}/%{name}/doc __doc
rm -rf %{buildroot}%{_datadir}/%{name}/doc
# remove unnecessary files
rm -rf %{buildroot}%{_libdir}/libltdl*
rm -rf %{buildroot}%{_includedir}/ltdl.h

find %{buildroot} -name "*.la" -delete

%check
%if %{do_test}
# regression test 
(cd cmd/dot && make dot_builtins)
cd rtest
%make rtest
%endif

%clean
[ "%{buildroot}" = '/' ] || %__rm -rf %{buildroot}

# run "dot -c" to generate plugin config in %{_libdir}/%{name}/config
%post
/sbin/ldconfig
%{_bindir}/dot -c

%postun
/sbin/ldconfig
if ! test -x %{_bindir}/dot; then rm -f %{_libdir}/%{name}/config; fi

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README*
%{_bindir}/*
%dir %{_libdir}/%{name}
#%%{_libdir}/%{name}/*.fdb
%{_libdir}/*.so.*
%{_libdir}/%{name}/*.so.*
%{_mandir}/man1/*.1*
%{_mandir}/man7/*.7*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/lefty
%{_datadir}/%{name}/gvedit
%{_datadir}/%{name}/gvpr
%exclude %{_libdir}/%{name}/*/*

%files demo
%defattr(-, root, root)
%{_datadir}/graphviz/demo
%{_datadir}/graphviz/graphs

%files tcltk
%defattr(-, root, root)
%dir %{_libdir}/%{name}/tcl
%{_libdir}/tcl%{tcl_version}/%{name}/*
%{_mandir}/man3/*.3tcl*
%{_mandir}/man3/tkspline.3tk*

%files ruby
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/ruby
%{_libdir}/%{name}/ruby/*
%{ruby_vendorarch}/gv.so
%{_mandir}/man3/*.3ruby*

%files python
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/python
%{_libdir}/%{name}/python/*
%{_libdir}/python%{py_ver}/site-packages/*so*
%{_libdir}/python%{py_ver}/site-packages/*py*
%{_mandir}/man3/gv.3python*

%files php
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/php
%{_libdir}/%{name}/php/*
%{_libdir}/php/modules/*.so*
%{_datadir}/php/gv.php
%{_mandir}/man3/gv.3php*

%files perl
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/perl
%{_libdir}/%{name}/perl/*
%{perl_vendorarch}/gv.pm
%{perl_vendorarch}/gv.so*
%{_mandir}/man3/gv.3perl*

%files guile
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/guile
%{_libdir}/%{name}/guile/*
%{_mandir}/man3/gv.3guile*

%if 0
%files sharp
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/sharp
%{_libdir}/%{name}/sharp/*
%endif

%if 0
%files io
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/io
%{_libdir}/%{name}/io/*
%endif

%if 0
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/java
%{_libdir}/%{name}/java/*
%endif

%if 0
%files lua
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/lua
%{_libdir}/%{name}/lua/*
%endif

%if 0
%files ocaml
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}/ocaml
%{_libdir}/%{name}/ocaml/*
%endif

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/*a
%{_libdir}/%{name}/*.so
%{_libdir}/%{name}/*a
%{_libdir}/pkgconfig/*.pc
%exclude %{_mandir}/man3/gv.3guile*
%exclude %{_mandir}/man3/*.3tcl*
%exclude %{_mandir}/man3/tkspline.3tk*
%exclude %{_mandir}/man3/gv.3python*
%exclude %{_mandir}/man3/gv.3php*
%exclude %{_mandir}/man3/gv.3perl*
%{_mandir}/man3/*.3*
%exclude %{_libdir}/%{name}/*/*

%files doc
%defattr(-,root,root)
%doc __doc/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36.0-3m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36.0-2m)
- enable to build with freetype-2.5.3, lasi-1.1.0

* Thu Apr 10 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.36.0-1m)
- update to 2.36.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-14m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-13m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-12m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-11m)
- rebuild against perl-5.16.3

* Tue Jan 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-10m)
- revert to 2.28.0

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-9m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-8m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-7m)
- rebuild against perl-5.16.0

* Fri May 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.28.0-6m)
- [BUILD FIX] add -fpermissive to CXXFLAGS

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-5m)
- rebuild against ghostscript-9.05

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.28.0-4m)
- use RbConfig

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-2m)
- rebuild against perl-5.14.1

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26.3-10m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.3-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.3-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.3-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26.3-6m)
- rebuild against perl-5.12.2

* Thu Sep 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-5m)
- build fix libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.3-4m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.3-3m)
- rebuild against ruby-1.9.2

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26.3-2m)
- rebuild against perl-5.12.1

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.3-1m)
- update 2.26.3

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-3m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-2m)
- rebuild against libjpeg-8a

* Sat Jan  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-1m)
- update 2.26
- [SECURITY] CVE-2009-3736 in embeded libtool codes
-- add libtoolize --ltdl to fix vulnerability

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.0-1m)
- update 2.24

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.2-2m)
- rebuild against perl-5.10.1

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.22.2-1m)
- update 2.22.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.3-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.20.3-2m)
- rebuild against python-2.6.1-2m

* Sun Oct 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.3-1m)
- [SECURITY] CVE-2008-4555
- update to 2.20.3
- add a configure option --without-gts
- modify %%files tcltk

* Sat Jul  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-1m)
- update 2.18
- disable ming support

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.12-9m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12-8m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12-7m)
- %%NoSource -> NoSource

* Sat Jun  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12-6m)
- build with gd-2.0.34

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12-5m)
- BuildRequires: freetype2-devel -> freetype-devel

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12-4m)
- delete libtool library

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.12-3m)
- add BuildRequires: swig

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12-2m)
- add BuildRequires: ming-devel

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12-1m)
- update 2.12

* Wed Nov 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-4m)
- rebuild against fontconfig-2.2.3-14m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.8-3m)
- rebuild against expat-2.0.0-1m)

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-2m)
- revise %%files tcktk

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.8-1m)
- update to 2.8
- new subpackage: ruby, perl, python, guile, php
- ocaml, lua, io, java, sharp are disabled temporarily

* Sun Jul  3 2005 TASHIRO Hideo <tashiron@momonga-linux.org>
- (2.2.1-3m)
- add BuildRequires: tcl-devel, tk-devel
- remove BuildRequires: tcl, tk
- do not execute autogen.sh

* Wed May 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.1-2m)
- revised includedir

* Tue May 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Sun Jan  9 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-1m)
- version up
- add graphviz-doc package

* Mon Oct 04 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.12-2m)
- %%makeinstall transform='s,x,x,'

* Wed Aug 11 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.12-1m)
- up to 1.12

* Mon Nov 10 2003 zunda <zunda at freeshell.org>
- (1.9-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Jun 23 2003 Kenta MURATA <muraken2@nifty.com>
- (1.9-2m)
- add Conflicts: gc-devel <= 6.2-5m.

* Sun Jun 22 2003 Kenta MURATA <muraken2@nifty.com>
- (1.9-1m)
- first specfile.
