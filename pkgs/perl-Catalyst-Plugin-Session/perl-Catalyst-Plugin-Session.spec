%global         momorel 3

Name:           perl-Catalyst-Plugin-Session
Version:        0.39
Release:        %{momorel}m%{?dist}
Summary:        Generic Session plugin - ties together server side storage and client side state required to maintain session data
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Plugin-Session/
Source0:        http://www.cpan.org/authors/id/J/JJ/JJNAPIORK/Catalyst-Plugin-Session-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-Catalyst-Runtime >= 5.90017
BuildRequires:  perl-Digest
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Moose >= 0.76
BuildRequires:  perl-MooseX-Emulate-Class-Accessor-Fast >= 0.00801
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-namespace-clean >= 0.10
BuildRequires:  perl-Object-Signature
BuildRequires:  perl-PathTools
BuildRequires:  perl-Test-Deep
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Tie-RefHash >= 1.34
Requires:       perl-Catalyst-Runtime >= 5.90017
Requires:       perl-Digest
Requires:       perl-File-Temp
Requires:       perl-Moose >= 0.76
Requires:       perl-MooseX-Emulate-Class-Accessor-Fast >= 0.00801
Requires:       perl-MRO-Compat
Requires:       perl-namespace-clean >= 0.10
Requires:       perl-Object-Signature
Requires:       perl-PathTools
Requires:       perl-Test-Simple
Requires:       perl-Tie-RefHash >= 1.34
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
The Session plugin is the base of two related parts of functionality
required for session management in web applications.

%prep
%setup -q -n Catalyst-Plugin-Session-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Plugin/Session
%{perl_vendorlib}/Catalyst/Plugin/Session.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-2m)
- rebuild against perl-5.18.2

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Fri Sep 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-2m)
- rebuild against perl-5.16.3

* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.16.2

* Sat Oct 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35
- rebuild against perl-5.16.0

* Mon Mar 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Sat Feb 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-4m)
- set do_test 0 due to tests fail on i686 and x86_64

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31-2m)
- rebuild for new GCC 4.5

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.30-2m)
- full rebuild for mo7 release

* Fri Jun 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.29-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Nov  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-2m)
- rebuild against perl-5.10.1

* Mon Jul 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- add BuildRequires: and Requires: perl-MooseX-Emulate-Class-Accessor-Fast

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-2m)
- rebuild against gcc43

* Tue Oct  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18
- do not use %%NoSource macro

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.13-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-2m)
- delete dupclicate directory

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.12-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
