%global momorel 10

Summary: a daily backup system similar to Plan9's dumpfs
Name: pdumpfs
Version: 1.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Url: http://www.namazu.org/~satoru/pdumpfs/
Group: Applications/Archiving
Source0: http://www.namazu.org/~satoru/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: pdumpfs-1.3-ruby19.patch
Patch1: pdumpfs-1.3-fix-test.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
Requires: ruby

%description
pdumpfs is a simple daily backup system similar to Plan9's dumpfs which
preserves every daily snapshot. pdumpfs is written in Ruby. You can access the
past snapshots at any time for retrieving a certain day's file. Let's backup
your home directory with pdumpfs!

%prep
%setup -q
%patch0 -p1 -b .ruby19
%patch1 -p1 -b .fix-test~

%build
make pdumpfs
make check

%install
rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__mkdir_p %{buildroot}%{_mandir}/man8
%__mkdir_p %{buildroot}%{_mandir}/ja/man8
%__install -m 755 pdumpfs %{buildroot}%{_bindir}
%__install man/man8/pdumpfs.8 %{buildroot}%{_mandir}/man8

# install Japanese manual page
iconv -f iso-2022-jp -t utf-8 man/ja/man8/pdumpfs.8 > \
  %{buildroot}%{_mandir}/ja/man8/pdumpfs.8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README
%doc doc/pdumpfs-ja.html doc/pdumpfs.html
%{_bindir}/pdumpfs
%{_mandir}/man8/pdumpfs.8*
%{_mandir}/ja/man8/pdumpfs.8*

%changelog
* Mon Aug  1 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-10m)
- add patch to fix possible test failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-7m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-6m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-3m)
- rebuild against gcc43

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-2m)
- convert ja.man from EUC-JP to UTF-8

* Tue Apr 05 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.3-1m)
- update to 1.3
- remove unused patches

* Sat Sep 04 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-1m)
- update to 1.2
- change %%doc

* Wed Jul 07 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Wed Sep 17 2003 zunda <zunda at freeshell.org>
- (0.6-4m)
- one more bug fix added to pdumpfs-0.6-ruby-1.8.patch

* Thu Sep 11 2003 zunda <zunda at freeshell.org>
- (0.6-3m)
- pdumpfs-0.6-ruby-1.8.patch to supply String.sub a Regexp

* Tue Feb 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-2m)
- change the charset of the japanese man page to euc-jp

* Tue Aug  6 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Thu Mar 19 2002 kitaj <kita@kitaj.no-ip.com>
- initial build
