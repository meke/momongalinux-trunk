%global momorel 8

Summary: sysfsutils, library interface to sysfs.
Name: sysfsutils
Version: 2.1.0
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/linux-diag/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: sysfsutils-2.0.0-redhatify.patch
Patch1: sysfsutils-2.0.0-class-dup.patch
License: LGPL
Group: Development/Tools
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
This package's purpose is to provide a set of utilities for interfacing
with sysfs.
#'

%package -n libsysfs
Summary: Shared library for interfacing with sysfs
Group: System Environment/Libraries
License: LGPL

%description -n libsysfs
Library used in handling linux kernel sysfs mounts and their various files.

%package -n libsysfs-devel
Summary: Static library and headers for libsysfs
Group: Development/Libraries
Requires: libsysfs = %{version}
Obsoletes: sysfsutils-devel

%description -n libsysfs-devel
libsysfs-devel provides the header files and static libraries required
to build programs using the libsysfs API.

%prep
%setup -q
%patch0 -p1 -b .redhatify
%patch1 -p1

%build
%configure
make

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

rm -f %{buildroot}%{_bindir}/dlist_test \
      %{buildroot}%{_bindir}/get_bus_devices_list \
      %{buildroot}%{_bindir}/get_class_dev \
      %{buildroot}%{_bindir}/get_classdev_parent \
      %{buildroot}%{_bindir}/get_device \
      %{buildroot}%{_bindir}/get_driver \
      %{buildroot}%{_bindir}/testlibsysfs \
      %{buildroot}%{_bindir}/write_attr
rm -f %{buildroot}%{_libdir}/*.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/systool
%{_bindir}/get_module
%{_mandir}/man1/systool.1.*
%doc COPYING AUTHORS README NEWS CREDITS ChangeLog docs/libsysfs.txt cmd/GPL

%files -n libsysfs
%defattr(-,root,root)
%{_libdir}/libsysfs.so.*
%doc COPYING AUTHORS README NEWS CREDITS ChangeLog docs/libsysfs.txt lib/LGPL

%files -n libsysfs-devel
%defattr(-,root,root)
%dir %{_includedir}/sysfs
%{_includedir}/sysfs/libsysfs.h
%{_includedir}/sysfs/dlist.h
%{_libdir}/libsysfs.a
%{_libdir}/libsysfs.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-2m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.0-1m)
- sync FC-devel 2.1.0-1

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-1m)
- re-sync with fc-devel 2.0.0-6 for xen - kpart(device-mapper-multipath) - libsysfs-devel

* Fri Dec 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-1m)
- sync with fc-devel

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-1m)
- import from FC3.
