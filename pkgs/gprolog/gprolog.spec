%global momorel 1

Name:           gprolog
Version:	1.4.4
Release:	%{momorel}m%{?dist}
Summary: 	GNU Prolog is a free Prolog compiler

Group: 		Development/Languages
License:	GPLv2
URL: 		http://www.gprolog.org
Source0:        ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource:	0	

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch:	x86_64 %{ix86} ppc alpha

%description 
GNU Prolog is a native Prolog compiler with constraint solving over
finite domains (FD) developed by Daniel Diaz
(http://loco.inria.fr/~diaz).

GNU Prolog is a very efficient native compiler producing (small)
stand-alone executables. GNU-Prolog also offers a classical
top-level+debugger.

GNU Prolog conforms to the ISO standard for Prolog but also includes a
lot of extensions (global variables, DCG, sockets, OS interface,...).

GNU Prolog also includes a powerful constraint solver over finite
domains with many predefined constraints+heuristics.

%package examples
Summary:	Examples for GNU Prolog
Group:		Development/Languages
Requires:	%{name} = %{version}-%{release}

%description examples
Examples for GNU Prolog.

%package docs
Summary:	Documentation for GNU Prolog
Group:		Documentation
Requires:	%{name} = %{version}-%{release}

%description docs
Documentation for GNU Prolog.

%prep
%setup -q

%build
cd src

# gprolog only acccept -O0 and don't like -fomit-frame-pointer

CFLG="$(echo $RPM_OPT_FLAGS | sed -s "s/\-O2/-O1/g" \
     		    | sed -e "s/\-fomit-frame-pointer//")"

# Based on a gentoo ebuild (??)
CFLG="$CFLG -funsigned-char"

# sed -i -e "s:TXT_FILES      = @TXT_FILES@:TXT_FILES=:" Makefile.in
./configure \
       --with-install-dir=%{buildroot}%{_libdir}/gprolog-%{version} \
       --without-links-dir --without-examples-dir \
       --with-doc-dir=dist-doc \
       --with-c-flags="$CFLG"

# _smp_flags seems to make trouble
make

%check
cd src
#
export PATH=%{buildroot}%{_bindir}:$PATH
#
make check

%install
rm -rf %{buildroot}
cd src
(
    make install
    mkdir %{buildroot}%{_bindir}
    cd %{buildroot}%{_libdir}/gprolog-%{version}/bin
    for i in *; do
 	ln -s ../%{_lib}/gprolog-%{version}/bin/$i %{buildroot}%{_bindir}/$i
    done
)
rm -f dist-doc/*.{chm,dvi,ps}
rm -f dist-doc/compil-scheme.pdf
rm -f dist-doc/debug-box.pdf

for file in ChangeLog COPYING NEWS VERSION
do
    rm -f %{buildroot}%{_libdir}/gprolog-%{version}/$file
done

cd ../examples/ExamplesPl

rm -rf BINPROLOG CIAO SICSTUS
rm -rf SWI WAMCC XSB YAP

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README COPYING ChangeLog NEWS PROBLEMS VERSION
%{_bindir}/*
%{_libdir}/gprolog-%{version}

%files examples
%defattr(-,root,root,-)
%doc examples

%files docs
%defattr(-,root,root,-)
%doc src/dist-doc/*

%changelog
* Sun May  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Fri Apr 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-7m)
- fix for BTS #336 (workaround)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-2m)
- sync with Rawhide (1.3.1-5) for gcc44 with graphite
-- ExclusiveArch: x86_64 %%{ix86} ppc alpha

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-5m)
- build with --with-c-flags="$RPM_OPT_FLAGS"
- use -O0 when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-3m)
- revise for rpm46 (s/Patch/Patch0/)
- License: GPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-2m)
- rebuild against gcc43

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-2m)
- fix %%changelog section

* Mon Feb 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0
- use gcc-4.1.x

* Sun Jan  8 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.16-4m)
- export CC=gcc_3_2, force using gcc 3.2.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.16-3m)
- revised spec for enabling rpm 4.2.

* Wed Jul  2 2003 zunda <zunda at freeshell.org>
- (kossori)
- change source location (thanks to Mr. Tanaka)

* Tue Jun 17 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.16-2m)
- change source location(reported by SIVA)

* Sat Oct 26 2002 Kikutani Makoto <poo@momonga-linux.org>
- (1.2.16-1m)
- upgrade to 1.2.16

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.13-1m)
- upgrade to 1.2.13

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- nigittenu

* Mon Nov 20 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- move CFLAGS to configure

* Tue Jul  4 2000 AYUHANA Tomonori <l@kondara.org>
- (1.1.2-2k)
- SPEC fixed ( URL )
- add -q at %setup
- add -b at %patch
- add RPM_OPT_FLAGS at make
- change %%install section
- change %files section

* Sat Jan 29 2000 Toru Hoshina <t@kondara.org>
- add some fix to spec file.

* Wed Jan 26 2000 AYUHANA Tomonori <l@kondara.org>
- enable to install in Kondara MNU/Linux
