%global momorel 8
%global geckover 1.9.2

Name: totem
Summary: A sound and video player

Version: 3.2.1
Release: %{momorel}m%{?dist}
License: "GPLv2 with exception"
URL: http://hadess.net/totem.php3
Group: User Interface/Desktops
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: gstreamer-plugins-base-devel >= 0.10.22
BuildRequires: gstreamer-devel >= 0.10.22
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: iso-codes
BuildRequires: glib2-devel >= 2.22.1
BuildRequires: gtk3-devel
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: gnome-icon-theme >= 3.0.0
BuildRequires: totem-pl-parser-devel >= 3.4.2
BuildRequires: libXrandr-devel
BuildRequires: libXxf86vm-devel
BuildRequires: pygtk2-devel >= 2.14.1
BuildRequires: libgalago-devel >= 0.5.2
BuildRequires: dbus-glib-devel >= 0.80
BuildRequires: bluez-libs-devel >= 4.32
BuildRequires: tracker-devel >= 0.10
BuildRequires: shared-mime-info
BuildRequires: startup-notification-devel
BuildRequires: nautilus-devel >= 2.26.1
BuildRequires: brasero-devel
BuildRequires: libgdata-devel >= 0.6.1
BuildRequires: vala-devel >= 0.9.8
BuildRequires: libmx-devel
BuildRequires: gnome-control-center-devel
BuildRequires: dbus-devel
BuildRequires: gnome-desktop-devel
BuildRequires: libglade2-devel
BuildRequires: libxml2-devel
BuildRequires: libmusicbrainz-devel
BuildRequires: pygobject-devel
BuildRequires: lirc-devel
BuildRequires: cheese
BuildRequires: xulrunner-devel >= %{geckover}
BuildRequires: libpeas-devel >= 1.1.0
BuildRequires: libarchive-devel >= 3.0.4

Requires: gstreamer-plugins-good
Requires: gnome-screensaver
Requires(pre): GConf2
Requires(pre): hicolor-icon-theme gtk2
Obsoletes: nautilus-media

Obsoletes: totem-xine totem-youtube totem-mythtv totem-publish
Obsoletes: totem-galago totem-nautilus totem-gstreamer
Obsoletes: totem-lirc
Obsoletes: totem-tracker

%description
Totem is simple movie player for the Gnome desktop. It features a
simple playlist, a full-screen mode, seek and volume controls, as well as
a pretty complete keyboard navigation.

%package mozplugin
Summary: Mozilla plugin for Totem
Group: Applications/Internet
Requires: gecko-libs >= %{geckover}
Obsoletes: totem-mozilla-plugin

%description mozplugin
Totem is simple movie player for the Gnome desktop. The mozilla plugin
for totem allows totem to be embeded into a web browser. 

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
export MOZILLA_PLUGINDIR=%{_libdir}/mozilla/plugins
%configure \
    --enable-silent-rules \
    --enable-static=no \
    --enable-gstreamer \
    --enable-easy-codec-installation \
    --enable-python \
    --enable-vala \
    --enable-browser-plugins \
    --enable-gmp-plugin \
    --enable-complex-plugin \
    --enable-narrowspace-plugin \
    --enable-mully-plugin \
    --enable-cone-plugin \
    --enable-mozilla \
    --enable-nautilus \
    --enable-gtk-doc \
    --disable-scrollkeeper \
    --disable-schemas-install \
    --disable-nvtv
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
rarian-sk-update
update-desktop-database -q
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
/sbin/ldconfig
rarian-sk-update
update-desktop-database -q
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO
%{_bindir}/%{name}*
%{_libexecdir}/%{name}
%{_libdir}/libtotem.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/Totem-1.0.typelib
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%{_libdir}/totem/plugins/bemused
%{_libdir}/totem/plugins/chapters
%{_libdir}/totem/plugins/gromit
%{_libdir}/totem/plugins/lirc
%{_libdir}/totem/plugins/media-player-keys
%{_libdir}/totem/plugins/ontop
%{_libdir}/totem/plugins/properties
%{_libdir}/totem/plugins/pythonconsole
%{_libdir}/totem/plugins/screensaver
%{_libdir}/totem/plugins/skipto
%{_libdir}/totem/plugins/rotation
%{_libdir}/totem/plugins/youtube
%{_libdir}/totem/plugins/brasero-disc-recorder
%{_libdir}/totem/plugins/opensubtitles
%{_libdir}/totem/plugins/screenshot
%{_libdir}/totem/plugins/dbus
%{_libdir}/totem/plugins/iplayer
%{_libdir}/totem/plugins/im-status
%{_libdir}/totem/plugins/save-file
%{_libdir}/totem/plugins/zeitgeist-dp

%{_libdir}/nautilus/extensions-3.0/*.so
%{_libdir}/nautilus/extensions-3.0/*.la

%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/omf/totem
%{_datadir}/thumbnailers/totem.thumbnailer
%{_datadir}/GConf/gsettings/opensubtitles.convert
%{_datadir}/GConf/gsettings/pythonconsole.convert
%{_datadir}/GConf/gsettings/totem.convert
%{_datadir}/glib-2.0/schemas/org.gnome.totem.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.totem.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.totem.plugins.opensubtitles.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.totem.plugins.pythonconsole.gschema.xml

%{_mandir}/man1/*.1*
%doc %{_datadir}/gnome/help/totem
%doc %{_datadir}/gtk-doc/html/totem/*

%files mozplugin
%defattr(-, root, root)
%{_libdir}/mozilla/plugins/*
%{_libexecdir}/totem-plugin-viewer

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/gir-1.0/Totem-1.0.gir

%changelog
* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-8m)
- BuildRequires: totem-pl-parser-3.4.2

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-7m)
- rebuild for tracker 0.14.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-6m)
- rebuild for glib 2.33.2

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-5m)
- rebuild against libarchive-3.0.4

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-4m)
- add BuildRequires

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-3m)
- remove BR hal

* Wed Dec 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.1-2m)
- delete Obsoletes: totem-devel

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.4-3m)
- build with pygobject-2.28.6

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.4-2m)
- fix BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4

* Mon Jun 27 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.1-4m)
- revised BR

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-3m)
- fix %%files

* Mon May  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-2m)
- add BuildRequires

* Wed May  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.0-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-7m)
- add patch0 for vala-0.9.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-6m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-4m)
- split tracker

* Wed Jun 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-3m)
- enable tracker

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu May 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.4-2m)
- rebuild against libgdata

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.4-1m)
- update to 2.29.4

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.5-3m)
- rebuild against xulrunner-1.9.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.5-2m)
- delete __libtoolize hack

* Wed Dec 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.5-1m)
- update to 2.28.5

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.4-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Mon Nov 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.4-1m)
- update to 2.28.4

* Mon Nov 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-3m)
- del BuildRequires: nautilus-cd-burner

* Thu Oct 15 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-2m)
- add BuildRequires

* Wed Sep 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-2m)
- add BuildRequires

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.2-4m)
- rebuild against xulrunner-1.9.1

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26.2-3m)
- rebuild against xulrunner-1.9.0.11-1m

* Fri May 22 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.26.2-2m)
- add define __libtoolize

* Mon May  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Tue Apr 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.26.1-3m)
- rebuild against xulrunner-1.9.0.10-1m

* Wed Apr 22 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.26.1-2m)
- rebuild against xulrunner-1.9.0.9-1m

* Fri Apr  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.26.0-3m)
- rebuild against xulrunner-1.9.0.8-1m

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-2m)
- separate tracker plugin

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24.3-7m)
- rebuild against xulrunner-1.9.0.6-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-6m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24.3-5m)
- rebuild against python-2.6.1 and pygtk2-2.13.0-3m

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.3-4m)
- rebuild against bluez-4.22

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24.3-3m)
- rebuild against xulrunner-1.9.0.5-1m

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24.3-2m)
- rebuild against xulrunner-1.9.0.4-1m

* Tue Oct 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.2

* Fri Oct 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.23.2-7m)
- rebuild against xulrunner-1.9.0.3-1m

* Thu Oct  9 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
- return to Momonga Spec ;-P
* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.23.2-6m)
- rebuild against xulrunner-1.9.0.2-1m

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.23.2-5m)
- change Requires: gst-plugins-base to gstreamer-plugins-base
- change Requires: gst-plugins-good to gstreamer-plugins-good 
- change BuildRequires: gst-plugins-base to gstreamer-plugins-base
- change BuildRequires: gst-plugins-base-devel to gstreamer-plugins-base-devel
- change BuildRequires: gst-plugins-good to gstreamer-plugins-good 
- change Requires: gst-plugins-good to gstreamer-plugins-good

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.23.2-4m)
- rebuild against xulrunner-1.9.0.1-1m

* Wed Jun  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.2-3m)
- add BuildRequires: tracker-devel

* Thu May  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.23.2-2m)
- move Obsoletes: totem-mozilla-plugin to %%package mozplugin

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.23.2-1m)
- update to 2.23.2
- rebuild against firefox-3

* Fri Apr 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2
- use xine again

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sat Mar  8 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.20.3-2m)
- add BuildRequires: lirc-devel

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Aug 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Sun Jun  3 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.18.2-2m)
- use specopt to choose backend engine, gstreamer or xine

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Thu Apr  5 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPrereq: gst-plugins-good

* Sat Mar 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-2m)
- rewind %%post script (gconftool-2)

* Wed Feb 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92
- delete patch0 (ja.po)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-1m)
- update to 2.16.5

* Sun Jan 21 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.16.4-2m)
- add ja.po patch

* Thu Nov 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Wed Nov 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Tue Nov 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-2m)
- good-bye mozilla

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-2m)
- remove category X-Red-Hat-Base X-Ximian-Main Application

* Mon Sep 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Mon Sep 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- use xine-libs

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-3m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.3-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-1m)
- update 1.4.3

* Sun Jul  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-2m)
- change directory for mozilla plugin
-- separated package plugin

* Thu Jun 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2
-- delete libtool library

* Sun May 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-2m)
- delete patch4: now vfx works with dbus.

* Mon May 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Wed May 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-4m)
- fix mozilla version
- add patch1, vfx does not work :-(

* Wed May 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-3m)
- add requires gnome-screensaver
- add schemas install totem-handlers.schemas

* Thu Apr 20 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.0-2m)
- rebuild against libmusicbrainz

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Dec 31 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-2m)
- add Patch0 totem-1.2.0-dbus060.patch
- add BuildPrereq: dbus-devel >= 0.60

* Tue Nov 22 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.2.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Fri Nov  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.99.16-3m)
- rebuild against with nautilus-cd-burner-2.8.6-4m (hal-0.5.4)

* Sun Jul 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.16-2m)
- add BuildPrereq: nautilus-cd-burner-devel

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.99.16-1m)
- version up

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.99.11-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sat Apr 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.11-1m)
- for gnome-2.6

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.99.8-2m)
- revised spec for rpm 4.2.

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.99.8-1m)
- version 0.99.8

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99.7-1m)
- version 0.99.7

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99.6-1m)
- version 0.99.6

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.99.5-1m)
- version 0.99.5

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99.2-1m)
- version 0.99.2

* Mon Jun 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99.1-1m)
- version 0.99.1

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99.0-1m)
- version 0.99.0

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.98.0-1m)
- version 0.98.0


