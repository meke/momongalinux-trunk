%global momorel 2

Summary: full-featured integrated development environment (IDE) for mono
Name: monodevelop
Version: 2.6.0.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+
URL: http://www.monodevelop.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: mono-devel >= 2.10
Requires: mono-core >= 2.10
Requires: mono-addins
BuildRequires: pkgconfig
BuildRequires: gtk-sharp2-devel >= 2.12.4
BuildRequires: gnome-sharp-devel >= 2.24.0
BuildRequires: gnome-desktop-sharp-devel
BuildRequires: gecko-sharp-2.0 >= 0.12
BuildRequires: mono-tools-devel >= 2.10
BuildRequires: mono-addins-devel >= 0.5

Patch2: monodevelop-1.9.1-libdir.patch
## http://www.serializing.net/index.php?gadget=Blog&action=SingleView&id=76

%description
This is MonoDevelop which is intended to be a full-featured integrated
development environment (IDE) for mono and Gtk#.  It was originally a
port of SharpDevelop 0.98.  See http://monodevelop.com/ for more info.

%prep
%setup -q
%patch2 -p1 -b .libdir

%build
autoreconf -vif
%configure \
    --program-prefix="" \
    --disable-update-mimedb \
    --disable-update-desktopdb
#    --enable-gtksourceview2 \
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_prefix}/lib/monodevelop

%{_libdir}/pkgconfig/monodevelop.pc
%{_libdir}/pkgconfig/monodevelop-core-addins.pc

%{_datadir}/applications/monodevelop.desktop
%{_datadir}/locale/*/*/*
%{_datadir}/mime/packages/*.xml
%{_datadir}/icons/hicolor/*/apps/monodevelop.*
%{_mandir}/man1/mdtool.1.*
%{_mandir}/man1/monodevelop.1.*

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0.1-2m)
- rebuild for mono-2.10.9

* Fri Sep 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0.1-1m)
- update to 2.6.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-2m)
- use BuildRequires

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-2m)
- update Patch2 for fuzz=0
- License: GPLv2+

* Sun Dec 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1 (unstable Alpha 2)
- comment out --enable-gtksourceview2
- http://lists.ximian.com/pipermail/mono-packagers-list/2008-November/000055.html

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-5m)
- good-bye debug symbol

* Mon Oct  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-4m)
- rebuild against gnome-sharp-2.24.0

* Sat Sep 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-3m)
- add Requires: mono-addins

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19-2m)
- fix BPR gnome-desktop-sharp

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sat Jan  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.1-1m)
- update to 0.18.1

* Thu Dec 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Wed Nov  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Tue Oct  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Thu Aug  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sun Jul  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Fri Mar  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.1-1m)
- update to 0.13.1

* Fri Feb 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.11-2m)
- revised for multilibarch

* Fri May 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-2m)
- rebuild mono -> mono-core

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-1m)
- update to 0.10
- delete patch0 patch2

* Fri Feb 03 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.9-2m)
- add patch2 for using $libdir

* Thu Feb  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0. 9-1m)
- start.
- but, now debugger does not work!!!
