%global momorel 17
%global scimver 1.4.9
%global qtver 3.3.7
%global kdever 3.5.10
%global qtdir %{_libdir}/qt-%{qtver}
%global kdedir /usr

Name: scim-qtimm
Summary: SCIM context plugin for qt-immodule
Version: 0.9.4
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: GPLv2
URL: http://www.scim-im.org/projects/scim_qtimm/
Source0: http://dl.sourceforge.net/sourceforge/scim/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: disable-debug.diff
Patch1: bugzilla-116220-keyboard-layout.patch
Patch2: bugzilla-206547-%{name}-crash.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt3 = %{qtver}
Requires: scim >= %{scimver}
BuildRequires: qt3-devel = %{qtver}
BuildRequires: scim-devel >= %{scimver}

%description
SCIM context plugin for qt-immodule

%prep
%setup -q

%patch0 -p1 -b .disable-debug
%patch1 -p1 -b .keybord
%patch2 -p1 -b .fix-crash

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-dir=%{qtdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# remove unneeded file
rm -f %{buildroot}/%{qtdir}/plugins/inputmethods/*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS TODO
%{qtdir}/plugins/inputmethods/*.so
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-15m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-14m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-12m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-11m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-10m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-9m)
- %%NoSource -> NoSource

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-8m)
- import bugzilla-206547-scim-qtimm-crash.patch from opensuse
 +* Fri Dec 22 2006 - zsu@suse.de
 +- Bugzilla #206547: Fix crash of many kde applications caused by
 +  scim-qtimm when closing/opening/switching application windows.
- import bugzilla-116220-keyboard-layout.patch from opensuse
 +* Mon Sep 12 2005 - mfabian@suse.de
 +- Bugzilla #116220: Fix a keyboard layout related bug in scim-qtimm

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-7m)
- use md5sum_src0

* Tue Feb 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-6m)
- remove libqscim.la

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-5m)
- rebuild against qt-3.3.7

* Tue Feb 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-4m)
- import disable-debug.diff from opensuse

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-3m)
- rebuild against qt-3.3.5-1m

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- add --disable-rpath to configure
- remove kdelibs-devel from BuildPreReq

* Wed Aug 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-1m)
- update to 0.9.4

* Thu Aug 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-1m)
- update to 0.9.3
- remove 64bit.patch
- remove disable-debug.diff
- remove nokdelibsuff.patch

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.95-2m)
- add --with-qt-libraries=%%{qtdir}/lib to configure

* Sun May 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.95-1m)
- update to 0.8.95 (minor bug fix release)
- change URL

* Tue Apr 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.9-1m)
- update to 0.8.9 (bug fix release)
- add scim-qtimm-0.8.9-nokdelibsuff.patch

* Fri Mar 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-0.20050317.1m)
- update to 20050317 CVS snapshot
- update Patch0: 64bit.patch

* Thu Mar 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-2m)
- correct Requires and BuildPreReq

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- update to 0.8.5
- import 64bit.patch and disable-debug.diff from SUSE
  pub/projects/m17n/9.2/src/scim-qtimm-0.8.5-2.1.src.rpm

* Mon Feb 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- initial package for Momonga Linux

* Thu Jul 17 2004 LiuCougar <liuspider@users.sourceforge.net> 0.5-0.r10.1
- update the version

* Thu Jul 15 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.0.1-0.r03.3
- spec for official scim-qtimm

* Mon Jul 12 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.0.1-0.r03.2ut
- fix URL
- remove Conflicts

* Tue Jul 06 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.0.1-0.r03.1mdk
- fix buildrequires
- initial spec for mdk (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
