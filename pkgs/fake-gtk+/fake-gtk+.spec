%global momorel 1
%global gtk2ver 2.24.19

Summary: just a fake pakckage provides gtk+
Name: fake-gtk+
Version: %{gtk2ver}
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2 = %{gtk2ver}
Provides: gtk+ = %{version}-%{release}
BuildArch: noarch

%description
This is a fake package for libreoffice depending on gtk+.
This package should be removed from trunk ASAP.

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files 
%defattr(-, root, root)

%changelog
* Sat Nov 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24.19-1m)
- initial package for smooth upgrade
- this is a fake package for libreoffice depending on gtk+
- this package should be removed from trunk ASAP