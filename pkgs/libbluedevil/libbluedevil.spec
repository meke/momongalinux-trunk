%global         momorel 1
%global         srcver 1.9-4
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable/%{name}
%else
%global         release_dir stable/%{name}
%endif
%global         kdever 4.11.1
%global         ftpdirver 1.9.4
%global         sourcedir %{release_dir}/%{ftpdirver}/src
%global         distname Momonga Linux
%global         qtver 4.8.5
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m

Name:           libbluedevil
Version:        1.9.4
Release:        %{momorel}m%{?dist}
Summary:        A Qt wrapper for bluez
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://ereslibre.es/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  automoc4
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
Requires:       bluez

%description
%{name} is Qt-based library written handle all Bluetooth functionality.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
Development files for %{name}.

%prep
%setup -q -n %{name}-v%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc HACKING
%{_kde4_includedir}/*
%{_kde4_libdir}/*.so
%{_kde4_libdir}/pkgconfig/*.pc

%changelog
* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.4-1m)
- update to 1.9.4

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-1m)
- update to 1.9.3

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2-1m)
- update to 1.9.2

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1
- use %%{cmake_kde4} macro

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-1m)
- import from Fedora devel

* Tue Nov 30 2010 Jaroslav Reznik <jreznik@redhat.com> - 1.8-3
- update to 1.8-1 (respin?)

* Wed Sep 29 2010 jkeating - 1.8-2
- Rebuilt for gcc bug 634757

* Tue Sep 21 2010 Jaroslav Reznik <jreznik@redhat.com> - 1.8-1
- update to 1.8

* Fri Aug 13 2010 Jaroslav Reznik <jreznik@redhat.com> - 1.7-1
- initial package
