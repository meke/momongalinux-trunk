%global momorel 10

Summary: GNU Documents in Japanese.
Name: gnujdoc
Version: 20051101
Release: %{momorel}m%{?dist}
License: GPL
Group: Documentation
URL: http://openlab.ring.gr.jp/gnujdoc/

Source0: http://openlab.ring.gr.jp/gnujdoc/snapshot/%{name}-%{version}.tar.bz2
#NoSource: 0

Patch0:	gnujdoc-direntry.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
BuildRequires: emacs

Requires(post): info
Requires(preun): info

%description
This is the GNU Japanese Documentation package. We intend to make a
package for Japanese documents about the GNU Project, and provide
simple and easy installation methods for end-users.

%package html
Summary: GNU Documents HTML version in Japanese.
Group: Documentation

%description html
This is the GNU Japanese Documentation package. We intend to make a
package for Japanese documents about the GNU Project, and provide
simple and easy installation methods for end-users.

%prep
%setup -q -n gnujdoc
%patch0 -p1
%build
autoreconf
%configure \
  --with-emacs=emacs \
  --disable-automake-1_3 \
  --disable-automake-1_4 \
  --disable-automake-1_5 \
  --disable-automake-1_6 \
  --disable-automake-1_7 \
  --disable-automake-1_8 \
  --disable-coreutils-5_0 \
  --disable-cvs \
  --disable-cvs-1_10_5 \
  --disable-cvs-1_10_6 \
  --disable-cvs-1_10_7 \
  --disable-cvs-1_10_8 \
  --disable-autoconf-2_12 \
  --disable-autoconf-2_13 \
  --disable-autoconf-2_52 \
  --disable-autoconf-2_53 \
  --disable-autoconf-2_54 \
  --disable-autoconf-2_57 \
  --disable-hurd \
  --disable-gdb-4_16 \
  --disable-gdb-4_17 \
  --disable-flex-2_3_7 \
  --disable-emacs-20_3 \
  --disable-bison-1_25 \
  --disable-grep-2_4_2 \
  --disable-wget-1_5_3 \
  --disable-wget-1_6 \
  --disable-wget-1_7 \
  --disable-wget-1_8_1 \
  --disable-wget-1_9 \
  --disable-textutils-2_0 \
  --disable-texinfo-4_0 \
  --disable-texinfo-4_2 \
  --disable-fileutils-4_0 \
  --disable-fileutils-4_1 \
  --disable-libtool-1_3_5 \
  --disable-libtool-1_4 \
  --disable-libtool-1_4_1 \
  --disable-libtool-1_4_2 \
  --disable-sh-utils-2_0 \
  --disable-binutils-2_10_1 \
  --disable-binutils-2_11 \
  --disable-binutils-2_11_2 \
  --disable-binutils-2_12_1 \
  --disable-binutils-2_13 \
  --disable-sed-3_02 \
  --disable-findutils-4_1 \
  --disable-sed-4_0_1
%__make info 
%__make MAKEINFOHTML="makeinfo --html" html

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%__make infodir=%{buildroot}%{_infodir} install
%__rm -f %{buildroot}%{_infodir}/dir

%define gnujdoc-html %{_docdir}/gnujdoc/html
%__mkdir_p %{gnujdoc-html}
find . -type d -name "*.html" -exec %__cp -r {} %{gnujdoc-html} \;

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/autoconf-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/automake-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/binutils-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/bison-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/coreutils-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/cvs-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/diff-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/elisp-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/emacs-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/find-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/flex-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/gdb-ja.info %{_infodir}/dir --entry="* Gdb(ja): (gdb-ja).  The GNU debugger." --section="Programming & development tools."
/sbin/install-info %{_infodir}/grep-ja.info %{_infodir}/dir --section="Miscellaneous" --entry="* grep(ja): (grep-ja).                   print lines matching a pattern."
/sbin/install-info %{_infodir}/gzip-ja.info %{_infodir}/dir --section="Utilities"
/sbin/install-info %{_infodir}/info-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/info-stnd-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/libtool-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/m4-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/sed-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/standards-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info %{_infodir}/texinfo-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/wget-ja.info %{_infodir}/dir
/sbin/install-info %{_infodir}/gengetopt-ja.info %{_infodir}/dir --entry="* Gengetopt(ja): (gengetopt-ja).  Getopt option parser generator." --section="Programming & development tools."

%preun
/sbin/install-info --delete %{_infodir}/autoconf-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/automake-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/binutils-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/bison-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/coreutils-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/cvs-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/diff-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/elisp-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/emacs-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/find-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/flex-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/gdb-ja.info %{_infodir}/dir --entry="* Gdb(ja): (gdb-ja).  The GNU debugger." --section="Programming & development tools."
/sbin/install-info --delete %{_infodir}/grep-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/gzip-ja.info %{_infodir}/dir --section="Utilities"
/sbin/install-info --delete %{_infodir}/info-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/info-stnd-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/libtool-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/m4-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/sed-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/standards-ja.info %{_infodir}/dir --section="Miscellaneous"
/sbin/install-info --delete %{_infodir}/texinfo-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/wget-ja.info %{_infodir}/dir
/sbin/install-info --delete %{_infodir}/gengetopt-ja.info %{_infodir}/dir --entry="* Gengetopt(ja): (gengetopt-ja).  Getopt option parser generator." --section="Programming & development tools."

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL*
%doc MANUALS NEWS README THANKS TODO
%{_infodir}/*.info*

%files html
%defattr(-,root,root)
%doc %{gnujdoc-html}/*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20051101-10m)
- rebuild for emacs-24.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20051101-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20051101-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20051101-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (20051101-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20051101-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20051101-4m)
- rebuild against rpm-4.6

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20051101-3m)
- fix install-info of grep-ja

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20051101-2m)
- rebuild against gcc43

* Fri Nov 04 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (20051101-1m)
- version up.

* Tue Oct 04 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (20051004-1m)
- version up.

* Tue Sep 27 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (20050927-1m)
- version up.

* Tue Jun 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (20050823-1m)
- version up.

* Tue Jun 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (200506014-1m)
- version up.
- add html package.

* Tue Jun  7 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (20050607-1m)
- version up.
- delete html package (Give up!).

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (20021223-2m)
- revised spec for enabling rpm 4.2.

* Wed Dec 25 2003 Kenta MURATA <muraken2@nifty.com>
- (20021223-1m)
- version up.

* Sun Feb 16 2003 Kenta MURATA <muraken2@nifty.com>
- (20020211-1m)
- version up.

* Wed Oct 30 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (20021001-3m)
- fix direntry

* Sat Oct 05 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (20021001-2m)
- Source0 gz -> bz2.

* Sat Oct 05 2002 Kenta MURATA <muraken2@nifty.com>
- (20021001-1m)
- up to 20021001

* Wed Apr 17 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (20020416-2k)
- up to 20020416
- stop NoSource

* Fri Apr  5 2002 Kenta MURATA <muraken@kondara.org>
- (20020402-4k)
- %post, %postun ga...

* Fri Apr  5 2002 Kenta MURATA <muraken@kondara.org>
- (20020402-2k)
- version up.
- gzip -> bzip2.
- install directory change to %{_infodir}.

* Wed Nov 22 2000 Kenichi Matsubara <m@kondara.org>
- (20001108-2k)
- info.gz to info.*.

* Mon Oct  2 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (20001001-1k)
- version up

* Sun Sep  3 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (20000816-3k)
- change install directory to /usr/share/info/ja_JP.eucJP

* Fri Aug 30 2000 Kenzi Cale <kc@furukawa.ch.kagu.sut.ac.jp>
- (20000816-1K)
- make "dir" available to another Info file
- make post and preun wise
- description -l ja to UTF-8

* Tue Aug 29 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (20000816-0K)
- delete source except gnujdoc
- delete patch
- change install directory to /usr/share/info/ja_JP.eucJP/info

* Sat Jul 29 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (20000510-3k)
- add 'chmod +w -R .' because patch1 does not able by permission
- change %setup

* Thu Jul 29 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (20000510-2k)
- fix Requires -> BuildRequires

* Thu Jul 27 2000 AYUHANA Tomonori <l@kondara.org>
- (20000510-1k)
- add -q at %setup
- rebuild against glibc-2.1.91, X-4.0, rpm-3.0.5

* Sat May 20 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- modify spec file. And add Wget.

* Fri May 5 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- modify spec file. change version.

* Fri Apr 28 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- modify spec file. change version.

* Sat Apr 22 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- modify spec file. for emacs, elisp, gcc, and texinfo

* Sun Apr 18 2000 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- create.
