%global momorel 14
# AucTeX includes preview-latex which allows previeweing directly in the Emacs
# buffer. This makes use of preview.sty, a LaTeX class, which is also included
# with AucTex. preview-latex can either use a privately installed copy of
# preview.sty, or it can use one installed in the system texmf tree. If the
# following is set to 1, an add-on LaTeX package will be created which installs
# into the system texmf tree, and preview-latex will use that. However, TeXLive
# already includes preview.sty and so this may not be desireable -- setting the
# following value to 0 means that preview-latex/AucTeX will use a privately
# installed copy of preview.sty.
%global separate_preview 0

Summary: 	Enhanced TeX modes for Emacs
Name: 		emacs-auctex
Version: 	11.86
Release: %{momorel}m%{?dist}
License: 	GPLv3+
Group: 		Applications/Editors
URL: 		http://www.gnu.org/software/auctex/
Obsoletes: 	auctex
Provides: 	auctex
Conflicts: 	emacspeak < 18
Requires: 	emacs(bin) >= %{_emacs_version}
Requires:	ghostscript texlive-dvipng
Requires:	texlive-texmf-latex texlive-dvipsk
Requires(pre): 	/sbin/install-info 
Requires(post): /sbin/install-info
%if %{separate_preview}
Requires: 	tex-preview = %{version}-%{release}
%endif
Obsoletes:	elisp-auctex
Provides:	elisp-auctex

%if ! %{separate_preview}
Obsoletes:	tetex-preview
%endif

Source0: 	ftp://ftp.gnu.org/pub/gnu/auctex/auctex-%{version}.tar.gz
NoSource:	0
# This patch is present upstream and can be removed in the next release after 11.86
Patch0:		auctex-11.86-fix-equation-preview.patch

BuildArch: 	noarch
BuildRequires: 	emacs texlive-texmf-latex texinfo-tex ghostscript
BuildRequires:  texlive-pdftex >= 2010-9m

%description 
AUCTeX is an extensible package that supports writing and formatting
TeX files for most variants of Emacs.

AUCTeX supports many different TeX macro packages, including AMS-TeX,
LaTeX, Texinfo and basic support for ConTeXt.  Documentation can be
found under /usr/share/doc, e.g. the reference card (tex-ref.pdf) and
the FAQ. The AUCTeX manual is available in Emacs info (C-h i d m
AUCTeX RET). On the AUCTeX home page, we provide manuals in various
formats.

AUCTeX includes preview-latex support which makes LaTeX a tightly
integrated component of your editing workflow by visualizing selected
source chunks (such as single formulas or graphics) directly as images
in the source buffer.

This package is for GNU Emacs.

%package el
Summary: 	Elisp source files for %{name}
Group: 		Applications/Editors
Requires: 	%{name} = %{version}-%{release}

%description el
This package contains the source Elisp files for AUCTeX for Emacs.

%package doc
Summary:	Documentation in various formats for AUCTeX
Group:		Documentation

%description doc
Documentation for the AUCTeX package for emacs in various formats,
including HTML and PDF.

%if %{separate_preview}
%package -n tex-preview
Summary: 	Preview style files for LaTeX
Group: 		Applications/Publishing
Requires: 	texlive-texmf-latex
Obsoletes:	tetex-preview
Provides:	tetex-preview

%description -n tex-preview 
The preview package for LaTeX allows for the processing of selected
parts of a LaTeX input file.  This package extracts indicated pieces
from a source file (typically displayed equations, figures and
graphics) and typesets with their base point at the (1in,1in) magic
location, shipping out the individual pieces on separate pages without
any page markup.  You can produce either DVI or PDF files, and options
exist that will set the page size separately for each page.  In that
manner, further processing (as with Ghostscript or dvipng) will be
able to work in a single pass.

The main purpose of this package is the extraction of certain
environments (most notably displayed formulas) from LaTeX sources as
graphics. This works with DVI files postprocessed by either Dvips and
Ghostscript or dvipng, but it also works when you are using PDFTeX for
generating PDF files (usually also postprocessed by Ghostscript).

The tex-preview package is generated from the AUCTeX package for 
Emacs.
%endif

%prep
%setup -q -n auctex-%{version}

%patch0 -p0 -d preview

%build
%if %{separate_preview}
%configure --with-emacs 
%else
%configure --with-emacs --without-texmf-dir
%endif

make
# Build documentation in various formats
pushd doc
make extradist
popd

# Fix some encodings
iconv -f ISO-8859-1 -t UTF8 RELEASE > RELEASE.utf8 && touch -r RELEASE RELEASE.utf8 && mv RELEASE.utf8 RELEASE

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_emacs_sitestartdir}
make DESTDIR=%{buildroot} install
rm -rf %{buildroot}%{_var}

# Remove /usr/share/doc/auctex directory from buildroot since we don't want doc
# files installed here
rm -rf %{buildroot}%{_docdir}/auctex

%post
/sbin/install-info %{_infodir}/auctex.info %{_infodir}/dir 2>/dev/null || :
/sbin/install-info %{_infodir}/preview-latex.info %{_infodir}/dir 2>/dev/null || :

%preun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/auctex.info %{_infodir}/dir 2>/dev/null || :
  /sbin/install-info --delete %{_infodir}/preview-latex.info %{_infodir}/dir 2>/dev/null || :
fi

%if %{separate_preview}
%post -n tex-preview
/usr/bin/texhash > /dev/null 2>&1 || :

%postun -n tex-preview
/usr/bin/texhash > /dev/null 2>&1 || :
%endif

%files
%defattr(-,root,root,-)
%doc RELEASE COPYING README TODO FAQ CHANGES
%doc %{_infodir}/*.info*
%exclude %{_infodir}/dir
%{_emacs_sitestartdir}/*
%dir %{_emacs_sitelispdir}/auctex
%dir %{_emacs_sitelispdir}/auctex/style
%{_emacs_sitelispdir}/auctex/*.elc
%{_emacs_sitelispdir}/auctex/style/*.elc
%{_emacs_sitelispdir}/auctex/.nosearch
%{_emacs_sitelispdir}/auctex/style/.nosearch
%{_emacs_sitelispdir}/auctex/images
%{_emacs_sitelispdir}/tex-site.el
%if !%{separate_preview}
%{_emacs_sitelispdir}/auctex/latex
%{_emacs_sitelispdir}/auctex/doc
%endif

%files el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/auctex/*.el
%{_emacs_sitelispdir}/auctex/style/*.el

%if %{separate_preview}
%files -n tex-preview
%defattr(-,root,root,-)
%doc COPYING
%{_datadir}/texmf/tex/latex/preview
%{_datadir}/texmf/doc/latex/styles
%endif

%files doc
%doc doc/*.{dvi,ps,pdf}
%doc doc/html

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.86-14m)
- fix possible build failure

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.86-13m)
- rebuild for emacs-24.1

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (11.86-12m)
- add BuildRequires: texlive-pdftex >= 2010-9m
- needs texlive built with newpoppler

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.86-11m)
- set Obsoletes: tetex-preview to main package

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.86-10m)
- revise requires tag

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.86-9m)
- rename the package name
- reimport from fedora

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (11.86-8m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.86-7m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.86-6m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.86-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (11.86-4m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.86-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.86-2m)
- merge auctex-emacs to elisp-auctex
- kill auctex-xemacs

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.86-1m)
- update to 11.86

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.85-15m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.85-14m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-13m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-12m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-11m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-10m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-9m)
- rebuild against emacs-23.0.92

* Sun Mar  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-8m)
- not make backup file of Patch0
- License: GPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-7m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-6m)
- fix INFO-DIR-SECTION of info files

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.85-2m)
- rebuild against gcc43

* Tue Feb 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.85-1m)
- update to 11.85
- License: GPLv3

* Wed Jul 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.84-4m)
- add PreReq: kpathsea (tetex-preview package)

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.84-3m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.84-2m)
- rebuild against emacs-22.1

* Thu Apr 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.84-1m)
- update to 11.84 (sync with Fedora)
  preview-latex was merged into this package
- add Patch0: auctex-11.84-tex-jp.el.patch
  see <http://at-aka.blogspot.com/2007/01/auctex-1184-tex-jpel.html>

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-11m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-10m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-9m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-8m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-7m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-6m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-5m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (11.55-4m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (11.55-3m)
- use %%{xe_sitedir}, %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.55-2m)
- rebuild against emacs 22.0.50

* Thu Feb 10 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (11.55-1m)
- version up

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (11.14-2m)
- rebuild against emacs-21.3.50

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (11.14-1m)
- verup

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.11-4m)
- rebuild against emacs-21.3

* Tue Dec 24 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (11.11-3m)
- change upstream URL ([Momonga-devel.ja:01123] thanks, SIVA)

* Thu Jun 13 2002 Kenta MURATA <muraken2@nifty.com>
- (11.11-2k)
- version up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (10.0g-12k)
- /sbin/install-info -> info in PreReq.

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (10.0g-10k)
- rebuild against emacs-21.2

* Sun Nov 18 2001 Kenta MURATA <muraken2@nifty.com>
- (10.0g-8k)
- add auctex.el for config-sample
- auctex-kondara.patch

* Mon Oct 29 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (elisp-auctex-10.0g-6k)
- modified %BuildPrereq and %Requires (added xemacs-sumo)

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (elisp-auctex-10.0g-4k)
- merge auctex-emacs and auctex-xemacs

* Tue Jul 17 2001 Hidetomo Machi <mcHT@kondara.org>
- (10.0g-2k)
- import to 2.0
- use %{_tmppath} macro
- add Makefile patch

* Mon Jul 16 2001 Toshiro HIKITA <toshi@sodan.org>
- (10.0g-3k)
- updated to 10.0g

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (9.10t-3k)
- modify specfile (License)
- modify %post and %preun
- modify %doc

* Wed Oct 18 2000 Toshiro HIKITA <toshi@sodan.org>
- (9.10t-1k)
- imported from 9.9p-2mdk of Mandarake
- Kondarize, adopt to Specfile Gaidance
- updated to 9.10t

* Sat Sep  2 2000 Pixel <pixel@mandrakesoft.com> 9.9p-2mdk
- mandrake adaptation
- cleanup

* Fri Jul 10 1998 W.L. Estes <wlestes@wlestes.uncg.edu>
- build root now used
- cleaned up file list
- fixed url tag
- added CHANGES and ChangeLog to doc files
- now requires emacs >= 20
- added \%clean
- misc spec file cosmetic cleanups

 *Wed Mar 11 1998 Martin Schimschak <masch@theo-phys.uni-essen.de>
 - initial revision for auctex 9.8l
