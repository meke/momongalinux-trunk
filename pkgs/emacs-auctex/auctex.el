;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; auctex configuration sample by muraken <muraken2@nifty.com>
;;
;; Please copy & paste to .emacsen
;;

(require 'tex-site)
(require 'tex-jp)
(if window-system
    (require 'font-latex))

;; Set default to japanese-latex-mode
(setq TeX-default-mode 'japanese-latex-mode)

;; Default style for japanese LaTeX
(setq-default japanese-LaTeX-default-style "jsarticle")

;; File parsing
(setq TeX-parse-self t	;; Enable parse on load
      TeX-auto-save t	;; Enable parse on save
      )

;; Some indent levels
(setq LaTeX-indent-level     2
      LaTeX-item-indent      2
      TeX-brace-indent-level 2
      )

;; Automatically remove all tabs from a file before saving it
(setq TeX-auto-untabify nil)

;; Query for master file
(setq-default TeX-master nil)
;; All master files called "hoge"
;;(setq-default TeX-master "hoge")

;; Internationalization
(add-hook 'TeX-language-de-hook
	  (function (lambda () (ispell-change-dictionary "german"))))
