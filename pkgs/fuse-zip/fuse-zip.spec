%global momorel 6

Name:           fuse-zip
Version:        0.2.11
Release:        %{momorel}m%{?dist}
Summary:        Fuse-zip is a fs to navigate, extract, create and modify ZIP archives
Group:          System Environment/Libraries
License:        GPLv3+
URL:            http://code.google.com/p/%{name}/
Source0:        http://%{name}.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
# Submitted patch
# http://code.google.com/p/fuse-zip/issues/detail?id=15
Patch0:         fuse-zip-makefile.patch
Patch1:         fuse-zip-0.2.11-size.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libzip-devel >= 0.10, pkgconfig, fuse-devel, zlib-devel
Requires:       fuse

%description
fuse-zip is a FUSE file system to navigate, extract, create and modify
ZIP archives based in libzip implemented in C++.

With fuse-zip you really can work with ZIP archives as real directories.
Unlike KIO or Gnome VFS, it can be used in any application without
modifications.

Unlike other FUSE filesystems, only fuse-zip provides write support
to ZIP archives. Also, fuse-zip is faster that all known implementations
on large archives with many files.

%prep
%setup -q
%patch0 -p1 -b .makefile
%patch1 -p1 -b .size

%build
make %{?_smp_mflags} CXXFLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf %{buildroot}
make install INSTALLPREFIX=%{buildroot}/usr
rm -rf %{buildroot}/usr/share/doc/fuse-zip

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE README changelog
%{_bindir}/fuse-zip
%{_mandir}/man1/%{name}.1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.11-6m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.11-5m)
- add patch1 to fix build failure on i686

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.11-4m)
- rebuild against libzip-0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.11-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.11-1m)
- import from Fedora

* Sat Jan 30 2010 Rakesh Pandit <rakesh@fedoraproject.org> - 0.2.11-1
- Updated to 0.2.11

* Fri Dec 04 2009 Rakesh Pandit <rakesh@fedoraproject.org> - 0.2.8-1
- Updated to 0.2.8

* Thu Sep 17 2009 Peter Lemenkov <lemenkov@gmail.com> - 0.2.7-4
- Rebuilt with new fuse

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 29 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.7-1
- Upgraded to 0.2.7

* Mon Dec 29 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.6-6
- fixed man page spelling mistake

* Sun Dec 07 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.6-5
- fixed debug info package

* Fri Nov 08 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.6-4
- removed INSTALL file from package - not useful

* Fri Nov 07 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.6-3
- fix flag, save timestamp and clean %%install

* Tue Nov 04 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.6-2
- Makefile patch by Debarshi Ray <rishi@fedoraproject.org>, fix debuginfo

* Tue Nov 04 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.2.6-1
- initial package

