%global momorel 2
%global dracutlibdir %{_prefix}/lib/dracut

%if %{defined gittag}
%define rdist .git%{gittag}%{?dist}
%define dashgittag -%{gittag}
%else
%define rdist %{?dist}
%endif

Name: dracut
Version: 037
Release: %{momorel}m%{?dist}
Summary: Initramfs generator using udev
Group: System Environment/Base		
License: GPLv2+	
URL: https://dracut.wiki.kernel.org/index.php/Main_Page
Source0: http://www.kernel.org/pub/linux/utils/boot/dracut/dracut-%{version}.tar.xz
NoSource: 0
Source1: momonga.conf

Patch1: 0001-dracut-initramfs-restore-fix-unpacking-with-early-mi.patch
Patch2: 0002-systemd-add-systemd-gpt-auto-generator.patch
Patch3: 0003-fcoe-wait-for-lldpad-to-be-ready.patch
Patch4: 0004-network-handle-ip-dhcp6-for-all-interfaces.patch
Patch5: 0005-lsinitrd.sh-prevent-construct.patch
Patch6: 0006-network-DCHPv6-set-valid_lft-and-preferred_lft.patch
Patch7: 0007-dm-add-dm-cache-modules.patch
Patch8: 0008-fcoe-workaround-fcoe-timing-issues.patch
Patch9: 0009-fstab-do-not-mount-and-fsck-from-fstab-if-using-syst.patch
Patch10: 0001-kernel-modules-Fix-storage-module-selection-for-sdhc.patch

# Revert dangerous behaviour change which breaks boot for multiple reporters
# https://bugzilla.redhat.com/show_bug.cgi?id=1084766
Patch100: 0001-Revert-Add-no-hostonly-cmdline-option-handling-for-g.patch
Patch101: 0002-Revert-Add-flag-to-toggle-hostonly-cmdline-storing-i.patch

BuildRequires: dash bash
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: asciidoc docbook-style-xsl docbook-dtds libxslt
BuildRequires: systemd-units

# no "provides", because dracut does not offer
# all functionality of the obsoleted packages
Obsoletes: mkinitrd <= 6.0.93
Obsoletes: mkinitrd-devel <= 6.0.93
Obsoletes: nash <= 6.0.93
Obsoletes: libbdevid-python <= 6.0.93

Provides: mkinitrd = 6.0.93 nash = 6.0.93

Obsoletes: dracut-kernel < 005
Provides:  dracut-kernel = %{version}-%{release}

Requires: bash
Requires: bzip2
Requires: coreutils
Requires: cpio
Requires: dash
Requires: filesystem >= 2.1.0
Requires: findutils
Requires: grep
Requires: gzip
Requires: kbd
Requires: mktemp >= 1.5-5
Requires: module-init-tools >= 3.7-9
Requires: sed
Requires: tar
Requires: udev

Requires: util-linux >= 2.16
Requires: initscripts >= 8.63-1
Requires: plymouth >= 0.8.4


%description
Dracut contains tools to create a bootable initramfs for 2.6 Linux kernels.
Unlike existing implementations, dracut does hard-code as little as possible
into the initramfs. Dracut contains various modules which are driven by the
event-based udev. Having root on MD, DM, LVM2, LUKS is supported as well as
NFS, iSCSI, NBD, FCoE with the dracut-network package.

%package network
Summary: Dracut modules to build a dracut initramfs with network support
Requires: %{name} = %{version}-%{release}
Requires: rpcbind
Requires: nbd
Requires: iproute
Requires: bridge-utils

Requires: iscsi-initiator-utils
Requires: nfs-utils
Requires: dhclient

Requires: net-tools
Requires: vconfig

Obsoletes: dracut-generic < 013-5m
Provides:  dracut-generic = %{version}-%{release}

%description network
This package requires everything which is needed to build a generic
all purpose initramfs with network support with dracut.

%package fips
Summary: Dracut modules to build a dracut initramfs with an integrity check
Requires: %{name} = %{version}-%{release}
Requires: hmaccalc
Requires: nss-softokn
Requires: nss-softokn-freebl

%description fips
This package requires everything which is needed to build an
all purpose initramfs with dracut, which does an integrity check.

%package fips-aesni
Summary: Dracut modules to build a dracut initramfs with an integrity check with aesni-intel
Requires: %{name}-fips = %{version}-%{release}

%description fips-aesni
This package requires everything which is needed to build an
all purpose initramfs with dracut, which does an integrity check 
and adds the aesni-intel kernel module.

%package caps
Summary: Dracut modules to build a dracut initramfs which drops capabilities
Requires: %{name} = %{version}-%{release}
Requires: libcap

%description caps
This package requires everything which is needed to build an
all purpose initramfs with dracut, which drops capabilities.

%package tools
Summary: Dracut tools to build the local initramfs
Requires: %{name} = %{version}-%{release}

%description tools
This package contains tools to assemble the local initrd and host configuration.

%prep
%setup -q -n %{name}-%{version}

%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1

%patch100 -p1
%patch101 -p1

%build
make all

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} \
     libdir=%{_prefix}/lib \
     sysconfdir=/etc mandir=%{_mandir} \
     systemdsystemunitdir=%{_unitdir}

echo "DRACUT_VERSION=%{version}-%{release}" > $RPM_BUILD_ROOT/%{dracutlibdir}/dracut-version.sh

# we do not support dash in the initramfs
rm -fr $RPM_BUILD_ROOT/%{dracutlibdir}/modules.d/00dash

# remove gentoo specific modules
rm -fr %{buildroot}%{dracutlibdir}/modules.d/50gensplash

mkdir -p $RPM_BUILD_ROOT/boot/dracut
mkdir -p $RPM_BUILD_ROOT/var/lib/dracut/overlay
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/log
touch $RPM_BUILD_ROOT%{_localstatedir}/log/dracut.log
mkdir -p $RPM_BUILD_ROOT%{_sharedstatedir}/initramfs

install -m 0644 dracut.conf.d/fedora.conf.example $RPM_BUILD_ROOT/etc/dracut.conf.d/01-dist.conf
install -m 0644 dracut.conf.d/fips.conf.example $RPM_BUILD_ROOT/etc/dracut.conf.d/40-fips.conf
rm -f $RPM_BUILD_ROOT%{_mandir}/man?/*suse*

sed -i 's|/etc/virc||g' dracut.conf.d/fedora.conf.example

install -m 0644 dracut.conf.d/fedora.conf.example %{buildroot}/etc/dracut.conf.d/01-dist.conf
install -m 0644 dracut.conf.d/fips.conf.example %{buildroot}/etc/dracut.conf.d/40-fips.conf

mkdir -p %{buildroot}/etc/logrotate.d
install -m 0644 dracut.logrotate %{buildroot}/etc/logrotate.d/dracut_log

install -m 0644 %{SOURCE1} %{buildroot}/etc/dracut.conf.d/momonga.conf

# create compat symlink
mkdir -p  %{buildroot}%{_sbindir}
ln -sr  %{buildroot}%{_bindir}/dracut  %{buildroot}%{_sbindir}/dracut

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,0755)
%doc README HACKING TODO COPYING AUTHORS NEWS dracut.html dracut.png dracut.svg
%{_bindir}/dracut
# compat /usr/sbin/dracut or /sbin/dracut
%{_sbindir}/dracut
%{_bindir}/mkinitrd
%{_bindir}/lsinitrd
%{_datadir}/bash-completion/completions/dracut
%{_datadir}/bash-completion/completions/lsinitrd
%dir %{dracutlibdir}
%{dracutlibdir}/dracut-functions
%{dracutlibdir}/dracut-functions.sh
%{dracutlibdir}/dracut-version.sh
%{dracutlibdir}/dracut-logger.sh
%{dracutlibdir}/dracut-initramfs-restore
%{dracutlibdir}/dracut-install
%{dracutlibdir}/skipcpio
%config(noreplace) /etc/dracut.conf
%config(noreplace) /etc/dracut.conf.d/momonga.conf
%dir /etc/dracut.conf.d
%config /etc/dracut.conf.d/01-dist.conf
%{_mandir}/man8/dracut.8*
%{_mandir}/man8/*service.8*
%{_mandir}/man7/dracut.kernel.7*
%{_mandir}/man7/dracut.cmdline.7*
%{_mandir}/man7/dracut.modules.7*
%{_mandir}/man7/dracut.bootup.7*
%{_mandir}/man5/dracut.conf.5*
%{_mandir}/man8/mkinitrd.8*
%{_mandir}/man1/lsinitrd.1*
%{dracutlibdir}/modules.d/00bash
%{dracutlibdir}/modules.d/00bootchart
%{dracutlibdir}/modules.d/00systemd-bootchart
%{dracutlibdir}/modules.d/03modsign
%{dracutlibdir}/modules.d/03rescue
%{dracutlibdir}/modules.d/04watchdog
%{dracutlibdir}/modules.d/05busybox
%{dracutlibdir}/modules.d/10i18n
%{dracutlibdir}/modules.d/30convertfs
%{dracutlibdir}/modules.d/45url-lib
%{dracutlibdir}/modules.d/50drm
%{dracutlibdir}/modules.d/50plymouth
%{dracutlibdir}/modules.d/80cms
##%%{dracutlibdir}/modules.d/90bcache
%{dracutlibdir}/modules.d/90btrfs
%{dracutlibdir}/modules.d/90crypt
%{dracutlibdir}/modules.d/90dm
%{dracutlibdir}/modules.d/90dmraid
%{dracutlibdir}/modules.d/90dmsquash-live
%{dracutlibdir}/modules.d/90kernel-modules
%{dracutlibdir}/modules.d/90lvm
%{dracutlibdir}/modules.d/90mdraid
%{dracutlibdir}/modules.d/90multipath
%{dracutlibdir}/modules.d/90qemu
%{dracutlibdir}/modules.d/90qemu-net
%{dracutlibdir}/modules.d/91crypt-gpg
%{dracutlibdir}/modules.d/91crypt-loop
%{dracutlibdir}/modules.d/95debug
%{dracutlibdir}/modules.d/95resume
%{dracutlibdir}/modules.d/95rootfs-block
%{dracutlibdir}/modules.d/95dasd
%{dracutlibdir}/modules.d/95dasd_mod
%{dracutlibdir}/modules.d/95dasd_rules
%{dracutlibdir}/modules.d/95fstab-sys
%{dracutlibdir}/modules.d/95zfcp
%{dracutlibdir}/modules.d/95zfcp_rules
%{dracutlibdir}/modules.d/95terminfo
%{dracutlibdir}/modules.d/95udev-rules
%{dracutlibdir}/modules.d/95virtfs
%{dracutlibdir}/modules.d/96securityfs
%{dracutlibdir}/modules.d/97biosdevname
%{dracutlibdir}/modules.d/97masterkey
%{dracutlibdir}/modules.d/98ecryptfs
%{dracutlibdir}/modules.d/98integrity
%{dracutlibdir}/modules.d/98pollcdrom
%{dracutlibdir}/modules.d/98selinux
%{dracutlibdir}/modules.d/98syslog
%{dracutlibdir}/modules.d/98usrmount
%{dracutlibdir}/modules.d/98systemd
%{dracutlibdir}/modules.d/99base
%{dracutlibdir}/modules.d/99img-lib
%{dracutlibdir}/modules.d/99fs-lib
%{dracutlibdir}/modules.d/99shutdown
%config(noreplace) /etc/logrotate.d/dracut_log
%attr(0644,root,root) %ghost %config(missingok,noreplace) %{_localstatedir}/log/dracut.log
%dir %{_sharedstatedir}/initramfs
%{_unitdir}/*.service
%{_unitdir}/*/*.service

%{_prefix}/lib/kernel/install.d/50-dracut.install
%{_prefix}/lib/kernel/install.d/51-dracut-rescue.install

%files network
%defattr(-,root,root,0755)
%{dracutlibdir}/modules.d/40network
%{dracutlibdir}/modules.d/95fcoe
%{dracutlibdir}/modules.d/95fcoe-uefi
%{dracutlibdir}/modules.d/95iscsi
%{dracutlibdir}/modules.d/90livenet
%{dracutlibdir}/modules.d/95cifs
%{dracutlibdir}/modules.d/95nbd
%{dracutlibdir}/modules.d/95nfs
%{dracutlibdir}/modules.d/95ssh-client
%{dracutlibdir}/modules.d/45ifcfg
%{dracutlibdir}/modules.d/95znet
%{dracutlibdir}/modules.d/99uefi-lib

%files fips
%defattr(-,root,root,0755)
%{dracutlibdir}/modules.d/01fips
%config(noreplace) /etc/dracut.conf.d/40-fips.conf

%files fips-aesni
%defattr(-,root,root,0755)
%doc COPYING
%{dracutlibdir}/modules.d/02fips-aesni

%files caps
%defattr(-,root,root,0755)
%{dracutlibdir}/modules.d/02caps

%files tools
%defattr(-,root,root,0755)
%{_mandir}/man8/dracut-catimages.8*
%{_bindir}/dracut-catimages
%dir /boot/dracut
%dir /var/lib/dracut
%dir /var/lib/dracut/overlay

%changelog
* Mon May 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (037-2m)
- import fedora patches

* Wed Mar 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (037-1m)
- update 37

* Sat Jan 18 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (034-5m)
- correct location for parse-uefifcoe.sh

* Thu Jan 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (034-4m)
- import from fedora patches

* Fri Dec 06 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (034-3m)
- import from fedora patches

* Sat Nov 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (034-2m)
- import from fedora patches

* Sun Oct 13 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (034-1m)
- update to 034

* Sat Sep 14 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (033-1m)
- update to 033

* Wed Aug 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (032-1m)
- update 032

* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (031-1m)
- update 031

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (030-1m)
- update 030

* Sun Jul  7 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (028-3m)
- set udevdir=/lib/udev in 01-dist.conf

* Thu Jun 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (028-2m)
- update patch

* Thu Jun 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (028-1m)
- update to 028

* Thu May  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (027-1m)
- update to 027

* Fri Mar  1 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (025-1m)
- update to 025

* Thu Feb 28 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (024-2m)
- update Patch100: dracut-024-momonga.patch for udev directory

* Wed Oct 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (024-1m)
- update 024 release

* Thu Aug  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (023-1m)
- update 023 release

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (022-1m)
- update 022 release

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (020-1m)
- update 020 release

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (019-1m)
- update 019 release

* Fri Apr  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (018-1m)
- update 018 release

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (018-0.1m)
- update 018-snapshot

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (017-1m)
- update 017
-- fix iscsi support
-- fix lsinitrd
-- better systemd support

* Thu Feb  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (015-1m)
- update 015

* Thu Jan 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (014-3m)
- mv command patch /usr/bin to /sbin

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (014-2m)
- use %%{dracutlibdir} instead of %%{_libdir}/dracut

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (014-1m)
- update 014

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-8m)
- update patches

* Fri Nov  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-7m)
- add patches

* Fri Oct 28 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (013-6m)
- modify BuildRequires and Requires

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-5m)
- add patches

* Mon Oct 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (013-4m)
- fix "W: Couldn't find SSL CA cert bundle; HTTPS won't work." error

* Thu Oct  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-3m)
- add patches

* Sun Sep 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-2m)
- add patches

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-1m)
- update 013

* Wed Jul 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (011-1m)
- update 011

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (010-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (010-1m)
- update 010

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (007-1m)
- update 007

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (006-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (006-2m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (006-1m)
- update 006

* Tue May 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (005-4m)
- adjustment Requires

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (005-3m)
- import fedora patches

* Tue Apr  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (005-2m)
- Provides: mkinitrd nash

* Sun Mar 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (005-1m)
- update 005

* Thu Jan 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (003-1m)
- Initial commit Momonga Linux. from Fedora

* Fri Nov 27 2009 Harald Hoyer <harald@redhat.com> 003-1
- version 003
