%global momorel 21

Summary: HTTP daemon for accessing Electronic Dictionaries
Name: ebhttpd
Version: 1.0.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Daemons
Source0: ftp://ftp.sra.co.jp/pub/net/www/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}-init
Patch0: %{name}-%{version}-eb4.patch
Patch1: %{name}-%{version}-canonicalize.patch
URL: http://www.sra.co.jp/people/m-kasahr/ebhttpd/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: zlib >= 1.1.4-5m
Requires(post): chkconfig info
Requires(preun): chkconfig info
BuildRequires: eb-devel >= 4.4.3

%description
EBHTTPD is a server for accessing CD-ROM books with HTTP on TCP.

%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1
%patch1 -p1 -b .canonicalize

perl -p -i -e "s/ndtpd/ebhttpd/" src/Makefile.* src/ebhtcontrol.c

%build
aclocal-1.7 -I m4
autoconf
automake-1.7
%configure
%make

%install
%makeinstall

install -d %{buildroot}%{_initscriptdir}
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/ebhttpd

# remove
rm -f %{buildroot}/etc/ndtpd.conf.sample
rm -f %{buildroot}/usr/share/info/dir
%preun
if [ "$1" = 0 ]; then
    /sbin/chkconfig --del ebhttpd
    [ -f /var/lock/subsys/ebhttpd ] && %{_initscriptdir}/ebhttpd stop
    /sbin/install-info --delete %{_infodir}/ebhttpd.info %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/ebhttpd-ja.info %{_infodir}/dir
fi

%post
# /sbin/chkconfig --add ebhttpd
[ -f /var/lock/subsys/ebhttpd ] && %{_initscriptdir}/ebhttpd restart
if [ "$1" = 1 ]; then
    /sbin/install-info %{_infodir}/ebhttpd.info %{_infodir}/dir
    /sbin/install-info %{_infodir}/ebhttpd-ja.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README* UPGRADE* ndtpd.conf.sample
%dir %attr(0755,nobody,nobody) %{_localstatedir}/ebhttpd
%{_infodir}/ebhttpd.info*
%{_infodir}/ebhttpd-ja.info*
%{_sbindir}/ebhtcheck
%{_sbindir}/ebhtcontrol
%{_sbindir}/ebhttpd
%config %{_initscriptdir}/ebhttpd

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-19m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-18m)
- rebuild against eb-4.4.3

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-17m)
- rebuild against eb-4.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-15m)
- rebuild against eb-4.4.1-3m

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-14m)
- rebuild against eb-4.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-13m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.6-12m)
- apply canonicalize.patch to enable build

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-11m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-10m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-9m)
- rebuild against eb-4.3.1
- License: GPLv2

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-8m)
- rebuild against eb-4.2

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-7m)
- rebuild against eb-4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-6m)
- rebuild against eb-4.1.2

* Sat Jun 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-5m)
- rebuild against eb-4.1

* Wed Mar 17 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.6-4m)
- revised spec for enabling rpm 4.2.

* Thu Jan 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-3m)
- revise for eb-4.0

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-2m)
- rebuild against eb-4.0

* Sun May 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-1m)

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-7m)
- rebuild against zlib 1.1.4-5m

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.5-6m)
- rebuild against eb-3.3.2

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.5-5m)
- rebuild against eb-3.3.1

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.5-4m)
- rebuild against eb-3.3

* Mon Jul 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.5-3m)
- change Require -> PreReq for chkconfig
- add PreReq: info
- Require: eb >= 3.2.2-2k
- BuildPreReq: eb-devel >= 3.2.2-2k

* Tue Apr 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.5-2k)

* Wed Feb 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.4-2k)
- build against eb-3.2
- temporary NoNoSource

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.0.3-2k)

* Fri Nov 30 2001 Toru Hoshina <t@kondara.org>
- (1.0.2-6k)
- comment out %chkconfig

* Mon Nov 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.0.2-4k)
- use %{_localstatedir}/ebhttpd (not ndtpd) by default

* Sat Sep  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.0.2-2k)
