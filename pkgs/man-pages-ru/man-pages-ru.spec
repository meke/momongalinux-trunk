%global momorel 9

Summary: Russian man pages from the Linux Documentation Project.
Name: man-pages-ru
Version: 0.97
Release: %{momorel}m%{?dist}
License: see "README"
Group: Documentation
#URL: http://alexm.here.ru/manpages-ru/
URL: http://linuxshare.ru/projects/trans/
Source: manpages-ru-%{version}.rh.tar.bz2
#Source2: http://alexm.here.ru/manpages-ru/download/manpages-ru-0.7.tar.gz
#Source3: http://linuxshare.ru/projects/trans/manpages-ru-%{version}.tar.bz2
Patch1: manpages-ru-0.97.rh-bash42.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: filesystem
BuildArch: noarch


%description
Manual pages from the Linux Documentation Project, translated into Russian.

%prep
%setup -q -n manpages-ru-%{version}.rh
%patch1 -p1 -b .bash42~ 

%build

%install
rm -fr %{buildroot}
for i in man*/*; do
	iconv -f koi8-r -t UTF-8 < $i > $i.new
	mv -f $i.new $i
done
mkdir -p %{buildroot}%{_mandir}
LC_ALL=ru_RU make install INSTALLPATH=%{buildroot}%{_mandir} \
    LANG_SUBDIR=ru
 

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc NEWS README
%{_mandir}/ru/*/*

%changelog
* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.97-9m)
- release a directory provided by filesystem

* Sun Apr 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-8m)
- fix build failure; add patch for bash-4.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-2m)
- rebuild against gcc43

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.97-1m)
- sync FC

* Tue Nov 11 2003 zunda <zunda at freshell.org>
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- You can hopefully translate the Russian README at
  http://www.translate.ru/text.asp?lang=en copying the last paragraph
  of the file through the clipboard of Mozilla or something.

* Thu Aug  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- s/Copyright/License/
- Own %%{_mandir}/ru

* Wed Apr  4 2001 Trond Eivind Glomsrod <teg@redhat.com>
- roff fixes

* Mon Feb  5 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Version 0.6

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
