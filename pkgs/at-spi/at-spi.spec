%global momorel 5

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Assistive Technology Service Provider Interface 
Name: at-spi
Version: 1.32.0
Release: %{momorel}m%{?dist}
URL: http://developer.gnome.org/projects/gap/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.32/%{name}-%{version}.tar.bz2
NoSource: 0

License: LGPLv2+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 
Requires: gtk2 >= 2.22.0
Requires: libbonobo >= 2.32.0
Requires: ORBit2 >= 2.14.18
Requires: atk >= 1.32.0
BuildRequires: pango-devel >= 1.28.1
BuildRequires: gtk2-devel >= 2.22.0
BuildRequires: libbonobo-devel >= 2.32.0
BuildRequires: ORBit2-devel >= 2.14.18
BuildRequires: atk-devel >= 1.32.0
BuildRequires: fontconfig
BuildRequires: gettext
BuildRequires: perl(XML::Parser)
BuildRequires: libX11-devel
BuildRequires: libXtst-devel
BuildRequires: libXi-devel
BuildRequires: libXevie-devel 
BuildRequires: libXt-devel 
BuildRequires: python-devel >= 2.7

%description
at-spi allows assistive technologies to access GTK-based
applications. Essentially it exposes the internals of applications for
automation, so tools such as screen readers, magnifiers, or even
scripting interfaces can query and interact with GUI controls.


%package devel
Summary: Development libraries and headers for at-spi
Group: Development/Libraries
Requires: %name = %{version}-%{release}
Requires: atk-devel
Requires: gtk2-devel
Requires: libbonobo-devel
Requires: ORBit2-devel

%description devel
Libraries and header files allowing compilation of apps that use at-spi.
 
%prep
%setup -q

%build
%configure --enable-gtk-doc \
	--disable-schemas-install \
	--disable-static
make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%post
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/at-spi.schemas > /dev/null 2>&1 || :

%pre
if [ "$1" -gt 1 ]; then
   export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
   gconftool-2 --makefile-uninstall-rule \
     %{_sysconfdir}/gconf/schemas/at-spi.schemas > /dev/null 2>&1 || :
fi

%preun
if [ "$1" -eq 0 ]; then
   export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
   gconftool-2 --makefile-uninstall-rule \
     %{_sysconfdir}/gconf/schemas/at-spi.schemas > /dev/null 2>&1 || :
fi

%postun
/sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README
%{_sysconfdir}/gconf/schemas/at-spi.schemas
%{_sysconfdir}/xdg/autostart/at-spi-registryd.desktop
%{_libdir}/libcspi.so.*
%{_libdir}/libloginhelper.so.*
%{_libdir}/libspi.so.*
%exclude %{_libdir}/lib*.la
%{_libdir}/orbit-2.0/*.so
%exclude %{_libdir}/orbit-2.0/*.la
%exclude %{_libdir}/gtk-2.0/modules/libatk-bridge.so
%exclude %{_libdir}/gtk-2.0/modules/libatk-bridge.la

%{_libdir}/bonobo/servers/Accessibility_Registry.server
%{_libexecdir}/at-spi-registryd
%{_datadir}/locale/*/*/*
%exclude %{python_sitearch}/pyatspi

%files devel
%defattr(-,root,root)
%{_datadir}/idl/at-spi-1.0
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/cspi-1.0.pc
%{_libdir}/pkgconfig/libloginhelper-1.0.pc
%{_libdir}/pkgconfig/libspi-1.0.pc
%dir %{_datadir}/gtk-doc/html/at-spi-cspi
%{_datadir}/gtk-doc/html/at-spi-cspi/*
%{_includedir}/at-spi-1.0

%changelog
* Sat May 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.32.0-5m)
- delete python module
- delete libatk-bridge module 

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.32.0-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32.0-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.32.0-1m)
- update to 1.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30.1-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.30.1-1m)
- update to 1.30.1

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.30.0-1m)
- update to 1.30.0

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.90-1m)
- update to 1.29.90

* Thu Jan 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.6-1m)
- update to 1.29.6

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.5-1m)
- update to 1.29.5

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28.1-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.1-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.1-1m)
- update to 1.28.1

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.0-1m)
- update to 1.28.0

* Wed Mar 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.26.0-1m)
- update to 1.26.0

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.25.92-1m)
- update to 1.25.92

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.25.5-1m)
- update to 1.25.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25.2-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.25.2-2m)
- update to 1.25.2
- rebuild against python-2.6.1-1m and PyQt4-4.4.4-2m

* Sat Dec 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.1-1m)
- update to 1.24.1

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.0-1m)
- update to 1.24.0

* Tue Jul  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.23.3-1m)
- update to 1.23.3 for at-spi-python used dogtail

* Tue Jun  3 2008 Matthias Clasen <mclasen@redhat.com> - 1.23.3-1
- Update to 1.23.3

* Mon May  5 2008 Matthias Clasen <mclasen@redhat.com> - 1.22.1-2
- Bump rev

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.1-1m)
- update to 1.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.0-1m)
- update to 1.22.0

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.0-1m)
- update to 1.20.0

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.1-2m)
- add -maxdepth 1 to del .la

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.18.1-1m)
- update to 1.18.1
 
* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.0-1m)
- update to 1.18.0
 
* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.17.2-1m)
- update to 2.17.2

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.17.0-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.17.0-1m)
- update to 1.17.0 (unstable)

* Tue Dec 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.14-1m)
- update to 1.7.14

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.12-1m)
- update to 1.7.12

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.11-3m)
- rebuild against gail-1.9.2

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.7.11-2m)
- rebuild against expat-2.0.0-1m

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.11-1m)
- update to 1.7.11

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.7-2m)
- delete libtool library

* Wed Apr  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-3m)
- comment out unnessesary autoreconf

* Fri Nov 18 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-2m)
- enable gtk-doc

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-1m)
- version up.
- GNOME 2.12.1 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.6.2-1m)
- version 1.6.2
- GNOME 2.8 Desktop

* Tue Dec 15 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.2-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (1.4.2-1m)
- version 1.4.2

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.0-1m)
- version 1.4.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Thu Jan 22 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.3.11-1m)
- version 1.3.11

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.8-1m)
- version 1.3.8

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.3.7-1m)
- version 1.3.7

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.6-1m)
- version 1.3.6

* Tue Jul 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.5-1m)
- version 1.3.5

* Tue Jun 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.4-1m)
- version 1.3.4

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.2-2m)
- delete "export _POSIX2_VERSION=199209"
- don't include -lhoge in CFLAGS...
"'

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.2-1m)
- version 1.3.2

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.1-2m)
- export _POSIX2_VERSION=199209 in %%build section

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.1-1m)
- version 1.3.1

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.9-1m)
- version 1.1.9

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.8-2m)
- rebuild against for XFree86-4.3.0

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.8-1m)
- version 1.1.8

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-1m)
- version 1.1.7

* Mon Dec 09 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-1m)
- version 1.1.6

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.5-1m)
- version 1.1.5

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.2-1m)
- version 1.1.2

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-14m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-13m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-12m)
- rebuild against for gnome-games-2.0.1.1
- rebuild against for gail-0.17
- rebuild against for pygtk-1.99.12
- rebuild against for librsvg-2.0.1
- rebuild against for eel-2.0.1
- rebuild against for nautilus-2.0.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-11m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-10k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-8k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-2k)
- version 1.0.1

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-6k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-4k)
- rebuild against for libglade-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-2k)
- version 1.0.0

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-16k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-14k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-12k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-10k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-8k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-6k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-4k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Thu Apr 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.1-2k)
- version 0.12.1

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.11.0-2k)
- version 0.11.0

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.0-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.0-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.0-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.0-2k)
- version 0.10.0

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-32k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-30k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-28k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-26k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-24k)
- rebuild against for gail-0.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-22k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-20k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-18k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-16k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-14k)
- rebuild against for libglade-1.99.8

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-12k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-8k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-6k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-4k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.8-2k)
- version 0.0.8

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.7-8k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.7-6k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.7-4k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.7-2k)
- version 0.0.7
* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-22k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-20k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-18k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-14k)
- rebuild against for libglade-1.99.6

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-8k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libgnomecanvas-1.110.0
- rebuild against for libbonobo-1.110.0

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-2k)
- version 0.0.6
- rebuild against for libIDL-0.7.3
- rebuild against for gail-0.6
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.5-4k)
- rebuild against for linc-0.1.15

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (0.0.3-0.020011228002k)
- port from RawHide
- cvs version because 0.0.4 tar ball is broken

* Mon Nov 26 2001 Havoc Pennington <hp@redhat.com>
- Initial build.
