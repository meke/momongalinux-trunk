%global momorel 16

Summary: Tux the Linux Penguin races down steep.
Name: tuxracer
Version: 0.61
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/tuxracer/tuxracer-0.61.tar.gz
Source1: http://dl.sourceforge.net/sourceforge/tuxracer/tuxracer-data-0.61.tar.gz
Source2: tuxracer.png
Source3: tuxracer.desktop
Patch0: tuxracer-0.60.1-GL.patch
Patch1: tuxracer-0.61-config.patch
Patch2: tuxracer-0.61-ia64.patch
Patch3: tuxracer-0.61-gcc33.patch
License: GPLv2+
Group: Amusements/Games
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: audiofile >= 0.2.1, SDL >= 1.2.7-9m
BuildRequires: audiofile-devel >= 0.2.1, SDL-devel >= 1.2.7-9m
BuildRequires: libvorbis >= 1.0-2m
BuildRequires: desktop-file-utils
BuildRequires: mesa-libGL-devel, mesa-libGLU-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXext-devel
BuildRequires: libXi-devel, libXmu-devel, libXt-devel
NoSource: 0
NoSource: 1

%description
Tux Racer lets you take on the role of Tux the Linux Penguin as he races down
steep, snow-covered mountains. Enter cups and compete to win the title!
Tux Racer includes a variety of options for gameplay, including the ability
to race courses in fog, at night, and under high winds.

%prep
rm -rf %{buildroot}

%setup -q -a 1
%patch0 -p1 -b .GL
%patch1 -p1 -b .config
%patch2 -p1 -b .ia64
%patch3 -p1 -b .gcc33

%build
CFLAGS="%{optflags} -DGLX_GLXEXT_LEGACY";export CFLAGS
%configure --with-data-dir=%{_datadir}/tuxracer --with-gl-libs=/usr/X11R6/lib
make %{?_smp_mflags} || make

%install
%makeinstall
mkdir -p %{buildroot}%{_datadir}/tuxracer
cp -rf tuxracer-data-0.61/* %{buildroot}%{_datadir}/tuxracer
cp %{SOURCE2}  %{buildroot}%{_datadir}/tuxracer
find %{buildroot}%{_datadir}/tuxracer -type f | xargs chmod 644

mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/applications/net-tuxracer.desktop

%clean
rm -rf %{buildroot}

%files
%defattr (-, root, root, 755)
%{_bindir}/tuxracer
%dir %{_datadir}/tuxracer
%{_datadir}/tuxracer/*
%{_datadir}/applications/*
%doc AUTHORS COPYING ChangeLog README contrib

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.61-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.61-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.61-14m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.61-13m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.61-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.61-11m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.61-10m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.61-9m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.61-8m)
- rebuild against gcc43

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.61-7m)
- rebuild against SDL-1.2.7-9m

* Tue Feb  8 2005 Toru Hoshina <t@momonga-linux.org>
- (0.61-6m)
- rebuild against new environment.

* Sun Apr  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.61-5m)
- fix file attribute.

* Fri Jun  6 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.61-4m)
- add '-DGLX_GLXEXT_LEGACY' (devel.ja:01727)

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (0.61-3m)
- added BuildPreReq: libvorbis-devel >= 1.0-2m

* Sat Aug 25 2001 Toru Hoshina <t@kondara.org>
- (0.61-2k)

* Wed Apr 18 2001 Kenichi Matsubara <m@digitalfactory.co.jp>
- (0.60.3-20k)
- rebuild against SDL-1.2.0.

* Tue Apr 10 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.60.3-18k)
- errased Glide3 Glide3-devel from tag

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.60.3-17k)
- rebuild against audiofile-0.2.1.

* Wed Mar 28 2001 Toru Hoshina <toru@df-usa.com>
- (0.60.3-15k)
- rebuild against SDL_mixer 1.1.0.

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.60.3-13k)
- added tuxracer.as for afterstep

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (0.60.3-11k)
- rebuild against gcc 2.96.

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.60.3-9k)
- imported tuxracer.desktop

* Thu Jan  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.60.3-7k)
- rebuild against Glide3

* Tue Dec 26 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.60.3-5k)
- fixed the bug reading /usr/local/share/tuxracer

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (0.60.3-5k)
- rebuild against audiofile-0.2.0.

* Mon Oct 30 2000 Toru Hoshina <toru@df-usa.com>
- 1st release for Kondara.
