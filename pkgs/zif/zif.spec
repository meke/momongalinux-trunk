%global momorel 1

Summary:   Simple wrapper for rpm and the package metadata
Name:      zif
Version:   0.3.6
Release:   %{momorel}m%{?dist}
License:   GPLv2+
Group:     System Environment/Base
URL:       http://github.com/hughsie/zif
#Source0:   http://www.packagekit.org/releases/%{name}-%{version}.tar.xz
Source0:   http://people.freedesktop.org/~hughsient/zif/releases/zif-%{version}.tar.xz
NoSource:  0

BuildRequires: glib2-devel
BuildRequires: rpm-devel
BuildRequires: sqlite-devel
BuildRequires: libsoup-devel
BuildRequires: libtool
BuildRequires: libarchive-devel >= 3.0.4
BuildRequires: docbook-utils
BuildRequires: gnome-doc-utils
BuildRequires: gtk-doc
BuildRequires: bzip2-devel
BuildRequires: zlib-devel
BuildRequires: gpgme-devel

%description
Zif is a simple yum-compatible library that only provides read-only
access to the rpm database and the Fedora metadata for PackageKit.

Zif is not designed as a replacement to yum, nor to be used by end users.

%package devel
Summary: GLib Libraries and headers for zif
Requires: %{name} = %{version}-%{release}
Requires: bzip2-devel
Requires: zlib-devel

%description devel
GLib headers and libraries for zif.

%prep
%setup -q

%build
#  zif-0.2.8  requires this
export LDFLAGS=`pkg-config gmodule-2.0 --libs`
%configure \
        --enable-gtk-doc \
        --disable-static \
	--disable-gtk-doc \
        --disable-dependency-tracking

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/libzif*.la

%find_lang Zif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f Zif.lang
%defattr(-,root,root,-)
%doc README AUTHORS NEWS COPYING
%{_bindir}/zif
%{_libdir}/*libzif*.so.*
%{_libdir}/girepository-1.0/Zif-1.0.typelib
%{_mandir}/man1/*.1*
%{_sysconfdir}/bash_completion.d/zif-completion.bash
%{_sysconfdir}/zif/zif.conf
%{_libdir}/girepository-1.0/*.typelib
%ghost %verify(not md5 size mtime) %{_localstatedir}/lib/zif/history.db

%files devel
%defattr(-,root,root,-)
%{_libdir}/libzif*.so
%{_libdir}/pkgconfig/zif.pc
%dir %{_includedir}/libzif
%{_includedir}/libzif/*.h
%{_datadir}/gtk-doc/html/zif
%{_datadir}/gir-1.0/*.gir

%changelog
* Thu Jul 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.6-1m)
- update 0.3.6

* Wed Oct 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.3-1m)
- update 0.3.3

* Sat Oct 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-2m)
- disable gtk-doc for a while

* Sat Jun 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-1m)
- update 0.3.0

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8-3m)
- fix build failure with glib 2.33+

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.8-2m)
- rebuild against libarchive-3.0.4

* Mon Mar 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.8-1m)
- update 0.2.8

* Tue Feb 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.7-1m)
- update 0.2.7

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.6-1m)
- update 0.2.6

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.5-1m)
- update 0.2.5

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-1m)
- update 0.2.3

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.2-1m)
- update 0.2.2

* Sat Jul 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-1m)
- update 0.2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.5-3m)
- rebuild for new GCC 4.6

* Sun Mar 06 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.1.5-2m)
- add BR: gpgme-devel

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.5-1m)
- update 0.1.5

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.3-1m)
- update 0.1.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-3m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.2-2m)
- change gtk-doc path name

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.2-1m)
- Initial commit Momonga Linux. import from Fedora

* Mon Nov 01 2010 Richard Hughes <richard@hughsie.com> 0.1.2-1
- New upstream release with many bugfixes and performance improvements.

* Tue Oct 05 2010 Parag Nemade <paragn AT fedoraproject.org> 0.1.1-2
- drop the ldconfig and pkgconfig as a Requires.

* Mon Oct 04 2010 Richard Hughes <richard@hughsie.com> 0.1.1-1
- New upstream release with many bugfixes and performance improvements.

* Fri Oct 01 2010 Richard Hughes <richard@hughsie.com> 0.1.0-4
- Take ownership of the gtk-doc directory, and use another macro.

* Fri Oct 01 2010 Richard Hughes <richard@hughsie.com> 0.1.0-3
- Remove group from devel subpackage, and make the description better.

* Fri Oct 01 2010 Richard Hughes <richard@hughsie.com> 0.1.0-2
- Address some review comments, many thanks.

* Wed Sep 08 2010 Richard Hughes <richard@hughsie.com> 0.1.0-1
- Initial package for review.
