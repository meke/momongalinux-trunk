%global momorel 5

Summary: give fixed name to dynamic devices
Name: scsidev
Version: 2.37
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
Source0: http://www.garloff.de/kurt/linux/scsidev/scsidev-%{version}.tar.gz
NoSource: 0
Patch0: scsidev-2.37-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
 This program scans the SCSI bus and creates device nodes in
/dev/scsi/, which have a naming corresponding to their SCSI IDs
and LUNs, just like with devfs. (The devfs has no notion of
hostadapter IDs, scsidev is better here.) Furthermore, the devices
are inquired to tell their names and serial numbers. Those can be
compared with the entries in a database /etc/scsi.alias and device
nodes corresponding to these entries are being built. So, this
will even work if you change the SCSI IDs of a device, where
the devfs approach would fail. 

%prep
%setup -q
%patch0 -p1 -b .glibc210

%build
%configure
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_mandir}/man8
mkdir -p %{buildroot}/dev/scsi

%{__install} -m 755 scsidev %{buildroot}/sbin
%{__install} -m 644 scsidev.8 %{buildroot}%{_mandir}/man8
%{__install} -m 644 scsi.alias %{buildroot}/etc/scsi.alias.sample

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/etc/scsi.alias.sample
/sbin/scsidev
%{_mandir}/man8/scsidev.8*
%dir /dev/scsi
%doc CHANGES COPYING README VERSION TODO

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.37-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.37-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.37-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.37-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.37-1m)
- update to 2.37

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.28-2m)
- rebuild against gcc43

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.28-1m)
- update to 2.28

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0-1m)
- fix rel

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0-0.1.1m)
- inital version for momonga
