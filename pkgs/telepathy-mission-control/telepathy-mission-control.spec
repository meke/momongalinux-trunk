%global momorel 1

Summary: Mission Control, or MC, for telepathy 
Name: telepathy-mission-control
Version: 5.14.1
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://sourceforge.net/projects/mission-control/
Source0: http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: python
BuildRequires: libxslt >= 1.1.24
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.18.1
BuildRequires: dbus-devel >= 1.2.3
BuildRequires: dbus-glib-devel >= 0.76
BuildRequires: telepathy-glib-devel >= 0.19.10
BuildRequires: libtelepathy-devel >= 0.3.3
BuildRequires: GConf2-devel >= 2.24.0
BuildRequires: gcr-devel >= 2.24.0

%description
Mission Control, or MC, is a telepathy (chat and voip framework)
component providing a way for "end-user" applications to abstract some
details of low level telepathy components such as connection managers.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: dbus-devel
Requires: dbus-glib-devel
Requires: telepathy-glib-devel
Requires: libtelepathy-devel
Requires: GConf2-devel
Requires: gcr-devel

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --enable-static=no \
    --enable-gtk-doc \
    --enable-plugins \
    --enable-gnome-keyring \
    LIBS="-ltelepathy-glib -lgobject-2.0 -lglib-2.0 -ldbus-glib-1 -ldbus-1 -lgnome-keyring"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog NEWS
%{_bindir}/mc-tool
%{_bindir}/mc-wait-for-name
%{_libdir}/libmission-control-plugins.so.*
%exclude %{_libdir}/*.la

%{_libexecdir}/mission-control-5
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.AccountManager.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.MissionControl5.service
%{_datadir}/glib-2.0/schemas/im.telepathy.MissionControl.FromEmpathy.gschema.xml

%{_mandir}/man1/mc-*.1.*
%{_mandir}/man8/mission-control-5.8.*


%files devel
%defattr(-, root, root)
%{_libdir}/libmission-control-plugins.so
%{_libdir}/pkgconfig/mission-control-plugins.pc
%{_includedir}/mission-control-5.5
%doc %{_datadir}/gtk-doc/html/mission-control-plugins

%changelog
* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.14.1-1m)
- update to 5.14.1

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.14.0-1m)
- update to 5.14.0

* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.13.2-1m)
- update to 5.13.2

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.12.1-1m)
- update to 5.12.1

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.12.0-1m)
- update to 5.12.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.11.0-2m)
- rebuild for glib 2.33.2

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.0-1m)
- update to 5.11.0

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.10.0-2m)
- add patch0 for Empathy-3.2.2

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10.0-1m)
- update to 5.10.0

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.3-1m)
- update to 5.9.3

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.2-1m)
- update to 5.9.2

* Fri Jul 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.1-1m)
- update to 5.9.1

* Thu May 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.0-1m)
- update to 5.9.0

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.11-1m)
- update to 5.7.11

* Mon May  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.10-1m)
- update to 5.7.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7.9-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.9-1m)
- update to 5.7.9

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.7-1m)
- update to 5.7.7

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.6-1m)
- update to 5.7.6

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.4-1m)
- update to 5.7.4

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.2-1m)
- update to 5.7.2

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-1m)
- update to 5.7.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.1-1m)
- update to 5.6.1

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.0-1m)
- update to 5.6.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.5.3-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.3-1m)
- update to 5.5.3

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.1-2m)
- rebuild with gtk-doc-1.14

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.1-1m)
- update to 5.5.1

* Fri Jun 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.3-1m)
- update to 5.4.3

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3.4-2m)
- explicitly link some libraries

* Sun Apr 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.3.4-1m)
- update to 5.3.4

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.3.2-1m)
- update to 5.3.2

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.3.1-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.3.1-1m)
- update to 5.3.1

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.67-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.15-1m)
- initial build
