%global momorel 2

Summary: X.Org X11 libXft runtime library
Name: libXft
Version: 2.3.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-%{version}-freetype.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel >= 7.2
BuildRequires: libX11-devel >= 1.0.99.1
BuildRequires: libXrender-devel
BuildRequires: freetype-devel >= 2.5.3
BuildRequires: fontconfig-devel >= 2.4.2-13m

Requires: fontconfig >= 2.4.2-13m

#Obsoletes: XFree86-libs, xorg-x11-libs

%description
X.Org X11 libXft runtime library

%package devel
Summary: X.Org X11 libXft development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

Requires: xorg-x11-proto-devel
Requires: libXrender-devel
Requires: fontconfig-devel
Requires: freetype-devel

#Obsoletes: XFree86-devel, xorg-x11-devel

%description devel
X.Org X11 libXft development package

%prep
%setup -q
%patch0 -p1 -b .freetype

# Disable static library creation by default.
%define with_static 0

%build

%configure \
%if ! %{with_static}
	--disable-static
%endif
%make

%install

rm -rf --preserve-root %{buildroot}
%makeinstall

# FIXME: There's no real good reason to ship these anymore, as pkg-config
# is the official way to detect flags, etc. now.
rm -f $RPM_BUILD_ROOT%{_bindir}/xft-config
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/xft-config*

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README INSTALL ChangeLog
%{_libdir}/libXft.so.2
%{_libdir}/libXft.so.2.*

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/X11/Xft
%{_includedir}/X11/Xft/Xft.h
%{_includedir}/X11/Xft/XftCompat.h
%if %{with_static}
%{_libdir}/libXft.a
%endif
%{_libdir}/libXft.so
%{_libdir}/pkgconfig/xft.pc
%{_mandir}/man3/Xft.3*

%changelog
* Mon Apr 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-2m)
- enable to build with freetype-2.5.3

* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.0-1m)
- update 2.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update 2.2.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.14-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.14-1m)
- update 2.1.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.13-2m)
- rebuild against rpm-4.6

* Thu Jul  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.13-1m)
- update 2.1.13

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.12-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.12-4m)
- %%NoSource -> NoSource

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.12-3m)
- remove xorg-momonga-redhat-embeddedbitmap.patch
- remove fcpackage.2_1-Xft-2.1.8.2-fix-cjk-20060305.diff
- Requires: fontconfig >= 2.4.2-13m, cache should be cleared

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.12-2m)
- libXft-devel Requires: freetype2-devel -> freetype-devel

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.12-1m)
- rewind 2.1.12

* Sat Jun  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.8.2-5m)
- rewind 2.1.8.2

* Sat Jun  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.9-2m)
- add patch0 patch1 (motodo-ri)

* Sat Jun  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.9-1m)
- update to 2.1.9
- delete patch0 patch1

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.8.2-4m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.8.2-3.3m)
- add Patch0, Patch1

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.8.2-3.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.1.8.2-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.1.8.2-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Feb  2 2006 Mike A. Harris <mharris@redhat.com> 2.1.8.2-3
- Added missing dependencies to devel subpackage to fix (#176744)

* Mon Jan 23 2006 Mike A. Harris <mharris@redhat.com> 2.1.8.2-2
- Bumped and rebuilt

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 2.1.8.2-1
- Updated libXft to version 2.1.8.2 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 2.1.8.1-1
- Updated libXft to version 2.1.8.1 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.
- Added "Requires: libXrender-devel" to -devel subpackage for (#175465)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 2.1.8-2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 2.1.8-1
- Updated libXft to version 2.1.8 from X11R7 RC1

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 2.1.7-5
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro
- Fix BuildRequires to use new style X library package names

* Sun Sep 4 2005 Mike A. Harris <mharris@redhat.com> 2.1.7-4
- Added "BuildRequires: fontconfig-devel >= 2.2" dependency that was
  previously missed.  Also added "Requires: fontconfig >= 2.2" runtime
  dependency.
- Added missing defattr to devel subpackage.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 2.1.7-3
- Added freetype-devel build dependency.

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 2.1.7-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 2.1.7-1
- Initial build.
