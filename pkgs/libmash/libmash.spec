%global momorel 1
Summary: A library for using real 3D models within a Clutter scene
Name: libmash
Version: 0.2.0
Release: %{momorel}m%{?dist}
URL: http://wiki.clutter-project.org/wiki/Mash
Source0: http://source.clutter-project.org/sources/mash/0.2/mash-%{version}.tar.xz
NoSource: 0

# Already sent upstream for review,
# see http://lists.clutter-project.org/pipermail/clutter-devel-list/2011-March/000196.html
Patch0:		0001-Use-the-system-version-of-rply-if-available.patch

License: LGPLv2+
Group: System Environment/Libraries
BuildRequires: libtool
BuildRequires: glib2-devel >= 2.16
BuildRequires: clutter-devel
BuildRequires: gtk-doc
BuildRequires: rply-devel

# Do not BR: mx-devel, as the lighting example isn't actually installed

%description
Mash is a small library for using real 3D models within a Clutter
scene. Models can be exported from Blender or other 3D modeling
software as PLY files and then used as actors. It also supports a
lighting model with animatable lights.

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains libraries and header files needed for
development of programs using %{name}.

%prep
%setup -q -n mash-%{version}
#%patch0 -p1 -b .use-system-rply

%build
autoconf
%configure
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README COPYING.LIB NEWS AUTHORS
%{_libdir}/libmash-0.2.so.*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/*.typelib

%files devel
%defattr(-,root,root)
%dir %{_includedir}/mash-0.2
%{_includedir}/mash-0.2/*
%{_libdir}/libmash-0.2.so
%{_libdir}/pkgconfig/mash-0.2.pc
%dir %{_datadir}/gtk-doc/html/mash
%{_datadir}/gtk-doc/html/mash/*
%{_datadir}/gir-1.0/*.gir
%dir %{_datadir}/gir-1.0

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-1m)
- import from fedora

