%global momorel 12
%global srcname %{name}-%{version}

Name: inkscape
Summary: A Vector Drawing Application
Version: 0.48.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://inkscape.org/
Source0: http://dl.sourceforge.net/sourceforge/inkscape/%{srcname}.tar.bz2
NoSource: 0
Patch0: inkscape-0.48.2-types.patch
Patch1: inkscape-0.48.4-spuriouscomma.patch

Patch50: 1255830-fix-freetype-includes-backport-0.48.x-v1.diff

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: GConf2-devel
BuildRequires: ImageMagick-devel >= 6.8.8.10
BuildRequires: ImageMagick-c++-devel >= 6.8.8.10
BuildRequires: ORBit2-devel
BuildRequires: atk-devel
BuildRequires: boost-devel
BuildRequires: cairo-devel
BuildRequires: cairomm-devel
BuildRequires: desktop-file-utils
BuildRequires: dos2unix
BuildRequires: enchant-devel
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel >= 2.5.3
BuildRequires: gc-devel >= 6.7
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: glibmm-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: gsl-devel
BuildRequires: gtk2-devel
BuildRequires: gtkmm-devel >= 2.8.8
BuildRequires: gtkspell-devel
BuildRequires: intltool
BuildRequires: lcms-devel >= 1.13
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libpng-devel
BuildRequires: libsigc++-devel >= 2.1.1
BuildRequires: libwpg-devel
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: libxslt-devel >= 1.1.15
BuildRequires: loudmouth-devel
BuildRequires: openssl-devel
BuildRequires: pango-devel
BuildRequires: pkgconfig
BuildRequires: poppler-glib-devel >= 0.24.3
BuildRequires: popt-devel
BuildRequires: zlib-devel
BuildRequires: libgnomeprintui-devel >= 2.12.1
BuildRequires: perl-ExtUtils-Embed
BuildRequires: perl-SVG >= 2.33
BuildRequires: perl-XML-Parser
BuildRequires: perl-libxml-enno
BuildRequires: python-devel >= 2.7
BuildRequires: perl-devel >= 5.10.1
Requires: pstoedit
Requires: perl-SVG-Parser
Requires: perl-libxml-enno

Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

%description
Inkscape is an SVG-based generic vector-drawing program for Linux/Unix/Windows/Mac.

%prep
%setup -q -n %{srcname}
%patch0 -p1 -b .types
%patch1 -p0 -b .comma~

%patch50 -p0 -b .freetype

# https://bugs.launchpad.net/inkscape/+bug/314381
# A couple of files have executable bits set,
# despite not being executable
find . -name '*.cpp' | xargs chmod -x
find . -name '*.h' | xargs chmod -x
find share/extensions -name '*.py' | xargs chmod -x

# Fix end of line encodings
dos2unix -k -q share/extensions/*.py

# patch3 updates Makefile.am
autoreconf -vfi

%build
%configure \
  --disable-dependency-tracking	\
  --enable-static=no \
  --enable-lcms \
  --enable-poppler-cairo \
  --enable-inkboard \
  --with-xft \
  --with-gnome-vfs \
  --with-inkjar \
  --with-perl \
  --with-python \
%ifnarch %{ix86}
  --disable-mmx \
%endif
  LIBS="-lX11" CPPFLAGS=-DGLIB_COMPILATION

%make

%install
rm -rf %{buildroot}
%makeinstall
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

desktop-file-install --vendor= --delete-original \
    --dir %{buildroot}%{_datadir}/applications \
    %{buildroot}%{_datadir}/applications/%{name}.desktop

# No skencil anymore
rm -f %{buildroot}%{_datadir}/%{name}/extensions/sk2svg.sh

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
update-desktop-database %{_datadir}/applications > /dev/null 2>&1 || :

%postun
update-desktop-database %{_datadir}/applications > /dev/null 2>&1 || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/inkscape
%{_bindir}/inkview
%dir %{_datadir}/inkscape
%{_datadir}/inkscape/clipart
%{_datadir}/inkscape/extensions
%{_datadir}/inkscape/filters
%{_datadir}/inkscape/fonts
%{_datadir}/inkscape/gradients
%{_datadir}/inkscape/icons
%{_datadir}/inkscape/keys
%{_datadir}/inkscape/markers
%{_datadir}/inkscape/palettes
%{_datadir}/inkscape/patterns
%{_datadir}/inkscape/screens
%{_datadir}/inkscape/templates
%{_datadir}/inkscape/ui
%{_datadir}/inkscape/tutorials
%{_datadir}/inkscape/examples
%{_datadir}/applications/inkscape.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/icon-theme.cache
%{_datadir}/pixmaps/inkscape.png
%{_mandir}/man1/inkscape*.1*
%{_mandir}/man1/inkview.1*
%{_mandir}/*/man1/inkscape*.1*

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-12m)
- rebuild against ImageMagick-6.8.8.10

* Tue Mar 25 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-11m)
- enable to build with freetype-2.5

* Tue Mar 11 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.4-10m)
- fix build failure for findutils-4.5.12

* Thu Jan 16 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.4-9m)
- import patch from fedora

* Fri Nov  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-8m)
- rebuild against poppler-0.24.3

* Fri Sep 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.48.4-7m)
- rebuild against ImageMagick-6.8.6.10

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-6m)
- rebuild against ImageMagick-6.8.6

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-5m)
- rebuild against poppler-0.22.3

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-4m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-3m)
- rebuild against ImageMagick-6.8.2.10

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.48.4-2m)
- rebuild against poppler-0.22.0

* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.4-1m)
- update to 0.48.4

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.2-12m)
- rebuild against ImageMagick-6.8.0.10

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.2-11m)
- rebuild against poppler-0.20.5

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.2-10m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.48.2-9m)
- rebuild for boost 1.50.0

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.48.2-8m)
- rebuild against poppler-0.20.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.2-7m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.48.2-6m)
- build fix

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.2-5m)
- rebuild for boost-1.48.0

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.2-4m)
- rebuild against poppler-0.18.2

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.2-3m)
- rebuild against ImageMagick-6.7.2.10

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.48.2-2m)
- rebuild against poppler-0.18.0

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.48.2-1m)
- update to 0.48.2

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.48.1-5m)
- rebuild against poppler-0.17.4

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (0.48.1-4m)
- rebuild for boost

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.48.1-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.1-2m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.1-1m)
- update to 0.48.1
- rebuild against poppler-0.16.4

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.0-3m)
- rebuild against boost-1.46.1
- add patch for gcc46

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48.0-2m)
- rebuild against boost-1.46.0

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.0-1m)
- update to 0.48.0

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-12m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-11m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-10m)
- rebuild against boost-1.44.0

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-9m)
- add patch for gcc45

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.47-8m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.47-7m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-6m)
- fix build

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-5m)
- rebuild against ImageMagick-6.6.2.10

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.47-4m)
- rebuild against poppler-0.14.0

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-3m)
- change BuildRequires: from poppler-devel to poppler-glib-devel

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-2m)
- rebuild against ImageMagick-6.5.9.10

* Sat Jan  9 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-0.3.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-0.3.1m)
- update to 0.47pre3

* Sat Sep 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.47-0.2.2m)
- rebuild against poppler-0.12.0

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-0.2.1m)
- update to 0.47pre2

* Thu Sep  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.47-0.1.2m)
- rebuild against perl-5.10.1

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-0.1.1m)
- update to 0.47pre1

* Fri Jun 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-0.0.1m)
- fix versioning

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47pre0-1m)
- update to 0.47pre0

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-14m)
- rebuild against openssl-0.9.8k

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-13m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-12m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-11m)
- rebuild against python-2.6.1

* Sun Nov  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46-10m)
- add patch for libpng's setjmp issue 

* Sat Oct 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-9m)
- import Patch10-18 from Fedora devel (0.46-6)
- rebuild against poppler-0.10.0

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-8m)
- rebuild against ImageMagick-6.4.2.1

* Fri Jun 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.46-7m)
- add patch from 2ch http://pc11.2ch.net/test/read.cgi/linux/1188293074/258
- add patch from gentoo (for poppler-0.8.3)

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-6m)
- rebuild against openssl-0.9.8h-1m

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-5m)
- rebuild against poppler-0.8.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.46-4m)
- rebuild against gcc43

* Tue Mar 18 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.46-3m)
- fix script name of uniconvertor in {cdr,wmf}_input.inx

* Sun Mar 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-2m)
- remove --with-perl from configure for the moment
  (to enable build on trunk Revision: 22801)

* Fri Mar 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.46-1m)
- update to 0.46
- remove merged patches

* Sat Feb 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.45.1+0.46pre1-1m)
- sync with Fedora devel, but --without-perl
- * Thu Feb 14 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.45.1+0.46pre1-4
- - Tolerate recoverable errors in OCAL feeds
- - Fix OCAL insecure temporary file usage (#432807)
- 
- * Wed Feb 13 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.45.1+0.46pre1-3
- - Fix crash when adding text objects (#432220)
- 
- * Thu Feb 07 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.45.1+0.46pre1-2
- - Build with gcc-4.3
- 
- * Wed Feb 06 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.45.1+0.46pre1-1
- - 0.46 prerelease
- - Minor cosmetic changes to satisfy the QA script
- - Dependency on Boost
- - Inkboard is not optional
- - Merge from Denis Leroy's svn16571 snapshot:
- - Require specific gtkmm24-devel versions
- - enable-poppler-cairo
- - No longer BuildRequire libsigc++20-devel
- License: GPLv2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.45.1-4m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.45.1-3m)
- fix build on new sigc++

* Fri Jun 15 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.45.1-2m)
- import patchs from Fedora (inkscape-0.44.1-psinput.patch, inkscape-0.45-python.patch)
- Requires: pstoedit for PS and EPS importing

* Mon Apr 09 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.45.1-1m)
- update to 0.45.1

* Wed Feb 21 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Tue Dec 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.44.1-2m)
- add static_cast patch
-- support : gcc-4.1.1-20061220 

* Sat Sep 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.44.1-1m)
- update to 0.44.1

* Tue Jun 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.44-1m)
- update to 0.44
- tempolary disabled perl script support because of a link error.

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.43-2m)
- rebuild against gtkmm-2.8.8

* Tue Apr 18 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.43-4m)
- revised an optimization to use "-O2" etc.

* Mon Jan 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.43-3m)
- add CXXFLAGS="-fno-strict-aliasing"

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-2m)
- add gcc-4.1 patch.
-- inkscape-0.43-gcc41.patch

* Fri Nov 25 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Tue Nov 15 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.42.2-2m)
- rebuild against pkgconfig-0.19

* Sat Sep 24 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.42.2-1m)
- update to 0.42.2

* Thu Jul 28 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.42-2m)
- --with-python

* Wed Jul 27 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.42-1m)
- Update to 0.42

* Mon May 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.41-1m)
- initial import to Momonga

* Wed Nov 24 2004 Kees Cook <kees@outflux.net>
- Added SMP flags so I can build faster
- Added static build capability

* Sat Jul 17 2004 Bryce W. Harrington <bryce@bryceharrington.com>
- Removing _libdir and TODO, updating description

* Thu May 01 2003 Christian Schaller <uraeus@gnome.org>
- Fix up the spec file for current release

* Mon Sep 23 2002 Dag Wieers <dag@wieers.com>
- Update to 0.2.6

* Thu Sep 12 2002 Dag Wieers <dag@wieers.com>
- Update to 0.2.5
- Changed SPEC to benefit from macros
