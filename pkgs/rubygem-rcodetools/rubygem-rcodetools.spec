%global momorel 6

# Generated from rcodetools-0.8.1.0.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname rcodetools
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: rcodetools is a collection of Ruby code manipulation tools
Name: rubygem-%{gemname}
Version: 0.8.5.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://eigenclass.org/hiki.rb?rcodetools
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
rcodetools is a collection of Ruby code manipulation tools.  It includes
xmpfilter and editor-independent Ruby development helper tools, as well as
emacs and vim interfaces.  Currently, rcodetools comprises: * xmpfilter:
Automagic Test::Unit assertions/RSpec expectations and code annotations *
rct-complete: Accurate method/class/constant etc. completions * rct-doc:
Document browsing and code navigator * rct-meth-args: Precise method info
(meta-prog. aware) and TAGS generation


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x
sed -i "s|/usr/local/bin/ruby|/usr/bin/ruby|" %{buildroot}%{geminstdir}/bin/*
sed -i "s|/usr/bin/ruby18|/usr/bin/ruby|" %{buildroot}%{geminstdir}/bin/*
sed -i "s|/usr/bin/ruby18|/usr/bin/ruby|" %{buildroot}/%{_bindir}/*

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/rct-complete
%{_bindir}/rct-doc
%{_bindir}/xmpfilter
%{_bindir}/rct-meth-args
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Nov 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.5.0-6m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.5.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.5.0-1m)
- update 0.8.5.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1.0-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1.0-1m)
- Initial package for Momonga Linux
