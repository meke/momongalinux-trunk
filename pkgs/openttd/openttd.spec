%global momorel 1
%global relver %{version}
%global srcname %{name}-%{version}-source
%global dirname %{name}-%{version}

Summary:  An Open Source clone of Chris Sawyer's Transport Tycoon Deluxe
Name:     openttd
Version:  1.3.3
Release:  %{momorel}m%{?dist}
License:  GPLv2
URL:      http://www.openttd.org/
Group:    Amusements/Games
Source0:  http://us.binaries.openttd.org/binaries/releases/%{relver}/%{srcname}.tar.xz
NoSource: 0
BuildRequires: allegro-devel >= 4.4.0.1
BuildRequires: freetype-devel
BuildRequires: SDL-devel
BuildRequires: desktop-file-utils
BuildRequires: libicu-devel >= 52
BuildRequires: fontconfig-devel
BuildRequires: lzo-devel
BuildRequires: xz-devel
BuildRequires: grfcodec
Requires: gtk2
Requires: fontconfig
Requires: libicu
Requires: libpng
Requires: SDL
Requires: zlib
Requires: freetype
Requires: lzo
Requires: openttd-opengfx >= 0.4.4
Requires: openttd-opensfx
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
OpenTTD is a reimplementation of the Microprose game "Transport Tycoon Deluxe"
with lots of new features and enhancements. 

OpenTTD is licensed under the GNU General Public License version 2.0. For more
information, see the file 'COPYING' included with every release and source    
download of the game.  

%prep
%setup -q -n %{dirname}

%build
./configure \
	--prefix-dir=%{_prefix} \
	--binary-dir=bin \
	--binary-name=%{name} \
	--menu-name=OpenTTD \
	--data-dir=share/games/%{name} \
	--doc-dir=share/doc/%{name}-%{version} \
	--icon-dir=share/pixmaps \
	--icon-theme-dir=share/icons/hicolor \
	--man-dir=share/man/man6 \
	--menu-dir=share/applications \
	--enable-debug=0 \
	--menu-group="Game;" \
	--with-sdl \
	--with-zlib \
	--with-png \
	--with-freetype \
	--with-fontconfig \
	--with-icu \
	--enable-strip

%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall INSTALL_DIR=%{buildroot}

desktop-file-install \
	--delete-original \
	--vendor="" \
	--remove-key Version \
	--dir="%{buildroot}/%{_datadir}/applications/" \
	"%{buildroot}/%{_datadir}/applications/%{name}.desktop"

%clean
rm -rf --preserve-root %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files
%defattr(-,root,root)
%dir %{_datadir}/doc/%{name}-%{version}
%dir %{_datadir}/games/%{name}
%{_bindir}/%{name}
%{_datadir}/doc/%{name}-%{version}/*
%{_datadir}/games/%{name}/*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/*
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_mandir}/man6/%{name}.6*

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Sat Apr 20 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0
- change URL

* Tue Aug  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-3m)
- [SECURITY] CVE-2012-3436

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-2m)
- fix requires

* Mon Jun 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-1m)
- [SECURITY] CVE-2012-0049
- update to 1.1.5

* Sat Dec 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Wed Sep 21 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Thu Aug 18 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-2m)
- rebuild against icu-4.6

* Thu Jun 23 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1
- remove japanese patch
- remove BuildRequires: nforenum (merged to grfcodec)
- add    BuildRequires: xz-devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-3m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.0.5-2m)
- Add requires: openttd-opensfx

* Mon Dec  6 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.0.5-1m)
- [SECURITY] CVE-2010-4168
- update to 1.0.5
- Add Requires: lzo
- Add BuildRequires: lzo-devel nforenum grfcodec
- Add patch for japanese.txt (translation)
- fix %%description. We now provide openttd-opengfx for graphics.
- You can also play OpenTTD with original graphics.

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-0.1.5m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-0.1.4m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-0.1.3m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-0.1.2m)
- rebuild against allegro-4.4.0.1

* Sat Jul 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-0.1.1m)
- [SECURITY] CVE-2010-2534
- update to 1.0.3-RC1

* Mon May 17 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.1-3m)
- add BuildRequires version of libicu-devel

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.1-2m)
- rebuild against icu-4.2.1

* Tue May  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-1m)
- [SECURITY] CVE-2010-0401 CVE-2010-0402 CVE-2010-0406
- update to 1.0.1

* Sun Feb 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.0.0-0.1.1m)
- Version 1.0.0-RC1
- Momonganize from official rpm package
- Remove non ASCII charactor.

* Mon Oct 20 2008 Benedikt Bruggemeier <skidd13@openttd.org>

- Added libicu dependency

* Thu Sep 23 2008 Benedikt Bruggemeier <skidd13@openttd.org>

- Merged both versions of the spec file

* Fri Aug 29 2008 Jonathan Coome <maedhros@openttd.org>

- Rewrite spec file from scratch.

* Sat Aug 02 2008 Benedikt Bruggemeier <skidd13@openttd.org>

- Updated spec file

* Thu Mar 27 2008 Denis Burlaka <burlaka@yandex.ru>

- Universal spec file
