%global momorel 1

Name: iperf
Version: 2.0.5
Release: %{momorel}m%{?dist}
Summary: Measurement tool for TCP/UDP bandwidth performance
License: BSD
Group: Applications/Internet
URL: http://iperf.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/iperf/iperf-%{version}.tar.gz
NoSource: 0
Patch0: iperf-2.0.5-debuginfo.patch
Patch1: iperf-2.0.5-tcpdual.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Iperf is a tool to measure maximum TCP bandwidth, allowing the tuning of
various parameters and UDP characteristics. Iperf reports bandwidth, delay
jitter, datagram loss.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%makeinstall -C src INSTALL_DIR="%{buildroot}%{_bindir}"

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING README doc/*.gif doc/*.html
%{_bindir}/iperf

%changelog
* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.5-1m)
- version up 2.0.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-2m)
- rebuild against rpm-4.6

* Wed Apr  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-1m)
- import from fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.0.2-5
- Autorebuild for GCC 4.3

* Sat Oct 27 2007 Gabriel Somlo <somlo at cmu.edu> 2.0.2-4
- replace usleep with sched_yield to avoid hogging CPU (bugzilla #355211)

* Mon Jan 29 2007 Gabriel Somlo <somlo at cmu.edu> 2.0.2-3
- patch to prevent removal of debug info by ville.sxytta(at)iki.fi

* Fri Sep 08 2006 Gabriel Somlo <somlo at cmu.edu> 2.0.2-2
- rebuilt for FC6MassRebuild

* Wed Apr 19 2006 Gabriel Somlo <somlo at cmu.edu> 2.0.2-1
- initial build for fedora extras (based on Dag Wieers SRPM)
- fixed license tag: BSD (U. of IL / NCSA), not GPL
