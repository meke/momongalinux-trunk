%global momorel 1

Summary: collection library for GObject
Name: libgee
Version: 0.6.6
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Languages
URL: http://live.gnome.org/Vala
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: vala

%description
libgee is a collection library providing GObject-based interfaces and
classes for commonly used data structures.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
	--enable-silent-rules \
	--enable-introspection=yes
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog MAINTAINERS NEWS README
%{_libdir}/libgee.so.*
%exclude %{_libdir}/libgee.la
%{_libdir}/girepository-1.0/Gee-1.0.typelib

%files devel
%defattr(-, root, root)
%{_includedir}/gee-1.0
%{_libdir}/libgee.so
%{_libdir}/pkgconfig/gee-1.0.pc

# gir
%{_datadir}/gir-1.0/Gee-1.0.gir
# vala
%{_datadir}/vala/vapi/gee-1.0.vapi

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update to 0.6.6

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-2m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.4-1m)
- update to 0.6.4

* Sun Nov 13 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-2m)
- rebuild for new GCC 4.6

* Wed Feb  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-2m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Mon Sep 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3
- comment out gir for (need gobject-introspection-0.9)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-3m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-2m)
- vapi and gir to devel

* Wed Jun 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initial build
-- disable doc (need valadoc)
