%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-calendar
Version:        2.03.1
Release:        %{momorel}m%{?dist}
Summary:        Objective Caml library for managing dates and times

Group:          Development/Libraries
License:        LGPLv2
URL:            http://calendar.forge.ocamlcore.org/
Source0:        http://forge.ocamlcore.org/frs/download.php/333/calendar-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel, ocaml-ocamldoc
BuildRequires:  gawk

# Ignore all generated modules *except* CalendarLib, since everything
# now appears in that namespace.
%global __ocaml_requires_opts -i Calendar_builder -i Calendar_sig -i Date -i Date_sig -i Fcalendar -i Ftime -i Period -i Printer -i Time -i Time_sig -i Time_Zone -i Utils -i Version
%global __ocaml_provides_opts -i Calendar_builder -i Calendar_sig -i Date -i Date_sig -i Fcalendar -i Ftime -i Period -i Printer -i Time -i Time_sig -i Time_Zone -i Utils -i Version


%description
Objective Caml library for managing dates and times.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
## 2.02?
%setup -q -n calendar-2.02


%build
./configure --libdir=%{_libdir}
make
make doc

mv TODO TODO.old
iconv -f iso-8859-1 -t utf-8 < TODO.old > TODO


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc CHANGES README TODO LGPL COPYING
%{_libdir}/ocaml/calendar
%if %opt
%exclude %{_libdir}/ocaml/calendar/*.cmx
%endif
%exclude %{_libdir}/ocaml/calendar/*.mli


%files devel
%defattr(-,root,root,-)
%doc CHANGES README TODO LGPL COPYING calendarFAQ-2.6.txt doc/*
%if %opt
%{_libdir}/ocaml/calendar/*.cmx
%endif
%{_libdir}/ocaml/calendar/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03.1-1m)
- update to 2.03.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02-1m)
- update to 2.02
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-3m)
- no NoSource: 0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- sync with Fedora devel (2.0.4-2)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 2.0.2-2
- Rebuild for OCaml 3.10.2

* Fri Mar 28 2008 Richard W.M. Jones <rjones@redhat.com> - 2.0.2-1
- New upstream version 2.0.2 (rhbz #439124)

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 2.0-2
- Rebuild for ppc64.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 2.0-1
- New upstream version 2.0.
- Rebuild for OCaml 3.10.1.

* Thu Sep  6 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-9
- Force rebuild because of updated requires/provides scripts in OCaml.

* Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-8
- Force rebuild because of base OCaml.

* Thu Aug 30 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-7
- Force rebuild because of changed BRs in base OCaml.

* Tue Aug  7 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-6
- ExcludeArch ppc64
- Clarify license is LGPLv2
- Add LGPL, COPYING, calendarFAQ-2.6.txt and doc/ subdirectory to docs.
- BR +ocaml-ocamldoc

* Mon Jun 11 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-5
- Updated to latest packaging guidelines.

* Sat Jun  2 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-4
- Handle bytecode-only architectures.

* Tue May 29 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-3
- Remove Debian DISTDIR patch.

* Fri May 25 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-2
- Added find-requires and find-provides.

* Fri May 18 2007 Richard W.M. Jones <rjones@redhat.com> - 1.10-1
- Initial RPM release.

