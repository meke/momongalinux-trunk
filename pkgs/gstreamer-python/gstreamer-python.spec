%global momorel 1

%global srcname gst-python

%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name: gstreamer-python
Version: 0.10.22
Release: %{momorel}m%{?dist}
Summary: Python bindings for GStreamer

Group: Development/Languages
License: LGPL
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer >= 0.10.32
Requires: python
Requires: gnome-python2
Requires: pygtk2


BuildRequires: python >= 2.7
BuildRequires: pkgconfig
BuildRequires: gstreamer-devel >= 0.10.32
BuildRequires: gstreamer-plugins-base-devel >= 0.10.32
BuildRequires: pygobject-devel >= 2.12.3
BuildRequires: pygtk2-devel >= 2.24
BuildRequires:  xmlto
Obsoletes: gst-python 

%description
This module contains a wrapper that allows GStreamer applications
to be written in Python.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README gst-python.doap
%{_libdir}/gstreamer-0.10/libgstpython.so
%{_libdir}/gstreamer-0.10/libgstpython.la
%{python_sitearch}/gst-0.10
%{python_sitearch}/pygst.*
%{python_sitearch}/gstoption.so
%{python_sitearch}/gstoption.la
%{_datadir}/%{srcname}

%files devel
%defattr(-, root, root)
%{_includedir}/gstreamer-0.10/gst/pygst.h
%{_includedir}/gstreamer-0.10/gst/pygstexception.h
%{_includedir}/gstreamer-0.10/gst/pygstminiobject.h
%{_includedir}/gstreamer-0.10/gst/pygstvalue.h
%{_libdir}/pkgconfig/gst-python-0.10.pc

%changelog
* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.22-1m)
- update to 0.10.22

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.21-3m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.21-2m)
- rebuild for new GCC 4.6

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.21-1m)
- update to 0.10.21

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.19-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.19-1m)
- update to 0.10.19

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.18-2m)
- use BuildRequires

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.17-1m)
- update to 0.10.17

* Wed Aug  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.15-2m)
- add devel package

* Sun Jun  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.15-1m)
- update to 0.10.15

* Wed Jan 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.14-1m)
- update to 0.10.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.13-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.10.13-2m)
- rebuild against python-2.6.1-1m

* Mon Oct 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.12-2m)
- rename gst-python to gstreamer-python

* Fri Jun 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.12-1m)
- update to 0.10.12

* Sun May 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.7-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.7-2m)
- %%NoSource -> NoSource

* Thu Mar 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- initial build
