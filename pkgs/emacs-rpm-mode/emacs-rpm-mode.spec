%global momorel 37
%global pkg rpm-mode
%global pkgname rpm-mode

Summary: RPM Spec File mode for Emacs
Name: emacs-%{pkg}
Version: 1.38
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://www.karaba.org/~mk/rpm/rpm.html
#Source0: http://www.karaba.org/~mk/rpm/rpm-mode.el.gz
#NoSource: 0
Source0: rpm-mode.el
Patch0: rpm-mode-font-lock-keywords.patch
Patch1: rpm-mode-C-c-d.patch
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
Requires: emacs(bin) >= %{_emacs_version}
Obsoletes: rpm-mode-emacs
Obsoletes: rpm-mode-xemacs
Obsoletes: elisp-rpm-mode
Provides: elisp-rpm-mode

%description
rpm-mode is Emacs Lisp for editing RPM spec file.

%package -n %{name}-el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}

%description -n %{name}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q -c -T 0
cp -p %{SOURCE0} .
%patch0 -p0 -b .original
%patch1 -p0 -b .c-c-d

%build
%{_emacs_bytecompile} rpm-mode.el

cat > %{pkg}-init.el <<"EOF"
(autoload 'rpm-mode "rpm-mode" nil t)
EOF

%install
rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -m 644 rpm-mode.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -m 644 rpm-mode.elc %{buildroot}%{_emacs_sitelispdir}/%{pkg}

%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m 644 %{pkg}-init.el %{buildroot}%{_emacs_sitestartdir}/%{pkg}-init.el

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n %{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.38-37m)
- rebuild for emacs-24.1

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.38-35m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.38-34m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-33m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.38-32m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.38-31m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-30m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-29m)
- merge rpm-mode-emacs to elisp-rpm-mode
- kill rpm-mode-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-28m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-27m)
- rebuild against emacs-23.1

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-26m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-25m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-24m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-23m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-22m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-21m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-20m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.38-19m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-18m)
- rebuild against emacs-22.1

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-17m)
- add rpm-mode-C-c-d.patch

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-16m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-15m)
- rebuild against emacs-22.0.94

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-14m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-13m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-12m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.38-11m)
- use %%{e_sitedir} %%{xe_sitedir}.
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-10m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.38-9m)
- rebuild against emacs-21.3.50

* Fri Dec 26 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.38-8m)
- modify patch0 again.

* Sat Dec 20 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.38-7m)
- modify patch0 again.

* Wed Dec 17 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.38-6m)
- modify patch0

* Wed Apr  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.38-5m)
- modify patch0

* Wed Mar 26 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.38-4m)
- modify fontify keywords. (add patch0)

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-2m)
- rebuild against emacs-21.3
- use %%{_prefix} %%{_datadir} %%{_libdir} macro

* Sat Jul  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.38-1m)
