;$Id: rpm-mode.el,v 1.38 2000/02/07 15:13:10 mk Exp $
;;;                           RPM-MODE 
;;; rpm-mode.el -- mode for editing spec file.
;;
;; Author: Kanda Mitsuru <kanda@nn.iij4u.or.jp> 1997-2000
;; Created: Nov 1997
;; Modified: MATSUMOTO Shoji <shom@i.h.kyoto-u.ac.jp> 1998
;;           MATSUDA Shigeki <matsu@math.s.chiba-u.ac.jp> 1999
;; 
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, you can either send email to this
;; program's maintainer or write to: The Free Software Foundation,
;; Inc.; 59 Temple Place, Suite 330; Boston, MA 02111-1307, USA.
;;
;; <<はじめに>>
;; spec ファイルを(特に最初から)書くときに便利なように、
;; rpm-mode.elを作ってみました．rpm コマンドも呼び出せるように
;; してみましたので試してみてください．
;; それほど需要があるとは思えないので、
;; 特に、案内も無く、マイナーバージョンアップを繰り返していますので、
;; お使いになられている方は、たまに、サイトに訪れてチェックしてください．
;; (http://www.nn.iij4u.or.jp/~kanda/)
;; バグ、綴りの間違い、タグの説明の間違い等はまだたくさんあると思いますが
;; 発見された方は、ご連絡を．email: kanda@nn.iij4u.or.jp
;;
;; <<Setting>>
;; ~/.emacs に 以下を記述してください．
;; ;;RPM-MODE
;; (setq auto-mode-alist (nconc '(("\\.spec" . rpm-mode)) auto-mode-alist))
;; (autoload 'rpm-mode "rpm-mode" "Major mode for editing SPEC file of RPM." t)
;; 以下はお好みに合わせて記述してください．
;; (setq packager "Kanda Mitsuru <kanda@nn.iij4u.or.jp>");ご自分の名前を！
;; (setq buildrootroot "/tmp") ; <buildroot>/<packagename> になります．
;;                             ; (デフォルトは /var/tmp)
;; (setq projectoname "jrpm") ; プロジェクト名 (jrpm,pjeが定義されています．)
;; 
;; Mule-2.3@19.34、Emacs20、XEmacsでは font-lock-mode を有効してあれば、 
;; キーワードがカラー化されます。
;; (=> (global-font-lock-mode t) などと ~/.emacsに書いておく。)
;; Emacs19.28 ベースの Mule では hilit19 にてカラー化されます。
;;
;; <<Usage>>
;; [1] はじめに
;; 下の rpm-mode-map の define-key にキーバインドの説明があります．
;; 簡単に説明するとキーバインドは タグ名が
;;     1) %〜 と %から始まるものは、基本的に C-c (先頭の大文字) ←例外有り
;;     2) その他は C-c (先頭の小文字)
;; としてあります．(詳しくは下記 rpm-mode-map のコメントを参照のこと)
;; よく使うであろうものに
;; C-c h ヘルプ
;; C-c d %changelog 用の dateを挿入
;; C-c r rpm コマンドの実行
;; C-c v rpmで定義される環境変数の挿入
;; C-c l ディレクトリのダンプ
;; があります．
;; また、X上で使用の場合はメニューバーも用意してあります．
;; 普通、rpm パッケージを作るときは試しながら作っていくと思いますが、
;; 一気に大まかな部分を作れるときなどは、
;; C-c I を実行してください．(対話的に作っていく)
;; この時まだよく分からない部分は単にリターンキーを押して飛ばしてください．
;; ("但し" BuildRoot: だけは使うのならなるべく入力してください．)
;; C-c S で %setupマクロを呼び出すときは、
;; C-c s で Sourceタグ を挿入していなければなりません．
;; (C-c I でインタラクティブモード時に挿入していれば問題はない)
;; 同様に、
;; C-c P で %patchマクロを呼び出すときは、
;; C-c p で Patchタグ を挿入していなければなりません．
;; (C-c I でインタラクティブモード時に挿入していれば問題はない)
;;
;; [2] rpm コマンドの呼び出し
;; rpm コマンドを呼び出すときは、C-c r 
;;
;; [3] ヘルプ
;; spec ファイル中の使えるタグ・スクリプト・環境変数 は C-c h で確認できます．
;;
;; [4] ディレクトリ/ファイル リストのダンプ  (C-c l)
;; BuildRoot が(C-c bなどで呼び出して)定義されていれば、
;; (実際 rpmモードが見ているのは内部変数 buildroot なので、
;;  自分でスペックファイル中に BuildRootを入力しているときは、
;;  判断できないので、質問してきます．)
;; C-c l でBuildRoot以下のディレクトリのダンプがとれます．
;; (定義されていない場合、定義するように質問してきます．)
;; (%files 以下に流し込んで"雛型"にすると便利でしょう．)
;; C-c l を実行した時に BuildRoot(つまりrpm-mode.el内部の変数buildroot)
;; が定義されていないときは、改めて、入力を求められます．
;;
;; <<TODO>>
;; * rpm-interactive-install-info () の後始末。
;;   (つまり、 %preun で /sbin/install-info --delete ...)     
(provide 'rpm-mode)

; emacs 19.28 では、hilit を使う。
(cond
 ((and 
   (not (featurep 'xemacs))
   (string= (substring emacs-version 0 5) "19.28")
   (eq window-system 'x))
  (require 'hilit19))
 )

(defvar rpm-mode-map nil
  "RPM モードバッファのためのローカルキーマップ")

(defvar project "jrpm")
(cond 
 ((string-match project "jrpm")
  (defvar distribution "Japanese RPM(JRPM)")
  (defvar vendor "Linux Japanese RPM Project"))
 ((string-match project "pje")
  (defvar distribution "PJE")
  (defvar vendor "Project Japanese Extentions"))
 (t
  (defvar distribution "") ;  "RPM モードでの Distribution: の値"
  (defvar vendor ""))      ;  "RPM モードでの Vendor: の値"
)


(defvar buildrootroot "/var/tmp"
  "定義されていれば RPM モードで Build: <buildrootroot>/<name> になる")

(if rpm-mode-map
    ()
  (setq rpm-mode-map (make-sparse-keymap))
  (define-key rpm-mode-map "\C-ca" 'rpm-add-all)                  
					;適当な雛形をバッファに挿入
  (define-key rpm-mode-map "\C-cv" 'rpm-interactive-variable)     
					;環境変数を代入
  (define-key rpm-mode-map "\C-cs" 'rpm-interactive-source)       
					;Source: タグを挿入
  (define-key rpm-mode-map "\C-cp" 'rpm-interactive-patch)        
					;Patch: タグを挿入
  (define-key rpm-mode-map "\C-cg" 'rpm-interactive-group)        
					;group タグを対話的に挿入
  (define-key rpm-mode-map "\C-cb" 'rpm-interactive-buildroot)    
					;buildroot を定義
  (define-key rpm-mode-map "\C-cl" 'rpm-interactive-if_defined_buildroot_dumpdir)
					;上の関数を使ってbuildrootを定義しているとき
					;buildrootをダンプする
					;("C-c l"の"l"はfile list の "l")
  (define-key rpm-mode-map "\C-cB" 'rpm-interactive-subpackages)   
					;subpackage 
  (define-key rpm-mode-map "\C-cD" 'rpm-interactive-description)  
					;%description タグを挿入
  (define-key rpm-mode-map "\C-cC" 'rpm-interactive-changelog)
					;%changelog タグを挿入
  (define-key rpm-mode-map "\C-cd" 'rpm-interactive-changelog-date)
					;%changelog タグ中用dateを挿入
  (define-key rpm-mode-map "\C-cS" 'rpm-interactive-setupmacro)        
					;%setup マクロ
  (define-key rpm-mode-map "\C-cP"  'rpm-interactive-patchmacro)       
					;%patch マクロ
  (define-key rpm-mode-map "\C-cF" 'rpm-interactive-files)        
					;%files タグを挿入
  (define-key rpm-mode-map "\C-cI" 'rpm-interactive)      
					;interactive モード
  (define-key rpm-mode-map "\C-ch" 'rpm-help)             
					;ヘルプモード
  (define-key rpm-mode-map "\C-cr" 'rpm-exec)        
					;rpm コマンド実行
  (define-key rpm-mode-map "\C-ct" 'rpm-interactive-install-info)        
					;install-info の挿入
  )

;;-------------------- RPM-mode -------------------------------
(defun rpm-mode ()
  "Major mode for editing SPEC file."
  (interactive)
  (kill-all-local-variables)
  (make-local-variable 'arg)
  (make-local-variable 'other-arg)
  (make-local-variable 'spec-name)
  (make-local-variable 'rpm-process)
  (make-local-variable 'omax)
  (make-local-variable 'mode-line-process)
  (setq mode-name "RPM")
  (setq major-mode 'rpm-mode)
  (use-local-map rpm-mode-map)


;;-------------------- Menu bar ------------------------------
;for mule emacs20
  (if (not (featurep 'xemacs))
      (progn
	(defvar menu-bar-rpm-menu
	  (make-sparse-keymap "RPM"))
	(define-key rpm-mode-map 
	  [menu-bar rpm]
	  (cons "RPM" menu-bar-rpm-menu))
	(define-key menu-bar-rpm-menu
	  [rpm-exec]
	  '("Exec rpm command" . rpm-exec))
	(define-key menu-bar-rpm-menu
	  [rpmline6]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-variable]
	  '("Env variable" . rpm-interactive-variable))
	(define-key menu-bar-rpm-menu
	  [rpmline5]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-if_defined_buildroot_dumpdir]
	  '("Dump $BuildRoot" . rpm-interactive-if_defined_buildroot_dumpdir))
	(define-key menu-bar-rpm-menu
	  [rpmline4]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-install-info]
	  '("Install Info" . rpm-interactive-install-info))
	(define-key menu-bar-rpm-menu
	  [rpmline3]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-files]
	  '("%files" . rpm-interactive-files))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-patchmacro]
	  '("%patch" . rpm-interactive-patchmacro))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-setupmacro]
	  '("%setup" . rpm-interactive-setupmacro))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-subpackages]
	  '("%package" . rpm-interactive-subpackages))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-changelog]
	  '("%changelog" . rpm-interactive-changelog))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-changelog-date]
	  '("insert date for %changelog" . rpm-interactive-changelog-date))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-description]
	  '("%description" . rpm-interactive-description))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-buildroot]
	  '("BuildRoot:" . rpm-interactive-buildroot))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-group]
	  '("Group:" . rpm-interactive-group))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-patch]
	  '("Patch<n>:" . rpm-interactive-patch))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-source]
	  '("Source<n>:" . rpm-interactive-source))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-distribution]
	  '("Distribution:" . rpm-interactive-distribution))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-vendor]
	  '("Vendor:" . rpm-interactive-vendor))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-copyright]
	  '("Copyright:" . rpm-interactive-copyright))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-release]
	  '("Release:" . rpm-interactive-release))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-version]
	  '("Version:" . rpm-interactive-version))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-name]
	  '("Name:" . rpm-interactive-name))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive-summary]
	  '("Summary:" . rpm-interactive-summary))
	(define-key menu-bar-rpm-menu
	  [rpmline2]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu
	  [rpm-sampleform]
	  '("Insert a sample form" . rpm-add-all))
	(define-key menu-bar-rpm-menu
	  [rpmline1]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu
	  [rpm-interactive]
	  '("Interactive Mode" . rpm-interactive))
	(define-key menu-bar-rpm-menu
	  [rpmline0]
	  '("---------------------------------".() ))
	(define-key menu-bar-rpm-menu 
	  [rpm-help]
	  '("Available tags(HELP)" . rpm-help))
	)
    )
  ;for xemacs
  (if (featurep 'xemacs)
      (progn
	(defvar rpm-mode-popup-menu
	  (purecopy '("RPM"
		      ["Available tags(HELP)" rpm-help t]
		      "---"
		      ["Interactive Mode" rpm-interactive t]
		      "---"
		      ["Insert a sample form" rpm-add-all t]
		      "---"
		      ["Summary:"  rpm-interactive-summary t]
		      ["Name:" rpm-interactive-name t]
		      ["Version:" rpm-interactive-version t]
		      ["Release:" rpm-interactive-release t]
		      ["Copyright:" rpm-interactive-copyright t]
		      ["Vendor:" rpm-interactive-vendor t]
		      ["Distribution:" rpm-interactive-distribution t]
		      ["Source<n>:" rpm-interactive-source t]
		      ["Patch<n>:" rpm-interactive-patch t]
		      ["Group:" rpm-interactive-group t]
		      ["BuildRoot:" rpm-interactive-buildroot t]
		      ["%description" rpm-interactive-description t]
		      ["insert date for %changelog" rpm-interactive-changelog-date t]
		      ["%changelog" rpm-interactive-changelog t]
		      ["%package" rpm-interactive-subpackages t]
		      ["%setup" rpm-interactive-setupmacro t]
		      ["%files" rpm-interactive-files t]
		      "---"
		      ["Install Info" rpm-interactive-install-info t]
		      "---"
		      ["Dump %BuildRoot" rpm-interactive-if_defined_buildroot_dumpdir t]
		      "---"
		      ["Env variable" rpm-interactive-variable t]
		      "---"
		      ["Exec rpm command" rpm-exec t]
		      )
		    ))
	(easy-menu-define rpm-mode-menu rpm-mode-map "" rpm-mode-popup-menu)
	(easy-menu-add rpm-mode-menu)
	)
    )
  (run-hooks 'rpm-mode-hook)
  )
;;-------------------- Commands ------------------------------
;C-c v
(defun rpm-interactive-variable ()
  "Insert a Variable"
  (interactive)
  (message "0)DIR関係 1)OPT_FLAGS 2)ARCH 3)OS 4)NAME 5)VERSION 6)RELEASE")
  (let ((ans (char-to-string (read-char))))
    (cond 
     ((string-match "0" ans)
      (message "0)SOURCE_DIR 1)BUILD_DIR 2)DOC_DIR 3)ROOT_DIR 4)BUILD_ROOT 5)INSTALL_PREFIX")
      (let ((ans2 (char-to-string (read-char))))
	(cond
	 ((string-match "0" ans2)
	  (insert "$RPM_SOURCE_DIR"))
	 ((string-match "1" ans2)
	  (insert "$RPM_BUILD_DIR"))
	 ((string-match "2" ans2)
	  (insert "$RPM_DOC_DIR"))
	 ((string-match "3" ans2)
	  (insert "$RPM_ROOT_DIR"))
	 ((string-match "4" ans2)
	  (insert "$RPM_BUILD_ROOT"))
	 ((string-match "5" ans2)
	  (insert "$RPM_INSTALL_PREFIX"))
	 )))
     ((string-match "1" ans)
      (insert "$RPM_OPT_FLAGS"))
     ((string-match "2" ans)
      (insert "$RPM_ARCH"))
     ((string-match "3" ans)
      (insert "$RPM_OS"))
     ((string-match "4" ans)
      (insert "$RPM_PACKAGE_NAME"))
      ((string-match "5" ans)
       (insert "$RPM_PACKAGE_VERSION"))
      ((string-match "6" ans)
       (insert "$RPM_PACKAGE_RELEASE"))
      ))
  )



;; C-c a
(defun rpm-add-all ()
  "Add all tags"
  (interactive)
  (save-excursion
    (insert "Summary: \n")
    (insert "Name: \n")
    (insert "Version: \n")
    (insert "Release: \n")
    (insert "Copyright: \n")
    (insert "Distribution: " distribution "\n")
    (insert "Vendor: " vendor "\n")
    (insert "Group: \n")
    (insert "Source: \n")
    (insert "Patch: \n")
    (if (boundp 'packager)
        (insert "Packager: " packager"\n")
      (insert "Packager: \n"))
    (insert "URL: \n")
    (if (boundp 'buildrootroot)
	(insert "BuildRoot: " buildrootroot "/\n")
      (insert "BuildRoot: \n"))
    (insert "\n")
    (insert "%description\n")
    (insert "\n")
    (insert "%changelog\n")
    (insert "\n")
    (insert "%prep\n")
    (insert "\n")    
    (insert "%setup \n")
    (insert "%patch -p1\n")
    (insert "\n")
    (insert "%build\n")
    (insert "\n")
    (insert "%install\n")
    (insert "\n")
    (insert "%clean\n")
    (if (boundp 'buildroot)
	(insert "rm -rf $RPM_BUILD_ROOT\n"))
    (insert "\n") 
    (insert "%pre\n")
    (insert "\n")
    (insert "%post\n")
    (insert "\n")
    (insert "%preun\n")
    (insert "\n")
    (insert "%postun\n")
    (insert "\n")
    (insert "%files \n")  
    (insert "\n")
    (insert "%doc \n") 
    )
)

;C-c h
(defun rpm-help ()
  "Display all available spec tags."
  (interactive)
  (with-output-to-temp-buffer "*Available Tags & Scripts in SPEC FILE*"
    (princ "========== プリアンブル・タグ ==========\n")
    (princ "Name:                   |(プログラム名)\n")
    (princ "Version:                |(プログラムのバージョン)\n")
    (princ "Release:                |(rpm パッケージのリリース番号)\n")
    (princ "Serial: <num>           |(パッケージのシリアル番号)\n")
    (princ "%define                 |(別名定義\n")
    (princ "                        | 例: %define version %{PACKAGE_VERSION}\n")
    (princ "                        | 以降%{version}で%{PACKAGE_VERSION}を使用可能)\n") 
    (princ "Summary:                |(パッケージの説明(一行))\n")
    (princ "Summary(lang):          |(langによるパッケージの説明(一行))\n")
    (princ "Copyright:              |(著作権表示(GPL,MIT,etc)\n")
    (princ "Distribution:           |(配布形態名(配布するパッケージ名))\n")
    (princ "Icon:                   |(アイコン名,glintで使用する)\n")
    (princ "Vendor:                 |(ベンダー名)\n")
    (princ "URL:                    |(このプログラムについての情報が得られる URL)\n")
    (princ "Group:                  |(このパッケージの属するグループ名)\n")
    (princ "Packager:               |(パッケージ作製者名)\n")
    (princ "%description [-l <lang>]|(パッケージについての詳細な説明(複数行可))\n")
    (princ "%changelog              |(変更履歴\n")
    (princ "                        |フォーマット * 曜日 月 日 年 氏名 <e-mail adress\n")
    (princ "                        |             - 変更したことを記入\n")
    (princ "Source[<n>]:            |(プログラムのソース名)\n")
    (princ "Patch[<n>]:             |(プログラムのパッチ名)\n")
    (princ "NoSource: [<n>]         |(src.rpmパッケージに含めたくないソース名) \n")
    (princ "NoPatch: [<n>]          |(src.rpmパッケージに含めたくないパッチ名)\n")
    (princ "ExcludeArch: <arch>     |(<arch>上ではパッケージを作成できない)\n")
    (princ "ExclusiveArch: <arch>   |(<arch>上でのみパッケージを作成できる)\n")
    (princ "ExcludeOS: <os>         |(<os>上ではパッケージを作成できない)\n")
    (princ "ExclusiveOS: <os>       |(<os>上でのみパッケージを作成できる)\n")
    (princ "Prefix: <path>          |(インストールパスの接頭辞)\n")
    (princ "Prefixes: <path>...     |(インストールパスの接頭辞の複数指定(空白をあけて指定))\n")
    (princ "BuildRoot: <path>       |(パッケージ作成時の仮インストール場所)\n")
    (princ "BuildConflicts:         |(パッケージ作成時に競合するパッケージ)\n")
    (princ "BuildRequires:          |(パッケージ作成時に必要となるパッケージ、ファイル)\n")
    (princ "BuildArchitectures:     |(作成するパッケージの種類(i386,noarch,...)を指定する)\n")
    (princ "Provides:               |(仮想パッケージを提供する)\n")
    (princ "Requires:               |(他に必要なパッケージ名(<,>,=,>=,<=,使用可))\n")
    (princ "Conflicts:              |(競合するパッケージ名(<,>,=,>=,<=,使用可))\n")
    (princ "PreReq:                 |(%pre,%post,%postun,%preun等で実行するコマンド等\n")
    (princ "                        | インストール時に予め必要なコマンドもしくはパッケージ名)\n")
    (princ "AutoReqProv: (yes|no)   |(パッケージ作成時に依存性/機能提供チェックをするか)\n")
    (princ "AutoReq: (yes|no)       |(パッケージ作成時に依存性チェックをするか)\n")
    (princ "AutoProv: (yes|no)      |(パッケージ作成時に機能提供チェックをするか)\n")
    (princ "DocDir: <dir>           |(文書ディレクトリを /usr/doc から <dir>へ変更)\n")
    (princ "%package [-n] <name>    |(複数のパッケージがあるときのパッケージ名)\n")
    (princ "\n")
    (princ "===== Build Script タグ & 環境変数 ====\n")
    (princ "%prep                   |(RPMがビルド時のチェックを行うスクリプト)\n")
    (princ "%setup                  |(ソースを展開するためのマクロ)\n")
    (princ "   -n <name>            ||(展開して cd する先のディレクトリ名)\n")
    (princ "   -c                   ||(展開して cd する先のディレクトリを作成する)\n")
    (princ "   -D                   ||(展開する時に前に展開したディレクトリを消去しない\n")
    (princ "                        || -a や -b -T オプションとともに使われることが多い)\n")
    (princ "   -T                   ||(展開するときに Source0 を再び展開しない\n")
    (princ "                        || -a や -b -D オプションとともに使われることが多い)\n")
    (princ "   -b <n>               ||(n番目のソースを展開する前に)\n")
    (princ "   -a <n>               ||(n番目のソースを展開した後に)\n")
    (princ "%patch                  |(パッチを当てるためのマクロ)\n")
    (princ "   -P <n>               ||(Patch<n> をあてない)\n")
    (princ "   -p <n>               ||(patch コマンドの -p オプションと同じ)\n")
    (princ "   -b <name>            ||(パッチを当てる前のファイル名に付加する拡張子を \n")
    (princ "                        ||.orig ではなく、<name> にする)\n")
    (princ "   -E                   ||(パッチを当てた後からになったファイルを削除する)\n")
    (princ "%build                  |(実際にプログラムを作成するためのタグ(make等を記述))\n")
    (princ "%configure              |(configuer スクリプトを実行)\n")
    (princ "%install                |(インストール時に行うことを記述 (make install等))\n")
    (princ "%clean                  |(ビルドしたディレクトリツリーを消去する\n")
    (princ "                        | ことなどを記述する    例: rm -rf  $RPM_BUILD_ROOT)\n")
    (princ "$RPM_SOURCE_DIR         |(ソースファイルの置かれるディレクトリ) \n")
    (princ "                        | デフォルト:/usr/src/redhat/SOURCES\n")
    (princ "$RPM_BUILD_DIR          |(ビルド時に使われるディレクトリ)\n")
    (princ "                        | デフォルト:/usr/src/redhat/BUILD\n")
    (princ "$RPM_DOC_DIR            |(ドキュメントの置かれるディレクトリ)\n")
    (princ "                        | デフォルト:/usr/doc\n")
    (princ "$RPM_OPT_FLAGS          |(rpmrc から得られる make時などのオプショナルフラグ)\n")
    (princ "$RPM_ARCH               |(パッケージをビルドする対象のアーキテクチャ)\n")
    (princ "$RPM_OS                 |(パッケージをビルドする対象の OS)\n")
    (princ "$RPM_ROOT_DIR           |(パッケージ作成時のルートディレクトリとなるもの\n")
    (princ "                        | $RPM_BUILD_ROOT 指定時にはそのディレクトリ)\n")
    (princ "$RPM_BUILD_ROOT         |(仮想的にインストールする場合のルートディレクトリ)\n")
    (princ "$RPM_PACKAGE_NAME       |(Name: タグで指定した名前)\n")
    (princ "$RPM_PACKAGE_VERSION    |(Version: タグで指定したバージョン)\n")
    (princ "$RPM_PACKAGE_RELEASE    |(Release: タグで指定したりリース)\n")
    (princ "\n")
    (princ "= インストール時の Script タグ & 環境変数 =\n")
    (princ "%pre [-p]               |(パッケージをインストールする前にすることを記述)\n")
    (princ "%post [-p]              |(パッケージをインストールした後にすることを記述)\n")
    (princ "%preun [-p]             |(パッケージをアンインストールする前にすることを記述)\n")
    (princ "%postun [-p]            |(パッケージをアンインストールした後にすることを記述)\n")
    (princ "%verifyscript           |(パッケージの照合(verify)時に実行されるスクリプト\n")
    (princ "%trigger{un|in|postun} [[-n] <subpackage>] [-p <program>] -- <trigger> | \n")
    (princ "                    (<trigger>のinstall、uninstall、postuninstall時に\n")
    (princ "                      変更する必要がある場合その動作を記述する．<=>使用可)\n")
    (princ "$RPM_INSTALL_PREFIX     |(パッケージインストール時のパスの接頭辞)\n")
    (princ "\n")
    (princ "====== ファイルリスト タグ ======\n")
    (princ "%files                  |(インストールするファイルを列挙) \n")
    (princ "   -f <file>            |(インストールされるファイルをリストした<file>を指定)\n")
    (princ "   -n <name>            |(サブパッケージ<name>のファイルリスト)\n")
    (princ "%doc <file>             |(ドキュメントファイル <file>)\n")
    (princ "%config <file>          |(設定ファイルなどインストールの後に更新されるもの)\n")
    (princ "%config(missingok) <file> |(%configなファイルでかつ存在していなくても良いもの)\n")
    (princ "%config(noreplace) <file> |(%configなファイルでかつ置き換えてはいけないもの)\n")
    (princ "%docdir <dir>           |(ドキュメントディレクトリ)\n")
    (princ "%dir <dir>              |(パッケージのみが使うディレクトリ)\n")
    (princ "%ghost <file>           |(常にあるとは限らないファイル(ログファイルなど)\n")
    (princ "%lang()                 |(ある言語に属するファイルを指定する)\n")
    (princ "%attr(<mode>,<user>,<group>) <file>| \n")
    (princ "               (RPMによって照合される９つのファイル属性のコントロール)\n")
    (princ "               (一般ユーザがパッケージを作成したときなどでは、\n")
    (princ "                インストール時に%doc 等が作製者の属性になるのを防ぐために\n")
    (princ "                用いる．)\n")
    (princ "%defattr(<mode>,<usr>,<group>)     | \n")
    (princ "               (%defattr()以下に列挙されているの全てのファイル\n")
    (princ "                もしくは他の%defattr()が出てくるまでのデフォルト\n")
    (princ "                属性を設定する．つまり\n")
    (princ "                複数のファイルを一括指定可能な %attr()\n")
    (princ "%verify([not]<attr>...) <file> |\n")
    (princ "               (RPMがファイルを照合するときに不必要な属性まで\n")
    (princ "                チェックしないようにチェックする属性を指定\n")
    (princ "%defverify()                    \n")
    (princ "               (%defverify()以下の全てのファイル(もしくは\n")
    (princ "                他の%defverify()が出てくるまで)のデフォルト\n")
    (princ "                パラメータを設定する．つまり\n")
    (princ "                複数のファイルを一括指定可能な%verify()\n")
    (princ "\n")
    (princ "====== 制御文(これ以外にももちろん sh の制御文も使用可能) ======\n")
    (princ "%ifarch <arch> \n")
    (princ "%ifnarch <arch> \n")
    (princ "%ifos <os> \n")
    (princ "%ifnos <os> \n")
    (princ "%else \n")
    (princ "%endif \n")
    (princ "======= その他 =======\n")
    (princ "-p オプション\n")
    (princ "%pre*/post* の中身が空で -p <プログラムのパス>が指定されていれば\n")
    (princ "それが実行される。この -p で指定されたプログラムは prerequisite\n")
    (princ "として登録される(そうでなければ /bin/sh)\n")
    ))

;;==========================================================================
;; インタラクティブなタグの挿入=============================================
;;==========================================================================
;; C-c I
(defun rpm-interactive ()
  "Interactive Inserting Tags"
  (interactive)
  (rpm-interactive-summary)
  (rpm-interactive-name)
  (rpm-interactive-version)
  (rpm-interactive-release)
  (rpm-interactive-copyright)
  (rpm-interactive-distribution)
  (rpm-interactive-vendor)
  (rpm-interactive-group) 
  (rpm-interactive-source)
  (rpm-interactive-patch)
  (rpm-interactive-url)
  (rpm-interactive-packager)
  (rpm-interactive-icon)
  (rpm-interactive-buildroot)
  (rpm-interactive-buildarchitectures)
  (rpm-interactive-preambletags)
  (rpm-interactive-description)
  (rpm-interactive-changelog)
  (rpm-interactive-subpackages)

  ;(message "Going Build Script Environment tags...")
  (message "Build スクリプトを作ります")

  (insert "%prep\n")

  (rpm-interactive-setupmacro)
  (rpm-interactive-patchmacro)

  (rpm-interactive-if_defined_buildroot)
  (rpm-interactive-build)
  (rpm-interactive-install)
  (rpm-interactive-clean)

  ;(message "Going Install-Time Script Environment tags...")
  (message "インストール時のスクリプトを作ります")

  (rpm-interactive-install_time_script)

  ;(message "Going File list Directives tags...")
  (message "ファイルリストを作ります")

  (rpm-interactive-files)

  (message "Interactive mode done ...")
  )

;---------------------------------------
;----- インタラクティブ時の内部関数-----
;---------------------------------------

; Summary タグ
(defun rpm-interactive-summary ()
  "Insert a Summary interactively"
  (interactive)
  ;(setq summary (read-string "Please insert Summary: ")) 
  (setq summary (read-string "Summary を入力してください=> ")) 
  (insert "Summary: " summary"\n")
  )

;Name タグ
(defun rpm-interactive-name ()
  "Insert a Name interactively"
  (interactive)
  ;(setq name (read-string "Please insert Name: ")) 
  (setq name (read-string "Name を入力してください=> ")) 
  (insert "Name: " name"\n")
  )

; Version タグ
(defun rpm-interactive-version ()
  "Insert a Version interactively"
  (interactive)
  ;(setq version (read-string "Please insert Version: ")) 
  (setq version (read-string "Version を入力してください=> ")) 
  (insert "Version: " version"\n")
  )

;Release タグ
(defun rpm-interactive-release ()
  "Insert a Release interactively"
  (interactive)
  ;(setq release (read-string "Please insert Release: ")) 
  (setq release (read-string "Release を入力してください=> ")) 
  (insert "Release: " release"\n")
  )

;Copyright タグ
(defun rpm-interactive-copyright ()
  "Insert a Copyright tag interactively"
  (interactive)
  (message "Copyright:0)GPL 1)BSD 2)MIT 3)Distributable 4)PublicDomain 5)non-free 6)Others")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Copyright: GPL\n"))
     ((string-match "1" ans)
      (insert "Copyright: BSD\n"))
     ((string-match "2" ans)
      (insert "Copyright: MIT\n"))
     ((string-match "3" ans)
      (insert "Copyright: Distributable\n"))
     ((string-match "4" ans)
      (insert "Copyright: Public Domain\n"))
     ((string-match "5" ans)
      (insert "Copyright: Non-free\n"))
     ((string-match "6" ans)
      ;(setq copyright (read-string "Please insert Copyright: ")) 
      (setq copyright (read-string "Copyright を入力してください=> ")) 
      (insert "Copyright: " copyright"\n"))
     (t (insert "Copyright: \n"))
     ))
  )
    
(defun rpm-interactive-distribution ()
  "Insert a Distribution tag interactively"
  (interactive)
  ;(message "Do you want to insert a Distribution tag ? Y)es N)o : ")
  (message "Distribution タグを書きますか？ Y)es N)o : ")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
      (if (y-or-n-p (concat "Distribution: " distribution " OK? "))
	  t
	;(setq distribution(read-string "Please insert Distribution: ")))
	(setq distribution(read-string "Distribution を入力してください=> ")))
      (insert "Distribution: " distribution"\n"))
     ((string-match "n" ans)
      (message "Distribution tag skipped!"))
     (t (message "Distribution tag skipped!"))
       )
    )
  )

;Group tag
(defun rpm-interactive-group () 
  "0)Amu  means  Amusements.
1)App  means  Applications.
2)Dev  means  Development.
3)Doc  means  Documentation.
4)Sys  means  System Environment.
6)UI   means  User Interface.
b)Ex   means  Extensions."
  (interactive)
  (message "Group: 0)Amu 1)App 2)Dev 3)Doc 4)Sys 5)UI 6)Ex")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (rpm-group-tag-amusements))
     ((string-match "1" ans)
      (rpm-group-tag-applications))
     ((string-match "2" ans)
      (rpm-group-tag-development))
     ((string-match "3" ans)
      (rpm-group-tag-documentation))
     ((string-match "4" ans)
      (rpm-group-tag-system-environment))
     ((string-match "5" ans)
      (rpm-group-tag-user-interface))
     ((string-match "6" ans)
      (rpm-group-tag-extensions))
     (t (insert "Group: \n"))
     ))
  )

;Group: Amusements/
(defun rpm-group-tag-amusements ()
  "0)Games  means Games.
1)Graphics means Graphics
2)Other    means Other."
  (interactive)
  (message "0)Games 1)Graphics")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Group: Amusements/Games\n"));0
     ((string-match "1" ans)
      (insert "Group: Amusements/Graphics\n"));1
     ((string-match "2" ans)
      (insert "Group: Amusements\n"));d
     (t (insert "Group: Amusements\n"))
     ))
  )


;Group: Applications/
(defun rpm-group-tag-applications ()
  "0)Arc    means  Archiving.
1)Com    means  Communications.
2)Db     means  Databases.
3)Edtr   means  Editors.
4)Emu    means  Emulators.
5)Eng    means  Engineering.
6)File   means  File. :-)
7)Inet   means  Internet.
8)Mm     means  Multimedia.
9)Prod   means  Productivity.
a)Pub    means  Publishing.
b)Sys    means  System
c)Txt    means  Text
d)Oths   means  Others. (You must insert a tag yourself.)"
  (interactive)
  (message 
   "0)Arc 1)Com 2)Db 3)Edtr 4)Emu 5)Eng 6)File 7)Inet 8)Mm 9)Prod a)Pub b)Sys c)Txt d)Oths")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Group: Applications/Archiving\n"));0
     ((string-match "1" ans)
      (insert "Group: Applications/Communications\n"));1
     ((string-match "2" ans)
      (insert "Group: Applications/Databases\n"));2
     ((string-match "3" ans)
      (insert "Group: Applications/Editors\n"));3
     ((string-match "4" ans)
      (insert "Group: Applications/Emulators\n"));4
     ((string-match "5" ans)
      (insert "Group: Applications/Engineering\n"));5
     ((string-match "6" ans)
      (insert "Group: Applications/File\n"));6
     ((string-match "7" ans)
      (insert "Group: Applications/Internet\n"));7
     ((string-match "8" ans)
      (insert "Group: Applications/Multimedia\n"));8
     ((string-match "9" ans)
      (insert "Group: Applications/Productivity\n"));9
     ((string-match "a" ans)
      (insert "Group: Applications/Publishing\n"));a
     ((string-match "b" ans)
      (insert "Group: Applications/System\n"));b
     ((string-match "c" ans)
      (insert "Group: Applications/Text\n"));c
     ((string-match "d" ans)
      (insert "Group: Applications\n"));d
     (t (insert "Group: Applications\n"))
     ))
  )

;Group:Development/
(defun rpm-group-tag-development ()
  "0)Debbugers  means  \"Development/Debbugers\".
1)Languages  means  \"Development/Languages\".
2)Libraries  means  \"Development/Libraries\".
3)System     means  \"Development/System\".
4)Tools      means  \"Development/Tools\".
5)Others     means  Others. (You must insert a tag yourself.)"
  (interactive)
  (message "0)Debbugers 1)Languages 2)Libraries 3)System 4)Tools 5)Others")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Group: Development/Debbugers\n"))
     ((string-match "1" ans)
      (insert "Group: Development/Languages\n"))
     ((string-match "2" ans)
      (insert "Group: Development/Libraries\n"))
     ((string-match "3" ans)
      (insert "Group: Development/System\n"))
     ((string-match "4" ans)
      (insert "Group: Development/Tools\n"))
     ((string-match "5" ans)
      (insert "Group: Development\n"))
     (t (insert "Group: Development\n"))
     ))
  )

;Group: Documentaion/
(defun rpm-group-tag-documentation ()
  "Insert \"Group: Documentation\"."
  (interactive)
  (insert "Group: Documentation\n")
  )

;Group: System Environment/
(defun rpm-group-tag-system-environment ()
  "0)Base  means Base.
1)Daemons  means Daemons.
2)Kernel   means Kernsl.
3)Lib      means Libraries.
4)Shell   means Shells.
5)Other    means Other."
  (interactive)
  (message "0)Base 1)Daemons 2)Kernel 3)Lib 4)Shell 5)Other")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Group: System Environment/Base\n"));0
     ((string-match "1" ans)
      (insert "Group: System Environment/Daemons\n"));1
     ((string-match "2" ans)
      (insert "Group: System Environment/Kernel\n"));2
     ((string-match "3" ans)
      (insert "Group: System Environment/Libraries\n"));3
     ((string-match "4" ans)
      (insert "Group: System Environment/Shells\n"));4
     ((string-match "5" ans)
      (insert "Group: System Environment\n"));5
     (t (insert "Group: System Environment\n"))
     ))
  )

;Group: User Interface/
(defun rpm-group-tag-user-interface ()
  "0)desktops  means Desktops.
1)X        means X.
2)X hard   means X Hardware Support.
4)Other    means Other."
  (interactive)
  (message "0)desktops 1)X  2)X hard 3)Other")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Group: User Interface/Desktops\n"));0
     ((string-match "1" ans)
      (insert "Group: User Interface/X\n"));1
     ((string-match "2" ans)
      (insert "Group: User Interface/X hardware Support\n"));2
     ((string-match "3" ans)
      (insert "Group: User Interface\n"));3
     (t (insert "Group: User Interface\n"))
     ))
  )

;Group: Extensions/
(defun rpm-group-tag-extensions ()
"0)Japanese  means  \"Extensions/Japanese\".
 1) Others   means  Others. (You must insert a tag yourself.)"
  (interactive)
  (message "0)Japanese 1)Others")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (insert "Group: Extensions/Japanese\n"))
     ((string-match "1" ans)
      (insert "Group: Extensions\n"))
     (t (insert "Group: Extensions\n"))
     ))
  )

(defun rpm-interactive-source ()
  "Insert the Source<n> interactively"
  (interactive)
  (setq nums 0)
  (setq multiplesource "y")
  (while (string-match "y" multiplesource)
    ;(setq source (read-string "Please insert the source name: "))
    (setq source (read-string "ソースファイル名を入力してください=> "))
    (insert (format "Source%d: %s \n" nums source))
    ;(message "Need more sources ? Y)es N)o :")
    (message "その他にソースファイルはありますか？ Y)es N)o :")
    (if (string-match "y" (char-to-string (read-char)))
	(progn (setq multiplesource "y") (setq nums (+ nums 1)))
      (setq multiplesource "n")
      )
    )
  )
(defun rpm-interactive-patch ()
  "Insert the Patch<n> interactively"
  (interactive)
  (setq nump 0)
  (setq multiplepatch "y")
  ;(message "Do you need to insert a patch ? Y)es N)o :")
  (message "パッチが必要ですか？ Y)es N)o :")
  (if (string-match "y" (char-to-string (read-char)))
      (while (string-match "y" multiplepatch)
	;(setq patch (read-string "Please insert the patch name: "))
	(setq patch (read-string "パッチファイル名を入力してください=> "))
	(insert (format "Patch%d: %s \n" nump patch))
	;(message "Need more patches ? Y)es N)o :")
	(message "さらにパッチが必要ですか？ Y)es N)o :")
	(if (string-match "y" (char-to-string (read-char)))
	    (progn (setq multiplepatch "y") (setq nump (+ nump 1)))
	  (setq multiplepatch "n")
	  )
	)
    (setq nump -1)
    )
  )
  
;Vendor タグ
(defun rpm-interactive-vendor ()
  "Insert a Vendor tag interactively"
  (interactive)
  ;(message "Do you want to insert a Vendor tag ? Y)es N)o: ")
  (message "Vendor タグが必要ですか？ Y)es N)o: ")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
      (if (y-or-n-p (concat "Vendor: " vendor " OK? "))
	  t
	;(setq vendor(read-string "Please insert Vendor: ")))
	(setq vendor(read-string "Vendor 名を入力してください=> ")))
      (insert "Vendor: " vendor "\n"))
     ((string-match "n" ans)
      (message "Vendor tag skipped!"))
     (t (message "Vendor tag skipped!"))
       )
    )
  )

;URL タグ
(defun rpm-interactive-url ()
  "Insert the URL interactively"
  (interactive)
  ;(message "Do you want to insert URL ? Y)es N)o :")
  (message "URL を入れますか？ Y)es N)o :")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
      (rpm-url-tag))
     ((string-match "n" ans)
      (message "No URL"))
     (t (message "skipped an url tag!"))
     ))
  )
;URL タグのサブルーチン(URL記入部)
(defun rpm-url-tag ()
  "Subroutine for rpm-interactive-rul"
  (interactive)
  ;(setq url (read-string "Please insert url name=> "))
  (setq url (read-string "URL を入力してください=> "))
  (insert "URL: " url"\n")
  )
;Packager タグ
(defun rpm-interactive-packager ()
  "Insert the Packager interactively"
  (interactive)
  (if (boundp 'packager)
      (insert "Packager: " packager"\n")
    (progn
      ;(defvar packager (read-string "Please insert Packager: "))
      (defvar packager (read-string "Packager 名を入力してください=> "))
      (insert "Packager: " packager"\n"))
    )
  )

;Icon タグ
(defun rpm-interactive-icon ()
  "Insert the Icon interactively"
  (interactive)
  ;(message "Icon exist ? Y)es  N)o :") ; Icon タグ
  (message "Icon がありますか？ Y)es  N)o :") ; Icon タグ
  (let ((ans (char-to-string (read-char))))
    (cond 
     ((string-match "y" ans)
      (rpm-icon-tag))
     ((string-match "n" ans)
      (message "No icons"))
     (t (message "skipped icon tag!"))
     ))
  )
;Icon タグのサブルーチン(アイコン名記入部)
(defun rpm-icon-tag ()
  "Subroutine for rpm-interactive-icon"
  (interactive)
  ;(setq icon (read-string "Please insert icon name: "))
  (setq icon (read-string "Icon 名を入力してください=> "))
  (insert "Icon: " icon"\n")
  )
;BuildRoot タグ
(defun rpm-interactive-buildroot ()
  "Insert the BuildRoot interactively"
  (interactive)
  ;(message "Do you want to use BuildRoot tag ? Y)es N)o :") ;BuildRoot タグ
  (message "BuildRoot タグを付けますか？ Y)es N)o :") ;BuildRoot タグ
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
      (message "BuildRoot: ")
      (if (boundp 'buildrootroot)
	  (setq buildroot (concat buildrootroot "/" name)))
      (if (boundp 'buildroot)
	  (insert "BuildRoot: " buildroot "\n");
	(progn
	  (setq buildroot (read-string "BuildRoot: "))
	  (insert "BuildRoot: " buildroot"\n"))
	))
     ((string-match "n" ans)
      (makunbound 'buildroot )
      (message "No BuildRoot"))
     (t (message "skipped BuildRoot tag!"))
     ))
 )
;BuildArchitectures タグ
(defun rpm-interactive-buildarchitectures ()
  "Insert the BuildArchitectures interactively"
  (interactive)
  ;(message "Do you want to use BuildArchitectures tag ? Y)es N)o :")
  (message "BuildArchitectures タグが必要ですか？ Y)es N)o :")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
;      (message "BuildArchitectures: ")
      (message "0)noarch 1)alpha 2) i386 3)ppc 4)sparc 5)その他")
      (let ((ansba (char-to-string (read-char))))
	(cond 
	 ((string-match "0" ansba)
	  (insert "BuildArchitectures: noarch\n"))
	 ((string-match "1" ansba)
	  (insert "BuildArchitectures: alpha\n"))
	 ((string-match "2" ansba)
	  (insert "BuildArchitectures: i386\n"))
	 ((string-match "3" ansba)
	  (insert "BuildArchitectures: ppc\n"))
	 ((string-match "4" ansba)
	  (insert "BuildArchitectures: sparc\n"))
	 ((string-match "5" ansba)
	  (setq buildarchitectures (read-string "BuildArchitectures: "))
	  (insert "BuildArchitectures: " buildarchitectures"\n"))
	 )))
     ((string-match "n" ans)
      (message "No BuildArchitectures"))
     (t (message "skipped BuildArchitectures tag!"))
     ))
  )
;他のプリアンブルタグ
(defun rpm-interactive-preambletags ()
  "Insert another preambles"
  (interactive)
  ;(message "Do you want to use any other Preamble tags ? Y)es N)o :")
  (message "他のプリアンブルタグが必要ですか？ Y)es N)o :")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
      (rpm-preamble-tag))
     ((string-match "n" ans)
      (message "No use any other tags."))
     (t (message "skipped any other Preamble tags"))
     ))
  )
;他のプリアンブルタグのサブルーチン
(defun rpm-preamble-tag ()
  "Subroutine for rpm-interactive-preamvlestags"
  (interactive)
  (message "0)Ex 1)Prefix 2)Provides 3)Requires 4)Serial 5)Conflicts 6)Req/Prov 7)その他")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (message "a)ExcludeArch b)ExclusiveArch c)ExcludeOS d)ExclusiveOS")
      (let ((ans2 (char-to-string (read-char))))
	(cond
	 ((string-match "a" ans2)
	  (setq excludearch (read-string "ExcludeArch:<arch> "))
	  (insert "ExcludeArch: " excludearch"\n"))
	 ((string-match "b" ans2)
	  (setq exclusivearch (read-string "ExclusiveArch:<arch> "))
	  (insert "ExclusiveArch: " exclusivearch"\n"))
	 ((string-match "c" ans2)
	  (setq excludeos (read-string "ExcludeOS:<os> "))
	  (insert "ExcludeOS: " excludeos"\n"))
	 ((string-match "d" ans2)
	  (setq exclusiveos (read-string "ExclusiveOS:<os> "))
	  (insert "ExclusiveOS: " exclusiveos"\n"))
	 )))
     ((string-match "1" ans)
      (message "a)Prefix: b)Prefixes")
      (let ((ansprefix (char-to-string (read-char))))
	(cond
	 ((string-match "a" ansprefix)
	  (setq prefix (read-string "Prefix:<path> "))
	  (insert "Prefix: " prefix"\n"))
	 ((string-match "b" ansprefix)
	  (setq prefixes (read-string "Prefixes: "))
	  (insert "Prefixes: " prefixes"\n"))
	 )))
     ((string-match "2" ans)
      (setq provides (read-string "Provides: "))
      (insert "Provides: " provides"\n"))
     ((string-match "3" ans)
      (setq requires (read-string "Requires: "))
      (insert "Requires: " requires"\n"))
     ((string-match "4" ans)
      (setq serial (read-string "Serial:<num> "))
      (insert "Serial: " serial"\n"))
     ((string-match "5" ans)
      (setq conflicts (read-string "Conflicts: "))
      (insert "Conflicts: " conflicts"\n"))
     ((string-match "6" ans)
      (message "a)AutoReqProv b)AutoReq c)AutoProv")
      (let ((ansauto (char-to-string (read-char))))
	(cond
	 ((string-match "a" ansauto)
	  (setq autoreqprov (read-string "AutoReqProv:{yes|no} "))
	  (insert "AutoReqProv: " autoreqprov"\n"))
	 ((string-match "b" ansauto)
	  (setq autoreq (read-string "AutoReq:{yes|no} "))
	  (insert "AutoReq: " autoreq"\n"))
	 ((string-match "c" ansauto)
	  (setq autoprov (read-string "AutoProv:{yes|no} "))
	  (insert "AutoProv: " autoprov"\n")))))
     ((string-match "7" ans)
      (message "a)Obsoletes b)Docdir c)PreReq")
      (let ((ansetc (char-to-string (read-char))))
	(cond
	 ((string-match "a" ansetc)
	  (setq obsoletes (read-string "Obsoletes: "))
	  (insert "Obsoletes: " obsoletes"\n"))
	 ((string-match "b" ansetc)
	  (setq docdir (read-string "Docdir: "))
	  (insert "Docdir: " docdir"\n"))
	 ((string-match "c" ansetc)
	  (setq prereq (read-string "PreReq: "))
	  (insert "PreReq: " prereq"\n"))
	 )))
     )))

;%description タグの挿入
(defun rpm-interactive-description ()
  "Insert %description"
  (interactive)
  (insert "\n%description\n\n\n")
  )
;%changelog タグの挿入
(defun rpm-interactive-changelog ()
  "Insert %changelog"
  (interactive)
  (insert "%changelog \n")
  (if (boundp 'packager)
      (progn
	(setq time (current-time-string))
	(setq year (substring time -4 nil))
	(setq today (substring time 0 11))
	(setq date (concat today year))
	(insert "* " date " " packager"\n")
	(insert "- \n")
	(insert "\n"))
    (progn
      ;(defvar packager (read-string "Please insert Packager: "))
      (defvar packager (read-string "Packager 名を入力してください=> "))
      (setq time (current-time-string))
	(setq year (substring time -4 nil))
	(setq today (substring time 0 11))
	(setq date (concat today year))
	(insert "* " date " " packager"\n")
	(insert "- \n")
	(insert "\n"))
    )
  )

;%changelog タグ中用のdateを挿入
(defun rpm-interactive-changelog-date ()
  "insert date for %changelog"
  (interactive)
  (if (boundp 'packager)
      (progn
	(setq time (current-time-string))
	(setq year (substring time -4 nil))
	(setq today (substring time 0 11))
	(setq date (concat today year))
	(insert "* " date " " packager"\n")
	(insert "- \n"))
    (progn
      ;(defvar packager (read-string "Please insert Packager: "))
      (defvar packager (read-string "Packager 名を入力してください=> "))
      (setq time (current-time-string))
	(setq year (substring time -4 nil))
	(setq today (substring time 0 11))
	(setq date (concat today year))
	(insert "* " date " " packager"\n")
	(insert "- \n"))
    )
  )

;複数のパッケージをつくる場合のタグ
(defun rpm-interactive-subpackages ()
  "Insert tags for subpackages"
  (interactive)
  (message "サブパッケージをつくりますか？ Y)es N)o :") ;別パッケージをつくるか
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "y" ans)
      (rpm-subpackages ans))
     ((string-match "n" ans)
      (insert "\n"))
     (t (insert "\n"))
     ))
  )

;複数のパッケージをつくる場合のタグ(%package)
(defun rpm-subpackages (subpackages)
  "Subroutine for rpm-interactive-subpackages"
  (interactive)
;  (defvar subpackages "y")
  (while (string-match "y" subpackages)
    ;(setq packname (read-string "Please insert a subpackage name: "))
    (setq packname (read-string "サブパッケージ名を入力してください=> "))
    (insert "\n%package " packname"\n")
    (rpm-interactive-summary)
    (rpm-interactive-group)
    (insert "%description " packname"\n\n\n")
    ;(message "Do you want to build more subpackages ? Y)es N)o :")
    (message "さらに他のサブパッケージを作りますか？ Y)es N)o : ")
    (if (string-match "y" (char-to-string (read-char)))
    	(setq subpackages "y") (setq subpackages "n"))
    )
  )

; %setupマクロの挿入
(defun rpm-interactive-setupmacro ()
  "Insert the %setup interactively"
  (interactive)
  (while (>= nums 0)
    (message "この %%setup マクロにオプションは必要ですか？ Y)es N)o :")
    (let ((ans (char-to-string (read-char))))
      (cond
       ((string-match "y" ans)
	(rpm-setupmacro ans))
       ((string-match "n" ans)
	(insert "%setup\n"))
       (t (insert "%setup\n"))))
    (setq nums (- nums 1))
    )
  )

; %setupマクロのサブルーチン
(defun rpm-setupmacro (setupmacroans)
  "Subroutine for rpm-interactive-setupmacro"
  (interactive)
  (setq setupmacron nil)
  (setq setupmacroc nil)
  (setq setupmacroD nil)
  (setq setupmacroT nil)
  (setq setupmacrob nil)
  (setq setupmacroa nil) 
  (while (string-match "y" setupmacroans)
    (message "0)-n<name>  1)-c  2)-D  3)-T  4)-b<n>  5)-a<n>")
    (let ((ans (char-to-string (read-char))))
      (cond
       ((string-match "0" ans) ;-n
	;(setq setupmacron (read-string "Please insert a directory name for -n: "))
	(setq setupmacron (read-string "-n のためのディレクトリ名を入力してください=> "))
	(setq setupmacron (concat "-n " setupmacron)))
       ((string-match "1" ans) ;-c
	(setq setupmacroc "-c"))
       ((string-match "2" ans) ;-D
	(setq setupmacroD "-D"))
       ((string-match "3" ans) ;-T
	(setq setupmacroT "-T"))
       ((string-match "4" ans) ;-b
	;(setq setupmacrob (read-string "Please insert a source number for -b: "))
	(setq setupmacrob (read-string "-b のためのソースファイル番号を入力してください=> "))
	(setq setupmacrob (concat "-b "  setupmacrob)))
       ((string-match "5" ans) ;-a
	;(setq setupmacroa (read-string "Please insert a source number for -a: "))
	(setq setupmacroa (read-string "-a のためのソースファイル番号を入力してください=> "))
	(setq setupmacroa (concat "-a "  setupmacroa)))
       (t (message "skipped setup macro!"))
       ))
    (message "他のオプションもつけますか？ Y)es N)o :")
    (let ((ans (char-to-string (read-char))))
      (cond
       ((string-match "y" ans)
	(setq setupmacroans "y"))
       ((string-match "n" ans)
	(setq setupmacroans "n"))
       (t (setq setupmacroans "n"))
       )))
  (setq setupmacrooptions (concat setupmacron " " setupmacroc " " setupmacroD 
				  " " setupmacroT " " setupmacrob 
				  " " setupmacroa "\n"))
  (insert "%setup " setupmacrooptions)
  )

; %patch マクロの挿入
(defun rpm-interactive-patchmacro ()
  "Insert the %patch interactively"
  (interactive)
  (if (>= nump 0)
      (progn (while (>= nump 0)
	       (message "次の %%patch マクロにオプションは必要ですか? Y)es N)o :")
	       (let ((ans (char-to-string (read-char))))
		 (cond
		  ((string-match "y" ans)
		   (rpm-patchmacro ans))
		  ((string-match "n" ans)
		   (insert "%patch \n"))
		  (t (message "skipped!"))))
	       (setq nump (- nump 1))
	       ))
    )
  )

; %patch マクロのサブルーチン
(defun rpm-patchmacro (patchmacroans)
  "Subroutine for rpm-interactive-patchmacro"
  (interactive)
  (setq patchmacroP nil)
  (setq patchmacrop nil)
  (setq patchmacrob nil)
  (setq patchmacroE nil)
  (setq patchmacroans "y")
  (while (string-match "y" patchmacroans)
    (message "0)-P<n>  1)-p<n>  2)-b<name>  3)-E")
    (let ((ans (char-to-string (read-char))))
      (cond
       ((string-match "0" ans);-P
	;(setq patchmacroP (read-string "Please insert patch number: "))
	(setq patchmacroP (read-string "patch ファイル番号(Patch<n>: の n)=> "))
	(setq patchmacroP (concat "-P " patchmacroP)))
       ((string-match "1" ans);-p
	;(setq patchmacrop (read-string "Please insert the pathname strip count: "))
	(setq patchmacrop (read-string "パスを除く数(patch コマンドの -p オプション)=> "))
	(setq patchmacrop (concat "-p " patchmacrop)))
       ((string-match "2" ans);-b
	;(setq patchmacrob (read-string "Please insert name as patch suffix: "))
	(setq patchmacrob (read-string "パッチを当てる前のファイルにつける拡張子=> "))
	(setq patchmacrob (concat "-b " patchmacrob)))
       ((string-match "3" ans)
	(setq patchmacroE "-E "))
       ))
    (message "他のオプションもつけますか？ Y)es N)o :")
    (let ((ans (char-to-string (read-char))))
      (cond
       ((string-match "y" ans)
	(setq patchmacroans "y"))
       ((string-match "n" ans)
	(setq patchmacroans "n"))
       (t (setq patchmacroans "n"))
       )))
  (setq patchmacrooptions (concat patchmacroP " " patchmacrop " " patchmacrob
				  " " patchmacroE "\n"))
  (insert "%patch " patchmacrooptions)
  )

;%build タグの挿入
(defun rpm-interactive-build ()
  "Insert the %build interactively"
  (interactive)
  (insert "\n\n%build\n")
  ;(message "This source has Imakefile or configure ? I)makefile C)onfigure N)one :")
  (message "Imakefile や configure スクリプトはありますか？ I)makefile C)onfigure N)one :")
  (setq makemode
	(let ((ans (char-to-string (read-char))))
	  (cond
	   ((string-match "i" ans)
	    (insert "xmkmf -a\n")
	    (insert "make\n")
	    "Imakefile")
	   ((string-match "c" ans)
	    (insert "%configure\n")
	    (insert "make\n")
	    "Configure")
	   ("None"))))
  (insert "\n\n\n")
  )

;$RPM_BUILD_ROOT が定義されているとき X11 関係のスクリプトを代入
(defun rpm-interactive-if_defined_buildroot ()
  "buildroot が定義されているとき、%prepタグで rm -rf 及び X11 関係の
ディレクトリの作成."
  (interactive)
  (if (boundp 'buildroot)
    (progn
      (insert "\nrm -rf $RPM_BUILD_ROOT\n")
      ;(message "Do you want to create the X11R6 directory ? Y)es N)o :")
      (message "X11R6 用のディレクトリを作りますか？ Y)es N)o :")
      (let ((ans (char-to-string (read-char))))
	(cond
	 ((string-match "y" ans)
	  (rpm-buildroot-x11))
	 ((string-match "n" ans)
	  (insert "mkdir -p $RPM_BUILD_ROOT\n"))
	 (t (insert "mkdir -p $RPM_BUILD_ROOT\n"))
	 )))

    )
  )
;上の関数のサブルーチン(スクリプト挿入)
(defun rpm-buildroot-x11 ()
  "Subroutine for rpm-interactive-if_defined_buildroot"
  (interactive)
  (insert "mkdir -p $RPM_BUILD_ROOT/usr/X11R6\n")
  (insert "if [ -d /usr/X11R6.1 ];then")
  (insert "  ln -sf $RPM_BUILD_ROOT/usr/X11R6 $RPM_BUILD_ROOT/usr/X11R6.1\n")
  (insert "fi\n")
  (insert "if [ -d /usr/X11R6.3 ];then\n")
  (insert "  ln -sf $RPM_BUILD_ROOT/usr/X11R6 $RPM_BUILD_ROOT/usr/X11R6.3\n")
  (insert "fi\n")
  (insert "mkdir -p $RPM_BUILD_ROOT/usr/X11R6/bin\n")
  (insert "mkdir -p $RPM_BUILD_ROOT/usr/X11R6/man/man1\n\n\n")
  )
;%install タグの挿入
(defun rpm-interactive-install ()
  "Insert %install"
  (interactive)
  (insert "%install\n")
  (cond
   ((string-match "Imakefile" makemode)
    (insert "make DESTDIR=$RPM_BUILD_ROOT install\n")
    (insert "make DESTDIR=$RPM_BUILD_ROOT install.man\n"))
   ((string-match "Configure" makemode)
    (insert "make DESTDIR=$RPM_BUILD_ROOT install\n")))
  (insert "\n\n\n")
  )
;%clean タグの挿入(RPM_BUILD_ROOT が定義されているときの挿入も含む)
(defun rpm-interactive-clean ()
  "Insert %clean tag"
  (interactive)
  (insert "%clean\n")
  (if (boundp 'buildroot)
      (insert "rm -rf $RPM_BUILD_ROOT\n\n\n")
    (insert "\n\n\n")
    )
  )

;インストール時のタグ(%pre %post %preun %postun %triggerなど)の挿入
(defun rpm-interactive-install_time_script ()
  "Insert %pre %post %preun %postun %trigger "
  (interactive)
  (message "%%pre タグを挿入しますか？ Y)es: N)o:")
  (if (string-match "y" (char-to-string (read-char)))
      (insert "%pre\n\n"))
  (message "%%post タグを挿入しますか？ Y)es: N)o:")
  (if (string-match "y" (char-to-string (read-char)))
      (progn (insert "%post\n\n")
	     (rpm-interactive-install-info)))
  (message "%%preun タグを挿入しますか？ Y)es: N)o:")
  (if (string-match "y" (char-to-string (read-char)))
      (insert "%preun\n\n"))
  (message "%%trigger タグを挿入しますか？ Y)es: N)o:")
  (if (string-match "y" (char-to-string (read-char)))
      (insert "%trigger\n\n"))
  )


;%files
(defun rpm-interactive-files ()
  (interactive)
  (insert "%files\n")
  )

;
;install info file
(defun rpm-interactive-install-info ()
  "Insert INFO file"
  (interactive)
  (setq infoans "y")
  (message "Info ファイルをインストールしますか？ Y)es: N)o:")
  (if (string-match "y" (char-to-string (read-char)))
      (while (string-match "y" infoans)
	(setq infofile (read-string "Info ファイル名を入力してください=> "))
	(setq infoentry (read-string "Info エントリ名を入力してください=> "))
	(setq infosection (read-string "Info セクション名を入力してください=> "))
	(insert "/sbin/install-info /usr/info/" infofile " /usr/info/dir --entry=\"" infoentry
		"\" " "--section=\"" infosection "\" \n" )
	(message "他にも Info ファイルはありますか？ Y)es: N)o:")
	(if (string-match "y" (char-to-string (read-char)))
	    (setq infoans "y")
	  (progn (setq infoans "n")
		 (message "%%preun タグ等に削除文を書くのを忘れないでください。"))
	  )
	)
    )
  (insert "\n")
  )

;==============================================================
;==============================================================
;----- rpm の呼び出し関係 -----------------------------------
;; rpm のプロセス監視用
(defvar rpm-process nil)


;; C-c r
(defun rpm-exec ()
  "Exec rpm "
  (interactive)
  (message "rpm Build option :0)-bp 1)-bl 2)-bc 3)-bi 4)-bb 5)-ba")
  (let ((ans (char-to-string (read-char))))
    (cond
     ((string-match "0" ans)
      (setq bopt "-bp"))
     ((string-match "1" ans)
      (setq bopt "-bl"))
     ((string-match "2" ans)
      (setq bopt "-bc"))
     ((string-match "3" ans)
      (setq bopt "-bi"))
     ((string-match "4" ans)
      (setq bopt "-bb"))
     ((string-match "5" ans)
      (setq bopt "-ba"))
     (t ())
     )
    )
  (if (y-or-n-p "他のオプションも付けますか?")
      (progn
	;(setq otheropt(read-string "Please insert other options: "))
	(setq otheropt(read-string "その他のオプション=> "))
	(rpm-rpm bopt otheropt)
	(message (format "Exectuting rpm %s %s ..." bopt otheropt)))
    (progn
      (rpm-rpm bopt)
      (message (format "Exectuting rpm %s ..." bopt))
      )
    )
  )



;;rpm-rpm (eldic.el からの流用＆改造)
(defun rpm-rpm (bopt &optional otheropt)
  "rpm コマンド実行部"
  (if rpm-process
      (if (or (not (eq (process-status rpm-process) 'run)) 
	      ;(yes-or-no-p "rpm process is running,  kill it ? "))
	      (yes-or-no-p "他の rpm プロセスが存在します、kill しますか？ Y)es N)o :"))
	  (condition-case ()
	      (let ((comp-proc rpm-process))
		(interrupt-process comp-proc)
		(sit-for 1)
		(delete-process comp-proc))
	    (error nil))
	(error "Can not have two rpm processes")))
  (setq rpm-process nil)
  (setq spec-name (buffer-name))
;  (setq LANG (setenv "LANG" "C"))
  (if (eq nil otheropt)
      (progn 
	(setq rpm-process (start-process "compilation"  "*RPM RESULT*"
				   "rpm" bopt  spec-name)))
    (progn 
      (setq rpm-process (start-process "compilation"  "*RPM RESULT*"
				   "rpm" bopt otheropt  spec-name)))
    )
  (with-output-to-temp-buffer "*RPM RESULT*")
  (let (save-excursion (set-buffer "*RPM RESULT*")))
  (set-process-sentinel rpm-process 'rpm-sentinel)
  )
;; process 監視
(defun rpm-sentinel (proc msg)
  "プロセス監視部"
  (cond ((null (buffer-name (process-buffer proc)))
         ;; buffer killed
	 (set-process-buffer proc nil))
        ((memq (process-status proc) '(signal exit))
         (let* ((obuf (current-buffer))
                omax opoint)
           ;; save-excursion isn't the right thing if
	   ;;  process-buffer is current-buffer
	   (unwind-protect
	       (progn
		 ;; Write something in *compilation* and hack its mode line,
		 (set-buffer (process-buffer proc))
		 (setq omax (point-max) opoint (point))
		 (goto-char (point-max))
		 (insert ?\n mode-name " " msg)
		 (forward-char -1)
		 (insert " at "
			 (substring (current-time-string) 0 -5))
		 (forward-char 1)
		 (setq mode-line-process
		       (concat ": "
			       (symbol-name (process-status proc))))
		 ;; If buffer and mode line will show that the process
		 ;; is dead, we can delete it now.  Otherwise it
		 ;; will stay around until M-x list-processes.
		 (delete-process proc))
	     (setq rpm-process nil)
	     ;; Force mode line redisplay soon
	     (set-buffer-modified-p (buffer-modified-p)))
	   (if (and opoint (< opoint omax))
	       (goto-char opoint))
	   (set-buffer obuf)))))

;;C-c d
;;------------------- dump $BuildRoot -------------------
;;C-c b等で rpm-mode.el 内の変数 buildroot (=$RPM_BUILD_ROOT)が
;;定義されているとき、buildroot のディレクトリ構成をダンプする．
;;(もちろん、$RPM_BUILD_ROOT 内にインストール(C-c r 等で rpm -bi
;; などして)されていることが前提！)
(defun rpm-interactive-if_defined_buildroot_dumpdir ()
  "buildroot を定義しているとき$buildrootをダンプする"
  (interactive)
  (if (boundp 'buildroot)
      (if (not (equal nil buildroot))
	  (progn
;	    (setq com 
;		  (concat "\"cd" " " buildroot ";" 
;			  "tar" " "  "cvf"  " "  "/dev/null"  " ./" 
;			  "|sed" " " "-e '/^\\./d'"
;			  "|sed" " " "-e '/^/s//\\//'\""))
	    (setq com
		  (concat "\"find" " " buildroot " -print " "|"
			  "sed \"s^" buildroot "^^\"\""))
	    (setq currentbuf (current-buffer))
	    (if (y-or-n-p 
		 (message (format "BuildRoot: [%s]" buildroot)))
		(start-process-shell-command "dump-build-dir" currentbuf 
					     shell-file-name 
					     "-c" com)
	      (progn
		;(setq buildroot (read-string "Please insert BuildRoot: "))
		(setq buildroot (read-string "BuildRoot を入力してください=> "))
		(rpm-interactive-if_defined_buildroot_dumpdir))))
	      (error "Defined Buildroot is void. Quit!"))
    (progn
      (defvar buildroot 
	;(read-string "BuildRoot not defined ! Please insert BuildRoot: "))
	(read-string "BuildRoot は定義されていません！ BuildRoot を入力して下さい=> "))
      (rpm-interactive-if_defined_buildroot_dumpdir))
    )
  )

;;------------------- hilit ------------------------------
;;タグの色付け(かなりいい加減)
(if (and 
     (not (featurep 'xemacs))
     (string= (substring emacs-version 0 5) "19.28")
     (eq window-system 'x))
    (cond (window-system
       (hilit-set-mode-patterns 
	'rpm-mode
	'(("#.*" nil comment)
	  ("[Nn]ame:" nil defun)
	  ("[Vv]ersion:" nil defun)
	  ("[Rr]elease:" nil defun)
	  ("[Ss]erial:" nil defun)
	  ("[Ss]ummary:" nil defun)
	  ("[Ss]ummary([a-zA-Z._]+):" nil defun)
	  ("[Cc]opyright:" nil defun)
	  ("[Dd]istribution:" nil defun)
	  ("[Ii]con:" nil  defun)
	  ("[Vv]endor:" nil defun)
	  ("[Uu][Rr][Ll]:" nil defun)
	  ("[Gg]roup:" nil defun)
	  ("[Pp]ackager:" nil defun)
	  ("[Nn]o[Ss]ource[0-9]*:" nil defun)
	  ("[Nn]o[Pp]atch[0-9]*:" nil defun)
	  ("[Ss]ource[0-9]*:" nil defun)
	  ("[Pp]atch[0-9]*:" nil defun)
	  ("[Ee]xclude[Aa]rch:" nil defun)
	  ("[Ee]xclusive[Aa]rch:" nil defun)
	  ("[Ee]xclude[Oo][Ss]:" nil defun)
	  ("[Ee]xclusive[Oo][Ss]:" nil defun)
	  ("[Pp]refixes:" nil defun)
	  ("[Pp]refix:" nil defun)
	  ("[Bb]uild[Rr]oot:" nil defun)
	  ("[Bb]uild[Cc]onflicts:" nil defun)
	  ("[Bb]uild[Rr]equires:" nil defun)
	  ("[Bb]uild[Aa]rchitectures:" nil defun)
	  ("[Pp]rovides:" nil defun)
	  ("[Rr]equires:" nil defun)
	  ("[Pp]re[Rr]eq:" nil defun)
	  ("[Cc]onflicts:" nil defun)
	  ("[Oo]bsoletes:" nil defun)
	  ("[Aa]uto[Rr]eq[Pp]rov:" nil defun)
	  ("[Aa]uto[Rr]eq:" nil defun)
	  ("[Aa]uto[Pp]rov:" nil defun)
	  ("[Dd]oc[Dd]ir:" nil defun)
	  ("%description" nil define)
	  ("%changelog" nil define)
	  ("%package" nil define)
	  ("%prep" nil define)
	  ("%setup" nil define)
	  ("%patch" nil define)
	  ("%build" nil define)
	  ("%configure" nil define)
	  ("%clean" nil define)
	  ("%install" nil define)
	  ("%pre" nil define)
	  ("%post" nil define)
	  ("%preun" nil define)
	  ("%postun" nil define)
	  ("%verifyscript" nil define)
	  ("%triggerin" nil define)
	  ("%triggerun" nil define)
	  ("%triggerpostun " nil define)
	  ("%files" nil define)
	  ("%doc" nil define)
	  ("%config" nil define)
	  ("%config(missingok)" nil define)
	  ("%config(noreplace)" nil define)
	  ("%ghost" nil define)
	  ("%docdir" nil define)
	  ("%dir" nil define)
	  ("%attr(.*,.*,.*)" nil define)
	  ("%defattr(.*,.*,.*)" nil define)
	  ("%verify(.*)" nil define)
	  ("%defverify(.*)" nil define)
	  ("%verifyscript" nil define)
	  ("%lang" nil define)
	  ("%lang([a-zA-Z._]+)" nil define)
	  ("%{[a-zA-Z0-9._+-]+}" nil define)
	  ("%ifarch" nil define)
	  ("%ifnarch" nil define)
	  ("%ifos" nil define)
	  ("%ifnos" nil define)
	  ("%else" nil define)
	  ("%endif" nil define)
	  ("%define" nil define)
	  ("$RPM_SOURCE_DIR" nil include)
	  ("$RPM_BUILD_DIR" nil include)
	  ("$RPM_DOC_DIR" nil include)
	  ("$RPM_ROOT_DIR" nil include)
	  ("$RPM_BUILD_ROOT" nil include)
	  ("$RPM_INSTALL_PREFIX" nil include)
	  ("$RPM_OPT_FLAGS" nil include)
	  ("$RPM_ARCH" nil include)
	  ("$RPM_OS" nil include)
	  ("$RPM_PACKAGE_NAME" nil include)
	  ("$RPM_PACKAGE_VERSION" nil include)
	  ("$RPM_PACKAGE_RELEASE" nil include)
	  ("${[a-zA-Z_][a-zA-Z0-9_]*}" nil include)
	  ))
       ))
  )

;;------------ face (for xemacs, emacs19.34, emacs20) -----------
;;タグの色付け(hilitのときと同様適当 (^^; )
(if (or (and (featurep 'font-lock)
	     (not (string= (substring emacs-version 0 5) "19.28")))
	(featurep 'xemacs))
    (progn
      (defconst rpm-font-lock-keywords 
	(list
	 '("^#.*$" . font-lock-comment-face)
	 (cons
	  (mapconcat
	   'identity
	   '("[Ss]ummary:"
	     "[Ss]ummary([a-zA-Z._]+):"
	     "[Vv]ersion:"
	     "[Rr]elease:"
	     "[Nn]ame:"
	     "[Ss]erial:"
	     "[Cc]opyright:"
	     "[Dd]istribution:"
	     "[Vv]endor:"
	     "[Gg]roup:"
	     "[Pp]ackager:"
	     "[Uu][Rr][Ll]:"
	     "\\([Nn]o\\|\\)[Ss]ource[0-9]*:"
	     "\\([Nn]o\\|\\)[Pp]atch[0-9]*:"
	     "[Ee]xclude[Aa]rch:"
	     "[Ee]xclusive[Aa]rch:"
	     "[Ee]xclude[Oo][Ss]:"
	     "[Ee]xclusive[Oo][Ss]:"
	     "[Ii]con:"
	     "[Pp]rovides:"
	     "[Rr]equires:"
	     "[Pp]re[Rr]eq:"
	     "[Cc]onflicts:"
	     "[Oo]bsoletes:"
	     "[Pp]refix:"
	     "[Pp]refixes:"
	     "[Bb]uild[Rr]oot:"
	     "[Bb]uild[Cc]onflicts:"
	     "[Bb]uild[Rr]equires:"
	     "[Bb]uild[Aa]rchitectures:"
	     "[Aa]uto[Rr]eq[Pp]rov:"
	     "[Aa]uto[Rr]eq:"
	     "[Aa]uto[Pp]rov:"
	     "[Dd]ocdir:"
	     )
	   "\\|")
	  'font-lock-keyword-face)
	 (cons
	  (mapconcat
	   'identity
	   '("%define"
	     "%description"
	     "%changelog"
	     "%package"
	     "%setup"
	     "%patch"
	     "%build"
	     "%configure"
	     "%clean"
	     "%install"
	     "%pre\\(un\\|p\\|\\)"
	     "%post\\(un\\|\\)"
	     "%trigger\\(in\\|un\\|postun\\)"
	     "%files"
	     "%doc"
	     "%config\\(\(missingok\)\\|\(noreplace\)\\|\\)"
	     "%ghost"
	     "%docdir"
	     "%dir"
	     "%attr(.*,.*,.*)"
	     "%defattr(.*,.*,.*)"
	     "%verify(.*)"
	     "%defverify(.*)"
	     "%verifyscript"
	     "%lang\\(\([a-zA-Z._]+\)\\|\\)"
	     "%{[a-zA-Z0-9._+-]+}"
	     "%ifarch"
	     "%ifos"
	     "%ifnos"
	     "%else"
	     "%endif"
	     )
	   "\\|")
	  (if (featurep 'xemacs)
	      'font-lock-string-face
	    'font-lock-function-name-face))
	 (cons
	  (mapconcat
	   'identity
	   '("$RPM_SOURCE_DIR"
	     "$RPM_BUILD_DIR"
	     "$RPM_DOC_DIR"
	     "$RPM_ROOT_DIR"
	     "$RPM_BUILD_ROOT"
	     "$RPM_INSTALL_PREFIX"
	     "$RPM_OPT_FLAGS"
	     "$RPM_ARCH"
	     "$RPM_OS"
	     "$RPM_PACKAGE_NAME"
	     "$RPM_PACKAGE_VERSION"
	     "$RPM_PACKAGE_RELEASE"
	     "${[a-zA-Z_][a-zA-Z0-9_]*}"
	     )
	   "\\|")
	  'font-lock-variable-name-face)
	 ))
      (if (featurep 'xemacs)
	  (put 'rpm-mode 'font-lock
	       '(rpm-font-lock-keywords nil))
	(add-hook
	 'rpm-mode-hook
	 (lambda ()
	   (make-local-variable 'font-lock-defaults)
	   (setq font-lock-defaults
		 '((rpm-font-lock-keywords) nil nil
		   ((?/ . "w") (?~ . "w") (?. . "w") (?- . "w") (?_ . "w"))))))
	)
      ))
      
;;------------------- Setup -----------------------------------
(setq auto-mode-alist (append '(("\\.spec$" . rpm-mode))
                              auto-mode-alist))
