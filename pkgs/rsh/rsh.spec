%global momorel 33
%define	_snapshot	-pre20000412

Summary: Clients for remote access commands (rsh, rlogin, rcp).
Name: rsh
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-rsh-%{version}.tar.gz
Source1: rexec.pam
Source2: rlogin.pam
Source3: rsh.pam
Source4: http://www.tc.cornell.edu/~sadd/rexec-1.5.tar.gz
Source5: shell-xinetd
Source6: login-xinetd
Source7: exec-xinetd
Patch2: netkit-rsh-0.17-sectty.patch
Patch3: netkit-rsh-0.10-rexec.patch
Patch4: netkit-rsh-0.10-stdarg.patch
Patch5: netkit-rsh-0.16-jbj.patch
Patch6: netkit-rsh-0.16-pamfix.patch
Patch7: netkit-rsh-0.16-jbj2.patch
Patch8: netkit-rsh-0.16-jbj3.patch
Patch9: netkit-rsh-0.16-jbj4.patch
Patch10: netkit-rsh-0.16-prompt.patch
Patch11: netkit-rsh-0.16-rlogin=rsh.patch
Patch12: netkit-rsh-0.16-nokrb.patch
Patch13: netkit-rsh-0.17-pre20000412-jbj5.patch

Patch100: netkit-rsh-0.17-glibc2790.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
The rsh package contains a set of programs which allow users to run
commmands on remote machines, login to other machines and copy files
between machines (rsh, rlogin and rcp).  All three of these commands
use rhosts style authentication.  This package contains the clients
needed for all of these services.
The rsh package should be installed to enable remote access to other
machines.

%package server
Summary: Servers for remote access commands (rsh, rlogin, rcp).
Group: System Environment/Daemons
Requires: pam >= 0.59

%description server
The rsh-server package contains a set of programs which allow users
to run commmands on remote machines, login to other machines and copy
files between machines (rsh, rlogin and rcp).  All three of these
commands use rhosts style authentication.  This package contains the
servers needed for all of these services.  It also contains a server
for rexec, an alternate method of executing remote commands.
All of these servers are run by inetd and configured using
/etc/inetd.conf and PAM.  The rexecd server is disabled by default,
but the other servers are enabled.

The rsh-server package should be installed to enable remote access
from other machines.

%prep
%setup -q -n netkit-rsh-%{version} -a 4
%patch2 -p1 -b .sectty
%patch3 -p1 -b .rexec
%patch4 -p1 -b .stdarg
%patch5 -p1 -b .jbj

# XXX patches {6,7,8} not applied
#%patch6 -p1 -b .pamfix
#%patch7 -p1 -b .jbj2
#%patch8 -p1 -b .jbj3

%patch9 -p1 -b .jbj4
%patch10 -p1 -b .prompt
%patch11 -p1 -b .rsh
%patch12 -p1 -b .rsh.nokrb

%patch13 -p1 -b .jbj5

%patch100 -p1 -b .glibc2790

# No, I don't know what this is doing in the tarball.
rm -f rexec/rexec

%build
sh configure
perl -pi -e '
    s,^CC=.*$,CC=cc,;
    s,-O2,\$(RPM_OPT_FLAGS),;
    s,^BINDIR=.*$,BINDIR=%{_bindir},;
    s,^MANDIR=.*$,MANDIR=%{_mandir},;
    s,^SBINDIR=.*$,SBINDIR=%{_sbindir},;
    ' MCONFIG

make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man{1,5,8}
mkdir -p %{buildroot}/etc/pam.d

make INSTALLROOT=%{buildroot} BINDIR=%{_bindir} MANDIR=%{_mandir} install

install -m 644 $RPM_SOURCE_DIR/rexec.pam %{buildroot}/etc/pam.d/rexec
install -m 644 $RPM_SOURCE_DIR/rlogin.pam %{buildroot}/etc/pam.d/rlogin
install -m 644 $RPM_SOURCE_DIR/rsh.pam %{buildroot}/etc/pam.d/rsh

mkdir -p %{buildroot}/etc/xinetd.d/
install -m644 %SOURCE5 %{buildroot}/etc/xinetd.d/shell
install -m644 %SOURCE6 %{buildroot}/etc/xinetd.d/login
install -m644 %SOURCE7 %{buildroot}/etc/xinetd.d/exec

rm -f %{buildroot}/usr/share/man/rexec.1*

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(4755,root,root)	%{_bindir}/rcp
%{_bindir}/rexec
%attr(4755,root,root)	%{_bindir}/rlogin
%attr(4755,root,root)	%{_bindir}/rsh
%{_mandir}/man1/*.1*

%files server
%defattr(-,root,root)
%config	/etc/pam.d/rsh
%config	/etc/pam.d/rlogin
%config	/etc/pam.d/rexec
%{_sbindir}/in.rexecd
%{_sbindir}/in.rlogind
%{_sbindir}/in.rshd
%config(missingok,noreplace) /etc/xinetd.d/*
%{_mandir}/man8/*.8*

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-33m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-32m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-31m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-30m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-28m)
- rebuild against rpm-4.6

* Sun Apr 06 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.17-27m)
- add Patch100: netkit-rsh-0.17-glibc2790.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-26m)
- rebuild against gcc43

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.17-25m)
- rebuild against pam-0.99.7-1m
- update rexec.pam rlogin.pam rsh.pam

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-24m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.17-23k)
- use %%{momorel}

* Mon May 27 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.17-22k)
  /etc/pam.d/* changed.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.17-18k)
- update to 0.17 release.

* Thu May 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.17-14k)
- modified  /etc/xinetd.d/* file name for fixing to xinetdconf

* Mon Apr 16 2001 Akira Higuchi <a@kondara.org>
- (0.17-13k)
- modified *-xinetd file

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.17-11k)
- modified *-xinetd file

* Sun Jan 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.17-9k)
- modified rexec-xinetd

* Thu Jan  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.17-7k)
- set disable on default

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.17-3k)
- only up rel num for FHS

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- Kondarized.

* Fri Jul 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix typo in the rlogin PAM config file
- continue the tradition of messed-up release numbers

* Tue Jul 18 2000 Bill Nottingham <notting@redhat.com>
- add description & default to xinetd file

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.
- update to 0.17.

* Thu Jun  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- modify PAM setup to use system-auth

* Mon May 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- expunge all mentions of kerberos authentication or DES encryption using
  kerberos from the man pages

* Thu May 25 2000 Trond Eivind Glomsrod <teg@redhat.com>
- switched to xinetd

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Fri Mar 04 2000 Nalin Dahyabhai <nalin@redhat.com>
- make rlogin still work correctly when argv[0] = "rsh"

* Mon Feb 28 2000 Jeff Johnson <jbj@redhat.com>
- workaround (by explicitly prompting for password) #4328 and #9715.

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- mark pam config files as %config.

* Fri Feb  4 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Sun Jan 30 2000 Bill Nottingham <notting@redhat.com>
- remove bogus rexec binary when building; it causes weirdness

* Fri Jan 28 2000 Jeff Johnson <jbj@redhat.com>
- Make sure that rshd is compiled with -DUSE_PAM.

* Mon Jan 10 2000 Jeff Johnson <jbj@redhat.com>
- Fix bug in rshd (hangs forever with zombie offspring) (#8313).

* Wed Jan  5 2000 Jeff Johnson <jbj@redhat.com>
- fix the PAM fix yet again (#8133).

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.
- dup setuid bits into files list.

* Fri Jul 30 1999 Jeff Johnson <jbj@redhat.com>
- update to rexec-1.5 client (#4262)

* Wed May 19 1999 Jeff Johnson <jbj@redhat.com>
- fix broken rexec protocol in in.rexecd (#2318).

* Tue May  4 1999 Justin Vallon <vallon@mindspring.com>
- rcp with error was tricked by stdarg side effect (#2300)

* Thu Apr 15 1999 Michael K. Johnson <johnsonm@redhat.com>
- rlogin pam file was missing comment magic

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- strip rexec

* Fri Mar 26 1999 Jeff Johnson <jbj@redhat.com>
- rexec needs pam_set_item() (#60).
- clarify protocol in rexecd.8.
- add rexec client from contrib.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 14 1998 Erik Troan <ewt@redhat.com>
- built against new ncurses

* Sat Apr  5 1998 Marcelo F. Vianna <m-vianna@usa.net>
- Packaged for RH5.0 (Hurricane)

* Tue Oct 14 1997 Michael K. Johnson <johnsonm@redhat.com>
- new pam conventions

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
