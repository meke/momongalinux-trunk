%global momorel 16

%define enginever 4.0.4
%define filtersver 4.0.4

Summary: Database of printers and printer drivers
Name:       foomatic
Version:    %{enginever}
Release:    %{momorel}m%{?dist}
License:    GPLv2+
Group: System Environment/Libraries

# The database engine.
Source0: http://www.openprinting.org/download/foomatic/foomatic-db-engine-%{enginever}.tar.gz

# The CUPS driver and filter.
Source1: http://www.openprinting.org/download/foomatic/foomatic-filters-%{filtersver}.tar.gz

## PATCHES FOR FOOMATIC-FILTERS (PATCHES 1 TO 100)

# Use libdir.
Patch1: foomatic-filters-libdir.patch

# Use mkstemp, not mktemp.
Patch2: foomatic-mkstemp.patch

## PATCHES FOR FOOMATIC-DB-ENGINE (PATCHES 101 TO 200)

# Use libdir.
Patch101: foomatic-db-engine-libdir.patch

# Handle non-UTF-8 encodings in imported PPD files.
Patch102: foomatic-bad-utf8.patch

# Fixed installation path for perl module.
Patch103: foomatic-db-engine-perl.patch

## PATCHES FOR FOOMATIC-DB-HPIJS (PATCHES 201 TO 300)

Url:            http://www.linuxprinting.org
BuildRequires:  perl >= 3:5.8.1
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  libxml2-devel
BuildRequires:  autoconf, automake
BuildRequires:  cups
BuildRequires:  ghostscript-devel
Requires:      %{name}-filters = %{version}-%{release}
Requires:       perl >= 5.12.0
Requires:       %(eval `perl -V:version`; echo "perl(:MODULE_COMPAT_$version)")
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:	perl(Foomatic::GrovePath)
# For 'rm' and '/sbin/service' in post
Requires(pre): fileutils initscripts

# foomatic-filters checks for a conversion utility (bug #124931).
BuildRequires: mpage

# Make sure we get postscriptdriver tags.  Safe to comment out when
# bootstrapping a new architecture.
BuildRequires: pycups, cups, foomatic, foomatic-db

Requires: foomatic-db
Requires: cups
Requires: ghostscript

%description
Foomatic is a comprehensive, spooler-independent database of printers,
printer drivers, and driver descriptions. It contains utilities to
generate driver description files and printer queues for CUPS, LPD,
LPRng, and PDQ using the database. There is also the possibility to
read the PJL options out of PJL-capable laser printers and take them
into account at the driver description file generation.

There are spooler-independent command line interfaces to manipulate
queues (foomatic-configure) and to print files/manipulate jobs
(foomatic printjob).

The site http://www.linuxprinting.org/ is based on this database.

%package filters
Summary: CUPS print filters for the foomatic package
License: GPLv2+
Group: System Environment/Libraries

%description filters
CUPS print filters for the foomatic package.

%prep
%setup -q -c -a 1

pushd foomatic-filters-%{filtersver}
%patch1 -p1 -b .libdir
%patch2 -p1 -b .mkstemp
aclocal
automake
autoconf
popd

pushd foomatic-db-engine-%{enginever}
chmod a+x mkinstalldirs
%patch101 -p1 -b .libdir
%patch102 -p1
%patch103 -p1 -b .perl
aclocal
autoconf
popd

%build
export LIB_CUPS=/usr/lib/cups               # /usr/lib NOT libdir
export CUPS_BACKENDS=/usr/lib/cups/backend  # /usr/lib NOT libdir
export CUPS_FILTERS=/usr/lib/cups/filter    # /usr/lib NOT libdir
export CUPS_PPDS=%{_datadir}/cups/model

### MUST NOT USE THE %%{_smp_mflags}.
%undefine _smp_mflags

pushd foomatic-filters-%{filtersver}
%configure
make PREFIX=%{_prefix} CFLAGS="$RPM_OPT_FLAGS"
popd

pushd foomatic-db-engine-%{enginever}
%configure --disable-xmltest
make PREFIX=%{_prefix} CFLAGS="$RPM_OPT_FLAGS"
popd

%install
rm -rf %{buildroot}

pushd foomatic-filters-%{filtersver}
eval `perl '-V:installvendorlib' '-V:installvendorarch'`
mkdir -p $RPM_BUILD_ROOT/$installvendorlib
export INSTALLSITELIB=$RPM_BUILD_ROOT/$installvendorlib
export INSTALLSITEARCH=$RPM_BUILD_ROOT/$installvendorarch
make    DESTDIR=%buildroot PREFIX=%{_prefix} \
        INSTALLSITELIB=$RPM_BUILD_ROOT/$installvendorlib \
        INSTALLSITEARCH=$RPM_BUILD_ROOT/$installvendorarch \
        install-main install-cups
popd

pushd foomatic-db-engine-%{enginever}
make    DESTDIR=%buildroot PREFIX=%{_prefix} \
        INSTALLSITELIB=$installvendorlib \
        INSTALLSITEARCH=$installvendorarch \
        install
popd

# Use relative, not absolute, symlink for CUPS filter and driver.
ln -sf ../../../bin/foomatic-rip %{buildroot}/usr/lib/cups/filter/foomatic-rip
ln -sf ../../../bin/foomatic-ppdfile %{buildroot}/usr/lib/cups/driver/foomatic

mkdir -p %{buildroot}%{_var}/cache/foomatic

echo cups > %{buildroot}%{_sysconfdir}/foomatic/defaultspooler

# Remove things we don't ship.
rm -rf  %{buildroot}%{_libdir}/perl5/site_perl \
        %{buildroot}%{_libdir}/ppr \
        %{buildroot}%{_sysconfdir}/foomatic/filter.conf.sample \
        %{buildroot}%{_datadir}/foomatic/templates
find %{buildroot} -name .packlist | xargs rm -f

%post
/bin/rm -f /var/cache/foomatic/*
exit 0

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc --parents foomatic-db-engine-%{enginever}/COPYING
%config(noreplace) %{_sysconfdir}/foomatic/defaultspooler
%{_bindir}/foomatic-combo-xml
%{_bindir}/foomatic-compiledb
%{_bindir}/foomatic-configure
%{_bindir}/foomatic-datafile
%{_bindir}/foomatic-perl-data
%{_bindir}/foomatic-ppd-options
%{_bindir}/foomatic-ppd-to-xml
%{_bindir}/foomatic-ppdfile
%{_bindir}/foomatic-printjob
%{_bindir}/foomatic-searchprinter
%{_sbindir}/*
%{perl_vendorlib}/Foomatic
/usr/lib/cups/driver/*
%{_mandir}/man1/foomatic-combo-xml.1*
%{_mandir}/man1/foomatic-compiledb.1*
%{_mandir}/man1/foomatic-configure.1*
%{_mandir}/man1/foomatic-perl-data.1*
%{_mandir}/man1/foomatic-ppd-options.1*
%{_mandir}/man1/foomatic-ppdfile.1*
%{_mandir}/man1/foomatic-printjob.1*
%{_mandir}/man8/*
%{_var}/cache/foomatic

%files filters
%defattr(-,root,root,-)
%doc --parents foomatic-filters-%{filtersver}/COPYING
%dir %{_sysconfdir}/foomatic
%config(noreplace) %{_sysconfdir}/foomatic/filter.conf
%{_bindir}/foomatic-rip
/usr/lib/cups/backend/beh
/usr/lib/cups/filter/foomatic-rip
%{_mandir}/man1/foomatic-rip.1*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-16m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.4-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.4-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.4-1m)
- update 4.0.4
- sync Fedora

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-5m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-2m)
- rebuild against perl-5.10.1

* Sun Jun  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-1m)
- sync with Fedora 11 (4.0.0-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-13m)
- rebuild against rpm-4.6

* Sun Jul 20 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.0.2-12m)
- fix lib64 patch of dg-engine

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.2-11m)
- rebuild against gcc43

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-10m)
- rebuild against perl-5.10.0

* Tue Jan  2 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.0.2-9m)
- add BuildRequire: cups
- fix for initial build.

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-8m)
- revise %%files to avoid conflicting with cups

* Thu May 11 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.0.2-7m)
- add lib64 patch

* Fri May  5 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.2-6m)
- snapver up

* Sun Jan 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.2-5m)
- sync with fc-devel

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.2-4m)
- rebuilt against perl-5.8.7

* Fri Apr  8 2005 kourin <kourin@rinn.ne.jp>
- (3.0.2-3m)
- add Requires: foomatic-db

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (3.0.2-2m)
- enable x86_64.
- correct symlink.

* Thu Nov 11 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2
  security fix release. (Security Advisory CAN-2004-0801).

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.0.1-1m)
  update to 3.0.1

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.0.1-0.1.20031121.3m)
- rebuild against for libxml2-2.6.8

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.1-0.1.20031121.2m)
- revised spec for enabling rpm 4.2.
- perl = 5.8.2 is toooooooo strict as well as Japanese traditional OYAJI.

* Fri Dec 19 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.0.1-0.1.20031121.1m)
- update to 3.0.1rc1
- do not require cups, since foomatic can also be used with LPRng,
  and LPRng conflicts with cups.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-0.20031102.2m)
- rebuild against perl-5.8.2

* Sun Nov 02 2003 Kenta MURATA <muraken2@nifty.com>
- (3.0.0-0.20031102.1m)
- version up.
- MUST NOT USE THE %%{_smp_mflags}.

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (3.0.0-0.20030509.1m)
- pretty spec file.

* Sat Jul  5 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.0.0-0.20030509.1m)
- update to post-3.0.0 release (CVS version as of 2003/05/09)

* Mon Mar 24 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.0.0-0.1.20030310.1m)
- update to post-3.0.0beta1 (CVS version as of 2003/03/10)
- add "-l" option to foomatic-compiledb

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.1-0.2002040603m)
- rebuild against perl-5.8.0

* Sat Apr  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.1-0.2002040602k)
- update database.
- zaoriku to Omni-printers.tar.bz2.

* Sat Apr  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.1-0.2001101810k)
- remove Omni-printers.tar.bz2 and foomatic-for-omni-printer.patch.

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (1.1-0.2001101808k)
- remove Foomatic::GrovePath module

* Wed Feb 27 2002 Kenta MURATA <muraken@kondara.org>
- (1.1-0.2001101806k)
- perl = 1:5.6.1

* Wed Feb  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.1-0.2001101804k)
- add option definition file for Canon BJC-700J.

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (1.1-0.2001101802k)
- merge from rawhide.

* Thu Oct 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20011018.1
- rolled to pull in latest information.
- added Omni printers to the printer list.

* Fri Oct 05 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20011005.1
- rolled to pull in an ia64 fix to foomatic-combo-xml.c

* Mon Oct 01 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20011001.1
- rolled to pull in foomatic fixes to foomatic-combo-xml.c

* Wed Sep 05 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010905.1
- upgraded to latest foomatic, we now have fast overview generation!
- this means that there is no prebuilt overview file.

* Tue Aug 28 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010828.1
- WOW! The latest foomatic uses Till Kamppeter's C based combo compiler.
- It is now fast enough that there is no real benifit to precompiling.
- NOTE: this forces the package to stop being noarched.

* Mon Aug 27 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010827.1
- fresh pull, maybe it fixes the build errors.

* Sat Aug 25 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010825.1
- did a fresh database pull, which contains the old japanese printers as well.
- removed japanese hack.

* Tue Aug 14 2001 Akira TAGOH <tagoh@redhat.com> 1.1-0.20010717.5
- Add Japanese printer entry.

* Mon Aug  6 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010717.4
- move the cache back to /var, sigh.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010717.2
- made foomatic pre-compute its db

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.1-0.20010717.1
- imported from mandrake.

* Tue Jul 17 2001 Till Kamppeter <till@mandrakesoft.com> 1.1-0.20010717mdk
- Added job listing/removal/manipulation and queue control to
  foomatic-printjob
- Support for printing multiple copies with PDQ

* Fri Jul 14 2001 Till Kamppeter <till@mandrakesoft.com> 1.1-0.20010714mdk
- Included the cupsomatic filter script
- When a queue is set up, default options can be set now
- Help messages of foomatic-configure and foomatic-printjob cleaned up.

* Fri Jul 13 2001 Till Kamppeter <till@mandrakesoft.com> 1.1-0.20010713mdk
- Many bugfixes in "foomatic-printjob".
- "foomatic-configure" adds the Foomatic config file directory automatically
  to the search paths of PDQ.
- Printing a help page under PDQ was broken.

* Thu Jul 12 2001 Stefan van der Eijk <stefan@eijk.nu> 1.1-0.20010712mdk
- BuildRequires:  perl-devel

* Wed Jul 11 2001 Till Kamppeter <till@mandrakesoft.com> 1.1-0.20010711mdk
- initial release.
- Deleted the obsolete drivers "stp", "cZ11", and "hpdj".
- Patch applied which flushes the memory cache regularly, otherwise
  foomatic-configure would hang when the Foomatic data of GIMP-Print is
  installed.
