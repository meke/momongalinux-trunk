%global momorel 16
%define ver 1.0.0b3

Version: 1.0.0
Release: %{momorel}m%{?dist}
Summary: UDF Tools - Linux UDF utilities
Group: Applications/System
Name: udftools
License: GPL
URL: http://www.trylinux.com/projects/udf/index.html
#URL: http://linux-udf.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/linux-udf/udftools-%{ver}.tar.gz 
NoSource: 0
Patch0: udftools-1.0.0b3-pktsetup-chardev.patch
Patch1: udftools-1.0.0b3-wrudf-gcc4.patch
Patch2: udftools-1.0.0b3-int_max.patch
Patch10: udftools-1.0.0b3-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: readline-devel >= 5.0

%description
The UDF tools let you perform certain actions on CD/DVD rewritable media.
(blanking, formatting with a UDF filesystem for packet writing, etc.).

%package devel
Summary: development libralies for udftools
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
The development libralies that the UDF tools let you perform certain actions
on CD/DVD rewritable media.

%prep
%setup -q -n %{name}-%{ver}
%patch0 -p1 -b .pktsetup-chardev
%patch1 -p1 -b .wrudf-gcc4
%patch2 -p1 -b .int_max
%patch10 -p1 -b .glibc210

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr (-, root, root)
%{_bindir}/mkudffs
%{_bindir}/cdrwtool
%{_bindir}/pktsetup
%{_bindir}/udffsck
%{_bindir}/wrudf
%{_mandir}/man*/*

%files devel
%defattr (-, root, root)
%{_libdir}/libudffs.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-14m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-13m)
- rebuild against readline6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-12m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-10m)
- apply glibc210 patch

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-9m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-6m)
- %%NoSource -> NoSource

* Wed Oct 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.0-5m)
- add INT_MAX patch for kernel-2.6.23

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-4m)
- delete libtool library

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-3m)
- rebuild against readline-5.0

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.0-2m)
- update to 1.0.0b3
- import patch0 and patch1 from FC

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.0.0-1m)
- modify release number only.

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kodnara.org>
- (1.0.0-0.0002002k)
- modify package version.
- modify URL. (for Super Kondara Antenna) 

* Sat Apr 20 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (1.0.0-0.0001002k)
- based upon 1.0.0b2
- initial release for Kondara MNU/Linux

