%global         momorel 2

Name:           perl-DateTime-Format-HTTP
Version:        0.42
Release:        %{momorel}m%{?dist}
Summary:        Date conversion routines
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Format-HTTP/
Source0:        http://www.cpan.org/authors/id/C/CK/CKRAS/DateTime-Format-HTTP-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-DateTime >= 0.17
BuildRequires:  perl-libwww-perl >= 1.44
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-DateTime >= 0.17
Requires:       perl-libwww-perl >= 1.44
Requires:       perl-Test-Simple >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides functions that deal the date formats used by the HTTP
protocol (and then some more).

%prep
%setup -q -n DateTime-Format-HTTP-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CREDITS LICENSE README
%{perl_vendorlib}/DateTime/Format/HTTP*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-2m)
- rebuild against perl-5.20.0

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-13m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-12m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-11m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-10m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-9m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-8m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-7m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-2m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-2m)
- full rebuild for mo7 release

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.38-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-3m)
- rebuild against perl-5.10.1

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-2m)
- modify BuildRequires

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
