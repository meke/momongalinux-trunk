%global         momorel 4
%global boost_version 1.55.0
%global         githash1 gcac7b57
%global         githash2 94d1bf3

Name:           Field3D
Version:        1.3.2
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
Summary:        Library for storing voxel data
License:        BSD
URL:            https://sites.google.com/site/field3d/
# The source archive is created on the fly at github.com:
# https://nodeload.github.com/imageworks/Field3D/tarball/v1.3.2
Source0:        imageworks-%{name}-v%{version}-0-%{githash1}.tar.gz
Patch0:         Field3D-1.3.2-libboost.patch
BuildRequires:  cmake doxygen
BuildRequires:  hdf5-devel
BuildRequires:  boost-devel >= %{boost_versin}
BuildRequires:  ilmbase-devel

%description
Field3D is an open source library for storing voxel data. It provides C++
classes that handle in-memory storage and a file format based on HDF5 that
allows the C++ objects to be written to and read from disk.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development headers and documentation for %{name}.

%prep
%setup -q -n imageworks-%{name}-%{githash2}
%patch0 -p1 -b .boost

%build
rm -rf build && mkdir build && pushd build
%cmake -DINSTALL_DOCS=OFF \
       ../

make %{?_smp_mflags}

%install
pushd build
make install DESTDIR=%{buildroot}
popd

install -D -m 0644 man/f3dinfo.1 %{buildroot}%{_mandir}/man1/f3dinfo.1

%check
pushd build
./unitTest

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc CHANGES COPYING README
%{_bindir}/f3dinfo
%{_libdir}/libField3D.so.*
%{_mandir}/man1/f3dinfo.1.*

%files devel
%doc docs/html/
%{_includedir}/Field3D/
%{_libdir}/libField3D.so

%changelog
* Wed Jan 15 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-4m)
- rebuild against hdf5
- remove "Requires: hdf5 = 1.8.10" because it is now generated automatically

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-2m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- import from Fedora

* Mon Dec  3 2012 Orion Poplawski <orion@cora.nwra.com> - 1.3.2-7
- Rebuild for hdf5 1.8.10

* Wed Aug  8 2012 David Malcolm <dmalcolm@redhat.com> - 1.3.2-6
- rebuild against boost-1.50

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue May 15 2012 Orion Poplawski <orion@cora.nwra.com> - 1.3.2-3
- Rebuild for hdf5 1.8.9
- Explicitly require the version of hdf5 built with

* Fri Mar 23 2012 Richard Shaw <hobbes1069@gmail.com> - 1.3.2-2
- Bump EVR for oops with F17 package to make sure rawhide package is newer.

* Tue Feb 28 2012 Richard Shaw <hobbes1069@gmail.com> - 1.3.2-1
- Update to latest release.

* Tue Feb 28 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.1-4
- Rebuilt for c++ ABI breakage

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sun Jan 05 2012 Richard Shaw <hobbes1069@gmail.com> - 1.2.1-2
- Fixed building under GCC 4.7.0.

* Sat Nov 12 2011 Richard Shaw <hobbes1069@gmail.com> - 1.2.1-1
- Initial release.
