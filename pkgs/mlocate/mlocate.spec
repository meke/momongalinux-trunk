%global momorel 4

Summary: An utility for finding files by name
Name: mlocate
Version: 0.22.4
Release: %{momorel}m%{?dist}
License: GPLv2
URL: https://fedorahosted.org/mlocate/
Group: Applications/System
Source0: https://fedorahosted.org/releases/m/l/mlocate/mlocate-%{version}.tar.xz
Source1: updatedb.conf
Source2: mlocate.cron
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): shadow-utils
Requires(triggerpostun): shadow-utils
Requires(post): grep, sed
Obsoletes: slocate <= 2.7-30
Provides: slocate

%description
mlocate is a locate/updatedb implementation.  It keeps a database of
all existing files and allows you to lookup files by name.

The 'm' stands for "merging": updatedb reuses the existing database to avoid
rereading most of the file system, which makes updatedb faster and does not
trash the system caches as much as traditional locate implementations.

%prep
%setup -q

%build
%configure --localstatedir=%{_localstatedir}/lib
make %{?_smp_mflags} groupname=slocate

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p' groupname=slocate

mkdir -p $RPM_BUILD_ROOT{%{_sysconfdir},/etc/cron.daily}
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/updatedb.conf
install -p -m 755 %{SOURCE2} $RPM_BUILD_ROOT/etc/cron.daily/mlocate.cron
# %%ghost semantics is so stupid
touch $RPM_BUILD_ROOT%{_localstatedir}/lib/mlocate/mlocate.db

%find_lang mlocate

%clean
rm -rf $RPM_BUILD_ROOT

%pre
getent group slocate >/dev/null || groupadd -g 21 -r -f slocate
exit 0

%post
if /bin/grep -q '^[^#]*DAILY_UPDATE' %{_sysconfdir}/updatedb.conf; then
    /bin/sed -i.rpmsave -e '/DAILY_UPDATE/s/^/#/' %{_sysconfdir}/updatedb.conf
fi

%triggerpostun -- slocate <= 2.7-30
getent group slocate >/dev/null || groupadd -g 21 -r -f slocate
exit 0

%files -f mlocate.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
/etc/cron.daily/mlocate.cron
%config(noreplace) %{_sysconfdir}/updatedb.conf
%attr(2711,root,slocate) %{_bindir}/locate
%{_bindir}/updatedb
%{_mandir}/man*/*
%dir %attr(0750,root,slocate) %{_localstatedir}/lib/mlocate
%ghost %{_localstatedir}/lib/mlocate/mlocate.db

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22.4-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22.4-1m)
- update to 0.22.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21.1-2m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21.1-1m)
- update to 0.21.1
-
- merge Fedora 0.21.1-3
- changelog is below
- Merge review fixes, based on a patch by Parag AN:
  - Use %%{_localstatedir}/lib instead of hard-coding /var/lib
  - Use %%{?_smp_mflags}
  - Preserve file time stamps
  - Only create the group if it doesn't exist, hide errors from rpm

* Thu Jul 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.21-2m)
- remove non ASCII code

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-1m)
- update to 0.21
- add Define PRUNENAMES to exclude .git
- merge fedora 0.21-1
- changelog is below
-
- * Mon Jun 30 2008 Miloslav Trmac <mitr@redhat.com> - 0.21-1
- - Update to mlocate-0.21
- - Define PRUNENAMES to exclude .svn and .hg
- 
- * Wed Apr  9 2008 Miloslav Trmac <mitr@redhat.com> - 0.20-1
- - Update to mlocate-0.20
- 
- * Mon Mar  3 2008 Miloslav Trmac <mitr@redhat.com> - 0.19-1
- - Update to mlocate-0.19
- - New home page at https://fedorahosted.org/mlocate/ .
- 
- * Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.18-2
- - Autorebuild for GCC 4.3
- 
- * Sat Aug 25 2007 Miloslav Trmac <mitr@redhat.com> - 0.18-1
- - Update to mlocate-0.18
- - Run updatedb with reduced I/O priority
-   Resolves: #254165
- 
- * Wed Apr 25 2007 Miloslav Trmac <mitr@redhat.com> - 0.17-1
- - Update to mlocate-0.17
-   Resolves: #237120

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-3m)
- rebuild against gcc43

* Tue Oct 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-2m)
- added mlocate-0.16-ptrdiff.patch

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-1m)
- update to 0.16 (sync Fedora)

* Sun May 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-2m)
- import to Momonga

* Sat Mar 18 2006 Miloslav Trmac <mitr@redhat.com> - 0.14-2
- Ship NEWS

* Sat Mar 18 2006 Miloslav Trmac <mitr@redhat.com> - 0.14-1
- Update to mlocate-0.14

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.12-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.12-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Sat Dec 31 2005 Miloslav Trmac <mitr@redhat.com> - 0.12-1
- Update to mlocate-0.12

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Dec  2 2005 Miloslav Trmac <mitr@redhat.com> - 0.11-2
- Comment out DAILY_UPDATE from updatedb.conf (#174693)

* Thu Nov 10 2005 Miloslav Trmac <mitr@redhat.com> - 0.11-1
- Update to mlocate-0.11
- Add scriptlets to create group slocate

* Thu Jul 28 2005 Miloslav Trmac <mitr@volny.cz> - 0.10-0.testing.1
- Update to mlocate-0.10

* Thu Jul 28 2005 Miloslav Trmac <mitr@volny.cz> - 0.09-0.testing.1
- Initial build.
