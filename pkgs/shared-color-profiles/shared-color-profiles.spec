%global momorel 4

Summary: Shared color profiles to be used in color management aware applications
Name: shared-color-profiles
Version: 0.1.1
Release: %{momorel}m%{?dist}
URL: http://github.com/hughsie/shared-color-profiles
Source0: http://people.freedesktop.org/~hughsient/releases/%{name}-%{version}.tar.gz
NoSource: 0
License: GPLv2+ and Public Domain and "zlib"
Group: System Environment/Libraries
BuildArch: noarch

Requires: color-filesystem

%description 
The shared-color-profiles package contains various profiles which are useful for
programs that are color management aware.
This package only contains the free profiles that can be safely distributed
with Fedora.

%package extra
Summary: More profiles for color management that are less commonly used.
Requires: %{name} = %{version}-%{release}

%description extra
More color profiles for color management that are less commonly used.
This may be useful for CMYK soft-proofing or for extra device support.

%prep
%setup -q

%build
%configure

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog
%dir %{_datadir}/shared-color-profiles

# Argyll
%dir %{_datadir}/color/icc/Argyll
%dir %{_datadir}/shared-color-profiles/Argyll
%{_datadir}/shared-color-profiles/Argyll/*

# common colorspaces
%{_datadir}/color/icc/Argyll/ClayRGB1998.icm
%{_datadir}/color/icc/Argyll/lab2lab.icm
%{_datadir}/color/icc/Argyll/sRGB.icm

# so we can display at least something in the CMYK dropdown
%{_datadir}/color/icc/Fogra27L.icc

# monitor test profiles
%{_datadir}/color/icc/bluish.icc
%{_datadir}/color/icc/AdobeGammaTest.icm

%files extra
%defattr(-,root,root,-)

# Oyranos
%dir %{_datadir}/color/icc/Oyranos
%{_datadir}/color/icc/Oyranos/FOGRA*.icc
%{_datadir}/color/icc/Oyranos/GRACoL*.icc
%{_datadir}/color/icc/Oyranos/SNAP*.icc
%{_datadir}/color/icc/Oyranos/SWOP*.icc
%{_datadir}/shared-color-profiles/Oyranos/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- import from Fedora 13

* Tue Apr 06 2010 Richard Hughes <rhughes@redhat.com> - 0.1.1-1
- New upstream release.
- Update to the latest version of the Fedora Packaging Guidelines
- Install some CMYK profiles to an -extra subpackage as most users will not
  need these and they are over 15Mb in size and will clog up the desktop spin.

* Mon Dec 07 2009 Richard Hughes <richard@hughsie.com> 0.1.0-1
- Initial import of 0.1.0

