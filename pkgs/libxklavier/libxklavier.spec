%global momorel 1
Summary:	High-level API for X Keyboard Extension
Name:		libxklavier
Version:	5.2.1
Release: %{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Libraries
URL: http://www.freedesktop.org/wiki/Software/LibXklavier
BuildRequires: libxml2-devel
BuildRequires: libxkbfile-devel
BuildRequires: libX11-devel
BuildRequires: libXi-devel
BuildRequires: libxml2-devel
BuildRequires: glib2-devel >= 2.6.0
BuildRequires: iso-codes-devel
BuildRequires: gobject-introspection-devel
Requires: iso-codes
Source: http://download.gnome.org/sources/libxklavier/5.2/%{name}-%{version}.tar.xz
NoSource: 0
# http://bugs.freedesktop.org/show_bug.cgi?id=22687
Patch0: flags.patch
Patch2: catch-more-xerrors.patch

%description
libxklavier is a library providing a high-level API for the X Keyboard
Extension (XKB). This library is intended to support XFree86 and other
commercial X servers. It is useful for creating XKB-related software
(layout indicators etc).

%package devel
Summary: Development files for libxklavier
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libxml2-devel

%description devel
This package contains libraries, header files and developer documentation
needed to develop libxklavier applications.

%prep
%setup -q
%patch0 -p1 -b .flags
%patch2 -p1 -b .catch-more-xerrors

%build

%configure \
  --disable-static \
  --with-xkb-base='%{_datadir}/X11/xkb' \
  --with-xkb-bin-base='%{_bindir}'

make V=1 %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/*.{a,la}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS NEWS README COPYING.LIB
%{_libdir}/libxklavier.so.16*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Xkl-1.0.typelib

%files devel
%defattr(-, root, root)

%{_libdir}/pkgconfig/libxklavier.pc
%{_libdir}/libxklavier.so
%{_includedir}/libxklavier/
%{_datadir}/gtk-doc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Xkl-1.0.gir

%changelog
* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.1-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1-3m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.1-1m)
- update to 5.1 but GNOME release (NOT ORIGINAL)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0-3m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.0-2m)
- disable-static

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0-1m)
- update to 4.0

* Thu Mar 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.9-1m)
- update to 3.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8-2m)
- rebuild against rpm-4.6

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.8-1m)
- update to 3.8

* Mon Oct 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7-1m)
- update to 3.7

* Mon Apr 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6-1m)
- update to 3.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5-1m)
- update to 3.5

* Sun Feb 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.4-1m)
- update to 3.4

* Sat Oct 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3-1m)
- update to 3.3

* Thu Mar 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Fri Nov  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1-1m)
- update to 3.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0-1m)
- update 3.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-2m)
- delete libtool library

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Tue Nov 22 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-3m)
- add gcc4 patch. import from Debian
- Patch0: libxklavier_2.0-0.2.0.0.1.gcc4.diff.gz

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- delete autoreconf and make check

* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.04-1m)
- version 1.04
- GNOME 2.8 Desktop
- add configure option : --enable-doxygen --with-xkb-base=/etc/X11/xkb

* Wed Dec 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.02-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.02-1m)
- version 1.02
- GNOME 2.6 Desktop

* Sat Nov 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.95-1m)
- version 0.95
