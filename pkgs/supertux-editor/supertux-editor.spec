%global momorel 15

%define targetlib %{buildroot}%{_prefix}/lib/supertux
%define name supertux-editor
%define version 0.3.0

Summary: the SuperTux Milestone 2 editor
Name: %{name}
Version: %{version}
Release: %{momorel}m%{?dist}
Source0: http://download.berlios.de/supertux/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: supertux-editor-0.3.0-setting.patch
Patch1: supertux-editor-0.3.0-mono-1.2.6.patch
Patch2: supertux-editor-0.3.0-mono-2.8.patch

License: GPL
Group: Amusements/Games
URL: http://supertux.berlios.de/

Requires: supertux >= %{version}
Requires: mono-core >= 1.1.13
Requires: gtk-sharp2 >= 2.12.0

BuildRequires: jam

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is an unstable release of the SuperTux Milestone 2 editor. Do not
expect everything to work smoothly and save often...

%prep
rm -rf --preserve-root %{buildroot}

%setup -q
%patch0 -p1 -b .setting
%patch1 -p1 -b .mono
%patch2 -p1 -b .mono28

%build
jam

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{targetlib}
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/pixmaps/

echo "exec mono %{_prefix}/lib/supertux/supertux-editor.exe" > \
    %{buildroot}%{_bindir}/supertux-editor
chmod 755 %{buildroot}%{_bindir}/supertux-editor
install -m 755 supertux-editor.exe %{targetlib}

install -m 755 Lisp.dll %{targetlib}
install -m 755 LispReader.dll %{targetlib}
install -m 755 Resources.dll %{targetlib}
install -m 755 gtkgl-sharp.dll %{targetlib}
install -m 755 libeditor.dll %{targetlib}

install -m 644 gtkgl-sharp.dll.config %{targetlib}
install -m 644 libeditor.dll.config %{targetlib}

install -m 644 supertux-editor.desktop %{buildroot}%{_datadir}/applications
install -m 644 supertux-editor.png %{buildroot}%{_datadir}/pixmaps
install -m 644 supertux-editor.xpm %{buildroot}%{_datadir}/pixmaps

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README
%{_bindir}/*
%{_prefix}/lib/supertux/*
%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/supertux-editor.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-14m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-13m)
- build fix (add patch2)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-12m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-9m)
- rebuild against gcc43

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-8m)
- rebuild against gtk-sharp2-2.12.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-7m)
- %%NoSource -> NoSource

* Sun Dec 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-6m)
- add patch1 for mono-1.2.6

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-5m)
- go to Main

* Thu Jan  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-4m)
- install *.dll.config
- add patch0 (default directory)

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-3m)
- revise %%files section again...

* Wed Jan  3 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3.0-2m)
- modify library dir in %files section

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- initial build for Momonga Linux
