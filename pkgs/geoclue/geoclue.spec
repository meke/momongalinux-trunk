%global momorel 1

%global majorver 0.12
%global minorver 99

Name:          geoclue
Version:       %{majorver}.%{minorver}
Release:       %{momorel}m%{?dist}
Summary:       A modular geoinformation service

Group:         System Environment/Libraries
License:       LGPLv2
URL:           http://geoclue.freedesktop.org/
Source0:       http://freedesktop.org/~hadess/%{name}-%{version}.tar.gz
NoSource:      0

BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: gammu-devel
BuildRequires: glib2-devel
BuildRequires: libxml2-devel
BuildRequires: libsoup-devel
BuildRequires: GConf2-devel
BuildRequires: gtk2-devel
BuildRequires: NetworkManager-devel >= 0.8.997
BuildRequires: NetworkManager-glib-devel >= 0.8.997
BuildRequires: gypsy-devel
BuildRequires: gtk-doc
Obsoletes: geoclue-gpsd

Requires: dbus

%description
Geoclue is a modular geoinformation service built on top of the D-Bus 
messaging system. The goal of the Geoclue project is to make creating 
location-aware applications as simple as possible. 

%package devel
Summary: Development package for geoclue
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-devel
Requires: libxml2-devel
Requires: pkgconfig

%description devel
Files for development with geoclue.

%package doc
Summary: Developer documentation for geoclue
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Developer documentation for geoclue

%package gui
Summary: Testing gui for geoclue
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description gui
Testing gui for geoclue

%package gypsy
Summary: gypsy provider for geoclue
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description gypsy
A gypsy provider for geoclue

%package gsmloc
Summary: gsmloc provider for geoclue
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description gsmloc
A gsmloc provider for geoclue

%prep
%setup -q

%build
%configure --disable-static --enable-gtk-doc --enable-networkmanager=yes --enable-gypsy=yes --enable-skyhook=yes --enable-gsmloc=yes --enable-gpsd=no
%make  V=1

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# Install the test gui as it seems the test isn't installed any more
mkdir %{buildroot}%{_bindir}
cp test/.libs/geoclue-test-gui %{buildroot}%{_bindir}/

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%dir %{_datadir}/geoclue-providers
%{_libdir}/libgeoclue.so.0
%{_libdir}/libgeoclue.so.0.0.0
%{_datadir}/GConf/gsettings/geoclue
%{_datadir}/glib-2.0/schemas/org.freedesktop.Geoclue.gschema.xml
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Master.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Example.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Geonames.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Hostip.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Localnet.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Manual.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Nominatim.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Plazes.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Skyhook.service
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Yahoo.service
%{_datadir}/geoclue-providers/geoclue-example.provider
%{_datadir}/geoclue-providers/geoclue-geonames.provider
%{_datadir}/geoclue-providers/geoclue-hostip.provider
%{_datadir}/geoclue-providers/geoclue-localnet.provider
%{_datadir}/geoclue-providers/geoclue-manual.provider
%{_datadir}/geoclue-providers/geoclue-nominatim.provider
%{_datadir}/geoclue-providers/geoclue-plazes.provider
%{_datadir}/geoclue-providers/geoclue-skyhook.provider
%{_datadir}/geoclue-providers/geoclue-yahoo.provider
%{_libexecdir}/geoclue-example
%{_libexecdir}/geoclue-geonames
%{_libexecdir}/geoclue-hostip
%{_libexecdir}/geoclue-localnet
%{_libexecdir}/geoclue-manual
%{_libexecdir}/geoclue-nominatim
%{_libexecdir}/geoclue-master
%{_libexecdir}/geoclue-plazes
%{_libexecdir}/geoclue-skyhook
%{_libexecdir}/geoclue-yahoo

%files devel
%defattr(-,root,root,-)
%{_includedir}/geoclue
%{_libdir}/pkgconfig/geoclue.pc
%{_libdir}/libgeoclue.so

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/geoclue/

%files gui
%defattr(-,root,root,-)
%{_bindir}/geoclue-test-gui

%files gypsy
%defattr(-,root,root,-)
%{_libexecdir}/geoclue-gypsy
%{_datadir}/geoclue-providers/geoclue-gypsy.provider
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Gypsy.service

%files gsmloc
%defattr(-,root,root,-)
%{_libexecdir}/geoclue-gsmloc
%{_datadir}/geoclue-providers/geoclue-gsmloc.provider
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.Providers.Gsmloc.service

%changelog
* Thu Aug  2 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.99-1m)
- update to 0.12.99

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-10m)
- fix BTS #453 by adding source0

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-9m)
- change source URL
-- see BTS #453, thanks to futoshi-san

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-8m)
- reimport from fedora
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-7m)
- build fix

* Mon Aug 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-6m)
- disable gpsd support for a while. it seems not to be maintained

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-5m)
- rebuild against NetworkManager-0.8.999

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-3m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-3m)
- add %%files

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.0-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0 official release version
- geoclue-gpsd was OBSOLETED

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.1.1-0.0.20091026.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.1.1-0.0.20091026.1m)
- update to 20091026 git version
- rebuild against gammu-1.26.90
- almost sync with fedora

* Wed Jun 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.1.1-0.0.20090310.1m)
- import from Fedora 11 for webkitgtk
- create gsmloc subpackage
- apply gammu124 patch

* Thu Apr 09 2009 Peter Robinson <pbrobinson@gmail.com> 0.11.1.1-0.3
- Fix install of test gui

* Sun Mar 29 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.11.1.1-0.2
- Rebuild for new gpsd

* Tue Mar 10 2009 Peter Robinson <pbrobinson@gmail.com> 0.11.1.1-0.1
- Move to a git snapshot until we finally get a new stable release

* Wed Mar 4 2009 Peter Robinson <pbrobinson@gmail.com> 0.11.1-15
- Move docs to noarch, a few spec file cleanups

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.1-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 22 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-13
- Fix summary

* Thu Jul 31 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-12
- Once more for fun

* Thu Jul 31 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-11
- Increment build number to allow for clean F-8 and F-9 to F-10 upgrade

* Wed Jul 2 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-6
- Fixed spec file so gpsd and gypsy are actually properly in a subpackage

* Sun May 18 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-5
- Added gypsy and gpsd providers to build as sub packages

* Mon Apr 28 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-4
- Moved api documentation to -devel

* Sat Apr 26 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-3
- Cleanup requires and group for test gui

* Sat Apr 26 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-2
- Some spec file cleanups

* Fri Apr 25 2008 Peter Robinson <pbrobinson@gmail.com> 0.11.1-1
- Initial package
