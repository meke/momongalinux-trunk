%global momorel 1

%define drupaldir %{_datadir}/drupal
Name: drupal
Version: 6.27
Release: %{momorel}m%{?dist}
Summary: An open-source content-management platform

Group: Applications/Publishing
License: GPLv2+
URL: http://www.drupal.org/
Source0: http://ftp.drupal.org/files/projects/%{name}-%{version}.tar.gz
NoSource: 0
Source1: drupal.conf.dist
Source2: drupal-README.momonga
Source3: drupal-cron
Patch0:  %{name}-6.26-scripts-noshebang.patch

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: php, php-gd, php-mbstring, wget

%description
Equipped with a powerful blend of features, Drupal is a Content Management 
System written in PHP that can support a variety of websites ranging from
personal weblogs to large community-driven websites.  Drupal is highly
configurable, skinnable, and secure.

%prep

%setup -q

%patch0 -p1 
chmod -x scripts/drupal.sh

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{drupaldir}
cp -pr * %{buildroot}%{drupaldir}
cp -pr .htaccess %{buildroot}%{drupaldir}
mkdir -p %{buildroot}%{_sysconfdir}/httpd
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
cp -pr %SOURCE1 %{buildroot}%{_sysconfdir}/httpd/conf.d
mkdir -p %{buildroot}%{_sysconfdir}/drupal
mv %{buildroot}%{drupaldir}/sites/* %{buildroot}%{_sysconfdir}/drupal
rmdir %{buildroot}%{drupaldir}/sites
ln -s ../../..%{_sysconfdir}/drupal %{buildroot}%{drupaldir}/sites
mkdir -p %{buildroot}%{_docdir}
cp -pr %SOURCE2 .
install -D -p -m 0644 %SOURCE3 %{buildroot}%{_sysconfdir}/cron.hourly/drupal 
mkdir -p %{buildroot}%{_localstatedir}/lib/drupal
ln -s ../../..%{_localstatedir}/lib/drupal %{buildroot}%{drupaldir}/files

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGELOG.txt INSTALL* LICENSE* MAINTAINERS.txt UPGRADE.txt drupal-README.momonga sites/all/README.txt
%{drupaldir}
%config(noreplace) %{drupaldir}/.htaccess
%exclude %{drupaldir}/CHANGELOG.txt
%exclude %{drupaldir}/INSTALL* 
%exclude %{drupaldir}/LICENSE* 
%exclude %{drupaldir}/MAINTAINERS.txt 
%exclude %{drupaldir}/UPGRADE.txt
%dir %{_sysconfdir}/drupal/
%config(noreplace) %{_sysconfdir}/drupal/all
%exclude %{_sysconfdir}/drupal/all/README.txt
%config(noreplace) %{_sysconfdir}/drupal/default
%config(noreplace) %{_sysconfdir}/httpd/conf.d/drupal.conf.dist
%attr(755,root,apache) %{_sysconfdir}/cron.hourly/drupal
%dir %attr(775,root,apache) %{_localstatedir}/lib/drupal/

%changelog
* Fri Dec 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.27-1m)
- [SECURITY] CVE-2012-5651 CVE-2012-5652 CVE-2012-5653
- update to 6.27

* Fri May 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.26-1m)
- [SECURITY] CVE-2012-2341 CVE-2012-2922
- update to 6.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.19-2m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.19-1m)
- [SECURITY] DRUPAL-SA-CORE-2010-002 (fixed in 6.18)
- update to 6.19

* Sat Jun  5 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.17-1m)
- update to 6.17

* Fri Mar 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.16-1m)
- [SECURITY] CVE-2010-2250 CVE-2010-2471 CVE-2010-2472 CVE-2010-2473
- [SECURITY] SA-CORE-2010-001
- update to 6.16

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.15-1m)
- [SECURITY] CVE-2009-4369 CVE-2009-4370 CVE-2009-4371
- [SECURITY] DRUPAL-SA-CORE-2009-009
- update to 6.15

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.14-1m)
- [SECURITY] DRUPAL-SA-CORE-2009-008
- update to 6.14

* Mon Jul 13 2009 Ryu SASAOKA <ryu@momonga-linux.org> 
- (6.13-2m)
- stop cron.hourly

* Mon Jul  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13-1m)
- update 6.13
- [SECURITY] CVE-2009-2372 CVE-2009-2373 CVE-2009-2374
- [SECURITY] SA-CORE-2009-007

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.12-1m)
- [SECURITY] CVE-2009-1844
- [SECURITY] SA-CORE-2009-006
- [SECURITY] http://drupal.org/node/461886
- update to 6.12

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.11-1m)
- [SECURITY] CVE-2009-1575 CVE-2009-1576
- [SECURITY] DRUPAL-SA-CORE-2009-005
- [SECURITY] http://drupal.org/node/449078
- update to 6.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-1m)
- [SECURITY] DRUPAL-SA-CORE-2009-001
- [SECURITY] http://drupal.org/node/358957
- update to 6.9

* Fri Dec 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8-1m)
- [SECURITY] CVE-2008-6532 CVE-2008-6533
- [SECURITY] DRUPAL-SA-2008-073
- [SECURITY] http://drupal.org/node/345833
- update to 6.8

* Fri Oct 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.6-1m)
- [SECURITY] CVE-2008-6170 CVE-2008-6171 CVE-2008-6176
- [SECURITY] DRUPAL-SA-2008-067
- [SECURITY] http://drupal.org/node/324824
- update to 6.6

* Wed Oct 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5-1m)
- [SECURITY] DRUPAL-SA-2008-060
- [SECURITY] http://drupal.org/node/318706
- sync with F9 updates (6.5-1)
- * Thu Oct 09 2008 Jon Ciesla <limb@jcomserv.net> - 6.5-1
- - Upgrade to 6.5, SA-2008-060.
- - Added notes to README and drupal.conf re CVE-2008-3661.

* Mon Aug 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4-1m)
- [SECURITY] SA-2008-047 - Drupal core - Multiple vulnerabilities
- see http://drupal.org/node/295053
- update to 6.4

* Mon Apr 14 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (6.2-1m)
- version up 6.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc43

* Sat Feb 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (6.0-1m)
- version up 6.0

* Wed Feb 13 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5.6-1m)
- import from fedora

* Fri Jan 11 2008 Jon Ciesla <limb@jcomserv.net> - 5.6-1
- Upgrade to 5.6, upstream security fixes.

* Mon Jan 07 2008 Jon Ciesla <limb@jcomserv.net> - 5.5-2
- Include .htaccess file, BZ 427720.

* Mon Dec 10 2007 Jon Ciesla <limb@jcomserv.net> - 5.5-1
- Upgrade to 5.5, critical fixes.

* Thu Dec 06 2007 Jon Ciesla <limb@jcomserv.net> - 5.4-2
- Fix /files -> /var/lib/drupal dir perms, BZ 414761.

* Wed Dec 05 2007 Jon Ciesla <limb@jcomserv.net> - 5.4-1
- Upgrade to 5.4, advisory ID DRUPAL-SA-2007-031.
- Augmented README regarding symlinks, BZ 254228.

* Thu Oct 18 2007 Jon Ciesla <limb@jcomserv.net> - 5.3-1
- Upgrade to 5.3, fixes:
- HTTP response splitting.
- Arbitrary code execution.
- Cross-site scripting.
- Cross-site request forgery.
- Access bypass.

* Mon Sep 24 2007 Jon Ciesla <limb@jcomserv.net> - 5.2-3
- Minor doc correction, BZ 301541.

* Thu Aug 16 2007 Jon Ciesla <limb@jcomserv.net> - 5.2-2
- License tag correction.

* Thu Jul 26 2007 Jon Ciesla <limb@jcomserv.net> - 5.2-1
- Upgrade to 5.2, Cross-site request forgery fix.

* Fri Jul 20 2007 Jon Ciesla <limb@jcomserv.net> - 5.1-5
- Corrected buildroot.
- Moved /etc/drupal/all/README.txt to correct place.

* Wed Jul 04 2007 Jon Ciesla <limb@jcomserv.net> - 5.1-4
- Made settings.php not readonly by default, with note in drupal-README.fedora
- Locked down initial security configuration, documented steps required.
- Description cleanup.
- Added wget requires.

* Wed Jun 06 2007 Jon Ciesla <limb@jcomserv.net> - 5.1-3
- Fixed initial setting.php perms.
- Added files dir.

* Wed May 30 2007 Jon Ciesla <limb@jcomserv.net> - 5.1-2
- Fixed category, duped docs, apache restart, cron job.

* Wed May 30 2007 Jon Ciesla <limb@jcomserv.net> - 5.1-1
- Initial packaging.
