%global momorel 1
%global mplus_medium_src mplus-TESTFLIGHT-057
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: M+ outline fonts
Name: mplus_medium
Version: 0.57
Release: %{momorel}m%{?dist}
License: Public Domain
Group: User Interface/X
BuildArch: noarch
Source0: http://dl.sourceforge.jp/mplus-fonts/6650/%{mplus_medium_src}.tar.xz
NoSource: 0
URL: http://mplus-fonts.sourceforge.jp/mplus-outline-fonts/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fontforge
BuildRequires: fontpackages-devel
BuildRequires: perl
BuildRequires: ttmkfdir
BuildRequires: xorg-x11-font-utils
Requires: fontpackages-filesystem

%description
M+ outline fonts are free softwares.
Unlimited permission is granted to use, copy, and distribute it, with
or without modification, either commercially and noncommercially.
THESE FONTS ARE PROVIDED "AS IS" WITHOUT WARRANTY.

%prep
%setup -q -n %{mplus_medium_src}

%build
ttmkfdir
mkfontdir

%install
%{__rm} -rf --preserve-root %{buildroot}
%{__mkdir_p} -m 0755 %{buildroot}%{_fontdir}
%{__install} -m 0644 *.ttf %{buildroot}%{_fontdir}
%{__install} -m 0644 fonts.* %{buildroot}%{_fontdir}

%{__install} -d -m 0755 %{buildroot}%{catalogue}
ln -sf %{_fontdir} %{buildroot}%{catalogue}/%{name}

%clean
%{__rm} -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc README_* LICENSE_*
%{catalogue}/%{name}
%dir %{_fontdir}
%{_fontdir}/*.ttf
%verify(not md5 size mtime) %{_fontdir}/fonts.dir
%verify(not md5 size mtime) %{_fontdir}/fonts.scale

%changelog
* Wed Jan 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.57-1m)
- update to TESTFLIGHT-057

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.53-1m)
- update to TESTFLIGHT-053

* Thu Sep 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.52-1m)
- update to TESTFLIGHT-052

* Sat Aug  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.51-1m)
- update to TESTFLIGHT-051

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.49-1m)
- update to TESTFLIGHT-049

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.47-1m)
- update to TESTFLIGHT-047

* Thu Mar  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.46-1m)
- update to TESTFLIGHT-046

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.42-1m)
- update to TESTFLIGHT-042

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.41-1m)
- update to TESTFLIGHT-041

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.39-1m)
- update to TESTFLIGHT-039

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.37-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.37-1m)
- update to TESTFLIGHT-037

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.34-3m)
- rebuild for new GCC 4.5

* Fri Nov 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.34-2m)
- fix source name...

* Fri Nov 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.34-1m)
- update to TESTFLIGHT-034

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.31-1m)
- update to TESTFLIGHT-031

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-1m)
- update to TESTFLIGHT-030

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.27-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.27-1m)
- update to TESTFLIGHT-027
- correct font catalogue

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.24-1m)
- update TESTFLIGHT 024

* Fri Apr 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.22-1m)
- update TESTFLIGHT 022

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-6m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-5m)
- rebuild against rpm-4.6

* Sun Jul 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-4m)
- modify fontdir

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14-2m)
- %%NoSource -> NoSource

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14-1m)
- update TESTFLUGHT 014

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.20040108-0.20040108.04m)
- revised fonts installdir

* Wed Jan 11 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.20040108-0.20040108.03m)
  fontforge instead of pfaedit.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.20040108-0.20040108.02m)
  use relative path in symlink.
  unset LANG.

* Thu Jan 08 2004  Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20040108-0.20040108.01m)
- cvs update at 2004-01-08.

* Fri Dec 26 2003  Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031226-0.20031226.01m)
- cvs update at 2003-12-26.

* Thu Dec 25 2003  Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031225-0.20031225.01m)
- cvs update at 2003-12-25.

* Tue Dec 23 2003  Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031223-0.20031223.01m)
- cvs update at 2003-12-23.

* Mon Dec 22 2003  Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031222-0.20031222.01m)
- cvs update at 2003-12-22.

* Sat Dec 20 2003  Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031220-0.20031220.01m)
- cvs update at 2003-12-20.

* Fri Dec 19 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031219-0.20031219.01m)
- update to cvs date 2003-12-19 version.

* Thu Dec 18 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.20031218-0.20031218.01m)
- Initial specfile
- source is cvs version.




