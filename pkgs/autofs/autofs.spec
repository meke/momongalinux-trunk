%global momorel 1

Summary: A tool for automatically mounting and unmounting filesystems
Name: autofs
Version: 5.1.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://wiki.autofs.net/
Source0: ftp://ftp.kernel.org/pub/linux/daemons/autofs/v5/autofs-%{version}.tar.gz
NoSource: 0

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: systemd-units
BuildRequires: autoconf, hesiod-devel, openldap-devel, bison, flex, libxml2-devel, cyrus-sasl-devel, openssl-devel module-init-tools util-linux nfs-utils e2fsprogs 
BuildRequires: libtirpc-devel >= 0.2.4
Conflicts: cyrus-sasl-lib < 2.1.23-9
Requires: kernel >= 2.6.17
Requires: bash coreutils sed gawk textutils sh-utils grep module-init-tools /bin/ps
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Summary(de): autofs daemon 
Summary(fr): démon autofs
Summary(tr): autofs sunucu süreci
Summary(sv): autofs-daemon

%description
autofs is a daemon which automatically mounts filesystems when you use
them, and unmounts them later when you are not using them.  This can
include network filesystems, CD-ROMs, floppies, and so forth.

%description -l de
autofs ist ein Dämon, der Dateisysteme automatisch montiert, wenn sie 
benutzt werden, und sie später bei Nichtbenutzung wieder demontiert. 
Dies kann Netz-Dateisysteme, CD-ROMs, Disketten und ähnliches einschließen. 

%description -l fr
autofs est un démon qui monte automatiquement les systèmes de fichiers
lorsqu'on les utilise et les démonte lorsqu'on ne les utilise plus. Cela
inclus les systèmes de fichiers réseau, les CD-ROMs, les disquettes, etc.

%description -l tr
autofs, kullanýlan dosya sistemlerini gerek olunca kendiliðinden baðlar
ve kullanýmlarý sona erince yine kendiliðinden çözer. Bu iþlem, að dosya
sistemleri, CD-ROM'lar ve disketler üzerinde yapýlabilir.

%description -l sv
autofs är en daemon som mountar filsystem när de använda, och senare
unmountar dem när de har varit oanvända en bestämd tid.  Detta kan
inkludera nätfilsystem, CD-ROM, floppydiskar, och så vidare.

%prep
%setup -q
echo %{version}-%{release} > .version

%build
%configure --disable-mount-locking --enable-ignore-busy --with-libtirpc --with-systemd
make initdir=%{_initscriptdir} DONTSTRIP=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
install -d -m 755 %{buildroot}%{_unitdir}
mkdir -p -m755 %{buildroot}%{_sbindir}
mkdir -p -m755 %{buildroot}%{_libdir}/autofs
mkdir -p -m755 %{buildroot}%{_mandir}/{man5,man8}
mkdir -p -m755 %{buildroot}/%{_sysconfdir}/sysconfig
mkdir -p -m755 %{buildroot}/%{_sysconfdir}/auto.master.d

make install mandir=%{_mandir} initdir=%{_initscriptdir} INSTALLROOT=%{buildroot}
make -C redhat
install -m 755 -d %{buildroot}/misc
# Configure can get this wrong when the unit files appear under /lib and /usr/lib
find %{buildroot} -type f -name autofs.service -exec rm -f {} \;
install -m 644 redhat/autofs.service %{buildroot}%{_unitdir}/autofs.service
install -m 644 redhat/autofs.sysconfig %{buildroot}/etc/sysconfig/autofs

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service 
    
%postun
%systemd_postun_with_restart %{name}.service

%files
%defattr(-,root,root,-)
%doc CREDITS INSTALL COPY* README* patches/* samples/ldap* samples/autofs.schema
%config %{_unitdir}/autofs.service
%config(noreplace,missingok) /etc/auto.master
%config(noreplace) /etc/autofs.conf
%config(noreplace,missingok) /etc/auto.misc
%config(noreplace,missingok) /etc/auto.net
%config(noreplace,missingok) /etc/auto.smb
%config(noreplace) /etc/sysconfig/autofs
%config(noreplace) /etc/autofs_ldap_auth.conf
%{_sbindir}/automount
%{_mandir}/*/*
%{_libdir}/autofs/
%dir /etc/auto.master.d

%changelog
* Thu Jun  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.1.0-1m)
- update to 5.1.0
- sync with fc21

* Mon Apr  7 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.8-3m)
- fix build failure

* Thu Dec 26 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.8-2m)
- fix "undefined symbol: auth_put" bug in mount_nfs.so
- import bug fix patches from fedora

* Sun Nov 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.8-1m)
- update 5.0.8

* Tue Mar 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.6-7m)
- import fedora patch

* Mon Feb 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.6-6m)
- improve systemd support

* Sun Feb 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.6-5m)
- add systemd support
- import a lot of patches from fedora's autofs-5.0.6-11

* Wed Jan 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.6-4m)
- add source tarball
- can not get source tarball...

* Thu Nov 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.6-3m)
- import bug fix patches from fedora

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.6-2m)
- modify Requires

* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0.6-1m)
- version up 5.0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.4-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.4-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.4-2m)
- rebuild against rpm-4.6

* Wed Dec 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.4-1m)
- update to 5.0.4
-- import 2 patches from fc-devel (autofs-5_0_4-4)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.3-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.3-1m)
- sync with fc-devel (autofs-5_0_3-6)
-* Mon Feb 25 2008 Ian Kent <ikent@redhat.com> - 5.0.3-6
-- fix expire calling kernel more often than needed.
-- fix unlink of mount tree incorrectly causing autofs mount fail.
-- add miscellaneous device node interface library.
-- use miscellaneous device node, if available, for active restart.
-- device node and active restart fixes.
-- update is_mounted to use device node ioctl, if available.
-
-* Fri Feb 1 2008 Ian Kent <ikent@redhat.com> - 5.0.3-5
-- another fix for don't fail on empty master map.
-
-* Fri Jan 25 2008 Ian Kent <ikent@redhat.com> - 5.0.3-4
-- correction to the correction for handling of LDAP base dns with spaces.
-- avoid using UDP for probing NFSv4 mount requests.
-- use libldap instead of libldap_r.
-
-* Mon Jan 21 2008 Ian Kent <ikent@redhat.com> - 5.0.3-3
-- catch "-xfn" map type and issue "no supported" message.
-- another correction for handling of LDAP base dns with spaces.
-
--* Mon Jan 14 2008 Ian Kent <ikent@redhat.com> - 5.0.3-2
-- correct configure test for ldap page control functions.
-
-* Mon Jan 14 2008 Ian Kent <ikent@redhat.com> - 5.0.3-1
-- update source to version 5.0.3.
-
--* Fri Dec 21 2007 Ian Kent <ikent@redhat.com> - 5.0.2-25
-- Bug 426401: CVE-2007-6285 autofs default doesn't set nodev in /net [rawhide]
-  - use mount option "nodev" for "-hosts" map unless "dev" is explicily specified.

* Tue Dec 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.2-4m)
- sync with fc-devel (autofs-5_0_2-14)
--* Tue Dec 18 2007 Ian Kent <ikent@redhat.com> - 5.0.2-23
--- Bug 397591 SELinux is preventing /sbin/rpc.statd (rpcd_t) "search" to <Unknown> (sysctl_fs_t).
--- prevent fork between fd open and setting of FD_CLOEXEC.
--* Thu Dec 13 2007 Ian Kent <ikent@redhat.com> - 5.0.2-21
--- Bug 421371: CVE-2007-5964 autofs defaults don't restrict suid in /net [rawhide]
--- use mount option "nosuid" for "-hosts" map unless "suid" is explicily specified.
--* Thu Dec  6 2007 Jeremy Katz <katzj@redhat.com> - 1:5.0.2-19
--- rebuild for new ldap
--* Tue Nov 20 2007 Ian Kent <ikent@redhat.com> - 5.0.2-18
--- fix schema selection in LDAP schema discovery.
--- check for "*" when looking up wildcard in LDAP.
--- fix couple of edge case parse fails of timeout option.
--- add SEARCH_BASE configuration option.
--- add random selection as a master map entry option.
--- re-read config on HUP signal.
--- add LDAP_URI, LDAP_TIMEOUT and LDAP_NETWORK_TIMEOUT configuration options.
--- fix deadlock in submount mount module.
--- fix lack of ferror() checking when reading files.
--- fix typo in autofs(5) man page.
--- fix map entry expansion when undefined macro is present.
--- remove unused export validation code.
--- add dynamic logging (adapted from v4 patch from Jeff Moyer).
--- fix recursive loopback mounts (Matthias Koenig).
--- add map re-load to verbose logging.
--- fix handling of LDAP base dns with spaces.
--- handle MTAB_NOTUPDATED status return from mount.
--- when default master map, auto.master, is used also check for auto_master.
--- update negative mount timeout handling.
--- fix large group handling (Ryan Thomas).
--- fix for dynamic logging breaking non-sasl build (Guillaume Rousse).
--- eliminate NULL proc ping for singleton host or local mounts.
--* Mon Sep 24 2007 Ian Kent <ikent@redhat.com> - 5.0.2-16
--- add descriptive comments to config about LDAP schema discovery.
--- work around segfault at exit caused by libxml2.
--- fix foreground logging (also fixes shutdown needing extra signal bug).
--* Wed Sep 5 2007 Ian Kent <ikent@redhat.com> - 5.0.2-15
--- fix LDAP schema discovery.

* Mon Sep  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.2-3m)
- sync with fc-devel (autofs-5_0_2-14)
--* Tue Aug 28 2007 Ian Kent <ikent@redhat.com> - 5.0.2-14
--- update patch to prevent failure on empty master map.
--- if there's no "automount" entry in nsswitch.conf use "files" source.
--- add LDAP schema discovery if no schema is configured.
--* Wed Aug 22 2007 Ian Kent <ikent@redhat.com> - 5.0.2-13
--- fix "nosymlink" option handling and add desription to man page.
--* Tue Aug 21 2007 Ian Kent <ikent@redhat.com> - 5.0.2-12
--- change random multiple server selection option name to be consistent
---  with upstream naming.
--* Tue Aug 21 2007 Ian Kent <ikent@redhat.com> - 5.0.2-11
--- don't fail on empty master map.
--- add support for the "%%" hack for case insensitive attribute schemas.

* Tue Jul 31 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.2-2m)
- merged some bug fixes from fc-devel (autofs-5_0_2-10)
--* Mon Jul 30 2007 Ian Kent <ikent@redhat.com> - 5.0.2-10
--- mark map instances stale so they aren't "cleaned" during updates.
--- fix large file compile time option.
--* Fri Jul 27 2007 Ian Kent <ikent@redhat.com> - 5.0.2-9
--- fix version passed to get_supported_ver_and_cost (bz 249574).
--* Tue Jul 24 2007 Ian Kent <ikent@redhat.com> - 5.0.2-8
--- fix parse confusion between attribute and attribute value.
--* Fri Jul 20 2007 Ian Kent <ikent@redhat.com> - 5.0.2-7
--- fix handling of quoted slash alone (bz 248943).
--* Wed Jul 18 2007 Ian Kent <ikent@redhat.com> - 5.0.2-6
--- fix wait time resolution in alarm and state queue handlers (bz 247711).
--* Mon Jul 16 2007 Ian Kent <ikent@redhat.com> - 5.0.2-5
--- fix mount point directory creation for bind mounts.
--- add quoting for exports gathered by hosts map.
--* Mon Jun 25 2007 Ian Kent <ikent@redhat.com> - 5.0.2-4
--- update multi map nsswitch patch.
--* Mon Jun 25 2007 Ian Kent <ikent@redhat.com> - 5.0.2-3
--- add missing "multi" map support.
--- add multi map nsswitch lookup.
--* Wed Jun 20 2007 Ian Kent <ikent@redhat.com> - 5.0.2-2
--- include krb5.h in lookup_ldap.h (some openssl doesn't implicitly include it).
--- correct initialization of local var in parse_server_string.

* Wed Jun 20 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.2-1m)
- update to 5.0.2
- modify init script

* Mon May 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-2m)
- sync with fc-devel (autofs-5_0_1-10)

* Mon May 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-1m)
- update to 5.0.1
- sync with fc-devel 5.0.1-9

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.4-2m)
- rebuild against openssl-0.9.8a

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.4-1m)
- sync with fc-devel  

* Tue Nov 30 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.1.3-1m)
  update to 4.1.3(based fc development 4.1.3-41)

* Wed Nov  3 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (3.1.7-21m)
- add patch for gcc 3.4

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.1.7-20m)
- revive lookup_hesiod.so

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.1.7-19m)
- remove Epoch

* Sat Jul  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.7-18m)
- stop daemon

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.7-17m)
- rebuild against openldap

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.7-16m)
  rebuild against cyrus-sasl2
  
* Thu May 15 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.7-15m)
  to noreplace auto.misc

* Thu May  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.7-14m)
- sync with rawhide (autofs-3.1.7-37)
- remove hesiod upport

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.7-13m)
  rebuild against openssl 0.9.7a

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.7-12m)
- add URL

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.7-11m)
- rebuild against for gdbm

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.1.7-10k)
- /sbin/chkconfig -> chkconfig in PreReq.
- /bin/bash -> bash and /bin/ps -> procps in Requires.

* Tue Jan 15 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.1.7-8k)
- s/chkconfig: 345 18 82/chkconfig: 345 28 82/ (jittebug #1013)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (3.1.7-6k)
- rebuild against openssl 0.9.6.

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 3.1.7, based redhat 3.1.5 package

* Fri Sep  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- add -lresolv to build properly with OpenLDAP 2

* Wed Aug 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- convert YP map names that start with "auto_" to "auto." (#16753)

* Wed Aug 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- add an extra pause after the initial shutdown attempt

* Fri Aug 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- patch init script to not shut down --submount mounts on reload
- remove temporary files again during reload

* Thu Aug 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- revert to 3.1.5
- add 3.1.6pre1 patch to handle LDAP maps
- add Epoch: to upgrade from Raw Hide or Pinstripe
- update to 3.1.6pre2

* Fri Aug  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- work around kernel insmod concurrency problem (#14972) by loading the module
  at start-time

* Wed Aug  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix duplicate detection for false matches (e.g. "/user" == "/users") (#15183)

* Mon Aug  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- merge ldap subpackage back into the main one and Obsolete: it
- clean up build warnings
- fix errors automounting ext2 filesystems due to uninitialized option string

* Sat Jul 15 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- move the init script to the right place

* Fri Jul  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- add /net directory to the package

* Thu Jul  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- do manual unmounting in shutdown script
- change initscripts prereq to /etc/init.d
- move autoconf invocation to setup section

* Wed Jul  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- move condrestart to postun
- make condrestart check the right file to determine if already running

* Wed Jul  5 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- ignore errors in post script

* Tue Jul  4 2000 Matt Wilson <msw@redhat.com>
- use full path to chkconfig
- full *correct* path to chkconfig

* Tue Jun 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- smoother starting/stopping on upgrades/removal

* Thu Jun 15 2000 Nalin Dahyabhai <nalin@redhat.com>
- be more aggressive when shutting down autofs
- add condrestart support

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- move man pages to %%{_mandir}

* Wed May 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweak argument parsing (bug #11801)

* Mon May 22 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new build environment

* Mon May  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix duplicate detection logic (duh!)

* Tue May  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- add config: and processname: tags to init script

* Sun Apr 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix shutdown logic (test -z $pid fails with multiple autofs daemons)

* Wed Apr 26 2000 Nalin Dahyabhai <nalin@redhat.com>
- split off autofs4 for testing & development
- update patches for autofs4 for feeding back to autofs list
- make auto.master a noreplace file

* Mon Apr 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- split lookup_ldap into a subpackage (bug #10874)

* Tue Apr 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- add LDAP map support, requires openldap
- tweak init script patch to use fields 2+ for the map name instead of just 2

* Wed Apr 05 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix bug #10402 by fixing the init script, hopefully for everybody
- merge patches for the init script into a single patch to send to hpa
- patch to build on 2.2 and late-series 2.3 kernels

* Tue Feb 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- enable hesiod support over libbind

* Mon Feb 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix init script bug when startup failed
- fix option passing in init script

* Sun Feb 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- make autofs start after ypbind by moving start priority 18, stop to 82
- make sure that calls to "ps aux" include "www" to avoid snippage

* Wed Feb  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- make sure all the docs get packaged
- make init script do status messages like others
- add "nosuid,nodev" to options for /misc/cd
- switch to using INSTALLROOT during make install

* Fri Jan 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.1.4

* Mon Sep 20 1999 Cristian Gafton <gafton@redhat.com>
- use ps axw instead of ps ax in the init script

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %%preun, not %%postun

* Tue Sep 07 1999 Cristian Gafton <gafton@redhat.com>
- add patch from HJLu to handle NIS auto.master better

* Wed Aug 25 1999 Cristian Gafton <gafton@redhat.com>
- fix bug #4708

* Sat Aug 21 1999 Bill Nottingham <notting@redhat.com>
- fix perms on /usr/lib/autofs/* 
- add support for specifying maptype in auto.master

* Fri Aug 13 1999 Cristian Gafton <gafton@redhat.com>
- add patch from rth to avoid an infinite loop

* Wed Apr 28 1999 Cristian Gafton <gafton@redhat.com>
- use "autofs" instead of "automount" for /var/lock/subsys lock file

* Fri Apr 09 1999 Cristian Gafton <gafton@redhat.com>
- enahanced initscript to try to load maps over NIS
- changed the mount point back to misc (there is a reason we leave /mnt
  alone)
- patched back autofs.misc to the version shipped on 5.2 to avoid replacing
  yet one more config file for those who upgrade

* Wed Mar 24 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 3.1.3, fixing smbfs stuff and other things
- changed mountpoint from /misc to /mnt

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)

* Mon Feb  8 1999 Bill Nottingham <notting@redhat.com>
- build for kernel-2.2/glibc2.1

* Tue Oct  6 1998 Bill Nottingham <notting@redhat.com>
- fix bash2 breakage in init script

* Sun Aug 23 1998 Jeff Johnson <jbj@redhat.com>
- typo in man page.

* Mon Jul 20 1998 Jeff Johnson <jbj@redhat.com>
- added sparc to ExclusiveArch.

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- updated to 3.1.1

* Wed Apr 22 1998 Michael K. Johnson <johnsonm@redhat.com>
- enhanced initscripts

* Fri Dec 05 1997 Michael K. Johnson <johnsonm@redhat.com>
- Link with -lnsl for glibc compliance.

* Thu Oct 23 1997 Michael K. Johnson <johnsonm@redhat.com>
- exclusivearch for i386 for now, since our kernel packages on
  other platforms don't include autofs yet.
- improvements to initscripts.

* Thu Oct 16 1997 Michael K. Johnson <johnsonm@redhat.com>
- Built package from 0.3.14 for 5.0
