%global momorel 12

%global rbname nora
Summary: Nora - Web Application Library
Name: ruby-%{rbname}

%global reldate 20041008

Version: 0.0
Release: 0.%{reldate}.%{momorel}m%{?dist}
Group: Applications/Text
License: Ruby
URL: http://rwiki.moonwolf.com/rw-cgi.cgi?cmd=view;name=Nora 

Source0: http://www.moonwolf.com/ruby/archive/%{rbname}-%{version}.%{reldate}.tar.gz 
NoSource: 0
Patch0: ruby-nora-ruby19.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2

%description
Web Application Library
* CGI/mod_ruby interface
* Persistent Framework
* multipart/form support
* Cookie support

%prep
%setup -q -n %{rbname}-%{version}.%{reldate}

%patch0 -p1 -b .ruby19

%build
ruby install.rb --quiet config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_sitelibdir} \
    --so-dir=%{buildroot}%{ruby_sitearchdir}
ruby install.rb setup

#find sample/ -type f \
#    | xargs ruby -Ke -p -i -e '$_.sub!(%r|/usr/local/bin|, "%{_bindir}")'
find sample/ -type f \
 | xargs sed -i 's/usr\/local\/bin/usr\/bin/g'

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby install.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.ja sample
%{ruby_sitelibdir}/web.rb
%{ruby_sitelibdir}/web
%{ruby_sitearchdir}/web

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-0.20041008.12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-0.20041008.11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0-0.20041008.10m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0-0.20041008.8m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0-0.20041008.7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-0.20041008.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-0.20041008.5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0-0.20041008.4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0-0.20041008.3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0-0.20041008.2m)
- rebuild against ruby-1.8.6-4m

* Fri Oct  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20041008.1m)
- version up

* Thu Sep 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20040916.1m)
- version up

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20040310.3m)
- rebuild against ruby-1.8.2

* Sun Apr 25 2004 Toru Hoshina <t@momonga-linux.org>
- (0.0-0.20040310.2m)
- hello.cgi contains dependency to ruby1.7, sigh.

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20040310.1m)

* Sun Feb 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20040222.1m)

* Wed Feb 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20040216.1m)

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.0-0.20021118.4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.0-0.20021118.3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.0-0.20021118.2m)
- rebuild against ruby-1.8.0.

* Sat Nov 30 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.0-0.20021118.1m)
- update
- ver/rel follow Specfile-Guidance

* Wed Oct  2 2002 Junichiro Kita <kita@momonga-linux.org>
- (20020929-1m)
- update to 20020929

* Sun Sep  8 2002 Junichiro Kita <kita@momonga-linux.org>
- (20020825-1m)
- initial build
