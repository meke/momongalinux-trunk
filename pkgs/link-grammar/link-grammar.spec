%global momorel 6


Summary: A Grammar Checking library
Name: link-grammar
Version: 4.5.7
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: Modified BSD 
Source: http://www.abisource.com/downloads/link-grammar/%{version}/link-grammar-%{version}.tar.gz
NoSource: 0
URL: http://abisource.com/projects/link-grammar/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A library that can perform grammar checking.

%package devel
Summary: Support files necessary to compile applications with liblink-grammar
Group: Development/Libraries
Requires: link-grammar = %{version}-%{release}

%description devel
Libraries, headers, and support files needed for using liblink-grammar.

%prep
%setup -q

%build
%configure --disable-java-bindings
make
# currently the build system can not handle smp_flags properly
# make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la

%files
%defattr(-,root,root)
%doc LICENSE README
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_datadir}/link-grammar
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/link-grammar.pc
%{_includedir}/link-grammar

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.7-4m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.7-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.7-1m)
- update to 4.5.7
- build with --disable-java-bindings

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.3.4-3m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.4-2m)
- rebuild against rpm-4.6

* Sat Apr 05 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.3.4-1m)
- import to Momonga (from Fedora, based on 4.2.5-2)

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 4.2.5-2
- Autorebuild for GCC 4.3

* Mon Nov 12 2007 Marc Maurer <uwog@uwog.net> 4.2.5-1
- New upstream version, fixes bug 371221.

* Mon Sep 11 2006 Marc Maurer <uwog@abisource.com> 4.2.2-2.fc6
- Rebuild for FC6

* Wed Apr 12 2006 Marc Maurer <uwog@abisource.com> 4.2.2-1
- New upstream version

* Mon Apr 10 2006 Marc Maurer <uwog@abisource.com> 4.2.1-2
- Rebuild

* Mon Apr 10 2006 Marc Maurer <uwog@abisource.com> 4.2.1-1
- New upstream version

* Wed Feb 15 2006 Marc Maurer <uwog@abisource.com> 4.1.3-4
- Rebuild for Fedora Extras 5
- Use %%{?dist} in the release name

* Wed Aug 10 2005 Marc Maurer <uwog@abisource.com> - 4.1.3-3
- Set the buildroot to the standard Fedora buildroot
- Make the package own the %{_datadir}/link-grammar
  directory (thanks go to Aurelien Bompard for both issues)

* Wed Aug 10 2005 Marc Maurer <uwog@abisource.com> - 4.1.3-2
- Remove epoch
- Make rpmlint happy

* Sun Aug 7 2005 Marc Maurer <uwog@abisource.com> - 1:4.1.3-1
- Initial version
