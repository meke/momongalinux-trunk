%global momorel 1

%global module turbojson

Name:           python-turbojson
Version:        1.3.2
Release:        %{momorel}m%{?dist}
Summary:        Python template plugin that supports json

Group:          Development/Languages
License:        MIT
URL:            http://cheeseshop.python.org/pypi/TurboJson
Source0:        http://pypi.python.org/packages/source/T/TurboJson/TurboJson-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: python-peak-rules >= 0.5a1.dev-0.2600
BuildRequires: python-nose
BuildRequires: python-sqlalchemy
BuildRequires: python-sqlobject

BuildRequires: python-simplejson >= 1.9.1
Requires:      python-simplejson >= 1.9.1

Requires:      python-peak-rules >= 0.5a1.dev-0.2600

%description
This package provides a template engine plugin, allowing you
to easily use Json with TurboGears, Buffet or other systems
that support python.templating.engines.


%prep
%setup -q -n TurboJson-%{version}

%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root=%{buildroot}

%check
python setup.py test

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{python_sitelib}/%{module}/
%{python_sitelib}/*.egg-info/


%changelog
* Fri Dec 23 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.2-1m)
- verion up 1.3.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.2-1m)
- version up 1.1.2
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.9-1m)
- import from Fedora

* Fri Dec  8 2006 Luke Macken <lmacken@redhat.com> 0.9.9-3
- Rebuild for new python

* Fri Oct  6 2006 Luke Macken <lmacken@redhat.com> 0.9.9-2
- Update source url

* Mon Sep 25 2006 Luke Macken <lmacken@redhat.com> 0.9.9-1
- 0.9.9
- Rename to python-turbojson
- Own the %%{python_sitelib/turbojson directory
- Install the EGG-INFO

* Sat Sep 16 2006 Luke Macken <lmacken@redhat.com> 0.9.8-1
- Initial creation
