%global momorel 4

Summary: automatically restart SSH sessions and tunnels
Name: autossh
Version: 1.4b
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.harding.motd.ca/autossh/
Source0: http://www.harding.motd.ca/autossh/autossh-%{version}.tgz
NoSource: 0
BuildRequires: openssh-clients
Requires: openssh-clients
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
autossh is a program to start a copy of ssh and monitor it, restarting
it as necessary should it die or stop passing traffic. The idea is
from rstunnel (Reliable SSH Tunnel), but implemented in C.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf "%{buildroot}"

%makeinstall

rm -rf %{buildroot}/%{_datadir}/examples/autossh
rm -rf %{buildroot}/%{_datadir}/doc/autossh
mkdir -p examples
cp -p autossh.host rscreen examples
chmod 0644 examples/*

%clean
[ "%{buildroot}" != "/" ] && rm -rf "%{buildroot}"

%files
%defattr(-, root, root)
%doc CHANGES README
%doc examples
%{_bindir}/autossh
%{_mandir}/man1/autossh.1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4b-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4b-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4b-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4b-1m)
- update to 1.4b (bug fix release)

* Fri Apr  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4a-1m)
- initial import
