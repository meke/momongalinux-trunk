%global momorel 2

Summary: GnomeICU is a clone of Mirabilis' popular ICQ written with GTK.
Name: gnomeicu
Version: 0.99.16
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Communications
URL: http://gnomeicu.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Requires:       libxml2 >= 2.4.7
BuildRequires:  libgnomeui >= 2.0.0
BuildRequires:  libxml2 >= 2.4.7
BuildRequires:  scrollkeeper >= 0.3.5
BuildRequires:  openssl-devel >= 0.9.8a
BuildRequires:  gnome-panel >= 3.0
BuildRequires: gdbm-devel >= 1.8.0
BuildRequires: gcr-devel
BuildRequires: intltool
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): GConf2 scrollkeeper
Requires(postun): scrollkeeper
Obsoletes: %{name}-applet

%description
GnomeICU is a clone of Mirabilis' popular ICQ written with GTK.
The original source was taken from Matt Smith's mICQ.  This is ment as
a replacement for the JavaICQ, which is slow and buggy.  If you would
like to contribute, please contact Jeremy Wise <jwise@pathwaynet.com>.

%prep
%setup -q

%build
%configure "LIBS=-lX11"
%make

%install
%makeinstall
%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/gnomeicu.schemas > /dev/null
scrollkeeper-update
                                                                                                               
%postun
scrollkeeper-update

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_sysconfdir}/gconf/schemas/gnomeicu.schemas
%config %{_sysconfdir}/sound/events/GnomeICU.soundlist
%{_bindir}/gnomeicu
%{_bindir}/gnomeicu-client
%{_datadir}/omf/*
%{_datadir}/gnome/help/*
%{_datadir}/sounds/gnomeicu/
%{_datadir}/gnomeicu/
%{_datadir}/applications/GnomeICU.desktop
%{_datadir}/pixmaps/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.16-2m)
- rebuild for gcr-devel

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.16-1m)
- update to 0.99.16
- delete applet

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.12-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.12-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.12-10m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.12-9m)
- build fix

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.12-8m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.12-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.12-6m)
- fix build

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.12-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.12-4m)
- rebuild against gcc43

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.12-3m)
- add intltoolize (intltool-0.36.0)

* Mon Jul 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.12-2m)
- add autoreconf (libtool-1.5.24)

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.99.12-1m)
- up to 0.99.12
- add requires

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.99.5-3m)
- rebuild against expat-2.0.0-1m

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.5-2m)
- rebuild against openssl-0.9.8a

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.5-1m)
- update to 0.99.5

* Sat Apr 24 2004 Toru Hoshina <t@momonga-linux.org>
- (0.99-6m)
- *_DISABLE_DEPRECATED flags are disabled.

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.99-5m)
- accept gdbm-1.8.0 or newer

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99-4m)
- rebuild against gdbm-1.8.0

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99-3m)
- rebuild against gnet1

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.99-1m)
- version 0.99

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.98.126-3m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.98.126-2m)
- rebuild against for gdbm

* Thu Nov 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.98.126-1m)
- version 0.98.126

* Wed Aug 28 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.98.111-3m)
- added gnomeicu-ja.po and utf8time patch

* Mon Aug 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.98.111-2m)
- rebuild against for gnet-1.1.4-2m

* Sun Aug 11 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.98.111-1m)
- version 0.98.111 for gtk+2

* Thu Mar 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.98.2-2k)
- version 0.98.2
- disable applet

* Thu Dec  6 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.96.1-14k)
- modify for "ja_JP.EUC-JP" locale

* Wed Nov  7 2001 Shingo Akagaki <dora@kondara.org>
- (0.96.1-12k)
- clean up

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.96.1-10k)
- rebuild against gettext 0.10.40.

* Sat Oct 06 2001 Motonobu Ichimura <famao@kondara.org>
- (0.96.1-8k)
- change URL

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (0.96.1-6k)
- add alphaev5 support.

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.96.1-5k)
- rebuild against audiofile-0.2.1.

* Thu Mar 29 2001 Shingo Akagaki <dora@kondara.org>
- version 0.96.1
- K2K

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (0.93-7k)
- rebuild against audiofile-0.2.0.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat May 13 2000 Shingo Akagaki <dora@kondara.org>
- version 0.93

* Sat Apr 15 2000 Hidetomo Machi <mcHT@kondara.org>
- fix sendmsg.c

* Wed Apr  5 2000 Shingo Akagaki <dora@kondara.org>
- version 0.92

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Mon Feb 21 2000 Shingo Akagaki <dora@kondara.org>
- auto away...
- Away_Message Kanji code

* Wed Jan 26 2000 Shingo Akagaki <dora@kondara.org>
- typo fix

* Mon Jan 24 2000 Shingo Akagaki <dora@kondara.org>
- add gnomeicu-0.90b-file.patch from
      Makoto Yamazaki <zaki@mt.is.noda.sut.ac.jp>
- change ja.po from Makoto Yamazaki <zaki@mt.is.noda.sut.ac.jp>
  thanks 

* Fri Jan 21 2000 Shingo Akagaki <dora@kondara.org>
- change cr_conv.c

* Fri Jan 14 2000 Shingo Akagaki <dora@kondara.org>
- fix send/receive URL in japanese mode (very dirty)

* Thu Jan 13 2000 Shingo Akagaki <dora@kondara.org>
- version 0.90b

* Wed Jan 12 2000 Shingo Akagaki <dora@kondara.org>
- version 0.90

* Mon Jan 10 2000 Shingo Akagaki <dora@kondara.org>
- many bug fix

* Fri Jan 07 2000 Shingo Akagaki <dora@kondara.org>
- rewrite japanese code conv code

* Tue Jan 04 2000 Shingo Akagaki <dora@kondara.org>
- call convert_kanji_auto from response.c (Tyuta #255)
- call convert_kanji_auto from histadd.c (Tyuta #255-R1)
- Happy new year and fix Y2K problem (Tyuta #255-R3)

* Sun Dec 19 1999 Shingo Akagaki <dora@kondara.org>
- version 0.68

* Fri Dec  3 1999 Shingo Akagaki <dora@kondara.org>
- added japanese code conv patch
- added japanese catalog file
  original patch was gnomeicu-0.65-ja.5.diff.bz2
  written by Takuo KITAME

* Fri Sep 10 1999 Herbert Valerio Riedel <hvr@gnu.org>
- added support for SMP builds

* Sun Jul 25 1999 Herbert Valerio Riedel <hvr@gnu.org>
- added online documentation
- added locale files

* Sat Jul 10 1999 Herbert Valerio Riedel <hvr@gnu.org>
- no need to define %{name} and %{version} explicitly (thanks to 
  Graham Cole <dybbuk@earthlink.net> for this hint)

* Tue Jun 29 1999 Herbert Valerio Riedel <hvr@gnu.org>
- first try at an official RPM

