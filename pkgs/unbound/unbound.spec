%global momorel 1

# not ready yet
%{?!with_python:      %global with_python      1}

%if %{with_python}
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%endif

Summary: Validating, recursive, and caching DNS(SEC) resolver
Name: unbound
Version: 1.4.16
Release: %{momorel}m%{?dist}
License: BSD
Url: http://www.nlnetlabs.nl/unbound/
# Url: http://unbound.net/
Source0: http://www.unbound.net/downloads/%{name}-%{version}.tar.gz
NoSource: 0
Source1: unbound.service
Source2: unbound.conf
Source3: unbound.munin
Source4: unbound_munin_
Source5: root.key
Source6: dlv.isc.org.key
Source7: unbound-keygen.service
Source8: tmpfiles-unbound.conf
Patch1: unbound-1.2-glob.patch

Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: flex, openssl-devel >= 1.0.0, ldns-devel >= 1.5.0, 
BuildRequires: libevent-devel >= 2.0.10
%if %{with_python}
BuildRequires:  python-devel swig
%endif
# Required for SVN versions
BuildRequires: bison

BuildRequires: systemd-units

Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: ldns >= 1.5.0
Requires(pre): shadow-utils

Obsoletes:      dnssec-conf < 1.27-2
Provides:       dnssec-conf = 1.27-1

%description
Unbound is a validating, recursive, and caching DNS(SEC) resolver.

The C implementation of Unbound is developed and maintained by NLnet
Labs. It is based on ideas and algorithms taken from a java prototype
developed by Verisign labs, Nominet, Kirei and ep.net.

Unbound is designed as a set of modular components, so that also
DNSSEC (secure DNS) validation and stub-resolvers (that do not run
as a server, but are linked into an application) are easily possible.

%package munin
Summary: Plugin for the munin / munin-node monitoring package
Group:     System Environment/Daemons
Requires: munin-node
Requires: %{name} = %{version}-%{release}, bc

%description munin
Plugin for the munin / munin-node monitoring package

%package devel
Summary: Development package that includes the unbound header files
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}, openssl-devel, ldns-devel

%description devel
The devel package contains the unbound library and the include files

%package libs
Summary: Libraries used by the unbound server and client applications
Group: Applications/System
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: openssl >= 0.9.8g-12

%description libs
Contains libraries used by the unbound server and client applications

%if %{with_python}
%package python
Summary: Python modules and extensions for unbound
Group: Applications/System
Requires: %{name}-libs = %{version}-%{release}

%description python
Python modules and extensions for unbound
%endif #%%{with_python}

%prep
%setup -q
%patch1 -p1

%build
%configure  --with-ldns= --with-libevent --with-pthreads --with-ssl \
            --disable-rpath --disable-static \
            --with-conf-file=%{_sysconfdir}/%{name}/unbound.conf \
            --with-pidfile=%{_localstatedir}/run/%{name}/%{name}.pid \
%if %{with_python}
            --with-pythonmodule --with-pyunbound \
%endif
            --enable-sha2 --disable-gost
%{__make} %{?_smp_mflags}

%install
rm -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install
install -d 0755 %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/unbound.service
install -m 0644 %{SOURCE7} %{buildroot}%{_unitdir}/unbound-keygen.service
install -m 0755 %{SOURCE2} %{buildroot}%{_sysconfdir}/unbound
# Install munin plugin and its softlinks
install -d 0755 %{buildroot}%{_sysconfdir}/munin/plugin-conf.d
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/munin/plugin-conf.d/unbound
install -d 0755 %{buildroot}%{_datadir}/munin/plugins/
install -m 0755 %{SOURCE4} %{buildroot}%{_datadir}/munin/plugins/unbound
for plugin in unbound_munin_hits unbound_munin_queue unbound_munin_memory unbound_munin_by_type unbound_munin_by_class unbound_munin_by_opcode unbound_munin_by_rcode unbound_munin_by_flags unbound_munin_histogram; do
    ln -s unbound %{buildroot}%{_datadir}/munin/plugins/$plugin
done 
            
# Install tmpfiles.d config
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d/
install -m 0644 %{SOURCE8} %{buildroot}%{_sysconfdir}/tmpfiles.d/unbound.conf

# install root and DLV key
install -m 0644 %{SOURCE5} %{SOURCE6} %{buildroot}%{_sysconfdir}/unbound/

# remove static library from install (fedora packaging guidelines)
rm -rf %{buildroot}%{_libdir}/*.la
%if %{with_python}
rm -rf %{buildroot}%{python_sitearch}/*.la
%endif

mkdir -p %{buildroot}%{_localstatedir}/run/unbound


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc/README doc/CREDITS doc/LICENSE doc/FEATURES
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}-keygen.service
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}
%ghost %attr(0755,unbound,unbound) %dir %{_localstatedir}/run/%{name}
%config(noreplace) %{_sysconfdir}/tmpfiles.d/unbound.conf
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/unbound.conf
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/dlv.isc.org.key
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/root.key
%{_sbindir}/*
%{_mandir}/*/*

%if %{with_python}
%files python
%{python_sitearch}/*
%doc libunbound/python/examples/*
%doc pythonmod/examples/*
%endif

%files munin
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/munin/plugin-conf.d/unbound
%{_datadir}/munin/plugins/unbound*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libunbound.so
%{_includedir}/unbound.h
%doc README

%files libs
%defattr(-,root,root,-)
%{_libdir}/libunbound.so.*
%doc doc/README doc/LICENSE

%pre
getent group unbound >/dev/null || groupadd -r unbound
getent passwd unbound >/dev/null || \
useradd -r -g unbound -d %{_sysconfdir}/unbound -s /sbin/nologin \
-c "Unbound DNS resolver" unbound
exit 0

%post 
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi
# dnssec-conf used to contain our DLV key, but now we include it via unbound
# If unbound had previously been configured with dnssec-configure, we need
# to migrate the location of the DLV key file (to keep DLV enabled, and because
# unbound won't start with a bad location for a DLV key file.
sed -i "s:/etc/pki/dnssec-keys[/]*dlv:/etc/unbound:" %{_sysconfdir}/unbound/unbound.conf

%post libs -p /sbin/ldconfig

%preun
if [ $1 -eq 0 ]; then
	# Package removal, not upgrade
	/bin/systemctl --no-reload disable unbound.service > /dev/null 2>&1 || :
	/bin/systemctl stop unbound.service > /dev/null 2>&1 || :
	/bin/systemctl --no-reload disable unbound-keygen.service > /dev/null 2>&1 || :
	/bin/systemctl stop unbound-keygen.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart unbound.service >/dev/null 2>&1 || :
    /bin/systemctl try-restart unbound-keygen.service >/dev/null 2>&1 || :
fi

%postun libs -p /sbin/ldconfig

%triggerun -- unbound < 1.4.13-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply unbound
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save unbound >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del unbound >/dev/null 2>&1 || :
/bin/systemctl try-restart unbound.service >/dev/null 2>&1 || :
/bin/systemctl try-restart unbound-keygen.service >/dev/null 2>&1 || :

%changelog
* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.16-1m)
- update to 1.4.16

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.14-1m)
- [SECURITY] CVE-2011-4528
- update to 1.4.14

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-2m)
- apply patch2

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.13-1m)
- update to 1.4.13
- support systemd

* Wed Jul  6 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.11-1m)
- update to 1.4.11

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.10-1m)
- [SECURITY] CVE-2011-1922
- update to 1.4.10

* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-1m)
- update to 1.4.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.8-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8
- rebuild against libevent-2.0.10

* Sun Nov 28 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.7-1m)
- update to 1.4.7
- add --disable-gost at configure

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Sun Jul 18 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-2m)
- explicitly link libpthread

* Wed Apr 28 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-2m)
- rebuild against openssl-1.0.0

* Thu Mar 18 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-1m)
- [SECURITY] CVE-2010-0969
- update to 1.4.3

* Thu Mar 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Sun Mar  7 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-2m)
- add Source4: dlv.isc.org.key merged from Fedora 1.4.1-5.fc14

* Fri Jan  8 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Tue Dec  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-1m)
- [SECURITY] CVE-2009-3602
- update to 1.3.4

* Thu Aug  6 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Wed Jul 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Tue Jun 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0
- add Patch1: unbound-r1657.patch, Patch2: unbound-r1670.patch from fedora
- add Patch3: unbound-r1677.patch, Patch4: unbound-1.2-glob.patch from fedora

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against openssl-0.9.8k

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Tue Feb  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-3m)
- rebuild against ldns-1.4.0
- merge fedora 1.2.0-2 spec
- add Source2: unbound.conf
- add Source3: unbound.munin
- separate sub packages: munin, devel and libs

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed Sep 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Jul 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2m)
- move configure from befor %%build section to after

* Sat May 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0
- this spec file came from unbound-1.0.0/contrib/unbound.spec
- use %%{_initscriptdir} instead of %%{_initrddir}
- add "--with-ldns=" in configure for use external ldns
- delete "--disable-static", this option cause build error

* Thu Apr 25 2008 Wouter Wijngaards <wouter@nlnetlabs.nl> - 0.12
- Using parts from ports collection entry by Jaap Akkerhuis.
- Using Fedoraproject wiki guidelines.

* Wed Apr 23 2008 Wouter Wijngaards <wouter@nlnetlabs.nl> - 0.11
- Initial version.
