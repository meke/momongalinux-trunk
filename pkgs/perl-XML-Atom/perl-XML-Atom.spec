%global         momorel 10

Name:           perl-XML-Atom
Version:        0.41
Release:        %{momorel}m%{?dist}
Summary:        Atom feed and API implementation
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-Atom/
Source0:        http://www.cpan.org/authors/id/M/MI/MIYAGAWA/XML-Atom-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Class-Data-Inheritable
BuildRequires:  perl-DateTime
BuildRequires:  perl-Digest-SHA1
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-URI
BuildRequires:  perl-XML-LibXML >= 1.69
BuildRequires:  perl-XML-XPath
Requires:       perl-Class-Data-Inheritable
Requires:       perl-DateTime
Requires:       perl-Digest-SHA1
Requires:       perl-libwww-perl
Requires:       perl-MIME-Base64
Requires:       perl-URI
Requires:       perl-XML-LibXML >= 1.69
Requires:       perl-XML-XPath
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Atom is a syndication, API, and archiving format for weblogs and other
data. XML::Atom implements the feed format as well as a client for the API.

%prep
%setup -q -n XML-Atom-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/XML/Atom.pm
%{perl_vendorlib}/XML/Atom
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.14.2

* Tue Sep 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-2m)
- rebuild against perl-5.14.1

* Tue Jun 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Mon May 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.37-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.37-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.37-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-2m)
- rebuild against perl-5.12.0

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.35-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-2m)
- rebuild against perl-5.10.1

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33-2m)
- rebuild against rpm-4.6

* Thu Jan  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Mon Nov 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Sat Nov 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30
- add BuildRequires: perl-URI
- add BuildRequires: perl-HTML-Parser

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.28-2m)
- rebuild against gcc43

* Wed Nov  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Tue Sep 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.27-2m)
- BuildRequires: perl-XML-LibXML >= 1.64

* Sun Sep 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27
- do not use %%NoSource macro

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.25-2m)
- use vendor

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-1m)
- spec file was autogenerated
