%global         momorel 1

Name:           vym
Version:        2.3.0
Release:        %{momorel}m%{?dist}
Summary:        View your mind

Group:          Applications/Productivity
License:        GPLv2+
URL:            http://www.insilmaril.de/vym/
Source0:        http://dl.sourceforge.net/project/vym/Development/%{name}-%{version}.tar.bz2
NoSource:       0
# Currently obtained from OpenSUSE, SF releases to resume shortly
# http://download.opensuse.org/repositories/home://insilmaril/openSUSE_11.1/src/
# See also BZ 511290
Source1:        %{name}.desktop
Source2:        vym.xml
Source3:	vym-logo-new-16.png
Source4:	vym-logo-new-22.png
Source5:	vym-logo-new-24.png
Source6:	vym-logo-new-32.png
Source7:	vym-logo-new-48.png
Source8:	vym-logo-new-256.png

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  qt-devel >= 4.8.2
BuildRequires:  libXext-devel
BuildRequires:  desktop-file-utils

%description
VYM (View Your Mind) is a tool to generate and manipulate maps
which show your thoughts. Such maps can help you to improve
your creativity and effectivity. You can use them for time management,
to organize tasks, to get an overview over complex contexts.

%prep
%setup -q

%build

qmake-qt4 DOCDIR="%{_docdir}/%{name}-%{version}" PREFIX=%{_prefix}

%{__make} %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
%{__make} install INSTALL_ROOT=%{buildroot} COPY="%{__cp} -p -f"

%{__mkdir} -p %{buildroot}%{_datadir}/applications/

desktop-file-install --vendor=                 \
    --dir %{buildroot}%{_datadir}/applications \
    %{SOURCE1}

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/16x16/apps
%{__cp} -p %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{__cp} -p icons/%{name}.xpm %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.xpm

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/22x22/apps
%{__cp} -p %{SOURCE4} %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/24x24/apps
%{__cp} -p %{SOURCE5} %{buildroot}%{_datadir}/icons/hicolor/24x24/apps/%{name}.png

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/32x32/apps
%{__cp} -p %{SOURCE6} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/48x48/apps
%{__cp} -p %{SOURCE7} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{__cp} -p icons/%{name}-editor.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}-editor.png

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/256x256/apps
%{__cp} -p %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{name}.png

install -m a+rx,u+w -d %{buildroot}%{_datadir}/mime/packages
install -p -m a+r,u+w %{SOURCE2} %{buildroot}%{_datadir}/mime/packages/vym.xml


%clean
%{__rm} -rf %{buildroot}

%post 
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

update-desktop-database &> /dev/null || :
  
%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

update-desktop-database &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc LICENSE.txt README.txt INSTALL.txt demos/* doc/*
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/16x16/apps/%{name}*
%{_datadir}/icons/hicolor/22x22/apps/%{name}*
%{_datadir}/icons/hicolor/24x24/apps/%{name}*
%{_datadir}/icons/hicolor/32x32/apps/%{name}*
%{_datadir}/icons/hicolor/48x48/apps/%{name}*
%{_datadir}/icons/hicolor/256x256/apps/%{name}*
%{_datadir}/mime/packages/vym.xml


%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0 (sync with Fedora)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.7-5m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.7-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12.7-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.7-2m)
- rebuild against qt-4.6.3-1m

* Mon Apr  5 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.7-1m)
- import from Fedora 13
- update to 1.12.7

* Wed Dec 02 2009 Jon Ciesla <limb@jcomserv.net> - 1.12.6-1
- Updated to new upstream version, BZ 543442.

* Mon Nov 23 2009 Rex Dieter <rdieter@fedoraproject.org> - 1.12.5-2 
- rebuild (for qt-4.6.0-rc1, f13+)

* Mon Nov 16 2009 Jon Ciesla <limb@jcomserv.net> - 1.12.5-1
- Updated to new upstream version, BZ 537799.

* Tue Aug 04 2009 Jon Ciesla <limb@jcomserv.net> - 1.12.4-0
- Updated to new upstream version.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.12.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.12.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec 05 2008 Jon Ciesla <limb@jcomserv.net> - 1.12.2-1
- Updated to new upstream version.

* Mon Jul 14 2008 Jon Ciesla <limb@jcomserv.net> - 1.12.0-1
- Updated to new upstream version.
- Dropped all patches, applied upstream.

* Tue Jun 10 2008 Jon Ciesla <limb@jcomserv.net> - 1.10.0-4
- Added mime type xml, BZ434929.

* Fri Feb 08 2008 Jon Ciesla <limb@jcomserv.net> - 1.10.0-3
- GCC 4.3 rebuild.

* Wed Jan 09 2008 Jon Ciesla <limb@jcomserv.net> - 1.10.0-2
- Added typeinfo patches.

* Thu Oct 18 2007 Jon Ciesla <limb@jcomserv.net> - 1.10.0-1
- Upgrade to 1.10.0.
- Applied several patches from Till Maas, which he sent upstream.
- Dropped findlang, as it doesn't work with Vym.

* Thu Aug 16 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-9
- License tag correction.

* Wed Mar 21 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-8
- Converted patches to unified output format.

* Wed Mar 21 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-7
- Dropped symlink in favor of path patches.

* Wed Mar 21 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-6
- Fixed Source URL.

* Tue Mar 20 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-5
- doc link fix, icon fix.

* Mon Mar 19 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-4
- Fixed Desktop icon path, make copy timestamps, cleaned scripts.
- Added symlink to fix PDF location.

* Tue Mar 13 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-3
- Fixed source url.
- added desktop-file-utils,kdelibs BRs.
- Fixed desktop file handling.

* Tue Mar 13 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.1-2
- Submitting for review.

* Wed Nov 22 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org > - 1.8.1-1
- Initial Package.
