%global momorel 15

%global xfce4ver 4.10.0

Summary: 	Print dialog and printer manager for XFce 4
Name: 		xfprint
Version: 	4.6.1
Release:	%{momorel}m%{?dist}
License:	BSD
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/archive/%{name}/4.6/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         %{name}-%{version}-cups16.patch
Group: 		User Interface/Desktops
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	xfconf-devel >= %{xfce4ver}
BuildRequires:	xfce4-settings >= %{xfce4ver}
BuildRequires:	a2ps
BuildRequires:	cups-devel >= 1.6.1
BuildRequires:	gnutls-devel
BuildRequires:	gettext
BuildRequires:	glib2-devel
BuildRequires:	gtk2-devel
BuildRequires:	libxml2-devel
BuildRequires:	pkgconfig
Requires: a2ps
Requires: gnome-icon-theme
Requires: libxfcegui4 >= %{xfce4ver}

%description
xfprint contains a print dialog and a printer manager for the
Xfce 4 Desktop Environment

%package devel
Summary: Development tools for the Xfce xfprint manager
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Static libraries and header files for the Xfce xfprint library.

%prep
%setup -q
%patch0 -p0 -b .cups16

%build
%configure LIBS="-lX11"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}

find %{buildroot} -name "*.a" -delete
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
#touch --no-create %{_datadir}/icons/hicolor || :
#%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%postun
#touch --no-create %{_datadir}/icons/hicolor || :
#%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc README ChangeLog NEWS INSTALL COPYING AUTHORS
%{_bindir}/*
%{_datadir}/locale/*/*/*
%{_datadir}/icons/*/*/*/*
%exclude %{_datadir}/icons/hicolor/*/devices/printer.png
%exclude %{_datadir}/icons/hicolor/scalable/devices/printer.svg
%{_datadir}/xfce4/doc/*/images/*
%{_datadir}/xfce4/doc/*/xfprint.html
%{_datadir}/applications/*
%{_datadir}/gtk-doc/html/libxfprint/
%{_libdir}/*.so.*
%{_libdir}/xfce4/xfprint-plugins/*

%files devel
%defattr(-, root, root)
%{_includedir}/xfce4/libxfprint
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-15m)
- rebuild with cups >= 1.6

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.6.1-14m)
- rebuild against xfce4-4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-13m)
- change Source0 URI

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-12m)
- rebuild for glib 2.33.2

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-11m)
- rebuild against xfce 4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-10m)
- rebuild against xfce 4.8pre2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.1-7m)
- full rebuild for mo7 release

* Wed Aug  4 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-6m)
- rebuild against gcxfce4 4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.1-5m)
- fix build

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- revise BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Wed Jan 31 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-2m)
- exclude %%{_datadir}/icons/hicolor/*/devices/printer.png
  and %%{_datadir}/icons/hicolor/scalable/devices/printer.svg
  use gnome-icon-theme
- add Requires: gnome-icon-theme

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.2.3-3m)
- rebuild against expat-2.0.0-1m

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-2m)
- delete duplicated dir

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)
- minor bugfixes

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.5-1m)
- version update to 4.0.5.

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-3m)
- rebuild against for gtk+-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-2m)
- rebuild against for libxml2-2.6.8
- rebuild against for glib-2.4.0

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.4-1m)

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- version up to 4.0.3

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-1m)
- version up to 4.0.2

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-1m)
- version up to 4.0.1

* Fri Sep 26 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (4.0.0-1m)
- version up

* Sat Sep 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.99.4-1m)
- XFce4 RC4 release
