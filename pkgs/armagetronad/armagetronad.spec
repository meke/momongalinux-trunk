%global momorel 12

Summary: A multiplayer OpenGL 'Tron' racing game clone
Name: armagetronad
Version: 0.2.8.2.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Amusements/Games  
Source0: http://dl.sourceforge.net/sourceforge/armagetronad/%{name}-%{version}.src.tar.bz2
NoSource: 0
Source1: http://armagetron.sourceforge.net/addons/moviepack.zip
NoSource: 1
Source2: http://armagetron.sourceforge.net/addons/moviesounds_fq.zip
NoSource: 2

Source3: settings.cfg
Patch0: %{name}-0.2.8.2-uninstall.patch
Patch100: %{name}-0.2.8.2.1-gcc43.patch
URL: http://armagetron.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: SDL_image >= 1.2.0 esound
BuildRequires: gcc-c++ >= 3.4.1-1m, zlib-devel, libpng-devel, libjpeg-devel >= 8a
BuildRequires: mesa-libGLU-devel, mesa-libGL-devel
BuildRequires: SDL_image-devel, SDL-devel >= 1.2.7-9m, esound-devel
BuildRequires: unzip, ImageMagick, desktop-file-utils >= 0.16

%description
There is not much to be said about the game: you ride a lightcycle,
a kind of motorbike that can't be stoppen and leaves a wall where
it goes. You can make turns of 90 degrees and can accelerate by 
driving close to walls. Make your enemies hit a wall while avoiding
the same fate.

%package moviepack
Summary: Extra graphics and sounds to give armagetron the real 'Tron' look.
Group: Amusements/Games
Requires: %{name}

%description moviepack
This package includes all files needed by armagetron to have the "real" look
from the original Tron movie. This includes neat colorful graphics and a few
sounds.
In this package's documentation directory, you will also find a new config
file that you can use in %{_sysconfdir}/armagetronad to have the game be a bit
more realistic (read "fast!" ;-)).

%prep
%setup -q
%patch0 -p1 -b .uninstall
%patch100 -p1 -b .gcc43

%build
case "`gcc -dumpversion`" in
4.6.*)
	# armagetronad.spec-0.2.8.2.1 requires this
	CFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	CXXFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	;;
esac
export CFLAGS CXXFLAGS
%configure \
	--enable-music \
	--disable-sysinstall \
	--disable-uninstall
#	--disable-games
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot} installed-docs
%makeinstall transform='s,x,x,'

# Put the docs where we include them with %%doc
mkdir -p installed-docs
mv %{buildroot}%{_docdir}/games/armagetronad/html installed-docs/

# Add the moviepack stuff
unzip -d %{buildroot}%{_datadir}/games/armagetronad %{SOURCE1}
unzip -d %{buildroot}%{_datadir}/games/armagetronad %{SOURCE2}

# Yeah, add icons for the menu entry!
# New freedesktop locations
install -D -p -m 644 desktop/icons/large/armagetronad.png \
    %{buildroot}%{_datadir}/icons/hicolor/48x48/armagetronad.png
install -D -p -m 644 desktop/icons/medium/armagetronad.png \
    %{buildroot}%{_datadir}/icons/hicolor/32x32/armagetronad.png
install -D -p -m 644 desktop/icons/small/armagetronad.png \
    %{buildroot}%{_datadir}/icons/hicolor/16x16/armagetronad.png
# Legacy location (put 32 x 32 in there)
install -D -p -m 644 desktop/icons/medium/armagetronad.png \
    %{buildroot}%{_datadir}/pixmaps/armagetronad.png

# Put the realistic config where we can get it
cp -a %{SOURCE3} settings.cfg.realistic

# Install desktop file
sed -e "s/Version.*//g" \
    -e "s/GamesAction;//g" \
    -e "s/GameAction;//g" \
    -e "s/ActionGames;//g" \
    -i desktop/armagetronad.desktop
mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications     \
  desktop/armagetronad.desktop

# Workaround for 0.2.8_beta3 not finding the config files in /etc/
#ln -s %{_sysconfdir}/armagetronad %{buildroot}%{_datadir}/armagetronad/config

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc installed-docs/html
%dir %{_sysconfdir}/games/armagetronad
%config(noreplace) %{_sysconfdir}/games/armagetronad/*
%{_bindir}/armagetronad
%{_datadir}/applications/armagetronad.desktop
%{_datadir}/doc/games/armagetronad
%dir %{_datadir}/games/armagetronad
%exclude %{_datadir}/games/armagetronad/desktop
%{_datadir}/games/armagetronad/language
%{_datadir}/games/armagetronad/models
%{_datadir}/games/armagetronad/resource
%exclude %{_datadir}/games/armagetronad/scripts
%{_datadir}/games/armagetronad/sound
%{_datadir}/games/armagetronad/textures
%{_datadir}/icons/hicolor/*/armagetronad.png
%{_datadir}/pixmaps/armagetronad.png

%files moviepack
%defattr(644, root, root, 755)
%doc settings.cfg.realistic
%{_datadir}/games/armagetronad/moviepack
%{_datadir}/games/armagetronad/moviesounds

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8.2.1-12m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8.2.1-11m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8.2.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.8.2.1-9m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8.2.1-8m)
- build fix with desktop-file-utils-0.16

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.2.1-7m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.2.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.8.2.1-5m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.2.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.8.2.1-3m)
- rebuild against gcc43

* Tue Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.8.2.1-2m)
- fix gcc-4.3 build error

* Fri Feb  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8.2.1-1m)
- version 0.2.8.2.1
- import armagetronad-0.2.8.2-uninstall.patch from dag
 +* Wed Jun 28 2006 Matthias Saou <http://freshrpms.net/> 0.2.8.2-1
 +- Include patch to fix the uninstall lines in Makefile.in.
- modify spec file

* Fri Feb  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8.1-2m)
- include executable armagetronad

* Fri Dec 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.8.1-1m)
- update 0.2.8.1

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.7.1-4m)
- remove category Application

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.7.1-3m)
- remove vendor from desktop file
- BuildRequires: desktop-file-utils

* Sat Sep 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.7.1-2m)
- add gcc4 patch
- Patch2: armagetronad-0.2.7.1-gcc4.patch

* Sat Jul  2 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.7.1-1m)
- update to 0.2.7.1
- change package name to "armagetronad"
- remove x86_64 patch

* Sun May 29 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.2.6.1-1m)
- up to 0.2.6.1
- armagetron was changed to armagetronad, if you play armagetron, plz update to armagetronad

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.2.5.2-5m)
- enable x86_64.

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.5.2-4m)
- rebuild againist SDL-1.2.7-9m

* Tue Aug 31 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.2.5.2-3m)
- rebuild against gcc-c++-3.4.1
- add gcc34 patch

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.5.2-2m)
- rebuild against DirectFB-0.9.21

* Thu Mar 16 2004 Toru Hoshina <t@momonga-linux.org>
- (0.2.5.2-1m)
- import from freshrpms.net.

* Fri Dec 12 2003 Matthias Saou <http://freshrpms.net/> - 0.2.5.2-2.fr
- Added missing XFree86-Mesa-libGLU build dep :-(
- Rebuild for Fedora Core 1 at last.

* Wed Oct 15 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.5.2.
- Removed %%{prever} stuff.

* Sun Jul 20 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.5.

* Thu Jul 10 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.4.

* Sun Jul  6 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.3.

* Mon Jun 30 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.2.
- The build is now more standard, no more manual copying of files.
- PPC include workaround removed, not needed anymore.
- Major spec file updates.

* Sat Jun 14 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.pre3.

* Mon Jun  2 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- s/Games/Game for the desktop file, doh!

* Mon Mar 31 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Rebuilt for Red Hat Linux 9.

* Sat Mar 15 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Remove CVS directories from the docs.
- Added --without freedesktop option in order to rebuild for 7.x.
- Added YellowDog 2.3 include path workaround.

* Sat Feb 15 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Applied Ben Liblit's changes at last!
- Install language files.
- Change wrapper to save settings to ~/.armagetron/

* Sun Sep 29 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Rebuilt for Red Hat Linux 8.0.
- New menu entry.

* Tue Jul  2 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.2.0.pre_020624.

* Thu May  2 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Rebuilt against Red Hat Linux 7.3.
- Added the %{?_smp_mflags} expansion.

* Fri Apr 19 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Rebuilt without the NVIDIA libGLcore.so.1 dependency (doh!).

* Tue Nov 20 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Added a simple shell script to have the menu entry work with KDE.
- Added a separate package for the "moviepack"... that thing is cool :-)
- Changed the binary to sgid "games" so that high scores are saved.
- Added an icon for the menu entry.

* Mon Nov 19 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.1.4.9.
- Spec file cleanup.

* Thu Jan  4 2001 Tim Powers <timp@redhat.com>
- defattr was in wrong place in files list, leaving files owned by the
  build system, fixed

* Tue Nov 28 2000 Karsten Hopp <karsten@redhat.de>
- initial RPM

