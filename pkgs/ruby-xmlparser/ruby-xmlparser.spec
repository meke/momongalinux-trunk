%global momorel 4

%global rbname xmlparser
Summary: XML Parser for Ruby
Name: ruby-xmlparser

Version: 0.7.1
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Ruby
URL: http://www.yoshidam.net/Ruby.html

Source0: http://www.yoshidam.net/%{rbname}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: expat-devel >= 1.95.2
BuildRequires: perl-XML-Parser

Requires: ruby >= 1.9.2 expat >= 1.95.2

%description
XML parser for Ruby.

%prep
%setup -q -n xmlparser

%build
ruby extconf.rb --with-perl-enc-map=%{perl_sitearch}/XML/Parser/Encodings
make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_datadir}/XML/Parser
cp -R Encodings %{buildroot}%{_datadir}/XML/Parser

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README README.ja
%{ruby_sitearchdir}/xmlparser.so
%{ruby_sitelibdir}/*.rb
%{ruby_sitelibdir}/xml
%{_datadir}/XML

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.8-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.8-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.8-7m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.8-6m)
- rebuild against ruby-1.8.6-4m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.6.8-5m)
- rebuild against expat-2.0.0-1m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6.8-4m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.8-3m)
- rebuild against ruby-1.8.2

* Thu Apr  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.8-2m)
- Buildrequires: perl-XML-Parser

* Wed Apr  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.8-1m)
- fix a buffer overflow of character encoding name.
- taint outputs when a input XML is tainted.                               
- append XML::Parser object to block parameters of XML::Parser#parse method
- delete features of expat-1.2

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.6.5-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.6.5-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.6.5-2m)
- rebuild against ruby-1.8.0.

* Wed Sep 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.5-1m)

* Sun Mar 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.6.2-2k)
- BuildPreReq: not expat but expat-devel

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.6.1-10k)
- nigittenu

* Mon Jun  4 2001 Toru Hoshina <toru@df-usa.com>
- (0.6.1-6k)

* Sat Jan 06 2001 Kenichi Matsubara <m@kondara.org>
- (0.6.1-5k)
- add BuildPreReq: expat.

* Wed Jan 03 2001 Kusunoki Masanori <nori@kondara.org>
- (0.6.1-3k)
- up to 0.6.1.
- split expat.

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- it should be suitable to both ruby 1.4.x and 1.6.x.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Thu Apr 20 2000 Toru Hoshina <t@kondara.org>
- Multi platform support.

* Sat Apr  8 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- First release for Kondara.
