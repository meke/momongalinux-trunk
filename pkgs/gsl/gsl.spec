%global momorel 4

Summary:        GNU Scientific Library (GSL)
name:           gsl
Version:        1.14
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Group:          Development/Libraries
URL:            http://www.gnu.org/software/gsl/
Source0:        ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
Patch1:         gsl-1.10-lib64.patch
BuildRequires:  sed
BuildRequires:  pkgconfig
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The GNU Scientific Library (GSL) is a numerical library for C and C++
programmers. It contains over 1000 mathematical routines written in ANSI
C. The library follows modern coding conventions, and lends itself to
being used in very high level languages (VHLLs).

The library covers the following subject areas:

  Complex Numbers             Roots of Polynomials     Special Functions
  Vectors and Matrices        Permutations             Sorting
  BLAS Support                Linear Algebra           Eigensystems
  Fast Fourier Transforms     Quadrature               Random Numbers
  Quasi-Random Sequences      Random Distributions     Statistics
  Histograms                  N-Tuples                 Monte Carlo Integration
  Simulated Annealing         Differential Equations   Interpolation
  Numerical Differentiation   Chebyshev Approximation  Series Acceleration
  Discrete Hankel Transforms  Root-Finding             Minimization
  Least-Squares Fitting       Physical Constants       IEEE Floating-Point

Further information can be found in the GSL Reference Manual.

Install the gsl package if you need a library for high-level
scientific numerical analysis.

%package devel
Summary:        Static libraries and header files for GSL development.
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires(post): info
Requires(preun): info

%description devel
The gsl-devel package contains the header files and static libraries
necessary for developing programs using the GSL (GNU Scientific
Library).

%package static
Summary:        Static libraries of the GSL package
Group:          Development/Libraries
Requires:       %{name}-devel = %{version}-%{release}

%description static
The gsl-static package includes static libraries of GSL.

%prep
%setup -q
%patch1 -p1 -b .lib64~

%build
#  compiler options for all architecture

# -ffast-math is harmful for numerical libraries
RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS -fno-fast-math`

## gcc-4.3.1-4m with -fstrict-aliasing causes some tests to fail
RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS -fno-strict-aliasing`

#  architecture dependent compiler options
# last -O? is effective
%ifarch alpha ppc
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -O1"
#  `make check' fails with the -O2 option on alpha machines.
#  (1.0-2k, zunda <zunda@kondara.org>)
%endif

case "`gcc -dumpversion`" in
4.4.*)
    RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed s/-O2/-O1/`
;;
esac

CFLAGS="$RPM_OPT_FLAGS" ./configure %{_target_platform} --prefix=%{_prefix}
%make

%check
%ifnarch ia64 alpha
%make check
%endif
#  It might be a good idea to check compilation everytime we rebuild the
#  package, because the result strongly depends upon the compile options,
#  which changes upon rebuild even if it takes time. Otherwise the
#  library may return false values. When the check fails, check the
#  compiler options first and the file INSTALL in the source directory
#  would help you.
#  (1.0-2k, zunda <zunda@kondara.org>)

%install
rm -rf %{buildroot}
%makeinstall

# remove
%__rm -f %{buildroot}%{_infodir}/dir

# for multilib
gslcsuffix=`echo "%{_libdir}" | sed s,/usr/,,`
mv %{buildroot}%{_bindir}/gsl-config %{buildroot}%{_bindir}/gsl-config-$gslcsuffix
cat > %{buildroot}%{_bindir}/gsl-config << EOF
#!/bin/sh
if [ -e %{_bindir}/gsl-config-lib64 ]; then
  exec %{_bindir}/gsl-config-lib64 "\$@"
elif [ -e %{_bindir}/gsl-config-* ]; then
  gslcfile="\`ls %{_bindir}/gsl-config-* | head -n1\`"
  exec \$gslcfile "\$@" 
fi
EOF
chmod 755 %{buildroot}%{_bindir}/gsl-config

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/gsl-ref.info %{_infodir}/dir

%preun devel
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gsl-ref.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS THANKS COPYING INSTALL
%doc README TODO BUGS
%{_libdir}/*so.*
%{_bindir}/gsl-histogram
%{_bindir}/gsl-randist
%{_mandir}/man1/gsl-histogram.1*
%{_mandir}/man1/gsl-randist.1*

%files devel
%defattr(-,root,root)
%doc AUTHORS COPYING
%{_bindir}/gsl-config*
%{_datadir}/aclocal/*
%{_includedir}/*
%{_infodir}/*info*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man1/gsl-config.1*
%{_mandir}/man3/*

%files static
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_libdir}/*.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-1m)
- update to 1.12 which work well with recent autotools
-- drop Patch2, not needed
- use -O1 when gcc44
- License: GPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-2m)
- rebuild against rpm-4.6

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-1m)
- update 1.11
- revise RPM_OPT_FLAGS
- add static sub-package for *.a

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-2m)
- rebuild against gcc43

* Fri Feb  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-1m)
- update to 1.10
- add CFLAGS="-O0" for make test

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-2m)
- fix %%changelog section

* Thu May 31 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Sun Apr 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8-5m)
- remove "-mtune=generic" from optflags for gcc-4.1.2-7m
- and modify optflags s/i686/i586/ again

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8-4m)
- delete libtool library

* Sat Nov 11 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.8-3m)
- separated devel package
- import gsl-1.1-nousr.patch and gsl-1.8-lib64.patch from Fedora Core
- make check does not fail for i686 option

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8-2m)
- delete duplicated dir

* Thu Apr 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.8-1m)
- update to 1.8
- revised for multilib (like FC)

* Wed Jan 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7-3m)
- modify optflags s/i686/i586/ for gcc-4.1.0-0.6m

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7-2m)
- enable ia64, alpha

* Fri Sep 16 2005 zunda <zunda at freeshell.org>
- (1.7-1m)
- source update
- tested with gcc-3.4.3-16m on a Pentum 4 as i686

* Sun Jan 30 2005 zunda <zunda at freeshell.org>
- (1.6-1m)
- source update
- tested with gcc-3.4.3-5m on a Pentum 4 as i686

* Sun Jan  8 2005 Toru Hoshina <t@momonga-linux.org>
- (1.5-2m)
- force gcc 3.2.3.

* Tue Aug 10 2004 zunda <zunda at freeshell.org>
- (1.5-1m)
- source update
- Patch0: gsl-1.4-gsl_const.patch removed

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4-3m)
- revised spec for enabling rpm 4.2.

* Tue Sep 16 2003 zunda <zunda at freeshell.org>
- (1.4-2m)
- bug in gsl/const.h fixed with gsl-1.4-gsl_const.patch

* Thu Sep 11 2003 zunda <zunda at freeshell.org>
- (1.4-1m)
- source update, tested on a Pentium 4

* Thu Jan  9 2003 zunda <zunda@freeshell.org>
- (1.3-1m, kossori)
- removed MACHINES file from %doc: it has been merged into INSTALL

* Thu Jan  9 2003 zunda <zunda@freeshell.org>
- (1.3-1m) 
- source update: Changed interface for gsl_sf_coupling_6j...(...) etc.
- MACHINES file is missing in the source tar ball
- make test passes at least on an Athlon with gcc-3.2-8m

* Mon Oct  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2-3m)
- compile option tuning for gcc-3.2
- use gcc-3.2 on ppc

* Mon Sep 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2-2m)
- use gcc_2_95_3 on ppc

* Thu Jul 25 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2-1m)
  update to 1.2

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.1.1-8k)
- cancel gcc-3.1 autoconf-2.53

* Tue Apr 23 2002 zunda <zunda@kondara.org>
- (1.1.1-6k)
- gsl_qrng_init from gsl-discuss <5556.22035.593447.329481@debian>

* Tue Apr 16 2002 zunda <zunda@kondara.org>
- (1.1.1-4k)
- gsl_rng_mt19937_1998 was missing in gsl/gsl_rng.h

* Sun Mar 17 2002 zunda <zunda@kondara.org>
- (1.1.1-2k)
- source update

* Tue Mar  5 2002 zunda <zunda@kondara.org>
- (1.1-2k)
- source update
- description copied from gsl.spec.in in gsl-1.1.tar.gz

* Mon Feb 25 2002 zunda <zunda@kondara.org>
- (1.0-3.020020225001k)
- CVS version of gsl
- compiler option adaptation for Jirai

* Thu Feb 21 2002 zunda <zunda@kondara.org>
- (1.0-2k)
- OmoiKondarize
- literal %configure procedure copied from macro file in rpm-3.0.6-44k
  to avoid -ffast-math option

* Thu Nov 24 2001 zunda <zunda@m-net.arbornet.org>
- (1.0-3z) Info did not install correctly

* Thu Nov 20 2001 zunda <zunda@m-net.arbornet.org>
- (1.0-2z) Changed doc list not to include the file ../info/dir

* Thu Nov  7 2001 zunda <zunda@m-net.arbornet.org>
- (1.0-1z) Update

* Thu Oct 30 2001 zunda <zunda@m-net.arbornet.org>
- (0.9.4-1z) Update

* Thu Sep  6 2001 zunda <zunda@m-net.arbornet.org>
- (0.9.2-1z) Update

* Sun Aug 26 2001 zunda <zunda@m-net.arbornet.org>
- (0.9.1-1z) Merged specfiiles from gsl-0.9.1-0 and gsl-0.9-1z

* Thu Jul 12 2001 zunda <zunda@m-net.arbornet.org>
- (0.9-1z) Merged specfile in gsl-0.9-0.src.rpm
- rm -rf $RPM_BUILD_ROOT adddd after %%install to avoid hunging in gzipping

* Mon Apr 16 2001 zunda <zunda@m-net.arbornet.org>
- (0.7-1z)
- Kondarize

* Thu Jan 18 2001 Preston Brown <pbrown@redhat.com>
- prereq install-info (#24250)

* Mon Dec 11 2000 Preston Brown <pbrown@redhat.com>
- 0.7, remove excludearch for ia64

* Sun Jul 30 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- fix %post to be a real shell and add ldconfig to %post

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jun 19 2000 Preston Brown <pbrown@redhat.com>
- don't include the info dir file...

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- add %%defattr

* Mon Jun 12 2000 Preston Brown <pbrown@redhat.com>
- 0.6, FHS paths
- exclude ia64, it is having issues

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Thu Mar 11 1999 Bill Nottingham <notting@redhat.com>
- update to 0.3f
- add patches to fix glibc-2.1 compilation, doc oddity

* Thu Feb 25 1999 Bill Nottingham <notting@redhat.com>
- new summary/description, work around automake oddity

* Tue Jan 12 1999 Michael K. Johnson <johnsonm@redhat.com>
- libtoolize for arm

* Thu Sep 10 1998 Cristian Gafton <gafton@redhat.com>
- spec file fixups

* Sat May 9 1998 Michael Fulbright <msf@redhat.com>
- started with package for gmp from Toshio Kuratomi <toshiok@cats.ucsc.edu>
- cleaned up file list
- fixed up install-info support
