%global         momorel 2

Name:           perl-Capture-Tiny
Version:        0.24
Release:        %{momorel}m%{?dist}
Summary:        Capture STDOUT and STDERR from Perl, XS or external programs
License:        Apache
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Capture-Tiny/
Source0:        http://www.cpan.org/authors/id/D/DA/DAGOLDEN/Capture-Tiny-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp
BuildRequires:  perl-IO
BuildRequires:  perl-PathTools
BuildRequires:  perl-Test-Simple >= 0.62
Requires:       perl-File-Temp
Requires:       perl-IO
Requires:       perl-PathTools
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Capture::Tiny provides a simple, portable way to capture anything sent to
STDOUT or STDERR, regardless of whether it comes from Perl, from XS code or
from an external program. Optionally, output can be teed so that it is
captured while being passed through to the original handles. Yes, it even
works on Windows. Stop guessing which of a dozen capturing modules to use
in any particular situation and just use this one.

%prep
%setup -q -n Capture-Tiny-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes examples LICENSE README Todo xt
%{perl_vendorlib}/Capture/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24
- rebuild against perl-5.18.2

* Mon Oct 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-2m)
- rebuild against perl-5.18.0

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.16.3

* Thu Nov 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.16.2

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-2m)
- rebuild against perl-5.16.1

* Tue Aug  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.14.1

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-2m)
- rebuild for new GCC 4.6

* Mon Feb  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.08-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- updatee to 0.08

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-2m)
- rebuild against perl-5.12.0

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
