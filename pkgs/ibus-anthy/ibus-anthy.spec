%global momorel 1
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%global fedora 18

%define sub_version                     1.0
%define require_ibus_version            1.4.99.20120203
%define have_default_layout             1
%define have_bridge_hotkey              1

Name:       ibus-anthy
Version:    1.4.99.20121006
Release: %{momorel}m%{?dist}
Summary:    The Anthy engine for IBus input platform
License:    GPLv2+
Group:      System Environment/Libraries
URL:        http://code.google.com/p/ibus/
Source0:    http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0

# Patch0:     ibus-anthy-HEAD.patch

%if 0%{?fedora} <= 17
# IBusProperty.symbol is not used by ibus-gjs
Patch91:     ibus-anthy-xx-disable-prop-symbol.patch
%endif

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  anthy-devel
BuildRequires:  gettext-devel
BuildRequires:  glib2-devel
BuildRequires:  ibus 
BuildRequires:  intltool
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  gobject-introspection-devel

Requires:   ibus >= %{require_ibus_version}
Requires:   anthy
Requires:   kasumi
Requires:   pygobject3

%description
The Anthy engine for IBus platform. It provides Japanese input method from
a library of the Anthy.

%package devel
Summary:    Development tools for ibus
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   glib2-devel
Requires:   anthy-devel

%description devel
The ibus-anthy-devel package contains .so file and .gir files
for developers.

%prep
%setup -q
# patch0 -p1

%if 0%{?fedora} <= 17
%patch91 -p1
%endif

%build
%configure \
%if %have_default_layout
  --with-layout='default' \
%endif
%if %have_bridge_hotkey
  --with-hotkeys \
%endif
  --disable-static
# make -C po update-gmo
%make 

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/libanthygobject-%{sub_version}.la

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
# recreate icon cache
touch --no-create %{_datadir}/icons/hicolor || :
[ -x %{_bindir}/gtk-update-icon-cache ] && \
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
# recreate icon cache
touch --no-create %{_datadir}/icons/hicolor || :
[ -x %{_bindir}/gtk-update-icon-cache ] && \
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
# dir {python_sitearch}/ibus
%{_libdir}/libanthygobject-%{sub_version}.so.*
%{_libdir}/girepository-1.0/Anthy*.typelib
%{_libexecdir}/ibus-*-anthy
%{_datadir}/applications/ibus-setup-anthy.desktop
%{_datadir}/ibus-anthy
%{_datadir}/ibus/component/*
%{_datadir}/icons/hicolor/scalable/apps/ibus-anthy.svg 

%files devel
%{_datadir}/gir-1.0/Anthy*.gir
%{_includedir}/ibus-anthy-%{sub_version}
%{_libdir}/libanthygobject-%{sub_version}.so

%changelog
* Sat Oct 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.99.20121006-1m)
- update to 1.4.99.20121006

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.6-1m)
- update to 1.2.6

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.5-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-2m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2.20101015-1m)
- update to 1.2.2.20101015

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-3m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- decrease rank to 80

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-1m)
- udpate 1.2.1

* Thu Jan 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20100115-1m)
- update to 1.2.0.20100115

* Sat Dec 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20091127-1m)
- update to 1.2.0.20091127

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090917-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090917-1m)
- update to 1.2.0.20090917

* Wed Jun  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090603.1m)
- update 1.1.0-20090603

* Tue Apr  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090402.1m)
- update 1.1.0-20090402

* Mon Mar 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090211.1m)
- update 1.1.0-20090211

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-0.20080912.3m)
- rebuild against rpm-4.6

* Sat Jan  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-0.20080912.1m)
- merge from TSUPPA4RI-branch

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-0.20080912.1m)
- update 20080912

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080819.1m)
- update to 20080819 git snapshot

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080810.1m)
- initial package for Momonga Linux
