%global momorel 1
%global qtver 4.8.5
%global kdever 4.11.2
%global kdelibsrel 1m
%global kdegraphicssrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: CHM viewer with KDE support
Name: kchmviewer
Version: 6.0
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Publishing
URL: http://www.kchmviewer.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-5.0-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: kdegraphics-libs >= %{kdever}-%{kdegraphicssrel}
Requires: chmlib
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: ImageMagick
BuildRequires: chmlib-devel
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libxml2

%description
KchmViewer is a chm (MS HTML help file format) viewer, written in C++. 
Unlike most existing CHM viewers for Unix, it uses Trolltech Qt widget 
library. It is compiled with full KDE support, including KDE widgets 
and KIO/KHTML.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install icons
mkdir -p %{buildroot}%{_kde4_iconsdir}/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
convert -scale 16x16 packages/%{name}.png %{buildroot}%{_kde4_iconsdir}/hicolor/16x16/apps/%{name}.png
convert -scale 22x22 packages/%{name}.png %{buildroot}%{_kde4_iconsdir}/hicolor/22x22/apps/%{name}.png
convert -scale 32x32 packages/%{name}.png %{buildroot}%{_kde4_iconsdir}/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 packages/%{name}.png %{buildroot}%{_kde4_iconsdir}/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 packages/%{name}.png %{buildroot}%{_kde4_iconsdir}/hicolor/64x64/apps/%{name}.png
install -m 644 packages/%{name}.png %{buildroot}%{_kde4_iconsdir}/hicolor/128x128/apps/

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog FAQ README
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Oct  6 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0-1m)
- version 6.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.3-2m)
- rebuild for new GCC 4.6

* Fri Jan 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3-1m)
- update to 5.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-4m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-2m)
- full rebuild for mo7 release

* Sun Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2-1m)
- version 5.2
- import kde-build.patch from gentoo to enable build
 +- 17 May 2010; Peter Volkov <pva@gentoo.org> kchmviewer-5.2.ebuild,
 +- +files/kchmviewer-5.2-kde-build.patch:
 +- Fix build issue with USE=kde, bug #320191, thank Leonid Podolny and
 +- Vasileios P. Lourdas for report.
- http://bugs.gentoo.org/320191

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1-3m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.1-2m)
- touch up spec file

* Wed Jan  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.1-1m)
- version 5.1
- move icons from crystalsvg to hicolor

* Tue Dec 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2-1m)
- version 4.2
- change License from GPLv2 to GPLv3

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1-1m)
- version 4.1
- update desktop.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-2m)
- remove kio_msits.so to avoid conflicting with kdegraphics-libs-4.1.81
- License: GPLv2

* Tue Dec  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-1m)
- update to 4.0 official release

* Sat Jun 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-0.3.2m)
- remove msits.protocol to avoid conflicting with kdegraphics-4.0.80

* Wed Jun 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.3.1m)
- update to 4.0beta3
- change source URI, sourceforge file release system is broken...

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.2.3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0-0.2.2m)
- rebuild against gcc43

* Tue Feb 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-0.2.1m)
- update to 4.0 beta2

* Sun Jun 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-0.1.1m)
- update to version 4.0 beta1 for KDE4

* Sun Jun 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1-1m)
- version 3.1

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-1m)
- initial package for Momonga Linux
