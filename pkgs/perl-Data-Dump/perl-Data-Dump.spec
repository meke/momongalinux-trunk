%global         momorel 5

Name:           perl-Data-Dump
Version:        1.22
Release:        %{momorel}m%{?dist}
Summary:        Pretty printing of data structures
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Data-Dump/
Source0:        http://www.cpan.org/authors/id/G/GA/GAAS/Data-Dump-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-Test
Requires:       perl-MIME-Base64
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provide a few functions that traverse their argument and
produces a string as its result. The string contains Perl code that, when
evaled, produces a deep copy of the original arguments.

%prep
%setup -q -n Data-Dump-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Data/Dump
%{perl_vendorlib}/Data/Dump.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-2m)
- rebuild against perl-5.18.0

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-2m)
- rebuild against perl-5.16.0

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-2m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.08-3m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.08-2m)
- use vendor

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.06-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
