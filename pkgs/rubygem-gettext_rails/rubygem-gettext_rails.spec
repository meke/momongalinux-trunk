%global momorel 6

# Generated from gettext_rails-2.0.4.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname gettext_rails
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Localization support for Ruby on Rails(>=2.3) by Ruby-GetText-Package
Name: rubygem-%{gemname}
Version: 2.1.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://gettext.rubyforge.org/
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
#NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(gettext_activerecord) >= 2.1.0
Requires: rubygem(locale_rails) >= 2.0.5
Requires: rubygem(rails) >= 2.3.2
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Localization support for Ruby on Rails(>=2.3.2) by Ruby-GetText-Package.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.0-6m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-3m)
- full rebuild for mo7 release

* Fri Nov 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-2m)
- temporarily no NoSource
-- because some hosts of gems.rubyforge.org mirror servers return old
   archives (e.g. s3.amazonaws.com)

* Mon Nov 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- Initial package for Momonga Linux
