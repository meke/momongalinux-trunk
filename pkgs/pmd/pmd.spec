%global momorel 6

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
Name:           pmd
Version:        4.2.5
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Scans Java source code and looks for potential problems
License:        BSD

Source0:        http://dl.sourceforge.net/sourceforge/pmd/pmd-src-%{version}.zip
NoSource:       0
# This patch has not been sent upstream.  It causes the build to use installed
# jars for dependencies rather than use those distributed with the source.  It
# also kills retroweaver dead, dead, dead so it won't interfere with the build.
Patch0:         pmd-4.2.5-build.patch
# This patch was sent upstream on 11 Feb 2009.  It fixes a null pointer
# exception when using the nicerhtml output format.
Patch1:         pmd-4.2.4-nicerhtml.patch
Url:            http://pmd.sourceforge.net/

BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  java-devel
BuildRequires:  ant >= 0:1.6
BuildRequires:  ant-junit
BuildRequires:  junit4 = 0:4.6
BuildRequires:  jaxen >= 0:1.1.1
BuildRequires:  objectweb-asm >= 0:3.1
BuildRequires:  xml-commons-apis >= 1.3.02
Requires:       jpackage-utils >= 0:1.6
Requires:       java
Requires:       jaxen >= 0:1.1.1
Requires:       objectweb-asm >= 0:3.1
Requires:       xerces-j2
Requires:       xml-commons-apis >= 1.3.02
Requires:       jakarta-oro
Requires(post):       jpackage-utils
Requires(postun):     jpackage-utils
Group:          Development/Tools
Buildarch:      noarch
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Provides:       %{name}-manual = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-manual < 0:4.0.0-1

%description
PMD scans Java source code and looks for potential problems like:
* Possible bugs: empty try/catch/finally/switch statements
+ Dead code: unused local variables, parameters and private methods
+ Suboptimal code: wasteful String/StringBuffer usage
+ Overcomplicated expressions: unnecessary if statements, for loops
  that could be while loops
+ Duplicate code: copied/pasted code means copied/pasted bugs

PMD has plugins for JDeveloper, Eclipse, JEdit, JBuilder, BlueJ,
CodeGuide, NetBeans/Sun Java Studio Enterprise/Creator, IntelliJ IDEA,
TextPad, Maven, Ant, Gel, JCreator, and Emacs.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;

# remove an unneeded script in an otherwise documentation directory
rm -f etc/fr_docs/copy_up.sh

%build
ant -Djunit.version=4.6 jar
ant -f bin/build.xml -Ddir.lib=%{_javadir} javadoc

%install
rm -rf $RPM_BUILD_ROOT

# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 lib/%{name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; \
   do ln -sf ${jar} ${jar/-%{version}/}; done)
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
cp -p etc/pmd-nicerhtml.xsl $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}
cp -pr etc/xslt $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}/rulesets
cp -pr rulesets/* $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}/rulesets

# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}.pom

%add_to_maven_depmap %{name} %{name} %{version} JPP %{name}

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(0644,root,root,0755)
%doc LICENSE.txt etc/changelog.txt etc/fr_docs etc/readme.txt
%doc etc/pmdProperties.rtf
%{_javadir}/*.jar
%{_datadir}/%{name}-%{version}
%config(noreplace) %{_sysconfdir}/pmd-nicerhtml.xsl
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.5-4m)
- full rebuild for mo7 release

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.5-3m)
- sync with Fedora 13 (0:4.2.5-5)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.5-1m)
- update to 4.2.5 based on Fedora 11 (0:4.2.5-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-1jpp.3m)
- rebuild against rpm-4.6

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6-1jpp.2m)
- remove package manual to avoid conflicting 
- it's completely same as %%doc of main package

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-1jpp.1m)
- import from Fedora to Momonga
- restrict "jaxen >= 1.1-1jpp.2m" BuildRequires and Requires

* Mon Mar 26 2007 Matt Wringe <mwringe@redhat.com> - 0:3.6-1jpp.3
- Fix unowned doc directory for pmd

* Mon Mar 19 2007 Matt Wringe <mwringe@redhat.com> - 0:3.6-1jpp.2
- Add missing jakarta-commons-oro build requires

* Tue Mar 13 2007 Jeff Johnston <jjohnstn@redhat.com> - 0:3.6-1jpp.1
- Updated per Fedora package review process
- Resolves: #227109

* Mon Jun 19 2006 Deepak Bhole <dbhole@redhat.com> - 0:3.6-1jpp
- Upgrade to 3.6

* Fri Mar 10 2006 Fernando Nasser <fnasser@redhat.com> - 0:3.3-2jpp
- First JPP 1.7 build

* Wed Nov 09 2005 Ralph Apel <r.apel at r-apel.de> - 0:3.3-1jpp
- Upgrade to 3.3
- Fix Groups

* Tue Feb 22 2005 Laurent Goujon <laurent at gobio2.net> - 2.3-1jpp
- Upgrade to 2.3
- Use bin/build.xml

* Mon Sep 13 2004 Ralph Apel <r.apel at r-apel.de> - 1.9-2jpp
- Drop saxpath requirements
- Require jaxen >= 0:1.1
- Relax some versioned requirements

* Wed Aug 25 2004 Fernando Nasser <fnasser@redhat.com> - 1.9-1jpp
- Upgrade to 1.9

* Wed Aug 24 2004 Fernando Nasser <fnasser@redhat.com> - 0:1.5-4jpp
- Rebuild with Ant 1.6.2

* Fri Aug 06 2004 Ralph Apel <r.apel at r-apel.de> - 1.5-3jpp
- Void change

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 1.5-2jpp
- Upgrade to Ant 1.6.X

* Wed Mar 24 2004 Ralph Apel <r.apel at r-apel.de> 1.5-1jpp
- first JPackage release
