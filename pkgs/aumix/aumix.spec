%global momorel 1

# Wether or not to include the X11 version of xaumix
%define with_xaumix		0
# Wether or not to install desktop menu files so it is in GNOME/KDE menus
%define with_desktop_files	0

Summary: An ncurses-based audio mixer
Name: aumix
Version: 2.9.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.jpj.net/~trevor/aumix.html
Source0: http://www.jpj.net/~trevor/aumix/releases/%{name}-%{version}.tar.bz2
Source1: aumix.desktop
Patch0: aumix-2.8-include-signal_h.diff
Patch102: aumix-fix-cursor-color-on-exit.patch
Patch103: aumix-2.9.1-fix-changing-level-non-interactively.patch
BuildRequires: ncurses-devel, autoconf, automake
BuildRequires: gpm-devel >= 1.20.5
Requires: initscripts >= 4.42
# xaumix and its manpage moved from aumix-X11 to aumix in 2.8-1
Conflicts: aumix-X11 < 2.8
# If we're not building the -X11 subpackage, obsolete it so that upgrades
# work on systems that previously had aumix-X11 installd
%if ! %{with_xaumix}
Obsoletes: aumix-X11
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
Aumix is a tty-based, interactive method of controlling a sound card 
mixer. It lets you adjust the input levels from the CD, microphone,
and board synthesizers, as well as the output volume. Aumix can 
adjust audio mixers from the command line, from a script, or 
interactively at the console or terminal with an
ncurses-based interface.

Install aumix if you need to control an audio mixer.  If you want to use
Aumix's GUI, you'll need to install ncurses and gpm for mouse support.

%if %{with_xaumix}
%package X11
Group: Applications/Multimedia
Summary: An X11-based audio mixer
Requires: %{name} = %{version}

%description X11
Aumix-X11 is a gtk-based, interactive method of controlling a sound card
mixer. It lets you adjust the input levels from the CD, microphone,
and board synthesizers, as well as the output volume. Aumix can
adjust audio mixers from the command line, from a script, or
interactively at the console or terminal with an
ncurses-based interface.

Install aumix if you need to control an audio mixer from X11.
%endif

%prep
%setup -q

%patch0 -p1 -b .signal
%patch102 -p0 -b .cursor
%patch103 -p1 -b .overflow

%build
export CFLAGS="$RPM_OPT_FLAGS"

%configure --prefix=/ --without-alsa \
	--without-ncurses --without-gtk --without-gtk1
make
mv src/aumix ./aumix-minimal

%if %{with_xaumix}
make distclean
%configure --without-alsa --without-ncurses
make
mv src/aumix ./aumix-X11
%endif

make distclean
%configure --without-alsa \
	--without-gtk --without-gtk1
make

%install
rm -rf %{buildroot}
%makeinstall

make prefix=%{buildroot}/usr install -C po
mkdir -p %{buildroot}/bin
install -c -m755 aumix-minimal %{buildroot}/bin

%if %{with_xaumix}
#mkdir -p %{buildroot}/usr/X11R6/bin
install -c -m755 aumix-X11 %{buildroot}/%{_bindir}
%{!?with_desktop_files:mkdir -p %{buildroot}/etc/X11/applink/Multimedia}
%{!?with_desktop_feils:install -m 644 %SOURCES1 %{buildroot}/etc/X11/applink/Multimedia/aumix.desktop}
%endif

mkdir -p %{buildroot}/etc
touch %{buildroot}/etc/{aumixrc,.aumixrc}

%find_lang %name

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS BUGS COPYING README TODO NEWS ChangeLog
/bin/aumix-minimal
%{_bindir}/aumix
%{_bindir}/mute
%{_bindir}/xaumix
%{_mandir}/man1/aumix.*
%{_mandir}/man1/mute.*
%{_mandir}/man1/xaumix.*
%{_datadir}/aumix
%ghost %config(missingok,noreplace) %verify(not md5 size mtime) /etc/aumixrc
%ghost %config(missingok,noreplace) %verify(not md5 size mtime) /etc/.aumixrc

%if %{with_xaumix}
%files X11
%defattr(-,root,root)
%{_bindir}/aumix-X11
%{!?with_desktop_files:/etc/X11/applink/Multimedia/aumix.desktop}
%endif

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8-9m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8-8m)
- rebuild against gpm-1.20.5
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8-7m)
- rebuild against gcc43

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8-6m)
- updete spec sync with FC3

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-5m)
- remove aumix desktop
  aumix-X11 doesn't work... X11 package should be removed
- import 3 patches from cooker
 +* Tue Jan 04 2005 Thierry Vignaud <tvignaud@mandrakesoft.com> 2.8-9mdk
 +- merge in fedora patches:
 +  o patch 102: fix cursor color on exit
 +  o patch 103: fix a buffer overflow
 +  o patch 104: fix /usr/bin/mute not restorin volume when unmuting
- add URL

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.8-4m)
- revised spec for enabling rpm 4.2.

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.8-3m)
- rebuild against for XFree86-4.3.0

* Thu Feb  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.8-2m)
  added configure parameter '--without-gtk1'

* Thu Dec 12 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.8-1m)
- update to 2.8

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (2.7-8k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (2.7-4k)
- rebuild against gettext 0.10.40.

* Fri Feb 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- langify
- don't create the .desktop file from within the spec file

* Thu Dec  7 2000 Crutcher Dunnavant <crutcher@redhat.com>
- Upgrade to 2.7

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 27 2000 Preston Brown <pbrown@redhat.com>
- make desktop entry a config file
- 2.6.1
- the nogtk patch has been rolled into the main distro, removed.

* Thu Jun  8 2000 Bill Nottingham <notting@redhat.com>
- FHS stuff

* Mon May 22 2000 Bill Nottingham <notting@redhat.com>
- make desktop link point to correct binary (#11413)

* Wed Apr 13 2000 Bill Nottingham <notting@redhat.com>
- 2.6

* Wed Mar 29 2000 Bill Nottingham <notting@redhat.com>
- 2.5

* Wed Mar 22 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.3
- rebuild with current libncurses
- fix up and re-enable aumix-X11, and move it to a separate package
- add a .desktop file for aumix-X11

* Mon Mar 13 2000 Bill Nottingham <notting@redhat.com>
- update to 2.2

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Mon Dec 20 1999 Bill Nottingham <notting@redhat.com>
- update to 1.30.1

* Mon Nov 29 1999 Bill Nottingham <notting@redhat.com>
- update to 1.28

* Thu Nov  4 1999 Bill Nottingham <notting@redhat.com>
- update to 1.26

* Mon Oct 18 1999 Bill Nottingham <notting@redhat.com>
- update to 1.23.1

* Mon Oct  4 1999 Bill Nottingham <notting@redhat.com>
- update to 1.23

* Mon Sep 20 1999 Bill Nottingham <notting@redhat.com>
- oops, fix regular aumix

* Fri Sep 17 1999 Bill Nottingham <notting@redhat.com>
- add stripped down version in /bin
- kill sound.init (in initscripts now)

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %preun, not %postun

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Wed Jun 23 1999 Bill Nottingham <notting@redhat.com>
- move initscript from rhsound to here. Woohoo - we got rid of a package! ;)

* Tue Jun 22 1999 Bill Nottingham <notting@redhat.com>
- 1.22.1

* Mon Jun 14 1999 Bill Nottingham <notting@redhat.com>
- 1.22

* Sat May 21 1999 Bill Nottingham <notting@redhat.com>
- 1.18.4

* Thu May 06 1999 Bill Nottingham <notting@redhat.com>
- update to 1.18.3

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Wed Feb 24 1999 Bill Nottingham <notting@redhat.com>
- update to 1.18.2

* Mon Feb 22 1999 Bill Nottingham <notting@redhat.com>
- update to 1.18.1

* Mon Feb  8 1999 Bill Nottingham <notting@redhat.com>
- update to 1.17

* Mon Feb  1 1999 Bill Nottingham <notting@redhat.com>
- update to 1.15

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- update to 1.14

* Sat Oct 10 1998 Cristian Gafton <gafton@redhat.com>
- strip binary

* Fri Oct  2 1998 Bill Nottingham <notting@redhat.com>
- updated to 1.13

* Fri Aug 28 1998 Bill Nottingham <notting@redhat.com>
- updated to 1.12

* Mon Aug 17 1998 Bill Nottingham <notting@redhat.com>
- updated to 1.11

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 09 1998 Cristian Gafton <gafton@redhat.com>
- updated to 1.8

* Tue Oct 21 1997 Otto Hammersmith <otto@redhat.com>
- fixed source url
- updated version

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built with glibc

* Thu Mar 20 1997 Michael Fulbright <msf@redhat.com>
- Updated to v. 1.6.1.

