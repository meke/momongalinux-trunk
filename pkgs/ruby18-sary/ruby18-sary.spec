%global momorel 14
%global rbname sary

%global ruby18_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')

Summary: Sary extention for Ruby
Name: ruby18-%{rbname}
Version: 1.2.0
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: LGPLv2+
URL: http://sary.namazu.org/ruby/
Source0: http://sary.sourceforge.net/%{rbname}-ruby-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18
BuildRequires: ruby18-devel
BuildRequires: sary-devel >= 1.2.0

%description
This package add sary methods for Ruby.

%prep
%setup -q -n %{rbname}-ruby-%{version}

%build
ruby18 extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc MANIFEST ChangeLog README* Reference* rsary.rb
%{ruby18_sitearchdir}/sary.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-12m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-11m)
- copy from ruby-sary and build with ruby18

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-10m)
- support ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-5m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-4m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-3m)
- /usr/lib/ruby

* Sat Apr 16 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-2m)
- revised spec. remove itself prior to build.

* Fri Apr 15 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0
- change Source0 URI
- change %setup

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.1-0.20031102.3m)
- rebuild against ruby-1.8.2

* Tue Jun 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.1-0.20031102.2m)
- really add and apply Patch0 (sorry...)

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.1-0.20031102.1m)
- use cvs version in http://www.digital-genes.com/~yatsu/prime/SRPMS/
- add patch0:ruby-sary-20031102-str2cstr.patch

* Sun Jun 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-7m)
- no nosource0

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4-6m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4-5m)
- rebuild against ruby-1.8.0.

* Mon Mar  4 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.4-4k)
- Should BuildPreReq: sary-devel.

* Fri Feb 22 2002 Toshiro HIKITA <toshi@sodan.org>
- (0.4-2k)
- 1st release.

