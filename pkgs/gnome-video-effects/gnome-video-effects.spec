%global momorel 1
Name:           gnome-video-effects
Version:        0.4.0
Release: %{momorel}m%{?dist}
Summary:        Collection of GStreamer video effects

Group:          System Environment/Libraries
License:        GPLv2
URL:            http://live.gnome.org/GnomeVideoEffects
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.4/%{name}-%{version}.tar.xz
NoSource: 0
Buildarch:      noarch

BuildRequires:  intltool

%description
A collection of GStreamer effects to be used in different GNOME Modules.

%prep
%setup -q

%build
%configure
%make 


%install
make install DESTDIR=%{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS  COPYING NEWS README
%{_datadir}/pkgconfig/gnome-video-effects.pc
%{_datadir}/gnome-video-effects


%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-1m)
- reimport from fedora

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-1m)
- initial build
