%global         momorel 3

Name:           perl-Time-ParseDate
Version:        2013.1113
Release:        %{momorel}m%{?dist}
Summary:        Date parsing both relative and absolute
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Time-ParseDate/
Source0:        http://www.cpan.org/authors/id/M/MU/MUIR/modules/Time-ParseDate-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Time-modules < %{version}
Provides:       perl-Time-modules = %{version}

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module recognizes the above date/time formats. Usually a date and a
time are specified. There are numerous options for controlling what is
recognized and what is not.

%prep
%setup -q -n Time-ParseDate-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json README
%{perl_vendorlib}/Time/CTime.pm
%{perl_vendorlib}/Time/DaysInMonth.pm
%{perl_vendorlib}/Time/JulianDay.pm
%{perl_vendorlib}/Time/ParseDate.pm
%{perl_vendorlib}/Time/Timezone.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2013.1113-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2013.1113-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2013.1113-1m)
- update to 2013.1113

* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2013.0920-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
