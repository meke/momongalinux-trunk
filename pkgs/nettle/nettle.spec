%global momorel 2

Summary: a low-level cryptographic library
Name: nettle
Version: 2.7.1
Release: %{momorel}m%{?dist}
Source0: http://www.lysator.liu.se/~nisse/archive/%{name}-%{version}.tar.gz
NoSource: 0
Patch0:	nettle-2.7.1-remove-ecc-testsuite.patch
Patch1:	nettle-2.7.1-tmpalloc.patch
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.lysator.liu.se/~nisse/nettle/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gmp-devel
Requires(post): info
Requires(preun): info
Obsoletes: nettle-static < %{version}

%description
Nettle is a cryptographic library that is designed to fit easily
in more or less any context: In crypto toolkits for object-oriented
languages (C++, Python, Pike, ...), in applications like LSH or GNUPG,
or even in kernel space. 

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q
%patch0 -p1
%patch1 -p1

# Disable -ggdb3 which makes debugedit unhappy
sed s/ggdb3/g/ -i configure

%build
%configure --enable-shared --disable-openssl
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_libdir}/*.a

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir

%postun
/sbin/ldconfig

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --del %{_infodir}/%{name}.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING.LIB ChangeLog NEWS README TODO
%{_bindir}/nettle-hash
%{_bindir}/nettle-lfib-stream
%{_bindir}/pkcs1-conv
%{_bindir}/sexp-conv
%{_libdir}/libhogweed.so.*
%{_libdir}/libnettle.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/hogweed.pc
%{_libdir}/pkgconfig/%{name}.pc
%{_infodir}/nettle.info*

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.1-2m)
- import fedora patch

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4
- correct %%post and %%preun scripts

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Nishio Fuotshi <futoshi@momonga-linux.org>
- (2.1-1m)
- initial build
