%global momorel 4

Summary: LXDE Meta Package
Name: lxde
Version: 0.5.4
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2+
URL: http://www.lxde.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

Requires: lxde-common = %{version}
Requires: lxde-icon-theme
Requires: lxde-settings-daemon
Requires: lxmenu-data
Requires: lxpanel
Requires: lxsession
Requires: openbox
Requires: pcmanfm
Requires: gpicview
Requires: leafpad
Requires: lxappearance
Requires: lxinput
Requires: lxsession-edit
Requires: lxshortcut
Requires: lxtask
Requires: lxterminal
Requires: obconf
#Requires: slim
Requires: xarchiver
Requires: xscreensaver-base
Requires: lxlauncher
#Requires: lxmusic
Requires: obmenu
Requires: parcellite
Requires: lxrandr

%description
This is a meta package for LXDE.

%files

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-2m)
- full rebuild for mo7 release

* Sat Aug 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-1m)
- add Requires: lxrandr

* Thu Dec 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-3m)
- add Requires: lxde-icon-theme

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-1m)
- initial packaging
