%global momorel 5
%global pythonver 2.7
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name: tre
Summary: POSIX compatible regexp library with approximate matching
Version: 0.8.0
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
Source0: http://laurikari.net/tre/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-chicken.patch
Patch1: %{name}-tests.patch
URL: http://laurikari.net/tre/
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= %{pythonver}

%description
TRE is a lightweight, robust, and efficient POSIX compatible regexp
matching library with some exciting features such as approximate
matching.

%package devel
Requires: tre = %{version}-%{release}
Summary: Development files for use with the tre package
Group: Development/Libraries

%description devel
This package contains header files and static libraries for use when
building applications which use the TRE library.

%package -n python-%{name}
Group: System Environment/Libraries
Summary: Python bindings for the tre library

%description -n python-%{name}
This package contains the python bindings for the TRE library.

%package -n agrep
Summary: Approximate grep utility
Group: Applications/Text

%description -n agrep
The agrep tool is similar to the commonly used grep utility, but agrep
can be used to search for approximate matches.

The agrep tool searches text input for lines (or records separated by
strings matching arbitrary regexps) that contain an approximate, or
fuzzy, match to a specified regexp, and prints the matching lines.
Limits can be set on how many errors of each kind are allowed, or
only the best matching lines can be output.

Unlike other agrep implementations, TRE agrep allows full POSIX
regexps of any length, any number of errors, and non-uniform costs.

%prep
%setup -q
# hack to fix python bindings build
ln -s lib tre
%patch0 -p1 -b .chicken
%patch1 -p1 -b .tests

%build
%configure --disable-static --disable-rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%{__make} %{?_smp_mflags}
pushd python
%{__python} setup.py build
popd

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
pushd python
%{__python} setup.py install --skip-build --root %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
popd
rm -rf %{buildroot}%{_libdir}/*.la
%find_lang %{name}

%check
# revert rpath removal for building internal test programs
sed -i 's|^runpath_var=DIE_RPATH_DIE|runpath_var=LD_RUN_PATH|g' libtool
%{__make} check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(644,root,root,755)
%doc AUTHORS ChangeLog INSTALL LICENSE NEWS README THANKS TODO
%doc doc/*.html doc/*.css
%attr(755,root,root) %{_libdir}/libtre.so.*

%files devel
%defattr(644,root,root,755)
%{_libdir}/libtre.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files -n python-%{name}
%defattr(644,root,root,755)
%{python_sitearch}/tre.so
%{python_sitearch}/*.egg-info

%files -n agrep
%defattr(644,root,root,755)
%attr(755,root,root) %{_bindir}/agrep
%{_mandir}/man1/agrep.1*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0 based on Fedora 13 (0.8.0-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.5-4m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.5-3m)
- restrict python ver-rel for egginfo

* Mon Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.5-2m)
- enable egg-info

* Tue Apr 22 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.5-1m)
- import to Momonga (from Fedora)

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.7.5-5
- Autorebuild for GCC 4.3

* Tue Jan 01 2008 Dominik Mierzejewski <rpm@greysector.net> 0.7.5-4
- fix build in rawhide (include python egg-info file)

* Wed Oct 31 2007 Dominik Mierzejewski <rpm@greysector.net> 0.7.5-3
- include python bindings (bug #355241)
- fix chicken-and-egg problem when building python bindings

* Wed Aug 29 2007 Dominik Mierzejewski <rpm@greysector.net> 0.7.5-2
- rebuild for BuildID
- update license tag

* Mon Jan 29 2007 Dominik Mierzejewski <rpm@greysector.net> 0.7.5-1
- update to 0.7.5
- remove redundant BRs
- add %%check

* Thu Sep 14 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.4-6
- remove ExcludeArch, the bug is in crm114

* Tue Aug 29 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.4-5
- mass rebuild

* Fri Aug 04 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.4-4
- bump release to fix CVS tag

* Thu Aug 03 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.4-3
- per FE guidelines, ExcludeArch only those problematic arches

* Wed Aug 02 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.4-2
- fixed rpmlint warnings
- ExclusiveArch: %%{ix86} until amd64 crash is fixed and somebody
  tests ppc(32)

* Wed Jul 26 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.4-1
- 0.7.4
- disable evil rpath
- added necessary BRs
- license changed to LGPL

* Sun Feb 19 2006 Dominik Mierzejewski <rpm@greysector.net> 0.7.2-1
- \E bug patch
- FE compliance

* Sun Nov 21 2004 Ville Laurikari <vl@iki.fi>
- added agrep man page

* Sun Mar 21 2004 Ville Laurikari <vl@iki.fi>
- added %%doc doc

* Wed Feb 25 2004 Ville Laurikari <vl@iki.fi>
- removed the .la file from devel package

* Mon Dec 22 2003 Ville Laurikari <vl@iki.fi>
- added %%post/%%postun ldconfig scriplets.

* Fri Oct 03 2003 Ville Laurikari <vl@iki.fi>
- included in the TRE source tree as `tre.spec.in'.

* Tue Sep 30 2003 Matthew Berg <mberg@synacor.com>
- tagged release 1
- initial build
