%global momorel 1

# Generated from flexmock-0.8.7.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname flexmock
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Simple and Flexible Mock Objects for Testing
Name: rubygem-%{gemname}
Version: 0.9.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://flexmock.rubyforge.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

Provides: ruby-flexmock rubygem-flexmock-doc
Obsoletes: ruby-flexmock rubygem-flexmock-doc

%description
FlexMock is a extremely simple mock object class compatible
with the Test::Unit framework.  Although the FlexMock's
interface is simple, it is very flexible.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/CHANGES
%doc %{geminstdir}/doc/GoogleExample.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.4.0.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.4.1.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.4.2.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.4.3.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.5.0.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.5.1.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.6.0.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.6.1.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.6.2.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.6.3.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.6.4.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.7.0.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.7.1.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.8.0.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.8.2.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.8.3.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.8.4.rdoc
%doc %{geminstdir}/doc/releases/flexmock-0.8.5.rdoc
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.11-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.11-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.11-1m)
- update 0.8.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-1m)
- update 0.8.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 13 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-1m)
- import from Fedora to Momonga

* Thu Jul 30 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.8.6-1
- Switch to gem, repackage

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Nov 08 2007 Paul Nasrat <pauln@truemesh.com> - 0.7.1-3
- Fix repoid

* Wed Nov 07 2007 Paul Nasrat <pauln@truemesh.com> - 0.7.1-2
- Spec cleanups in response to review
- Fix license
- strip out shebangs

* Sun Sep 09 2007 Paul Nasrat <pauln@truemesh.com> - 0.7.1-1
- Initial vesion


