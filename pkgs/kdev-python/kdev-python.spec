%global momorel 1
%global unstable 0
%global kdever 4.11.4
%global kdelibsrel 1m
%global kdebaserel 1m
%global kdesdkrel 1m
%global kdevplatformver 1.6.0
%global kdevplatformrel 1m
%global kdevelopver 1.6.0
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.6.0
%if 0%{?unstable}
%global sourcedir unstable/kdevelop/%{ftpdirver}/src
%else
%global sourcedir stable/kdevelop/%{ftpdirver}/src
%endif

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name: kdev-python
Summary: Python plugin for kdevelop
Version: 1.6.0
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: GPLv2+
URL: http://www.kdevelop.org
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: kdevplatform-devel >= %{kdevplatformver}
BuildRequires: kdevelop-pg-qt-devel
BuildRequires: perl
BuildRequires: phonon-devel
BuildRequires: python-devel
Requires: kdevelop >= %{kdevelopver}

%description
This plugin adds PHP language support (including classview and code-completion)
to KDevelop.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

## not smp safe ?
make -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc DESIGN INSTALL README TODO
%{_kde4_libdir}/*.so*
%{_kde4_libdir}/kde4/*.so
%{_kde4_appsdir}/kdevappwizard/templates/*
%{_kde4_appsdir}/kdevpythonsupport
%{_kde4_datadir}/config/kdev_python_docfiles.knsrc
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/kdevpython.mo

%changelog
* Tue Dec 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Thu Oct 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Sat Apr 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-2m)
- rebuild against new KDevelop-1.4.90

* Mon Nov 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- initial build for Momonga Linux
