%global momorel 3

Summary: Helps troubleshoot SELinux problems
Name: setroubleshoot
Version: 3.1.9
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: https://fedorahosted.org/setroubleshoot
Source0: %{name}-%{version}.tar.gz
Source2: setroubleshoot.logrotate
Patch0: setroubleshoot-3.0.38-momonga.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl-XML-Parser
BuildRequires: libcap-ng-devel
BuildRequires: intltool gettext python >= 2.7
BuildRequires: desktop-file-utils dbus-devel gtk2-devel libnotify-devel >= 0.7.2 audit-libs-devel libselinux-devel polkit-devel
Requires: %{name}-server = %{version}-%{release} 
Requires: pygtk2-libglade >= 2.9.2
Requires: gtk2
Requires: dbus
Requires: dbus-python
Requires: pygobject2
Requires: report-gtk
Requires: polkit
Requires: yum
Requires(post): desktop-file-utils
Requires(post): dbus
Requires(postun): desktop-file-utils
Requires(postun): dbus
Requires: notify-python

BuildRequires: xdg-utils
Requires: xdg-utils

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

%global pkgpythondir  %{python_sitelib}/%{name}
%define pkgdocdir     %{_datadir}/doc/%{name}-%{version}
%define pkgguidir     %{_datadir}/%{name}/gui
%define pkgdatadir    %{_datadir}/%{name}
%define pkglibexecdir %{_prefix}/libexec/%{name}
%define pkgvardatadir %{_localstatedir}/lib/%{name}
Requires: yum
Requires: gnome-python2-gnomekeyring
Requires(post): desktop-file-utils
Requires(post): dbus
Requires(postun): desktop-file-utils
Requires(postun): dbus
Requires: notify-python

BuildRequires: xdg-utils
Requires: xdg-utils

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

%global pkgpythondir  %{python_sitelib}/%{name}
%define pkgdocdir     %{_datadir}/doc/%{name}-%{version}
%define pkgguidir     %{_datadir}/%{name}/gui
%define pkgdatadir    %{_datadir}/%{name}
%define pkglibexecdir %{_prefix}/libexec/%{name}
%define pkgvardatadir %{_localstatedir}/lib/%{name}
%define pkgrundir     %{_localstatedir}/run/%{name}
%define pkgconfigdir  %{_sysconfdir}/%{name}
%define pkglogdir     %{_localstatedir}/log/%{name}
%global pkgdatabase   %{pkgvardatadir}/audit_listener_database.xml

%description
setroubleshoot gui. Application that allows you to view setroubleshoot-server 
messages.
Provides tools to help diagnose SELinux problems. When AVC messages
are generated an alert can be generated that will give information
about the problem and help track its resolution. Alerts can be configured
to user preference. The same tools can be run on existing log files.

%files
%defattr(-,root,root,-)
%{pkgguidir}
%config(noreplace) %{_sysconfdir}/xdg/autostart/*
%{_datadir}/applications/*.desktop
%{_datadir}/dbus-1/services/sealert.service
%{_datadir}/icons/hicolor/*/*/*
%dir %attr(0755,root,root) %{pkgpythondir}
%{pkgpythondir}/browser.py*
%{pkgpythondir}/gui_utils.py*
%{_bindir}/seapplet

%post
touch --no-create %{_datadir}/icons/hicolor || :
dbus-send --system /com/redhat/setroubleshootd com.redhat.SEtroubleshootdIface.restart string:'rpm install' >/dev/null 2>&1 || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%posttrans
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :


%prep
%setup -q
%patch0 -p1 -b .momonga

%build
%configure
make


%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
%{__install} -D -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
desktop-file-install --vendor="" --dir=%{buildroot}%{_datadir}/applications %{buildroot}/%{_datadir}/applications/%{name}.desktop
touch %{buildroot}%{pkgdatabase}
touch %{buildroot}%{pkgvardatadir}/email_alert_recipients
%find_lang %{name}

# sealertauto.desktop breaks KDE4's tray (kdebase-workspace-4.0.3)
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_sysconfdir}/xdg/autostart \
  --remove-key X-KDE-autostart-after \
  --add-only-show-in XFCE \
  --add-only-show-in GNOME \
  %{buildroot}%{_sysconfdir}/xdg/autostart/sealertauto.desktop

%package server
Summary: SELinux troubleshoot server
Group: Applications/System

Requires: audit >= 1.2.6-3
Requires: audit-libs-python >= 1.2.6-3
Requires: pygobject >= 2
Requires: dbus libnotify
Requires: dbus-python
Requires: libxml2-python
Requires: setools-libs-python
Requires: rpm-python
Requires: gtk2 libnotify
Requires: libselinux-python  >= 1.30.15-1
Requires: policycoreutils-python 
BuildRequires: intltool gettext python
BuildRequires: setools-devel >= 3.3.6-3
BuildRequires: python-devel
Requires: python-slip-dbus

Requires(post):   chkconfig
Requires(post):   initscripts
Requires(preun):  chkconfig
Requires(preun):  initscripts

%description server
Provides tools to help diagnose SELinux problems. When AVC messages
are generated an alert can be generated that will give information
about the problem and help track its resolution. Alerts can be configured
to user preference. The same tools can be run on existing log files.

%post server
/sbin/service auditd reload >/dev/null 2>&1 || :

%postun server
if [ $1 = 0 ]; then
   /sbin/service auditd reload >/dev/null 2>&1 || :
fi

%triggerun server -- %{name}-server < 2.1.1
/sbin/service %{name} stop >/dev/null 2>&1 || :
chkconfig --del %{name}  || :

%clean 
rm -rf %{buildroot}

%files server -f %{name}.lang 
%defattr(-,root,root,-)
%{_bindir}/sealert
%{_sbindir}/sedispatch
%{_sbindir}/setroubleshootd
%{python_sitelib}/sesearch*.egg-info
%{python_sitelib}/setroubleshoot*.egg-info
%dir %attr(0755,root,root) %{pkgconfigdir}
%dir %attr(0755,root,root) %{pkgpythondir}
%dir %attr(0755,root,root) %{pkgpythondir}/sesearch
%{pkgpythondir}/Plugin.py*
%{pkgpythondir}/__init__.py*
%{pkgpythondir}/access_control.py*
%{pkgpythondir}/analyze.py*
%{pkgpythondir}/audit_data.py*
%{pkgpythondir}/avc_audit.py*
%{pkgpythondir}/config.py*
%{pkgpythondir}/email_alert.py*
%{pkgpythondir}/errcode.py*
%{pkgpythondir}/html_util.py*
%{pkgpythondir}/rpc.py*
%{pkgpythondir}/serverconnection.py*
%{pkgpythondir}/rpc_interfaces.py*
%{pkgpythondir}/server.py*
%{pkgpythondir}/signature.py*
%{pkgpythondir}/util.py*
%{pkgpythondir}/uuid.py*
%{pkgpythondir}/xml_serialize.py*
%{pkgpythondir}/sesearch/__init__.py*
%attr(0755,root,root) %{pkgpythondir}/sesearch/_sesearch.so
%attr(0755,root,root) %{pkgpythondir}/default_encoding_utf8.so
%dir %{pkgdatadir}
%{pkgdatadir}/SetroubleshootFixit.py*
%{pkgdatadir}/updater.py*
%config(noreplace) %{pkgconfigdir}/%{name}.conf
%dir %{pkglogdir}
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/org.fedoraproject.Setroubleshootd.conf
%dir %{pkgrundir}
%dir %{pkgvardatadir}
%ghost %attr(0600,root,root) %{pkgdatabase}
%ghost %attr(0644,root,root) %{pkgvardatadir}/email_alert_recipients
%{_mandir}/man8/sealert.8.*
%{_mandir}/man8/sedispatch.8.*
%{_mandir}/man8/setroubleshootd.8.*
%config /etc/audisp/plugins.d/sedispatch.conf
%{_datadir}/dbus-1/system-services/org.fedoraproject.Setroubleshootd.service
%{_datadir}/polkit-1/actions/org.fedoraproject.setroubleshootfixit.policy
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/org.fedoraproject.SetroubleshootFixit.conf
%{_datadir}/dbus-1/system-services/org.fedoraproject.SetroubleshootFixit.service

%package doc
Summary: Setroubleshoot documentation
Group: System Environment/Base
Requires(pre): setroubleshoot = %{version}-%{release}

%description doc
Setroubleshoot documentation package

%files doc
%doc %{pkgdocdir}

%changelog
* Wed Aug  1 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.9-3m)
- good-bye gnome-python2-gtkhtml2

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.9-2m)
- fix %%files to avoid conflicting
- keep Requires: gnome-python2-gtkhtml2, please fix it

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.9-1m)
- update 3.1.9

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.41-1m)
- update 3.0.41

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.38-1m)
- update 3.0.38

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.91-8m)
- rebuild against python-2.7

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.91-7m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.91-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.91-5m)
- rebuild for new GCC 4.5

* Mon Sep  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.91-4m)
- set bug_report_url to Momonga BTS

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.91-3m)
- full rebuild for mo7 release

* Tue Aug 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.91-2m)
- fix can't run setroubleshootd 
-- no read /etc/sysconfig/i18n 

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.91-1m)
- update to 2.2.91

* Thu Jul 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.88-1m)
- update to 2.2.88 based on Fedora 13 (2.2.88-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.12-1m)
- sync with Fedora 11 (2.1.12-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.6-4m)
- rebuild against python-2.6.1-1m

* Wed Apr 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.6-3m)
- remove sealertauto.desktop from plasma-workspace again
- sealertauto.desktop breaks KDE4's tray
- kdebase-workspace-4.0.3

* Wed Apr 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.6-2m)
- modify %%files to avoid conflicting
- use %%{_initscriptdir} instead of %%{_initrddir}
- modify Requires

* Wed Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.6-1m)
- version up 2.0.6
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-5m)
- rebuild against gcc43

* Mon Mar 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.4-4m)
- remove sealertauto.desktop from plasma-workspace for the moment
- plasma was crashed by %%{_sysconfdir}/xdg/autostart/sealertauto.desktop
- kdebase-workspace-4.0.2

* Tue Jul 10 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.4-3m)
- fix missing setroubleshoot_icon.png problem at initial build

* Sat Jul  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.4-2m)
- release %%{_datadir}/icons/hicolor/96x96/apps, it's already provided by hicolor-icon-theme

* Fri Jul  6 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-1m)
- import from fc to Momonga
- comment out temporary "BuildArch: noarch" for configure error

* Mon May  7 2007 John Dennis <jdennis@redhat.com> - 1.9.4-2
	- Resolves bug# 233760, fix autogen problem resulting in /usr/local prefix

* Mon Mar 19 2007 Dan Walsh <dwalsh@redhat.com> - 1.9.4-1
	- Remove disable_trans boolean
	- Check for paths in filesystem before suggesting chcon -R 
	- Remove default to listen on local ports

* Mon Mar  5 2007 John Dennis <jdennis@redhat.com> - 1.9.3-1
	- install icon in /usr/share/icons, refer to icon by name using standard API
	- Fix performance problems in setroubleshoot browser log file scanning
	- Significant rewrite of data/view management code in setroubleshoot
	  browser. data and view now cleanly separated, can easily switch
	  between data views while maintaining selections, view state, with
	  proper update of status information in status area
	- Resolves Bug# 227806: right click context menu resets selection
	- Logfile scans now operate in independent thread, proper asynchronous
	  updates of browser during scan, browser used to appear to hang
	- Resolves Bug# 224340: Rewrite Menu/Toobar/Popup to use UIManger instead of glade
	- Add toobar support
	- Implement GUI to edit email recipient list in setroubleshoot browser
	- Added user help to setroubleshoot browser
	- Related Bug# 224343: Fix setroubleshoot browser to respond to desktop theme changes
	- improve traceback error reporting in sealert
	- rewrite AboutDialog, replacing glade version
	- Resolves bug #229849  Bug# 230115, Relates bug #221850: fix uuid code to resolve
	  '_uuid_generate_random' is not defined error
	
* Thu Feb  22 2007 Dan Walsh <dwalsh@redhat.com> - 1.9.2-1
	- Suck in AuditMsg since audit libs are dropping support

* Fri Feb  16 2007 Dan Walsh <dwalsh@redhat.com> - 1.9.1-1
	- Split into server and gui packages

* Fri Feb  16 2007 Dan Walsh <dwalsh@redhat.com> - 1.8.19-1
	- Remove use of ctypes in uuid, which is causing bad avc messages

* Fri Feb  9 2007 Dan Walsh <dwalsh@redhat.com> - 1.8.18-1
	- Remove avc from Plugin.py

* Wed Feb  7 2007 Dan Walsh <dwalsh@redhat.com> - 1.8.17-1
	- Remove tempfile handling in util.py.  Causes lots of avc's and is not used

* Fri Feb  2 2007 John Dennis <jdennis@redhat.com> - 1.8.16-1
    [John Dennis <jdennis@redhat.com>]
	- Resolves: Bug# 224343 sealert's "Aditional Info:" text should be in white box
	- Resolves: Bug# 224336 sealert should have GtkRadioButtons in menu View
	- Related: bug #224351
	  Rewrite parts of logging support to better support changing output
	  categories, output destinations. Now -v -V verbose works in sealert.
	- Resolves bug# 225161, granted AVC's incorrectly identified as a denial
	- add alert count to status bar
	- add "Help" command to Help menu, opens web browser on wiki User FAQ
    [Dan Walsh  <dwalsh@redhat.com>]
	- Make setroubleshoot.logrotate correctly

* Fri Jan 12 2007 Dan Walsh <dwalsh@redhat.com> - 1.8.15-1
	- Update po
	- Additional Plugins
	- Cleanup Plugins

* Thu Jan 11 2007 John Dennis <jdennis@redhat.com> - 1.8.14-1
	- Resolves: bug# 221850
	  plugin module loading was failing in python 2.5 with the message
	  "SystemError: Parent module 'plugins' not loaded". This is due to a
	  change in behavior between python 2.4 and 2.5, in python 2.4 the lack
	  of a parent module was silently ignored. The fix is to load
	  plugins.__init__ first.
	
* Sat Jan  6 2007 John Dennis <jdennis@redhat.com> - 1.8.13-1
	- update translations

	- change SETroubleshootDatabase so it is optional if it's backed
	  by a file, this fixes the problem of us littering temporary files
	  when scanning logfiles which does not require persistence.

	- disable the view logfile menu item if no logfile has been opened

	- fix redundant log messages for case where there is no log file and
	  the console flag is set. When there is no log file the logging
	  module opens a console stream, thus the console stream produced
	  by the console flag was redundant.

	- add username and password command line arguments
	  rework startup logic so that all command line args are processed
	  before we do any real work
	
	- rework the email preferences so that each email address can
	  have a filter type associated with it.

	  add a new filter_type "Ignore After First Alert" which filters
	  after the first alert has been delivered

	- add UI for setting the email addresses alerts are sent to.
	  Add menu item to edit email list, add email list dialog.
	  Remove 'recipient' config file entry, now list is stored
	  in seperate file. Add rpc to query and set the email list,
	  the GUI calls this to get the current list from the server
	  and set it in the server, it is the server which reads and 
	  writes the file. Add 'enable' flag to each email entry.
	  Modify how the server iterates over the email list when it
	  receives an alert. When marking an alert as having been sent
	  the username is the email address but with 'email:' prepended so
	  as not to collide with non-email filtering options for the same user.

* Wed Dec 20 2006 John Dennis <jdennis@redhat.com> - 1.8.12-1
	- remove obsolte requires for python element tree 

* Mon Dec 18 2006 John Dennis <jdennis@redhat.com> - 1.8.11-1
	- Resolves: #216575, more translations
	- Replace delete and expunge menu labels with something more intuitive
	- add ability for browser to be restarted with identical window
	  position and state
	- add pkg version and protocol version to logon handshake, test for
	  compatibility between clint and server, prompt for restart
	- add non-modal restart dialog
	- add dialog to display traceback if sealert faults with an uncaught
	  exception, try to limit invisible errors
	- fix return args on rpc method
	- add instance id to server

* Wed Dec  9 2006 Dan Walsh <dwalsh@redhat.com> - 1.8.10-1
	- Improve quality of plugins
	- Make matching easier
	- Resolves: #216575

* Wed Dec  9 2006 Dan Walsh <dwalsh@redhat.com> - 1.8.9-1
	- Additional Translations
	- Resolves: #216575

* Sat Dec  8 2006 Dan Walsh <dwalsh@redhat.com> - 1.8.8-1
	- Additional Translations
	- Change sealert to be able to run without X-Windows
	- Resolves: #216575

* Fri Dec  8 2006 Dan Walsh <dwalsh@redhat.com> - 1.8.7-1
	- Additional Translations
	- Change avc_audit.py to allow it to analyze /var/log/messages

* Mon Dec  4 2006 John Dennis <jdennis@redhat.com> - 1.8.6-1
	- Resolves: bug# 218150,
	  "If view is set to "hide delete" you cannot filter new entries"
	  Actually, the bug was toggle cell renderer was connected to the
	  base model instead of the model attached to the view, the sort
	  model, this meant the toggle was occuring on the wrong row if
	  the view was sorted differently than the base model.

* Fri Dec  1 2006 John Dennis <jdennis@redhat.com> - 1.8.5-1
	- fix bug, "could not convert path to a GtkTreePath" when database 
	  is initially empty, caused by last_selected_row == None

* Thu Nov 30 2006 John Dennis <jdennis@redhat.com> - 1.8.3-1
	- Resolves: bug# 217961, sealert needs pygtk2-libglade
	- more i18n translations
	- Resolves: bug# 217710, date representation did not respect locale,
	  at the same time remove old date formatting code, now cruft since
	  we can't use it because it was specific to US English.
	- fix how selections are handled when rows are expunged.
	- add Copy to Edit menu, for copying selection from detail pane,
	  unfortunately gtkhtml2 widget does not preserve line breaks between
	  table rows.

* Tue Nov 28 2006 John Dennis <jdennis@redhat.com> - 1.8.1-1
	- Resolves: bug# 216936, bug# 215290, add 'Copy Alert' edit menu item
	- clean up menu items, add tooltips
	- fix printing so it will work with multiple alerts, force font to
	  monospace 10pt, display error dialog if printing fails.
	- Resolves: bug# 216908, platform and raw audit messages were not wrapped
	  to fit on page.
	- Related: bug# 216575, update i18n po files
	- Resolves: bug# 216941, set default folder for save operation, also set
	  default filename
	- Resolves: #bug 216327 add menu items "toggle hide deleted", "select none". Add model
	  filter to control visibility of alerts
	- Resolves: bug# 214218, sealert with no command line
	  arguments induces startup as dbus service, this had been a
	  regression.
	- Resolves: bug# 216327, rework how deletes are performed in browser. Delete
	  now marks each seleted siginfo with a delete flag, expunge
	  permanently deletes siginfo's marked for deletion, also add undelete
	  command, removed delete confirmation dialog. Modify how text
	  attributes in cell renderer are computed to allow for
	  strike-throughs of alerts marked for deletion.
	- multiple alerts can now be selected, add select all command, 

* Tue Nov 23 2006 Dan Walsh <dwalsh@redhat.com> - 1.7.1-1
	- New Icon and translations

* Tue Nov 21 2006 Dan Walsh <dwalsh@redhat.com> - 1.7-1
    [John Dennis <jdennis@redhat.com>]
	- Add command line utilities
	- logfile scanning finally seems to work connected to browser
	- Additional Information section of report now includes line
	  number information (if alert was generated from logfile)
	- replace database update_callback() with notify interface, a more
	  generic solution more easily shared between components
	- object implementing rpc method is now explicitly attached via
	  connect_rpc_interface() instead of walking the MRO chain with
	  magic exclusions. explicitly connecting is more flexible and
	  robust (no getting the wrong object by mistake)
	- fix handling of return args in local rpc case
	- fix signal connections between audit and logfile
	- split databae and database_properties for audit and logfile
	- fix initial connection state
	- fix lookup_local_id

* Wed Nov 8 2006 Dan Walsh <dwalsh@redhat.com> - 1.5-1
	- Speed up startup of service

* Tue Nov 6 2006 Dan Walsh <dwalsh@redhat.com> - 1.4-1
	- Many fixes
	- Changed the api

* Tue Oct 24 2006 Dan Walsh <dwalsh@redhat.com> - 1.3-1
	- Speed enhancments
    [John Dennis <jdennis@redhat.com>]
	- log file parsing now approx 4 times faster
	- greatly enhance the statistics reporting capability in attempt
	  to diagnose slow log file parsing performance
	- make gathering of environmenatal information optional,
	  environment information is only relevant at the time the
	  alert fires, not in a post processing scenario
	- clean up several places where environmental information was
	  assumed and/or was always gathered, or gathered in the wrong place.

* Tue Oct 17 2006 Dan Walsh <dwalsh@redhat.com> - 1.2-1
	- Fix signature for PORT_NUMBER src command

* Tue Oct 3 2006 Dan Walsh <dwalsh@redhat.com> - 1.1-1
	- Additional Plugins for port_t and device_t and mislabled files.

* Tue Oct 3 2006 Dan Walsh <dwalsh@redhat.com> - 1.0-1
	- Release of first version
	- Fix icon
    [John Dennis <jdennis@redhat.com>]
	- Memory leak fixes
	- Substitution fixes
	- File names in hex fixes

* Fri Sep 29 2006 Dan Walsh <dwalsh@redhat.com> - 0.48-1
	- Sealert only notify dropped connection once
	- setroubleshoot shutdown cleanly
    [John Dennis <jdennis@redhat.com>]
	- Gui cleanups

* Wed Sep 27 2006 Dan Walsh <dwalsh@redhat.com> - 0.47-1
	- Change close key binding to ctrl-w

* Tue Sep 26 2006 Dan Walsh <dwalsh@redhat.com> - 0.46-1
	- Add new plugins cvs_data, rsync_data, xen_image, swapfile, samba_share
    [John Dennis <jdennis@redhat.com>]
	- clear the GUI of old data before loading new data,
	  fix the code used to display the filter icon in the filter column

* Tue Sep 26 2006 Dan Walsh <dwalsh@redhat.com> - 0.45-1
    [John Dennis <jdennis@redhat.com>]
	- Major rewrite of the client/server RPC code,

* Sat Sep 16 2006 Dan Walsh <dwalsh@redhat.com> - 0.44-1
	- Fix Affected RPMS handling

* Fri Sep 15 2006 Dan Walsh <dwalsh@redhat.com> - 0.43-1
	- Fix mail handling
	- fix bugs related to recording per user per signature filtering
    [John Dennis <jdennis@redhat.com>]
	- fix bugs related to recording per user per signature filtering
    [Karl MacMillan <kmacmill@redhat.com>]
	- Add signal handling to client and server.
	- Fix minor plugin bugs.

* Thu Sep 7 2006 Dan Walsh <dwalsh@redhat.com> - 0.42-1
    [Karl MacMillan <kmacmill@redhat.com>]
	- Add rpm information for target.
	- Add hostname and uname to signature info
	- Add display of the full AVC
	- Add display of the analysis id
	- Change html generation to be separated out and us elemmenttree
    [John Dennis <jdennis@redhat.com>]
	- add CommunicationChannel class to encapsulate data transfer
	  operations, in particular to provide an object threads can lock
	  during data transfer.
	- checkpoint the logfile scanning code, somewhat working

* Fri Aug 31 2006 Dan Walsh <dwalsh@redhat.com> - 0.41-1
	- Fix printing

* Fri Aug 31 2006 Dan Walsh <dwalsh@redhat.com> - 0.40-1
	- Fix notification window problems.  Now dissappears and does not regenerate if
it has already been seen

* Fri Aug 31 2006 Dan Walsh <dwalsh@redhat.com> - 0.39-1
	- Add Icon
    [John Dennis <jdennis@redhat.com>]
	- dispatcher.py: rework how audit messages injected into the
	  system and processed. Much of this work was in support of log file
	  scanning which should be coupled to the exact same processing code
	  as audit messages arriving from the audit socket. In essence log
	  file scanning synthesizes an audit message and we inject it into
	  the system the same way socket messages are injected. This was
	  also an excellent moment correctly handle out of order audit
	  messages, something we were not able to handle previously. This
	  may have been contributing to splitting what should have been a
	  single alert into two or more separate alerts because we didn't
	  recongize the incoming audit events as a single event. Correctly
	  assembling out of order messages introduced a fair amount of extra
	  complexity as we now maintain a cache of recent audit events, this
	  is fully documented in dispatcher.py
	- Turn notifications back on by default.
    [Karl MacMillan <kmacmill@redhat.com>]
	- Separated out HTML rendering and made it easier to translate.

* Fri Aug 30 2006 Dan Walsh <dwalsh@redhat.com> - 0.38-1
    [Dan  Walsh]
	- Hook up the rest of the menu bars on browser window
	- Add public_content.py plugin
    [John Dennis <jdennis@redhat.com>]
	- add delete_signatures() method to AlertClient class
	- start using the AppBar in the browser.
	- "open logfile" now connected all the way from browser menu
	  to server rpc, still needs implementation, but "plumbing" is working.
	- fixes for the date/time dialog
	- remove install of setroubleshoot.glade, we now only use
	  setroubleshoot_browser.glade
	- some fixed to DateTimeDialog

* Fri Aug 25 2006 Dan Walsh <dwalsh@redhat.com> - 0.37-1
	- Add back in the status icon

* Thu Aug 24 2006 John Dennis <jdennis@redhat.com> - 0.36-1
	- change dbclear trigger to 0.35

* Thu Aug 24 2006 John Dennis <jdennis@redhat.com> - 0.35-1
	- add sorting on category column and seen column in browser,
	  fix reference to my_draw() in print function.

	- make browser window hidden by default so it does not flash
	  when it's first realized, connect to the "realize" signal to
	  initially position the vpane, add signal handlers to track
	  when the browser is visible, the presentation of the status
	  icon now checks if the browser is visible, the status icon is
	  not presented if the browser is already displayed.

* Thu Aug 22 2006 Dan Walsh <dwalsh@redhat.com> - 0.34-1
	- Standardize on the browser. remove alert window
    [John Dennis <jdennis@redhat.com>]
	- remove all vestiges of popup alert, now browser is the only
	  UI game in town
	- restore the automatic updating of the browser window which had
	  been a regression, the AlertClient class now emits signals which
	  the GUI classes can connect to receive signals from the fault server,
	  also fix the "mark seen" regression
	- browser.py: restore mark_seen timeout

* Tue Aug 22 2006 Dan Walsh <dwalsh@redhat.com> - 0.33-1
	- Spell check plugins
	- fix dbus instantiation

* Tue Aug 22 2006 Dan Walsh <dwalsh@redhat.com> - 0.32-1
	- Add avc_syslog to syslog translated avc message
	- Fix submitbug button
    [John Dennis <jdennis@redhat.com>]
	- fix signature inflation, all data attached to a signature is now
	  encapsulated in a SEFaultSignatureInfo (siginfo) class. The GUI no
	  longer reaches into a signature looking for information, it looks
	  in the siginfo. The Plugin class now defines the method
	  get_signature() which report() calls to obtain the signature. The
	  default signature provided by the Plugin class includes the
	  analysisID, an AVC with just the src & target contexts, and the
	  object_path. All data accesses and parameters which had been "sig
	  and solution" are now done via the unified siginfo class. There is
	  still a bit more work to be done on this but this represents a
	  reasonble point to checkpoint the code in CVS.

* Tue Aug 22 2006 Dan Walsh <dwalsh@redhat.com> - 0.31-1
	- Fix desktop

* Tue Aug 22 2006 John Dennis <jdennis@redhat.com> - 0.30-1
	- fix bug #203479, missing requires of audit-libs-python
	- add support to sealert to listen on a dbus session signal to display
	  the gui. This is needed for when the status icon is not visible and
	  the user wants to see the UI. There is now a seperate program
	  setroubleshoot_launch_gui which emits the signal.

* Tue Aug 22 2006 Dan Walsh <dwalsh@redhat.com> - 0.29-1
	- Add Requires: audit-libs-python
	- Add translations

* Mon Aug 21 2006 Dan Walsh <dwalsh@redhat.com> - 0.28-1
	- Fix allow_execmem.py file
	- Add translations

* Mon Aug 21 2006 John Dennis <jdennis@redhat.com> - 0.27-1
	- load_plugins() now catches exceptions when a plugin won't load,
	  reports the traceback in the log file, and continues with the next
	  plugin. Previously a bad plugin caused the entire plugin loading
	  to abort and no plugins were loaded.
	- Add "daemon_name" to automake variables, change pid file to match
	- turn off "noreplace" on config file till things settle down a bit
	- browser.py now validates data, also test for missing column data in the
	  cell_data function to avoid exceptions.
	- add stub for analyzie_logfile() rpc call
	- turn off balloon notifications by default in config file,
	  libnotify is just plain busted at this point :-(
	- only the setroubleshootd daemon creates it's log file
	  under /var/log now, the user app's do it in /tmp, change file
	  permissions on /var/log/setroubleshoot back to 0644.
	- sealert now looks up the username rather than hardcoding it to "foo"
	- CamelCase to lowercase_underscore clean up

* Mon Aug 21 2006 Dan Walsh <dwalsh@redhat.com> - 0.26-1
	- Zero out datbase.xml for updated browser

* Mon Aug 21 2006 Dan Walsh <dwalsh@redhat.com> - 0.25-1
	- Fix 64 bit issue that caused runaway problem

* Sun Aug 20 2006 Dan Walsh <dwalsh@redhat.com> - 0.24-1
	- add missing runcmd

* Thu Aug 17 2006 John Dennis <jdennis@redhat.com> - 0.23-1

	- fix for bug #202206, require correct version of audit,
	  fixes for audit connection.

* Thu Aug 10 2006 Dan Walsh <dwalsh@redhat.com> - 0.20-1
	- add html support
	- remove setroubleshoot_dispatcher

* Tue Aug 8 2006 Dan Walsh <dwalsh@redhat.com> - 0.19-1

2006-08-08  Dan Walsh <dwalsh@redhat.com>
	- Fix up handling of mls ranges in context
	- Cleanup some pychecker errors

2006-08-07  John Dennis <jdennis@redhat.com>
	- add first seen, last seen, and report count to alert detail view
	- make the seen icon work, if the alert has been displayed more
	  than N seconds, mark the alert as having been seen by the user
	  and update the icon is the list view
	- change the schema for the xml data; the database now has a version,
	  there is a local id attached to each signature, the filter list in
	  the siginfo was replaced by a list of per user data, the per user
	  data now contains the filter, seen_flag. Modify all the code which
	  was operating on the filter information to use the new model.
	- fix the xml serialization so that booleans can be used as a basic
	  type and also so that non-string types can be used in element
	  attributes (e.g. int, bool) and the serialization code will
	  automatically convert between python types and strings.

* Mon Aug 7 2006 Dan Walsh <dwalsh@redhat.com> - 0.18-1
	- Add dispatcher.py

* Sat Aug 5 2006 Dan Walsh <dwalsh@redhat.com> - 0.17-1
    [John Dennis <jdennis@redhat.com>]
	- clean up and rework the timestamp code in util.py so that
	  time zones are handled properly, there were a number of bugs.
	  Hopefully it's correct now because timezone handling is a pain.
	- change the time format in the browser so all times are displayed
	  identically, the friendly time relative format was hard to compare.
	- modify the plugin 'make install' to delete all existing plugin's
	  prior to installing the new ones
	- add popup menu to status icon to choose between browser and
	  alert GUI (not fully connected yet). Several bug fixes related
	  to changing the filter_type from a string to an int.
	- add filter selection to bottom pane, change filter_type from
	  string to integer constant. Enhance how columns are handled.
	  Get init_combo_box to work. Remove unused RPM and Bugzilla
	  fields from bottom pane. Modify the default size of the browser
	  window. Fix missing import in util.py.
	- add ability in broswer to sort on columns, initially the report
	  count column and the last seen date column. The date column now
	  stores a TimeStamp object instead of a string. Add new method
	  to TimeStamp to return a friendly string relative to the current
	  time. The date column in the browser now has a cell data function
	  which invokes the friendly format method of the TimeStamp object.
	- add ability fo serialize to/from xml for classes which can
	  inititialized from strings and serialized as strings (e.g. numbers,
	  TimeStamps, etc.)
	- add count of how many times a signature is reported, the date
	  when first and last reported, add columns for report count and
	  last date count to browser.
	- checkpoint browser code, list pane and detail pane now working.
	- add initial support for browser applet, move some functions which
	  kept getting reused to util.py
	- add reporting of environment to email alert (email alerts still
	  need work)
    [Dan  Walsh <dwalsh@redhat.com>]
	- Fix disable_trans.py set_boolean call
	- Complete all boolean plugins except disable
	- Change interface to use audit unix domain socket

* Mon Jul 28 2006 Dan Walsh <dwalsh@redhat.com> - 0.16-1
    [John Dennis <jdennis@redhat.com>]
	- modify SetFilter in server to return errors instead of
	  throwing an exception. Default the filter list on each alert display.
	- minor tweaks to alert queue handling
	- fix analyze() parameter list in ftp_is_daemon.py plugin
	- sealert now responds to pending alerts more correctly, it shows
	  how many pending alerts are in the queue, if you filter the pending
	  alert status is updated, the next alert button will advance you
	  to the next alert in the queue
	- simplify major pieces of sealert by coalescing common code
	  into subroutines.
    [Dan  Walsh <dwalsh@redhat.com>]
	- Complete all boolean plugins except disable
	- Make Close button work.
	- Make setroubleshoot_dispatcher exit if it gets an avc about itself

* Mon Jul 26 2006 Dan Walsh <dwalsh@redhat.com> - 0.15-1
    [Karl MacMillan <kmacmill@redhat.com>]
	- Add generic templating mechanism to Plugin
	- Ported all plugins to use templating mechanism

* Sat Jul 22 2006 Dan Walsh <dwalsh@redhat.com> - 0.13-1
	- Fixes to plugins
	- Fixes to dispatcher

* Fri Jul 21 2006 Dan Walsh <dwalsh@redhat.com> - 0.12-1
	- Fix problem in dispatcher

* Fri Jul 21 2006 John Dennis <jdennis@redhat.com> - 0.11-1
	- add email alerts
	- stop the status icon from blinking, add notification balloon.

* Fri Jul 21 2006 Dan Walsh <dwalsh@redhat.com> - 0.10-1
	- Fix startup order for setrobleshoot
	- Fix Plugins

* Tue Jul 20 2006 Dan Walsh <dwalsh@redhat.com> - 0.9-1
	- Additional Plugins plus a lot of cleanup

* Mon Jul 19 2006 Dan Walsh <dwalsh@redhat.com> - 0.8-1
	- Added a bunch more plugins
    [Karl MacMillan <kmacmill@redhat.com>]
	- Add allow_cvs_read_shadow.py, allow_ftp_use_cifs, allow_ftp_use_nfs, and allow_gssd_read_tmp.
	- Change AVC to have additional helpers for matching messages.
	- Change Plugin to work better with more than one solution.

* Mon Jul 19 2006 Dan Walsh <dwalsh@redhat.com> - 0.7-1
	- Fix setroubleshoot_dispatcher to catch all information from
	  avc. Much cleaner interface and no longer uses audit2allow cruft.
	- Remove toolbar from popup window since it did nothing, and I
	  think it looks better without it.
	- fix allow_execmod plugin to report better data.

* Mon Jun 26 2006 John Dennis <jdennis@redhat.com> - 0.3-1
	- add missing /var/log directory %files section in spec file,
	  and add logrotate script

* Mon Jun 26 2006 John Dennis <jdennis@redhat.com> - 0.2-1
	- clean up spec file, reduce rpmlint complaints

* Fri May 19 2006 John Dennis <jdennis@redhat.com> - 0.1-1
	- Initial build.

