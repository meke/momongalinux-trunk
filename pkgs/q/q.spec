%global         momorel 15

Summary: 	Equational programming language
Name: 		q
Version: 	7.11
Release: 	%{momorel}m%{?dist}
License: 	GPLv2+
Group: 		Development/Languages
URL: 		http://q-lang.sourceforge.net/
Source: 	http://dl.sourceforge.net/sourceforge/q-lang/q-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	ImageMagick-devel >= 6.8.8.10
BuildRequires:	bison
BuildRequires:	curl-devel
BuildRequires:	dx-devel
BuildRequires:	flex
BuildRequires:	freetype-devel
BuildRequires:	gdbm-devel
BuildRequires:	gmp-devel >= 5.0
BuildRequires:	libxml2-devel
BuildRequires:	libxslt-devel
BuildRequires:	ncurses-devel
BuildRequires:	readline-devel
BuildRequires:	tcl-devel
BuildRequires:	tk-devel
BuildRequires:	unixODBC-devel
BuildRequires:	which
BuildRequires:	zlib-devel
Requires(post): info
Requires(postun): info


%description
Q is a powerful and extensible functional programming language based
on the term rewriting calculus. You specify an arbitrary system of
equations which the interpreter uses as rewrite rules to reduce
expressions to normal form. Q is useful for scientific programming and
other advanced applications, and also as a sophisticated kind of
desktop calculator. The distribution includes the Q programming tools,
a standard library, add-on modules for interfacing to Curl, GNU dbm,
ODBC, GNU Octave, ImageMagick, Tcl/Tk, XML/XSLT and an Emacs mode.


%package dx
Summary:        DX module for Q
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}

%description dx
%{summary}.


%package curl
Summary:        cURL module for Q
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}

%description curl
%{summary}.


%package magick
Summary:        ImageMagick module for Q
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}

%description magick
%{summary}.


%package tk
Summary:        Tk module for Q
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}

%description tk
%{summary}.


%package devel
Summary:        Headers and static library for developing programs using Q
Group:		Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:	libtool


%description devel
%{summary}.


%prep
%setup -q
sed -i 's|@libtool@|libtool|' src/Makefile.in

%define _use_internal_dependency_generator 0

# Filter unwanted Provides
cat > %{name}-prov <<EOF
#!/bin/sh
%{__find_provides} $* |\
  sed -e '/\.so[ \t]*$/d'
EOF

%define __find_provides %{_builddir}/%{name}-%{version}/%{name}-prov
chmod +x %{__find_provides}


%build
%configure --with-unicode --with-rl="-lreadline -lncurses" --with-dxl="-lDX -lDXL" --with-magick="`pkg-config ImageMagick --libs`" --with-magick-includes="`pkg-config ImageMagick --cflags`"
# %{?_smp_mflags} breaks the build
make


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

rm -f $RPM_BUILD_ROOT%{_libdir}/*.{la,a}
rm -f $RPM_BUILD_ROOT%{_libdir}/q/*.{la,a}
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

mkdir -p $RPM_BUILD_ROOT%{_docdir}/q-%{version}
mv $RPM_BUILD_ROOT%{_datadir}/q/etc $RPM_BUILD_ROOT%{_docdir}/q-%{version}
mv $RPM_BUILD_ROOT%{_datadir}/q/examples $RPM_BUILD_ROOT%{_docdir}/q-%{version}


%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/qdoc.info %{_infodir}/dir 2>/dev/null || :


%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/qdoc.info %{_infodir}/dir 2>/dev/null || :
fi


%files
%defattr(-,root,root,-)
%{_bindir}/q
%{_bindir}/qc
%{_infodir}/*
%{_libdir}/lib*.so.*
%{_libdir}/q
%{_mandir}/man*/*
%{_datadir}/q
%{_docdir}/q-%{version}
%docdir %{_docdir}/q-%{version}/
%exclude %{_libdir}/q/dxl.so
%exclude %{_datadir}/q/lib/dxl.q
%exclude %{_libdir}/q/curl.so
%exclude %{_datadir}/q/lib/curl.q
%exclude %{_libdir}/q/magick.so
%exclude %{_datadir}/q/lib/magick.q
%exclude %{_libdir}/q/tk.so
%exclude %{_datadir}/q/lib/tk.q


%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/lib*.so
%{_bindir}/qcc
%{_bindir}/qld


%files dx
%defattr(-,root,root,-)
%{_libdir}/q/dxl.so
%{_datadir}/q/lib/dxl.q


%files curl
%defattr(-,root,root,-)
%{_libdir}/q/curl.so
%{_datadir}/q/lib/curl.q

%files magick
%defattr(-,root,root,-)
%{_libdir}/q/magick.so
%{_datadir}/q/lib/magick.q


%files tk
%defattr(-,root,root,-)
%{_libdir}/q/tk.so
%{_datadir}/q/lib/tk.q


%changelog
* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.11-15m)
- rebuild against ImageMagick-6.8.8.10

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.11-14m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.11-13m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.11-12m)
- rebuild against ImageMagick-6.8.0.10

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.11-11m)
- rebuild against ImageMagick-6.7.2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.11-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.11-9m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.11-8m)
- rebuild against gmp-5.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.11-7m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.11-6m)
- rebuild against ImageMagick-6.6.2.10

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.11-5m)
- rebuild against readline6

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.11-4m)
- rebuild against ImageMagick-6.5.9.10

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.11-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.11-1m)
- import from Fedora 11

* Wed Mar 18 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 7.11-5
- Comment-out the AcquireOnePixel patch, appears to be no longer necessary for 
  new ImageMagick-6.4.9 (#490874)

* Thu Mar 05 2009 Caolan McNamara <caolanm@redhat.com> - 7.11-4
- adapt AcquireOnePixel usage for current ImageMagick api

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 7.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Aug  5 2008 Gerard Milmeister <gemi@bluewin.ch> - 7.11-2
- new release 7.11
- split some modules into separate packages

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 7.10-3
- Autorebuild for GCC 4.3

* Sat Jan  5 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 7.10-2
- Rebuild for new Tcl 8.5

* Sun Dec 30 2007 Gerard Milmeister <gemi@bluewin.ch> - 7.10-1
- new release 7.10

* Wed Oct 24 2007 Gerard Milmeister <gemi@bluewin.ch> - 7.8-1
- new version 7.8

* Thu Feb 15 2007 Gerard Milmeister <gemi@bluewin.ch> - 7.6-2
- use ncurses instead of termcap

* Sun Jan  7 2007 Gerard Milmeister <gemi@bluewin.ch> - 7.6-1
- new version 7.6

* Tue Oct 31 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.5-2
- patch for curl options

* Tue Oct 31 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.5-1
- new version 7.5

* Wed Aug 30 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.4-1
- new version 7.4

* Mon Aug 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.1-3
- Rebuild for FE6

* Tue Jun 13 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.1-2
- disable provides for modules

* Mon Jun 12 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.1-1
- new version 7.1
- use system libtool

* Sun Jun 11 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.1-0.2.rc2
- removed %%{_infodir}/dir
- modified %%description
- built apache module
- removed gqbuilder until gnocl is available

* Sat Jun 10 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.1-0.1.rc2
- changed version scheme

* Thu Jun  8 2006 Gerard Milmeister <gemi@bluewin.ch> - 7.1-1
- new version 7.1rc2

* Sun Sep 18 2005 Gerard Milmeister <gemi@bluewin.ch> - 6.2-1
- New Version 6.2

* Sun Mar  6 2005 Gerard Milmeister <gemi@bluewin.ch> - 6.0-1
- First Fedora release

