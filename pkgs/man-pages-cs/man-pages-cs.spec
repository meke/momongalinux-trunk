%global momorel 5

%define coreutils_version 20081218
Summary: Czech man pages from the Linux Documentation Project
Name: man-pages-cs
Version: 0.18.20090209
Release: %{momorel}m%{?dist}
License: GFDL and GPL+
Group: Documentation
URL: http://sweb.cz/tropikhajma/man-pages-cs/index.html
Source: http://tropikhajma.sweb.cz/%{name}/%{name}-%{version}.tar.lzma
Patch1: man-pages-cs-01.patch
Patch2: man-pages-cs-02.patch
Patch3: man-pages-cs-03.patch
Patch4: man-pages-cs-04.patch
Patch5: man-pages-cs-05.patch
Patch6: man-pages-cs-06.patch
Patch7: man-pages-cs-07.patch
Patch8: man-pages-cs-08.patch
Patch9: man-pages-cs-09.patch
Patch10: man-pages-cs-10.patch
Patch11: man-pages-cs-11.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: filesystem
BuildArch: noarch

%description
Manual pages from the Linux Documentation Project, translated into
Czech.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1

%build
# coreutils directory contains newer version
rm ./procps/kill.1
rm ./procps/uptime.1
rm ./man-pages/man1/du.1
# links to ls - better version in coreutils directory
rm ./man-pages/man1/dir.1
rm ./man-pages/man1/vdir.1

%install
rm -fr $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_mandir}
make install DESTDIR=$RPM_BUILD_ROOT MANDIR=%{_mandir}/cs

%clean
rm -fr $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CONTRIB README README.Czech
%{_mandir}/cs/*/*

%changelog
* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18.20090209-5m)
- release some directories provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.20090209-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.20090209-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18.20090209-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.20090209-1m)
- sync with Fedora 13 (0.18.20090209-7)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17.20080113-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17.20080113-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17.20080113-1m)
- update to 0.17.20080113

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-6m)
- rebuild against gcc43

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-5m)
- remove duplicated line from %%install
- remove %%{_mandir}/cs from %%files, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16-4m)
- sync FC7

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-3m)
- modify %%install to avoid conflicting with man

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-2m)
- modify %%install to avoid conflicting with shadow-utils

* Thu Nov 20 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (0.16-1m)
- change Source0 location
- remove Patch1

* Wed Nov 12 2003 zunda <zunda at freeshell.org>
- (0.14-8m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.14-7m)
- remove conflicts

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- mada nigirusugi

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- nigirusugi

* Tue Aug 14 2001 Tim Powers <timp@redhat.com>
- rebuilt to hopefully fix the rpm verify problem

* Thu Aug  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- s/Copyright/License/
- Own %%{_mandir}/cs

* Tue Apr  3 2001 Trond Eivind Glomsrod <teg@redhat.com>
- make pdf2dsc(1) use hyphen.cs, not hyphens.cs (#34181)

* Tue Dec 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 0.14
- new location

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
