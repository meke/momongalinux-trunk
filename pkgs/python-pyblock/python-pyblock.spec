%global momorel 1

%define realname pyblock
%define pyver %(%{__python} -c "import sys; print sys.version[:3]")
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

Summary: Python modules for dealing with block devices
Name: python-%{realname}
Version: 0.53
Release: %{momorel}m%{?dist}
Source0: %{realname}-%{version}.tar.bz2
Patch0:  pyblock-0.49-gcc46.patch
License: GPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7 , gettext, device-mapper
BuildRequires: dmraid-devel >= 1.0.0.rc16 , libselinux-devel, libsepol-devel
Requires: python >= %{pyver}, device-mapper , libselinux
Requires: dmraid

excludearch: s390 s390x

%description
The pyblock contains Python modules for dealing with block devices.

%prep
%setup -q -n %{realname}-%{version}
sed -i -e 's|/usr/share/doc/pyblock-${VERSION}|%{_pkgdocdir}|' Makefile

%build
make %{?_smp_mflags} OPTFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf %{buildroot}
make DESTDIR=${RPM_BUILD_ROOT} SITELIB=%{python_sitearch} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%dir %{_docdir}/%{name}-%{version}/*
%dir %{python_sitearch}/block
%{python_sitearch}/block

%changelog
* Tue May 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.53-1m)
- update 0.53

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.49-1m)
- update 0.49

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.47-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-5m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-4m)
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.47-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.47-1m)
- update 0.47

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-3m)
- remove "Requires: libbdevid". unuse dracut

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-2m)
- rebuid against dmraid-1.0.0.rc16

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-1m)
- update 0.46

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.42-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42 (sync with Fedora devel)
- rebuild against dmraid-1.0.0.rc15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.31-3m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-1m)
- update 0.31

* Fri Jan 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.30-1m)
- update 0.30

* Fri May 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.27-2m)
- add Requires: dmraid >= 1.0.0.rc14 for upgrading from STABLE_3

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.27-1m)
- update 0.27

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24-2m)
- down grade 0.24
- add python-2.5 patch

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.27-1m)
- update 0.27

* Mon Oct 16 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24-1m)
- update 0.24

* Mon May  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-1m)
- import from FC-devel

* Wed Feb 22 2006 Peter Jones <pjones@redhat.com> - 0.15-1
- Fix use of devices in /tmp to avoid duplicates. (fixes console spew during
  install)

* Mon Feb 13 2006 Peter Jones <pjones@redhat.com> - 0.14-1
- remove member partitions when we activate, rebuild them when we deactivate
- add another "count_devices(ctx->lc, NATIVE)" in discover_raiddevs.  it
  seems to help...

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.13-1.1
- bump again for double-long bug on ppc(64)

* Mon Feb  6 2006 Peter Jones <pjones@redhat.com> - 0.13-1
- partition naming/creation/detection fixes
- fixes for isw (ICH[4567]R) "groups"

* Tue Jan 31 2006 Peter Jones <pjones@redhat.com> - 0.12-1
- split __init__.py into separate files according to function
- disable "nosync" hack for now
- fix a refcounting bug in pydmraid_raidset_get_dm_table()
- add block.RaidDev.__cmp__()
- fix some type errors gcc can't check for when using pyblock_potoll
- be a little pickier about types for mode, devices, and sizes.
- add make rules for debugging
- fix "_init__" typo
- always use local import paths, and be much more strict about namespaces
- always make a new dm.device in BlockDev.From*()
- better defaults in BlockDev.create()
- add setter for block.dmraid.raidset.name, and rework RaidSet.set_name()
- rework RaidDev.get_bdev()
- rework "prefix" for RaidSet and RaidDev
- add getter for block.dmraid.raidset.map
- change arg order on block.dm.map.__init__() since there's no way to pass
  keyword args through the "abstract" interface.
- use self.name not self.rs.name in the RaidSet, and make changing the name
  work.
- make pydm_map_compare() compare names _last_, so we can compare a map
  that's been renamed with its earlier instantiations correctly.
- mark a device as degraded if there's any descrepancy at all between
  the number of members we find vs what we expect

* Thu Jan  5 2006 Peter Jones <pjones@redhat.com> - 0.11-1
- never trust dmraid on sync vs nosync; right now, always transform the
  table to "default" (no argument), which is to sync only when necessary,
  whatever that means.  Seems to lock up less often.
    
* Wed Jan  4 2006 Peter Jones <pjones@redhat.com> - 0.10-1
- fix checking for "degraded" raids

* Mon Dec 19 2005 Peter Jones <pjones@redhat.com> - 0.9-1
- fix some backwards isinstance() calls that cause RaidSet.get_valid()
  to fail

* Thu Dec 15 2005 Peter Jones <pjones@redhat.com> - 0.8-1
- prevent getRaidSets() from returning devices with missing members
- add "make flat_install" to make installing in RHupdates easier ;)

* Sun Dec 11 2005 Peter Jones <pjones@redhat.com> - 0.7-1
- merge debugging work from last several weeks

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> - 0.6-3.1
- rebuilt

* Sun Dec  4 2005 Peter Jones <pjones@redhat.com> - 0.6-3
- rebuild for newer libs

* Thu Nov 17 2005 Peter Jones <pjones@redhat.com> - 0.6-2
- temporarily mask exceptions

* Thu Nov 17 2005 Peter Jones <pjones@redhat.com> - 0.6-1
- fix RaidSets/getRaidSets

* Wed Nov 16 2005 Peter Jones <pjones@redhat.com> - 0.5-2
- rebuild for newer libdevmapper.a

* Fri Nov 11 2005 Peter Jones <pjones@redhat.com> - 0.5-1
- make it possible to easily build dm maps from dmraid tables
- support for partition table scanning

* Thu Nov 10 2005 Peter Jones <pjones@redhat.com> - 0.4-1
- minor fixups before adding to the distro

* Wed Nov  9 2005 Peter Jones <pjones@redhat.com> - 0.3-1
- make dmraid probing much simpler

* Thu Sep 22 2005 Peter Jones <pjones@redhat.com> - 0.2-2
- flush out dmraid mappings, add a lot of wrapper code in the toplevel

* Tue Sep 13 2005 Peter Jones <pjones@redhat.com> - 0.2-1
- add deps on libdevmapper and libdmraid

* Fri Sep  9 2005 Peter Jones <pjones@redhat.com> - 0.1-1
- initial package
