%global momorel 13
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Provides a full screen view of all open windows
Name: kompose
Version: 0.5.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: http://kompose.berlios.de/
Source0: http://download.berlios.de/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: admin.tar.bz2
Patch0: %{name}-0.5.3-desktop.patch
Patch1: %{name}-%{version}-iconsdir.patch
Patch2: %{name}-%{version}-correct-version.patch
Patch3: %{name}-%{version}-ktoolbar-patch
Patch4: %{name}-%{version}-automake111.patch
Patch5: %{name}-%{version}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}, imlib2
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: imlib2-devel
BuildRequires: libtool

%description
Kompose currently allows a fullscreen view of all your virtual desktops where
every window is represented by a scaled screenshot of it's own.

The Composite extension is used if available from the X server.

%prep
%setup -q

# build fix
rm -rf admin
tar xf %{SOURCE1}

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .iconsdir
%patch2 -p1 -b .version
%patch3 -p0 -b .ktoolbar
%patch4 -p1 -b .automake111
%patch5 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# move desktop file
mkdir -p %{buildroot}%{_datadir}/applications/kde
mv -f %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop %{buildroot}%{_datadir}/applications/kde/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/hicolor/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-11m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-10m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-9m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-7m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-6m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-2m)
- %%NoSource -> NoSource

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-1m)
- version 0.5.4
- import kompose-0.5.4-ktoolbar-patch from gentoo-x86-portage
 +- 09 Aug 2006; Carsten Lohrke <carlo@gentoo.org>
 +- +files/kompose-0.5.4-ktoolbar-patch, +kompose-0.5.4.ebuild:
 +- Version bump. Thanks to Tobias Roeser for the patch, bug #125209.
- replace admin for new autotools
- update iconsdir.patch
- add correct-version.patch

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-8m)
- use md5sum_src0

* Mon Sep 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-7m)
- update desktop.patch
- good-bye desktop-file-utils

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-6m)
- rebuild against kdelibs-3.5.4-3m

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-5m)
- add X-KDE-Utilities-Desktop to Categories of kompose.desktop
- update desktop.patch

* Sun Feb 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-4m)
- add enable_gcc_check_and_hidden_visibility switch

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-3m)
- rebuild against KDE 3.5 RC1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-2m)
- add --disable-rpath to configure

* Sun Jul 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-1m)
- version 0.5.3
- add desktopfile.patch

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-0.1.1m)
- update to version 0.5.2_beta1
- modify spec file
- add iconsdir.patch
- BuildPreReq: desktop-file-utils

* Tue Jan 25 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.5.1-1m)
- first import
