%global momorel 2
## Minimum required versions.
%global	gtk3_min_version	3.5.1
%global	glib2_min_version	2.30.0
%global	tp_mc_min_version	5.12.0
%global	tp_glib_min_version	0.19.9
%global	enchant_version		1.2.0
%global network_manager_version 0.7.0
%global libcanberra_version     0.4
%global webkit_version          1.3.13
%global goa_version		3.5.90
%global libnotify_version       0.7.0
%global libchamplain_version    0.12.1
%global folks_version           0.7.1

Name:		empathy
Version:	3.6.4
Release: %{momorel}m%{?dist}
Summary:	Instant Messaging Client for GNOME

Group:		Applications/Communications
License:	GPLv2+
URL:		http://live.gnome.org/Empathy

Source0:	http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Source1:	%{name}-README.ConnectionManagers

BuildRequires:	enchant-devel >= %{enchant_version}
BuildRequires:	iso-codes-devel
BuildRequires:	desktop-file-utils
#BuildRequires:	evolution-data-server-devel
BuildRequires:	gettext
BuildRequires:	glib2-devel >= %{glib2_min_version}
BuildRequires:	gnome-doc-utils >= 0.17.3
BuildRequires:	libcanberra-devel >= %{libcanberra_version}
BuildRequires:	webkitgtk3-devel >= %{webkit_version}
BuildRequires:	gtk3-devel >= %{gtk3_min_version}
BuildRequires:	intltool
BuildRequires:	libxml2-devel
BuildRequires:	scrollkeeper
BuildRequires:	gsettings-desktop-schemas-devel
BuildRequires:	telepathy-glib-devel >= %{tp_glib_min_version}
BuildRequires:  telepathy-farstream-devel >= 0.2.1
BuildRequires:  gstreamer-devel >= 0.10.32
BuildRequires:	libnotify-devel >= %{libnotify_version}
BuildRequires:	NetworkManager-glib-devel >= %{network_manager_version}
BuildRequires:  libchamplain-gtk-devel >= %{libchamplain_version}
BuildRequires:  clutter-gtk-devel >= 1.1.2
BuildRequires:  geoclue-devel >= 0.11
BuildRequires:	nautilus-sendto-devel
BuildRequires:  telepathy-logger-devel >= 0.6.0
BuildRequires:	folks-devel >= %{folks_version}
BuildRequires:	cheese-libs-devel
BuildRequires:	pulseaudio-libs-devel
BuildRequires:	libgudev1-devel
BuildRequires:	telepathy-mission-control-devel
BuildRequires:	gnome-online-accounts-devel >= %{goa_version}
BuildRequires:	gcr-devel
BuildRequires:  gnutls-devel >= 3.2.0
BuildRequires:  itstool

Requires:	telepathy-filesystem
Requires:	telepathy-mission-control >= %{tp_mc_min_version}
## We install the following connection managers by default.
Requires:	telepathy-gabble >= 0.16.0
Requires:	telepathy-salut >= 0.8.0
Requires:	telepathy-idle
Requires:	telepathy-haze >= 0.6.0

Requires(post): /usr/bin/gtk-update-icon-cache
Requires(postun): /usr/bin/gtk-update-icon-cache


%description
Empathy is powerful multi-protocol instant messaging client which
supports Jabber, GTalk, MSN, IRC, Salut, and other protocols.
It is built on top of the Telepathy framework.


%prep
%setup -q
# force this to be regenerated
rm data/empathy.desktop


%build
## GCC complains about some unused functions, so we forcibly show those as
## simple warnings instead of build-halting errors.
%configure --disable-static \
	   --enable-gst-1.0=no
## RPATHs are yucky.
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
# Parallel builds are broken.
make
install -m 0644 %{SOURCE1} ./README.ConnectionManagers


%install
rm -rf %{buildroot}
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
## Remove telepathy-yell devel files until it's a seperate tarball.
rm -f %{buildroot}/%{_libdir}/pkgconfig/telepathy-yell.pc
rm -Rf %{buildroot}/%{_includedir}/*
## Temporary work around until I properly fix it.
rm -Rf %{buildroot}/%{_datadir}/help/en_GB/*
rm -Rf %{buildroot}/%{_datadir}/help/zh_CN/*

%find_lang %{name} --with-gnome

desktop-file-install --vendor= --delete-original	\
	--dir %{buildroot}%{_datadir}/applications	\
	%{buildroot}%{_datadir}/applications/%{name}.desktop


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/sbin/ldconfig

%postun
if [ $1 -eq 0 ]; then
   touch --no-create %{_datadir}/icons/hicolor &>/dev/null
   gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
   glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
/sbin/ldconfig


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README README.ConnectionManagers NEWS
%doc COPYING-DOCS COPYING.LGPL COPYING.SHARE-ALIKE
%{_bindir}/%{name}
%{_bindir}/%{name}-accounts
%{_bindir}/%{name}-debugger
%{_libdir}/nautilus-sendto/plugins/libnstempathy.so
%{_libdir}/mission-control-plugins.0/mcp-account-manager-goa.so
%{_datadir}/empathy/
%{_datadir}/adium/message-styles
%{_datadir}/applications/%{name}*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Empathy.Call.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Empathy.Chat.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Empathy.Auth.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Empathy.FileTransfer.service
%{_datadir}/GConf/gsettings/empathy.convert
%{_datadir}/glib-2.0/schemas/org.gnome.Empathy.gschema.xml
%{_datadir}/telepathy/clients/Empathy.Call.client
%{_datadir}/telepathy/clients/Empathy.Chat.client
%{_datadir}/telepathy/clients/Empathy.Auth.client
%{_datadir}/telepathy/clients/Empathy.FileTransfer.client
%{_datadir}/help/*
%{_mandir}/man1/empathy*.1.bz2
%{_libexecdir}/empathy-auth-client
%{_libexecdir}/empathy-call
%{_libexecdir}/empathy-chat
%{_libdir}/empathy/

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-2m)
- rebuild against gnutls-3.2.0

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-1m)
- update to 3.6.4
- remove fedora from vendor (desktop-file-install)

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-2m)
- rebuild against telepathy-logger-0.6.0

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Wed Oct 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0.3-1m)
- update to 3.6.0.3

* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0.1-1m)
- update to 3.6.0.1

* Fri Oct  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0
- disable gstreamer-1.0 for a while

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- add BuildRequires

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1.1-3m)
- rebuild against telepathy-logger-0.2.12

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1.1-2m)
- rebuild against telepathy-logger-0.2.11

* Thu Oct 27 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1.1-1m)
- update to 3.2.1.1

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Oct 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0.1-1m)
- update to 3.2.0.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-3m)
- rebuild against libchamplain-0.12

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91-2m)
- fix BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Thu Aug 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-2m)
- fix BuildRequires

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Tue May  3 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-3m)
- rebuild against NetworkManager-0.8.998

* Tue May  3 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- fix BR version

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-2m)
- rebuild against libnotify-0.7.2

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 2.34.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.2-7m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32.2-6m)
- rebuild against telepathy-logger-0.2.8

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32.2-5m)
- rebuild against telepathy-logger-0.2.7

* Wed Mar  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-4m)
- update patch0 (build fix)

* Sat Feb 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.2-3m)
- import upstream patches to enable build with telepathy-logger-0.2.1
- applying Patch0 is failed
- see Patch0 and modify it or bump version to 2.33.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.2-2m)
- rebuild for new GCC 4.5

* Fri Nov 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-1m)
- update to 2.32.2

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Tue Oct 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-5m)
- rebuild against folks-0.2.1

* Sun Oct 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-4m)
- add BuildRequires

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-3m)
- rebuild against webkitgtk-1.3.4

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add glib-compile-schemas script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-5m)
- rebuild against libchamplain-0.7.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-4m)
- full rebuild for mo7 release

* Sun Jun 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- add BuildRequires

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- rebuild against libchamplain-0.6.1

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.1-2m)
- rebuild against evolution-2.30.2

* Thu Jun 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.1-1m)
- update to 2.30.1.1

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Wed Apr 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0.2-1m)
- update to 2.30.0.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0.1-1m)
- update to 2.30.0.1

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.29.91-2m)
- rebuild against NetworkManager-0.8

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90
- delete devel package

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Thu Nov 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1.2-1m)
- update to 2.28.1.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1.1-1m)
- update to 2.28.1.1

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sun Oct  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.1-1m)
- update to 2.28.0.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- delete patch0

* Tue Sep 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.92-1m)
- update to 2.27.92
-- add patch0 for libchampain-0.4.0

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.91.1-1m)
- update to 2.27.91.1

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.2-3m)
- add BuildPrereq

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-2m)
- move pc file to devel

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Tue Mar 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0.1-1m)
- update to 2.26.0.1

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.92-2m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.90-2m)
- add BuildPrereq: telepathy-farsight

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-4m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.1-2m)
- rebuild against python-2.6.1-1m

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- welcome evolution-data-server

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- initial build
- good-bye evolution
