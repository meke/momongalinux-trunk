%global momorel 2

Summary: The Chewing engine for IBus input platform
Name: ibus-chewing
Version: 1.3.10
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://code.google.com/p/ibus/
Group: System Environment/Libraries
Source0: http://ibus.googlecode.com/files/%{name}-%{version}-Source.tar.gz
NoSource: 0
Source1: https://fedorahosted.org/releases/c/m/cmake-fedora/cmake-fedora-0.8.1-modules-only.tar.gz
NoSource: 1

Patch0: ibus-chewing-ibus141.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: GConf2-devel
BuildRequires: cmake >= 2.4
BuildRequires: gettext-devel
BuildRequires: gob2 >= 2.0.16-3m
BuildRequires: gtk2-devel
BuildRequires: ibus-devel >= 1.4.99.20121006
BuildRequires: libX11-devel
BuildRequires: libXtst-devel
BuildRequires: libchewing-devel >= 0.3.2
BuildRequires: pkgconfig
Requires: ibus >= 1.4.99.20121006
Requires: libchewing >= 0.3.2
Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2

%description
The Chewing engine for IBus platform. It provides Chinese input method from
libchewing.

%prep
%setup -q -n %{name}-%{version}-Source

tar xzvf %{SOURCE1}

%patch0 -p1 -b .ibus141

%build
%cmake -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo -DGNOME_SHELL=1 .
make VERBOSE=1  %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%pre
if [ "$1" -gt 1 ] ; then
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-uninstall-rule \
%{_sysconfdir}/gconf/schemas/%{name}.schemas >/dev/null || :
fi

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
%{_sysconfdir}/gconf/schemas/%{name}.schemas > /dev/null || :

%preun
if [ "$1" -eq 0 ] ; then
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-uninstall-rule \
%{_sysconfdir}/gconf/schemas/%{name}.schemas > /dev/null || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS README ChangeLog INSTALL COPYING
%config(noreplace) %{_sysconfdir}/gconf/schemas/%{name}.schemas
%{_libexecdir}/ibus-engine-chewing
%{_datadir}/ibus-chewing
%{_datadir}/ibus/component/chewing.xml

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.10-2m)
- rebuild against ibus-1.4.99.20121006

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.10-1m)
- update 1.3.10
- add ibus-1.4.1 patch

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9.2-3m)
- rebuild for ibus-1.3.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.9.2-2m)
- rebuild for new GCC 4.6

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.9.2-1m)
- update to 1.3.9.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.7.20100910-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7.20100910-1m)
- update to 1.3.7.20100910

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.5.20100706-3m)
- full rebuild for mo7 release

* Sun Jul 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5.20100706-2m)
- rebuild against gob2-2.0.16-3m

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.5.20100706-1m)
- update to 1.3.5.20100706

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.4.20100608-1m)
- update to 1.3.4.20100608

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.99.20100317-1m)
- update to 1.2.99.20100317

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090917-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090917-1m)
- update to 1.2.0.20090917

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-0.20090508.1m)
- version 1.0.9

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080819.1m)
- initial package for Momonga Linux
