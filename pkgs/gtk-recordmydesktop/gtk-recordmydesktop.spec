%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           gtk-recordmydesktop
Version:        0.3.8
Release:        %{momorel}m%{?dist}
Summary:        GUI Desktop session recorder with audio and video

Group:          Applications/Multimedia
License:        GPL
URL:            http://recordmydesktop.iovar.org/
Source0:        http://dl.sourceforge.net/sourceforge/recordmydesktop/%{name}-%{version}.tar.gz        
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7, pygtk2-devel	 
BuildRequires:  desktop-file-utils, gettext
Requires:       recordmydesktop >= 0.3.8.1

%description
Graphical frontend for the recordmydesktop desktop session recorder.

recordMyDesktop is a desktop session recorder for linux that attempts to be easy to use, yet also effective at it's primary task.

As such, the program is separated in two parts; a simple command line tool that performs the basic tasks of capturing and encoding and an interface that exposes the program functionality in a usable way.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -c -p"

%find_lang gtk-recordMyDesktop

desktop-file-install --vendor= --delete-original  \
        --dir $RPM_BUILD_ROOT%{_datadir}/applications   \
        --remove-category Application \
        $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop

%clean
rm -rf $RPM_BUILD_ROOT

%files -f gtk-recordMyDesktop.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README ChangeLog
%{_bindir}/*
%{python_sitelib}/*
%{_datadir}/applications/*
%{_datadir}/pixmaps/*

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.8-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.8-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.8-2m)
- rebuild against python-2.6.1-1m

* Wed Dec 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8

* Mon May 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7.2-2m)
- remove vendor="fedora" from desktop file

* Wed May 14 2008 Masayuki SANO
- (0.3.7.2-1m)
- import to Momonga from Fedora
- update to 0.3.7.2

* Thu Jan 17 2008 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.7-1
- New upstream release
* Sun Oct 21 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.6-1
- New version
- Update URL
* Sat Jun 02 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.4-1
- New version 0.3.4
* Tue Mar 06 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.3.1-2 
- Preserve timestamps
* Mon Mar 05 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.3.1-2 
- Add missing BR
* Sun Mar 04 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.3.1-1
- Initial build
