%global         momorel 2

Name:           perl-Devel-CheckOS
Version:        1.72
Release:        %{momorel}m%{?dist}
Summary:        check what OS we're running on
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Devel-CheckOS/
Source0:        http://www.cpan.org/authors/id/D/DC/DCANTRELL/Devel-CheckOS-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.00405
BuildRequires:  perl-Data-Compare >= 1.21
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Find-Rule >= 0.28
BuildRequires:  perl-File-Temp >= 0.19
BuildRequires:  perl-Test-Simple >= 0.62
Requires:       perl-Data-Compare >= 1.21
Requires:       perl-ExtUtils-MakeMaker
Requires:       perl-File-Find-Rule >= 0.28
Requires:       perl-File-Temp >= 0.19
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
BuildArch:      noarch

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Devel::CheckOS provides a more friendly interface to $^O, and also lets
you check for various OS "families" such as "Unix", which includes things
like Linux, Solaris, AIX etc.

%prep
%setup -q -n Devel-CheckOS-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ARTISTIC.txt CHANGELOG GPL2.txt README TODO
%{_bindir}/use-devel-assertos
%{perl_vendorlib}/Devel/AssertOS.pm
%{perl_vendorlib}/Devel/AssertOS
%{perl_vendorlib}/Devel/CheckOS.pm
%{perl_vendorlib}/Devel/CheckOS
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-2m)
- rebuild against perl-5.20.0

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-1m)
- update to 1.72

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
