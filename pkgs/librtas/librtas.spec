%global momorel 8

Summary: Libraries to provide access to RTAS calls and RTAS events.
Name: librtas
Version: 1.2.4
Release: %{momorel}m%{?dist}
URL: http://librtas.ozlabs.org
License: "IBM Common Public License (CPL) v1.0"
Group: System Environment/Libraries
Source: librtas-src-1.2.4.tar.gz
Patch0: librtas-1.2.4-no_syscall1.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: ppc ppc64

%description
The librtas shared library provides userspace with an interface
through which certain RTAS calls can be made.  The library uses
either of the RTAS User Module or the RTAS system call to direct 
the kernel in making these calls.

The librtasevent shared library provides users with a set of
definitions and common routines useful in parsing and dumping
the contents of RTAS events.

%package devel
Summary: Static libraries and header files for librtas development.
Group: Development/Libraries
Requires: librtas = %{version}

%description devel

The librtas-devel packages contains the header files and static libraries
necessary for developing programs using libdaemon.

%prep
%setup -q
%patch0 -p1 -b .no_syscall

%build
make CFLAGS="$RPM_OPT_FLAGS -fPIC -DPIC"


%install
make DESTDIR=$RPM_BUILD_ROOT install
rm -rf $RPM_BUILD_ROOT/usr/share/doc

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%doc COPYRIGHT README
%{_libdir}/librtas.so.%{version}
%{_libdir}/librtasevent.so.%{version}

%files devel
%defattr(-, root, root, -)
%doc COPYRIGHT README
%{_libdir}/*.so
%{_libdir}/*.a
%{_includedir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.4-3m)
- rebuild against gcc43

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.4-2m)
- fix release and files

* Thu Dec 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.4-1m)
- import from FC to Momonga for ppc64-utils

* Tue Aug 01 2006 Paul Nasrat <pnasrat@redhat.com> - 1.2.4-2
- Backport syscall fix from upstream 

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.2.4-1.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.2.4-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.2.4-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Nov 03 2005 Paul Nasrat <pnasrat@redhat.com> 1.2.4-1
- Update to latest version

* Thu Nov 03 2005 Paul Nasrat <pnasrat@redhat.com> 1.2.2-1
- Initial release
