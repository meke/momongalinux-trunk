%global         momorel 1

Name:           perl-Net-SSLeay
Version:        1.64
Release:        %{momorel}m%{?dist}
Summary:        Perl extension for using OpenSSL
License:        BSD
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-SSLeay/
Source0:        http://www.cpan.org/authors/id/M/MI/MIKEM/Net-SSLeay-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Array-Compare
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Sub-Uplevel
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Warn
BuildRequires:  perl-Tree-DAG_Node
BuildRequires:  openssl-devel >= 1.0.1
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
There is a related module called Net::SSLeay::Handle included in this
distribution that you might want to use instead. It has its own pod
documentation.

%prep
%setup -q -n Net-SSLeay-%{version}

%build
echo "y" | CFLAS="%{optflags}" %{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes Credits Debian_CPANTS.txt QuickRef README README.Win32
%{perl_vendorarch}/auto/Net/SSLeay
%{perl_vendorarch}/Net/SSLeay*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-1m)
- rebuild against perl-5.20.0
- update to 1.64

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.58-1m)
- update to 1.58
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-2m)
- rebuild against perl-5.18.1

* Sat Jun  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-1m)
- update to 1.55

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-2m)
- rebuild against perl-5.18.0

* Sat Mar 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54

* Fri Mar 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-2m)
- rebuild against perl-5.16.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Fri Dec 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Thu Dec 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.50

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-2m)
- rebuild against perl-5.16.2

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-1m)
- update to 1.49

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48
- rebuild against perl-5.16.0

* Wed Mar 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-2m)
- rebuild against openssl-1.0.1

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-2m)
- rebuild against perl-5.14.2

* Mon Oct  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Sun Sep 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Wed Sep 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Fri Sep 16 2011 nARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.36-9m)
- rebuild for new GCC 4.6

* Mon Jan  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.36-8m)
- add "do_test" flag

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.36-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.36-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-3m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.36-2m)
- rebuild against openssl-1.0.0

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.35-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-4m)
- rebuild against perl-5.10.1

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.35-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.35-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Thu Jul 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.32-3m)
- rebuild against gcc43

* Mon Oct  1 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.32-2m)
- disable make test

* Wed Sep  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32
- do not use %%NoSource macro
- source name was changed from Net_SSLeay.pm-x.xx.tar.gz to Net-SSLeay-x.xx.tar.gz
- spec file was remade with cpanspec

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.30-3m)
- rebuild against openssl-0.9.8a

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.30-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.30-1m)
- update to 1.30

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.25-6m)
- rebuilt against perl-5.8.7

* Wed Feb  9 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.25-5m)
- enable x86_64.

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.25-4m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.25-3m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.25-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.25-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.21-7m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.21-6m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.21-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.21-4m)
  rebuild against openssl 0.9.7a

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.21-3m)
- rebuild against perl-5.8.0

* Thu Nov 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.21-2m)
- remove a needless patch

* Wed Nov 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.21-1m)

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.20-1m)

* Sun Aug 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.19-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.18-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.09-9m)
- remove BuildRequires: gcc2.95.3

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.09-8k)
- rebuild against for perl-5.6.1

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.08-6k)
- s/Copyright/License/

* Fri Sep 28 2001 YAMAGUCHI Kenji <yamk@kondra.org>
- (1.08-4k)
- removed "meke test"

* Thu Sep 27 2001 YAMAGUCHI Kenji <yamk@kondra.org>
- (1.08-2k)
- from PLD
- Kondarize.

Revision 1.17  2001/07/25 21:26:45  qboosh
- don't include patch backup in examples
- release 2

Revision 1.16  2001/07/25 09:12:13  mis
- autoupdated to 1.08

Revision 1.15  2001/05/09 00:14:58  kloczek
- release 2: rebuild against openssl 0.9.6a.

Revision 1.14  2001/04/30 16:05:27  kloczek
- added using %%{rpmcflags} macro.

Revision 1.13  2001/04/23 14:35:34  mis
- autoupdated to 1.07

Revision 1.12  2001/04/17 12:13:34  baggins
- release 2
- rebuild with perl 5.6.1

Revision 1.11  2001/04/14 18:00:09  baggins
- removed explicit requirements for perl = %%{version} and %%{perl_sitearch}
  they will be added by rpm if needed

Revision 1.10  2001/04/10 19:47:55  mis
- autoupdated to 1.06

Revision 1.9  2001/03/19 01:04:23  kloczek
- release 3.

Revision 1.8  2001/03/17 20:42:03  agaran
Added openssl-tools to buildrequires (not for to send to builders)

Revision 1.7  2001/01/25 20:03:45  misiek

Massive attack. We use -O0 instead -O flags while debug enabled.

Revision 1.6  2001/01/19 06:19:29  kloczek
- release 2: rebuild against perl 5.6 and use rpm automation.

Revision 1.5  2000/06/09 07:23:40  kloczek
- added using %%{__make} macro.

Revision 1.4  2000/05/21 20:11:42  kloczek
- spec adapterized.

Revision 1.3  2000/04/01 11:15:20  zagrodzki
- changed all BuildRoot definitons
- removed all applnkdir defs
- changed some prereqs/requires
- removed duplicate empty lines

Revision 1.2  2000/03/28 16:54:58  baggins
- translated kloczkish into english

Revision 1.1  2000/02/17 20:14:11  pius
- initial release
