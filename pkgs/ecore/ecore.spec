%global momorel 1
#%%global snapdate 2010-06-27

Summary: Enlightened Core X interface library
Name: ecore
Version: 1.7.7
Release: %{momorel}m%{?dist} 
License: Modified BSD
Group: User Interface/X
URL: http://www.enlightenment.org/efm.html
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: eet-devel >= 1.7.7
BuildRequires: evas-devel >= 1.7.7
BuildRequires: libX11-devel libXext-devel 
BuildRequires: libXcursor-devel libXrender-devel libxcb-devel 
BuildRequires: libXinerama-devel libXrandr-devel libXScrnSaver-devel 
BuildRequires: libXcomposite-devel libXfixes-devel libXdamage-devel 
BuildRequires: xorg-x11-proto-devel SDL-devel 
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libcurl-devel chrpath doxygen pkgconfig
BuildRequires: perl, pkgconfig, doxygen
Requires: openldap
Requires: libssh2

%description
Ecore is the event/X abstraction layer that makes doing selections,
Xdnd, general X stuff, event loops, timeouts and idle handlers fast,
optimized, and convenient.


%package devel
Summary: Ecore headers and development libraries.
Group: Development/Libraries
Requires: %{name} =  %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{name}-%{version}

%build
%configure --disable-static \
 --enable-ecore-fb \
 --enable-ecore-sdl \
 --enable-ecore-desktop \
 --enable-simple-x11 \
 --enable-ecore-evas-software-buffer \
 --enable-ecore-evas-software-x11 \
 --enable-ecore-evas-xrender-x11 \
 --enable-ecore-evas-opengl-x11 \
 --enable-ecore-evas-software-16-x11 \
 --enable-ecore-evas-software-xcb \
 --enable-ecore-evas-xrender-xcb \
 --enable-ecore-evas-fb

make %{?_smp_mflags}
sed -i -e 's/$projectname Documentation Generated: $datetime/$projectname Documentation/' doc/foot.html

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p' transform='s,x,x,'
chrpath --delete %{buildroot}%{_libdir}/lib%{name}_*.so.*
chrpath --delete %{buildroot}%{_libdir}/lib%{name}.so.*
find %{buildroot} -name '*.la' -delete
%find_lang %{name} 

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/libecore.so.*
%{_libdir}/libecore_*.so.*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/immodules
%{_libdir}/%{name}/immodules/*.so
%{_datadir}/locale/*/*/ecore.mo

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7
- rebuild against gnutls-3.2.0

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49539-1m)
- update to new svn snap

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.9.063-3m)
- build without autoconf

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.063-2m)
- rebuild against openssl-1.0.0

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.062-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.062-1m)
- update to new svn snap

* Sun Aug  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.9.061-2m)
- Req change from devel to core

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.060-0.20090607.1m)
- update

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.9.050-4m)
- add autoreconf (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.050-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.050-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.050-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.043-1m)
- update to 0.9.9.043

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.042-1m)
- merge from T4R
- 
- changelog is below
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (0.9.9.042-1m)
- - delete %%{_bindir}/ecore-config
- - delete %%{_datadir}/aclocal/ecore.m4
- - add PreReq: openldap-devel
- - add PreReq: libssh2-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.037-0.20061231.3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.037-0.20061231.2m)
- %%NoSource -> NoSource

* Mon Jan  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.037-0.20061231.1m)
- version 0.9.9.037-20061231

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.9.022-0.20051209.2m)
- rebuild against openssl-0.9.8a

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.022-0.20051209.1m)
- version 0.9.9.022-20051209

* Fri Jul  1 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.010-0.20050627.2m)
- revised x86_64 patch

* Tue Jun 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.010-0.20050627.1m)
- version 0.9.9.010-20050627

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.0-0.20050215.2m)

* Thu Feb 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-0.20050215.1m)
- version 1.0.0-20050215

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.20041218.3m)
- rebuild against evas-1.0.0-0.20041218.4m

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.0-0.20041218.2m)
- enable x86_64.

* Tue Dec 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-0.20041218.1m)
- first import to Momonga
