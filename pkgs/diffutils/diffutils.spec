%global momorel 1

Summary: A GNU collection of diff utilities.
Name: diffutils
Version: 3.3
Release: %{momorel}m%{?dist}
Group: Applications/Text
URL: http://www.gnu.org/directory/diffutils.html
Source0: ftp://ftp.gnu.org/gnu/diffutils/diffutils-%{version}.tar.xz
NoSource: 0
#Source1: cmp.1
#Source2: diff.1
#Source3: diff3.1
#Source4: sdiff.1
License: GPL
#Prefix: %{_prefix}
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Diffutils includes four utilities: diff, cmp, diff3 and sdiff. Diff
compares two files and shows the differences, line by line.  The cmp
command shows the offset and line numbers where two files differ, or
cmp can show the characters that differ between the two files.  The
diff3 command shows the differences between three files.  Diff3 can be
used when two people have made independent changes to a common
original; diff3 can produce a merged file that contains both sets of
changes and warnings about conflicts.  The sdiff command can be used
to merge two files interactively.

Install diffutils if you need to compare text files.

%prep
%setup -q

%build
#autoconf
%configure
#make PR_PROGRAM=%{_bindir}/pr
make

%install
rm -rf %{buildroot}
%makeinstall

#( cd %{buildroot}
##  bzip2 .%{_infodir}/diff*
#  mkdir -p .%{_mandir}/man1
#  for manpage in %{SOURCE1} %{SOURCE3} %{SOURCE4}
#  do
#    install -m 0644 ${manpage} .%{_mandir}/man1
#  done
#)

# remove
rm -f %{buildroot}%{_datadir}/info/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/diffutils.info.* %{_infodir}/dir --entry="* diff: (diff).                 The GNU diff."

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/diffutils.info.* %{_infodir}/dir --entry="* diff: (diff).                 The GNU diff."
fi

%files
%defattr(-,root,root)
%doc NEWS README AUTHORS COPYING ChangeLog THANKS
%{_bindir}/*
%{_mandir}/*/*
%{_infodir}/diffutils.info*

%changelog
* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3-1m)
- update to 3.3

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2-1m)
- update to 3.2

* Fri Aug 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1-1m)
- update to 3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-2m)
- full rebuild for mo7 release

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Tue Apr 27 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.9-1m)
- update to 2.9

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.1-10m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.1-7m)
- rebuild against gcc43

* Wed Mar 17 2004 Toru Hoshina <t@momonga-linux.org>
- (2.8.1-6m)
- revised spec for enabling rpm 4.2.

* Wed Nov  5 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.8.1-5m)
- use %%{momorel}

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.8.1-4k)
- /sbin/install-info -> info in PreReq.

* Fri Apr 12 2002 Junichiro Kita <kita@kondara.org>
- (2.8.1-2k)

* Wed Mar 27 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (2.8-2k)
- update to 2.8
- remove man pages. now they are included in tar-ball.
- find-lang

* Mon Jan 29 2001 Kenichi Matsubara <m@kondara.org>
- bugfix %files section.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Oct 25 2000 Kenichi Matsubara <m@kondara.org>
- info*.gz to info*.bz2.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fix %%changelog entries (escape them)
- update source location
- remove manual stripping
- add URL

* Tue Jun 06 2000 Than Ngo <than@redhat.de>
- add %%defattr
- use rpm macros

* Wed May 31 2000 Ngo Than <than@redhat.de>
- put man pages and info files in correct place
- cleanup specfile

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man pages.

* Mon Apr 19 1999 Jeff Johnson <jbj@redhat.com>
- man pages not in %%files.
- but avoid conflict for diff.1

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 14)

* Sun Mar 14 1999 Jeff Johnson <jbj@redhat.com>
- add man pages (#831).
- add %%configure and Prefix.

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Tue Jul 14 1998 Bill Kawakami <billk@home.com>
- included the four man pages stolen from Slackware

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sun May 03 1998 Cristian Gafton <gafton@redhat.com>
- fixed spec file to reference/use the %{buildroot} always
    
* Wed Dec 31 1997 Otto Hammersmith <otto@redhat.com>
- fixed where it looks for 'pr' (/usr/bin, rather than /bin)

* Fri Oct 17 1997 Donnie Barnes <djb@redhat.com>
- added BuildRoot

* Sun Sep 14 1997 Erik Troan <ewt@redhat.com>
- uses install-info

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
