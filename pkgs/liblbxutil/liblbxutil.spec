%global momorel 3

Summary: Low Bandwith X extension (LBX) utility routines
Name: liblbxutil
Version: 1.1.0
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel

%description
liblbxutil - Low Bandwith X extension (LBX) utility routines

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README ChangeLog NEWS
%{_libdir}/liblbxutil.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/lbxbuf.h
%{_includedir}/X11/extensions/lbxbufstr.h
%{_includedir}/X11/extensions/lbxdeltastr.h
%{_includedir}/X11/extensions/lbximage.h
%{_includedir}/X11/extensions/lbxopts.h
%{_includedir}/X11/extensions/lbxzlib.h
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/lbxutil.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- initial build
