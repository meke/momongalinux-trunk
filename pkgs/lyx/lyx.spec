%global momorel 1
%global boost_version 1.55.0
%global fontname lyx 

Summary: WYSIWYM (What You See Is What You Mean) document processor
Name:	 lyx
Version: 2.1.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: 	 Applications/Publishing
URL: 	 http://www.lyx.org/
Source0: ftp://ftp.lyx.org/pub/lyx/stable/2.1.x/%{name}-%{version}.tar.xz
NoSource: 0
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
## upstreamable patches
# submitted, but upstream rejected it.  we currently agree to disagree.
Patch50: lyx-2.1.0-xdg_open.patch
Source1: lyxrc.dist
Source2: defaults.lyx
Source10: lyx.desktop
BuildRequires: aiksaurus-devel
BuildRequires: aspell-devel
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: python
BuildRequires: qt-devel >= 4.7.0
BuildRequires: zlib-devel
BuildRequires: tetex-dvips tetex-ptex >= 3.1.10-16m
Requires(post): kpathsea
Requires(postun): kpathsea
Requires: tetex-dvips tetex-latex tetex-ptex >= 3.1.10-16m
Requires: ghostscript
Requires: xdvik
Requires: %{fontname}-fonts = %{version}-%{release}

%description
LyX is a modern approach to writing documents which breaks with the
obsolete "typewriter paradigm" of most other document preparation
systems.

It is designed for people who want professional quality output
with a minimum of time and effort, without becoming specialists in
typesetting.

The major innovation in LyX is WYSIWYM (What You See Is What You Mean).
That is, the author focuses on content, not on the details of formatting.
This allows for greater productivity, and leaves the final typesetting
to the backends (like LaTeX) that are specifically designed for the task.

With LyX, the author can concentrate on the contents of his writing,
and let the computer take care of the rest.

%package fonts
Summary: Lyx/MathML fonts
Group:   Applications/Publishing
# The actual license says "The author of these fonts, Basil K. Malyshev, has
# kindly granted permission to use and modify these fonts."
# One of the font files (wasy10) is separately licensed GPL+.
License: "Copyright only" and GPL+
BuildArch: noarch
Requires: fontpackages-filesystem
Obsoletes: mathml-fonts < 1.0-6m
Provides:  mathml-fonts = 1.0-6m
Provides:  lyx-cmex10-fonts = %{version}-%{release}
Provides:  lyx-cmmi10-fonts = %{version}-%{release}
Provides:  lyx-cmr10-fonts = %{version}-%{release}
Provides:  lyx-cmsy10-fonts = %{version}-%{release}

%description  fonts
A collection of Math symbol fonts for %{name}. 

%prep
%setup -q -n %{name}-%{version}%{?pre}
%patch50 -p1 -b .xdg_open

%build
%configure \
  --disable-dependency-tracking \
  --disable-rpath \
  --enable-build-type=release \
  --enable-optimization="%{optflags}" \
  --with-enchant \
  --with-qt4-dir=`pkg-config --variable=prefix QtCore` \
  --with-aiksaurus \
  --without-included-boost \
  --with-aspell \
  --disable-debug --disable-stdlib-debug --disable-assertions --disable-concept-checks \
  LIBS="-lX11"

make %{?_smp_mflags} X_LIBS=-lX11

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} transform='s,x,x,'

# misc/extras
install -p -m644 -D %{SOURCE1} %{buildroot}%{_datadir}/lyx/lyxrc.dist

# Set up the lyx-specific class files where TeX can see them
texmf=%{_datadir}/texmf
mkdir -p %{buildroot}${texmf}/tex/latex
mv %{buildroot}%{_datadir}/lyx/tex \
   %{buildroot}${texmf}/tex/latex/lyx

# .desktop
desktop-file-install --vendor="" \
  --dir %{buildroot}%{_datadir}/applications \
  %{SOURCE10}

# icon
install -p -D -m644 lib/images/lyx.png \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/lyx.png

# ghost'd files
touch %{buildroot}%{_datadir}/lyx/lyxrc.defaults
touch %{buildroot}%{_datadir}/lyx/{packages,textclass}.lst
touch %{buildroot}%{_datadir}/lyx/doc/LaTeXConfig.lyx

# fonts
install -m 0755 -d %{buildroot}%{_fontdir}
mv %{buildroot}%{_datadir}/lyx/fonts/*.ttf %{buildroot}%{_fontdir}/
rm -rf %{buildroot}%{_datadir}/lyx/fonts

# install default template (use platex with jarticle class)
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/lyx/templates/defaults.lyx

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
update-desktop-database >& /dev/null ||:
%{_bindir}/texhash > /dev/null 2>&1 ||:

%postun
if [ $1 -eq 0 ]; then
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
update-desktop-database >& /dev/null ||:
%{_bindir}/texhash > /dev/null 2>&1 ||:
fi

%if 0
## Catch installed/uninstalled helpers
##   not sure if this is really needed anymore, as it seems to be a per-user thing,
##   and besides, we use xdg-open now -- Rex
%triggerin -- latex2html,wv
if [ $2 -gt 1 ]; then
cd %{_datadir}/lyx && ./configure.py --without-latex-config > /dev/null 2>&1 ||:
fi

%triggerun -- latex2html,wv
if [ $2 -eq 0 ]; then
cd %{_datadir}/lyx && ./configure.py --without-latex-config > /dev/null 2>&1 ||:
fi
%endif

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc ANNOUNCE lib/CREDITS NEWS README
%{_bindir}/*
%{_mandir}/man1/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*
%dir %{_datadir}/lyx
%{_datadir}/lyx/CREDITS
%{_datadir}/lyx/autocorrect
%{_datadir}/lyx/bind
%{_datadir}/lyx/chkconfig.ltx
%dir %{_datadir}/lyx/commands
%{_datadir}/lyx/commands/default.def
%{_datadir}/lyx/configure*
%{_datadir}/lyx/doc
%{_datadir}/lyx/encodings
%{_datadir}/lyx/examples
%{_datadir}/lyx/external_templates
%{_datadir}/lyx/images
%{_datadir}/lyx/kbd
%{_datadir}/lyx/languages
%{_datadir}/lyx/latexfonts
%{_datadir}/lyx/layouts
%{_datadir}/lyx/layouttranslations
%{_datadir}/lyx/lyx2lyx
%{_datadir}/lyx/scripts
%{_datadir}/lyx/symbols
%{_datadir}/lyx/syntax.default
%{_datadir}/lyx/templates
%{_datadir}/lyx/ui
%{_datadir}/lyx/unicodesymbols
%config(noreplace) %{_datadir}/lyx/lyxrc.dist
%ghost %{_datadir}/lyx/lyxrc.defaults
%ghost %{_datadir}/lyx/*.lst
%ghost %{_datadir}/lyx/doc/LaTeXConfig.lyx
%{_datadir}/texmf/tex/latex/lyx/

%files fonts
%defattr(-,root,root,-)
%{_fontdir}/
%doc lib/fonts/BaKoMaFontLicense.txt
%doc lib/fonts/ReadmeBaKoMa4LyX.txt

%posttrans fonts
fc-cache %{_fontdir} 2> /dev/null ||:

%changelog
* Wed Jun 25 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-2m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-15m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.6.6.1-14m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-13m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-12m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-11m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-10m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-9m)
- rebuild against boost-1.46.0

* Fri Feb 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-8m)
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-7m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6.1-6m)
- rebuild against boost-1.44.0

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.6.1-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.6.1-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.6.1-3m)
- rebuild against qt-4.6.3-1m

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.6.1-2m)
- rebuild against boost-1.43.0

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.6.1-1m)
- update to 1.6.6.1
- split out lyx-fonts which provides and obsoletes mathml-fonts

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-6m)
- explicitly link libX11

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-5m)
- rebuild against boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-3m)
- rebuild against boost-1.40

* Wed Sep  2 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-2m)
- use installed boost library instead of the bundled one.
- delete --without-warings from configure: it was obsoleted

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-5m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-4m)
- rebuild against rpm-4.6

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.4-3m)
- rebuild against qt-4.4.0-1m

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-2m)
- modify Requires

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.4-1m)
- import to Momonga (based on lyx-1.5.4-1.src.rpm in Fedora)
- modified default settings (to use jarticle/platex/platex2pdf in Japanese(non-CJK) language setting) to make Japanese typesetting more easier in Momonga.

* Mon Feb 25 2008 Rex Dieter <rdieter@fedoraproject.org> 1.5.4-1
- lyx-1.5.4 (#434689)
- reintroduce xdg-utils patch (reverted upstream).
- omit bakoma ttf fonts

* Mon Feb 11 2008 Jose Matos <jamatos[AT]fc.up.pt> - 1.5.3-2
- Rebuild for gcc 4.3

* Mon Dec 17 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.3-1
- lyx-1.5.3

* Tue Dec 04 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.2-2
- drop scriptlet optimization hack

* Mon Oct 08 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.2-1
- lyx-1.5.2

* Sat Aug 25 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.1-2
- respin (BuildID)

* Thu Aug 09 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.1-1
- lyx-1.5.1
- License: GPLv2+

* Wed Jul 25 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-1
- lyx-1.5.0(final)

* Sun Jul 15 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.10.rc2
- upstream patch for 'lyx --export latex' crasher (#248282)

* Thu Jun 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.9.rc2
- scriptlet optmization

* Thu Jun 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.8.rc2
- lyx-1.5.0rc2

* Fri Jun 01 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.7.rc1
- lyx-1.5.0rc1

* Fri May 18 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.6.beta3
- lyx-1.5.0beta3

* Sun Apr 22 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.5.beta2
- lyx-1.5.0beta2

* Mon Apr 02 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.4.beta1
- fix qt-4.3 crasher

* Tue Mar 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.3.beta1
- stop omitting -fexceptions

* Wed Mar 21 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.2.beta1
- +Requires: tetex-IEEEtran (#232840)

* Mon Mar 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.5.0-0.1.beta1
- lyx-1.5.0beta1
- tweak lyxrc.dist

* Thu Feb 15 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.4.4-2
- biffed sources, respin

* Wed Feb 14 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.4.4-1
- lyx-1.4.4
- .desktop's: -Category=Application
- mark -xforms as deprecated

* Sun Oct 01 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.3-3
- sync .desktop files with upstream
- use xdg-open as default helper, +Requires: xdg-utils

* Thu Sep 21 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.3-1
- lyx-1.4.3

* Thu Sep 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.2-5
- fc6 respin

* Thu Aug 17 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.2-4
- owowned files, incomplete package removal (bug #201197)

* Thu Jul 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.2-2
- 1.4.2

* Wed Jun 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-9
- Requires(hint): wv (bug #193858)
- fix dependancy -> dependency

* Thu Jun 15 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-8
- BR: gettext
- fc4: restore Requires(hint): tetex-preview

* Thu May 25 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-7.1
- fc4: drop Requires: tetex-preview, it's not ready yet.

* Wed May 24 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-7
- use serverpipe "~/.lyx/lyxpipe" instead, that was the old default
  and what pybibliographer expects.

* Tue May 23 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-6
- set defaults for (see %{_datadir}/lyx/lyxrc.defaults.custom)
  screen_font_roman "Serif"
  screen_font_sans "Sans"
  screen_font_typewriter "Monospace"
  screen_zoom 100
  serverpipe "~/.lyx/pipe"
  (bug #192253)

* Mon May 22 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-5
- Requires(hint): tetex-preview

* Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-4
- add generic app icon (rh #191944)

* Fri Apr 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-3
- Requires(hint): tetex-dvipost
  adds support for lyx's Document->Change Tracking

* Tue Apr 11 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.1-2
- 1.4.1

* Thu Mar 30 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-5
- %%trigger ImageMagick (#186319)

* Thu Mar 09 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-4
- fix stripping of -fexceptions from %%optflags

* Wed Mar 08 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-3
- include beamer.layout

* Wed Mar 08 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-2
- 1.4.0(final)
- drop boost bits

* Tue Mar 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.12.rc
- 1.4.0rc
- drop boost patch (for now)

* Fri Mar 03 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.11.pre6
- 1.4.0pre6
- --disable-stdlib-debug --disable-assertions --disable-concept-checks
- don't use --without-included-boost (for now)

* Mon Feb 20 2006 Rex Dieter <rexdieter[AT]usres.sf.net> 1.4.0-0.10.pre5
- gcc41 patch
- document boost/gcc41 patches
- avoid --without-included-boost on fc4/gcc-4.0.2 (gcc bug)

* Tue Feb 14 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.9.pre5
- updated boost patch
- drop -fexceptions from %%optflags

* Mon Feb 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.7.pre5
- --without-included-boost
- BR: boost-devel

* Mon Feb 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.5.pre5
- 1.4.0pre5

* Tue Jan 31 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.2.pre4
- 1.4.0pre4

* Fri Jan 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0-0.1.pre3
- 1.4.0pre3

* Fri Jan 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.7-4
- cleanup/fix snarfing of intermediate frontend builds.

* Fri Jan 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.7-2
- BR: libXpm-devel

* Tue Jan 17 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.7-1
- 1.3.7
- -qt/-xforms: frontend pkgs (#178116)

* Fri Oct 21 2005 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.6-5
- %%post/%%postun: update-desktop-database

* Fri Sep 02 2005 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.6-4
- leave out kde-redhat bits in Fedora Extras build
- define/use safer (esp for x86_64) QTDIR bits

* Fri Aug 05 2005 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.6-3
- touchup helpers script
- fix for (sometimes missing) PSres.upr

* Mon Aug 01 2005 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.6-2
- use triggers to configure/unconfigure helper (ps/pdf/html) apps

* Sat Jul 23 2005 Rex Dieter <rexdieter[AT]users.sf.net> 1.3.6-1
- 1.3.6

* Mon May 23 2005 Rex Dieter <rexdieter[At]users.sf.net> 1.3.5-4
- qt_immodule patch (lyx bug #1830)
- update -helpers patch to look-for/use evince (rh bug #143992)
- drop (not-strictly-required) Req's on helper apps
  htmlview, gsview (rh bug #143992)
- %%configure: --with-aiksaurus
- %%configure: --enable-optimization="$$RPM_OPT_FLAGS"
- %%configure: --disable-dependency-tracking

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 1.3.5
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Thu Oct 21 2004 Rex Dieter <rexdieter[At]users.sf.net> 0:1.3.5-3
- BR: htmlview
- Requires: htmlview, gsview (so build-time-detected helper apps are
  available at runtime)

* Thu Oct 21 2004 Rex Dieter <rexdieter[At]users.sf.net> 0:1.3.5-0.fdr.3
- BR: htmlview
- Requires: htmlview, gsview (so build-time-detected helper apps are
  available at runtime)

* Wed Oct 20 2004 Rex Dieter <rexdieter[At]users.sf.net> 0:1.3.5-0.fdr.2
- BR: pspell-devel -> aspell-devel
- BR: tetex-* (helper detection, fonts)
- -helpers patch: find/use htmlview, gsview, kdvi, ggv, kghostview
- .desktop: GenericName: WYSIWYM document processor

* Wed Oct 06 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.5-0.fdr.1
- 1.3.5

* Fri Apr 30 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.6
- BR: libtool

* Fri Apr 23 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.5
- Group: Applications/Publishing
- BR: desktop-file-utils
- Requires(post,postun): tetex

* Sat Apr 10 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.4
- .desktop: separate file
- .desktop: drop 'Utility' category, add 'Qt'
- .desktop: Name: lyx -> LyX
- .desktop: Comment: lyx 1.3.4 -> WYSIWYM document processor
- convert icon xpm -> png

* Thu Mar 11 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.3
- dynamically determine version for qt dependency.

* Tue Mar 09 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.2
- add a few (mostly superfluous) BuildRequires to make fedora.us's
  buildsystem happy.

* Tue Mar 09 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.1
- Allow building/use for any qt >= 3.1

* Thu Feb 19 2004 Rex Dieter <rexdieter at sf.net> 0:1.3.4-0.fdr.0
- 1.3.4
- Categories=Office

* Mon Nov 24 2003 Rex Dieter <rexdieter at sf.net> 0:1.3.3-0.fdr.3
- Requires: tetex-latex
- support MimeType(s): application/x-lyx;text/x-lyx

* Sat Nov 22 2003 Rex Dieter <rexdieter at sf.net> 0:1.3.3-0.fdr.2
- let rpm auto-require qt.
- remove (optional) xforms support.
- Requires: latex-xft-fonts

* Mon Oct 06 2003 Rex Dieter <rexdieter at sf.net> 0:1.3.3-0.fdr.1
- 1.3.3
- update macros for Fedora Core.

* Mon May 12 2003 Rex Dieter <rexdieter at users.sf.net> 0:1.3.2-0.fdr.0
- 1.3.2
- fedora-ize.

* Tue Mar 25 2003 Rex Dieter <rexdieter at users.sf.net> 1.3.1-1.0
- 1.3.1 release.

* Fri Feb 21 2003 Rex Dieter <rexdieter at users.sf.net> 1.3.0-1.0
- yank kmenu

* Fri Feb 07 2003 Rex Dieter <rexdieter at users.sf.net> 1.3.0-0.0
- 1.3.0

