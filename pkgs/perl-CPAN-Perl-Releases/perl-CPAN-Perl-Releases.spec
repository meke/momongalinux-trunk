%global         momorel 2

Name:           perl-CPAN-Perl-Releases
Version:        1.74
Release:        %{momorel}m%{?dist}
Summary:        Mapping Perl releases on CPAN to the location of the tarballs
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CPAN-Perl-Releases/
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/CPAN-Perl-Releases-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.16.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
CPAN::Perl::Releases is a module that contains the mappings of all perl
releases that have been uploaded to CPAN to the authors/id/ path that the
tarballs reside in.

%prep
%setup -q -n CPAN-Perl-Releases-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README tools
%{perl_vendorlib}/CPAN/Perl/Releases.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.74-2m)
- rebuild against perl-5.20.0

* Sun Jun  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.74-1m)
- update to 1.74

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.68-1m)
- update to 1.68

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.66-1m)
- update to 1.66

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-1m)
- update to 1.64

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.58-1m)
- update to 1.58

* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48

* Mon Oct 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Sat Sep 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22
- rebuild against perl-5.18.0

* Thu May 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Mon May 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sun Apr 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Fri Mar 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06
- rebuild against perl-5.16.3

* Mon Mar 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Fri Mar  8 2013 NARITA Koichi <pulsar[momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon Jan 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Sat Dec 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Sun Nov  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-2m)
- rebuild against perl-5.16.2

* Sat Oct 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Sun Oct 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-1m)
- update to 0.78

* Sat Oct 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-1m)
- update to 0.74

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Tue Aug 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50
- rebuild against perl-5.16.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
