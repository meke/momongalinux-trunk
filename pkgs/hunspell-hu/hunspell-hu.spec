%global momorel 4

Name: hunspell-hu
Summary: Hungarian hunspell dictionaries
Version: 1.6
Release: %{momorel}m%{?dist}
Source: http://downloads.sourceforge.net/magyarispell/hu_HU-%{version}.tar.gz
Group: Applications/Text
URL: http://magyarispell.sourceforge.net
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+ or GPLv2+ or MPLv1.1
BuildArch: noarch

Requires: hunspell

%description
Hungarian hunspell dictionaries.

%prep
%setup -q -n hu_HU-%{version}

%build
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_hu_HU.txt LEIRAS.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-1m)
- import from Fedora to Momonga

* Mon Mar 17 2008 Caolan McNamara <caolanm@redhat.com> - 1.3-1
- latest version

* Mon Nov 05 2007 Caolan McNamara <caolanm@redhat.com> - 1.2.2-1
- latest version

* Tue Oct 02 2007 Caolan McNamara <caolanm@redhat.com> - 1.2.1-1
- latest version

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 1.2-2
- clarify that this is tri-licenced

* Fri Jun 01 2007 Caolan McNamara <caolanm@redhat.com> - 1.2-1
- next version

* Sat May 05 2007 Caolan McNamara <caolanm@redhat.com> - 1.1.1-1
- latest canonical version

* Wed Feb 14 2007 Caolan McNamara <caolanm@redhat.com> - 0.20061105-2
- match licence

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20061105-1
- initial version
