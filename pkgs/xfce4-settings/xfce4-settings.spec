%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0
%global exover 0.10.2

Name: 		xfce4-settings
Version: 	4.11.2
Release:	%{momorel}m%{?dist}
Summary: 	ulti channel settings manager

Group: 		User Interface/Desktops
License:	GPL
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: gettext
BuildRequires: gtk2-devel
#BuildRequires: libxfcegui4-devel >= %{xfce4ver}
BuildRequires: libxfce4util-devel >= %{xfce4ver}
BuildRequires: xfconf-devel
BuildRequires: imake
BuildRequires: libXt-devel
BuildRequires: exo-devel >= %{exover}
BuildRequires: libxfce4ui-devel >= %{xfce4ver}
BuildRequires: libxklavier-devel >= 5.0
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: garcon-devel >= 0.3.0
#do not set xfce4-session version for avoid loop
# Requires: xfce4-session >= 4.7.0
Requires: xfce4-session
Requires: xfconf

Obsoletes: xfce-mcs-manager
Provides: xfce-mcs-manager
Obsoletes: xfce-mcs-manager-devel
Provides: xfce-mcs-manager-devel
Obsoletes: xfce-mcs-plugins
Provides: xfce-mcs-plugins

Obsoletes: libxfce4menu
Provides: libxfce4menu
Obsoletes: libxfce4menu-devel

%description
This package inlcudes a set of plugins for the multi channel
settings manager.

%prep
%setup -q 

%build
%configure --disable-static LIBS="-lm -lX11"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "*.a" -delete

# start xfce4-settings-helper XFCE only
echo "OnlyShowIn=XFCE;" >> \
    %{buildroot}%{_sysconfdir}/xdg/autostart/xfce4-settings-helper-autostart.desktop

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README COPYING AUTHORS
%{_sysconfdir}/xdg/autostart/xfce4-settings-helper-autostart.desktop
%{_sysconfdir}/xdg/autostart/xfsettingsd.desktop
%{_sysconfdir}/xdg/menus/xfce-settings-manager.menu
%{_sysconfdir}/xdg/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml
%{_bindir}/xfce4-*
%{_bindir}/xfsettingsd
%{_libdir}/xfce4/settings/appearance-install-theme
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/devices/xfce-display-*.png
%{_datadir}/locale/*/*/*

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.2-1m)
- update to 4.11.2

* Tue Sep 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.10.0-2m)
- fix build failure; add BuildRequires

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-3m)
- change Source0 URI

* Thu Jun  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8.0-2m)
- Obsoletes: libxfce4menu-devel

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update
- rebuild against xfce4-4.8
- delete Patch1: xfce4-settings-4.6.5-libnotify-0.7.2.patch

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.6-1m)
- update

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.5-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.5-3m)
- full rebuild for mo7 release

* Thu Aug 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.5-2m)
- fix BuildRequires

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.5-1m)
- update
- comment out Patch0:         xfce4-settings-4.6.1-libxklavier-5.0.patch

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.1-7m)
- fix build

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-6m)
- revise BuildRequires

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.1-5m)
- rebuild against libxklavier-5.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.1-2m)
- rebuild against libxklavier-4.0

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.0-2m)
- --add-only-show-in XFCE

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update
- add old, broken, kludge libtools hack "%%define __libtoolize :"

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-2m)
- use exover

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update
- fix %%files

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-2m)
- add Provides and Obsoletes

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- initial Momonga package
