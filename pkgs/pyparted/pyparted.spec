%global momorel 2

Summary: Python module for GNU parted
Name: pyparted
Version: 3.9
Release: %{momorel}m%{?dist}
License: GPLv2+
Group:   System Environment/Libraries
Source0: https://fedorahosted.org/releases/p/y/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: 0001-Do-not-traceback-when-calling-setlocale-875354.patch
Patch2: 0002-Convert-Constraint-to-__ped.Constraint-in-partition..patch
Patch3: 0003-Subject-PATCH-pyparted-export-ped_disk_new-functiona.patch
Patch4: pyparted-3.9-tests-fixes.patch
Patch5: pyparted-3.9-aarch64.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: parted >= 3.1
BuildRequires: python-devel
BuildRequires: parted-devel >= 3.0
BuildRequires: pkgconfig
Obsoletes: parted-python
Provides: parted-python

%description
Python module for the parted library.  It is used for manipulating
partition tables.

%prep
%setup -q
%patch1 -p 1
%patch2 -p 1
%patch3 -p 1
%patch4 -p 1
%patch5 -p 1

%build
%{__make} %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install  DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS COPYING ChangeLog NEWS README TODO
%{python_sitearch}/_pedmodule.so
%{python_sitearch}/parted
%{python_sitearch}/%{name}-%{version}-*.egg-info

%changelog
* Fri Jun 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9-2m)
- add patches

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9-1m)
- version up 3.9

* Wed Sep  7 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.8-1m)
- version up 3.8

* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6-3m)
- rebuild against python-2.7

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6-2m)
- enable to build with python 2.6

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6-2m)
- rebuild against python-2.7

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6-1m)
- update 3.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3-1m)
- update 3.3

* Mon Mar 15 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-1m)
- update 3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.12-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.12-2m)
- rebuild against parted-1.8.8-9m

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.12-1m)
- update 2.0.12

* Sun Feb 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-1m)
- update 2.0.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.9-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.9-4m)
- rebuild against python-2.6.1-2m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.9-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Sat May 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.9-2m)
- fix install path

* Fri May 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.9-1m)
- update 1.8.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.5-3m)
- rebuild against gcc43

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.5-2m)
- rebuild against parted-1.8.6

* Wed Feb 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-1m)
- update
- rebuild against parted-1.8.2

* Tue Jan 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.3-1m)
- update 1.8.3
-- fix "glibc detected" error

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3-5m)
- rebuild against python-2.5

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.3-4m)
- rebuild against db4-4.5.20-1m

* Mon Oct 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.3-2m)
- revise %%files to avoid conflicting with python

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momoga-linux.org>
- (1.7.3-1m)
- update to 1.7.3

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momoga-linux.org>
- (1.6.10-5m)
- delete %%dir %%{_libdir}/python?.?/site-packages/, this dir is owned by python

* Mon May 22 2006 Yohsuke Ooi <meke@momoga-linux.org>
- (1.6.10-4m)
- rebuild against python-2.4.2

* Thu May 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.10-3m)
- rebuild against python-2.4.3

* Tue May 16 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.10-2m)
- rebuild against parted-1.6.25.1

* Sat May 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.10-1m)
- update 1.6.10

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.8-3m)
- rebuild against python-2.4.2

* Fri Jul  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.8-2m)
- rebuild against python-2.3.5-1m

* Tue Dec  7 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.8-1m)
  imported from FC development 1.6.8-3

* Mon Nov  8 2004 Jeremy Katz <katzj@redhat.com> - 1.6.8-3
- rebuild for python 2.4

* Mon Oct 11 2004 Warren Togami <wtogami@redhat.com> - 1.6.8-2
- #135100 req python-abi (Robert Scheck)

* Tue Aug 17 2004 Jeremy Katz <katzj@redhat.com> - 1.6.8-1
- update for new parted ABI
  - device -> heads, sectors, cylinders now refer to the bios geometry
- require parted >= 1.6.12

* Thu Jul 22 2004 Jeremy Katz <katzj@redhat.com> - 1.6.7-3
- build on ppc64 again

* Thu May 13 2004 Jeremy Katz <katzj@redhat.com> - 1.6.7-1
- fix build for newer versions of gcc (fix from Jeff Law)

* Tue Mar 16 2004 Jeremy Katz <katzj@redhat.com> 1.6.6-2
- fix PARTITION_PROTECTED definition (#118451)

* Fri Mar 12 2004 Jeremy Katz <katzj@redhat.com>
- Initial build split out into separate source from the parted package.
- Don't build on ppc64 right now due to parted build problems (#118183)
