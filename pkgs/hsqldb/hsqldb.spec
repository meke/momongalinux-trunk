%global momorel 10

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define section		devel

%define cvs_version	1_8_0_10

Name:		hsqldb
Version:	1.8.0.10
Release:	1jpp.1.%{momorel}m%{?dist}
#Epoch:		1
Summary:	Hsqldb Database Engine
License:	BSD
Url:		http://hsqldb.sourceforge.net/
#http://downloads.sourceforge.net/hsqldb/hsqldb_1_8_0_10.zip
Source0:    %{name}_%{cvs_version}.zip
Source1:    %{name}-1.8.0-standard.cfg
Source2:    %{name}-1.8.0-standard-server.properties
Source3:    %{name}-1.8.0-standard-webserver.properties
Source4:    %{name}-1.8.0-standard-sqltool.rc
Patch0:     %{name}-1.8.0-scripts.patch
Patch1:     hsqldb-tmp.patch
Patch2:     %{name}-1.8.0-specify-su-shell.patch
Patch10:    %{name}-init.patch
Requires:   servletapi5
Requires(post):   coreutils
Requires(post):   servletapi5
#Requires(preun): /bin/rm
Requires(preun): coreutils
Requires(pre):	shadow-utils
BuildRequires:	ant
BuildRequires:	junit
BuildRequires:	jpackage-utils >= 1.5
BuildRequires:	servletapi5
Group:          Development/Tools
%if ! %{gcj_support}
Buildarch:	noarch
%endif
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
HSQLdb is a relational database engine written in JavaTM , with a JDBC
driver, supporting a subset of ANSI-92 SQL. It offers a small (about
100k), fast database engine which offers both in memory and disk based
tables. Embedded and server modes are available. Additionally, it
includes tools such as a minimal web server, in-memory query and
management tools (can be run as applets or servlets, too) and a number
of demonstration examples.
Downloaded code should be regarded as being of production quality. The
product is currently being used as a database and persistence engine in
many Open Source Software projects and even in commercial projects and
products! In it's current version it is extremely stable and reliable.
It is best known for its small size, ability to execute completely in
memory and its speed. Yet it is a completely functional relational
database management system that is completely free under the Modified
BSD License. Yes, that's right, completely free of cost or restrictions!

%package manual
Summary:	Manual for %{name}
Group:          Development/Tools

%description manual
Documentation for %{name}.

%package javadoc
Summary:	Javadoc for %{name}
Group:          Development/Tools
Requires(post):   coreutils
Requires(preun): coreutils

%description javadoc
Javadoc for %{name}.

%package demo
Summary:	Demo for %{name}
Group:          Development/Tools
#Requires:	%{name} = %{eoch}:%{version}-%{release}
Requires:	%{name} = %{version}-%{release}

%description demo
Demonstrations and samples for %{name}.

%prep
%setup -T -c -n %{name}
(cd ..
unzip -q %{SOURCE0} 
)
# set right permissions
find . -name "*.sh" -exec chmod 755 \{\} \;
# remove all _notes directories
for dir in `find . -name _notes`; do rm -rf $dir; done
# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;
find . -name "*.war" -exec rm -f {} \;
# correct silly permissions
chmod -R go=u-w *

%patch0
%patch1 -p1
%patch2
%patch10 -p1

%build
export CLASSPATH=$(build-classpath \
jsse/jsse \
jsse/jnet \
jsse/jcert \
jdbc-stdext \
servletapi5 \
junit)
pushd build
ant jar javadoc
popd

%install
# jar
install -d -m 755 %{buildroot}%{_javadir}
install -m 644 lib/%{name}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} ${jar/-%{version}/}; done)
# bin
install -d -m 755 %{buildroot}%{_bindir}
install -m 755 bin/runUtil.sh %{buildroot}%{_bindir}/%{name}RunUtil
# sysv init
install -d -m 755 %{buildroot}%{_initscriptdir}
install -m 755 bin/%{name} %{buildroot}%{_initscriptdir}/%{name}
# config
install -d -m 755 %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
# serverconfig
install -d -m 755 %{buildroot}%{_localstatedir}/lib/%{name}
install -m 644 %{SOURCE2} %{buildroot}%{_localstatedir}/lib/%{name}/server.properties
install -m 644 %{SOURCE3} %{buildroot}%{_localstatedir}/lib/%{name}/webserver.properties
install -m 600 %{SOURCE4} %{buildroot}%{_localstatedir}/lib/%{name}/sqltool.rc
# lib
install -d -m 755 %{buildroot}%{_localstatedir}/lib/%{name}/lib
install -m 644 lib/functions 	%{buildroot}%{_localstatedir}/lib/%{name}/lib
# data
install -d -m 755 %{buildroot}%{_localstatedir}/lib/%{name}/data
# demo
install -d -m 755 %{buildroot}%{_datadir}/%{name}/demo
install -m 755 demo/*.sh 	%{buildroot}%{_datadir}/%{name}/demo
install -m 644 demo/*.html 	%{buildroot}%{_datadir}/%{name}/demo
# javadoc
install -d -m 755 %{buildroot}%{_javadocdir}/%{name}-%{version}
cp -r doc/src/* %{buildroot}%{_javadocdir}/%{name}-%{version}
rm -rf doc/src
# manual
install -d -m 755 %{buildroot}%{_docdir}/%{name}-%{version}
cp -r doc/* %{buildroot}%{_docdir}/%{name}-%{version}
cp index.html %{buildroot}%{_docdir}/%{name}-%{version}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf %{buildroot}

%pre
# Add the "hsqldb" user and group
# we need a shell to be able to use su - later
%{_sbindir}/groupadd -g 96 -f -r %{name} 2> /dev/null || :
%{_sbindir}/useradd -u 96 -g %{name} -s /sbin/nologin \
    -d %{_localstatedir}/lib/%{name} -r %{name} 2> /dev/null || :

%post

/sbin/chkconfig --add hsqldb
if [ $1 = 1 ] ; then
  /sbin/chkconfig hsqldb off
fi

rm -f %{_localstatedir}/lib/%{name}/lib/hsqldb.jar
rm -f %{_localstatedir}/lib/%{name}/lib/servlet.jar
(cd %{_localstatedir}/lib/%{name}/lib
    ln -s $(build-classpath hsqldb) hsqldb.jar
    ln -s $(build-classpath servletapi5) servlet.jar
)

%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%preun
if [ "$1" = "0" ]; then
    rm -f %{_localstatedir}/lib/%{name}/lib/hsqldb.jar
    rm -f %{_localstatedir}/lib/%{name}/lib/servlet.jar
    #%{_sbindir}/userdel %{name} >> /dev/null 2>&1 || :
    #%{_sbindir}/groupdel %{name} >> /dev/null 2>&1 || :
    /sbin/chkconfig --del hsqldb
fi

%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%preun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/%{name}
fi

%files
%defattr(0644,root,root,0755)
%dir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/hsqldb_lic.txt
%{_javadir}/*
%attr(0755,root,root) %{_bindir}/*
%attr(0755,root,root) %{_initscriptdir}/%{name}
%attr(0644,root,root) %{_sysconfdir}/sysconfig/%{name}
%attr(0755,hsqldb,hsqldb) %{_localstatedir}/lib/%{name}/data
%{_localstatedir}/lib/%{name}/lib
%attr(0644,root,root) %{_localstatedir}/lib/%{name}/server.properties
%attr(0644,root,root) %{_localstatedir}/lib/%{name}/webserver.properties
%attr(0600,hsqldb,hsqldb) %{_localstatedir}/lib/%{name}/sqltool.rc
%dir %{_localstatedir}/lib/%{name}

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files manual
%defattr(0644,root,root,0755)
%doc %{_docdir}/%{name}-%{version}/*
%exclude %{_docdir}/%{name}-%{version}/hsqldb_lic.txt

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

%files demo
%defattr(0644,root,root,0755)
%{_datadir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0.10-1jpp.1.10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0.10-1jpp.1.9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0.10-1jpp.1.8m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0.10-1jpp.1.7m)
- fix up

* Sun Aug 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0.10-1jpp.1.6m)
- do not start daemon at start up time

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0.10-1jpp.1.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0.10-1jpp.1.4m)
- rebuild against rpm-4.6

* Mon Oct  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0.10-1jpp.1.3m)
- add & remmove service

* Fri Jul 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0.10-1jpp.1.2m)
- modify Requires

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0.10-1jpp.1.1m)
- update to 1.8.0.10, because 1.8.0.9 can not compile

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0.9-2jpp.1.1m)
- update to 1.8.0.9
- fedora changelog is below
-
- * Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1:1.8.0.9-2jpp.1
- - Autorebuild for GCC 4.3
- 
- * Tue Jan 22 2008 Jon Prindiville <jprindiv@redhat.com> 1.8.0.9-1jpp.1
- - Fix for bz# 428520: Defining JAVA_HOME in /etc/sysconfig/hsqldb
- 
- * Thu Jan 17 2008 Jon Prindiville <jprindiv@redhat.com> 1.8.0.9-1jpp
- - Upgrade to 1.8.0.9
- 
- * Tue Dec 04 2007 Jon Prindiville <jprindiv@redhat.com> 1.8.0.8-1jpp.5
- - Backport patch, addressing CVE-2007-4576
- 
- * Tue Oct 16 2007 Deepak Bhole <dbhole@redhat.com> 1.8.0.8-1jpp.4
- - Rebuild
- 
- * Tue Oct 16 2007 Deepak Bhole <dbhole@redhat.com> 1.8.0.8-1jpp.3
- - Fix bz# 218135: Init script now specifies shell when starting service
- 
- * Thu Sep 20 2007 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.8-1jpp.2
- - Added %%{?dist} to release, as per Fedora policy
- 
- * Fri Aug 31 2007 Fernando Nasser <fnasser@redhat.com> 1:1.8.0.8-1jpp.1
- - Merge with upstream
- 
- * Fri Aug 31 2007 Fernando Nasser <fnasser@redhat.com> 1:1.8.0.8-1jpp
- - Upgrade to 1.8.0.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0.7-2jpp.4m)
- rebuild against gcc43

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0.7-2jpp.3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jul  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0.7-2jpp.2m)
- modify %%files to avoid conflicting
- modify Requires for Momonga Linux

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0.7-2jpp.1m)
- import from f7 to Momonga
- delete Epoch

* Thu Jan 22 2007 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.7-1jpp.2
- Bump release to build in rawhide

* Thu Jan 22 2007 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.7-1jpp.1
- Updgrade to 1.8.0.7

* Thu Nov 30 2006 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.4-4jpp.2
- Bump release to build in rawhide

* Wed Nov 29 2006 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.4-4jpp.1
- Added missing entries to the files section

* Wed Oct 11 2006 Fernando Nasser <fnasser@redhat.com> 1:1.8.0.4-3jpp.4
- Add post requires for servletapi5 to ensure installation order

* Sun Oct 01 2006 Jesse Keating <jkeating@redhat.com> 1:1.8.0.4-3jpp.3
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Wed Sep 20 2006 Steve Grubb <sgrubb@redhat.com> 1:1.8.0.4-3jpp.2
- Apply patch correcting tmp file usage

* Mon Aug 21 2006 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.4-3jpp.1
- Add missing postun section.

* Tue Aug 08 2006 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.4-2jpp.2
- Fix regression re: missing shadow-utils prereq.

* Fri Aug 04 2006 Deepak Bhole <dbhole@redhat.com> 1:1.8.0.4-2jpp.1
- Add missing requirements.
- Merge with fc spec.
  - From gbenson@redhat.com:
    - Change /etc/init.d to /etc/rc.d/init.d.
    - Create hsqldb user and group with low IDs (RH bz #165670).
    - Do not remove hsqldb user and group on uninstall.
    - Build with servletapi5.
  - From ashah@redhat.com:
    - Change hsqldb user shell to /sbin/nologin.
  - From notting@redhat.com
    - use an assigned user/group id

* Fri Apr 28 2006 Fernando Nasser <fnasser@redhat.com> 1:1.8.0.4-1jpp
- First JPP 1.7 build
- Upgrade to 1.8.0.4

* Tue Jul 26 2005 Fernando Nasser <fnasser@redhat.com> 0:1.80.1-1jpp
- Upgrade to 1.8.0.1

* Mon Mar 07 2005 Fernando Nasser <fnasser@redhat.com> 0:1.73.3-1jpp
- Upgrade to 1.7.3.3

* Wed Mar 02 2005 Fernando Nasser <fnasser@redhat.com> 0:1.73.0-1jpp
- Upgrade to 1.7.3.0

* Wed Aug 25 2004 Ralph Apel <r.apel at r-apel.de> 0:1.72.3-2jpp
- Build with ant-1.6.2

* Mon Aug 16 2004 Ralph Apel <r.apel at r-apel.de> 0:1.72.3-1jpp
- 1.7.2.3 stable

* Fri Jun 04 2004 Ralph Apel <r.apel at r-apel.de> 0:1.72-0.rc6b.1jpp
- 1.7.2 preview

* Tue May 06 2003 David Walluck <david@anti-microsoft.org> 0:1.71-1jpp
- 1.71
- update for JPackage 1.5

* Mon Mar 18 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.61-6jpp 
- generic servlet support

* Mon Jan 21 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.61-5jpp 
- versioned dir for javadoc
- no dependencies for javadoc package
- stricter dependencies for demo package
- section macro
- adaptation to new servlet3 package

* Mon Dec 17 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.61-4jpp
- javadoc in javadoc package
- doc reorganisation
- removed Requires: ant
- patches regenerated and bzipped

* Wed Nov 21 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.61-3jpp
- removed packager tag
- new jpp extension

* Fri Nov 09 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.61-2jpp
- added BuildRequires:	servletapi3 ant
- added Requires:	servletapi3 ant

* Fri Nov 09 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.61-1jpp
- complete spec restyle
- splitted & improved linuxization patch

* Fri Nov 09 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.60-1jpp
- 1.60 first "official release" of Hsqldb

* Fri Nov 09 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.43-2jpp
- fixed version

* Fri Nov 09 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.43-1jpp
- first release
- linuxization patch (doc + script)

