%global momorel 21

Summary: CannonSmash is a 3D tabletennis game.
Name: csmash
Version: 0.6.6
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/cannonsmash/csmash-%{version}.tar.gz 
NoSource: 0
Source1: csmash.desktop
Patch0: csmash-0.6.4-alpha.patch
Patch1: csmash-0.6.4-ttinc.h.patch
Patch2: csmash-types.patch
Patch3: csmash-0.6.6-64bit-fixes.patch
Patch4: csmash-0.6.6-gcc4.patch
Patch5: csmash-0.6.6-gcc41.patch
Patch6: csmash-0.6.6-acinclude.patch
License: GPLv2+
Group: Amusements/Games
URL: http://cannonsmash.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: mesa-libGL, mesa-libGLU, mesa-libGLw
Requires: libstdc++
Requires: libjpeg
Requires: zlib >= 1.1.4-5m
Requires: SDL_mixer
Requires: gtk2
BuildRequires: gcc-c++
BuildRequires: libvorbis-devel >= 1.0-2m
BuildRequires: libstdc++-devel
BuildRequires: libjpeg-devel
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: SDL_mixer-devel
BuildRequires: SDL_image-devel
BuildRequires: SDL >= 1.2.7-10m
BuildRequires: gtk2-devel
BuildRequires: desktop-file-utils
BuildRequires: mesa-libGL-devel, mesa-libGLU-devel, mesa-libGLw-devel
BuildRequires: libX11-devel, libXcursor-devel, libXext-devel, libXfixes-devel
BuildRequires: libXft-devel, libXi-devel, libXinerama-devel, libXmu-devel
BuildRequires: libXrandr-devel, libXrender-devel, libXt-devel

%description
CannonSmash is a 3D tabletennis game. The goal of this project is to represent
various strategy of tabletennis on computer game.

%prep
%setup -q
%ifarch alpha alphaev5
%patch0 -p1
%endif
%patch1 -p1 -b .ttinc
#%patch2 -p1 -b .types
%patch3 -p1 -b .64bit~
%patch4 -p1 -b .gcc4
%patch5 -p1 -b .gcc41
%patch6 -p1 -b .acinc

%build
autoreconf -fi
%configure
make

%install
rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

# desktop file
mkdir -p %{buildroot}/%{_datadir}/applications
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Game \
  --add-category ArcadeGame \
    %{SOURCE1}

install -m 644 win32/orange.ico %{buildroot}%{_datadir}/games/csmash/

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING CREDITS ChangeLog INSTALL NEWS README README.en
%{_bindir}/*
%{_datadir}/games/csmash
%{_datadir}/applications/*.desktop

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-21m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-18m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-17m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-15m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-14m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-13m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-12m)
- %%NoSource -> NoSource

* Sun Sep 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.6-11m)
- update csmash.desktop to UTF-8

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-10m)
- remove category Application

* Mon Mar 27 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.6-9m)
- change Mesa -> mesa-*

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-8m)
- revise for xorg-7.0
- add acinclude.m4 patch 

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-7m)
- add gcc41 patch
- Patch5: csmash-0.6.6-gcc41.patch
- change src URL. telia -> jaist

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-6m)
- add gcc4 patch
- Patch4: csmash-0.6.6-gcc4.patch

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.6-5m)
- enable x86_64.

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.6-4m)
- rebuild against SDL

* Wed Nov 10 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.6-3m)
- install desktop files with desktop-file-utils

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.6-2m)
- revised spec for enabling rpm 4.2.

* Fri Dec 12 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.5-3m)
  rebuild against DirectFB 0.9.18

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.5-2m)
- rebuild against zlib 1.1.4-5m

* Mon Dec 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5
- add Patch2: csmash-types.patch

* Sun Oct 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.6.4-12m)
- use gcc instead of gcc2_95_3

* Fri Aug 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.4-11m)
- transform='s,x,x,' in %makeinstall

* Sun Aug  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.4-10m)
- add 'csmash-0.6.4-ttinc.h.patch' to avoid build failure on ppc

* Sat Jul 20 2002 kourin <kourin@momonga-linux.org>
- (0.6.4-9m)
- add libvorbis-devel >= 1.0-2m

* Tue May 28 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.6.4-6k)
  use g++ 2.95.3

* Tue May 14 2002 Toru Hoshina <t@kondara.org>
- (0.6.4-4k)
- muuu... alpha...

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (0.6.4-2k)
- version up.

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.3-2k)
- up to 0.6.3

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.4.7-22k)
- add Tag

* Tue Apr 10 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.7-20k)
- errased Glide3 Glide3-devel from tag

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.4.7-19k)
- rebuild against audiofile-0.2.1.

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.7-17k)
- added csmash.as for afterstep

* Sun Jan 14 2001 Kenichi Matsubara <m@kondara.org>
- (0.4.7-15k)
- remove BuildPreReq: Mesa-devel.

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (0.4.7-13k)
- rebuild against gcc 2.96.

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.7-11k)
- imported csmash.desktop

* Thu Jan  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.7-9k)
- rebuild against Glide3

* Thu Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.7-7k)
- build against audiofile-0.2.0

* Thu Nov 23 2000 Toru Hoshina <toru@df-usa.com>
- I forgot about esd. tehe /(>_<)\

* Mon Oct 30 2000 Toru Hoshina <toru@df-usa.com>
- 1st release for Kondara.
