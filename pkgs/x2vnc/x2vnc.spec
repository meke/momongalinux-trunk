%global momorel 4

Name:          x2vnc 
Version:       1.7.2 
Release:       %{momorel}m%{?dist}
Summary:       Dual screen hack for VNC 

Group:         User Interface/X 
License:       GPLv2+
URL:           http://fredrik.hubbe.net/x2vnc.html
Source0:       http://fredrik.hubbe.net/x2vnc/%{name}-%{version}.tar.gz 
NoSource:      0
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0:        x2vnc_makefile.in.patch
BuildRequires: libXxf86dga-devel, libXrandr-devel, libXinerama-devel
BuildRequires: libXScrnSaver-devel

%description
This program will let you use two screens on two different computers
as if they were connected to the same computer even if 
one computer runs Windows. 

%prep
%setup -q
%patch0 -b .patch0
#Fix a file-not-utf8 warning in rpmlint
for file in README x2vnc.man ; do
  iconv -f ISO-8859-5 -t UTF-8 $file > $file.tmp && \
    mv $file.tmp $file || rm -f $file.tmp
done

%build
%configure
make %{?_smp_mflags}


%install 
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README ChangeLog COPYING
%{_bindir}/x2vnc
%{_mandir}/man1/x2vnc.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-2m)
- full rebuild for mo7 release

* Sun Dec 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2-1m)
- import from Rawhide

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.2-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Aug 12 2008 Jason L Tibbitts III <tibbs@math.uh.edu> - 1.7.2-9
- Fix %%patch without corresponding "Patch:" tag error which broke the build.

* Tue Aug 12 2008 Jason L Tibbitts III <tibbs@math.uh.edu> - 1.7.2-8
- Fix license tag.

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.7.2-7
- Autorebuild for GCC 4.3

* Sun Mar 04 2007 Michael Stahnke <mastahnke@gmail.com> - 1.7.2-6
- Fixed a bug in spec

* Sun Mar 04 2007 Michael Stahnke <mastahnke@gmail.com> - 1.7.2-5
- Cleaned up spec file 

* Sat Mar 03 2007 Michael Stahnke <mastahnke@gmail.com> - 1.7.2-4
- Removed ls and pwd from spec (was used for debugging).

* Sat Feb 24 2007 Michael Stahnke <mastahnke@gmail.com> - 1.7.2-3
- Fixing a few more items presented in Bug #228434.

* Tue Feb 20 2007 Michael Stahnke <mastahnke@gmail.com> - 1.7.2-2
- Fixing Items presented in Bug #228434.

* Tue Feb 12 2007 Michael Stahnke <mastahnke@gmail.com> - 1.7.2-1
- Initial packaging.
