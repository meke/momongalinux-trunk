%global momorel 9

Summary: A MOD music file player
Name: mikmod
Version: 3.2.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://mikmod.raphnet.net/
Source0: http://mikmod.raphnet.net/files/mikmod-%{version}-beta1.tar.gz
Patch0: mikmod-3.1.6-varargs.patch
Requires: ncurses
BuildRequires: libmikmod-devel >= 3.1.11
Requires: libmikmod >= 3.1.11
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
MikMod is one of the best and most well known MOD music file players for 
UNIX-like systems.  This particular distribution is intended to compile
fairly painlessly in a Linux environment. MikMod uses the OSS /dev/dsp
driver including all recent kernels for output, and will also write .wav
files. Supported file formats include MOD, STM, S3M, MTM, XM, ULT, and IT.
The player uses ncurses for console output and supports transparent
loading from gzip/pkzip/zoo archives and the loading/saving of playlists.

Install the mikmod package if you need a MOD music file player.

%prep
%setup -q -n %{name}-%{version}-beta1
%ifarch ia64 alpha sparc mipsel mips
%patch0 -p1
%endif

%build
%configure
make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL NEWS README
%{_prefix}/bin/*
%{_mandir}/man1/*
%{_datadir}/mikmod/mikmodrc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.2-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-4m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-3m)
- rebuild against libmikmod.so.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-2m)
- rebuild against gcc43

* Mon May 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sat Apr 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.6-15m)
- %%{prefix} -> %%{_prefix}

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.1.6-14m)
- enable ia64, alpha, sparc, mipsel

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (3.1.6-13m)
- rebuild against libtermcap and ncurses

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (3.1.6-12m)
- rebuild against ncurses 5.3.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.6-11m)
- put source into repository

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1.6-a-10k)
- updated Source0 URL.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Dec 17 1999 Yuichiro Orino <yuu_@pop21.odn.ne.jp>
- divide libmikmod package

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Aug  2 1999 Bill Nottingham <notting@redhat.com>
- add more patches

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- update to 3.1.6/3.1.7

* Mon Mar 22 1999 Cristian Gafton <gafton@redhat.com>
- fixed spec file description and group according to sepcspo

* Mon Mar 22 1999 Michael Maher <mike@redhat.com>
- changed spec file, updated package
- added libmikmod to the package

* Mon Feb 15 1999 Miodrag Vallat <miodrag@multimania.com>
- Created for 3.1.5

* Tue Jan 24 1999 Michael Maher <mike@redhat.com>
- changed group
- fixed bug #145

* Fri Sep 04 1998 Michael Maher <mike@redhat.com>
- added patch for alpha

* Wed Sep 02 1998 Michael Maher <mike@redhat.com>
- built package; obsoletes the ancient tracker program.

