%global momorel 12
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: KDE front-end of lm_sensors
Name: ksensors
Version: 0.7.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://ksensors.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: http://ftp.debian.org/debian/pool/main/k/%{name}/%{name}_%{version}-15.diff.gz
Patch2: %{name}-%{version}-po.patch
Patch3: %{name}-%{version}-fix-min-max.patch
Patch4: %{name}-%{version}-lm_sensors-3.x.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: lm_sensors
Requires: hddtemp
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: lm_sensors-devel >= 3.0.2
ExclusiveArch: %{ix86} x86_64 alpha

%description
KSensors is a nice lm-sensors frontend for the K Desktop Environment.
Install the hddtemp package if you wish to monitor hard disk
temperatures with KSensors.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1
%patch2 -p1 -b .po~
%patch3 -p1 -b .minmax
%patch4 -p1 -b .lm_sensors3x

perl -pi -e 's/\$\(kde_datadir\)/\$(datadir)/' src/sounds/Makefile.*

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category System \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog FAQ INSTALL LEEME LIESMICH
%doc LISEZMOI README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/sounds/%{name}_alert.wav

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-10m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-9m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-7m)
- rebuild against rpm-4.6

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-6m)
- rebuild against lm_sensors-3.0.2
- import fix-min-max.patch and lm_sensors-3.x.patch from Fedora
 +* Sun Nov 11 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.7.3-14
 +- Patch for and Rebuild against lm_sensors-3.0.0
 +* Sun Nov 11 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.7.3-13
 +- Fix reading of min and max tresholds from libsensors
- update debian's patch
- remove hddtemp-detect.patch

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.3-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-2m)
- %%NoSource -> NoSource

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-1m)
- initial package for Momonga Linux
- patches are imported from Fedora
