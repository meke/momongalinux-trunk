%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Summary: Command line tools for the Google Data APIs
Name: googlecl
Version: 0.9.13
Release: %{momorel}m%{?dist}
License: "ASL 2.0"
Group: Applications/Internet
URL: http://code.google.com/p/googlecl/
Source0: http://googlecl.googlecode.com/files/googlecl-%{version}.tar.gz
NoSource: 0
BuildRequires: python-devel >= 2.7
Requires: python-gdata

%description
GoogleCL brings Google services to the command line.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install -O1 --skip-build --root %{buildroot}
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 man/google.1 %{buildroot}%{_mandir}/man1/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.config README.txt changelog
%{_bindir}/google
%{_mandir}/man1/google.1*
%dir %{python_sitelib}/googlecl
%{python_sitelib}/googlecl/*.py*
%dir %{python_sitelib}/googlecl/blogger
%{python_sitelib}/googlecl/blogger/*.py*
%dir %{python_sitelib}/googlecl/calendar
%{python_sitelib}/googlecl/calendar/*.py*
%dir %{python_sitelib}/googlecl/config
%{python_sitelib}/googlecl/config/*.py*
%dir %{python_sitelib}/googlecl/contacts
%{python_sitelib}/googlecl/contacts/*.py*
%dir %{python_sitelib}/googlecl/docs
%{python_sitelib}/googlecl/docs/*.py*
%dir %{python_sitelib}/googlecl/finance
%{python_sitelib}/googlecl/finance/*.py*
%dir %{python_sitelib}/googlecl/picasa
%{python_sitelib}/googlecl/picasa/*.py*
%dir %{python_sitelib}/googlecl/youtube
%{python_sitelib}/googlecl/youtube/*.py*
%{python_sitelib}/googlecl-*.egg-info

%changelog
* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.13-1m)
- update to 0.9.13

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.12-1m)
- update 0.9.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-1m)
- initial packaging
