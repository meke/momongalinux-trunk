#
# It is this symbol if you want to use atok. Please make it 1.
#
%define WITHATOK 0
%global momorel 17

Summary: kinput2 is an input server for X11 applications that want Japanese text input.
Name: kinput2
Version: 3.1
Release: %{momorel}m%{?dist}
License: BSD
Group: User Interface/X
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source: ftp://ftp.sra.co.jp/pub/x11/%{name}/%{name}-v%{version}.tar.gz
Source101: utf8-locale-list.text
Patch0: kinput2-v3.1-conf.patch
Patch1: kinput2-v3-nn.patch
Patch2: kinput2-v3-utf8.patch
Patch3: kinput2.ppc.patch
Patch4: kinput2.sawfish-hack.patch
Patch5: kinput2-override_redirect.patch
Patch6: kinput2-v3.1-beta4-atok.patch
Requires: Canna >= 3.6, Canna-library >= 3.6, libwnn7
BuildRequires: Canna-library, Canna-devel, libwnn7, libwnn7-devel
BuildRequires: imake
%if %WITHATOK
BuildRequires: atoklib
%endif
NoSource: 0

%description
Kinput2 is an input server for X11 applications that want Japanese
text input.

A client that wants kana-kanji conversion service for Japanese text
sends a request to kinput2.  Kinput2 receives the request, does
kana-kanji conversion, and sends the converted text back to the
client.

%prep
%setup -n kinput2-v3.1 -q
%patch0 -p1 -b .conf
%patch1 -p1
%patch2 -p1
%ifarch ppc
%patch3 -p1
%endif
%patch4 -p 1
%patch5 -p1 -b .override_redirect
%if %WITHATOK
%patch6 -p1 -b .atok
%endif

chmod 644 cmd/Kinput2.ad
echo "*IMProtocol.locales: ja_JP.SJIS, ja_JP, japanese, japan, \\" >> cmd/Kinput2.ad
for i in `cat $RPM_SOURCE_DIR/utf8-locale-list.text`; do
	echo "  $i.UTF-8, \\" >> cmd/Kinput2.ad
done
echo "  ja" >> cmd/Kinput2.ad
chmod 444 cmd/Kinput2.ad

%build
xmkmf
make Makefiles
make depend
make

%define ximdir /etc/X11/xinit/xim.d
%define imhost .xinit.d/im_host

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR="%{buildroot}" install
make DESTDIR="%{buildroot}" install.man
# gzip -9 %{buildroot}/usr/X11R6/man/man1/kinput2.1x

mkdir -p %{buildroot}%{ximdir}

cat <<END1 > %{buildroot}/%{ximdir}/Canna
NAME="Canna/kinput2"
LANGUAGE="Japanese"
IFEXISTS="/var/lock/subsys/canna:/usr/bin/kinput2"
IM_HOST=unix
if [ -f \$HOME/%{imhost} ]; then
	IM_HOST=\`awk -F: '{print \$1}' \$HOME/%{imhost}\`
fi
IM_EXEC="kinput2 -canna -cannaserver \$IM_HOST"
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
XMODIFIERS=@im=kinput2
export IM_EXEC XMODIFIERS GTK_IM_MODULE QT_IM_MODULE
END1

cat <<END2 > %{buildroot}/%{ximdir}/FreeWnn-kinput2
NAME="FreeWnn/kinput2"
LANGUAGE="Japanese"
IFEXISTS="/var/lock/subsys/FreeWnn:/usr/bin/kinput2"
IM_HOST=localhost
if [ -f \$HOME/%{imhost} ]; then
	IM_HOST=\`awk -F: '{print \$1}' \$HOME/%{imhost}\`
fi
IM_EXEC="kinput2 -wnn -jserver \$IM_HOST -wnnenvrc4 /etc/FreeWnn/ja/wnnenvrc -xrm Kinput2.ConversionManager.IMProtocol.ServerName:kinput2wnn"
XMODIFIERS=@im=kinput2wnn
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
export IM_EXEC XMODIFIERS GTK_IM_MODULE QT_IM_MODULE
END2

cat <<END3 > %{buildroot}/%{ximdir}/Wnn6-kinput2
NAME="Wnn6/kinput2"
LANGUAGE="Japanese"
IFEXISTS="/usr/local/OMRONWnn6/Wnn6linux/jserver:/usr/bin/kinput2"
IM_HOST=localhost
if [ -f \$HOME/%{imhost} ]; then
	IM_HOST=\`awk -F: '{print \$1}' \$HOME/%{imhost}\`
fi
if [ -f /usr/local/OMRONWnn6/wnn6linux/ja_JP/wnnenvrc ]; then
	WNNENVRC=/usr/local/OMRONWnn6/wnn6linux/ja_JP/wnnenvrc
elif [ -f /etc/wnn6/ja_JP/wnnenvrc ]; then
	WNNENVRC=/etc/wnn6/ja_JP/wnnenvrc
else
	WNNENVRC=""
fi
if [ "\$WNNENVRC" = "" ]; then
	IM_EXEC="kinput2 -wnn -jserver \$IM_HOST -xrm Kinput2.ConversionManager.IMProtocol.ServerName:kinput2wnn6"
else
	IM_EXEC="kinput2 -wnn -jserver \$IM_HOST -wnnenvrc6 \$WNNENVRC -xrm Kinput2.ConversionManager.IMProtocol.ServerName:kinput2wnn6"
fi
XMODIFIERS=@im=kinput2wnn6
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
export IM_EXEC XMODIFIERS GTK_IM_MODULE QT_IM_MODULE
END3

# clean up
rm -rf %{buildroot}/usr/lib*/X11/app-defaults

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README NEWS doc/*
%config %{_sysconfdir}/X11/xinit/xim.d/*
%dir %{_sysconfdir}/kinput2
%config %{_sysconfdir}/kinput2/*
%config %{_datadir}/X11/app-defaults/Kinput2
%{_bindir}/kinput2
%{_mandir}/man1/kinput2.1x.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-15m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1-14m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.1-12m)
- add BR imake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-10m)
- rebuild against gcc43

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1-9m)
- revise spec file for rpm-4.4.2

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1-8m)
- use libwnn7(not libwnn6) (kinput2-v3.1-conf.patch)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1-7m)
- revise for xorg-7.0
- change install dir

* Sun Jan  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.1-6m)
- rebuild against libwnn6-3.0-17m. libwnn6's SONAME was changed.
#'

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1-5m)
- change xim.d/{Canna,FreeWnn-kinput2,Wnn6-kinput2} (add QT_IM_MODULE)

* Fri Nov  5 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (3.1-4m)
- change xim.d/{Canna,FreeWnn-kinput2,Wnn6-kinput2} (add GTK_IM_MODULE)

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-3m)
- edit spec to include /etc/X11/app-defaults/Kinput2

* Wed Nov 27 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- use gcc 3.x
- Requires: Canna >= 3.6

* Wed Nov 27 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-1m)
- update src from 3.1-beta4 to 3.1
- adapt changes of Canna 3.6 
  IM_HOST=localhost to IM_HOST=unix
  (thanks to Masahiko Tsuruta:[Momonga-devel.ja:00890])
- NoSource: 0

* Tue Mar  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1-0.00040006k)
- a override_redirect patch merged from STABLE_2_1

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (3.1-0.00040004k)
- NoNoSource..

* Fri Feb 15 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1-0.00040002k)
- update to v3.1-beta4
  added atok support switch.

* Fri Dec 21 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (3-28k)
  override_redirect is True on modeShell and selectionShell.

* Fri Nov 23 2001 Motonobu Ichimura <famao@kondara.org>
- (3-26k)
- added LANGUAGE support for latest ximswitch

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (3-24k)
- rebuild against new environment. because gcc 2.95.3 doesn't recognize
  -fno-merge-constants, use gcc_2_95_3 instead of gcc -V 2.95.3.
#'

* Thu May 24 2001 Akira Higuchi <a@kondara.org>
- (3-22k)
- added sawfish-hack.patch

* Wed May 23 2001 Toru Hoshina <toru@df-usa.com>
- (3-20k)
- force gcc 2.95.3.

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3-19k)
- modified Required tag from Canna to Canna-library

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3-17k)
- modified IFEXISTS file on wnn6 to /usr/local/OMRONWnn6/Wnn6linux/jserver

* Thu Oct 26 2000 Kenichi Matsubara <m@kondara.org>
- man.gz to man.bz2.

* Wed Aug 02 2000 Akira Higuchi <a@kondara.org>
- some fixes in xim scripts

* Sun Jul 23 2000 Akira Higuchi <a@kondara.org>
- set different server names for kinput2-wnn and kinput2-wnn6 so that
  they can coexist on the same display.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed May 24 2000 Akira Higuchi <a@kondara.org>
- support for utf8 locales

* Tue Feb 1 2000 Norihito Ohmori <nono@konodara.org>
- change group
- change FreeWnn-kinput2 for new FreeWnn.

* Sat Dec 08 1999 Shingo Akagaki <dora@kondara.org>
- fixed the part FreeWnn-kinput2 for checking FreeWnn is working.

* Mon Nov 29 1999 Hidetomo Machi <mcHT@kondara.org>
- fixed the part Wnn6-kinput2 for checking Wnn6 is working.

* Sat Nov 27 1999 Akira Higuchi <a@kondara.org>
- modified the xim scripts according to the changes in the xinitrc
  package.
- fixed the problem that some macros and awk scripts in xim scripts,
  are evaluated at build time.

* Thu Nov 25 1999 Norihito Ohmori <nono@kondara.org>
- fix /etc/X11/im/* bug with wdm

* Thu Nov 18 1999 Norihito Ohmori <nono@kondara.org>
- add NEWS in %doc
- change Copyright
- add Require: and BuildPreReq: tag
- use dynamic link library for Wnn6

* Mon Oct 25 1999 Jun NISHII <jun@flatout.org>
- added defattr

* Thu Oct 14 1999 Jun NISHII <jun@flatout.org>
- build for Vine-1.9

* Thu Aug 12 1999 Norihito Ohmori <ohmori@flatout.org>
- change conversion from nn -> n.

* Wed Jul 8 1999 Norihito Ohmori <ohmori@flatout.org>
- version up to kinpu2v3
- support both Canna and Wnn6

* Mon Jan 18 1999 ZUKERAN, shin <shin@ryukyu.ad.jp>
- Group changed:
  Japanese/X11 -> X11/Applications
- Version up to kinput2-v2-fix5-alpha5
