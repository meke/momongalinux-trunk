%global momorel 3

Summary: Video processing tool
Name: avidemux
Version: 2.5.6
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia

URL: http://www.avidemux.org/
Source0: http://download.berlios.de/%{name}/%{name}_%{version}.tar.gz
NoSource: 0
Source1: %{name}.desktop
Source2: %{name}.png
Patch2: avidemux-2.5.3-pluginlibs.patch
Patch3: avidemux-2.5.3-mpeg2enc.patch
# see http://avidemux.org/admForum/viewtopic.php?id=7819
Patch4: avidemux-2.5.3-gcc45.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel >= 1.2.7-11m
BuildRequires: alsa-lib-devel
BuildRequires: arts-devel
BuildRequires: cmake >= 2.8.9
BuildRequires: desktop-file-utils
BuildRequires: faad2-devel >= 2.7
BuildRequires: ffmpeg-devel >= 0.10
BuildRequires: fontconfig-devel
BuildRequires: gtk2-devel
BuildRequires: lame-devel
BuildRequires: libid3tag-devel
BuildRequires: libmad
BuildRequires: libvorbis-devel
BuildRequires: mjpegtools-devel >= 1.6.0-5m
BuildRequires: qt-devel >= 4.7.1
BuildRequires: slang-devel
BuildRequires: x264-devel >= 0.0.2358
BuildRequires: xvid
BuildRequires: libva-devel >= 1.0
BuildRequires: libxml2-devel
BuildRequires: zlib-devel
BuildRequires: gettext-devel
BuildRequires: mesa-libGL-devel
BuildRequires: libvpx-devel >= 1.0.0

%description 
Avidemux is a graphical tool to edit video. It can open AVI, MPEG,
Nuppelvideo and BMPs. Most common codecs are supported (M-JPEG, MPEG,
DivX, Xvid, huffyuv, WMA, etc.) thanks to libavcodec. Video can be
edited, cut, appended, filtered (resize/crop/denoise), and re-encoded
to either AVI (DivX/Xvid) or MPEG 1/2.

%prep
%setup -q -n %{name}_%{version}
%patch2 -p1 -b .plugin
%patch3 -p1 -b .mpeg2enc
%patch4 -p1 -b .gcc45~

# libdir is nicely hardcoded
sed -i 's,Dir="lib",Dir="%{_lib}",' avidemux/main.cpp avidemux/ADM_core/src/ADM_fileio.cpp
grep -q '"%{_lib}"' avidemux/main.cpp
grep -q '"%{_lib}"' avidemux/ADM_core/src/ADM_fileio.cpp

%build
case "`gcc -dumpversion`" in
4.6.*)
	# avidemux.spec-2.5.4 requires this
	CFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	CXXFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	;;
esac
export CFLAGS CXXFLAGS
mkdir build
pushd build
export LIBDIR=%{_libdir}
PATH="%{_qt4_bindir}:$PATH" %{cmake} ..
# FIX ME: 2.5.6 broken paralell build
# make %{?_smp_mflags} VERBOSE=1 
make VERBOSE=1 

# plugin build expects libraries to be already installed; we fake a prefix
# in build/ by symlinking all libraries to build/%%{_lib}
mkdir -p %{_lib}
pushd %{_lib}
find ../avidemux -name '*.so*' | xargs ln -sft . 
popd

pushd ../plugins
mkdir build
pushd build
PATH="%{_qt4_bindir}:$PATH" %{cmake} \
    -DAVIDEMUX_SOURCE_DIR=%{_builddir}/%{name}_%{version} \
    -DAVIDEMUX_CORECONFIG_DIR=%{_builddir}/%{name}_%{version}/build/config \
    -DAVIDEMUX_INSTALL_PREFIX=%{_builddir}/%{name}_%{version}/build \
    ..
make %{?_smp_mflags} VERBOSE=1
popd
popd

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
pushd build
make DESTDIR=%{buildroot} install
popd

mkdir -p %{buildroot}%{_libdir}

pushd plugins/build
make DESTDIR=%{buildroot} install
popd

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps/
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/

# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category AudioVideoEditing \
  --add-category AudioVideo \
  %{SOURCE1}

pushd %{buildroot}%{_bindir}
ln -s avidemux2_gtk avidemux2
ln -s avidemux2_gtk avidemux
popd

# where shoud it go?
rm -rf %{buildroot}%{_bindir}/i18n

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_bindir}/avidemux
%{_bindir}/avidemux2
%{_bindir}/avidemux2_cli
%{_bindir}/avidemux2_gtk
%{_bindir}/avidemux2_qt4
%{_libdir}/libADM*.so*
%{_libdir}/ADM_plugins
%{_datadir}/ADM_addons
%{_datadir}/ADM_scripts
%{_datadir}/%{name}/i18n
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/applications/avidemux.desktop
%{_datadir}/pixmaps/avidemux.png

%changelog
* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.6-3m)
- rebuild against x264

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.6-2m)
- add BuildRequires: cmake >= 2.8.9

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.6-1m)
- update 2.5.6

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.5-9m)
- rebuild against libvpx-1.0.0

* Mon Dec 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.5-8m)
- specify version and release of ffmpeg

* Mon Dec 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.5-7m)
- rebuild against x264-2120-0.20111215

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.5-6m)
- rebuild against x264-2106-0.20111024

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.5-5m)
- rebuild against libva

* Mon Aug  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.5-4m)
- fix a possible build failure on x86_64; we must use %%{_lib} not "lib"

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.5-3m)
- rebuild against x264 and ffmpeg

* Thu Jul 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.5-2m)
- fix BuildRequires

* Tue Jul  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.5-1m)
- update 2.5.5

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.4-5m)
- rebuild against x264 and ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.4-5m)
- rebuild for new GCC 4.6

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.4-4m)
- rebuild against x264-1913

* Mon Feb 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.4-3m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.4-1m)
- update 2.5.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-10m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-9m)
- specify PATH for Qt4

* Sun Oct 31 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-8m)
- add patch for gcc45

* Fri Oct 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.3-7m)
- rebuild against x264

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.3-5m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.3-4m)
- rebuild against x264

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-3m)
- rebuild against qt-4.6.3-1m

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.3-2m)
- fix build error

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.3-1m)
- update 2.5.3

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.2-7m)
- rebuild against ffmpeg and x264

* Sun Apr 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.2-6m)
- add BuildRequires: libva-devel

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.2-5m)
- rebuild against x264-0.0.1523

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.2-4m)
- rebuild against x264-0.0.1471

* Thu Feb 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.2-3m)
- rebuild against x264-0.0.1406-0.20100222.1m

* Sat Jan 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.2-2m)
- remove lib64 hack

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1 and enable plugins
-- import some hacks from PLF (2.5.1-2plf2010.0)

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-10m)
- rebuild against x264

* Mon Aug 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-9m)
- rebuild against x264

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-8m)
- rebuild against x264

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.3-7m)
- fix up broken avidemux/CMakeLists.txt to enable build with new cmake
- http://bugs.gentoo.org/show_bug.cgi?id=268618

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.3-6m)
- rebuild against faad2-2.7

* Mon Apr 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-5m)
- apply gcc44 patch from RPM Fusion (2.4.4-5)

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-4m)
- rebuild against x264 ffmpeg

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-3m)
- rebuild against x264 ffmpeg

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-2m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-1m)
- update 2.4.3
- rebuild against x264 ffmpeg

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-6m)
- rebuild against x264

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-5m)
- rebuild against qt-4.4.0-1m

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-4m)
- rebuild against x264

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-3m)
- rebuild against gcc43

* Fri Mar 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.1-2m)
- import two patches from gentoo
-- http://bugs.gentoo.org/show_bug.cgi?id=213099

* Thu Feb 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1
- no longer annoying firefox dependency

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-19m)
- rebuild against firefox-2.0.0.12

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-18m)
- rebuild against x264-0.0.712-0.20071219

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.0-17m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.0-16m)
- rebuild against firefox-2.0.0.10

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.0-15m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.0-14m)
- rebuild against firefox-2.0.0.8

* Wed Sep 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-13m)
- rebuild against firefox-2.0.0.7

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-12m)
- rebuild against libvorbis-1.2.0-1m

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.0-11m)
- add Requires for firefox

* Mon Aug  6 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.3.0-10m)
- update firefox_ver
- rebuild against 2.0.0.6

* Tue Jul 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.3.0-9m)
- update firefox_ver

* Sat Jun  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.0-8m)
- rebuild against firefox

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3.0-7m)
- rebuild against x264

* Thu Mar 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.0-6m)
- update firefox_ver

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.0-5m)
- update firefox_ver

* Tue Mar 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-4m)
- add gcc-4.2 patch
-- avidemux-2.3.0-gcc42.patch

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.3.0-3m)
- enable ppc64

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-2m)
- revise %%files to avoid conflicting
- %%{_datadir}/locale/*/LC_MESSAGES is already provided by glibc-common

* Fri Dec 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-1m)
- update 2.3.0

* Mon Nov 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.2-7m)
- copy from momonga's trunk (r12695)
- use firefox-devel instead of mozilla-devel

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-6m)
- good-bye autoreconf

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-5m)
- remove category Application

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.1.2-4m)
- rebuild against expat-2.0.0-1m

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.2-3m)
- revised run script

* Sun Apr 23 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.2-2m)
- rebuild against mozilla-1.7.13
- revised spec

* Fri Mar 10 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.2-1m)
- version 2.1.2

* Sun Feb  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.42-2m)
- use gcc_3_2 on 64bit archs

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.42-1m)
- update 2.0.42
- add gcc-4.1 patch.
- Patch0:  avidemux-2.0.42-gcc41.patch

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.38-0.1.2m)
- rebuild against SDL-1.2.7-11m

* Fri Feb 25 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.38-0.1.1m)
  update to 2.0.38rc1

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.36-2m)
- move avidemux.desktop to %%{_datadir}/applications/
- BuildPreReq: desktop-file-utils

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.0.36-1m)
- update to 2.0.36.

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.30-3m)
- rebuild against SDL-devel

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (2.0.30-2m)
- rebuild with lame, xvid.

* Mon Oct 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.30-1m)
- update to 2.0.30

* Fri Oct 01 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.28-1m)
- update to 2.0.28

* Sun May  2 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.22-1m)
- update to 2.0.22
- rebuild against xvid-1.0.0rc4 (apiversion 4)

* Tue Jan  6 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.20-1m)
- version 2.0.20
- Major feature enhancements
- - the UI has changed a lot (the shortcut keys have changed)

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.18-2m)
- use lidmad

* Wed Nov  5 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.18-1m)
- major feature enhancements

* Sat Sep 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.16-1m)
- major feature enhancements

* Thu Aug 21 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.14-1m)
- update to 2.0.14
- major bugfixes
- use %%NoSource macro

* Thu Aug  7 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.12-1m)
- gtk2 version
- This is still development version, but according to the project homepage,
-  "0.9 branch is becoming really obsolete and has problems dealing with
-  newer libxml and libdivx libraries"
-  add BuildPreReq: xvid, mad-devel
- minor feature enhancements
- more detailed description

* Wed Apr 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-1m)
- minor bugfixes

* Sun Mar 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.103.1m)
- 0.9rc3
- major bugfixes

* Sun Mar  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.102.1m)
- 0.9rc2
- major bugfixes

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.101.1m)
- 0.9rc1
- major feature enhancements

* Tue Jan 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.30.1m)
- 0.9pre30
- major feature enhancements

* Thu Jan 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9-0.26.2m)
- apply an official patch for freetype2-2.1.3

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9-0.26.1m)
- 0.9pre26
