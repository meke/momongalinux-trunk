%global         momorel 1

Name:           perl-Sys-Virt
Version:        1.2.5
Release:        %{momorel}m%{?dist}
Summary:        Represent and manage a libvirt hypervisor connection
License:        GPL+
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Sys-Virt/
Source0:        http://www.cpan.org/authors/id/D/DA/DANBERR/Sys-Virt-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Sys-Hostname
BuildRequires:  perl-Test-Pod
BuildRequires:  perl-Test-Pod-Coverage
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Time-HiRes
BuildRequires:  perl-XML-XPath
BuildRequires:  libvirt-devel >= %{version}
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Sys::Virt module provides a Perl XS binding to the libvirt virtual
machine management APIs. This allows machines running within arbitrary
virtualization containers to be managed with a consistent API.

%prep
%setup -q -n Sys-Virt-%{version}

%build
rm -f perl-Sys-Virt.spec.PL
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS Changes HACKING INSTALL LICENSE perl-Sys-Virt.spec README
%{perl_vendorarch}/auto/Sys/Virt
%{perl_vendorarch}/Sys/Virt.pm
%{perl_vendorarch}/Sys/Virt
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- rebuild against perl-5.20.0
- update to 1.2.5

* Tue Apr  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-2m)
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-2m)
- rebuild against perl-5.18.1

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-2m)
- rebuild against perl-5.18.0

* Wed May 15 2013 narita Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-2m)
- rebuild against perl-5.16.3

* Sun Mar 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (!.0.2-1m)
- update to 1.0.2

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-2m)
- rebuild against perl-5.16.2

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.13-2m)
- rebuild against perl-5.16.1

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.13-1m)
- update to 0.9.13

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.12-1m)
- update to 0.9.12
- rebuild against perl-5.16.0

* Fri Feb 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.10-1m)
- update to 0.9.10

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Wed Jan  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-2m)
- rebuild against perl-5.14.2

* Mon Aug 08 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Sun Jul  3 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.6-1m)
- import from Fedora 15

* Wed Feb 16 2011 Daniel P Berrange <berrange@berrange.com> - 0.2.6-2
- Workaround Test::More's inability to cast XML::XPath::Number to an int

* Wed Feb 16 2011 Daniel P Berrange <dan@berrange.com> - 0.2.6-1
- Update to 0.2.6 release

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Dec 22 2010 Marcela Maslanova <mmaslano@redhat.com> - 0.2.4-3
- 661697 rebuild for fixing problems with vendorach/lib

* Sat Aug 14 2010 Daniel P. Berrange <berrange@redhat.com> - 0.2.4-2
- Rebuild against perl 5.12.0
- Fix hostname test

* Wed May 19 2010 Daniel P. Berrange <berrange@redhat.com> - 0.2.4-1
- Update to 0.2.4 release

* Thu May 06 2010 Marcela Maslanova <mmaslano@redhat.com> - 0.2.3-2
- Mass rebuild with perl-5.12.0

* Fri Jan 15 2010 Daniel P. Berrange <berrange@redhat.com> - 0.2.3-1
- Update to 0.2.3 release

* Mon Dec  7 2009 Stepan Kasal <skasal@redhat.com> - 0.2.1-2
- rebuild against perl 5.10.1

* Wed Aug 26 2009 Stepan Kasal <skasal@redhat.com> - 0.2.1-1
- new upstream version
- remove upstreamed patch

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Mar 30 2009 Stepan Kasal <skasal@redhat.com> - 0.2.0-2
- BR: libvirt >= 0.6.1

* Mon Mar 30 2009 Stepan Kasal <skasal@redhat.com> - 0.2.0-1
- new upstream version (#237421)

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Mar 07 2008 Daniel P. Berrange <berrange@redhat.com> - 0.1.2-3
- Fix calls to free() in XS binding

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.1.2-2
Rebuild for new perl

* Wed Mar 05 2008 Steven Pritchard <steve@kspei.com> 0.1.2-1
- Update to 0.1.2.
- Drop Sys-Virt-doc.patch.
- BR XML::XPath.
- No longer need to BR pkgconfig or xen-devel.
- Disable 100-connect test.

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.1-10
- Autorebuild for GCC 4.3

* Tue Apr 17 2007 Steven Pritchard <steve@kspei.com> 0.1.1-9
- BR ExtUtils::MakeMaker.

* Tue Apr 17 2007 Steven Pritchard <steve@kspei.com> 0.1.1-8
- Use fixperms macro instead of our own chmod incantation.

* Mon Aug 28 2006 Steven Pritchard <steve@kspei.com> 0.1.1-7
- Rebuild.

* Sat Aug 19 2006 Steven Pritchard <steve@kspei.com> 0.1.1-6
- More documentation fixes.

* Fri Aug 18 2006 Steven Pritchard <steve@kspei.com> 0.1.1-5
- Make this spec work on FC5 or FC6.
- Add ExclusiveArch to match xen and libvirt.

* Fri Aug 18 2006 Steven Pritchard <steve@kspei.com> 0.1.1-4
- BR xen-devel.

* Fri Aug 18 2006 Steven Pritchard <steve@kspei.com> 0.1.1-3
- BR Test::Pod and Test::Pod::Coverage.

* Sat Aug 12 2006 Steven Pritchard <steve@kspei.com> 0.1.1-2
- Add Sys-Virt-Domain-doc.patch.

* Sat Aug 12 2006 Steven Pritchard <steve@kspei.com> 0.1.1-1
- Specfile autogenerated by cpanspec 1.68.
- BR libvirt-devel and pkgconfig.
- Fix License.
- Drop non-doc autobuild.sh and add the examples directory.
- Don't try to build the included perl-Sys-Virt.spec.
