%global momorel 5
%global xmmsinputdir %{_libdir}/xmms/Input
%global build_xmms 0

Summary: Library and frontend for decoding MPEG2/4 AAC.
Name: faad2
Version: 2.7
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.audiocoding.com/
Source0: http://dl.sourceforge.net/sourceforge/faac/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gtk2-devel
BuildRequires: id3lib-devel
BuildRequires: libtool
BuildRequires: libsndfile-devel >= 1.0.0
BuildRequires: xmms-devel
BuildRequires: zlib-devel
%if ! %{build_xmms}
Obsoletes: xmms-aac
%endif

%description
FAAD 2 is a LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder, completely
written from scratch.

%if %{build_xmms}
%package -n xmms-aac
Summary: X MultiMedia System input plugin to play AAC files.
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}, xmms, id3lib

%description -n xmms-aac
This xmms plugin reads AAC files with and without ID3 tags (version 2.x).
AAC files are MPEG2 or MPEG4 files that can be found in MPEG4 audio files(.mp4). MPEG4 files with AAC inside can be read by RealPlayer or Quicktime.
%endif

%package devel
Summary: Development libraries of the FAAD 2 AAC decoder.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
FAAD 2 is a LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder, completely written from scratch.

This package contains development files and documentation for libfaad.

%prep
%setup -q 

autoreconf -vif

%build
%configure \
	--disable-static \
%if %{build_xmms}
	--with-xmms \
%endif
	--with-drm

%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

# Remove this wrong include
%{__perl} -pi -e 's|#include <systems.h>||g' \
    %{buildroot}%{_includedir}/mpeg4ip.h

rm -f %{buildroot}%{_libdir}/libmp4ff.a

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README* TODO
%{_bindir}/faad
%{_libdir}/libfaad.so.*
%{_mandir}/manm/faad.man.bz2

%if %{build_xmms}
%files -n xmms-aac
%defattr(-,root,root,-)
%doc plugins/xmms/AUTHORS plugins/xmms/ChangeLog plugins/xmms/NEWS
%doc plugins/xmms/README plugins/xmms/TODO
%exclude %{xmmsinputdir}/*.la
%{xmmsinputdir}/*.so
%endif

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%exclude %{_libdir}/libfaad.la
%{_libdir}/libfaad.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7-1m)
- version 2.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-2m)
- rebuild against rpm-4.6

* Mon Nov 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-1m)
- version 2.6.1
- License: GPLv2
- replace Patch100 to faad2-2.6.1-main-overflow.patch from gentoo
- remove rejected buildfix.patch
- clean up spec file

* Mon Nov 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5-4m)
- [SECURITY] CVE-2008-4201
- apply main-overflow.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-2m)
- %%NoSource -> NoSource

* Thu Apr 12 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.5-1m)
- update to 2.5
- sync with freshrpms.net, but backward compatibility is left.
- 
- * Mon Jan  8 2007 Matthias Saou <http://freshrpms.net/> 2.5-2
- - Add patch to remove backwards compatibility in the header so that we can
-   easily identify and patch all programs requiring a rebuild.
- * Fri Dec 15 2006 Matthias Saou <http://freshrpms.net/> 2.5-1
- - Update to 2.5.
- - Completely remove xmms/bmp plugin, it's a real mess anyway. Use audacious.
- - Rip out libmp4v2 too, it's best as a separate package.
- - Add libsysfs-devel build requirement, as it seems configure checks for it.

* Mon Mar 27 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- change xmmsinputdir

* Tue Nov  8 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0-7m)
- update to 20050513 cvs version
- add gcc4 patch (Patch0)

* Sat May  7 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-6m)
- delete /usr/lib/libmp4v2.so.0.0.0, /usr/include/mp4.h, 
  /usr/include/mpeg4ip.h, /usr/lib/libmp4v2.a from files list
  use faac
- add --with-mpeg4ip and --disable-static at configure
- delete --with-mpeg4ip (go to next change)
- use cvs Source0
- - delete Patch0: faad2-Makefile.tab.patch
- - delete Patch1: faad2-2.0-gcc34.patch
- - delete Patch2: faad2-2.0-xmms.patch

* Mon May  2 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-5m)
- fix xmms plugin crash

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.0-4m)
- enable x86_64.

* Sun Aug 22 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0-3m)
- rebuild against gcc-c++-3.4.1
- add gcc34 patch

* Wed Jun 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-2m)
- correct the directory name

* Sat May 29 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0-1m)
- import from paken.

* Fri Jan 31 2004 Takeru Komoriya <komoriya@paken.org>
- Repackaged based on faad2-2.0-rc1_1.rhfc1.at

* Tue Aug 12 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 2.0rc1.
- Introduced LD_LIBRARY_PATH workaround.
- Removed optional xmms plugin build, it seems mandatory now.
- Added gtk+ build dep for the xmms plugin.

* Wed May 14 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Added xmms plugin build.

* Wed Apr  9 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Spec file cleanup.
- Now exclude .la file.
- Update to latest CVS checkout to fix compile problem.

* Fri Aug 10 2002 Alexander Kurpiers <a.kurpiers@nt.tu-darmstadt.de>
- changes to compile v1.1 release

* Tue Jun 18 2002 Alexander Kurpiers <a.kurpiers@nt.tu-darmstadt.de>
- First RPM.

