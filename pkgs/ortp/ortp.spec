%global         momorel 2

Name:           ortp
Version:        0.18.0
Release:        %{momorel}m%{?dist}
Summary:        A C library implementing the RTP protocol (RFC3550)
Group:          System Environment/Libraries
License:        "LGPLv2+ and VSL"
URL:            http://www.linphone.org/index.php/code_review/ortp
Source0:        http://download.savannah.nongnu.org/releases/linphone/%{name}/sources/%{name}-%{version}.tar.gz
#NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  doxygen
BuildRequires:  graphviz
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  openssl-devel >= 1.0.0

%description
oRTP is a C library that implements RTP (RFC3550).

%package        devel
Summary:        Development libraries for ortp
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
Libraries and headers required to develop software with ortp.

%prep
%setup0 -q

%{__perl} -pi.dot  -e 's/^(HAVE_DOT\s+=)\s+NO$/\1 YES/;s/^(CALL_GRAPH\s+=)\s+NO$/\1 YES/;s/^(CALLER_GRAPH\s+=)\s+NO$/\1 YES/' ortp.doxygen.in

%build
libtoolize --copy --force
aclocal
autoheader
automake --add-missing --copy
autoconf
%configure --disable-static --enable-ipv6
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name \*.la -exec rm {} \;
rm doc/html/html.tar
rm -r %{buildroot}%{_datadir}/doc/ortp

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog TODO
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/html
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/ortp.pc

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.18.0-2m)
- rebuild against graphviz-2.36.0-1m

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18.0-1m)
- update to 0.18.0

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.5-1m)
- update to 0.16.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.3-2m)
- rebuild for new GCC 4.6

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3-1m)
- update to 0.16.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16.1-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.1-2m)
- rebuild against openssl-1.0.0

* Tue Dec  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.1-1m)
- update to 0.16.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.0-2m)
- rebuild against openssl-0.9.8k

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.1-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.1-1m)
- import from Fedora devel

* Mon Aug 11 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1:0.14.2-0.3.20080211
- fix license tag
- epoch bump to fix pre-release versioning

* Thu Feb 14 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.14.2-0.20080211.2%{?dist}
- Update to 0.14.2 snapshot

* Tue Feb  5 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.14.1-0.20080123.2
- Apply patch to remove -Werror from the build (for PPC).

* Fri Feb  1 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.14.1-0.20080123.1
- Update to 0.14.1 (using CVS snapshot until official release is available).

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.13.1-4
- Rebuild for selinux ppc32 issue.

* Fri Jun 22 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.1-2
- Fix URL

* Mon Apr 23 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.1-1
- Update to 0.13.1
- BR doxygen and graphviz for building documentation

* Mon Jan 29 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.0-1
- Update to 0.13.0
- ortp-devel BR pkgconfig
- Add ldconfig scriptlets

* Tue Nov 21 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.12.0-1
- Update to 0.12.0

* Mon Oct  9 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.0-2
- Bring back -Werror patch (needed for building on PPC)

* Mon Oct  9 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.0-1
- Update to 0.11.0
- Remove ortp-0.8.1-Werror.patch

* Wed Aug 30 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.8.1-3
- Bump release and rebuild

* Mon Feb 13 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.1-2
- Rebuild for Fedora Extras 5

* Tue Jan  3 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.1-1
- Upstream update

* Thu Dec 22 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.1-2
- Added ortp.pc to -devel

* Sat Dec  3 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.1-1
- Upstream update

* Wed Nov 30 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.0-6
- Fix a typo in Requires on -devel

* Wed Nov 30 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.0-5
- Add missing Requires on -devel

* Sun Nov 13 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.0-4
- Split from linphone
