%global momorel 1

%define __os_install_post    /usr/lib/rpm/momonga/brp-compress /usr/lib/rpm/momonga/brp-strip %{__strip} /usr/lib/rpm/momonga/brp-strip-comment-note %{__strip} %{__objdump} %{nil}

Name: mjpegtools
Version: 2.1.0
Release: %{momorel}m%{?dist}
Summary: Tools for recording, editing, playing back and mpeg-encoding video under linux
License: GPL
URL: http://mjpeg.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/mjpeg/mjpegtools-%{version}.tar.gz
NoSource: 0
Source2: http://dl.sourceforge.net/sourceforge/mjpeg/libmovtar-0.1.3.tar.gz
NoSource: 2
Source3: http://dl.sourceforge.net/sourceforge/mjpeg/jpeg-mmx-0.1.6.tar.gz
NoSource: 3
Patch3: jpeg-mmx-0.1.6-gcc34.patch
Patch4: libmovtar-0.1.3-gcc34.patch
Patch5: libmovtar-0.1.3-gcc4.patch
Patch7: movtar.m4.patch
Patch100: mjpegtools-1.8.0-fix_constraint_in_inline_asm.diff
Patch102: mjpegtools-1.9.0rc2-kernel2625.patch
#Patch103: mjpegtools-1.9.0-glibc210.patch
Patch104: mjpegtools-1.9.0-cpuinfio.patch
#Patch200: %{name}-%{version}-header-fix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel >= 1.2.7-11m
BuildRequires: SDL_gfx-devel >= 2.0.20
BuildRequires: arts-devel
BuildRequires: esound-devel
BuildRequires: freetype-devel
BuildRequires: libdv >= 0.98-1m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: gcc-c++ >= 4.1.1-35m
BuildRequires: libquicktime-devel
BuildRequires: nasm
BuildRequires: kernel-headers >= 2.6.39-1m

%description
The MJPEG-tools are a basic set of utilities for recording, editing, 
playing back and encoding (to mpeg) video under linux. Recording can
be done with zoran-based MJPEG-boards (LML33, Iomega Buz, Pinnacle
DC10(+), Marvel G200/G400), these can also playback video using the
hardware. With the rest of the tools, this video can be edited and
encoded into mpeg1/2 or divx video.

%package devel
Summary: Development headers and libraries for the mjpegtools
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains static libraries and C system header files
needed to compile applications that use part of the libraries
of the mjpegtools package.

%prep
%setup -q -n %{name}-%{version} -a 2 -a 3
%patch3 -p0 -b .gcc34
%patch4 -p0 -b .gcc34
%patch5 -p0 -b .gcc4

mv libmovtar-* libmovtar

%patch7 -p0 -b .m4~

%patch100 -p1 -b .gcc41~
%patch102 -p1 -b .kernel2625~
###%patch103 -p1 -b .glibc210
%patch104 -p1 -b .cpuinfo

#%patch200 -p1 -b .remove_config_h

mkdir usr

%build
tmp_prefix="`pwd`/usr"
mkdir -p $tmp_prefix/{include,lib,bin,share}

CFLAGS="`echo ${CFLAGS:-%optflags}|sed -e 's, -funroll-loops,,'|sed -e 's, -fstack-protector,,'|sed -e 's, -mtune=generic,,'`" ; export CFLAGS

CXXFLAGS="${CXXFLAGS:-%optflags}" ; export CXXFLAGS

# add -fpermissive
# build error gcc-4.1
case "`gcc -dumpversion`" in
4.1.*)
export CXXFLAGS="$CXXFLAGS -fpermissive"
  ;;
esac

%ifarch %{ix86}
(cd jpeg-mmx && ./configure)
make -C jpeg-mmx libjpeg-mmx.a
%endif

%ifarch %{ix86}
CONF_ARGS="--with-jpeg-mmx=`pwd`/jpeg-mmx"

(cd libmovtar && 
   ./configure --prefix="$tmp_prefix" \
	--with-m4data-prefix="$tmp_prefix/share/aclocal" \
	$CONF_ARGS )

make -C libmovtar
make -C libmovtar install
%endif

CONF_ARGS="$CONF_ARGS --prefix=%{_prefix} --libdir=%{_libdir}"

%ifarch %{ix86}
(cd libmovtar&& ./configure $CONF_ARGS)
make -C libmovtar
%endif

CONF_ARGS="$CONF_ARGS --with-quicktime"
%ifarch %{ix86}
CONF_ARGS="$CONF_ARGS --with-movtar-prefix=$tmp_prefix"
MOVTAR_CONFIG=$tmp_prefix/bin/movtar-config; export MOVTAR_CONFIG
%else
CONF_ARGS="$CONF_ARGS --disable-simd-accel"
%endif
./configure $CONF_ARGS --enable-large-file --enable-cmov-extension
%make

%install
rm -rf %{buildroot}
%ifarch %{ix86}
%makeinstall -C libmovtar m4datadir=%{buildroot}%{_datadir}/aclocal
%endif
%makeinstall
# remove unnecessary dependencies
perl -p -i -e 's, -ljpeg-mmx,,' %{buildroot}%{_libdir}/*la
perl -p -i -e "s, -L`pwd`/jpeg-mmx,," %{buildroot}%{_libdir}/*la
perl -p -i -e 's, -L/usr/local/lib,,' %{buildroot}%{_libdir}/*la
rm -f %{buildroot}%{_bindir}/multiblend.flt
rm -f %{buildroot}%{_infodir}/dir

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/mjpeg-howto.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/mjpeg-howto.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS BUGS CHANGES COPYING HINTS PLANS README TODO
%{_bindir}/pgmtoy4m
%{_bindir}/yuvfps
%{_bindir}/yuvinactive
%{_bindir}/glav
%{_bindir}/jpeg2yuv
%{_bindir}/lav2avi.sh
%{_bindir}/lav2mpeg
%{_bindir}/lav2wav
%{_bindir}/lav2yuv
%{_bindir}/lavaddwav
%{_bindir}/lavinfo
%{_bindir}/lavpipe
%{_bindir}/lavplay
###%{_bindir}/lavrec
%{_bindir}/lavtc.sh
%{_bindir}/lavtrans
###%{_bindir}/lavvideo
%{_bindir}/matteblend.flt
%{_bindir}/mp2enc
%{_bindir}/mpeg2enc
%{_bindir}/mpegtranscode
%{_bindir}/mplex
%{_bindir}/png2yuv
%{_bindir}/ppmtoy4m
###%{_bindir}/testrec
%{_bindir}/transist.flt
%{_bindir}/y4mblack
%{_bindir}/y4mcolorbars
%{_bindir}/y4mshift
%{_bindir}/y4mtoppm
%{_bindir}/y4mtoyuv
%{_bindir}/y4mdenoise
%{_bindir}/y4mhist
%{_bindir}/y4minterlace
%{_bindir}/y4mivtc
%{_bindir}/y4mspatialfilter
%{_bindir}/y4mstabilizer
%{_bindir}/y4mscaler
%{_bindir}/y4mtopnm
%{_bindir}/y4munsharp
%{_bindir}/y4mtoqt
%{_bindir}/qttoy4m
%{_bindir}/ypipe
%{_bindir}/yuv2lav
%{_bindir}/yuv4mpeg
%{_bindir}/yuvcorrect
%{_bindir}/yuvcorrect_tune
%{_bindir}/yuvdenoise
%{_bindir}/yuvkineco
%{_bindir}/yuvmedianfilter
%{_bindir}/yuvplay
%{_bindir}/yuvscaler
%{_bindir}/yuvycsnoise
%{_bindir}/yuvdeinterlace
%{_bindir}/yuyvtoy4m
%{_bindir}/anytovcd.sh
%{_bindir}/mjpeg_simd_helper
%{_bindir}/pnmtoy4m
%{_libdir}/*.so.*
%{_mandir}/man1/*
%{_mandir}/man5/*
%ifarch %{ix86}
%{_bindir}/movtar_index
%{_bindir}/movtar_play
%{_bindir}/movtar_setinfo
%{_bindir}/movtar_split
%{_bindir}/movtar_unify
%{_bindir}/movtar_yuv422
%{_bindir}/pnm2rtj
%{_bindir}/rtjshow
%endif
%{_infodir}/mjpeg-howto.info*

%files devel
%defattr(-,root,root)
%ifarch %{ix86}
%{_bindir}/movtar-config
%endif
%{_includedir}/mjpegtools
%ifarch %{ix86}
%{_includedir}/movtar.h
%{_datadir}/aclocal/*.m4
%endif
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.a
%{_libdir}/*.so

%changelog
* Sat Jan 25 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-1m)
- update 2.1.0

* Sun Jun  5 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-3m)
- add BuildRequires: kernel-headers >= 2.6.39-1m
  for avoid build error
  error: Installed (but unpackaged) file(s) found:
   /usr/bin/lavrec
   /usr/bin/lavvideo
   /usr/bin/testrec

* Sat Jun  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-2m)
- remove config.h from mplex/stream_params.hpp to enable build gstreamer-plugins-bad

* Fri Jun  3 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-11m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.9.0-10m)
- fix for AMD CPU

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-9m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-8m)
- rebuild against libjpeg-8a

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-7m)
- modify __os_install_post for new momonga-rpmmacros

* Sun Dec 27 2009 NARITA Koichi <pulsar@momnga-linux.org>
- (1.9.0-6m)
- rebuild against SDL_gfx-2.0.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-4m)
- apply glibc210 patch

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-3m)
- rebuild against libjpeg-7

* Sat Aug 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0-2m)
- add BPR nasm

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0 official release

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-0.2.10m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-0.2.9m)
- run install-info

* Wed Apr 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-0.2.8m)
- add patch for kernel-2.6.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0-0.2.7m)
- rebuild against gcc43

* Sun Feb  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-0.2.6m)
- switch to use libquicktime-devel
- remove unused patches

* Mon Dec  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-0.2.5m)
- add patch for gcc43

* Sat Jun 30 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.0-0.2.4m)
- modify quicktime4linux $OBJDIR

* Sat Apr 14 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.9.0-0.2.3m)
- revised EM64T patch
- thanks to maxwel, see Momonga-devel.ja:03469

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.0-0.2.2m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.2.1m)
- change version 

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0rc2-1m)
- update 1.9.0rc2

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-10m)
- delete libtool library

* Sun Jan  7 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-9m)
- mjpegtools-1.8.0-workaround-gcc-bug29535.patch is no longer required.

* Sun Dec 24 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-8m)
- Added mjpegtools-1.8.0-workaround-gcc-bug29535.patch.

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.8.0-7m)
- rebuild against expat-2.0.0-1m

* Mon Jul 31 2006 Nishio Futoshi <futoshi@momonga-linuc.org>
- (1.8.0-6m)
- remove patch1

* Mon Jan  9 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (1.8.0-5m)
- fixed build error gcc-4.1
   jpeg-mmx/jquant_x86simd.c: use specific operand constraint instead of almighty operand constraint "X"
   quicktime4linux/libdv/mmx.h: use specific operand constraint instead of almighty operand constraint "X"

* Sun Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-4m)
- fix build error gcc-4.1
-- if used gcc-4.x
--  export CXXFLAGS="$CXXFLAGS -fpermissive"

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.0-3m)
- add Patch8: mjpegtools-1.8.0-EM64T.patch (for EM64T Pentium4/D & Xeon)

* Tue Nov 22 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-2m)
- fixed not ix86 arch.

* Wed Nov 18 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.8.0-1m)
- version up
- update Patch3: jpeg-mmx-0.1.6-gcc34.patch
- remove Patch5: mjpegtools-1.6.2-gcc34.patch
         Patch6: mjpegtools-1.6.2-unsigned_long_long.patch
- add    Patch5: libmovtar-0.1.3-gcc4.patch

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.2.12m)
- suppress AC_DEFUN warning.

* Sat Feb 26 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.6.2-11m)
- append ULL to unsigned long long immediate values;

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.2-10m)
- rebuild against SDL

* Sun Feb 20 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6.2-9m)
- enable x86_64.
- fix *.la

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.2-8m)
- rebuild against SDL-1.2.7-10m

* Sun Feb  6 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6.2-7m)
- enable ppc.

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.6.2-6m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++
- add gcc34 patch

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.2-5m)
- build against SDL-1.2.7-3m
- add Patch1:  mjpegtools-1.6.2-kernel2681.patch

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.2-4m)
- rebuild against DirectFB-0.9.21

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.2-3m)
- rebuild against libdv-0.102

* Sat May  1 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.2-2m)
- revised spec for rpm 4.2.1.

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.2-1m)
- update mjepgtools to 1.6.2, jpeg-mmx to 0.1.5
- - ommit -fstak-protector from CFLAGS (avoid internal compiler error in making lavtools/yuvscaler)
- - *divx* tools has been replaced by lav2avi.sh
- - some new tools

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.1-2m)
- revised spec for enabling rpm 4.2.

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.1-1m)
  update to 1.6.1(rebuild against DirectFB 0.9.18)

* Sat Mar 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.6.0-10m)
- rebuild against for XFree86-4.3.0

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-9m)
- add defattr at files devel section

* Fri Jan 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-8m)
- BuildPreReq: libdv >= 0.98-1m for libdv.so.2

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-7m)
- rebuild against for libdv

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-6m)
- add BuildPreReq: libdv >= 0.9.5-9m for /usr/lib/libdv.so

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-5m)
- remove unnecessary dependencies from '*.la'

* Sun Dec 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-4m)
- revise for ppc and spec cleanup

* Sat Nov  2 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6.0-3m)
- add --with-dv-prefix=%{_includedir}/libdv

* Wed Oct 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-2m)
- enable dv and firewire on building quicktime4linux

* Tue Oct 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-1m)

* Tue Feb 12 2002 Geoffrey T. Dairiki <dairiki@dairiki.org>
- Fix spec file to build in one directory, etc...

* Thu Dec 06 2001 Ronald Bultje <rbultje@ronald.bitfreak.net>
- separated mjpegtools and mjpegtools-devel
- added changes by Marcel Pol <mpol@gmx.net> for cleaner RPM build

* Wed Jun 06 2001 Ronald Bultje <rbultje@ronald.bitfreak.net>
- 1.4.0-final release, including precompiled binaries (deb/rpm)
