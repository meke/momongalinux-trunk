%global momorel 2
Name:           gtk-sharp2
Version:        2.12.11
Release: %{momorel}m%{?dist}
Summary:        GTK+ and GNOME bindings for Mono

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.mono-project.com/GtkSharp
Source0:        http://origin-download.mono-project.com/sources/gtk-sharp212/gtk-sharp-%{version}.tar.bz2
NoSource: 0
Patch1:         gtk-sharp2-2.12.11-glib-include.patch

BuildRequires:  mono-devel gtk2-devel libglade2-devel monodoc-devel
BuildRequires:  automake, libtool

Obsoletes:	gtk-sharp2-atk
Obsoletes:	gtk-sharp2-libglade2
Obsoletes:	gtk-sharp2-glib2
Obsoletes:	gtk-sharp2-pango


# Mono only available on these:
ExclusiveArch: %ix86 x86_64 ppc ppc64 ia64 %{arm} sparcv9 alpha s390x

%description
This package provides a library that allows you to build
fully native graphical GNOME applications using Mono. Gtk#
is a binding to GTK+, the cross platform user interface
toolkit used in GNOME. It includes bindings for Gtk, Atk,
Pango, Gdk. 

%package gapi
Group:        Development/Languages
Summary:      Glib and GObject C source parser and C generator for the creation and maintenance of managed bindings for Mono and .NET
Requires:     perl-XML-LibXML perl-XML-SAX

%description gapi
This package provides developer tools for the creation and
maintenance of managed bindings to native libraries which utilize
glib and GObject. Some examples of libraries currently bound using
the GAPI tools and found in Gtk# include Gtk, Atk, Pango, Gdk.

%package devel
Summary:      Files needed for developing with gtk-sharp2
Group:        Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package provides the necessary development libraries and headers
for writing gtk-sharp2 applications.

%package doc
Group:        Documentation
Summary:      Gtk# documentation
Requires:     %{name} = %{version}-%{release}
Requires:     monodoc

%description doc
This package provides the Gtk# documentation for monodoc.

%prep
%setup -q -n gtk-sharp-%{version}
%patch1 -p1 -b .glib-include

# Fix permissions of source files
find -name '*.c' -exec chmod a-x {} \;

%build
export MONO_SHARED_DIR=%{_builddir}/%{?buildsubdir}
%configure
make

%install
export MONO_SHARED_DIR=%{_builddir}/%{?buildsubdir}
make install DESTDIR=%{buildroot}

#Remove libtool archive
%{__rm} %{buildroot}%{_prefix}/%{_lib}/*.*a

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_libdir}/*.so
%dir %{_prefix}/lib/gtk-sharp-2.0
%{_prefix}/lib/mono/gac
%{_prefix}/lib/mono/gtk-sharp-2.0

%files gapi
%defattr(-,root,root,-)
%{_bindir}/gapi2-codegen
%{_bindir}/gapi2-fixup
%{_bindir}/gapi2-parser
%{_prefix}/lib/gtk-sharp-2.0/gapi_codegen.exe
%{_prefix}/lib/gtk-sharp-2.0/gapi-fixup.exe
%{_prefix}/lib/gtk-sharp-2.0/gapi-parser.exe
%{_prefix}/lib/gtk-sharp-2.0/gapi_pp.pl
%{_prefix}/lib/gtk-sharp-2.0/gapi2xml.pl
%{_datadir}/gapi-2.0
%{_libdir}/pkgconfig/gapi-2.0.pc

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/*-sharp-2.0.pc
%{_libdir}/pkgconfig/gtk-dotnet-2.0.pc

%files doc
%defattr(-,root,root,-)
%{_prefix}/lib/monodoc/sources/*

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.11-2m)
- rebuild for mono-2.10.9

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.11-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.10-9m)
- rebuild for glib 2.33.2

* Tue Mar 13 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.10-8m)
- build fix

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.10-7m)
- add Requires
-- .pc files in %%{name}-devel needs .dll files in the other sub pkgs.

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.10-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.10-5m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.10-4m)
- rebuild against mono-2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.10-3m)
- full rebuild for mo7 release

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.10-2m)
- add %%dir

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.10-1m)
- update to 2.12.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.9-3m)
- fix build on x86_64

* Thu Oct 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.9-2m)
- split package

* Sun May 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.9-1m)
- update to 2.12.9

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.8-1m)
- update to 2.12.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.7-3m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.7-2m)
- update Patch0 for fuzz=0
- License: LGPLv2+

* Sat Dec 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.7-1m)
- update to 2.12.7

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-3m)
- modify patch0

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-2m)
- add patch0 for gacutil

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-1m)
- update to 2.12.6

* Thu Oct 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-1m)
- update to 2.12.4

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Wed Apr 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.0-2m)
- rebuild against gcc43

* Wed Mar  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Mon Sep 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2
- Change Source URL

* Fri Jul  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.1-2m)
- import patch from Debian to fix gmime build problem

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.0-4m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-3m)
- rename gtk-sharp -> gtk-sharp2

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-2m)
- delete patch0 (gtk+ version up)

* Tue Aug 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0
-- split devel package

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.3-2m)
- rebuild against gnome
- delete libtool library

* Mon Aug 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.2-3m)
- revised for multilibarch

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2-2m)
- rebuild mono -> mono-core

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2

* Thu Feb 02 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.8.0-3m)
- add patch3 because of hard-coding path in some sources.

* Wed Feb 01 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.8.0-2m)
- add and revised patches for x86_64:
-         monodoc-2.8.0-config.patch
-         monodoc-2.8.0-pc.patch
-         monodoc-2.8.0-lib64.patch
- change from "%make" to "make". because of building fail on SMP env.

* Sun Jan 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-1m)
- start.
