%global momorel 6
%global srcname wxMaxima

Summary: wxWidgets interface for Maxima
Name: wxmaxima
Version: 0.7.5
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Engineering
URL: http://wxmaxima.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2, libxml2, wxGTK-unicode
BuildRequires: libxml2-devel, wxGTK-unicode-devel
BuildRequires: desktop-file-utils

%description
wxMaxima is a wxWidgets interface for the computer algebra system Maxima.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure --with-wx-config=/usr/bin/wx-config-unicode
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'
mkdir -p %{buildroot}%{_datadir}/applications
cat > %{name}.desktop <<EOF
[Desktop Entry]
Name=wxMaxima
GenericName=wxMaxima Maxima GUI Frontend
Comment=Perform symbolic and numeric calculations using Maxima
Exec=wxmaxima
Icon=wxmaxima.png
Terminal=0
Type=Application
Encoding=UTF-8
EOF
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --add-category Engineering \
  --add-category Science \
  --add-category Math \
    ./%{name}.desktop
install -D -m 644 wxmaxima.png %{buildroot}/%{_datadir}/pixmaps/wxmaxima.png

%find_lang %{srcname}

%clean
rm -rf %{buildroot}

%files -f %{srcname}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/wxmaxima
%{_datadir}/wxMaxima
%{_datadir}/pixmaps/wxmaxima.png
%{_datadir}/applications/wxmaxima.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.5-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-2m)
- rebuild against rpm-4.6

* Thu May 15 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.4-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.4-1m)
- import to Momonga

* Fri Jun 23 2006 Andrej Vodopivec <andrejv@users.sourceforge.net>
- Updated for wxMaxima 0.6.6

* Wed Dec 15 2004 Andrej Vodopivec <andrejv@users.sourceforge.net>
- Added french translation files.

* Wed Aug 25 2004 Andrej Vodopivec <andrejv@users.sourceforge.net>
- Initial spec file.
