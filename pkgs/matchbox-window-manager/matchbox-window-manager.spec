%global momorel 5
%global	srcdate 20071114

Summary:       Window manager for the Matchbox Desktop
Name:          matchbox-window-manager
Version:       1.2
Release:       0.%{srcdate}.%{momorel}m%{?dist}
License:       GPLv2+
Group:         User Interface/Desktops
URL:           http://projects.o-hand.com/matchbox/
# svn checkout http://svn.o-hand.com/repos/matchbox/trunk/matchbox-window-manager
Source0:       %{name}-%{version}-%{srcdate}.tar.xz
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       filesystem
BuildRequires:  expat-devel
BuildRequires:  libXcomposite-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXdamage-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXrender-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libmatchbox-devel >= 1.9
BuildRequires:  libpng-devel
BuildRequires:  pango-devel
BuildRequires:  pkgconfig 
BuildRequires:  startup-notification-devel

%description
Matchbox is a base environment for the X Window System running on non-desktop
embedded platforms such as handhelds, set-top boxes, kiosks and anything else
for which screen space, input mechanisms or system resources are limited.

This package contains the window manager from Matchbox.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README
%dir %{_sysconfdir}/matchbox
%config(noreplace) %{_sysconfdir}/matchbox/kbdconfig
%{_bindir}/matchbox-remote
%{_bindir}/matchbox-window-manager
%{_datadir}/matchbox
%{_datadir}/themes/Default/matchbox
%{_datadir}/themes/MBOpus
%{_datadir}/themes/blondie

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-0.20071114.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-0.20071114.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-0.20071114.3m)
- full rebuild for mo7 release

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.20071114.2m)
- touch up spec file

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-0.20071114.1m)
- Initial commit Momonga Linux. import from fedora.
