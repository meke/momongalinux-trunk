%global momorel 1

Summary: visual diff and merge tool
Name: meld
Version: 1.8.5
Release: %{momorel}m%{?dist}
License: GPL
URL: http://meld.sourceforge.net/
Group: Applications/Productivity
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/1.8/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): hicolor-icon-theme gtk2

%description
Meld is a visual diff and merge tool. You can compare two or three
files and edit them in place (diffs update dynamically). You can
compare two or three folders and launch file comparisons. You can
browse and view a working copy from popular version control systems
such such as CVS, Subversion, Bazaar-ng and Mercurial. Look at the
screenshots page for more detailed features.

%prep
%setup -q

%build
make

%install
rm -rf --preserve-root %{buildroot}
make prefix=%{buildroot}/usr libdir=%{buildroot}%{_libdir} install
rm -rf %{buildroot}/usr/var/lib/scrollkeeper
sed --in-place 's,\/local\/lib,\/%{_lib},' %{buildroot}%{_bindir}/meld
sed --in-place 's,\/local\/share,\/share,' %{buildroot}%{_libdir}/meld/meld/paths.py
rm -f %{buildroot}%{_libdir}/meld/paths.py[co]
sed --in-place 's,\/local\/share,\/share,' %{buildroot}%{_datadir}/omf/%{name}/meld-C.omf
sed --in-place 's,\/local\/share,\/share,' %{buildroot}%{_datadir}/omf/%{name}/meld-es.omf

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files
%defattr(-,root,root)
%doc COPYING INSTALL NEWS
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/mimeinfo.cache
%{_datadir}/gnome/help/%{name}
%{_datadir}/omf/%{name}/%{name}-*.omf
%{_datadir}/icons/HighContrast/scalable/apps/meld.svg
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/locale/*/*/*
%{_datadir}/mime/*

%changelog
* Sat Jun  7 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update to 1.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-2m)
- full rebuild for mo7 release

* Fri Jun 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1
- add patch1 (why not update ja.po?)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Fri Jun 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.5.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5.1-2m)
- %%NoSource -> NoSource

* Thu Jun 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5.1-1m)
- update to 1.1.5.1

* Sun Jun 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-2m)
- delete paths.py[co] (bad path)

* Sun Jun 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Sat Oct 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.4-2m)
- fix lib64 problem

* Thu Oct 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- initial build


