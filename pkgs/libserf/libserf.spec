%global momorel 1
%global oname   serf

Name:           libserf
Version:        1.2.1
Release:        %{momorel}m%{?dist}
Summary:        High-Performance Asynchronous HTTP Client Library
License:        ASL 2.0
Group: 		System Environment/Libraries
Url:            http://code.google.com/p/serf
Source0:        https://serf.googlecode.com/files/serf-%{version}.tar.bz2
NoSource:	0
Patch0:		libserf-HEAD.patch
BuildRequires:  autoconf
BuildRequires:  apr-util-devel
BuildRequires:  apr-devel
BuildRequires:  krb5-devel
BuildRequires:  libtool
BuildRequires:  libgssapi-devel
BuildRequires:  openssl-devel
BuildRequires:  pkgconfig
BuildRequires:  zlib-devel

%description
The serf library is a C-based HTTP client library built upon the Apache 
Portable Runtime (APR) library. It multiplexes connections, running the
read/write communication asynchronously. Memory copies and transformations are
kept to a minimum to provide high performance operation.

%package        devel
Summary:        Development files for %{name}
Group:		Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       apr-devel%{?_isa}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{oname}-%{version}

%patch0 -p1

%ifarch x86_64 ppc64 ia64
sed -i 's|$PREFIX/lib|$PREFIX/lib64|g' SConstruct
%endif

sed -i "s|ccflags = [ ]|ccflags = [%{optflags}]|g" SConstruct

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"
scons %{?_smp_mflags}

%install
rm -rf %{buildroot}
#make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}/%{_prefix}
scons install PREFIX=%{buildroot}/%{_prefix}
find %{buildroot} -name '*.*a' -exec rm -f {} ';'

#%%check
#%scons check

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc CHANGES LICENSE NOTICE README design-guide.txt
#%%{_libdir}/*.so.*
%{_libdir}/*.so

%files devel
%{_includedir}/%{oname}-2/
#%%{_libdir}/*.so
%{_libdir}/pkgconfig/%{oname}*.pc

%changelog
* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-2m)
- update serf-HEAD.patch
- 1.2.1 broken subversion-HTTP(Momonga Repo,etc...

* Fri Jun 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- initial commit Momonga Linux
- use subversion-1.8

* Mon Jun 17 2013 Christopher Meng <rpm@cicku.me> - 1.2.1-3
- SPEC cleanup.

* Thu Jun 13 2013 Christopher Meng <rpm@cicku.me> - 1.2.1-2
- Fix the permission of the library.

* Sun Jun 09 2013 Christopher Meng <rpm@cicku.me> - 1.2.1-1
- Initial Package.
