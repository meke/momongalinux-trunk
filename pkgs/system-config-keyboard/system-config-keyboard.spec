%global momorel 9

%global debug_package %{nil}
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           system-config-keyboard
Version:        1.3.1
Release:        %{momorel}m%{?dist}
Summary:        A graphical interface for modifying the keyboard

Group:          System Environment/Base
License:        GPLv2+
URL:            https://fedorahosted.org/system-config-keyboard/
Source0:        https://fedorahosted.org/releases/s/y/system-config-keyboard/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  intltool

Requires:       python
Requires:       usermode >= 1.36
Requires:       dbus-python

Obsoletes:      kbdconfig
Obsoletes:      redhat-config-keyboard
Patch0:         s-c-keyboard-do_not_remove_the_OK_button.patch
Patch1:		sck-1.3.1-no-pyxf86config.patch


%description
system-config-keyboard is a graphical user interface that allows 
the user to change the default keyboard of the system.


%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
make


%install
rm -rf %{buildroot}
make INSTROOT=%{buildroot} install
desktop-file-install --vendor system --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/system-config-keyboard.desktop

%find_lang %{name}


%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi


%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi


%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
%{_sbindir}/system-config-keyboard
%{_bindir}/system-config-keyboard
%{_datadir}/system-config-keyboard
#%attr(0755,root,root) %dir %{_datadir}/firstboot/modules
%{_datadir}/firstboot/modules/*
%attr(0644,root,root) %{_datadir}/applications/system-config-keyboard.desktop
%attr(0644,root,root) %config %{_sysconfdir}/security/console.apps/system-config-keyboard
%attr(0644,root,root) %config %{_sysconfdir}/pam.d/system-config-keyboard
%attr(0644,root,root) %{_datadir}/icons/hicolor/48x48/apps/system-config-keyboard.png
%{python_sitelib}/system_config_keyboard


%changelog
* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-9m)
- not use pyxf86config

* Sat Sep 24 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-8m)
- sync Fedora
- modify spec

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-7m)
- add patch

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-3m)
- full rebuild for mo7 release

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- remove Requires: rhpl

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-1m)
- update 1.3.1

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.15-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.15-2m)
- rebuild against rpm-4.6

* Tue May 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.15-1m)
- update 1.2.15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-2m)
- rebuild against gcc43

* Tue Feb 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-1m)
- update 1.2.11

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.7-3m)
- remove category X-Red-Hat-Base SystemSetup Application

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.7-2m)
- delete duplicated dir

* Tue May  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-1m)
- update 1.2.7
- remove Requires newt-python, kudzu
- add Prereq gtk+

* Sun May  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- Require kudzu-python -> kudzu

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.5-2m)
- add System to Categories of desktop file for KDE

* Mon Dec  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.5-1m)
  update to 1.2.5

* Fri Oct 08 2004 Paul Nasrat <pnasrat@redhat.com> - 1.2.5-1
- Firstboot fix for xorg.conf

* Fri Oct 01 2004 Paul Nasrat <pnasrat@redhat.com> - 1.2.4-1
- Translations 

* Wed Sep 22 2004 Jeremy Katz <katzj@redhat.com> - 1.2.3-1
- fix traceback when using treeview typeahead (#133178)

* Tue Sep 07 2004 Paul Nasrat <pnasrat@redhat.com> 1.2.2-1
- i18n desktop

* Thu Jun 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.1-3m)
- add Requires: newt-python and kudzu-python

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.1-2m)
- import from Fedora.

* Thu Apr  8 2004 Brent Fox <bfox@redhat.com> 1.2.1-2
- fix icon path (bug #120175)

* Wed Nov 12 2003 Brent Fox <bfox@redhat.com> 1.2.1-1
- renamed from redhat-config-keyboard
- add Obsoletes for redhat-config-keyboard
- make changes for Python2.3

* Mon Oct 13 2003 Brent Fox <bfox@redhat.com> 1.1.5-2
- pull in Croatian translation (bug #106617)

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.1.5-1
- tag on every build

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.1.3-2
- bump release and rebuild

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.1.3-1
- mark string for translation

* Tue Jul  1 2003 Brent Fox <bfox@redhat.com> 1.1.2-2
- bump version and rebuild

* Tue Jul  1 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- fix formatting problem (bug #97873)
- create an XkbOption if it doesn't exist (bug #97877)

* Wed May 21 2003 Brent Fox <bfox@redhat.com> 1.1.1-2
- made a typo in redhat-config-keyboard.py

* Wed May 21 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- Created a command line interface
- updated all copyright dates

* Wed Apr  2 2003 Brent Fox <bfox@redhat.com> 1.0.5-1
- pass window size into rhpl

* Wed Mar  5 2003 Brent Fox <bfox@redhat.com> 1.0.4-1
- add functions in keyboard_gui.py for anaconda popup mode

* Wed Feb 12 2003 Jeremy Katz <katzj@redhat.com> 1.0.3-4
- fixes for tui cjk (#83518)

* Tue Feb  4 2003 Brent Fox <bfox@redhat.com> 1.0.3-3
- change packing order a little for firstboot reconfig mode

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.3-2
- bump and build

* Wed Jan 22 2003 Brent Fox <bfox@redhat.com> 1.0.3-1
- add a us keymap to keymaps with non-latin chars (bug #82440)
- write out the XkbVariant line if it is present
- handle XkbOptions to allow toggling between keymaps

* Mon Dec 23 2002 Brent Fox <bfox@redhat.com> 1.0.2-1
- add a textdomain for rhpl so we pull in translations (bug #78831)

* Thu Dec 12 2002 Brent Fox <bfox@redhat.com> 1.0.1-9
- remove requires for pygtk2 since we have a text mode now

* Wed Dec 11 2002 Brent Fox <bfox@redhat.com> 1.0.1-8
- fall back to text mode if gui mode fails

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.0.1-7
- pam path changes

* Thu Nov 07 2002 Brent Fox <bfox@redhat.com> 1.0.1-6
- Add keyboard_backend.py to cvs

* Thu Oct 31 2002 Brent Fox <bfox@redhat.com> 1.0.1-5
- Obsolete kbdconfig

* Wed Oct 09 2002 Brent Fox <bfox@redhat.com> 1.0.1-4
- Added a tui mode - keyboard_tui.py
- Moved some non-UI code to keyboard_backend.py

* Fri Oct 04 2002 Brent Fox <bfox@redhat.com> 1.0.1-3
- Add a window icon
- set selection mode to browse

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-1
- Make no arch

* Wed Aug 14 2002 Brent Fox <bfox@redhat.com> 0.9.9-6
- rebuild for translations

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-5
- fix textdomain so translations show up correctly

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-4
- pull translations into desktop file

* Mon Aug 12 2002 Tammy Fox <tfox@redhat.com> 0.9.9-3
- Replace System with SystemSetup in desktop file categories

* Sun Aug 11 2002 Brent Fox <bfox@redhat.com> 0.9.9-2
- Fix ordering of layout and model.  Fixes bug 71067

* Thu Aug 08 2002 Brent Fox <bfox@redhat.com> 0.9.9-1
- Added Requires for pyxf86config

* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Make changes for new pam timestamp policy

* Thu Aug 01 2002 Brent Fox <bfox@redhat.com> 0.9.7-2
- sort the list by the full keyboard name, not the keymap

* Thu Aug 01 2002 Brent Fox <bfox@redhat.com> 0.9.7-1
- make calls to pyxf86config to update XF86Config file

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-3
- fix Makefiles and spec files so that translations get installed

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- update spec file for public beta 2

* Tue Jul 23 2002 Brent Fox <bfox@redhat.com> 0.9.6-1
- put desktop file in correct location

* Mon Jul 22 2002 Jeremy Katz <katzj@redhat.com> 0.9.5-2
- add scrollto hack back

* Fri Jul 19 2002 Brent Fox <bfox@redhat.com> 0.9.5-1
- add version dependency for pygtk2 API change

* Thu Jul 18 2002 Jeremy Katz <katzj@redhat.com> 0.9.4-3
- add fix for list store changes in new pygtk2

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- bump rev num and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-2
- Update changelogs and rebuild

* Mon Jul  1 2002 Jeremy Katz <katzj@redhat.com> 0.9.3-1
- add wacky scrollto hack so that the screen in the installer scrolls properly

* Mon Jul 01 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Bump rev number

* Mon Jul 1 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Wrap the destroy call in a try/except because there is no self.mainWindow in firstboot reconfig mode

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Fixed description

* Tue Jun 25 2002 Brent Fox <bfox@redhat.com> 0.9.0-5
- Create pot file

* Mon Jun 24 2002 Brent Fox <bfox@redhat.com> 0.9.0-4
- Fix spec file

* Fri Jun 21 2002 Brent Fox <bfox@redhat.com> 0.9.0-3
- Print init message on debug mode

* Thu Jun 20 2002 Brent Fox <bfox@redhat.com> 0.9.0-2
- Pass doDebug into launch instead of setupScreen
- Add snapsrc to Makefile

* Tue Jun 18 2002 Brent Fox <bfox@redhat.com> 0.9.0-1
- Create a way to pass keymap name back to firstboot

* Wed May 29 2002 Brent Fox <bfox@redhat.com> 0.2.0-3
- Make symbolic link in /usr/share/firstboot/modules point to keyboard_gui.py

* Tue May 28 2002 Jeremy Katz <katzj@redhat.com>
- changes to be usable within an anaconda context 

* Tue Dec 05 2001 Brent Fox <bfox@redhat.com>
- initial coding and packaging

