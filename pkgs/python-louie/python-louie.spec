%global momorel 5

%{!?python:%define python python}
%{!?python_sitearch: %define python_sitearch %(%{python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           %{python}-louie
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Dispatches signals between Python objects in a wide variety of contexts
Group:          Development/Libraries
License:        BSD
URL:            http://pylouie.org/
Source0:        http://pypi.python.org/packages/source/L/Louie/Louie-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: python-setuptools
# From the egg requires.txt
Requires: python-nose >= 0.8.3
BuildRequires: python-devel >= 2.7
BuildRequires: python-setuptools

Buildarch: noarch

%description
Louie provides Python programmers with a straightforward way to dispatch
signals between objects in a wide variety of contexts. It is based on
PyDispatcher, which in turn was based on a highly-rated recipe in the
Python Cookbook.


%prep
%setup -q -n Louie-%{version}


%build
CFLAGS="$RPM_OPT_FLAGS" %{python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{python} setup.py install --skip-build --root $RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/*
%{python_sitelib}/Louie-*.egg-info/
%{python_sitelib}/louie/


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1-1m)
- import from Fedora 13

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb  8 2009 Matthias Saou <http://freshrpms.net/> 1.1-4
- Add python-nose >= 0.8.3 requirement taken from the egg file.

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.1-3
- Rebuild for Python 2.6

* Tue Aug 28 2007 Matthias Saou <http://freshrpms.net/> 1.1-2
- Update python-setuptools build requirement to new python-setuptools-devel.

* Fri Feb  9 2007 Matthias Saou <http://freshrpms.net/> 1.1-1
- Initial RPM release.
