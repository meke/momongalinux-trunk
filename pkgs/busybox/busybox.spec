%global momorel 2

Summary: Statically linked binary providing simplified versions of system commands
Name: busybox
Version: 1.19.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Shells
Source0: http://www.busybox.net/downloads/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: busybox-static.config
Source2: busybox-petitboot.config
Source3: http://www.uclibc.org/downloads/uClibc-0.9.30.1.tar.bz2
NoSource: 3
Source4: uClibc.config
Patch16: busybox-1.10.1-hwclock.patch
Patch100: busybox-1.19.2-linux3.patch
# patch to avoid conflicts with getline() from stdio.h, already present in upstream VCS
Patch22: uClibc-0.9.30.1-getline.patch
Patch200: uClibc-0.9.30.1-gcc46.patch
Obsoletes: busybox-anaconda
URL: http://www.busybox.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libselinux-devel >= 1.27.7-2
BuildRequires: libsepol-devel
BuildRequires: libselinux-static
BuildRequires: libsepol-static
BuildRequires: glibc-static

%define debug_package %{nil}

%package petitboot
Group: System Environment/Shells
Summary: Version of busybox configured for use with petitboot

%description
Busybox is a single binary which includes versions of a large number
of system commands, including a shell.  This package can be very
useful for recovering from certain types of system failures,
particularly those involving broken shared libraries.

%description petitboot
Busybox is a single binary which includes versions of a large number
of system commands, including a shell.  The version contained in this
package is a minimal configuration intended for use with the Petitboot
bootloader used on PlayStation 3. The busybox package provides a binary
better suited to normal use.

%prep
%setup -q -a3
%patch16 -b .ia64 -p1
%patch100 -b .linux3 -p1
cat %{SOURCE4} >uClibc-0.9.30.1/.config1
%patch22 -b .getline -p1
%patch200 -p1 -b .gcc46

%build
# create static busybox - the executable is kept as busybox-static
# We use uclibc instead of system glibc, uclibc is several times
# smaller, this is important for static build.
# Build uclibc first.
cd uClibc-0.9.30.1
# fixme:
mkdir kernel-include
cp -a /usr/include/asm kernel-include
cp -a /usr/include/asm-generic kernel-include
cp -a /usr/include/linux kernel-include
# uclibc can't be built on ppc64,s390,ia64, we set $arch to "" in this case
arch=`uname -m | sed -e 's/i.86/i386/' -e 's/ppc/powerpc/' -e 's/ppc64//' -e 's/powerpc64//' -e 's/ia64//' -e 's/s390.*//'`
echo "TARGET_$arch=y" >.config
echo "TARGET_ARCH=\"$arch\"" >>.config
cat .config1 >>.config
if test "$arch"; then yes "" | make oldconfig; fi
if test "$arch"; then cat .config; fi
if test "$arch"; then make V=1; fi
if test "$arch"; then make install; fi
if test "$arch"; then make install_kernel_headers; fi

cd ..
# we are back in busybox-NN.MM dir now
cp %{SOURCE1} .config
# set all new options to defaults
yes "" | make oldconfig
# gcc needs to be convinced to use neither system headers, nor libs,
# nor startfiles (i.e. crtXXX.o files)
if test "$arch"; then \
    mv .config .config1 && \
    grep -v ^CONFIG_SELINUX .config1 >.config && \
    yes "" | make oldconfig && \
    cat .config && \
    make V=1 \
        EXTRA_CFLAGS="-isystem uClibc-0.9.30.1/installed/include" \
        CFLAGS_busybox="-static -nostartfiles -LuClibc-0.9.30.1/installed/lib uClibc-0.9.30.1/installed/lib/crt1.o uClibc-0.9.30.1/installed/lib/crti.o uClibc-0.9.30.1/installed/lib/crtn.o"; \
else \
    cat .config && \
    make V=1 CC="gcc $RPM_OPT_FLAGS"; \
fi
cp busybox busybox.static
cp docs/busybox.1 docs/busybox.static.1

# create busybox optimized for petitboot
make clean
# copy new configuration file
cp %{SOURCE2} .config
# set all new options to defaults
yes "" | make oldconfig
make V=1 CC="%__cc $RPM_OPT_FLAGS"
cp busybox busybox.petitboot
cp docs/busybox.1 docs/busybox.petitboot.1

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/sbin
install -m 755 busybox.static $RPM_BUILD_ROOT/sbin/busybox
install -m 755 busybox.petitboot $RPM_BUILD_ROOT/sbin/busybox.petitboot
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1
install -m 644 docs/busybox.static.1 $RPM_BUILD_ROOT/%{_mandir}/man1/busybox.1
install -m 644 docs/busybox.petitboot.1 $RPM_BUILD_ROOT/%{_mandir}/man1/busybox.petitboot.1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE docs/busybox.net/*.html
/sbin/busybox
%{_mandir}/man1/busybox.1*

%files petitboot
%defattr(-,root,root,-)
%doc LICENSE
/sbin/busybox.petitboot
%{_mandir}/man1/busybox.petitboot.1*

%changelog
* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.19.2-2m)
- support linux3 headers 

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.19.2-1m)
- update 1.19.2

* Tue Apr 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.1-6m)
- add gcc46 patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15.1-5m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.15.1-4m)
- fix for make 3.82

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.15.1-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.1-1m)
- sync with Fedora 13 (1:1.15.1-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14.1-1m)
- update 1.14.1

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13.2-1m)
- sync with Fedora 11 (1:1.13.2-5)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.1-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.1-1m)
- sync with Rawhide (1:1.12.1-2), but not apply Patch11

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-3m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.1-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.1-1m)
- update 1.9.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-2m)
- %%NoSource -> NoSource

* Mon Oct 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-1m)
- update 1.7.2
- import fedora patch

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- import fedora patch
- cleanup spec

* Mon Jan 02 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.01-2m)
- update busybox-selinux.patch

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.01-1m)
- ver up 1.01 

* Mon Feb 14 2005 Toru Hoshina <t@momonga-linux.org>
- (1.00-0.020050214.1m)
- ver up.

* Mon Apr 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.60.5-6m)
- import from FC1, again :-p

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Mon Jan 13 2003 Jeremy Katz <katzj@redhat.com> 0.60.5-5
- lost nolock for anaconda mount when rediffing, it returns (#81764)

* Mon Jan 6 2003 Dan Walsh <dwalsh@redhat.com> 0.60.5-4
- Upstream developers wanted to eliminate the use of floats

* Thu Jan 3 2003 Dan Walsh <dwalsh@redhat.com> 0.60.5-3
- Fix free to work on large memory machines.

* Sat Dec 28 2002 Jeremy Katz <katzj@redhat.com> 0.60.5-2
- update Config.h for anaconda build to include more useful utils

* Thu Dec 19 2002 Dan Walsh <dwalsh@redhat.com> 0.60.5-1
- update latest release

* Thu Dec 19 2002 Dan Walsh <dwalsh@redhat.com> 0.60.2-8
- incorporate hammer changes

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon May 06 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- fix compilation on mainframe

* Tue Apr  2 2002 Jeremy Katz <katzj@redhat.com>
- fix static busybox (#60701)

* Thu Feb 28 2002 Jeremy Katz <katzj@redhat.com>
- don't include mknod in busybox.anaconda so we get collage mknod

* Fri Feb 22 2002 Jeremy Katz <katzj@redhat.com>
- rebuild in new environment

* Wed Jan 30 2002 Jeremy Katz <katzj@redhat.com>
- update to 0.60.2
- include more pieces for the anaconda version so that collage can go away
- make the mount in busybox.anaconda default to -onolock

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
`- automated rebuild

* Mon Jul  9 2001 Tim Powers <timp@redhat.com>
- don't obsolete sash
- fix URL and spelling in desc. to satisfy rpmlint

* Thu Jul 05 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- add missing defattr for anaconda subpackage

* Thu Jun 28 2001 Erik Troan <ewt@redhat.com>
- initial build for Red Hat
