%global momorel 9

Summary: Tools to manage multipath devices using device-mapper
Name: device-mapper-multipath
Version: 0.4.9
Release: %{momorel}m%{?dist}
License: GPL+
Group: System Environment/Base
URL: http://christophe.varoqui.free.fr/

Source0: multipath-tools-130222.tgz
Source1: multipath.conf

Patch0001: 0001-RH-dont_start_with_no_config.patch
Patch0002: 0002-RH-multipath.rules.patch
Patch0003: 0003-RH-Make-build-system-RH-Fedora-friendly.patch
Patch0004: 0004-RH-multipathd-blacklist-all-by-default.patch
Patch0005: 0005-RH-add-mpathconf.patch
Patch0006: 0006-RH-add-find-multipaths.patch
Patch0007: 0007-RH-add-hp_tur-checker.patch
Patch0008: 0008-RH-revert-partition-changes.patch
Patch0009: 0009-RH-RHEL5-style-partitions.patch
Patch0010: 0010-RH-dont-remove-map-on-enomem.patch
Patch0011: 0011-RH-deprecate-uid-gid-mode.patch
Patch0012: 0012-RH-kpartx-msg.patch
Patch0013: 0013-RHBZ-883981-cleanup-rpmdiff-issues.patch
Patch0014: 0014-RH-handle-other-sector-sizes.patch
Patch0015: 0015-RH-fix-output-buffer.patch
Patch0016: 0016-RH-dont-print-ghost-messages.patch
#Patch0017: 0017-RH-fix-sigusr1.patch
Patch0018: 0018-RH-fix-factorize.patch
Patch0019: 0019-RH-fix-sockets.patch
Patch0020: 0020-RHBZ-907360-static-pthread-init.patch
Patch0021: 0021-RHBZ-919119-respect-kernel-cmdline.patch
Patch0022: 0022-RH-multipathd-check-wwids.patch
Patch0023: 0023-RH-multipath-wipe-wwid.patch
Patch0024: 0024-RH-multipath-wipe-wwids.patch
Patch0025: 0025-UPBZ-916668_add_maj_min.patch
Patch0026: 0026-fix-checker-time.patch
Patch0027: 0027-RH-get-wwid.patch
Patch0028: 0028-RHBZ-929078-refresh-udev-dev.patch
Patch0029: 0029-RH-no-prio-put-msg.patch
Patch0030: 0030-RHBZ-916528-override-queue-no-daemon.patch
Patch0031: 0031-RHBZ-957188-kpartx-use-dm-name.patch
Patch0032: 0032-RHBZ-956464-mpathconf-defaults.patch
Patch0033: 0033-RHBZ-829963-e-series-conf.patch
Patch0034: 0034-RHBZ-851416-mpathconf-display.patch
Patch0035: 0035-RHBZ-891921-list-mpp.patch
Patch0036: 0036-RHBZ-949239-load-multipath-module.patch
Patch0037: 0037-RHBZ-768873-fix-rename.patch
Patch0038: 0038-RHBZ-799860-netapp-config.patch
Patch0039: 0039-RH-detect-prio-fix.patch
Patch0040: 0040-RH-bindings-fix.patch
Patch0041: 0041-RH-check-for-erofs.patch
Patch0042: 0042-UP-fix-signal-handling.patch
Patch0043: 0043-RH-signal-waiter.patch
Patch0044: 0044-RHBZ-976688-fix-wipe-wwids.patch
Patch0045: 0045-RHBZ-977297-man-page-fix.patch
Patch0046: 0046-RHBZ-883981-move-udev-rules.patch
Patch0047: 0047-RHBZ-980777-kpartx-read-only-loop-devs.patch
Patch0048: 0048-RH-print-defaults.patch
Patch0049: 0049-RH-remove-ID_FS_TYPE.patch
#Patch0050: 0050-RH-listing-speedup.patch
Patch0051: 0051-UP-fix-cli-resize.patch
Patch0052: 0052-RH-fix-bad-derefs.patch
Patch0053: 0053-UP-fix-failback.patch
Patch0054: 0054-UP-keep-udev-ref.patch
Patch0055: 0055-UP-handle-quiesced-paths.patch
Patch0056: 0056-UP-alua-prio-fix.patch
Patch0057: 0057-UP-fix-tmo.patch
Patch0058: 0058-UP-fix-failback.patch
Patch0059: 0059-UP-flush-failure-queueing.patch
Patch0060: 0060-UP-uevent-loop-udev.patch
Patch0061: 0061-RH-display-find-mpaths.patch
Patch0062: 0062-RH-dont-free-vecs.patch
Patch0063: 0063-RH-fix-warning.patch
Patch0064: 0064-fix-ID_FS-attrs.patch
Patch0065: 0065-UPBZ-995538-fail-rdac-on-unavailable.patch
Patch0066: 0066-UP-dos-4k-partition-fix.patch
Patch0067: 0067-RHBZ-1022899-fix-udev-partition-handling.patch
Patch0068: 0068-RHBZ-1034578-label-partition-devices.patch
Patch0069: 0069-UPBZ-1033791-improve-rdac-checker.patch
Patch0070: 0070-RHBZ-1036503-blacklist-td-devs.patch
Patch0071: 0071-RHBZ-1031546-strip-dev.patch
Patch0072: 0072-RHBZ-1039199-check-loop-control.patch
Patch0073: 0073-RH-update-build-flags.patch
Patch0074: 0074-RHBZ-1056976-dm-mpath-rules.patch
Patch0075: 0075-RHBZ-1056976-reload-flag.patch
Patch0076: 0076-RHBZ-1056686-add-hw_str_match.patch

# runtime
Requires: %{name}-libs = %{version}-%{release}
Requires: kpartx = %{version}-%{release}
Requires: device-mapper >= 1.02.82
Requires: initscripts
Requires(post): systemd-units systemd-sysv chkconfig
Requires(preun): systemd-units
Requires(postun): systemd-units

# build/setup
BuildRequires: libaio-devel, device-mapper-devel >= 1.02.82
BuildRequires: libselinux-devel, libsepol-devel
BuildRequires: readline-devel, ncurses-devel

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
%{name} provides tools to manage multipath devices by
instructing the device-mapper multipath kernel module what to do. 
The tools are :
* multipath - Scan the system for multipath devices and assemble them.
* multipathd - Detects when paths fail and execs multipath to update things.

%package libs
Summary: The %{name} modules and shared library
License: GPL+
Group: System Environment/Libraries

%description libs
The %{name}-libs provides the path checker
and prioritizer modules. It also contains the multipath shared library,
libmultipath.

%package sysvinit
Summary: SysV init script for device-mapper-multipath
Group: System Environment/Libraries

%description sysvinit
SysV style init script for device-mapper-multipth. It needs to be
installed only if systemd is not used as the system init process.

%package -n kpartx
Summary: Partition device manager for device-mapper devices
Group: System Environment/Base

%description -n kpartx
kpartx manages partition creation and removal for device-mapper devices.

%prep
%setup -q -n multipath-tools-130222
%patch0001 -p1
%patch0002 -p1
%patch0003 -p1
%patch0004 -p1
%patch0005 -p1
%patch0006 -p1
%patch0007 -p1
%patch0008 -p1
%patch0009 -p1
%patch0010 -p1
%patch0011 -p1
%patch0012 -p1
%patch0013 -p1
%patch0014 -p1
%patch0015 -p1
%patch0016 -p1
# %%patch0017 -p1
%patch0018 -p1
%patch0019 -p1
%patch0020 -p1
%patch0021 -p1
%patch0022 -p1
%patch0023 -p1
%patch0024 -p1
%patch0025 -p1
%patch0026 -p1
%patch0027 -p1
%patch0028 -p1
%patch0029 -p1
%patch0030 -p1
%patch0031 -p1
%patch0032 -p1
%patch0033 -p1
%patch0034 -p1
%patch0035 -p1
%patch0036 -p1
%patch0037 -p1
%patch0038 -p1
%patch0039 -p1
%patch0040 -p1
%patch0041 -p1
%patch0042 -p1
%patch0043 -p1
%patch0044 -p1
%patch0045 -p1
%patch0046 -p1
%patch0047 -p1
%patch0048 -p1
%patch0049 -p1
# %%patch0050 -p1
%patch0051 -p1
%patch0052 -p1
%patch0053 -p1
%patch0054 -p1
%patch0055 -p1
%patch0056 -p1
%patch0057 -p1
%patch0058 -p1
%patch0059 -p1
%patch0060 -p1
%patch0061 -p1
%patch0062 -p1
%patch0063 -p1
%patch0064 -p1
%patch0065 -p1
%patch0066 -p1
%patch0067 -p1
%patch0068 -p1
%patch0069 -p1
%patch0070 -p1
%patch0071 -p1
%patch0072 -p1
%patch0073 -p1
%patch0074 -p1
%patch0075 -p1
%patch0076 -p1

cp %{SOURCE1} .

%build
%define _sbindir /usr/sbin
%define _libdir /usr/%{_lib}
%define _libmpathdir %{_libdir}/multipath
make %{?_smp_mflags} LIB=%{_lib}

%install
rm -rf %{buildroot}

make install \
	DESTDIR=%{buildroot} \
	bindir=%{_sbindir} \
	syslibdir=%{_libdir} \
	libdir=%{_libmpathdir} \
	rcdir=%{_initscriptdir} \
	unitdir=%{_unitdir}

# tree fix up
# install -m 0644 %{SOURCE1} %{buildroot}/etc/multipath.conf
install -d %{buildroot}/etc/multipath

%clean
rm -rf %{buildroot}

%post
%systemd_post multipathd.service

%preun
%systemd_preun multipathd.service

%postun
if [ $1 -ge 1 ] ; then
        /usr/sbin/multipathd forcequeueing daemon > /dev/null 2>&1 || :
fi
%systemd_postun_with_restart multipathd.service
        
%triggerun --  %{name} < 0.4.9-5m
%{_bindir}/systemd-sysv-convert --save multipathd >/dev/null 2>&1 ||:
%{_bindir}/systemctl --no-reload enable multipathd.service >/dev/null 2>&1 ||:
%{_sbindir}/chkconfig --del multipathd >/dev/null 2>&1 || :
%{_bindir}/systemctl try-restart multipathd.service >/dev/null 2>&1 || :
        
%triggerpostun -n %{name}-sysvinit -- %{name} < 0.4.9-5m
%{_sbindir}/chkconfig --add mdmonitor >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%{_sbindir}/multipath
%{_sbindir}/multipathd
%{_sbindir}/mpathconf
%{_sbindir}/mpathpersist
%{_unitdir}/multipathd.service
%{_mandir}/man3/mpath_persistent_reserve_in.3*
%{_mandir}/man3/mpath_persistent_reserve_out.3*
%{_mandir}/man5/multipath.conf.5*
%{_mandir}/man8/mpathconf.8*
%{_mandir}/man8/multipath.8*
%{_mandir}/man8/multipathd.8*
%{_mandir}/man8/mpathpersist.8*
%config %{_prefix}/lib/udev/rules.d/11-dm-mpath.rules
%config %{_prefix}/lib/udev/rules.d/62-multipath.rules
%doc AUTHOR COPYING FAQ
%doc multipath.conf
%dir /etc/multipath

%files libs
%defattr(-,root,root,-)
%doc AUTHOR COPYING
%{_libdir}/libmultipath.so
%{_libdir}/libmultipath.so.*
%{_libdir}/libmpathpersist.so
%{_libdir}/libmpathpersist.so.*
%dir %{_libmpathdir}
%{_libmpathdir}/*

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files sysvinit
%{_initscriptdir}/multipathd

%files -n kpartx
%defattr(-,root,root,-)
/usr/sbin/kpartx
%{_mandir}/man8/kpartx.8*

%changelog
* Sat Mar  8 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-9m)
- fix build

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-8m)
- update source_base and paches

* Wed Sep 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-7m)
- update source_base and paches

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-6m)
- update systemd service file

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-5m)
- import fedora patch
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-1m)
- sync with Fedora 13 (0.4.9-14)

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-3m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-1m)
- sync with Fedora 11 (0.4.8-10)

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.7-5m)
- [SECURITY] CVE-2009-0115
- apply a security patch (Patch3) based on Fedora 9 (0.4.7-17)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.7-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.7-3m)
- update Patch1 for fuzz=0
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-2m)
- rebuild against gcc43

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-1m)
- import from fc for xen

* Thu Sep  7 2006 Peter Jones <pjones@redhat.com> - 0.4.7-5
- Fix kpartx to handle with drives >2TB correctly.

* Thu Aug 31 2006 Peter Jones <pjones@redhat.com> - 0.4.7-4.1
- Split kpartx out into its own package so dmraid can use it without
  installing multipathd
- Fix a segfault in kpartx

* Mon Jul 17 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-4.0
- Updated to latest source. Fixes bug in default multipath.conf

* Wed Jul 12 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-3.1
- Added ncurses-devel to BuildRequires

* Wed Jul 12 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-3.0
- Updated to latest source. deals with change in libsysfs API

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.4.7-2.2.1
- rebuild

* Mon Jul 10 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-2.2
- fix tagging issue.

* Mon Jul 10 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-2.1
- changed BuildRequires from sysfsutils-devel to libsysfs-devel

* Wed Jun 28 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-2.0
- Updated to latest upstream source, fixes kpartx udev rule issue

* Mon Jun 06 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.7-1.0
- Updated to Christophe's latest source

* Mon May 22 2006 Alasdair Kergon <agk@redhat.com> - 0.4.5-16.0
- Newer upstream source (t0_4_5_post59).

* Mon May 22 2006 Alasdair Kergon <agk@redhat.com> - 0.4.5-12.3
- BuildRequires: libsepol-devel, readline-devel

* Mon Feb 27 2006 Benjamin Marzinski <bmarzins@redhat.com> 0.4.5-12.2
- Prereq: chkconfig

* Mon Feb 20 2006 Karsten Hopp <karsten@redhat.de> 0.4.5-12.1
- BuildRequires: libselinux-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.4.5-12.0.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Benjamin Marzinski <bmarzins@redhat.com> -0.4.5-12.0
- Updated to latest upstream source (t0_4_5_post56)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.4.5-9.1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Dec 19 2005 Benjamin Marzinski <bmarzins@redhat.com> - 0.4.5-9.1
- added patch for fedora changes

* Fri Dec 16 2005 Benjamin Marzinski <bmarzins@redhat.com> - 0.4.5-9.0
- Updated to latest upstream source (t)_4_5_post52)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sun Dec  4 2005 Peter Jones <pjones@redhat.com> - 0.4.4-2.6
- rebuild for newer libs

* Tue Nov 15 2005 Peter Jones <pjones@redhat.com> - 0.4.4-2.5
- unsplit kpartx.  parted knows how to do this now, so we don't
  need this in a separate package.

* Tue Nov 15 2005 Peter Jones <pjones@redhat.com> - 0.4.4-2.4
- split kpartx out into its own package

* Fri May 06 2005 Bill Nottingham <notting@redhat.com> - 0.4.4-2.3
- Fix last fix.

* Thu May 05 2005 Alasdair Kergon <agk@redhat.com> - 0.4.4-2.2
- Fix last fix.

* Wed May 04 2005 Alasdair Kergon <agk@redhat.com> - 0.4.4-2.1
- By default, disable the multipathd service.

* Tue Apr 19 2005 Alasdair Kergon <agk@redhat.com> - 0.4.4-2.0
- Fix core dump from last build.

* Tue Apr 19 2005 Alasdair Kergon <agk@redhat.com> - 0.4.4-1.0
- Move cache file into /var/cache/multipath.

* Fri Apr 08 2005 Alasdair Kergon <agk@redhat.com> - 0.4.4-0.pre8.1
- Remove pp_balance_units.

* Mon Apr 04 2005 Alasdair Kergon <agk@redhat.com> - 0.4.4-0.pre8.0
- Incorporate numerous upstream fixes.
- Update init script to distribution standards.

* Tue Mar 01 2005 Alasdair Kergon <agk@redhat.com> - 0.4.2-1.0
- Initial import based on Christophe Varoqui's spec file.
