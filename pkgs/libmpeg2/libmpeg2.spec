%global momorel 6

Summary: MPEG-2 and MPEG-1 decoding library and test program
Name: libmpeg2
Version: 0.5.1
%global srcver %{version}
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://libmpeg2.sourceforge.net/
Source0: http://libmpeg2.sourceforge.net/files/%{name}-%{srcver}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel, pkgconfig, gcc-c++
BuildRequires: libICE-devel, libSM-devel, libX11-devel
BuildRequires: libXext-devel, libXv-devel
BuildRequires: autoconf, automake19, libtool

Provides:  mpeg2dec
Obsoletes: mpeg2dec

%description
A free library for decoding MPEG-2 and MPEG-1 video streams.

%package devel
Summary: Development files for mpeg2dec's libmpeg2
Group: Development/Libraries
Requires: %{name} = %{version}, pkgconfig
Provides:  mpeg2dec-devel
Obsoletes: mpeg2dec-devel

%description devel
A free library for decoding MPEG-2 and MPEG-1 video streams.

This package contains files needed to build applications that use mpeg2dec's libmpeg2.

%prep
%setup -q -n %{name}-%{version}
./bootstrap

%build
CFLAGS="%{optflags}" \
%configure \
%ifnarch %{ix86}
    --disable-accel-detect \
%endif
    --enable-shared 
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%makeinstall

%post
/sbin/ldconfig 2>/dev/null

%postun
/sbin/ldconfig 2>/dev/null


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING NEWS README TODO
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files devel
%defattr(-, root, root, 0755)
%doc doc/*.txt doc/*.c
%{_includedir}/mpeg2dec/
%{_libdir}/*.a
%exclude %{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against rpm-4.6

* Sat Nov 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1m)
- rename mpeg2dec to libmpeg2
- update 0.5.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- %%NoSource -> NoSource

* Tue Nov 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.1-1m)
- bugfix release 0.4.1

* Tue May 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- import to Momonga

* Fri Dec 10 2004 Matthias Saou <http://freshrpms.net/> 0.4.0-5b
- Add patch from rpm.livna.org to remove -prefer-non-pic.
- Downgrade to 0.4.0b to fix permanent segfaults...

* Fri Nov 26 2004 Matthias Saou <http://freshrpms.net/> 0.4.1-0.20040610.2
- Use --disable-accel-detect for non-x86 to fix asm build failure on x86_64.

* Mon Nov 15 2004 Matthias Saou <http://freshrpms.net/> 0.4.1-0.20040610.1
- Update to latest available CVS snaphsot, fixes build issues on FC3.
- Added SDL support.

* Sun May 30 2004 Dag Wieers <dag@wieers.com> - 0.4.0-4b
- Added -fPIC for non ix86 archs.
- Merged with my SPEC file.

* Wed Mar 24 2004 Matthias Saou <http://freshrpms.net/> 0.4.0-3b
- Removed explicit dependency on XFree86 for the binary package.

* Thu Feb  5 2004 Matthias Saou <http://freshrpms.net/> 0.4.0-2b
- Update to 0.4.0b.

* Mon Jan  5 2004 Matthias Saou <http://freshrpms.net/> 0.4.0-1
- Update to 0.4.0.

* Fri Nov  7 2003 Matthias Saou <http://freshrpms.net/> 0.3.2-0.20030701.2
- Rebuild for Fedora Core 1.

* Tue Jul  1 2003 Matthias Saou <http://freshrpms.net/>
- Update to today's snapshot, enabled the spec to build snapshots since
  videolan-client 0.6.0 requires 0.3.2 cvs.

* Mon Mar 31 2003 Matthias Saou <http://freshrpms.net/>
- Rebuilt for Red Hat Linux 9.

* Thu Jan 16 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.3.1.

* Mon Oct 28 2002 Matthias Saou <http://freshrpms.net/>
- Major spec file cleanup.

* Mon Jun 17 2002 Thomas Vander Stichele <thomas@apestaart.org>
- remove .la
- release 3

* Wed May 29 2002 Thomas Vander Stichele <thomas@apestaart.org>
- wrote out the different libs
- added docs
- removed autogen.sh option

* Wed May 08 2002 Erik Walthinsen <omega@temple-baptist.com>
- changed whitespace
- removed %attr and changed %defattr to (-,root,root)

* Fri May 03 2002 Thomas Vander Stichele <thomas@apestaart.org>
- adapted from PLD spec for 0.2.1

