%global momorel 5

# Review:       https://bugzilla.redhat.com/487973

Name:           lxmenu-data
Version:        0.1.1
Release:        %{momorel}m%{?dist}
Summary:        Data files for the LXDE menu

Group:          User Interface/Desktops
License:        LGPLv2+
URL:            http://lxde.org/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource: 0
Source1:        lxmenu-data-0.1-COPYING
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  intltool >= 0.40.0
BuildArch:      noarch

%description
The lxmenu-data contains files used to build the menu in LXDE according to 
the freedesktop-org menu spec. Currently it's used by LXPanel and LXLauncher.


%prep
%setup -q
# install correct license
rm -f COPYING
cp %{SOURCE1} COPYING


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
#FIXME: add changelog when there is one
%doc AUTHORS COPYING README TODO
%config(noreplace) %{_sysconfdir}/xdg/menus/lxde-applications.menu
%{_datadir}/desktop-directories/lxde-*.directory


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-1m)
- import from Fedora 11
- momonganize

* Sun Mar 22 2009 Christoph Wickert <cwickert@fedoraproject.org> 0.1-2
- Change menu structure to vendor default
- Fix license

* Fri Dec 12 2008 Christoph Wickert <cwickert@fedoraproject.org> 0.1-1
- Initial Fedora package
