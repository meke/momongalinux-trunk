%global         momorel 1

Summary:        A pipeline manipulation library
Name:           libpipeline
Version:        1.2.3
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Group:          Development/Libraries
URL:            http://libpipeline.nongnu.org/
Source0:        http://download.savannah.gnu.org/releases/libpipeline/%{name}-%{version}.tar.gz
NoSource:       0
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libtool
Provides:       bundled(gnulib)

%description
libpipeline is a C library for setting up and running pipelines of
processes, without needing to involve shell command-line parsing which is
often error-prone and insecure.  This alleviates programmers of the need to
laboriously construct pipelines using lower-level primitives such as fork(2)
and execve(2).

%package devel
Summary:        Header files and libraries for pipeline manipulation library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
libpipeline-devel contains the header files and libraries needed
to develop programs that use libpipeline library.

%prep
%setup -q

%build
./configure --libdir=%{_libdir}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} prefix=%{_prefix} \
             INSTALL='install -p'
rm %{buildroot}/%{_libdir}/libpipeline.la

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
exit 0

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog NEWS
%{_libdir}/libpipeline.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libpipeline.so
%{_includedir}/*.h
%{_mandir}/man3/*
%{_libdir}/pkgconfig/libpipeline.pc

%changelog
* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sun Oct 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- import fron Fedora for man-db

* Tue Apr 19 2011 Ivana Hutarova Varekova <varekova@redhat.com> - 1.2.0-1
- initial build
