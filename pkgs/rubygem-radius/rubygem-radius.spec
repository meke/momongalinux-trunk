# Generated from radius-0.7.3.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname radius

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A tag-based templating language for Ruby
Name: rubygem-%{gemname}
Version: 0.7.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/jlong/radius
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) > 1.3.1
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) > 1.3.1
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Radius is a powerful tag-based template language for Ruby inspired by the
template languages used in MovableType and TextPattern. It uses tags similar
to XML, but can be used to generate any form of plain text (HTML, e-mail,
etc...).


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/CHANGELOG
%doc %{geminstdir}/CONTRIBUTORS
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/QUICKSTART.rdoc
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.3-1m)
- udpate 0.7.3

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- Initial package for Momonga Linux
