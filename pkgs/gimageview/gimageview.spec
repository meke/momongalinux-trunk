%global momorel 24

Summary: Graphic Viewer
Name: gimageview

# include local configuration
%{?include_specopt}

%{?!mplayer_support:   %define mplayer_support   0}
%{?!spi_support:       %define spi_support       0}

Version: 0.2.27
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.homa.ne.jp/~ashie/gimageview/
Source0: http://dl.sourceforge.net/sourceforge/gtkmmviewer/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-x86_64.patch
Patch1: %{name}-%{version}-desktop.patch
Patch2: %{name}-%{version}-gtk214.patch
Patch3: %{name}-%{version}-edit.patch
Patch4: %{name}-%{version}-sort.patch
Patch10: %{name}-%{version}-xine-1.2.4.patch
Requires: gtk2, glib2, libmng >= 1.0.0, libwmf >= 0.2.8 , xine-lib >= 1.1.4-3m
BuildRequires: gtk2-devel, glib2-devel, libmng-devel >= 1.0.0
BuildRequires: libwmf-devel >= 0.2.8
BuildRequires: libjpeg-devel >= 8a
BuildRequires: xine-lib-devel >= 1.2.4, librsvg2-devel >= 2.22.2-3m
BuildRequires: fontconfig-devel, expat-devel
BuildRequires: desktop-file-utils

%if %{mplayer_support}
Requires: mplayer
BuildRequires: mplayer
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GImageView is a GTK+ based image viewer.
It support tabbed browsing, thumbnail table views, directory tree views,
drag and drop, reading thumbnail cache of other famous image viewers,
and flexible user interface.

%prep
%setup -q

%patch0 -p1 -b x86_64~
%patch1 -p1 -b .desktop~
%patch2 -p1 -b .gtk214~
%patch3 -p1 -b .edit~
%patch4 -p1 -b .sort~
%patch10 -p1 -b .xine

%build
%if %{mplayer_support}
MPLAYER="--enable-mplayer"
%endif

%if %{spi_support}
SPI="--enable-spi"
%endif

if [ ! -f configure ]; then
   ./autogen.sh
fi
CFLAGS="%{optflags} -fpic -DPIC"
%configure --with-gtk2 --with-xine $MPLAYER $SPI LIBS="-lm"
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%__mkdir_p %{buildroot}%{_datadir}/doc/%{name}-%{version}
%makeinstall

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Viewer \
  --add-category Graphics \
  --add-category GTK \
  %{buildroot}%{_datadir}/gnome/apps/Graphics/%{name}.desktop

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL NEWS README TODO
%doc %{_docdir}/%{name}
%{_bindir}/gimv
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/gimv.png

%changelog
* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.27-24m)
- rebuild against xine-lib-1.2.4

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-23m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-22m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.27-19m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.27-18m)
- fix build

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.27-17m)
- fix up desktop file
- License: GPLv2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.27-16m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-15m)
- delete __libtoolize hack

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.27-14m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.27-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.27-12m)
- rebuild against libjpeg-7

* Mon Feb  9 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-11m)
- import two bug fix patches from debian
-- Bug#459288, Bug#440648

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.27-10m)
- rebuild against rpm-4.6

* Fri Oct 31 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.27-9m)
- add patch for gtk2-2.14.4

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.27-8m)
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.27-7m)
- rebuild against gcc43

* Mon Apr 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.27-6m)
- add Categories to desktop file
- add desktop.patch

* Sun Apr 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.27-5m)
- move desktop file

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.27-4m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.27-3m)
- rebuild against expat-2.0.0-1m

* Sun Jan 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.27-2m)
- add Patch0: gimageview-0.2.27-x86_64.patch

* Sun Jan  9 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.27-1m)
- version 0.2.27
- add CFLAGS="%{optflags} -fpic -DPIC" for i686 problem?

* Sat Dec  4 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.2.26-1m)
- update to 0.2.26
 
* Fri Apr 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.25-5m)
- rebuild against libcroco-0.5.0

* Sun Apr 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.25-4m)
- add patch0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.25-3m)
- rebuild against for glib-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.25-2m)
- rebuild against for libxml2-2.6.8

* Mon Mar 1 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.2.25-1m)
- update to 0.2.25

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.24-2m)
- s/Copyright:/License:/

* Tue Aug 26 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.2.24-1m)
- update to 0.2.24

* Sat Jul 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.23-2m)
- rebuild against libcroco-0.3.0

* Sat Jun 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.2.23-1m)
  update to 0.2.23

* Wed May  7 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.2.14-5m)
- remove gimageview-0.2.14-xine-1.0.patch(included in next patch)
- import patch released on sf.net

* Wed May  7 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.2.14-4m)
- use xine.m4 included in xine-1.0 (gimageview-0.2.14-xine-1.0.patch)

* Sat Apr 12 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (0.2.14-3m)
- rebuild against for librsvg (libcroco).

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.14-2m)
- remove gdk-pixbuf from BuildPrereq and Requires.
  gtk2 nano yo!

* Mon Mar 31 2003 smbd <smbd@momonga-linux.org>
- (0.2.14-1m)
- up to 0.2.14
- support specopt

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.10-3m)
- rebuild against for XFree86-4.3.0

* Wed Dec  4 2002 OGAWA Youhei <t-nyan2@nifty.com>
- (0.2.10-2m)
- add gimageview-0.2.10-scroll.patch

* Tue Dec  3 2002 OGAWA Youhei <t-nyan2@nifty.com>
- (0.2.10-1m)
- update to new upstream version.

* Wed Nov  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.2.7-1m)
- update to new upstream version.

* Mon Sep 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.5-2m)
- fix file list
- add configure.patch
- remove BuildPrereq: gdk-pixbuf-devel
- remove Require: tag

* Mon Sep 23 2002 OGAWA Youhei <t-nyan2@nifty.com>
- (0.2.5-1m)
- update
- delete gtk2 patch

* Sun Sep 22 2002 OGAWA Youhei <t-nyan2@nifty.com>
- (0.2.4-1m)
- update
- fix config patch for gtk2

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.3-2m)
- add config patch for gtk2

* Mon Jul 8 2002 Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (0.2.3-1m)
- update

* Wed Jun 19 2002 Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (0.2.2-4k)
- fix help

* Thu Jun 13 2002 Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (0.2.2-2k)
- update to 0.2.2

* Thu May 9 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.2.1-2k)
- update to 0.2.1

* Thu Apr 25 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.2.0-4k)
- update to 0.2.0

* Sun Apr 21 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.2.0-2k)
- update to 0.2.0-pre1

* Tue Apr 15 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.106-2k)
- update to 0.1.106

* Tue Apr 9 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.105-2k)
- update to 0.1.105

* Thu Mar 28 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.104-4k)
- fix libdir

* Fri Mar 22 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.104-2k)
- kondaraization

* Sun Feb 17 2002 Takuro Ashie <ashie@homa.ne.jp>
- Bug fix.

* Thu Feb 12 2002 Takuro Ashie <ashie@homa.ne.jp>
- Update to 0.1.97

* Thu Jan  3 2002 ARAKI Manabu <hora@st.rim.or.jp>
- 0.1.7-0vl1

* Tue Dec 25 2001 ARAKI Manabu <hora@st.rim.or.jp>
- 0.1.6-0vl1

* Mon Dec 10 2001 ARAKI Manabu <hora@st.rim.or.jp>
- 0.1.5-0vl1
- change Source0

* Thu Nov 15 2001 ARAKI Manabu <hora@st.rim.or.jp>
- 0.1.3-0vl1
- build for Vine Linux 2.1.*
- modify spec file (base: 0.1.2-3k.nosrc.rpm)
- add Japanese summary and description.

* Sat Nov 02 2001 Ogawa Youhei <t-nyan2@nifty.com>
- for version 0.1.2

* Sat Nov 02 2001 Ogawa Youhei <t-nyan2@nifty.com>
- change files section for 2001-11-01 nightly

* Sat Oct 15 2001 Ogawa Youhei <t-nyan2@nifty.com>
- for nightly

* Sat Oct 12 2001 Ogawa Youhei <t-nyan2@nifty.com>
- for version 0.1.1

* Sat Sep 08 2001 Ogawa Youhei <t-nyan2@nifty.com>
- for version 0.1.0

* Sat Sep 08 2001 Ogawa Youhei <t-nyan2@nifty.com>
- Initial build.
