%global momorel 1
Name:		cppcheck
Version:	1.63
Release: %{momorel}m%{?dist}
Summary:	Tool for static C/C++ code analysis
Group:		Development/Languages
License:	GPLv3+
URL:		http://cppcheck.wiki.sourceforge.net/
Source0:	http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	pcre-devel
BuildRequires:	tinyxml2-devel
BuildRequires:	docbook-style-xsl
BuildRequires:	libxslt

%description
Cppcheck is a static analysis tool for C/C++ code. Unlike C/C++
compilers and many other analysis tools it does not detect syntax
errors in the code. Cppcheck primarily detects the types of bugs that
the compilers normally do not detect. The goal is to detect only real
errors in the code (i.e. have zero false positives).


%prep
%setup -q
# Make sure bundled tinyxml is not used
rm -r externals/tinyxml

%build
# TINYXML= prevents use of bundled tinyxml
CXXFLAGS="%{optflags} -DNDEBUG $(pcre-config --cflags)" \
    LDFLAGS="$RPM_LD_FLAGS" LIBS=-ltinyxml2 make TINYXML= \
    CFGDIR=%{_datadir}/%{name} \
    DB2MAN=%{_datadir}/sgml/docbook/xsl-stylesheets/manpages/docbook.xsl \
    %{?_smp_mflags} all man
xsltproc --nonet -o man/manual.html \
    %{_datadir}/sgml/docbook/xsl-stylesheets/xhtml/docbook.xsl \
    man/manual.docbook

%install
rm -rf %{buildroot}
install -D -p -m 755 cppcheck %{buildroot}%{_bindir}/cppcheck
install -D -p -m 644 cppcheck.1 %{buildroot}%{_mandir}/man1/cppcheck.1

# Install cfg files
cd cfg
for f in *; do
    install -D -p -m 644 $f %{buildroot}%{_datadir}/cppcheck/$f
done

%check
make TINYXML= check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING man/manual.html
%{_datadir}/cppcheck/
%{_bindir}/cppcheck
%{_mandir}/man1/cppcheck.1*

%changelog
* Fri Jan 17 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.63-1m)
- initial import from fedora

