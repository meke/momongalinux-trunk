%global momorel 1
#%%global snapdate 2010-06-27

Summary: Wrappers around dbus for EFL based applications
Name: e_dbus
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2 
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ecore-devel >= 1.7.7
Obsoletes: ecore-dbus <= 0.9.9.040

%description
Basic convenience wrappers around dbus to ease integrating dbus with EFL based
applications.

%package devel
Summary: Development files for %{name}
Group: System Environment/Libraries
Requires: %{name} = %{version}
Requires: dbus-devel ecore-devel evas-devel pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
%{__make} %{?_smp_mflags} %{?mflags}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
rm %{buildroot}%{_bindir}/%{name}_*
chrpath --delete %{buildroot}%{_bindir}/e-notify-send
chrpath --delete %{buildroot}%{_libdir}/*.so.*
find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root,-)
%doc AUTHORS COPYING README
%{_bindir}/*
%{_libdir}/*.so.*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/logo.png

%files devel
%defattr(-, root, root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.5.0.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.5.0.49539-1m)
- update to new svn snap

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.5.0.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.062-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.062-1m)
- update to new svn snap

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.5.0.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.5.0.060-1m)
- update

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.0.050-3m)
- add autoreconf. support libtool-2.2.x

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.050-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.050-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.043-1m)
- update to 0.5.0.043

* Tue Mar 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0.042-1m)
- initial made for Momonga Linux
