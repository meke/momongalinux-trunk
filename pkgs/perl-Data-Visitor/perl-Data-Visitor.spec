%global         momorel 4

Name:           perl-Data-Visitor
Version:        0.30
Release:        %{momorel}m%{?dist}
Summary:        Visitor style traversal of Perl data structures
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Data-Visitor/
Source0:        http://www.cpan.org/authors/id/D/DO/DOY/Data-Visitor-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Class-Load >= 0.06
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 0.89
BuildRequires:  perl-namespace-clean >= 0.19
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Tie-ToObject >= 0.01
Requires:       perl-Class-Load >= 0.06
Requires:       perl-Moose >= 0.89
Requires:       perl-namespace-clean >= 0.19
Requires:       perl-Task-Weaken
Requires:       perl-Tie-ToObject >= 0.01
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is a simple visitor implementation for Perl values.

%prep
%setup -q -n Data-Visitor-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Data/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.18.1

* Tue Jun 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.27-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.27-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.27-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.12.0

* Wed Feb  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19-2m)
- rebuild against rpm-4.6

* Wed Aug 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18
- add BuildRequires: perl-Mouse and perl-namespace-clean
- add Requires: perl-Mouse and perl-namespace clean

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15-2m)
- rebuild against gcc43

* Wed Jan 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Tue Jan  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.05-2m)
- use vendor

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.05-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
