%global momorel 1
%{?!with_python:      %global with_python      1}
 
%if %{with_python}
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%endif

Summary: Lowlevel DNS(SEC) library with API
Name: ldns
Version: 1.6.11
Release: %{momorel}m%{?dist}
License: BSD
Url: http://www.nlnetlabs.nl/%{name}/
#Url: http://open.nlnetlabs.nl/%{name}/
Source0: http://www.nlnetlabs.nl/downloads/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
#Patch1: ldns-installfix-r3167.patch
#Patch2: ldns-rpathfix.patch

Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool, autoconf, automake, gcc-c++, doxygen,
BuildRequires: perl, libpcap-devel >= 1.1.1, openssl-devel >= 1.0.0

%if %{with_python}
BuildRequires:  python-devel >= 2.7, swig
%endif

%description
ldns is a library with the aim to simplify DNS programming in C. All
low-level DNS/DNSSEC operations are supported. We also define a higher
level API which allows a programmer to (for instance) create or sign
packets.

%package devel
Summary: Development package that includes the ldns header files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The devel package contains the ldns library and the include files

%if %{with_python}
%package python
Summary: Python extensions for ldns
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description python
Python extensions for ldns
%endif #%%{with_python}


%prep
%setup -q 
# To built svn snapshots
# rm config.guess config.sub ltmain.sh
# aclocal
# libtoolize -c 
# autoreconf 

%build
%configure --disable-rpath --disable-static --with-sha2 --disable-gost \
%if %{with_python}
 --with-pyldns
%endif

(cd drill ; %configure --disable-rpath --disable-static --with-sha2 --disable-gost --with-ldns=%{buildroot}/lib/ )
(cd examples ; %configure --disable-rpath --disable-static --with-sha2 --disable-gost --with-ldns=%{buildroot}/lib/ )

make %{?_smp_mflags}
( cd drill ; make %{?_smp_mflags} )
( cd examples ; make %{?_smp_mflags} )
make %{?_smp_mflags} doc

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} INSTALL="%{__install} -p" install 
make DESTDIR=%{buildroot} INSTALL="%{__install} -p" install-doc

# don't package building script for install-doc in doc section
rm doc/doxyparse.pl
#remove double set of man pages
rm -rf doc/man
# remove .la files
rm -rf %{buildroot}%{_libdir}/*.la %{buildroot}%{python_sitearch}/*.la
(cd drill ; make DESTDIR=%{buildroot} install)
(cd examples; make DESTDIR=%{buildroot} install)

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root)
%{_libdir}/libldns*so.*
%{_bindir}/drill
%{_bindir}/ldnsd
%{_bindir}/ldns-chaos
%{_bindir}/ldns-compare-zones
%{_bindir}/ldns-[d-z]*
%doc README LICENSE 
%{_mandir}/*/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libldns*so
%{_bindir}/ldns-config
%dir %{_includedir}/ldns
%{_includedir}/ldns/*.h
%doc doc Changelog README

%if %{with_python}
%files python
%defattr(-,root,root)
%{python_sitearch}/*
%endif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.11-1m)
- update to 1.6.11

* Thu May  5 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.9-1m)
- update to 1.6.9

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.7-1m)
- update to 1.6.7

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.5-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.5-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5-1m)
- update to 1.6.5
- delete Patch1, Patch2

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-3m)
- rebuild against libpcap-1.1.1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-2m)
- rebuild against openssl-1.0.0

* Thu Mar 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4
- add Patch1, Patch2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-3m)
- fix build with new libtool using Fedora's hack

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against openssl-0.9.8k

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1-1m)
- [SECURITY] CVE-2009-1086
- update to 1.5.1

* Tue Feb  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-2m)
- rebuild against openssl-0.9.8h-1m

* Sat May 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-1m)
- import to Momonga from ldns sample spec file
- merge fedora spec

* Wed Nov 21 2007 Jelte Jansen <jelte@NLnetLabs.nl> 
- Added support for HMAC-MD5 keys in generator
- Added a new example tool (written by Ondrej Sury): ldns-compare-zones
- ldns-keygen now checks key sizes for rfc conformancy
- ldns-signzone outputs SSL error if present
- Fixed manpages (thanks to Ondrej Sury)
- Fixed Makefile for -j <x>
- Fixed a $ORIGIN error when reading zones
- Fixed another off-by-one error

* Tue Sep 18 2007 Jelte Jansen <jelte@NLnetLabs.nl> 1.2.1-1
- Updated spec file for release

* Wed Aug  8 2007 Paul Wouters <paul@xelerance.com> 1.2.0-10
- Patch for ldns-key2ds to write to stdout
- Again remove extra set of man pages from doc

* Wed Aug  8 2007 Paul Wouters <paul@xelerance.com> 1.2.0-10
- Added sha256 DS record patch to ldns-key2ds
- Minor tweaks for proper doc/man page installation.
- Workaround for parallel builds

* Mon Aug  6 2007 Paul Wouters <paul@xelerance.com> 1.2.0-2
- Own the /usr/include/ldns directory (bug #233858)
- Removed obsoleted patch
- Remove files form previous libtool run accidentally packages by upstream

* Mon Sep 11 2006 Paul Wouters <paul@xelerance.com> 1.0.1-4
- Commented out 1.1.0 make targets, put make 1.0.1 targets.

* Mon Sep 11 2006 Paul Wouters <paul@xelerance.com> 1.0.1-3
- Fixed changelog typo in date
- Rebuild requested for PT_GNU_HASH support from gcc
- Did not upgrade to 1.1.0 due to compile issues on x86_64

* Fri Jan  6 2006 Paul Wouters <paul@xelerance.com> 1.0.1-1
- Upgraded to 1.0.1. Removed temporary clean hack from spec file.

* Sun Dec 18 2005 Paul Wouters <paul@xelerance.com> 1.0.0-8
- Cannot use make clean because there are no Makefiles. Use hardcoded rm.

* Sun Dec 18 2005 Paul Wouters <paul@xelerance.com> 1.0.0-7
- Patched 'make clean' target to get rid of object files shipped with 1.0.0

* Sun Dec 13 2005 Paul Wouters <paul@xelerance.com> 1.0.0-6
- added a make clean for 2.3.3 since .o files were left behind upstream,
  causing failure on ppc platform

* Sun Dec 11 2005 Tom "spot" Callaway <tcallawa@redhat.com> 1.0.0-5
- minor cleanups

* Wed Oct  5 2005 Paul Wouters <paul@xelerance.com> 0.70_1205
- reworked for svn version

* Sun Sep 25 2005 Paul Wouters <paul@xelerance.com> - 0.70
- Initial version
