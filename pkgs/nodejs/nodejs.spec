%global momorel 1
%define   _base node

Name:%{_base}js
Version: 0.6.16
Release: %{momorel}m%{?dist}
Summary: Evented I/O for V8 Javascript.
Group:   Development/Libraries
License: MIT
URL:     http://nodejs.org/
Source0: http://nodejs.org/dist/v%{version}/%{_base}-v%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 

BuildRequires: python, openssl-devel

%description
Evented I/O for V8 Javascript.

%prep
%setup -q -n %{_base}-v%{version}

%build
./configure --prefix=/usr
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%defattr(-,root,root,-)
%dir %{_includedir}/node
%{_includedir}/node/*.h
%{_includedir}/node/c-ares/*.h
%{_includedir}/node/uv-private/*.h
#%{_includedir}/node/ev/*.h
#%{_prefix}/lib/pkgconfig/nodejs.pc
%attr(755,root,root) %{_bindir}/node
%attr(755,root,root) %{_bindir}/node-waf
%attr(755,root,root) %{_bindir}/npm
%dir %{_prefix}/lib/node
%dir %{_prefix}/lib/node/wafadmin
%dir %{_prefix}/lib/node/wafadmin/Tools
%{_prefix}/lib/node/wafadmin/*
%{_prefix}/lib/node_modules/npm
%{_mandir}/man1/*

%doc
/usr/share/man/man1/node.1.bz2

%changelog
* Tue May  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.16-1m)
- version up 0.6.16
- [SECURITY] CVE-2011-5037 CVE-2012-2330

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.12-1m)
- version up 0.4.12

* Wed Jun 08 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.4.8-1m)
- version upto 0.4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-2m)
- rebuild for new GCC 4.6

* Thu Feb 24 2011 - yohgaki@ohgaki.net
- Initial version for momonga

* Tue Oct 19 2010 - susan@managedopensource.com
- Initial version of RPM specification file.

