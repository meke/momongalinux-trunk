# Note: building with PCRE instead of GNU regex because of license
# incompatibilities (this one's basically a BSD with advertising clause).

%global momorel 7


Name: ngrep
Summary:  Network layer grep tool

Version:  1.45
Release:  %{momorel}m%{?dist}

Group:    Applications/Internet
License:  BSD
URL:      http://ngrep.sourceforge.net/

Source0:  http://downloads.sourceforge.net/project/ngrep/ngrep/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1:  %{name}-README.momonga
Patch0:   %{name}-system-pcre.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: autoconf >= 2.57
BuildRequires: pcre-devel >= 8.31
BuildRequires: libpcap-devel >= 1.1.1

%description
ngrep strives to provide most of GNU grep's common features, applying them
to the network layer. ngrep is a pcap-aware tool that will allow you to
specify extended regular or hexadecimal expressions to match against data
payloads of packets. It currently recognizes TCP, UDP, ICMP, IGMP and Raw
protocols across Ethernet, PPP, SLIP, FDDI, Token Ring, 802.11 and null
interfaces, and understands bpf filter logic in the same fashion as more
common packet sniffing tools, such as tcpdump and snoop.

%prep
%setup -q
%patch0 -p1
autoconf
install -pm 644 %{SOURCE1} ./README.fedora

%build
export EXTRA_INCLUDES=$(pcre-config --cflags)
export EXTRA_LIBS=$(pcre-config --libs)
%configure --enable-pcre --enable-ipv6 --with-pcap-includes=%{_includedir}/pcap
make %{?_smp_mflags} STRIPFLAG=

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT BINDIR_INSTALL=%{_sbindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc doc/CHANGES.txt doc/CREDITS.txt doc/README.txt LICENSE.txt README.fedora
%{_sbindir}/ngrep
%{_mandir}/man8/ngrep.8*

%changelog
* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-7m)
- rebuild against pcre-8.31

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-4m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-3m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 9 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- 1.45-1m
- import to momonga

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.45-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.45-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 13 2008 Patrick "Jima" Laughton <jima@beer.tclug.org> 1.45-4
- Bump-n-build for GCC 4.3

* Wed Aug 22 2007 Patrick "Jima" Laughton <jima@beer.tclug.org> 1.45-3
- License clarification

* Tue Aug 21 2007 Patrick "Jima" Laughton <jima@beer.tclug.org> 1.45-2
- Rebuild for BuildID

* Wed Nov 29 2006 Patrick "Jima" Laughton <jima@beer.tclug.org> 1.45-1
- Upgrade to 1.45
- Enable IPv6 support
- Rebuild due to libpcap upgrade

* Wed Oct 04 2006 Patrick "Jima" Laughton <jima@beer.tclug.org> 1.44-7
- Bump-n-build

* Tue Sep 19 2006 Patrick "Jima" Laughton <jima@beer.tclug.org>	- 1.44-6
- Bump for FC6 rebuild

* Fri Sep 08 2006 Oliver Falk <oliver@linux-kernel.at>		- 1.44-5
- Fix BR

* Tue Oct 18 2005 Oliver Falk <oliver@linux-kernel.at>		- 1.44-4
- Bug #170967, useless debuginfo was generated

* Wed Aug 24 2005 Oliver Falk <oliver@linux-kernel.at>		- 1.44-3
- Bugs from #166481

* Mon Aug 22 2005 Oliver Falk <oliver@linux-kernel.at>		- 1.44-2
- Bug #165963
- Merge with package from Ville
  See also https://www.redhat.com/archives/fedora-extras-list/2005-July/msg01009.html

* Thu Aug 11 2005 Oliver Falk <oliver@linux-kernel.at>		- 1.44-1
- Initial build for Fedora Extras
