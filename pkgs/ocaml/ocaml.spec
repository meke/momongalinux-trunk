%global momorel 2
#%%global rcver 1
%global srcname %{name}-%{version}%{?rcver:+rc%{rcver}}
%global major 3
%global minor 12
%global patchlevel 1

%global _default_patch_fuzz 2

Summary: Objective Caml compiler and programming environment
Name: ocaml
Version: %{major}.%{minor}.%{patchlevel}
Release: %{?rcver:0.%{rcver}.}%{momorel}m%{?dist}
Group: Development/Languages
License: "QPL" and "LGPLv2+ with exceptions"
URL: http://caml.inria.fr/
Source0: http://caml.inria.fr/distrib/ocaml-%{major}.%{minor}/%{srcname}.tar.bz2
NoSource: 0
Source1: http://caml.inria.fr/distrib/ocaml-%{major}.%{minor}/ocaml-%{major}.%{minor}-refman.html.tar.gz
NoSource: 1
Source2: http://caml.inria.fr/distrib/ocaml-%{major}.%{minor}/ocaml-%{major}.%{minor}-refman.pdf
NoSource: 2
Source3: http://caml.inria.fr/distrib/ocaml-%{major}.%{minor}/ocaml-%{major}.%{minor}-refman.info.tar.gz
NoSource: 3
# Useful utilities from Debian, and sent upstream.
# http://git.debian.org/?p=pkg-ocaml-maint/packages/ocaml.git;a=tree;f=debian/ocamlbyteinfo;hb=HEAD
Source6: ocamlbyteinfo.ml
##Source7: ocamlplugininfo.ml
Patch0: ocaml-3.12.0-rpath.patch
Patch1: ocaml-user-cflags.patch
Patch2: debian_patches_0013-ocamlopt-arm-add-.type-directive-for-code-symbols.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel
BuildRequires: gdbm-devel
BuildRequires: tcl-devel
BuildRequires: tk-devel
BuildRequires: emacs >= %{_emacs_version}
BuildRequires: gawk
BuildRequires: perl
BuildRequires: util-linux
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXaw-devel
BuildRequires: libXext-devel
BuildRequires: libXft-devel
BuildRequires: libXmu-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: chrpath
BuildRequires: rpm-build >= 4.8.0
Requires: gcc
Requires: ncurses-devel
Requires: gdbm-devel
Requires: rpm-build >= 4.8.0
Provides: ocaml(compiler) = %{version}
Obsoletes: ocaml-xemacs
ExcludeArch: ppc64

%global __ocaml_requires_opts -c -f %{buildroot}%{_bindir}/ocamlobjinfo
%global __ocaml_provides_opts -f %{buildroot}%{_bindir}/ocamlobjinfo

%description
Objective Caml is a high-level, strongly-typed, functional and
object-oriented programming language from the ML family of languages.

This package comprises two batch compilers (a fast bytecode compiler
and an optimizing native-code compiler), an interactive toplevel system,
parsing tools (Lex,Yacc,Camlp4), a replay debugger, a documentation generator,
and a comprehensive library.


%package runtime
Group: System Environment/Libraries
Summary: Objective Caml runtime environment
Requires: util-linux-ng
Provides: ocaml(runtime) = %{version}
Provides: %{_bindir}/ocamlrun

%description runtime
Objective Caml is a high-level, strongly-typed, functional and
object-oriented programming language from the ML family of languages.

This package contains the runtime environment needed to run Objective
Caml bytecode.


%package source
Group: Development/Languages
Summary: Source code for Objective Caml libraries
Requires: ocaml = %{version}-%{release}

%description source
Source code for Objective Caml libraries.


%package x11
Group: System Environment/Libraries
Summary: X11 support for Objective Caml
Requires: ocaml-runtime = %{version}-%{release}
Requires: libX11-devel

%description x11
X11 support for Objective Caml.


%package labltk
Group: System Environment/Libraries
Summary: Tk bindings for Objective Caml
Requires: ocaml-runtime = %{version}-%{release}
Provides: labltk = %{version}-%{release}
Obsoletes: labltk

%description labltk
Labltk is a library for interfacing Objective Caml with the scripting
language Tcl/Tk.

This package contains the runtime files.


%package labltk-devel
Group: Development/Libraries
Summary: Development files for labltk
Requires: ocaml = %{version}-%{release}
Requires: %{name}-labltk = %{version}-%{release}
Requires: libX11-devel
Provides: labltk = %{version}-%{release}
Obsoletes: labltk

%description labltk-devel
Labltk is a library for interfacing Objective Caml with the scripting
language Tcl/Tk.

This package contains the development files.  It includes the ocaml
browser for code editing and library browsing.


%package camlp4
Group: Development/Languages
Summary: Pre-Processor-Pretty-Printer for Objective Caml
Requires: ocaml-runtime = %{version}-%{release}
Provides: camlp4 = %{version}-%{release}
Obsoletes: camlp4

%description camlp4
Camlp4 is a Pre-Processor-Pretty-Printer for Objective Caml, parsing a
source file and printing some result on standard output.

This package contains the runtime files.


%package camlp4-devel
Group: Development/Languages
Summary: Pre-Processor-Pretty-Printer for Objective Caml
Requires: ocaml = %{version}-%{release}
Requires: %{name}-camlp4 = %{version}-%{release}
Provides: camlp4 = %{version}-%{release}
Obsoletes: camlp4

%description camlp4-devel
Camlp4 is a Pre-Processor-Pretty-Printer for Objective Caml, parsing a
source file and printing some result on standard output.

This package contains the development files.


%package ocamldoc
Group: Development/Languages
Summary: Documentation generator for Objective Caml.
Requires: ocaml = %{version}-%{release}
Provides: ocamldoc

%description ocamldoc
Documentation generator for Objective Caml.


%package -n emacs-ocaml-mode
Group: Development/Languages
Summary: Emacs mode for Objective Caml
Requires: emacs >= %{_emacs_version}
Obsoletes: ocaml-emacs
Obsoletes: elisp-ocaml-mode
Provides: elisp-ocaml-mode

%description -n emacs-ocaml-mode
Emacs mode for Objective Caml.


%package docs
Group: Development/Languages
Summary: Documentation for Objective Caml
Requires: ocaml = %{version}-%{release}
Requires(post): info
Requires(preun): info

%description docs
Objective Caml is a high-level, strongly-typed, functional and
object-oriented programming language from the ML family of languages.

This package contains documentation in PDF and HTML format as well as
man pages and info files.


%prep
%setup -q -T -b 0 -n %{srcname}
%setup -q -T -D -a 1 -n %{srcname}
%setup -q -T -D -a 3 -n %{srcname}
%patch0 -p1 -b .rpath
%patch1 -p1 -b .cflags
%patch2 -p1 -b .arm-type-dir

cp %{SOURCE2} refman.pdf


%build
CFLAGS="$RPM_OPT_FLAGS" ./configure \
    -bindir %{_bindir} \
    -libdir %{_libdir}/ocaml \
    -x11lib %{_libdir} \
    -x11include %{_includedir} \
    -mandir %{_mandir}/man1
%ifnarch ppc64
make world opt opt.opt
%else
make world
%endif
# %{?_smp_mflags} breaks the build
make -C emacs ocamltags

# Currently these tools are supplied by Debian, but are expected
# to go upstream at some point.
cp %{SOURCE6} .
includes="-nostdlib -I stdlib -I utils -I parsing -I typing -I bytecomp -I asmcomp -I driver -I otherlibs/unix -I otherlibs/str -I otherlibs/dynlink"
boot/ocamlrun ./ocamlc $includes dynlinkaux.cmo ocamlbyteinfo.ml -o ocamlbyteinfo


%install
rm -rf %{buildroot}

make install \
     BINDIR=%{buildroot}%{_bindir} \
     LIBDIR=%{buildroot}%{_libdir}/ocaml \
     MANDIR=%{buildroot}%{_mandir}
perl -pi -e "s|^%{buildroot}||" %{buildroot}%{_libdir}/ocaml/ld.conf

(
    # install emacs files
    cd emacs;
    make install \
         BINDIR=%{buildroot}%{_bindir} \
         EMACSDIR=%{buildroot}%{_emacs_sitelispdir}/caml
    make install-ocamltags BINDIR=%{buildroot}%{_bindir}
)

(
    # install info files
    mkdir -p %{buildroot}%{_infodir};
    cd infoman; cp ocaml*.gz %{buildroot}%{_infodir}
)

echo %{version} > %{buildroot}%{_libdir}/ocaml/momonga-ocaml-release

# Remove rpaths from stublibs .so files.
chrpath --delete %{buildroot}%{_libdir}/ocaml/stublibs/*.so

install -m 0755 ocamlbyteinfo %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}


%post docs
/sbin/install-info \
    --entry="* ocaml: (ocaml).	 The Objective Caml compiler and programming environment" \
    --section="Programming Languages" \
    %{_infodir}/%{name}.info \
    %{_infodir}/dir 2>/dev/null || :


%preun docs
if [ "$1" -eq 0 ]; then
    /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir 2>/dev/null || :
fi


%files
%defattr(-,root,root,-)
%ifnarch ppc64
%{_bindir}/ocamlc.opt
%{_bindir}/ocamldep.opt
%{_bindir}/ocamllex.opt
%{_bindir}/ocamlopt
%{_bindir}/ocamlopt.opt
%endif
%{_bindir}/ocaml
%{_bindir}/ocamlbuild
%{_bindir}/ocamlbuild.byte
%{_bindir}/ocamlbuild.native
%{_bindir}/ocamlc
%{_bindir}/ocamlcp
%{_bindir}/ocamldebug
%{_bindir}/ocamldep
%{_bindir}/ocamllex
%{_bindir}/ocamlmklib
%{_bindir}/ocamlmktop
%{_bindir}/ocamlobjinfo
%{_bindir}/ocamlprof
%{_bindir}/ocamlyacc
%{_libdir}/ocaml/camlheader
%{_libdir}/ocaml/camlheader_ur
%{_libdir}/ocaml/expunge
%{_libdir}/ocaml/extract_crc
%{_libdir}/ocaml/ld.conf
%{_libdir}/ocaml/Makefile.config
%{_libdir}/ocaml/*.a
%{_libdir}/ocaml/*.cmxs
%{_libdir}/ocaml/*.cmxa
%{_libdir}/ocaml/*.cmx
%{_libdir}/ocaml/*.mli
%{_libdir}/ocaml/*.o
%{_libdir}/ocaml/libcamlrun_shared.so
%{_libdir}/ocaml/objinfo_helper
%{_libdir}/ocaml/vmthreads/*.mli
%{_libdir}/ocaml/vmthreads/*.a
%{_libdir}/ocaml/threads/*.a
%{_libdir}/ocaml/threads/*.cmxa
%{_libdir}/ocaml/threads/*.cmx
%{_libdir}/ocaml/caml
%{_libdir}/ocaml/ocamlbuild
%exclude %{_libdir}/ocaml/graphicsX11.mli
%{_bindir}/ocamlbyteinfo


%files runtime
%defattr(-,root,root,-)
%{_bindir}/ocamlrun
%dir %{_libdir}/ocaml
%{_libdir}/ocaml/*.cmo
%{_libdir}/ocaml/*.cmi
%{_libdir}/ocaml/*.cma
%{_libdir}/ocaml/stublibs
%dir %{_libdir}/ocaml/vmthreads
%{_libdir}/ocaml/vmthreads/*.cmi
%{_libdir}/ocaml/vmthreads/*.cma
%dir %{_libdir}/ocaml/threads
%{_libdir}/ocaml/threads/*.cmi
%{_libdir}/ocaml/threads/*.cma
%{_libdir}/ocaml/momonga-ocaml-release
%exclude %{_libdir}/ocaml/graphicsX11.cmi
%exclude %{_libdir}/ocaml/stublibs/dlllabltk.so
%doc README LICENSE Changes


%files source
%defattr(-,root,root,-)
%{_libdir}/ocaml/*.ml


%files x11
%defattr(-,root,root,-)
%{_libdir}/ocaml/graphicsX11.cmi
%{_libdir}/ocaml/graphicsX11.mli


%files labltk
%defattr(-,root,root,-)
%{_bindir}/labltk
%dir %{_libdir}/ocaml/labltk
%{_libdir}/ocaml/labltk/*.cmi
%{_libdir}/ocaml/labltk/*.cma
%{_libdir}/ocaml/labltk/*.cmo
%{_libdir}/ocaml/stublibs/dlllabltk.so


%files labltk-devel
%defattr(-,root,root,-)
%{_bindir}/ocamlbrowser
%{_libdir}/ocaml/labltk/labltktop
%{_libdir}/ocaml/labltk/pp
%{_libdir}/ocaml/labltk/tkcompiler
%{_libdir}/ocaml/labltk/*.a
%{_libdir}/ocaml/labltk/*.cmxa
%{_libdir}/ocaml/labltk/*.cmx
%{_libdir}/ocaml/labltk/*.mli
%{_libdir}/ocaml/labltk/*.o
%doc otherlibs/labltk/examples_labltk
%doc otherlibs/labltk/examples_camltk


%files camlp4
%defattr(-,root,root,-)
%dir %{_libdir}/ocaml/camlp4
%{_libdir}/ocaml/camlp4/*.cmi
%{_libdir}/ocaml/camlp4/*.cma
%{_libdir}/ocaml/camlp4/*.cmo
%dir %{_libdir}/ocaml/camlp4/Camlp4Filters
%{_libdir}/ocaml/camlp4/Camlp4Filters/*.cmi
%{_libdir}/ocaml/camlp4/Camlp4Filters/*.cmo
%dir %{_libdir}/ocaml/camlp4/Camlp4Parsers
%{_libdir}/ocaml/camlp4/Camlp4Parsers/*.cmo
%{_libdir}/ocaml/camlp4/Camlp4Parsers/*.cmi
%dir %{_libdir}/ocaml/camlp4/Camlp4Printers
%{_libdir}/ocaml/camlp4/Camlp4Printers/*.cmi
%{_libdir}/ocaml/camlp4/Camlp4Printers/*.cmo
%dir %{_libdir}/ocaml/camlp4/Camlp4Top
%{_libdir}/ocaml/camlp4/Camlp4Top/*.cmi
%{_libdir}/ocaml/camlp4/Camlp4Top/*.cmo


%files camlp4-devel
%defattr(-,root,root,-)
%{_bindir}/camlp4*
%{_bindir}/mkcamlp4
%{_libdir}/ocaml/camlp4/*.a
%{_libdir}/ocaml/camlp4/*.cmxa
%{_libdir}/ocaml/camlp4/*.cmx
%{_libdir}/ocaml/camlp4/*.o
%{_libdir}/ocaml/camlp4/Camlp4Filters/*.cmx
%{_libdir}/ocaml/camlp4/Camlp4Filters/*.o
%{_libdir}/ocaml/camlp4/Camlp4Parsers/*.cmx
%{_libdir}/ocaml/camlp4/Camlp4Parsers/*.o
%{_libdir}/ocaml/camlp4/Camlp4Printers/*.cmx
%{_libdir}/ocaml/camlp4/Camlp4Printers/*.o
%{_libdir}/ocaml/camlp4/Camlp4Top/*.cmx
%{_libdir}/ocaml/camlp4/Camlp4Top/*.o
%{_mandir}/man1/*


%files ocamldoc
%defattr(-,root,root,-)
%{_bindir}/ocamldoc*
%{_libdir}/ocaml/ocamldoc
%doc ocamldoc/Changes.txt


%files docs
%defattr(-,root,root,-)
%doc refman.pdf htmlman
%{_infodir}/*
%{_mandir}/man3/*


%files -n emacs-ocaml-mode
%defattr(-,root,root,-)
%doc emacs/COPYING emacs/README emacs/README.itz
%{_emacs_sitelispdir}/caml
%{_bindir}/ocamltags


%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.1-2m)
- rebuild for emacs-24.1

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.1-1m)
- update to 3.12.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.2-8m)
- rename elisp-ocaml-mode to emacs-ocaml-mode

* Wed Apr 13 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.2-7m)
- apply upstream patch to enable build on new binutils

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11.2-4m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.2-3m)
- rebuild against emacs-23.2

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.2-2m)
- rebuild against readline6

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.2-1m)
- update to 3.12.2
- import ocamlbyteinfo.ml and ocamlplugininfo.ml

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-13m)
- rename ocaml-emacs to elisp-ocaml-mode
- kill ocaml-xemacs

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-12m)
- rebuild against rpm-4.8.0
-- rpm-4.8.0 includes ocaml-find-{requires,provides}.sh

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.0-10m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.0-9m)
- rebuild against emacs 23.0.96

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-8m)
- move dllgraphics.so to runtime

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-7m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-6m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-5m)
- rebuild against xemacs-21.5.29

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-4m)
- revise install-info to avoid invalid pointer

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-1m)
- update to 3.11.0
-- apply Patch5

* Sun Nov 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11.0-0.1.1m)
- update to 3.11.0+rc1 based on Fedora devel (3.11.0-0.4.beta1)
-- update Source4, Patch0,3 from Fedora devel
-- drop Patch2,4, merged upstream or unused

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.2-6m)
- refix man1 location

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.2-5m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sun May 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.2-4m)
- import map32bit.patch and ppc64.patch from Fedora
- https://bugzilla.redhat.com/show_bug.cgi?id=445545

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.10.2-3m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.10.2-2m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.2-1m)
- update to 3.10.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.1-2m)
- %%NoSource -> NoSource

* Fri Jan 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.1-1m)
- update to 3.10.1

* Mon Oct  1 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.0-2m)
- fix man1 location

* Sat Sep 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.0-1m)
- update to 3.10.0
-- sync with Fedora devel
-- * Thu Sep  6 2007 Richard W.M. Jones <rjones@redhat.com> - 3.10.0-7
-- - Run chrpath to delete rpaths used on some of the stublibs.
-- - Ignore Parsetree module in dependency calculation.
-- - Fixed ocaml-find-{requires,provides}.sh regexp calculation so it doesn't
--   over-match module names.
-- 
-- * Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 3.10.0-6
-- - ocaml-runtime provides ocaml(runtime) = 3.10.0, and
--   ocaml-find-requires.sh modified so that it adds this requires
--   to other packages.  Now can upgrade base ocaml packages without
--   needing to rebuild everything else.
-- 
-- * Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 3.10.0-5
-- - Don't include the release number in fedora-ocaml-release file, so
--   that packages built against this won't depend on the Fedora release.
-- 
-- * Wed Aug 29 2007 Gerard Milmeister <gemi@bluewin.ch> - 3.10.0-4
-- - added BR util-linux-ng
-- 
-- * Wed Aug 29 2007 Gerard Milmeister <gemi@bluewin.ch> - 3.10.0-3
-- - added BR gawk
-- 
-- * Tue Aug 28 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 3.10.0-2
-- - Rebuild for selinux ppc32 issue.
-- 
-- * Sat Jun  2 2007 Gerard Milmeister <gemi@bluewin.ch> - 3.10.0-1
-- - new version 3.10.0
-- - split off devel packages
-- - rename subpackages to use ocaml- prefix

* Wed Jan 10 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.09.3-3m)
- Provides: /usr/bin/ocamlrun

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.09.3-2m)
- disable opt for ppc64

* Fri Oct  6 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.09.3-1m) 
- update to 3.09.3
- revise %%changelog

* Wed Aug 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.09.2-3m)
- delete duplicated file

* Mon Aug 21 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.09.2-2m)
- add ocaml-xemacs

* Wed Jul 12 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.09.2-1m)
- import from Fedora Extras 5

* Sun Apr 30 2006 Gerard Milmeister <gemi@bluewin.ch> - 3.09.2-1
- new version 3.09.2

* Fri Feb 17 2006 Gerard Milmeister <gemi@bluewin.ch> - 3.09.1-2
- Rebuild for Fedora Extras 5

* Thu Jan  5 2006 Gerard Milmeister <gemi@bluewin.ch> - 3.09.1-1
- new version 3.09.1

* Sun Jan  1 2006 Gerard Milmeister <gemi@bluewin.ch> - 3.09.0-1
- new version 3.09.0

* Sun Sep 11 2005 Gerard Milmeister <gemi@bluewin.ch> - 3.08.4-1
- New Version 3.08.4

* Wed May 25 2005 Toshio Kuratomi <toshio-tiki-lounge.com> - 3.08.3-5
- Bump and re-release as last build failed due to rawhide syncing.

* Sun May 22 2005 Toshio Kuratomi <toshio-tiki-lounge.com> - 3.08.3-4
- Fix for gcc4 and the 32 bit assembly in otherlibs/num.
- Fix to allow compilation with RPM_OPT_FLAG defined -O level.

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 3.08.3-3
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Mar 26 2005 Gerard Milmeister <gemi@bluewin.ch> - 3.08.3-1
- New Version 3.08.3

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 0:3.08.2-2
- Added patch for removing rpath from shared libs

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 0:3.08.2-1
- New Version 3.08.2

* Thu Dec 30 2004 Thorsten Leemhuis <fedora[AT]leemhuis[DOT]info> - 0:3.07-6
- add -x11lib %%{_prefix}/X11R6/%%{_lib} to configure; fixes labltk build
  on x86_64

* Tue Dec  2 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:3.07-0.fdr.5
- ocamldoc -> ocaml-ocamldoc
- ocaml-doc -> ocaml-docs

* Fri Nov 28 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:3.07-0.fdr.4
- Make separate packages for labltk, camlp4, ocamldoc, emacs and documentation

* Thu Nov 27 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:3.07-0.fdr.2
- Changed license tag
- Register info files
- Honor RPM_OPT_FLAGS
- New Patch

* Fri Oct 31 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:3.07-0.fdr.1
- First Fedora release

* Mon Oct 13 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Updated to 3.07.

* Wed Apr  9 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Rebuilt for Red Hat 9.

* Tue Nov 26 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Added %%{_mandir}/mano/* entry
