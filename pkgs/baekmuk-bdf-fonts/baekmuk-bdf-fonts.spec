%global momorel 7

%define fontname   baekmuk-bdf

%define fontdir      %{_datadir}/fonts/%{fontname}
%define catalogue    %{_sysconfdir}/X11/fontpath.d

Name:           %{fontname}-fonts
Version:        2.2
Release:        %{momorel}m%{?dist}
Summary:        Korean bitmap fonts

Group:          User Interface/X
License:        BSD
URL:            http://kldp.net/projects/baekmuk/
Source:         http://kldp.net/frs/download.php/1428/%{fontname}-%{version}.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  xorg-x11-font-utils
Conflicts:      fonts-korean < 2.2-1m
Requires:       fontpackages-filesystem

%description
This package provides the Korean Baekmuk bitmap fonts.


%prep
%setup -q -n %{fontname}-%{version}

%build
for file in bdf/*.bdf; do
    bdftopcf $file | gzip -9 > ${file%.bdf}.pcf.gz
done

%install
rm -rf $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{fontdir}

# for bmp font
install -m 0644 bdf/*.pcf.gz $RPM_BUILD_ROOT%{fontdir}/
install -m 0444 bdf/fonts.alias $RPM_BUILD_ROOT%{fontdir}/

# for catalogue
install -d $RPM_BUILD_ROOT%{catalogue}
ln -sf ../../..%{fontdir} $RPM_BUILD_ROOT%{catalogue}/%{name}

mkfontdir $RPM_BUILD_ROOT%{fontdir} 

# convert Korean copyright file to utf8
iconv -f EUC-KR -t UTF-8 COPYRIGHT.ks > COPYRIGHT.ko

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ -x %{_bindir}/fc-cache ]; then
  %{_bindir}/fc-cache %{_datadir}/fonts
fi

%postun
if [ "$1" = "0" ]; then
  if [ -x %{_bindir}/fc-cache ]; then
    %{_bindir}/fc-cache %{_datadir}/fonts
  fi
fi

%files
%defattr(-,root,root,-)
%doc COPYRIGHT COPYRIGHT.ko README
%dir %{fontdir}
%{fontdir}/*.gz
%{fontdir}/fonts.alias
%verify(not md5 size mtime) %{fontdir}/fonts.dir
%{catalogue}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2-1m)
- import from Fedora

* Wed Nov 14 2007 Jens Petersen <petersen@redhat.com> - 2.2-5
- better url
- use fontname macro

* Tue Sep 25 2007 Jens Petersen <petersen@redhat.com> - 2.2-4
- fix name of fonts.alias file

* Mon Sep 24 2007 Jens Petersen <petersen@redhat.com> - 2.2-3
- conflict with fonts-korean < 2.2-5

* Mon Sep 24 2007 Jens Petersen <petersen@redhat.com> - 2.2-2
- convert Korean copyright file to utf8 (Mamoru Tasaka, #302451)

* Tue Sep 11 2007 Jens Petersen <petersen@redhat.com> - 2.2-1
- initial packaging separated from fonts-korean (#253155)
