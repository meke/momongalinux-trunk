%global momorel 21
%define gconf_version 2.14

Summary: Gtk+ IRC client
Name: xchat
Version: 2.8.8
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2+
URL: http://xchat.org/

Source0: http://xchat.org/files/source/2.8/%{name}-%{version}.tar.xz
NoSource: 0

Source1: xchat-ja.po

# Patch33 was regenerated as fuzz=0
Patch33: http://takuo.jp/junk/xchat/99_plus.dpatch
Patch34: http://takuo.jp/junk/xchat/99_x_dialog.dpatch

Patch100: xchat-2.8.4-default-font.patch
Patch101: xchat-2.8.8-servlist.patch

Patch102:  xchat-2.8.8-glib2332.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl
BuildRequires: perl-ExtUtils-Embed
BuildRequires: python-devel >= 2.7
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pkgconfig
BuildRequires: GConf2-devel
BuildRequires: dbus-devel >= 0.60, dbus-glib-devel >= 0.60
BuildRequires: glib2-devel >= 2.10.0, gtk2-devel >= 2.10.0
BuildRequires: bison >= 1.35
BuildRequires: gettext
BuildRequires: sed
BuildRequires: libtool
#BuildRequires: libntlm-devel
BuildRequires: libsexy-devel
BuildRequires: desktop-file-utils >= 0.10
# For gconftool-2:
Requires(post): GConf2 >= %{gconf_version}
Requires(preun): GConf2 >= %{gconf_version}

# Ensure that a compatible libperl is installed
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

Provides: xchat-perl = %{version}-%{release}
Obsoletes: xchat-perl < %{version}-%{release}
Provides: xchat-python = %{version}-%{release}
Obsoletes: xchat-python < %{version}-%{release}

%description
X-Chat is yet another IRC client for the X Window System, using the Gtk+
toolkit. It is pretty easy to use compared to the other Gtk+ IRC clients
and the interface is quite nicely designed.

%prep
%setup -q
%patch33 -p1 -b .plus
%patch34 -p1 -b .x_dialog
%patch100 -p1 -b .default-font
%patch101 -p1 -b .servlist
%patch102 -p1 -b .glib2332~

cp %{SOURCE1} po/ja.po

%build
# Remove CVS files from source dirs so they're not installed into doc dirs.
find . -name CVS -type d | xargs rm -rf

export CFLAGS="$RPM_OPT_FLAGS $(perl -MExtUtils::Embed -e ccopts)"
export LDFLAGS="$(perl -MExtUtils::Embed -e ldopts) `pkg-config gobject-2.0 --libs` `pkg-config gmodule-2.0 --libs`"

%configure --disable-textfe \
           --enable-gtkfe \
           --enable-openssl \
           --enable-python \
           --disable-tcl \
           --enable-ipv6 \
           --enable-spell=libsexy \
           --enable-shm

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot} GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1

find %{buildroot} -name "*.la" -delete

# Install the .desktop file properly
%{__rm} -f %{buildroot}%{_datadir}/applications/xchat.desktop
desktop-file-install --vendor="" \
  --dir %{buildroot}%{_datadir}/applications xchat.desktop

%find_lang %name

# do not Provide plugins .so
%define _use_internal_dependency_generator 0
%{__cat} << \EOF > %{name}.prov
#!%{_buildshell}
%{__grep} -v %{_docdir} - | %{__find_provides} $* \
	| %{__sed} '/\.so$/d'
EOF
%define __find_provides %{_builddir}/%{name}-%{version}/%{name}.prov
%{__chmod} +x %{__find_provides}

%post
# Install schema
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule /etc/gconf/schemas/apps_xchat_url_handler.schemas >& /dev/null || :


%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule /etc/gconf/schemas/apps_xchat_url_handler.schemas >& /dev/null || :
fi

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule /etc/gconf/schemas/apps_xchat_url_handler.schemas >& /dev/null || :
fi

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc README ChangeLog AUTHORS COPYING
%doc plugins/plugin20.html plugins/perl/xchat2-perl.html
%{_bindir}/xchat
%dir %{_libdir}/xchat
%dir %{_libdir}/xchat/plugins
%{_libdir}/xchat/plugins/perl.so
%{_libdir}/xchat/plugins/python.so
%{_datadir}/applications/xchat.desktop
%{_datadir}/pixmaps/*
%{_sysconfdir}/gconf/schemas/apps_xchat_url_handler.schemas
%{_datadir}/dbus-1/services/org.xchat.service.service

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-15m)
- rebuild against perl-5.16.1

* Sun Jul 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-14m)
- more fix for link error

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.8-13m)
- fix build failure

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-12m)
- rebuild against perl-5.16.0

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.8-11m)
- fix build failure with glib 2.33.2

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-8m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.8-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.8-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.8-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-4m)
- rebuild against perl-5.12.2

* Wed Sep  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.8-3m)
- update servlist

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.8-2m)
- full rebuild for mo7 release

* Sun Aug  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-1m)
- update to 2.8.8

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.6-10m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.6-9m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-8m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.6-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-5m)
- change coding-system of #momonga@irc.momonga-linux.org to UTF-8

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.6-4m)
- rebuild against perl-5.10.1

* Sun Jul  5 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.6-3m)
- update ja.po

* Fri Jun 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.6-2m)
- add patch (xchat-2.8.4-shm-pixmaps.patch)

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-1m)
- update to 2.8.6
- %%define __libtoolize :

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-9m)
- rebuild against openssl-0.9.8k

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.4-8m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-7m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-6m)
- remove %%{epoch}

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.4-5m)
- rebuild against python-2.6.1

* Fri Oct 31 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.4-4m)
- add patch for gtk2-2.14.4

* Tue Sep 23 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-3m)
- add Patch101: xchat-2.8.4-servlist.patch

* Tue Sep 23 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-2m)
- add Patch100: xchat-2.8.4-default-font.patch

* Sat Jul 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-1m)
- update to 2.8.4 (based on Fedora 9)

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.8-9m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.8-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.8-7m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-6m)
- rebuild against perl-5.10.0-1m

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.8-5m)
- revised spec for debuginfo

* Wed Jun 27 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.8-4m)
- revised xchat-ja.po

* Tue Feb 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.8-3m)
- delete libtool library

* Sat Nov 25 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.6.8-2m)
- add patch50,51 from upstream

* Sat Oct 28 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.6.8-1m)
- version up 2.6.8

* Fri Oct 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.6-3m)
- specify automake version 1.9

* Fri Sep 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.6-2m)
- comment out aclocal (for gettext-0.15)

* Sat Sep 16 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.6.6-1m)
- version up 2.6.6
- add patch33,34 from "xchat plus" (http://takuo.jp/junk/xchat/)

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-3m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.6.2-2m)
- rebuild against expat-2.0.0-1m

* Tue May 16 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- add BuildRequire gettext-devel
-
* Sun May 14 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2
- modify default servlist_.conf.
- revised xchat-ja.po

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.1-3m)
- rebuild against dbus-0.61

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.1-2m)
- rebuild against openssl-0.9.8a

* Tue Feb  7 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1
- revised xchat-ja.po

* Sat Dec 31 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.0-2m)
- Patch30: xchat-2.6.0-dbus060.patch
- rebuild against dbus-0.60-2m

* Wed Nov 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0
- revised xchat-ja.po

* Sun Apr 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.3-2m)
- revised xchat-ja.po

* Sun Apr 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.3-1m)
- version 2.4.3
- add xchat-2.4.3-localedir.patch

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Tue Mar  8 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.0.8-5m)
- add xchat-2.0.6-browsermenu-firefox.patch

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.8-4m)
- invoke auto* for so-missing problem

* Mon Aug 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-3m)
- remove unnecessary desktop file

* Mon Jun 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.8-2m)
- rebuild against tcl

* Wed Apr 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.8-1m)
- version update to 2.0.8
- removed patch6 and updated iroiro patch to suit 2.0.8

* Tue Mar 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.6-4m)
- revise browser menu for galeon
- remove browser menu for MozillaFirebird

* Tue Mar 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.6-3m)
- added patch to add browser menu for epiphany

* Mon Mar  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.6-2m)
- add patch to revise browser menu

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.6-1m)
- ver. 2.0.6
- add patch
  http://mail.nl.linux.org/xchat-announce/2003-12/msg00000.html

* Mon Sep 22 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.5-1m)
- version 2.0.5
- obsolete patch3
- modify xchat-iroiro.patch for 2.0.5

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.4-1m)
- version 2.0.4

* Wed Aug  6 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.3-3m)
- remove URL string from popup menu that appears when you right-click on URL string

* Tue Jul  1 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.0.3-2m)
- add patch3

* Mon Jun 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Thu May  1 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Wed Apr  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-5m)
- remove GDK_SHIFT_MASK from accelerator modifier.

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-4m)
- rebuild against for XFree86-4.3.0

* Tue Mar 18 2003 Kenta MURATA <muraken2@nifty.com>
- (2.0.1-3m)
- add GDK_SHIFT_MASK to accelerator modifier.

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.1-2m)
  rebuild against openssl 0.9.7a

* Tue Mar  4 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Mon Feb  3 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-0.0.1.1m)
- version 2.0.0pre1

* Mon Feb  3 2003 smbd <smbd@momonga-linux.org>
- (1.9.8-2m)
- change xchat-1.9.7-ja.po to xchat-1.9.8-ja.po

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.8-1m)
- version 1.9.8

* Mon Dec 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.7-1m)
- version 1.9.7

* Wed Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.6-1m)
- version 1.9.6

* Thu Nov 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.4-1m)
- version 1.9.4

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.3-1m)
- version 1.9.3

* Sat Sep 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.2-5m)
- update activate patch

* Mon Jul 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.2-4m)
- fe-text bug fix.

* Sun Jul 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.2-3m)
- add xchat-1.9.2-selection.patch

* Sun Jul 28 2002 Shingo Akaigaki <dora@kitty.dnsalias.org>
- (1.9.2-2m)
- add xchat-1.9.2-activate_event.patch

* Sun Jul 28 2002 Shingo Akaigaki <dora@kitty.dnsalias.org>
- (1.9.2-1m)
- version 1.9.2

* Thu May 23 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.8.9-2k)
- update server list( by smbd )
- update to 1.8.9
  this update includes security fix. See http://online.securityfocus.com/bid/4376

* Thu Apr 18 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.8.8-4k)
- add /etc/X11/applnk/Internet/xchat

* Thu Mar 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.8.8-2k)

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.8.7-10k)
- rebuild against for db3,4

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.8.7-8k)
- perl 5.6.1

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.8.7-6k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.8.7-4k)
- automake autoconf 1.5

* Sun Jan 13 2002 WATABE Toyokazu <toy2@kondara.org>
- (1.8.7-2k)
- update to 1.8.7, including security fix.
  http://www.securityfocus.com/bid/3830

* Mon Jan  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.8.5-10k)
  *USE* JCONV. I *HAVE* not JCONV. :-P

* Sat Dec  8 2001 Tsutomu Yasuda <tom@kondara.org>
- (1.8.5-8k)
  remove default serverlist.
  remove key bindings(C-b, C-k, C-o).

* Tue Oct 30 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.5-2k)
- up to 1.8.5
- modify utf8 patch to handle dcc chat
- zh_TW.Big5 -> zh_TW

* Thu Oct 18 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.8.4-2k)
- backport from Jirai

* Mon Oct 01 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.4-3k)
- up to 1.8.4

* Mon Sep 03 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.3-3k)
- up to 1.8.3

* Wed Jul 18 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.2-3k)
- up to 1.8.2

* Tue Jul 10 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.0-3k)
- up to 1.8.0
- it is an unstable release. (especially to use Multibyte Char)

* Tue May 29 2001 Akira Higuchi <a@kondara.org>
- (1.6.4-12k)
- change some default settings

* Tue May  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.6.4-10k)
- move autoconf from Requires tag to BuildRequires tag

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (1.6.4-8k)
- rebuild against openssl 0.9.6.

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- (1.6.4-7k)
- ja.po (x-euc-jp -> EUC-JP)
- We need original ja.po (for Codeset menu). I'll added it in the future.

* Wed Mar 06 2001 Motonobu Ichimura <famao@kondara.org>
- (1.6.4-5k)
- modified patch to handle menu_quick_item

* Fri Mar 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.6.4-3k)
- up to 1.6.4
- re-made utf8 patch (but still need to modify)

* Thu Nov 16 2000 Motonobu Ichimura <famao@kondara.org>
- dump up to 1.6.0
- it is STILL test release ;-)

* Sat Oct 01 2000 Motonobu Ichimura <famao@kondara.org>
- test release ;-)

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Mar 11 2000 Shingo Akagaki <dora@kondara.org>
- install xchat.desktop file

* Sun Nov  7 1999 binn <binn@kondara.org>
- fixed DCC chat trouble.
- Update to 1.3.6.ja-2

* Thu Nov  4 1999 binn <binn@kondara.org>
- done to use gdk_fontset_load default when you upgrade from old one.
- Update to 1.3.6.ja-1

* Tue Oct 19 1999 famao <famao@kondara.org>
- when compiled xchat with 'enable-gnome', xchat uses libzvt as default
- so don't use libzvt if define I18N_MB  for japanese

* Sat Oct 16 1999 binn <binn@binn.dnd.to>
- fixed sigsegv when getting the channellist
- Update to 1.3.4.ja-3

* Sat Oct 16 1999 binn <binn@binn.dnd.to>
- slightly tiny bug in ja.po fiexed
- Update to 1.3.4.ja-2

* Fri Oct 15 1999 binn <binn@binn.dnd.to>
- Update to 1.3.4.ja-1

* Fri Oct  1 1999 binn <binn@binn.dnd.to>
- Fixed DCC info trouble
- Update to 1.3.3.ja-7

* Tue Sep 28 1999 binn <binn@binn.dnd.to>
- changed the position whicth make patched
- But not yet fixed DCC info trouble
- Update to 1.3.3.ja-6

* Mon Sep 27 1999 binn <binn@binn.dnd.to>
- Put convert_kanji_auto() in read_data() [src/common/xchat.c]
- But not yet fixed DCC info trouble
- Update to 1.3.3.ja-5

* Sat Sep 25 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- fix channel list dialog
- Update to 1.3.3.ja-4

* Sat Sep 25 1999 binn <binn@binn.dnd.to>
- add slightly changed to src and put ja.po very much
  for japanized comment output

* Fri Sep 24 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- fix channel list dialog

* Thu Sep 23 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- release 1.3.3.ja-01 that's dev. version of Japanese localized one.


