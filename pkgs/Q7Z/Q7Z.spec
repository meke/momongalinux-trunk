%global momorel 2

%global		qtver 4.7.1
%global		qtdir %{_libdir}/qt4

Name:		Q7Z
Version:	0.9.1
Release:	%{momorel}m%{?dist}
Summary:	P7Zip GUI for Linux
License:	GPL
Group:		Applications/Archiving
URL:		http://k7z.sourceforge.net/7Z/Q7Z/
Source0:	http://dl.sourceforge.net/sourceforge/k7z/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  python >= 2.4
BuildRequires:  PyQt4-devel >= 4.7
BuildRequires:  p7zip
BuildRequires:  python3-PyQt4-devel
Requires:       qt4 >= %{qtver}
Requires:       PyQt4 >= 4.7
Requires:       tar >= 1.5
Requires:       p7zip >= 4.30
Requires:       python3-PyQt4

%description
Q7Z is a P7Zip GUI for Linux, which attempts to simplify data compression and backup.

Use Q7Z if you want to:
* Update existing archives quickly
* Backup multiple folders to a storage location
* Create or extract protected archives
* Lessen effort by using archiving profiles and lists

Ark (2.6.3) is handy but it doesn't seem to:
* Create archives with passwords
* Extract (some) archives with passwords
* Update existing archives by default

Q7Z is a simple application that attempts to fill these voids. It probably won't become a total archiving solution.

%prep
%setup -q -n %{name}

%build
export QTDIR=%{qtdir} QTLIB=%{qtdir}/lib
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

pushd Build
make
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
pushd Build
make install DESTDIR=%{buildroot}
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_bindir}/*
%{_datadir}/applications/Q7Z.desktop
%{_datadir}/%{name}
%{_datadir}/kde4/services/ServiceMenus/Q7Z*
%{_datadir}/icons/*/*/*/*.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-2m)
- rebuild for new GCC 4.6

* Mon Jan 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-8m)
- rebuild for new GCC 4.5

* Tue Sep 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-5m)
- rebuild against qt-4.6.3-1m

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 13 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-2m)
- add BuildPreReq

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Sun Feb 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.1-4m)
- rebuild against python-2.6.1-1m and PyQt4-4.4.4-2m

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-2m)
- rebuild against gcc43

* Sat Jan 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- initial build for Momonga Linux 4
