%global momorel 5

Summary: Network File System Support Library
Name: nfs-utils-lib
Version: 1.1.5
Release: %{momorel}m%{?dist}
URL: http://www.citi.umich.edu/projects/nfsv4/linux/
License: GPL

%define idmapvers 0.24
%define libnfsidmap libnfsidmap
%define rpcsecgssvers 0.18
%define librpcsecgss librpcsecgss
%define libs %{librpcsecgss} %{libnfsidmap}

%define _docdir             %{_defaultdocdir}/%{name}-%{version}
%define librpcsecgss_docdir %{_docdir}/%{librpcsecgss}-%{rpcsecgssvers}
%define libnfsidmap_docdir  %{_docdir}/%{libnfsidmap}-%{idmapvers}

Source0: http://www.citi.umich.edu/projects/nfsv4/linux/libnfsidmap/%{libnfsidmap}-%{idmapvers}.tar.gz
NoSource: 0
Source1: http://www.citi.umich.edu/projects/nfsv4/linux/librpcsecgss/%{librpcsecgss}-%{rpcsecgssvers}.tar.gz
NoSource: 1

Patch000: nfs-utils-lib-changelicensetoBSD.patch
Patch001: nfs-utils-lib-1.1.5-libnfsidmap-0-25-rc1.patch

Patch100: nfs-utils-lib-fedora-idmapd.conf-default.patch
Patch101: nfs-utils-lib-1.1.5-warnings.patch
Patch102: nfs-utils-lib-1.1.5-compile.patch

Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig, gettext >= 0.15-9m
BuildRequires: libgssglue-devel , openldap-devel >= 2.4.0
Requires(postun): /sbin/ldconfig
Requires(pre): /sbin/ldconfig
Requires: libgssglue , openldap

%description
Support libraries that are needed by the commands and 
daemons the nfs-utils rpm.

%package devel
Summary: Development files for the nfs-utils-lib library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package includes header files and libraries necessary for
developing programs which use the nfs-utils-lib library.

%prep
%setup -c -q -a1
mv %{libnfsidmap}-%{idmapvers} %{libnfsidmap}
mv %{librpcsecgss}-%{rpcsecgssvers} %{librpcsecgss}

%patch000 -p1
%patch001 -p1

%patch100 -p1
%patch101 -p1
%patch102 -p1

%build
pushd  %{libnfsidmap}
./autogen.sh
%configure --disable-static
make %{?_smp_mflags} all
popd

pushd  %{librpcsecgss}
%configure --disable-static
make %{?_smp_mflags} all
popd

%install
pushd  %{libnfsidmap}
make install DESTDIR=%{buildroot}
popd

pushd  %{librpcsecgss}
make install DESTDIR=%{buildroot}
popd

mkdir -p %{buildroot}/%{librpcsecgss_docdir}
pushd %{librpcsecgss}
for file in AUTHORS ChangeLog NEWS README ; do
        install -m 644 $file %{buildroot}/%{librpcsecgss_docdir}
done
popd

mkdir -p %{buildroot}/%{libnfsidmap_docdir}
pushd %{libnfsidmap} 
for file in AUTHORS ChangeLog NEWS README ; do
        install -m 644 $file %{buildroot}/%{libnfsidmap_docdir}
done
popd

mkdir -p %{buildroot}/etc
install -m 644 %{libnfsidmap}/idmapd.conf $RPM_BUILD_ROOT/etc/idmapd.conf
mkdir -p %{buildroot}%{_mandir}/man5

# Delete unneeded libtool libs
rm -rf %{buildroot}%{_libdir}/*.la %{buildroot}%{_libdir}/libnfsidmap/*.la
        
%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root)
%dir %{_docdir}
%{_libdir}/librpcsecgss.so.*
%dir %{librpcsecgss_docdir}
%{librpcsecgss_docdir}/*

%config(noreplace) /etc/idmapd.conf
%{_libdir}/libnfsidmap*.so.*
%dir %{libnfsidmap_docdir}
%{libnfsidmap_docdir}/*
%{_libdir}/pkgconfig/libnfsidmap.pc
%dir %{_libdir}/libnfsidmap
%{_libdir}/libnfsidmap/*.so
%{_mandir}/man3/nfs4_uid_to_name.3.*
%{_mandir}/man5/idmapd.conf.5.*

%files devel
%defattr(0644,root,root,755)
%{_libdir}/librpcsecgss.so
%dir %{_includedir}/rpcsecgss
%dir %{_includedir}/rpcsecgss/rpc
%{_includedir}/rpcsecgss/rpc/auth.h
%{_includedir}/rpcsecgss/rpc/auth_gss.h
%{_includedir}/rpcsecgss/rpc/svc.h
%{_includedir}/rpcsecgss/rpc/svc_auth.h
%{_includedir}/rpcsecgss/rpc/rpc.h
%{_includedir}/rpcsecgss/rpc/rpcsecgss_rename.h
%{_includedir}/nfsidmap.h
%{_libdir}/libnfsidmap*.so
%{_libdir}/pkgconfig/librpcsecgss.pc

%changelog
* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-5m)
- add fedora patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5 (almost sync with Fedora devel)
-- update libnfsidmap to 0.23
-- update librpcsecgss to 0.19

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-6m)
- add %%dir

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-4m)
- move pc file to devel

* Thu May 28 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.4-3m)
- libtool build fix

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-1m)
- update 1.1.4
- import 4 patches from fc-devel (nfs-utils-lib-1_1_4-1_fc10)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1
- use libgssglue

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-2m)
- %%NoSource -> NoSource

* Wed Aug 15 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-1m)
- merged changes from fc-devel (nfs-utils-lib-1_1_0-1_fc8)
-* Sat Jul 28 2007  Steve Dickson <steved@redhat.com> 1.1.0-1
-- Updated libnfsidmap to the 0.20 release
-- Added rules to install/remove /etc/idmap.conf
-* Mon Mar 12 2007  Steve Dickson <steved@redhat.com> 1.0.8-9
-- Removed the --prefix=$RPM_BUILD_ROOT from the %%configure (bz 213152)

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.10-2m)
- revive la files and move them to main package
- rebuild against libgssapi-0.10-3m

* Thu Feb 22 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.10-1m)
- up to librpcsecgss-0.14 and libnfsidmap-0.19

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-3m)
- delete libtool library

* Sat Jun 10 2006 Masahiro Takahatata <takahata@momonga-linux.org>
- (1.0.8-2m)
- delete duplicate files

* Fri Jun  9 2006 Masahiro Takahatata <takahata@momonga-linux.org>
- (1.0.8-1m)
- initialize based fc

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.8-3.1
- bump again for double-long bug on ppc(64)

* Thu Feb 09 2006 Florian La Roche <laroche@redhat.com> 1.0.8-3
- remove empty scripts

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.8-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Jan 19 2006 Steve Dickson <steved@redhat.com> 1.0.8-2
- Added debugging routines to libnfsidmap

* Fri Jan  6 2006 Steve Dickson <steved@redhat.com> 1.0.8-1
- Initial commit
