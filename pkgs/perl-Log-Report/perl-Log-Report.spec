%global         momorel 1

Name:           perl-Log-Report
Version:        1.04
Release:        %{momorel}m%{?dist}
Summary:        Report a problem, pluggable handlers and language support
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Log-Report/
Source0:        http://www.cpan.org/authors/id/M/MA/MARKOV/Log-Report-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Encode >= 2.00
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Log-Report-Optional >= 1.01
BuildRequires:  perl-Mojolicious
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Sys-Syslog >= 0.27
BuildRequires:  perl-Test-Pod >= 1.00
BuildRequires:  perl-Test-Simple >= 0.86
Requires:       perl-Encode >= 2.00
Requires:       perl-Log-Report-Lexicon >= 1.02
Requires:       perl-Log-Report-Optional >= 1.01
Requires:       perl-Mojolicious
Requires:       perl-Scalar-Util
Requires:       perl-Sys-Syslog >= 0.27
Requires:       perl-Test-Pod >= 1.00
Requires:       perl-Test-Simple >= 0.86
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
Handling messages to users can be a hassle, certainly when the same module
is used for command-line and in a graphical interfaces, and has to cope
with internationalization at the same time; this set of modules tries to
simplify this. Log::Report combines gettext features with Log::Dispatch-
like features. However, you can also use this module to do only
translations or only message dispatching.

%prep
%setup -q -n Log-Report-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Win32::TieRegistry)/d'

EOF
%define __perl_requires %{_builddir}/Log-Report-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{perl_vendorlib}/MojoX/Log/Report.pm
%{perl_vendorlib}/MojoX/Log/Report.pod
%{perl_vendorlib}/Log/Report
%{perl_vendorlib}/Log/Report.pm
%{perl_vendorlib}/Log/Report.pod
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- rebuild against perl-5.20.0
- update to 1.04

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01
- rebuild against perl-5.18.2

* Wed Oct 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.998-1m)
- update to 0.998

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.997-1m)
- update to 0.997

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.996-1m)
- update to 0.996

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.995-1m)
- update to 0.995

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-1m)
- update to 0.994

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-2m)
- rebuild against perl-5.18.0

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-1m)
- update to 0.993

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.992-2m)
- rebuild against perl-5.16.3

* Sat Dec 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.992-1m)
- update to 0.992

* Tue Nov 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.991-1m)
- update to 0.991

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-2m)
- rebuild against perl-5.16.2

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.90-2m)
- rebuild for new GCC 4.6

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.28-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
