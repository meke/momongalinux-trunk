%global momorel 1
%global ofxver 0.9.5
%global qtver 4.8.5
%global kdever 4.12.97
%global kdelibsrel 1m
%global kdesdkrel 1m
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global ftpdirver 1.9.0
%global sourcedir %{release_dir}/%{name}


Summary: A personal finances manager for KDE
Name: skrooge
Version: %{ftpdirver}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Productivity
URL: http://skrooge.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-1.7.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: qt-sqlite
BuildRequires: qt-devel >= %{qtver}
BuildRequires: qt-webkit-devel
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{kdever}
BuildRequires: kdesdk-devel >= %{kdever}-%{kdesdkrel}
# we need kdesdk for /usr/bin/extractrc
BuildRequires: kde-dev-scripts >= %{kdever}-%{kdesdkrel}
BuildRequires: akonadi-devel >= 1.9.0
BuildRequires: cmake
BuildRequires: grantlee-devel >= 0.3.0
BuildRequires: libofx-devel >= %{ofxver}
BuildRequires: nepomuk-core-devel >= %{kdever}
BuildRequires: qca2-devel
BuildRequires: sqlite-devel

%description
Skrooge is a personal finances manager for KDE4, aiming at being simple
and intuitive. It still lacks a few features, but should be more or less
ready for wide testing...

%package libs
Summary: Shared runtime libraries of skrooge
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of skrooge.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/oxygen/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS CHANGELOG COPYING README TODO
%{_kde4_bindir}/akonadi_skroogeakonadi_resource
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/%{name}convert
%{_kde4_libdir}/libskgbankgui.so*
%{_kde4_libdir}/libskgbasegui.so*
%{_kde4_libdir}/kde4/skg*.so
%{_kde4_libdir}/kde4/%{name}_*.so
%{_kde4_libdir}/kde4/plugins/grantlee
%{_kde4_datadir}/akonadi/agents/skroogeakonadiresource.desktop
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/skg*
%{_kde4_appsdir}/%{name}*
%{_kde4_datadir}/config/skrooge_monthly.knsrc
%{_kde4_datadir}/config/skrooge_unit.knsrc
%{_kde4_datadir}/config.kcfg/skg*.kcfg
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/*/application-x-skg*.png
%{_kde4_iconsdir}/hicolor/*/*/skg*
%{_kde4_iconsdir}/hicolor/*/*/%{name}*.png
%{_kde4_iconsdir}/hicolor/*/*/application-x-skg*.svgz
%{_kde4_iconsdir}/hicolor/*/*/%{name}*.svgz
%{_kde4_datadir}/kde4/services/plasma-runner-skrooge-add-operation.desktop
%{_kde4_datadir}/kde4/services/skgadvicedataengine.desktop
%{_kde4_datadir}/kde4/services/skg-plugin-*.desktop
%{_kde4_datadir}/kde4/services/%{name}-plugin-*.desktop
%{_kde4_datadir}/kde4/services/%{name}-import-*.desktop
%{_kde4_datadir}/kde4/servicetypes/skg-plugin.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}-import-plugin.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/mime/packages/x-skg.xml
%{_datadir}/pixmaps/%{name}.png

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/libskgbank*.so*
%{_kde4_libdir}/libskgbase*.so*
%exclude %{_kde4_libdir}/libskgbankgui.so*
%exclude %{_kde4_libdir}/libskgbasegui.so*

%changelog
* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- version 1.9.0

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- version 1.8.0

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.8-1m)
- version 1.7.8

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- version 1.7.7

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.5-1m)
- version 1.7.5

* Mon Jun 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.4-1m)
- version 1.7.4

* Tue May 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- version 1.7.1

* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- version 1.4.0
- rebuild against libofx-0.9.5, grantlee-0.3.0

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- version 1.3.3

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- version 1.3.2

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- version 1.3.0

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- version 1.1.1

* Tue Nov 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- version 1.1.0

* Fri Oct  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0

* Fri Aug 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- version 0.9.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- version 0.9.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- version 0.8.1

* Sat Dec 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- version 0.8.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-2m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-1m)
- version 0.7.3

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-1m)
- version 0.7.2
- split package libs
- update desktop.patch

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-2m)
- rebuild against qt-4.6.3-1m

* Wed May 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1
- update desktop.patch

* Mon May 10 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-2m)
- add BuildRequires

* Fri Apr  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.0-1m)
- version 0.7.0
- update desktop.patch

* Sun Feb  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- version 0.6.0
- update desktop.patch

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.5-1m)
- version 0.5.5

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-1m)
- version 0.5.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-1m)
- version 0.5.3
- update desktop.patch
- remove merged docdir.patch

* Thu Oct  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-1m)
- version 0.5.2
- set NoSource: 0 again

* Wed Sep 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-2m)
- unset NoSource: 0 for the moment

* Mon Sep 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1
- update desktop.patch

* Sat Aug 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0
- update URL
- update desktop.patch
- remove fix-build.patch
- add fix-docdir.patch

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-2m)
- rebuild against libofx-0.9.1

* Thu Jun  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-1m)
- version 0.2.9
- update desktop.patch

* Tue May  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8-1m)
- version 0.2.8
- remove automoc-lib64.patch

* Sat May  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.7-2m)
- fix build on x86_64

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.7-1m)
- version 0.2.7

* Thu Apr  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.6-2m)
- fix BPR

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.6-1m)
- version 0.2.6
- update desktop.patch

* Tue Mar 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-1m)
- version 0.2.5
- update desktop.patch

* Wed Mar  4 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-2m)
- add "BuildPreReq: kdesdk" for /usr/bin/extractrc

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-1m)
- initial package for Momonga Linux
