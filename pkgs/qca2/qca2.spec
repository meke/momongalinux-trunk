%global momorel 2

Name:           qca2
Version:        2.0.3
Release:        %{momorel}m%{?dist}
Summary:        Qt Cryptographic Architecture
Group:          System Environment/Libraries
License:        LGPL
URL:            http://delta.affinix.com/qca
Source0:        http://delta.affinix.com/download/qca/2.0/qca-%{version}.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= 4.7.0
# For certstore
BuildRequires:  openssl

%description
Taking a hint from the similarly-named Java Cryptography Architecture,
QCA aims to provide a straightforward and cross-platform crypto API,
using Qt datatypes and conventions. QCA separates the API from the
implementation, using plugins known as Providers. The advantage of this
model is to allow applications to avoid linking to or explicitly depending
on any particular cryptographic library. This allows one to easily change
or upgrade crypto implementations without even needing to recompile the
application!

%package        devel
Summary:        Qt Cryptographic Architecture development files
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
This packages contains the development files for QCA

%prep
%setup -q -n qca-%{version}

%build
unset QTDIR
./configure \
  --prefix=%{_prefix} \
  --includedir=%{_includedir} \
  --libdir=%{_libdir} \
  --datadir=%{_datadir} \
  --no-separate-debug-info \
  --verbose

sed -i -e /strip/d Makefile
make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README TODO
%{_bindir}/qcatool2
%{_libdir}/*.so.*
%{_mandir}/*/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/QtCrypto
%{_libdir}/*.so
%{_libdir}/pkgconfig/qca2.pc
%{_libdir}/libqca.prl
%{_libdir}/qt4/mkspecs/features/crypto.prf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-2m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-7m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-4m)
- rebuild against qt-4.6.3-1m

* Mon Nov 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-3m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-2m)
- rebuild against rpm-4.6

* Sun Aug 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-2m)
- rebuild against gcc43

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- import from Fedora devel

* Sun Oct 21 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-1
- version 2.0.0 final

* Sun Oct 21 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-0.4.beta7
- fix build on x86_64

* Sun Oct 21 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-0.3.beta7
- missing BR: openssl

* Thu Sep 13 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-0.2.beta7
- review from bug 289681 (thanks Rex)

* Sun Sep 09 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-0.1.beta7
- initial package 
