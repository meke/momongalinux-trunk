%global         momorel 6

Name:           perl-Net-IP
Version:        1.26
Release:        %{momorel}m%{?dist}
Summary:        Net::IP Perl module
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-IP/
Source0:        http://www.cpan.org/authors/id/M/MA/MANU/Net-IP-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides functions to deal with IPv4/IPv6 addresses. The module
can be used as a class, allowing the user to instantiate IP objects, which
can be single IP addresses, prefixes, or ranges of addresses. There is also
a procedural way of accessing most of the functions. Most subroutines can
take either IPv4 or IPv6 addresses transparently.

%prep
%setup -q -n Net-IP-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING ipcount iptab README
%{_bindir}/ipcount
%{_bindir}/iptab
%{perl_vendorlib}/Net/IP.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-2m)
- rebuild against perl-5.16.3

* Thu Nov 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.25-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.25-4m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.25-3m)
- use vendor

* Thu Aug 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.25-2m)
- modify install dir

* Fri Jun  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.25-1m)
- update to 1.25

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.24-3m)
- built against perl-5.8.8

* Sun Jan  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-2m)
- enable lib64

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.24-1m)
- initial import to Momonga Linux 2

