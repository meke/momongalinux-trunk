%global momorel 10

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%define python_abi %(%{__python} -c "import sys; print sys.version[:3]")

%define srcname  PyProtocols
%define eggver   1.0
%define alphatag a0dev
%define revision r2302
%define filever  %{eggver}%{alphatag}-%{revision}

Name:           python-protocols
Version:        1.0
Release:        0.6.%{alphatag}_%{revision}.%{momorel}m%{?dist}

Summary:        Open Protocols and Component Adaptation for Python

Group:          Development/Libraries
License:        "PSF" or "ZPL"
URL:            http://peak.telecommunity.com/PyProtocols.html
Source0:        http://www.turbogears.org/download/eggs/%{srcname}-%{filever}.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel >= 0.6c9-2m

%description
PyProtocols is an implementation of PEP 246 allowing Python programmers to
define Interfaces and adapters between them, thereby reducing or eliminating
fragile 'isinstance' if type() comparisons.

%prep
%setup -q -n %{srcname}-%{filever}

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT --single-version-externally-managed

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README.txt CHANGES.txt docs/*
%dir %{python_sitearch}/protocols
%{python_sitearch}/protocols/*.so
%{python_sitearch}/protocols/*.py
%{python_sitearch}/protocols/*.pyc
%{python_sitearch}/protocols/*.pyo
%{python_sitearch}/protocols/*/*.py
%{python_sitearch}/protocols/*/*.pyc
%{python_sitearch}/protocols/*/*.pyo
%dir %{python_sitearch}/%{srcname}-%{eggver}%{alphatag}_%{revision}-py%{python_abi}.egg-info
%{python_sitearch}/%{srcname}-%{eggver}%{alphatag}_%{revision}-py%{python_abi}.egg-info/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.6.a0dev_r2302.10m)
- rebuild for new python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.5m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.6.a0dev_r2302.4m)
- rebuild against python-2.6.1

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.3m)
- rebuild against python-setuptools-0.6c9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.6.a0dev_r2302.2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-0.6-a0dev_r2302.1m)
- import from Fedora

* Thu May 10 2007 Luke Macken <lmacken@redhat.com> 1.0-0.6-a0dev_r2302
- Add revision back to Release

* Thu May 10 2007 Luke Macken <lmacken@redhat.com> 1.0-0.5-a0dev_r2302
- 1.0a0dev-r2302

* Fri Dec 15 2006 Luke Macken <lmacken@redhat.com> 1.0-0.4-a0dev_r2082
- Rebuild

* Sat Sep 16 2006 Shahms E. King <shahms@shahms.com> 1.0-0.3-a0dev_r2082
- Rebuild for FC6

* Fri Aug 11 2006 Shahms E. King <shahms@shahms.com> 1.0-0.2.a0dev_r2082
- Include, don't ghost .pyo files per new guidelines

* Wed Apr 19 2006 Shahms E. King <shahms@shahms.com> 1.0-0.1.a0dev_r2082
- Update to new upstream location and snapshot version

* Mon Feb 13 2006 Shahms E. King <shahms@shahms.com> 0.9.3-7
- Rebuild for FC5

* Tue Jan 31 2006 Shahms E. King <shahms@shahms.com> 0.9.3-6
- BuildRequires setuptools, rather than including it

* Thu May 12 2005 Shahms E. King <shahms@shahms.com> 0.9.3-4
- rebuilt, add dist tag

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Mar 21 2005 Shahms E. King <shahms@shahms.com> - 0.9.3-3
- Replace python_sitelib with python_sitearch
- Other misc. specfile cleanups

* Tue Mar 01 2005 Shahms E. King <shahms@shahms.com> 0.9.3-2
- Clean up spec file

* Tue Aug 31 2004 Shahms E. King <shahms@shahms.com> -
- Update to 0.9.3

* Fri Jul 30 2004 Shahms E. King <shahms@shahms.com> -
- Update to 0.9.3rc2

* Thu May 27 2004 Shahms King <shahms@shahms.com> -
- Initial Release

