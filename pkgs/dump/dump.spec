%global momorel 1

Summary: Programs for backing up and restoring ext2/ext3 filesystems
Name: dump

# XXX --enable-kerberos		needs krcmd
%global myoptions --with-binmode=0755 --with-manowner=root --with-mangrp=root --with-manmode=0644 --with-dumpdates="%{_sysconfdir}/dumpdates --disable-static"

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# %_specoptdir/dump.specopt and edit it.
%{?!with_readline:	%global with_readline	0}

Version: 0.4b43
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Archiving
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://dump.sourceforge.net/
BuildRequires: e2fsprogs-devel >= 1.40.6
BuildRequires: zlib-devel >= 1.2.3-1m
BuildRequires: bzip2-devel
BuildRequires: device-mapper, libselinux-devel, libsepol-devel
BuildRequires: e2fsprogs-devel >= 1.18, readline-devel >= 4.2
BuildRequires: zlib-devel, bzip2-devel, ncurses-devel, autoconf, automake
BuildRequires: device-mapper-devel, libselinux-devel, libsepol-devel
# This Requires is now mandatory because we only ship static binaries, and
# need to ensure the "disk" group is created before installation (#60461)
Requires: setup, e2fsprogs-libs, zlib, bzip2, device-mapper
Requires: rmt, libselinux, libsepol, glibc
# This Requires is now required because need to ensure the "disk" group
# is created before installation
Requires: setup
Requires: rmt
Obsoletes: dump-static
Provides: dump-static
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The dump package contains both dump and restore. Dump examines files
in a filesystem, determines which ones need to be backed up, and
copies those files to a specified disk, tape, or other storage medium.
The restore command performs the inverse function of dump; it can
restore a full backup of a filesystem. Subsequent incremental backups
can then be layered on top of the full backup. Single files and
directory subtrees may also be restored from full or partial backups.

Install dump if you need a system for both backing up filesystems and
restoring filesystems after backups.

%package -n rmt
Summary: Provides certain programs with access to remote tape devices.
Group: Applications/Archiving

%description -n rmt
The rmt utility provides remote access to tape devices for programs
like dump (a filesystem backup program), restore (a program for
restoring files from a backup), and tar (an archiving program).

%prep
%setup -q

%build
%if ! %{with_readline}
%append myoptions -- --disable-readline
%endif

autoreconf -ifv

# XXX --enable-kerberos needs krcmd
%configure --disable-static \
    --enable-transselinux \
    --enable-rmt \
    --enable-largefile \
    --enable-qfa \
    --enable-readline \
    --with-binmode=0755 \
    --with-manowner=root \
    --with-mangrp=root \
    --with-manmode=0644 \
    --with-dumpdates="%{_sysconfdir}/dumpdates"
 
make OPT="$RPM_OPT_FLAGS -Wall -Wpointer-arith -Wstrict-prototypes \
                         -Wmissing-prototypes -Wno-char-subscripts"
 


make distclean

%configure %{myoptions}

make OPT="%{optflags} -Wall -Wpointer-arith -Wstrict-prototypes -Wmissing-prototypes -Wno-char-subscripts"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}/%{_sbindir} 
mkdir -p %{buildroot}%{_mandir}/man8

%makeinstall \
    SBINDIR=%{buildroot}%{_sbindir} \
    BINDIR=%{buildroot}%{_sbindir} \
    MANDIR=%{buildroot}%{_mandir}/man8 \
    BINOWNER=$(id -un) \
    BINGRP=$(id -gn) \
    MANOWNER=$(id -un) \
    MANGRP=$(id -gn)

pushd %{buildroot}
    ln -sf dump .%{_sbindir}/rdump
    ln -sf dump .%{_sbindir}/rdump.static
    ln -sf dump .%{_sbindir}/dump.static
    ln -sf restore .%{_sbindir}/rrestore
    ln -sf restore .%{_sbindir}/rrestore.static
    ln -sf restore .%{_sbindir}/restore.static
    chmod ug-s .%{_sbindir}/rmt
    mkdir -p .%{_sysconfdir}
    > .%{_sysconfdir}/dumpdates
    ln -sf ..%{_sbindir}/rmt .%{_sysconfdir}/rmt
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES COPYRIGHT KNOWNBUGS MAINTAINERS README REPORTING-BUGS THANKS TODO
%doc dump.lsm examples
%attr(0664,root,disk)   %config(noreplace) %{_sysconfdir}/dumpdates
%{_sbindir}/dump
%{_sbindir}/rdump
%{_sbindir}/restore
%{_sbindir}/rrestore
%{_sbindir}/*.static
%{_mandir}/man8/dump.*
%{_mandir}/man8/rdump.*
%{_mandir}/man8/restore.*
%{_mandir}/man8/rrestore.*

%files -n rmt
%defattr(-,root,root)
%{_sbindir}/rmt
%{_sysconfdir}/rmt
%{_mandir}/man8/rmt.* 

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4b43-1m)
- update 0.4b43

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4b42-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4b42-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4b42-2m)
- full rebuild for mo7 release

* Tue May 18 2010 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (0.4b42-1m)
- update to 0.4b42

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4b41-11m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4b41-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4b41-9m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4b41-8m)
- drop Patch3 for fuzz=0, bombing by mistake
- License: Modified BSD

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4b41-7m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4b41-6m)
- disable-static

* Mon Mar 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4b41-5m)
- remove BuildRequre libtermcap

* Sat Mar  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4b41-4m)
- support newer device-mapper

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4b41-3m)
- %%NoSource -> NoSource

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4b41-2m)
- import dump-dmfix.patch from Fedora Core devel
 +* Tue Jan 11 2006 Jindrich Novy <jnovy@redhat.com> 0.4b41-1
 +- link against device-mapper

* Sat Jan 21 2006 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b41-1m)
- update to 0.4b41
  maintenance release

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.4b40-3m)
- rebuild against zlib-1.2.3 (CAN-2005-1849)

* Sat Jul 12 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- rebuild dump-static against zlib-1.2.2-2m, which fixes CAN-2005-2096.

* Tue May 10 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b40-1m)
- update to 0.4b40
  Major feature enhancements: support for ext2/ext3 extended
  attributes (EA).
- sync with original SRPM.

* Thu Jan 27 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b39-1m)
- update to 0.4b39
  Major bugfixes: 'restore -C' which could cause damage on the
  filesystem being compared.

* Thu Jan 13 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b38-1m)
- update to 0.4b38
  several small bug fixes and a number of performance enhancements

* Tue Sep  7 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b37-1m)
- update to 0.4b37

* Sat Apr 24 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b36-1m)
- update to 0.4b36
- remove readline support default configuration
  conflict between BSD license and readline's

* Fri Dec 26 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b35-1m)
- update to 0.4b35

* Sat Aug  9 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.4b34-1m)
- update to 0.4b34
- use %%{momorel} macro
- change Licence from UCB to BSD
- use %%Source0: and 
- add --enable-readline, --enable-qfa, --enable-largefile for configure
- add examples to %%doc

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4b32-2m)
- rebuild against zlib 1.1.4-5m
- delete define rpm macros _sbindir and _sysconfdir, use /sbin

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4b32-1m)
- update to 0.4b32

* Tue Mar 12 2002 Masahiro Takahata <takahata@kondara.org>
- (0.4b27-2k)
- update to 0.4b27

* Thu Oct  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4b24-2k)
- update to 0.4b24

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4b20-4k)
- rebuild against glibc-2.2.2 and add dump.glibc222.time.h

* Thu Nov 30 2000 HIROSE Masaaki <hirose31@t3.rim.or.jp>
- (0.4b20-1k)
- update to 0.4b20

* Thu Nov  2 2000 HIROSE Masaaki <hirose31@t3.rim.or.jp>
- (0.4b19-2k)
- drop setuid, setgid bits from /sbin/dump, /sbin/restore for security issue.
- change group of /sbin/dump, /sbin/restore for root from tty.

* Fri Oct 20 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4b19-1k)
- update to 0.4b19

* Fri Aug 11 2000 AYUHANA Tomonori <l@kondara.org>
- (0.4b18-0.2k)
- add %define _sysconfdir /etc (jitter#637)
- add BuildRequires: e2fsprogs-devel

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Jul  1 2000 HIROSE Masaaki <hirose31@t3.rim.or.jp>
- dump 0.4b18 released, first packaging.
- fix buffer overflow vulnerability in restore.
- add japanese description and summary.

* Tue Jun 20 2000 AYUHANA Tomonori <l@kondara.org>
- merge from RawHide (0.4b17-1)
- remove static package
- SPEC fixed ( URL )

* Tue Mar 14 2000 HIROSE Masaaki <hirose31@t3.rim.or.jp>
- dump 0.4b16 released, first packaging.

* Fri Jan 28 2000 Takaaki Tabuchii <tab@kondara.org>
- be able to rebuild non-root user. 

* Wed Nov 18 1999 Norihito Ohmori <nono@kondara.org>
-  be a NoSrc :-P

* Wed Nov 5 1999 Stelian Pop <pop@cybercable.fr>
- dump 0.4b9 released, first packaging.

* Wed Nov 3 1999 Stelian Pop <pop@cybercable.fr>
- dump 0.4b8 released, first packaging.

* Thu Oct 8 1999 Stelian Pop <pop@cybercable.fr>
- dump 0.4b7 released, first packaging.

* Thu Sep 30 1999 Stelian Pop <pop@cybercable.fr>
- dump 0.4b6 released, first packaging.

* Fri Sep 10 1999 Jeff Johnson <jbj@redhat.com>
- recompile with e2fsprogs = 1.15 (#4962).

* Sat Jul 31 1999 Jeff Johnson <jbj@redhat.com>
- workaround egcs bug (#4281) that caused dump problems (#2989).
- use sigjmp_buf, not jmp_buf (#3260).
- invoke /etc/rmt (instead of rmt) like other unices. (#3272).
- use glibc21 err/glob rather than the internal compatibility routines.
- wire $(OPT) throughout Makefile's.
- fix many printf problems, mostly lint clean.
- merge SuSE, Debian and many OpenBSD fixes.

* Thu Mar 25 1999 Jeff Johnson <jbj@redhat.com>
- remove setuid/setgid bits from /sbin/rmt (dump/restore are OK).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 6)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- strip binaries.

* Thu Mar 18 1999 Jeff Johnson <jbj@redhat.com>
- Fix dangling symlinks (#1551).

* Wed Mar 17 1999 Michael Maher <mike@redhat.com>
- Top O' the morning, build root's fixed for man pages.  

* Fri Feb 19 1999 Preston Brown <pbrown@redhat.com>
- upgraded to dump 0.4b4, massaged patches.

* Tue Feb 02 1999 Ian A Cameron <I.A.Cameron@open.ac.uk>
- added patch from Derrick J Brashear for traverse.c to stop bread errors

* Wed Jan 20 1999 Jeff Johnson <jbj@redhat.com>
- restore original 6755 root.tty to dump/restore, defattr did tty->root (#684).
- mark /etc/dumpdates as noreplace.

* Tue Jul 14 1998 Jeff Johnson <jbj@redhat.com>
- add build root.

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- added a patch for resolving linux/types.h and sys/types.h conflicts

* Wed Dec 31 1997 Erik Troan <ewt@redhat.com>
- added prototype of llseek() so dump would work on large partitions

* Thu Oct 30 1997 Donnie Barnes <djb@redhat.com>
- made all symlinks relative instead of absolute

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Thu Mar 06 1997 Michael K. Johnson <johnsonm@redhat.com>
- Moved rmt to its own package.

* Tue Feb 11 1997 Michael Fulbright <msf@redhat.com>
- Added endian cleanups for SPARC

* Fri Feb 07 1997 Michael K. Johnson <johnsonm@redhat.com> 
- Made /etc/dumpdates writeable by group disk.
