%global        momorel 1

Summary:       A DSSI Software Synthesizer Plugin
Name:          whysynth
Version:       20120729
Release:       %{momorel}m%{?dist}
License:       GPLv2
URL:           http://smbolton.com/whysynth.html
Group:         Applications/Multimedia
Source0:       http://smbolton.com/%{name}/%{name}-%{version}.tar.bz2
NoSource:      0
BuildRequires: dssi-devel >= 0.9, liblo >= 0.12, pkgconfig, fftw >= 2.0.0
BuildRequires: gtk2-devel >= 2.4, ladspa-devel >= 1.0, alsa-lib-devel
Requires:      dssi, alsa-lib

%description
WhySynth is a versatile softsynth which operates as a plugin for the
DSSI Soft Synth Interface.  A brief list of features:

- 4 oscillators, 2 filters, 3 LFOs, and 5 envelope generators per
    voice.

- 10 oscillator modes: minBLEP, wavecycle, asynchronous granular,
    three FM modes, waveshaper, noise, PADsynth, and phase
    distortion.

- 6 filter modes.

- flexible modulation and mixdown options, plus effects.

WhySynth is something of a mongrel, combining bits from Xsynth-DSSI,
hexter, Csound, Mats Olsson's MSS, and various other programs, with
inspiration from a number of my favorite long-hair-days synths
(Matrix 6, ESQ-1, K4), and wavecycle data resynthesized from Claude
Kaber's Virtual K4 samples and //christian's exegesis of the Ensoniq
SQ-80 wavetable ROMs. See the enclosed file AUTHORS for more
details.

The patches distributed with WhySynth, including the default
'factory' patches and those found in the 'extra' directory, have
been placed in the public domain by their respective authors.  See
the enclosed file COPYING-patches for details.

### '

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{_libdir}/pkgconfig
export PKG_CONFIG_PATH

%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%doc extra/COPYING-patches
%doc doc
%{_libdir}/dssi/whysynth.so
%{_libdir}/dssi/whysynth.la
%{_libdir}/dssi/whysynth/
%{_datadir}/whysynth

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120729-1m)
- update to 20120729

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090608-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090608-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090608-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090608-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20090608-2m)
- add %%clean section

* Sun Sep 13 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (20090608-1m)
- Initial Build for Momonga Linux
