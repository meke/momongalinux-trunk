%global momorel 11

# Generated from opml-1.0.0.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname opml
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: A simple wrapper for parsing OPML files
Name: rubygem-%{gemname}
Version: 1.0.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyforge.org/projects/opml/
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
A simple wrapper for parsing OPML files.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec
%doc %{gemdir}/doc/%{gemname}-%{version}

%changelog
* Tue Nov 15 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-11m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-6m)
- fix build

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-5m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-4m)
- remove fixing opml-1.0.0.gemspec part
- probably install well with rubygems-1.3.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-3m)
- rebuild against gcc43

* Wed Feb 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-2m)
- modify opml-1.0.0.gemspec 

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-1m)
- Initial package for Momonga Linux
