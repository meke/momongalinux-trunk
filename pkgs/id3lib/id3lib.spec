%global momorel 15

Summary: A software library for manipulating ID3v1 and ID3v2 tags.
Name: id3lib
Version: 3.8.3
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://id3lib.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: %{name}-alpha.patch
Patch10: %{name}-%{version}-unicode16.patch
Patch11: id3lib-3.8.3-gcc43.patch

# [Security]
Patch100: %{name}-%{version}-CVE-2007-4460.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: zlib

%description
This package provides a software library for manipulating ID3v1 and ID3v2 tags.
It provides a convenient interface for software developers to include 
standards-compliant ID3v1/2 tagging capabilities in their applications.  
Features include identification of valid tags, automatic size conversions, 
(re)synchronisation of tag frames, seamless tag (de)compression, and optional
padding facilities.

%package devel
Summary: Headers for developing programs that will use id3lib
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the headers that programmers will need to develop
applications which will use id3lib, the software library for ID3v1 and ID3v2
tag manipulation.

%prep
%setup -q

%ifarch alpha alphaev5
%patch0 -p1
%endif

%patch10 -p1 -b .utf16
%patch11 -p1 -b .gcc43~

# security fix
%patch100 -p1 -b .CVE-2007-4460

%build
libtoolize -c -f
aclocal
autoconf

%configure --enable-debug=no
%make

pushd doc
make index.html
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog HISTORY NEWS README THANKS TODO
%doc doc/*.html doc/*.css doc/*.gif doc/*.png doc/*.txt
%{_bindir}/id3*
%{_libdir}/libid3*.so.*
%{_libdir}/libid3*.so

%files devel
%defattr(-, root, root)
%{_includedir}/id3
%{_includedir}/id3.h
%{_libdir}/libid3*.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.3-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.3-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.3-13m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.3-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.3-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.3-10m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.3-9m)
- %%NoSource -> NoSource

* Sun Jan  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.3-8m)
- add patch for gcc43

* Thu Oct  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.3-7m)
- [SECURITY] CVE-2007-4460
- apply id3lib-3.8.3-CVE-2007-4460.patch to fix CVE-2007-4460
- import id3lib-3.8.3-unicode16.patch from gentoo
 +- 05 Oct 2006; Diego Petteno <flameeyes@gentoo.org>
 +- +files/id3lib-3.8.3-unicode16.patch, +id3lib-3.8.3-r5.ebuild:
 +- Add patch to fix unicode16 writing. Thanks for the users in bug #130922.

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.3-6m)
- use %%NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.8.3-5m)
- delete libtool library

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.8.3-4m)
- enable x86_64.

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.8.3-3m)
- rebuild against gcc-c++-3.4.1

* Wed May 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.8.3-2m)
- [id3lib-devel] Requires: %{name} = %{version}-%{release}

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (3.8.3-1m)
- ver up.

* Thu Mar  6 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.2-2m)
- fix URL

* Sat Feb 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.8.2-1m)
  update to 3.8.2

* Sun Dec 22 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.8.0-0.0002009m)
- add BuildPreReq: libstdc++-devel

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.8.0-0.0002008m)
- use gcc_2_95_3

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.8.0-0.0002006k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.8.0-0.0002004k)
  update to 3.8.0pre2.1

* Sun Apr 28 2002 Toru Hoshina <t@@kondara.org>
- (3.8.0-0.0002002k)
- add hetare patch for alpha... no more type mismatch please.
- revised spec file. raw configure sometimes missed alpha platform.

* Sat Apr 27 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.8.0-0.0002001k)
- first release for Kondara MNU/Linux

* Sat Sep 08 2001 Cedric Tefft <cedric@earthling.net> 3.8.0pre2
- Version 3.8.0pre2

* Mon Nov 20 2000 Scott Thomas Haug <scott@id3.org> 3.8.0pre1-1
- Version 3.8.0pre1

* Thu Sep 14 2000 Scott Thomas Haug <scott@id3.org> 3.7.13-1
- Version 3.7.13

* Sat Aug 26 2000 Scott Thomas Haug <scott@id3.org> 3.7.12-2
- Removed -mpreferred-stack-boundary option from RPM_OPT_FLAGS for RedHat 6.2

* Fri Jul 07 2000 Scott Thomas Haug <scott@id3.org> 3.7.12-1
- Version 3.7.12

* Fri Jul 05 2000 Scott Thomas Haug <scott@id3.org> 3.7.11-1
- Version 3.7.11

* Fri Jun 23 2000 Scott Thomas Haug <scott@id3.org> 3.7.10-1
- Version 3.7.10

* Wed May 24 2000 Scott Thomas Haug <scott@id3.org> 3.7.9-1
- Version 3.7.9

* Wed May 10 2000 Scott Thomas Haug <scott@id3.org> 3.7.8-1
- Version 3.7.8

* Wed May 10 2000 Scott Thomas Haug <scott@id3.org> 3.7.7-1
- Version 3.7.7

* Wed May 03 2000 Scott Thomas Haug <scott@id3.org> 3.7.6-1
- Version 3.7.6

* Fri Apr 28 2000 Scott Thomas Haug <scott@id3.org> 3.7.5-1
- Version 3.7.5

* Wed Apr 26 2000 Scott Thomas Haug <scott@id3.org> 3.7.4-1
- Version 3.7.4

* Mon Apr 24 2000 Scott Thomas Haug <scott@id3.org> 3.7.3-1
- Version 3.7.3
- Added explicit RPM_OPT_FLAGS def based on arch, since -fno-exceptions and
  -fno-rtti are part of the default flags in rpmrc and we need both exceptions
  and rtti (exceptions uses rtti)

* Fri Apr 21 2000 Scott Thomas Haug <scott@id3.org> 3.7.2-1
- Version 3.7.2
- More conditional blocks for noarch
- More thorough cleaning of files for documentation
- Updated html directory

* Thu Apr 20 2000 Scott Thomas Haug <scott@id3.org> 3.7.1-2
- Fixed date of changelog entry for 3.7.1-1
- Added conditional blocks so docs only get built for noarch target

* Wed Apr 19 2000 Scott Thomas Haug <scott@id3.org> 3.7.1-1
- Version 3.7.1
- Removed zlib-devel requirement from devel
- Added doc package to distribute documentation
- Added examples package to distribute binary examples (id3tag, id3info, ...)
- Moved doc/ and examples/ source files from devel to doc package

* Mon Apr 17 2000 Scott Thomas Haug <scott@id3.org> 3.7.0-1
- First (s)rpm build
