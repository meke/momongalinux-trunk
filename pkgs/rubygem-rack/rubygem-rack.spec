# Generated from rack-1.4.1.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname rack

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: a modular Ruby webserver interface
Name: rubygem-%{gemname}
Version: 1.4.5
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rack.rubyforge.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Rack provides a minimal, modular and adaptable interface for developing
web applications in Ruby.  By wrapping HTTP requests and responses in
the simplest way possible, it unifies and distills the API for web
servers, web frameworks, and software in between (the so-called
middleware) into a single method call.
Also see http://rack.rubyforge.org.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/rackup
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/KNOWN-ISSUES
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- [SECURITY] CVE-2013-0262 CVE-2013-0263
- update 1.4.5

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-1m)
- update 1.4.1

* Wed Nov  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.5-2m)
- clenup spec

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.5-1m)
- update 1.3.5

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m) 
- update 1.3.3

* Thu Sep  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m) 
- update 1.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Mon Aug  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-2m)
- rebuild against ruby-1.9.2

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-1m)
- update 1.0.1

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update 1.0.0

* Thu Apr  2 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-1m)
- import from Fedora to Momonga for rubygem-passenger

* Mon Mar 16 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.9.1-1
- New upstream version

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Nov 09 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.4.0-2
- Remove unused macro (#470694)
- Add ruby(abi) = 1.8 as required by package guidelines (#470694)
- Move %%{gemdir}/bin/rackup to %%{_bindir} (#470694)

* Sat Nov 08 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.4.0-1
- Initial package
