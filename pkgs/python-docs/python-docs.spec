%global momorel 1

%{!?__python_ver:%define __python_ver EMPTY}

%if "%{__python_ver}" != "EMPTY"
%define main_python 0
%define python python%{__python_ver}
%else
%define main_python 1
%define python python
%endif

%define pybasever 2.7.6

Summary: Documentation for the Python programming language
Name: %{python}-docs
Version: %{pybasever}
Release: %{momorel}m%{?dist}
License: see "LICENSE"
Group: Documentation
Source0: http://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz
NoSource: 0
Source1: tools-r73552.tar.lzma
Patch0:  python-docs-2.7.3-remove-collapsiblesidebar.patch
Patch18: python-2.6-extdocmodules.patch
BuildArch: noarch
Requires: %{python} = %{version}
%if %{main_python}
Obsoletes: python2-docs
Provides: python2-docs = %{version}
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: tetex-latex, %{python}, latex2html
URL: http://www.python.org/

%description
The python-docs package contains documentation on the Python
programming language and interpreter.  The documentation is provided
in ASCII text files and in LaTeX source files.

Install the python-docs package if you'd like to use the documentation
for the Python language.

%prep
%setup -q -n Python-%{version}

%patch0 -p1

rm -rf Doc/tools
tar xzf %{SOURCE1} -C Doc

%patch18 -p1 -b .extdocmodules

%build
topdir=`pwd`

pushd Doc
make html
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,755)
%doc Misc/NEWS  Misc/README Misc/cheatsheet 
%doc Misc/HISTORY Doc/build/html

%changelog
* Mon Mar 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.6-1m)
- version 2.7.6

* Mon Jan 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.5-1m)
- version 2.7.5

* Thu May  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.4-1m)
- version 2.7.4

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- version 2.7.3
- rebuild against python-2.7.3

* Thu Jun 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.2-1m)
- version 2.7.2
- rebuild against python-2.7.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update 2.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.5-2m)
- full rebuild for mo7 release

* Sun May  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.5-1m)
- version 2.6.5
- rebuild against python-2.6.5
- update extdocmodules.patch

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.4-2m)
- use BuildRequires

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.4-1m)
- rebuild against python-2.6.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-2m)
- update tools

* Thu Jun 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.2-1m)
- rebuild against python-2.6.2
- update extdocmodules.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-2m)
- add Source1 which contains various python modules from http://svn.python.org/view/doctools/
  see checkout rule in Python-2.6.1/Doc/Makefile
- use snapshot of http://svn.python.org/view/python/trunk/Doc/tools/
  because Python-2.6.1's Doc/tools/ is buggy
- import Patch18 from Rawhide (2.6-1)

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.1-1m)
- rebuild against python-2.6.1-1m

* Mon Oct 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-3m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.1-2m)
- revised spec for debuginfo

* Mon Jun  4 2007 Ryu SASAOKA <ryu@momonga-linuex.org>
- (2.5.1-1m)
- rebuild against python 2.5.1

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5-11m)
- import from Fedora
- set Release: 11m for smooth upgrading

* Tue Dec 12 2006 Jeremy Katz <katzj@redhat.com> - 2.5-1
- update to 2.5

* Tue Oct 24 2006 Jeremy Katz <katzj@redhat.com> - 2.4.4-1
- update to 2.4.4

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2.4.3-1.1
- rebuild

* Sat Apr  8 2006 Mihai Ibanescu <misa@redhat.com> 2.4.3-1
- updated to 2.4.3

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Nov 16 2005 Mihai Ibanescu <misa@redhat.com> 2.4.2-1
- updated to 2.4.2

* Fri Apr  8 2005 Mihai Ibanescu <misa@redhat.com> 2.4.1-1
- updated to 2.4.1

* Thu Mar 17 2005 Mihai Ibanescu <misa@redhat.com> 2.4-102
- changed package to noarch

* Mon Mar 14 2005 Mihai Ibanescu <misa@redhat.com> 2.4-100
- split the doc building step into a separate source rpm
