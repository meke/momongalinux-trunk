%global momorel 13

%define real_version 0.3-4

Summary: DVD subtitle ripper
Name: subtitleripper
Version: 0.3.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://subtitleripper.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/subtitleripper/subtitleripper-%{real_version}.tgz 
NoSource: 0
Patch0: subtitleripper-0.3.4-20041108cvs.patch
Patch1: subtitleripper-0.3.4-libnetpbm.patch
Patch2: subtitleripper-0.3.4-nopng.patch
Patch3: subtitleripper-0.3.4-pgm2txt.patch
Patch4: subtitleripper-0.3.4-glibc210.patch
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: netpbm, transcode, gocr
BuildRequires: netpbm, netpbm-devel, libpng-devel, zlib-devel

%description
Converts DVD subtitles into text format (e.g. subrip) or VobSub.


%prep
%setup -n %{name}
%patch0 -p1 -b .20041108cvs
%patch1 -p1 -b .libnetpbm
%patch2 -p1 -b .nopng
%patch3 -p1 -b .pgm2txt
%patch4 -p1 -b .glibc210

%build
%{__make} %{?_smp_mflags} COPT="%{optflags}" CFLAGS="-I/usr/include/netpbm"


%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_bindir}
%{__install} -p -m 0755 pgm2txt srttool subtitle2pgm subtitle2vobsub vobsub2pgm \
    %{buildroot}%{_bindir}/

chmod 755 ChangeLog README*

%{__mkdir_p} %{buildroot}%{_datadir}/%{name}
%{__install} -m 644 gocrfilter_*.sed %{buildroot}%{_datadir}/%{name}/

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root)
%doc ChangeLog README README.gocr README.srttool README.subtitle2pgm README.vobsub
%{_bindir}/*
%{_datadir}/%{name}/gocrfilter_*.sed

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-11m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-10m)
- fix build failure with netpbm >= 10.47.17-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-8m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-7m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-6m)
- update Patch2 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-4m)
- %%NoSource -> NoSource

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.4-3m)
- fix for missing vobsub2pgm

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.4-2m)
- fix permission of docs
- install language filters

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.4-1m)
- import to Momonga from freshrpms.net


* Fri Mar 17 2006 Matthias Saou <http://freshrpms.net/> 0.3.4-3
- Release bump to drop the disttag number in FC5 build.

* Fri Apr 22 2005 Matthias Saou <http://freshrpms.net/> 0.3.4-2
- Update to 20041108 CVS (with a patch).
- Set COPT to use optflags.
- Disable png support for now as it errors out on FC4. Fix required!

* Wed May 19 2004 Matthias Saou <http://freshrpms.net/> 0.3.4-1
- Update to 0.3-4.
- Added patch to fix libppm vs. libnetpbm issue.
- Rebuild for Fedora Core 2.

* Thu Nov 13 2003 Matthias Saou <http://freshrpms.net/> 0.3.2-1
- Update to 0.3-2 (numbered as 0.3.2).
- Rebuild for Fedora Core 1.

* Mon Mar 31 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.3.
- Rebuilt for Red Hat Linux 9.

* Mon Dec  9 2002 Matthias Saou <http://freshrpms.net/>
- Spec file cleanup.

* Thu Oct 17 2002 Michel Alexandre Salim <salimma@freeshell.org>
- Initial RPM release.

