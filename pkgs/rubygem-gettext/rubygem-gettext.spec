# Generated from gettext-2.3.6.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname gettext

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Gettext is a pure Ruby libary and tools to localize messages
Name: rubygem-%{gemname}
Version: 2.3.6
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://ruby-gettext.github.com/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(locale) 
Requires: rubygem(levenshtein) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc
Obsoletes: ruby-gettext-package < %{version}-%{release}
Provides: ruby-gettext-package = %{version}-%{release}

%description
Gettext is a GNU gettext-like program for Ruby.
The catalog file(po-file) is same format with GNU gettext.
So you can use GNU gettext tools for maintaining.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/rmsginit
%{_bindir}/rxgettext
%{_bindir}/rmsgfmt
%{_bindir}/rmsgmerge
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Fri Jan  4 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.6-2m)
- revert Obsoletes: ruby-gettext-package
--       Provides: ruby-gettext-package}

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.6-1m)
- update 2.3.6

* Wed Sep 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-1m)
- update 2.3.0

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update 2.2.0

* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.0-6m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-3m)
- full rebuild for mo7 release

* Fri Nov 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-2m)
- temporarily no NoSource
-- because some hosts of gems.rubyforge.org mirror servers return old
   archives (e.g. s3.amazonaws.com)

* Mon Nov 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 04 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- renamed from ruby-gettext-package
