%global momorel 1

Summary: Utility to set/show the host name or domain name
Name: hostname
Version: 3.15
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://packages.qa.debian.org/h/hostname.html
Source0: http://ftp.de.debian.org/debian/pool/main/h/hostname/hostname_%{version}.tar.gz
NoSource: 0
Source1: hostname.1.pt
Source2: hostname.1.de

# Initial changes
Patch1: hostname-rh.patch

#net-tools < 1.60-18m includes hostname
Conflicts: net-tools < 1.60-18m

%description
This package provides commands which can be used to display the system's
DNS name, and to display or set its hostname or NIS domain name.

%prep
%setup -q -n hostname
%patch1 -p1 -b .rh

#man pages conversion
#french 
iconv -f iso-8859-1 -t utf-8 -o hostname.tmp hostname.1.fr && mv hostname.tmp hostname.1.fr

%build
export CFLAGS="$RPM_OPT_FLAGS $CFLAGS"
make

%install
make BASEDIR=%{buildroot} install

install -d %{buildroot}%{_mandir}/pt/man1
install -m 0644 %SOURCE1 %{buildroot}%{_mandir}/pt/man1/hostname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/pt/man1/dnsdomainname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/pt/man1/domainname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/pt/man1/ypdomainname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/pt/man1/nisdomainname.1

install -d %{buildroot}%{_mandir}/de/man1
install -m 0644 %SOURCE2 %{buildroot}%{_mandir}/de/man1/hostname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/de/man1/dnsdomainname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/de/man1/domainname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/de/man1/ypdomainname.1
ln -fs hostname.1 %{buildroot}%{_mandir}/de/man1/nisdomainname.1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYRIGHT
/bin/*
%{_mandir}/man1/*
%{_mandir}/*/man1/*

%changelog
* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15-1m)
- update to 3.15

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.04-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.04-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.04-3m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.04-2m)
- modify %%files

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (3.04-1m)
- import from Fedora

* Fri Apr 30 2010 Ville Skyttä <ville.skytta@iki.fi> - 3.04-2
- Mark localized man pages with %%lang.

* Thu Mar 25 2010  Jiri Popelka <jpopelka@redhat.com> - 3.04-1
- Upgrade to 3.04

* Tue Feb 02 2010  Jiri Popelka <jpopelka@redhat.com> - 3.03-1
- Upgrade to 3.03

* Tue Nov 10 2009  Jiri Popelka <jpopelka@redhat.com> - 3.01-1
- Initial package. Up to now hostname has been part of net-tools package.
- This package is based on Debian's hostname because Debian has had hostname
  as separate package since 1997 and the code is much better then the old one
  contained in net-tools.
