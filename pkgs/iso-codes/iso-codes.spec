%global         momorel 1

Name:           iso-codes
Summary:        ISO code lists and translations
Version:        3.40
Release:        %{momorel}m%{?dist}
License:        LGPLv2+
Group:          System Environment/Base
URL:            http://alioth.debian.org/projects/pkg-isocodes/
Source0:        http://pkg-isocodes.alioth.debian.org/downloads/iso-codes-%{version}.tar.xz
NoSource:       0
BuildRequires:  gettext-devel
BuildArch:      noarch
# for /usr/share/xml
Requires:       xml-common

%description
This package provides the ISO 639 Language code list, the ISO 4217
Currency code list, the ISO 3166 Territory code list, and ISO 3166-2
sub-territory lists, and all their translations in gettext format.

%package devel
Summary: Files for development using %{name}
Group:  Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the pkg-config files for development
when building programs that use %{name}.


%prep
%setup -q

%build
%configure
%make

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"

%find_lang iso-codes --all-name

%files -f iso-codes.lang
%doc ChangeLog README LICENSE
%dir %{_datadir}/xml/iso-codes
%{_datadir}/xml/iso-codes/*.xml

%files devel
%{_datadir}/pkgconfig/iso-codes.pc

%changelog
* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.40-1m)
- update to 3.40

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.37-1m)
- reimport from fedora

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.33-1m)
- update to 3.33

* Mon Dec  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.31-1m)
- update to 3.31

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.29-1m)
- update to 3.29

* Wed Jul 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.27-1m)
- update to 3.27
- no NoSource

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.25.1-1m)
- update to 3.25.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.25-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.25-1m)
- update to 3.25

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.23-1m)
- update to 3.23

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-2m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.20-1m)
- update to 3.20

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.18-2m)
- full rebuild for mo7 release

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18-1m)
- update to 3.18

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.17-1m)
- update to 3.17

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10.3-1m)
- update to 3.10.3

* Sat Jun 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10-1m)
- update to 3.10

* Sun Jun  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.9-1m)
- update to 3.9

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.8-1m)
- update to 3.8

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6-1m)
- update to 3.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3-1m)
- update to 3.3

* Sun Jun 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-2m)
- rebuild against gcc43

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- version 1.0

* Fri Apr 21 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.49-1m)
- version 0.49

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.46-2m)
- delete make check

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- first import for momonga-linux


