%global         momorel 2

Name:           rxvt-unicode
Version:        9.20
Release:        %{momorel}m%{?dist}
Summary:        Unicode version of rxvt

%{?include_specopt}
%{?!use_perl: %global use_perl 1}

Group:          User Interface/X
License:        GPLv2+
URL:            http://software.schmorp.de/pkg/rxvt-unicode.html
Source0:        http://dist.schmorp.de/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:        rxvt-unicode.desktop
Source2:        rxvt-unicode-ml.desktop
Source3:        rxvt-unicode-256color.desktop
Source4:        rxvt-unicode-256color-ml.desktop
Patch0:         rxvt-unicode-9.20-Fix-hard-coded-wrong-path-to-xsubpp.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
BuildRequires:  glib2-devel
BuildRequires:  ncurses ncurses-base ncurses-devel
BuildRequires:  desktop-file-utils
BuildRequires:  libX11-devel
BuildRequires:  libXft-devel
BuildRequires:  libXrender-devel
BuildRequires:  libXt-devel
BuildRequires:  xorg-x11-proto-devel
BuildRequires:  perl-devel, perl(ExtUtils::Embed)
%if 0%{?fedora} > 13 || !0%{?rhel}
BuildRequires:  gdk-pixbuf2-devel
%endif
%if 0%{?fedora} >= 15
BuildRequires:  libev-source
%endif
%if %{use_perl}
BuildRequires:  perl-devel, perl(ExtUtils::Embed)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
%endif
Requires:       ncurses-base

%description
rxvt-unicode is a clone of the well known terminal emulator rxvt, modified to
store text in Unicode (either UCS-2 or UCS-4) and to use locale-correct input
and output. It also supports mixing multiple fonts at the same time, including
Xft fonts.

%package ml
Summary:        Multi-language version of rxvt-unicode
Group:          User Interface/X
Requires:       %{name} = %{version}-%{release}

%description ml
Version of rxvt-unicode with enhanced multi-language support.

%package 256color
Summary:        256 color version of rxvt-unicode
Group:          User Interface/X
Requires:       %{name} = %{version}-%{release}

%description 256color
256 color version of rxvt-unicode

%package 256color-ml
Summary:        256 color multi-language version of rxvt-unicode
Group:          User Interface/X
Requires:       %{name} = %{version}-%{release}

%description 256color-ml
Version of rxvt-unicode with 256color and enhanced multi-language support.

%prep
%setup -q -c %{name}-%{version}
pushd %{name}-%{version}
%patch0 -p1
popd

cp -r %{name}-%{version} %{name}-%{version}-ml
cp -r %{name}-%{version} %{name}-%{version}-256color
cp -r %{name}-%{version} %{name}-%{version}-256color-ml

%if 0%{?fedora} >= 15
rm -rf %{name}-%{version}/libev
ln -s %{_datadir}/libev-source %{name}-%{version}/libev

rm -rf %{name}-%{version}-ml/libev
ln -s %{_datadir}/libev-source %{name}-%{version}-ml/libev

rm -rf %{name}-%{version}-256color/libev
ln -s %{_datadir}/libev-source %{name}-%{version}-256color/libev

rm -rf %{name}-%{version}-256color-ml/libev
ln -s %{_datadir}/libev-source %{name}-%{version}-256color-ml/libev
%endif

%build
# standard version
pushd %{name}-%{version}
%configure \
 --enable-keepscrolling \
 --enable-selectionscrolling \
 --enable-pointer-blank \
 --enable-utmp \
 --enable-wtmp \
 --enable-lastlog \
 --enable-xft \
 --enable-font-styles \
 --enable-afterimage \
 --enable-pixbuf \
 --enable-transparency \
 --enable-fading \
 --enable-rxvt-scroll \
 --enable-next-scroll \
 --enable-xterm-scroll \
%if !%{use_perl}
 --disable-perl \
%endif
 --enable-mousewheel \
 --enable-slipwheeling \
 --enable-smart-resize \
 --enable-frills \
 --disable-iso14755 \
 --with-term=rxvt-unicode

make CFLAGS="%{optflags}" LDFLAGS="-lfontconfig" %{?_smp_mflags}
popd

# multi-language version
pushd %{name}-%{version}-ml
%configure \
 --enable-keepscrolling \
 --enable-selectionscrolling \
 --enable-pointer-blank \
 --enable-utmp \
 --enable-wtmp \
 --enable-lastlog \
 --enable-unicode3 \
 --enable-combining \
 --enable-xft \
 --enable-font-styles \
 --enable-afterimage \
 --enable-pixbuf \
 --enable-transparency \
 --enable-fading \
 --enable-rxvt-scroll \
 --enable-next-scroll \
 --enable-xterm-scroll \
%if !%{use_perl}
 --disable-perl \
%endif
 --enable-xim \
 --enable-iso14755 \
 --with-codesets=all \
 --enable-frills \
 --enable-mousewheel \
 --enable-slipwheeling \
 --enable-smart-resize \
 --with-term=rxvt-unicode \
 --with-name=urxvt-ml

make CFLAGS="%{optflags}" LDFLAGS="-lfontconfig" %{?_smp_mflags}
popd

# 256 color version
pushd %{name}-%{version}-256color
%configure \
 --enable-keepscrolling \
 --enable-selectionscrolling \
 --enable-pointer-blank \
 --enable-utmp \
 --enable-wtmp \
 --enable-lastlog \
 --enable-xft \
 --enable-font-styles \
 --enable-afterimage \
 --enable-pixbuf \
 --enable-transparency \
 --enable-fading \
 --enable-rxvt-scroll \
 --enable-next-scroll \
 --enable-xterm-scroll \
%if !%{use_perl}
 --disable-perl \
%endif
 --enable-mousewheel \
 --enable-slipwheeling \
 --enable-smart-resize \
 --enable-frills \
 --disable-iso14755 \
 --with-term=rxvt-unicode-256color \
 --with-name=urxvt256c \
 --enable-256-color

make CFLAGS="%{optflags}" LDFLAGS="-lfontconfig" %{?_smp_mflags}
popd

# multi-language version with 256color
pushd %{name}-%{version}-256color-ml
%configure \
 --enable-keepscrolling \
 --enable-selectionscrolling \
 --enable-pointer-blank \
 --enable-utmp \
 --enable-wtmp \
 --enable-lastlog \
 --enable-unicode3 \
 --enable-combining \
 --enable-xft \
 --enable-font-styles \
 --enable-afterimage \
 --enable-pixbuf \
 --enable-transparency \
 --enable-fading \
 --enable-rxvt-scroll \
 --enable-next-scroll \
 --enable-xterm-scroll \
%if !%{use_perl}
 --disable-perl \
%endif
 --enable-xim \
 --enable-iso14755 \
 --with-codesets=all \
 --enable-frills \
 --enable-mousewheel \
 --enable-slipwheeling \
 --enable-smart-resize \
 --with-term=rxvt-unicode-256color \
 --with-name=urxvt256c-ml \
 --enable-256-color

make CFLAGS="%{optflags}" LDFLAGS="-lfontconfig" %{?_smp_mflags}
popd

%install
rm -rf %{buildroot}


for ver in \
 %{name}-%{version} %{name}-%{version}-ml \
 %{name}-%{version}-256color %{name}-%{version}-256color-ml;
do
    pushd ${ver}
    make install DESTDIR=%{buildroot}
    popd
done;

# create links for man pages
pushd %{buildroot}%{_mandir}/man1
for ver in -ml 256c 256c-ml;
do
    ln -s urxvt.1.gz urxvt${ver}.1.gz
    ln -s urxvtc.1.gz urxvt${ver}c.1.gz
    ln -s urxvtd.1.gz urxvt${ver}d.1.gz
done;
popd

# install desktop files
desktop-file-install \
  --vendor= \
  --dir=%{buildroot}%{_datadir}/applications \
  %{SOURCE1}

desktop-file-install \
  --vendor= \
  --dir=%{buildroot}%{_datadir}/applications \
  %{SOURCE2}

desktop-file-install \
  --vendor= \
  --dir=%{buildroot}%{_datadir}/applications \
  %{SOURCE3}

desktop-file-install \
  --vendor= \
  --dir=%{buildroot}%{_datadir}/applications \
  %{SOURCE4}

# install terminfo for 256color
mkdir -p %{buildroot}%{_datadir}/terminfo/r/
tic -e rxvt-unicode-256color -s -o %{buildroot}%{_datadir}/terminfo/ \
 %{name}-%{version}/doc/etc/rxvt-unicode.terminfo

# remove terminfo/r/rxvt-unicode-256color to avoid conflicting ncurses-base
# it's provided by ncurses-base and this package Requires ncurses-base
rm -f %{buildroot}%{_datadir}/terminfo/r/rxvt-unicode-256color

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc %{name}-%{version}/README.FAQ
%doc %{name}-%{version}/INSTALL
%doc %{name}-%{version}/doc/README.xvt
%doc %{name}-%{version}/doc/etc
%doc %{name}-%{version}/doc/changes.txt
%doc %{name}-%{version}/COPYING
%{_bindir}/urxvt
%{_bindir}/urxvtc
%{_bindir}/urxvtd
%{_mandir}/man1/urxvt*.1*
%{_mandir}/man3/*
%{_mandir}/man7/*
%{_datadir}/applications/*rxvt-unicode.desktop
%{_libdir}/urxvt
## this file is provided by ncurses-base
# %%{_datadir}/terminfo/r/rxvt-unicode-256color
%exclude %{_mandir}/man1/urxvt-ml*.1*
%exclude %{_mandir}/man1/urxvt256c*.1*
%exclude %{_mandir}/man1/urxvt256c-ml*.1*

%files ml
%defattr(-,root,root,-)
%{_bindir}/urxvt-ml
%{_bindir}/urxvt-mlc
%{_bindir}/urxvt-mld
%{_mandir}/man1/urxvt-ml*.1*
%{_datadir}/applications/*rxvt-unicode-ml.desktop

%files 256color
%defattr(-,root,root,-)
%{_bindir}/urxvt256c
%{_bindir}/urxvt256cc
%{_bindir}/urxvt256cd
%{_mandir}/man1/urxvt256c*.1*
%{_datadir}/applications/*rxvt-unicode-256color.desktop
%exclude %{_mandir}/man1/urxvt256c-ml*.1*

%files 256color-ml
%defattr(-,root,root,-)
%{_bindir}/urxvt256c-ml
%{_bindir}/urxvt256c-mlc
%{_bindir}/urxvt256c-mld
%{_mandir}/man1/urxvt256c-ml*.1*
%{_datadir}/applications/*rxvt-unicode-256color-ml.desktop

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.20-2m)
- rebuild against perl-5.20.0

* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (9.20-1m)
- update to 9.20
- sync with fc21

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.18-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.18-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.18-2m)
- rebuild against perl-5.18.0

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.18-1m)
- update to 9.18
- remove fedora from vendor (desktop-file-install)

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.16-2m)
- rebuild against perl-5.16.3

* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.16-1m)
- update to 9.16

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.15-2m)
- rebuild against perl-5.16.2

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-lonux.org>
- (9.15-1m)
- update to 9.15

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.14-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.14-3m)
- rebuild against perl-5.16.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.14-2m)
- rebuild for glib 2.33.2

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.14-1m)
- update to 9.14

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.12-3m)
- rebuild against perl-5.14.2

* Fri Sep  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.12-2m)
- remove terminfo/r/rxvt-unicode-256color to avoid conflicting ncurses-base
- it's provided by new ncurses-base and this package Requires ncurses-base

* Mon Aug 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.12-1m)
- update 9.12
- merge from fedora
- enable perl support by default

* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.10-3m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.10-2m)
- rebuild for new GCC 4.6

* Sun Dec 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9.10-1m)
- update to 9.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.09-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9.09-1m)
- update to 9.09

* Tue Nov  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.07-3m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.07-2m)
- full rebuild for mo7 release

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9.07-1m)
- update to 9.07

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.06-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.06-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.06-6m)
- apply glibc210 patch

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (9.06-5m)
- rebuild against perl-5.10.1

* Sun Jul 12 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.06-4m)
- temporarily disable perl support

* Sun Jul 12 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.06-3m)
- add use_perl option to avoid "Freeze on C-<right click>" issue; see http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=241

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.06-2m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.06-1m)
- update to 9.06
- change Source0 URI

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.05-2m)
- [SECURITY] CVE-2008-1142
-- import security patch from Gentoo
-- http://bugs.gentoo.org/show_bug.cgi?id=219760

* Sun Jul 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.05-1m)
- update 9.05-1
- revise BuildPreReqs
- import some changes from fc-devel (9.05-1)

* Sun Apr  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2-6m)
- add patch for glibc-2.7.90

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2-5m)
- rebuild against gcc43

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.2-4m)
- rebuild against perl-5.10.0

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.2-3m)
- BuildPreReq: freetype2 -> freetype-devel

* Sun Mar  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.2-2m)
- delete %%{_datadir}/terminfo/r/rxvt-unicode

* Mon Feb 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.2-1m)
- update to 8.2

* Sat Dec  9 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.1-1m)
- update to 8.1

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.7-4m)
- remove Utility from Categories of desktop file

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-3m)
- remove category Application TerminalEmulator

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-2m)
- revise for xorg-7.0
- remove --with-xpm-* from configure

* Sun Mar 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.7-1m)
- update to 7.7
- change install path (/usr/X11R6 -> /usr)

* Sat Jan 14 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.0-1m)
- update to 7.0

* Thu Jan  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.3-1m)
- update to 6.3

* Wed Jan  4 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.2-1m)
- update to 6.2

* Wed Dec 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.1-1m)
- update to 6.1

* Wed Jul 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.7-2m)
- add terminfo
- revised docdir permission

* Wed Jul 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.7-1m)
- update to 5.7

* Tue Jul 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.6-1m)
- initial import to Momonga

