%global momorel 5
%global pythonver 2.7

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

%define buildflags WXPORT=gtk2 UNICODE=1

Name:           wxPython
Version:        2.8.11.0
Release:	%{momorel}m%{?dist}

Summary:        GUI toolkit for the Python programming language

Group:          Development/Languages
License:        LGPLv2+
URL:            http://www.wxpython.org/
Source0:        http://dl.sourceforge.net/sourceforge/wxpython/%{name}-src-%{version}.tar.bz2
NoSource:       0
# http://trac.wxwidgets.org/ticket/10703
Patch0:         wxPython-2.8.9.2-treelist.patch
Patch1:         wxPython-2.8.11.0-aui.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libGL-devel
BuildRequires:  libGLU-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libpng-devel
BuildRequires:  libtiff-devel
BuildRequires:  pkgconfig
BuildRequires:  python-devel >= %{pythonver}
BuildRequires:  wxGTK-unicode-devel >= 2.8.11
BuildRequires:  wxGTK-unicode-gl >= 2.8.11
BuildRequires:  zlib-devel

# packages should depend on "wxPython", not "wxPythonGTK2", but in case
# one does, here's the provides for it.
Provides:       wxPythonGTK2 = %{version}-%{release}

%description
wxPython is a GUI toolkit for the Python programming language. It allows
Python programmers to create programs with a robust, highly functional
graphical user interface, simply and easily. It is implemented as a Python
extension module (native code) that wraps the popular wxWindows cross
platform GUI library, which is written in C++.

%package        devel
Group:          Development/Libraries
Summary:        Development files for wxPython add-on modules
Requires:       %{name} = %{version}-%{release}
Requires:       wxGTK-unicode-devel

%description devel
This package includes C++ header files and SWIG files needed for developing
add-on modules for wxPython. It is NOT needed for development of most
programs which use the wxPython toolkit.

%package        docs
Group:          Documentation
Summary:        Documentation and samples for wxPython
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch

%description docs
Documentation, samples and demo application for wxPython.


%prep
%setup -q -n wxPython-src-%{version}
%patch0 -p1 -b .treelist
%patch1 -p1 -b .aui

# fix libdir otherwise additional wx libs cannot be found
sed -i -e 's|/usr/lib|%{_libdir}|' wxPython/config.py


%build
# Just build the wxPython part, not all of wxWindows which we already have
# in Fedora
cd wxPython
# included distutils is not multilib aware; use normal
rm -rf distutils
python setup.py %{buildflags} build


%install
rm -rf $RPM_BUILD_ROOT
cd wxPython
python setup.py %{buildflags} install --root=$RPM_BUILD_ROOT

# this is a kludge....
%if "%{python_sitelib}" != "%{python_sitearch}"
mv $RPM_BUILD_ROOT%{python_sitelib}/wx.pth  $RPM_BUILD_ROOT%{python_sitearch}
mv $RPM_BUILD_ROOT%{python_sitelib}/wxversion.py* $RPM_BUILD_ROOT%{python_sitearch}
%endif

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc wxPython/licence
%{_bindir}/*
%{python_sitearch}/wx.pth
%{python_sitearch}/wxversion.py*
%dir %{python_sitearch}/wx-2.8-gtk2-unicode/
%{python_sitearch}/wx-2.8-gtk2-unicode/wx
%{python_sitearch}/wx-2.8-gtk2-unicode/wxPython
%{python_sitelib}/*egg-info
%{python_sitearch}/wx-2.8-gtk2-unicode/*egg-info

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/wx-2.8/wx/wxPython
%{_includedir}/wx-2.8/wx/wxPython/*.h
%dir %{_includedir}/wx-2.8/wx/wxPython/i_files
%{_includedir}/wx-2.8/wx/wxPython/i_files/*.i
%{_includedir}/wx-2.8/wx/wxPython/i_files/*.py*
%{_includedir}/wx-2.8/wx/wxPython/i_files/*.swg

%files docs
%defattr(-,root,root,-)
%doc wxPython/docs wxPython/demo wxPython/samples


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.11.0-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.11.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.11.0-1m)
- update to 2.8.11.0
- separate off docs subpackage

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9.2-3m)
- [SECURITY] CVE-2009-2369
- import upstream patch (Patch1)

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9.2-2m)
- add wxPython_common egg-info workaround for x86_64

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9.2-1m)
- update to 2.8.9.2
-- wxaddons was removed upstream
-- import Patch0 from Fedora 11 (2.8.9.2-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.7.1-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.8.7.1-2m)
- rebuild agaisst python-2.6.1-1m

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7.1-1m)
- update to 2.8.7.1

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.4.0-5m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.4.0-4m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.4.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.4.0-2m)
- %%NoSource -> NoSource

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.4.0-1m)
- version 2.8.4.0 build with wxGTK-unicode-2.8.6

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.3.3-2m)
- change source URI

* Mon Apr 09 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.3.3-1m)
- import to Momonga from Fedora

* Mon Dec 11 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.2-3
- bump release for rebuild against python 2.5.

* Mon Aug 28 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.2-2
- bump release for FC6 rebuild

* Thu Apr 13 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.2-1
- version 2.6.3.2
- move wxversion.py _into_ lib64. Apparently that's the right thing to do. :)
- upstream tarball no longer includes embedded.o (since I finally got around
  to pointing that out to the developers instead of just kludging it away.)
- buildrequires to just libGLU-devel instead of mesa-libGL-devel

* Fri Mar 31 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.0-4
- grr. bump relnumber.

* Fri Mar 31 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.0-3
- oh yeah -- wxversion.py not lib64.

* Fri Mar 31 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.0-2
- buildrequires mesa-libGLU-devel

* Thu Mar 30 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.3.0-1
- update to 2.6.3.0
- wxGTK and wxPython versions are inexorably linked; make BuildRequires
  be exact, rather than >=.
- make devel subpackage as per comment #7 in bug #163440.

* Thu Nov 24 2005 Matthew Miller <mattdm@mattdm.org> - 2.6.1.0-1
- update to 2.6.0.0
- merge in changes from current extras 2.4.x package
- Happy Thanksgiving
- build animate extention again -- works now.

* Thu Apr 28 2005 Matthew Miller <mattdm@bu.edu> - 2.6.0.0-bu45.1
- get rid of accidental binaries in source tarball -- they generates
  spurious dependencies and serve no purpose
- update to 2.6.0.0 and build for Velouria
- switch to Fedora Extras base spec file
- enable gtk2 and unicode and all the code stuff (as FE does)
- disable BUILD_ANIMATE extension from contrib -- doesn't build
- files are in a different location now -- adjust to that
- zap include files (needed only for building wxPython 3rd-party modules),
  because I don't think this is likely to be very useful. Other option
  would be to create a -devel package, but I think that'd be confusing.

* Tue Feb 08 2005 Thorsten Leemhuis <fedora at leemhuis dot info> 0:2.4.2.4-4
- remove included disutils - it is not multilib aware; this
  fixes build on x86_64

* Tue Jan 06 2004 Panu Matilainen <pmatilai@welho.com> 0:2.4.2.4-0.fdr.3
- rename package to wxPythonGTK2, provide wxPython (see bug 927)
- dont ship binaries in /usr/share

* Thu Nov 20 2003 Panu Matilainen <pmatilai@welho.com> 0:2.4.2.4-0.fdr.2
- add missing buildrequires: python-devel, wxGTK2-gl

* Sun Nov 02 2003 Panu Matilainen <pmatilai@welho.com> 0:2.4.2.4-0.fdr.1
- Initial RPM release.
~
