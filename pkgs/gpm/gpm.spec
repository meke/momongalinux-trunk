%global momorel 1

Summary: A mouse server for the Linux console
Name: gpm
Version: 1.20.7
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://www.nico.schottelius.org/software/gpm/
Source0: http://www.nico.schottelius.org/software/gpm/archives/%{name}-%{version}.tar.lzma
NoSource: 0
Source1: gpm.service
Patch1: gpm-1.20.6-multilib.patch
Patch2: gpm-1.20.1-lib-silent.patch
Patch4: gpm-1.20.5-close-fds.patch
Patch5: gpm-1.20.1-weak-wgetch.patch
Patch7: gpm-1.20.7-rhbz-668480-gpm-types-7-manpage-fixes.patch
Patch8: gpm-1.20.6-missing-header-dir-in-make-depend.patch
Requires(post): systemd-units systemd-sysv chkconfig info
Requires(preun): systemd-units info
Requires(postun): systemd-units
Requires: systemd-units
Requires: bash >= 2.0
# this defines the library version that this package builds.
%define LIBVER 2.1.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: sed gawk texinfo bison ncurses-devel autoconf automake libtool

%description
Gpm provides mouse support to text-based Linux applications like the
Emacs editor and the Midnight Commander file management system.  Gpm
also provides console cut-and-paste operations using the mouse and
includes a program to allow pop-up menus to appear at the click of a
mouse button.

%package libs
Summary: Dynamic library for for the gpm
Group: System Environment/Libraries

%description libs
This package contains the libgpm.so dynamic library which contains
the gpm system calls and library functions.

%package devel
Requires: %{name} = %{version}-%{release}
Summary: Development files for the gpm library
Group: Development/Libraries

%description devel
The gpm-devel package includes header files and libraries necessary
for developing programs which use the gpm library. The gpm provides
mouse support to text-based Linux applications.

%package static
Requires: %{name} = %{version}-%{release}
Summary: Static development files for the gpm library
Group: Development/Libraries

%description static
The gpm-static package includes static libraries of gpm. The gpm provides
mouse support to text-based Linux applications.

%prep
%setup -q

./autogen.sh

%patch1 -p1 -b .multilib
%patch2 -p1 -b .lib-silent
%patch4 -p1 -b .close-fds
%patch5 -p1 -b .weak-wgetch
%patch7 -p1
# not sure if this is really needed
%patch8 -p1

#%patch7 -p1 -b .capability

iconv -f iso-8859-1 -t utf-8 -o TODO.utf8 TODO
touch -c -r TODO TODO.utf8
mv -f TODO.utf8 TODO

%build
autoreconf -fi
%configure
%make

%install
rm -rf %{buildroot}

%makeinstall

chmod 0755 %{buildroot}%{_libdir}/libgpm.so.%{LIBVER}
ln -sf libgpm.so.%{LIBVER} %{buildroot}%{_libdir}/libgpm.so

rm -f %{buildroot}%{_datadir}/emacs/site-lisp/t-mouse.el

mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_unitdir}
install -m 644 conf/gpm-* %{buildroot}%{_sysconfdir}
# Systemd
mkdir -p %{buildroot}/%{_unitdir}
install -m644 %{SOURCE1} %{buildroot}/%{_unitdir}
rm -rf %{buildroot}%{_initscriptdir}

%clean
rm -rf %{buildroot}

%post
%systemd_post gpm.service
if [ -e %{_infodir}/gpm.info.bz2 ]; then
  /sbin/install-info %{_infodir}/gpm.info.bz2 %{_infodir}/dir || :
fi

%preun
%systemd_preun gpm.service
if [ $1 = 0 -a -e %{_infodir}/gpm.info.bz2 ]; then
  /sbin/install-info %{_infodir}/gpm.info.bz2 --delete %{_infodir}/dir || :
fi

%postun
%systemd_postun_with_restart gpm.service

%triggerun -- gpm < 1.20.6-8m
%{_bindir}/systemd-sysv-convert --save gpm >/dev/null 2>&1 ||:
/bin/systemctl enable gpm.service >/dev/null 2>&1
/sbin/chkconfig --del gpm >/dev/null 2>&1 || :
/bin/systemctl try-restart gpm.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc COPYING README TODO
%doc doc/README* doc/FAQ doc/Announce 
%{_infodir}/gpm.info*
%config(noreplace) %{_sysconfdir}/gpm-*
%{_unitdir}/gpm.service
%{_sbindir}/*
%{_bindir}/*
%{_mandir}/man?/*

%files libs
%defattr(-,root,root,-)
%{_libdir}/libgpm.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libgpm.so

%files static
%defattr(-,root,root,-)
%{_libdir}/libgpm.a

%changelog
* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20.7-1m)
- update 1.20.7

* Sun Dec 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20.6-10m)
- update URL

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20.6-8m)
- update systemd support
- drop sysv support

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20.6-8m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20.6-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20.6-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20.6-5m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20.6-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.20.6-2m)
- stop daemon

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.6-1m)
- sync with Fedora 11 (1.20.6-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.5-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.5-1m)
- sync with Rawhide (1.20.5-2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20.1-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20.1-3m)
- %%NoSource -> NoSource

* Sun Oct 21 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.20.1-3m)
- add openmax patch from fc-devel

* Mon May 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.20.1-2m)
- apply patch from fc-devel

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (1.20.1-1m)
- ver up. sync with FC3(1.20.1-66).

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.19.3-20m)
- rebuild against ncurses-5.4-1m.

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19.3-19m)
- add Patch11: gpm-1.19.3-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Mon Aug 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.19.3-18m)
- changed to use libncurses.so instead of libncurses.a for prelink

* Sat Jul  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.19.3-17m)
- stop daemon

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.19.3-16m)
- revised spec for enabling rpm 4.2.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.19.3-15m)
- pretty spec file.

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.19.3-15m)
- import patches from rawhide
- include source file

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Tue Oct 17 2000 Hidetomo Machi <mcHT@kondara.org>
- add %doc section
- move t-mouse.{el,elc} to %doc
  (Don't install under /usr/share/emacs/site-lisp in package)

* Wed Jul 26 2000 Preston Brown <pbrown@redhat.com>
- clarification: pam requirement added to fix permissions on /dev/gpmctl (#12849)

* Sat Jul 22 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.19.3

* Sat Jul 15 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jun 30 2000 Matt Wilson <msw@redhat.com>
- use sysconf(_SC_OPEN_MAX)

* Tue Jun 27 2000 Preston Brown <pbrown@redhat.com>
- don't prereq, only require initscripts

* Mon Jun 26 2000 Preston Brown <pbrown@redhat.com>
- fix up and move initscript
- prereq initscripts >= 5.20

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- fix %config tag for initscript

* Thu Jun 15 2000 Bill Nottingham <notting@redhat.com>
- move it back

* Thu Jun 15 2000 Preston Brown <pbrown@redhat.com>
- move init script

* Wed Jun 14 2000 Preston Brown <pbrown@redhat.com>
- security patch on socket descriptor from Chris Evans.  Thanks Chris.
- include limits.h for OPEN_MAX

* Mon Jun 12 2000 Preston Brown <pbrown@redhat.com>
- 1.19.2, fix up root (setuid) patch
- FHS paths

* Thu Apr  6 2000 Jakub Jelinek <jakub@redhat.com>
- 1.19.1
- call initgroups in gpm-root before spawning command as user
- make gpm-root work on big endian

* Sun Mar 26 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- call ldconfig directly in postun

* Wed Mar 22 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild with new libncurses

* Sat Mar 18 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.19.0
- fix build on systems that don't have emacs
  (configure built t-mouse* only if emacs was installed)

* Tue Feb 29 2000 Preston Brown <pbrown@redhat.com>
- important fix: improperly buildrooted for /usr/share/emacs/site-lisp, fixed.

* Tue Feb 15 2000 Jakub Jelinek <jakub@redhat.com>
- avoid cluttering of syslog with gpm No data messages

* Mon Feb 14 2000 Preston Brown <pbrown@redhat.com>
- disable-paste and mouse-test removed, they seem broken.

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- updated gpm.init to have better shutdown and descriptive messages
- strip lib

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- man pages are compressed

* Wed Jan 12 2000 Preston Brown <pbrown@redhat.com>
- 1.18.1.

* Tue Sep 28 1999 Preston Brown <pbrown@redhat.com>
- upgraded to 1.18, hopefully fixes sparc protocol issues

* Fri Sep 24 1999 Bill Nottingham <notting@redhat.com>
- install-info sucks, and then you die.

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %preun, not %postun

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 1.17.9
- the maintainers are taking care of .so version now, removed patch

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Wed Jun  2 1999 Jeff Johnson <jbj@redhat.com>
- disable-paste need not be setuid root in Red Hat 6.0 (#2654)

* Tue May 18 1999 Michael K. Johnson <johnsonm@redhat.com>
- gpm.init had wrong pidfile name in comments; confused linuxconf

* Mon Mar 22 1999 Preston Brown <pbrown@redhat.com>
- make sure all binaries are stripped, make init stuff more chkconfig style
- removed sparc-specific mouse stuff
- bumped libver to 1.17.5
- fixed texinfo source

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Thu Mar  4 1999 Matt Wilson <msw@redhat.com>
- updated to 1.75.5

* Tue Feb 16 1999 Cristian Gafton <gafton@redhat.com>
- avoid using makedev for internal functions (it is a #define in the system
  headers)

* Wed Jan 13 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 1.17.2.

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- enforce the use of -D_GNU_SOURCE so that it will compile on the ARM
- build against glibc 2.1

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 22 1998 Michael K. Johnson <johnsonm@redhat.com>
- enhanced initscript

* Fri Apr 10 1998 Cristian Gafton <gafton@redhat.com>
- recompiled for manhattan

* Wed Apr 08 1998 Erik Troan <ewt@redhat.com>
- updated to 1.13

* Mon Nov 03 1997 Donnie Barnes <djb@redhat.com>
- added patch from Richard to get things to build on the SPARC

* Tue Oct 28 1997 Donnie Barnes <djb@redhat.com>
- fixed the emacs patch to install the emacs files in the right
  place (hopefully).

* Mon Oct 13 1997 Erik Troan <ewt@redhat.com>
- added chkconfig support
- added install-info

* Thu Sep 11 1997 Donald Barnes <djb@redhat.com>
- upgraded from 1.10 to 1.12
- added status/restart functionality to init script
- added define LIBVER 1.11

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc
