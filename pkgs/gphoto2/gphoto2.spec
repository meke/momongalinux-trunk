%global momorel 1

Summary: Software for accessing digital cameras
Name: gphoto2
Version: 2.5.1
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.gphoto.org/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/gphoto/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libexif-devel >= 0.6.12
BuildRequires: libgphoto2-devel >= %{version}
BuildRequires: libtool
BuildRequires: slang-devel

%description
The gPhoto2 project is a universal, free application and library
framework that lets you download images from several different
digital camera models, including the newer models with USB
connections. Note that
a) for some older camera models you must use the old "gphoto" package.
b) for USB mass storage models you must use the driver in the kernel

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# clean up
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}.1*

%changelog
* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.0-1m)
- version 2.5.0

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.10-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.10-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.10-1m)
- update to 2.4.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.9-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-1m)
- update to 2.4.9

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.8-3m)
- rebuild against readline6

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.8-2m)
- rebuild against libjpeg-8a

* Wed Mar 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.8-1m)
- version 2.4.8
- add NEWS to %%doc
- remove %%{_docdir}/%%{name}
- remove PreReq

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.7-1m)
- update to 2.4.7

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-2m)
- %%NoSource -> NoSource

* Thu Mar 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- version 2.3.1

* Sun Jun 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-1m)
- version 2.2.0

* Sun Jun 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.6-2m)
- remove PreReq: hotplug

* Sun May 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.6-1m)
- update to 2.1.6

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.5-3m)
- rebuild against libexif-0.6.12-1m

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.5-2m)
- disable aalib

* Thu Jan 20 2005 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.5-1m)
- version 2.1.5

* Sat Jan 24 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.4-1m)
- version 2.1.4

* Fri Oct 31 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.3-1m)
- version 2.1.3

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.1.2-1m)
- version 2.1.2

* Wed Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.1-5m)
- rebuild against libexif-0.5.12
- use %%NoSource macro

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.1-4m)
- rebuild against slang(utf8 version)
- use %%{?_smp_mflags}

* Thu Feb 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-3m)
- rebuild against for libexif

* Mon Dec 10 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.1-2m)
- remove %post %postun

* Mon Dec 9 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.1-1m)
- Ver. up
- gphoto2 command line interface was split from the libgphoto2 library

* Mon Aug 5 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.0-6m)
- Build against libexif-0.5.4
- 2.1.0_for_libexif_0.5.4.patch from CVS

* Sun Aug 4 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.0-5m)
- without libexif

* Sat Jul 27 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.0-4m)
- BuildRequire slang-devel
- Modify %files section

* Sat Jul 27 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.0-3m)
- BuildRequire XFree86-devel

* Sat Jul 27 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.0-2m)
- libtoolize
- Add BuildRequires libjpeg-devel libtool

* Wed Jul 24 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.0-1m)
- Import from RawHide

* Fri Jun 28 2002 Tim Waugh <twaugh@redhta.com> 2.1.0-2
- Don't use -rpath (bug #65983).

* Tue Jun 25 2002 Tim Waugh <twaugh@redhat.com> 2.1.0-1
- 2.1.0.
- No longer need cvsfixes or consolelock patches.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 2.0-9
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com> 2.0-8
- automated rebuild

* Thu May  9 2002 Tim Waugh <twaugh@redhat.com> 2.0-7
- Require lockdev (bug #64193).

* Mon Apr 29 2002 Tim Waugh <twaugh@redhat.com> 2.0-6
- In fact, don't even build for mainframe.

* Mon Apr 29 2002 Florian La Roche <Florian.LaRoche@redhat.de> 2.0-5
- do not require hotplug for mainframe

* Mon Apr 15 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0-4
- Set the owner of the device to the console lock holder, not the owner
  of /dev/console, in the hotplug agent, fixing access for users who log
  in at VTs and use startx (#62976).

* Fri Apr 12 2002 Tim Waugh <twaugh@redhat.com> 2.0-3
- Rebuild (fixed bug #63355).

* Wed Feb 27 2002 Tim Waugh <twaugh@redhat.com> 2.0-2
- Fix from CVS: close port unconditionally in gp_camera_exit().

* Mon Feb 25 2002 Tim Waugh <twaugh@redhat.com> 2.0-1
- 2.0 is released.
- Ship the .so symlinks in the devel package.

* Mon Feb 25 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.rc4.1
- 2.0rc4.

* Fri Feb 22 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.rc3.1
- 2.0rc3.  No longer need CVS patch.
- Build no longer requires xmlto.

* Thu Feb 21 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.rc2.2
- Fix DC240 hangs (patch from CVS).
- Rebuild in new environment.

* Tue Feb 19 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.rc2.1
- 2.0rc2 (bug #59993).  No longer need docs patch or man page.
- Really fix up libtool libraries (bug #60002).

* Fri Feb 15 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.beta5.2
- PreReq /sbin/ldconfig, grep, and fileutils (bug #59941).

* Tue Feb 12 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.beta5.1
- 2.0beta5.
- Fix Makefiles so that documentation can be built.
- Ship pkgconfig file.
- Add man page.

* Thu Feb  7 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.beta4.1
- 2.0beta4.
- Build requires transfig, and at least version 0.1.5 of libusb.
- Clean up file lists.
- Build documentation.

* Fri Jan 25 2002 Tim Waugh <twaugh@redhat.com> 2.0-0.beta3.2
- Rebuild in new environment.
- Dump docbook-dtd30-sgml requirement; gtk-doc is sufficient.

* Sun Nov 18 2001 Tim Waugh <twaugh@redhat.com> 2.0-0.beta3.1
- Adapted for Red Hat Linux.

* Sat Oct 27 2001 Hans Ulrich Niedermann <gp@n-dimensional.de>
- fixed update behaviour for hotplug list (do not erase it when updating)

* Thu Oct 25 2001 Tim Waugh <twaugh@redhat.com>
- hotplug dependency is a prereq not a requires (the package scripts
  need it).

* Sun Oct 14 2001 Hans Ulrich Niedermann <gp@n-dimensional.de>
- integrated spec file into source package

* Sun Oct 14 2001 Hans Ulrich Niedermann <gp@n-dimensional.de>
- 2.0beta3

* Tue Oct  2 2001 Tim Waugh <twaugh@redhat.com> 2.0beta2-0.1
- Adapted for Red Hat Linux.
- 2.0beta2.

* Mon Aug  6 2001 Till Kamppeter <till@mandrakesoft.com> 2.0-0.beta1.2mdk
- Corrected "Requires:"

* Mon Aug  6 2001 Till Kamppeter <till@mandrakesoft.com> 2.0-0.beta1.1mdk
- Initial release
