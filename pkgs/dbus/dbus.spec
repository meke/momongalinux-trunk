%global gettext_package dbus
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

%define expat_version      2.0.0
%define libselinux_version 1.34

%define dbus_user_uid      81

%global momorel 1

Summary: D-BUS message bus
Name: dbus
Version: 1.8.4
Release: %{momorel}m%{?dist}
License: "AFL/GPL"
Group: System Environment/Libraries
URL: http://www.freedesktop.org/software/dbus/
Source0: http://dbus.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Source2: 00-start-message-bus.sh

Patch0: bindir.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: shadow-utils
Requires: libselinux >= %{libselinux_version}
Requires: libxml2-python
Requires(post): chkconfig
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
BuildRequires: Pyrex >= 0.9.4
BuildRequires: audit-libs-devel >= 2.0.4
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: doxygen
BuildRequires: expat-devel >= %{expat_version}
BuildRequires: gettext
BuildRequires: libX11-devel
BuildRequires: libcap-devel
BuildRequires: libselinux-devel >= %{libselinux_version}
BuildRequires: libtool
BuildRequires: libxslt
BuildRequires: xmlto
BuildRequires: systemd

# Conflict with cups prior to configuration file change, so that the
# %postun service condrestart works.
Conflicts: cups < 1.1.20

%description
D-BUS is a system for sending messages between applications. It is
used both for the systemwide message bus service, and as a
per-user-login-session messaging facility.

%package libs
Summary: Libraries for accessing D-BUS
Group: Development/Libraries

%description libs
Lowlevel libraries for accessing D-BUS

%package doc
Summary: Developer documentation for D-BUS
Group: Documentation
Requires: %{name} = %{version}-%{release}
Requires: devhelp

%description doc 
This package contains DevHelp developer documentation for D-Bus along with
other supporting documentation such as the introspect dtd file

%package devel
Summary: Libraries and headers for D-BUS
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel

Headers and static libraries for D-BUS.

%package x11
Summary: X11-requiring add-ons for D-BUS
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description x11

D-BUS contains some tools that require Xlib to be installed, those are
in this separate package so server systems need not install X.

%prep
%setup -q
%patch0 -p1 

autoreconf -f -i

# For some reason upstream ships these files as executable
# Make sure they are not
/bin/chmod 0644 COPYING ChangeLog NEWS

%build
COMMON_ARGS="--enable-libaudit --enable-selinux=yes --with-init-scripts=redhat --with-system-pid-file=%{_localstatedir}/run/messagebus.pid --with-dbus-user=dbus --libdir=/%{_lib} --bindir=/bin --sysconfdir=/etc --exec-prefix=/ --libexecdir=/%{_lib}/dbus-1 --with-systemdsystemunitdir=/lib/systemd/system/ --docdir=%{_pkgdocdir} --enable-doxygen-docs --enable-xml-docs --disable-silent-rules"

# leave verbose mode so people can debug their apps but make sure to
# turn it off on stable releases with --disable-verbose-mode
%configure $COMMON_ARGS --disable-tests --disable-asserts
make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/%{_libdir}/pkgconfig

#change the arch-deps.h include directory to /usr/lib[64] instead of /lib[64]
sed -e 's@-I${libdir}@-I${prefix}/%{_lib}@' %{buildroot}/%{_lib}/pkgconfig/dbus-1.pc > %{buildroot}/%{_libdir}/pkgconfig/dbus-1.pc
rm -f %{buildroot}/%{_lib}/pkgconfig/dbus-1.pc

mkdir -p %{buildroot}/%{_bindir}
mv -f %{buildroot}/bin/dbus-launch %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_libdir}/dbus-1.0/include/
mv -f %{buildroot}/%{_lib}/dbus-1.0/include/* %{buildroot}/%{_libdir}/dbus-1.0/include/
rm -rf %{buildroot}/%{_lib}/dbus-1.0

rm -f %{buildroot}/%{_lib}/*.a
rm -f %{buildroot}/%{_lib}/*.la

install -D -m755 %{SOURCE2} %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d/00-start-message-bus.sh

mkdir -p %{buildroot}%{_datadir}/dbus-1/interfaces

# Make sure that when somebody asks for D-Bus under the name of the
# old SysV script, that he ends up with the standard dbus.service name
# now.
ln -s dbus.service %{buildroot}/lib/systemd/system/messagebus.service

## %find_lang %{gettext_package}
# Delete the old legacy sysv init script
rm -rf %{buildroot}/etc/rc.d/init.d

mkdir -p %{buildroot}/var/lib/dbus

install -pm 644 -t %{buildroot}%{_pkgdocdir} \
    COPYING doc/introspect.dtd doc/introspect.xsl doc/system-activation.txt

%clean
rm -rf --preserve-root %{buildroot}

%pre
# Add the "dbus" user and group
/usr/sbin/groupadd -r -g %{dbus_user_uid} dbus 2>/dev/null || :
/usr/sbin/useradd -c 'System message bus' -u %{dbus_user_uid} -g %{dbus_user_uid} \
	-s /sbin/nologin -r -d '/' dbus 2> /dev/null || :

%post libs -p /sbin/ldconfig

%preun
if [ $1 = 0 ]; then
  /bin/systemctl stop dbus.service dbus.socket > /dev/null 2>&1 || :
fi

%postun libs -p /sbin/ldconfig

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%triggerun -- dbus < 1.4.12-1m
/sbin/chkconfig --del messagebus >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%defattr(-,root,root)

%dir %{_pkgdocdir}
%{_pkgdocdir}/COPYING

%dir %{_sysconfdir}/dbus-1
%config %{_sysconfdir}/dbus-1/*.conf
%dir %{_sysconfdir}/dbus-1/system.d
%dir %{_sysconfdir}/dbus-1/session.d
%ghost %dir %{_localstatedir}/run/dbus
%dir %{_localstatedir}/lib/dbus/
/bin/dbus-daemon
/bin/dbus-send
/bin/dbus-cleanup-sockets
/bin/dbus-run-session
/bin/dbus-monitor
/bin/dbus-uuidgen
%{_mandir}/man*/dbus-cleanup-sockets.1.*
%{_mandir}/man*/dbus-daemon.1.*
%{_mandir}/man*/dbus-run-session.1.*
%{_mandir}/man*/dbus-monitor.1.*
%{_mandir}/man*/dbus-send.1.*
%{_mandir}/man*/dbus-uuidgen.1.*
%dir %{_datadir}/dbus-1
%{_datadir}/dbus-1/services
%{_datadir}/dbus-1/system-services
%{_datadir}/dbus-1/interfaces
%dir /%{_lib}/dbus-1
# See doc/system-activation.txt in source tarball for the rationale
# behind these permissions
%attr(4750,root,dbus) /%{_lib}/dbus-1/dbus-daemon-launch-helper
/lib/systemd/system/dbus.service
/lib/systemd/system/dbus.socket
/lib/systemd/system/dbus.target.wants/dbus.socket
/lib/systemd/system/messagebus.service
/lib/systemd/system/multi-user.target.wants/dbus.service
/lib/systemd/system/sockets.target.wants/dbus.socket

%files libs
%defattr(-,root,root,-)
/%{_lib}/*dbus-1*.so.*

%files x11
%defattr(-,root,root)
%{_bindir}/dbus-launch
%{_datadir}/man/man*/dbus-launch.1.*
%{_sysconfdir}/X11/xinit/xinitrc.d/00-start-message-bus.sh

%files doc
%defattr(-,root,root)
%{_pkgdocdir}/*
%exclude %{_pkgdocdir}/COPYING

%files devel
%defattr(-,root,root)

/%{_lib}/lib*.so
%dir %{_libdir}/dbus-1.0
%{_libdir}/dbus-1.0/include/
%{_libdir}/pkgconfig/dbus-1.pc
%{_includedir}/*

%changelog
* Wed Jun 18 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Tue Nov 12 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.18-1m)
- update to 1.6.18

* Wed Oct 09 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.16-1m)
- update to 1.6.16

* Tue Jun 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.12-1m)
- update to 1.6.12
- [SECURITY] CVE-2013-2168

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.10-1m)
- update to 1.6.10

* Thu Oct 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.8-1m)
- update to 1.6.8
- [SECURITY] CVE-2012-3524

* Mon Aug  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update to 1.6.4

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update to 1.6.0

* Sat May 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.20-1m)
- update to 1.4.20
- Make D-Bus work in containers

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.18-1m)
- update to 1.4.18

* Sat Oct  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.16-4m)
- update %post

* Tue Sep 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.16-3m)
- cleanup spec

* Tue Sep 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.16-2m)
- add tmpfiles.d file

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.16-1m)
- update to 1.4.16

* Thu Sep  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.14-2m)
- remove conflict docs

* Mon Aug 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.14-1m)
- update to 1.4.14

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.12-1m)
- update to 1.4.12

* Fri Jul  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.10-4m)
- BR systemd

* Sun Jul  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.10-3m)
- update systemd support

* Fri Jul  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.10-2m)
- add systemd support

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.10-1m)
- update to 1.4.10

* Wed Apr 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.26-2m)
- rebuild for new GCC 4.6

* Fri Dec 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.26-1m)
-  update 1.2.26
- [SECURITY] CVE-2010-4352

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.24-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.24-2m)
- full rebuild for mo7 release

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.24-1m)
- update 1.2.24

* Sun Mar  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.20-1m)
- update 1.2.20

* Thu Feb  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.18-1m)
- update 1.2.18

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.16-1m)
- update 1.2.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4.6permissive-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4.6permissive-1m)
- update 1.2.4.6permissive
- [SECURITY] CVE-2009-1189

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4.2permissive-2m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4.2permissive-1m)
- update 1.2.4.2permissive
-- fix http://bugs.freedesktop.org/show_bug.cgi?id=19005
--     http://bugs.freedesktop.org/show_bug.cgi?id=19060

* Sun Dec 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-2m)
- revert to 1.2.4

* Sat Dec 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.8-1m)
- version 1.2.8

* Mon Dec  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update 1.2.6
- [SECURITY] CVE-2008-4311

* Thu Oct 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-1m)
- [SECURITY] CVE-2008-3834 version 1.2.4
- import increase-timeout.patch and re-import start-early.patch from Fedora
- remove fix_userdb_macro.patch

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3
- comment out patch2

* Sat Jul 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-6m)
- own %%{_datadir}/dbus-1/services

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-5m)
- import fix_userdb_macro.patch from Fedora
 +* Fri May 05 2008 John (J5) Palmieri <johnp@redhat.com> - 1.2.1-2
 +- patch to enable dbus userdb caching as was the default in 1.0.x
 +- previous upstream commit had accidentally disabled it because
 +  of mispelled macro names - the non-cached codepath can cause
 +  a crash of the bus
 +- fd.o bug #15588 - https://bugs.freedesktop.org/show_bug.cgi?id=15588

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-4m)
- rebuild against qt3 and qt-4.4.0-1m

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-3m)
- own %%{_datadir}/dbus-1/interfaces

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-2m)
- Pass 'dbus' instead of 81 as --with-dbus-user; otherwise the setuid
  system bus activation helper fails

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-1m)
- update 1.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.20-2m)
- rebuild against gcc43

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.20-1m)
- update 1.1.20 (unstable)
- [SECURITY] CVE-2008-0595 (maybe)
- comment out patch3 patch4

* Mon Feb 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-2m)
- own %%{_sysconfdir}/dbus-1/session.d
- remove /%%{_lib}/*dbus-1*.so.* from dbus
- remove %%{_datadir}/devhelp/books/dbus from dbus-devel

* Sun Feb 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-1m)
- update 1.1.4(1.2.0-RC2)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-8m)
- %%NoSource -> NoSource

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-7m)
- %%NoSource -> NoSource

* Sun Jun  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-6m)
- move prefix from /usr to /
- modify messagebus start time from 97 to 22
- sync with F-7

* Fri Apr 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-5m)
- import dbus-1.0.2-selinux.patch from Fedora
 +* Thu Apr 12 2007 David Zeuthen <davidz@redhat.com> - 1.0.2-3
 +- Start SELinux thread after setuid call (#221168)

* Mon Feb 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-4m)
- dbus-devel Requires: pkgconfig

* Wed Feb 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-3m)
- add PreReq initscripts

* Tue Feb  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-2m)
- update to 1.0.2 again
- update and apply dbus-0.61-selinux-avc-audit.patch again
- import dbus-0.92-audit-system.patch from Fedora Core devel
 +* Wed Sep 6 2006 Dan Walsh <dwalsh@redhat.com> - 0.92-2
 +- Only audit on the system bus

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.92-5m)
- roll back 0.92

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
- comment out patch2 (maybe needed)

* Mon Feb  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.92-4m)
- %%post used condrestart

* Sun Jan 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.92-3m)
- [SECURITY] CVE-2006-6107
- import dbus-0.92-CVE-2006-6107.patch from Mandriva
 +* Mon Dec 18 2006 Vincent Danen <vdanen@mandriva.com> 0.92-8.2mdv2007.0
 +- P3: security fix for CVE-2006-6107

* Tue Sep  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.92-2m)
- fix %%post script

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.92-1m)
- update to 0.92
- delete packages dbus-glib dbus-qt dbus-python

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.61-2m)
- rebuild against expat-2.0.0-1m

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.61-1m)
- version dbus-0.61

* Sun Feb 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.60-3m)
- enable audit

* Fri Dec 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.60-2m)
- enable Qt3 bindings

* Fri Dec 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.60-1m)
- sync with fc-devel
- disable audit

* Tue Nov  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.50-3m)
- fix Source1: messagebus

* Tue Nov  1 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.50-2m)
- fix build error x86_64

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.50-1m)
- version 0.50

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22-2m)
- add Patch10:	dbus-initddir-bus-Makefile-01.patch
- delete --with-init-scripts=redhat from cflags

- (0.22-10m)
- import from FC3.

* Wed Oct 13 2004 John (J5) Palmieri <johnp@redhat.com>
- Bump up release and rebuild

* Mon Oct 11 2004 Tim Waugh <twaugh@redhat.com>
- Run /sbin/ldconfig for glib sub-package (bug #134062).

* Wed Sep 22 2004 John (J5) Palmieri <johnp@redhat.com>
- Fixed patch to use dbus-1 instead of dbus-1.0
- (configure.in): Exported just the datadir instead of
  the full path to the dbus datadir for consistency

* Wed Sep 22 2004 John (J5) Palmieri <johnp@redhat.com>
- Adding patch to move /usr/lib/dbus-1.0/services to
  /usr/share/dbus-1.0/services 

* Thu Sep 16 2004 John (J5) Palmieri <johnp@redhat.com>
- reverting BuildRequires: redhat-release because of issues with build system
- added precompiled version of the messagebus init script

* Thu Sep 16 2004 John (J5) Palmieri <johnp@redhat.com>
- changed /etc/redhat-release to the package redhat-release

* Thu Sep 16 2004 John (J5) Palmieri <johnp@redhat.com>
- added python int64 patch from davidz

* Thu Sep 16 2004 John (J5) Palmieri <johnp@redhat.com>
- added BuildRequires: /etc/redhat-release (RH Bug #132436)

* Wed Aug 18 2004 John (J5) Palmieri <johnp@redhat.com>
- Added Steve Grubb's spec file patch (RH Bug #130201)

* Mon Aug 16 2004 John (J5) Palmieri <johnp@redhat.com>
- Disabled dbus-gtk since dbus-viewer doesn't do anything right now

* Mon Aug 16 2004 John (J5) Palmieri <johnp@redhat.com>
- Moved dbus-viewer to new dbus-gtk package so that dbus-glib
  no longer requires X or GTK libraries. (RH Bug #130029)

* Thu Aug 12 2004 John (J5) Palmieri <johnp@redhat.com>
- Update to new 0.22 release

* Thu Aug 05 2004 John (J5) Palmieri <johnp@redhat.com> 
- Added BuildRequires for libselinux-devel and Requires for libselinux

* Tue Aug 02 2004 Colin Walters <walters@redhat.com>
- Add SE-DBus patch

* Fri Jul 30 2004 John (J5) Palmieri <johnp@redhat.com>
- Added lib64 workaround for python bindings installing to
  the wrong lib directory on 64 bit archs

* Fri Jul 30 2004 John (J5) Palmieri <johnp@redhat.com>
- Updated console-auth patch
- rebuild
 
* Thu Jul 22 2004 John (J5) Palmieri <johnp@redhat.com>
- Update to upstream CVS build
- Added console-auth patch

* Fri Jun 25 2004 John (J5) Palmieri <johnp@redhat.com>
- Workaround added to fix gcc-3.4 bug on ia64

* Fri Jun 25 2004 John (J5) Palmieri <johnp@redhat.com>
- require new Pyrex version and see if it builds this time

* Fri Jun 25 2004 John (J5) Palmieri <johnp@redhat.com>
- rebuild with updated Pyrex (0.9.2.1)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Jun 04 2004 John (J5) Palmieri <johnp@redhat.com>
- Moved dbus-viewer, dbus-monitor and dbus-glib-tool 
  into the dbus-glib package so that the main dbus
  package does not depend on glib (Bug #125285) 

* Thu Jun 03 2004 John (J5) Palmieri <johnp@redhat.com>
- rebuilt

* Thu May 27 2004 John (J5) Palmieri <johnp@redhat.com>
- added my Python patch
- took out the qt build requires
- added a gtk+ build requires 

* Fri Apr 23 2004 John (J5) Palmieri <johnp@redhat.com>
- Changed build requirement to version 0.9-3 of Pyrex
  to fix problem with builing on x86_64

* Tue Apr 20 2004 John (J5) Palmieri <johnp@redhat.com>
- update to upstream 0.21
- removed dbus-0.20-varargs.patch patch (fixed upstream)

* Mon Apr 19 2004 John (J5) Palmieri <johnp@redhat.com>
- added a dbus-python package to generate python bindings
- added Pyrex build dependacy

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Feb 25 2004 Bill Nottingham <notting@redhat.com> 0.20-4
- fix dbus error functions on x86-64 (#116324)
- add prereq (#112027)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Tim Waugh <twaugh@redhat.com>
- Conflict with cups prior to configuration file change, so that the
  %%postun service condrestart works.

* Wed Feb 11 2004 Havoc Pennington <hp@redhat.com> 0.20-2
- rebuild in fc2, cups now updated

* Wed Jan  7 2004 Bill Nottingham <notting@redhat.com> 0.20-1
- update to upstream 0.20

* Thu Oct 16 2003 Havoc Pennington <hp@redhat.com> 0.13-6
- hmm, dbus doesn't support uids in the config file. fix.

* Thu Oct 16 2003 Havoc Pennington <hp@redhat.com> 0.13-5
- put uid instead of username in the config file, to keep things working with name change

* Thu Oct 16 2003 Havoc Pennington <hp@redhat.com> 0.13-4
- make subpackages require the specific release, not just version, of base package

* Thu Oct 16 2003 Havoc Pennington <hp@redhat.com> 0.13-3
- change system user "messagebus" -> "dbus" to be under 8 chars

* Mon Sep 29 2003 Havoc Pennington <hp@redhat.com> 0.13-2
- see if removing qt subpackage for now will get us through the build system,
  qt bindings not useful yet anyway

* Sun Sep 28 2003 Havoc Pennington <hp@redhat.com> 0.13-1
- 0.13 fixes a little security oops

* Mon Aug  4 2003 Havoc Pennington <hp@redhat.com> 0.11.91-3
- break the tiny dbus-launch that depends on X into separate package
  so a CUPS server doesn't need X installed

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sat May 17 2003 Havoc Pennington <hp@redhat.com> 0.11.91-1
- 0.11.91 cvs snap properly merges system.d

* Fri May 16 2003 Havoc Pennington <hp@redhat.com> 0.11.90-1
- build a cvs snap with a few more fixes

* Fri May 16 2003 Havoc Pennington <hp@redhat.com> 0.11-2
- fix a crash that was breaking cups

* Thu May 15 2003 Havoc Pennington <hp@redhat.com> 0.11-1
- 0.11

* Thu May 15 2003 Havoc Pennington <hp@redhat.com> 0.10.90-1
- use rc.d/init.d not init.d, bug #90192
- include the new man pages

* Fri Apr 11 2003 Havoc Pennington <hp@redhat.com> 0.9-1
- 0.9
- export QTDIR explicitly
- re-enable qt, the problem was most likely D-BUS configure

* Tue Apr  1 2003 Havoc Pennington <hp@redhat.com> 0.6.94-1
- update from CVS with a fix to set uid after gid

* Tue Apr  1 2003 Havoc Pennington <hp@redhat.com> 0.6.93-1
- new cvs snap that actually forks to background and changes 
  user it's running as and so forth
- create our system user in pre

* Mon Mar 31 2003 Havoc Pennington <hp@redhat.com> 0.6.92-1
- fix for "make check" test that required a home directory

* Mon Mar 31 2003 Havoc Pennington <hp@redhat.com> 0.6.91-1
- disable qt for now because beehive hates me
- pull a slightly newer cvs snap that creates socket directory
- cat the make check log after make check fails

* Mon Mar 31 2003 Havoc Pennington <hp@redhat.com> 0.6.90-1
- initial build

