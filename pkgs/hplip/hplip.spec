%global momorel 12

Summary: HP Linux Imaging and Printing Project
Name: hplip
Version: 3.10.5
Release: %{momorel}m%{?dist}
License: GPLv2+ and MIT
Group: System Environment/Daemons
Conflicts: system-config-printer < 0.6.132
Obsoletes: hpoj <= 0.91
Obsoletes: xojpanel <= 0.91

# Need selinux-policy to know about new port numbers (bug #201357).
Conflicts: selinux-policy < 3.0.3-3

Url: http://hplip.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/hplip/%{name}-%{version}.tar.gz
NoSource: 0
Source1: hpcups-update-ppds.sh
Patch1: hplip-pstotiff-is-rubbish.patch
Patch2: hplip-strstr-const.patch
Patch3: hplip-ui-optional.patch
Patch4: hplip-no-asm.patch
Patch5: hplip-deviceIDs-drv.patch
Patch6: hplip-mucks-with-spooldir.patch
Patch7: hplip-udev-rules.patch
Patch8: hplip-retry-open.patch
Patch9: hplip-snmp-quirks.patch
Patch10: hplip-discovery-method.patch
Patch11: hplip-device-reconnected.patch
Patch12: hplip-clear-old-state-reasons.patch
Patch13: hplip-bad-state-attr.patch
Patch14: hplip-hpcups-sigpipe.patch
Patch16: hplip-bad-low-ink-warning.patch
Patch17: hplip-deviceIDs-ppd.patch
Patch18: hplip-skip-blank-lines.patch
Patch19: hplip-dbglog-newline.patch
Patch20: hplip-no-system-tray.patch
Patch21: hplip-openPPD.patch
Patch22: hplip-ppd-ImageableArea.patch

## Security patch
Patch100: %{name}-%{version}-CVE-2011-2722.patch

# patch from arch
Patch200: cups-1.6-buildfix.diff

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(pre): initscripts
Requires(post): chkconfig
Requires(preun): initscripts
Requires(preun): chkconfig

%define hpijs_epoch 1
Requires: hpijs = %{hpijs_epoch}:%{version}-%{release}

Requires: python-imaging

BuildRequires: net-snmp-devel >= 5.7.1
BuildRequires: cups-devel
BuildRequires: python-devel >= 2.7
BuildRequires: libjpeg-devel >= 8a
BuildRequires: desktop-file-utils
BuildRequires: libusb-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: sane-backends-devel
BuildRequires: dbus-devel

%description
The Hewlett-Packard Linux Imaging and Printing Project provides
drivers for HP printers and multi-function peripherals.

%package common
Summary: Files needed by the HPLIP printer and scanner drivers
Group: System Environment/Libraries
License: GPLv2+
Requires: udev

%description common
Files needed by the HPLIP printer and scanner drivers.

%package libs
Summary: HPLIP libraries
Group: System Environment/Libraries
License: GPLv2+ and MIT

%description libs
Libraries needed by HPLIP.

%package gui
Summary: HPLIP graphical tools
Group: Applications/System
License: Modified BSD
Requires: PyQt4
Requires(post): desktop-file-utils >= 0.2.92
Requires(postun): desktop-file-utils >= 0.2.92
Requires: %{name} = %{version}-%{release}

%description gui
HPLIP graphical tools.

%package -n hpijs
Summary: HP Printer Drivers
Group: Applications/Publishing
License: Modified BSD
Epoch: %{hpijs_epoch}
Requires: %{name}-libs = %{version}-%{release}
Requires: python-reportlab
Requires: net-snmp
Requires: cupsddk-drivers

%description -n hpijs
hpijs is a collection of optimized drivers for HP printers.
hpijs supports the DeskJet 350C, 600C, 600C Photo, 630C, Apollo 2000,
Apollo 2100, Apollo 2560, DeskJet 800C, DeskJet 825, DeskJet 900,
PhotoSmart, DeskJet 990C, and PhotoSmart 100 series.

%package -n libsane-hpaio
Summary: SANE driver for scanners in HP's multi-function devices
Group: System Environment/Daemons
License: GPLv2+
Obsoletes: libsane-hpoj <= 0.91
Requires: sane-backends
Requires: %{name}-libs = %{version}-%{release}
ExcludeArch: s390 s390x

%description -n libsane-hpaio
SANE driver for scanners in HP's multi-function devices (from HPOJ).

%prep
%setup -q

## apply security patch first
%patch100 -p1 -b .CVE-2011-2722

## patch for new cups
%patch200 -p1 -b .cups16

# The pstotiff filter is rubbish so replace it (launchpad #528394).
%patch1 -p1 -b .pstotiff-is-rubbish

# Fix compilation.
%patch2 -p1 -b .strstr-const

# Make utils.checkPyQtImport() look for the gui sub-package (bug #243273).
%patch3 -p1 -b .ui-optional

# Make sure to avoid handwritten asm.
%patch4 -p1 -b .no-asm

# Corrected several IEEE 1284 Device IDs using foomatic data.
# HP LaserJet P1007 (bug #585272).
# HP Color LaserJet CM1312nfi (bug #581005).
# HP Color LaserJet 3800 (bug #581935).
# HP Color LaserJet 2840 (bug #582215).
%patch5 -p1 -b .deviceIDs-drv

# Stopped hpcups pointlessly trying to read spool files
# directly (bug #552572).
%patch6 -p1 -b .mucks-with-spooldir

# Removed SYSFS use in udev rules and actually made them work
# (bug #560754).
%patch7 -p1 -b .udev-rules

# Retry when connecting to device fails (bug #532112).
%patch8 -p1 -b .retry-open

# Mark SNMP quirks in PPD for HP OfficeJet Pro 8500 (bug #581825).
%patch9 -p1 -b .snmp-quirks

# Fixed hp-setup traceback when discovery page is skipped (bug #523685).
%patch10 -p1 -b .discovery-method

# Give up trying to print a job to a reconnected device (bug #515481).
%patch11 -p1 -b .device-reconnected

# Clear old printer-state-reasons we used to manage (bug #510926).
%patch12 -p1 -b .clear-old-state-reasons

# Fixed marker-supply attributes in hpijs (bug #605269).
%patch13 -p1 -b .bad-state-attr

# Avoid busy loop in hpcups when backend has exited (bug #525944).
%patch14 -p1 -b .hpcups-sigpipe

# Fixed Device ID parsing code in hpijs's dj9xxvip.c (bug #510926).
%patch16 -p1 -b .bad-low-ink-warning

# Add Device ID for
# HP LaserJet 1200 (bug #577308)
# HP LaserJet 1320 series (bug #579920)
# HP LaserJet 2300 (bug #576928)
# HP LaserJet P2015 Series (bug #580231)
# HP LaserJet 4250 (bug #585499).
# HP Color LaserJet 2605dn (bug #583953).
# HP Color LaserJet 3800 (bug #581935).
# HP Color LaserJet 2840 (bug #582215).
for ppd_file in $(grep '^diff' %{PATCH17} | cut -d " " -f 4);
do
  gunzip ${ppd_file#*/}.gz
done
%patch17 -p1 -b .deviceIDs-ppd
for ppd_file in $(grep '^diff' %{PATCH17} | cut -d " " -f 4);
do
  gzip -n ${ppd_file#*/}
done

# Hpcups (ljcolor) was putting black lines where should be blank lines (bug #579461).
%patch18 -p1 -b .skip-blank-lines

# Added missing newline to string argument in dbglog() call (bug #585275).
%patch19 -p1 -b .dbglog-newline

# Wait longer to see if a system tray becomes available (bug #569969).
%patch20 -p1 -b .no-system-tray

# Prevent segfault in cupsext when opening PPD file (bug #572775).
%patch21 -p1 -b .openPPD

# Fix ImageableArea for Laserjet 8150/9000 (bug #596298).
for ppd_file in $(grep '^diff' %{PATCH22} | cut -d " " -f 4);
do
  gunzip ${ppd_file#*/}.gz
done
%patch22 -p1 -b .deviceIDs-ppd
for ppd_file in $(grep '^diff' %{PATCH22} | cut -d " " -f 4);
do
  gzip -n ${ppd_file#*/}
done

sed -i.duplex-constraints \
    -e 's,\(UIConstraints.* \*Duplex\),//\1,' \
    prnt/drv/hpcups.drv.in

%build
%configure \
        --enable-scan-build --enable-gui-build --enable-fax-build \
        --disable-foomatic-rip-hplip-install --enable-pp-build \
        --enable-qt4 --enable-hpcups-install --enable-cups-drv-install \
        --enable-hpijs-install --enable-udev-acl-rules \
        --disable-policykit

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
make install DESTDIR=%{buildroot}

# Remove unpackaged files
rm -rf  %{buildroot}%{_sysconfdir}/sane.d \
        %{buildroot}%{_docdir} \
        %{buildroot}%{_datadir}/hal/fdi \
        %{buildroot}%{_datadir}/hplip/pkservice.py \
        %{buildroot}%{_bindir}/hp-pkservice

rm -f   %{buildroot}%{_bindir}/foomatic-rip \
        %{buildroot}%{_libdir}/cups/filter/foomatic-rip \
        %{buildroot}%{_libdir}/*.la \
        %{buildroot}%{_libdir}/python*/site-packages/*.la \
        %{buildroot}%{_libdir}/libhpip.so \
        %{buildroot}%{_libdir}/sane/*.la \
        %{buildroot}%{_datadir}/cups/model/foomatic-ppds \
        %{buildroot}%{_datadir}/applications/hplip.desktop \
        %{buildroot}%{_datadir}/ppd/HP/*.ppd

mkdir -p %{buildroot}%{_datadir}/applications
sed -i -e '/^Categories=/d' hplip.desktop
desktop-file-install --vendor HP                                \
        --dir %{buildroot}%{_datadir}/applications              \
        --add-category System                                   \
        --add-category Settings                                 \
        --add-category HardwareSettings                         \
        --add-category Application                              \
        hplip.desktop

# Regenerate hpcups PPDs on upgrade if necessary (bug #579355).
install -p -m755 %{SOURCE1} %{buildroot}%{_bindir}/hpcups-update-ppds

%{__mkdir_p} %{buildroot}%{_sysconfdir}/sane.d/dll.d
echo hpaio > %{buildroot}%{_sysconfdir}/sane.d/dll.d/hpaio

# Images in docdir should not be executable (bug #440552).
find doc/images -type f -exec chmod 644 {} \;

# Create an empty plugins directory to make sure it gets the right
# SELinux file context (bug #564551).
%{__mkdir_p} %{buildroot}%{_datadir}/hplip/prnt/plugins

# Remove files we don't want to package.
rm -f %{buildroot}%{_datadir}/hplip/hpaio.desc
rm -f %{buildroot}%{_datadir}/hplip/hplip-install
rm -rf %{buildroot}%{_datadir}/hplip/install.*
rm -f %{buildroot}%{_datadir}/hplip/hpijs.drv.in.template
rm -f %{buildroot}%{_datadir}/cups/mime/pstotiff.types

# The systray applet doesn't work properly (displays icon as a
# window), so don't ship the launcher yet.
rm -f %{buildroot}%{_sysconfdir}/xdg/autostart/hplip-systray.desktop

%clean
rm -rf %{buildroot}

%pre
# No daemons any more.
/sbin/chkconfig --del hplip 2>/dev/null || true
if [ -x /etc/init.d/hplip ]; then
  /sbin/service hplip stop
fi

%post gui
/usr/bin/update-desktop-database &>/dev/null ||:

%postun gui
/usr/bin/update-desktop-database &>/dev/null ||:

%post -n libsane-hpaio
/sbin/ldconfig
if [ -f /etc/sane.d/dll.conf ] && \
   ! grep ^hpaio /etc/sane.d/dll.conf >/dev/null 2>/dev/null ; then \
        echo hpaio >> /etc/sane.d/dll.conf; \
fi
exit 0

%files
%defattr(-,root,root)
%doc doc/*
%{_bindir}/hp-align
%{_bindir}/hp-clean
%{_bindir}/hp-colorcal
%{_bindir}/hp-devicesettings
%{_bindir}/hp-fab
%{_bindir}/hp-faxsetup
%{_bindir}/hp-firmware
%{_bindir}/hp-info
%{_bindir}/hp-levels
%{_bindir}/hp-linefeedcal
%{_bindir}/hp-makecopies
%{_bindir}/hp-makeuri
%{_bindir}/hp-mkuri
%{_bindir}/hp-plugin
%{_bindir}/hp-pqdiag
%{_bindir}/hp-printsettings
%{_bindir}/hp-probe
%{_bindir}/hp-query
%{_bindir}/hp-scan
%{_bindir}/hp-sendfax
%{_bindir}/hp-setup
%{_bindir}/hp-testpage
%{_bindir}/hp-timedate
%{_bindir}/hp-unload
%{_bindir}/hp-wificonfig
# Note: this must be /usr/lib not %{_libdir}, since that's the
# CUPS serverbin directory.
/usr/lib/cups/backend/hp
/usr/lib/cups/backend/hpfax
/usr/lib/cups/filter/pstotiff
%{_datadir}/cups/mime/pstotiff.convs
# Files
%{_datadir}/hplip/align.py*
%{_datadir}/hplip/clean.py*
%{_datadir}/hplip/colorcal.py*
%{_datadir}/hplip/devicesettings.py*
%{_datadir}/hplip/fab.py*
%{_datadir}/hplip/fax
%{_datadir}/hplip/faxsetup.py*
%{_datadir}/hplip/firmware.py*
%{_datadir}/hplip/hpdio.py*
%{_datadir}/hplip/hpssd*
%{_datadir}/hplip/info.py*
%{_datadir}/hplip/__init__.py*
%{_datadir}/hplip/levels.py*
%{_datadir}/hplip/linefeedcal.py*
%{_datadir}/hplip/makecopies.py*
%{_datadir}/hplip/makeuri.py*
%{_datadir}/hplip/plugin.py*
%{_datadir}/hplip/pqdiag.py*
%{_datadir}/hplip/printsettings.py*
%{_datadir}/hplip/probe.py*
%{_datadir}/hplip/query.py*
%{_datadir}/hplip/scan.py*
%{_datadir}/hplip/sendfax.py*
%{_datadir}/hplip/setup.py*
%{_datadir}/hplip/testpage.py*
%{_datadir}/hplip/timedate.py*
%{_datadir}/hplip/unload.py*
%{_datadir}/hplip/wificonfig.py*
# Directories
%{_datadir}/hplip/base
%{_datadir}/hplip/copier
%{_datadir}/hplip/data/ldl
%{_datadir}/hplip/data/localization
%{_datadir}/hplip/data/pcl
%{_datadir}/hplip/data/ps
%{_datadir}/hplip/installer
%{_datadir}/hplip/pcard
%{_datadir}/hplip/prnt
%{_datadir}/hplip/scan
%{_localstatedir}/lib/hp

%files common
%defattr(-,root,root,-)
%doc COPYING
%{_sysconfdir}/udev/rules.d/*
%dir %{_sysconfdir}/hp
%config(noreplace) %{_sysconfdir}/hp/hplip.conf
%dir %{_datadir}/hplip
%dir %{_datadir}/hplip/data
%{_datadir}/hplip/data/models

%files libs
%defattr(-,root,root)
%{_libdir}/libhpip.so.*
%{_libdir}/libhpmud.so*
# Python extension
%{_libdir}/python*/site-packages/*

%files gui
%{_bindir}/hp-check
%{_bindir}/hp-print
%{_bindir}/hp-systray
%{_bindir}/hp-toolbox
%{_datadir}/applications/*.desktop
# Files
%{_datadir}/hplip/check.py*
%{_datadir}/hplip/print.py*
%{_datadir}/hplip/systray.py*
%{_datadir}/hplip/toolbox.py*
# Directories
%{_datadir}/hplip/data/images
%{_datadir}/hplip/ui4

%files -n hpijs
%defattr(-,root,root)
%{_bindir}/hpijs
%{_bindir}/hpcups-update-ppds
%dir %{_datadir}/ppd/HP
%{_datadir}/ppd/HP/*.ppd.gz
%{_datadir}/cups/drv/*
# Note: this must be /usr/lib not %{_libdir}, since that's the
# CUPS serverbin directory.
/usr/lib/cups/filter/hpcac
/usr/lib/cups/filter/hpcups
/usr/lib/cups/filter/hpcupsfax
/usr/lib/cups/filter/hplipjs

%files -n libsane-hpaio
%defattr(-,root,root)
%{_libdir}/sane/libsane-*.so*
%config(noreplace) %{_sysconfdir}/sane.d/dll.d/hpaio

%changelog
* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.5-12m)
- hplip-gui does not depend on PyQt

* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.5-11m)
- import cups-1.6-buildfix.diff from Arch Linux to enable build new cups

* Sat Jun  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.5-10m)
- [SECURITY] CVE-2011-2722

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.10.5-9m)
- rebuild against net-snmp-5.7.1

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.5-8m)
- remove BR hal

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.5-7m)
- rebuild against net-snmp-5.7

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.5-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.5-5m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.5-4m)
- rebuild against net-snmp-5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.5-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@mmonga-linux.org>
- (3.10.5)
- update to 3.10.5
- sync with Fedora devel (3.10.5-7)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-7m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-6m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-5m)
- rebuild against net-snmp-5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.2-3m)
- rebuild against libjpeg-7

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-2m)
- import Patch14 from Fedora 11 (3.9.2-5)

* Sun Jun  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-1m)
- sync with Fedora 11 (3.9.2-4)

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.8.12-4m)
- add autoreconf (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.12-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.12-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.12-4m)
- sync with Rawhide (2.8.12-2)

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.8.2-3m)
- rebuild against python-2.6.1-2m

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.2-2m)
- release %%{_datadir}/foomatic/db/source/PPD/HP, it's provided by foomatic

* Tue Jul 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.2-1m)
- sync Fedora

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.4a-6m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.4a-5m)
- rebuild against gcc43

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.4a-4m)
- add Requires: chkconfig

* Sun Jul 15 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.4a-3m)
- add Patch10: hplip-1.7.4a-viewstatus.patch
  reported by tattsan [Momonga-devel.ja:03503] and [Momonga-devel.ja:03504]
  [momongaja:00189] http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=189

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.4-2m)
- rebuild against net-snmp

* Wed May 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.4-1m)
- update 1.7.4a

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-1m)
- update 1.7.2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.12-1m)
- update 1.6.12

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6a-5m)
- remove category X-Red-Hat-Extra Application

* Thu Aug 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.6a-4m)
- remove hplip-cups-backend.patch

* Wed Aug  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.6a-3m)
- rebuild against net-snmp 5.3.1.0

* Tue Aug  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.6a-2m)
- add hplip-1.6.6-disable-service.patch for stable release
- revise %%files

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.6a-1m)
- update 1.6.6a

* Wed Jun 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.6-1m)
- update 1.6.6

* Mon May 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.11-1m)
- update 0.9.11

* Fri Nov 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-1m)
- update 0.9.7

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-2m)
- rebuild against python-2.4.2
- Requires: PyQt >= 3.13-7m

* Fri Oct 14 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-1m)
- update 0.9.6

* Sun Oct  2 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-1m)
- update 0.9.5

* Sun Aug 28 2005 Yohsuke Ooi <meke@momonga-linux.org>
- First import, based on Fedora (0.9.4-3).

