%global momorel 1
%global srcver 9.5.5
%global dirver 9.5.5
%global fontver 910
%global acrodir %{_libdir}/AdobeReader

# Browser plugin doesn't work with stripped binaries/libs
%global __os_install_post /usr/lib/rpm/momonga/brp-compress %{nil}

Name: adobe-reader
Summary: Adobe Reader
Version: %{srcver}
Release: %{momorel}m%{?dist}
# Copyright: Adobe Systems Incorporated, <http://www.adobe.com/>
License: see "LICREAD*.TXT"
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: Applications/Publishing
URL: http://get.adobe.com/reader/
Source0: ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/%{dirver}/enu/AdbeRdr%{srcver}-1_i486linux_enu.tar.bz2
Nosource: 0
#japanese fonts is now in japanese version of adobereader
Source10: ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.1/misc/FontPack%{fontver}_jpn_i486-linux.tar.bz2
Nosource: 10
Source11: ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.1/misc/FontPack%{fontver}_chs_i486-linux.tar.bz2
Nosource: 11
Source12: ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.1/misc/FontPack%{fontver}_cht_i486-linux.tar.bz2
Nosource: 12
Source13: ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.1/misc/FontPack%{fontver}_kor_i486-linux.tar.bz2
Nosource: 13
Source14: ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.1/misc/FontPack%{fontver}_xtd_i486-linux.tar.bz2
Nosource: 14
Source20: AdobeReader.desktop

# KozGoProVI-Medium.otf
Source100: ftp://ftp.adobe.com/pub/adobe/reader/unix/8.x/8.1.2/misc/FontPack81_jpn_i486-linux.tar.gz
Nosource: 100

ExclusiveArch: %{ix86}
Provides: adobe-reader-common
Obsoletes: adobe-reader-common
Provides: acrobat-reader
Obsoletes: acrobat-reader
AutoReq: No
AutoProv: No

%description
Adobe Reader %{srcver} is the free viewing companion to Adobe Acrobat %{srcver}.
Adobe Reader lets you view, navigate and print Portable Document Format (PDF)
files. Adobe Acrobat %{srcver} is a complete solution for creating, enhancing,
reviewing, editing, and sharing information in PDF.

%package chsfont
Summary: Chinese Simplified Font for adobe-reader
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: acrobat-reader-chsfont
Obsoletes: acrobat-reader-chsfont

%description chsfont
The Chinese Simplified Font Pack is for Adobe Reader. And the fonts in the
Chinese Simplified Font Pack are for use only with Adobe Reader; they cannot
 be used with other applications.

%package chtfont
Summary: Chinese Traditional Font for adobe-reader
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: acrobat-reader-chtfont
Obsoletes: acrobat-reader-chtfont

%description chtfont
The Chinese Traditional Font Pack is for Adobe Reader. And the fonts in the
Chinese Simplified Font Pack are for use only with Adobe Reader; they cannot
 be used with other applications.

%package jpnfont
Summary: Japanese Font for adobereader
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: acrobat-reader-jpnfont
Obsoletes: acrobat-reader-jpnfont

%description jpnfont
The Japanese Font Pack is for Adobe Reader. And the fonts in the Japanese
Font Pack are for use only with Adobe Reader; they cannot be used with other
applications.

%package korfont
Summary: Korean Font for adobereader
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: acrobat-reader-korfont
Obsoletes: acrobat-reader-korfont

%description korfont
The Korean Font Pack is for Adobe Acrobat Reader. And the fonts in the
Chinese Simplified Font Pack are for use only with Adobe Reader; they cannot
 be used with other applications.

%package xtdfont
Summary: Extended Font for adobereader
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: acrobat-reader-xtdfont
Obsoletes: acrobat-reader-xtdfont

%description xtdfont
The Extended Language Support Font Pack is for Adobe Reader. And the
fonts in the Extended Language Support Font Pack are for use only with
Adobe Reader; they cannot be used with other applications.

%prep
%setup -q -n AdobeReader -a 10 -a 11 -a 12 -a 13 -a 14
tar xpf COMMON.TAR
tar xpf ILINXR.TAR

# license file for fonts (same in all langs)
cp JPNKIT/LICREAD.TXT LICREAD.FONTS.TXT

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{acrodir}

pushd Adobe/Reader9
cp -a {bin,Browser,Reader,Resource} %{buildroot}%{acrodir}
popd

mkdir -p %{buildroot}%{_bindir}
pushd %{buildroot}%{_bindir}
ln -s ../..%{_libdir}/AdobeReader/bin/acroread acroread
ln -s ../..%{_libdir}/AdobeReader/bin/acroread adobereader
popd

# common CMap files
tar xf JPNKIT/LANGCOM.TAR -C %{buildroot}%{acrodir} --strip-components=2

# fonts files except for Japanese
for i in CHS CHT KOR
do
    tar xf "$i"KIT/LANG"$i".TAR -C %{buildroot}%{acrodir} --strip-components=2
done

# extended languages
pushd xtdfont
tar xf XTDFONT.TAR
install -m 644 Adobe/Reader9/Resource/Font/AdobeArabic* %{buildroot}%{acrodir}/Resource/Font/
install -m 644 Adobe/Reader9/Resource/Font/AdobeHebrew* %{buildroot}%{acrodir}/Resource/Font/
install -m 644 Adobe/Reader9/Resource/Font/AdobeThai* %{buildroot}%{acrodir}/Resource/Font/
popd

# KozGoProVI-Medium.otf
mkdir tmp
pushd tmp
tar xzf %{SOURCE100}
pushd JPNKIT
tar xf LANGJPN.TAR
install -m 644 Adobe/Reader8/Resource/CIDFont/KozGoProVI-Medium.otf %{buildroot}%{acrodir}/Resource/CIDFont/
popd
popd

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -D -m 644 %{SOURCE20} %{buildroot}%{_datadir}/applications/AdobeReader.desktop

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -D -m 644 Adobe/Reader9/Resource/Icons/128x128/AdobeReader9.png %{buildroot}%{_datadir}/pixmaps/AdobeReader9.png

# install man
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 Adobe/Reader9/Resource/Shell/acroread.1.gz %{buildroot}%{_mandir}/man1/

%clean
rm -rf %{buildroot}

%post
if [ -d "%{_libdir}/mozilla/plugins/" ]; then
  (cd %{_libdir}/mozilla/plugins; \
  install -m 755 %{acrodir}/Browser/intellinux/nppdf.so .)
fi

if [ -d "%{_libdir}/firefox/plugins/" ]; then
  (cd %{_libdir}/firefox/plugins; \
  install -m 755 %{acrodir}/Browser/intellinux/nppdf.so .)
fi

%preun
if [ $1 -eq 0 ]; then
  if [ -h "%{_libdir}/firefox/plugins/nppdf.so" ]; then
    %{__rm} -f %{_libdir}/firefox/plugins/nppdf.so
  fi
  if [ -h "%{_libdir}/mozilla/plugins/nppdf.so" ]; then
    %{__rm} -f %{_libdir}/mozilla/plugins/nppdf.so
  fi
fi

%files
%defattr(-,root,root)
%doc LICREAD.FONTS.TXT ReadMe.htm
%{_bindir}/*
%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/*.png
%{acrodir}/bin
%{acrodir}/Browser
%dir %{acrodir}/Reader
%{acrodir}/Reader/*
%dir %{acrodir}/Resource
%dir %{acrodir}/Resource/CIDFont
%{acrodir}/Resource/Font
%{acrodir}/Resource/Linguistics
%{acrodir}/Resource/Support
%{acrodir}/Resource/Icons
%{acrodir}/Resource/Shell
%{acrodir}/Resource/TypeSupport
%dir %{acrodir}/Resource/CMap
%{acrodir}/Resource/CMap/Identity-H
%{acrodir}/Resource/CMap/Identity-V
%{_mandir}/man1/acroread.1*

%files chsfont
%defattr(-,root,root)
%{acrodir}/Resource/CIDFont/AdobeSongStd-Light.otf
%{acrodir}/Resource/CIDFont/AdobeHeitiStd-Regular.otf
%{acrodir}/Resource/CMap/Adobe-GB1-GBK-EUC
%{acrodir}/Resource/CMap/Adobe-GB1-GBpc-EUC
%{acrodir}/Resource/CMap/Adobe-GB1-H-CID
%{acrodir}/Resource/CMap/Adobe-GB1-H-Host
%{acrodir}/Resource/CMap/Adobe-GB1-H-Mac
%{acrodir}/Resource/CMap/Adobe-GB1-UCS2
%{acrodir}/Resource/CMap/GB-EUC-H
%{acrodir}/Resource/CMap/GB-EUC-V
%{acrodir}/Resource/CMap/GBK-EUC-H
%{acrodir}/Resource/CMap/GBK-EUC-UCS2
%{acrodir}/Resource/CMap/GBK-EUC-V
%{acrodir}/Resource/CMap/GBK2K-H
%{acrodir}/Resource/CMap/GBK2K-V
%{acrodir}/Resource/CMap/GBKp-EUC-H
%{acrodir}/Resource/CMap/GBKp-EUC-V
%{acrodir}/Resource/CMap/GBT-EUC-H
%{acrodir}/Resource/CMap/GBT-EUC-V
%{acrodir}/Resource/CMap/GBpc-EUC-H
%{acrodir}/Resource/CMap/GBpc-EUC-UCS2
%{acrodir}/Resource/CMap/GBpc-EUC-UCS2C
%{acrodir}/Resource/CMap/GBpc-EUC-V
%{acrodir}/Resource/CMap/UCS2-GBK-EUC
%{acrodir}/Resource/CMap/UCS2-GBpc-EUC
%{acrodir}/Resource/CMap/UniGB-UCS2-H
%{acrodir}/Resource/CMap/UniGB-UCS2-V
%{acrodir}/Resource/CMap/UniGB-UTF16-H
%{acrodir}/Resource/CMap/UniGB-UTF16-V

%files chtfont
%defattr(-,root,root)
%{acrodir}/Resource/CIDFont/AdobeMingStd-Light.otf
%{acrodir}/Resource/CMap/Adobe-CNS1-B5pc
%{acrodir}/Resource/CMap/Adobe-CNS1-ETen-B5
%{acrodir}/Resource/CMap/Adobe-CNS1-H-CID
%{acrodir}/Resource/CMap/Adobe-CNS1-H-Host
%{acrodir}/Resource/CMap/Adobe-CNS1-H-Mac
%{acrodir}/Resource/CMap/Adobe-CNS1-UCS2
%{acrodir}/Resource/CMap/B5pc-H
%{acrodir}/Resource/CMap/B5pc-UCS2
%{acrodir}/Resource/CMap/B5pc-UCS2C
%{acrodir}/Resource/CMap/B5pc-V
%{acrodir}/Resource/CMap/CNS-EUC-H
%{acrodir}/Resource/CMap/CNS-EUC-V
%{acrodir}/Resource/CMap/ETHK-B5-H
%{acrodir}/Resource/CMap/ETHK-B5-V
%{acrodir}/Resource/CMap/ETen-B5-H
%{acrodir}/Resource/CMap/ETen-B5-UCS2
%{acrodir}/Resource/CMap/ETen-B5-V
%{acrodir}/Resource/CMap/ETenms-B5-H
%{acrodir}/Resource/CMap/ETenms-B5-V
%{acrodir}/Resource/CMap/HKdla-B5-H
%{acrodir}/Resource/CMap/HKdla-B5-V
%{acrodir}/Resource/CMap/HKdlb-B5-H
%{acrodir}/Resource/CMap/HKdlb-B5-V
%{acrodir}/Resource/CMap/HKgccs-B5-H
%{acrodir}/Resource/CMap/HKgccs-B5-V
%{acrodir}/Resource/CMap/HKm314-B5-H
%{acrodir}/Resource/CMap/HKm314-B5-V
%{acrodir}/Resource/CMap/HKm471-B5-H
%{acrodir}/Resource/CMap/HKm471-B5-V
%{acrodir}/Resource/CMap/HKscs-B5-H
%{acrodir}/Resource/CMap/HKscs-B5-V
%{acrodir}/Resource/CMap/UCS2-B5pc
%{acrodir}/Resource/CMap/UCS2-ETen-B5
%{acrodir}/Resource/CMap/UniCNS-UCS2-H
%{acrodir}/Resource/CMap/UniCNS-UCS2-V
%{acrodir}/Resource/CMap/UniCNS-UTF16-H
%{acrodir}/Resource/CMap/UniCNS-UTF16-V

%files jpnfont
%defattr(-,root,root)
%{acrodir}/Resource/CIDFont/KozGoProVI-Medium.otf
#{acrodir}/Resource/CIDFont/KozMinPr6N-Regular.otf
%{acrodir}/Resource/CMap/83pv-RKSJ-H
%{acrodir}/Resource/CMap/90ms-RKSJ-H
%{acrodir}/Resource/CMap/90ms-RKSJ-UCS2
%{acrodir}/Resource/CMap/90ms-RKSJ-V
%{acrodir}/Resource/CMap/90msp-RKSJ-H
%{acrodir}/Resource/CMap/90msp-RKSJ-V
%{acrodir}/Resource/CMap/90pv-RKSJ-H
%{acrodir}/Resource/CMap/90pv-RKSJ-UCS2
%{acrodir}/Resource/CMap/90pv-RKSJ-UCS2C
%{acrodir}/Resource/CMap/Add-RKSJ-H
%{acrodir}/Resource/CMap/Add-RKSJ-V
%{acrodir}/Resource/CMap/Adobe-Japan1-90ms-RKSJ
%{acrodir}/Resource/CMap/Adobe-Japan1-90pv-RKSJ
%{acrodir}/Resource/CMap/Adobe-Japan1-H-CID
%{acrodir}/Resource/CMap/Adobe-Japan1-H-Host
%{acrodir}/Resource/CMap/Adobe-Japan1-H-Mac
%{acrodir}/Resource/CMap/Adobe-Japan1-PS-H
%{acrodir}/Resource/CMap/Adobe-Japan1-PS-V
%{acrodir}/Resource/CMap/Adobe-Japan1-UCS2
%{acrodir}/Resource/CMap/EUC-H
%{acrodir}/Resource/CMap/EUC-V
%{acrodir}/Resource/CMap/Ext-RKSJ-H
%{acrodir}/Resource/CMap/Ext-RKSJ-V
%{acrodir}/Resource/CMap/H
%{acrodir}/Resource/CMap/UCS2-90ms-RKSJ
%{acrodir}/Resource/CMap/UCS2-90pv-RKSJ
%{acrodir}/Resource/CMap/UniJIS-UCS2-H
%{acrodir}/Resource/CMap/UniJIS-UCS2-HW-H
%{acrodir}/Resource/CMap/UniJIS-UCS2-HW-V
%{acrodir}/Resource/CMap/UniJIS-UCS2-V
%{acrodir}/Resource/CMap/UniJIS-UTF16-H
%{acrodir}/Resource/CMap/UniJIS-UTF16-V
%{acrodir}/Resource/CMap/V

%files korfont
%defattr(-,root,root)
%{acrodir}/Resource/CIDFont/AdobeMyungjoStd-Medium.otf
%{acrodir}/Resource/CMap/Adobe-Korea1-H-CID
%{acrodir}/Resource/CMap/Adobe-Korea1-H-Host
%{acrodir}/Resource/CMap/Adobe-Korea1-H-Mac
%{acrodir}/Resource/CMap/Adobe-Korea1-KSCms-UHC
%{acrodir}/Resource/CMap/Adobe-Korea1-KSCpc-EUC
%{acrodir}/Resource/CMap/Adobe-Korea1-UCS2
%{acrodir}/Resource/CMap/KSC-EUC-H
%{acrodir}/Resource/CMap/KSC-EUC-V
%{acrodir}/Resource/CMap/KSCms-UHC-H
%{acrodir}/Resource/CMap/KSCms-UHC-HW-H
%{acrodir}/Resource/CMap/KSCms-UHC-HW-V
%{acrodir}/Resource/CMap/KSCms-UHC-UCS2
%{acrodir}/Resource/CMap/KSCms-UHC-V
%{acrodir}/Resource/CMap/KSCpc-EUC-H
%{acrodir}/Resource/CMap/KSCpc-EUC-UCS2
%{acrodir}/Resource/CMap/KSCpc-EUC-UCS2C
%{acrodir}/Resource/CMap/UCS2-KSCms-UHC
%{acrodir}/Resource/CMap/UCS2-KSCpc-EUC
%{acrodir}/Resource/CMap/UniKS-UCS2-H
%{acrodir}/Resource/CMap/UniKS-UCS2-V
%{acrodir}/Resource/CMap/UniKS-UTF16-H
%{acrodir}/Resource/CMap/UniKS-UTF16-V

%files xtdfont
%defattr(-,root,root)
%{acrodir}/Resource/Font/AdobeArabic-BoldItalic.otf
%{acrodir}/Resource/Font/AdobeArabic-Bold.otf
%{acrodir}/Resource/Font/AdobeArabic-Italic.otf
%{acrodir}/Resource/Font/AdobeArabic-Regular.otf
%{acrodir}/Resource/Font/AdobeHebrew-BoldItalic.otf
%{acrodir}/Resource/Font/AdobeHebrew-Bold.otf
%{acrodir}/Resource/Font/AdobeHebrew-Italic.otf
%{acrodir}/Resource/Font/AdobeHebrew-Regular.otf
%{acrodir}/Resource/Font/AdobeThai-BoldItalic.otf
%{acrodir}/Resource/Font/AdobeThai-Bold.otf
%{acrodir}/Resource/Font/AdobeThai-Italic.otf
%{acrodir}/Resource/Font/AdobeThai-Regular.otf

%changelog
* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.5.5-1m)
- [SECURITY] CVE-2013-2549 CVE-2013-2550 CVE-2013-2718 CVE-2013-2719
- [SECURITY] CVE-2013-2720 CVE-2013-2721 CVE-2013-2722 CVE-2013-2723
- [SECURITY] CVE-2013-2724 CVE-2013-2725 CVE-2013-2726 CVE-2013-2727
- [SECURITY] CVE-2013-2729 CVE-2013-2730 CVE-2013-2731 CVE-2013-2732
- [SECURITY] CVE-2013-2733 CVE-2013-2734 CVE-2013-2735 CVE-2013-2736
- [SECURITY] CVE-2013-2737 CVE-2013-3337 CVE-2013-3338 CVE-2013-3339
- [SECURITY] CVE-2013-3340 CVE-2013-3341 CVE-2013-3342
- update to 9.5.5

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.5.4-1m)
- [SECURITY] CVE-2013-0640 CVE-2013-0641
- update to 9.5.4

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.5.3-1m)
- [SECURITY] CVE-2012-1530 CVE-2013-0601 CVE-2013-0602 CVE-2013-0603
- [SECURITY] CVE-2013-0604 CVE-2013-0605 CVE-2013-0606 CVE-2013-0607
- [SECURITY] CVE-2013-0608 CVE-2013-0609 CVE-2013-0610 CVE-2013-0611
- [SECURITY] CVE-2013-0612 CVE-2013-0613 CVE-2013-0614 CVE-2013-0615
- [SECURITY] CVE-2013-0616 CVE-2013-0617 CVE-2013-0618 CVE-2013-0619
- [SECURITY] CVE-2013-0620 CVE-2013-0621 CVE-2013-0622 CVE-2013-0623
- [SECURITY] CVE-2013-0624 CVE-2013-0626 CVE-2013-0627
- update to 9.5.3

* Wed Apr 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.5.1-1m)
- [SECURITY] following issues are fixed in 9.4.6
- [SECURITY] CVE-2011-1353 CVE 2011-2431 CVE 2011-2432 CVE-2011-2433
- [SECURITY] CVE-2011-2434 CVE-2011-2435 CVE-2011-2436 CVE-2011-2437
- [SECURITY] CVE-2011-2438 CVE-2011-2439 CVE-2011-2440 CVE-2011-2441
- [SECURITY] CVE-2011-2442 CVE-2011-4374
- [SECURITY] following issues are fixed in 9.4.7
- [SECURITY] CVE-2011-2462 CVE-2011-4369
- [SECURITY] following issues are fixed in 9.5.1
- [SECURITY] CVE-2012-0774 CVE-2012-0775 CVE-2012-0776 CVE-2012-0777
- update to 9.5.1, English version

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.4.2-2m)
- rebuild for new GCC 4.6

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.4.2-1m)
- [SECURITY] CVE-2010-4091 CVE-2011-0562 CVE-2011-0563 CVE-2011-0564
- [SECURITY] CVE-2011-0565 CVE-2011-0566 CVE-2011-0567 CVE-2011-0568
- [SECURITY] CVE-2011-0570 CVE-2011-0585 CVE-2011-0586 CVE-2011-0587
- [SECURITY] CVE-2011-0588 CVE-2011-0589 CVE-2011-0590 CVE-2011-0591
- [SECURITY] CVE-2011-0592 CVE-2011-0593 CVE-2011-0594 CVE-2011-0595
- [SECURITY] CVE-2011-0596 CVE-2011-0598 CVE-2011-0599 CVE-2011-0600
- [SECURITY] CVE-2011-0602 CVE-2011-0603 CVE-2011-0604 CVE-2011-0605
- [SECURITY] CVE-2011-0606
- [SECURITY] see http://www.adobe.com/support/security/bulletins/apsb11-03.html
- update to 9.4.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.4-3m)
- rebuild for new GCC 4.5

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9.4-2m)
- correct Source0 URI

* Thu Oct  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.4-1m)
- [SECURITY] CVE-2010-2883 CVE-2010-2884 CVE-2010-2887
- update to 9.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.3.4-2m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.3.4-1m)
- [SECURITY] CVE-2010-2862 CVE-2010-1240
- update to 9.3.4

* Mon Jul 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.3.3-1m)
- [SECURITY] CVE-2010-1240 CVE-2010-1285 CVE-2010-1295 CVE-2010-1297
- [SECURITY] CVE-2010-2168 CVE-2010-2201 CVE-2010-2202 CVE-2010-2203
- [SECURITY] CVE-2010-2204 CVE-2010-2205 CVE-2010-2206 CVE-2010-2207
- [SECURITY] CVE-2010-2208 CVE-2010-2209 CVE-2010-2210 CVE-2010-2211
- [SECURITY] CVE-2010-2212
- update to 9.3.3

* Wed Apr 14 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.3.2-1m)
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb10-09.html
 - CVE-2010-0190, CVE-2010-0191, CVE-2010-0192, CVE-2010-0193,
   CVE-2010-0194, CVE-2010-0195, CVE-2010-0196, CVE-2010-0197,
   CVE-2010-0198, CVE-2010-0199, CVE-2010-0201, CVE-2010-0202,
   CVE-2010-0203, CVE-2010-0204, CVE-2010-1241

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.3.1-2m)
- modify __os_install_post for new momonga-rpmmacros

* Wed Feb 17 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.3.1-1m)
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb10-07.html
 - CVE-2010-0188, CVE-2010-0186

* Wed Jan 13 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.3-1m)
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb10-02.html
  CVE-2009-3953, CVE-2009-3954, CVE-2009-3955, CVE-2009-3956, CVE-2009-3957,
  CVE-2009-3958, CVE-2009-3959, CVE-2009-4324

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.2-1m)
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb09-15.html
  CVE-2007-0048, CVE-2007-0045, CVE-2009-2564, CVE-2009-2979, CVE-2009-2980,
  CVE-2009-2981, CVE-2009-2982, CVE-2009-2983, CVE-2009-2984, CVE-2009-2985,
  CVE-2009-2986, CVE-2009-2987, CVE-2009-2988, CVE-2009-2989, CVE-2009-2990,
  CVE-2009-2991, CVE-2009-2992, CVE-2009-2993, CVE-2009-2994, CVE-2009-2995,
  CVE-2009-2996, CVE-2009-2997, CVE-2009-2998, CVE-2009-3431, CVE-2009-3458,
  CVE-2009-3459, CVE-2009-3460, CVE-2009-3461, CVE-2009-3462

* Mon Aug  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.1.3-2m)
- add KozGoProVI-Medium.otf

* Sat Aug  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.1.3-1m)
- [SECURITY] CVE-2009-1862
- update to 9.1.3

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.1.2-1m)
- [SECURITY] CVE-2009-0198 CVE-2009-0509 CVE-2009-0510 CVE-2009-0511 CVE-2009-0512 
- [SECURITY] CVE-2009-0888 CVE-2009-0889 CVE-2009-1855 CVE-2009-1856 CVE-2009-1857 
- [SECURITY] CVE-2009-1858 CVE-2009-1859 CVE-2009-1861
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb09-07.html
- update to 9.1.2

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.1.1-1m)
- [SECURITY] CVE-2009-1492 CVE-2009-1493
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb09-06.html
- update to 9.1.1

* Mon Apr  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.1.0-1m)
- update to 9.1.0
-- remove all workarounds

* Wed Mar 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.4-1m)
- [SECURITY] CVE-2009-0658 CVE-2009-0193 CVE-2009-0928 CVE-2009-1061 CVE-2009-1062
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb09-04.html
- update to 8.1.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.3-2m)
- rebuild against rpm-4.6

* Wed Nov  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.3-1m)
- [SECURITY] CVE-2009-0927
- [SECURITY] CVE-2008-2992 CVE-2008-2549 CVE-2008-4812 CVE-2008-4813
- [SECURITY] CVE-2008-4817 CVE-2008-4816 CVE-2008-4814 CVE-2008-4815
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb08-19.html
- update to 8.1.3

* Mon Jul 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1.2_SU1-1m)
- update 8.1.2_SU1 
-- [SECURITY] CVE-2008-2641, CVE-2008-0883
-- http://www.adobe.com/jp/support/security/bulletins/apsb08-15.html
- revise a workaround for scim 

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (8.1.2-3m)
- add patch2 (fix AdobeReader.desktop)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.2-2m)
- rebuild against gcc43

* Thu Feb  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.2-1m)
- [SECURITY] Adobe Reader Unspecified Vulnerabilities - Advisories - Secunia
- http://secunia.com/advisories/28802/

* Mon Jan 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.1-3m)
- revive and update Patch0: acroread-set-gtkimmodule.patch for atokx problem

* Sat Jan  5 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.1-2m)
- change fonts directories
- add %{acrodir}/Resource/CIDFont/KozGoPro-Medium.otf for Japanese Gothic fonts
- - Fontpack708_jpn_i386-Linux.tar.gz 10.7MB

* Fri Jan  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.1-1m)
- up to 8.1.1
- comment out Patch0: acroread-set-gtkimmodule.patch
- comment out Patch1: acroread-expr.patch

* Fri Jan 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (7.0.9-1m)
- up to 7.0.9
- [SECURITY] CVE-2007-0044 CVE-2007-0045 CVE-2007-0046 CVE-2007-0047 CVE-2007-0047
- use old version of FontPack, because FontPack for 7.0.9 is not yet provided

* Mon Oct  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.8-2m)
- add expr patch

* Sat Aug 19 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (7.0.8-1m)
- version up 7.0.8

* Thu Jun 15 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.0.5-3m)
- revised %%files to remove entry duplication.

* Wed Apr 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.0.5-2m)
- use Japanese version (AdobeReader_jpn-7.0.5-1.i386.tar.gz) to enable Japanese UI.
- merge adobe-reader-common and obsolete it
- - now adobe-reader itself makes asian language fontpack subpackages
- remove obsoleted weblnk file (That is for acrobat 4.x/5.x )

* Sun Jan 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.5-1m)
- Adobe Reader 7.0.5
- modify gtkimmodule.patch just like Centro

* Thu Aug 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.1-1m)
- [SECURITY] CAN-2005-2470
  http://www.adobe.com/support/techdocs/321644.html

* Tue Apr 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-2m)
- import acroread-unset-gtkimmodule.patch from AdobeReader-7.0-yk.1.nosrc.rpm
 +* Wed Apr 20 2005 Yoshinori KUNIGA <kuniga@users.sourceforge.net> - 7.0-yk.1
 +- unset GTK_IM_MODULE if you use scim
  http://wiki.fedora.jp/?Rpms%2FAcrobatReader

* Fri Apr 22 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.0-1m)
- Adobe Reader 7.0
- this specfile was based on that of fedoranews (http://fedoranews.org/tchung/AdobeReader/7.0/AdobeReader.spec)

* Fri Mar 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.10-6m)
- rebuild against firefox 1.0.2
- rebuild against mozilla-1.7.6
- install plugins to %%{_libdir}/mozilla/plugins and %%{_libdir}/firefox/plugins

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (5.0.10-5m)
- rebuild against firefox 1.0.1.

* Sat Jan 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.10-4m)
- import WebLink from acrobat-reader-5.0.10-matchy1.nosrc.rpm
 +* Wed Dec 17 2004 MACHIDA Hideki <h@matchy.net> 5.0.10-matchy2
 +- add WebLink (from 5.0.9-1.dag)
- import acrobat-reader.desktop and acrobat-reader.png from 5.0.10-matchy1
 +* Wed Jan 14 2004 MACHIDA Hideki <h@matchy.net> 5.0.8-matchy3
 +- merge from http://fedoranews.org/tchung/acroread/
  http://linux.matchy.net/AcroReadRpm.html

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (5.0.10-3m)
- rebuild against mozilla-1.7.5

* Wed Dec 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.10-2m)
- %%define __os_install_post (do not strip for mozilla plug-in)
  http://www.matchy.net/tdiary/20031215.html#p02

* Thu Dec 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.10-1m)
- [SECURITY] http://secunia.com/advisories/13471/

* Sat Oct 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (5.0.9-2m)
- install plugins

* Thu Jun 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.0.9-1m)
- security fix

* Wed Mar  3 2004 Toru Hoshina <t@momonga-linux.org>
- (5.0.8-3m)
- no more tar xfo...

* Mon Nov  3 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.0.8-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- revise spec

* Sat Aug 16 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (5.0.8-1m)
- [security] update to 5.0.8
  from README:

    A security patch was applied that solves a problem reported
    with long URLs in weblinks which can cause a buffer overrun.

* Fri Jun 20 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (5.0.7-1m)
- [security] update to 5.0.7
  see http://www.kb.cert.org/vuls/id/200132 for details.

* Tue Aug  6 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.0.6-1m)
- [security] update to 5.0.6
  from README:

    A security patch was applied that solves the problem reported in
    http://online.securityfocus.com/archive/1/278984 where opening
    the font cache when the application starts up can unintentionally
    cause the permissions of other files to change.

- Change installation directory (/usr/X11R6/share -> /usr/X11R6/lib)
  since executables are installed into it.
  By this change, Conflicts: acrobat-reader-common < 386-8m
  is added.

* Mon May 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (5.0.5-1k)
- up to 5.0.5
- delete netscape plugin

* Sun May  5 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (4.05-5k)
- add mozilla plugin

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Mon Sep 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Change Group

* Sun Jan  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (4.05-3k)
- Kondarization
