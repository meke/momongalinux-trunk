%global momorel 10

%global fontname sil-abyssinica

Name:           %{fontname}-fonts
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        SIL Abyssinica fonts

Group:          User Interface/X
License:        OFL
URL:            http://scripts.sil.org/AbyssinicaSIL
# download from http://scripts.sil.org/cms/scripts/render_download.php?site_id=nrsi&format=file&media_id=AbyssinicaSIL1.0.zip&filename=abyssinicasil1.0.zip
Source0:        abyssinicasil%{version}.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       fontpackages-filesystem
BuildRequires:  fontpackages-devel
BuildRequires:  dos2unix

# Package was renamed from abyssinica-fonts during the F13 development cycle.
# Those 2 lines could probably be removed during the F15 one.
Provides: abyssinica-fonts = %{version}-%{release}
Obsoletes: abyssinica-fonts < 1.0-7m

%description

SIL Abyssinica is a Unicode typeface family containing glyphs for the
Ethiopic script.

The Ethiopic script is used for writing many of the languages of Ethiopia and
Eritrea. Abyssinica SIL supports all Ethiopic characters which are in Unicode
including the Unicode 4.1 extensions. Some languages of Ethiopia are not yet
able to be fully represented in Unicode and, where necessary, we have included
non-Unicode characters in the Private Use Area (see Private-use (PUA)
characters supported by Abyssinica SIL).

Abyssinica SIL is based on Ethiopic calligraphic traditions. This release is
a regular typeface, with no bold or italic version available or planned.

%prep
%setup -q -c


%build
dos2unix FONTLOG.txt OFL.txt OFL-FAQ.txt README.txt


%install
rm -rf %{buildroot}

#fonts
install -d -m 0755 %{buildroot}%{_fontdir}
install -m 0644 *.ttf %{buildroot}%{_fontdir}


%clean
rm -rf %{buildroot}


%_font_pkg *.ttf

%doc FONTLOG.txt OFL.txt OFL-FAQ.txt README.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-8m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- renamed from abyssinica-fonts and so on
-- * Tue Feb 16 2010 Mathieu Bridon <bochecha@fedoraproject.org> - 1.0-8
-- - Fix Obsolete: that was wrong when renaming the package.
-- - The font base folder was listed twice (one was hidden in the font macro)
-- 
-- * Thu Feb 11 2010 Mathieu Bridon <bochecha@fedoraproject.org> - 1.0-7
-- - Use new archive from upstream at same location (which is lowercase now)
-- - Remove dubious Provides: (RHBZ#563395)
-- 
-- * Wed Feb 10 2010 Mathieu Bridon <bochecha@fedoraproject.org> - 1.0-6
-- - Renamed from abyssinica-fonts to sil-abyssinica-fonts
-- 
-- * Mon Feb  1 2010 Jens Petersen <petersen@redhat.com>
-- - use general SIL url and simplify download url
-- 
-- * Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-5
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-4
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Sun Dec 21 2008 Bernie Innocenti <bernie@codewiz.org> 1.0-3
-- - Updated to current Fedora font packaging guidelines

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-4m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Thu Oct 04 2007 Todd Zullinger <tmz@pobox.com> 1.0-2
- use upstream zip file as Source0
- fix license tag

* Fri Sep 14 2007 Bernardo Innocenti <bernie@codewiz.org> 1.0-1
- Initial packaging, borrowing many things from gentium-fonts
