%global momorel 2

Name: libnice
Version: 0.1.3
Release: %{momorel}m%{?dist}
Summary: The GLib ICE implementation
URL: http://nice.freedesktop.org/wiki/
Group: System Environment/Base
License: LGPLv2 and MPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://nice.freedesktop.org/releases/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires: pkgconfig
BuildRequires: gstreamer-devel
BuildRequires: gstreamer1-devel
BuildRequires: GConf2-devel
BuildRequires: gupnp-igd-devel >= 0.2.0

%description
Nice GLib ICE library

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --enable-gtk-doc --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYING.LGPL COPYING.MPL ChangeLog NEWS README
%{_bindir}/stunbdc
%{_bindir}/stund
%{_libdir}/libnice.so.*
%exclude %{_libdir}/libnice.la
%{_libdir}/gstreamer-0.10/libgstnice010.*
%{_libdir}/gstreamer-1.0/libgstnice.*

%files devel
%defattr(-, root, root)
%{_includedir}/nice
%{_includedir}/stun
%{_libdir}/libnice.so
%{_libdir}/pkgconfig/nice.pc
%{_datadir}/gtk-doc/html/libnice

%changelog
* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.3-2m)
- rebuild with gstreamer1

* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.3-1m)
- update to 0.1.3

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.2-1m)
- update to 0.1.2
- rebuild against gupnp-igd-0.2.0

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-2m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- update to 0.1.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.13-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.13-1m)
- update to 0.0.13

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.12-2m)
- full rebuild for mo7 release

* Fri May 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.12-1m)
- update to 0.0.12

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.11-1m)
- update to 0.0.11

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.10-1m)
- update to 0.0.10

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.9-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.9-1m)
- update to 0.0.9

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.6-1m)
- update to 0.0.6

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4-1m)
- initial build
