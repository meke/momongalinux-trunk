# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global momorel 1

Name:           python-decorator
Version:        3.4.0
Release:        %{momorel}m%{?dist}
Summary:        Module to simplify usage of decorators

Group:          Development/Languages
License:        BSD
URL:            http://www.phyast.pitt.edu/~micheles/python/documentation.html
Source0:        http://pypi.python.org/packages/source/d/decorator/decorator-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
BuildRequires:  python3-devel >= 3.4
BuildRequires:  python-setuptools-devel
BuildRequires:  python-nose

%description
The aim of the decorator module is to simplify the usage of decorators for
the average programmer, and to popularize decorators usage giving examples
of useful decorators, such as memoize, tracing, redirecting_stdout, locked,
etc.  The core of this module is a decorator factory called decorator.

%package -n python3-decorator
Summary:        Module to simplify usage of decorators in python3
Group:          Development/Languages

%description -n python3-decorator
The aim of the decorator module is to simplify the usage of decorators for
the average programmer, and to popularize decorators usage giving examples
of useful decorators, such as memoize, tracing, redirecting_stdout, locked,
etc.  The core of this module is a decorator factory called decorator.

%prep
%setup -q -n decorator-%{version}
chmod a-x *.txt *.py
%{__sed} -i 's/\r//' README.txt

rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'

%build
%{__python} setup.py build

pushd %{py3dir}
%{__python3} setup.py build
popd

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT

pushd %{py3dir}
%{__python3} setup.py install --skip-build --root $RPM_BUILD_ROOT
popd

%clean
rm -rf $RPM_BUILD_ROOT

%check
nosetests --with-doctest -e documentation3

pushd %{py3dir}
PYTHONPATH=$(pwd)/build/lib python3 documentation3.py
popd

%files
%defattr(-,root,root,-)
%doc *.txt
%{python_sitelib}/*

%files -n python3-decorator
%defattr(-,root,root,-)
%doc *.txt documentation3.py
%{python3_sitelib}/*

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.0-1m)
- update 3.4.0

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-1m)
- update 3.3.1
- add python3 package

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-2m)
- full rebuild for mo7 release

* Mon Mar 15 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-1m)
- Initial Commit Momonga Linux (import from Fedora)

* Tue Oct 6 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 3.1.2-2
- Really include the new source tarball

* Tue Oct 6 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 3.1.2-1
- Update to upstream release 3.1.2

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun May 31 2009 Luke Macken <lmacken@redhat.com> - 3.0.1-2
- Only run the test suite on Fedora 11, which has Py2.6 and the multiprocessing
  module.  We can disable this once the compat module is packaged for F10 and
  below.

* Thu May 21 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 3.0.1-1
- Update to upstream release 3.0.1.

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jan 21 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 2.3.2-1
- Update to 2.3.2
- Enable tests via nose

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 2.2.0-2
- Rebuild for Python 2.6
