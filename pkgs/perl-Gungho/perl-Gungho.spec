%global         momorel 26
%global		origver 0.09008

Name:           perl-Gungho
Version:        0.09.008
Release:        %{momorel}m%{?dist}
Summary:        Yet Another High Performance Web Crawler Framework
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Gungho/
#Source0:        http://www.cpan.org/modules/by-module/Gungho/Gungho-%%{origver}.tar.gz
Source0:        http://www.cpan.org/authors/id/D/DM/DMAKI/Gungho-%{origver}.tar.gz
NoSource:       0
Patch0:		perl-Gungho-0.09008-IO-Async.patch
Patch1:         perl-Gungho-0.09008-POE-test.patch
Patch2:         perl-Gungho-0.09008-qw.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-Best
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-Class-C3 >= 0.16
BuildRequires:	perl-Class-C3-Componentised
BuildRequires:  perl-Class-C3-XS >= 0.02
BuildRequires:  perl-Class-Inspector
BuildRequires:  perl-Config-Any
BuildRequires:  perl-Danga-Socket >= 1.57
BuildRequires:  perl-Danga-Socket-Callback
BuildRequires:  perl-Data-Dump
BuildRequires:  perl-Data-Throttler >= 0.01
BuildRequires:	perl-Data-Throttler-Memcached >= 0.0003
BuildRequires:	perl-Data-Valve
BuildRequires:	perl-Event-Notify >= 0.00004
BuildRequires:  perl-Exception-Class
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:	perl-HTML-RobotsMETA
BuildRequires:  perl-HTTP-Parser
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-IO-Async >= 0.21
BuildRequires:  perl-Net-DNS
BuildRequires:  perl-Path-Class
BuildRequires:  perl-POE >= 0.9999
BuildRequires:  perl-POE-Component-Client-DNS >= 1.051-10m
BuildRequires:  perl-POE-Component-Client-Keepalive >= 0.267-2m
BuildRequires:  perl-POE-Component-Client-HTTP >= 0.943-2m
BuildRequires:  perl-Regexp-Common
BuildRequires:  perl-UNIVERSAL-isa >= 0.06
BuildRequires:  perl-UNIVERSAL-require
BuildRequires:  perl-URI
BuildRequires:	perl-Web-Scraper-Config
BuildRequires:  perl-WWW-RobotRules-Parser
BuildRequires:  perl-YAML
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

Obsoletes:	perl-Xango

%description
Gungho is Yet Another Web Crawler Framework, aimed to be extensible and
fast. Its meant to be a culmination of lessons learned while building Xango
-- Xango was *fast*, but it was horribly hard to debug or to extend (Gungho
even works right out of the box ;)

%prep
%setup -q -n Gungho-%{origver}
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
make test ||:

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{_bindir}/*
%{perl_vendorlib}/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-26m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-25m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-24m)
- reuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-23m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-22m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-21m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-20m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-19m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-18m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-17m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-16m)
- rebuild against perl-5.14.0-0.2.1m

* Fri Apr 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-15m)
- skip some tests for a while...

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09.008-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09.008-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.09.008-11m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-10m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-9m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09.008-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09.008-7m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-6m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-5m)
- rebuild with new IO::Async (>= 0.21)

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09.008-4m)
- now check t/02_config.t and t/engine/danga-socket/04_dns.t

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-3m)
- perl-Xango was obsoleted

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09.008-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.008-1m)
- update to 0.09008

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09.007-2m)
- rebuild against gcc43

* Wed Jan 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.007-1m)
- update to 0.09007

* Thu Jan 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.006-1m)
- update to 0.09006

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.005-1m)
- update to 0.09005
- add BuildRequires: perl-Event-Notify

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.004-1m)
- update to 0.09004

* Fri Nov  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.002-1m)
- update to 0.09002

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09.001-1m)
- update to 0.09001

* Wed Oct 31 2007 NARTIA Koichi <pulsar@momonga-linux.org>
- (0.09.000-1m)
- update to 0.09000
- add BuildRequires: perl-Class-C3-Componentised
- add BuildRequires: perl-Web-Scraper-Config

* Mon Oct 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08.016-1m)
- update to 0.08016

* Sat Oct 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08.015-1m)
- update to 0.08015

* Thu Oct 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08.014-1m)
- update to 0.08014
- add BuildRequires: perl-HTML-RobotsMETA

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08.012-1m)
- update to 0.08012

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08.011-1m)
- update to 0.08011

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08.010-1m)
- update to 0.08010
- add BuildRequires: perl-Data-Throttler-Memcached

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08.009-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
