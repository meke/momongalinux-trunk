%global momorel 13
%global rcver 1
%global prever 1
%global artsver 1.5.10
%global artsrel 1m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Satellite prediction software for KDE
Name: ktrack
Version: 0.3.0
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPLv2
Group: Applications/Communications
URL: http://ktrack.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}-rc%{rcver}.tar.bz2 
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
# patch from Fedora
Patch10: %{name}-headers.patch
Patch11: %{name}-0.3.0-abs-conflict.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: hamlib
Requires: xplanet
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: hamlib-devel

%description
K-Track is an KDE3 satellite prediction software, especially suited for radio
amateurs operating through the satellites that are providing linear
transponders.

%prep
%setup -q -n %{name}-%{version}-rc%{rcver}

%patch0 -p1 -b .desktop~

# Fedora patch
%patch10 -p 1 -b .headers

# gcc43 patch
%patch11 -p 1 -b .abs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--mandir=%{_mandir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category Utility \
  --add-category HamRadio \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Applications/HamRadio/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-0.1.13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-0.1.12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-0.1.11m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-0.1.10m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-0.1.9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-0.1.8m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-0.1.7m)
- rebuild against qt3

* Thu Apr 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-0.1.6m)
- support gcc-4.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-0.1.5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-0.1.4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-0.1.3m)
- %%NoSource -> NoSource

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-0.1.2m)
- add Requires: xplanet

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-0.1.1m)
- initial package for Momonga Linux
- impoer ktrack-headers.patch from Fedora
