%global momorel 11
%global qtdir %{_libdir}/qt3

Summary: Editor for manipulating PDF documents
Name: pdfedit
Version: 0.4.5
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://pdfedit.petricek.net/
Group: Applications/Publishing
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
Source1: %{name}.desktop
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt3
Requires: fontconfig
Requires: freetype
Requires: libjpeg >= 8a
Requires: libmng
Requires: libpng
Requires: t1lib
Requires: zlib
BuildRequires: qt3-devel
BuildRequires: autoconf
BuildRequires: boost-devel
BuildRequires: fontconfig-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libmng-devel
BuildRequires: libpng-devel
BuildRequires: t1lib-devel
BuildRequires: zlib-devel

%description
Editor for manipulating PDF documents. GUI version + commandline interface.
Scripting is used to a great extent in editor and almost anything can be 
scripted, it is possible to create own scripts or plugins.

%prep
%setup -q

%build
export QTDIR=%{qtdir}
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

%configure \
	--disable-release \
%ifarch %{ix86}
	--disable-largefile \
%endif
	--with-boost-lib=%{_libdir} \
	--docdir=%{_docdir}/%{name}-%{version}
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make INSTALL_ROOT=%{buildroot} install

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 src/gui/icon/pdfedit_icon_48.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# fix permission before preparing docs
find %{buildroot}%{_datadir} -type f -exec chmod 644 {} \;

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%docdir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/*
%{_bindir}/pdfedit
%{_datadir}/applications/pdfedit.desktop
%{_mandir}/man?/*
%{_datadir}/pdfedit
%{_datadir}/pixmaps/pdfedit.png

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-11m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.4.5-10m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-9m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-8m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-7m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-6m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-5m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-4m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-2m)
- full rebuild for mo7 release

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5
- drop merged patch

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-5m)
- rebuild against libjpeg-8a

* Fri Feb 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-4m)
- [SECURITY] CVE-2009-1188 CVE-2009-3603 CVE-2009-3604
- [SECURITY] CVE-2009-3606 CVE-2009-3608 CVE-2009-3609
- apply xpdf patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-2m)
- rebuild against libjpeg-7

* Thu Jul 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-1m)
- [SECURITY] CVE-2009-0146 CVE-2009-0147 CVE-2009-0166 CVE-2009-0195
- [SECURITY] CVE-2009-0799 CVE-2009-0800 CVE-2009-1179 CVE-2009-1180
- [SECURITY] CVE-2009-1181 CVE-2009-1182 CVE-2009-1183
- update to 0.4.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2
-- drop Patch1, merged upstream
-- drop Patch2, not needed
- License: GPLv2+

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-3m)
- rebuild against gcc43

* Fri Mar 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-2m)
- fix build on x86_64
- import boost-lib-param.patch from ALT Linux
 +* Mon Mar 10 2008 Michael Shigorin <mike@altlinux.org> 0.4.1-alt3
 +- replaced Damir's subst with a patch proposed by 
 +  Michal Hocko <mstsxfx/gmail>
 +* Thu Mar 06 2008 Michael Shigorin <mike@altlinux.org> 0.4.1-alt2
 +- fixed x86_64 build, thanks damir

* Thu Mar 27 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Sun Jan  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-2m)
- add patch for gcc43

* Mon Sep  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- [SECURITY] CVE-2007-3387
- update to 0.3.2
- --disable-release at configure (mmm...)
- do not use %%NoSource macro

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-3m)
- Requires: freetype2 -> freetype

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-2m)
- fix permission
- install icon and desktop file

* Sat Mar 17 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.2.5-1m)
- welcome to Momonga
