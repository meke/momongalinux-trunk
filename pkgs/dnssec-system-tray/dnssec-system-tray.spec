%global momorel 1

Summary: System Tray for monitoring log files for DNSSEC errors
Name: dnssec-system-tray
Version: 1.12
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.dnssec-tools.org/
Source0: https://www.dnssec-tools.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: dnssec-system-tray.desktop
# generated from a larger PNG inside the source code
Source2: dnssec-system-tray-64x64.png

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: qt-devel
BuildRequires: desktop-file-utils

%description
A simply system-tray application that monitors log files (eg libval,
or bind/named logfiles) for DNSSEC error messages that should be
displayed to the user.

%prep
%setup -q 

%build
qmake-qt4 PREFIX=/usr
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install INSTALL_ROOT=%{buildroot}

%{__mkdir_p} %{buildroot}/%{_datadir}/icons/hicolor/64x64/apps/
%{__install} -p -m 644 %{SOURCE2} %{buildroot}/%{_datadir}/icons/hicolor/64x64/apps/
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%{__mkdir_p} %{buildroot}/%{_mandir}/man1
%{__install} -p -D -m 644 man/dnssec-system-tray.1 %{buildroot}/%{_mandir}/man1/dnssec-system-tray.1

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor &> /dev/null
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc COPYING
%doc %{_mandir}/man1/*
%{_bindir}/dnssec-system-tray
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/applications/dnssec-system-tray.desktop

%changelog
* Thu Mar 29 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-1m)
- import from Fedora devel to Momonga

* Tue Jan 31 2012 Wes Hardaker <wjhns174@hardakers.net> - 1.12-1
- Upgraded to version 1.12

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.11.p2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Oct 27 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11.p2-1
- updated to upstream version with man page and COPYING file

* Mon Oct 17 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11-2
- initial version for approval

