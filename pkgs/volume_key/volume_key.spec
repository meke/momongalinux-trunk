%global momorel 2

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: An utility for manipulating storage encryption keys and passphrases
Name: volume_key
Version: 0.3.8
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: https://fedorahosted.org/volume_key/
Requires: volume_key-libs = %{version}-%{release}
BuildRequires: cryptsetup-luks-devel >= 1.4.3

Source0: https://fedorahosted.org/releases/v/o/volume_key/volume_key-%{version}.tar.xz
BuildRequires: cryptsetup-luks-devel, gettext-devel, glib2-devel, gnupg
BuildRequires: gpgme-devel, libblkid-devel, nss-devel, python-devel

%description
This package provides a command-line tool for manipulating storage volume
encryption keys and storing them separately from volumes.

The main goal of the software is to allow restoring access to an encrypted
hard drive if the primary user forgets the passphrase.  The encryption key
back up can also be useful for extracting data after a hardware or software
failure that corrupts the header of the encrypted volume, or to access the
company data after an employee leaves abruptly.

%package devel
Summary: A library for manipulating storage encryption keys and passphrases
Group: Development/Libraries
Requires: volume_key-libs = %{version}-%{release}

%description devel
This package provides libvolume_key, a library for manipulating storage volume
encryption keys and storing them separately from volumes.

The main goal of the software is to allow restoring access to an encrypted
hard drive if the primary user forgets the passphrase.  The encryption key
back up can also be useful for extracting data after a hardware or software
failure that corrupts the header of the encrypted volume, or to access the
company data after an employee leaves abruptly.

%package libs
Summary: A library for manipulating storage encryption keys and passphrases
Group: System Environment/Libraries

%description libs
This package provides libvolume_key, a library for manipulating storage volume
encryption keys and storing them separately from volumes.

The main goal of the software is to allow restoring access to an encrypted
hard drive if the primary user forgets the passphrase.  The encryption key
back up can also be useful for extracting data after a hardware or software
failure that corrupts the header of the encrypted volume, or to access the
company data after an employee leaves abruptly.

%package -n python-volume_key
Summary: Python bindings for libvolume_key
Group: System Environment/Libraries
Requires: volume_key-libs = %{version}-%{release}

%description -n python-volume_key
This package provides Python bindings for libvolume_key, a library for
manipulating storage volume encryption keys and storing them separately from
volumes.

The main goal of the software is to allow restoring access to an encrypted
hard drive if the primary user forgets the passphrase.  The encryption key
back up can also be useful for extracting data after a hardware or software
failure that corrupts the header of the encrypted volume, or to access the
company data after an employee leaves abruptly.

volume_key currently supports only the LUKS volume encryption format.  Support
for other formats is possible, some formats are planned for future releases.

%prep
%setup -q

%build
%configure CFLAGS="%{optflags} -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'

%find_lang volume_key

%clean
rm -rf $RPM_BUILD_ROOT

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/volume_key
%{_mandir}/man8/volume_key.8*

%files devel
%defattr(-,root,root,-)
%{_includedir}/volume_key
%exclude %{_libdir}/libvolume_key.la
%{_libdir}/libvolume_key.so

%files libs -f volume_key.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS
%{_libdir}/libvolume_key.so.*

%files -n python-volume_key
%defattr(-,root,root,-)
%exclude %{python_sitearch}/_volume_key.la
%{python_sitearch}/_volume_key.so
%{python_sitearch}/volume_key.py*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8-2m)
- rebuild for glib 2.33.2

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.8-1m)
- update 0.3.8
-- rebuild against cryptsetup-luks-1.4.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.6-1m)
- update 0.3.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-1m)
- Initial commit Momonga Linux. import from Fedora

* Fri Mar 26 2010 Miloslav Trmac <mitr@redhat.com> - 0.3.3-1
- Update to volume_key-0.3.3

* Thu Mar  4 2010 Miloslav Trmac <mitr@redhat.com> - 0.3.2-1
- Update to volume_key-0.3.2
- Drop no longer necessary references to BuildRoot:

* Fri Feb  5 2010 Miloslav Trmac <mitr@redhat.com> - 0.3.1-2
- Fix a crash when an empty passphrase is provided
  Resolves: #558410

* Fri Dec 11 2009 Miloslav Trmac <mitr@redhat.com> - 0.3.1-1
- Update to volume_key-0.3.1.

* Wed Sep 30 2009 Miloslav Trmac <mitr@redhat.com> - 0.3-1
- Update to volume_key-0.3.
- Drop bundled libcryptsetup.

* Sat Aug  8 2009 Miloslav Trmac <mitr@redhat.com> - 0.2-3
- Handle changed "TYPE=crypto_LUKS" from libblkid
- Preserve file timestamps during installation

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 30 2009 Miloslav Trmac <mitr@redhat.com> - 0.2-1
- Initial build.
