%global momorel 1

%define libsepolver 2.3
%define libselinuxver 2.3

Summary: SELinux binary policy manipulation library 
Name: libsemanage
Version: 2.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
#Source: http://www.nsa.gov/selinux/archives/libsemanage-%{version}.tgz
Source0: http://userspace.selinuxproject.org/releases/20140506/libsemanage-%{version}.tar.gz
NoSource: 0
Source1: semanage.conf

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libselinux-devel >= %{libselinuxver} swig ustr-devel
BuildRequires: libsepol-devel >= %{libsepolver} 
BuildRequires: bison flex
BuildRequires: python-devel >= 2.7 python3-devel >= 3.4

%description
Security-enhanced Linux is a feature of the Linux(R) kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement(R), Role-based Access
Control, and Multi-level Security.

libsemanage provides an API for the manipulation of SELinux binary policies.
It is used by checkpolicy (the policy compiler) and similar tools, as well
as by programs like load_policy that need to perform specific transformations
on binary policies such as customizing policy boolean settings.

%package devel
Summary: Header files and libraries used to build policy manipulation tools
Group: Development/Libraries
Requires: libsemanage = %{version}-%{release} ustr

%description devel
The semanage-devel package contains the static libraries and header files
needed for developing applications that manipulate binary policies. 

%package static
Summary: Static library used to build policy manipulation tools
Group: Development/Libraries
Requires: libsemanage-devel = %{version}-%{release}

%description static
The semanage-static package contains the static libraries 
needed for developing applications that manipulate binary policies. 

%package python
Summary: semanage python bindings for libsemanage
Group: Development/Libraries
Requires: libsemanage = %{version}-%{release} 

%description python
The libsemanage-python package contains the python bindings for developing 
SELinux management applications. 

%package python3
Summary: semanage python 3 bindings for libsemanage
Group: Development/Libraries
Requires: libsemanage = %{version}-%{release} 

%description python3
The libsemanage-python3 package contains the python 3 bindings for developing
SELinux management applications.

%prep
%setup -q

%build
# To support building the Python wrapper against multiple Python runtimes
# Define a function, for how to perform a "build" of the python wrapper against
# a specific runtime:
BuildPythonWrapper() {
  BinaryName=$1

  # Perform the build from the upstream Makefile:
  make \
    PYTHON=$BinaryName \
    PY_INCLUDE_FLAGS="$PyIncludeFlags" \
    PY_LD_FLAGS="$PyLdFlags" \
    CFLAGS="%{optflags}" LIBDIR="%{_libdir}" SHLIBDIR="%{_lib}" \
    pywrap
}


make clean
make CFLAGS="%{optflags}" swigify
make CFLAGS="%{optflags}" LIBDIR="%{_libdir}" SHLIBDIR="%{_lib}" all

# python2
BuildPythonWrapper \
  %{__python}
  
# python3
BuildPythonWrapper \
  %{__python3}

%install
InstallPythonWrapper() {
  BinaryName=$1

  make \
    PYTHON=$BinaryName \
    DESTDIR="${RPM_BUILD_ROOT}" LIBDIR="${RPM_BUILD_ROOT}%{_libdir}" SHLIBDIR="${RPM_BUILD_ROOT}/%{_libdir}" \
    install-pywrap
}

rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/%{_lib} 
mkdir -p ${RPM_BUILD_ROOT}/%{_libdir} 
mkdir -p ${RPM_BUILD_ROOT}%{_includedir} 
make DESTDIR="${RPM_BUILD_ROOT}" LIBDIR="${RPM_BUILD_ROOT}%{_libdir}" SHLIBDIR="${RPM_BUILD_ROOT}/%{_libdir}" install

# python2
InstallPythonWrapper \
  %{__python} \
  .so

# python3
InstallPythonWrapper \
  %{__python3} \
  $(python3-config --extension-suffix)
  

cp %{SOURCE1} ${RPM_BUILD_ROOT}/etc/selinux/semanage.conf
ln -sf  /%{_libdir}/libsemanage.so.1 ${RPM_BUILD_ROOT}/%{_libdir}/libsemanage.so

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%config(noreplace) /etc/selinux/semanage.conf
/%{_libdir}/libsemanage.so.1

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files static
%defattr(-,root,root)
%{_libdir}/libsemanage.a

%files devel
%defattr(-,root,root)
%{_libdir}/libsemanage.so
%{_libdir}/pkgconfig/libsemanage.pc
%dir %{_includedir}/semanage
%{_includedir}/semanage/*.h
%{_mandir}/man3/*
%{_mandir}/man5/*

%files python
%{python_sitearch}/_semanage.so
%{python_sitearch}/semanage.py*

%files python3
%defattr(-,root,root)
%{python3_sitearch}/*.so
%{python3_sitearch}/semanage.py*
%{python3_sitearch}/__pycache__/semanage*

%changelog
* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- update 2.3

* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.10-2m)
- rebuild against python-3.4

* Mon Mar 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.10-1m)
- update to 2.1.10
- sync with fc19
- remove Patch0: libsemanage-rhat.patch

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.2-1m)
- update 2.1.2

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.46-1m)
- update 2.0.46
- add python3 package

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.45-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.45-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.45-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.45-2m)
- full rebuild for mo7 release

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.45-1m)
- update 2.0.45

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.31-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.31-1m)
- update 2.0.31

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.25-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.25-3m)
- rebuild against python-2.6.1-2m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.25-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: LGPLv2+

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.25-1m)
- update 2.0.25

* Wed Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.24-1m)
- version up 2.0.24
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-2m)
- rebuild against gcc43

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-1m)
- update 2.0.3

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Thu Mar  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.1-1m)
- update 1.10.1

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2-1m)
- update 1.9.2

* Sun May  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.2-1m)
- import from fc5

* Tue Apr 4 2006 Dan Walsh <dwalsh@redhat.com> - 1.6.2-2.fc5
- Bump for fc5

* Wed Mar 29 2006 Dan Walsh <dwalsh@redhat.com> - 1.6.2-2
- Fix leaky descriptor

* Tue Mar 21 2006 Dan Walsh <dwalsh@redhat.com> - 1.6.2-1
- Upgrade to latest from NSA
	* Merged Makefile PYLIBVER definition patch from Dan Walsh.
	* Merged man page reorganization from Ivan Gyurdiev.

* Mon Mar 18 2006 Dan Walsh <dwalsh@redhat.com> - 1.6-1.fc5
- Rebuild for FC5

* Fri Mar 17 2006 Dan Walsh <dwalsh@redhat.com> - 1.6-1
- Make work on RHEL4
- Upgrade to latest from NSA
	* Merged abort early on merge errors patch from Ivan Gyurdiev.
	* Cleaned up error handling in semanage_split_fc based on a patch
	  by Serge Hallyn (IBM) and suggestions by Ivan Gyurdiev.
	* Merged MLS handling fixes from Ivan Gyurdiev.

* Fri Feb 17 2006 Dan Walsh <dwalsh@redhat.com> - 1.5.28-1
- Upgrade to latest from NSA
	* Merged bug fix for fcontext validate handler from Ivan Gyurdiev.
	* Merged base_merge_components changes from Ivan Gyurdiev.

* Thu Feb 16 2006 Dan Walsh <dwalsh@redhat.com> - 1.5.26-1
- Upgrade to latest from NSA
	* Merged paths array patch from Ivan Gyurdiev.
	* Merged bug fix patch from Ivan Gyurdiev.
	* Merged improve bindings patch from Ivan Gyurdiev.
	* Merged use PyList patch from Ivan Gyurdiev.	
	* Merged memory leak fix patch from Ivan Gyurdiev.
	* Merged nodecon support patch from Ivan Gyurdiev.
	* Merged cleanups patch from Ivan Gyurdiev.
	* Merged split swig patch from Ivan Gyurdiev.

* Mon Feb 13 2006 Dan Walsh <dwalsh@redhat.com> - 1.5.23-1
- Upgrade to latest from NSA
	* Merged optionals in base patch from Joshua Brindle.
	* Merged treat seusers/users_extra as optional sections patch from
	  Ivan Gyurdiev.
	* Merged parse_optional fixes from Ivan Gyurdiev.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.5.21-2.1
- bump again for double-long bug on ppc(64)

* Fri Feb 10 2006 Dan Walsh <dwalsh@redhat.com> - 1.5.21-2
- Fix handling of seusers and users_map file

* Tue Feb 07 2006 Dan Walsh <dwalsh@redhat.com> - 1.5.21-1
- Upgrade to latest from NSA
	* Merged seuser/user_extra support patch from Joshua Brindle.
	* Merged remote system dbase patch from Ivan Gyurdiev.	

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.5.20-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Feb 2 2006 Dan Walsh <dwalsh@redhat.com> 1.5.20-1
- Upgrade to latest from NSA
	* Merged clone record on set_con patch from Ivan Gyurdiev.	

* Mon Jan 30 2006 Dan Walsh <dwalsh@redhat.com> 1.5.19-1
- Upgrade to latest from NSA
	* Merged fname parameter patch from Ivan Gyurdiev.
	* Merged more size_t -> unsigned int fixes from Ivan Gyurdiev.
	* Merged seusers.system patch from Ivan Gyurdiev.
	* Merged improve port/fcontext API patch from Ivan Gyurdiev.	

* Fri Jan 27 2006 Dan Walsh <dwalsh@redhat.com> 1.5.18-1
- Upgrade to latest from NSA
	* Merged seuser -> seuser_local rename patch from Ivan Gyurdiev.
	* Merged set_create_store, access_check, and is_connected interfaces
	  from Joshua Brindle.

* Fri Jan 13 2006 Dan Walsh <dwalsh@redhat.com> 1.5.16-1
- Upgrade to latest from NSA
	* Regenerate python wrappers.

* Fri Jan 13 2006 Dan Walsh <dwalsh@redhat.com> 1.5.15-1
- Upgrade to latest from NSA
	* Merged pywrap Makefile diff from Dan Walsh.
	* Merged cache management patch from Ivan Gyurdiev.
	* Merged bugfix for dbase_llist_clear from Ivan Gyurdiev.
	* Merged remove apply_local function patch from Ivan Gyurdiev.
	* Merged only do read locking in direct case patch from Ivan Gyurdiev.
	* Merged cache error path memory leak fix from Ivan Gyurdiev.
	* Merged auto-generated file header patch from Ivan Gyurdiev.
	* Merged pywrap test update from Ivan Gyurdiev.
	* Merged hidden defs update from Ivan Gyurdiev.

* Fri Jan 13 2006 Dan Walsh <dwalsh@redhat.com> 1.5.14-2
- Break out python out of regular Makefile

* Fri Jan 13 2006 Dan Walsh <dwalsh@redhat.com> 1.5.14-1
- Upgrade to latest from NSA
	* Merged disallow port overlap patch from Ivan Gyurdiev.
	* Merged join prereq and implementation patches from Ivan Gyurdiev.
	* Merged join user extra data part 2 patch from Ivan Gyurdiev.
	* Merged bugfix patch from Ivan Gyurdiev.
	* Merged remove add_local/set_local patch from Ivan Gyurdiev.
	* Merged user extra data part 1 patch from Ivan Gyurdiev.
	* Merged size_t -> unsigned int patch from Ivan Gyurdiev.
	* Merged calloc check in semanage_store patch from Ivan Gyurdiev,
	  bug noticed by Steve Grubb.
	* Merged cleanups after add/set removal patch from Ivan Gyurdiev.

* Fri Jan 7 2006 Dan Walsh <dwalsh@redhat.com> 1.5.9-1
- Upgrade to latest from NSA
	* Merged const in APIs patch from Ivan Gyurdiev.
	* Merged validation of local file contexts patch from Ivan Gyurdiev.
	* Merged compare2 function patch from Ivan Gyurdiev.
	* Merged hidden def/proto update patch from Ivan Gyurdiev.

* Thu Jan 6 2006 Dan Walsh <dwalsh@redhat.com> 1.5.8-1
- Upgrade to latest from NSA
	* Re-applied string and file optimization patch from Russell Coker,
	  with bug fix.
	* Reverted string and file optimization patch from Russell Coker.
	* Clarified error messages from parse_module_headers and 
	  parse_base_headers for base/module mismatches.

* Thu Jan 6 2006 Dan Walsh <dwalsh@redhat.com> 1.5.6-1
- Upgrade to latest from NSA
	* Clarified error messages from parse_module_headers and 
	  parse_base_headers for base/module mismatches.
	* Merged string and file optimization patch from Russell Coker.
	* Merged swig header reordering patch from Ivan Gyurdiev.
	* Merged toggle modify on add patch from Ivan Gyurdiev.
	* Merged ports parser bugfix patch from Ivan Gyurdiev.
	* Merged fcontext swig patch from Ivan Gyurdiev.
	* Merged remove add/modify/delete for active booleans patch from Ivan Gyurdiev.
	* Merged man pages for dbase functions patch from Ivan Gyurdiev.
	* Merged pywrap tests patch from Ivan Gyurdiev.

* Wed Jan 5 2006 Dan Walsh <dwalsh@redhat.com> 1.5.4-2
- Patch to fix add

* Wed Jan 5 2006 Dan Walsh <dwalsh@redhat.com> 1.5.4-1
- Upgrade to latest from NSA
	* Merged patch series from Ivan Gyurdiev.
	  This includes patches to:
	  - separate file rw code from linked list
	  - annotate objects
	  - fold together internal headers
	  - support ordering of records in compare function
	  - add active dbase backend, active booleans
	  - return commit numbers for ro database calls
	  - use modified flags to skip rebuild whenever possible
	  - enable port interfaces
	  - update swig interfaces and typemaps
	  - add an API for file_contexts.local and file_contexts
	  - flip the traversal order in iterate/list
	  - reorganize sandbox_expand
	  - add seusers MLS validation
	  - improve dbase spec/documentation
	  - clone record on set/add/modify

* Tue Dec 27 2005 Dan Walsh <dwalsh@redhat.com> 1.5.3-3
- Add Ivans patch to turn on ports

* Wed Dec 14 2005 Dan Walsh <dwalsh@redhat.com> 1.5.3-2
- Remove patch since upstream does the right thing

* Wed Dec 14 2005 Dan Walsh <dwalsh@redhat.com> 1.5.3-1
- Upgrade to latest from NSA
	* Merged further header cleanups from Ivan Gyurdiev.
	* Merged toggle modified flag in policydb_modify, fix memory leak
	  in clear_obsolete, polymorphism vs headers fix, and include guards
	  for internal headers patches from Ivan Gyurdiev.

* Tue Dec 13 2005 Dan Walsh <dwalsh@redhat.com> 1.5.1-2
- Upgrade to latest from NSA
	* Merged toggle modified flag in policydb_modify, fix memory leak
	  in clear_obsolete, polymorphism vs headers fix, and include guards
	  for internal headers patches from Ivan Gyurdiev.

* Mon Dec 12 2005 Dan Walsh <dwalsh@redhat.com> 1.5.1-1
- Upgrade to latest from NSA
	* Added file-mode= setting to semanage.conf, default to 0644.
	  Changed semanage_copy_file and callers to use this mode when
	  installing policy files to runtime locations.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Dec 7 2005 Dan Walsh <dwalsh@redhat.com> 1.4-1
- Fix mode of output seusers file

* Tue Dec 6 2005 Dan Walsh <dwalsh@redhat.com> 1.3.64-1
- Upgrade to latest from NSA
	* Changed semanage_handle_create() to set do_reload based on
	  is_selinux_enabled().  This prevents improper attempts to
	  load policy on a non-SELinux system.

* Mon Dec 5 2005 Dan Walsh <dwalsh@redhat.com> 1.3.63-1
- Upgrade to latest from NSA
	* Dropped handle from user_del_role interface.
	* Removed defrole interfaces.

* Tue Nov 29 2005 Dan Walsh <dwalsh@redhat.com> 1.3.61-1
- Upgrade to latest from NSA
	* Merged Makefile python definitions patch from Dan Walsh.
	* Removed is_selinux_mls_enabled() conditionals in seusers and users
	  file parsers. 

* Wed Nov 23 2005 Dan Walsh <dwalsh@redhat.com> 1.3.59-1
- Add additional swig objects
	* Merged wrap char*** for user_get_roles patch from Joshua Brindle.
	* Merged remove defrole from sepol patch from Ivan Gyurdiev.
	* Merged swig wrappers for modifying users and seusers from Joshua Brindle.

* Wed Nov 23 2005 Dan Walsh <dwalsh@redhat.com> 1.3.56-2
- Add additional swig objects

* Fri Nov 16 2005 Dan Walsh <dwalsh@redhat.com> 1.3.56-1
- Upgrade to latest from NSA
	* Fixed free->key_free bug.
	* Merged clear obsolete patch from Ivan Gyurdiev.
	* Merged modified swigify patch from Dan Walsh 
	  (original patch from Joshua Brindle).
	* Merged move genhomedircon call patch from Chad Sellers.

* Mon Nov 14 2005 Dan Walsh <dwalsh@redhat.com> 1.3.53-3
- Add genhomedircon patch from Joshua Brindle

* Fri Nov 11 2005 Dan Walsh <dwalsh@redhat.com> 1.3.53-2
- Add swigify patch from Joshua Brindle

* Fri Nov 11 2005 Dan Walsh <dwalsh@redhat.com> 1.3.53-1
- Upgrade to latest from NSA
	* Merged move seuser validation patch from Ivan Gyurdiev.
	* Merged hidden declaration fixes from Ivan Gyurdiev,
	  with minor corrections.

* Wed Nov 9 2005 Dan Walsh <dwalsh@redhat.com> 1.3.52-1
- Upgrade to latest from NSA
	* Merged cleanup patch from Ivan Gyurdiev.
	  This renames semanage_module_conn to semanage_direct_handle,
	  and moves sepol handle create/destroy into semanage handle
	  create/destroy to allow use even when disconnected (for the
	  record interfaces).

* Tue Nov 8 2005 Dan Walsh <dwalsh@redhat.com> 1.3.51-1
- Upgrade to latest from NSA
	* Clear modules modified flag upon disconnect and commit.
        * Added tracking of module modifications and use it to
	  determine whether expand-time checks should be applied
	  on commit.
	* Reverted semanage_set_reload_bools() interface.

* Tue Nov 8 2005 Dan Walsh <dwalsh@redhat.com> 1.3.48-1
- Upgrade to latest from NSA
	* Disabled calls to port dbase for merge and commit and stubbed
	  out calls to sepol_port interfaces since they are not exported.
	* Merged rename instead of copy patch from Joshua Brindle (Tresys).
	* Added hidden_def/hidden_proto for exported symbols used within 
	  libsemanage to eliminate relocations.  Wrapped type definitions
	  in exported headers as needed to avoid conflicts.  Added
	  src/context_internal.h and src/iface_internal.h.
	* Added semanage_is_managed() interface to allow detection of whether
	  the policy is managed via libsemanage.  This enables proper handling
	  in setsebool for non-managed systems.
	* Merged semanage_set_reload_bools() interface from Ivan Gyurdiev,
	  to enable runtime control over preserving active boolean values
	  versus reloading their saved settings upon commit.

* Mon Nov 7 2005 Dan Walsh <dwalsh@redhat.com> 1.3.43-1
- Upgrade to latest from NSA
	* Merged seuser parser resync, dbase tracking and cleanup, strtol
	  bug, copyright, and assert space patches from Ivan Gyurdiev.
	* Added src/*_internal.h in preparation for other changes.
 	* Added hidden/hidden_proto/hidden_def to src/debug.[hc] and
          src/seusers.[hc].


* Thu Nov 3 2005 Dan Walsh <dwalsh@redhat.com> 1.3.41-1
- Upgrade to latest from NSA
	* Merged interface parse/print, context_to_string interface change,
	  move assert_noeof, and order preserving patches from Ivan Gyurdiev.
        * Added src/dso.h in preparation for other changes.
p	* Merged install seusers, handle/error messages, MLS parsing,
	  and seusers validation patches from Ivan Gyurdiev.

* Mon Oct 31 2005 Dan Walsh <dwalsh@redhat.com> 1.3.39-1
- Upgrade to latest from NSA
	* Merged record interface, dbase flush, common database code,
	  and record bugfix patches from Ivan Gyurdiev.

* Fri Oct 28 2005 Dan Walsh <dwalsh@redhat.com> 1.3.38-1
- Upgrade to latest from NSA
	* Merged dbase policydb list and count change from Ivan Gyurdiev.
	* Merged enable dbase and set relay patches from Ivan Gyurdiev.

* Thu Oct 27 2005 Dan Walsh <dwalsh@redhat.com> 1.3.36-1
- Update from NSA
	* Merged query APIs and dbase_file_set patches from Ivan Gyurdiev.

* Wed Oct 26 2005 Dan Walsh <dwalsh@redhat.com> 1.3.35-1
- Update from NSA
	* Merged sepol handle passing, seusers support, and policydb cache
	  patches from Ivan Gyurdiev.

* Tue Oct 25 2005 Dan Walsh <dwalsh@redhat.com> 1.3.34-1
- Update from NSA
	* Merged resync to sepol changes and booleans fixes/improvements 
	  patches from Ivan Gyurdiev.
	* Merged support for genhomedircon/homedir template, store selection,
	  explicit policy reload, and semanage.conf relocation from Joshua
	  Brindle.

* Mon Oct 24 2005 Dan Walsh <dwalsh@redhat.com> 1.3.32-1
- Update from NSA
	* Merged resync to sepol changes and transaction fix patches from
	  Ivan Gyurdiev.
	* Merged reorganize users patch from Ivan Gyurdiev.
	* Merged remove unused relay functions patch from Ivan Gyurdiev.

* Fri Oct 21 2005 Dan Walsh <dwalsh@redhat.com> 1.3.30-1
- Update from NSA
	* Fixed policy file leaks in semanage_load_module and
	  semanage_write_module.
	* Merged further database work from Ivan Gyurdiev.
	* Fixed bug in semanage_direct_disconnect.

* Thu Oct 20 2005 Dan Walsh <dwalsh@redhat.com> 1.3.28-1
- Update from NSA
	* Merged interface renaming patch from Ivan Gyurdiev.
	* Merged policy component patch from Ivan Gyurdiev.
	* Renamed 'check=' configuration value to 'expand-check=' for 
	  clarity.
	* Changed semanage_commit_sandbox to check for and report errors 
	  on rename(2) calls performed during rollback.
	* Added optional check= configuration value to semanage.conf 
	  and updated call to sepol_expand_module to pass its value
	  to control assertion and hierarchy checking on module expansion.
	* Merged fixes for make DESTDIR= builds from Joshua Brindle.

* Tue Oct 18 2005 Dan Walsh <dwalsh@redhat.com> 1.3.24-1
- Update from NSA
	* Merged default database from Ivan Gyurdiev.
	* Merged removal of connect requirement in policydb backend from
	  Ivan Gyurdiev.
	* Merged commit locking fix and lock rename from Joshua Brindle.
	* Merged transaction rollback in lock patch from Joshua Brindle.
	* Changed default args for load_policy to be null, as it no longer
	  takes a pathname argument and we want to preserve booleans.
	* Merged move local dbase initialization patch from Ivan Gyurdiev.
	* Merged acquire/release read lock in databases patch from Ivan Gyurdiev.
	* Merged rename direct -> policydb as appropriate patch from Ivan Gyurdiev.
	* Added calls to sepol_policy_file_set_handle interface prior
	  to invoking sepol operations on policy files.
	* Updated call to sepol_policydb_from_image to pass the handle.


* Tue Oct 18 2005 Dan Walsh <dwalsh@redhat.com> 1.3.20-1
- Update from NSA
	* Changed default args for load_policy to be null, as it no longer
	  takes a pathname argument and we want to preserve booleans.
	* Merged move local dbase initialization patch from Ivan Gyurdiev.
	* Merged acquire/release read lock in databases patch from Ivan Gyurdiev.
	* Merged rename direct -> policydb as appropriate patch from Ivan Gyurdiev.
	* Added calls to sepol_policy_file_set_handle interface prior
	  to invoking sepol operations on policy files.
	* Updated call to sepol_policydb_from_image to pass the handle.

* Tue Oct 18 2005 Dan Walsh <dwalsh@redhat.com> 1.3.20-1
- Update from NSA
	* Merged user and port APIs - policy database patch from Ivan
	Gyurdiev.
	* Converted calls to sepol link_packages and expand_module interfaces
	from using buffers to using sepol handles for error reporting, and 
	changed direct_connect/disconnect to create/destroy sepol handles.

* Sat Oct 15 2005 Dan Walsh <dwalsh@redhat.com> 1.3.18-1
- Update from NSA
	* Merged bugfix patch from Ivan Gyurdiev.
	* Merged seuser database patch from Ivan Gyurdiev.
	Merged direct user/port databases to the handle from Ivan Gyurdiev.
	* Removed obsolete include/semanage/commit_api.h (leftover).
	Merged seuser record patch from Ivan Gyurdiev.
	* Merged boolean and interface databases from Ivan Gyurdiev.

* Fri Oct 14 2005 Dan Walsh <dwalsh@redhat.com> 1.3.14-1
- Update from NSA
	* Updated to use get interfaces for hidden sepol_module_package type.
	* Changed semanage_expand_sandbox and semanage_install_active
	to generate/install the latest policy version supported	by libsepol
	by default (unless overridden by semanage.conf), since libselinux
	will now downgrade automatically for load_policy.
	* Merged new callback-based error reporting system and ongoing
	database work from Ivan Gyurdiev.

* Wed Oct 12 2005 Dan Walsh <dwalsh@redhat.com> 1.3.11-1
- Update from NSA
	* Fixed semanage_install_active() to use the same logic for
	selecting a policy version as semanage_expand_sandbox().  Dropped
	dead code from semanage_install_sandbox().

* Mon Oct 10 2005 Dan Walsh <dwalsh@redhat.com> 1.3.10-1
- Update from NSA
	* Updated for changes to libsepol, and to only use types and interfaces
	provided by the shared libsepol.

* Fri Oct 7 2005 Dan Walsh <dwalsh@redhat.com> 1.3.9-1
- Update from NSA
	* Merged further database work from Ivan Gyurdiev.

* Tue Oct 4 2005 Dan Walsh <dwalsh@redhat.com> 1.3.8-1
- Update from NSA
	* Merged iterate, redistribute, and dbase split patches from
	Ivan Gyurdiev.

* Mon Oct 3 2005 Dan Walsh <dwalsh@redhat.com> 1.3.7-1
- Update from NSA
	* Merged patch series from Ivan Gyurdiev.
	  (pointer typedef elimination, file renames, dbase work, backend
	   separation)
	* Split interfaces from semanage.[hc] into handle.[hc], modules.[hc].
	* Separated handle create from connect interface.
	* Added a constructor for initialization.
	* Moved up src/include/*.h to src.
	* Created a symbol map file; dropped dso.h and hidden markings.

* Wed Sep 28 2005 Dan Walsh <dwalsh@redhat.com> 1.3.5-1
- Update from NSA
	* Split interfaces from semanage.[hc] into handle.[hc], modules.[hc].
	* Separated handle create from connect interface.
	* Added a constructor for initialization.
	* Moved up src/include/*.h to src.
	* Created a symbol map file; dropped dso.h and hidden markings.

* Fri Sep 23 2005 Dan Walsh <dwalsh@redhat.com> 1.3.4-1
- Update from NSA
	* Merged dbase redesign patch from Ivan Gyurdiev.

* Wed Sep 21 2005 Dan Walsh <dwalsh@redhat.com> 1.3.3-1
- Update from NSA
	* Merged boolean record, stub record handler, and status codes 
	  patches from Ivan Gyurdiev.

* Tue Sep 20 2005 Dan Walsh <dwalsh@redhat.com> 1.3.2-1
- Update from NSA
	* Merged stub iterator functionality from Ivan Gyurdiev.
	* Merged interface record patch from Ivan Gyurdiev.

* Wed Sep 14 2005 Dan Walsh <dwalsh@redhat.com> 1.3.1-1
- Update from NSA
	* Merged stub functionality for managing user and port records,
	and record table code from Ivan Gyurdiev.
	* Updated version for release.

* Thu Sep 1 2005 Dan Walsh <dwalsh@redhat.com> 1.1.6-1
- Update from NSA
	* Merged semod.conf template patch from Dan Walsh (Red Hat),
	but restored location to /usr/share/semod/semod.conf.
	* Fixed several bugs found by valgrind.
	* Fixed bug in prior patch for the semod_build_module_list leak.
	* Merged errno fix from Joshua Brindle (Tresys).
	* Merged fix for semod_build_modules_list leak on error path
	  from Serge Hallyn (IBM).  Bug found by Coverity.

* Thu Aug 25 2005 Dan Walsh <dwalsh@redhat.com> 1.1.3-1
- Update from NSA
	* Merged errno fix from Joshua Brindle (Tresys).
	* Merged fix for semod_build_modules_list leak on error path
	  from Serge Hallyn (IBM).  Bug found by Coverity.
	* Merged several fixes from Serge Hallyn (IBM).  Bugs found by
	  Coverity.
	* Fixed several other bugs and warnings.
	* Merged patch to move module read/write code from libsemanage
	  to libsepol from Jason Tang (Tresys).	
	* Merged relay records patch from Ivan Gyurdiev.
	* Merged key extract patch from Ivan Gyurdiev.

- Initial version
- Created by Stephen Smalley <sds@epoch.ncsc.mil> 



