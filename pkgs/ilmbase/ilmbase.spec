%global momorel 1

Summary: Abraction/convenience libraries
Name: ilmbase
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: Modified BSD
URL: http://www.openexr.org/
Group: System Environment/Libraries
Source0: https://github.com/downloads/openexr/openexr/%{name}-%{version}.tar.gz
NoSource: 0
## upstreamable patches
# revert soname bump
# upstream missed bumping to so7 for OpenEXR-1.7.0, decided to do so now for
# OpenEXR-1.7.1.  given fedora has shipped OpenEXR-1.7.0 since f15, bumping
# ABI now makes little sense.
Patch50: %{name}-%{version}-so6.patch
# explicitly add $(PTHREAD_LIBS) to libIlmThread linkage (helps workaround below)
Patch51: %{name}-1.0.2-no_undefined.patch
# the FPU exception code is x86 specific
Patch52: %{name}-%{version}-secondary.patch
# add Requires.private: gl glu to IlmBase.pc
Patch53: %{name}-%{version}-pkgconfig.patch
## upstream patches
# fix build on i686/32bit
# https://github.com/openexr/openexr/issues/3
Patch100: %{name}-%{version}-ucontext.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: pkgconfig

%description
Half is a class that encapsulates the ilm 16-bit floating-point format.

IlmThread is a thread abstraction library for use with OpenEXR
and other software packages.

Imath implements 2D and 3D vectors, 3x3 and 4x4 matrices, quaternions
and other useful 2D and 3D math functions.

Iex is an exception-handling library.

%package devel
Summary: Headers for developing programs that will use %{name}.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libGL-devel
Requires: libGLU-devel
Requires: pkgconfig

%description devel
This package contains the static libraries and header files needed for
developing applications with OpenEXR.

%prep
%setup -q

%patch50 -p1 -b .so6
%patch51 -p1 -b .no_undefined
%patch52 -p1 -b .secondary
%patch53 -p1 -b .pkgconfig
%if %{__isa_bits} == 32
%patch100 -p1 -b .ucontext
%endif
./bootstrap

%build
# fix build (make check) on i686
%ifarch %{ix86}
%global optflags `echo %{optflags} | sed s/-O2/-O1/`
%endif

%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%check
make check

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL LICENSE NEWS README*
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/OpenEXR
%{_libdir}/pkgconfig/IlmBase.pc
%{_libdir}/lib*.so

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sun Jul 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-2m)
- fix build on i686

* Sat Jul 14 2012 NARITA koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
- patches were imported from Fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- initial package for OpenEXR-1.6.1
- import 2 patches from Fedora
