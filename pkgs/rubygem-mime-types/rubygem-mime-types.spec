# Generated from mime-types-1.18.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname mime-types

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: This library allows for the identification of a file's likely MIME content type
Name: rubygem-%{gemname}
Version: 1.18
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://mime-types.rubyforge.org/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
This library allows for the identification of a file's likely MIME content
type. This is release 1.17.2. The identification of MIME content type is based
on a file's filename extensions.
MIME::Types for Ruby originally based on and synchronized with MIME::Types for
Perl by Mark Overmeer, copyright 2001 - 2009. As of version 1.15, the data
format for the MIME::Type list has changed and the synchronization will no
longer happen.
:include: Licence.rdoc


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.rdoc
%doc %{geminstdir}/Licence.rdoc
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/type-lists/application.txt
%doc %{geminstdir}/type-lists/audio.txt
%doc %{geminstdir}/type-lists/image.txt
%doc %{geminstdir}/type-lists/message.txt
%doc %{geminstdir}/type-lists/model.txt
%doc %{geminstdir}/type-lists/multipart.txt
%doc %{geminstdir}/type-lists/text.txt
%doc %{geminstdir}/type-lists/video.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.18-1m)
- update 1.18

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17.2-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17.2-1m)
- update 1.17.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16-2m)
- full rebuild for mo7 release

* Mon Aug 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.16-1m)
- Initial package for Momonga Linux
