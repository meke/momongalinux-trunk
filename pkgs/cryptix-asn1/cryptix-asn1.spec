%global momorel 6

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}


%define pname		Cryptix-asn1
%define snapshot	20011119
%define section		free

Name:		cryptix-asn1
#FIXME: We will have to tackle this obscene version number soon.
#Perhaps this package will just die before then.
Version:	20011119
Release:	8jpp.%{momorel}m%{?dist}
#Epoch:		0
Summary:	Cryptix ASN1 implementation
License:	BSD
Group:		Applications/File
Url:		http://www.cryptix.org
Source0:	%{pname}-%{snapshot}-RHCLEAN.tar.bz2
Source1:	%{name}.build.script
Requires:	cryptix
BuildRequires:	ant, cryptix, jpackage-utils > 1.5
BuildRequires:  java >= 1.6.0
%if ! %{gcj_support}
BuildArch:	noarch
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
Java crypto package and asn1 implementation.

%package javadoc
Group:		Documentation
Summary:	Javadoc for %{name}

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{pname}-%{snapshot}
cp %{SOURCE1} build.xml
# remove all binary libs
rm -fr $(find . -name "*.jar")

%build
ant clean jar javadoc

%install
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 build/lib/%{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} ${jar/-%{version}/}; done)
# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(-,root,root)
%doc cryptix/asn1/docs/*
%{_javadir}/*.jar

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/cryptix-asn1-%{version}.jar.*
%endif

%files javadoc
%defattr(-,root,root)
%{_javadocdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20011119-8jpp.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20011119-8jpp.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20011119-8jpp.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20011119-8jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20011119-8jpp.2m)
- rebuild against rpm-4.6

* Mon Apr 07 2008 Deepak Bhole <dbhole@redhat.com> 20011119-8jpp.3
- Fix bz# 440855: BR java-devel >= 1.6 to bypass sinjdoc issues

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0:20011119-8jpp.2
- Autorebuild for GCC 4.3

* Fri Aug 11 2006 Vivek Lakshmanan <vivekl@redhat.com> 0:20011119-7jpp.2
- Rebuild with new naming convention.

* Mon Jul 24 2006 Vivek Lakshmanan <vivekl@redhat.com> 0:20011119-7jpp_1fc
- Remove duplicate NVR defines.
- Conditional native compilation for GCJ.
- Continuing to use the same version for now since the original package
  version is too big.
- May need an epoch bump next time to bring it inline with JPP version number.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> 0:20011119-4jpp_3fc
- Rebuilt

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:20011119-4jpp_2fc.1.1.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jun 21 2005 Gary Benson <gbenson@redhat.com> 20011119-4jpp_2fc
- Remove classes and jarfiles from the tarball.

* Fri Jan 21 2005 Gary Benson <gbenson@redhat.com> 20011119-4jpp_1fc
- Build into Fedora.

* Fri Mar  5 2004 Frank Ch. Eigler <fche@redhat.com> 20011119-4jpp_1rh
- RH vacuuming

* Wed Mar 26 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> 20011119-4jpp
- for jpackage-utils 1.5

* Tue May 07 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 20011119-3jpp 
- vendor, distribution, group tags
- section macro

* Fri Jan 18 2002 Henri Gomez <hgomez@slib.fr> 20011119-2jpp
- add version in javadoc dir

* Fri Dec 14 2001 Henri Gomez <hgomez@slib.fr> 20011119-1jpp
- first JPackage release
- this snapshot came from puretls site, www.rtfm.com, since it fixes problems with puretls
