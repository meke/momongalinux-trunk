%global momorel 4

%global srcname JavaCsv

Name:		javacsv
Version:	2.0
Release:	%{momorel}m%{?dist}
Summary:	Stream-based Java library for reading and writing CSV and other delimited data

Group:		Development/Libraries
License:	LGPLv2+
URL:		http://www.csvreader.com/
Source0:	http://downloads.sourceforge.net/project/%{name}/%{srcname}/%{srcname}%20{version}/%{name}%{version}.zip
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

BuildRequires:	jpackage-utils
BuildRequires:	java-devel >= 0:1.5
BuildRequires:	ant

Requires:	jpackage-utils

Requires:	java >= 0:1.5


%description
Java CSV is a small fast open source Java 
library for reading and writing CSV and plain
delimited text files. All kinds of CSV files
can be handled, text qualified, Excel formatted, etc.

%package javadoc
Summary:	Javadocs for %{name}
Group:		Documentation
Requires:	%{name} = %{version}-%{release}
Requires:	jpackage-utils

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -c

find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;

rm src/AllTests.java


%build
ant
ant -buildfile javadoc.xml

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p %{name}.jar \
$RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
rm doc/javadocs.zip
cp -rp doc/*  \
$RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_javadir}/%{name}*
%doc

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-1m)
- import from Fedora 13

* Sun Nov 29 2009 Andreas Osowski <th0br0@mkdir.name> - 2.0-3
- Cosmetic Changes

* Sun Nov 29 2009 Andreas Osowski <th0br0@mkdir.name> - 2.0-2
- Added symlinks

* Sat Nov 21 2009 Andreas Osowski <th0br0@mkdir.name> - 2.0-1
- Initial package build
