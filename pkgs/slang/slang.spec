%global momorel 2

Summary: The shared library for the S-Lang extension language.
Name: slang
Version: 2.2.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
Source: ftp://space.mit.edu/pub/davis/slang/v2.2/slang-%{version}.tar.bz2
NoSource: 0
Patch1: slang-2.2.4-perms.patch
URL: http://www.jedsoft.org/slang/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool, ncurses-devel >= 5.6-10
BuildRequires: libpng-devel oniguruma-devel pcre-devel >= 8.31
Requires: ncurses

%description
S-Lang is an interpreted language and a programming library.  The
S-Lang language was designed so that it can be easily embedded into
a program to provide the program with a powerful extension language.
The S-Lang library, provided in this package, provides the S-Lang
extension language.  S-Lang's syntax resembles C, which makes it easy
to recode S-Lang procedures in C if you need to.

%package slsh
Summary: Interpreter for S-Lang scripts
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description slsh
slsh (slang-shell) is a program for interpreting S-Lang scripts.
It supports dynamic loading of S-Lang modules and includes a readline
interface for interactive use.

This package also includes S-Lang modules that are distributed with
the S-Lang distribution.

%package devel
Summary: Summary: Development files for the S-Lang extension language
Group: Development/Libraries
Requires: slang = %{version}
Requires: ncurses-devel

%description devel
This package contains the S-Lang extension language static libraries
and header files which you'll need if you want to develop S-Lang based
applications.  Documentation which may help you write S-Lang based
applications is also included.

Install the slang-devel package if you want to develop applications
based on the S-Lang extension language.

%package static
Summary: The static library for the S-Lang extension language
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
This package includes static library for the S-Lang extension
language.

%prep
%setup -n slang-%{version} -q
%patch1 -p1 -b .perms

%build
%configure --includedir=%{_includedir}/slang \
	   --with-{pcre,onig,png}lib=%{_libdir} \
	   --with-{pcre,onig,png}inc=%{_includedir}
# fails with %{?_smp_mflags}
make RPATH="" install_doc_dir=%{_docdir}/%{name}-%{version}

%install
rm -rf ${RPM_BUILD_ROOT}

make install-all INSTALL="install -p" DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT%{_docdir}/{slang,slsh}

# remove unpackaged files from the buildroot
rm -rf ${RPM_BUILD_ROOT}%{_prefix}/doc

%clean
rm -rf ${RPM_BUILD_ROOT}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING doc/README changes.txt doc/*/slang*.txt doc/*.txt
%{_libdir}/libslang.so.*

%files slsh
%defattr(-,root,root)
%doc slsh/doc/html/slsh*.html
%config(noreplace) %{_sysconfdir}/slsh.rc
%{_bindir}/slsh
%{_libdir}/slang
%{_mandir}/man1/slsh.1*
%{_datadir}/slsh

%files devel
%defattr(-,root,root)
%doc doc/README doc/*/*.txt doc/*.txt
%{_libdir}/libslang*.so
%{_libdir}/pkgconfig/slang.pc
%{_includedir}/slang

%files static
%defattr(-,root,root)
%{_libdir}/libslang*.a

%changelog
* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.4-2m)
- rebuild against pcre-8.31

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.4-1m)
- update 2.2.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-2m)
- full rebuild for mo7 release

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-1m)
- update 2.2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.4-2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.4-1m)
- sync with Fedora devel (2.1.4-1)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-2m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-1m)
- version up

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-1m)
- version up

* Sun May 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.5-1m)
- version up

* Sun Nov  6 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.4.9-9m)
- filter GLIBC_PRIVATE

* Fri Feb 18 2005 Toru Hoshina <t@momonga-linux.org>
- (1.4.9-8m)
- revised spec so the static library avoid stack protection.

* Fri Dec  3 2004 TAKAHASHI Tamotsu <tamo>
- (1.4.9-7m)
- Patch4: slang-1.4.9-buffer.patch
 + * Wed Nov 03 2004 Adrian Havill <havill@redhat.com> 1.4.9-7
 + - fixed potentional two buffer overflows (#120291)

* Fri Aug 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.4.9-6m)
  added libslang-nossp.a

* Thu Dec  5 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.9-5m)
- modify spec file

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.9-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.4.9-3m)
- rename to slang from slang-utf8

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.4.9-2m)
- sync with rawhide(slang-1.4.5-16)
- use %%{_smp_mflags}

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-1m)
- update to 1.4.9
- delete define ver
- use rpm macros

* Sat Feb 01 2003 TAKAHASHI Tamotsu
- (1.4.8-1m)
- ACS chars are mapped to UTF-8 (acs patch)
- doc/README doc/OLD doc/internal/* doc/text/* doc/tm/* -> doc/*

* Mon Jan 27 2003 TAKAHASHI Tamotsu
- (1.4.5-3m)
- update patch
- update source location
- libslang -> libslang-utf8 (utf8hack patch)
- include/slang -> include/slang-utf8
- comment out Provides, Obsoletes, Conflicts
- devel: Requires the same release

* Fri Apr 19 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (slang-utf8-1.4.5-2k)
- update to 1.4.5

* Mon Feb  4 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (slang-utf8-1.4.4-2k)
- renamed
- applied EGE's utf-8 patch, and removed jp patch
- changed url
- decided to conflict with jed

* Sun Mar 25 2001 Toru Hoshina <toru@df-usa.com>
( 1.4.4-2k)

* Sun Dec  3 2000 Toru Hoshina <toru@df-usa.com
- R.I.P, slang-ja...

* Sat Oct 28 2000 Toru Hoshina <toru@df-usa.com>
- version up (1.4.2jp0-1k)

* Tue Jul 25 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91.

* Thu Jun 20 2000 Toru Hoshina <t@kondara.org>
- version up.

* Wed May 17 2000 MATSUDA, Daiki <dyky@df-usa.com>
- Modify the bug that libslang-ja.so.1 is not made in ldconfig on alpha architecture

* Fri Mar 31 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot, Summary, description )

* Sat Mar 11 2000 Kikutani Makoto <g@kondara.org>
- new upstream version
- remove skktcp

* Sat Dec 18 1999 Tenkou N. Hattori <tnh@kondara.org>
- Package name is changed to slang-ja.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Aug 11 1999 Toru Hoshina <Hoshina@best.com>
- version up to 1.3.8-j067.

* Mon Jun 14 1999 Toru Hoshina <Hoshina@best.com>
- version up to 1.3.7-j065. Guhaaaa :-)

* Wed Jun 9 1999 Toru Hoshina <Hoshina@best.com>
- version up to 1.3.7-j064.

* Thu May 27 1999 Toru Hoshina <Hoshina@best.com>
- version up to 1.3.6-j064.

* Tue May 18 1999 Tenkou N. Hattori <tnh@aurora.dti.ne.jp>
- revision up to 1.3.5-j063.

* Tue Mar 16 1999 Toru Hoshina<hoshina@best.com>
- rebuild against rawhide 1.2.9

* Wed Mar  3 1999 Toru Hoshina <hoshina@best.com>
- revision up.

* Sat Feb 20 1999 Toru Hoshina <hoshina@best.com>
- added tcp.c that is the source file for "skktcp" supplementary utility.

* Wed Jan 06 1999 Toru Hoshina <hoshina@best.com>
- bug fix. make sure to be enabled 'ESC' key ;-P

* Sat Nov 21 1998 Toru Hoshina <hoshina@best.com>
- re-build against RedHat 5.2 under libwcsmbs.

* Mon Oct 26 1998 Toru Hoshina <hoshina@best.com>
- revision up to J054.

* Sun Aug 23 1998 Toru Hoshina <hoshina@best.com>
- revision up. Package name is changed to slang_jp in order to be coexist with slang 0.xx.

* Mon Jul  6 1998 Toru Hoshina <hoshina@best.com>
- First release.
