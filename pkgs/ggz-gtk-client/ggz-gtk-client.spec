%global momorel 6

Summary: Gtk+ client libraries for GGZ gaming zone
Name: ggz-gtk-client
Version: 0.99.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Libraries    
URL: http://www.ggzgamingzone.org/
Source0: http://mirrors.ibiblio.org/pub/mirrors/ggzgamingzone/ggz/snapshots/%{name}-snapshot-%{version}.tar.gz
Source1: ggz16.png
Source2: ggz22.png
Source3: ggz32.png
Source4: ggz48.png
Source5: ggz64.png
Source6: ggz128.png
Patch0: %{name}-0.0.14.1-add-icon.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: gettext
BuildRequires: ggz-base-libs-devel
BuildRequires: gtk2-devel

%description
The GGZ Gaming Zone GTK+ Client provides a GTK+ 2.x user interface
for logging into a GGZ server, chatting with other players, and
locating and launching game tables.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ggz-client-libs-devel
Requires: gtk2-devel
Requires: libggz-devel

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{name}-snapshot-%{version}
# Avoid lib64 rpaths
%if "%{_libdir}" != "/usr/lib"
sed -i -e 's|"/lib /usr/lib|"/%{_lib} %{_libdir}|' configure
%endif

%patch0 -p1 -b .icon

%build
# Will look at building the gaim plugin down the road.
%configure --disable-gaim-plugin	\
	   --disable-debug		\
	   --enable-static=no		\
	   --disable-pidgin-plugin

# hack to omit unused-direct-shlib-dependencies
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -exec rm -f {} ';'

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/ggz.png
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/ggz.png
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/ggz.png
install -m 644 %{SOURCE4} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/ggz.png
install -m 644 %{SOURCE5} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/ggz.png
install -m 644 %{SOURCE6} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/ggz.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/ggz.png %{buildroot}%{_datadir}/pixmaps/ggz.png

%find_lang ggz-gtk

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f ggz-gtk.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS QuickStart.GGZ README* TODO
%{_sysconfdir}/ggz.modules.d/ggz-gtk-client
%{_bindir}/ggz-gtk
%{_libdir}/ggz/*
%{_libdir}/libggz-gtk.so.*
%{_datadir}/applications/ggz-gtk.desktop
%{_datadir}/ggz/*
%{_datadir}/icons/*/*/*/ggz.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_mandir}/man6/ggz-gtk.6.*
%{_datadir}/pixmaps/ggz.png

%files devel
%defattr(-,root,root,-)
%{_includedir}/ggz-*.h
%{_libdir}/libggz-gtk.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.5-4m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.5-3m)
- remove dups

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-2m)
- re-add lost icons Source1-6
- remove vendor=fedora from desktop file
- sort %%files
- sort BRs
- get rid of mixed tabs and spaces

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.5-1m)
- sync with Fedora 13 (0.99.5-2)
- --disable-pidgin-plugin

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14.1-11m)
- build fix gcc-4.4.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.14.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.14.1-9m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.14.1-8m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Wed Sep 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.14.1-7m)
- add icons Source1-6, because convert -size makes blank

* Tue Sep 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14.1-6m)
- add -size ??x?? to convert (%%install) build fix only

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14.1-5m)
- do not modify desktop file

* Fri May 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14.1-4m)
- add category GNOME for menu

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.14.1-3m)
- add icon for menu entry

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.14.1-2m)
- rebuild against gcc43

* Mon Mar 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.14.1-1m)
- update to 0.0.14.1

* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14-1m)
- initial build
