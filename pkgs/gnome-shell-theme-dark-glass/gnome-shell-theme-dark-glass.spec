%global src_name gnome_shell___dark_glass_by_half_left-d36szax.zip
%global theme_name dark-glass
%global theme Dark-Glass
%global momorel 2


Name:           gnome-shell-theme-%{theme_name}
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        The %{theme} gnome-shell theme  
Group:          User Interface/Desktops     

License:        GPLv3
URL:            http://half-left.deviantart.com/art/GNOME-Shell-Dark-Glass-192828345
## http://www.deviantart.com/download/192828345/%{src_name}
Source0:        %{src_name}

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       gnome-shell-extension-user-theme
Requires:       gnome-shell >= 3.0.1
BuildArch:      noarch

%description
The %{theme} gnome-shell theme created by half_left

%prep
%setup -q -c 

%build
# nothing to build

%install
mkdir -p -m755 %{buildroot}/%{_datadir}/themes/%{theme}/gnome-shell

# put the theme files into some data dir
cp -r gs-%{theme_name}/* %{buildroot}/%{_datadir}/themes/%{theme}/.

# delete backup files (*~)
find %{buildroot} -name *~ -type f -print | xargs /bin/rm -f

# remove LICENSE from BUILDROOT
find %{buildroot} -name LICENSE -type f -print | xargs /bin/rm -f


%files
%doc gs-%{theme_name}/gnome-shell/LICENSE
%dir %{_datadir}/themes/%{theme}
%{_datadir}/themes/%{theme}/*


%changelog
* Tue Jul 31 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- change Requires from gnome-shell-extensions-user-theme to gnome-shell-extension-user-theme

* Mon Aug 29 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.0-1m)
- initial build

* Sun Jun 12 2011 Tim Lauridsen <timlau@fedoraproject.org> 1.0
- initial rpm build
