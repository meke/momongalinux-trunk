%global momorel 4

Name:           ptouch-driver
Version:        1.3
Release:        %{momorel}m%{?dist}
Summary:        CUPS driver for Brother P-touch label printers

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.diku.dk/~panic/P-touch/
Source0:        http://www.diku.dk/~panic/P-touch/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:         ptouch-driver-1.2-gcc43.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cups-devel
BuildRequires:  automake
Requires:       cups

%description
This is a CUPS raster filter for Brother P-touch label printers.  It is
meant to be used by the PostScript Description files of the drivers from
the foomatic package.

%prep
%setup -q
%patch0 -p1 -b .gcc43

%build
# On 64bits, we need to install into lib, not lib64
# and this package for some reason uses libdir
%configure --libdir=%{_prefix}/lib
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install-exec DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_prefix}/lib/cups/filter/rastertoptch
%doc AUTHORS ChangeLog COPYING NEWS README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 13 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.2-9
- Fix build with gcc-4.3

* Wed Aug 22 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-8
- Bump revision for BuildID in -debuginfo rebuild

* Fri Aug 3 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-7
- Modify the License tag in accordance with the new guidelines

* Fri Aug 3 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-6
- No. No automake. For the kids!

* Fri Jul 27 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-5
- ...and call it

* Fri Jul 27 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-4
- ...the specific version of automake

* Fri Jul 27 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-3
- We need automake for patch0 to have effect

* Fri Jul 27 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-2
- Install to the right place on 64bit platforms

* Fri Jul 20 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.2-1
- Initial package
