%global momorel 8

Summary: Fortran77 subroutines for solving large scale eigenvalue problems
Name: arpack
Version: 2.1
Release: %{momorel}m%{?dist}
License: "RiceBSD.txt"
Group: Development/Libraries
URL: http://www.caam.rice.edu/software/ARPACK/
Source0: http://www.caam.rice.edu/software/ARPACK/SRC/arpack96.tar.gz
NoSource: 0
Source1: http://www.caam.rice.edu/software/ARPACK/SRC/patch.tar.gz
NoSource: 1
Source2: http://www.caam.rice.edu/software/ARPACK/RiceBSD.doc
Source3: RiceBSD.txt
# https://bugzilla.redhat.com/bugzilla/attachment.cgi?id=148107
Source4: clarification-note-by-authors.txt
Patch0: arpack-2.1-redhat.patch
# see http://www.ann.jussieu.fr/pipermail/freefempp/2006/000213.html
Patch1: arpack-second-bug.patch
BuildRequires: gcc-gfortran
BuildRequires: lapack-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ARPACK is a collection of Fortran77 subroutines designed to solve large 
scale eigenvalue problems. 

The package is designed to compute a few eigenvalues and corresponding
eigenvectors of a general n by n matrix A. It is most appropriate for
large sparse or structured matrices A where structured means that a
matrix-vector product w <- Av requires order n rather than the usual
order n**2 floating point operations. This software is based upon an
algorithmic variant of the Arnoldi process called the Implicitly
Restarted Arnoldi Method (IRAM).

%package devel
Summary: Files needed for developing arpack based applications
Group: Development/Libraries
Requires: arpack = %{version}-%{release}

%description devel
ARPACK is a collection of Fortran77 subroutines designed to solve
large scale eigenvalue problems. This package contains the so
library links used for building arpack based applications.

%package static
Summary: Static library for developing arpack based applications
Group: Development/Libraries
Requires: arpack-devel = %{version}-%{release}

%description static
ARPACK is a collection of Fortran77 subroutines designed to solve
large scale eigenvalue problems. This package contains the static
library and so links used for building arpack based applications.

%prep
%setup -q -b 1 -n ARPACK
%patch0 -p1 -b .rh
%patch1 -p1 -b .sb
mkdir static shared

%build
cd shared
for dir in ../SRC ../UTIL; do
  make -f $dir/Makefile VPATH=$dir srcdir=$dir \
       FC=gfortran FFLAGS="%{optflags} -fPIC" \
       single double complex complex16
done
gcc -shared -llapack -Wl,-soname,libarpack.so.2 -o libarpack.so.2.1 *.o
cd ..
cd static
for dir in ../SRC ../UTIL; do
  make -f $dir/Makefile VPATH=$dir srcdir=$dir \
  FC=gfortran FFLAGS="%{optflags}" LDFLAGS="-s" \
       all
done
ar rv libarpack.a *.o
ranlib libarpack.a
cd ..

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}
install -p -m644 static/libarpack.a %{buildroot}%{_libdir}
install -p -m755 shared/libarpack.so.2.1 %{buildroot}%{_libdir}
ln -s libarpack.so.2.1 %{buildroot}%{_libdir}/libarpack.so.2
ln -s libarpack.so.2 %{buildroot}%{_libdir}/libarpack.so
cp -p %{SOURCE2} %{SOURCE3} %{SOURCE4} .

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc RiceBSD.doc clarification-note-by-authors.txt
%{_libdir}/libarpack.so.*

%files devel
%defattr(-,root,root,-)
%doc DOCUMENTS EXAMPLES
%{_libdir}/libarpack.so

%files static
%{_libdir}/libarpack.a

%changelog
* Wed Dec 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-8m)
- rebuild for lapack-3.4.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-2m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Fri Apr 18 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1-1m)
- import to Momonga (from Fedora (2.1-7))

* Wed Oct 24 2007 Dominik 'Rathann' Mierzejewski <rpm@greysector.net> 2.1-7
- apply Frederic Hecht's patch for eigenvalue bug
- move static libs to separate package

* Mon Mar 26 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 2.1-6
- Add license and clarification note
- Add lapack support

* Thu Nov  9 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 2.1-4
- Freshen up for submitting to fedora extras.
- Fix permissions of so file.
- Add forgotten ldconfig
- Remove dot from summaries.

* Wed Jul 16 2003 Axel Thimm <Axel.Thimm@ATrpms.net>
- Initial build.
