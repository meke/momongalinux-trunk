%global momorel 3
%global qtver 4.8.2
%global kdever 4.8.97
%global kdelibsrel 1m

Summary: KMediaFactory is DVD authoring tool for KDE
Name: kmediafactory
Version: 0.8.1
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://code.google.com/p/kmediafactory/
Group: Applications/Multimedia
Source0: http://kmediafactory.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.8.0-desktop.patch
Patch1: %{name}-0.8.0-link.patch
Patch2: %{name}-%{version}-header.patch
Patch3: %{name}-%{version}-ffmpeg.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils
Requires(postun): shared-mime-info
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: dvdauthor
Requires: ffmpeg
Requires: k3b
Requires: kaffeine
Requires: libkexiv2 >= %{kdever}
Requires: mjpegtools
Requires: phonon
Requires: zip
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: dvdauthor
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: fontconfig-devel
BuildRequires: gettext
BuildRequires: ghostscript-devel 
BuildRequires: k3b-devel
BuildRequires: kaffeine
BuildRequires: libdvdread-devel
BuildRequires: libkexiv2-devel >= %{kdever}
BuildRequires: mjpegtools-devel
BuildRequires: openoffice.org-devel
BuildRequires: phonon
BuildRequires: shared-mime-info
BuildRequires: xine-lib-devel
BuildRequires: zip

%description
KMediaFactory is easy to use template based dvd authoring tool.
You can quickly create DVD menus for home videos and TV recordings in three simple steps.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja~
%patch1 -p1 -b .link
%patch2 -p1 -b .header
%patch3 -p1 -b .ffmpeg

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
%{_bindir}/update-desktop-database &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
/sbin/ldconfig
%{_bindir}/update-desktop-database &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING CREDITS ChangeLog INSTALL NEWS README TODO
%{_kde4_bindir}/%{name}
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/kde4/plugins/designer/kmfwidgets.so
%{_kde4_libdir}/kde4/%{name}_*.so
%{_kde4_libdir}/lib%{name}interfaces.so*
%{_kde4_libdir}/lib%{name}kstore.so*
%{_kde4_libdir}/libkmf.so*
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/%{name}_template
%{_kde4_appsdir}/kmfwidgets
%{_kde4_configdir}/%{name}.knsrc
%{_kde4_configdir}/%{name}_template.knsrc
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_datadir}/config.kcfg/outputplugin.kcfg
%{_kde4_datadir}/config.kcfg/slideshowplugin.kcfg
%{_kde4_datadir}/config.kcfg/templateplugin.kcfg
%{_kde4_datadir}/config.kcfg/videoplugin.kcfg
%{_kde4_docdir}/HTML/en/%{name}
%{_kde4_iconsdir}/hicolor/*/*/*%{name}*.png
%{_kde4_iconsdir}/hicolor/*/*/*%{name}*.svgz
%{_kde4_datadir}/kde4/services/%{name}_*.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}plugin.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{name}*.mo
%{_datadir}/locale/*/LC_MESSAGES/libkmf.mo
%{_kde4_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/%{name}.png

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-3m)
- rebuild against ffmpeg

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-2m)
- rebuild with KDE 4.9 RC2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- version 0.8.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-9m)
- rebuild against KDE 4.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-7m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-5m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-4m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-3m)
- rebuild against qt-4.6.3-1m

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-2m)
- fix build with gcc-4.4.4

* Fri Apr  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- version 0.8.0
- update desktop.patch
- remove mplayer from BR and Requires

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-2m)
- change BuildRequires: from k3b to k3b-devel

* Sun Mar 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-1m)
- version 0.7.5

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-4m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-2m)
- fix up desktop-file-install section

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-1m)
- initial package for DVD freaks using Momonga Linux
