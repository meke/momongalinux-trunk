%global momorel 2
%define pkgname xcursor-themes
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/data/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: libXcursor-devel

%description
%{pkgname}

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_datadir}/icons/handhelds
%{_datadir}/icons/redglass
%{_datadir}/icons/whiteglass

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- initial build
