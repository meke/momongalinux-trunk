%global momorel 5

Name:		tokyodystopia
Summary:	full-text search system
Version:	0.9.15
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Libraries
URL:		http://fallabs.com/tokyodystopia/
Source0:	http://fallabs.com/tokyodystopia/%{name}-%{version}.tar.gz 
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	tokyocabinet >= 1.4.45
BuildRequires: zlib-devel, bzip2-devel

%description
okyo Dystopia is a full-text search system. 
You can search lots of records for some records including specified patterns. 
The characteristic of Tokyo Dystopia is the following.

%prep
%setup -q

%build
%configure --libexecdir=%{_libdir}
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm %{buildroot}%{_libdir}/*.a

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*
%{_includedir}/*
%{_libdir}/lib*
%{_libdir}/*.cgi
%{_libdir}/pkgconfig/*.pc
%{_mandir}/*/*
%{_datadir}/%{name}

%changelog
* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.15-5m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.15-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.15-1m)
- update 0.9.15

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.14-1m)
- update 0.9.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.13-1m)
- initial package
