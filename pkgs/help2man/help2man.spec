%global momorel 1

# Supported build option:
#
# --with nls ... build this package with --enable-nls 

Name:           help2man
Summary:        Create simple man pages from --help output
Version:        1.42.1
Release:        %{momorel}m%{?dist}
Group:          Development/Tools
License:        GPLv2+
URL:            http://www.gnu.org/software/help2man
Source:         ftp://ftp.gnu.org/gnu/help2man/help2man-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%{!?_with_nls:BuildArch: noarch}
%{?_with_nls:BuildRequires: perl(Locale::gettext)}
%{?_with_nls:Requires: perl(Locale::gettext)}

Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description
help2man is a script to create simple man pages from the --help and
--version output of programs.

Since most GNU documentation is now in info format, this provides a
way to generate a placeholder man page pointing to that resource while
still providing some useful information.

%prep
%setup -q -n help2man-%{version}
iconv -f ISO-8859-1 -t utf-8 THANKS > THANKS~
mv THANKS~ THANKS

%build

%configure %{!?_with_nls:--disable-nls}
make %{?_smp_mflags}

%install
rm -fr $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
make install_l10n DESTDIR=$RPM_BUILD_ROOT

%clean
rm -fr $RPM_BUILD_ROOT

%post
/sbin/install-info %{_infodir}/help2man.info %{_infodir}/dir 2>/dev/null || :

%preun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/help2man.info \
    %{_infodir}/dir 2>/dev/null || :
fi

%files
%defattr(-, root, root,-)
%doc README NEWS THANKS
%{_bindir}/help2man
%{_infodir}/*
%{_mandir}/man1/*

%if "%{?_with_nls}"
%{_libdir}/*.so
%endif

%lang(de)    %{_datadir}/locale/de/LC_MESSAGES/help2man.mo
%lang(el)    %{_datadir}/locale/el/LC_MESSAGES/help2man.mo
%lang(eo)    %{_datadir}/locale/eo/LC_MESSAGES/help2man.mo
%lang(fi)    %{_datadir}/locale/fi/LC_MESSAGES/help2man.mo
%lang(fr)    %{_datadir}/locale/fr/LC_MESSAGES/help2man.mo
%lang(hr)    %{_datadir}/locale/hr/LC_MESSAGES/help2man.mo
%lang(it)    %{_datadir}/locale/it/LC_MESSAGES/help2man.mo
%lang(ja)    %{_datadir}/locale/ja/LC_MESSAGES/help2man.mo
%lang(pl)    %{_datadir}/locale/pl/LC_MESSAGES/help2man.mo
%lang(pt_BR) %{_datadir}/locale/pt_BR/LC_MESSAGES/help2man.mo
%lang(ru)    %{_datadir}/locale/ru/LC_MESSAGES/help2man.mo
%lang(sr)    %{_datadir}/locale/sr/LC_MESSAGES/help2man.mo
%lang(sv)    %{_datadir}/locale/sv/LC_MESSAGES/help2man.mo
%lang(uk)    %{_datadir}/locale/uk/LC_MESSAGES/help2man.mo
%lang(vi)    %{_datadir}/locale/vi/LC_MESSAGES/help2man.mo

%lang(de)    %{_mandir}/de/man1/*
%lang(el)    %{_mandir}/el/man1/*
%lang(eo)    %{_mandir}/eo/man1/*
%lang(fi)    %{_mandir}/fi/man1/*
%lang(fr)    %{_mandir}/fr/man1/*
%lang(hr)    %{_mandir}/hr/man1/*
%lang(it)    %{_mandir}/it/man1/*
%lang(ja)    %{_mandir}/ja/man1/*
%lang(pl)    %{_mandir}/pl/man1/*
%lang(pt_BR) %{_mandir}/pt_BR/man1/*
%lang(ru)    %{_mandir}/ru/man1/*
%lang(sr)    %{_mandir}/sr/man1/*
%lang(sv)    %{_mandir}/sv/man1/*
%lang(uk)    %{_mandir}/uk/man1/*
%lang(vi)    %{_mandir}/vi/man1/*

%changelog
* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42.1-1m)
- update to 1.42.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.38.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.38.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.38.2-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38.2-1m)
- update to 1.38.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.36.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.36.4-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.36.4-1m)
- import from Fedora

* Sun Feb 10 2008 Ralf Corsepius <rc040203@freenet.de> - 1.36.4-2
- Update license tag.
- Convert THANKS to utf-8.

* Tue Sep 05 2006 Ralf Corsepius <rc040203@freenet.de> - 1.36.4-1
- Upstream update.
- utf-8 encode l10n'd man pages.

* Fri Dec 23 2005 Ralf Corsepius <rc04203@freenet.de> - 1.36.3-1
- Upstream update.
- Add build option --with nls.

* Fri Dec 23 2005 Ralf Corsepius <rc04203@freenet.de> - 1.35.1-2
- Fix disttag (#176473).
- Cleanup spec.

* Fri Apr 29 2005 Ralf Corsepius <ralf[AT]links2linux.de> - 1.35.1-1
- Update to 1.35.1
- Minor spec fixes.
