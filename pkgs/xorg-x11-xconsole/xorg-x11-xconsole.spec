%global momorel 3
%define pkgname xconsole
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.0.4
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
#Patch0: %{pkgname}-%{version}-stropts.patch
BuildRequires: pkgconfig
BuildRequires: libXt-devel

%description
%{pkgname}

%prep
%setup -q -n %{pkgname}-%{version}
#%%patch0 -p1 -b .stropts

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{_bindir}/%{pkgname}
%{_datadir}/X11/app-defaults/XConsole
%{_mandir}/man1/%{pkgname}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-5m)
- rebuild against rpm-4.6

* Fri Apr  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-4m)
- stropts.h was removed from glibc; don't #include it anymore

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- %%NoSource -> NoSource

* Tue Aug 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Thu Nov  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- sepalated from xorg-x11-apps
