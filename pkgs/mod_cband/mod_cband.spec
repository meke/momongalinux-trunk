%global         momorel 10

Name:           mod_cband
Version:        0.9.7.5
Release:        %{momorel}m%{?dist}
Summary:        Bandwidth limiting for virtual hosts
Group:          System Environment/Daemons
License:        GPL
URL:            http://cband.linux.pl/
Source0:        http://cband.linux.pl/download/mod-cband-%{version}.tgz
Source1:        mod_cband.conf
Patch0:         mod_cband-0.9.7.5-http24.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  httpd-devel
Requires:   	httpd httpd-mmn = %([ -a %{_includedir}/httpd/.mmn ] && cat %{_includedir}/httpd/.mmn || echo missing)    

%description
mod_cband is an Apache 2 module provided to solve the problem of limiting
virtualhosts bandwidth usage. When the configured virtualhost's transfer limit
is exceeded, mod_cband will redirect all further requests to a location
specified in the configuration file.


%prep
%setup -q -n mod-cband-%{version}
%patch0 -p1 -b .httpd24


%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
install -D -m755 src/.libs/mod_cband.so %{buildroot}/%{_libdir}/httpd/modules/mod_cband.so
install -D -m644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/httpd/conf.d/mod_cband.conf.dist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS Changes INSTALL LICENSE conf *.copyright
%{_libdir}/httpd/modules/mod_cband.so
%config(noreplace) %{_sysconfdir}/httpd/conf.d/mod_cband.conf.dist

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7.5-10m)
- rebuild against httpd-2.4.3

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7.5-9m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7.5-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7.5-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7.5-3m)
- rebuild against gcc43

* Sat Jun  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7.5-2m)
- set PATH to %%{_sbindir}

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.7.5-1m)
- import from Fedora

* Sun Nov 26 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.5-1
- New upstream release.

* Sun Sep 3 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.4-2
- Rebuild

* Mon May 29 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.4-1
- New upstream release

* Mon May 1 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.3-2
- New upstream release

* Thu Mar 17 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.2-3
- Config file set not to be replaced on upgrade
- Status information URI limited to localhost by default.

* Tue Mar 7 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.2-2
- Updated tarball from upstream (some very small changes in code,
  essentially in integer handling).
- Example config file added and installed in Apache's conf.d dir

* Sun Feb 5 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 0.9.7.2-1
- Fedora Extras review release.
