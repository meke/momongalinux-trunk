%global momorel 11

# Copyright (c) 2000-2006, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define parent maven2
%define subname common-poms

Name:              %{parent}-%{subname}
Version:           1.0
Release:           %{momorel}m%{?dist}
Epoch:             0
Summary:           Common poms for maven2
License:           "ASL 2.0" and BSD
Group:             Development/Languages
URL:               http://jpackage.org/

# No source location for these. They are ascii files generated from maven
# repositories, and are not in cvs/svn.
Source0:           %{name}-src.tar.xz
Source1:           %{name}-jpp-depmap.xml
Source2:           %{name}-docs.tar.gz

BuildRoot:         %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:         noarch
BuildRequires:     jpackage-utils >= 0:1.7.2
Requires:          jpackage-utils >= 0:1.7.2

%description
This package is a collection of poms required by various maven2-dependent
packages.

%prep
%setup -q -n %{name}

tar xzf %{SOURCE2}

%build

%install
rm -rf $RPM_BUILD_ROOT

# Map
install -dm 755 $RPM_BUILD_ROOT%{_mavendepmapdir}
cp %{SOURCE1} $RPM_BUILD_ROOT%{_mavendepmapdir}/maven2-versionless-depmap.xml

install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/maven2
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/default_poms
install -pm 644 *.pom $RPM_BUILD_ROOT%{_datadir}/maven2/default_poms

install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/maven2
ln -s %{_datadir}/maven2/default_poms $RPM_BUILD_ROOT%{_javadir}/maven2

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc APACHE_LICENSE.TXT JSCH_LICENSE.TXT FEDORA.README
%{_mavendepmapdir}/maven2-versionless-depmap.xml
%{_javadir}/maven2
%{_datadir}/maven2/*

%changelog
* Mon Jan  9 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-11m)
- new source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-8m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- re-re-sync with Fedora 13 (0:1.0-12)

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- re-sync with Fedora 11 (0:1.0-5.4) for maven2
-- we can not build maven2-2.0.4 with maven2-common-poms-1.0-5m

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- sync with Fedora 13 (0:1.0-12)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4jpp.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4jpp.3m)
- rebuild against rpm-4.6

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4jpp.2m)
- modify %%files and Requires: maven-jxr for smart handling of directories

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-4jpp.1m)
- import from Fedora to Momonga

* Thu Mar 15 2007 Deepak Bhole <dbhole@redhat.com> 1.0-4jpp.2
- Updated depmap to make javax-servlet be tomcat-2.4-servlet-api

* Mon Feb 12 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-4jpp.1
- Fix spec per Fedora guidelines.

* Fri Oct 13 2006 Deepak Bhole <dbhole@redhat.com> 1.0-3jpp
- Changed file names to comply with the new maven2 rpm.

* Fri Sep 18 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-2jpp
- Moved common poms from maven2 rpm into this one.

* Tue Jun 13 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-1jpp
- Initial build
