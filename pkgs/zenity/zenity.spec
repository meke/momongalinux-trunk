%global momorel 1

Name: zenity
Summary: Provides graphical prompts and dialogues for shell scripts.
Version: 3.8.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.8/%{name}-%{version}.tar.xz
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gtk3-devel
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: libgnomecanvas-devel >= 2.26.0
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libnotify-devel >= 0.7.2
Requires: gtk-doc
Requires(pre): rarian

%description
This is zenity - a rewrite of gdialog, the GNOME port of dialog
which allows you to display dialog boxes from the commandline
and shell scripts. If you understand, things are just as they
are. If you don't understand, things are just as they are.
#'

%prep
%setup -q

%build
%configure --disable-scrollkeeper --enable-libnotify
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}%{_bindir}/gdialog

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update

%postun
rarian-sk-update

%files -f %{name}.lang
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/zenity
#%{_bindir}/gdialog
%{_mandir}/man1/zenity.1*
%{_datadir}/zenity
%{_datadir}/help/*/zenity

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.0-1m)
- update to 3.8.0

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-2m)
- rebuild for glib 2.33.2

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Fri Jan 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Mon Aug  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-2m)
- add Requies: gtk-doc

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (2.6.0-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (1.8-2m)
- revised spec for rpm 4.2.

* Mon Jan 26 2004 Masayuki SANO <sano@momonga-linux.org>
- (1.8-1m)
- version 1.8

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.6-1m)
- version 1.6

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-1m)
- version 1.5

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.4-1m)
- version 1.4

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3-1m)
- version 1.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2-1m)
- version 1.2

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1-1m)
- version 1.1
