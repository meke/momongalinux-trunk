# Generated from mail-2.4.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname mail

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Mail provides a nice Ruby DSL for making, sending and reading emails
Name: rubygem-%{gemname}
Version: 2.4.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/mikel/mail
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(mime-types) => 1.16
Requires: rubygem(mime-types) < 2
Requires: rubygem(treetop) => 1.4.8
Requires: rubygem(treetop) < 1.5
Requires: rubygem(i18n) >= 0.4.0
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
A really Ruby Mail handler.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.md
%doc %{geminstdir}/CONTRIBUTING.md
%doc %{geminstdir}/CHANGELOG.rdoc
%doc %{geminstdir}/TODO.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.4-1m)
- update 2.4.4

* Thu Sep  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-2m) 
- remake gem2spec

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-1m)
- update 2.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.9-1m)
- update 2.2.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.5-2m)
- full rebuild for mo7 release

* Mon Aug 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.5-1m)
- Initial package for Momonga Linux
