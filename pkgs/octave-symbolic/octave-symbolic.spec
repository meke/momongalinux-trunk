%global momorel	2
%global octpkg symbolic

Name:           octave-%{octpkg}
Version:        1.0.9
Release:        %{momorel}m%{?dist}
Summary:        Symbolic computations for Octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/symbolic/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
# This patch fixes a compile issue with an octave 3.4.0 API change.
Patch0:         %{name}.patch

BuildRequires:  octave-devel
BuildRequires:  ginac-devel

Requires:       octave(api) = %{octave_api}
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
Symbolic toolbox based on GiNaC and CLN.

%prep
%setup -q -n %{octpkg}-%{version}
%patch0 -p0 -b .fix340

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install
rm -f %{buildroot}/%{octpkgdir}/doc/INSTALL

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)
%{octpkglibdir}

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%doc %{octpkgdir}/doc

%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.9-2m)
- rebuild against octave-3.6.1-1m

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-1m)
- initial import from fedora
