%global momorel 5

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define namedversion 1.0-alpha-2

Name:           plexus-mail-sender
Version:        1.0
Release:        0.1.%{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus Mail Sender
License:        MIT and "ASL 1.1"
Group:          Development/Tools
URL:            http://plexus.codehaus.org/
# svn export http://svn.codehaus.org/plexus/tags/PLEXUS_MAIL_SENDER_1_0_ALPHA_2/
# Note: Exported revision 8188.
# mv PLEXUS_MAIL_SENDER_1_0_ALPHA_2/ plexus-mail-sender-1.0-a2
# tar czf plexus-mail-sender-1.0-a2-src.tar.gz plexus-mail-sender-1.0-a2
Source0:        plexus-mail-sender-%{version}-a2-src.tar.gz

Source1:        %{name}-mapdeps.xsl
Source2:        %{name}-jpp-depmap.xml
Source3:        maven2-settings.xml

# http://jira.codehaus.org/browse/PLX-417
# http://fisheye.codehaus.org/rdiff/plexus?csid=8336&u&N
Patch0:         %{name}-clarifylicense.patch

BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven-surefire-maven-plugin
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven-doxia-sitetools
BuildRequires:  dumbster
BuildRequires:  saxon
BuildRequires:  saxon-scripts
BuildRequires:  java-devel >= 0:1.6.0

BuildArch:      noarch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Plexus project seeks to create end-to-end developer tools for 
writing applications. At the core is the container, which can be 
embedded or for a full scale application server. There are many 
reusable components for hibernate, form processing, jndi, i18n, 
velocity, etc. Plexus also includes an application server which 
is like a J2EE application server, without all the baggage.  This 
Plexus component provides SMTP transport.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{name}-%{version}-a2

%patch0 -p3

%build

mkdir external_repo
ln -s %{_javadir} external_repo/JPP

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
        -e \
        -Dmaven2.jpp.depmap.file="%{SOURCE2}" \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        -Dmaven.test.skip=true \
        install javadoc:javadoc

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus
install -pm 644 \
  plexus-mail-senders/plexus-mail-sender-javamail/target/%{name}-javamail-%{namedversion}-SNAPSHOT.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/mail-sender-javamail-%{version}.jar
install -pm 644 \
  plexus-mail-senders/plexus-mail-sender-simple/target/%{name}-simple-%{namedversion}-SNAPSHOT.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/mail-sender-simple-%{version}.jar
install -pm 644 \
  plexus-mail-senders/plexus-mail-sender-test/target/%{name}-test-%{namedversion}-SNAPSHOT.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/mail-sender-test-%{version}.jar
install -pm 644 \
  plexus-mail-sender-api/target/%{name}-api-%{namedversion}-SNAPSHOT.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/mail-sender-api-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir}/plexus && for jar in *-%{version}*; do \
  ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)
# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/api
cp -pr plexus-mail-sender-api/target/site/apidocs/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/api
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/test
cp -pr plexus-mail-senders/plexus-mail-sender-test/target/site/apidocs/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/test
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/simple
cp -pr plexus-mail-senders/plexus-mail-sender-simple/target/site/apidocs/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/simple
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/javamail
cp -pr plexus-mail-senders/plexus-mail-sender-javamail/target/site/apidocs/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/javamail
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_javadir}/plexus/*

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.1.3m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.2m)
- sync with Fedora 13 (0:1.0-0.a2.14)

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.1m)
- import from Fedora 12

* Fri Aug 21 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.a2.12
- Force OpenJDK

* Fri Aug 21 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.a2.11
- Add BRs on maven2-plugin-{compiler,surefire,jar,install.javadoc}

* Fri Aug 21 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.a2.10
- Add BR on maven2-plugin-resources

* Fri Aug 21 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.a2.9
- Remove gcj support
- Remove javadoc post scriplet
- Add patch to clarify license (http://jira.codehaus.org/browse/PLX-417)
- Fix license
- Use tarball with versioned directory

* Sun May 17 2009 Fernando Nasser <fnasser@redhat.com> - 0:1.0-0.a2.8
- Fix license

* Tue Apr 30 2009 Yong Yang <yyang@redhat.com> - 0:1.0-0.a2.7
- Don't run the tests as they reqyuire access to the net
- Add BRs maven-doxia*
- Rebuild with new maven2 2.0.8 built in non-bootstrap mode

* Tue Mar 17 2009 Yong Yang <yyang@redhat.com> - 0:1.0-0.a2.6
- rebuild with new maven2 2.0.8 built in bootstrap mode

* Thu Feb 05 2009 Yong Yang <yyang@redhat.com> - 0:1.0-0.a2.5
- Fix release tag

* Wed Jan 14 2009 Yong Yang <yyang@redhat.com> - 0:1.0-0.a2.4jpp
- Import from dbhole's maven packages, initial building

* Tue May 15 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.a2.3jpp
- Make Vendor, Distribution based on macro

* Thu Mar 15 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.a2.2jpp
- Add dumbster BR

* Wed Sep 13 2006 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.a2.1jpp
- First JPP-1.7 release
- Add post/postun Requires for javadoc
- Add gcj_support option
