%global momorel 38

%global emacsver %{_emacs_version}
%global apelver 10.8-6m
%global flimver 1.14.9-27m
%global semiver 1.14.6-41m
%global e_sitedir %{_emacs_sitelispdir}
%global sfdir 7488

Summary: A rolodex-like database program for SEMI based MUA
Name: emacs-lsdb
Version: 0.11
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://dl.sourceforge.jp/lsdb/%{sfdir}/lsdb-%{version}.tar.gz 
NoSource: 0
URL: http://sourceforge.jp/projects/lsdb/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
BuildRequires: emacs-flim >= %{flimver}
BuildRequires: emacs-semi >= %{semiver}
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Requires: emacs-flim >= %{flimver}
Requires: emacs-semi >= %{semiver}
Obsoletes: lsdb-emacs
Obsoletes: lsdb-xemacs

Obsoletes: elisp-lsdb
Provides: elisp-lsdb

%description
LSDB (The Lovely Sister Database) is a rolodex-like database program
for SEMI based MUA.  It intends to be a lightweight replacement for
BBDB (The Insidious Big Brother Database).  Unfortunately, it
currently doesn't support the all features of BBDB.
#'

%prep
%setup -q -n lsdb-%{version}

%build
make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{_datadir}/emacs/%{emacsver}/site-lisp

%install
rm -rf %{buildroot}

make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{_datadir}/emacs/%{emacsver}/site-lisp \
  install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README bbdb-to-lsdb.el
%{e_sitedir}/lsdb

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-38m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-37m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11-36m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-35m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-34m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-33m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-32m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-31m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-30m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-29m)
- merge lsdb-emacs to elisp-lsdb
- kill lsdb-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-28m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-27m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-26m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-25m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-24m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-23m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-22m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-21m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-20m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-19m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-18m)
- rebuild against apelver 10.7-11m
- rebuild against flimver 1.14.9-5m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-17m)
- rebuild against xemacs-21.5.28
- add Patch0: LSDB-MK-xemacs.patch

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-16m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-15m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-14m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-13m)
- rebuild against flim-1.14.9

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-12m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-11m)
- rebuild against emacs-22.1

* Tue Apr 24 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.11-10m)
- add BuildPreReq  elisp-semi, semi-emacs, semi-xemacs

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-9m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-8m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-7m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-6m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-2m)
- rebuild against emacs-22.0.90

* Sat Dec 24 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.11-1m)
- update to 0.11.

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.10-7m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.10-6m)
- use %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-5m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.10-4m)
- rebuild against emacs-21.3.50

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-3m)
- rebuild against emacs-21.3
- use %{_prefix} %%{_datadir} macro

* Mon Jan 13 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (0.10-2m)
- rebuild against elisp-emiko-1.14.1-22m

* Thu Jan  9 2003 Junichiro Kita <kita@momonga-linux.org>
- (0.10-1m)
- ver up

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- add BuildPreReq: apel-emacs, apel-xemacs
- add BuildPreReq: emiko-emacs, emiko-xemacs

* Sat Sep  7 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.9-1m)
- ver up

* Mon Aug 26 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.8-1m)
- ver up

* Fri May 24 2002 Junichiro Kita <kitaj@kondara.org>
- initial build
