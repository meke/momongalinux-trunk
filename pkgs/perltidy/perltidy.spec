%global         momorel 2

Name:           perltidy
Version:        20140328
Release:        %{momorel}m%{?dist}
Summary:        Parses and beautifies perl source
License:        "Distributable, see COPYING"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Perl-Tidy/
Source0:        http://www.cpan.org/authors/id/S/SH/SHANCOCK/Perl-Tidy-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-devel >= 5.16.0
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Perl-Tidy < %{version}-%{release}
Provides:       perl-Perl-Tidy = %{version}-%{release}

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module makes the functionality of the perltidy utility available to
perl scripts. Any or all of the input parameters may be omitted, in which
case the @ARGV array will be used to provide input parameters as described
in the perltidy(1) man page.

%prep
%setup -q -n Perl-Tidy-%{version}
rm -f docs/perltidy.1 examples/pt.bat
f=CHANGES ; iconv -f iso-8859-1 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS CHANGES COPYING README TODO docs/ examples/
%{_bindir}/perltidy
%{perl_vendorlib}/Perl/
%{_mandir}/man1/perltidy.1*
%{_mandir}/man3/Perl::Tidy.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20140328-2m)
- rebuild against perl-5.20.0

* Sat Apr  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20140328-1m)
- update to 20140328

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20130922-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20130922-1m)
- update to 20130922

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20130806-2m)
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20130806-1m)
- update to 20130806

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20130717-1m)
- update to 20130717

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20121207-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20121207-2m)
- rebuild against perl-5.16.3

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20121207-1m)
- update to 20121207

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120714-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120714-2m)
- rebuild against perl-5.16.1

* Sun Jul 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120714-1m)
- update to 20120714

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120701-1m)
- update to 20120701
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20101217-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20101217-4m)
- rebuild against perl-5.14.1

* Mon May  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20101217-3m)
- rebuild against perl-5.14.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20101217-2m)
- rebuild for new GCC 4.6

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20101217-1m)
- update to 20101217

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090616-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090616-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090616-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090616-4m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090616-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090616-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090616-1m)
- update to 20090616

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (20071205-3m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071205-2m)
- obsolete perl-Perl-Tidy

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20071205-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20071205-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 27 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 20071205-3
- Rebuild for perl 5.10 (again)

* Sun Jan 13 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 20071205-2
- rebuild for new perl

* Thu Dec  6 2007 Ville Skytta <ville.skytta at iki.fi> - 20071205-1
- 20071205.
- Convert docs to UTF-8.

* Wed Aug  1 2007 Ville Skytta <ville.skytta at iki.fi> - 20070801-1
- 20070801.

* Wed May  9 2007 Ville Skytta <ville.skytta at iki.fi> - 20070508-1
- 20070508.

* Sat May  5 2007 Ville Skytta <ville.skytta at iki.fi> - 20070504-1
- 20070504.

* Tue Apr 24 2007 Ville Skytta <ville.skytta at iki.fi> - 20070424-1
- 20070424.

* Tue Apr 17 2007 Ville Skytta <ville.skytta at iki.fi> - 20060719-3
- BuildRequire perl(ExtUtils::MakeMaker).

* Fri Sep 15 2006 Ville Skytta <ville.skytta at iki.fi> - 20060719-2
- Rebuild.

* Thu Jul 20 2006 Ville Skytta <ville.skytta at iki.fi> - 20060719-1
- 20060719.
- Fix order of options to find(1) in %%install.

* Thu Jun 15 2006 Ville Skytta <ville.skytta at iki.fi> - 20060614-1
- 20060614, specfile cleanups, include examples in docs.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Thu Dec 16 2004 Ville Skytta <ville.skytta at iki.fi> - 0:20031021-1
- Sync with fedora-rpmdevtools' Perl spec template to fix x86_64 build.
- Move version to the version field.

* Wed Oct 22 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20031021
- Update to 20031021.

* Sat Oct 11 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20030726
- Install into vendor dirs.
- Spec cleanups.

* Tue Jul 29 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20030726
- Update to 20030726.
- Use fedora-rpm-helper.

* Mon Jun 23 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20021130
- Address issues in #194:
- Patch to get rid of a warning on startup.
- Do defattr before doc.

* Fri May 30 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20021130
- Fix release naming scheme (this is snapshot-only).

* Wed May  7 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.0.2.20021130
- Own dirs.
- Save .spec in UTF-8.

* Mon Apr 21 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.0.1.20021130
- First Fedora release, based on Simon Perreault's work.

* Mon Mar 10 2003 Simon Perreault <nomis80@nomis80.org> 20021130-2
- Changed architecture from i386 to noarch
- Added my name as packager
- Bumped up release number, which was forgotten by Anthony Rumble

* Sun Mar 09 2003 Anthony Rumble <anthony@linuxhelp.com.au>
- Tidied up RPM Source

* Sun Dec  1 2002 Simon Perreault <nomis80@linuxquebec.com>
- Update to 20021130

* Sat Nov  9 2002 Simon Perreault <nomis80@linuxquebec.com>
- Update to 20021106

* Mon Sep 23 2002 Simon Perreault <nomis80@linuxquebec.com>
- Update to 20020922

* Wed Aug 28 2002 Simon Perreault <nomis80@linuxquebec.com>
- Update to 20020826

* Tue May 7 2002 Simon Perreault <nomis80@linuxquebec.com>
- Require 5.6.1 because Tidy.pm is placed in a directory dependant on perl
  version.

* Sat Apr 27 2002 Simon Perreault <nomis80@linuxquebec.com>
- Update to 20020425.

* Wed Apr 17 2002 Simon Perreault <nomis80@linuxquebec.com>
- Generalized spec file. Added some documentation.

* Wed Apr 17 2002 Simon Perreault <nomis80@linuxquebec.com>
- Upgraded to version 20020416

* Mon Feb 25 2002 Simon Perreault <nomis80@linuxquebec.com>
- Spec file was created on release of 20020225
