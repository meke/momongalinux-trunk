%global momorel 17

%global lv_priority 80
%global majorver 4
%global minorver 51
%global tarballver %{majorver}%{minorver}
%global ver %{majorver}.%{minorver}

Summary: lv - a Powerful Multilingual File Viewer
Name: lv
Version: %{ver}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
Source: http://www.ff.iij4u.or.jp/~nrt/freeware/lv%{tarballver}.tar.gz
NoSource: 0
Patch0: lv451-lzma-xz.patch
Patch1: lv-4.49.4-nonstrip.patch
Patch2: lv-4.51-162372.patch
Patch3: lv-+num-option.patch
Patch4: lv-fastio.patch
Patch5: lv-lfs.patch

Url: http://www.ff.iij4u.or.jp/~nrt/lv/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel >= ncurses-5.6-11m
Requires(post): chkconfig  >= 1.3.6-1m
Requires(postun): chkconfig  >= 1.3.6-1m
Provides: pager

%description
a Powerful Multilingual File Viewer.

%prep
%setup -q -n lv%{tarballver}
%patch0 -p1 -b .lzma-xz~
%patch1 -p1 -b .nonstrip~
%patch2 -p1 -b .162372~
%patch3 -p1 -b .num~
%patch4 -p1 -b .fastio~
%patch5 -p1 -b .lfs~

%build
cd src
autoconf
./configure --prefix=%{_prefix} --libdir=%{_libdir}
%make

%install
(
  cd src
  rm -rf %{buildroot}
  mkdir -p %{buildroot}%{_bindir}
  mkdir -p %{buildroot}%{_mandir}/man1
  make prefix=%{buildroot}%{_prefix} mandir=%{buildroot}%{_mandir} libdir=%{buildroot}%{_libdir} install
)

cat << EOF > %{buildroot}/%{_libdir}/lv/lv-pager
#!/bin/sh -
LV=\${LV_PAGER:-'-c'} exec %{_bindir}/lv "\$@"
EOF
chmod 0755 %{buildroot}/%{_libdir}/lv/lv-pager

%files
%defattr(-, root, root)
%doc GPL.txt README build hello.sample hello.sample.gif index.html
%doc lv.1 lv.hlp relnote.html
%{_bindir}/lv
%{_bindir}/lgrep
%{_mandir}/man1/lv.1*
%{_libdir}/lv

%clean
rm -rf %{buildroot}

%post
%{_sbindir}/alternatives --install /usr/bin/pager pager %{_libdir}/lv/lv-pager %{lv_priority}

%postun
[ -e %{_libdir}/lv/lv-pager ] || %{_sbindir}/alternatives --remove pager %{_libdir}/lv/lv-pager

%changelog
* Mon Nov 25 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.51-17m)
- import 5 patches from fedora
-- improve large file processing performance up to 300%
-- improve large file support in 32bit arch.
-- add +num and +/pattern
-- fix debuginfo package
-- minor bug fixes

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.51-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.51-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.51-14m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.51-13m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.51-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.51-11m)
- support xz (Patch0)
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.51-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.51-9m)
- rebuild against gcc43

* Sun Mar  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.51-8m)
- use ncurses

* Thu Feb 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.51-7m)
- add lzma patch

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.51-6m)
- %%NoSource -> NoSource

* Sat Dec  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.51-5m)
- fixed alternatives

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.51-4m)
- revised alternatives

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (4.51-3m)
- rebuild against libtermcap-2.0.8-38m.

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.51-2m)
- enable x86_64.

* Wed Apr 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.51-1m)
- update to 4.51
- delete included Patch0: lv-rh.patch

* Mon Nov 17 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.50-2m)
- fix Source0 URI
- fix setup directory name
- change smf_mflags at make

* Sat Nov 15 2003 kourin <kourin@fh.freeserve.ne.jp>
- (4.50-1m)
- update to 4.50

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.49.5-3m)
- fix error in %%post and %%preun

* Thu May 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.49.5-2m)
- update Source0
- use more rpm macros

* Wed May 14 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.49.5-1m)
- fix the post and postun script for alternatives

* Sat Jan 25 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.49.4-6m)
- add %{_libdir}/lv/lv-pager for pager
   define LV_PAGER if you'd like to override the default options for lv
- revice %%post and %%postun

* Fri Nov 29 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.49.4-6m)
- add version at requires chkconfig for alternatives

* Tue Nov 19 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (4.49.4-5m)
- adopt the alternatives system (requires chkconfig, provides pager).

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Jan  4 2000 Tenkou N. Hattori <tnh@kondara.org>
- version up to 4.49.3.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Sep 19 1999 Tenkou N. Hattori <tnh@aurora.dti.ne.jp>
- first release.
