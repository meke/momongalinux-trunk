%global momorel 5

Summary: apache module that provides ASP.NET functionality
Name: mod_mono
Version: 2.10
Release: %{momorel}m%{?dist}
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: mod_mono-2.10-httpd24.patch
License: BSD
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: mono-devel >= 2.10
BuildRequires: httpd-devel >= 2.4.3
Requires: mono-core >= 2.10
Requires: httpd

%description
mod_mono is an apache module that provides ASP.NET functionality.

%prep
%setup -q
%patch0 -p1 -b .httpd24

%build
%configure --program-prefix="" \
    --with-apxs=%{_httpd_apxs} \
    --with-apr-config=%{_bindir}/apr-1-config
rm -f libtool && ln -s /usr/bin/libtool .
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}/%{_sysconfdir}/httpd/conf.d/
mv %{buildroot}/%{_sysconfdir}/httpd/conf/mod_mono.conf \
    %{buildroot}/%{_sysconfdir}/httpd/conf.d/mod_mono.conf.dist

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README
%{_sysconfdir}/httpd/conf.d/mod_mono.conf.dist
%{_libdir}/httpd/modules/mod_mono.so.0.0.0
%{_libdir}/httpd/modules/mod_mono.so
%{_mandir}/man8/mod_mono.8.*

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-5m)
- rebuild against httpd-2.4.3

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-4m)
- change Source0 URI

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-3m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.3-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-2m)
- use BuildRequires

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Fri Dec 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Tue May 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2-2m)
- support libtool-2.2.x

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.4

* Sun Feb 22 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2-3m)
- remove --enable-gcov option

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2
- add SU.PLEASE

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- change source url (NoSource)

* Thu Aug 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update 2.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-2m)
- rebuild against gcc43

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sun Sep  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Wed May 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-2m)
- delete patch0 and delete autoreconf

* Fri Nov  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Tue Oct 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.18-1m)
- update to 1.1.18

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17

* Mon Aug 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.16.1-1m)
- update to 1.1.16.1

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.14-5m)
- rebuild against mono-core

* Mon Jun  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.14-4m)
- add patch1 (Restarting a killed mod-mono-server)

* Fri May  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.14-3m)
- add patch0 for apache 2.2
-- configure.in check version 1.3 or 2.2 :-( 

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.14-2m)
- rebuild mono -> mono-core

* Mon Apr 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.14-1m)
- update to 1.1.14

* Sun Apr 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13-3m)
- rename mod_mono.conf -> mod_mono.conf.dist (please edit and rename this file)

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13-2m)
- mv httpd/conf/* http/conf.d/*

* Sun Jan 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13-1m)
- start.
