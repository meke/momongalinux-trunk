%global momorel 18

%define samplesname 	dxsamples
%define sver	4.4.0
%define dxdir	%{_libdir}/dx
%define iconsdir %{_datadir}/pixmaps
%define kdeiconsdir %{_datadir}/icons/hicolor
%define dxbuilddir %{_builddir}/dx-%{version}

Name:		opendx
Summary:	OpenDX - Opensourced IBM Data Explorer

# include local configuration
%{?include_specopt}

%{?!with_HDF:          %define with_HDF       1}
%{?!with_netcdf:       %define with_netcdf    1}
%{?!with_cdf:          %define with_cdf       0}
%{?!with_java:          %define with_java       0}

Version:	4.4.4
Release:	%{momorel}m%{?dist}
URL:		http://www.opendx.org/
Group:		Applications/Engineering
License:	"IBM Public License"
Source:		http://opendx.npaci.edu/source/dx-%{version}.tar.gz
Source1:	http://opendx.npaci.edu/source/%{samplesname}-%{sver}.tar.gz
Source2:	icons-dx.tar.bz2
Patch5:		dx-4.2.0-xkb.patch
Patch6:		dx-4.3.2-types.patch
# patches from [opendx2-devel:04618] http://www.mail-archive.com/opendx2-dev@lists.berlios.de/msg04618.html
Patch21: dx-make-jN.patch
Patch22: dx-sys_h.patch
Patch23: dx-typo.patch
Patch24: dx-4.4.4-browser.patch
Patch25: dx-gcc43.patch
Patch26: dx-4.4.4-libtool-2.2.6.patch
BuildRequires:	autoconf bison flex freetype-devel openmotif-devel
BuildRequires:	ImageMagick >= 6.4.2.1
BuildRequires:	libjpeg-devel libpng-devel ghostscript-devel
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  desktop-file-utils
BuildRequires:  libGL-devel
BuildRequires:  libGLU-devel
BuildRequires:  libtool
BuildRequires:  libXinerama-devel
BuildRequires:  libXpm-devel
BuildRequires:  openssh-clients
Requires:       openssh-clients
Requires:	openmotif
%if %{with_HDF}
Requires:       hdf
BuildRequires:  hdf
%endif
%if %{with_netcdf}
Requires:       netcdf 
BuildRequires:  netcdf >= 4.1.2
%endif
%if %{with_cdf}
Requires:       cdf
BuildRequires:  cdf
%endif
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       dx
Provides:       dx-libs

%description
OpenDX is a uniquely powerful, full-featured software package for the
visualization of scientific, engineering and analytical data: Its open
system design is built on a standard interface environments. And its
sophisticated data model provides users with great flexibility in
creating visualizations.

With OpenDX, you can create the visualizations you want to create. OpenDX has
been designed to be the place where the art of science and the science of
visualization come together. It's the place where they're combined into one
powerful, flexible framework that lets you "Simply Visualize."

%package devel
Summary:	Development libraries for OpenDX
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Provides: dx-devel

%description devel
This package contains the header files and includes necessary to for developing
applications with OpenDX.

%package samples
Summary:	Sample data files for OpenDX
Group:		Applications/Engineering
Requires:	%{name} = %{version}-%{release}
Provides: dx-samples

%description samples
This package contains the sample data files for OpenDX. This file is 
needed for a builtin tutorial.


%prep
%setup -q -n dx-%{version} -a 1

%patch5 -p1 -b .xkb
%patch6 -p1 -b .types
%patch21 -p1 -b .make_jN
%patch22 -p1 -b .sys_h
%patch23 -p1 -b .typo
%patch24 -p1 -b .browser
%patch25 -p1 -b .gcc43
%patch26 -p1 -b .libtool-2.2.6

## libMagick.so must be linked, but -lMagick option is omitted in Makefile.
for i in `find . -type f -name Makefile.in`
do
  perl -p -i -e "s/^LIBS = \@LIBS\@/LIBS = \@LIBS\@ -lMagick/g" $i
done

## FIX ME!
## Somehow JAVA(jni.h), HDF(dshdf.h) and ImageMagick(magick.h) header files 
## are not recognized in configure script.
## So it is needed to link these headers to %{dxbuilddir}/include/

%if %{with_java}
## Java headers (java-sun-j2se-devel)
ln -sf /usr/lib/java/sun-j2se1.5-sdk//include/* include/
ln -sf /usr/lib/java/sun-j2se1.5-sdk//include/linux include/
%endif

%if 0
## ImageMagick headers (ImageMagick-devel)
## if ImageMagick support is not needed, comment following lines.
ln -s /usr/include/X11/magick include/magick
## HDF headers (HDF)
## if HDF support is not needed, comment following lines.
#%%if %{with_HDF}
for i in `rpm -ql hdf | grep "/usr/include"`; \
do ln -s $i include/${i##/usr/include/}; done
#%%endif
%endif

%build
CFLAGS="-O1 -fno-fast-math -fno-exceptions -fPIC" CXXFLAGS="-O1 -fno-fast-math -fno-exceptions -Wno-deprecated -fPIC" \

%if %{with_HDF}
WITHHDF="--with-hdf"
%endif
%if %{with_netcdf}
WITHNETCDF="--with-netcdf"
%endif
%if %{with_cdf}
WITHCDF="--with-cdf"
%endif
autoreconf -vfi
%configure --prefix=%{_libdir} \
           --with-x \
           --with-magick $WITHNDF $WITHNETCDF $WITHCDF \
           --with-jbig \
           --with-rsh=%{_bindir}/ssh \
%if %{with_java}
           --with-javadx \
	   --with-jni-path=%{dxbuilddir}/include:%{dxbuilddir}/include/linux
%else
           --without-javadx
%endif
## FIX ME!
## These flags could be used if Java JDK is installed
## but Sun J2SE 1.4 causes compilation error because of 
## lacking vrml.field package which seems to be included 
## in Java3D in JDK 1.3.1
# --with-jni-path=%{builddir}/include:%{builddir}/include/linux \
# --with-java40jar-path=/usr/lib/netscape/java/classes/java40.jar

## For ImageMagick support, include/dxconfig.h which is created
## by configure must be modified

%if 0
cp include/dxconfig.h include/dxconfig.h.pre
sed 's/\/\* \#undef HAVE_LIBMAGICK \*\//\#define HAVE_LIBMAGICK 1/g' include/dxconfig.h.pre > include/dxconfig.h
%endif

%make

(cd %{samplesname}-%{sver}
%configure --prefix=%{_libdir}
make)

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir} \
	%{buildroot}%{_includedir}
%makeinstall prefix=%{buildroot}%{_libdir} \
	libdir=%{buildroot}%{dxdir} \
	mandir=%{buildroot}%{_mandir}/man1
( cd %{buildroot}/%{_includedir}
ln -sf ../../%{dxdir}/include/dxconfig.h dxconfig.h
ln -sf ../../%{dxdir}/include/dxl.h dxl.h
ln -sf ../../%{dxdir}/include/dx dx
)
( cd %{buildroot}/%{_libdir}
ln -sf ../../%{dxdir}/lib_linux/libDX.a  libDX.a
ln -sf ../../%{dxdir}/lib_linux/libDXcallm.a libDXcallm.a
ln -sf ../../%{dxdir}/lib_linux/libDXL.a  libDXL.a
ln -sf ../../%{dxdir}/lib_linux/libDXlite.a libDXlite.a
)
## this link is needed
(cd %{buildroot}%{dxdir}/bin_linux
ln -sf ../../../bin/dxexec dxexec
)

rm -rf %{buildroot}%{dxdir}/man
#
(cd %{buildroot}/%{dxdir}/html
ln -sf allguide.htm index.htm
ln -sf allguide.htm index.html
)
#
(cd %{samplesname}-%{sver}
make install prefix=%{buildroot}%{_libdir}
)

mkdir -p %{buildroot}%{dxdir}/lib
install -m 644 ./lib/mdf2c.awk %{buildroot}%{dxdir}/lib/

## icons
## GNOME /usr/share/pixmaps/*
## KDE /usr/share/icons/hicolor/16x16/apps/*
## /usr/share/icons/hicolor/32x32/apps/*
## /usr/share/icons/hicolor/48x48/apps/*
mkdir -p %{buildroot}%{iconsdir} \
         %{buildroot}%{kdeiconsdir}/{16x16,32x32,48x48}/apps/
tar xjf %{SOURCE2} -C %{buildroot}%{iconsdir}
mv %{buildroot}%{iconsdir}/large/* %{buildroot}%{kdeiconsdir}/48x48/apps/
mv %{buildroot}%{iconsdir}/mini/* %{buildroot}%{kdeiconsdir}/16x16/apps/
cp %{buildroot}%{iconsdir}/dx.png %{buildroot}%{kdeiconsdir}/32x32/apps/

## menu
cat > %{name}.desktop <<EOF
[Desktop Entry]
Name=OpenDX
Name[ja]=OpenDX
GenericName=OpenDX Data Explorer
Comment=powerful scientific visualization tool
Exec=/usr/bin/dx
Icon=dx.png
Terminal=false
Type=Application
EOF
# install desktop file
%__mkdir_p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --add-category Engineering \
  --add-category Science \
    ./%{name}.desktop

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post

%postun

%files
%defattr(-,root,root)
%doc AUTHORS LICENSE README ChangeLog NEWS INSTALL COPYING
%{_bindir}/*
%{_mandir}/*/*
%dir %{dxdir}
%{dxdir}/bin
%{dxdir}/bin_linux
%{dxdir}/doc
%{dxdir}/fonts
%{dxdir}/help
%{dxdir}/html
%dir %{dxdir}/lib
%{dxdir}/lib/colors.txt
%{dxdir}/lib/dx*
%{dxdir}/lib/messages
%{dxdir}/lib/outboard.c
#%{dxdir}/samples
%{dxdir}/ui
%{dxdir}/java
%{iconsdir}/dx.*
%{kdeiconsdir}/16x16/apps/dx.*
%{kdeiconsdir}/32x32/apps/dx.*
%{kdeiconsdir}/48x48/apps/dx.*
#%{_liconsdir}/dx.*
#%{_miconsdir}/dx.*
%{_datadir}/applications/*

%files samples
%defattr(-,root,root)
%{dxdir}/samples

%files devel
%defattr(-,root,root)
%attr(644,root,root) %{_libdir}/*.a
%{_includedir}/*
%{dxdir}/include
%{dxdir}/lib_linux
%{dxdir}/lib/mdf2c.awk

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-18m)
- add source(s)

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-17m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.4-16m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-15m)
- rebuild against netcdf-4.1.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.4-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.4-13m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.4.4-12m)
- build fix with desktop-file-utils-0.16

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.4-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-9m)
- add -fPIC, so that enable to build q on x86_64

* Mon Jun  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.4.4-8m)
- add patch26 (for libtool-2.2.6) and autoreconf

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-7m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-6m)
- drop Patch4 for fuzz=0, probably not needed
- update Patch22,24 for fuzz=0

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-5m)
- rebuild against ImageMagick-6.4.2.1

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.4-4m)
- remove non ASCII char

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.4-3m)
- rebuild against gcc43

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-2m)
- modify %%files to avoid conflicting

* Fri Mar 07 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.4.4-1m)
- import to Momonga

* Mon Nov 13 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.4.4-0.0.2m)
- add patches (based on the patch in Fedora Extra dx-4.4.4-2.src.rpm)
- (http://www.mail-archive.com/opendx2-dev@lists.berlios.de/msg04618.html)
- - dx-make-jN.patch: fixes make -jN builds (missing dependency and three rules using/overwriting/deleting the same temporary files)
- - dx-sys_h.patch: <linux/sys.h> is a kernel-space header and shouldn't be used in normal programs. It is also no longer included in current userspace kernel headers package in Fedora. OpenDX build fine without it so I assume it's not even necessary.
- - dx-typo.patch: fixes a typo which prevented the Help from being loaded.
- - dx-4.4.4-browser.patch: use firefox instead of netscape

* Fri Sep 29 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.4.4-0.0.1m)
- update to 4.4.4

* Tue Aug 02 2005 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.3.2-0.0.3m)
- import Patch6 from Mandrake Cooker (OpenDX-4.3.2-10mdk)

* Tue Aug 02 2005 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.3.2-0.0.2m)
- revised for rpm4.2
- use desktop-file-utils

* Mon Dec 22 2003 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.3.2-0.0.1m)
- update to 4.3.2

* Wed Aug  5 2003 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.2.0-0.0.3m)
- sync with OpenDX-4.2.0-8mdk (Patch6 for gcc3.3)
- rebuild against glibc-2.3

* Fri Apr 4 2003 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.2.0-0.0.2m)
- rebuild agains XFree86 4.3.0
- patch2,3 (for ?) and patch4,5 (for recent XFree86 and glibc) from Mandrake CVS web (http://cvs.mandrakesoft.com/cgi-bin/cvsweb.cgi/SPECS/OpenDX)
- now support for HDF,netcdf,cdr can be controlled by /etc/rpm/specopt/OpenDX.specopt

* Sat Jan 18 2003 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.2.0-0.0.1m)
- build for Momonga Linux
- Patch1 from Mandrake (OpenDX-4.2.0-5mdk) to avoid compile error by using gcc3.2

* Thu Sep 19 2002 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.2.0-0.2m)
- build against openmotif 2.2

* Sat Jun 29 2002 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (4.2.0-0.1kl)
- first build for Kondara MNU/Linux 2.1
- modified the spec file from Mandrake
- HDF, NetCDF, CDF support is included
- Java support is not included because Java3D API errors with J2SE 1.4

* Mon Jun  3 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 4.1.3-8mdk
- Use %%make
- Patch1: ISO C++ fixes
- Patch0: Don't include /usr/include in include search path

* Thu Feb 21 2002 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.3-7mdk
- rebuilt.

* Sat Feb 02 2002 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.3-6mdk
- rebuilt against HDF 4.1r5.

* Thu Jan 24 2002 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.3-5mdk
- fixed icons (png).

* Mon Oct 15 2001 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.3-4mdk
- Rebuilt against latest libpng.

* Thu Sep 06 2001 Stefan van der Eijk <stefan@eijk.nu> 4.1.3-3mdk
- BuildRequires:	flex
- Removed BuildRequires:	zlib-devel

* Wed Aug 01 2001 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.3-2mdk
- added -fno-exceptions to compilation flags (thanks to Randall Hopper).
- forced -O2 due to gcc bug.

* Tue Jun 12 2001 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.3-1mdk
- updated to version 4.1.3.
- cleaned SPEC file.

* Thu Mar 22 2001 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.0-7mdk
- run aclocal/autoconf only for non ix86 architectures.
- added menu entry.

* Sat Mar 17 2001 David BAUDENS <baudens@mandrakesoft.com> 4.1.0-6mdk
- Don't BuildRequires HDF on PPC (HDF doesn't support PPC architecture)
- Use optimizations on PPC

* Fri Mar 16 2001 Jeff Garzik <jgarzik@mandrakesoft.com> 4.1.0-5mdk
- Prefer arch "linux" to arch "alphax", for alpha.
- Re-generate configure with autoconf for each build.

* Mon Feb 26 2001 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.0-4mdk
- add netcdf support.

* Sat Feb 24 2001 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.0-3mdk
- added samples.
- added patches for allowing static building (thanks jloup).
- added mandirs.
- fixed docs.
- split into main and devel package.

* Tue Dec 26 2000 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.0-2mdk
- added netcdf, hdf, cdf support.

* Mon Oct 09 2000 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.0-1mdk
- more macros.
- added patch for ImageMagick.

* Sat Oct 07 2000 Giuseppe Ghibo <ghibo@mandrakesoft.com> 4.1.0-0.9mdk
- initial release.
