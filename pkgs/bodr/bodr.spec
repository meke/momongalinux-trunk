%global momorel 4

Name:           bodr
Version:        9
Release:        %{momorel}m%{?dist}
Summary:        Blue Obelisk Data Repository

Group:          System Environment/Libraries
License:        MIT
URL:            http://www.blueobelisk.org
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2 
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  libxml2
BuildRequires:  libxslt
Requires:       pkgconfig  

%description
The Blue Obelisk Data Repository lists many important chemoinformatics data
such as element and isotope properties, atomic radii, etc. including
references to original literature. Developers can use this repository to make
their software interoperable.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildrot}
make install DESTDIR=%{buildroot}
mv %{buildroot}%{_docdir}/bodr DOC

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc DOC/* NEWS TODO
%{_datadir}/bodr
%{_datadir}/pkgconfig/bodr.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9-1m)
- update to 9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8-2m)
- rebuild against rpm-4.6

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8-1m)
- update to 8

* Sat Apr  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-4m)
- fix %%clean

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6-2m)
- %%NoSource -> NoSource

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6-1m)
- import from Fedora

* Sat Apr 21 2007 Julian Sikorski <belegdol[at]gmail[dot]com> - 6-1
- Initial RPM release
