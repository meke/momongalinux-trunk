%global momorel 1

Summary: Graphical music notation program
Name: denemo
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-1.0.0.patch
URL: http://www.denemo.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: portaudio-devel 
BuildRequires: portmidi-devel
BuildRequires: aubio-devel 
BuildRequires: gtk2-devel 
BuildRequires: guile-devel >= 2.0.9
BuildRequires: gettext 
BuildRequires: libxml2-devel 
BuildRequires: fftw-devel 
BuildRequires: desktop-file-utils
BuildRequires: libtool-ltdl-devel 
BuildRequires: jack-devel 
BuildRequires: fontpackages-devel 
BuildRequires: lash-devel 
BuildRequires: libsamplerate-devel
BuildRequires: gtksourceview2-devel 
BuildRequires: fluidsynth-devel >= 1.1.5
BuildRequires: chrpath

Requires: TiMidity++
Requires: denemo-music-fonts

%package music-fonts
Summary:	Music notation fonts used in Denemo
Group:		Applications/Multimedia
License:	OFL
BuildArch:	noarch
Requires:	fontpackages-filesystem

%description
Denemo is a graphical music notation program written in C with
gtk+. As of April 2000, it is an official part of the GNU project
(http://www.gnu.org/)

It is intended to be used in conjunction with GNU Lilypond
(http://www.cs.uu.nl/hanwen/lilypond/), but is adaptable to other
computer-music-related purposes as well. 

%description music-fonts
This font set witten by Robert Allgeyer is the 2001 legacy version
of MusiSync. It is a musical notations font set which was reordered 
by Richard Shann for Denemo. 

This font set is used by Denemo.

%prep
%setup -q
%patch0 -p1

%build
%{configure} \
    --disable-static \
    --enable-jack \
    --enable-lash \
    --enable-doc \
    --with-included-smf \
    CPPFLAGS=-DGLIB_COMPILATION
%{make}
chrpath -d src/denemo

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
desktop-file-install --vendor="" \
	--dir=%{buildroot}/%{_datadir}/applications \
	--add-category=X-Notation \
	%{buildroot}/%{_datadir}/applications/denemo.desktop
%find_lang %{name}
install -m 0755 -d %{buildroot}%{_fontdir}
rm -f %{buildroot}/%{_bindir}/denemo-lilypond.bat

install -m 0644 -p fonts/*.ttf %{buildroot}%{_fontdir}
rm -rf %{buildroot}/%{_datadir}/fonts/truetype
rm -rf %{buildroot}/%{_includedir}

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%dir %{_sysconfdir}/denemo
%config(noreplace) %{_sysconfdir}/denemo/denemo.conf
%{_datadir}/denemo/
%{_datadir}/pixmaps/denemo.png
%{_datadir}/applications/denemo.desktop
%{_bindir}/denemo
%{_bindir}/denemo_file_update.sh

%_font_pkg -n music *.ttf

%changelog
* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- rebuild against guile-2.0.9

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-2m)
- remove Requires: bug-buddy

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2
- rebuild against fluidsynth-1.1.5

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-2m)
- build fix

* Mon Aug  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.20-4m)
- rebuild against fluidsynth-1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.20-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.20-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.20-1m)
- update 0.8.20

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.18-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.18-1m)
- update to 0.8.18

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.16-1m)
- update to 0.8.16

* Mon Jan 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.12-1m)
- import from Fedora and update to 0.8.12

* Wed Sep 23 2009 Orcan Ogetbil <oget[DOT]fedora[AT]gmail[DOT]com> - 0.8.4-3
- Update desktop file according to F-12 FedoraStudio feature

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat May 30 2009 Roy Rankin <rrankin@ihug.com.au> - 0.8.4-1
-Update for Denemo release 0.8.4 
  fix fedora bugzilla 499692
  new features
	custom prolog
	tagged directives
	print preview pane
	genereal edit object action
	background printing

* Fri Feb 27 2009 Roy Rankin <rrankin@ihug.com.au> - 0.8.2-3
- font subpackage needs same version as main package, use noarch
 
* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 03 2009 Roy Rankin <rrankin@ihug.com.au> - 0.8.2-1
-Update for Denemo release 0.8.2
	improve MIDI input
	more scripting support
	better menu organization
-Spec file fix unowned directories (Bugzilla 483337)

* Sat Jan 10 2009 Roy Rankin <rrankin@ihug.com.au> - 0.8.0-2
-Font split into seperate RPM package (Bugzilla 477375)

* Sun Nov 30 2008 Roy Rankin <rrankin@ihug.com.au> - 0.8.0-1
-Update for Denemo release 0.8.0

* Wed Sep 03 2008 Roy Rankin <rrankin@ihug.com.au> - 0.7.9-5
-Add Patches assert undo crash, un-needed messages on start up
* Mon Aug 18 2008 Roy Rankin <rrankin@ihug.com.au> 0.7.9-4
-Simplify Requires
* Sat Aug 16 2008 Roy Rankin <rrankin@ihug.com.au> 0.7.9-3
-Remove defines and fixup BuildRoot path
* Sun Aug  3 2008 Roy Rankin <rrankin@ihug.com.au> 0.7.9-2
-Additional BuildRequires from Mock testing, 
 fixed desktop-file-install usage
* Fri Jul 25 2008 Roy Rankin <rrankin@ihug.com.au> 0.7.9-1
-Update for 0.7.9 and Fedora 8
* Fri Dec 14 2001 Adam Tee <ajtee@ajtee.uklinux.net>
-Update for 0.5.8
* Sun Nov 12 2000 Matt Hiller <mhiller@pacbell.net>
- Update for 0.5.5
* Wed Jun 21 2000 Sourav K. Mandal <smandal@mit.edu>
- Initial release of RPM package
