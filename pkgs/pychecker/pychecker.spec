%global momorel 14
%global pythonver 2.7
%define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary:  A python source code checking tool.
Name: pychecker
Version: 0.8.17
Release: %{momorel}m%{?dist}
URL: http://pychecker.sourceforge.net
Source0: http://dl.sourceforge.net/pychecker/pychecker-%{version}.tar.gz
Patch0: pychecker-0.8.17-root.patch
License: BSD
Group: Development/Tools
Requires: python >= %{pythonver}
BuildRequires: python  >= %{pythonver}
BuildRequires: python-devel >= %{pythonver}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
PyChecker is a python source code checking tool to help you find
common bugs. It is meant to find problems that are typically caught by
a compiler.

%prep
%setup -q
%patch0 -p1 -b .root

%build
CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc CHANGELOG COPYRIGHT KNOWN_BUGS MAINTAINERS README TODO
%{_bindir}/pychecker
%{python_sitelib}/PyChecker-*.egg-info
%{python_sitelib}/pychecker
%exclude %{python_sitelib}/pychecker/CHANGELOG
%exclude %{python_sitelib}/pychecker/COPYRIGHT
%exclude %{python_sitelib}/pychecker/KNOWN_BUGS
%exclude %{python_sitelib}/pychecker/MAINTAINERS
%exclude %{python_sitelib}/pychecker/README
%exclude %{python_sitelib}/pychecker/TODO
%exclude %{python_sitelib}/pychecker/VERSION

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.17-14m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.17-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.17-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.17-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.17-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.17-9m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.17-8m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.17-7m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.17-6m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.17-5m)
- rebuild against gcc43

* Sun Mar 11 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.8.17-4m)
- add python libdir macro and modify python libdir

* Sun Feb 25 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.17-3m)
- modify libdir

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.17-2m)
- rebuild against python-2.5

* Sun Jul 16 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.17-1m)
- import from fc

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.8.17-1.1
- rebuild

* Sat Feb  4 2006 Miloslav Trmac <mitr@redhat.com> - 0.8.17-1
- Update to pychecker-0.8.17

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Sep 12 2005 Miloslav Trmac <mitr@redhat.com> - 0.8.16-1
- Update to pychecker-0.8.16
- Don't ship VERSION
- Small spec file cleanups

* Tue Jun 28 2005 Miloslav Trmac <mitr@redhat.com> - 0.8.14-5
- Don't ship documentation in the python*/site-packages
- Rely on redhat-rpm-config for .py[co] generation
- Backport a fix for spurious warnings about divisions
- Disable warning about mismatch of implicit and explicit returns, it has too
  many false positives with Python 2.4
- Fix handling of functions with no known return value
- Fix handling of tuple literals

* Thu Jun  9 2005 Miloslav Trmac <mitr@redhat.com> - 0.8.14-4
- Backport a fix for spurious warnings about "is{, not} None"

* Fri Feb 18 2005 Miloslav Trmac <mitr@redhat.com> - 0.8.14-3
- Fix build failure on lib64 platforms

* Mon Nov  8 2004 Jeremy Katz <katzj@redhat.com> - 0.8.14-2
- rebuild against python 2.4

* Sun Jul 18 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- 0.8.14

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Feb 23 2004 Jeremy Katz <katzj@redhat.com> - 0.8.13-3
- rebuild

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Dec 11 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- 0.8.13 release

* Thu Nov  6 2003 Jeremy Katz <katzj@redhat.com> 0.8.12-2
- rebuild for python 2.3

* Sun May 04 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- 0.8.12

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 0.8.11-3
- lib64'ize

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Jun  6 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.8.11-1
- 0.8.11

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Mar 21 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.8.10-1
- 0.8.10, bugfix release

* Tue Feb 26 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.8.9-2
- Rebuild

* Mon Feb  4 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.8.9-1
- 0.8.9

* Mon Jan 14 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.8.8-1
- 0.8.8

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun Jan  6 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.8.7-1
- 0.8.7
- Include optimized bytecode
- Use the pychecker script from pychecker package

* Fri Nov 16 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.8.6-1
- 0.8.6

* Wed Oct 17 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.8.5-1
- 0.8.5

* Mon Sep 24 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.8.4-1
- 0.8.4

* Sat Sep 15 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.8.3-1
- 0.8.3
- build for python 2.2

* Sun Jul 22 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.7.5

* Fri Jul 20 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add pyhon-devel to buildrequires (#49567)

* Thu Jul 19 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.7

* Thu Jul  5 2001 Matt Wilson <msw@redhat.com>
- removed glob from /usr/bin/*, changed it to /usr/bin/pychecker, as that is the
  only file not listed in installed_files

* Thu Jul  5 2001 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Mon Jul  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.6.1

* Wed May 30 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.5
- change the file location in the pychecker wrapper
- Use distutils to build and install

* Tue Apr 24 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Initial build.


