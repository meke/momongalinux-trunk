%global momorel 4

Summary: Library for using OBEX
Name: openobex
Version: 1.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://dev.zuckschwerdt.org/openobex/
Source0: http://downloads.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.0-Source.zip
NoSource: 0
Patch0: openobex-apps-flush.patch
Patch1: openobex-1.3-push.patch
Patch2: openobex-1.3-autoconf.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf >= 2.57, docbook-utils >= 0.6.13, bluez-libs-devel >= 4.0, sed, libusb-devel
BuildRequires: automake autoconf libtool
ExcludeArch: s390 s390x

%description
Open OBEX shared c-library

%package devel
Summary: Files for development of applications which will use OBEX
Group: Development/Libraries
Requires: %{name} = %{version}-%{release} bluez-libs-devel libusb-devel pkgconfig

%description devel
Open OBEX shared c-library

%package apps
Summary: Applications for using OBEX
Group: System Environment/Libraries
BuildRequires: bluez-libs-devel
BuildRequires: autoconf >= 2.57
Excludearch: s390x s390

%description apps
Open OBEX Applications

%prep
rm -rf $RPM_BUILD_ROOT

%setup -q -n %{name}-%{version}.0-Source
%patch0 -p1 -b .flush
%patch1 -p1 -b .push
%patch2 -p1 -b .autoconf
autoreconf --install --force

%build
autoreconf --install --force
%configure --disable-static --enable-apps --enable-usb --disable-dependency-tracking
%make
make -C doc 

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# we do not want .la files
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%{_libdir}/libopenobex*.so.*

%files devel
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README 
%{_libdir}/libopenobex*.so
%dir %{_includedir}/openobex
%{_includedir}/openobex/*.h
%{_libdir}/pkgconfig/openobex.pc

%files apps
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/irobex_palm3
%{_bindir}/irxfer
%{_bindir}/ircp
%{_bindir}/obex_tcp
%{_bindir}/obex_test
%{_bindir}/obex_push
%{_mandir}/man1/obex_push.1*


%changelog
* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-4m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-2m)
- rebuild for new GCC 4.5

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5-1m)
- update 1.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update 1.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc43

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-1m)
- import from fc to Momonga

* Wed Feb  7 2007 Harald Hoyer <harald@redhat.com> 
- readded obex_push

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.3-3.1
- rebuild

* Tue Jun 27 2006 Harald Hoyer <harald@redhat.com> - 1.3-3
- removed more patches

* Tue Jun 27 2006 Harald Hoyer <harald@redhat.com> - 1.3-2
- added more build requirements
- built now with enable-usb

* Fri Jun 16 2006 Harald Hoyer <harald@redhat.com> - 1.3-1
- version 1.3

* Tue Jun 13 2006 Harald Hoyer <harald@redhat.com> - 1.2-2
- more build requirements

* Tue Jun 13 2006 Harald Hoyer <harald@redhat.com> - 1.2-1
- version 1.2

* Thu Feb 16 2006 Harald Hoyer <harald@redhat.com> 1.1-1
- version 1.1

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-4.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-4.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon May 02 2005 Harald Hoyer <harald@redhat.com> 1.0.1-4
- added `OBEX_ServerAccept' to the exported symbols (bug rh#146353)

* Wed Mar 02 2005 Harald Hoyer <harald@redhat.com> 
- rebuilt

* Wed Feb 09 2005 Harald Hoyer <harald@redhat.com>
- rebuilt

* Mon Sep 13 2004 Harald Hoyer <harald@redhat.de> 1.0.1-1
- version 1.0.1

* Tue Jun 22 2004 Alan Cox <alan@redhat.com>
- removed now unneeded glib requirement

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Apr 19 2004 David Woodhouse <dwmw2@redhat.com> 1.0.0-5
- import for for #121271 from openobex CVS tree

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun  4 2003 Harald Hoyer <harald@redhat.de> 1.0.0-2
- excludeArch s390 s390x

* Wed Jun  4 2003 Harald Hoyer <harald@redhat.de> 1.0.0-1
- redhatified specfile
- bump to version 1.0.0

* Thu May 18 2000 Pontus Fuchs <pontus.fuchs@tactel.se>
- Initial RPM


