%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name: kate
Summary: Advanced Text Editor
Version: %{kdever}
Release: %{momorel}m%{?dist}
# kwrite LGPLv2+
# kate: app LGPLv2, plugins, LGPLv2 and LGPLv2+ and GPLv2+
# ktexteditor: LGPLv2
# katepart: LGPLv2
License: LGPLv2 and LGPLv2+ and GPLv2+
Group: Applications/Editors
URL: https://projects.kde.org/projects/kde/kdebase/kate
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
## upstreamable patches
# https://bugzilla.redhat.com/show_bug.cgi?id=1050944
Patch50: kate-4.12.90-python_library_realpath.patch
BuildRequires: desktop-file-utils
BuildRequires: kdelibs-devel >= %{kdever}
Requires: kdebase-runtime >= %{kdever}
Requires: %{name}-part = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description
%{summary}.

%package devel
Summary:  Development files for %{name}
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel

%description devel
%{summary}.

%package libs
Summary:  Runtime files for %{name}
Group: System Environment/Libraries
# when split occurred
Conflicts: kdesdk-libs < %{version}
Requires: %{name} = %{version}-%{release}

%description libs
%{summary}.

%package part
Summary: Kate kpart plugin
Group: User Interface/Desktops
License: LGPLv2
# when split occurred
Conflicts: kdelibs < %{version}

%description part
%{summary}.

%package -n kwrite
Summary: Text Editor
License: LGPLv2+
Group: Applications/Editors
# when split occurred
Conflicts: kdebase < %{version}
Requires: %{name}-part = %{version}-%{release}
Requires: kdebase-runtime >= %{kdever}

%description -n kwrite
%{summary}.

%package -n pate
Summary: python plugin for kate
Group: User Interface/Desktops
License: LGPLv2
Conflicts: kdebase < %{version}
Requires: kdebase-runtime >= %{kdever}
Requires: %{name}-part = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description -n pate
%{summary}

%prep
%setup -q
%patch50 -p1 -b .python_library_realpath

%build
export CXXFLAGS="%{optflags} -fpermissive"

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang kate --with-kde --without-mo
%find_lang kwrite --with-kde --without-mo

# move devel symlinks (that would otherwise conflict with kdelibs3-devel)
mkdir -p %{buildroot}%{_kde4_libdir}/kde4/devel
pushd %{buildroot}%{_kde4_libdir}
for i in lib*.so
do
  case "$i" in
    libkate*interfaces.so)
      linktarget=`readlink "$i"`
      rm -f "$i"
      ln -sf "../../$linktarget" "kde4/devel/$i"
      ;;
  esac
done
popd

## unpackaged files
# playground artsticomment -devel bits
rm -fv %{buildroot}%{_kde4_includedir}/artisticcomment.h
rm -fv %{buildroot}%{_kde4_libdir}/libacomment.a

%check
for f in %{buildroot}%{_kde4_datadir}/applications/kde4/*.desktop ; do
  desktop-file-validate $f
done

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
update-mime-database %{_kde4_datadir}/mime >& /dev/null

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
update-mime-database %{_kde4_datadir}/mime >& /dev/null
fi

%post part -p /sbin/ldconfig
%postun part -p /sbin/ldconfig

%files -f kate.lang
%doc AUTHORS COPYING.LIB README
%{_kde4_bindir}/kate
%{_kde4_appsdir}/kate
%{_kde4_appsdir}/katexmltools
%{_kde4_appsdir}/kconf_update/kate*.upd
%{_kde4_appsdir}/ktexteditor_*
%exclude %{_kde4_appsdir}/kate/pate
%exclude %{_kde4_appsdir}/kate/plugins/pate
%{_kde4_libdir}/libkdeinit4_kate.so
%{_kde4_libdir}/kde4/kate*plugin.so
%exclude %{_kde4_libdir}/kde4/katepateplugin.so
%{_kde4_libdir}/kde4/katefiletemplates.so
%{_kde4_libdir}/kde4/ktexteditor_*.so
%{_kde4_libdir}/kde4/plasma_applet_katesession.so
%{_kde4_libdir}/kde4/kate_kttsd.so
%{_kde4_configdir}/katerc
%{_kde4_datadir}/applications/kde4/kate.desktop
%{_kde4_datadir}/kde4/services/kate*.desktop
%exclude %{_kde4_datadir}/kde4/services/katepart.desktop
%{_kde4_datadir}/kde4/services/ktexteditor_*.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-katesession.desktop
%{_kde4_datadir}/kde4/servicetypes/kateplugin.desktop
%{_kde4_iconsdir}/hicolor/*/*/*
%{_mandir}/man1/kate.1.*

%files libs
%{_kde4_libdir}/libkateinterfaces.so.4*

%files devel
%{_kde4_includedir}/kate_export.h
%{_kde4_includedir}/kate
%{_kde4_libdir}/kde4/devel/libkateinterfaces.so
%{_kde4_libdir}/kde4/devel/libkatepartinterfaces.so

%files part
%doc part/INDENTATION part/README* part/TODO*
%{_kde4_libdir}/kde4/katepart.so
%{_kde4_libdir}/libkatepartinterfaces.so.4*
%{_kde4_appsdir}/katepart
%{_kde4_configdir}/katemoderc
%{_kde4_configdir}/kateschemarc
%{_kde4_configdir}/katesyntaxhighlightingrc
%{_kde4_configdir}/ktexteditor_codesnippets_core.knsrc
%{_kde4_datadir}/kde4/services/katepart.desktop

%files -n kwrite -f kwrite.lang
%{_kde4_bindir}/kwrite
%{_kde4_libdir}/libkdeinit4_kwrite.so
%{_kde4_appsdir}/kwrite
%{_kde4_datadir}/applications/kde4/kwrite.desktop

%files -n pate
%{python_sitearch}/PyKate4
%{_kde4_appsdir}/kate/pate
%{_kde4_appsdir}/kate/plugins/pate
%{_kde4_libdir}/kde4/katepateplugin.so
%{_kde4_datadir}/kde4/services/pate.desktop
%{_kde4_datadir}/kde4/servicetypes/katepythonplugin.desktop

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Sat Dec 22 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.95-2m)
- fix build on i686

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Fri Oct  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- import From Fedora for KDE 4.7.0

* Tue Jul 26 2011 Jaroslav Reznik <jreznik@redhat.com> 4.7.0-1
- 4.7.0

* Mon Jul 18 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-1
- first try

