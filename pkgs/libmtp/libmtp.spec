%global momorel 1

Summary: A library implementation of the Media Transfer Protocol (MTP)
Name: libmtp
Version: 1.1.6
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://libmtp.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: udev
BuildRequires: doxygen
BuildRequires: libusb-devel
BuildRequires: pkgconfig

%description
libmtp is a LGPL library implementation of the Media Transfer Protocol (MTP), a superset of the Picture Transfer Protocol (PTP).

%package examples
Summary:        Example programs for libmtp
Group:          Applications/Multimedia
Requires:       %{name} = %{version}-%{release}

%description examples
This package provides example programs for communicating with MTP
devices.

%package devel
Summary: Header files and static libraries from libmtp
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libmtp.

%prep
%setup -q

%build
%define _program_prefix mtp-
%configure --disable-static \
	   --with-udev-rules=60-libmtp.rules
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
# Remove libtool archive remnant
rm -f %{buildroot}%{_libdir}/libmtp.la
# Replace links with relative links
rm -f %{buildroot}%{_bindir}/mtp-delfile
rm -f %{buildroot}%{_bindir}/mtp-getfile
rm -f %{buildroot}%{_bindir}/mtp-newfolder
rm -f %{buildroot}%{_bindir}/mtp-sendfile
rm -f %{buildroot}%{_bindir}/mtp-sendtr
pushd %{buildroot}%{_bindir}
ln -sf mtp-connect mtp-delfile
ln -sf mtp-connect mtp-getfile
ln -sf mtp-connect mtp-newfolder
ln -sf mtp-connect mtp-sendfile
ln -sf mtp-connect mtp-sendtr
popd
# Copy documentation to a good place
mkdir -p -m 755 %{buildroot}%{_docdir}/%{name}-%{version}
install -p -m 644 AUTHORS ChangeLog COPYING INSTALL README TODO \
%{buildroot}%{_docdir}/%{name}-%{version}
# Touch generated files to make them always have the same time stamp.
touch -r configure.ac \
      %{buildroot}%{_includedir}/*.h \
      %{buildroot}%{_libdir}/pkgconfig/*.pc \

# FIXME
mv %{buildroot}/lib/udev/{mtp-,}mtp-probe

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root,root,-)
%{_libdir}/*.so.*
/lib/udev/rules.d/*
/lib/udev/mtp-probe

%files examples
%defattr(-,root,root,-)
%{_bindir}/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%dir %{_docdir}/%{name}-%{version}
%{_docdir}/%{name}-%{version}/*
%{_includedir}/*.h
%{_libdir}/pkgconfig/*.pc

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Mon Aug 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0
- remove Requires hal

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-4m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-3m)
- iPhone 3GS mislabelled as Sony DCR-SR75

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-1m)
- version 1.0.3

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- version 1.0.2
- remove merged pkgconfig.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m) 
- set _program_prefix to mtp-

* Sat Sep 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7-1m)
- version 0.3.7

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-1m)
- version 0.3.6
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.5-1m)
- version 0.3.5

* Sun Nov  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-2m)
- fix pkg-config file

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-1m)
- version 0.3.3
- remove pam module

* Wed Aug 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-1m)
- version 0.3.1

* Fri Aug 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6.1-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.6.1-1m)
- version 0.2.6.1

* Thu Mar  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.6-1m)
- version 0.2.6

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-2m)
- %%NoSource -> NoSource

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-1m)
- version 0.2.5

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-1m)
- version 0.2.4

* Fri Oct 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.3-1m)
- version 0.2.3

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- version 0.2.1

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.5-1m)
- version 0.1.5

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.4-1m)
- version 0.1.4

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.3-1m)
- version 0.1.3

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-1m)
- version 0.1.0 (0.1.1 and 0.1.2 doesn't work with amarok-1.4.4)
- get rid of *.la files

* Tue Oct 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.21-1m)
- version 0.0.21

* Wed Aug 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.11-1m)
- initial package for amarok-1.4.2
