%global momorel 12

%define ISONAME KOI8-R
%define catalogue /etc/X11/fontpath.d
Name: fonts-%{ISONAME}
Version: 1.0
Release: %{momorel}m%{?dist}
License: MIT
#URL: <none yet>
Source: http://www.inp.nsk.su/~bolkhov/files/fonts/cyr-rfx/srctgz/cyr-rfx-koi8-ub-1.1.bdfs.tar.bz2
Source1: Makefile.cyrfonts
Source2: ftp://ftp.ksi-linux.com/pub/patches/ksi-XFree86-cyrillic-fonts.tar.bz2
Patch0: koi8-ub-bcl.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Group: User Interface/X
Summary: Russian and Ukrainian language fonts for the X Window System.
Obsoletes: XFree86-KOI8-R
Provides: XFree86-KOI8-R
BuildRequires:  xorg-x11-mkfontdir, xorg-x11-font-utils
Requires:  fontpackages-filesystem
 
%description
If you use the X Window System and you want to display Russian and
Ukrainian fonts, you should install this package.

This package contains a full set of Russian and Ukrainian fonts, in
compliance with the KOI8-R standard. The fonts included in
this package are distributed free of charge and can be used
freely, subject to the accompanying license.


%package 75dpi
Group: User Interface/X
Summary: A set of 75 dpi Russian and Ukrainian language fonts for X.
Requires:  fontpackages-filesystem
Obsoletes: XFree86-KOI8-R-75dpi-fonts
Provides: XFree86-KOI8-R-75dpi-fonts

%description 75dpi
This package contains a set of Russian and Ukrainian language fonts
at 75 dpi resolution for the X Window System.

If you are installing the X Window System and you need to display
Russian and Ukrainian language characters in 75 dpi resolution, you should
install this package.

%package 100dpi
Group: User Interface/X
Summary: KOI8-R fonts in 100 dpi resolution for the X Window System.
Requires:  fontpackages-filesystem
Obsoletes: XFree86-KOI8-R-100dpi-fonts
Provides: XFree86-KOI8-R-100dpi-fonts

%description 100dpi
This package contains a set of Russian and Ukrainian language fonts
at 100 dpi resolution for the X Window System.

If you are installing the X Window System and you need to display
Russian and Ukrainian language characters in 100 dpi resolution, you should
install this package.

%prep
%setup -q -n koi8-ub -a 2
cp -a Cyrillic/100dpi ./
cp %{SOURCE1} ./Makefile
%patch0 -p1

%build
make all

%install
rm -rf $RPM_BUILD_ROOT

DSTFONT=$RPM_BUILD_ROOT/usr/share/fonts/%{ISONAME}
make install PREFIX=$RPM_BUILD_ROOT \
	FONTDIR=$DSTFONT
mkdir -p $RPM_BUILD_ROOT%{catalogue}
ln -sf /usr/share/fonts/%{ISONAME}/misc $RPM_BUILD_ROOT%{catalogue}/%{ISONAME}-misc
ln -sf /usr/share/fonts/%{ISONAME}/75dpi $RPM_BUILD_ROOT%{catalogue}/%{ISONAME}-75dpi
ln -sf /usr/share/fonts/%{ISONAME}/100dpi $RPM_BUILD_ROOT%{catalogue}/%{ISONAME}-100dpi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc doc  Cyrillic/COPYRIGHT
%dir /usr/share/fonts/%{ISONAME}
%{catalogue}/%{ISONAME}-misc
/usr/share/fonts/%{ISONAME}/misc

%files 75dpi
%defattr(-,root,root)
%{catalogue}/%{ISONAME}-75dpi
/usr/share/fonts/%{ISONAME}/75dpi

%files 100dpi
%defattr(-,root,root)
%{catalogue}/%{ISONAME}-100dpi
/usr/share/fonts/%{ISONAME}/100dpi

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-8m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Sun Apr 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- replace BuildRequires: xorg-x11-bdftopcf with xorg-x11-font-utils

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-4m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc43

* Wed Jun 13 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0-2m)
- change BuildRequires:  mkfontdir --> xorg-x11-mkfontdir
-                        bdftopcf  --> xorg-x11-bdftopcf

* Sun Jun 10 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.0-9.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 07 2005 Caolan McNamara <caolanm@redhat.com> 1.0-9

* Thu Jul 07 2005 Karsten Hopp <karsten@redhat.de> 1.0-8
- BuildRequires xorg-x11-font-utils for bdftopcf and mkfontdir

* Tue Sep 21 2004 Than Ngo <than@redhat.com> 1.0-7
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 1.0-4
- rebuild

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Feb 26 2002 Mike A. Harris <mharris@redhat.com> 1.0-1
- Renamed package from XFree86-KOI8-R to fonts-KOI8-R since this is not part
  of XFree86, nor released by XFree86.org, and should not be named as such,
  as it causes confusion.
- Updated package descriptions to be fitting for the times.
- Removed bogus dependancy on XFree86 as X shouldn't be required just to
  install fonts.

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Mar 28 2001 Than Ngo <than@redhat.com>
- fixed incorect LANG (Bug #33250)

* Mon Dec 11 2000 Than Ngo <than@redhat.com>
- rebuilt with the fixed fileutils

* Thu Nov 16 2000 Than Ngo <than@redhat.com>
- add missing Prereq on /usr/sbin/chkfontpath

* Sun Sep  3 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- fix several typos with wrong directories in scripts

* Thu Jul 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix %%defattr in the wrong place in the files list

* Wed Jul 12 2000 Than Ngo <than@redhat.de>
- adopted for Winston. 
