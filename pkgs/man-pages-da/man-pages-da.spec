%global momorel 21

Summary: Danish man (manual) pages from the Linux Documentation Project
Name: man-pages-da
Version: 0.1.1
Release: %{momorel}m%{?dist}
# These are translated copies of GPL licensed man pages (LDP).
License: GPL
Group: Documentation
Patch0: manpages-da-0.1.1-manpath.patch 
URL: http://www.sslug.dk/locale/man-sider/
#Source0: http://www.sslug.dk/locale/man-sider/manpages-da-%{version}.tar.gz
#NoSource: 0
Source0: http://www.sslug.dk/locale/man-sider/manpages-da-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
Manual pages from the Linux Documentation Project, translated into Danish.

%prep
%setup -q -n manpages-da-%{version}
%patch0 -p1

%build

%install
for i in *.1; do
        iconv -f ISO-8859-1 -t UTF-8 < $i > $i.new
        mv -f $i.new $i
done
rm -fr %{buildroot}
make PREFIX=%{buildroot}/usr MANDIR=%{buildroot}%{_mandir} install

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog
%{_mandir}/da/*/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-19m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-17m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-16m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPL

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-15m)
- rebuild against gcc43

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-14m)
- remove %%{_mandir}/da from %%files, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.1-13m)
- sync FC

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-12m)
- NoSource to Source

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.1.1-11m)
- rebuild against new environment.

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- mada nigirisugi

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- nigirisugi

* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jul 02 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rpmify
