%global momorel 3
%global qtver 4.8.6
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global kdever 4.13.0
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: Efficient ID3 tag editor
Name: kid3
Version: 3.0.2
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://kid3.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-3.0-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: dbus
Requires: %{name}-libs = %{version}
BuildRequires: qt-devel >= %{qtver}-%{qtrel}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: dbus-devel
BuildRequires: desktop-file-utils
BuildRequires: flac-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: gettext
BuildRequires: id3lib-devel
BuildRequires: libchromaprint-devel
BuildRequires: libmp4v2-devel >= 1.9.1
BuildRequires: libogg-devel
BuildRequires: libtunepimp-devel
BuildRequires: libvorbis-devel
BuildRequires: libxml2
BuildRequires: taglib-devel >= 1.4
BuildRequires: phonon-devel

%description
If you want to easily tag multiple MP3, Ogg/Vorbis or FLAC files
(e.g. full albums) without typing the same information again and again
and have control over both ID3v1 and ID3v2 tags, then Kid3 is the
program you are looking for.

%package qt
Group: Applications/Multimedia
Summary: Efficient ID3 tag editor for Qt
Requires: %{name}-libs = %{version}

%description qt
This package does not use KDE libraries, if you use KDE you should use kid3.

%package libs
Group: Applications/Multimedia
Summary: Audio tag editor core libraries and data

%description libs
This package contains common libraries and data used by both kid3 and kid3-qt.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
%ifarch x86_64
%define lib_dir lib64
%else
%define lib_dir lib
%endif

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DWITH_APPS="Qt;KDE" \
	-DBUILD_SHARED_LIBS=ON \
	-DCMAKE_SKIP_RPATH=ON \
	-DWITH_LIBDIR:STRING="%{lib_dir}" \
	-DWITH_PLUGINSDIR:STRING="%{lib_dir}/kid3/plugins" \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/kid3.png %{buildroot}%{_datadir}/pixmaps/kid3.png
ln -s ../icons/hicolor/48x48/apps/kid3-qt.png %{buildroot}%{_datadir}/pixmaps/kid3-qt.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q &> /dev/null
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%post qt
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun qt
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%posttrans qt
update-desktop-database -q &> /dev/null
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog LICENSE README
%{_kde4_bindir}/kid3
%{_kde4_datadir}/applications/kde4/kid3.desktop
%{_kde4_appsdir}/kid3
%{_datadir}/dbus-1/interfaces/net.sourceforge.Kid3.xml
%{_kde4_docdir}/HTML/*/kid3
%{_kde4_iconsdir}/*/*/*/kid3.png
%{_kde4_iconsdir}/*/*/*/kid3.svgz
%{_mandir}/man1/kid3.1*
%{_mandir}/de/man1/kid3.1*
%{_datadir}/pixmaps/kid3.png

%files qt
%defattr(-,root,root,-)
%{_kde4_bindir}/kid3-qt
%{_kde4_datadir}/applications/*kid3-qt.desktop
%{_kde4_iconsdir}/*/*/*/kid3-qt.png
%{_kde4_iconsdir}/*/*/*/kid3-qt.svg
%{_kde4_datadir}/doc/kid3-qt
%{_mandir}/man1/kid3-qt.1*
%{_mandir}/de/man1/kid3-qt.1*
%{_datadir}/pixmaps/kid3-qt.png

%files libs
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING LICENSE README
%{_kde4_libdir}/*.so*
%{_kde4_libdir}/kid3
%{_kde4_datadir}/kid3/translations

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-3m)
- adjust %%files

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.2-2m)
- rebuild against ffmpeg

* Sat Nov 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu Oct 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Mon Mar 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Tue Dec  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Sat Oct 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sun Jun 10 2012 NARITA Koichi ,pulsar@momonga-linux.org>
- (2.1-1m)
- update to 2.1

* Thu Oct  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Sep  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-2m)
- rebuild for new GCC 4.6

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-2m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-3m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-2m)
- rebuild against qt-4.6.3-1m

* Sat Mar  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-1m)
- version 1.4
- update desktop.patch

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3-3m)
- rebuild against libmp4v2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- version 1.3

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-1m)
- version 1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- version 1.1
- remove merged gcc43.patch

* Sun Jun 29 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-2m)
- add BuildRequires: phonon-devel

* Wed May 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0
- remove merged fix-docdir.patch
- BPR: libmp4v2-devel

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-8m)
- revise %%{_docdir}/HTML/*/kid3/common

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-7m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-6m)
- rebuild against gcc43

* Wed Mar  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-5m)
- build with KDE4
- fix HTML documentation's directories

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-4m)
- fix gcc-4.3

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-2m)
- %%NoSource -> NoSource

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-1m)
- version 0.10
- License: GPLv2

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-2m)
- rebuild against libvorbis-1.2.0-1m

* Thu Jun 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-1m)
- version 0.9

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- initial package for Momonga Linux
