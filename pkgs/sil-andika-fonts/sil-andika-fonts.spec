%global momorel 6

%global fontname sil-andika
%global fontconf 60-%{fontname}.conf

# Someday SIL will release sanely named archives
%global archivename AndikaBasicR_10

Name:    %{fontname}-fonts
Version: 1.0
Release: %{momorel}m%{?dist}
Summary: A font for literacy and beginning readers

Group:     User Interface/X
License:   OFL
URL:       http://scripts.sil.org/Andika
# Actual download URL
# http://scripts.sil.org/cms/scripts/render_download.php?site_id=nrsi&format=file&media_id=%{archivename}&filename=%{archivename}.zip
Source0:   %{archivename}.zip
Source1:   %{name}-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem
Obsoletes:     andika-fonts < 1.0-4

%description
Andika is a sans serif, Unicode-compliant font designed especially for
literacy use, taking into account the needs of beginning readers. The focus is
on clear, easy-to-perceive letterforms that will not be readily confused with
one another.

A sans serif font is preferred by some literacy personnel for teaching people
to read. Its forms are simpler and less cluttered than those of most serif
fonts. For years, literacy workers have had to make do with fonts that were
not really suitable for beginning readers and writers. In some cases, literacy
specialists have had to tediously assemble letters from a variety of fonts in
order to get all of the characters they need for their particular language
project, resulting in confusing and unattractive publications. Andika
addresses those issues.


%prep
%setup -q -n %{archivename}

# Text files sanitization
for txt in *.txt ; do
   # Unicode compliant, check
   iconv -f WINDOWS-1252 -t UTF-8 -o $txt.1 $txt
   fold -s $txt.1 > $txt.2
   sed -i 's/\r//' $txt.2
   touch -r $txt $txt.2
   mv $txt.2 $txt
   rm $txt.1
done


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc *.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0-5
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Sun Jan 25 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0-4
- renamed to sil-andika-fonts to apply Packaging:FontsPolicy#Naming

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0-3
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0-2
- Rebuild using new rpm-fonts

* Tue Jun 24 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.0-1
- Initial packaging
