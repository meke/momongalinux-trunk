%global momorel 1

Summary: X509/LDAP certificate and revocation list client
Name: dirmngr
Version: 1.1.1
Release: %{momorel}m%{?dist}
URL: http://www.gnupg.org/
Source0: ftp://ftp.gnupg.org/gcrypt/dirmngr/dirmngr-%{version}.tar.bz2
NoSource: 0

License: GPLv2+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libgcrypt
Requires: pth
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: libksba >= 0.9.11
BuildRequires: libgcrypt-devel >= 1.2.2-2m
BuildRequires: pth-devel
BuildRequires: libassuan-devel >= 2.0.0
BuildRequires: libgpg-error-devel

Source10: dirmngr.conf
Source11: ldapservers.conf
Source12: dirmngr.logrotate

BuildRequires: gawk
BuildRequires: gettext
BuildRequires: libassuan-devel
# BuildRequires: libassuan
BuildRequires: libgcrypt-devel >= 1.2.0
# BuildRequires: libksba-devel >= 1.0.0
BuildRequires: libksba >= 1.0.0
BuildRequires: openldap-devel
BuildRequires: pth-devel

%description
Dirmngr is a server for managing and downloading certificate
revocation lists (CRLs) for X.509 certificates and for downloading
the certificates themselves. Dirmngr also handles OCSP requests as
an alternative to CRLs. Dirmngr is either invoked internally by
gpgsm (from gnupg2) or when running as a system daemon through
the dirmngr-client tool.

%prep
%setup -q

pushd doc
iconv -f iso-8859-1 -t utf-8 dirmngr.texi -o dirmngr.texi.NEW && mv dirmngr.texi.NEW dirmngr.texi
iconv -f iso-8859-1 -t utf-8 dirmngr.info -o dirmngr.info.NEW && mv dirmngr.info.NEW dirmngr.info
popd

%build
export LDAPLIBS="-lldap -llber"
%configure
%make

%install
rm -rf %{buildroot}

# dirs
mkdir -p %{buildroot}%{_sysconfdir}/dirmngr/trusted-certs
mkdir -p %{buildroot}%{_var}/cache/dirmngr/crls.d
mkdir -p %{buildroot}%{_var}/lib/dirmngr/extra-certs
mkdir -p %{buildroot}%{_var}/log/dirmngr
mkdir -p %{buildroot}%{_var}/run/dirmngr

make install DESTDIR=%{buildroot}

# dirmngr.log, logrotate
install -p -m644 -D %{SOURCE12} %{buildroot}%{_sysconfdir}/logrotate.d/dirmngr

# conf files
install -p -m644 %{SOURCE10} %{SOURCE11} %{buildroot}%{_sysconfdir}/dirmngr/

%find_lang %{name}

## unpackaged files
rm -f %{buildroot}%{_infodir}/dir
rm -rf %{buildroot}%{_docdir}/dirmngr/examples

%check
make check

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/dirmngr.info %{_infodir}/dir ||:

%postun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/dirmngr.info %{_infodir}/dir ||:
fi


%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING README ChangeLog NEWS
%{_bindir}/dirmngr
%{_bindir}/dirmngr-client
%{_infodir}/dirmngr*
%{_datadir}/locale/*/LC_MESSAGES/dirmngr.mo
%{_libexecdir}/dirmngr_ldap
%dir %{_sysconfdir}/dirmngr
# TODO/FIXME
#{_initrddir}/*
## files/dirs for --daemon mode
%config(noreplace) %{_sysconfdir}/dirmngr/*.conf
%config %{_sysconfdir}/logrotate.d/*
%{_infodir}/dirmngr.info*
%{_mandir}/man1/*
%{_var}/cache/dirmngr/
%{_var}/lib/dirmngr/
%{_var}/log/dirmngr/
%{_var}/run/dirmngr/

%changelog
* Tue Apr 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0
- rebuild against libassuan-2.0.0

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-5m)
- explicitly link liblber

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.2-2m)
- change spec: BuildPreReq: libassuan -> libassuan-devel
               BuildRequires: libassuan ->  libassuan-devel

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-4m)
- do not specify info file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- update 1.0.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.2-4m)
- rebuild against openssl-0.9.8a

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.2-3m)
- rebuild against openldap-2.3.11

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-2m)
- BuildPreReq: libgpg-error-devel

* Sat Apr 23 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.2-1m)

* Sat Mar 19 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.1-1m)
- minor bugfix

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.0-1m)
- Alter -> Main
- _sbindir -> _bindir
- requires: pth

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (0.5.5-1m)

* Fri Sep 12 2003 TAKAHASHI Tamotsu <tamo>
- (0.4.5-1m)
- dirmngr, divided from newpg

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.4-6m)
  rebuild against cyrus-sasl2
  
* Wed May 14 2003 TAKAHASHI Tamotsu <tamo>
- (0.9.4-5m)
- pinentry 0.6.9 (has only qt-related changes)
- dirmngr  0.4.5 (src/ChangeLog has been merged to
 toplevel ChangeLog file)
- no longer declare CC=gcc
- make with _smp_mflags
- enable pinentry-qt when pinqt is 1 (default 0)

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.4-4m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.4-3m)
- rebuild against for gdbm

* Sun Feb 09 2003 TAKAHASHI Tamotsu <tamo>
- (0.9.4-2m)
- dir 0.4.4
- pin 0.6.8 (URL changed?)
- BuildPreReq: libgcrypt-devel >= 1.1.12
- autogen pinentry
- BuildPreReq: db2

* Thu Dec 05 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.4-1m)
- BuidPreReq: libksba >= 0.4.6

* Mon Dec 02 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.3-1m)
- npg 0.9.3
- dir 0.4.3
- pin 0.6.7 (URI changed)

* Sat Oct 26 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.2-3m)
- "LIBS=-ldb2" no longer needed

* Fri Oct 11 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.2-2m)
- all: misc docs added (rpmdoc/*)
- pinentry: option --enable-fallback-curses added

* Sat Oct 05 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.9.2-1m)
- now preun/post is in if-fi
- npg 0.9.2
- dir 0.4.2
- pin 0.6.5
- (libksba >= 0.4.5)
- (libgcrypt-devel >= 1.1.10)

* Sun Sep 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-2m)
- BuildPreReq: libksba >= 0.4.4

* Fri Sep 13 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.9.1-1m)
- dirver 0.4.1-p1
- pinver 0.6.4
- added dirmngr.info, renamed gpgsm.info to gnupg.info
- added newpg.mo
- removed README-alpha, agent/keyformat.txt and doc
- corrected info installation

* Fri Jul 12 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.9-2m)
- sorry, --with-*-pgm's should be ProGraMs (not dir)

* Thu Jul 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.9-1m)
- update
- install-info
- --disable-scdaemon --enable-gpg --without-pth
- URL: http://www.gnupg.org/
- dirver 0.3.0
- pinver 0.6.3 ENable-curses ENable-gtk DISable-qt
- libdir/newpg/protect-tools

* Thu May 02 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.3.5-0.020020501002k)
- from anoncvs
- url unknown
- not committed yet
