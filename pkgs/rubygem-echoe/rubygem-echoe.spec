# Generated from echoe-4.6.3.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname echoe

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A Rubygems packaging tool that provides Rake tasks for documentation, extension compiling, testing, and deployment
Name: rubygem-%{gemname}
Version: 4.6.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://fauna.github.com/fauna/echoe/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.8.4
Requires: ruby 
Requires: rubygem(gemcutter) >= 0.7.0
Requires: rubygem(rubyforge) >= 2.0.4
Requires: rubygem(allison) >= 2.0.3
Requires: rubygem(rdoc) >= 3.6.1
Requires: rubygem(rake) >= 0.9.2
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.8.4
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
A Rubygems packaging tool that provides Rake tasks for documentation,
extension compiling, testing, and deployment.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/CHANGELOG
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README
%doc %{geminstdir}/lib/echoe.rb
%doc %{geminstdir}/lib/echoe/extensions.rb
%doc %{geminstdir}/lib/echoe/platform.rb
%doc %{geminstdir}/lib/echoe/rubygems.rb
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-1m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-1m)
- update 4.6.3

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.1-1m)
- update 4.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.1-1m)
- update to 4.3.1

* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-1m)
- update to 4.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-1m)
- Initial package for Momonga Linux
