%global momorel 2

Summary: The NIS daemon which binds NIS clients to an NIS domain
Name: ypbind
Version: 1.37.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Daemons
Source0: ftp://ftp.kernel.org/pub/linux/utils/net/NIS/ypbind-mt-%{version}.tar.bz2
Url: http://www.linux-nis.org/nis/ypbind-mt/index.html
Source2: nis.sh
Source3: ypbind.service
Source4: ypbind-pre-setdomain
Source5: ypbind-post-waitbind
# Fedora-specific patch. Renaming 'ypbind' package to proper
# 'ypbind-mt' would allow us to drop it.
Patch1: ypbind-1.11-gettextdomain.patch
# Not sent to upstream.
Patch2: ypbind-helpman.patch
# This is for /bin/systemctl
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: rpcbind, yp-tools
BuildRequires: dbus-glib-devel, docbook-style-xsl

%description
The Network Information Service (NIS) is a system that provides
network information (login names, passwords, home directories, group
information) to all of the machines on a network. NIS can allow users
to log in on any machine on the network, as long as the machine has
the NIS client programs running and the user's password is recorded in
the NIS passwd database. NIS was formerly known as Sun Yellow Pages
(YP).

This package provides the ypbind daemon. The ypbind daemon binds NIS
clients to an NIS domain. Ypbind must be running on any machines
running NIS client programs.

Install the ypbind package on any machines running NIS client programs
(included in the yp-tools package). If you need an NIS server, you
also need to install the ypserv package to a machine on your network.

%prep
%setup -q -n ypbind-mt-%{version}
%patch1 -p1 -b .gettextdomain
%patch2 -p1 -b .helpman

autoconf

%build
%configure --enable-dbus-nm
make

%install
rm -rf %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/yp/binding
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/dhcp/dhclient.d
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}
install -m 644 etc/yp.conf $RPM_BUILD_ROOT%{_sysconfdir}/yp.conf
install -m 755 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/dhcp/dhclient.d/nis.sh
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/ypbind.service
install -m 755 %{SOURCE4} $RPM_BUILD_ROOT%{_libexecdir}/ypbind-pre-setdomain
install -m 755 %{SOURCE5} $RPM_BUILD_ROOT%{_libexecdir}/ypbind-post-waitbind

%{find_lang} %{name}

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files -f %{name}.lang
%files
%{_sbindir}/*
%{_mandir}/*/*
%{_libexecdir}/*
%{_unitdir}/*
%{_sysconfdir}/dhcp/dhclient.d/*
%config(noreplace) %{_sysconfdir}/yp.conf
%dir %{_localstatedir}/yp/binding
%doc README NEWS COPYING

%changelog
* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.37.1-2m)
- update service and patch files

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.37.1-1m)
- update 1.37.1

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-2m)
- add source

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.33-1m)
- update 1.33

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.32-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.32-1m)
- update to 1.32 based on Rawhide (3:1.32-1)

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20.4-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.4-2m)
- rebuild against rpm-4.6

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20.4-1m)
- update to 1.20.4
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19-2m)
- rebuild against gcc43

* Wed Nov  9 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.19-1m)
- up to 1.19

* Sun May 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17.3-1m)
- update 1.17.3

* Sun Mar 28 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.12-4m)
- remove %%files "-f %{name}.lang"

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (1.12-3m)
- revised spec for rpm 4.2.

* Tue Jun 11 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.12-2k)
  update to 1.12

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.10-4k)
- Prereq: /sbin/chkconfig -> chkconfig

* Tue Jan 22 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.10-2k)
- update to 1.10 and merge to rh 1.10-2

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.8-4k)
- nigirisugi /var/yp

* Wed Oct  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.8-2k)
- update to 1.8 and merge rh 1.8-1

* Mon Sep 24 2001 Toru Hoshina <t@kondara.org>
- (1.7-14k)
- ypbind.init 1.2.

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- /etc/rc.d/init.d -> /etc/init.d

* Tue Jan 30 2001 WATABE Toyokazu <toy2@kondara.org>
- (1.7-10k)
- remove definition %_initscriptdir

* Thu Jan 18 2001 Toru Hoshina <toru@df-usa.com>
- fixed initscript.

* Sun Jan 14 2001 WATABE Toyokazu <toy2@kondara.org>
- (1.7-4k)
- rebuild against glibc-2.1.3.

* Thu Dec  7 2000 AYUHANA Tomonori <l@kondara.org>
- (1.7-3k)
- version mt-1.7 (ported from RawHide 1.7-1)

* Tue Nov 28 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Apr 08 2000 Takaaki Tabuci <tab@kondara.org>
- merge redhat-6.2 (3.3-28).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jma mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Sat Feb 05 2000 Cristian Gafton <gafton@redhat.com>
- fix postun to be a preun (doh!)

* Fri Feb 04 2000 Nalin Dahyabhai <nalin@redhat.com>
- bug #9122: check that domainname isn't whitspace before removing files
- bug #9086: wrong path in man page fixed with --sbindir
- use DESTDIR at install-time

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-ypbind-20000115

* Mon Jan 24 2000 Cristian Gafton <gafton@redhat.com>
- use getopt to parse options; add --ping option
- decrease the ping time to 10 sec (use --ping above if you don't like it)
- hack the init script to use the new --ping option
- add the -broadcast patch to fallback on broadcasting if the NIS server
  goes down. Make initscript know about that.

* Tue Nov 30 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-ypbind-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Sep 21 1999 Bill Nottingham <notting@redhat.com>
- fix initscript typo

* Tue Sep 07 1999 Cristian Gafton <gafton@redhat.com>
- patch from HJLu to fix queries for domains other than the default one.

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Thu Jul  1 1999 Bill Nottingham <notting@redhat.com>
- move start script b/c ypserv moved

* Thu Apr 15 1999 Cristian Gafton <gafton@redhat.com>
- requires yp-tools, because ypwhcih is part of that package

* Tue Apr 13 1999 Bill Nottingham <notting@redhat.com>
- don't run ypwhich script if ypbind doesn't start

* Wed Apr 07 1999 Bill Nottingham <notting@redhat.com>
- add a 10 second timeout for initscript...

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- strip binary

* Thu Apr 01 1999 Preston Brown <pbrown@redhat.com>
- fixed init script to wait until domain is really bound (bug #1928)

* Thu Mar 25 1999 Cristian Gafton <gafton@redhat.com>
- revert to stabdard ypbind; ypbind-mt sucks.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Sat Feb 13 1999 Cristian Gafton <gafton@redhat.com>
- build as ypbind instead of ypbind-mt

* Fri Feb 12 1999 Michael Maher <mike@redhat.com>
- addressed bug #609

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- provides ypbind
- switch to ypbind-mt instead of plain ypbind
- build for glibc 2.1
