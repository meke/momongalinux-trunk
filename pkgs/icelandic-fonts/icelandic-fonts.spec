%global momorel 6

%define fontname icelandic

Name:           %{fontname}-fonts
Version:        1.001
Release:        %{momorel}m%{?dist}
Summary:        Icelandic Magical Staves

Group:          User Interface/X
License:        OFL
URL:            http://openfontlibrary.org/media/files/asyropoulos/238
Source0:        http://openfontlibrary.org/people/asyropoulos/asyropoulos_-_Icelandic.otf
Source1:        README.license
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
A font that includes most "magical" staves that have been "used" in Iceland.
Original drawings from the Museum of Sorcery & Witchcraft web site at
www.galdrasyning.is/.

%prep
cp %{SOURCE1} $RPM_BUILD_DIR

%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{buildroot}%{_fontdir}


%clean
rm -fr %{buildroot}

%_font_pkg *.otf


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.001-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.001-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.001-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.001-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.001-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.001-1m)
- import from Fedora

* Sun Mar 8 2009 Jon Stanley <jonstanley@gmail.com> - 1.001-7
- Fix including %%post in here from last fix

* Sun Mar 8 2009 Jon Stanley <jonstanley@gmail.com> - 1.001-6
- Use font_pkg macro again

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.001-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb 20 2009 Jon Stanley <jonstanley@gmail.com> - 1.001-4
- No fontconfig file, so can't use the font_pkg macro

* Fri Feb 20 2009 Jon Stanley <jonstanley@gmail.com> - 1.001-3
- Update to new packaging guidelines

* Sun Jul 12 2008 Jon Stanley <jonstanley@gmail.com> - 1.001-2
- Change font filename to something sane

* Sun Jul 12 2008 Jon Stanley <jonstanley@gmail.com> - 1.001-1
- Change version to match font file
- Include license explanation
- Change around spec a little

* Mon May 05 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-1
- Initial package

