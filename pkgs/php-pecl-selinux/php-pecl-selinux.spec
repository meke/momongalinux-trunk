%global momorel 3

# PECL(PHP Extension Community Library) related definitions
%{!?__pecl:     %{expand: %%global __pecl     %{_bindir}/pecl}}
%define pecl_name selinux

Summary: SELinux binding for PHP scripting language
Name: php-pecl-selinux
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: PHP
Group: Development/Languages
URL: http://pecl.php.net/package/%{pecl_name}
Source0: http://pecl.php.net/get/%{pecl_name}-%{version}.tgz
NoSource: 0
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires: php-devel >= 5.5.2, php-pear >= 1.9.4, libselinux-devel >= 2.0.80
Requires: php >= 5.5.2, libselinux >= 2.0.80
Requires(post): %{__pecl}
Requires(postun): %{__pecl}
%if 0%{?php_zend_api}
Requires: php(zend-abi) = %{php_zend_api}
Requires: php(api) = %{php_core_api}
%else
Requires: php-api = %{php_apiver}
%endif
Provides: php-pecl(%{pecl_name}) = %{version}-%{release}

%description
This package is an extension to the PHP Hypertext Preprocessor.
It wraps the libselinux library and provides a set of interfaces
to the PHP runtime engine.
The libselinux is a set of application program interfaces towards in-kernel
SELinux, contains get/set security context, communicate security server,
translate between raw and readable format and so on.

%prep
%setup -c -q

%build
pushd %{pecl_name}-%{version}
%{_bindir}/phpize
%configure  --enable-selinux
make %{?_smp_mflags}
(echo "; Enable SELinux extension module"
 echo "extension=selinux.so") > selinux.ini.dist
popd

%install
pushd %{pecl_name}-%{version}
rm -rf %{buildroot}
install -D -p -m 0755 modules/selinux.so %{buildroot}%{php_extdir}/selinux.so
install -D -p -m 0644 selinux.ini.dist %{buildroot}%{_sysconfdir}/php.d/selinux.ini.dist

# Install XML package description
%{__mkdir_p} %{buildroot}%{pecl_xmldir}
%{__install} -m 644 ../package.xml %{buildroot}%{pecl_xmldir}/%{name}.xml
popd

%clean
rm -rf %{buildroot}

%if 0%{?pecl_install:1}
%post
%{pecl_install} %{pecl_xmldir}/%{name}.xml >/dev/null || :
%endif

%if 0%{?pecl_uninstall:1}
%postun
if [ $1 -eq 0 ] ; then
    %{pecl_uninstall} %{pecl_name} >/dev/null || :
fi
%endif

%files
%defattr(-,root,root,-)
%doc %{pecl_name}-%{version}/LICENSE %{pecl_name}-%{version}/README
%config(noreplace) %{_sysconfdir}/php.d/selinux.ini.dist
%{php_extdir}/selinux.so
%{pecl_xmldir}/%{name}.xml

%changelog
* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-3m)
- rebuild against php-5.5.2

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-2m)
- rebuild against php-5.4.1

* Sun May 01 2011 Yasuo Ohgaki <yohgkai@momonga-linux.org>
- (0.3.1-1m)
- Import from fedora

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Mar  3 2010 KaiGai Kohei <kaigai@kaigai.gr.jp> - 0.3.1-5
- Rebuilt for package 

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 13 2009 Remi Collet <Fedora@FamilleCollet.com> - 0.3.1-2
- rebuild for new PHP 5.3.0 ABI (20090626)

* Thu Apr 16 2009 KaiGai Kohei <kaigai@kaigai.gr.jp> - 0.3.1-1
- The "permissive" tag was added to selinux_compute_av
- The selinux_deny_unknown() was added
- README is updated for the new features

* Thu Mar 12 2009 KaiGai Kohei <kaigai@kaigai.gr.jp> - 0.2.1-1
- Specfile to build RPM package is added.

* Thu Mar  5 2009 KaiGai Kohei <kaigai@kaigai.gr.jp> - 0.1.2-1
- The initial release of SELinux binding for PHP script language.
