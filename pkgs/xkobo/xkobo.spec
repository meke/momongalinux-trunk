%global momorel 25

Summary: xkobo version 1.12
Name: xkobo
Version: 1.12
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Amusements/Games
Source0: %{name}-%{version}.tar.gz
Source1: %{name}.desktop
Source2: %{name}.as
Source3: %{name}-128.png
Patch1: %{name}-%{version}-fix.patch
Patch2: xkobo-1.12-gcc45.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: imake
BuildRequires: libX11-devel
BuildRequires: libXext-devel

%description
xkobo version 1.12.

%prep
%setup -q
%patch1 -p1 -b .fix~
%patch2 -p1 -b .gcc45~

%build
xmkmf -a
make %{name}

%install
rm -rf --preserve-root %{buildroot}

# install xkobo
mkdir -p %{buildroot}%{_bindir}
install %{name} %{buildroot}%{_bindir}

# install man file
mkdir -p %{buildroot}%{_mandir}/man6
install %{name}.man %{buildroot}%{_mandir}/man6/%{name}.6x

# own directories
mkdir -p %{buildroot}/var/lib/games/%{name}
chmod 777 %{buildroot}/var/lib/games/%{name}

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

mkdir -p %{buildroot}%{_datadir}/afterstep/start/Games/
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/afterstep/start/Games/

# install and convert icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64,128x128}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
convert -scale 16x16 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
rm -rf --preserve-root %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-,root,root)
%doc CHANGES COPYING README README.jp
%attr(1777,root,root) %dir /var/lib/games/%{name}
%attr(755,root,root) %{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/afterstep/start/Games/%{name}.as
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_mandir}/man6/%{name}.6*
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-24m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-23m)
- add gcc45 patch

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-22m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12-21m)
- add icons for menu and fix up desktop file
- add %%post and %%postun
- License: GPLv2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-20m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-18m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-17m)
- add a patch to fix compilation error

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-16m)
- rebuild against gcc43

* Tue Apr 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12-15m)
- revise xkobo.desktop and xkobo.as

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.12-14m)
- revised installdir

* Wed Mar 23 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.12-13m)
- revised desktop file

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.12-12m)
- rebuild against new environment.

* Mon Apr 16 2001 Akira Higuchi <a@kondara.org>
- (1.12-11k)
- don't use setuid. set sticky bit for the score directory.

* Thu Jan 18 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.12-9k)
- added xkobo.as for afterstep

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (1.12-7k)
- rebuild against gcc 2.96.

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.12-5k)
- imported xkobo.desktop
* Mon Oct 30 2000 Akira Higuchi <a@kondara.org>
- update to version 1.12
* Mon Apr 24 2000 Hironobu ABE <hiro-a@mars.dti.ne.jp>
- fixed HighScore read/write routine
* Sat Apr 22 2000 Hironobu ABE <hiro-a@mars.dti.ne.jp>
- 1st release for Vine Linux 2.0


