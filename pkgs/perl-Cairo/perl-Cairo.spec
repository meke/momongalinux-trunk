%global         momorel 3

Name:           perl-Cairo
Version:        1.104
Release:        %{momorel}m%{?dist}
Summary:        Cairo Perl module
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Cairo/
Source0:        http://www.cpan.org/authors/id/X/XA/XAOC/Cairo-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cairo-devel >= 1.0.0
BuildRequires:  perl-ExtUtils-Depends >= 0.2
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-ExtUtils-PkgConfig >= 1
BuildRequires:  perl-Test-Number-Delta >= 1
Requires:       perl-ExtUtils-Depends >= 0.2
Requires:       perl-ExtUtils-PkgConfig >= 1
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
perl Makefile.PL make make test make install

%prep
%setup -q -n Cairo-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc cairo-perl.typemap ChangeLog.pre-git doctypes LICENSE NEWS perl-Cairo.doap README TODO
%{perl_vendorarch}/Cairo*
%{perl_vendorarch}/auto/Cairo/
%{_mandir}/man3/*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.104-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.104-2m)
- rebuild against perl-5.18.2

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.104-1m)
- update to 1.104

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.103-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.103-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.103-2m)
- rebuild against perl-5.16.3

* Sun Feb  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.103-1m)
- update to 1.103

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.102-1m)
- update to 1.102

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.101-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.101-2m)
- rebuild against perl-5.16.1

* Wed Aug  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.101-1m)
- update to 1.101

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.100-1m)
- update to 1.100
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.090-1m)
- update to 1.090

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.082-1m)
- update to 1.082

* Sat Oct 15 2011 NARTIA Koichi <pulsar@momonga-linux.org>
- (1.081-1m)
- update to 1.081

* Fri Oct 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.080-1m)
- update to 1.080

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.062-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.062-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.062-2m)
- rebuild against perl-5.14.0-0.2.1m

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.062-1m)
- update to 1.062

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.061-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.061-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.061-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.061-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.061-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.061-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.061-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.061-2m)
- rebuild against perl-5.10.1

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.061-1m)
- update to 1.061

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.060-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.060-1m)
- update to 1.060

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.045-2m)
- rebuild against gcc43

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.045-1m)
- update to 1.045

* Fri Nov 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.044-1m)
- update to 1.044

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.043-1m)
- update to 1.043

* Fri Sep 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.042-1m)
- update to 1.042
- add BuildRequires: perl-Test-Number-Delta
- do not use %%NoSource macro

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.041-1m)
- update to 1.041

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.023-3m)
- use vendor

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.023-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.023-1m)
- import to Momonga from Fedora Development


* Mon Feb 26 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.023-1
- Update to 1.023.

* Sun Dec 31 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.022-1
- Update to 1.022.

* Sat Nov 11 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.021-1
- Update to 1.021.

* Sat Nov 11 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.02-1
- Update to 1.02.

* Mon Oct  2 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.01-2
- Rebuild (https://www.redhat.com/archives/fedora-maintainers/2006-October/msg00005.html).

* Tue Sep 26 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.01-1
- Update to 1.01.

* Tue Sep  5 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.00-1
- Update to 1.00.

* Wed Aug 16 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.92-1
- Update to 0.92.

* Sat Aug 12 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.91-1
- Update to 0.91.

* Sun Jul 16 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.90-1
- Update to 0.90.

* Tue Apr 18 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.03-2
- Disabled the test suite as it fails in mock.

* Sun Mar 19 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.03-1
- First build.
