%global momorel 7

Name:           qgit
Version:        2.3
Release:        %{momorel}m%{?dist}
Summary:        GUI browser for git repositories

Group:          Development/Tools
License:        GPLv2
URL:            http://digilander.libero.it/mcostalba/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:        %{name}.desktop
Source2:        %{name}48d.png
Patch0:         %{name}-2.3-qmake.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  qt-devel  >= 4.7.0
Requires:       git-core >= 1.4.0

%description
With qgit you are able to browse revisions history, view patch content
and changed files, graphically following different development branches.

%prep
%setup -q -n %{name}
%patch0 -p1 -b .qmake

# fix permissions
chmod a-x src/*.{cpp,h}


%build
%{_qt4_qmake} %{name}.pro
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install INSTALL_ROOT=$RPM_BUILD_ROOT

desktop-file-install --vendor=                                  \
        --dir $RPM_BUILD_ROOT%{_datadir}/applications           \
        %{SOURCE1}

install -m 0644 -p -D %{SOURCE2} $RPM_BUILD_ROOT%{_datadir}/pixmaps/%{name}.png


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-6m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-3m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-1m)
- import from Fedora 11

* Sun May 10 2009 Dan Horak <dan[at]danny.cz> 2.3-1
- update to upstream version 2.3

* Sat Feb 28 2009 Dan Horak <dan[at]danny.cz> 2.2-4
- update desktop file for recent standards

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Nov 25 2008 Dan Horak <dan[at]danny.cz> 2.2-2
- shorten Summary

* Thu Jul 17 2008 Dan Horak <dan[at]danny.cz> 2.2-1
- update to upstream version 2.2

* Sun Feb 10 2008 Dan Horak <dan[at]danny.cz> 2.1-3
- rebuild for gcc 4.3

* Mon Dec 31 2007 Dan Horak <dan[at]danny.cz> 2.1-2
- add missing patch

* Mon Dec 31 2007 Dan Horak <dan[at]danny.cz> 2.1-1
- update to upstream version 2.1

* Sat Sep  8 2007 Dan Horak <dan[at]danny.cz> 1.5.7-1
- update to upstream version 1.5.7
- fixes #268381

* Tue Aug 21 2007 Dan Horak <dan[at]danny.cz> 1.5.6-2
- update license tag
- rebuild for F8

* Fri May 23 2007 Dan Horak <dan[at]danny.cz> 1.5.6-1
- update to upstream version 1.5.6

* Mon Apr  9 2007 Dan Horak <dan[at]danny.cz> 1.5.5-2
- added an icon for the desktop file

* Sun Feb 25 2007 Dan Horak <dan[at]danny.cz> 1.5.5-1
- update to upstream version 1.5.5

* Fri Dec 29 2006 Dan Horak <dan[at]danny.cz> 1.5.4-1
- update to upstream version 1.5.4

* Sat Nov 11 2006 Dan Horak <dan[at]danny.cz> 1.5.3-1
- update to upstream version 1.5.3

* Sat Sep 30 2006 Dan Horak <dan[at]danny.cz> 1.5.2-1
- update to upstream version 1.5.2

* Tue Sep 12 2006 Dan Horak <dan[at]danny.cz> 1.5.1-1
- update to upstream version 1.5.1

* Sat Sep  9 2006 Dan Horak <dan[at]danny.cz> 1.5-1
- update to upstream version 1.5

* Fri Sep  1 2006 Dan Horak <dan[at]danny.cz> 1.4-3
- rebuild for FC6

* Sun Jul 16 2006 Dan Horak <dan@danny.cz> 1.4-2
- remove the patch also from the spec file

* Sun Jul 16 2006 Dan Horak <dan@danny.cz> 1.4-1
- update to upstream version 1.4
- remove the patch as upstream reverted the linking behaviour
- requires git-core >= 1.4.0

* Sun Jun 11 2006 Dan Horak <dan@danny.cz> 1.3-2
- added patch for creating of usable debug package (#194782)

* Mon Jun 05 2006 Dan Horak <dan@danny.cz> 1.3-1
- update to upstream version 1.3

* Sat Apr 29 2006 Dan Horak <dan@danny.cz> 1.2-1
- update to upstream version 1.2

* Sun Mar 19 2006 Dan Horak <dan@danny.cz> 1.1.1-1
- update to upstream version 1.1.1

* Tue Mar  7 2006 Dan Horak <dan@danny.cz> 1.1-2
- change requires from git to git-core only

* Sun Feb 19 2006 Dan Horak <dan@danny.cz> 1.1-1
- updated to upstream version 1.1
- fixed download URL for Source0 from sourceforge.net
- requires git >= 1.2.0

* Sat Feb 11 2006 Dan Horak <dan@danny.cz> 1.1-0.1.rc3.3
- added docs

* Sat Feb 11 2006 Dan Horak <dan@danny.cz> 1.1-0.1.rc3.2
- added desktop file

* Wed Feb  8 2006 Dan Horak <dan@danny.cz> 1.1-0.1.rc3.1
- first version
