%global momorel 1

Summary: Virtual tunnel over TCP/IP networks
Name: vtun
Version: 3.0.3
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Daemons
URL: http://vtun.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: vtun.socket
Source2: vtun.service
Patch0: vtun-nostrip.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel
Buildrequires: lzo-devel >= 2.01
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: byacc flex
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
VTun provides the method for creating Virtual Tunnels over TCP/IP
networks and allows to shape, compress, encrypt traffic in that
tunnels.  Supported type of tunnels are: PPP, IP, Ethernet and most of
other serial protocols and programs.

VTun is easily and highly configurable: it can be used for various
network tasks like VPN, Mobil IP, Shaped Internet access, IP address
saving, etc.  It is completely a user space implementation and does
not require modification to any kernel parts.

%prep

%setup -q
%patch0 -p1

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} \
             INSTALL_OWNER= \
             INSTALL="/usr/bin/install -p"

install -D -m 0644 -p %{SOURCE1} %{buildroot}/%{_unitdir}/vtun.socket
install -D -m 0644 -p %{SOURCE2} %{buildroot}/%{_unitdir}/vtun.service
install -D -m 0644 -p vtund.conf %{buildroot}/%{_sysconfdir}/

%clean
rm -rf %{buildroot}

%post
%systemd_post vtun.service vtun.socket

%preun 
%systemd_preun vtun.service vtun.socket
             
%postun      
%systemd_postun vtun.service vtun.socket

%files
%defattr(644,root,root,755)
%doc ChangeLog Credits FAQ README README.LZO README.Setup README.Shaper TODO vtund.conf
%config(noreplace) %{_sysconfdir}/vtund.conf
%{_unitdir}/vtun.socket
%{_unitdir}/vtun.service
%{_sbindir}/vtund
%dir %{_localstatedir}/log/vtund
%ghost %dir %{_localstatedir}/lock/vtund
%{_mandir}/man5/vtund.conf.5*
%{_mandir}/man8/vtun.8*
%{_mandir}/man8/vtund.8*

%changelog
* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-1m)
- update 3.0.3

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-7m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1
-- import SOURCE1 from Fedora 11 (3.0.1-5)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-15m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-14m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-13m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6-11m)
- %%NoSource -> NoSource

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-10m)
- fix %%changelog section

* Sun May 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-9m)
- revise %%files for rpm-4.4.2

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6-8m)
- rebuild against openssl-0.9.8a

* Thu Jul 21 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6-7m)
- rebuild against lzo-2.01

* Sun Jun 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.6-6m)
- rebuild against lzo-2.00
- add 'vtun-2.6-lfd_lco.c.patch'

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6-5m)
- add chkconfig

* Sun Jul  4 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6-4m)
- stop daemon

* Wed Apr  7 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6-3m)
- add PreReq: coreutils

* Sun Mar 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6-2m)
- change docdir %%defattr

* Sat Oct 25 2003 Dai OKUYAMA <dai@3hands.net>
- (2.6-1m)
- first release for momonga

* Tue Mar 18 2003 Bishop Clark (LC957) <bishop@platypus.bc.ca> 2.6-1
- new release

* Sat Aug 17 2002 Bishop Clark (LC957) <bishop@platypus.bc.ca> 2.5-5
- fix GROUP for amanda's genhdlist and Michael Van Donselaar

* Tue Jun 5 2002 Bishop Clark (LC957) <bishop@platypus.bc.ca> 2.5-4
- Deprecated redundant directory creation in install
- More undisputed patches by Willems Luc for SuSE support
- Update of one SuSE config file, addition of another as per 
  Willems Luc

* Mon Jan 21 2002 Bishop Clark (LC957) <bishop@platypus.bc.ca> 2.5-3
- Macros updating as per 2.5 for better cross-distro build
- Added NO_USE_LZO compile option as per Willems Luc
- very initial SuSE 7.3 support as per Willems Luc
- removed packaging of vtun->vtund symlink in man8 as per build
  behaviour
- re-edited as per Jan 14 2002 edits

* Mon Jan 14 2002 Bishop Clark (LC957) <bishop@platypus.bc.ca> 2.5-2
- noreplace to vtund.conf to prevent Freshen from clobbering config.
- added buildrequires to prevent failed builds.

* Mon May 29 2000 Michael Tokarev <mjt@tls.msk.ru>
- Allow to build as non-root (using new INSTALL_OWNER option)
- Added vtund.conf.5 manpage
- Allow compressed manpages
- Added cleanup of old $RPM_BUILD_ROOT at beginning of %%install stage

* Sat Mar 04 2000 Dag Wieers <dag@mind.be> 
- Added USE_SOCKS compile option.
- Added Prefix-header

* Sat Jan 29 2000 Dag Wieers <dag@mind.be> 
- Replaced SSLeay-dependency by openssl-dependency
- Replaced README.Config by README.Setup
- Added TODO

* Tue Nov 23 1999 Dag Wieers <dag@mind.be> 
- Added Url and Obsoletes-headers
- Added ChangeLog ;)
- Changed summary
