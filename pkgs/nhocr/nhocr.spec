%global momorel 5

Summary: OCR
Name:    nhocr
Version: 0.16
Release: %{momorel}m%{?dist}
License: Apache
Group:   Applications/Text
URL:     http://code.google.com/p/nhocr/

Source0: http://nhocr.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: O2-tools >= 2.00

%description
NHocr is a command line OCR (Optical Character Recognition) program for Japanese language.
It has been designed to recognize machine-printed Japanese characters and some ASCII
characters/symbols in an image.
NHocr is probably the first Open Source Japanese OCR software,
except some experimental, partial codes open to the academic communities.

%prep
%setup -q

%build
%configure 
%make

%install
rm -rf %{buildroot}
%makeinstall datadir=%{buildroot}%{_datadir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/%{name}/*
%{_includedir}/*
%{_libdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-1m)
- initial package for Momonga
