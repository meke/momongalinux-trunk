%global momorel 3

Summary: monodevelop python plugin
Name: monodevelop-python
Version: 2.6.0.1
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: "BSDL like"
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk-sharp2-devel >= 2.12.9
BuildRequires: webkitgtk-devel >= 1.1.15.4
BuildRequires: mono-devel >= 2.6.7
BuildRequires: monodevelop >= 2.6.0.1
Requires: mono-core
Requires: gtk2
Requires: gtk-sharp2
Requires: monodevelop >= 2.4

%description
ValaBinding is a python language binding for MonoDevelop.

%prep
%setup -q

%build
./configure --prefix=%{_prefix} --libdir=%{_libdir}
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{_prefix}/lib/monodevelop/AddIns/PyBinding/PyBinding.dll
%{_libdir}/pkgconfig/monodevelop-pybinding.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0.1-3m)
- rebuild for mono-2.10.9

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0.1-2m)
- fix buildd failure on x86_64

* Fri Sep 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0.1-1m)
- initial build

