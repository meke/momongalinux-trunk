%global momorel 6

%define debug_package %{nil}
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-zope-filesystem
Version:        1
Release:        %{momorel}m%{?dist}
Summary:        Python-Zope Libraries Base Filesystem
Group:          Development/Languages
License:        "ZPLv2.1"
URL:            https://fedoraproject.org/wiki/SIGs/SciTech/SAGE
Source0:        python-zope-filesystem-__init__.py
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7


%description
This package contains the base filesystem layout for all Fedora
python-zope-* packages.


%prep
%setup -q -c -T
cp -p %{SOURCE0} __init__.py


%build
# nothing


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{python_sitelib}/zope
install -p -m 644 __init__.py $RPM_BUILD_ROOT%{python_sitelib}/zope
mkdir -p $RPM_BUILD_ROOT%{python_sitearch}/zope
install -p -m 644 __init__.py $RPM_BUILD_ROOT%{python_sitearch}/zope

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
# For noarch packages: sitelib
%{python_sitelib}/zope
%if "%{python_sitearch}" != "%{python_sitelib}"
# For arch-specific packages: sitearch
%{python_sitearch}/zope
%endif


%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-1m)
- import from Fedora 11

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Dec 17 2008 Conrad Meyer <konrad@tylerc.org> - 1-2
- Don't list files twice on non-lib64 platforms.
- Preserve timestamps.

* Sun Dec 14 2008 Conrad Meyer <konrad@tylerc.org> - 1-1
- Initial package.
