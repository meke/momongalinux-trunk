%global momorel 1

%global selinux_module 0
%global selinux_types %{nil}
%global selinux_variants %{nil}
%global selinux_buildreqs %{nil}

Name:		mod_fcgid
Version:	2.3.9
Release:	%{momorel}m%{?dist}
Summary:	FastCGI interface module for Apache 2
Group:		System Environment/Daemons
License:	ASL 2.0
URL:		http://httpd.apache.org/mod_fcgid/
Source0:        http://www.apache.org/dist/httpd/%{name}/%{name}-%{version}.tar.bz2
Source1:	fcgid.conf
Source2:	mod_fcgid-2.1-README.RPM
Source3:	mod_fcgid-2.1-README.SELinux
Source10:	fastcgi.te
Source11:	fastcgi-2.5.te
Source12:	fastcgi.fc
NoSource:       0
Patch0:		mod_fcgid-2.3.4-fixconf-shellbang.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	httpd-devel >= 2.4.3, pkgconfig
Requires:	httpd-mmn = %([ -a %{_includedir}/httpd/.mmn ] && %{__cat} %{_includedir}/httpd/.mmn || echo missing)

%description
mod_fcgid is a binary-compatible alternative to the Apache module mod_fastcgi.
mod_fcgid has a new process management strategy, which concentrates on reducing
the number of fastcgi servers, and kicking out corrupt fastcgi servers as soon
as possible.

%if %{selinux_module}
%global selinux_policyver %(%{__sed} -e 's,.*selinux-policy-\\([^/]*\\)/.*,\\1,' /usr/share/selinux/devel/policyhelp || echo 0.0.0)
%global selinux_policynum %(echo %{selinux_policyver} | %{__awk} -F. '{ printf "%d%02d%02d", $1, $2, $3 }')
%package selinux
Summary:	  SELinux policy module supporting FastCGI applications with mod_fcgid
Group:		  System Environment/Base
BuildRequires:	  %{selinux_buildreqs}
# selinux-policy is required for directory ownership of %{_datadir}/selinux/*
# Modules built against one version of a policy may not work with older policy
# versions, as noted on fedora-selinux-list:
# http://www.redhat.com/archives/fedora-selinux-list/2006-May/msg00102.html
# Hence the versioned dependency. The versioning will hopefully be replaced by
# an ABI version requirement or something similar in the future
Requires:	  selinux-policy >= %{selinux_policyver}
Requires:	  %{name} = %{version}-%{release}
Requires(post):	  /usr/sbin/semodule, /sbin/restorecon
Requires(postun): /usr/sbin/semodule, /sbin/restorecon

%description selinux
SELinux policy module supporting FastCGI applications with mod_fcgid.
%endif

%prep
%setup -q
%{__cp} -p %{SOURCE1} fcgid.conf
%{__cp} -p %{SOURCE2} README.RPM
%{__cp} -p %{SOURCE3} README.SELinux
%if 0%{?selinux_policynum} < 20501
%{__cp} -p %{SOURCE10} fastcgi.te
%else
%{__cp} -p %{SOURCE11} fastcgi.te
%endif
%{__cp} -p %{SOURCE12} fastcgi.fc

# Fix shellbang in fixconf script for our location of sed
%patch0 -p1

%build
APXS=%{_httpd_apxs} ./configure.apxs
%{__make}
%if %{selinux_module}
for selinuxvariant in %{selinux_variants}
do
	%{__make} NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile
	%{__mv} fastcgi.pp fastcgi.pp.${selinuxvariant}
	%{__make} NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile clean
done
%endif

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} MKINSTALLDIRS="%{__mkdir_p}" install
%{__install} -D -m 644 fcgid.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/fcgid.conf.dist
%{__install} -d -m 755 %{buildroot}%{_localstatedir}/run/mod_fcgid

# Include the manual as %doc, don't need it elsewhere
%{__rm} -rf %{buildroot}%{_var}/www/manual

# Install SELinux policy modules
%if %{selinux_module}
for selinuxvariant in %{selinux_variants}
do
	%{__install} -d %{buildroot}%{_datadir}/selinux/${selinuxvariant}
	%{__install} -p -m 644 fastcgi.pp.${selinuxvariant} \
		%{buildroot}%{_datadir}/selinux/${selinuxvariant}/fastcgi.pp
done
# Hardlink identical policy module packages together
/usr/sbin/hardlink -cv %{buildroot}%{_datadir}/selinux
%endif

%clean
%{__rm} -rf %{buildroot}

%if %{selinux_module}
%post selinux
# Install SELinux policy modules
for selinuxvariant in %{selinux_variants}
do
	/usr/sbin/semodule -s ${selinuxvariant} -i \
		%{_datadir}/selinux/${selinuxvariant}/fastcgi.pp &> /dev/null || :
done
# Fix up non-standard directory context from earlier packages
/sbin/restorecon -R %{_localstatedir}/run/mod_fcgid || :

%postun selinux
# Clean up after package removal
if [ $1 -eq 0 ]; then
	# Remove SELinux policy modules
	for selinuxvariant in %{selinux_variants}; do
		/usr/sbin/semodule -s ${selinuxvariant} -r fastcgi &> /dev/null || :
	done
	# Clean up any remaining file contexts (shouldn't be any really)
	[ -d %{_localstatedir}/run/mod_fcgid ] && \
		/sbin/restorecon -R %{_localstatedir}/run/mod_fcgid &> /dev/null || :
fi
exit 0
%endif

%files
%defattr(-,root,root,-)
# mod_fcgid.html.en is explicitly encoded as ISO-8859-1
%doc CHANGES-FCGID LICENSE-FCGID NOTICE-FCGID README-FCGID STATUS-FCGID
%doc docs/manual/mod/mod_fcgid.html.en modules/fcgid/ChangeLog
%doc build/fixconf.sed
%{_libdir}/httpd/modules/mod_fcgid.so
%config(noreplace) %{_sysconfdir}/httpd/conf.d/fcgid.conf.dist
%dir %attr(0755,apache,apache) %{_localstatedir}/run/mod_fcgid/
%{_datadir}/httpd/manual/mod/*

%if %{selinux_module}
%files selinux
%defattr(-,root,root,-)
%doc fastcgi.fc fastcgi.te README.SELinux
%{_datadir}/selinux/*/fastcgi.pp
%endif

%changelog
* Mon Oct 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.9-1m)
- [SECURITY] CVE-2013-4365
- update to 2.3.9

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.7-1m)
- update to 2.3.7
- rbuild against httpd-2.4.3

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.6-3m)
- [SECURITY] CVE-2012-1181

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.6-2m)
- rebuild for new GCC 4.6

* Fri Jan 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.6-1m)
- [SECURITY] CVE-2010-3872
- update to 2.3.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.5-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.5-1m)
- sync with Fedora 13 (2.3.5-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-5m)
- selinux_module is replaced by selinux-policy-targeted-3.6.12-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-4m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2-3m)
- rename fcgid.conf to fcgid.conf.dist

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-2m)
- modify Requires

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-1m)
- import from Fedora to Momonga

* Thu Feb 14 2008 Paul Howarth <paul@city-fan.org> 2.2-4
- Rebuild with gcc 4.3.0 for Fedora 9

* Mon Jan 14 2008 Paul Howarth <paul@city-fan.org> 2.2-3
- Update SELinux policy to fix occasional failures on restarts
  (move shared memory file into /var/run/mod_fcgid directory)

* Thu Jan  3 2008 Paul Howarth <paul@city-fan.org> 2.2-2
- Update SELinux policy to support file transition to httpd_tmp_t for
  temporary files

* Fri Sep 14 2007 Paul Howarth <paul@city-fan.org> 2.2-1
- Update to version 2.2
- Make sure docs are encoded as UTF-8

* Mon Sep  3 2007 Joe Orton <jorton@redhat.com> 2.1-6
- rebuild for fixed 32-bit APR (#254241)

* Thu Aug 23 2007 Paul Howarth <paul@city-fan.org> 2.1-5
- Update source URL to point to downloads.sf.net rather than dl.sf.net
- Upstream released new tarball without changing version number, though the
  only change was in arch/win32/fcgid_pm_win.c, which is not used to build the
  RPM package
- Clarify license as GPL (unspecified/any version)
- Unexpand tabs in spec
- Add buildreq of gawk

* Fri Aug  3 2007 Paul Howarth <paul@city-fan.org> 2.1-4
- Add buildreq of pkgconfig, a missing dependency of both apr-devel and
  apr-util-devel on FC5

* Fri Jun 15 2007 Paul Howarth <paul@city-fan.org> 2.1-3
- Major update of SELinux policy, supporting accessing data on NFS/CIFS shares
  and a new boolean, httpd_fastcgi_can_sendmail, to allow connections to SMTP
  servers
- Fix for SELinux policy on Fedora 7, which didn't work due to changes in the
  permissions macros in the underlying selinux-policy package

* Wed Mar 21 2007 Paul Howarth <paul@city-fan.org> 2.1-2
- Add RHEL5 with SELinux support
- Rename README.Fedora to README.RPM

* Fri Feb 16 2007 Paul Howarth <paul@city-fan.org> 2.1-1
- Update to 2.1
- Update documentation and patches
- Rename some source files to reduce chances of conflicting names
- Include SharememPath directive in conf file to avoid unfortunate upstream
  default location

* Mon Oct 30 2006 Paul Howarth <paul@city-fan.org> 2.0-1
- Update to 2.0
- Source is now hosted at sourceforge.net
- Update docs

* Wed Sep  6 2006 Paul Howarth <paul@city-fan.org> 1.10-7
- Include the right README* files

* Tue Aug 29 2006 Paul Howarth <paul@city-fan.org> 1.10-6
- Buildreqs for FC5 now identical to buildreqs for FC6 onwards

* Fri Jul 28 2006 Paul Howarth <paul@city-fan.org> 1.10-5
- Split off SELinux module into separate subpackage to avoid dependency on
  the selinux-policy package for the main package

* Fri Jul 28 2006 Paul Howarth <paul@city-fan.org> 1.10-4
- SELinux policy packages moved from %%{_datadir}/selinux/packages/POLICYNAME
  to %%{_datadir}/selinux/POLICYNAME
- hardlink identical policy module packages together to avoid duplicate files

* Thu Jul 20 2006 Paul Howarth <paul@city-fan.org> 1.10-3
- Adjust buildreqs for FC6 onwards
- Figure out where top_dir is dynamically since the /etc/httpd/build
  symlink is gone in FC6

* Wed Jul  5 2006 Paul Howarth <paul@city-fan.org> 1.10-2
- SELinux policy update: allow FastCGI apps to do DNS lookups

* Tue Jul  4 2006 Paul Howarth <paul@city-fan.org> 1.10-1
- Update to 1.10
- Expand tabs to shut rpmlint up

* Tue Jul  4 2006 Paul Howarth <paul@city-fan.org> 1.09-10
- SELinux policy update:
  * allow httpd to read httpd_fastcgi_content_t without having the
  | httpd_builtin_scripting boolean set
  * allow httpd_fastcgi_script_t to read /etc/resolv.conf without
  | having the httpd_can_network_connect boolean set

* Sun Jun 18 2006 Paul Howarth <paul@city-fan.org> 1.09-9
- Discard output of semodule in %%postun
- Include some documentation from upstream

* Fri Jun  9 2006 Paul Howarth <paul@city-fan.org> 1.09-8
- Change default context type for socket directory from var_run_t to
  httpd_fastcgi_sock_t for better separation

* Thu Jun  8 2006 Paul Howarth <paul@city-fan.org> 1.09-7
- Add SELinux policy module and README.Fedora
- Conflict with selinux-policy versions older than what we're built on

* Mon May 15 2006 Paul Howarth <paul@city-fan.org> 1.09-6
- Instead of conflicting with mod_fastcgi, don't add the handler for .fcg etc.
  if mod_fastcgi is present

* Fri May 12 2006 Paul Howarth <paul@city-fan.org> 1.09-5
- Use correct handler name in fcgid.conf
- Conflict with mod_fastcgi
- Create directory %%{_localstatedir}/run/mod_fcgid for sockets

* Thu May 11 2006 Paul Howarth <paul@city-fan.org> 1.09-4
- Cosmetic tweaks (personal preferences)
- Don't include INSTALL.TXT, nothing of use to end users

* Wed May 10 2006 Thomas Antony <thomas@antony.eu> 1.09-3
- Initial release
