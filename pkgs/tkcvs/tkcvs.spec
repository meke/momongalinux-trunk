%global momorel 1

%global majorver 8
%global minorver 2
%global patchver 3

%global ver %{majorver}.%{minorver}%{?patchver:.%{patchver}}
%global srcver %{majorver}_%{minorver}%{?patchver:_%{patchver}}

Summary: TkCVS - Tcl/Tk-based graphical interface to CVS
Name: tkcvs
Version: %{ver}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source0: http://www.twobarleycorns.net/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://www.twobarleycorns.net/tkcvs.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: tk >= 8.4, cvs >= 1.10
BuildRequires: tcl, perl
BuildArchitectures: noarch

%description
TkCVS is a Tcl/Tk-based graphical interface to the CVS and Subversion
configuration management systems. It will also help with RCS. TkDiff
is included for browsing and merging your changes.

TkCVS shows the status of the files in the current working directory,
and has tools for tagging, merging, importing, exporting, checking
in/out, and other user operations.

TkCVS also aids in browsing the repository. For Subversion, the
repository tree is browsed like an ordinary file tree. For CVS, the
CVSROOT/modules file is read. TkCVS extends CVS with a method to
produce a "user friendly" listing of modules. This requires special
comments in the CVSROOT/modules file.

Although TkCVS now supports Subversion, it will still work happily
without it in your CVS directories. It didn't abandon CVS, it just
grew some new capabilities.


%prep
%setup -q


%build
perl -pi -e 's|set TCDIR \[file join \$TclRoot tkcvs\]|set TCDIR "%{_datadir}/tkcvs"|' tkcvs/tkcvs.tcl
perl -pi -e 's|\[file join \$TclRoot tkcvs bitmaps\]|\[file join \$TCDIR bitmaps\]|' tkcvs/tkcvs.tcl


%install
install -d %{buildroot}%{_datadir}
install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_mandir}/man1
cd tkcvs
install -m 0755 tkcvs.tcl %{buildroot}%{_bindir}/tkcvs
rm -f tkcvs.blank mklocal mkmanpage.pl
install -m 0644 tkcvs.1 %{buildroot}%{_mandir}/man1
cd ../tkdiff
install -m 0755 tkdiff %{buildroot}%{_bindir}
cd ..
cp -fr tkcvs %{buildroot}%{_datadir}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root)
%doc CHANGELOG.txt FAQ.txt INSTALL LICENSE.txt
%{_datadir}/tkcvs
%{_bindir}/*
%{_mandir}/man1/*


%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.3-1m)
- update to 8.2.3

* Sat Aug 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.2-1m)
- update to 8.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.2-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-1m)
- update to 8.2
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.1-1m)
- update to 8.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.3-2m)
- rebuild against gcc43

* Thu Jun 29 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.3-1m)
- upgrade to 8.0.3

* Sun May 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.2.1-1m)
- upgrade to 7.2.1
- gone away README.tkcvs 

* Sat Dec  6 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (7.1.4-1m)
- upgrade to 7.1.4

* Wed May 14 2003 TAKAHASHI Tamotsu <tamo>
- (7.1.2-2m)
- correct patch0
  /usr/share/man/share/man1 -> /usr/share/man/man1

* Tue Dec 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (7.1.2-1m)
- update to 7.1.2 

* Thu Feb  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (7.0.3-2k)

* Sat Jan 19 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (7.0.2-2k)
