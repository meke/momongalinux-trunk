%global momorel 7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           fontypython
Version:        0.3.6
Release:        %{momorel}m%{?dist}
Summary:        TTF font manager

Group:          Applications/Multimedia
License:        GPLv2+
URL:            https://savannah.nongnu.org/projects/fontypython/
Source0:        http://download.savannah.nongnu.org/releases/fontypython/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7 , wxPython, python-imaging, desktop-file-utils
Requires:       wxPython, python-imaging

%description
Manage your ttf fonts on Gnu/Linux with Fonty Python.
    
You can collect any fonts together (even ones not in your system font folders)
into 'pogs' and then install and remove the pogs as you need them.
    
In this way you can control what fonts are in your user font folder, thus
avoiding long lists of fonts in the font chooser dialogues of your apps.



%prep
%setup -q


%build
python setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
sed -i 's|fontypython.png|fontypython|g' ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
desktop-file-install --vendor=                                  \
        --dir=${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --delete-original                                       \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
mv ${RPM_BUILD_ROOT}%{_bindir}/fp ${RPM_BUILD_ROOT}%{_bindir}/fontypython

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/fontypython
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/fontypython.png
%doc README COPYING 
%dir %{python_sitelib}/fontypythonmodules/
%{python_sitelib}/fontypythonmodules/*.py*
%{python_sitelib}/fontypythonmodules/help/
%{python_sitelib}/fontypythonmodules/pofiles/
%{python_sitelib}/fontypythonmodules/things/
%dir %{python_sitelib}/fontypythonmodules/locale/
%lang(fr) %{python_sitelib}/fontypythonmodules/locale/fr/
%lang(it) %{python_sitelib}/fontypythonmodules/locale/it/
%{python_sitelib}/%{name}-%{version}-py*.egg-info


%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.6-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-2m)
- remove fedora from --vendor

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.6-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.3.6-2
- Rebuild for Python 2.6

* Mon Sep 15 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.3.6-1
- update to 0.3.6

* Thu May 22 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.2.0-7
- fix license tag

* Tue Dec 20 2006 Chris Mohler <cr33dog@gmail.com> 0.2.0-6
- updated .desktop file - added correct executable name

* Mon Dec 19 2006 Chris Mohler <cr33dog@gmail.com> 0.2.0-5
- Renamed executable 'fp' to 'fontypython' to resolve bug #220046

* Thu Dec 14 2006 Chris Mohler <cr33dog@gmail.com> 0.2.0-4
- Corrected Source0 to include URL
- removed python >= 2.4 from Requires
 
* Tue Dec 12 2006 Chris Mohler <cr33dog@gmail.com> 0.2.0-3
- removed rpmdevtools from BuildRequires
- added python >= 2.4, wxPython, python-imaging to Requires
- removed 'Application;' from .desktop file
- shortened line length of description

* Mon Dec 11 2006 Chris Mohler <cr33dog@gmail.com> 0.2.0-2
- Added wxPython, python-imaging to build requires
- Removed copyright notice from description

* Sat Dec 09 2006 Chris Mohler <cr33dog@gmail.com> 0.2.0-1
- Created spec file
- Prepared RPM for Fedora Extras
