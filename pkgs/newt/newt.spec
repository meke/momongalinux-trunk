%global momorel 1

%global pythonver 2.7
%global python3ver 3.4

Summary: A development library for text mode user interfaces
Name: newt
Version: 0.52.17
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: System Environment/Libraries
URL: https://fedorahosted.org/newt/
Source0: https://fedorahosted.org/releases/n/e/newt/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: newt-0.52.14-no_python31.patch
BuildRequires: python,python-devel >= %{pythonver} ,perl, slang-devel >= 2.0.7-1m
#BuildRequires: tcl-devel >= 8.5.2
Requires: slang >= 2.0.7-1m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Newt is a programming library for color text mode, widget based user
interfaces.  Newt can be used to add stacked windows, entry widgets,
checkboxes, radio buttons, labels, plain text fields, scrollbars,
etc., to text mode user interfaces.  This package also contains the
shared library needed by programs built with newt, as well as a
/usr/bin/dialog replacement called whiptail.  Newt is based on the
slang library.

%package python
Summary: A development library for text mode user interfaces
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: snack
Requires: python >= %{pythonver}

%description python
Newt is a programming library for color text mode, widget based user
interfaces.  Newt can be used to add stacked windows, entry widgets,
checkboxes, radio buttons, labels, plain text fields, scrollbars,
etc., to text mode user interfaces.  This package also contains the
shared library needed by programs built with newt, as well as a
/usr/bin/dialog replacement called whiptail.  Newt is based on the
slang library.

%package python3
Summary: A development library for text mode user interfaces
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python3 >= %{pythonver}

%description python3
Newt is a programming library for color text mode, widget based user
interfaces.  Newt can be used to add stacked windows, entry widgets,
checkboxes, radio buttons, labels, plain text fields, scrollbars,
etc., to text mode user interfaces.  This package also contains the
shared library needed by programs built with newt, as well as a
/usr/bin/dialog replacement called whiptail.  Newt is based on the
slang library.

%package devel
Summary: Newt windowing toolkit development files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The newt-devel package contains the header files and libraries
necessary for developing applications which use newt.  Newt is a
development library for text mode user interfaces.  Newt is based on
the slang library.

Install newt-devel if you want to develop applications which will use
newt.

%package static
Summary: Newt windowing toolkit static library
Requires: %{name}-devel = %{version}-%{release}
Group: Development/Libraries

%description static
The newt-static package contains the static version of the newt library.
Install it if you need to link statically with libnewt.

%prep
%setup -q
#%%patch0 -p1 -b .no_python31

%build
# gpm support seems to smash the stack w/ we use help in anaconda??
#./configure --with-gpm-support
autoconf
%configure  --without-tcl
make RPM_OPT_FLAGS="`echo $RPM_OPT_FLAGS | sed -e 's|fstack|fno-stack|'`" depend
make RPM_OPT_FLAGS="`echo $RPM_OPT_FLAGS | sed -e 's|fstack|fno-stack|'`" all
chmod 0644 peanuts.py popcorn.py

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
%makeinstall

/sbin/ldconfig -n $RPM_BUILD_ROOT/%{_libdir}

python -c 'from compileall import *; compile_dir("'$RPM_BUILD_ROOT'%{_libdir}/python%{pythonver}",10,"%{_libdir}/python%{pythonver}")'

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel -p /sbin/ldconfig

%postun devel -p /sbin/ldconfig

%files -f %{name}.lang
%defattr (-,root,root)
%doc COPYING
%{_bindir}/whiptail
%{_libdir}/libnewt.so.*
#%%{_libdir}/whiptcl.so
%{_mandir}/man1/whiptail.1*

%files devel
%defattr (-,root,root)
%doc tutorial.sgml peanuts.py popcorn.py
%{_includedir}/newt.h
%{_libdir}/libnewt.so
%{_libdir}/pkgconfig/libnewt.pc

%files static
%defattr(-,root,root)
%{_libdir}/libnewt.a

%files python
%defattr (-,root,root)
%{_libdir}/python%{pythonver}/site-packages/*.so*
%{_libdir}/python%{pythonver}/site-packages/*py*

%files python3
%defattr (-,root,root)
%{_libdir}/python%{python3ver}/site-packages/*.so*
%{_libdir}/python%{python3ver}/site-packages/*py*

%changelog
* Wed Apr 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.17-1m)
- update 0.52.17

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.52.14-1m)
- update 0.52.14

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.52.13-1m)
- update 0.52.13

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.52.12-2m)
- rebuild against python-2.7

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.52.12-1m)
- update 0.52.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.52.11-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.52.11-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.11-3m)
- full rebuild for mo7 release

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.52.11-2m)
- add patch0 (not build python3.1)

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.11-1m)
- update 0.52.11

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.52.10-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.52.10-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.52.10-2m)
- [SECURITY] CVE-2009-2905
- import a security patch (Patch1) from Fedora 11 (0.52.10-4)

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.52.10-1m)
- update 0.52.10
- remove tcl wrapper

* Fri Mar 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.52.9-5m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.52.9-4m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.52.9-3m)
- rebuild agaisst python-2.6.1-1m

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.52.9-2m)
- rebuild against tcl-8.5.2

* Fri Apr 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.9-1m)
- update 0.52.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.52.8-3m)
- rebuild against gcc43

* Tue Feb 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.52.8-2m)
- remove %%{_libdir}/libnewt.a from newt-devel

* Mon Feb 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.8-1m)
- update 0.52.8

* Wed Mar  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.6-1m)
- update 0.52.6

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.5-1m)
- update 0.52.5

* Tue Jan 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.4-2m)
- add newt-objfree.patch
-- fix "glibc detected" error

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.4-1m)
- update 0.52.4

* Sun May 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.52.2-2m)
- rebuild against slang-2.0.5

* Thu May 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52.2-1m)
- update 0.52.2

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.51.6-7m)
- rebuild against python-2.4.2

* Fri Feb 18 2005 Toru Hoshina <t@momonga-linux.org>
- (0.51.6-6m)
- revised spec so the static library avoid stack protection.

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51.6-5m)
- rebuild against python2.3

* Fri Aug 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.51.6-4m)
  added libnewt-nossp.a

* Sat Apr 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.51.6-3m)
- R.I.P. minislang.

* Thu Mar  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.51.6-1m)
- upgrade 0.51.6 again.

* Sun Dec 21 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.50.35-6m)
- version down

* Wed Dec  3 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51.6-1m)
- upgrade 0.51.6

* Fri Nov 07 2003 Kenta MURATA <muraken2@nifty.com>
- (0.50.35-5m)
- separate python package.

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.50.35-4m)
- accept python not only 2.2 but 2.2.*

* Thu Nov 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.50.35-3m)
- remove python1 dependency

* Sun Apr  7 2002 Toru Hoshina <t@kondara.org>
- (0.50.35-2k)
- ver up.

* Fri Apr  5 2002 Toru Hoshina <t@kondara.org>
- (0.50.32-16k)
- python 1.5 and 2.2 support. uwa dassa-.

* Mon Apr  1 2002 Toru Hoshina <t@kondara.org>
- (0.50.32-14k)
- add %{buildpython2} flag.

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.50.32-12k)
- python-2.2

* Fri Dec 21 2001 Toru Hoshina <toru@df-usa.com>
- (0.50.32-10k)
- suma-so.

* Thu Dec 20 2001 Toru Hoshina <toru@df-usa.com>
- (0.50.32-8k)
- not depend on slang anymore...

* Thu Dec 20 2001 Toru Hoshina <toru@df-usa.com>
- (0.50.32-6k)
- avoid corrupted form...

* Fri Nov  9 2001 Toru Hoshina <toru@df-usa.com>
- (0.50.32-4k)
- fixed ja_JP.EUC-JP issue, and some termincal related issue.

* Fri Sep 14 2001 Toru Hoshina <toru@df-usa.com>
- (0.50.32-2k)

* Wed Aug 22 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.50.32-1
- re-ordered the width key of CheckboxTree.__init__; #52319

* Wed Aug  8 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.50.31-1
- right anchor the internal Listbox of CListboxes, so that empty
- scrollable CListboxes do not look like crape.

* Thu Jul 05 2001 Crutcher Dunnavant <crutcher@redhat.com>
- padded hidden checkboxes on CheckboxTrees

* Thu Jul 05 2001 Crutcher Dunnavant <crutcher@redhat.com>
- taught CheckboxTrees about width. Whohoo! 2-D!!!

* Thu Jul 05 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added 'hide_checkbox' and 'unselectable' options to CheckboxTrees

* Mon Jun 25 2001 Jeremy Katz <katzj@redhat.com>
- CListBox -> CListbox for API consistency
- fixup replace() method of CListbox

* Fri Jun 8 2001 Jeremy Katz <katzj@redhat.com>
- few bugfixes to the CListBox

* Fri Jun 8 2001 Jeremy Katz <katzj@redhat.com>
- added python binding for newtListboxClear() for Listbox and CListBox
- let ButtonBars optionally be made of CompactButtons

* Wed Jun  6 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added CListBox python convenience class

* Tue May 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.50.22-8k)
- built with minislang again on Alpha Architecture

* Tue May 15 2001 Michael Fulbright <msf@redhat.com>
- added python binding for CompactButton()

* Tue May 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.50.22-6k)
- errased minislang on Alpha architecture, becuase installer does not work
   correctly

* Sun May  6 2001 Toru Hoshina <toru@df-usa.com>
- (0.50.22-4k)
- include minislang 1.4.2 instead of using slang-ja.

* Wed Apr 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.50.22-2k)
- import from RHL 0.50.22-2

* Tue Apr  3 2001 Matt Wilson <msw@redhat.com>
- change from using SLsmg_touch_screen to SLsmg_touch_lines to prevent
  excessive flashing due to screen clears when using touch_screen (more
  Japanese handling)

* Mon Apr  2 2001 Matt Wilson <msw@redhat.com>
- redraw the screen in certain situations when LANG=ja_JP.eucJP to
  prevent corrupting kanji characters (#34362)

* Mon Apr  2 2001 Elloit Lee <sopwith@redhat.com>
- Allow python scripts to watch file handles
- Fix 64-bit warnings in snackmodule
- Misc snack.py cleanups
- Add NEWT_FD_EXCEPT to allow watching for fd exceptions
- In newtExitStruct, return the first file descriptor that an event occurred on 

* Fri Mar 30 2001 Matt Wilson <msw@redhat.com>
- don't blow the stack if we push a help line that is longer than the
  curret number of columns
- clip window to screen bounds so that if we get a window that is
  larger than the screen we can still redraw the windows behind it
  when we pop

* Sun Feb 11 2001 Than Ngo <than@redhat.com>
- disable building new-python2 sub package again

* Thu Feb 01 2001 Erik Troan <ewt@redhat.com>
- gave up on separate CHANGES file
- added newtCheckboxTreeSetCurrent() and snack binding
- don't require python2 anymore

* Mon Jan 22 2001 Than Ngo <than@redhat.com>
- don't build newt-python2 sub package.

* Fri Dec 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_tmppath}
- add python2 subpackage, with such support
- fix use of append in snack.py

* Fri Sep 08 2000 Trond Eivind Glomsrod <teg@redhat.com>
- bytecompile the snack python module
- move the libnewt.so symlink to the devel package
- include popcorn.py and peanuts.py in the devel package,
  so we have some documentation of the snack module

* Tue Aug 22 2000 Erik Troan <ewt@redhat.com>
- fixed cursor disappearing in suspend (again)

* Sat Aug 19 2000 Preston Brown <pbrown@redhat.com>
- explicit requirement of devel subpackage on same version of main package
  so that .so file link doesn't break

* Wed Aug 16 2000 Erik Troan <ewt@redhat.com>
- fixed cursor disappearing in suspend
- moved libnewt.so to main package from -devel

* Thu Aug  3 2000 Matt Wilson <msw@redhat.com>
- added setValue method for checkboxes in snack

* Wed Jul 05 2000 Michael Fulbright <msf@redhat.com>
- added NEWT_FLAG_PASSWORD for entering passwords and having asterix echo'd

* Fri Jun 16 2000 Matt Wilson <msw@redhat.com>
- build for new release

* Fri Apr 28 2000 Jakub Jelinek <jakub@redhat.com>
- see CHANGES

* Mon Mar 13 2000 Matt Wilson <msw@redhat.com>
- revert mblen patch, go back to our own wide char detection

* Fri Feb 25 2000 Bill Nottingham <notting@redhat.com>
- fix doReflow to handle mblen returning 0

* Wed Feb 23 2000 Preston Brown <pbrown@redhat.com>
- fix critical bug in fkey 1-4 recognition on xterms

* Wed Feb  9 2000 Matt Wilson <msw@redhat.com>
- fixed snack widget setcallback function

* Thu Feb 03 2000 Erik Troan <ewt@redhat.com>
- strip shared libs

* Mon Jan 31 2000 Matt Wilson <msw@redhat.com>
- added patch from Toru Hoshina <t@kondara.org> to improve multibyte
  character wrapping

* Thu Jan 20 2000 Erik Troan <ewt@redhat.com>
- see CHANGES

* Thu Jan 20 2000 Preston Brown <pbrown@redhat.com>
- fix segfault in newtRadioGetCurrent

* Mon Jan 17 2000 Erik Troan <ewt@redhat.com>
- added numerous bug fixes (see CHANGES)

* Mon Dec 20 1999 Matt Wilson <msw@redhat.com>
- rebuild with fix for listbox from Nalin

* Wed Oct 20 1999 Matt Wilson <msw@redhat.com>
- added patch to correctly wrap euc kanji

* Wed Sep 01 1999 Erik Troan <ewt@redhat.com>
- added suspend/resume to snack

* Tue Aug 31 1999 Matt Wilson <msw@redhat.com>
- enable gpm support

* Fri Aug 27 1999 Matt Wilson <msw@redhat.com>
- added hotkey assignment for gridforms, changed listbox.setcurrent to
  take the item key

* Wed Aug 25 1999 Matt Wilson <msw@redhat.com>
- fixed snack callback function refcounts, as well as optional data args
- fixed suspend callback ref counts

* Mon Aug 23 1999 Matt Wilson <msw@redhat.com>
- added buttons argument to entrywindow

* Thu Aug 12 1999 Bill Nottingham <notting@redhat.com>
- multi-state checkboxtrees. Woohoo.

* Mon Aug  9 1999 Matt Wilson <msw@redhat.com>
- added snack wrappings for checkbox flag setting

* Thu Aug  5 1999 Matt Wilson <msw@redhat.com>
- added snack bindings for setting current listbox selection
- added argument to set default selection in snack ListboxChoiceWindow

* Mon Aug  2 1999 Matt Wilson <msw@redhat.com>
- added checkboxtree
- improved snack binding

* Fri Apr  9 1999 Matt Wilson <msw@redhat.com>
- fixed a glibc related bug in reflow that was truncating all text to 1000
chars

* Fri Apr 09 1999 Matt Wilson <msw@redhat.com>
- fixed bug that made newt apps crash when you hit <insert> followed by lots
of keys

* Mon Mar 15 1999 Matt Wilson <msw@redhat.com>
- fix from Jakub Jelinek for listbox keypresses

* Fri Feb 27 1999 Matt Wilson <msw@redhat.com>
- fixed support for navigating listboxes with alphabetical keypresses

* Thu Feb 25 1999 Matt Wilson <msw@redhat.com>
- updated descriptions
- added support for navigating listboxes with alphabetical keypresses

* Mon Feb  8 1999 Matt Wilson <msw@redhat.com>
- made grid wrapped windows at least the size of their title bars

* Fri Feb  5 1999 Matt Wilson <msw@redhat.com>
- Function to set checkbox flags.  This will go away later when I have
  a generic flag setting function and signals to comps to go insensitive.

* Tue Jan 19 1999 Matt Wilson <msw@redhat.com>
- Stopped using libgpm, internalized all gpm calls.  Still need some cleanups.

* Thu Jan  7 1999 Matt Wilson <msw@redhat.com>
- Added GPM mouse support
- Moved to autoconf to allow compiling without GPM support
- Changed revision to 0.40

* Wed Oct 21 1998 Bill Nottingham <notting@redhat.com>
- built against slang-1.2.2

* Wed Aug 19 1998 Bill Nottingham <notting@redhat.com>
- bugfixes for text reflow
- added docs

* Fri May 01 1998 Cristian Gafton <gafton@redhat.com>
- devel package moved to Development/Libraries

* Thu Apr 30 1998 Erik Troan <ewt@redhat.com>
- removed whiptcl.so -- it should be in a separate package

* Mon Feb 16 1998 Erik Troan <ewt@redhat.com>
- added newtWinMenu()
- many bug fixes in grid code

* Wed Jan 21 1998 Erik Troan <ewt@redhat.com>
- removed newtWinTernary()
- made newtWinChoice() return codes consistent with newtWinTernary()

* Fri Jan 16 1998 Erik Troan <ewt@redhat.com>
- added changes from Bruce Perens
    - small cleanups
    - lets whiptail automatically resize windows
- the order of placing a grid and adding components to a form no longer
  matters
- added newtGridAddComponentsToForm()

* Wed Oct 08 1997 Erik Troan <ewt@redhat.com>
- added newtWinTernary()

* Tue Oct 07 1997 Erik Troan <ewt@redhat.com>
- made Make/spec files use a buildroot
- added grid support (for newt 0.11 actually)

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- Added patched from Clarence Smith for setting the size of a listbox
- Version 0.9

* Tue May 28 1997 Elliot Lee <sopwith@redhat.com> 0.8-2
- Touchups on Makefile
- Cleaned up NEWT_FLAGS_*

* Tue Mar 18 1997 Erik Troan <ewt@redhat.com>
- Cleaned up listbox
- Added whiptail
- Added newtButtonCompact button type and associated colors
- Added newtTextboxGetNumLines() and newtTextboxSetHeight()

* Tue Feb 25 1997 Erik Troan <ewt@redhat.com>
- Added changes from sopwith for C++ cleanliness and some listbox fixes.
