%global momorel 19

Summary: A Gnome Vector Graphics Application
Name: sodipodi
Version: 0.34
Release: %{momorel}m%{?dist}
License: GPLv2+ or LGPLv2+
Group: Applications/Multimedia
URL: http://sourceforge.net/projects/sodipodi/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.30.1-fc.patch
Patch1: %{name}-%{version}-desktop.patch
Patch2: sodipodi-0.34-freetype2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeprintui22-devel >= 1.116.0
BuildRequires: freetype-devel
%description
Sodipodi is a gneneral vector drawing program for GNOME environment.

%prep
%setup -q

# %%patch0 -p1
%patch1 -p1 -b .fix-menu
%patch2 -p1 -b .freetype2~

%build
%configure LIBS="-lm -lgmodule-2.0"
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/locale/*/*/%{name}.mo
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/%{name}

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.34-19m)
- fix build failure

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-18m)
- change primary site and download URIs

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.34-17m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.34-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.34-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.34-14m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.34-13m)
- fix build

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.34-12m)
- fix up desktop file

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.34-11m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.34-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.34-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.34-8m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.34-7m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.34-6m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+ or LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.34-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.34-4m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>-
- (0.34-3m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.34-2m)
- rebuild against expat-2.0.0-1m

* Sat Feb 14 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.34-1m)
- update to 0.34

* Fri Dec 12 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.33-1m)
- update to 0.33
- change URL

* Thu Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (0.32-1m)
- pretty spec file.

* Sat Jun 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.32-1m)
- version 0.32

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.30.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnslaias.org>
- (0.30.1-1m)
- version 0.30.1

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.27-1m)
- version 0.27

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.24.1-14k)
- BuildPreReq:	imlib-devel >= 1.9.14-4k

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.24.1-12k)
- rebuild against libpng 1.2.2.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>   
- (0.24.1-8k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Sat Jan 12 2002 OZAWA -Crouton- Skauro <crouton@kondara.org>
- (0.24.1-6k)
- s/Copyright/License/

* Fri Nov 30 2001 Shingo Akagaki <dora@kondara.org>
- (0.24.1-4k)
- configuration changed

* Thu Nov 22 2001 Shingo Akagaki <dora@kondara.org>
- (0.24.1-2k)
- create spec
