%global         momorel 1

Name:           perl-NetAddr-IP
Version:        4.075
Release:        %{momorel}m%{?dist}
Summary:        Manages IPv4 and IPv6 addresses and subnets
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/NetAddr-IP/
Source0:        http://www.cpan.org/authors/id/M/MI/MIKER/NetAddr-IP-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides an object-oriented abstraction on top of IP addresses
or IP subnets, that allows for easy manipulations. Version 4.xx of
NetAdder::IP will will work older versions of Perl and does not use
Math::BigInt as in previous versions.

%prep
%setup -q -n NetAddr-IP-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Artistic Changes Copying TODO
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/NetAddr*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.075-1m)
- rebuild against perl-5.20.0
- update to 4.075

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.073-1m)
- update to 4.073

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.072-1m)
- update to 4.072
- rebuild against perl-5.18.2

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.071-1m)
- update to 4.071

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.070-1m)
- update to 4.070

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.069-2m)
- rebuild against perl-5.18.1

* Sun May 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.069-1m)
- update to 4.069

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.068-2m)
- rebuild against perl-5.18.0

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.068-1m)
- update to 4.068

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.067-1m)
- update to 4.067

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.066-2m)
- rebuild against perl-5.16.3

* Thu Dec 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.066-1m)
- update to 4.066

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.065-2m)
- rebuild against perl-5.16.2

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.065-1m)
- update to 4.065

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.064-1m)
- update to 4.064

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.062-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.062-1m)
- update to 4.062
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.059-1m)
- update to 4.059

* Sat Nov 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.058-1m)
- update to 4.058

* Thu Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.056-1m)
- update to 4.056

* Sat Oct 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.055-1m)
- update to 4.055

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.054-1m)
- update to 4.054

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.053-1m)
- update to 4.053

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.052-1m)
- update to 4.052

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.051-1m)
- update to 4.051

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.050-1m)
- update to 4.050

* Thu Oct 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.049-1m)
- update to 4.049

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.048-1m)
- update to 4.048

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.047-1m)
- update to 4.047

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.044-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.044-2m)
- rebuild against perl-5.14.1

* Thu May 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.044-1m)
- update to 4.044

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.043-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.043-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.043-1m)
- update to 4.043

* Wed Mar 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.042-1m)
- update to 4.042

* Wed Mar  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.041-1m)
- update to 4.041

* Thu Feb 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.039-1m)
- update to 4.039

* Sat Dec 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.038-1m)
- update to 4.038

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.037-2m)
- rebuild for new GCC 4.5

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.037-1m)
- update to 4.037

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.036-1m)
- update to 4.036

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.035-1m)
- update to 4.035

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.034-1m)
- update to 4.034

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.033-1m)
- update to 4.033

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.032-1m)
- update to 4.032

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.030-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.030-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.030-1m)
- update to 4.030

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.029-1m)
- update to 4.029

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.028-2m)
- rebuild against perl-5.12.1

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.028-1m)
- update to 4.028

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.027-5m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.027-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.027-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.027-2m)
- rebuild against perl-5.10.1

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.027-1m)
- update to 4.027

* Sun Mar  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.025-1m)
- update to 4.025

* Tue Feb 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.024-1m)
- update to 4.024

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.023-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.023-1m)
- update to 4.023

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
= (4.022-1m)
- update to 4.022

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.020-1m)
- update to 4.020

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.019-1m)
- update to 4.019

* Mon Nov 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.017-1m)
- update to 4.017

* Mon Nov  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.015-1m)
- update to 4.015

* Mon Nov  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.014-1m)
- update to 4.014

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.013-1m)
- update to 4.013

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.012-1m)
- update to 4.012

* Sun Oct 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.011-1m)
- update to 4.011

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.007-2m)
- rebuild against gcc43

* Sun Jul 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.007-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
