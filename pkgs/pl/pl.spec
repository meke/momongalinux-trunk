%global momorel 2
%define with_java 1
%define separate_xpce 0

%define jdkvershort 1.6.0
%define jdkverlong %{jdkvershort}.0

Name:		pl
Version:	5.10.5
Release:	%{momorel}m%{?dist}

Summary:	SWI-Prolog - Edinburgh compatible Prolog compiler

Group:		Development/Languages
#packages/clib/random.c         Artistic
#packages/clib/uri.c            LGPLv2+
#packages/sgml/Test/test.pl     LGPLv2
#library/qsave.pl               GPLv2+
#library/COPYING                GPLv2 with SWI-Prolog extra clause
#library/unicode/blocks.pl      UCD
#packages/http/examples/calc.pl Public Domain
License:	"Artistic and LGPLv2+ and LGPLv2 and GPLv2 and GPLv2+ and UCD and Public Domain"
URL:		http://www.swi-prolog.org
Source0:	http://www.swi-prolog.org/download/stable/src/%{name}-%{version}.tar.gz
NoSource:	0
Source1:	http://www.swi-prolog.org/download/stable/doc/SWI-Prolog-%{version}.pdf
NoSource:	1
Source2:	http://www.swi-prolog.org/download/xpce/doc/userguide/userguide.html.tgz
NoSource:	2
Patch1: 	%{name}-5.10.5-jpl-configure.patch
Patch2: 	%{name}-5.10.5-man-files.patch
Patch3: 	%{name}-5.10.5-jni.patch
Patch4: 	%{name}-5.10.5-pc.patch
# Upstream bug #9, will be in 5.10.6, rhbz#732952
Patch5:	       xpce-5.10.5-SECURITY-Bug-9-Loading-incomplete-GIF-files-causes-a.patch
# Upstream bug #9, will be in 5.10.6, rhbz#732952
Patch6:	       xpce-5.10.5-SECURITY-Make-sure-all-pixels-are-within-the-allocat.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Base
BuildRequires:  gmp-devel
BuildRequires:  ncurses-devel
BuildRequires:  pkgconfig
BuildRequires:  readline-devel
# XPCE
BuildRequires:  libICE-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libSM-devel
BuildRequires:  libX11-devel
BuildRequires:  libXft-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXpm-devel
BuildRequires:  libXt-devel
# Freetype support in XPCE
BuildRequires:  freetype-devel
BuildRequires:  libXaw-devel
BuildRequires:  libXext-devel
BuildRequires:  libXmu-devel
BuildRequires:  libXrender-devel
# ODBC
BuildRequires:  unixODBC-devel
# SSL
BuildRequires:  openssl-devel
# jpl
%if %{with_java}
BuildRequires:  jpackage-utils
BuildRequires:  java-%{jdkvershort}-devel = %{jdkverlong}
%endif
# zlib
BuildRequires:  zlib-devel
# helpers for export_dynamic patch
BuildRequires: autoconf
Provides:       %{_bindir}/swipl

%description
ISO/Edinburgh-style Prolog compiler including modules, auto-load,
libraries, Garbage-collector, stack-expandor, C/C++-interface,
GNU-readline interface, very fast compiler.  Including packages clib
(Unix process control and sockets), cpp (C++ interface), sgml (reading
XML/SGML), sgml/RDF (reading RDF into triples) and XPCE (Graphics UI
toolkit, integrated editor (Emacs-clone) and source-level debugger).


%package devel
Summary: Development files for SWI Prolog
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: readline-devel, pkgconfig

%description devel
Development files for SWI Prolog.


%package static
Summary: Static library for SWI Prolog
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: readline-devel

%description static
Static library for SWI Prolog.


%if %{separate_xpce}
%package xpce
Summary: A toolkit for developing graphical applications in Prolog
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description xpce
XPCE is a toolkit for developing graphical applications in Prolog and other
interactive and dynamically typed languages. XPCE follows a rather unique
approach of for developing GUI applications, as follows:

- Add object layer to Prolog
- High level of abstraction
- Exploit rapid Prolog development cycle
- Platform independent programs
%endif


%if %{with_java}
%package jpl
Summary: A bidirectional Prolog/Java interface for SWI Prolog
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: java
Requires: jpackage-utils

%description jpl
JPL is a library using the SWI-Prolog foreign interface and the Java Native
Interface providing a bidirectional interface between Java and Prolog
that can be used to embed Prolog in Java as well as for embedding Java
in Prolog. In both setups it provides a re-entrant bidirectional interface.
%endif


%prep
%global docdir doc-install
%setup -q
%patch1 -p1 -b .libjvm
%patch2 -p1 -b .man-files
%patch3 -p1 -b .jni
%patch4 -p1 -b .pc
(
cd packages/xpce
%patch5 -p1 -b .incomplete_gif
%patch6 -p1 -b .validate_pixel_color
)
(
   cd src
   autoconf
)
(
   mkdir %{docdir}
   cp -p %{SOURCE1} %{docdir}
)
(
   mkdir %{docdir}-xpce
   cd %{docdir}-xpce
   tar -xzf %{SOURCE2}
   mv UserGuide xpce-UserGuide
)

# Adjustments to take into account the new location of JNI stuff
sed --in-place=.jni2 -e 's#LIBDIR#%{_libdir}#g' packages/jpl/jpl.pl
sed --in-place=.jni2 -e 's#LIBDIR#%{_libdir}#g' packages/jpl/src/java/jpl/fli/Prolog.java


%build
%if %{with_java}
export JAVA_HOME=/usr/lib/jvm/java-%{jdkvershort}-openjdk-%{jdkverlong}.%{_arch}
%ifarch x86_64
export JAVA_LIB=$JAVA_HOME/jre/lib/amd64
%else
%ifarch sparcv9
export JAVA_HOME=/usr/lib/jvm/java-%{jdkvershort}-openjdk-%{jdkverlong}
export JAVA_LIB=$JAVA_HOME/jre/lib/sparc
%else
%ifarch sparc64
export JAVA_LIB=$JAVA_HOME/jre/lib/sparcv9
%else
export JAVA_LIB=$JAVA_HOME/jre/lib/%{_arch}
%endif
%endif
%endif
%else
# Processed by packages/configure
export DISABLE_PKGS="jpl"
%endif

# Build interpreter needed for SWI packages compilation
%configure --enable-shared LDFLAGS="-Wl,--enable-new-dtags"
make COFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"

# Build SWI packages
pushd packages
%configure LDFLAGS="-Wl,--enable-new-dtags"
make COFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing" JAVALIBS="-L$JAVA_LIB/server -L$JAVA_LIB -ljava -lverify -ljvm"
popd


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
# Library must be executable to get its debuginfo
chmod 0755 %{buildroot}%{_libdir}/swipl-%{version}/lib/*/libswipl.so.*
# Script with shebang should be executable
chmod 0755 \
%{buildroot}%{_libdir}/swipl-%{version}/library/dialect/sicstus/swipl-lfr.pl

pushd packages
make install DESTDIR=%{buildroot}
# Do not chmod earlier, the run.sh's are executed
chmod -x jpl/examples/java/*/run.sh
popd

%if %{with_java}
# Move the JPL JNI stuff to where the Java packaging guidelines 
# say it should be
mkdir -p %{buildroot}%{_libdir}/swipl-jpl
mv %{buildroot}%{_libdir}/swipl-%{version}/lib/*/libjpl.so \
        %{buildroot}%{_libdir}/swipl-jpl/
mv %{buildroot}%{_libdir}/swipl-%{version}/lib/jpl.jar \
        %{buildroot}%{_libdir}/swipl-jpl/
%endif

%if %{separate_xpce}
# Move the various include files into /usr/include
mkdir -p %{buildroot}%{_includedir}/swipl/xpce
mv %{buildroot}%{_libdir}/swipl-%{version}/include/*.h \
        %{buildroot}%{_includedir}/swipl
#mv %{buildroot}%{_libdir}/swipl-%{version}/xpce-*/include/* \
#        %{buildroot}%{_includedir}/swipl/xpce
rm -rf %{buildroot}%{_libdir}/swipl-%{version}/include
#rm -rf %{buildroot}%{_libdir}/swipl-%{version}/xpce-*/include
%endif

# Move the binaries into %%{_bindir} directly instead of using links
rm -f %{buildroot}%{_bindir}/*
mv %{buildroot}%{_libdir}/swipl-%{version}/bin/*/* %{buildroot}%{_bindir}
rm -rf %{buildroot}%{_libdir}/swipl-%{version}/bin

# Clean up the other stuff that shouldn't be packaged
rm -rf %{buildroot}%{_libdir}/swipl-%{version}/man
rm -rf %{buildroot}%{_libdir}/swipl-%{version}/doc
rm -rf %{buildroot}%{_mandir}/man3/readline*
find packages/jpl/examples -name "*.class" | xargs rm -f
find packages/jpl/examples -name ".cvsignore" | xargs rm -f


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc ReleaseNotes/relnotes-5.10 README COPYING VERSION
%doc dotfiles/dotplrc
%doc %{docdir}/*
%{_mandir}/man1/*
%dir %{_libdir}/swipl-%{version}
%{_libdir}/swipl-%{version}/*
%{_bindir}/*
%if ! %{separate_xpce}
%doc %{docdir}-xpce/*
%endif

# Exclude the files that are in the sub-packages
%if %{with_java}
# JPL
%exclude %{_libdir}/swipl-%{version}/library/jpl.pl
%endif
# Devel
%exclude %{_libdir}/swipl-%{version}/lib/*/libswipl.so
%exclude %{_libdir}/swipl-%{version}/include
#%exclude %{_libdir}/swipl-%{version}/xpce-*/include
# Static
%exclude %{_libdir}/swipl-%{version}/lib/*/libswipl.a
%if %{separate_xpce}
# XPCE
%exclude %{_bindir}/xpce*
%exclude %{_libdir}/swipl-%{version}/bin/*/xpce
%exclude %{_libdir}/swipl-%{version}/dotfiles/dotxpcerc
%exclude %{_libdir}/swipl-%{version}/xpce*
%exclude %{_libdir}/swipl-%{version}/library/http/xpce_httpd.pl
%endif

%if %{separate_xpce}
%files xpce
%defattr(-,root,root,-)
%doc %[docdir-xpce}/*
%doc dotfiles/dotxpcerc
%{_bindir}/xpce*
%{_includedir}/swipl/xpce
%{_libdir}/swipl-%{version}/bin/*/xpce
%{_libdir}/swipl-%{version}/xpce*
%{_libdir}/swipl-%{version}/library/http/xpce_httpd.pl
%endif

%files devel
%defattr(-,root,root,-)
%{_libdir}/swipl-%{version}/include
#%{_libdir}/swipl-%{version}/xpce-*/include
%{_libdir}/swipl-%{version}/lib/*/libswipl.so
%{_libdir}/pkgconfig/swipl.pc

%files static
%defattr(-,root,root,-)
%{_libdir}/swipl-%{version}/lib/*/libswipl.a

%if %{with_java}
%files jpl
%defattr(-,root,root,-)
%doc packages/jpl/docs/*
%doc packages/jpl/examples
%{_libdir}/swipl-%{version}/library/jpl.pl
%{_libdir}/swipl-jpl
%endif


%changelog
* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.10.5-2m)
- Provides: %%{_bindir}/swipl again

* Sat Aug 27 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.10.5-1m)
- [SECURITY] CVE-2011-2896
- merge from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.10.2-5m)
- rebuild for new GCC 4.6

* Thu Feb 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.10.2-4m)
- import bug fix patch from upstream

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.10.2-3m)
- Epoch version of Java is unnecessary

* Tue Jan 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.10.2-2m)
- Provides: %%{_bindir}/swipl

* Sun Jan 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.10.2-1m)
- rebase from fedora's pl-5.10.2
  to fix build failure when using the latest xorg

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7.11-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.7.11-7m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.11-6m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.11-5m)
- rebuild against openssl-1.0.0

* Sat Mar 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7.11-4m)
- change JAVA_HOME configuration to fix build failure on i686

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.11-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7.11-2m)
- rebuild against libjpeg-7

* Sat Aug 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.11-1m)
- import from Rawhide for gcc44 with graphite

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.7.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul  7 2009 Mary Ellen Foster <mefoster at gmail.com> - 5.7.11-2
- Really fix issue with compiling "maildrop" packages

* Mon Jul  6 2009 Mary Ellen Foster <mefoster at gmail.com> - 5.7.11-1
- Move binaries into /usr/bin directly to fix multilib issues
- Update to latest upstream release
- Use officially-distributed PDF documentation instead of HTML
- Unify Java patches
- Remove strndup package; they fixed it upstream
- Fix compilation of "maildrop" packages
- Give the xpce documentation directory a clearer name
- Removed the FILES section of the man page because it also caused
  multilib conflicts (and was inaccurate anyway)

* Fri Jun 12 2009 Dennis Gilmore <dennis@ausil.us> 5.7.6-5
-dont use a static definition for strndup

* Mon Mar 02 2009 Dennis Gilmore <dennis@ausil.us> 5.7.6-4
- fix JAVA_HOME and JAVA_LIB for sparc arches

* Sun Mar 01 2009 Karsten Hopp <karsten@redhat.com> 5.7.6-3
- fix java LIBDIRS for mainframe, similar to alpha

* Wed Feb 25 2009 Mary Ellen Foster <mefoster at gmail.com> - 5.7.6-2
- Unify all changes:
  - Fix java LIBDIRS on alpha (Oliver Falk)

* Wed Feb 25 2009 Mary Ellen Foster <mefoster at gmail.com> - 5.7.6-1
- Update to version 5.7
  - Cleaned up virtual machine and compiler
  - Increased performance

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> - 5.6.60-3
- rebuild with new openssl

* Fri Sep 19 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 5.6.60-2
- forgot to remove ANNOUNCE from doc list

* Fri Sep 19 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 5.6.60-1
- update to 5.6.60
- use openjdk (FIXME: there may be a way to make this more generic)

* Wed Jul  2 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.57-2
- Build using any Java
- Include patch from SWI for Turkish locale (thanks to Keri Harris)

* Wed Jun 25 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.57-1
- Another update, after vacation

* Mon May 19 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.55-1
- Update to 5.6.55 (wow, fast updates!)
- Un-split xpce for now
- Conditionally build jpl (on Fedora 9 with openjdk, and on 
  Fedora 8 non-ppc with icedtea)

* Wed May 07 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.54-1
- Update to 5.6.54 and prepare to actually push this
- Try splitting xpce into own package

* Tue Apr 15 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.53-1
- Update to 5.6.53 -- fixes ppc64 problems, yay!

* Wed Apr 09 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.52-2
- Put JPL stuff where the new Java packaging guidelines say it should be
  and make all of the necessary adjustments in other files
- Split out "-devel" and "-static" packages per guidelines

* Mon Mar 31 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.52-1
- Switch jpl requirement from IcedTea to OpenJDK and enable it everywhere
- Upgrade to 5.6.52
- Patch jpl configure script to find Java libraries on ppc{64}
- NB: Still broken on ppc64, still trying to figure out why

* Mon Feb 25 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.51-1
- Upgrade to 5.6.51

* Fri Feb 22 2008 Mary Ellen Foster <mefoster at gmail.com> - 5.6.50-1
- Update to 5.6.50
- Enable JPL (as a sub-package) -- NB: it only builds with icedtea for now,
  so we disable that sub-package on ppc64 and ppc for the moment

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 5.6.47-9
- Autorebuild for GCC 4.3

* Thu Dec  6 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.47-8
- compile with -fno-strict-aliasing

* Wed Dec  5 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.47-5
- disable jpl for now

* Wed Dec  5 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.47-4
- enable shared library building

* Wed Dec  5 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.47-1
- new release 5.6.47

* Fri Jun  8 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.35-1
- new version 5.6.35
- add requires readline-devel

* Mon Apr 23 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.34-1
- new version 5.6.34

* Fri Feb 23 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.6.28-1
- new version 5.6.28

* Fri Dec  1 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.24-1
- new version 5.6.24

* Sun Oct  1 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.20-1
- new version 5.6.20

* Sat Sep  2 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.18-1
- updated to 5.6.18

* Mon Aug 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.16-3
- Rebuild for FE6

* Tue Jul 11 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.16-1
- new version 5.6.16

* Mon May  1 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.12-3
- added buildreq for libXinerama-devel

* Mon May  1 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.12-2
- added patch to compile with xft

* Sun Apr 30 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.12-1
- new version 5.6.12

* Wed Mar  8 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.7-1
- new version 5.6.7

* Sat Jan 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.3-1
- new version 5.6.3

* Mon Jan  2 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.6.0-1
- new version 5.6.0

* Wed Jun 22 2005 Gerard Milmeister <gemi@bluewin.ch> - 5.4.7-1
- new version 5.4.7

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 5.4.6-9
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Wed Feb 23 2005 David Woodhouse <dwmw2@infradead.org> - 5.4.6-7
- Fix visibility abuse. This may well fix x86_64 too, so re-enable that.

* Mon Feb 21 2005 Gerard Milmeister <gemi@bluewin.ch> - 5.4.6-6
- Exclude x86_64 for now (bugzilla 149038)

* Sun Feb 20 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 5.4.6-5
- Added patch1 for a few multilib Makefile/configure fixes.
- Use %%makeinstall and set libdir in install section.

* Sat Feb 12 2005 Warren Togami <wtogami@redhat.com> - 5.4.6-4
- remove duplicate RPATH patch
- remove Epoch
- remove redundant unixODBC from BR

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 5.4.6-2
- Added BuildRequires: unixODBC, unixODBC-devel
- Removed rpath from shared libs: pl-rpath.patch

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 5.4.6-1
- New Version 5.4.6

* Thu Jan 13 2005 Gerard Milmeister <gemi@bluewin.ch> - 5.4.5-0.fdr.1
- New Version 5.4.5
