%global         momorel 2

Name:           perl-Archive-Zip
Version:        1.37
Release:        %{momorel}m%{?dist}
Summary:        Provide an interface to ZIP archive files
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Archive-Zip/
Source0:        http://www.cpan.org/authors/id/P/PH/PHRED/Archive-Zip-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Compress-Raw-Zlib >= 2.017
BuildRequires:  perl-Cwd >= 0.80
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Path
BuildRequires:  perl-File-Temp
BuildRequires:  perl-IO
BuildRequires:  perl-Test-Simple >= 0.42
BuildRequires:  perl-Time-Local
Requires:       perl-Compress-Raw-Zlib >= 2.017
Requires:       perl-Cwd >= 0.80
Requires:       perl-File-Path
Requires:       perl-File-Temp
Requires:       perl-IO
Requires:       perl-Time-Local
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Archive::Zip module allows a Perl program to create, manipulate, read,
and write Zip archive files.

%prep
%setup -q -n Archive-Zip-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes examples
%{_bindir}/crc32
%{perl_vendorlib}/Archive/*.pm
%{perl_vendorlib}/Archive/Zip
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Sun Nov 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.30-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.30-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.30-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-2m)
- rebuild against perl-5.10.1

* Sun Aug  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Wed Jun 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-2m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.23-2m)
- rebuild against gcc43

* Thu Nov  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Thu Nov  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Tue Jun  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.18-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.16-2m)
- built against perl-5.8.8

* Sat Dec 24 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.16-1m)
- version up 1.16

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14-2m)
- built against perl-5.8.7

* Sat Nov 06 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.14-1m)
- [SECURITY FIX] Archive::Zip is fooled by manipulated ZIP directory 
  http://rt.cpan.org/NoAuth/Bug.html?id=8077
  http://www.idefense.com/application/poi/display?id=153&type=vulnerabilities&flashstatus=true

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.08-4m)
- rebuild against perl-5.8.5
- rebuild against perl-Compress-Zlib 1.19-6m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.08-3m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (1.08-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.08-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.06-3m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.06-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Sep  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.06-1m)

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.05-2m)
- rebuild against perl-5.8.0

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.05-1m)

* Thu Feb 28 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.11-4k)
- against perl 5.6.1
- clean up spec.

* Wed Jan 16 2002 YAMAGUCHI Kenji <yamk@sophiaworks.com>
- (0.11-2k)
- Kondarize.

* Sun Aug 26 2001 Ramiro Morales <rmrpms@usa.net>
- Build on RHL 6.2
- Include docs
- repackage source .zip -> .tar.gz
- Add {Build,}Req perl-Compress-Zlib
- s/Copyright/License/

* Thu Aug 23 2001 root <root@redhat.com>
- Spec file was autogenerated. 
