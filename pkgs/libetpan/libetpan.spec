%global momorel 1

%define soname 16

Name:           libetpan
Release:        %{momorel}m%{dist}
Version:        1.1
Summary:        Mail Handling Library
License:        BSD
Group:          Development/Libraries
Url:            http://libetpan.sourceforge.net/
Source0:        http://downloads.sourceforge.net/project/libetpan/libetpan/%{version}/libetpan-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-0.56-uninitialized.patch
BuildRequires:  cyrus-sasl-devel
BuildRequires:  db4-devel
# Stupid dependency. Compiles with gcc but tries to link with g++.
BuildRequires:  gcc-c++
BuildRequires:  libcurl-devel
BuildRequires:  expat-devel
BuildRequires:  openssl-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
libEtPan is a mail purpose library. It will be used for low-level mail
handling : network protocols (IMAP/NNTP/POP3/SMTP over TCP/IP and
SSL/TCP/IP, already implemented), local storage (mbox/MH/maildir),
message / MIME parse

%package -n libetpan%{soname}
Summary:        Mail handling library
Group:          Development/Libraries
Provides:       %{name} = %{version}
Obsoletes:      %{name} < %{version}

%description -n libetpan%{soname}
libEtPan is a mail purpose library. It will be used for low-level mail
handling : network protocols (IMAP/NNTP/POP3/SMTP over TCP/IP and
SSL/TCP/IP, already implemented), local storage (mbox/MH/maildir),
message / MIME parse

%package -n libetpan-devel
Summary:        Mail handling library
Group:          Development/Libraries
Requires:       cyrus-sasl-devel
Requires:       db4-devel
Requires:       libetpan%{soname} = %{version}
Requires:       openssl-devel

%description -n libetpan-devel
libEtPan is a mail purpose library. It will be used for low-level mail
handling : network protocols (IMAP/NNTP/POP3/SMTP over TCP/IP and
SSL/TCP/IP, already implemented), local storage (mbox/MH/maildir),
message / MIME parse

%prep
%setup -q
%patch0

%build
%configure --disable-static --with-gnutls=yes
make %{?_smp_mflags}

%install
make install DESTDIR=%{?buildroot}
# remove unneeded *.la files
rm %{buildroot}%{_libdir}/libetpan.la

%post -n libetpan%{soname} -p /sbin/ldconfig

%postun -n libetpan%{soname} -p /sbin/ldconfig

%files -n libetpan%{soname}
%defattr(-, root, root)
%doc ChangeLog NEWS doc/README*
%{_libdir}/libetpan.so.%{soname}*

%files -n libetpan-devel
%defattr(-, root, root)
%doc doc/API* doc/DOCUMENTATION
%{_bindir}/libetpan-config
%{_includedir}/libetpan/
%{_includedir}/libetpan.h
%{_libdir}/libetpan.so

%changelog
* Sun Aug 26 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.1-mo1)
- Initial Momonga release
- imported from opensuse