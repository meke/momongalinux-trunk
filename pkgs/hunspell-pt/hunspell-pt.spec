%global momorel 4

Name: hunspell-pt
Summary: Portuguese hunspell dictionaries
%define upstreamid 20100109
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source0: http://natura.di.uminho.pt/download/sources/Dictionaries/hunspell/hunspell-pt_PT-20091013.tar.gz
Source1: http://www.broffice.org/files/pt_BR-2010-01-09AOC.zip
Group: Applications/Text
URL: http://www.broffice.org/verortografico/baixar
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2 and GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Portuguese hunspell dictionaries.

%prep
%setup -q -n hunspell-pt_PT-20091013
unzip -q -o %{SOURCE1}
for i in README_pt_BR.TXT README_pt_PT.txt; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-1 -t UTF-8 $i > $i.new
    touch -r $i $i.new
    mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_pt_BR.TXT README_pt_PT.txt COPYING
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100109-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100109-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100109-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20100109-1m)
- sync with Fedora 13 (0.20100109-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090309-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090309-1m)
- update to 20090309

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080320-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20080320-1m)
- import from Fedora to Momonga

* Fri Mar 21 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080320-1
- latest version

* Thu Feb 21 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080221-1
- latest version

* Fri Feb 15 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080210-1
- latest version

* Fri Dec 14 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071212-1
- latest version

* Sat Nov 11 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071106-1
- latest version

* Mon Nov 05 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071101-1
- latest version

* Fri Oct 05 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071003-1
- next version

* Tue Oct 02 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071001-1
- next version

* Tue Aug 28 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070823-2
- source file audit shows that pt_BR-2007-04-11.zip silently changed
  content, updating to match

* Thu Aug 23 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070823-1
- latest version

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070709-2
- clarify licences

* Wed Jul 18 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070709-1
- latest pt_PT version

* Sun May 06 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070411-1
- latest versions

* Wed Feb 14 2007 Caolan McNamara <caolanm@redhat.com> - 0.20061026-2
- disambiguate readmes

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20061026-1
- initial version
