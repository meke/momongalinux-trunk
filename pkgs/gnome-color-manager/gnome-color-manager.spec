%global    momorel 1
Summary:   Color management tools for GNOME
Name:      gnome-color-manager
Version:   3.6.0
Release:   %{momorel}m%{?dist}
License:   GPLv2+
Group:     Applications/System
URL:       http://projects.gnome.org/gnome-color-manager/
Source0:   http://download.gnome.org/sources/gnome-color-manager/3.6/%{name}-%{version}.tar.xz
NoSource: 0

Requires:  color-filesystem >= 1-5
Requires:  gnome-icon-theme
Requires:  shared-mime-info
Requires:  shared-color-profiles
Requires:  dconf
Requires:  GConf2 >= 2.31.1

# Not actually a hard requirement; it just sucks to have to install the
# package using PackageKit before the user can calibrate
##Requires:  argyllcms

BuildRequires: gtk3-devel >= 3.0.0
BuildRequires: scrollkeeper
BuildRequires: gnome-doc-utils >= 0.3.2
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: vte3-devel
BuildRequires: gnome-doc-utils
BuildRequires: intltool
BuildRequires: libgudev1-devel
BuildRequires: libXrandr-devel
BuildRequires: lcms2-devel
BuildRequires: libtiff-devel
BuildRequires: libexif-devel
BuildRequires: exiv2-devel >= 0.23
BuildRequires: libcanberra-devel
BuildRequires: glib2-devel >= 2.25.9-2
BuildRequires: docbook-utils
BuildRequires: colord-devel >= 0.1.22
BuildRequires: libmash-devel
BuildRequires: clutter-gtk-devel
BuildRequires: gnome-desktop3-devel
BuildRequires: itstool
BuildRequires: colord-gtk-devel >= 0.1.22

Requires(post):   /usr/bin/gtk-update-icon-cache
Requires(postun): /usr/bin/gtk-update-icon-cache

# obsolete sub-package
Obsoletes: gnome-color-manager-devel <= 3.1.1
Provides: gnome-color-manager-devel

%description
gnome-color-manager is a session framework that makes it easy to manage, install
and generate color profiles in the GNOME desktop.

%prep
%setup -q

%build
%configure --disable-scrollkeeper --disable-schemas-install
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

for i in gcm-calibrate gcm-import ; do
  desktop-file-install --delete-original                                \
    --dir=$RPM_BUILD_ROOT%{_datadir}/applications/                      \
    $RPM_BUILD_ROOT%{_datadir}/applications/$i.desktop
done

%find_lang %name --with-gnome

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database %{_datadir}/applications &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
if [ $1 -eq 0 ]; then
    touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
update-desktop-database %{_datadir}/applications &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{_bindir}/gcm-*
%{_libexecdir}/gcm-*
%dir %{_datadir}/gnome-color-manager
%dir %{_datadir}/gnome-color-manager/targets
%dir %{_datadir}/gnome-color-manager/icons
%dir %{_datadir}/gnome-color-manager/figures
%dir %{_datadir}/gnome-color-manager/ti1
%{_datadir}/gnome-color-manager/targets
%{_datadir}/gnome-color-manager/icons
%{_datadir}/gnome-color-manager/figures
%{_datadir}/gnome-color-manager/ti1
%{_datadir}/man/man1/*.1.*
%{_datadir}/icons/hicolor/*/*/*.png
%{_datadir}/icons/hicolor/scalable/*/*.svg*
%{_datadir}/applications/gcm-*.desktop
%{_datadir}/help/*/*

%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Aug 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-2m)
- update to 3.5.90

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.3-2m)
- rebuild against exiv2-0.23

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-3m)
- rebuild for glib 2.33.2

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-2m)
- rebuild against libtiff-4.0.1

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-2m)
- rebuild against exiv2-0.22

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-4m)
- full rebuild for mo7 release

* Sun Jun 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.30.2-3m)
- fix build

* Sat Jun 19 2010 NARITA Koichi <pulsar@omonga-linux.org>
- (2.30.2-2m)
- enable to build on x86_64

* Sat Jun 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- initila build

