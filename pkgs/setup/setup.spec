%global momorel 1

Summary: A set of system configuration and setup files
Name: setup
Version: 2.9.0
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Base
URL: https://fedorahosted.org/setup/
Source0: https://fedorahosted.org/releases/s/e/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: bash tcsh >= 6.15.00-4m perl
Requires: bash >= 4.0
Conflicts: initscripts < 4.26, bash <= 2.0.4-21

%description
The setup package contains a set of important system configuration and
setup files, such as passwd, group, and profile.

%prep
%setup -q
./shadowconvert.sh

%build

%check
# Run any sanity checks.
make check

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc/profile.d
cp -ar * %{buildroot}/etc
rm -f %{buildroot}/etc/uidgid
rm -f %{buildroot}/etc/COPYING
mkdir -p %{buildroot}/var/log
touch %{buildroot}/var/log/lastlog
touch %{buildroot}/etc/environment
chmod 0644 %{buildroot}/etc/environment
chmod 0400 %{buildroot}/etc/{shadow,gshadow}
chmod 0644 %{buildroot}/var/log/lastlog
touch %{buildroot}/etc/fstab

# remove unpackaged files from the buildroot
rm -f %{buildroot}/etc/Makefile
rm -f %{buildroot}/etc/serviceslint
rm -f %{buildroot}/etc/uidgidlint
rm -f %{buildroot}/etc/shadowconvert.sh
rm -f %{buildroot}/etc/setup.spec

%clean
rm -rf %{buildroot}

#throw away useless and dangerous update stuff until rpm will be able to
#handle it ( http://rpm.org/ticket/6 )
%post -p <lua>
for i, name in ipairs({"passwd", "shadow", "group", "gshadow"}) do
     os.remove("/etc/"..name..".rpmnew")
end
if posix.access("/usr/bin/newaliases", "x") then
     os.execute("/usr/bin/newaliases >/dev/null")
end


%files
%defattr(-,root,root,-)
%doc uidgid COPYING
%verify(not md5 size mtime) %config(noreplace) /etc/passwd
%verify(not md5 size mtime) %config(noreplace) /etc/group
%verify(not md5 size mtime) %attr(0000,root,root) %config(noreplace,missingok) /etc/shadow
%verify(not md5 size mtime) %attr(0000,root,root) %config(noreplace,missingok) /etc/gshadow
%verify(not md5 size mtime) %config(noreplace) /etc/services
%verify(not md5 size mtime) %config(noreplace) /etc/exports
%config(noreplace) /etc/aliases
%config(noreplace) /etc/environment
%config(noreplace) /etc/filesystems
%config(noreplace) /etc/host.conf
%verify(not md5 size mtime) %config(noreplace) /etc/hosts
%verify(not md5 size mtime) %config(noreplace) /etc/hosts.allow
%verify(not md5 size mtime) %config(noreplace) /etc/hosts.deny
%verify(not md5 size mtime) %config(noreplace) /etc/motd
%config(noreplace) /etc/printcap
%verify(not md5 size mtime) %config(noreplace) /etc/inputrc
%config(noreplace) /etc/bashrc
%config(noreplace) /etc/profile
%verify(not md5 size mtime) %config(noreplace) /etc/protocols
%config(noreplace) /etc/csh.login
%config(noreplace) /etc/csh.cshrc
%dir /etc/profile.d
%config(noreplace) %verify(not md5 size mtime) /etc/shells
%ghost %attr(0644,root,root) %verify(not md5 size mtime) /var/log/lastlog
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) /etc/fstab

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.31-1m)
- update to 2.8.31
- remove /etc/mtab

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.23-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.23-1m)
- update to 2.8.23

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.18-1m)
- update to 2.8.18

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.15-1m)
- update to 2.8.15
- do no use /etc/mail/aliases which is one of momonga specific
  settings. i do not like that.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 8 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.3-3m)
- revert /etc/bashrc

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.3-2m)
- BuildRequires: tcsh >= 6.15.00-4m

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.3-1m)
- update 2.8.3 based on Fedora 11 (2.8.3-1)
-- still /etc/mail/aliases
-- neglect %%postun

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.14-2m)
- rebuild against rpm-4.6

* Mon May 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.14-1m)
- update 2.6.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-2m)
- rebuild against gcc43

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.10-1m)
- update 2.6.10

* Thu Jun 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.4-3m)
- delete duplicate file again

* Mon Jun 18 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6.4-2m)
- get back /etc/mail/aliases

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.4-1m)
- sync FC7
- update to 2.6.4

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.49-2m)
- delete duplicate file

* Sun May 21 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.49-1m)
- version up

* Sun Feb 19 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.5.36-4m)
- fix lastlog conflict

* Sat Apr  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.36-3m)
- remove setup-nogroup.patch

* Fri Apr 08 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.36-2m)
- add Patch2: setup-nogroup.patch

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.5.36-1m)
- version up

* Thu Aug 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.33-2m)
- comment out %%config(noreplace) /etc/aliases,
  because /etc/aliases != /etc/postfix/aliases

* Sat Jul 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.33-1m)
- version up.

* Sun May 16 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.27-1m)
- version up.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.12-6m)
- revised spec for rpm 4.2.

* Wed Mar  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.5.12-5m)
- add svnserve entry to /etc/services
- %%config(noreplace) for /etc/services

* Fri Oct 10 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5.12-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Jan 4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.12-3m)
- force rebuild

* Mon Feb 04 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.5.7-6k)
- s/ja_JP.eucJP/ja_JP.EUC-JP/ output-charset.sh, output-charset.csh
 (place at /etc/profile.d/output-charset.*, /etc/csh.cshrc)

* Mon Oct 22 2001 Toru Hoshina <t@kondara.org>
- (2.5.7-2k)
- version up.

* Tue Oct 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.2-20k)
- add etc.services.kondara.patch for solid service

* Thu May 31 2001 TABUCHI Takaaki <tab@kondara.org>
- (2.3.2-18k)
- modified services for adapt iana assignments
- cf. http://www.iana.org/assignments/port-numbers
- cf. ([Kondara-devel.ja:05280] RC1 /etc/services ) by Toshiro Hikita

* Mon May 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.2-16k)
- modified output-charset.sh to use = not ==

* Wed Mar 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.2-14k)
- errased %post section because it occurs package requirment loop

* Tue Jan  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.2-13k)
- errase following configs

* Tue Dec 19 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.2-11k)
- added some configs to /etc/hosts.deny

* Sun Nov 19 2000 AYUHANA Tomonori <l@kondara.org>
- (2.3.2-7k)
- add /etc/profile.d/output-charset.{sh,csh}
- (2.3.2-5k)
- add setup-tcsh-dspmbyte-euc.patch

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Tue Jul 25 2000 Bill Nottingham <notting@redhat.com>
- fix some of the csh stuff (#14622)

* Sun Jul 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- stop setting "multi on" in /etc/host.conf

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 27 2000 Bill Nottingham <notting@redhat.com>
- add hfs filesystem

* Wed Jun 21 2000 Preston Brown <pbrown@redhat.com>
- printcap is a noreplace file now

* Sun Jun 18 2000 Bill Nottingham <notting@redhat.com>
- fix typo

* Tue Jun 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- add linuxconf/tcp = 99 to /etc/services

* Sat Jun 10 2000 Bill Nottingham <notting@redhat.com>
- add some stuff to /etc/services
- tweak ulimit call again

* Tue Jun  6 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- homedir of ftp is now /var/ftp

* Sun May 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- move profile.d logic in csh.login to csh.cshrc

* Tue Apr 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- redirect ulimit -S -c to /dev/null to avoid clutter

* Thu Apr 13 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- s/ulimit -c/ulimit -S -c/ - bash 2.x adaption

* Mon Apr 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- Add more of the kerberos-related services from IANA's registry and krb5

* Wed Mar 29 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Add 2.4'ish vc/* devices to securetty

* Thu Feb 17 2000 Preston Brown <pbrown@redhat.com>
- add /etc/filesystems with sane defaults

* Wed Feb 16 2000 Bill Nottingham <notting@redhat.com>
- don't set prompt in /etc/profile (it's done in /etc/bashrc)

* Fri Feb  5 2000 Bill Nottingham <notting@redhat.com>
- yet more inputrc tweaks from Hans de Goede (hans@highrise.nl)

* Sun Jan 30 2000 Bill Nottingham <notting@redhat.com>
- yet more inputrc tweaks from Hans de Goede (hans@highrise.nl)

* Sun Jan 23 2000 Bill Nottingham <notting@redhat.com>
- fix mailq line. (#7140)

* Fri Jan 21 2000 Bill Nottingham <notting@redhat.com>
- add ldap to /etc/services

* Tue Jan 18 2000 Bill Nottingham <notting@redhat.com>
- kill HISTFILESIZE, it's broken

* Tue Jan 18 2000 Preston Brown <pbrown@redhat.com>
- some inputrc tweaks

* Wed Jan 12 2000 Bill Nottingham <notting@redhat.com>
- make some more stuff noreplace

* Fri Nov 19 1999 Bill Nottingham <notting@redhat.com>
- fix mailq line. (#7140)

* Fri Oct 29 1999 Bill Nottingham <notting@redhat.com>
- split csh.login into csh.login and csh.cshrc (#various)
- fix pop service names (#6206)
- fix ipv6 protocols entries (#6219)

* Thu Sep  2 1999 Jeff Johnson <jbj@redhat.com>
- rename /etc/csh.cshrc to /etc/csh.login (#2931).
- (note: modified /etc/csh.cshrc should end up in /etc/csh.cshrc.rpmsave)

* Fri Aug 20 1999 Jeff Johnson <jbj@redhat.com>
- add defattr.
- fix limit command in /etc/csh.cshrc (#4582).

* Thu Jul  8 1999 Bill Nottingham <notting@redhat.com>
- move /etc/inputrc here.

* Mon Apr 19 1999 Bill Nottingham <notting@redhat.com>
- always use /etc/inputrc

* Wed Mar 31 1999 Preston Brown <pbrown@redhat.com>
- added alias pointing to imap from imap2

* Tue Mar 23 1999 Preston Brown <pbrown@redhat.com>
- updated protocols/services from debian to comply with more modern 
- IETF/RFC standards

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Thu Feb 18 1999 Jeff Johnson <jbj@redhat.com>
- unset variables used in /etc/csh.cshrc (#1212)

* Mon Jan 18 1999 Jeff Johnson <jbj@redhat.com>
- compile for Raw Hide.

* Tue Oct 13 1998 Cristian Gafton <gafton@redhat.com>
- fix the csh.cshrc re: ${PATH} undefined

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Dec 05 1997 Erik Troan <ewt@redhat.com>
- /etc/profile uses $i, which needs to be unset

* Mon Nov 03 1997 Donnie Barnes <djb@redhat.com>
- made /etc/passwd and /etc/group %config(noreplace)

* Mon Oct 20 1997 Erik Troan <ewt@redhat.com>
- removed /etc/inetd.conf, /etc/rpc
- flagged /etc/securetty as missingok
- fixed buildroot stuff in spec file

* Thu Jul 31 1997 Erik Troan <ewt@redhat.com>
- made a noarch package

* Wed Apr 16 1997 Erik Troan <ewt@redhat.com>
- Don't verify md5sum, size, or timestamp of /var/log/lastlog, /etc/passwd,
  or /etc/group.

