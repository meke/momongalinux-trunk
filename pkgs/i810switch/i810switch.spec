%global momorel 6

Name: i810switch
Version: 0.6.5
Release: %{momorel}m%{?dist}

Summary: Utility for switching the LCD and external VGA displays on and off
Group: User Interface/X Hardware Support
License: GPL
URL: http://www16.plala.or.jp/mano-a-mano/i810switch.html
Source0: i810switch-0.6.5.tar.gz
Patch0: i810switch-0.6.2.makefile.patch
Patch1: i810switch-0.6.5-add-i945.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: pciutils

%description
i810switch is a utility for switching the LCD and external VGA
displays on and off under Linux. It was originally written by Antonino
Daplas, and is now maintained by Ken Mano.

%prep
%setup -q
%patch0 -p1 -b .buildroot
%patch1 -p1 -b .i945
make clean

%build
CFLAGS="$RPM_OPT_FLAGS" make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README TODO ChangeLog INSTALL
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-2m)
- rebuild against rpm-4.6

* Tue May 20 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.6.5-1m)
- import from FC

* Thu Sep 28 2007 Matt Domsch <Matt_Domsch@dell.com> - 0.6.5-6
- add i945 detection, fixes BZ297371

* Mon Oct 02 2006 Matt Domsch <Matt_Domsch@dell.com> - 0.6.5-5
- rebuild

* Sat Sep  2 2006 Matt Domsch <Matt_Domsch@dell.com> - 0.6.5-4
- rebuild

* Mon May 22 2006 Matt Domsch <Matt_Domsch@dell.com> - 0.6.5-3
- make clean in prep BZ192619

* Sun Feb 12 2006 Matt Domsch <Matt_Domsch@dell.com> - 0.6.5-2
- rebuild for FC5

* Tue Jan 17 2006 Matt Domsch <Matt_Domsch@dell.com> - 0.6.5-1
- update to 0.6.5, change URLs due to new upstream maintainer

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Tue Jan 18 2005 David Kaplan <dmk@localhost.localdomain> - 0:0.6.2-3
- Added back INSTALL file to rpm because it explains how to SUID root
  the executable so that all users can use i810switch.

* Thu Jan 13 2005 David Kaplan <dmk@localhost.localdomain> - 0:0.6.2-0.fdr.2
- Minor spec file fixes and addition of optimization flags to CFLAGS

* Tue Jan 11 2005 David Kaplan <dmk@localhost.localdomain> - 0:0.6.2-0.fdr.1
- Initial build of i810switch

