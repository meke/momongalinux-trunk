%global momorel 11
Summary: TMMS is a ncurse frontend for mpg123 (or mpg321) and ogg123.
Name: tmms
Version: 2.0.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://tnemeth.free.fr/projets/tmms.html
Source0: http://tnemeth.free.fr/projets/programmes/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: tmms-2.0.0-label.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ncurses, readline, mpg321, vorbis-tools
BuildRequires: ncurses-devel >= 5.6-10 , readline-devel >= 5.0

%description

%prep
%setup -q -n %{name}
%patch0 -p1
perl -p -i -e 's|^include \.dependencies|-include \.dependencies|' Makefile

%build
make PREFIX=/usr USER_CPPFLAGS= \
     USER_LDFLAGS='-lncurses -lreadline -lpthread'

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%{buildroot} \
	PREFIX=/usr \
	MANDIR=%{buildroot}%{_mandir}/man1 install

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog INSTALL README TODO
%{_bindir}/tmms
%{_mandir}/man1/tmms.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-9m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-8m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-4m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-3m)
- no use libtermcap

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.0-2m)
- rebuild against readline-5.0

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.0.0-1m)
- update to 2.0.0
- add patch0

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.1.0-1m)
- update to 1.1.0

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.1.0-2m)
- rebuild against libtermcap and ncurses

* Tue Jan 25 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.1.0-1m)
  imported.
