%global         momorel 1

Name:           perl-Data-Dump-Streamer
Version:        2.38
Release:        %{momorel}m%{?dist}
Summary:        Accurately serialize a data structure as Perl code
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Data-Dump-Streamer/
Source0:        http://www.cpan.org/authors/id/Y/YV/YVES/Data-Dump-Streamer-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Algorithm-Diff
BuildRequires:  perl-B
BuildRequires:  perl-base
BuildRequires:  perl-B-Deparse
BuildRequires:  perl-B-Utils
BuildRequires:  perl-Carp
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-Devel-Peek
BuildRequires:  perl-DynaLoader
BuildRequires:  perl-ExtUtils-CBuilder
BuildRequires:  perl-Hash-Util
BuildRequires:  perl-IO
BuildRequires:  perl-IO-Compress
BuildRequires:  perl-JSON-XS
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-Module-Build
BuildRequires:  perl-PadWalker >= 0.99
BuildRequires:  perl-re
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Text-Abbrev
BuildRequires:  perl-Text-Balanced
Requires:       perl-Algorithm-Diff
Requires:       perl-B
Requires:       perl-B-Deparse
Requires:       perl-B-Utils
Requires:       perl-Data-Dumper
Requires:       perl-DynaLoader
Requires:       perl-Hash-Util
Requires:       perl-IO
Requires:       perl-IO-Compress
Requires:       perl-JSON-XS
Requires:       perl-MIME-Base64
Requires:       perl-PadWalker >= 0.99
Requires:       perl-re
Requires:       perl-Text-Abbrev
Requires:       perl-Text-Balanced
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Given a list of scalars or reference variables, writes out their contents
in perl syntax. The references can also be objects. The contents of each
variable is output using the least number of Perl statements as convenient,
usually only one. Self-referential structures, closures, and objects are
output correctly.

%prep
%setup -q -n Data-Dump-Streamer-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Data::Dump::Streamer::_::Printers)/d'

EOF
%define __perl_requires %{_builddir}/Data-Dump-Streamer-%{version}/%{name}-req
chmod +x %{name}-req

%build
echo yes | %{__perl} Build.PL installdirs=vendor optimize="%{optflags}"
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes INSTALL.SKIP README
%{perl_vendorarch}/DDS.pm
%{perl_vendorarch}/auto/Data/Dump/Streamer
%{perl_vendorarch}/Data/Dump/Streamer.pm
%{perl_vendorarch}/Data/Dump/Streamer
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-1m)
- rebuild against perl-5.20.0
- update to 2.38

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.37-1m)
- update to 2.37
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-2m)
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-1m)
- update to 2.36

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-1m)
- update to 2.34
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32-2m)
- rebuild for new GCC 4.6

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32-1m)
- update to 2.32

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-1m)
- update to 2.31

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-1m)
- update to 2.25

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23-1m)
- update to 2.23

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22-2m)
- rebuild for new GCC 4.5

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
