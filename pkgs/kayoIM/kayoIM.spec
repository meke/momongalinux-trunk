%global momorel 22
%global qtver 4.8.6
%global qtdir %{_libdir}/qt4

Name: kayoIM
Summary: kayoIM is an input method system for Qt4
Version: 0.7.1
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: GPLv2+
URL: http://kayoim.net-p.org/
Source0: kayoIM-%{version}.tar.bz2
Requires: qt-x11 >= %{qtver}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: anthy-devel
BuildRequires: glibc-devel

%description
kayoIM is an input method system for Qt4.

%prep
%setup -q
sed -i -e 's|/usr/local|%{_prefix}|' qmake_def.inc
sed -i -e 's|$$PREFIX/bin|%{_bindir}|' qmake_def.inc
sed -i -e 's|$$PREFIX/lib|%{_libdir}|' qmake_def.inc
sed -i -e 's|$$PREFIX/share/kayoim|%{_datadir}/kayoim|' qmake_def.inc
sed -i -e 's|$$PREFIX/lib/kayoim|%{_libdir}/kayoim|' qmake_def.inc

%build
export QTDIR=%{qtdir}
qmake-qt4
# no _smp_mflags
make

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING README.jp.txt
%{_bindir}/kayoim
%{_libdir}/kayoim
%{_libdir}/libkayoIM*
%{qtdir}/plugins/inputmethods/libqt-kayoim.so

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-22m)
- rebuild against qt-4.8.6
- kayoIM links qt libraries dynamically, it does not depend on the qt version closely

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-21m)
- rebuild against qt-4.8.5

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-20m)
- rebuild against qt-4.8.4

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-19m)
- rebuild against qt-4.8.3

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-18m)
- rebuild against qt-4.8.2

* Wed Apr  4 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.1-17m)
- rebuild against qt-4.8.1

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-16m)
- rebuild against qt-4.8.0

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-15m)
- rebuild against qt-4.7.4

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-14m)
- rebuild against qt-4.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-13m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-12m)
- rebuild against qt-4.7.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-11m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-10m)
- rebuild against qt-4.7.1

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-9m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-8m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-7m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-6m)
- rebuild against qt-4.7.0

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-5m)
- rebuild against qt-4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-4m)
- rebuild against qt-4.6.1

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-3m)
- rebuild against qt-4.6.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Sun Mar 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Mon Mar 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.39-1m)
- initial packaging
