%global momorel 1

Name:           orc
Version:        0.4.17
Release:        %{momorel}m%{?dist}
Summary:        The Oil Run-time Compiler

Group:          System Environment/Libraries
License:        BSD
URL:            http://code.entropywave.com/projects/orc/
Source0:        http://code.entropywave.com/download/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk-doc
BuildRequires:  libtool


%description
Orc is a library and set of tools for compiling and executing
very simple programs that operate on arrays of data.  The "language"
is a generic assembly language that represents many of the features
available in SIMD architectures, including saturated addition and
subtraction, and many arithmetic operations.

%package doc
Summary:        Documentation for Orc
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch

%description doc
Documentation for Orc.

%package devel
Summary:        Development files and static libraries for Orc
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-compiler
Requires:       pkgconfig

%description devel
This package contains the files needed to build packages that depend
on orc.

%package compiler
Summary:        Orc compiler
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description compiler
The Orc compiler, to produce optimized code.

%prep
%setup -q 
autoreconf -vif

%build
%configure --disable-static --enable-gtk-doc
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

# Remove unneeded files.
find %{buildroot}/%{_libdir} -name \*.a -or -name \*.la -delete
rm -rf %{buildroot}/%{_libdir}/orc

touch -r stamp-h1 %{buildroot}%{_includedir}/%{name}-0.4/orc/orc-stdint.h   

%clean
rm -rf %{buildroot}

%check
%ifnarch s390 s390x
make check
%endif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/orc-bugreport
%{_libdir}/liborc-*.so.*

%files doc
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc/html/orc

%files devel
%defattr(-,root,root,-)
%doc examples/*.c
%{_includedir}/%{name}-0.4
%{_libdir}/liborc-*.so
%{_libdir}/pkgconfig/orc-0.4.pc
%{_datadir}/aclocal/orc.m4

%files compiler
%defattr(-,root,root,-)
%{_bindir}/orcc

%changelog
* Sun Sep  1 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.17-1m)
- update to 0.4.17

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.16-1m)
- update to 0.4.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.11-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.11-2m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.11-1m)
- update to 0.4.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.6-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-1m)
- import from Fedora devel and update to 0.4.6

* Tue Jul 13 2010 Dan Horak <dan[at]danny.cz> - 0.4.5-3
- don't run test on s390(x)

* Sun Jun 13 2010 Fabian Deutsch <fabiand@fedoraproject.org> - 0.4.5-2
- Added removed testing libraries to package.

* Sun Jun 13 2010 Fabian Deutsch <fabiand@fedoraproject.org> - 0.4.5-1
- Updated to 0.4.5.
- Removed testing libraries from package.

* Mon Apr 05 2010 Fabian Deutsch <fabiand@fedoraproject.org> - 0.4.4-2
- Docs as noarch.
- Sanitize timestamps of header files.
- orcc in -compiler subpackage.

* Tue Mar 30 2010 Fabian Deutsch <fabiand@fedoraproject.org> - 0.4.4-1
- Updated to 0.4.4: Includes bugfixes for x86_64.

* Wed Mar 17 2010 Fabian Deutsch <fabian.deutsch@gmx.de> - 0.4.3-2
- Running autoreconf to prevent building problems.
- Added missing files to docs.
- Added examples to devel docs.

* Thu Mar 04 2010 Fabian Deutsch <fabian.deutsch@gmx.de> - 0.4.3-1
- Updated to 0.4.3

* Sun Oct 18 2009 Fabian Deutsch <fabian.deutsch@gmx.de> - 0.4.2-4
- Removed unused libdir

* Sun Oct 18 2009 Fabian Deutsch <fabian.deutsch@gmx.de> - 0.4.2-3
- Specfile cleanup
- Removed tools subpackage
- Added docs subpackage

* Sat Oct 03 2009 Fabian Deutsch <fabian.deutsch@gmx.de> - 0.4.2-2
- Use orc as pakage name
- spec-file cleanup
- Added devel requirements
- Removed an rpath issue

* Fri Oct 02 2009 Fabian Deutsch <fabian.deutsch@gmx.de> - 0.4.2-1
- Initial release

