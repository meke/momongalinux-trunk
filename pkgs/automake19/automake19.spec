%global momorel 11

%define api_version 1.9

Summary:	A GNU tool for automatically creating Makefiles
Name:		automake19
Version:	1.9.6
Release:	%{momorel}m%{?dist}
License:	GPL
Group:		Development/Tools
Source:		http://ftp.gnu.org/gnu/automake/automake-%{version}.tar.bz2
NoSource:	0
Patch0:		automake-1.9.6-CVE-2009-4029.patch
URL:		http://sources.redhat.com/automake
Requires:	perl, autoconf >= 2.60
Buildrequires:	autoconf >= 2.60
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Automake is a tool for automatically generating `Makefile.in'
files compliant with the GNU Coding Standards.
#`

You should install Automake if you are developing software and would
like to use its ability to automatically generate GNU standard
Makefiles. If you install Automake, you will also need to install
GNU's Autoconf package.
#'

%prep
%setup -q -n automake-%{version}
%patch0 -p1 -b .CVE-2009-4029

%build
./configure --prefix=%{_prefix}
make

%install
rm -rf %{buildroot}

%makeinstall

# delete info
rm -fr %{buildroot}%{_infodir}

# delete version independent executable
rm -f %{buildroot}%{_bindir}/automake
rm -f %{buildroot}%{_bindir}/aclocal

%ifarch %{ix86}
%check
make check || make check VERBOSE=yes
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS README THANKS NEWS
%{_bindir}/automake-%{api_version}
%{_bindir}/aclocal-%{api_version}
%{_datadir}/automake-%{api_version}
%{_datadir}/aclocal-%{api_version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.6-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.6-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.6-9m)
- full rebuild for mo7 release

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.6-8m)
- [SECURITY] CVE-2009-4029
- fixed make dist*

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.6-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.6-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.6-5m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.9.6-4m)
- rebuild against perl-5.10.0-1m

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.6-3m)
- rename automake to automake19

* Sun Jan 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.6-2m)
- skip make check ifnarch ix86

* Mon Dec 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.6-1m)
- update 1.9.6
- include NEWS file
- add %%check

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.5-1m)
- seperate to automake14 automake15 automake16 automake17 and automake(1.9)

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.5-25m)
- automake-1.9.4
- new gnupg2 and dirmngr will require automake >= 1.9.3

* Tue Aug 24 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5-24m)
- automake-1.9.1
- thanks to Nishio Futoshi <fut_nis@d3.dion.ne.jp>

* Sun May  9 2004 Toru Hoshina <t@momonga-linux.org>
- (1.5-23m)
- noarch.

* Sun May  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-22m)
- add automake-1.8 (1.8.4)
- cancel noarch temporarily since automake-1.8 can't be configured with
  'noarch-momonga-linux'

* Fri Dec 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-21m)
- automake-1.7.9

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.5-20m)
- automake-1.7.7

* Tue Jun 10 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (1.5-19m)
- automake 1.7.3

* Mon Feb 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-18m)
- automake 1.7.2

* Tue Jan  7 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-17m)
- buildprereq autoconf >= 2.54

* Mon Nov 25 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5-16m)
- add %{_datadir}/aclocal-1.4 to %files

* Sun Nov 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5-15m)
- update automake-1.4 to 1.4-p6
- update automake-1.6 to 1.6.3
- add    automake-1.7(1.7.1)
- remove automake-new

* Mon Nov 18 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-14m)
- add autohoge-1.4 symlink

* Mon Jul 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-13m)
- define '__libtoolize' as '/bin/true' because libtool requires automake!

* Wed May 22 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5-12k) [kakure 1.6.1]
- added 1.6.1 (as an option)
- /usr/bin/* -> %{_bindir}/explicit_name
- why not simply update: because it seems to break aclocal-old
 compatibility... any idea?

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.5-10k)
- /sbin/install-info -> info in PreReq.

* Fri Aug 31 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5-5k)
- merging automake 1.5 and 1.4-p5

* Thu Aug 30 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5-3k)
- up to 1.5
- remove libtool.patch (no needed)

* Fri Aug 24 2001 Toru Hoshina <toru@df-usa.com>
- (1.4p5-3k)
- merge to Jirai.

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- (1.4p5-2k)

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Sun Jul  2 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.6.0 

* Fri Apr 21 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, Distribution, description )

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.4-6).

* Fri Feb 04 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix bug #8870

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Aug 21 1999 Jeff Johnson <jbj@redhat.com>
- revert to pristine automake-1.4.

* Mon Mar 22 1999 Preston Brown <pbrown@redhat.com>
- arm netwinder patch

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Mon Feb  8 1999 Jeff Johnson <jbj@redhat.com>
- add patches from CVS for 6.0beta1

* Sun Jan 17 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.4.

* Mon Nov 23 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.3b.
- add URL.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 07 1998 Erik Troan <ewt@redhat.com>
- updated to 1.3

* Tue Oct 28 1997 Cristian Gafton <gafton@redhat.com>
- added BuildRoot; added aclocal files

* Fri Oct 24 1997 Erik Troan <ewt@redhat.com>
- made it a noarch package

* Thu Oct 16 1997 Michael Fulbright <msf@redhat.com>
- Fixed some tag lines to conform to 5.0 guidelines.

* Thu Jul 17 1997 Erik Troan <ewt@redhat.com>
- updated to 1.2

* Wed Mar 5 1997 msf@redhat.com <Michael Fulbright>
- first version (1.0)
