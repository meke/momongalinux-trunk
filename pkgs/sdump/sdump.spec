%global momorel 15
%define version_suffix b

Summary: Reconstructs and dumps a TCP stream
Name: sdump
Version: 0.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source0: http://tf.happyhacking.net/archive/%{name}/%{name}-%{version}%{version_suffix}.tgz
Patch0: sdump-0.2b-glibconfig.patch
URL: http://tf.happyhacking.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib >= 1.2
BuildRequires: libnet-devel
BuildRequires: libnids-devel >= 1.24
BuildRequires: libpcap-devel >= 1.1.1

%description
sdump is the small tool which reconstructs and dumps a TCP stream.

%prep
rm -rf %{buildroot}
%setup -q -n %{name}-%{version}%{version_suffix}
%patch0 -p1 -b .glibconfig~

%build
%make

%{__chmod} 644 README
%{__chmod} 644 CHANGES

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_bindir}
%{__install} -c -m 755 src/%{name} %{buildroot}%{_bindir}/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README CHANGES
%{_bindir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-13m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-12m)
- rebuild against libpcap-1.1.1

* Thu Apr  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-11m)
- rebuild against libnids-1.24

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-9m)
- fix BuildRequires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-8m)
- rebuild against rpm-4.6

* Sun Sep 21 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.2-7m)
- modifiy BuildRequires

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-6m)
- rebuild against gcc43

* Thu Jun 07 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-5m)
- rebuild against libpcap-0.9.5 (restrict by version)

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-4m)
- rebuild against libpcap-0.9.5

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.2-3m)
- enable x86_64.

* Tue Jan 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-2m)
- update to 0.2b
- change Source0 URI
- No NoSource0
- fix documents permission

* Fri Oct 31 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.2-1m)
- Initial import.
