%global momorel 16

Summary: The XmBDFEd BDF Font Editor
Name: xmbdfed
Version: 4.7
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/Productivity
Source0: http://crl.nmsu.edu/~mleisher/%{name}-%{version}.tar.bz2
Patch0: xmbdfed-4.7-Makefile.patch
Patch1: xmbdfed-4.7-lib64.patch
Patch2: xmbdfed-4.7-gcc4.patch
Patch3: xmbdfed-4.7-glibc210.patch
URL: http://crl.nmsu.edu/~mleisher/xmbdfed.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: openmotif
BuildRequires: openmotif-devel, freetype-devel

%description
XmBDFEditor is a Motif-based BDF font editor

%prep
%setup -q 
%patch0 -p1
%patch2 -p1 -b .gcc4
%if %{_lib} == "lib64"
%patch1 -p1 -b .lib64~
%endif
%patch3 -p1 -b .glibc210

%build
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
install -m 755 xmbdfed %{buildroot}%{_bindir}
install -m 644 xmbdfed.man %{buildroot}%{_mandir}/man1/xmbdfed.1

%clean
rm -rf --preserve-root %{buildroot}

%files 
%defattr(-, root, root)
%doc CHANGES COPYRIGHTS README xmbdfedrc
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7-16m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7-13m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7-12m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7-10m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7-8m)
- rebuild against gcc43

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7-7m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.7-6m)
- rebuild against openmotif-2.3.0-beta2

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.7-5m)
- revised installdir

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7-4m)
- add gcc4 patch.
- Patch2: xmbdfed-4.7-gcc4.patch

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.7-3m)
- enable x86_64.

* Sat Dec  4 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.7-2m)
- rebuild against openmotif-2.2.4

* Thu Mar 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.7-1m)
- major feature enhancements

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.5-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (4.5-6k)
- rebuild against openmotif-2.2.2-2k.

* Wed Feb  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (4.5-4k)
- apply an official patch

* Mon Nov 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (4.5-2k)

* Thu Oct  5 2000 Kazuhiko <kazuhiko@kondara.org>
- (4.4-1k) initial release
- revise Makefile and bdf.h
- change fallback fontList resource from 100dpi to 75dpi
