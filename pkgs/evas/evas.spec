%global momorel 1
#%%global snapdate 2010-06-27

Summary: evas
Name: evas
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2 
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: evas-fix-DSO.patch
Patch1: evas-1.7.7-frbidi-header-path.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig, doxygen
BuildRequires: libX11-devel, libXext-devel, libXrender-devel
BuildRequires: freetype-devel
BuildRequires: fribidi-devel
BuildRequires: libjpeg-devel >= 8a, libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: eet-devel >= 1.7.7
BuildRequires: eina-devel >= 1.7.7
BuildRequires: cairo-devel >= 1.0.0

%description
Evas is a clean display canvas API for several target display systems
that can draw anti-aliased text, smooth super and sub-sampled scaled
images, alpha-blend objects and much more.


%package devel
Summary: Evas headers, static libraries, documentation and test programs
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p0
%patch1 -p1

%build
%configure --disable-static \
  --enable-simple-x11 \
  --enable-buffer \
  --enable-software-x11 \
  --enable-xrender-x11 \
  --enable-gl-x11 \
  --enable-software-xcb \
  --enable-xrender-xcb \
  --enable-software-sdl \
  --enable-fb \
  --enable-software-16-x11
#  --enable-directfb \
#  --enable-gl-glew \

make %{?_smp_mflags}
sed -i -e 's/$projectname Documentation Generated: $datetime/$projectname Documentation/' doc/foot.html

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p' transform='s,x,x,'
chrpath --delete %{buildroot}%{_libdir}/%{name}/modules/*/*/*/*.so
chrpath --delete %{buildroot}%{_libdir}/%{name}/cserve2/*/*/*/*.so
find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/libevas.so.*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/modules/*/*/*/module.so
%{_libdir}/%{name}/cserve2/*/*/*/module.so
%{_libexecdir}/dummy_slave
%{_libexecdir}/evas_cserve2
%{_libexecdir}/evas_cserve2_slave
%{_bindir}/evas_cserve*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/checkme
%{_datadir}/%{name}/examples


%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-2m)
- rebuild against libtiff-4.0.1

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49539-1m)
- update to new svn snap

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.063-2m)
- rebuild against libjpeg-8a

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.062-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9.062-2m)
- rebuild against libjpeg-7

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.062-1m)
- update to new svn snap

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.060-1m)
- update 

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.9.9.050-3m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.050-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.050-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.043-1m)
- update to 0.9.9.043
- add --enable-fb for ecore

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.042-1m)
- merge from T4R
-
- changelog is below
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (0.9.9.042-1m)
- - update to 0.9.9.042
- - delete %%{_bindir}/evas-config
- - add %%{_libdir}/pkgconfig/evas*.pc

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.037-0.20061231.4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.037-0.20061231.3m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.9.037-0.20061231.2m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Mon Jan  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.037-0.20061231.1m)
- version 0.9.9.037-20061231

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.022-0.20051209.1m)
- version 0.9.9.022-20051209

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.9.010-0.20050627.2m)
- build fix for non-SSE arch.

* Tue Jun 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.010-0.20050627.1m)
- version 0.9.9.010-20050627

* Thu Feb 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-0.20050215.1m)
- version 1.0.0-0.20050215

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.20041218.4m)
- rebuild without DirectFB

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.0-0.20041218.3m)
- enable x86_64.
  use '%%ifarch %%{ix86}' instead of '%%ifnarch ppc'

* Mon Jan 03 2005 mutecat <mutecat@momonga-linux.org>
- (1.0.0-0.20041218.2m)
- ppc without --enable-cpu-mmx

* Tue Dec 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-0.20041218.1m)
- first import to Momonga

* Sat Jun 23 2001 The Rasterman <raster@rasterman.com>
- Created spec file

