%global         momorel 1

Name:           lrzip
Version:        0.614
Release:        %{momorel}m%{?dist}
Summary:        Compression program optimised for large files

Group:          Applications/File
License:        GPLv2+
URL:            http://ck.kolivas.org/apps/lrzip/
Source0:        http://ck.kolivas.org/apps/lrzip/lrzip-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  bzip2-devel lzo-devel nasm

%description
Long Range ZIP or Lzma RZIP

This is a compression program optimised for large files. The larger the file
and the more memory you have, the better the compression advantage this will
provide, especially once the files are larger than 100MB. The advantage can
be chosen to be either size (much smaller than bzip2) or speed (much faster
than bzip2). Decompression is always much faster than bzip2.

%package devel
License:        LGPLv2+
Group:          System Environment/Libraries
Summary:        header files and libraries for %{name}

%description devel
%{summary}.

%prep
%setup -q


%build
%configure --enable-static=no
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -name \*.la | xargs rm -f


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/lrzip
%{_bindir}/lrztar
%{_bindir}/lrunzip
%{_bindir}/lrzcat
%{_bindir}/lrzuntar
%{_libdir}/*.so.*
%{_docdir}/%{name}
%{_mandir}/man1/lrzip.1.*
%{_mandir}/man1/lrunzip.1.*
%{_mandir}/man1/lrztar.1.*
%{_mandir}/man1/lrzcat.1.*
%{_mandir}/man1/lrzuntar.1.*
%{_mandir}/man5/lrzip.conf.5.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.614-1m)
- update to 0.614

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.606-1m)
- update to 0.606

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.45-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.45-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.45-2m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.45-1m)
- update 0.45

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-2m)
- rebuild against rpm-4.6

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23-1m)
- update 0.23

* Mon Jul  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-1m)
- import from Fedora to Momonga

* Fri Feb 08 2008 Warren Togami <wtogami@redhat.com> 0.19-1
- 0.19 and rebuild for gcc-4.3

* Mon Nov 12 2007 Warren Togami <wtogami@redhat.com> 0.18-1
- initial package
