%global momorel 14

Summary: A version control system
Name: cvs
Version: 1.12.13
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Source0: http://ftp.gnu.org/non-gnu/cvs/source/feature/%{version}/cvs-%{version}.tar.bz2
NoSource: 0
URL: http://cvs.nongnu.org/

## for CVE-2012-0804
Patch100: cvs-1.12.13-Fix-proxy-response-parser.patch

Requires(post): info
Requires(preun): info
BuildRequires: autoconf, automake
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: libtool
BuildRequires: krb5-devel
BuildRequires: e2fsprogs-devel >= 1.41.9
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRequires: openssl-devel >= 1.0.0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
CVS (Concurrent Version System) is a version control system which can
record the history of your files (usually, but not always, source
code). CVS only stores the differences between versions, instead of
every version of every file you've ever created. CVS also keeps a log
of who, when and why changes occurred.
#'

CVS is very helpful for managing releases and controlling the
concurrent editing of source files among multiple authors. Instead of
providing version control for a collection of files in a single
directory, CVS provides version control for a hierarchical collection
of directories consisting of revision controlled files.  These
directories and files can then be combined together to form a software
release.

Install the cvs package if you need to use a version control system.

%prep
%setup -q
%patch100 -p1 -b .CVE-2012-0804

%build
CFLAGS="`echo %{optflags} | sed -e 's/-Wp,-D_FORTIFY_SOURCE=2//'`"
%configure --with-gssapi --enable-encryption --with-external-zlib
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall
%__rm %{buildroot}%{_bindir}/rcs2log
%__mv %{buildroot}%{_datadir}/cvs/contrib/rcs2log %{buildroot}%{_bindir}
%__chmod -x %{buildroot}%{_datadir}/cvs/contrib/*

# remove unwanting file.
rm -f %{buildroot}%{_datadir}/info/dir

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/cvs.info %{_infodir}/dir
/sbin/install-info %{_infodir}/cvsclient.info %{_infodir}/dir 

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/cvs.info %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/cvsclient.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING* ChangeLog* DEVEL-CVS FAQ HACKING
%doc MINOR-BUGS NEWS PROJECTS README* TESTS TODO
#%%doc README.tunnel
%doc doc/*.pdf
%{_bindir}/cvs
%{_bindir}/cvsbug
%{_bindir}/rcs2log
%{_mandir}/man1/cvs.1*
%{_mandir}/man5/cvs.5*
%{_mandir}/man8/cvsbug.8*
%{_infodir}/cvs*
%{_datadir}/cvs

%changelog
* Fri Feb 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.13-14m)
- [SECURITY] CVE-2012-0804

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.13-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.13-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12.13-11m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.13-10m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.13-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.13-8m)
- rebuild against e2fsprogs-1.41.9

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.13-7m)
- temporarily drop -Wp,-D_FORTIFY_SOURCE=2
-- /usr/bin/autopoint fails
- fix install-info

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.13-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.13-5m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.13-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12.13-3m)
- rebuild against gcc43

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12.13-2m)
- change Source URL

* Mon Apr 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12.13-1m)
- update to 1.12.13
- change .ps to .pdf at %%doc section

* Wed Jul 27 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.12.12-1m)
- major version up

* Tue Apr 19 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.20-1m)
- security fix
- http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0753

* Sun Feb 20 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11.19-1m)
- upgrade
- rebuild against automake

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.17-3m)
- add Patch10: cvs-1.11.17-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.17-2m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m
- tmp comment out %%patch7 , and comment out %%doc README.tunnel

* Thu Jun 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.17-1m)
- update to 1.11.17
- security update

* Thu May 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.11.16-1m)
- update to 1.11.16
- security update

* Thu Apr 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.15-1m)
- security update
- revise IPV6 patch and tunnel-v6 patch for cvs-1.11.15

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.11.14-2m)
- revised spec for enabling rpm 4.2.

* Fri Mar 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.14-1m)

* Mon Feb 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.13-1m)

* Fri Dec 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.11-1m)
- security update
- Source0 from ring server

* Tue Dec  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.11.10-1m)
- update to 1.11.10
- [SECURITY]
  this release contains security update
- use aclocal-1.7, automake-1.7

* Thu Nov 13 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11.5-8m)
- add BuildPreReq: krb5-devel

* Thu Nov 13 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.11.5-7m)
- add --with-gssapi --enable-encryption to %%configure
- fix cvs-1.11.5-IPv6.patch to be compiled with --with-gssapi (Thanks to Hiromasa YOSHIMOTO)

* Wed Jul  2 2003 zunda <zunda at freeshell.org>
- (kossori)
- source URL changed (Thanks to Mr. Tanaka)
- explicit specification of README.*'s in %doc to avoid including README.tunnel.tunnel

* Thu May  1 2003 OZAWA Sakuro <crouton@ex-machina.jp>
- (1.11.5-6m)
- remove more dependency by chmod -x.
- use real rcs2log, instead of symlink to contrib one.

* Wed Apr 23 2003 zunda <zunda at freeshell.org>
- (1.11.5-5m)
- chmod -x /usr/share/cvs/contrib/sccs2rcs to avoid dependency to /bin/csh

* Fri Apr 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.5-4m)
- rebuild against zlib 1.1.4-5m
- add BuildPrereq: zlib-devel >= 1.1.4-5m
- delete define ver

* Tue Feb  4 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.11.5-3m)
- modify and rename tmprace patch to work rcs2log correctly

* Tue Jan 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.11.5-2m)
- revice IPv6 patch for 1.11.5

* Tue Jan 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.5-1m)
- major security fixes

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.11.2-15m)
- rebuild against gcc-3.2 with autoconf-2.53

* Tue Aug 20 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.11.2-14m)
- use autoconf(not autoconf-old)

* Wed Aug 07 2002 Kenta MURATA <muraken2@nifty.com>
- (1.11.2-13m)
- remove patch cvs-1.11.2-LIBOBJS.patch.
- autoconf-old.

* Wed Aug 07 2002 Kenta MURATA <muraken2@nifty.com>
- (1.11.2-12m)
- add patch cvs-1.11.2-LIBOBJS.patch.

* Tue Jul 30 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.11.2-11m)
- write rfc2732 support code for pserver tunnel(only for addresses from environment variables)
- modify tunnel patch to accept proxy host without port number from environment variable.

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.2-10m)
- add more documents to %doc

* Tue Jul 30 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.11.2-9m)
-  add cvs-1.11.2-200207251514-tunnel-v6.patch

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.11.2-8m)
- apply ftp://ftp.jpl.org/pub/misc/cvs-1.11.2-200207251514-tunnel.patch.gz

* Tue Jul  9 2002 YAMAZAKI Makoto <zaki@momonga-linux.org>
- (1.11.2-7m)
- ipv6: import ipv6 patch from http://www.manabi.gr.jp/~blend/
- ipv6: add rfc2732 support

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.11.2-6k) 
- cancel gcc-3.1 autoconf-2.53

* Sat May 25 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.11.2-4k)                      
- autoconf-2.53

* Mon Apr 22 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.11.2-2k)                      
- update to 1.11.2

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (1.11.1p1-4k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.11.1p1-2k)
- update to 1.11.1p1

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.11-4k)
- errased IPv6 function with %{_ipv6} macro

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.11

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct 09 2000 Motonobu Ichimura <famao@kondara.org>
- add ipv6 patch from PLD (http://www.pld.org.pl)

* Wed Jul 12 2000 AYUHANA Tomonori <l@kondara.org>
- (1.10.8-6k)
- disable kerberos

* Sun Jun 18 2000 AYUHANA Tomonori <l@kondara.org>
- ported form RawHide
- SPEC fixed ( BuildRoot, Distribution, URL )
- %{makeinstall} -> make install

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new build environment (release 5)
- FHS tweaks
- actually gzip the info pages

* Wed May 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- reverse sense of conditional kerberos dependency
- add kerberos IV patch from Ken Raeburn
- switch to using the system's zlib instead of built-in
- default to unstripped binaries

* Tue Apr  4 2000 Bill Nottingham <notting@redhat.com>
- eliminate explicit krb5-configs dependency

* Mon Mar 20 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.10.8

* Wed Mar  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- make kerberos support conditional at build-time

* Wed Mar  1 2000 Bill Nottingham <notting@redhat.com>
- integrate kerberos support into main tree

* Mon Feb 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- build with gssapi auth (--with-gssapi, --with-encryption)
- apply patch to update libs to krb5 1.1.1

* Fri Feb 04 2000 Cristian Gafton <gafton@redhat.com>
- fix the damn info pages too while we're at it.
- fix description
- man pages are compressed
- make sure %post and %preun work okay

* Mon Jan 31 2000 Takaaki Tabuchi <tab@kondara.org>
- update to version 1.10.8.
- add CFLAGS.
- comment out %patch1.

* Sun Jan 9 2000  Jim Kingdon <http://bugzilla.redhat.com/bugzilla>
- update to 1.10.7.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Jul 14 1999 Jim Kingdon <http://developer.redhat.com>
- add the patch to make 1.10.6 usable
  (http://www.cyclic.com/cvs/dev-known.html).

* Tue Jun  1 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.10.6.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Mon Feb 22 1999 Jeff Johnson <jbj@redhat.com>
- updated text in spec file.

* Mon Feb 22 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.10.5.

* Tue Feb  2 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.10.4.

* Tue Oct 20 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.10.3.

* Mon Sep 28 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.10.2.

* Wed Sep 23 1998 Jeff Johnson <jbj@redhat.com>
- remove trailing characters from rcs2log mktemp args

* Thu Sep 10 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.10.1

* Mon Aug 31 1998 Jeff Johnson <jbj@redhat.com>
- fix race conditions in cvsbug/rcs2log

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.10.

* Wed Aug 12 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.9.30.

* Mon Jun 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr

* Mon Jun  8 1998 Jeff Johnson <jbj@redhat.com>
- build root
- update to 1.9.28

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Oct 29 1997 Otto Hammersmith <otto@redhat.com>
- added install-info stuff
- added changelog section
