%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global akonadiver 1.12.1
%global strigiver 0.7.8

Summary: Personal Information Management (PIM) for KDE
Name: kdepim
Version: %{ftpdirver}
Epoch: 1
Release: %{momorel}m%{?dist}
Group: Applications/Productivity
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
# revert http://websvn.kde.org/?view=revision&revision=1072331 (rebased to 4.6)
# for kopete-cryptography (mostly)
Patch0: %{name}-4.11.90-install_kleopatra_headers.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: %{name}-libs = %{epoch}:%{version}-%{release}
Requires: kde-runtime >= %{kdever}
Requires: oxygen-icon-theme

##Optional bits
Requires(hint): pinentry
Requires(hint): spambayes

BuildRequires: akonadi-devel >= %{akonadiver}
BuildRequires: bison flex
BuildRequires: boost-devel >= 1.50.0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: coreutils
BuildRequires: cyrus-sasl-devel
BuildRequires: dblatex
BuildRequires: desktop-file-utils
BuildRequires: gnokii-devel >= 0.6.28
BuildRequires: gpgme-devel
BuildRequires: grantlee-devel
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{kdever}-%{kdepimlibsrel}
BuildRequires: libXpm-devel libXScrnSaver-devel
BuildRequires: libindicate-qt-devel
# TODO: libmapi from http://www.openchange.org/
#BuildRequires: libmapi-devel
BuildRequires: libmsn-devel >= 0.4
BuildRequires: libxslt-devel
BuildRequires: lockdev-devel
%ifnarch s390 s390x
BuildRequires: pilot-link-devel >= 0.12.3-3m
%endif
BuildRequires: python-devel
BuildRequires: qca2-devel
BuildRequires: strigi-devel >= %{strigiver}
BuildRequires: shared-desktop-ontologies >= 0.7.1
BuildRequires: zlib-devel
# these seem mostly bogus, not checked for, but linked nonetheless -- Rex
BuildRequires: giflib-devel
BuildRequires: openldap-devel
BuildRequires: pcre-devel
BuildRequires: prison-devel
Requires: gnupg
Requires: pinentry-gui
Requires: spambayes

Obsoletes: kmobiletools
Provides:  kmobiletools
Obsoletes: libopensync-plugin-kdepim

%description
kdepim is a collection of Personal Information Management (PIM) tools for
the K Desktop Enviromnent (KDE).
kdepim contains the following applications:

  kaddressbook: The KDE addressbook application.
  kandy: sync phone book entries between your cell phone and computer
  kmail: the KDE mail client
  kmailcvt: tool for importing mail related data from other programs
  knode: news client
  korganizer: a calendar-of-events and todo-list manager
  kpilot: synchronizing data with a Palm(tm) or compatible PDA
  kalarm: gui for setting up personal alarm messages, emails and commands
  kalarmd: alarm monitoring daemon, shared by korganizer and kalarm
  karm: Time tracker.
  knotes: Post-It notes on the desktop

and

  kontact: a unified interface that draws KDE's email, calendaring, address book, notes and other PIM features together into a familiar configuration.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}-libs = %{epoch}:%{version}-%{release}
Requires: kdelibs-devel

%description devel
%{summary}.
Install %{name}-devel if you want to write or compile %{name} plugins.

%package libs
Summary: %{name} runtime libraries
Group:   System Environment/Libraries
Requires: %{name} = %{epoch}:%{version}-%{release}

%description libs
%{summary}.

%prep
%setup -q
%patch0 -p1 -b .install_kleopatra_headers

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        -DKDEPIM_BUILD_MOBILE:BOOL=OFF \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

chrpath --list   %{buildroot}%{_kde4_bindir}/kabc2mutt && \
chrpath --delete %{buildroot}%{_kde4_bindir}/kabc2mutt

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
touch --no-create %{_kde4_iconsdir}/locolor &> /dev/null ||:
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/locolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  touch --no-create %{_kde4_iconsdir}/locolor &> /dev/null ||:
  touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/locolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  update-desktop-database -q &> /dev/null ||:
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README
%doc COPYING
%{_sysconfdir}/dbus-1/system.d/org.kde.kalarmrtcwake.conf
%{_kde4_bindir}/*
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/kde4/plugins/grantlee/0.3/grantlee_messageheaderfilters.so
%{_kde4_libdir}/akonadi/contact/editorpageplugins
%{_kde4_libexecdir}/kalarm_helper
%{_kde4_datadir}/akonadi/agents/*.desktop
%{_kde4_datadir}/applications/kde4/*
%{_kde4_appsdir}/akonadi_archivemail_agent
%{_kde4_appsdir}/akonadi_mailfilter_agent
%{_kde4_appsdir}/akonadi_notes_agent
%{_kde4_appsdir}/akonadi_sendlater_agent
%{_kde4_appsdir}/akonadiconsole
%{_kde4_appsdir}/akregator
%{_kde4_appsdir}/akregator_sharemicroblog_plugin
%{_kde4_appsdir}/blogilo
%{_kde4_appsdir}/composereditor
%{_kde4_appsdir}/contactthemeeditor
%{_kde4_appsdir}/desktoptheme/default/widgets/*
%{_kde4_appsdir}/headerthemeeditor
%{_kde4_appsdir}/kaddressbook
%{_kde4_appsdir}/kalarm
%{_kde4_appsdir}/kconf_update/*
%{_kde4_appsdir}/kdepimwidgets
%{_kde4_appsdir}/kjots
%{_kde4_appsdir}/kleopatra
%{_kde4_appsdir}/kmail2
%{_kde4_appsdir}/kmailcvt
%{_kde4_appsdir}/knode
%{_kde4_appsdir}/knotes
%{_kde4_appsdir}/kontact
%{_kde4_appsdir}/kontactsummary
%{_kde4_appsdir}/korgac
%{_kde4_appsdir}/korganizer
%{_kde4_appsdir}/ktimetracker
%{_kde4_appsdir}/ktnef
%{_kde4_appsdir}/kwatchgnupg
%{_kde4_appsdir}/libkleopatra
%{_kde4_appsdir}/libmessageviewer
%{_kde4_appsdir}/messagelist
%{_kde4_appsdir}/messageviewer
%{_kde4_appsdir}/pimsettingexporter
%{_kde4_appsdir}/sieve
%{_kde4_appsdir}/sieveeditor
%{_kde4_appsdir}/storageservicemanager
%{_datadir}/dbus-1/interfaces/*.xml
%{_datadir}/dbus-1/system-services/org.kde.kalarmrtcwake.service
%{_datadir}/polkit-1/actions/org.kde.kalarmrtcwake.policy
%{_kde4_datadir}/autostart/*
%{_kde4_datadir}/config/*rc
%{_kde4_datadir}/config.kcfg/*
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_iconsdir}/locolor/*/*/*
%{_kde4_iconsdir}/oxygen/*/*/*
%{_kde4_datadir}/kde4/services/kontact
%{_kde4_datadir}/kde4/services/korganizer
%{_kde4_datadir}/kde4/services/kresources/kcal/*
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/services/*.protocol
%{_kde4_datadir}/kde4/services/ServiceMenus/kmail_addattachmentservicemenu.desktop
%{_kde4_datadir}/kde4/servicetypes/*
%{_kde4_datadir}/doc/HTML/*/akonadi_archivemail_agent
%{_kde4_datadir}/doc/HTML/*/akonadi_notes_agent
%{_kde4_datadir}/doc/HTML/*/akonadi_sendlater_agent
%{_kde4_datadir}/doc/HTML/*/akregator
%{_kde4_datadir}/doc/HTML/*/blogilo
%{_kde4_datadir}/doc/HTML/*/contactthemeeditor
%{_kde4_datadir}/doc/HTML/*/headerthemeeditor
%{_kde4_datadir}/doc/HTML/*/importwizard
%{_kde4_datadir}/doc/HTML/*/kabcclient
%{_kde4_datadir}/doc/HTML/*/kalarm
%{_kde4_datadir}/doc/HTML/*/kioslave/*
%{_kde4_datadir}/doc/HTML/*/kjots
%{_kde4_datadir}/doc/HTML/*/kleopatra
%{_kde4_datadir}/doc/HTML/*/kmail
%{_kde4_datadir}/doc/HTML/*/kmailcvt
%{_kde4_datadir}/doc/HTML/*/knode
%{_kde4_datadir}/doc/HTML/*/knotes
%{_kde4_datadir}/doc/HTML/*/konsolekalendar
%{_kde4_datadir}/doc/HTML/*/kontact-admin
%{_kde4_datadir}/doc/HTML/*/kontact
%{_kde4_datadir}/doc/HTML/*/korganizer
%{_kde4_datadir}/doc/HTML/*/ktimetracker
%{_kde4_datadir}/doc/HTML/*/ktnef
%{_kde4_datadir}/doc/HTML/*/kwatchgnupg
%{_kde4_datadir}/doc/HTML/*/pimsettingexporter
%{_kde4_datadir}/doc/HTML/*/sieveeditor
%{_mandir}/man1/*

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/lib*.so.*
%{_kde4_libdir}/kde4/plugins/designer/*
%{_kde4_libdir}/kde4/plugins/accessible/messagevieweraccessiblewidgetfactory.so

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/kleo
%{_kde4_includedir}/kpgp
%{_kde4_includedir}/libkleopatraclient
%{_kde4_libdir}/lib*.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.97-1m)
- update to KDE 4.9 RC2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.8.4-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1:4.8.4-2m)
- rebuild for boost 1.50.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.1-3m)
- fix kalarm bug

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.1-2m)
- fix version

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.1-1m)
- update to KDE 4.8.1

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.8.0-2m)
- add BuildRequires

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.7.90-2m)
- rebuild for boost-1.48.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.1-1m)
- update to KDE 4.7.1

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.7.0-2m)
- rebuild for boost

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:4.6.1-3m)
- really build with new shared-desktop-ontologies

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.6.1-2m)
- rebuild against cmake-2.8.5-2m and shared-desktop-ontlogies-0.7.1

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.6.1-1m)
- update to kdepim-4.6.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.6.0-1m)
- update to kdepim-4.6.0

* Sun Jun  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.97-1m)
- update to kdepim-4.6rc2

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.96-1m)
- update to kdepim-4.6rc1

* Sat Apr 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.95-1m)
- update to kdepim-4.6beta5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.5.94.1-4m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.5.94.1-3m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.5.94.1-2m)
- rebuild against boost-1.46.0

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.94.1-1m)
- update to kdepim-4.6beta4

* Fri Jan  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:4.5.93-3m)
- get rid of conflicting translation files

* Fri Dec 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:4.5.93-2m)
- add missing categories to *-mobile.desktop, Merry Christmas!

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.93-1m)
- update to kdepim-4.6beta3

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.80-4m)
- add patch0 to install missing headers

* Wed Dec  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:4.5.80-3m)
- install headers to enable build kopete-cryptography

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4.93-3m)
- import patch10 from Fedora devel

* Tue Nov  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.4.93-2m)
- rebuild against boost 1.44.0
- add patch for boost 1.44

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4.93-1m)
- update to kdepim-4.4.93 (4.5 beta3)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5-0.1.3m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5-0.1.2m)
- do not use find_lang to avoid conflicting with kdelibs

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5-0.1.1m)
- update to 4.5-beta1

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Sun Feb 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-2m)
- import patch0 for devel package

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-2m)
- rebuild against gnokii-0.6.28

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- set BR: akonadi-devel >= 1.2.0 for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuld with new source

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-liux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-liux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Sat Mar 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.1-2m)
- fix up package splitting

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- import upstream patch
- remove unused patches

* Fri Feb 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-2m)
- [SECURITY] apply upstream security fix
  (code execution when following links)
- http://websvn.kde.org/?view=rev&revision=927081
- https://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/332069

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- add patch100 from Fedora devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sun Dec 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.85-3m)
- rebuild against pilot-link-0.12.3-3m

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.85-2m)
- rebuild against gnokii-0.6.27

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Tue Oct 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-2m)
- remove conflicting files with kdepim from kdepim-libs

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Thu Sep  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-5m)
- fix up karm.desktop

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-4m)
- revise ktnef.desktop

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-3m)
- rebuild against akonadi-1.0.0

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62

- * Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

* Wed Jul 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- fix up akonadiconsole.desktop again

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Fri Jul 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.98-2m)
- remove appointment-new.png to avoid conflicting with oxygen-icon-theme-4.0.98

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 1

* Sun Jun  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-4m)
- fix package splitting

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-3m)
- Obsoletes: libopensync-plugin-kdepim

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- fix up akonadiconsole.desktop and kjotspart.desktop

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-5m)
- add link of missing icons for menu entry
- Requires: kdeartwork-icons-crystalsvg for kpalmdoc.png
- import gnokii-no-libintl.patch from Fedora
 +* Tue Apr 01 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 6:3.5.9-7
 +- fix gnokii detection (thanks to Dirk Muller)

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.9-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.9-2m)
- remove BPR libtermcap-devel

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-1m)
- update to KDE 3.5.9
- apply the latest svn fixes to "enable final"
- set enable_final 0
- import upstream patches from Fedora
- build with libopensync again

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-4m)
- build without libopensync for the moment

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-3m)
- remove %%{_datadir}/config.kcfg/pimemoticons.kcfg to avoid conflicting with kdepimlibs

* Tue Feb 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-2m)
- modify headers directory
- remove conflicting files and directories with KDE4

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Thu Jul 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-3m)
- add Requires: gnupg2

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- enable new kitchensync

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete patch1
- adjust %%files section

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-3m)
- rebuild against kdeligs etc.

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-2m)
- rebuild against pilot-link-0.12.1 for support kpilot

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-11m)
- import upstream patch to fix following problem
  #139941, Fix invalid MapQuest URL

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-10m)
- import upstream patch to fix following problem
  #139370, postpone alarm: incorrent items for date chooser

* Fri Jan 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-9m)
- import upstream patch to fix following problem
  #123350, Kontact crashes if you click "OK" before completing "Resourse Selection"

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-8m)
- import upstream patch to fix following problem
  #113100, Incorrect mime headers in attachment of files with russian names

* Sun Dec 24 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-7m)
- import upstream patch to fix following problem
  #131597, reoccuring todos not written to journal when completed

* Thu Dec 21 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-6m)
- import upstream patch to fix following problem
  #136954, Reminders from outlook invites are set to unknown in exchange resources

* Wed Dec 20 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.5-5m)
- suppport PPC64 architecture
-- unuse --enable-final
-- add optflags : -mminimal-toc

* Sat Dec 16 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- import upstream patch to fix following problem
  #101696, korganizer crash at startup for a particular std.ics file

* Tue Nov 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.5-3m)
- rebuild against gnupg2-2.0.0-1m

* Mon Nov 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- rebuild against gnokii-0.6.14-1m
- add BuildPreReq: gnokii-devel
- import upstream patch to fix following problem
  #134423, rename of distribution list fails, list becomes unusable

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- import 4 upstream patches from Fedora Core devel
 +* Mon Sep 04 2006 Than Ngo <than@redhat.com> 6:3.5.4-3
 +- apply upstream patches
 +   fix kde#116607, crash in slotCheckQueuedFolders() on application exit
 +* Tue Aug 15 2006 Than Ngo <than@redhat.com> 6:3.5.4-2
 +- apply patch to fix crash when right clicking in an encapsulated email message, kde#131067
 +* Thu Aug 10 2006 Than Ngo <than@redhat.com> 6:3.5.4-1
 +- apply upstream patches,
 +   - Kmail crashes on startup, kde#132008
 +   - Cannot send to addresses containing an ampersand (&), kde#117882

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-3m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Fri Aug 18 2006 Nishio Futoshi <futoshi@momonga-linix.org>
- (3.5.4-2m)
- rebuild against kdelibs-3.5.4-2m
- rebuild against kdebase-3.5.4-6m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- remove merged upstream patches

* Wed Jul 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- import 1 upstream patch from Fedora Core devel
 +* Thu Jul 06 2006 Than Ngo <than@redhat.com> 6:3.5.3-4
 +- apply upstream patches,
 +   fix bugs: 110487, 118112, 119112, 121384, 127210, 130303
- modify options of cofigure for crypto/certificate manager support

* Sat Jul  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- import 4 upstream patches from Fedora Core devel
 +* Tue Jun 27 2006 Than Ngo <than@redhat.com> 6:3.5.3-3
 +- apply upstream patches

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- revise %%files section
- disable hidden visibility

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- remove kandy-lockdev.patch

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdepim-3.4.2-uic.patch
- remove kdepim-3.4.2-kmail-partNode-109003.patch
- remove kdepim-3.4.2-kpilot.patch

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- import kdepim-3.4.2-uic.patch from Fedora Core devel
 +* Wed Sep 21 2005 Than Ngo <than@redhat.com> 6:3.4.2-4
 +- fix uic build problem

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure
- import two patches from Fedore Core devel
 - kdepim-3.4.2-kpilot.patch
  +* Mon Aug 15 2005 Than Ngo <than@redhat.com> 6:3.4.2-2
  +- apply patch to fix kpilot crash
 - kdepim-3.4.2-kmail-partNode-109003.patch
  +* Tue Aug 02 2005 Than Ngo <than@redhat.com> 6:3.4.2-1
  +- apply patch to fix kmail bug, kde#109003
- BuildPreReq: bison, flex

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- remove nokdelibsuff.patch

* Tue Jun 28 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.1-2m)
- fixed build error gcc4. import FC patch.
-   kdepim-3.4.0-gcc4.patch
-   admin-visibility.patch

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- modify %%files section
- BuildPreReq: gnupg, gnupg2, gpgme-devel

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- import kdepim-3.4.0-kandy-lockdev.patch from Fedora Core
- import kandy icons and kandy-icons.patch from Fedora Core
 +* Wed Mar 23 2005 Than Ngo <than@redhat.com> 6:3.4.0-4
 +- add lockdev support patch in kandy from Peter Rockai #84143
 +- add missing kandy icons #141165
- BuildPreReq: kdebase, libart_lgpl-devel

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-3m)
- rebuild against libtermcap-2.0.8-38m.

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- HOT FIX for kmail (http://dot.kde.org/1075969434/)
- modified %%files

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Wed Dec 31 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- rebuild against for qt-3.2.3

* Thu Sep 25 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Thu Aug 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Tue Jul  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-3m)
- add -fno-stack-protector if gcc is 3.3

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-2m)
- remove --disable-warnings from configure

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
   changes are:
     korganizer: Exchange plugin supports secure WebDAV.
     korganizer: Fix timezone handling when timezone names are Unicode strings.
- remove kdepim-3.1.1-korganizer-pref-timezone-fix.patch
- add kdepim-3.1.2-miscfix.patch
- add BuildPreReq: libmal-devel and add -I%{_includedir}/libmal to CXXFLAGS
- revive kalarm

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-4m)
- remove requires: qt-Xt

* Thu Apr  3 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-3m)
- correct timezone string in pref dialog(kdepim-3.1.1-korganizer-pref-timezone-fix.patch)
   See: [Kdeveloper:02753]

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    KOrganizer bug fixes:
      Use correct default duration for events crossing a day boundary (#53477).
      Correctly save category colors (#54913).
      Don't show todos more than once in what's next view.
      Include todos in print output of month view (#53291).
      Don't restrict maximum size of search dialog (#54912).
      Make cancel button of template selection dialog work (#54852).
      Don't break sorting when changing todos by context menu (#53680).
      Update views on changes of todos directly in the todo list (#43162).
      Save state of statusbar (#55380).
    knotes: Escape "&" in note titles

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- add URL

* Sun Feb  2 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.
- remove subpackage "kalarm" because it make failed.

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-3m)
- use autoconf-2.53

* Sat Oct 12 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.4-2m)
- revise %files (remove kpilot related directories)

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.
- remove Patch1.

* Wed Jul 24 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-3m)
- 3.0.2-2m is spec file fix miss. sorry.

* Wed Jul 24 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- add qt-3.0.5 build patch.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.

* Sun Jun  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Fri Apr  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Thu Nov  8 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-10k)
- nigirisugi.
- revised spec file.

* Tue Oct 30 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.2.1-8k)
- add pilot-link-devel to BuildPreReq tag
- revised spec file.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alpha2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Fri Mar 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.1.1-3k)

* Thu Mar 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.1-2k)

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-14k)
- rebuild against QT-2.3.0.

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-12k]
- backport 2.0.1-13k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-11k]
- rebuild against.

* Wed Dec 20 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- update patch.
- (kdepim-2.0.1-korganizer-i18n-20001205.diff).

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild against new environment glibc-2.2 egcs++

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Sat Nov 04 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- add kdepim-2.0-korganizer-i18n-20001101.diff
- add GIF Support switch.
- release.

* Fri Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- modified Rrequires,BuildPrereq.
- add %%define qtver.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.

* Mon Sep 25 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Sat Jun 24 2000 Kenichi Matsubara <m@kondara.org>
- Initial release for Kondara MNU/Linux.
