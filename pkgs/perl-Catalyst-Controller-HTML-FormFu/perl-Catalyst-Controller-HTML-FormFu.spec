%global         momorel 3

Name:           perl-Catalyst-Controller-HTML-FormFu
Version:        1.00
Release:        %{momorel}m%{?dist}
Summary:        Catalyst::Controller::HTML::FormFu Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Controller-HTML-FormFu/
Source0:        http://www.cpan.org/authors/id/C/CF/CFRANKS/Catalyst-Controller-HTML-FormFu-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Action-RenderView
BuildRequires:  perl-Catalyst-Component-InstancePerContext
BuildRequires:  perl-Catalyst-Plugin-ConfigLoader >= 0.23
BuildRequires:  perl-Catalyst-Plugin-Session
BuildRequires:  perl-Catalyst-Plugin-Session-State-Cookie
BuildRequires:  perl-Catalyst-Plugin-Session-Store-File
BuildRequires:  perl-Catalyst-Runtime >= 5.71001
BuildRequires:  perl-Catalyst-View-TT
BuildRequires:  perl-Class-C3
BuildRequires:  perl-Config-Any
BuildRequires:  perl-Config-General
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTML-FormFu >= 0.04001
BuildRequires:  perl-Moose
BuildRequires:  perl-MRO-Compat >= 0.10
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-PathTools
BuildRequires:  perl-Regexp-Assemble
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Template-Toolkit
BuildRequires:  perl-Test-Aggregate
BuildRequires:  perl-Test-Simple >= 0.92
BuildRequires:  perl-Test-WWW-Mechanize >= 1.16
BuildRequires:  perl-Test-WWW-Mechanize-Catalyst
Requires:       perl-Catalyst-Component-InstancePerContext
Requires:       perl-Catalyst-Runtime >= 5.71001
Requires:       perl-Class-C3
Requires:       perl-Config-Any
Requires:       perl-HTML-FormFu >= 0.04001
Requires:       perl-Moose
Requires:       perl-MRO-Compat >= 0.10
Requires:       perl-namespace-autoclean
Requires:       perl-PathTools
Requires:       perl-Regexp-Assemble
Requires:       perl-Scalar-Util
Requires:       perl-Task-Weaken
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
NAME    Catalyst-Controller::HTML::FormFu - Catalyst integration for
HTML::FormFu

%prep
%setup -q -n Catalyst-Controller-HTML-FormFu-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Catalyst/Controller/*
%{perl_vendorlib}/Catalyst/Helper/*
%{perl_vendorlib}/HTML/FormFu/Constraint/*
%{perl_vendorlib}/HTML/FormFu/Element/*
%{perl_vendorlib}/HTML/FormFu/Plugin/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-2m)
- rebuild against perl-5.16.2

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-1m)
- update to 0.09004

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-4m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-2m)
- rebuild against perl-5.14.1

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-1m)
- update to 0.09003

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09001-1m)
- update to 0.09001

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09000-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09000-2m)
- rebuild for new GCC 4.6

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09000-1m)
- update to 0.09000

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08002-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08002-2m)
- rebuild against perl-5.12.2

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08002-1m)
- update to 0.08002

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06001-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-2m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-1m)
- update to 0.06001

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06000-1m)
- update to 0.06000

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05000-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.05000-4m)
- expand BuildRequires

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.05000-3m)
- add BuildRequires:  perl-Regexp-Copy

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05000-2m)
- rebuild against perl-5.10.1

* Mon Jul 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05000-1m)
- update to 0.05000

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04003-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
