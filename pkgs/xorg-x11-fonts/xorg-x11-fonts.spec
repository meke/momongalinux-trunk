%global momorel 1

# NOTE: There are no ELF objects produced by the package, so no need
# for debuginfo.
%define debug_package %{nil}

%define _catalogue /etc/X11/fontpath.d

# FIXME: The _sysfontdir stuff doesn't work yet, so don't use it.  Once
# upstream fonts have a build time configureable output directory that is
# a sane mechanism, we can rethink this.
%define _x11fontdirprefix	%{_datadir}
# NOTE: Fonts strictly intended for X core fonts, should be installed
# into _x11fontdir.
%define _x11fontdir		%{_x11fontdirprefix}/X11/fonts
%define _type1_fontdir		%{_x11fontdir}
%define _otf_fontdir		%{_x11fontdir}
%define _ttf_fontdir		%{_x11fontdir}

# Configuration section
%define with_ethiopic_fonts	1
%if %{with_ethiopic_fonts}
%define ethiopic_fonts -a35
%else
%define ethiopic_fonts ""
%endif

Summary:	X.Org X11 fonts
Name:		xorg-x11-fonts
Version:	7.7
Release:	%{momorel}m%{?dist}
License:	MIT and "Lucida" and "Public Domain"
Group:		User Interface/X
URL:		http://www.x.org
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:	noarch

%define xorg_font_url http://xorg.freedesktop.org/archive/individual/font/
Source0:  %{xorg_font_url}/encodings-1.0.4.tar.bz2
NoSource: 0
Source11: %{xorg_font_url}/font-adobe-75dpi-1.0.3.tar.bz2
NoSource: 11
Source10: %{xorg_font_url}/font-adobe-100dpi-1.0.3.tar.bz2
NoSource: 10
Source13: %{xorg_font_url}/font-adobe-utopia-75dpi-1.0.4.tar.bz2
NoSource: 13
Source12: %{xorg_font_url}/font-adobe-utopia-100dpi-1.0.4.tar.bz2
NoSource: 12
Source14: %{xorg_font_url}/font-adobe-utopia-type1-1.0.4.tar.bz2
NoSource: 14
Source1:  %{xorg_font_url}/font-alias-1.0.3.tar.bz2
NoSource: 1
Source15: %{xorg_font_url}/font-arabic-misc-1.0.3.tar.bz2
NoSource: 15
Source17: %{xorg_font_url}/font-bh-75dpi-1.0.3.tar.bz2
NoSource: 17
Source16: %{xorg_font_url}/font-bh-100dpi-1.0.3.tar.bz2
NoSource: 16
Source19: %{xorg_font_url}/font-bh-lucidatypewriter-75dpi-1.0.3.tar.bz2
NoSource: 19
Source18: %{xorg_font_url}/font-bh-lucidatypewriter-100dpi-1.0.3.tar.bz2
NoSource: 18
# Luxi fonts are under a bad license.
# See: https://bugzilla.redhat.com/show_bug.cgi?id=317641
Source20: %{xorg_font_url}/font-bh-ttf-1.0.3.tar.bz2
NoSource: 20
Source21: %{xorg_font_url}/font-bh-type1-1.0.3.tar.bz2
NoSource: 21
Source23: %{xorg_font_url}/font-bitstream-75dpi-1.0.3.tar.bz2
NoSource: 23
Source22: %{xorg_font_url}/font-bitstream-100dpi-1.0.3.tar.bz2
NoSource: 22
Source24: %{xorg_font_url}/font-bitstream-speedo-1.0.2.tar.bz2
NoSource: 24
Source25: %{xorg_font_url}/font-bitstream-type1-1.0.3.tar.bz2
NoSource: 25
Source26: %{xorg_font_url}/font-cronyx-cyrillic-1.0.3.tar.bz2
NoSource: 26
Source27: %{xorg_font_url}/font-cursor-misc-1.0.3.tar.bz2
NoSource: 27
Source28: %{xorg_font_url}/font-daewoo-misc-1.0.3.tar.bz2
NoSource: 28
Source29: %{xorg_font_url}/font-dec-misc-1.0.3.tar.bz2
NoSource: 29
Source30: %{xorg_font_url}/font-ibm-type1-1.0.3.tar.bz2
NoSource: 30
Source31: %{xorg_font_url}/font-isas-misc-1.0.3.tar.bz2
NoSource: 31
Source32: %{xorg_font_url}/font-jis-misc-1.0.3.tar.bz2
NoSource: 32
Source33: %{xorg_font_url}/font-micro-misc-1.0.3.tar.bz2
NoSource: 33
Source34: %{xorg_font_url}/font-misc-cyrillic-1.0.3.tar.bz2
NoSource: 34
%if %{with_ethiopic_fonts}
Source35: %{xorg_font_url}/font-misc-ethiopic-1.0.3.tar.bz2
NoSource: 35
%endif
Source36: %{xorg_font_url}/font-misc-meltho-1.0.3.tar.bz2
NoSource: 36
Source37: %{xorg_font_url}/font-misc-misc-1.1.2.tar.bz2
NoSource: 37
Source38: %{xorg_font_url}/font-mutt-misc-1.0.3.tar.bz2
NoSource: 38
Source39: %{xorg_font_url}/font-schumacher-misc-1.1.2.tar.bz2
NoSource: 39
Source40: %{xorg_font_url}/font-screen-cyrillic-1.0.4.tar.bz2
NoSource: 40
Source41: %{xorg_font_url}/font-sony-misc-1.0.3.tar.bz2
NoSource: 41
Source42: %{xorg_font_url}/font-sun-misc-1.0.3.tar.bz2
NoSource: 42
Source43: %{xorg_font_url}/font-winitzki-cyrillic-1.0.3.tar.bz2
NoSource: 43
Source44: %{xorg_font_url}/font-xfree86-type1-1.0.4.tar.bz2
NoSource: 44

# FIXME: Temporary requirement on autoconf for a workaround.
BuildRequires: autoconf

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
# The font-utils dep here is to ensure we have a fontutil.pc file which
# defines the 'fontdir' variable.
BuildRequires: font-utils >= 1.1.1
# FIXME: fontconfig is needed only because the upstream Makefiles invoke
# fc-cache at build time.  This is totally useless, because we do not ship
# any of the resulting cache files, we generate them at install time from the
# rpm scripts.  However, it is easier to depend on fontconfig here than it is
# to patch all 40+ tarballs to stop running fc-cache.
BuildRequires: fontconfig

BuildRequires: ucs2any, bdftruncate, bdftopcf
# FIXME: perl is used for now to avoid having to maintain a big patch.
BuildRequires: perl

BuildRequires: xorg-x11-font-utils >= 1:7.5-2m

Conflicts: xorg-x11-server-Xorg < 1.3.0.0-10
Conflicts: xorg-x11-server-Xnest < 1.3.0.0-10
Conflicts: xorg-x11-server-Xdmx < 1.3.0.0-10
Conflicts: xorg-x11-server-Xvfb < 1.3.0.0-10
Conflicts: xorg-x11-server-Xephyr < 1.3.0.0-10
Conflicts: xorg-x11-xfs < 1.0.4-1

%description
X.Org X Window System fonts

%package misc
Summary: misc bitmap fonts for the X Window System
Group: User Interface/X
Requires(post): mkfontdir, fontconfig
Requires(postun): mkfontdir, fontconfig
Obsoletes: XFree86-base-fonts
Obsoletes: xorg-x11-base-fonts
Obsoletes: fonts-xorg-base
Obsoletes: xorg-x11-fonts-base
Provides: xorg-x11-fonts-base
Obsoletes: xorg-x11-fonts-syriac
Obsoletes: xorg-x11-fonts-truetype

%description misc
This package contains misc bitmap Chinese, Japanese, Korean, Indic, and Arabic
fonts for use with X Window System.
#--------------------------------------------------------------------------
%package Type1
Summary: Type1 fonts provided by the X Window System
Group: User Interface/X
Requires(post): mkfontdir, fontconfig, ttmkfdir
Requires(postun): mkfontdir, fontconfig, ttmkfdir
Obsoletes: XFree86-base-fonts
Obsoletes: xorg-x11-base-fonts
Obsoletes: fonts-xorg-base

%description Type1
A collection of Type1 fonts which are part of the core X Window System
distribution.
#--------------------------------------------------------------------------
%if %{with_ethiopic_fonts}
%package ethiopic
Summary: Ethiopic fonts
Group: User Interface/X
Requires(post): mkfontdir, ttmkfdir, mkfontscale, fontconfig
Requires(postun): mkfontdir, ttmkfdir, mkfontscale, fontconfig

%description ethiopic
Ethiopic fonts which are part of the core X Window System distribution.
%endif
#--------------------------------------------------------------------------
%package 75dpi
Summary: A set of 75dpi resolution fonts for the X Window System.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-75dpi-fonts
Obsoletes: xorg-x11-75dpi-fonts
Obsoletes: fonts-xorg-75dpi

%description 75dpi
A set of 75 dpi fonts used by the X window system.
#--------------------------------------------------------------------------
%package 100dpi
Summary: A set of 100dpi resolution fonts for the X Window System.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-100dpi-fonts
Obsoletes: xorg-x11-100dpi-fonts
Obsoletes: fonts-xorg-100dpi

%description 100dpi
A set of 100 dpi fonts used by the X window system.
#--------------------------------------------------------------------------
%package ISO8859-1-75dpi
Summary: A set of 75dpi ISO-8859-1 fonts for X.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-75dpi-fonts
Obsoletes: xorg-x11-75dpi-fonts
# The ISO8859-1 fonts used to be coupled with the UCS fonts.
Obsoletes: fonts-xorg-75dpi
Conflicts: fonts-xorg-75dpi

%description ISO8859-1-75dpi
Contains a set of 75dpi fonts for ISO-8859-1.
#--------------------------------------------------------------------------
%package ISO8859-1-100dpi
Summary: A set of 100dpi ISO-8859-1 fonts for X.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-100dpi-fonts
Obsoletes: xorg-x11-100dpi-fonts
# The ISO8859-1 fonts used to be coupled with the UCS fonts.
Obsoletes: fonts-xorg-100dpi
Conflicts: fonts-xorg-100dpi

%description ISO8859-1-100dpi
Contains a set of 100dpi fonts for ISO-8859-1.
#--------------------------------------------------------------------------
%package ISO8859-2-75dpi
Summary: A set of 75dpi Central European language fonts for X.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-2-75dpi-fonts
Obsoletes: xorg-x11-ISO8859-2-75dpi-fonts
Obsoletes: fonts-xorg-ISO8859-2-75dpi

%description ISO8859-2-75dpi
Contains a set of 75dpi fonts for Central European languages.
#--------------------------------------------------------------------------
%package ISO8859-2-100dpi
Summary: A set of 100dpi Central European language fonts for X.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-2-100dpi-fonts
Obsoletes: xorg-x11-ISO8859-2-100dpi-fonts
Obsoletes: fonts-xorg-ISO8859-2-100dpi

%description ISO8859-2-100dpi
Contains a set of 100dpi fonts for Central European languages.
#--------------------------------------------------------------------------
%package ISO8859-9-75dpi
Summary: ISO8859-9-75dpi fonts
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-9-75dpi-fonts
Obsoletes: xorg-x11-ISO8859-9-75dpi-fonts
Obsoletes: fonts-xorg-ISO8859-9-75dpi

%description ISO8859-9-75dpi
Contains a set of 75dpi fonts for the Turkish language.
#--------------------------------------------------------------------------
%package ISO8859-9-100dpi
Summary: ISO8859-9-100dpi fonts
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-9-100dpi-fonts
Obsoletes: xorg-x11-ISO8859-9-100dpi-fonts
Obsoletes: fonts-xorg-ISO8859-9-100dpi

%description ISO8859-9-100dpi
Contains a set of 100dpi fonts for the Turkish language.
#--------------------------------------------------------------------------
%package ISO8859-14-75dpi
Summary: ISO8859-14-75dpi fonts
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-14-75dpi-fonts
Obsoletes: xorg-x11-ISO8859-14-75dpi-fonts
Obsoletes: fonts-xorg-ISO8859-14-75dpi

%description ISO8859-14-75dpi
Contains a set of 75dpi fonts in the ISO8859-14 encoding which
provide Welsh support.
#--------------------------------------------------------------------------
%package ISO8859-14-100dpi
Summary: ISO8859-14-100dpi fonts
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-14-100dpi-fonts
Obsoletes: xorg-x11-ISO8859-14-100dpi-fonts
Obsoletes: fonts-xorg-ISO8859-14-100dpi

%description ISO8859-14-100dpi
Contains a set of 100dpi fonts in the ISO8859-14 encoding which
provide Welsh support.
#--------------------------------------------------------------------------
%package ISO8859-15-75dpi
Summary: ISO8859-15-75dpi fonts
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-15-75dpi-fonts
Obsoletes: xorg-x11-ISO8859-15-75dpi-fonts
Obsoletes: fonts-xorg-ISO8859-15-75dpi

%description ISO8859-15-75dpi
Contains a set of 75dpi fonts in the ISO8859-15 encoding which
provide Euro support.
#--------------------------------------------------------------------------
%package ISO8859-15-100dpi
Summary: ISO8859-15-100dpi fonts
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-ISO8859-15-100dpi-fonts
Obsoletes: xorg-x11-ISO8859-15-100dpi-fonts
Obsoletes: fonts-xorg-ISO8859-15-100dpi

%description ISO8859-15-100dpi
Contains a set of 100dpi fonts in the ISO8859-15 encoding which
provide Euro support.
#--------------------------------------------------------------------------
%package cyrillic
Summary: Cyrillic fonts for X.
Group: User Interface/X
Requires(post): mkfontdir
Requires(postun): mkfontdir
Obsoletes: XFree86-cyrillic-fonts
Obsoletes: xorg-x11-cyrillic-fonts
Obsoletes: fonts-xorg-cyrillic

%description cyrillic
Contains a set of Cyrillic fonts.
#--------------------------------------------------------------------------
%prep
%define setup_fonts_misc	-a15 -a27 -a28 -a29 -a31 -a32 -a33 -a37 -a38 -a39 -a41 -a42
%define setup_fonts_100dpi	-a10 -a12 -a16 -a18 -a22
%define setup_fonts_75dpi	-a11 -a13 -a17 -a19 -a23
%define setup_fonts_cyrillic	-a26 -a34 -a40 -a43
%define setup_fonts_type1	-a14 -a25 -a44
%define setup_fonts_otf		%{ethiopic_fonts}

%define setup_fonts_bitmap	%{setup_fonts_misc} %{setup_fonts_100dpi} %{setup_fonts_75dpi} %{setup_fonts_cyrillic}
%define setup_fonts_scaleable	%{setup_fonts_type1} %{setup_fonts_otf}

%define setup_font_metadata	-a1

%setup -q -c %{name}-%{version} %{setup_font_metadata} %{setup_fonts_bitmap} %{setup_fonts_scaleable}

#--------------------------------------------------------------------------
%build
pushd encodings-*
%configure
make
popd

for dir in font-*; do
    pushd $dir
    # FIXME: Yes, this perl hack is fairly ugly, but beats the heck out of
    # making a patch that patches 35 or so configure.ac files and maintaining
    # it for an indefinite amount of time.  Hopefully my solution here will
    # get considered to be included in upstream 7.1 release in which case I'll
    # turn it into a series of diffs instead and submit it.  For now tho, perl
    # is my friend.  -- mharris
#    perl -p -i -e 's#(^DEFAULT(_|_OTF|_TTF)FONTDIR=)\${libdir}/X11/fonts#\1\$(pkg-config --variable=fontdir fontutil)#' configure.ac
#    autoreconf -vfi
    %configure \
	--disable-iso8859-3 --disable-iso8859-4 --disable-iso8859-6 \
	--disable-iso8859-10 --disable-iso8859-11 --disable-iso8859-12 \
	--disable-iso8859-13 --disable-iso8859-16
    make
    popd
done

#--------------------------------------------------------------------------
%install
rm -rf $RPM_BUILD_ROOT

for dir in *; do
    # FIXME: The upstream sources need to be patched to allow direct
    # specification of the 'fontdir' variable, instead of it being
    # relative to libdir.
    make -C $dir install DESTDIR=$RPM_BUILD_ROOT
done

# Install catalogue symlinks
mkdir -p $RPM_BUILD_ROOT%{_catalogue}
for f in misc:unscaled:pri=10 75dpi:unscaled:pri=20 100dpi:unscaled:pri=30 Type1 TTF OTF cyrillic; do
    ln -fs %{_x11fontdir}/${f%%%%:*} $RPM_BUILD_ROOT%{_catalogue}/xorg-x11-fonts-$f
done

# Generate the encodings.dir files in the encodings directories during
# install time to work around bugs in upstream Makefiles.  This is more
# consistent with how we generate fonts.dir files anyway.  Fixes bugs:
# https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=173875
# https://bugs.freedesktop.org/show_bug.cgi?id=6028
{
    ENCODINGSDIR=$RPM_BUILD_ROOT%{_x11fontdir}/encodings
    pushd "${ENCODINGSDIR}" &> /dev/null
    mkfontscale -n -e "${ENCODINGSDIR}" -e "${ENCODINGSDIR}/large"
    sed -i "s@$RPM_BUILD_ROOT@@" encodings.dir
    sed -i "s@$RPM_BUILD_ROOT@@" large/encodings.dir
    popd &> /dev/null
}


# Create fake %ghost files for file manifests.
{
    # Make ghost fonts.alias, fonts.dir, encodings.dir files
    FONTDIR=$RPM_BUILD_ROOT%{_x11fontdir}
    # Create fake %ghost fonts.alias
    for subdir in TTF OTF ; do
        touch $FONTDIR/$subdir/fonts.{alias,scale}
        chmod 0644 $FONTDIR/$subdir/fonts.{alias,scale}
    done
    # Create fake %ghost encodings.dir, fonts.dir, fonts.cache-*
    for subdir in Type1 TTF OTF 100dpi 75dpi cyrillic misc ; do
        rm -f $FONTDIR/$subdir/{encodings,fonts}.dir
        touch $FONTDIR/$subdir/{encodings,fonts}.dir
        chmod 0644 $FONTDIR/$subdir/{encodings,fonts}.dir

        # Create bogus fonts.cache-* files
        # Create somewhat future-proofed ghosted fonts.cache-* files so that
        # the font packages own these files.
        for fcver in $(seq 1 9) ; do
            touch $FONTDIR/$subdir/fonts.cache-$fcver
            chmod 0644 $FONTDIR/$subdir/fonts.cache-$fcver
        done
    done
}

#--------------------------------------------------------------------------
# FIXME: We should write a script, or couple of scripts for generic font
# installation - with options if necessary, to reduce the amount of code
# duplication in these font installation scripts, then add the generic
# font installation script to xorg-x11-font-utils or whatever.

%post misc
{
  FONTDIR=%{_x11fontdir}/misc
  mkfontdir $FONTDIR 
  fc-cache $FONTDIR
}

%postun misc
{
  # Rebuild fonts.dir when uninstalling package. (exclude the local, CID dirs)
  if [ "$1" = "0" -a -d %{_x11fontdir}/misc ]; then
    mkfontdir %{_x11fontdir}/misc
    # Only run fc-cache in the Type1 dir, gzipped pcf's take forever
    fc-cache %{_x11fontdir}/misc
  fi
}

%post Type1
{
  FONTDIR=%{_type1_fontdir}/Type1
  mkfontscale $FONTDIR
  mkfontdir $FONTDIR
  fc-cache $FONTDIR
} 

%postun Type1
{
  FONTDIR=%{_type1_fontdir}/Type1
  if [ "$1" = "0" -a -d $FONTDIR ]; then
    mkfontscale $FONTDIR
    mkfontdir $FONTDIR
    fc-cache $FONTDIR
  fi
}

%if %{with_ethiopic_fonts}
%post ethiopic
{
  FONTDIR=%{_ttf_fontdir}/TTF
  ttmkfdir -d $FONTDIR -o $FONTDIR/fonts.scale
  mkfontdir $FONTDIR
  fc-cache $FONTDIR

  FONTDIR=%{_otf_fontdir}/OTF
  mkfontscale $FONTDIR
  mkfontdir $FONTDIR
  fc-cache $FONTDIR
}

%postun ethiopic
{
  FONTDIR=%{_ttf_fontdir}/TTF
  if [ "$1" = "0" -a -d $FONTDIR ]; then
    ttmkfdir -d $FONTDIR -o $FONTDIR/fonts.scale
    mkfontdir $FONTDIR
    fc-cache $FONTDIR
  fi
  FONTDIR=%{_otf_fontdir}/OTF
  if [ "$1" = "0" -a -d $FONTDIR ]; then
    mkfontscale $FONTDIR
    mkfontdir $FONTDIR
    fc-cache $FONTDIR
  fi
}
%endif

%post 75dpi
mkfontdir %{_x11fontdir}/75dpi

%post 100dpi
mkfontdir %{_x11fontdir}/100dpi

%post ISO8859-1-75dpi
mkfontdir %{_x11fontdir}/75dpi

%post ISO8859-1-100dpi
mkfontdir %{_x11fontdir}/100dpi

%post ISO8859-2-75dpi
mkfontdir %{_x11fontdir}/75dpi

%post ISO8859-2-100dpi
mkfontdir %{_x11fontdir}/100dpi

%post ISO8859-9-75dpi
mkfontdir %{_x11fontdir}/75dpi

%post ISO8859-9-100dpi
mkfontdir %{_x11fontdir}/100dpi

%post ISO8859-14-75dpi
mkfontdir %{_x11fontdir}/75dpi

%post ISO8859-14-100dpi
mkfontdir %{_x11fontdir}/100dpi

%post ISO8859-15-75dpi
mkfontdir %{_x11fontdir}/75dpi

%post ISO8859-15-100dpi
mkfontdir %{_x11fontdir}/100dpi

%post cyrillic
mkfontdir %{_x11fontdir}/cyrillic

%postun 75dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/75dpi ]; then
    mkfontdir %{_x11fontdir}/75dpi
  fi
}

%postun 100dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/100dpi ]; then
    mkfontdir %{_x11fontdir}/100dpi
  fi
}

%postun ISO8859-1-75dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/75dpi ]; then
    mkfontdir %{_x11fontdir}/75dpi
  fi
}

%postun ISO8859-1-100dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/100dpi ]; then
    mkfontdir %{_x11fontdir}/100dpi
  fi
}

%postun ISO8859-2-75dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/75dpi ]; then
    mkfontdir %{_x11fontdir}/75dpi
  fi
}

%postun ISO8859-2-100dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/100dpi  ]; then
    mkfontdir %{_x11fontdir}/100dpi
  fi
}

%postun ISO8859-9-75dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/75dpi ]; then
    mkfontdir %{_x11fontdir}/75dpi
  fi
}

%postun ISO8859-9-100dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/100dpi  ]; then
    mkfontdir %{_x11fontdir}/100dpi
  fi
}

%postun ISO8859-14-75dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/75dpi ]; then
    mkfontdir %{_x11fontdir}/75dpi
  fi
}

%postun ISO8859-14-100dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/100dpi  ]; then
    mkfontdir %{_x11fontdir}/100dpi
  fi
}

%postun ISO8859-15-75dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/75dpi ]; then
    mkfontdir %{_x11fontdir}/75dpi
  fi
}

%postun ISO8859-15-100dpi
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/100dpi  ]; then
    mkfontdir %{_x11fontdir}/100dpi
  fi
}

%postun cyrillic
{
  if [ "$1" = "0" -a -d %{_x11fontdir}/cyrillic ]; then
    mkfontdir %{_x11fontdir}/cyrillic
  fi
}

#--------------------------------------------------------------------------
%check

#--------------------------------------------------------------------------
%clean
rm -rf $RPM_BUILD_ROOT
#--------------------------------------------------------------------------
%files misc
%defattr(-,root,root,-)
%doc
%{_catalogue}/xorg-x11-fonts-100dpi:unscaled:pri=30
%{_catalogue}/xorg-x11-fonts-75dpi:unscaled:pri=20
%{_catalogue}/xorg-x11-fonts-misc:unscaled:pri=10
%dir %{_x11fontdir}
%dir %{_x11fontdir}/100dpi
%dir %{_x11fontdir}/75dpi
%dir %{_x11fontdir}/misc
%{_x11fontdir}/misc/*
%dir %{_x11fontdir}/encodings
%dir %{_x11fontdir}/encodings/large
%{_x11fontdir}/encodings/*.enc.gz
%{_x11fontdir}/encodings/large/*.enc.gz
%ghost %verify(not md5 size mtime) %{_x11fontdir}/encodings/encodings.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/encodings/large/encodings.dir

%if %{with_ethiopic_fonts}
%files ethiopic
%defattr(-,root,root,-)
%doc
%{_catalogue}/xorg-x11-fonts-TTF
%{_catalogue}/xorg-x11-fonts-OTF
# TTF fonts
%dir %{_x11fontdir}/TTF
# font-misc-ethiopic
%{_x11fontdir}/TTF/GohaTibebZemen.ttf
%ghost %verify(not md5 size mtime) %{_x11fontdir}/TTF/encodings.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/TTF/fonts.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/TTF/fonts.alias
%ghost %verify(not md5 size mtime) %{_x11fontdir}/TTF/fonts.scale
%ghost %verify(not md5 size mtime) %{_x11fontdir}/TTF/fonts.cache-*
# OTF fonts
%dir %{_x11fontdir}/OTF
%{_x11fontdir}/OTF/GohaTibebZemen.otf
%ghost %verify(not md5 size mtime) %{_x11fontdir}/OTF/encodings.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/OTF/fonts.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/OTF/fonts.alias
%ghost %verify(not md5 size mtime) %{_x11fontdir}/OTF/fonts.scale
%ghost %verify(not md5 size mtime) %{_x11fontdir}/OTF/fonts.cache-*
%endif

%files 75dpi
%defattr(-,root,root,-)
%doc
# font-adobe-75dpi
%{_x11fontdir}/75dpi/cour[BOR]??.pcf*
%{_x11fontdir}/75dpi/courBO??.pcf*
%{_x11fontdir}/75dpi/helv[BOR]??.pcf*
%{_x11fontdir}/75dpi/helvBO??.pcf*
%{_x11fontdir}/75dpi/ncen[BIR]??.pcf*
%{_x11fontdir}/75dpi/ncenBI??.pcf*
%{_x11fontdir}/75dpi/tim[BIR]??.pcf*
%{_x11fontdir}/75dpi/timBI??.pcf*
%{_x11fontdir}/75dpi/symb??.pcf*
# font-adobe-utopia-75dpi
%{_x11fontdir}/75dpi/UTBI__??.pcf*
%{_x11fontdir}/75dpi/UT[BI]___??.pcf*
%{_x11fontdir}/75dpi/UTRG__??.pcf*
# font-bh-75dpi
%{_x11fontdir}/75dpi/luBIS??.pcf*
%{_x11fontdir}/75dpi/lu[BIR]S??.pcf*
%{_x11fontdir}/75dpi/lub[BIR]??.pcf*
%{_x11fontdir}/75dpi/lubBI??.pcf*
# font-bh-lucidatypewriter-75dpi
%{_x11fontdir}/75dpi/lut[BR]S??.pcf*
# font-bitstream-75dpi
%{_x11fontdir}/75dpi/char[BIR]??.pcf*
%{_x11fontdir}/75dpi/charBI??.pcf*
%{_x11fontdir}/75dpi/tech14.pcf*
%{_x11fontdir}/75dpi/techB14.pcf*
%{_x11fontdir}/75dpi/term14.pcf*
%{_x11fontdir}/75dpi/termB14.pcf*
%ghost %verify(not md5 size mtime) %{_x11fontdir}/75dpi/encodings.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/75dpi/fonts.dir
# NOTE: Xorg supplies this fonts.alias, so it is not ghosted
%verify(not md5 size mtime) %{_x11fontdir}/75dpi/fonts.alias
%ghost %verify(not md5 size mtime) %{_x11fontdir}/75dpi/fonts.cache-*

%files 100dpi
%defattr(-,root,root,-)
%doc
# font-adobe-100dpi
%{_x11fontdir}/100dpi/cour[BOR]??.pcf*
%{_x11fontdir}/100dpi/courBO??.pcf*
%{_x11fontdir}/100dpi/helv[BOR]??.pcf*
%{_x11fontdir}/100dpi/helvBO??.pcf*
%{_x11fontdir}/100dpi/ncen[BIR]??.pcf*
%{_x11fontdir}/100dpi/ncenBI??.pcf*
%{_x11fontdir}/100dpi/tim[BIR]??.pcf*
%{_x11fontdir}/100dpi/timBI??.pcf*
%{_x11fontdir}/100dpi/symb??.pcf*
# font-adobe-utopia-100dpi
%{_x11fontdir}/100dpi/UTBI__??.pcf*
%{_x11fontdir}/100dpi/UT[BI]___??.pcf*
%{_x11fontdir}/100dpi/UTRG__??.pcf*
# font-bh-100dpi
%{_x11fontdir}/100dpi/luBIS??.pcf*
%{_x11fontdir}/100dpi/lu[BIR]S??.pcf*
%{_x11fontdir}/100dpi/lub[BIR]??.pcf*
%{_x11fontdir}/100dpi/lubBI??.pcf*
# font-bh-lucidatypewriter-100dpi
%{_x11fontdir}/100dpi/lut[BR]S??.pcf*
# font-bitstream-100dpi
%{_x11fontdir}/100dpi/char[BIR]??.pcf*
%{_x11fontdir}/100dpi/charBI??.pcf*
%{_x11fontdir}/100dpi/tech14.pcf*
%{_x11fontdir}/100dpi/techB14.pcf*
%{_x11fontdir}/100dpi/term14.pcf*
%{_x11fontdir}/100dpi/termB14.pcf*
%ghost %verify(not md5 size mtime) %{_x11fontdir}/100dpi/encodings.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/100dpi/fonts.dir
# NOTE: Xorg supplies this fonts.alias, so it is not ghosted
%verify(not md5 size mtime) %{_x11fontdir}/100dpi/fonts.alias
%ghost %verify(not md5 size mtime) %{_x11fontdir}/100dpi/fonts.cache-*

%files ISO8859-1-75dpi
%defattr(-,root,root,-)
%doc
# font-adobe-75dpi
%{_x11fontdir}/75dpi/cour[BOR]??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/courBO??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/helv[BOR]??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/helvBO??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/ncen[BIR]??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/ncenBI??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/tim[BIR]??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/timBI??-ISO8859-1.pcf*
# font-adobe-utopia-75dpi
%{_x11fontdir}/75dpi/UTBI__??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/UT[BI]___??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/UTRG__??-ISO8859-1.pcf*
# font-bh-75dpi
%{_x11fontdir}/75dpi/luBIS??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/lu[BIR]S??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/lub[BIR]??-ISO8859-1.pcf*
%{_x11fontdir}/75dpi/lubBI??-ISO8859-1.pcf*
# font-bh-lucidatypewriter-75dpi
%{_x11fontdir}/75dpi/lut[BR]S??-ISO8859-1.pcf*

%files ISO8859-1-100dpi
%defattr(-,root,root,-)
%doc
# font-adobe-100dpi
%{_x11fontdir}/100dpi/cour[BOR]??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/courBO??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/helv[BOR]??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/helvBO??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/ncen[BIR]??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/ncenBI??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/tim[BIR]??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/timBI??-ISO8859-1.pcf*
# font-adobe-utopia-100dpi
%{_x11fontdir}/100dpi/UTBI__??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/UT[BI]___??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/UTRG__??-ISO8859-1.pcf*
# font-bh-100dpi
%{_x11fontdir}/100dpi/luBIS??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/lu[BIR]S??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/lub[BIR]??-ISO8859-1.pcf*
%{_x11fontdir}/100dpi/lubBI??-ISO8859-1.pcf*
# font-bh-lucidatypewriter-100dpi
%{_x11fontdir}/100dpi/lut[BR]S??-ISO8859-1.pcf*

%files ISO8859-2-75dpi
%defattr(-,root,root,-)
%doc
# font-adobe-75dpi
%{_x11fontdir}/75dpi/cour[BOR]??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/courBO??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/helv[BOR]??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/helvBO??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/ncen[BIR]??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/ncenBI??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/tim[BIR]??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/timBI??-ISO8859-2.pcf*
# font-adobe-utopia-75dpi
%{_x11fontdir}/75dpi/UTBI__??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/UT[BI]___??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/UTRG__??-ISO8859-2.pcf*
# font-bh-75dpi
%{_x11fontdir}/75dpi/luBIS??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/lu[BIR]S??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/lub[BIR]??-ISO8859-2.pcf*
%{_x11fontdir}/75dpi/lubBI??-ISO8859-2.pcf*
# font-bh-lucidatypewriter-75dpi
%{_x11fontdir}/75dpi/lut[BR]S??-ISO8859-2.pcf*

%files ISO8859-2-100dpi
%defattr(-,root,root,-)
%doc
# font-adobe-100dpi
%{_x11fontdir}/100dpi/cour[BOR]??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/courBO??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/helv[BOR]??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/helvBO??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/ncen[BIR]??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/ncenBI??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/tim[BIR]??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/timBI??-ISO8859-2.pcf*
# font-adobe-utopia-100dpi
%{_x11fontdir}/100dpi/UTBI__??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/UT[BI]___??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/UTRG__??-ISO8859-2.pcf*
# font-bh-100dpi
%{_x11fontdir}/100dpi/luBIS??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/lu[BIR]S??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/lub[BIR]??-ISO8859-2.pcf*
%{_x11fontdir}/100dpi/lubBI??-ISO8859-2.pcf*
# font-bh-lucidatypewriter-100dpi
%{_x11fontdir}/100dpi/lut[BR]S??-ISO8859-2.pcf*

%files ISO8859-9-75dpi
%defattr(-,root,root,-)
%doc
# font-adobe-75dpi
%{_x11fontdir}/75dpi/cour[BOR]??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/courBO??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/helv[BOR]??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/helvBO??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/ncen[BIR]??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/ncenBI??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/tim[BIR]??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/timBI??-ISO8859-9.pcf*
# font-adobe-utopia-75dpi
%{_x11fontdir}/75dpi/UTBI__??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/UT[BI]___??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/UTRG__??-ISO8859-9.pcf*
# font-bh-75dpi
%{_x11fontdir}/75dpi/luBIS??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/lu[BIR]S??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/lub[BIR]??-ISO8859-9.pcf*
%{_x11fontdir}/75dpi/lubBI??-ISO8859-9.pcf*
# font-bh-lucidatypewriter-75dpi
%{_x11fontdir}/75dpi/lut[BR]S??-ISO8859-9.pcf*

%files ISO8859-9-100dpi
%defattr(-,root,root,-)
%doc
# font-adobe-100dpi
%{_x11fontdir}/100dpi/cour[BOR]??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/courBO??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/helv[BOR]??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/helvBO??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/ncen[BIR]??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/ncenBI??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/tim[BIR]??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/timBI??-ISO8859-9.pcf*
# font-adobe-utopia-100dpi
%{_x11fontdir}/100dpi/UTBI__??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/UT[BI]___??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/UTRG__??-ISO8859-9.pcf*
# font-bh-100dpi
%{_x11fontdir}/100dpi/luBIS??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/lu[BIR]S??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/lub[BIR]??-ISO8859-9.pcf*
%{_x11fontdir}/100dpi/lubBI??-ISO8859-9.pcf*
# font-bh-lucidatypewriter-100dpi
%{_x11fontdir}/100dpi/lut[BR]S??-ISO8859-9.pcf*

%files ISO8859-14-75dpi
%defattr(-,root,root,-)
%doc
# font-adobe-75dpi
%{_x11fontdir}/75dpi/cour[BOR]??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/courBO??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/helv[BOR]??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/helvBO??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/ncen[BIR]??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/ncenBI??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/tim[BIR]??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/timBI??-ISO8859-14.pcf*
# font-adobe-utopia-75dpi
%{_x11fontdir}/75dpi/UTBI__??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/UT[BI]___??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/UTRG__??-ISO8859-14.pcf*
# font-bh-75dpi
%{_x11fontdir}/75dpi/luBIS??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/lu[BIR]S??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/lub[BIR]??-ISO8859-14.pcf*
%{_x11fontdir}/75dpi/lubBI??-ISO8859-14.pcf*
# font-bh-lucidatypewriter-75dpi
%{_x11fontdir}/75dpi/lut[BR]S??-ISO8859-14.pcf*

%files ISO8859-14-100dpi
%defattr(-,root,root,-)
%doc
# font-adobe-100dpi
%{_x11fontdir}/100dpi/cour[BOR]??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/courBO??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/helv[BOR]??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/helvBO??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/ncen[BIR]??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/ncenBI??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/tim[BIR]??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/timBI??-ISO8859-14.pcf*
# font-adobe-utopia-100dpi
%{_x11fontdir}/100dpi/UTBI__??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/UT[BI]___??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/UTRG__??-ISO8859-14.pcf*
# font-bh-100dpi
%{_x11fontdir}/100dpi/luBIS??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/lu[BIR]S??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/lub[BIR]??-ISO8859-14.pcf*
%{_x11fontdir}/100dpi/lubBI??-ISO8859-14.pcf*
# font-bh-lucidatypewriter-100dpi
%{_x11fontdir}/100dpi/lut[BR]S??-ISO8859-14.pcf*

%files ISO8859-15-75dpi
%defattr(-,root,root,-)
%doc
# font-adobe-75dpi
%{_x11fontdir}/75dpi/cour[BOR]??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/courBO??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/helv[BOR]??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/helvBO??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/ncen[BIR]??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/ncenBI??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/tim[BIR]??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/timBI??-ISO8859-15.pcf*
# font-adobe-utopia-75dpi
%{_x11fontdir}/75dpi/UTBI__??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/UT[BI]___??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/UTRG__??-ISO8859-15.pcf*
# font-bh-75dpi
%{_x11fontdir}/75dpi/luBIS??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/lu[BIR]S??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/lub[BIR]??-ISO8859-15.pcf*
%{_x11fontdir}/75dpi/lubBI??-ISO8859-15.pcf*
# font-bh-lucidatypewriter-75dpi
%{_x11fontdir}/75dpi/lut[BR]S??-ISO8859-15.pcf*

%files ISO8859-15-100dpi
%defattr(-,root,root,-)
%doc
# font-adobe-100dpi
%{_x11fontdir}/100dpi/cour[BOR]??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/courBO??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/helv[BOR]??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/helvBO??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/ncen[BIR]??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/ncenBI??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/tim[BIR]??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/timBI??-ISO8859-15.pcf*
# font-adobe-utopia-100dpi
%{_x11fontdir}/100dpi/UTBI__??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/UT[BI]___??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/UTRG__??-ISO8859-15.pcf*
# font-bh-100dpi
%{_x11fontdir}/100dpi/luBIS??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/lu[BIR]S??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/lub[BIR]??-ISO8859-15.pcf*
%{_x11fontdir}/100dpi/lubBI??-ISO8859-15.pcf*
# font-bh-lucidatypewriter-100dpi
%{_x11fontdir}/100dpi/lut[BR]S??-ISO8859-15.pcf*

%files Type1
%defattr(-,root,root,-)
%doc
%{_catalogue}/xorg-x11-fonts-Type1
%dir %{_x11fontdir}/Type1
# font-adobe-utopia-type1
%{_x11fontdir}/Type1/UT??____.[ap]f[ma]
# font-bitstream-type1
%{_x11fontdir}/Type1/c0???bt_.[ap]f[mb]
# font-xfree86-type1
%{_x11fontdir}/Type1/cursor.pfa
%ghost %verify(not md5 size mtime) %{_x11fontdir}/Type1/encodings.dir
%%ghost %verify(not md5 size mtime) %{_x11fontdir}/Type1/fonts.dir
%%ghost %verify(not md5 size mtime) %{_x11fontdir}/Type1/fonts.scale
%ghost %verify(not md5 size mtime) %{_x11fontdir}/Type1/fonts.cache-*


%files cyrillic
%defattr(-,root,root,-)
%doc
%{_catalogue}/xorg-x11-fonts-cyrillic
%dir %{_x11fontdir}/cyrillic
# font-cronyx-cyrillic
%{_x11fontdir}/cyrillic/crox[1-6]*.pcf*
%{_x11fontdir}/cyrillic/koi10x16b.pcf*
%{_x11fontdir}/cyrillic/koi10x20.pcf*
%{_x11fontdir}/cyrillic/koi6x10.pcf*
%{_x11fontdir}/cyrillic/koinil2.pcf*
# font-misc-cyrillic
%{_x11fontdir}/cyrillic/koi12x24*.pcf*
%{_x11fontdir}/cyrillic/koi6x13.pcf*
%{_x11fontdir}/cyrillic/koi6x13b.pcf*
%{_x11fontdir}/cyrillic/koi6x9.pcf*
%{_x11fontdir}/cyrillic/koi[5789]x*.pcf*
# font-screen-cyrillic
%{_x11fontdir}/cyrillic/screen8x16*.pcf*
# font-winitzki-cyrillic
%{_x11fontdir}/cyrillic/proof9x16.pcf*
%ghost %verify(not md5 size mtime) %{_x11fontdir}/cyrillic/encodings.dir
%ghost %verify(not md5 size mtime) %{_x11fontdir}/cyrillic/fonts.dir
# NOTE: Xorg supplies this fonts.alias, so it is not ghosted
%verify(not md5 size mtime) %{_x11fontdir}/cyrillic/fonts.alias
%ghost %verify(not md5 size mtime) %{_x11fontdir}/cyrillic/fonts.cache-*

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-1m)
- update 7.7 (version only)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-11m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-10m)
- update many fonts for 7.6 (final)

* Mon Nov 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-9m)
- update font-adobe-*

* Thu Nov 11 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-8m)
- update font-bh-ttf-1.0.3

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-7m)
- update many fonts

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.5-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.5-4m)
- BuildRequires: xorg-x11-font-utils >= 1:7.5-2m

* Sat Nov  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-3m)
- change fontsub dir from %%{_datadir}/fonts/X11 to %%{_datadir}/X11/fonts

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.5-2m)
- grip /usr/share/fonts/X11/Type1

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-1m)
- update to 7.5
-- delete patch0, patch1
-- change fontsub dir from %%{_datadir}/X11/fonts to 
---- %%{_datadir}/fonts/X11
-- encodings-1.0.3
-- font-alias-1.0.2
-- font-adobe-100dpi-1.0.1
-- font-adobe-75dpi-1.0.1
-- font-adobe-utopia-100dpi-1.0.2
-- font-adobe-utopia-75dpi-1.0.2
-- font-adobe-utopia-type1-1.0.2
-- font-arabic-misc-1.0.1
-- font-bh-100dpi-1.0.1
-- font-bh-75dpi-1.0.1
-- font-bh-lucidatypewriter-100dpi-1.0.1
-- font-bh-lucidatypewriter-75dpi-1.0.1
-- font-bh-ttf-1.0.1
-- font-bh-type1-1.0.1
-- font-bitstream-100dpi-1.0.1
-- font-bitstream-75dpi-1.0.1
-- font-bitstream-speedo-1.0.1
-- font-bitstream-type1-1.0.1
-- font-cronyx-cyrillic-1.0.1
-- font-cursor-misc-1.0.1
-- font-daewoo-misc-1.0.1
-- font-dec-misc-1.0.1
-- font-ibm-type1-1.0.1
-- font-isas-misc-1.0.1
-- font-jis-misc-1.0.1
-- font-micro-misc-1.0.1
-- font-misc-cyrillic-1.0.1
-- font-misc-ethiopic-1.0.1
-- font-misc-meltho-1.0.1
-- font-misc-misc-1.1.0
-- font-mutt-misc-1.0.1
-- font-schumacher-misc-1.1.0
-- font-screen-cyrillic-1.0.2
-- font-sony-misc-1.0.1
-- font-sun-misc-1.0.1
-- font-winitzki-cyrillic-1.0.1
-- font-xfree86-type1-1.0.2

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.2-6m)
- Obsoletes: xorg-x11-fonts-syriac and xorg-x11-fonts-truetype

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2-5m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.2-4m)
- use autoreconf (for autoconf-2.63)

* Sun Apr 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (7.2-3m)
- remove %%{_catalogue}

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.2-2m)
- package fonts-misc owns %%{_catalogue} and %%{_x11fontdir}
- package fonts-misc owns %%{_x11fontdir}/75dpi and %%{_x11fontdir}/100dpi
- package fonts-misc handles %%{_catalogue}/xorg-x11-fonts-100dpi:unscaled:pri=30
- package fonts-misc handles %%{_catalogue}/xorg-x11-fonts-75dpi:unscaled:pri=20
- xorg-x11-fonts-misc is required by xorg-x11-server-Xorg
- release fonts.dir and fonts-cache
- fonts.dir is created by mkfontdir
- fonts.cache is created by fc-cache

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (7.2-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-7m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-6m)
- font-xfree86-type1-1.0.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-5m)
- %%NoSource -> NoSource

* Tue Dec 12 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-4m)
- fix BuildRequires

* Thu Nov  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-3m)
- update
-- encodings-1.0.2
-- font-screen-cyrillic-1.0.1

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-2m)
- delete duplicate files
-- add dependency
-- misc -> base
-- *-100dpi -> 100dpi
-- *-75dpi -> 75dpi
-- ethiopic -> syriac truetype

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-1m)
- revise for xorg-X11-R7.1
- sync FC

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-2m)
- To trunk

* Thu Mar  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-1.1m)
- change BuildPrerq: xorg-x11-font-utils
- Comment out Obsolete, Conflict

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-1m)
- import to Momonga

* Thu Jan 26 2006 Mike A. Harris <mharris@redhat.com> 7.0-1
- Bumped artificial package version to 7.0, to indicate that the font tarballs
  are all from X11R7.0.
- Enabled the ethiopic font subpackage experimentally for bug (#176678)

* Tue Jan 17 2006 Mike A. Harris <mharris@redhat.com> 1.0.0-3
- Added missing post/postun scripts for ISO8859-1-75dpi and ISO8859-1-100dpi
  font packages. (#174416)

* Tue Jan 10 2006 Bill Nottingham <notting@redhat.com> 1.0.0-2
- fix obsoletes (#177377)

* Thu Dec 15 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all font packages to X11R7 RC4 versions.
- Added perl hack to build section to massage all of the configure.ac files
  to use pkgconfig to autodetect the top level X fontdir.
- Added font-alias-1.0.0-fonts-alias-fontdir-fix.patch to use pkgconfig to
  autodetect the top level X fontdir.
- Added encodings-1.0.0-encodings-fontdir-fix.patch to use pkgconfig to
  autodetect the top level X fontdir.
- Use new --disable-iso8859-* options instead of deleting unwanted encodings.
- Added dependency on font-utils 1.0.0

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated all font packages to X11R7 RC3 versions.

* Fri Nov 25 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-8
- Due to a bug in chkfontpath which will remove both 'misc' and 'misc:unscaled'
  from the fontpath, use sed magic to do it instead, based of xfs scripts.

* Wed Nov 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-7
- Undo the workaround implemented in build 0.99.0-5, and make the misc fonts
  directory ":unscaled" again.
- Invoke chkfontpath to remove the bare 'misc' font path without the :unscaled
  attribute from xfs config.

* Mon Nov 14 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-6
- Fixed mkfontscale invocation in Type1 font subpackage post/postun scripts
  by removing accidental -o argument that creeped in via cut and paste
  error. (#173059)

* Sun Nov 13 2005 Jeremy Katz <katzj@redhat.com> - 0.99.0-5
- don't use :unscaled for base fonts as a temporary workaround for #172997

* Wed Nov 9 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-4
- Added Obsoletes/Conflicts lines for fonts-xorg-* et al. to all subpackages,
  so that OS upgrades work properly.

* Tue Nov 8 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Split the old style 'base' fonts package up into 'base' and 'misc', where
  'base' now contains only the 'cursor' and 'fixed' fonts required by the
  X server, and 'misc' contains a variety of Asian, Arabic, Indic, and other
  fonts that can now be optionally installed or removed.
- Use globs in file manifests to reduce size of specfile.
- Add post/postun scripts for all font subpackages.
- Disable ethiopic fonts by default.

* Mon Nov 7 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Added "BuildArch: noarch" so that all fonts are noarch.

* Tue Oct 25 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial packaging.
