# Generated from mechanize-2.3.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname mechanize

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: The Mechanize library is used for automating interaction with websites
Name: rubygem-%{gemname}
Version: 2.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://mechanize.rubyforge.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby >= 1.8.7
Requires: rubygem(net-http-digest_auth) => 1.1
Requires: rubygem(net-http-digest_auth) < 2
Requires: rubygem(net-http-digest_auth) >= 1.1.1
Requires: rubygem(net-http-persistent) => 2.5
Requires: rubygem(net-http-persistent) < 3
Requires: rubygem(net-http-persistent) >= 2.5.2
Requires: rubygem(mime-types) => 1.17
Requires: rubygem(mime-types) < 2
Requires: rubygem(mime-types) >= 1.17.2
Requires: rubygem(nokogiri) => 1.4
Requires: rubygem(nokogiri) < 2
Requires: rubygem(ntlm-http) => 0.1
Requires: rubygem(ntlm-http) < 1
Requires: rubygem(ntlm-http) >= 0.1.1
Requires: rubygem(webrobots) 
Requires: rubygem(webrobots) < 1
Requires: rubygem(webrobots) >= 0.0.9
Requires: rubygem(domain_name) => 0.5
Requires: rubygem(domain_name) < 1
Requires: rubygem(domain_name) >= 0.5.1
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby >= 1.8.7
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
The Mechanize library is used for automating interaction with websites.
Mechanize automatically stores and sends cookies, follows redirects,
and can follow links and submit forms.  Form fields can be populated and
submitted.  Mechanize also keeps track of the sites that you have visited as
a history.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/CHANGELOG.rdoc
%doc %{geminstdir}/EXAMPLES.rdoc
%doc %{geminstdir}/GUIDE.rdoc
%doc %{geminstdir}/LICENSE.rdoc
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-1m)
- update 2.3

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1-1m)
- update 2.1

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-1m)
- update 2.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update 1.0.0

* Thu Dec  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.3-1m)
- Initial package for Momonga Linux
