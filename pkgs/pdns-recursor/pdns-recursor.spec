%global momorel 6

Summary:		Modern, advanced and high performance recursing/non authoritative nameserver
Name:			pdns-recursor
Version:		3.3
Release:		%{momorel}m%{?dist}
Group:			System Environment/Daemons
License:		GPLv2
URL:			http://powerdns.com/
Source0:		http://downloads.powerdns.com/releases/%{name}-%{version}.tar.bz2
NoSource:		0
Source1:		pdns-recursor.service

Patch0:			pdns-recursor-fixmakefile.patch
Patch1:			pdns-recursor-fixsysconfdir.patch

Provides:		powerdns-recursor = %{version}-%{release}
BuildRequires:		boost-devel, lua-devel
BuildRequires:		systemd-units
Requires(pre):          shadow-utils
Requires(post):		systemd-sysv
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units

%description
PowerDNS Recursor is a non authoritative/recursing DNS server. Use this
package if you need a dns cache for your network.

%prep
%setup -q
%patch0 -p1 -b .fixmakefile
%patch1 -p1 -b .fixsysconfdir

%build
LUA=1 LUA_CPPFLAGS_CONFIG= LUA_LIBS_CONFIG=-llua OPTFLAGS="%{optflags}" make %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot} CONFIGDIR="%{_sysconfdir}/%{name}"
%{__install} -D -p %{SOURCE1} %{buildroot}%{_unitdir}/pdns-recursor.service

%{__mv} %{buildroot}%{_sysconfdir}/%{name}/recursor.conf{-dist,}

# add pdns user and group to top of configfile
sed -i '1i\setuid=pdns-recursor' %{buildroot}%{_sysconfdir}/%{name}/recursor.conf
sed -i '2i\setgid=pdns-recursor' %{buildroot}%{_sysconfdir}/%{name}/recursor.conf

%pre
getent group pdns-recursor > /dev/null || groupadd -r pdns-recursor
getent passwd pdns-recursor > /dev/null || \
    useradd -r -g pdns-recursor -d / -s /sbin/nologin \
    -c "PwerDNS Recursor user" pdns-recursor
exit 0

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable pdns-recursor.service > /dev/null 2>&1 || :
    /bin/systemctl stop pdns-recursor.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart pdns-recursor.service >/dev/null 2>&1 || :
fi

%triggerun -- pdns-recursor < 3.3-3m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply pdns-recursor
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save pdns-recursor >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del pdns-recursor >/dev/null 2>&1 || :
/bin/systemctl try-restart pdns-recursor.service >/dev/null 2>&1 || :

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/rec_control
%{_sbindir}/pdns_recursor
%{_mandir}/man1/pdns_recursor.1.*
%{_mandir}/man1/rec_control.1.*
%{_unitdir}/pdns-recursor.service
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/recursor.conf
%doc COPYING README


%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3-6m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (3.3-5m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3-4m)
- rebuild for boost-1.48.0

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3-3m)
- support systemd

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3-2m)
- rebuild for boost

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-7m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-6m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-5m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-4m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-1m)
- sync with Fedora 13 (3.2-2)

* Thu Jan  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7.2-1m)
- [SECURITY] CVE-2009-4009 CVE-2009-4010
- update to 3.1.7.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-5m)
- apply gcc44 patch

* Tue Jan 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.7-4m)
- add patch to fix build failure

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-2m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.7-1m)
- update to 3.1.7

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.4-4m)
- rebuild against gcc43

* Wed Feb 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.4-3m)
- add gcc43 patch

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.4-2m)
- use initscriptdir in Momonga instead of initrddir
- add %%doc rrd

* Fri Oct  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.4-1m)
- import to Momonga from fedora

* Sun Jan 27 2007 <ruben@rubenkerkhof.com> 3.1.4-4
- Now really fix the description in init script
* Sat Jan 27 2007 <ruben@rubenkerkhof.com> 3.1.4-3
- Fixed Description in init script
* Wed Jan 24 2007 <ruben@rubenkerkhof.com> 3.1.4-2
- Fixes per bz review 221188:
- Changed user to pdns-recursor
- Patched the Makefile to not strip debugsymbols
- Skipped the configure step, it didn't do much
- Added a more Fedora-centric initscript
- Use condrestart instead of restart in %%postun
* Sun Dec 31 2006 <ruben@rubenkerkhof.com> 3.1.4-1
- Initial import

