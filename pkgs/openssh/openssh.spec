%global momorel 1

# Do we want SELinux & Audit
%define WITH_SELINUX 1

# OpenSSH privilege separation requires a user & group ID
%define sshd_uid    74
%define sshd_gid    74

# Do we want to disable building of gnome-askpass? (1=yes 0=no)
%define no_gnome_askpass 0

# Do we want to link against a static libcrypto? (1=yes 0=no)
%define static_libcrypto 0

# Do we want smartcard support (1=yes 0=no)
#Smartcard support is broken from 5.4p1
%define scard 0

# Use GTK2 instead of GNOME in gnome-ssh-askpass
%define gtk2 1

# Build position-independent executables (requires toolchain support)?
%define pie 1

# Do we want kerberos5 support (1=yes 0=no)
%define kerberos5 1

# Do we want libedit support
%define libedit 1

# Do we want LDAP support
%define ldap 1

# Whether or not /sbin/nologin exists.
%define nologin 1

# Whether to build pam_ssh_agent_auth
%define pam_ssh_agent 1

# Reserve options to override askpass settings with:
# rpm -ba|--rebuild --define 'skip_xxx 1'
%{?skip_gnome_askpass:%global no_gnome_askpass 1}

# Add option to build without GTK2 for older platforms with only GTK+.
# Red Hat Linux <= 7.2 and Red Hat Advanced Server 2.1 are examples.
# rpm -ba|--rebuild --define 'no_gtk2 1'
%{?no_gtk2:%global gtk2 0}

# Options for static OpenSSL link:
# rpm -ba|--rebuild --define "static_openssl 1"
%{?static_openssl:%global static_libcrypto 1}

# Options for Smartcard support: (needs libsectok and openssl-engine)
# rpm -ba|--rebuild --define "smartcard 1"
%{?smartcard:%global scard 1}

# Is this a build for the rescue CD (without PAM, with MD5)? (1=yes 0=no)
%define rescue 0
%{?build_rescue:%global rescue 1}
%{?build_rescue:%global rescue_rel rescue}

# Turn off some stuff for resuce builds
%if %{rescue}
%define kerberos5 0
%define libedit 0
%define pam_ssh_agent 0
%endif

# Do not forget to bump pam_ssh_agent_auth release if you rewind the main package release to 1
%define openssh_ver 6.4p1
%define pam_ssh_agent_ver 0.9.3
%define pam_ssh_agent_rel 1

Summary: An open source implementation of SSH protocol versions 1 and 2
Name: openssh
Version: %{openssh_ver}
Release: %{momorel}m%{?dist}
URL: http://www.openssh.com/portable.html
Source0: ftp://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-%{version}.tar.gz
NoSource: 0
#Source1: ftp://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-%{version}.tar.gz.asc
Source2: sshd.pam
Source3: sshd.init
Source4: http://prdownloads.sourceforge.net/pamsshagentauth/pam_ssh_agent_auth/pam_ssh_agent_auth-%{pam_ssh_agent_ver}.tar.bz2
NoSource: 4
Source5: pam_ssh_agent-rmheaders
Source6: ssh-keycat.pam
Source7: sshd.sysconfig
Source9: sshd_at.service
Source10: sshd.socket
Source11: sshd.service
Source12: sshd-keygen.service
Source13: sshd-keygen

# Internal debug
Patch0: openssh-5.9p1-wIm.patch

#?
Patch100: openssh-6.3p1-coverity.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1872
Patch101: openssh-6.3p1-fingerprint.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1894
#https://bugzilla.redhat.com/show_bug.cgi?id=735889
Patch102: openssh-5.8p1-getaddrinfo.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1889
Patch103: openssh-5.8p1-packet.patch

#https://bugzilla.mindrot.org/show_bug.cgi?id=1402
Patch200: openssh-6.4p1-audit.patch

# --- pam_ssh-agent ---
# make it build reusing the openssh sources
Patch300: pam_ssh_agent_auth-0.9.3-build.patch
# check return value of seteuid()
Patch301: pam_ssh_agent_auth-0.9.2-seteuid.patch
# explicitly make pam callbacks visible
Patch302: pam_ssh_agent_auth-0.9.2-visibility.patch
# don't use xfree (#1024965)
Patch303: pam_ssh_agent_auth-0.9.3-no-xfree.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1641 (WONTFIX)
Patch400: openssh-6.3p1-role-mls.patch
#https://bugzilla.redhat.com/show_bug.cgi?id=781634
Patch404: openssh-6.3p1-privsep-selinux.patch

#?-- unwanted child :(
Patch501: openssh-6.3p1-ldap.patch
#?
Patch502: openssh-6.3p1-keycat.patch

#http6://bugzilla.mindrot.org/show_bug.cgi?id=1644
Patch601: openssh-5.2p1-allow-ip-opts.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1701
Patch602: openssh-5.9p1-randclean.patch
#http://cvsweb.netbsd.org/cgi-bin/cvsweb.cgi/src/crypto/dist/ssh/Attic/sftp-glob.c.diff?r1=1.13&r2=1.13.12.1&f=h
Patch603: openssh-5.8p1-glob.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1893
Patch604: openssh-5.8p1-keyperm.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1329 (WONTFIX)
Patch605: openssh-5.8p2-remove-stale-control-socket.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1925
Patch606: openssh-5.9p1-ipv6man.patch
#?
Patch607: openssh-5.8p2-sigpipe.patch
#?
Patch608: openssh-6.1p1-askpass-ld.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1789
Patch609: openssh-5.5p1-x11.patch

#?
Patch700: openssh-6.3p1-fips.patch
#?
Patch701: openssh-5.6p1-exit-deadlock.patch
#?
Patch702: openssh-5.1p1-askpass-progress.patch
#?
Patch703: openssh-4.3p2-askpass-grab-info.patch
#?
Patch704: openssh-5.9p1-edns.patch
#?
Patch705: openssh-5.1p1-scp-manpage.patch
#?
Patch706: openssh-5.8p1-localdomain.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1635 (WONTFIX)
Patch707: openssh-6.3p1-redhat.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1890 (WONTFIX) need integration to prng helper which is discontinued :)
Patch708: openssh-6.2p1-entropy.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1640 (WONTFIX)
Patch709: openssh-6.2p1-vendor.patch
# warn users for unsupported UsePAM=no (#757545)
Patch711: openssh-6.1p1-log-usepam-no.patch
# make aes-ctr ciphers use EVP engines such as AES-NI from OpenSSL
Patch712: openssh-6.3p1-ctr-evp-fast.patch
# add cavs test binary for the aes-ctr
Patch713: openssh-6.3p1-ctr-cavstest.patch


#http://www.sxw.org.uk/computing/patches/openssh.html
#changed cache storage type - #848228
Patch800: openssh-6.3p1-gsskex.patch
#http://www.mail-archive.com/kerberos@mit.edu/msg17591.html
Patch801: openssh-6.3p1-force_krb.patch
Patch900: openssh-6.1p1-gssapi-canohost.patch
#https://bugzilla.mindrot.org/show_bug.cgi?id=1780
Patch901: openssh-6.3p1-kuserok.patch
# use default_ccache_name from /etc/krb5.conf (#991186)
Patch902: openssh-6.3p1-krb5-use-default_ccache_name.patch
# increase the size of the Diffie-Hellman groups (#1010607)
Patch903: openssh-6.3p1-increase-size-of-DF-groups.patch

License: BSD
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%if %{nologin}
Requires: util-linux
%endif

Requires: initscripts >= 5.20

%if ! %{no_gnome_askpass}
%if %{gtk2}
BuildRequires: gtk2-devel
BuildRequires: libX11-devel
%else
BuildRequires: gnome-libs-devel
%endif
%endif

%if %{scard}
BuildRequires: sharutils
%endif
%if %{ldap}
BuildRequires: openldap-devel
%endif
BuildRequires: autoconf, automake, perl, zlib-devel
BuildRequires: audit-libs-devel >= 2.0.4
BuildRequires: util-linux, groff
BuildRequires: pam-devel
BuildRequires: tcp_wrappers-devel
BuildRequires: fipscheck-devel >= 1.3.0
BuildRequires: openssl-devel >= 1.0.0

%if %{kerberos5}
BuildRequires: krb5-devel
%endif

%if %{libedit}
BuildRequires: libedit-devel ncurses-devel
%endif

%if %{WITH_SELINUX}
Requires: libselinux >= 1.27.7
BuildRequires: libselinux-devel >= 1.27.7
Requires: audit-libs >= 2.0.4
BuildRequires: audit-libs >= 2.0.4
%endif

BuildRequires: xorg-x11-xauth

%package clients
Summary: An open source SSH client applications
Requires: openssh = %{version}-%{release}
Group: Applications/Internet

%package server
Summary: An open source SSH server daemon
Group: System Environment/Daemons
Requires: openssh = %{version}-%{release}
Requires(post): chkconfig >= 0.9
Requires(pre): shadow-utils
Requires: pam >= 1.0.1-3

%package server-sysvinit
Summary: The SysV initscript to manage the OpenSSH server.
Group: System Environment/Daemons
Requires: %{name}-server = %{version}-%{release}

%description server-sysvinit
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package contains
the SysV init script to manage the OpenSSH server when running a legacy
SysV-compatible init system.

It is not required when the init system used is systemd.

%if %{ldap}
%package ldap
Summary: A LDAP support for open source SSH server daemon
Requires: openssh = %{version}-%{release}
Group: System Environment/Daemons
%endif

%package keycat
Summary: A mls keycat backend for openssh
Requires: openssh = %{version}-%{release}
Group: System Environment/Daemons

%package askpass
Summary: A passphrase dialog for OpenSSH and X
Group: Applications/Internet
Requires: openssh = %{version}-%{release}
Obsoletes: openssh-askpass-gnome
Provides: openssh-askpass-gnome

%package -n pam_ssh_agent_auth
Summary: PAM module for authentication with ssh-agent
Group: System Environment/Base
Version: %{pam_ssh_agent_ver}
Release: %{pam_ssh_agent_rel}m%{?dist}
License: BSD

%description
SSH (Secure SHell) is a program for logging into and executing
commands on a remote machine. SSH is intended to replace rlogin and
rsh, and to provide secure encrypted communications between two
untrusted hosts over an insecure network. X11 connections and
arbitrary TCP/IP ports can also be forwarded over the secure channel.

OpenSSH is OpenBSD's version of the last free version of SSH, bringing
it up to date in terms of security and features.

This package includes the core files necessary for both the OpenSSH
client and server. To make this package useful, you should also
install openssh-clients, openssh-server, or both.

%description clients
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package includes
the clients necessary to make encrypted connections to SSH servers.

%description server
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package contains
the secure shell daemon (sshd). The sshd daemon allows SSH clients to
securely connect to your SSH server.

%if %{ldap}
%description ldap
OpenSSH LDAP backend is a way how to distribute the authorized tokens
among the servers in the network.
%endif

%description keycat
OpenSSH mls keycat is backend for using the authorized keys in the
openssh in the mls mode.

%description askpass
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package contains
an X11 passphrase dialog for OpenSSH.

%description -n pam_ssh_agent_auth
This package contains a PAM module which can be used to authenticate
users using ssh keys stored in a ssh-agent. Through the use of the
forwarding of ssh-agent connection it also allows to authenticate with
remote ssh-agent instance.

The module is most useful for su and sudo service stacks.

%prep
%setup -q -a 4

%if 0
%patch0 -p1 -b .wIm
%endif

%patch100 -p1 -b .coverity
%patch101 -p1 -b .fingerprint
%patch102 -p1 -b .getaddrinfo
%patch103 -p1 -b .packet

%patch200 -p1 -b .audit

%if %{pam_ssh_agent}
pushd pam_ssh_agent_auth-%{pam_ssh_agent_ver}
%patch300 -p1 -b .psaa-build
%patch301 -p1 -b .psaa-seteuid
%patch302 -p1 -b .psaa-visibility
%patch303 -p1 -b .psaa-xfree
# Remove duplicate headers
rm -f $(cat %{SOURCE5})
popd
%endif

%if %{WITH_SELINUX}
%patch400 -p1 -b .role-mls
%patch404 -p1 -b .privsep-selinux
%endif

%if %{ldap}
%patch501 -p1 -b .ldap
%endif
%patch502 -p1 -b .keycat

%patch601 -p1 -b .ip-opts
%patch602 -p1 -b .randclean
%patch603 -p1 -b .glob
%patch604 -p1 -b .keyperm
%patch605 -p1 -b .remove_stale
%patch606 -p1 -b .ipv6man
%patch607 -p1 -b .sigpipe
%patch608 -p1 -b .askpass-ld

%patch700 -p1 -b .fips
%patch701 -p1 -b .exit-deadlock
%patch702 -p1 -b .progress
%patch703 -p1 -b .grab-info
%patch704 -p1 -b .edns
%patch705 -p1 -b .manpage
%patch706 -p1 -b .localdomain
%patch707 -p1 -b .redhat
%patch708 -p1 -b .entropy
%patch709 -p1 -b .vendor
%patch711 -p1 -b .log-usepam-no
%patch712 -p1 -b .evp-ctr
%patch713 -p1 -b .ctr-cavs

%patch800 -p1 -b .gsskex
%patch801 -p1 -b .force_krb

%patch900 -p1 -b .canohost
%patch901 -p1 -b .kuserok
%patch902 -p1 -b .ccache_name
%patch903 -p1 -b .dh

autoreconf
pushd pam_ssh_agent_auth-%{pam_ssh_agent_ver}
autoreconf
popd

%build
CFLAGS="$RPM_OPT_FLAGS"; export CFLAGS
%if %{rescue}
CFLAGS="$CFLAGS -Os"
%endif
%if %{pie}
%ifarch s390 s390x sparc sparcv9 sparc64
CFLAGS="$CFLAGS -fPIC"
%else
CFLAGS="$CFLAGS -fpic"
%endif
export CFLAGS
SAVE_LDFLAGS="$LDFLAGS"
LDFLAGS="$LDFLAGS -pie -z relro -z now"; export LDFLAGS
%endif
%if %{kerberos5}
if test -r /etc/profile.d/krb5-devel.sh ; then
        source /etc/profile.d/krb5-devel.sh
fi
krb5_prefix=`krb5-config --prefix`
if test "$krb5_prefix" != "%{_prefix}" ; then
	CPPFLAGS="$CPPFLAGS -I${krb5_prefix}/include -I${krb5_prefix}/include/gssapi"; export CPPFLAGS
	CFLAGS="$CFLAGS -I${krb5_prefix}/include -I${krb5_prefix}/include/gssapi"
	LDFLAGS="$LDFLAGS -L${krb5_prefix}/%{_lib}"; export LDFLAGS
else
	krb5_prefix=
	CPPFLAGS="-I%{_includedir}/gssapi"; export CPPFLAGS
	CFLAGS="$CFLAGS -I%{_includedir}/gssapi"
fi
%endif

%configure \
	--sysconfdir=%{_sysconfdir}/ssh \
	--libexecdir=%{_libexecdir}/openssh \
	--datadir=%{_datadir}/openssh \
	--with-tcp-wrappers \
	--with-default-path=/usr/local/bin:/bin:/usr/bin \
	--with-superuser-path=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin \
	--with-privsep-path=%{_var}/empty/sshd \
	--enable-vendor-patchlevel="MO-%{version}-%{release}" \
	--disable-strip \
	--without-zlib-version-check \
	--with-ssl-engine \
	--with-authorized-keys-command \
	--with-ipaddr-display \
%if %{scard}
	--with-smartcard \
%endif
%if %{ldap}
	--with-ldap \
%endif
%if %{rescue}
	--without-pam \
%else
	--with-pam \
%endif
%if %{WITH_SELINUX}
	--with-selinux --with-audit=linux \
%if 0
#seccomp_filter cannot be build right now
        --with-sandbox=seccomp_filter \
%else
        --with-sandbox=rlimit \
%endif
%endif
%if %{kerberos5}
	--with-kerberos5${krb5_prefix:+=${krb5_prefix}} \
%else
	--without-kerberos5 \
%endif
%if %{libedit}
	--with-libedit \
%else
	--without-libedit \
%endif
	LIBS="-lplc4 -ldl"

%if %{static_libcrypto}
perl -pi -e "s|-lcrypto|%{_libdir}/libcrypto.a|g" Makefile
%endif

make

# Define a variable to toggle gnome1/gtk2 building.  This is necessary
# because RPM doesn't handle nested %if statements.
%if %{gtk2}
	gtk2=yes
%else
	gtk2=no
%endif

%if ! %{no_gnome_askpass}
pushd contrib
if [ $gtk2 = yes ] ; then
	make gnome-ssh-askpass2
	mv gnome-ssh-askpass2 gnome-ssh-askpass
else
	make gnome-ssh-askpass1
	mv gnome-ssh-askpass1 gnome-ssh-askpass
fi
popd
%endif

%if %{pam_ssh_agent}
pushd pam_ssh_agent_auth-%{pam_ssh_agent_ver}
LDFLAGS="$SAVE_LDFLAGS"
%configure --with-selinux --libexecdir=/%{_lib}/security --with-mantype=man
make
popd
%endif

# Add generation of HMAC checksums of the final stripped binaries
%define __spec_install_post \
    %{?__debug_package:%{__debug_install_post}} \
    %{__arch_install_post} \
    %{__os_install_post} \
    fipshmac -d $RPM_BUILD_ROOT%{_libdir}/fipscheck $RPM_BUILD_ROOT%{_bindir}/ssh $RPM_BUILD_ROOT%{_sbindir}/sshd \
%{nil}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p -m755 $RPM_BUILD_ROOT%{_sysconfdir}/ssh
mkdir -p -m755 $RPM_BUILD_ROOT%{_libexecdir}/openssh
mkdir -p -m755 $RPM_BUILD_ROOT%{_var}/empty/sshd
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/ssh/ldap.conf

install -d $RPM_BUILD_ROOT/etc/pam.d/
install -d $RPM_BUILD_ROOT/etc/sysconfig/
install -d $RPM_BUILD_ROOT/etc/init.d
install -d $RPM_BUILD_ROOT%{_libexecdir}/openssh
install -d $RPM_BUILD_ROOT%{_libdir}/fipscheck
install -m644 %{SOURCE2} $RPM_BUILD_ROOT/etc/pam.d/sshd
install -m644 %{SOURCE6} $RPM_BUILD_ROOT/etc/pam.d/ssh-keycat
install -m755 %{SOURCE3} $RPM_BUILD_ROOT/etc/init.d/sshd
install -m644 %{SOURCE7} $RPM_BUILD_ROOT/etc/sysconfig/sshd
install -m755 %{SOURCE13} $RPM_BUILD_ROOT/%{_sbindir}/sshd-keygen
install -d -m755 $RPM_BUILD_ROOT/%{_unitdir}
install -m644 %{SOURCE9} $RPM_BUILD_ROOT/%{_unitdir}/sshd@.service
install -m644 %{SOURCE10} $RPM_BUILD_ROOT/%{_unitdir}/sshd.socket
install -m644 %{SOURCE11} $RPM_BUILD_ROOT/%{_unitdir}/sshd.service
install -m644 %{SOURCE12} $RPM_BUILD_ROOT/%{_unitdir}/sshd-keygen.service
install -m755 contrib/ssh-copy-id $RPM_BUILD_ROOT%{_bindir}/
install contrib/ssh-copy-id.1 $RPM_BUILD_ROOT%{_mandir}/man1/

%if ! %{no_gnome_askpass}
install -s contrib/gnome-ssh-askpass $RPM_BUILD_ROOT%{_libexecdir}/openssh/gnome-ssh-askpass
%endif

%if ! %{scard}
	rm -f $RPM_BUILD_ROOT%{_datadir}/openssh/Ssh.bin
%endif

%if ! %{no_gnome_askpass}
ln -s gnome-ssh-askpass $RPM_BUILD_ROOT%{_libexecdir}/openssh/ssh-askpass
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
install -m 755 contrib/redhat/gnome-ssh-askpass.csh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
install -m 755 contrib/redhat/gnome-ssh-askpass.sh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
%endif

%if %{no_gnome_askpass}
rm -f $RPM_BUILD_ROOT/etc/profile.d/gnome-ssh-askpass.*
%endif

perl -pi -e "s|$RPM_BUILD_ROOT||g" $RPM_BUILD_ROOT%{_mandir}/man*/*

%if %{pam_ssh_agent}
pushd pam_ssh_agent_auth-%{pam_ssh_agent_ver}
make install DESTDIR=$RPM_BUILD_ROOT
popd
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%pre server
getent group sshd >/dev/null || groupadd -g %{sshd_uid} -r sshd || :
%if %{nologin}
getent passwd sshd >/dev/null || \
  useradd -c "Privilege-separated SSH" -u %{sshd_uid} -g sshd  -s /sbin/nologin \
  -s /sbin/nologin -r -d /var/empty/sshd sshd 2> /dev/null || :
%else
getent passwd sshd >/dev/null || \
  useradd -c "Privilege-separated SSH" -u %{sshd_uid} -g sshd  -s /sbin/nologin \
  -s /dev/null -r -d /var/empty/sshd sshd 2> /dev/null || :
%endif

%post server
%systemd_post sshd.service sshd.socket

%preun server
%systemd_preun sshd.service sshd.socket

%postun server
%systemd_postun_with_restart sshd.service

%triggerun -n openssh-server -- openssh-server < 5.8p2-12
/usr/bin/systemd-sysv-convert --save sshd >/dev/null 2>&1 || :
/bin/systemctl enable sshd.service >/dev/null 2>&1
/bin/systemctl enable sshd-keygen.service >/dev/null 2>&1
/sbin/chkconfig --del sshd >/dev/null 2>&1 || :
/bin/systemctl try-restart sshd.service >/dev/null 2>&1 || :
# This one was never a service, so we don't simply restart it
/bin/systemctl is-active -q sshd.service && /bin/systemctl start sshd-keygen.service >/dev/null 2>&1 || :

%triggerpostun -n openssh-server-sysvinit -- openssh-server < 5.8p2-12
/sbin/chkconfig --add sshd >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc CREDITS ChangeLog INSTALL LICENCE OVERVIEW PROTOCOL* README README.platform README.privsep README.tun README.dns TODO
%attr(0755,root,root) %dir %{_sysconfdir}/ssh
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/ssh/moduli
%if ! %{rescue}
%attr(0755,root,root) %{_bindir}/ssh-keygen
%attr(0644,root,root) %{_mandir}/man1/ssh-keygen.1*
%attr(0755,root,root) %dir %{_libexecdir}/openssh
%attr(4111,root,root) %{_libexecdir}/openssh/ssh-keysign
%attr(0755,root,root) %{_libexecdir}/openssh/ctr-cavstest
%attr(0644,root,root) %{_mandir}/man8/ssh-keysign.8*
%endif
%if %{scard}
%attr(0755,root,root) %dir %{_datadir}/openssh
%attr(0644,root,root) %{_datadir}/openssh/Ssh.bin
%endif

%files clients
%defattr(-,root,root)
%attr(0755,root,root) %{_bindir}/ssh
%attr(0644,root,root) %{_libdir}/fipscheck/ssh.hmac
%attr(0644,root,root) %{_mandir}/man1/ssh.1*
%attr(0755,root,root) %{_bindir}/scp
%attr(0644,root,root) %{_mandir}/man1/scp.1*
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/ssh/ssh_config
%attr(0755,root,root) %{_bindir}/slogin
%attr(0644,root,root) %{_mandir}/man1/slogin.1*
%attr(0644,root,root) %{_mandir}/man5/ssh_config.5*
%if ! %{rescue}
%attr(2111,root,nobody) %{_bindir}/ssh-agent
%attr(0755,root,root) %{_bindir}/ssh-add
%attr(0755,root,root) %{_bindir}/ssh-keyscan
%attr(0755,root,root) %{_bindir}/sftp
%attr(0755,root,root) %{_bindir}/ssh-copy-id
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-pkcs11-helper
%attr(0644,root,root) %{_mandir}/man1/ssh-agent.1*
%attr(0644,root,root) %{_mandir}/man1/ssh-add.1*
%attr(0644,root,root) %{_mandir}/man1/ssh-keyscan.1*
%attr(0644,root,root) %{_mandir}/man1/sftp.1*
%attr(0644,root,root) %{_mandir}/man1/ssh-copy-id.1*
%attr(0644,root,root) %{_mandir}/man8/ssh-pkcs11-helper.8*
%endif

%if ! %{rescue}
%files server
%defattr(-,root,root)
%dir %attr(0711,root,root) %{_var}/empty/sshd
%attr(0755,root,root) %{_sbindir}/sshd
%attr(0755,root,root) %{_sbindir}/sshd-keygen
%attr(0644,root,root) %{_libdir}/fipscheck/sshd.hmac
%attr(0755,root,root) %{_libexecdir}/openssh/sftp-server
%attr(0644,root,root) %{_mandir}/man5/sshd_config.5*
%attr(0644,root,root) %{_mandir}/man5/moduli.5*
%attr(0644,root,root) %{_mandir}/man8/sshd.8*
%attr(0644,root,root) %{_mandir}/man8/sftp-server.8*
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/ssh/sshd_config
%attr(0644,root,root) %config(noreplace) /etc/pam.d/sshd
%attr(0640,root,root) %config(noreplace) /etc/sysconfig/sshd
%attr(0644,root,root) %{_unitdir}/sshd.service
%attr(0644,root,root) %{_unitdir}/sshd@.service
%attr(0644,root,root) %{_unitdir}/sshd.socket
%attr(0644,root,root) %{_unitdir}/sshd-keygen.service
%endif

%files server-sysvinit
%defattr(-,root,root)
%attr(0755,root,root) /etc/init.d/sshd

%if %{ldap}
%files ldap
%defattr(-,root,root)
%doc HOWTO.ldap-keys openssh-lpk-openldap.schema openssh-lpk-sun.schema ldap.conf
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-ldap-helper
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-ldap-wrapper
%attr(0644,root,root) %{_mandir}/man8/ssh-ldap-helper.8*
%attr(0644,root,root) %{_mandir}/man5/ssh-ldap.conf.5*
%endif

%files keycat
%defattr(-,root,root)
%doc HOWTO.ssh-keycat
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-keycat
%attr(0644,root,root) %config(noreplace) /etc/pam.d/ssh-keycat

%if ! %{no_gnome_askpass}
%files askpass
%defattr(-,root,root)
%attr(0644,root,root) %{_sysconfdir}/profile.d/gnome-ssh-askpass.*
%attr(0755,root,root) %{_libexecdir}/openssh/gnome-ssh-askpass
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-askpass
%endif

%if %{pam_ssh_agent}
%files -n pam_ssh_agent_auth
%defattr(-,root,root)
%doc pam_ssh_agent_auth-%{pam_ssh_agent_ver}/OPENSSH_LICENSE
%attr(0755,root,root) /%{_lib}/security/pam_ssh_agent_auth.so
%attr(0644,root,root) %{_mandir}/man8/pam_ssh_agent_auth.8*
%endif

%changelog
* Mon Nov 18 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (6.4p1-1m)
- update 6.4p1

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9p1-3m)
- sync with Fedora devel (openssh-5.9p1-19)
- many bug fixes (import and replace patches)

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9p1-2m)
- sync with Fedora devel (openssh-5.9p1-12)
- increase pam_ssh_agent_rel

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9p1-1m)
- update to 5.9p1

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8p2-2m)
- support systemd

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8p2-1m)
- update to 5.8p2

* Mon Apr 11 2011 ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8p1-4m)
- increase Release of package pam_ssh_agent_auth

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8p1-3m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8p1-2m)
- correct Release of package pam_ssh_agent_auth
- DO NOT DECREASE Release
- DO NOT USE -i option of OmoiKondara at packaging and version/release up
- please type "rpm -Uvh" manually
- it is one of safe methods detecting conflicts and version/release down

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8p1-1m)
- [SECURITY] http://www.openssh.com/txt/legacy-cert.adv
- update to 5.8p1 (sync with Fedora devel)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6p1-2m)
- rebuild for new GCC 4.5

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.6p1-1m)
- sync with Rawhide (5.6p1-1 + 0.9.2-27)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.5p1-4m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.5p1-3m)
- explicitly link -ldl for the moment

* Sat Aug  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5p1-2m)
- fix build failure when %%{with_lpk} is 0

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5p1-1m)
- update to 5.5p1
- sync with Fedora devel (5.5p1-18 + 0.9.2-26)

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.3p1-6m)
- fix build on x86_64

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3p1-5m)
- explicitly link libplc4

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3p1-4m)
- rebuild against openssl-1.0.0

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.3p1-3m)
- rebuild against audit-2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3p1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.3p1-1m)
- update to 5.3p1

* Mon Aug 24 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2p1-3m)
- enable openssh-lpk-5.2p1-0.3.10.patch

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2p1-2m)
- rebuild against openssl-0.9.8k

* Wed Feb 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2p1-1m)
- [SECURITY] http://www.openssh.com/txt/cbc.adv
- update to 5.2p1
- update Patch0: openssh-5.2p1-redhat.patch
- update Patch4: openssh-5.2p1-vendor.patch
- update Patch12: openssh-5.2p1-selinux.patch
- update Patch51: openssh-5.2p1-nss-keys.patch
- comment out Patch63: openssh-5.1p1-bannerlen.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1p1-4m)
- rebuild against rpm-4.6

* Fri Sep 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.1p1-3m)
- Obsoletes: openssh-keysign

* Fri Sep 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1p1-2m)
- sync Fedora
- base openssh-5.1p1-2.fc9

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1p1-1m)
- [SECURITY] fix vulnerability to the X11UseLocalhost=no hijacking attack
- update to 5.1p1

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0p1-3m)
- rebuild against openssl-0.9.8h-1m

* Wed Apr 16 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.0p1-2m)
- use condrestart instead of restart in %post script

* Fri Apr  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0p1-1m)
- update to 5.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.9p1-2m)
- rebuild against gcc43

* Mon Mar 31 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.9p1-1m)
- update to 4.9

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7p1-4m)
- %%NoSource -> NoSource

* Fri Oct 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7p1-3m)
- rebuild against openssl-0.9.8g

* Sat Oct 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7p1-2m)
- rebuild against openssl-0.9.8f

* Fri Sep  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7p1-1m)
- update to 4.7p1
- [SECURITY] Security bugs resolved in this release:
  * Prevent ssh(1) from using a trusted X11 cookie if creation of an
    untrusted cookie fails; found and fixed by Jan Pechanec.

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.6p1-2m)
- rebuild against pam-0.99.7-1m
- Patch: openssh-4.2p1-pam-no-stack.patch copied from fc devel

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6p1-1m)
- update to 4.6p1

* Wed Nov 08 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5p1-1m)
- update to 4.5p1

* Sat Sep 30 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.4p1-1m)
- update to 4.4p1
- [SECURITY] CVE-2006-4924
- http://www.openssh.com/txt/release-4.4

* Tue Mar 21 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3p2-2m)
- rebuild against openssl-0.9.8a

* Mon Feb 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.3p2-1m)
- update to 4.3p2

* Thu Feb 02 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (4.3p1-1m)
- update to 4.3

* Thu Jan 26 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.2p1-2m)
- [SECURITY] CVE-2006-0225

* Fri Sep 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2p1-1m)
- update to 4.2

* Mon Aug  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.1p1-2m)
- ChallengeResponseAuthentication no

* Thu May 26 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1p1-1m)
- update to 4.1

* Thu Mar 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0p1-1m)
- update to 4.0

* Sun Jan 23 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.9p1-4m)
- noinitlog for initscripts

* Sun Nov 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.9p1-3m)
- sunmaso ... (2m ni sashimodoshi)  m(_"_)m
- #"

* Sun Nov 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.9p1-3m)
- modify man directory

* Mon Aug 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.9p1-2m)
- added -u0 to sshd option (in /etc/sysconfig/sshd) to suppress DNS request
- changed attribute of /etc/sysconfig/sshd to noreplace

* Thu Aug 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9p1-1m)
- update to 3.9

* Thu Jul 29 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.8.1pl-3m)
- modify spec

* Wed Jul 21 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.8.1p1-2m)
- modify uid/gid

* Sat Apr 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.8.1p1-1m)
- minor bugfixes

* Wed Mar 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.8p1-1m)
- revise sshd_config.patch

* Tue Mar 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.8p1-1m)
- this version features many improvements and bugfixes

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (3.7.1p2-2m)
- revised spec for rpm 4.2.

* Wed Sep 24 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.7.1p2-1m)
- security fixes again

* Wed Sep 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.7.1p1-1m)
- security fixes

* Wed Apr 30 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.6.1p2-1m)

* Mon Apr  7 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (3.6.1p1-1m)
- update to 3.6.1

* Tue Apr  1 2003 HOSONO Hidetomo <h13o@h12o.org>
- (3.6p1-1m)

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.5p1-5m)
  rebuild against openssl-0.9.7a

* Thu Feb 27 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5p1-4m)
- openssh-askpass and openssh-askpass-gnome are separated from 
  openssh base package.

* Wed Dec 25 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5p1-3m)
- change URI of x11-ssh-askpass tar ball (thanks to NARITA Koichi [Momonga-devel.ja:01144])
- delete comment lines
- always include all files(by zaki)

* Sat Dec  7 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.5p1-2m)
- rebuild against openssl-0.9.6h
- remove unnecessary comment
- correct a misspelled comment :-)

* Sun Oct 20 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5p1-1m)
- update to 3.5p1
- port gnome-ssh-askpass[12] from contrib/redhat/openssh.spec
  cf. RedHat: Use contrib/ Makefile for building askpass programs
- change default gtk2 1
- change ssh-agent from attr(0755,root,root) to attr(2755,root,nobody)
  cf. RedHat: Install ssh-agent setgid nobody to prevent ptrace() key theft attacks
- change BuildPreReq gnome-libs-devel to pkgconfig

* Fri Sep 20 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (3.4p1-5m)
- add portable version to Version:
- %%no_x11_askpass and %%no_gnome_askpass can now be defined
  by command line arguments.
- add %%{?include_specopt}
- rsh support was deprecated, so remove BuildPrereq: rsh
- add ssh_config(5) man page
- add ssh-keysign sub package.
  since ssh-keysign(8) is a setuid programm and only needed
  during hostbased authentication with protocol version 2,
  installing it is not recommened.

* Thu Aug  1 2002 HOSONO Hidetomo <h@h12o.org>
- (3.4-4m)
- correct some wrong spelling of trojan(ed)

* Thu Aug  1 2002 HOSONO Hidetomo <h@h12o.org>
- (3.4-3m)
- stop building in %prep when trojaned openssh-3.4p1.tar.gz is find

* Tue Jun 27 2002 Masahiro Takahata <takahata@mtaka.com>
- (3.4-2k)
- upgrade to 3.4

* Thu Jun 25 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (3.3-4k)
- specify package name for papre and postun
- add sshd_config.5

* Thu Jun 25 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (3.3-2k)
- update to 3.3p1
- useradd/groupadd sshd for privilege separation

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.2.3-2k)
  update to 3.2.3p1

* Fri May 17 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.2.2p1-6k)
- add --with-4in6=yes configure option if _ipv6 is defined as 1

* Fri May 17 2002 HOSONO Hidetomo <h@kondara.org>
- (3.2.2p1-4k)
- change Prereq and BuildPreReq(openssl-0.9.6c or later).

* Fri May 17 2002 WATABE Toyokazu <toy2@kondara.org>
- (3.2.2p1-2k)
- update to 3.2.2p1 to fix several security ploblems.
  see http://marc.theaimsgroup.com/?l=openssh-unix-dev&m=102158874317836&w=2
- move scp to openssh package.

* Wed May  1 2002 WATABE Toyokazu <toy2@kondara.org>
- (3.1p1-6k)
- apply a patch.
  see http://marc.theaimsgroup.com/?l=openssh-unix-dev&m=101982330707988&w=2

* Fri Apr 12 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (3.1p1-4k)
- fix spec about rsa1 problem.

* Fri Mar  8 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1p1-2k)
- update to 3.1p1

* Tue Feb 19 2002 Shingo Akagaki <dora@Kondara.org>
- (3.0.2p1-8k)
- Obsoletes: sftp

* Sun Feb 17 2002 WATABE Toyokazu <toy2@kondara.org>
- (3.0.2p1-6k)
- be available sftp and its manual.

* Sun Dec  9 2001 Toru Hoshina <t@kondara.org>
- (3.0.2p1-4k)
- applied init patch.
- See. [Kondara-devel.ja:06055]

* Tue Dec  4 2001 Toru Hoshina <t@kondara.org>
- (3.0.2p1-2k)
- update to 3.0.2p1
- suma-so.

* Tue Nov 20 2001 Tsutomu Yasuda <tom@kondara.org>
- (3.0.1p1-2k)
  update to 3.0.1p1

* Mon Nov 12 2001 Tsutomu Yasuda <tom@kondara.org>
- (3.0p1-2k)
  update to 3.0p1

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.9.9p2-4k)
- rebuild against gettext 0.10.40.

* Sun Sep 30 2001 MATSUDA< Daiki <dyky@df-usa.com>
- (2.9.9p2-2k)
- update to 2.9.9p2 for security fix (http://www.securityfocus.com/bid/3369)
- update x11-ask-pass to 1.2.4.1
- rename /etc/ssh/primes to /etc/ssh/moduli for fixing original source

* Mon Sep 24 2001 Toru Hoshina <t@kondara.org>
- (2.9p2-16k)
- suid of /usr/bin/ssh is turned off.
- scp should be in ssh-clients.

* Fri Sep 21 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.9p2-14k)
  fix spec

* Sun Sep  9 2001 Toru Hoshina <t@kondara.org>
- (2.9p2-8k)
- rebuild against XFree 4.1.0, applied x11-askpass-1.2.2-xf41.patch, however
  it might be wrong for X403 environment.

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (2.9p2-6k)
- add alphaev5 support.

* Sun Jul 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.9p2-4k)
- update to 2.9p2 for openssl security fix
- so, modified add version '0.9.6b' to openssl required tag

* Fri Jun 22 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.9p2-3k)
- update to 2.9p2
- delete openssh-2.9-cookies.patch 
  since it is already imported into source

* Sat Jun 16 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.9p1-9k)
- added openssh-2.9-cookies.patch (from FreeBSD's openssh-portable port)
  this patch fixed the problem of
  http://www.securityfocus.com/archive/1/188450
- BuildPreReq: pam => pam-devel

* Thu May  3 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>                     
- (2.9p1-4k)
  added %{_sysconfdir}/sysconfig/sshd

* Thu May  3 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>                     
- (2.9p1-2k)                                                                    
  openssh update to 2.9p1                                                       
  x11-askpass update to 1.2.2

* Thu Apr 19 2001 Kenichi Matsubara <m@kondara.org>
- (2.5.2p2-10k)
- remove sftp,sftp.man.

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.5.2p2-8k)
- rebuild against openssl 0.9.6.

* Sat Mar 24 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.5.2p2-3k)
- update to 2.5.2p2

* Thu Mar 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.5.2p1-7k)
- support gtk+1-1.2.9

* Thu Mar 22 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.5.2p1-5k)
- when creating ssh_host_dsa_key, use "-t dsa" instead of "-d".
- create ssh_host_rsa_key (RSA key for protocol ver.2) on %%post.
- uncommentout "HostKey /etc/ssh/ssh_host_rsa_key"

* Wed Mar 20 2001 TABUCHI Takaaki <tab@kondara.org>
- (2.5.2p1-3k)
- update to 2.5.2p1
- change openssh-kondara.patch adapt for openssh-2.5.2p1.
  (and comment out "HostKey /etc/ssh/ssh_host_rsa_key".
   If you want to use this function, maybe you need to 
   add exec ssh-keygen at post server section in this spec file.)
- comment out make-ssh-known-hosts at install,
  because this file is deleted in openssh tarball.
  (This file is not listed in files section. miss?)
- add ssh-copy-id, ssh-copy-id.1* in clients files section

* Thu Mar  1 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.5.1p2-3k)
- update to 2.5.1p2

* Wed Feb 28 2001 TABUCHI Takaaki <tab@kondara.org>
- (2.5.1p1-5k)
- add --with-pam at configure
- comment out ssh-keyscan* at files for clients package

* Mon Feb 23 2001 TABUCHI Takaaki <tab@kondara.org>
- (2.5.1p1-3k)
- update to 2.5.1p1
- comment out patch1(openssh-init.patch)
- remove README.Ylonen, COPYING.Ylonen
- update x11-ssh-askpass for 1.2.0
- add /etc/ssh/primes at files
- fix missed directory /etc/ssh/primes
- change permission 600 to 644 /etc/pam.d/sshd

* Mon Feb 05 2001 WATABE Toyokazu <toy2@kondara.org>
- (2.3.0p1-13k)
- use _sysconfdir macro.
- add make-ssh-known-hosts and ssh-copy-id.
* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.0p1-11k)
- rebuild againt rpm-3.0.5-39k
* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- (2.3.0p1-7k)
- use _initscriptdir macro to keep backword compatibility.
* Thu Nov 16 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.3.0p1-5k)
- update gnome-ssh-askpass.* made by h@kondara.org.
* Wed Nov 15 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.3.0p1-3k)
- update x11-ssh-askpass for 1.1.1
- modified install of x11-ssh-askpass.
* Tue Nov 07 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.3.0p1-2k)
- update x11-ssh-askpass for 1.1.0
- add openssh-init.patch
* Tue Nov 07 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.3.0p1-1k)
- update openssh for 2.3.0p1
- update openssh-kondara.patch
- remove sshd.init
* Mon Oct 30 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.2.0p1-4k)
- update ssh-askpass for 1.0.3
* Mon Oct 16 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.2.0p1-2k)
- update ssh-askpass for 1.0.2
* Fri Sep 15 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.2.0p1-1k)
- Adopt to FHS-2.1
* Sat Sep 02 2000 Toyokazu WATABE <toy2@kondara.org>
- (2.2.0p1-0k)
- add openssh-2.2.0p1-kondara.patch
* Tue Aug 08 2000 Damien Miller <djm@mindrot.org>
- Some surgery to sshd.init (generate keys at runtime)
- Cleanup of groups and removal of keygen calls
* Wed Jul 12 2000 Damien Miller <djm@mindrot.org>
- Make building of X11-askpass and gnome-askpass optional
* Thu Jun 15 2000 Toyokazu Watabe <toy2@kondara.org>
- add openssh-2.1.1p1-kondara.patch
  fix config files and these pathes.
* Mon Jun 12 2000 Damien Miller <djm@mindrot.org>
- Glob manpages to catch compressed files
* Sat Jun 10 2000 Toyokazu Watabe <toy2@kondara.org>
- update for 2.1.1p1
* Mon May 29 2000 Hidetomo Hosono <h@kondara.org>
- patched for contrib/gnome-ssh-askpass.c
  because of the problem with grabbing X server and command arguments dealing.
  (very quick and ad-hoc hack)
* Wed May 10 2000 Toyokazu Watabe <toy2@kondara.org>
- update for 2.1.0
* Sun Mar 26 2000 Tenkou N. Hattori <tnh@kondara.org>
- be a NoSrc :-P
* Wed Mar 15 2000 Damien Miller <djm@ibs.com.au>
- Updated for new location
- Updated for new gnome-ssh-askpass build
* Sun Dec 26 1999 Damien Miller <djm@mindrot.org>
- Added Jim Knoble's <jmknoble@pobox.com> askpass
* Mon Nov 15 1999 Damien Miller <djm@mindrot.org>
- Split subpackages further based on patch from jim knoble <jmknoble@pobox.com>
* Sat Nov 13 1999 Damien Miller <djm@mindrot.org>
- Added 'Obsoletes' directives
* Tue Nov 09 1999 Damien Miller <djm@ibs.com.au>
- Use make install
- Subpackages
* Mon Nov 08 1999 Damien Miller <djm@ibs.com.au>
- Added links for slogin
- Fixed perms on manpages
* Sat Oct 30 1999 Damien Miller <djm@ibs.com.au>
- Renamed init script
* Fri Oct 29 1999 Damien Miller <djm@ibs.com.au>
- Back to old binary names
* Thu Oct 28 1999 Damien Miller <djm@ibs.com.au>
- Use autoconf
- New binary names
* Wed Oct 27 1999 Damien Miller <djm@ibs.com.au>
- Initial RPMification, based on Jan "Yenya" Kasprzak's <kas@fi.muni.cz> spec.
