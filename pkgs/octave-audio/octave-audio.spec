%global	momorel 2
%global octpkg audio

Name:           octave-%{octpkg}
Version:        1.1.4
Release: 	%{momorel}m%{?dist}
Summary:        Audio for Octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/audio/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
BuildRequires:  octave-devel  

Requires:       octave(api) = %{octave_api}
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
This package contains octave scripts for recording, playback, reading,
writing and plotting audio samples.

%prep
%setup -q -n %{octpkg}-%{version}

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)
%{octpkglibdir}

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%doc %{octpkgdir}/doc
%{octpkgdir}/sample.wav


%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.4-2m)
- rebuild against octave-3.6.1-1m 

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-1m)
- initial import from fedora
