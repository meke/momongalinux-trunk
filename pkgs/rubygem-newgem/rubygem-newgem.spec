%global momorel 5
%global abi_ver 1.9.1

# Generated from newgem-1.5.2.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname newgem
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Quickly bundle any Ruby libraries into a RubyGem and share it with the world, your colleagues, or perhaps just with yourself amongst your projects
Name: rubygem-%{gemname}
Version: 1.5.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://newgem.rubyforge.org
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(activesupport) >= 2.0.2
Requires: rubygem(rubigen) >= 1.5.2
Requires: rubygem(hoe) >= 2.3.1
Requires: rubygem(RedCloth) >= 4.1.1
Requires: rubygem(syntax) >= 1.0.0
Requires: rubygem(cucumber) >= 0.3.11
Requires: rubygem(hoe) >= 2.3.1
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Quickly bundle any Ruby libraries into a RubyGem and share it with the world,
your colleagues, or perhaps just with yourself amongst your projects.
RubyGems are centrally stored, versioned, and support dependencies between
other gems, so they are the ultimate way to bundle libraries, executables,
associated tests, examples, and more.
Within this gem, you get one thing - <code>newgem</code> - an executable to
create your own gems. Your new gems will include designated folders for Ruby
code, test files, executables, and even a default website page for you to
explain your project, and which instantly uploads to RubyForge website (which
looks just like this one by default)


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/newgem
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/PostInstall.txt
%doc %{geminstdir}/app_generators/newgem/templates/History.txt
%doc %{geminstdir}/app_generators/newgem/templates/PostInstall.txt
%doc %{geminstdir}/newgem_generators/install_website/templates/website/index.txt
%doc %{geminstdir}/newgem_theme_generators/long_box_theme/templates/website/index.txt
%doc %{geminstdir}/rubygems_generators/extconf/templates/README.txt
%doc %{geminstdir}/website/index.txt
%doc %{geminstdir}/website/rubyforge.txt
%doc %{geminstdir}/website/version-raw.txt
%doc %{geminstdir}/website/version.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Nov 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.3-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.3-1m)
- update 1.5.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-1m)
- Initial package for Momonga Linux
