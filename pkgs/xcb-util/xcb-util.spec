%global momorel 1

Summary: xcb-util
Name: xcb-util
Version: 0.3.9
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Development/Libraries
URL: http://xcb.freedesktop.org/
Source0: http://xcb.freedesktop.org/dist/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: libxcb-devel >= 1.3
BuildRequires: gperf


%description
The xcb-util module provides a number of libraries which sit on top of
libxcb, the core X protocol library, and some of the extension
libraries. These experimental libraries provide convenience functions
and interfaces which make the raw X protocol more usable. Some of the
libraries also provide client-side code which is not strictly part of
the X protocol but which have traditionally been provided by Xlib.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static --enable-silent-rules --enable-devel-docs
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog NEWS README
%{_libdir}/libxcb-util.so.*
%exclude %{_libdir}/libxcb-util.la

%files devel
%defattr(-, root, root)
%{_includedir}/xcb/xcb_*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/xcb-atom.pc
%{_libdir}/pkgconfig/xcb-aux.pc
%{_libdir}/pkgconfig/xcb-event.pc
%{_libdir}/pkgconfig/xcb-util.pc

%changelog
* Sun Aug 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.9-1m)
- update to 0.3.9

* Tue Jun 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.6-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-2m)
- fix libxcb version

* Sat Jul 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.5-1m)
- update to 0.3.5

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-1m)
- update to 0.3.4
- License: MIT/X

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-4m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 YAMAZAKI Makoto <zaki@momonga-linux.org>
- (0.2-3m)
- added BuildPrereq gperf

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc43

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2-1m)
- initial build
