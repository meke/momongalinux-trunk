%global momorel 1
%global srcrel  10
%global srchash g49d1efe
%global srcname yandod-%{name}-v%{version}-%{srcrel}-%{srchash}.tar.gz
%global arcname yandod-%{name}-49d1efe

# %define __find_requires %{nil}

Summary: Candycane is bug tracker similar to Redmine by PHP
Name: candycane
Version: 0.8.2.1
Release: %{momorel}m%{?dist}
License: MIT
Group: Development/Tools
URL: http://my.candycane.jp/
Source0: %{srcname}
Source1: candycane.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Autoreq: 0
Requires: webserver
Requires: php php-mbstring php-mysql php-mysqli php-pgsql

%description
Wiki written by PHP. Supports verious pulugins.

%prep
%setup -q -n %{arcname}

%build
# fix permissions
%{__chmod} 1777 app/config
%{__chmod} 0755 app/files
%{__chmod} 0755 app/tmp


%install
rm -rf %{buildroot}
install -d %{buildroot}%{_datadir}/candycane
install -d %{buildroot}%{_sysconfdir}/httpd/conf.d
install -d %{buildroot}%{_sysconfdir}/candycane
# install -d %{buildroot}%{_localstatedir}/www/candycane

cp -a * %{buildroot}%{_datadir}/%{name}
cp -a .htaccess %{buildroot}%{_datadir}/%{name}
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/.git*
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/coverage.sh
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/readme.md
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/app/vendors/tcpdf/fonts/utils/*.exe
%{__rm} -f  %{buildroot}/%{_datadir}/%{name}/app/vendors/tcpdf/fonts/utils/*.dll
%{__rm} -f  %{buildroot}/%{_datadir}/%{name}/app/vendors/tcpdf/fonts/utils/pfm2afm
%{__rm} -f  %{buildroot}/%{_datadir}/%{name}/app/vendors/tcpdf/fonts/utils/ttf2ufm
install -m0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/candycane.conf.dist


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc readme.md
%{_sysconfdir}/httpd/conf.d/candycane.conf.dist
%attr(0775,root,apache) %{_datadir}/candycane/app/tmp
%attr(0775,root,apache) %{_datadir}/candycane/app/files
%attr(0775,root,apache) %dir %{_datadir}/candycane/app/config
%attr(0775,root,apache) %config(noreplace) %{_datadir}/candycane/app/config/*.php
%attr(0775,root,apache) %config(noreplace) %{_datadir}/candycane/.htaccess
%{_datadir}/candycane/app/config/database.php.default
%{_datadir}/candycane/app/config/database.php.install
%{_datadir}/candycane/app/config/sql/db_acl.php
%{_datadir}/candycane/app/config/sql/db_acl.sql
%{_datadir}/candycane/app/config/sql/dump.sql
%{_datadir}/candycane/app/config/sql/i18n.php
%{_datadir}/candycane/app/config/sql/i18n.sql
%{_datadir}/candycane/app/config/sql/sessions.php
%{_datadir}/candycane/app/config/sql/sessions.sql
%{_datadir}/candycane/app/.htaccess
%{_datadir}/candycane/app/*.php
%{_datadir}/candycane/app/controllers
%{_datadir}/candycane/app/locale
%{_datadir}/candycane/app/models
%{_datadir}/candycane/app/plugins
%{_datadir}/candycane/app/tests
%{_datadir}/candycane/app/vendors
%{_datadir}/candycane/app/views
%{_datadir}/candycane/app/webroot
%{_datadir}/candycane/index.php
%{_datadir}/candycane/cake
%{_datadir}/candycane/vendors


# %dir %{_localstatedir}/www/candycane



%changelog
* Thu Nov 10 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.2-1m)
- initial release
