%global momorel 5

%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitelib: %define tcl_sitelib %{_datadir}/tcl%{tcl_version}}

Summary: The standard Tcl library
Name: tcllib
Version: 1.11.1
Release: %{momorel}m%{?dist}
License: see "license.terms"
Group: Development/Libraries
Source: http://dl.sourceforge.net/sourceforge/tcllib/tcllib-%{version}.tar.bz2
NoSource: 0
URL: http://tcllib.sourceforge.net/
BuildArchitectures: noarch
Requires: tcl(abi) = 8.5
BuildRequires: tcl >= 8.3.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Tcllib, the Tcl Standard Library is a collection of Tcl packages
that provide utility functions useful to a large collection of Tcl
programmers.

%prep
%setup -q
chmod -x modules/doctools/mpformats/fr.msg
# Convert a couple of files to UTF-8
for file in modules/struct/pool.html ; do
    iconv --from=ISO-8859-1 --to=UTF-8 ${file} > ${file}.new
    mv -f ${file}.new ${file}
done

%build
# Nothing to build!

%install
rm -rf $RPM_BUILD_ROOT
echo 'not available' > modules/imap4/imap4.n
%{_bindir}/tclsh installer.tcl -no-gui -no-wait -no-html -no-examples \
    -pkg-path $RPM_BUILD_ROOT/%{tcl_sitelib}/%name-%version \
    -app-path $RPM_BUILD_ROOT%{_bindir} \
    -nroff-path $RPM_BUILD_ROOT%_mandir/mann
# install HTML documentation into specific modules sub-directories:
pushd modules
cp ftp/docs/*.html ftp/
for module in comm exif ftp mime stooop struct textutil; do
    mkdir -p ../$module && cp $module/*.html ../$module/;
done
popd

# Clean up rpmlint warnings
find $RPM_BUILD_ROOT/%{_datadir} -name \*.tcl -exec chmod 0644 {} \;

iconv --from=ISO-8859-1 --to=UTF-8 $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip.n > $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip.n.utf8
iconv --from=ISO-8859-1 --to=UTF-8 $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip_util.n > $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip_util.n.utf8
mv $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip.n.utf8 $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip.n
mv $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip_util.n.utf8 $RPM_BUILD_ROOT/%{_mandir}/mann/docstrip_util.n

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc support/releases/PACKAGES README support/releases/history/README-1.9.txt
%doc ChangeLog license.terms
%doc exif/ ftp/ mime/ stooop/ struct/ textutil/
%{tcl_sitelib}/%{name}-%{version}
%{_mandir}/mann/*
%{_bindir}/dtplite
%{_bindir}/page
%{_bindir}/tcldocstrip

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.11.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.1-1m)
- update to 1.11.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-2m)
- rebuild against rpm-4.6

* Sat May 17 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.10-1m)
- import to Momonga from Fedora

* Thu Jan 3 2008 Wart <wart at kobold.org> - 1.10-2
- Rebuild for Tcl 8.5

* Sat Nov 24 2007 Wart <wart at kobold.org> - 1.10-1
- Update to 1.10
- Fix download URL

* Tue Aug 21 2007 Wart <wart at kobold.org> - 1.9-5
- License tag clarification

* Fri May 11 2007 Wart <wart at kobold.org> - 1.9-4
- Include command line applications

* Fri Feb 16 2007 Wart <wart at kobold.org> - 1.9-3
- Rebuild for reversion back to tcl8.4

* Thu Feb 1 2007 Wart <wart at kobold.org> 1.9-2
- Rebuild for tcl8.5 (changes tcllib's install directory)

* Thu Oct 19 2006 Wart <wart at kobold.org> 1.9-1
- Update to 1.9

* Mon Aug 21 2006 Wart <wart at kobold.org> 1.8-5
- Rebuild for Fedora Extras

* Thu Feb 16 2006 Wart <wart at kobold.org> 1.8-4
- Remove executable bits on the library files to clean up rpmlint
  warnings.

* Mon Oct 17 2005 Wart <wart at kobold.org> 1.8-3
- Bumped release number again so to match the release in the FC3/FC4
  branches.

* Sun Oct 16 2005 Wart <wart at kobold.org> 1.8-2
- Bumped release number to fix cvs tagging problem.

* Sun Oct 16 2005 Wart <wart at kobold.org> 1.8-1
- update to 1.8

* Sun Oct 2 2005 Wart <wart at kobold.org> 1.7-3
- Remove generated filelist; other minor spec file improvements.

* Mon Jul 4 2005 Wart <wart at kobold.org> 1.7-2
- Minor spec file changes in an attempt to conform to Fedora Extras
  packaging guidelines.

* Thu Oct 14 2004 Jean-Luc Fontaine <jfontain@free.fr> 1.7-1
- 1.7 version
- new modules: asn, bee, grammar_fa, http, ident, jpeg, ldap,
  png, rc4, ripemd, tar, tie, treeql, uuid
- modules removed: struct1

* Thu Feb 19 2004 Jean-Luc Fontaine <jfontain@free.fr> 1.6-1
- 1.6 version
- leaner and cleaner spec file based on Fedora standards
- install under tcl_library, not hard-coded /usr/lib
