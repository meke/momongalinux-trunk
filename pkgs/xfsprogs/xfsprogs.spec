%global         momorel 1

Summary:	Utilities for managing the XFS filesystem
Name:		xfsprogs
Version:	3.1.11
Release:	%{momorel}m%{?dist}
# Licensing based on generic "GNU GENERAL PUBLIC LICENSE"
# in source, with no mention of version.
# doc/COPYING file specifies what is GPL and what is LGPL
# but no mention of versions in the source.
License:	GPL+ and LGPLv2+
Source0:	ftp://oss.sgi.com/projects/xfs/cmd_tars/%{name}-%{version}.tar.gz
NoSource:	0
Source1:	xfsprogs-wrapper.h

# DO NOT REMOVE this patch
Patch10:        xfsprogs-3.1.1-mkfs-inode.patch

URL: http://oss.sgi.com/projects/xfs/
Group: System Environment/Base
Conflicts: xfsdump < 2.0.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre):  glibc
BuildRequires:	libtool, gettext
BuildRequires:	e2fsprogs-devel
BuildRequires:  ncurses-devel
Provides:	xfs-cmds
Provides:	/sbin/fsck.xfs
Obsoletes:	xfs-cmds <= %{version}
Conflicts:	xfsdump < 2.0.0

%description
A set of commands to use the XFS filesystem, including mkfs.xfs.

XFS is a high performance journaling filesystem which originated
on the SGI IRIX platform.  It is completely multi-threaded, can
support large files and large filesystems, extended attributes,
variable block sizes, is extent based, and makes extensive use of
Btrees (directories, extents, free space) to aid both performance
and scalability.

Refer to the documentation at http://oss.sgi.com/projects/xfs/
for complete details.  This implementation is on-disk compatible
with the IRIX version of XFS.

%package devel
Summary: XFS filesystem-specific static libraries and headers
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
xfsprogs-devel contains the libraries and header files needed to
develop XFS filesystem-specific programs.

You should install xfsprogs-devel if you want to develop XFS
filesystem-specific programs,  If you install xfsprogs-devel, you'll
also want to install xfsprogs.
#'

%prep
%setup -q
%patch10 -p1 -b .mkfs-inode

%build
export tagname=CC DEBUG=-DNDEBUG
#autoconf

# xfsprogs abuses libexecdir
export tagname=CC DEBUG=-DNDEBUG
%configure \
        --enable-readline=yes   \
        --enable-blkid=yes

# Kill rpaths
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# do not use smp_mflags
make -j1

%install
%{__rm} -rf %{buildroot}
make DIST_ROOT=$RPM_BUILD_ROOT install install-dev install-qa

# nuke .la files, etc
rm -f $RPM_BUILD_ROOT/{%{_lib}/*.{la,a,so},%{_libdir}/*.la}

# remove non-versioned docs location
rm -rf $RPM_BUILD_ROOT/%{_datadir}/doc/xfsprogs/

# ugly hack to allow parallel install of 32-bit and 64-bit -devel packages:
%define multilib_arches %{ix86} x86_64 ppc ppc64 s390 s390x sparcv9 sparc64

%ifarch %{multilib_arches}
mv -f $RPM_BUILD_ROOT%{_includedir}/xfs/platform_defs.h \
      $RPM_BUILD_ROOT%{_includedir}/xfs/platform_defs-%{_arch}.h
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_includedir}/xfs/platform_defs.h
%endif

mv $RPM_BUILD_ROOT/sbin/*  $RPM_BUILD_ROOT/%{_sbindir}/
mv $RPM_BUILD_ROOT/%{_lib}/*  $RPM_BUILD_ROOT/%{_libdir}/

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc doc/CHANGES doc/COPYING doc/CREDITS README
%{_sbindir}/*
%{_libdir}/*.so.*
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_sbindir}/*

%files devel
%defattr(-,root,root)
%{_mandir}/man3/*
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.so

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.10-1m)
- update to 3.1.10

* Fri Dec 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.10-1m)
- update to 3.1.10

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.8-1m)
- update to 3.1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.5-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.5-1m)
- update 3.1.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.4-1m)
- update 3.1.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.1-4m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-3m)
- rebuild against readline6

* Sat Feb 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-2m)
- revive mkfs-inode patch, and do not remove it...

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- sync with Fedora devel, update to 3.1.1

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- sync with Fedora devel, update to 3.1.0

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3
- xfsprogs could be built without removing xfsdump

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-4m)
- delete __libtoolize hack

* Fri Dec 18 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.1-3m)
- define __libtoolize (build fix) 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.1-1m)
- update 3.0.1
- commented out autoconf

* Fri May  8 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.0-2m)
- removed smp_mflags from make arguments

* Sat Feb  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-1m)
- update 3.0.0
- sync with fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.7-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.7-2m)
- rebuild against gcc43

* Fri Mar  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.7-1m)
- update 2.9.7

* Thu Feb  7 2008 Yohsuke Ooi <tab@momonga-linux.org>
- (2.9.6-1m)
- update 2.9.6
- use ncurses

* Tue Jan 22 2008 Yohsuke Ooi <tab@momonga-linux.org>
- (2.9.5-1m)
- update 2.9.5

* Fri Dec 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.18-1m)
- update

* Fri Dec 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.11-3m)
- chmod 0755

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.11-2m)
- update xfsprogs-wrapper.h for i686 arch

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.11-1m)
- update to 2.8.11
  merge from CentOS 4.3 CentOSplus

* Sun May 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.11-1m)
- update to 2.7.11

* Sat Mar 04 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.3-3m)
- add Patch2: xfsprogs-i-size-512.patch

* Wed Dec 28 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.7.3-2m)
- no NoSource

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.7.3-1m)
- update to 2.7.3

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.25-2m)
- enable x86_64.
- use relative path in symlinks.

* Wed Oct 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.25-1m)
  update to 2.6.25

* Sun May 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.13-1m)
- update 2.6.13
- update Patch1: xfsprogs-shlibexe.patch

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.0-2m)
- revised spec for rpm 4.2.

* Tue Nov 25 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.5.11-1m)
- update to 2.5.11

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.9-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Feb 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.3.9-1m)
  update to 2.3.9

* Tue Nov 26 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.3.6-1m)
- update to 2.3.6 ([Momonga-devel.ja:00833] thanks, nosanosa)
- add BuildPrereq: e2fsprogs-devel (libuuid is used)
- add execute permissions to shlibs
- delete NoSource

* Mon Sep  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.2-2m)
- fix file location

* Tue Sep  3 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.2-1m)
  update to 2.2.2
	  
* Wed Jul  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.6-1m)

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.3-4k)
- PreReq: /sbin/ldconfig -> glibc

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.0.3-2k)
- update to 2.0.3

* Fri Apr 12 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.1-2k)
- update to 2.0.1

* Sun Mar 17 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.0-2k)
- update to 2.0.0

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.3.17-2k)
- update to 1.3.17

* Thu Dec 18 2001 Masaru Sato <masachan@kondara.org>
- (1.3.13-6k)
- add xfsprogs-1.3.13.progpath.patch

* Thu Dec 18 2001 Masaru Sato <masachan@kondara.org>
- (1.3.13-6k)
- add xfsprogs-1.3.13.progpath.patch

* Thu Nov  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.3.13-2k)
- update to 1.3.13

* Fri Sep 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.3.7-2k)
- First Kondarization
