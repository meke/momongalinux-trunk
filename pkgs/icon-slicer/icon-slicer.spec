%global momorel 11
Summary: Utility for icon theme generation
Name: icon-slicer
Version: 0.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Source: icon-slicer-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel
BuildRequires: popt

%description
icon-slicer is a utility for generating icon themes and libXcursor cursor 
themes.

%prep
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}

%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README
%{_bindir}/icon-slicer

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-11m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-4m)
- rebuild against gcc43

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3-3m)
- merge from FC2-test2.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Aug 28 2003 Owen Taylor <otaylor@redhat.com> 0.3-2
- Rebuild with %%defattr fix for 99371

* Mon Jul  7 2003 Owen Taylor <otaylor@redhat.com>
- Initial package
