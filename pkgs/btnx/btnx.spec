%global momorel 10

Summary: A daemon for sending keyboard and mouse combination events when pressing mouse buttons
Name: btnx
Version: 0.4.11
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://www.ollisalonen.com/btnx/
Group: Applications/System
Source0: http://www.ollisalonen.com/%{name}/%{name}-%{version}.tar.gz
Source1: %{name}.init
Patch0: %{name}-0.4.7-momonga-init.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(preun): chkconfig
BuildRequires: libdaemon-devel

%description
btnx is a daemon that sniffs events from the mouse event handler.
If it recognizes a configured event, it sends a keyboard and/or mouse
combination event to uinput. This means you can configure certain
mouse buttons to send keyboard and mouse events to X.

It is useful for mice with more buttons than window managers can
handle. It also means you won't need to manually edit your window
manager and X configurations to get additional functionality from
extra buttons.

The configuration file for btnx is located at /etc/btnx/btnx_config.
The configuration file is edited with btnx-config. You must install
and run btnx-config before btnx will work.
After changing the config file, make sure to restart btnx in btnx-config
or by running

%prep
%setup -q
%patch0 -p1 -b .init

%build
CFLAGS="`echo %{optflags} | sed -e 's/-Wp,-D_FORTIFY_SOURCE=2//'`"
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# overwrite %%{_initscriptdir}/btnx
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/btnx

# fix permission
chmod 755 %{buildroot}%{_sbindir}/btnx

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/chkconfig --add btnx

%preun
if [ $1 = 0 ]; then
   %{_initscriptdir}/btnx stop > /dev/null 2>&1
   /sbin/chkconfig --del btnx
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%dir %{_sysconfdir}/btnx
%config %{_sysconfdir}/btnx/events
%config %{_initscriptdir}/btnx
%{_sbindir}/btnx

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.11-10m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.11-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.11-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.11-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.11-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.11-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.11-4m)
- temporarily drop -Wp,-D_FORTIFY_SOURCE=2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.11-3m)
- rebuild against rpm-4.6

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.11-2m)
- remove Requires: btnx-config

* Fri May  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.11-1m)
- version 0.4.11

* Fri Apr 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-1m)
- version 0.4.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-4m)
- rebuild against gcc43

* Tue Mar 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-3m)
- remove dummy configuration files

* Tue Mar 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-2m)
- fix %%preun and PreReq: chkconfig

* Tue Mar 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-1m)
- initial package for cutting-edge mouse freaks using Momonga Linux
