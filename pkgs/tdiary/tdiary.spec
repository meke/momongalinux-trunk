%global momorel 5

Summary: tDiary is a web application to write diary and weblog.
Name: tdiary
Version: 2.3.3
Release: %{momorel}m%{?dist}
Source0: http://www.tdiary.org/download/%{name}-full-%{version}.tar.gz
NoSource: 0
Source2: tdiary-setup
URL: http://www.tdiary.org/
License: GPL
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: ruby

%description
tDiary is a web application to write diary and weblog, which is written
in Ruby as CGI.  The special feature of tDiary is TSUKKOMI: readers of
tdiary make comments on each day's diary and everyone can watch the
messages.

%prep
%setup -q
rm -rf %{buildroot}

%build
%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/%{name}
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}/
install -m 755 index.rb %{buildroot}%{_datadir}/%{name}/
install -m 755 update.rb %{buildroot}%{_datadir}/%{name}/
install -m 644 tdiary.rb %{buildroot}%{_datadir}/%{name}/
install -m 644 tdiary.conf.sample %{buildroot}%{_datadir}/%{name}/
install -m 644 dot.htaccess %{buildroot}%{_datadir}/%{name}/
cp -a misc plugin skel theme tdiary %{buildroot}%{_datadir}/%{name}/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog doc/*
%{_datadir}/%{name}
%{_bindir}/tdiary-setup

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.3-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug  9 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.3-1m)
- version up 2.3.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc43

* Sun Dec 10 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.0.4-1m)
- [SECURITY] XSS vulnerability <http://www.tdiary.org/20061210.html>
- version up 2.0.4

* Sun Nov 26 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.0.3-1m)
- [SECURITY] XSS vulnerability <http://www.tdiary.org/20061126.html>
- version up 2.0.3

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.2-1m)
- up to 2.0.2
- [SECURITY] CSRF vulnerability <http://www.tdiary.org/20050720.html>

* Thu Mar 31 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.1-1m)
- version up

* Sat Jul  3 2004 kourin <kourin@fh.freeserve.ne.jp>
- (2.0.0-1m)
- version up

* Wed May  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.7-1m)
- version up

* Sun Oct 26 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (1.5.5-2m)
- refine tdiary-setup

* Tue Aug  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.5-1m)

* Thu May 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.4-1m)

* Mon Feb 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.5.3-1m)
- ver up 
- use 'full' tarball (because nonfree themes were separated from full tarball)

* Fri Jan 31 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.2-5m)
- rewrite tdiary-setup(default setting,symlink,plugin)

* Wed Jan 9 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.5.2-4m)
- rewrite tdiary-setup by yamk 

* Sun Dec 29 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.5.2-3m)
- don't use 'full' tarball

* Sun Dec 29 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.5.2-2m)
- rewrite tdiary-setup by kita 

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.2-1m)
- use 'full' tarball

* Wed Nov 20 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.5.1-1.020021120m)
- cvs version

* Wed Nov 20 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.5.1-1m)
- version up

* Tue Nov 12 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.5-0.2m)
- rewrite tdiary-setup

* Mon Nov 11 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.5-0.1m)
- new package
