%global momorel 1

Name:		libvirt-snmp
Version:	0.0.3
Release:	%{momorel}m%{?dist}
Summary:	SNMP functionality for libvirt

Group:		Development/Libraries
License:	GPLv2+
URL:		http://libvirt.org
Source0:	http://www.libvirt.org/sources/snmp/libvirt-snmp-%{version}.tar.gz
NoSource:	0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires: net-snmp-perl net-snmp net-snmp-utils net-snmp-devel libvirt-devel

%description
Provides a way to control libvirt through SNMP protocol.

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/libvirtMib_subagent
%{_datadir}/snmp/mibs/LIBVIRT-MIB.txt
%doc README NEWS ChangeLog AUTHORS
%{_mandir}/man1/libvirtMib_subagent.1*


%changelog
* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.3-1m)
- update 0.0.3

* Sun Oct 30 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.2-1m)
- import from Fedora

* Mon Sep 19 2011 Adam Jackson <ajax@redhat.com> 0.0.2-2
- Rebuild for new snmp lib sonames

* Wed Mar 23 2011 Michal Privoznik <mprivozn@redhat.com> 0.0.2-1
- add SNMP trap/notification support

* Fri Mar 11 2011 Michal Privoznik <mprivozn@redhat.com> 0.0.1-3
- remove LIBVIRT-MIB.txt from doc

* Wed Mar  9 2011 Michal Privoznik <mprivozn@redhat.com> 0.0.1-2
- resolve licensing conflicts
- add unified header to sources

* Thu Feb  2 2011 Michal Privoznik <mprivozn@redhat.com> 0.0.1-1
- initial revision
