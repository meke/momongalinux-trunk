%global momorel 1

%global pycups_version 1.9.62

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

Summary: A printer administration tool
Name: system-config-printer
Version: 1.3.11
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://cyberelk.net/tim/software/system-config-printer/
Group: System Environment/Base
Source0: http://cyberelk.net/tim/data/%{name}/1.3/%{name}-%{version}.tar.xz
NoSource: 0
Patch1: system-config-printer-no-applet-in-gnome.patch
Patch2: system-config-printer-FirewallD.patch

BuildRequires: cups-devel >= 1.6.1
BuildRequires: python-devel >= 2.7
BuildRequires: libsmbclient-devel >= 3.2
BuildRequires: desktop-file-utils >= 0.2.92
BuildRequires: systemd-devel >= 187
BuildRequires: gettext-devel
BuildRequires: intltool
BuildRequires: xmlto
BuildRequires: epydoc
BuildRequires: systemd-units
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: pygtk2 >= 2.4.0, pygtk2-libglade
#Requires: pygobject2
Requires: pygobject
Requires: usermode >= 1.94
Requires: desktop-file-utils >= 0.2.92
Requires: dbus-x11
Requires: dbus-python
Requires: system-config-printer-libs = %{version}-%{release}
Requires: gnome-icon-theme
Requires: desktop-notification-daemon
Requires: notify-python
Requires: gnome-python2-gnomekeyring
Requires: python-sexy
Requires: libxml2-python

Obsoletes: system-config-printer-gui <= 0.6.152
Provides: system-config-printer-gui = 0.6.152

Obsoletes: desktop-printing <= 0.20
Provides: desktop-printing = 0.20

%description
system-config-printer is a graphical user interface that allows
the user to configure a CUPS print server.

%package libs
Summary: Libraries and shared code for printer administration tool
Group: System Environment/Base
Requires: python
Requires: foomatic
Provides: pycups = %{pycups_version}
Provides: pysmbc = %{pysmbc_version}

%description libs
The common code used by both the graphical and non-graphical parts of
the configuration tool.

%package udev
Summary: Rules for udev for automatic configuration of USB printers
Group: System Environment/Base
Requires: system-config-printer-libs = %{version}-%{release}
Obsoletes: hal-cups-utils <= 0.6.20
Provides: hal-cups-utils = 0.6.20

%description udev
The udev rules and helper programs for automatically configuring USB
printers.

%prep
%setup -q
%patch1 -p1 -b .no-applet-in-gnome

# FirewallD support
%patch2 -p1 -b .FirewallD

sed -i -e 's/^NotShowIn=KDE$/NotShowIn=KDE;/' print-applet.desktop.in

%build
%configure --with-udev-rules

%install
rm -rf %buildroot
make DESTDIR=%buildroot install \
        udevrulesdir=/lib/udev/rules.d \
        udevhelperdir=/lib/udev

%{__mkdir_p} %buildroot%{_localstatedir}/run/udev-configure-printer
touch %buildroot%{_localstatedir}/run/udev-configure-printer/usb-uris

%find_lang system-config-printer

%clean
rm -rf %buildroot

%files libs -f system-config-printer.lang
%defattr(-,root,root,-)
%doc COPYING
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/com.redhat.NewPrinterNotification.conf
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/com.redhat.PrinterDriversInstaller.conf
%{_datadir}/dbus-1/interfaces/*.xml
%{_datadir}/dbus-1/services/*.service
%{_bindir}/scp-dbus-service
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/asyncconn.py*
%{_datadir}/%{name}/asyncipp.py*
%{_datadir}/%{name}/asyncpk1.py*
%{_datadir}/%{name}/authconn.py*
%{_datadir}/%{name}/config.py*
%{_datadir}/%{name}/cupspk.py*
%{_datadir}/%{name}/debug.py*
%{_datadir}/%{name}/dnssdresolve.py*
%{_datadir}/%{name}/errordialogs.py*
%{_datadir}/%{name}/firewallsettings.py*
%{_datadir}/%{name}/gtkinklevel.py*
%{_datadir}/%{name}/gtk_label_autowrap.py*
%{_datadir}/%{name}/gtkspinner.py*
%{_datadir}/%{name}/gui.py*
%{_datadir}/%{name}/installpackage.py*
%{_datadir}/%{name}/jobviewer.py*
%{_datadir}/%{name}/monitor.py*
%{_datadir}/%{name}/newprinter.py*
%{_datadir}/%{name}/options.py*
%{_datadir}/%{name}/optionwidgets.py*
%{_datadir}/%{name}/PhysicalDevice.py*
%{_datadir}/%{name}/ppdcache.py*
%{_datadir}/%{name}/ppdippstr.py*
%{_datadir}/%{name}/ppdsloader.py*
%{_datadir}/%{name}/printerproperties.py*
%{_datadir}/%{name}/probe_printer.py*
%{_datadir}/%{name}/pysmb.py*
%{_datadir}/%{name}/scp-dbus-service.py*
%{_datadir}/%{name}/smburi.py*
%{_datadir}/%{name}/statereason.py*
%{_datadir}/%{name}/timedops.py*
%dir %{_sysconfdir}/cupshelpers
%config(noreplace) %{_sysconfdir}/cupshelpers/preferreddrivers.xml
%dir %{python_sitelib}/cupshelpers
%{python_sitelib}/cupshelpers/__init__.py*
%{python_sitelib}/cupshelpers/config.py*
%{python_sitelib}/cupshelpers/cupshelpers.py*
%{python_sitelib}/cupshelpers/installdriver.py*
%{python_sitelib}/cupshelpers/openprinting.py*
%{python_sitelib}/cupshelpers/ppds.py*
%{python_sitelib}/cupshelpers/xmldriverprefs.py*
%{python_sitelib}/*.egg-info

%files udev
%defattr(-,root,root,-)
/lib/udev/rules.d/*.rules
/lib/udev/udev-*-printer
%ghost %dir %{_localstatedir}/run/udev-configure-printer
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) %attr(0644,root,root) %{_localstatedir}/run/udev-configure-printer/usb-uris
%{_unitdir}/udev-configure-printer.service

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{_bindir}/%{name}
%{_bindir}/%{name}-applet
%{_datadir}/%{name}/check-device-ids.py*
%{_datadir}/%{name}/HIG.py*
%{_datadir}/%{name}/SearchCriterion.py*
%{_datadir}/%{name}/serversettings.py*
%{_datadir}/%{name}/system-config-printer.py*
%{_datadir}/%{name}/ToolbarSearchEntry.py*
%{_datadir}/%{name}/userdefault.py*
%{_datadir}/%{name}/applet.py*
%{_datadir}/%{name}/troubleshoot
%{_datadir}/%{name}/icons
%dir %{_datadir}/%{name}/xml
%{_datadir}/%{name}/xml/*.rng
%{_datadir}/%{name}/xml/validate.py*
%dir %{_datadir}/%{name}/ui
%{_datadir}/%{name}/ui/*.ui
%{_datadir}/applications/system-config-printer.desktop
%{_sysconfdir}/xdg/autostart/print-applet.desktop
%{_mandir}/man1/*

%post
/bin/rm -f /var/cache/foomatic/foomatic.pickle
exit 0

%post udev
if [ $1 -eq 1 ] ; then
  # Initial installation
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun udev
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable udev-configure-printer.service >/dev/null 2>&1 || :
  /bin/systemctl stop udev-configure-printer.service >/dev/null 2>&1 || :
fi

%postun udev
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart udev-configure-printer.service >/dev/null 2>&1 || :
fi

%changelog
* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.11-1m)
- update to 1.3.11
- rebuild with new cups-1.6.1

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9-1m)
- update 1.3.9

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.7-1m)
- update 1.3.7

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-3m)
- full rebuild for mo7 release

* Wed Aug 18 2010 Mclellan Daniel <daniel.mclellan@gmail.com>
- (1.2.3-2m)
- build fix (add libudev-devel)

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Mon Jun 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-3m)
- build fix (add CFLAGS)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-1m)
- sync with Fedora 11 (1.1.7-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.8-2m)
- rebuild against python-2.6.1-1m

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8
- almost sync with Fedora devel

* Sat Aug 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.82.5-2m)
- remove "vendor redhat" from desktop files

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.82.5-1m)
- sync fedora

* Mon Jun 30 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.5-1
- 0.7.82.5:
  - Fixed traceback in troubleshooter.
  - Prevent hp-info from accessing X display (part of bug #452371).
  - Use gettext instead of rhpl.translate in cupshelpers (bug #452575).
  - Fixed thread safety when fetching PPDs list.

* Fri Jun 20 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.4-1
- Updated pycups to 1.9.40.
- 0.7.82.4.

* Fri Jun  6 2008 Tim Waugh <twaugh@redhat.com>
- Applied patch to fix 'install drivers' dialog (bug #449860).

* Thu Jun  5 2008 Tim Waugh <twaugh@redhat.com>
- Requires notify-python (bug #450139).

* Mon May 12 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.3-2
- Avoid zombie processes in the applet (bug #446055).
- Use dash instead of underscore when replacing disallowed characters in
  queue names (bug #445790).

* Fri May  9 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.3-1
- No longer requires system-install-packages (bug #444645).
- 0.7.82.3:
  - Connect as current user to print the test page (bug #444306).
  - Better authentication dialog (bug #444306).
  - Guard against locale errors (bug #417951).
  - applet: only include Install button if we are able to install packages.
  - troubleshoot: set transient for parent.
  - troubleshoot: made more robust when using subprocess module.
  - troubleshoot: try hp-info for hp:-scheme device URIs.
  - Fixed sr@latin translation.
  - Don't empty the JetDirect hostname text entry widget if it's already
    been pre-filled for a selected device (Ubuntu #220041).
  - Fixed model-pre-selection when changing PPD.
  - Reset 'media' default job option if it becomes invalid (bug #441836).
  - Fix display of Generic PostScript model, which was getting set to the
    empty string.
  - Avoid copied queue having extra job options set.
  - Set 'copy' dialog transient for main window.
  - Better LPD probe error handling (Ubuntu #213624).
  - Set Forward button sensitibity (Ubuntu #211867).
  - Mark bad pt translation fuzzy.
  - applet: prevent duplicate error notifications (bug #442491).
  - applet: don't check for errors when starting.
  - my-default-printer: Prevent traceback (Ubuntu #214579).
  - main app: make the About dialog appear in the right place.

* Fri May  9 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.2-5
- Updated pycups to 1.9.38 for getFile(fd=...).
- Back-ported use of getFile(fd=...) needed for new authconn code
  (bug #443205).

* Tue May  6 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.2-4
- Removed other upstream fixes introduced in 0.7.82.2-2.

* Fri May  2 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.2-3
- Back-ported authconn dialog for better authentication when running as
  a non-root user (bug #444306).
- Don't install consolehelper bits any more as they are no longer needed.

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.63.1-7m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.7.63.1-6m)
- add *egg-info to %files

* Thu Apr 15 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.2-2
- Some fixes from upstream:
  - Mark bad pt translation fuzzy.
  - applet: prevent duplicate error notifications (bug #442491).
  - applet: don't check for errors when starting.
  - my-default-printer: Prevent traceback (Ubuntu #214579).
  - main app: make the About dialog appear in the right place.

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.63.1-5m)
- rebuild against gcc43

* Thu Apr  3 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.2-1
- 0.7.82.2:
  - Various bug fixes.
  - Translation updates.

* Mon Mar 17 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.1-3
- Updated pycups to 1.9.37.
- More fixes from upstream.

* Wed Mar  5 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.1-2
- Updated pycups to 1.9.36.
- Some fixes from upstream.

* Mon Mar  3 2008 Tim Waugh <twaugh@redhat.com> 0.7.82.1-1
- Requires /usr/bin/system-install-packages not pirut (bug #435622).
- 0.7.82.1:
  - More trouble-shooting improvements.
  - applet: notify user about failed jobs (bug #168370).

* Tue Feb 19 2008 Tim Waugh <twaugh@redhat.com> 0.7.82-1
- Updated to pycups-1.9.35.
- 0.7.82:
  - More trouble-shooting improvements.

* Wed Feb 13 2008 Tim Waugh <twaugh@redhat.com> 0.7.81-1
- 0.7.81:
  - Trouble-shooting improvements and other minor fixes.

* Mon Feb 11 2008 Tim Waugh <twaugh@redhat.com> 0.7.80-2
- Rebuild for GCC 4.3.

* Mon Feb  4 2008 Tim Waugh <twaugh@redhat.com> 0.7.80-1
- Updated to pycups-1.9.34.
- 0.7.80:
  - Trouble-shooting support.

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.63.1-4m)
- rebuild against perl-5.10.0-1m

* Fri Jan 25 2008 Tim Waugh <twaugh@redhat.com> 0.7.79-1
- 0.7.79.

* Wed Jan 23 2008 Tim Waugh <twaugh@redhat.com> 0.7.78-5
- Updated to pycups-1.9.33.

* Wed Jan 16 2008 Tim Waugh <twaugh@redhat.com> 0.7.78-4
- Use config-util from new usermode (bug #428406).

* Thu Dec 20 2007 Tim Waugh <twaugh@redhat.com>
- Requires notification-daemon (Ubuntu #176929).
- Requires gnome-python2 for theme support (Ubuntu #176929).
- Requires gnome-icon-theme for printer icon (Ubuntu #176929).

* Mon Dec 17 2007 Tim Waugh <twaugh@redhat.com> 0.7.78-3
- Install Python egg-info file.
- Updated pycups to 1.9.32.

* Tue Nov 27 2007 Tim Waugh <twaugh@redhat.com> 0.7.78-2
- pycups: Applied patch from SVN to allow fetching printer attributes by URI.
- Sync to SVN 1748.

* Thu Nov 22 2007 Tim Waugh <twaugh@redhat.com> 0.7.78-1
- pycups: Fix job-sheets-default attribute.
- Updated pycups to 1.9.31.
- 0.7.78.

* Wed Nov 21 2007 Tim Waugh <twaugh@redhat.com>
- Applied patch to pycups to avoid reading uninitialised
  memory (bug #390431).

* Mon Nov 19 2007 Tim Waugh <twaugh@redhat.com>
- Updated pycups to 1.9.30.

* Tue Oct 30 2007 Tim Waugh <twaugh@redhat.com> 0.7.77-1
- 0.7.77:
  - Tooltips for the button bar buttons (bug #335601).

* Mon Oct 15 2007 Tim Waugh <twaugh@redhat.com> 0.7.76-1
- 0.7.76.

* Thu Oct  4 2007 Tim Waugh <twaugh@redhat.com> 0.7.75-1
- 0.7.75.

* Wed Oct  3 2007 Tim Waugh <twaugh@redhat.com>
- No need to run update-desktop-database because there are no MimeKey
  lines in the desktop files.
- Consistent macro style.

* Tue Oct  2 2007 Tim Waugh <twaugh@redhat.com> 0.7.74.4-1
- Changed PreReq to Requires.
- Mark console.apps file as a config file.
- Mark pam file as a config file (not replaceable).
- No need to ship empty NEWS file.
- Give cupsd.py executable permissions to satisfy rpmlint.
- Provides system-config-printer-gui.
- Mark D-Bus configuration file as a config file.
- Fixed libs summary.
- Better buildroot tag.
- Better defattr.
- Preserve timestamps on explicitly install files.
- Make example pycups program non-executable.
- 0.7.74.4:
  - Updated translations.
  - Several small bugs fixed.

* Thu Sep 27 2007 Tim Waugh <twaugh@redhat.com> 0.7.74.3-1
- 0.7.74.3:
  - Updated translations.
  - Other small bug fixes.

* Tue Sep 25 2007 Tim Waugh <twaugh@redhat.com> 0.7.74.2-3
- Pull in SVN patch from stable branch for foomatic recommended
  drivers (bug #292021).

* Fri Sep 21 2007 Tim Waugh <twaugh@redhat.com> 0.7.74.2-2
- Pull in SVN patch from stable branch for 'Allow printing from
  the Internet' check-box (bug #221003).

* Wed Sep 19 2007 Tim Waugh <twaugh@redhat.com> 0.7.74.2-1
- Updated pycups to 1.9.27.
- 0.7.74.2:
  - When a class is removed on the server, remove it from the UI.
  - When deleting a printer, select the default printer again.
  - Select newly-copied printer.
  - Updated translation (fi).
  - Better --help message.
  - Use strcoll to sort manufacturer names.
  - Avoid duplicate 'recommended' marks.
  - Remove duplicate device URIs.
  - Handle IPP_TAG_NOVALUE attributes (for CUPS 1.3.x).

* Wed Sep 12 2007 Tim Waugh <twaugh@redhat.com>
- Updated pycups to 1.9.26.
- Build requires epydoc.  Ship HTML documentation.

* Fri Sep  7 2007 Tim Waugh <twaugh@redhat.com> 0.7.74.1-1
- 0.7.74.1:
  - Updated Polish translation (bug #263001).
  - Don't select the default printer after changes to another printer have
    been made.
  - Always construct URI from input fields when changing device (bug #281551).
  - Avoid busy-cursor traceback when window is not yet displayed.

* Thu Aug 30 2007 Tim Waugh <twaugh@redhat.com> 0.7.74-1
- Updated pycups to 1.9.25.
- 0.7.74:
  - Fixed New Class dialog.
  - UI fixes.

* Sat Aug 25 2007 Tim Waugh <twaugh@redhat.com>
- More specific license tag.

* Fri Aug 24 2007 Tim Waugh <twaugh@redhat.com> 0.7.73-1
- 0.7.73.

* Fri Aug 10 2007 Tim Waugh <twaugh@redhat.com> 0.7.72-2
- Ship the applet's desktop file.

* Wed Aug  8 2007 Tim Waugh <twaugh@redhat.com> 0.7.72-1
- 0.7.72:
  - Fixed my-default-printer traceback.
  - Improvements to New Printer wizard (Till Kamppeter).

* Fri Aug  3 2007 Tim Waugh <twaugh@redhat.com> 0.7.71-1
- 0.7.71:
  - Don't discard make/model-matched devices when there are ID-matched
    devices (Till Kamppeter).
  - Fixed fallback if no text-only driver is available (Till Kamppeter).
  - Initialise the make/model list when an ID match failed (Till Kamppeter).
  - Better error-handling in default-print application (Ubuntu #129901).
  - UI tweak in admin tool (Ubuntu #128263).
  - Handle socket: URIs (Ubuntu #127074).

* Mon Jul 23 2007 Tim Waugh <twaugh@redhat.com> 0.7.70-2
- Obsoletes/provides desktop-printing.

* Mon Jul  9 2007 Tim Waugh <twaugh@redhat.com> 0.7.70-1
- Requires pirut for system-install-packages.
- 0.7.70:
  - Increased GetReady->NewPrinter timeout.
  - More binary names mapped to package named.
  - Run system-install-packages to install missing drivers (bug #246726).
  - Less debug output.
  - Desktop file fixes for KDE (bug #247299).

* Thu Jun 28 2007 Tim Waugh <twaugh@redhat.com> 0.7.69-1
- No longer requires PyXML (bug #233146).
- Moved applet to main package.
- 0.7.69:
  - Use HardwareSettings category for my-default-printer desktop
    file (bug #244935).
  - Removed unused code.
  - Filter PPDs by natural language (bug #244173).

* Mon Jun 25 2007 Tim Waugh <twaugh@redhat.com>
- The applet requires dbus-x11 (Ubuntu #119570).

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.63.1-3m)
- remove /usr/share/pixmaps/cupsprinter.png (SOURCE4)
- conflict with cups-1.2.11-1m

* Fri Jun 15 2007 Tim Waugh <twaugh@redhat.com> 0.7.68-1
- 0.7.68:
  - Fixed the notification bubbles.
  - Ship my-default-printer utility.

* Fri Jun  8 2007 Tim Waugh <twaugh@redhat.com> 0.7.67-1
- Don't put TrayIcon or SystemSetup categories in the desktop file.
- Updated pycups to 1.9.24.
- 0.7.67:
  - Fixed desktop files to have capital letters at the start of each
    word in the Name field (bug #242859).
  - Fixed crash when saving unapplied changes.
  - Fixed Device ID parser to always split the CMD field at commas.
  - New PPDs class means we no longer parse the foomatic XML database.

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.63.1-2m)
- modify %%files to avoid conflicting

* Sun Jun  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.63.1-1m)
- update to 0.7.63.1

* Wed May 30 2007 Tim Waugh <twaugh@redhat.com> 0.7.66-1
- 0.7.66:
  - Allow job-hold-until to be set (bug #239776).
  - Implement new printer notifications.

* Tue May 22 2007 Tim Waugh <twaugh@redhat.com> 0.7.65-1
- Build requires xmlto.
- Updated to pycups-1.9.22.
- 0.7.65:
  - Use urllib for quoting/unquoting (Val Henson, Ubuntu #105022).
  - Added kn translation.
  - Better permissions on non-scripts.
  - Added man pages.
  - Applet: status feedback.
  - Applet: fixed relative time descriptions.
  - Applet: limit refresh frequency.

* Mon Apr 16 2007 Tim Waugh <twaugh@redhat.com> 0.7.63.1-1
- 0.7.63.1:
  - Small applet fixes.

* Thu Apr  5 2007 Tim Waugh <twaugh@redhat.com> 0.7.63-1
- 0.7.63:
  - Translation updates.
  - Checked in missing file.

* Wed Apr  4 2007 Tim Waugh <twaugh@redhat.com>
- Updated to pycups-1.9.20 for printer-state-reasons fix.

* Mon Apr  2 2007 Tim Waugh <twaugh@redhat.com> 0.7.62-1
- 0.7.62:
  - Use standard icon for admin tool desktop file.
  - Fixed env path in Python scripts.
  - Applet: stop running when the session ends.
  - Prevent a traceback in the SMB browser (bug #225351).
  - 'Manage print jobs' desktop file.

* Fri Mar 30 2007 Tim Waugh <twaugh@redhat.com> 0.7.61-1
- 0.7.61:
  - Fixed retrieval of SMB authentication details (bug #203539).

* Tue Mar 27 2007 Tim Waugh <twaugh@redhat.com> 0.7.60-1
- Updated to pycups-1.9.19.
- Avoid %%makeinstall.
- 0.7.60:
  - Handle reconnection failure.
  - New applet name.

* Mon Mar 26 2007 Tim Waugh <twaugh@redhat.com> 0.7.59-1
- 0.7.59:
  - Fixed a translatable string.
  - Set a window icon (bug #233899).
  - Handle failure to start the D-Bus service.
  - Ellipsize the document and printer named (bug #233899).
  - Removed the status bar (bug #233899).
  - Added an icon pop-up menu for 'Hide' (bug #233899).

* Wed Mar 21 2007 Tim Waugh <twaugh@redhat.com> 0.7.57-1
- Added URL tag.
- 0.7.57:
  - Prevent traceback when removing temporary file (Ubuntu #92914).
  - Added print applet.

* Sun Mar 18 2007 Tim Waugh <twaugh@redhat.com> 0.7.56-2
- Updated to pycups-1.9.18.

* Fri Mar 16 2007 Tim Waugh <twaugh@redhat.com> 0.7.56-1
- 0.7.56:
  - Parse Boolean strings correctly in job options.
  - Small command-set list/string fix (bug #230665).
  - Handle hostname look-up failures.
  - Updated filter-to-driver map.
  - Don't parse printers.conf (bug #231826).

* Tue Feb 27 2007 Tim Waugh <twaugh@redhat.com> 0.7.55-1
- 0.7.55:
  - Use converted value for job option widgets.

* Tue Feb 27 2007 Tim Waugh <twaugh@redhat.com> 0.7.54-1
- 0.7.54:
  - Removed debugging code.

* Tue Feb 27 2007 Tim Waugh <twaugh@redhat.com> 0.7.53-1
- No longer requires rhpl (since 0.7.53).
- 0.7.53:
  - Use gettext instead of rhpl.translate.
  - Better layout for PPD options.
  - Added scrollbars to main printer list (bug #229453).
  - Set maximum width of default printer label (bug #229453).
  - Handle applying changes correctly when switching to another
    printer (bug #229378).
  - Don't crash when failing to fetch the PPD (bug #229406).
  - Make the text entry boxes sensitive but not editable for remote
    printers (bug #229381).
  - Better job options screen layout (bug #222272).

* Tue Feb 13 2007 Tim Waugh <twaugh@redhat.com> 0.7.52-1
- 0.7.52:
  - Sort models using cups.modelSort before scanning for a close
    match (bug #228505).
  - Fixed matching logic (bug #228505).

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-liunx.org>
- (0.7.39-5m)
- BuildRequires: cups-devel >= 1.2

* Fri Feb  9 2007 Tim Waugh <twaugh@redhat.com> 0.7.51-1
- 0.7.51:
  - Prevent display glitch in job options list when clicking on a printer
    repeatedly.
  - List conflicting PPD options, and embolden the relevant tab
    labels (bug #226368).
  - Fixed typo in 'set default' handling that caused a traceback (bug #227936).
  - Handle interactive search a little better (bug #227935).

* Wed Feb  7 2007 Tim Waugh <twaugh@redhat.com> 0.7.50-1
- 0.7.50:
  - Fixed hex digits list (bug #223770).
  - Added bs translation.
  - Don't put the ellipsis in the real device URI (bug #227643).
  - Don't check for existing drivers for complex command lines (bug #225104).
  - Allow floating point job options (bug #224651).
  - Prevent shared/published confusion (bug #225081).
  - Fixed PPD page size setting.
  - Avoid os.remove exception (bug #226703).
  - Handle unknown job options (bug #225538).

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.39-4m)
- delete pyc pyo

* Tue Jan 16 2007 Tim Waugh <twaugh@redhat.com> 0.7.49-1
- 0.7.49:
  - Fixed a traceback in the driver check code.
  - Fixed a typo in the conflicts message.
  - Handle InputSlot/ManualFeed specially because libcups does (bug #222490).

* Mon Jan 15 2007 Tim Waugh <twaugh@redhat.com> 0.7.48-1
- 0.7.48:
  - Updated translations.

* Fri Jan 12 2007 Tim Waugh <twaugh@redhat.com> 0.7.47-1
- 0.7.47:
  - Fixed minor text bugs (bug #177433).
  - Handle shell builtins in the driver check (bug #222413).

* Mon Jan  8 2007 Tim Waugh <twaugh@redhat.com> 0.7.46-1
- 0.7.46:
  - Fixed page size problem (bug #221702).
  - Added 'ro' to ALL_LINGUAS.

* Wed Jan  3 2007 Tim Waugh <twaugh@redhat.com> 0.7.45-1
- Updated to pycups-1.9.17.
- 0.7.45:
  - Fixed traceback in driver check.

* Tue Jan  2 2007 Tim Waugh <twaugh@redhat.com> 0.7.44-1
- 0.7.44:
  - Fixed traceback in error display (bug #220136).
  - Preserve case in model string when dumping debug output.

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.39-3m)
- rebuild against python-2.5

* Thu Dec 21 2006 Tim Waugh <twaugh@redhat.com> 0.7.43-1
- 0.7.43:
  - Don't check against IEEE 1284 DES field at all.
  - Merged device matching code (bug #219518).
  - Catch non-fatal errors when auto-matching device.
  - Fixed driver checking bug involving pipelines (bug #220347).
  - Show PPD errors (bug #220136).

* Mon Dec 11 2006 Tim Waugh <twaugh@redhat.com> 0.7.42-1
- 0.7.42:
  - Fixed typo in command set matching code.
  - Case-insensitive matching when Device ID not known to database.

* Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 0.7.41-2
- build against python 2.5

* Thu Dec  7 2006 Tim Waugh <twaugh@redhat.com> 0.7.41-1
- Updated pycups to 1.9.16.
- 0.7.41:
  - Reconnect smoothly after uploading new configuration.
  - Update lpoptions when setting default printer if it conflicts with
    the new setting (bug #217395).
  - Fixed typo in show_HTTP_Error (bug #217537).
  - Don't pre-select make and model when not discoverable for chosen
    device (bug #217518).
  - Set Forward button sensitive on Device screen in new-printer
    dialog (bug #217515).
  - Keep Server Settings selected after applying changes if it was selected
    before.
  - Set Connecting dialog transient for main window.
  - Center Connecting dialog on parent.
  - Optional 'reason' argument for cupshelpers.Printer.setEnabled.
  - Describe devices that have no optional parameters.

* Thu Nov 30 2006 Tim Waugh <twaugh@redhat.com>
- Provide pycups feature.

* Sat Nov 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.39-1m)
- update 0.7.39

* Tue Nov 21 2006 Tim Waugh <twaugh@redhat.com> 0.7.40-1
- 0.7.40:
  - Removed username:password from hint string because we add that in
    afterwards.
  - Don't set button widths in create-printer dialog (bug #217025).

* Tue Nov 21 2006 Tim Waugh <twaugh@redhat.com> 0.7.39-1
- 0.7.39:
  - Busy cursor while loading foomatic and PPD list (bug #215527).
  - Make PPD NickName selectable.
  - Added SMB hint label on device screen (bug #212759).

* Tue Nov 14 2006 Tim Waugh <twaugh@redhat.com> 0.7.38-1
- Updated pycups to 1.9.15.
- 0.7.38:
  - Fixed a bug in the 'ieee1284'/'ppd-device-id' parsing code.

* Mon Nov 13 2006 Tim Waugh <twaugh@redhat.com> 0.7.37-1
- 0.7.37:
  - Allow cancellation of test pages (bug #215054).

* Fri Nov 10 2006 Tim Waugh <twaugh@redhat.com> 0.7.36-1
- 0.7.36:
  - Match against commandset (bug #214181).
  - Parse 'ieee1284' foomatic autodetect entries (bug #214761).
  - Don't remove foomatic PPDs from the list (bug #197331).

* Tue Nov  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.32-3m)
- modify Patch1 (remove vender, add category)

* Tue Nov  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.32-2m)
- add Obsolete: system-config-printer-gui

* Tue Nov  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.32-1m)
- update to 0.7.32
- delete gui package

* Tue Nov  7 2006 Tim Waugh <twaugh@redhat.com> 0.7.35-1
- 0.7.35.

* Thu Nov  2 2006 Tim Waugh <twaugh@redhat.com>
- Updated to pycups-1.9.14 (bug #213136).

* Tue Oct 31 2006 Tim Waugh <twaugh@redhat.com>
- Update desktop database (bug #213249).

* Tue Oct 24 2006 Tim Waugh <twaugh@redhat.com>
- Build requires Python 2.4.

* Mon Oct  2 2006 Tim Waugh <twaugh@redhat.com> 0.7.32-1
- Updated to pycups-1.9.13 for HTTP_FORBIDDEN.
- 0.7.32:
  - Handle HTTP errors during connection (bug #208824).
  - Updated translations (bug #208873).

* Fri Sep 29 2006 Tim Waugh <twaugh@redhat.com> 0.7.31-1
- 0.7.31:
  - Select recommended driver automatically (bug #208606).
  - Better visibility of driver list (bug #203907).

* Fri Sep 29 2006 Tim Waugh <twaugh@redhat.com> 0.7.30-1
- 0.7.30:
  - Translations fixed properly (bug #206622).
  - Button widths corrected (bug #208556).

* Tue Sep 26 2006 Tim Waugh <twaugh@redhat.com> 0.7.28-1
- 0.7.28.  Translations fixed (bug #206622).

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.151.7-4m)
- remove category X-Red-Hat-Base SystemSetup Application

* Tue Sep 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.151.7-3m)
- enable fix-which.patch

* Tue Sep 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.151.7-2m)
- require gnome-python-extras-gtkhtml

* Wed Aug 30 2006 Tim Waugh <twaugh@redhat.com> 0.7.27-1
- Build requires intltool.
- 0.7.27.

* Wed Aug 23 2006 Tim Waugh <twaugh@redhat.com> 0.7.26-1
- 0.7.26.  Fixes bug # 203149.

* Mon Aug 14 2006 Florian Festi <ffesti@redhat.com> 0.7.25-1
- 0.7.25. (bug #202060)

* Fri Aug 11 2006 Tim Waugh <twaugh@redhat.com>
- Fixed description (bug #202189).

* Thu Aug  3 2006 Tim Waugh <twaugh@redhat.com> 0.7.24-1
- 0.7.24.

* Mon Jul 24 2006 Tim Waugh <twaugh@redhat.com> 0.7.23-1
- 0.7.23.  Fixes bug #197866.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.7.22-1.1
- rebuild

* Fri Jul  7 2006 Tim Waugh <twaugh@redhat.com> 0.7.22-1
- 0.7.22.

* Wed Jul  5 2006 Tim Waugh <twaugh@redhat.com> 0.7.21-1
- Updated to pycups-1.9.12.
- 0.7.21.

* Mon Jul  3 2006 Tim Waugh <twaugh@redhat.com> 0.7.20-1
- 0.7.20.

* Fri Jun 30 2006 Tim Waugh <twaugh@redhat.com> 0.7.19-1
- 0.7.19.
- Remove foomatic pickle file post-install.

* Tue Jun 27 2006 Tim Waugh <twaugh@redhat.com> 0.7.18-1
- 0.7.18.
- Ship translations with libs subpackage.

* Fri Jun 23 2006 Tim Waugh <twaugh@redhat.com> 0.7.17-1
- 0.7.17.

* Fri Jun 23 2006 Tim Waugh <twaugh@redhat.com> 0.7.16-1
- 0.7.16, now with SMB browser.

* Wed Jun 22 2006 Tim Waugh <twaugh@redhat.com> 0.7.15-1
- 0.7.15.
- Build requires gettext-devel.
- Ship translations.

* Tue Jun 20 2006 Tim Waugh <twaugh@redhat.com> 0.7.14-1
- 0.7.14.

* Mon Jun 19 2006 Tim Waugh <twaugh@redhat.com> 0.7.13-1
- 0.7.13.

* Fri Jun  9 2006 Tim Waugh <twaugh@redhat.com> 0.7.12-1
- 0.7.12.

* Tue Jun  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.151.7-1m)
- update 0.6.151.7

* Thu Jun  1 2006 Tim Waugh <twaugh@redhat.com> 0.7.11-3
- Fix libs dependency.

* Thu Jun  1 2006 Tim Waugh <twaugh@redhat.com> 0.7.11-2
- Moved the gtk_html2pango module to the libs package (needed by
  foomatic.py).

* Wed May 31 2006 Tim Waugh <twaugh@redhat.com> 0.7.11-1
- Split out system-config-printer-libs.
- Updated to system-config-printer-0.7.11.

* Sat May 27 2006 Tim Waugh <twaugh@redhat.com> 0.7.10-2
- Requires gobject2 (bug #192764).

* Fri May 26 2006 Tim Waugh <twaugh@redhat.com> 0.7.10-1
- Require foomatic (bug #192764).
- Updated to system-config-printer-0.7.10.

* Thu May 25 2006 Tim Waugh <twaugh@redhat.com> 0.7.9-1
- Updated to pycups-1.9.11.
- Updated to system-config-printer-0.7.9.

* Mon May 15 2006 Tim Waugh <twaugh@redhat.com> 0.7.8-1
- Updated to pycups-1.9.10.
- Updated to system-config-printer-0.7.8.

* Fri May  5 2006 Tim Waugh <twaugh@redhat.com>
- Fix pycups segfault.

* Fri May  5 2006 Tim Waugh <twaugh@redhat.com> 0.7.7-2
- Ship PAM and userhelper files.
- Requires usermode.
- Added missing options.py file.
- Fix getClasses() in pycups.

* Thu May  4 2006 Tim Waugh <twaugh@redhat.com> 0.7.7-1
- Updated to system-config-printer-0.7.7.
- Updated to pycups-1.9.9.
- Desktop file.
- Requires PyXML.

* Fri Apr 28 2006 Tim Waugh <twaugh@redhat.com>
- Make it actually run.

* Fri Apr 21 2006 Tim Waugh <twaugh@redhat.com>
- Build requires CUPS 1.2.

* Thu Apr 20 2006 Tim Waugh <twaugh@redhat.com> 0.7.5-1
- Updated to pycups-1.9.8.  No longer need threads patch.
- Updated to system-config-printer-0.7.5.

* Sat Apr 15 2006 Tim Waugh <twaugh@redhat.com>
- Updated to pycups-1.9.7.

* Thu Apr 13 2006 Tim Waugh <twaugh@redhat.com> 0.7.4-2
- Obsoletes: system-config-printer-gui <= 0.6.152

* Wed Apr 12 2006 Tim Waugh <twaugh@redhat.com> 0.7.4-1
- Updated to system-config-printer-0.7.4.

* Fri Apr  7 2006 Tim Waugh <twaugh@redhat.com> 0.7.3-1
- Added threads patch from pycups CVS.
- Updated to system-config-printer-0.7.3.

* Tue Apr  4 2006 Tim Waugh <twaugh@redhat.com>
- Updated to pycups-1.9.6.

* Fri Mar 24 2006 Tim Waugh <twaugh@redhat.com>
- Updated to pycups-1.9.5.

* Fri Mar 17 2006 Tim Waugh <twaugh@redhat.com>
- Package the actual system-config-printer command.

* Thu Mar 16 2006 Tim Waugh <twaugh@redhat.com> 0.7.1-1
- Include s-c-printer tarball.
- Updated to pycups-1.9.4.

* Wed Mar 15 2006 Tim Waugh <twaugh@redhat.com> 0.7.0-1
- Initial spec file.

* Sun Jan 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.147-1m)
- update 0.6.147

* Fri Nov 26 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.6.141-5m)
- rebuild against gnome-python-2.12.1-1m

* Tue Nov  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.141-4m)
- Requires: rhpl >= 0.151-4m, PyXML >= 0.8.3-3m for %%post

* Tue Nov  1 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.6.141-3m)
- change PreReq: python-abi to python(abi)

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.141-2m)
- rebuild against python-2.4.2

* Sun Aug 28 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.141-1m)
- update 0.6.141
- remake fix-which.patch

* Sun Apr  3 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6.116.1.1-2m)
- which needs to initialize.

* Sun Mar  6 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6.116.1.1-1m)
- sync with fc3 updates.

* Mon Feb 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.116-3m)
- desktop-file-install --vendor 'redhat' -> 'system'

* Sun Feb  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.116-2m)
- add System to Categories of desktop file for KDE

* Sun Feb  6 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6.116-1m)
- import from FC3.

* Tue Oct 19 2004 Tim Waugh <twaugh@redhat.com> 0.6.116-1
- 0.6.116:
  - Use the new file chooser dialog (bug #135746).
  - Fixed SNMP autodetection (bug #136294).

* Tue Oct 12 2004 Tim Waugh <twaugh@redhat.com> 0.6.115-1
- 0.6.115:
  - SMB URI fix for domain\user authentication (bug #132742).
  - Applied patch to fix printconf.pot from Ronny Buchmann (bug #135195).

* Thu Sep 30 2004 Tim Waugh <twaugh@redhat.com> 0.6.114-1
- 0.6.114:
  - Allow control codes in text-only printing (bug #124250).
  - More robustness in validating SMB queue information (bug #127348).
  - Include more translations (bug #133721).

* Tue Sep  7 2004 Tim Waugh <twaugh@redhat.com> 0.6.113-1
- 0.6.113:
  - Avoid depcrecated interface (bug #130811).

* Mon Sep  6 2004 Tim Waugh <twaugh@redhat.com> 0.6.112-1
- 0.6.112:
  - Revert change from bug #130811, to avoid traceback.

* Fri Sep  3 2004 Tim Waugh <twaugh@redhat.com> 0.6.111-1
- 0.6.111:
  - More robust printer make/model matching for remove-local (bug #131434).

* Tue Aug 31 2004 Tim Waugh <twaugh@redhat.com> 0.6.110-1
- 0.6.110:
  - Avoid deprecated interface (bug #130811).

* Fri Aug 20 2004 Tim Waugh <twaugh@redhat.com> 0.6.109-1
- 0.6.109:
  - Fixed IEEE 1284 ID reporting (missing newlines).
  - Fixed socket: URI parsing in cups_import (bug #130362).

* Wed Aug 11 2004 Tim Waugh <twaugh@redhat.com> 0.6.108-1
- 0.6.108:
  - Fixed text-only print queues (bug #124250).

* Thu Aug  5 2004 Tim Waugh <twaugh@redhat.com> 0.6.107-1
- 0.6.107:
  - Much more tolerance in printer ID matching.

* Wed Aug  4 2004 Tim Waugh <twaugh@redhat.com> 0.6.106-1
- 0.6.106:
  - Better unmatched IEEE 1284 ID reporting.

* Fri Jul 30 2004 Tim Waugh <twaugh@redhat.com> 0.6.105-1
- 0.6.105:
  - Redirect error output from ptal-devid.
  - Implement match-driver (bug #128789).

* Wed Jul 14 2004 Tim Waugh <twaugh@redhat.com> 0.6.104-1
- 0.6.104:
  - Fixed chkconfig invocation.
  - New po files.
  - Use GTK methods that are not deprecated.
  - Print a message about unmatched USB model/mfr strings.

* Tue Jul 13 2004 Tim Waugh <twaugh@redhat.com> 0.6.103-1
- Use %%{_libdir} (bug #127737).
- 0.6.103:
  - Use LIBDIR (bug #127737).

* Mon Jul  5 2004 Tim Waugh <twaugh@redhat.com> 0.6.102-1
- 0.6.102:
  - Run chkconfig to enable the spooler service (bug #126005).
  - Don't ship printcap.local (bug #127245).

* Thu Jun  3 2004 Tim Waugh <twaugh@redhat.com> 0.6.101-1
- 0.6.101:
  - Set LANG=C before running /usr/sbin/alternatives in places not fixed
    last time (bug #124217).

* Tue Jun  1 2004 Tim Waugh <twaugh@redhat.com> 0.6.100-1
- 0.6.100:
  - Set LANG=C before running /usr/sbin/alternatives (bug #124217).

* Tue May 25 2004 Tim Waugh <twaugh@redhat.com> 0.6.99-1
- 0.6.99:
  - Never alter the "Location /" section of cupsd.conf.

* Mon May 24 2004 Tim Waugh <twaugh@redhat.com>
- Require a version of alchemist that has correct python dependencies
  (bug #124013).

* Wed Mar 24 2004 Tim Waugh <twaugh@redhat.com> 0.6.98-1
- Ship /var/cache/alchemist/printconf.* directories so that SELinux
  contexts get set correctly.
- 0.6.98:
  - Icons for whether a queue is shared.  This is part of bug #116998.

* Mon Mar 22 2004 Tim Waugh <twaugh@redhat.com> 0.6.97-1
- 0.6.97:
  - More renaming bits (bug #118842).
  - More translations (bug #118792).

* Wed Mar 10 2004 Tim Waugh <twaugh@redhat.com> 0.6.96-1
- 0.6.96:
  - Minor fix to browsing logic.
  - Sharing button in the edit queue dialog (part of bug #116998).

* Mon Mar  8 2004 Tim Waugh <twaugh@redhat.com> 0.6.95-1
- 0.6.95:
  - Use rename() instead of unlink(), to avoid partial configuration files
    being read.
  - Use 10 second delay instead of 5 seconds for test page (bug #115585).
  - Set default for sharing_globals/browsing to TRUE.

* Fri Mar  5 2004 Tim Waugh <twaugh@redhat.com>
- Require python-abi = %%{pyver}.

* Thu Mar  4 2004 Tim Waugh <twaugh@redhat.com> 0.6.94-1
- 0.6.94:
  - Cope with there being no default queue at all (bug #117060).

* Fri Feb  6 2004 Tim Waugh <twaugh@redhat.com> 0.6.93-1
- 0.6.93:
  - Enable F12 in the text interface (bug #113732).
  - Fix the rest of bug #109942, and bug #115062.

* Thu Feb  5 2004 Tim Waugh <twaugh@redhat.com>
- Make gui package obsolete the correct thing (bug #114981).

* Tue Feb  3 2004 Tim Waugh <twaugh@redhat.com> 0.6.92-1
- 0.6.92:
  - Another 'single IP address' bug fix (bug #114414).

* Mon Jan 19 2004 Tim Waugh <twaugh@redhat.com> 0.6.91-1
- 0.6.91:
  - Attempt to fix bug #112835 ('allow from single IP address').
  - Switch to gimp-print-ijs as the preferred variant.

* Fri Jan  2 2004 Tim Waugh <twaugh@redhat.com> 0.6.90-1
- Requires foomatic >= 3.0.0-1, for foomatic-ppdfile.
- 0.6.90:
  - Use foomatic-ppdfile instead of foomatic-datafile, to avoid
    getting errors when using '-t ppd' (bug #112696).

* Fri Dec 19 2003 Tim Waugh <twaugh@redhat.com> 0.6.89-1
- 0.6.89:
  - PPD import support using foomatic-ppdload.

* Tue Dec 16 2003 Tim Waugh <twaugh@redhat.com> 0.6.88-1
- 0.6.88:
  - Fix up problems introduced by renaming.

* Tue Dec 16 2003 Tim Waugh <twaugh@redhat.com> 0.6.87-1
- 0.6.87:
  - More SNMP improvements.
  - Renamed to system-config-printer.

* Tue Dec 16 2003 Tim Waugh <twaugh@redhat.com> 0.6.86-1
- 0.6.86:
  - More SNMP improvements.

* Mon Dec 15 2003 Tim Waugh <twaugh@redhat.com> 0.6.85-1
- 0.6.85:
  - Use the correct SNMP OID.

* Fri Dec 12 2003 Tim Waugh <twaugh@redhat.com> 0.6.84-1
- 0.6.84:
  - Further sharing fixes (bug #109942).
  - Add SNMP make/model detection for JetDirect queues.

* Tue Nov 18 2003 Tim Waugh <twaugh@redhat.com> 0.6.83-1
- 0.6.83:
  - Allow remote queues to be the default (bug #103224).

* Fri Nov 14 2003 Tim Waugh <twaugh@redhat.com>
- Requires PyXML built for Python 2.3.

* Fri Nov 14 2003 Tim Waugh <twaugh@redhat.com> 0.6.82-1
- 0.6.82:
  - Requires gnome-python2-canvas (bug #110116).

* Thu Nov 13 2003 Tim Waugh <twaugh@redhat.com> 0.6.81-1
- 0.6.81:
  - More sharing fixes (bug #109942).

* Thu Nov  6 2003 Tim Waugh <twaugh@redhat.com> 0.6.80-1
- 0.6.80:
  - Build module for Python 2.3.

* Mon Oct 20 2003 Tim Waugh <twaugh@redhat.com> 0.6.79-1
- 0.6.79:
  - Small display bugfix for print.py.
  - Don't adjust LogLevel when printing a test page (bug #107537).

* Thu Oct  9 2003 Tim Waugh <twaugh@redhat.com> 0.6.78-1
- 0.6.78:
  - Run updateconf.py on exit from printconf_tui (bug #106478).
  - Build Croatian translation (bug #106610).

* Tue Oct  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.77-1
- 0.6.77:
  - Fixed traceback involving IPP transport (bug #106284).
  - Avoid smbclient's -N option unless absolutely necessary (bug #106395).

* Tue Sep 30 2003 Tim Waugh <twaugh@redhat.com> 0.6.76-1
- 0.6.76:
  - Fixed driver selection bug (bug #104380).
  - Prevent traceback when the workgroup is not known.
  - Update sensitivity of 'test' menu item when the browsed queues tree
    is collapsed.
  - Fix handling of network interfaces with no NETWORK line in the ifcfg
    file; also prevent explicit localhost sharing from causing an invalid
    configuration file (bug #105402).

* Fri Sep 12 2003 Tim Waugh <twaugh@redhat.com> 0.6.75-1
- 0.6.75:
  - Fix SMB URIs once and for all (bug #104293, bug #104136).

* Wed Sep 10 2003 Tim Waugh <twaugh@redhat.com> 0.6.74-1
- 0.6.74:
  - Avoid smbclient's -P option since its meaning is unpredictable
    (bug #104138).

* Fri Sep  5 2003 Tim Waugh <twaugh@redhat.com>
- Build does not require libglade-devel.

* Fri Aug 29 2003 Tim Waugh <twaugh@redhat.com> 0.6.73-1
- 0.6.73:
  - Try to get SMB URIs right when the password, but not the workgroup,
    is given (bug #80717).

* Fri Aug 29 2003 Tim Waugh <twaugh@redhat.com> 0.6.72-1
- Require cups (bug #85363).
- 0.6.72:
  - Back up cupsd.conf if this is the first time we are changing it
    (bug #97089).

* Mon Aug 18 2003 Tim Waugh <twaugh@redhat.com> 0.6.71-1
- 0.6.71:
  - Fixed translation issue (bug #102355).

* Wed Aug 13 2003 Tim Waugh <twaugh@redhat.com> 0.6.70-1
- 0.6.70:
  - Revert cjktexttops change (bug #100984).

* Wed Jul 30 2003 Tim Waugh <twaugh@redhat.com> 0.6.69-1
- 0.6.69:
  - Fix cjktexttops (bug #100984).
  - Warn about editing cupsd.conf (bug #97032).

* Mon Jul 28 2003 Tim Waugh <twaugh@redhat.com> 0.6.68-1
- 0.6.68:
  - Fix the menu 'quit' to check for changes.

* Wed Jul 23 2003 Tim Waugh <twaugh@redhat.com> 0.6.67-1
- 0.6.67:
  - Fix the dialog app.

* Wed Jul 23 2003 Tim Waugh <twaugh@redhat.com> 0.6.66-1
- Mark some config files as such (bug #99009).
- 0.6.66:
  - Include a simple print dialog app.

* Mon Jul  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.65-1
- 0.6.65:
  - Include all translations (bug #98225).

* Tue Jun 24 2003 Tim Waugh <twaugh@redhat.com> 0.6.64-1
- 0.6.64:
  - Proper fix for bug #97820.

* Mon Jun 23 2003 Tim Waugh <twaugh@redhat.com> 0.6.63-1
- 0.6.63:
  - Remove quotes from values taken from sysconfig files (bug #97820).

* Fri Jun  6 2003 Tim Waugh <twaugh@redhat.com> 0.6.62-1
- 0.6.62:
  - Prevent duplicate Listen lines (bug #88303).

* Tue Jun  3 2003 Tim Waugh <twaugh@redhat.com> 0.6.61-1
- 0.6.61:
  - Fixed another traceback in the backend (bug #88291).

* Mon Jun  2 2003 Tim Waugh <twaugh@redhat.com> 0.6.60-1
- Removed LPRng conflicts tag.
- 0.6.60: no code change.

* Wed May 28 2003 Tim Waugh <twaugh@redhat.com> 0.6.59-1
- PreReq rhpl instead of just Requires rhpl, hoping to fix bug #91844.
- 0.6.59: no code change.

* Tue May 27 2003 Tim Waugh <twaugh@redhat.com> 0.6.58-1
- 0.6.58:
  - Test page diagnostics.

* Fri May 23 2003 Tim Waugh <twaugh@redhat.com> 0.6.57-1
- Ship mime info file.
- 0.6.57:
  - Always write PageSize option (bug #91088).
  - Allow queue options to contain decimal points (bug #90413).

* Fri May 16 2003 Tim Waugh <twaugh@redhat.com> 0.6.56-1
- 0.6.56:
  - More test pages (bug #90912).
  - Better printer model sorting.

* Wed May 14 2003 Tim Waugh <twaugh@redhat.com> 0.6.55-1
- 0.6.55:
  - Fix bug preventing the workgroup from displaying in the password
    dialog (bug #90831).
  - Implement --add-with-url.

* Mon May 12 2003 Tim Waugh <twaugh@redhat.com> 0.6.54-1
- 0.6.54:
  - Set margins and text spacing such that a printed page is 80 by 66
    (bug #90413).

* Fri Apr 25 2003 Tim Waugh <twaugh@redhat.com> 0.6.53-1
- 0.6.53:
  - Fix 'quad' traceback (bug #88291).
  - Fix network interface sniffer (bug #88303).
  - Don't require newer Omni than was shipped in Shrike.

* Mon Apr  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.52-1
- 0.6.52:
  - Handle noninteger coordinates (from hpijs).
  - Re-label the druid's final 'Apply' button 'Finish' (bug #71725).
  - Grok foomatic 3.x autodetect/general markup.
  - Scroll to automatically-selected and custom devices.
  - Fix backslash replacement in SMB screen (bug #88021).

* Wed Apr  2 2003 Tim Waugh <twaugh@redhat.com> 0.6.51-1
- Run printconf-backend to rebuild queues instead of relying on the
  printcap.local hack.
- 0.6.51:
  - Make updateconf.py ready for foomatic-3.x.
  - Fix typo in updateconf.py (bug #87802).

* Tue Apr  1 2003 Tim Waugh <twaugh@redhat.com> 0.6.50-1
- Build requires intltool.
- 0.6.50:
  - Use intltool for desktop file translation (bug #82316, bug #79218).

* Fri Mar 21 2003 Tim Waugh <twaugh@redhat.com>
- Require at least Omni 0.7.3 (driver changed name).

* Wed Mar 19 2003 Tim Waugh <twaugh@redhat.com> 0.6.49-1
- 0.6.49:
  - Set mpage page margins in cjktexttops.
  - Fix incorrect uses of N_() (bug #86090).
  - Add PTAL support.

* Thu Mar  6 2003 Tim Waugh <twaugh@redhat.com> 0.6.48-1
- 0.6.48:
  - Set reserve=yes when lpd_strict_rfc1179 is set.
  - pycups: avoid exception when CUPS isn't running (not normal code path)
  - Fix stupid GUI bug (bug #85107).
  - Fix 'serial:' URIs on unusual serial ports (bug #85099).

* Thu Feb 13 2003 Tim Waugh <twaugh@redhat.com> 0.6.47-1
- 0.6.47:
  - Always write out PreFilter foomatic options (bug #84186).
  - Set encoding in driver notes window (bug #84151).

* Mon Feb 10 2003 Tim Waugh <twaugh@redhat.com> 0.6.46-1
- 0.6.46:
  - Documentation changes.

* Fri Feb  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.45-1
- 0.6.45:
  - Remove work-around for bug #74365 now that it's fixed (to fix
    bug #82450).

* Mon Feb  3 2003 Tim Waugh <twaugh@redhat.com> 0.6.44-1
- 0.6.44:
  - Use hpijs, not hpijs-rss, as the recommended driver (since that's what
    we ship).
  - Fix import issue (bug #83338).

* Wed Jan 29 2003 Tim Waugh <twaugh@redhat.com> 0.6.43-1
- 0.6.43:
  - Be less verbose when confronted with unknown URI types (bug #82892).

* Tue Jan 28 2003 Tim Waugh <twaugh@redhat.com> 0.6.42-1
- 0.6.42:
  - Another attempt to fix bug #80235.

* Mon Jan 27 2003 Tim Waugh <twaugh@redhat.com> 0.6.41-1
- 0.6.41:
  - Fix a bug in the 'share queue' dialog.
  - Fix password escaping in the LPRng backend (bug #80784).

* Fri Jan 23 2003 Tim Waugh <twaugh@redhat.com> 0.6.40-1
- 0.6.40:
  - Fix Omni library search path.
  - Fix state message display issues.

* Thu Jan 23 2003 Tim Waugh <twaugh@redhat.com> 0.6.39-1
- 0.6.39:
  - Fix bug #82520.

* Tue Jan 21 2003 Tim Waugh <twaugh@redhat.com> 0.6.38-1
- 0.6.38:
  - Add StartupNotify to desktop file.
  - Improve 'do you want to save?' dialog (bug #82110).
  - Display queue errors when they are reported (bug #82262).

* Mon Jan 20 2003 Tim Waugh <twaugh@redhat.com> 0.6.37-1
- 0.6.37:
  - Made USB probing work again.

* Thu Jan 16 2003 Tim Waugh <twaugh@redhat.com> 0.6.36-1
- 0.6.36:
  - Apply patch from bug #79745.
  - Fix translation encodings (bug #76079).

* Wed Jan 15 2003 Tim Waugh <twaugh@redhat.com> 0.6.35-1
- 0.6.35:
  - Finish off bug #79966.

* Tue Jan 14 2003 Tim Waugh <twaugh@redhat.com> 0.6.34-1
- 0.6.34:
  - Documentation update.

* Fri Jan 10 2003 Tim Waugh <twaugh@redhat.com> 0.6.33-1
- 0.6.33:
  - Fix printconf_conf translation issue.
  - Attempt at fixing issue related to bug #81173.

* Tue Jan  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.32-1
- 0.6.32:
  - Another attempt at fixing bug #81214.
  - Another attempt at fixing bug #80711.

* Tue Jan  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.31-1
- 0.6.31:
  - Fix PreFilter default (bug #80235).

* Tue Jan  7 2003 Tim Waugh <twaugh@redhat.com> 0.6.30-1
- 0.6.30:
  - Ask foomatic for the driver option in the current language.
  - Handle window delete events (bug #81213).
  - Fix glade XML for quit menu item.
  - Fix context menu on browsed queues (bug #81214).
  - String clarifications (bug #81276).

* Mon Jan  6 2003 Tim Waugh <twaugh@redhat.com> 0.6.29-1
- 0.6.29:
  - Several translation fixes (including bug #81177).
  - Small GUI fix for SMB screen.
  - SMB warning fix (bug #81014).

* Fri Jan  3 2003 Tim Waugh <twaugh@redhat.com> 0.6.28-1
- 0.6.28:
  - SMB browsing fix (bug #80711).

* Mon Dec 30 2002 Tim Waugh <twaugh@redhat.com> 0.6.27-1
- 0.6.27:
  - Fix SMB 'specify' dialog behaviour (bug #80711).
  - Generate correct URI for JetDirect types (bug #80682).
  - SMB password does not require username (bug #80714).

* Sun Dec 29 2002 Tim Waugh <twaugh@redhat.com> 0.6.26-1
- Move backend.py and cups_import.py into the base package (bug #80658).

* Mon Dec 23 2002 Tim Waugh <twaugh@redhat.com> 0.6.25-1
- Conflict with CUPS < 1.1.17-4 and LPRng < 3.8.15-3, for 'reload' in
  the initscript (bug #79953).
- 0.6.25:
  - Consistency for queue type names (bug #80000).
  - Default to browsing.
  - Remove work-around for bug #79953.

* Wed Dec 18 2002 Tim Waugh <twaugh@redhat.com> 0.6.24-1
- 0.6.24:
  - Work around bug #79953 for now.
  - GUI fixes (bugs #79966, #79967, #79968).

* Sat Dec 14 2002 Tim Waugh <twaugh@redhat.com> 0.6.23-1
- Build requires python-devel.
- 0.6.23:
  - Fix traceback (bug #79674).

* Fri Dec 13 2002 Tim Waugh <twaugh@redhat.com> 0.6.22-1
- 0.6.22:
  - Backend fixes.

* Fri Dec 13 2002 Tim Waugh <twaugh@redhat.com> 0.6.21-1
- Obsolete cups-drivers-* (bug #79469).
- 0.6.21:
  - Remove manual backend run now that the cups init script runs it on its
    own.
  - Add a tooltip for the SMB 'specify...' button.
  - backend: be less verbose (don't say anything during normal start-up).

* Wed Dec 11 2002 Tim Waugh <twaugh@redhat.com> 0.6.20-1
- 0.6.20:
  - Various GUI fixes.
  - Fix printconf-backend.

* Tue Dec 10 2002 Tim Waugh <twaugh@redhat.com> 0.6.19-1
- 0.6.19:
  - Fix support for specifying a share.

* Mon Dec  9 2002 Tim Waugh <twaugh@redhat.com> 0.6.18-1
- 0.6.18:
  - Support for specifying a share that is not browseable (bug #79273).

* Fri Dec  6 2002 Tim Waugh <twaugh@redhat.com> 0.6.17-1
- 0.6.17:
  - GUI improvement for selecting manufacturer.

* Mon Dec  2 2002 Tim Waugh <twaugh@redhat.com> 0.6.16-1
- 0.6.16:
  - Fix bug #78336 properly.

* Mon Nov 25 2002 Tim Waugh <twaugh@redhat.com> 0.6.15-1
- Conditionally restart cups.
- Run updateconf.py in the post scriptlet.
- Ship updateconf.py.
- 0.6.15:
  - JPEG test page.
  - updateconf.py
  - Fix bug #78336.
  - Small GUI fix for editQueue.

* Mon Nov 11 2002 Tim Waugh <twaugh@redhat.com> 0.6.14-1
- Fix %%post exit code (bug #77627).
- 0.6.14:
  - Fix icon paths.

* Wed Nov  6 2002 Tim Waugh <twaugh@redhat.com> 0.6.13-1
- Conflict with CUPS before 'reload' was added to its init script.
- 0.6.13:
  - GUI changes.
  - More test pages.
  - Use 'reload' not 'restart'.

* Tue Oct 15 2002 Tim Waugh <twaugh@redhat.com> 0.6.12-1
- Remove LPRng requirement.
- Add version to PyXML requirement (bug #74147).
- 0.6.12:
  - NCP URIs.
  - Fix jetdirectprint.

* Fri Oct 11 2002 Tim Waugh <twaugh@redhat.com> 0.6.11-1
- 0.6.11:
  - Fix cups-lpd warning.
  - Several GUI fixes.
  - Show error output when printing test page.

* Wed Oct  9 2002 Tim Waugh <twaugh@redhat.com> 0.6.10-1
- 0.6.10:
  - Fix imageable area coordinates.
  - Fix bug preventing printconf-tui from working.
  - Add LPD warning.
  - Fix paper size in 'edit queue' dialog.
  - Suggest generic PostScript option when appropriate, when adding a
    queue.

* Sat Oct  5 2002 Tim Waugh <twaugh@redhat.com> 0.6.9-1
- Ship CUPS filters.
- 0.6.9:
  - Support CJK encodings when converting from text to PS.

* Fri Oct  4 2002 Tim Waugh <twaugh@redhat.com> 0.6.8-1
- 0.6.8:
  - Banner page configuration.
  - Imageable area margins configuration.
  - Duplex test page.
  - Some GUI fixes.

* Wed Oct  2 2002 Tim Waugh <twaugh@redhat.com> 0.6.7-1
- Ship pysmb.py.
- 0.6.7:
  - Browse when adding SMB queues.

* Tue Oct  1 2002 Tim Waugh <twaugh@redhat.com> 0.6.6-1
- Ship cups_import.py.
- 0.6.6:
  - Configurable default filter options.
  - CUPS queue imports.

* Fri Sep 27 2002 Tim Waugh <twaugh@redhat.com> 0.6.5-1
- Build requires cups-devel.
- Ship the pycups module.
- 0.6.5:
  - New pycups Python module for CUPS queries.
  - Display browsed queues.
  - Use lpoptions for setting page margins.

* Thu Sep 26 2002 Tim Waugh <twaugh@redhat.com> 0.6.4-1
- 0.6.4:
  - Fix raw queues.
  - Add support for remote IPP queues.
  - Fix port binding when no queues are shared.

* Wed Sep 25 2002 Tim Waugh <twaugh@redhat.com> 0.6.3-1
- 0.6.3:
  - GUI improvements.
  - Pick sensible default page margins.

* Sun Sep 22 2002 Tim Waugh <twaugh@redhat.com> 0.6.2-1
- 0.6.2:
  - Back-end improvements for browsing, listening.
  - GUI adjustments for global sharing properties.

* Fri Sep 20 2002 Tim Waugh <twaugh@redhat.com> 0.6.1-1
- 0.6.1:
  - Sharing properties dialog.

* Wed Sep 18 2002 Tim Waugh <twaugh@redhat.com> 0.6.0-1
- 0.6.0:
  - Backend now CUPS-aware.

* Wed Sep 18 2002 Tim Waugh <twaugh@redhat.com> 0.5.3-1
- 0.5.3:
  - Fix small GUI bug.
  - Work around popen wackiness.

* Tue Sep 17 2002 Tim Waugh <twaugh@redhat.com> 0.5.2-1
- 0.5.2:
  - GUI fixes.
  - Work around an apparent GTK2 bug.

* Mon Sep 16 2002 Tim Waugh <twaugh@redhat.com> 0.5.1-1
- 0.5.1:
  - More GUI changes.

* Mon Sep  9 2002 Tim Waugh <twaugh@redhat.com> 0.5.0-1
- 0.5.0:
  - GUI rewritten.

* Tue Sep  3 2002 Tim Waugh <twaugh@redhat.com> 0.4.24-1
- 0.4.24:
  - Add Norwegian desktop file translation (bug #73175).

* Thu Aug 22 2002 Tim Waugh <twaugh@redhat.com> 0.4.23-1
- 0.4.23:
  - Fix a German translation problem (bug #71959).
  - Fix zh_CN/zh_TW translations (bug #72226).

* Fri Aug 16 2002 Tim Waugh <twaugh@redhat.com> 0.4.22-1
- 0.4.22:
  - Fix printconf-backend, which broke with the last change.

* Fri Aug 16 2002 Tim Waugh <twaugh@redhat.com> 0.4.21-1
- Replace System with SystemSetup in desktop file categories.
- Require rhpl.
- 0.4.21:
  - Prevent traceback in GUI when viewing driver tab the second time.
  - Fix strings in TUI (bug #70784).

* Mon Aug 12 2002 Tim Waugh <twaugh@redhat.com> 0.4.20-1
- 0.4.20: docs updated.

* Wed Aug  7 2002 Tim Waugh <twaugh@redhat.com> 0.4.19-1
- Ship console.apps file for redhat-config-printer-gui.
- Remove files from install root that aren't shipped.
- 0.4.19:
  - Fix window titles and icons (bug #68519).
  - Update blacklist messages (bug #70382).
  - Fix some menu item translation issues (bug #70631).
  - Fix printconf/redhat-config-printer PAM files.

* Mon Aug  5 2002 Tim Waugh <twaugh@redhat.com> 0.4.18-1
- 0.4.18:
  - Convert desktop file to UTF-8 (bug #70029).
  - Prevent errno being referenced instead of __errno_location.
  - Use the TUI PAM file for 'printconf' and 'redhat-config-printer',
    not the GUI one which uses pam_timestamp.
  - Remove GenericName entries from desktop file.

* Fri Jul 26 2002 Tim Waugh <twaugh@redhat.com> 0.4.17-1
- 0.4.17:
  - Use pam_timestamp.so (bug #69873).

* Tue Jul 23 2002 Tim Waugh <twaugh@redhat.com> 0.4.16-1
- 0.4.16:
  - Desktop file fixes (bug #69498).

* Mon Jul 22 2002 Tim Waugh <twaugh@redhat.com> 0.4.15-1
- 0.4.15:
  - Fix NCP strings in text user interface (bug #69164).

* Thu Jul 11 2002 Tim Waugh <twaugh@redhat.com> 0.4.14-1
- Provide printconf and printconf-gui.
- 0.4.14:
  - Fixed online help path (broken by name change).
  - Fixed bug #67657.

* Thu Jul 11 2002 Tammy Fox <tfox@redhat.com>
- Updated docs

* Thu Jul 11 2002 Tim Waugh <twaugh@redhat.com> 0.4.13-1
- Rename 'redhat-config-printer' (bug #68519).
- 0.4.13:
  - Show driver notes (bug #68567).
  - Fix upgrade behaviour (bug #68586).
  - Reinstate 'Grok USB autodetect information too'.

* Wed Jul 10 2002 Tim Waugh <twaugh@redhat.com> 0.4.12-1
- Add 'Application' category to the desktop file too (bug #68423).
- The desktop-file-install bug seems to be fixed in 0.2.92 so
  make the build require that.

* Wed Jul 10 2002 Tim Waugh <twaugh@redhat.com> 0.4.11-1
- Add 'System' category to the desktop file (bug #68423).
- Watch out for a desktop-file-install bug.
- 0.4.11:
  - Back out 'Grok USB autodetect information too'.

* Mon Jul  8 2002 Tim Waugh <twaugh@redhat.com> 0.4.10-1
- 0.4.10:
  - Fix JetDirect printing (bug #68032, others)

* Sat Jul  6 2002 Tim Waugh <twaugh@redhat.com> 0.4.9-1
- rpmlint: Make main package require usermode.
- Use desktop-file-install to install the .desktop file.
- 0.4.9:
  - Window position fixes (bug #68035).
  - Grok USB autodetect information too.

* Fri Jul  5 2002 Tim Waugh <twaugh@redhat.com> 0.4.8-1
- 0.4.8:
  - Translation fix-ups.
  - Fix 'autoselect driver'.
  - Mark recommended driver in driver list.

* Fri Jul  5 2002 Tim Waugh <twaugh@redhat.com> 0.4.7-1
- 0.4.7:
  - Fix bug #67923.
  - Some translation changes.

* Tue Jul  2 2002 Tim Waugh <twaugh@redhat.com> 0.4.6-1
- 0.4.6:
  - Extensions for automatic queue handling.
  - Start moving translations over to UTF-8.
  - Fix printconf's handling of translations in general (bindtextdomain).
  - Now 'printconf' and 'redhat-config-printer' figure out which type
    of interface should be used.

* Fri Jun 28 2002 Tim Waugh <twaugh@redhat.com> 0.4.5-1
- 0.4.5:
  - Fix 'driver options' crash.

* Fri Jun 28 2002 Tim Waugh <twaugh@redhat.com> 0.4.4-1
- 0.4.4:
  - Fix 'custom device' in druid.
  - Fix bug #66768.

* Thu Jun 20 2002 Tim Waugh <twaugh@redhat.com> 0.4.3-1
- 0.4.3.  More GTK2 fixes.

* Tue Jun 18 2002 Tim Waugh <twaugh@redhat.com> 0.4.2-1
- 0.4.2 (more fixes for bug #66141; proper fix for bug #38430;
  fix bug #64762).

* Mon Jun 17 2002 Tim Waugh <twaugh@redhat.com> 0.4.1-1
- 0.4.1 (fixes bug #64341, bug #66141, bug #66360).

* Thu Jun 13 2002 Tim Waugh <twaugh@redhat.com> 0.4.0-1
- Finish port to GTK2.

* Tue Jun 11 2002 Tim Waugh <twaugh@redhat.com> 0.3.100-1
- Begin port to GTK2. (Still incomplete.)
- Alter requirements.

* Fri May 24 2002 Tim Waugh <twaugh@redhat.com> 0.3.79-2
- Put printconf-tui's PAM files in the main package (bug #65436).

* Thu May 23 2002 Tim Waugh <twaugh@redhat.com> 0.3.79-1
- 0.3.79 (fixes bug #64417).

* Mon May 20 2002 Tim Waugh <twaugh@redhat.com> 0.3.78-1
- 0.3.78.

* Mon Apr 15 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.3.77-1
- Update tranlations

* Wed Apr  3 2002 Tim Waugh <twaugh@redhat.com> 0.3.76-1
- 0.3.76 (bug #62609).

* Tue Apr  2 2002 Tim Waugh <twaugh@redhat.com> 0.3.75-1
- 0.3.75 (should fix bug #61901).

* Thu Mar 14 2002 Tim Waugh <twaugh@redhat.com> 0.3.74-1
- 0.3.74 (updated docs).

* Wed Mar 13 2002 Tim Waugh <twaugh@redhat.com> 0.3.73-1
- 0.3.73 (bug #60266).
- Update local.adl to reflect gimp-print driver's name change in the
  %%post scriptlet (yuck).

* Wed Mar  6 2002 Tim Waugh <twaugh@redhat.com> 0.3.72-1
- 0.3.72 (bug #58980).

* Mon Mar  4 2002 Tim Waugh <twaugh@redhat.com> 0.3.71-1
- 0.3.71 (bug #27116, bug #59908).

* Sun Mar  3 2002 Tim Waugh <twaugh@redhat.com> 0.3.70-1
- 0.3.70 (fixes NAMETYPE error).

* Fri Mar  1 2002 Tim Waugh <twaugh@redhat.com> 0.3.69-1
- 0.3.69 (bug #59637, bug #60266).
- Use userhelper for printconf-tui too (bug #60463).

* Wed Feb 13 2002 Tim Waugh <twaugh@redhat.com> 0.3.68-1
- 0.3.68 (bug #58980, bug #27116 [partly]).
- Require mpage (bug #59481).
- PreReq fileutils, initscripts.
- Explicit path to 'touch' in %%post.
- Require new foomatic.
- Conditionally restart lpd in %%post, to make sure the queues are
  in sync with this package.

* Tue Feb  5 2002 Tim Waugh <twaugh@redhat.com> 0.3.67-1
- Ship the ChangeLog file.
- Another attempt at fixing bug #47964.

* Tue Feb  5 2002 Tim Waugh <twaugh@redhat.com> 0.3.66-1
- 0.3.66 (bug #47964).

* Thu Jan 31 2002 Tim Waugh <twaugh@redhat.com> 0.3.65-1
- 0.3.65: more alchemist work-arounds; change hpijs message now that we
  ship the driver.

* Tue Jan 29 2002 Tim Waugh <twaugh@redhat.com> 0.3.64-1
- 0.3.64 (works around bug #57986 for the time being).

* Sun Jan 27 2002 Tim Waugh <twaugh@redhat.com> 0.3.63-1
- 0.3.63 (fixes bugs #52715, #58360, #39846).

* Thu Jan 24 2002 Tim Waugh <twaugh@redhat.com> 0.3.62-1
- 0.3.62 (fixes bug #38430, bug #58769).
- Fix file list (bug #54528, bug #58160).
- Sync spec file in CVS.
- Don't configure in prep.

* Fri Jan 18 2002 Tim Waugh <twaugh@redhat.com> 0.3.61-2
- Get lpdomatic from /usr/sbin again.

* Mon Dec 17 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.61-1
- foomatic-backend checks for root now.

* Mon Dec 17 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.60-1
- foomatic moved lpdomatic, so we must change.

* Mon Dec 17 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.59-1
- rebuilt for errata

* Mon Dec  3 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.58-1
- added Omni and oki4w to the driver blacklist.

* Wed Nov 27 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.57-1
- made printconf-backend treat PAGEsize, mpage_page_size, and GS_PAPERSIZE
- differently, so that pswrite wont break everthing.

* Mon Nov 26 2001 Ben Woodard <woodard@redhat.com> 0.3.56-1
- added a couple of the magicfilter man pages.

* Tue Nov 13 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.55-1
- fixed valid_queue() so that it will validate CUSTOM type filters and queues.

* Tue Nov 06 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.54-1
- fixed printconf-tui so that it will set the selected queue after creation.

* Thu Oct 31 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.53-1
- fixed printconf-tui so that it will edit float type driver options.

* Mon Oct 22 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.52-1
- fixed printer selection after editing in -tui mode.

* Thu Oct 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.51-1
- the semantics of lpdomatic no longer accept '-d'

* Thu Oct 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.50-1
- itterate version to rebuild for errata, require Omni-foomatic

* Fri Oct 05 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.49-1
- patched to fix some bad C in magicfilter.

* Mon Oct 01 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.48-1
- use foomatics lpdomatic instead of the old mfomatic filter.

* Mon Sep 17 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.47-1
- use the new -dPARANOIDSAFER flag which I added to ghostscript.

* Wed Sep 12 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.46-1
- turned off header pages, they don't work.
- added --Xdefault mode to -tui.

* Mon Sep 10 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.45-1
- added printing header pages

* Fri Sep 07 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.44-1
- yet another bug in import_printtool_queues.py

* Fri Sep 07 2001 Yukihiro Nakai <ynakai@redhat.com> 0.3.43-1
- remove improper cjk testpages.

* Thu Sep 06 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.42-1
- finaly nail the moveto bugs in printconf-gui

* Thu Sep 06 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.41-1
- fixed 'cancel' case for printconf_tui.py

* Thu Sep 06 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.40-1
- po file churn :<

* Thu Sep 06 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.39-1
- hide import_printtool_queues.py output.

* Thu Sep 06 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.38-1
- fixed import_printtool_queues.py, which was borken badly.

* Wed Sep 05 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.37-1
- fixed annoying ctree bugs; #52968
- use fast dynamic foomatic overview instead of flat file.
- rebuild with new .po files and new docs.

* Wed Aug 29 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.36-1
- fixed the globbing on reads of the dynamic test manifests

* Tue Aug 28 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.35-1
- hooked up a url handler to the printer notes dialog.

* Tue Aug 28 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.34-1
- rolled in japanese et. all print tests, made printconf's tests dynamic.

* Tue Aug 28 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.33-1
- added setting of filtration locale; #47964
- fixed PostScript page size settings.

* Mon Aug 27 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.32-1
- handled cases where the database gives me enum options with no values.
- fixed the printer notes dialog.

* Mon Aug 27 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.31-1
- fixed some bugs with jetdirect and ncp printers in the -tui; #52242

* Sat Aug 25 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.30-1
- added USB printer autodetection

* Thu Aug 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.29-1
- pychecker induced cleanups and bug fixes.
- nicer busy states for the gui.

* Thu Aug 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.28-1
- rebuild with new docs
- fix traceback thinko bug in printconf_gui.py

* Tue Aug 21 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.27-1
- rebuild with new docs

* Tue Aug 21 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.26-1
- turn off debugging for the upgrade script; #51687
- zap the hacked japanase entries.

* Tue Aug 21 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.25-1
- added a %post touch on /etc/printcap.local to force dirty settings; #51224

* Tue Aug 21 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.24-1
- rebuilt with pbrown's wait cursor changes.
- added 'hpijs' driver to the HP blacklist
- fixed state-corruption-on-cancel-edit bug.

* Tue Aug 14 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.23-1
- fixed tracebacks on: tui set default, tui edit queue, tui edit alias

* Mon Aug 13 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.22-1
- fixed nasty traceback in printconf-backend,
- added "Assume Unknown Data is Text" option.

* Mon Aug 13 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.21-1
- add constraint checking for samba-client, ncpfs, and hpijs availability.

* Fri Aug 10 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.20-1
- fix printer options; #51224, #51383

* Thu Aug  9 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.19-1
- fixed a traceback in printconf-tui's new queue dialog.
- taught the tui and gui what happens to selection when you delete
- a overriden queue.

* Thu Aug  9 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.18-1
- added send_FF to give more robust termination options

* Thu Aug  9 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.17-1
- fixed thinko that kept A4 pages from printing in the gui; #51052

* Thu Aug  9 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.16-1
- move arround how magicfilter patches work.
- fix the patch fix that fixed magicfilter-t --help but broke -kc; #51051

* Thu Aug  9 2001 Alexander Larsson <alexl@redhat.com> 0.3.15-1
- Added a printer icon
- Install desktop file in sysconfig:

* Thu Aug  9 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.14-1
- added strip_control_file.sh, in the hopes of getting better interaction
- with solaris, maybe.

* Wed Aug  8 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.13-1
- tweaked the default rule to get better coverage.

* Wed Aug  8 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.12-1
- fixed printconf-gui's glade context, so we actually see translations.

* Fri Aug  3 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.11-1
- fixed traceback in get_default_queue_name in printconf_conf.py
- add clear option to printconf-tui

* Fri Aug  3 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.10-1
- fixed traceback in printconf_tui.py; changed 'qlf' namespace to be 'qld',
- which matches printconf_gui.py's usage. Prompt the user to override
- imported queues. added a srpm build target.

* Thu Aug  2 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.9-1
- fixed magicfilter-t help, it was segfaulting. #45687
- made exceptions in printconf-gui fatal
- fix Makefile so it does /not/ do 'rm -rf $(DESTDIR)'

* Wed Aug  1 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.8-1
- fixed shell string evaluation casued by princonf_backend.py, #33579 
- fixed deleting invalid queues in the gui, #50611
- re-added apply button, because people missed it

* Wed Aug  1 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.7-1
- fixed gui driver autoselection display; tui Send EOT option 
- restored ability to force all text to be renedered to postscript
- fixed formatting for printconf-tui edit summarys

* Tue Jul 31 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.6-1
- enabled test page printing for the tui.

* Tue Jul 31 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.5-1
- versioned the alchemist dependancy. enabled test page printing for the gui.
- fixed a traceback on creating a new printer.

* Tue Jul 31 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.4-1
- fixed gnome help paths. Mozilla now will /not/ imply the 'file://'
- scheme. Grr, behaviour change, badddd.

* Mon Jul 30 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.3-1
- fixed bugs #36598, #50248

* Mon Jul 30 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.2-1
- fixed bugs #50333, #50253, #50236, #42041, #50251
- fixed some bad charsets in the po files.
- added link to redhat-config-printer-gui and redhat-config-printer-tui

* Mon Jul 30 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.1-1
- fixed the gui type editor

* Wed Jul 24 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.0-6
- cleaned up the make/spec files
- added newt dependancy, grafted the docs back in.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.0-5
- made some changes to fix some /stupid/ python behaviour.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.0-4
- killed all use of consolehelper by printconf-tui, as it mucks with
- standard output.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.0-3
- changed printconf to depend on usermode "properly"

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.0-2
- added tests back into the package.
- fixed traceback in printconf_conf.py and printconf_backend.py

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.3.0-1
- initial packaging of 0.3.0 version. Watch out, it's on fire.
- luckily, I got to keep almost no code, but on the down side,
- there is now a printconf-tui tool to satisfy all those people
- who refuse to run X.

* Wed Apr 20 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed a bug in deleting an overriden context

* Wed Apr 18 2001 Crutcher Dunnavant <crutcher@redhat.com>
- taught printconf-gui what to do with a corrupt/missing local context.
- fixed override traceback typo (overriden != overridden)

* Wed Mar 28 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added pygtk-libglade and pygnome-libglade dependancies to printconf-gui

* Thu Mar 22 2001 Crutcher Dunnavant <crutcher@redhat.com>
- addedd the option 'r' to magicfilter's ignore list (how did we miss it before?)
- fixed bad thinko in the charset override filters
- reordered the compression filters to be defined first

* Tue Mar 20 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed O_NONBLOCK typo in scanning printers
- saved boolean foomatic defaults as strings to stop tracebacks
- fixed textdomain bug that killed half of the translations

* Sun Mar 18 2001 Yukihiro Nakai <ynakai@redhat.com>
- Update Japanese translation.

* Fri Mar 16 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed a bad thinko in the upgrade case and added japanese upgrades.
- cleanups in the code, more debugging states.
- Swedish desktop file from menthos
- killed LaserJet 5/lj5gray combo that Dell said wasn't working

* Sun Mar 11 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Taught printconf about page size in the postscript special case.
- undid my blunder that was killing languages.

* Fri Mar  8 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added context help, updated database

* Fri Mar  8 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed option setting
- twiddled postscript prepend case

* Wed Mar  7 2001 Crutcher Dunnavant <crutcher@redhat.com>
- moved EOT to the mf_wrapper stage,
- terminated text jobs with \r\n\014

* Tue Mar  6 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed upgrade case. It is now as smart as it's gonna get.

* Thu Feb 29 2001 Crutcher Dunnavant <crutcher@redhat.com>
- landed EOT sending, hope it works.

* Wed Feb 28 2001 Crutcher Dunnavant <crutcher@redhat.com>
- many, many, many tweaks
- Import docs directories

* Tue Feb 27 2001 Trond Eivind Glomsrod <teg@redhat.com>
- i18n update

* Fri Feb 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Fix langification

* Wed Feb 20 2001 Crutcher Dunnavant <crutcher@redhat.com>
- landed a huge chage

* Wed Feb 14 2001 Crutcher Dunnavant <crutcher@redhat.com>
- landed japanese, did some little bug fixes

* Tue Feb 13 2001 Crutcher Dunnavant <crutcher@redhat.com>
- tweaked locatations of the asc2goofycharset programs to put 'em in
- util, fixed crack in make_mfomatic_cfg.pl

* Tue Feb 13 2001 Crutcher Dunnavant <crutcher@redhat.com>
- changed to config(noreplace), tweaked some spelling, changed
- to a cleaner printcap format.

* Fri Feb  9 2001 Crutcher Dunnavant <crutcher@redhat.com>
- tweaked mf_wrapper to return magicfilter's return code

* Fri Feb  9 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added print testpage code

* Fri Feb  9 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added lang files

* Thu Feb  8 2001 Crutcher Dunnavant <crutcher@redhat.com>
- rebuild for more translations

* Wed Feb  7 2001 Jonathan Blandford <jrb@redhat.com>
- Fix up file.  Add foomatic stuff.

* Wed Feb  7 2001 Yukihiro Nakai <ynakai@redhat.com>
- Add more I18N

* Tue Feb  6 2001 Jonathan Blandford <jrb@redhat.com>
- Moved to auto*.  Much cleaner specfile

* Tue Feb  6 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added the smbprint, ncpprint, and jetdirectprint files (oops.)

* Sun Feb  4 2001 Crutcher Dunnavant <crutcher@redhat.com>
- moved some stuff to more appropriate subdirectories,
- allowed the non-specification of remote queues for LPD entries.

* Thu Feb  1 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Nailed down a spool/fpipe problem in magicfilter,
- tweaked some debug code.

* Wed Jan 31 2001 Crutcher Dunnavant <crutcher@redhat.com>
- split the netpbm, groff, and tetex filters out to their own
- packages. patched magicfilter's string reading code.

* Sun Jan 29 2001 Crutcher Dunnavant <crutcher@redhat.com>
- futzed with the filters, reworked the magicfilter config,
- moved everything that should go to /usr/share/printconf
- needs testing on a system with a printer.

* Thu Jan 25 2001 Crutcher Dunnavant <crutcher@redhat.com>
- stoped striping ppa support, tweaked pre/post to touch chkconfig

* Thu Jan 25 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added printcap.local to list

* Thu Jan 25 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Initial internationalization, stoped striping Postscript (oops)

* Wed Jan 24 2001 Crutcher Dunnavant <crutcher@redhat.com>
- I think upgrade from rhs-printfilters works now.

* Mon Jan 21 2001 Crutcher Dunnavant <crutcher@redhat.com>
- All but minor features complete. Needs upgrade case

* Sun Jan 20 2001 Crutcher Dunnavant <crutcher@redhat.com>
- checkin build, almost feature complete

* Fri Jan 18 2001 Crutcher Dunnavant <crutcher@redhat.com>
- changed the naming to use printconf-gui

* Fri Jan 18 2001 Crutcher Dunnavant <crutcher@redhat.com>
- rpm build fun

* Wed Jan  3 2001 Crutcher Dunnavant <crutcher@redhat.com>
- initial packaging of printconf collection

