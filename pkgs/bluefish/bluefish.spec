%global momorel 1
%global srcver 2.2.4
#%%global prever 3
#%%global rctag rc3

Summary:        The Bluefish HTML Editor
Name:           bluefish
Version:        %{srcver}
Release:        %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Editors
Source0:        http://www.bennewitz.com/%{name}/stable/source/%{name}-%{version}%{?rctag:-%{rctag}}.tar.bz2
NoSource:       0
Patch0:         %{name}-2.2.3-desktop.patch
URL:            http://bluefish.openoffice.nl/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       findutils
Requires:       grep
Requires:       %{name}-shared-data = %{version}-%{release}
BuildRequires:  desktop-file-utils
BuildRequires:  enchant-devel >= 1.4.2
BuildRequires:  gettext
BuildRequires:  glib2-devel >= 2.16
BuildRequires:  gtk2-devel >= 2.12
BuildRequires:  intltool
BuildRequires:  libgnomeui-devel >= 2.6
BuildRequires:  man
BuildRequires:  pcre-devel >= 3.9
BuildRequires:  python-devel >= 2.7
# Remove Please dummy BuildRequires:
BuildRequires:  gucharmap-devel >= 3.1.92

%description
bluefish is a GTK HTML editor for the experienced web designer. It is
currently in alpha stage, it is very usable but some features are not
yet completely finished. Bluefish is released under the GPL licence. 

%package shared-data
Summary:       Architecture-independent data for %{name}
Group:         Development/Tools
BuildArch:     noarch
# For ownership of %{_datadir}/mime/packages
Requires:      shared-mime-info
# For ownership of %{_datadir}/icons/hicolor/*/{apps,mimetypes}
Requires:      hicolor-icon-theme

%description shared-data
Files common to every architecture version of %{name}.

%prep
%setup -q -n %{name}-%{version}%{?rctag:-%{rctag}}
%patch0 -p1 -b .desktop

%build
%configure --disable-update-databases --disable-static --enable-python
make %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_datadir}/applications
%{__make} install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

%find_lang %{name}
%find_lang %{name}_plugin_about
%find_lang %{name}_plugin_charmap
%find_lang %{name}_plugin_entities
%find_lang %{name}_plugin_htmlbar
%find_lang %{name}_plugin_infbrowser
%find_lang %{name}_plugin_snippets
%{__cat} %{name}_plugin_{about,charmap,entities,htmlbar,infbrowser,snippets}.lang >> \
    %{name}.lang

/usr/bin/desktop-file-validate \
    %{buildroot}%{_datadir}/applications/%{name}.desktop

# Manually install docs so that they go into
# %{_defaultdocdir}/%{name}-%{version} even after we put them in the
# shared-data subpackage
%{__install} -d %{buildroot}%{_defaultdocdir}/%{name}-%{version}
%{__install} -m 644 -p -t %{buildroot}%{_defaultdocdir}/%{name}-%{version}/ \
    AUTHORS ChangeLog COPYING NEWS README TODO

# Unpackaged files
%{__rm} -f %{buildroot}%{_libdir}/%{name}/*.la

%clean
rm -rf %{buildroot}

%post
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :
/usr/bin/update-desktop-database &> /dev/null || :

%postun
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :
/usr/bin/update-desktop-database &> /dev/null || :

%files
%defattr(-, root, root)
%{_bindir}/%{name}
%{_libdir}/%{name}

%files shared-data -f %{name}.lang
%defattr(-,root,root,-)
%{_datadir}/%{name}
%{_datadir}/xml/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/mime/packages/*.xml
%{_datadir}/icons/hicolor/*/mimetypes/application-x-bluefish-project.png
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/scalable/mimetypes/*.svg
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/pixmaps/*.png
%{_docdir}/%{name}
%{_defaultdocdir}/%{name}-%{version}/
%{_mandir}/man1/*.1*

%changelog
* Sun Mar  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.4-1m)
- update to 2.2.4

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-2m)
- rebuild for glib 2.33.2

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-5m)
- good-bye gucharmap support again

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-4m)
- good-bye gucharmap support

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.3-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-2m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- full rebuild for mo7 release

* Sun Jun 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sat Apr 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.1m)
- update to 2.0.0rc3

* Wed Jan 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.2.1m)
- sync with Fedora devel and update to 2.0.0rc2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-7m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-6m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-4m)
- %%NoSource -> NoSource

* Tue Jul 10 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.7-3m)
- fix bluefish.desktop permission

* Fri May 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-2m)
- unhold /usr/share/mime-info

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7 (doc is still version 1.0-6)

* Thu Nov  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-2m)
- update doc to 1.0-6

* Sat Sep 30 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Thu May 25 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.5-1m)
- update to 1.0.5 and update doc version to 1.0-4
- change source URI to master site

* Fri Dec 23 2005 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.4-1m)
- update to 1.0.4
- modify patch0 (bluefish_desktop patch)

* Thu Mar 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-1m)
- up to 1.0

* Wed Aug  4 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (0.13-2m)
- add bluefish-0.13-bluefish_desktop.patch to fix icon path.

* Fri May 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.13-1m)
- verup

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.11-2m)
- revised spec for enabling rpm 4.2.

* Sun Aug 3 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.11-1m)
- update to 0.11
- use momorel macro

* Thu Jul 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9-4m)
- add BuildPrereq: pcre-devel

* Sat May  3 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9-3m)
- install .desktop file.

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9-2m)
- rebuild against for XFree86-4.3.0

* Tue Feb 18 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9-1m)
- version 0.9

* Thu Feb  6 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.7-13m)
- disable perl

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.7-12k)
  BuildRequires: imlib-devel >= 1.9.14-4k

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.7-10k)
- rebuild against libpng-1.2.2

* Fri Mar  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-8k)
- fix file attributes

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-6k)
- rebuild against for db3,4

* Thu Feb 07 2002 Motonobu Ichimura <famao@kondara.org>
- (0.7-4k)
- add perl,attrpage,internal-preview support

* Thu Feb 07 2002 Motonobu Ichimura <famao@kondara.org>
- (0.7-2k)
- add xim patch
- up to 0.7

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6-4k)
- not egcs, but gcc2.95.3

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6-2k)
- up to 0.6
- add BuildRequires: egcs ...
- include .desktop file

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.4-10k)
- rebuild against gettext 0.10.40.

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (0.4-8k)
- rebuild against libpng 1.2.0.

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4-4k)
- modified spec file with macros

* Thu Jul 13 2000 AYUHANA Tomonori <l@kondara.org>
- (0.4-2k)
- SPEC fixed ( BuildRoot )
- missing bluefish.desktop!

* Tue Jun 13 2000 haruka kusumi <haruka76@kondara.org>
  - changed some items for kondara 

* Fri May 5 2000   Bo Forslund  <bo.forslund@abc.se>
  - fine tuning of the spec file
  - possible to build with all processors on smp machines
  - an entry for RedHats wmconfig

* Tue Mar 21 2000 CW Zuckschwerdt <zany@triq.net>
  - complete rewrite of spec file
  - relocateable on build-time
  - no privileges required while building
  - fix for install_location (should really be $(LIBDIR)/bluefish!)
  - included man, locale and lib into RPM (was seriously broken)

* Thu Jan 13 2000 Chris Lea <chrislea@luciddesigns.com>
  - Fixed up spec file some. bluefish-0.3.5

* Wed Nov 17 1999 Chris Lea <chrislea@luciddesigns.com>
  - added spec file. this is my third RPM that I've made a spec
    file for, so please be merciful if I've screwed something up

