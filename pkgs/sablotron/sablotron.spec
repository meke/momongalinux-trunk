%global momorel 8

Summary: 	XSLT processor
Name: 		sablotron

%define		altname Sablot
%define 	builddir $RPM_BUILD_DIR/%{altname}-%{version}

Version: 	1.0.3
Release: %{momorel}m%{?dist}
Group: 		Applications/Text
License:	MPLv1.1 or GPLv2+
URL:		http://sourceforge.net/projects/sablotron/
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}-%{version}/%{altname}-%{version}.tar.gz
NoSource:       0

Requires:	expat >= 1.95.2
BuildRequires:  expat-devel >= 1.95.2
BuildRequires:  ncurses-devel, gcc-c++ >= 3.4.1-1m
BuildRequires:  automake15, autoconf
BuildRequires:  perl-XML-Parser

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
Sablotron is a C++ XSLT processor, written by Ginger Alliance.

%package devel
Requires: sablotron = %{version}
Summary: The development libraries and header files for Sablotron
Group: Development/Libraries

%description devel
These are the development libraries and header files for Sablotron

%prep
%setup -q -n %{altname}-%{version}

%build
export CXXFLAGS="%{optflags}"
touch NEWS AUTHORS ChangeLog COPYING
autoreconf -vif
rm NEWS AUTHORS ChangeLog COPYING
#libtoolize -c -f
#aclocal-1.5
#automake-1.5 --add-missing --copy
#autoconf
%configure \
		--enable-debugger \
		--enable-perlconnect \
		--enable-iconv-typecast \
		--enable-check-leaks

%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%files
%defattr(-,root,root)
%{_bindir}/sabcmd
%{_libdir}/libsablot.so.*
%{_mandir}/man1/sabcmd.1.*
%doc README RELEASE doc/misc/NOTES doc/misc/DEBUGGER


%files devel
%defattr(-,root,root)
%doc doc/apidoc/sablot
%doc doc/apidoc/jsdom-ref
%doc doc/apidoc/sxp
%{_bindir}/sablot-config
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_includedir}/*.h

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-8m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-6m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-5m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3
- License: MPLv1.1 or GPLv2+
- change URL

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-4m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.1-2m)
- rebuild against expat-2.0.0-1m

* Mon Sep 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1
- udpate gcc32 patch

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-c++-3.4.1

* Thu Oct 30 2003 zunda <zunda at freeshell.org>
- (1.0-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Aug 24 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-1m)

* Sun Dec  1 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96-5m)
- add BuildRequires: autoconf

* Sun Dec  1 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.96-4m)
- use automake-1.4 instead of automake1.4-p5

* Sun Nov  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.96-3m)
- add gcc32 patch(add -lstdc++ to link command)

* Sat Sep 28 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.96-2m)
- add patch for expat 1.95-5

* Sat Sep 28 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.70-8k)
- cancel gcc-3.1 autoconf-2.53

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.70-6k)
  applied gcc 3.1 patch

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (0.70-4k)
- Group correction.

* Sun Sep 23 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- Kondaraize

* Tue Sep 18 2001 Petr Cimprich <petr@gingerall.cz>
- sablotron 0.70 RPM release 1

* Wed Aug 15 2001 Petr Cimprich <petr@gingerall.cz>
- sablotron 0.65 RPM release 1

* Thu Jun 14 2001 Petr Cimprich <petr@gingerall.cz>
- sablotron 0.60 RPM release 1
- build under RedHat 7.1 with rpm 4.0.2

* Wed Apr 22 2001 Petr Cimprich <petr@gingerall.cz>
- sablotron 0.52 RPM release 1
- based on 0.51-5 spec by Henri Gomez

* Thu Feb 22 2001 Henri Gomez <hgomez@slib.fr>
- sablotron 0.51 RPM release 5
- apply patch to add -lexpat and -lstdc++ in libsablot.so
  REQUIRED for use with PHP4

* Thu Feb 22 2001 Henri Gomez <hgomez@slib.fr>
- sablotron 0.51 RPM release 4
- follow Redhat way to dispatch between pack and pack-devel

* Tue Feb 20 2001 Henri Gomez <hgomez@slib.fr>
- sablotron 0.51 RPM release 3
- added ldconfig is post/preun and cleanup stuff
- build under Redhat 6.2 + updates with rpm 3.0.5

* Mon Feb 19 2001 Henri Gomez <hgomez@slib.fr>
- sablotron 0.51 RPM release 2
- added Requires expat >= 1.95.1

* Mon Feb 19 2001 Henri Gomez <hgomez@slib.fr>
- sablotron 0.51 RPM release 1
- updated spec file
