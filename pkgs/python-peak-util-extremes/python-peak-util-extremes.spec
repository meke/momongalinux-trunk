%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define packagename Extremes

Name:           python-peak-util-extremes
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Production-quality 'Min' and 'Max' objects

Group:          Development/Languages
License:        "PSF" or "ZPL"
URL:            http://pypi.python.org/pypi/Extremes
Source0:        http://pypi.python.org/packages/source/E/%{packagename}/%{packagename}-%{version}.zip
NoSource: 0
Patch0:         %{name}-setup.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel

%description 
The peak.util.extremes module provides a production-quality implementation of
the Min and Max objects from PEP 326.  While PEP 326 was rejected for inclusion
in the language or standard library, the objects described in it are useful in
a variety of applications.  In PEAK, they have been used to implement generic
functions (in RuleDispatch and PEAK-Rules), as well as to handle scheduling and
time operations in the Trellis.  Because this has led to each project copying
the same code, we've now split the module out so it can be used independently.

%prep
%setup -q -n %{packagename}-%{version}
%patch0 -b .setup

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.txt
%{python_sitelib}/peak/util/*
%{python_sitelib}/%{packagename}-%{version}-py%{pyver}.egg-info
%{python_sitelib}/%{packagename}-%{version}-py%{pyver}-nspkg.pth

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- delete duplicate directory

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- import from Rawhide

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.1-2
- Rebuild for Python 2.6

* Sun Aug 12 2008 Luke Macken <lmacken@redhat.com> - 1.1-1
- Initial package for Fedora
