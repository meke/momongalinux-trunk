%global momorel 5

Name:		b43-openfwwf
Version:	5.2
Release:	%{momorel}m%{?dist}
Summary:	Open firmware for some Broadcom 43xx series WLAN chips
Group:		System Environment/Kernel
License:	GPLv2
URL:		http://www.ing.unibs.it/openfwwf/
Source0:	http://www.ing.unibs.it/openfwwf/firmware/openfwwf-%{version}.tar.gz
NoSource:	0
Source1:	README.openfwwf
Source2:	openfwwf.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	b43-tools
Requires:	udev
Requires:	module-init-tools


%description
Open firmware for some Broadcom 43xx series WLAN chips.
Currently supported models are 4306, 4311(rev1), 4318 and 4320.


%prep
%setup -q -n openfwwf-%{version}
sed -i s/"-o 0 -g 0"// Makefile
install -p -m 0644 %{SOURCE1} .
install -p -m 0644 %{SOURCE2} .

%build
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install PREFIX=%{buildroot}/lib/firmware/b43-open
install -p -D -m 0644 openfwwf.conf %{buildroot}%{_sysconfdir}/modprobe.d/openfwwf.conf



%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING LICENSE README.openfwwf
%dir /lib/firmware/b43-open
/lib/firmware/b43-open/b0g0bsinitvals5.fw
/lib/firmware/b43-open/b0g0initvals5.fw
/lib/firmware/b43-open/ucode5.fw
%{_sysconfdir}/modprobe.d/openfwwf.conf


%changelog
* Mon Oct 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2-5m)
- move b43 to b43-open

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2-1m)
- import from Fedora

* Wed Jan 27 2010 Peter Lemenkov <lemenkov@gmail.com> 5.2-3
- Fixed typo in summary.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jul 23 2009 Peter Lemenkov <lemenkov@gmail.com> 5.2-1
- Ver. 5.2

* Mon Jun 29 2009 Peter Lemenkov <lemenkov@gmail.com> 5.1-3
- Changed README a lot
- Changed description

* Fri Jun  5 2009 Peter Lemenkov <lemenkov@gmail.com> 5.1-2
- Added config-file for modprobe

* Wed Mar 18 2009 Peter Lemenkov <lemenkov@gmail.com> 5.1-1
- Initial package for Fedora

