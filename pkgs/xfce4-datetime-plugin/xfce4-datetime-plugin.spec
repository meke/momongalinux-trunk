%global momorel 1

%global xfce4ver 4.11.0
%global major 0.6

Summary: 	XFce4 panel plugin which shows date and time 
Name: 		xfce4-datetime-plugin
Version: 	0.6.2
Release:	%{momorel}m%{?dist}

Group: 		User Interface/Desktops
License:	GPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
# http://bugzilla.xfce.org/show_bug.cgi?id=6443
#Patch3:         xfce4-datetime-plugin-0.6.1-port-to-libxfce4ui.patch
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, perl-XML-Parser, dbus-devel >= 0.22
BuildRequires:  gtk2-devel
#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxml2-devel pkgconfig
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}

%description 
An alternative clock plugin for the Xfce panel. A calendar appears when you 
left-click on it.

%prep 
%setup -q 
#%patch3 -p1 -b .port-to-libxfce4ui
#autoreconf

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS THANKS
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.1-12m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-11m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-10m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-7m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-6m)
- rebuild against xfce4-4.6.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-5m)
- good-bye autoreconf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- add autoreconf. need libtool-2.2.x

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-2m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0
- comment out patch0

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-6m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-5m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-4m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-3m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-2m)
- rebuild against xfce4 4.1.99.2

* Wed Nov  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-1m)
- version up

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-4m)
- rebuild against xfce4 4.1.90

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3-3m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3-2m)
- rebuild against for libxml2-2.6.8

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3-1m)
- revise %files

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-3m)
- rebuild against xfce4 4.0.3

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-2m)
- rebuild against xfce4 4.0.1

* Wed Oct 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.2-1m)
- first import to Momonga

