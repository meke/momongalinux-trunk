%global momorel 2

Summary: Command line tool to access EXIF data in image files
Name: exiv2
Version: 0.23
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.exiv2.org/
Source0: http://www.exiv2.org/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: coreutils
BuildRequires: doxygen
BuildRequires: pkgconfig

%description
Exiv2 is a C++ library and command line utility to access image metadata.
Exiv2 is free software.

The Exiv2 library provides

    * full read and write access to the Exif and IPTC metadata of
      an image through Exiv2 keys and standard C++ iterators
      (Example1, Example2, Example3, Example4)
    * a smart IPTC implementation that does not affect data that
      programs like Photoshop store in the same image segment
    * Exif MakerNote support:
          o MakerNote tags can be accessed just like any other Exif metadata
          o a sophisticated write algorithm avoids corrupting the MakerNote:
              1) the MakerNote is not re-located if possible at all, and
              2) MakerNote Ifd offsets are re-calculated if the MakerNote 
                 needs to be moved (for known Ifd MakerNotes)
    * extract and delete methods for Exif thumbnails (both, JPEG and TIFF
      thumbnails)
    * set methods for Exif thumbnails (JPEG only, TIFF thumbnails can be
      set from individual tags)
    * complete API documentation (by Doxygen)

Exiv2 is a command line utility to

    * print the Exif metadata of JPEG, TIFF and several RAW image 
      formats as summary info, interpreted values, or the plain data 
      for each tag (a sample is here)
    * print the IPTC metadata of JPEG images
    * print, set and delete the JPEG comment of JPEG images
    * set, add and delete Exif and IPTC metadata of JPEG images
    * adjust the Exif timestamp (that's how it all started...)
    * rename Exif image files according to the Exif timestamp
    * extract, insert and delete Exif metadata, IPTC metadata and JPEG 
      comments
    * extract, insert and delete the thumbnail image embedded in the
      Exif metadata
    * fix the Exif ISO setting of picture taken with Nikon cameras

%package libs
Summary: Exif and Iptc metadata manipulation library
Group: System Environment/Libraries

%description libs
A C++ library to access image metadata, supporting full read and write access
to the Exif and Iptc metadata, Exif MakerNote support, extract and delete 
methods for Exif thumbnails, classes to access Ifd and so on.

%package devel
Summary: Header files and static libraries from exiv2
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on exiv2.

%package doc
Summary: Documentation for exiv2
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for exiv2.

%prep
%setup -q

%build
%configure
%make
%make doc

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# fix up permission
chmod 755 %{buildroot}%{_libdir}/lib*.so*

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING README doc/COPYING-XMPSDK 
%doc doc/ChangeLog doc/README-XMP doc/cmd*.txt
%{_bindir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}.1*

%files libs
%defattr(-,root,root)
%{_libdir}/lib%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so

%files doc
%defattr(-,root,root)
%doc doc/index.html doc/html doc/include

%changelog
* Fri Nov 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.23-2m)
- split package doc

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.21.1-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21.1-1m)
- version 0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20-3m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.20-2m)
- split package libs

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.20-1m)
- version 0.20

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- version 0.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18.2-1m)
- version 0.18.2

* Thu Apr  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18.1-1m)
- version 0.18.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-2m)
- drop Patch0 for fuzz=0, already merged upstream

* Thu Dec 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18-1m)
- version 0.18

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.17.1-1m)
- version 0.17.1

* Mon Jun  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.17-1m)
- [SECURITY] CVE-2008-2696
- version 0.17

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-4m)
- rebuild against gcc43

* Tue Feb 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3m)
- no %%NoSource

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-2m)
- update gcc43.patch

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-1m)
- version 0.16
- move docs from main package to devel package
- update gcc43.patch
- remove merged security patch
- License: GPLv2

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-3m)
- add patch for gcc43, generated by gen43patch(v1)

* Wed Dec 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15-2m)
- [SECURITY] CVE-2007-6353
- import CVE-2007-6353.diff from gentoo
 +- 16 Dec 2007; Stefan Briesenick <sbriesen@gentoo.org>
 +- +files/CVE-2007-6353.diff, +exiv2-0.13-r1.ebuild, +exiv2-0.15-r1.ebuild:
 +- added patch against integer overflow (see bug #202351).

* Thu Jul 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15-1m)
- version 0.15

* Fri Apr 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14-1m)
- version 0.14
- remove *.la files again, because exiv2-config was obsoleted

* Mon Mar  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13-1m)
- version 0.13

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-3m)
- retrived libtool library

* Wed Feb 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12-2m)
- exiv2-devel Requires: pkgconfig

* Thu Nov 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12-1m)
- version 0.12

* Wed Nov 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11-1m)
- initial package for digikam-0.9.0
- Summary and %%description are imported from cooker
