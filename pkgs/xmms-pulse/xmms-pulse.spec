%global momorel 8

Name: xmms-pulse
Summary: Pulseaudio output plugin for XMMS
Version: 0.9.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://0pointer.de/lennart/projects/xmms-pulse/
Source0: http://0pointer.de/lennart/projects/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pulseaudio
Requires: xmms
BuildRequires: pulseaudio-libs-devel
BuildRequires: xmms-devel

%description
XMMS Pulseaudio's Audio Codec plugin for XMMS!
There's currently no configuration dialog. 
Use PULSE_SERVER and PULSE_SINK to change the default output sinks.

%prep
%setup -q

%build
%configure --disable-lynx
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of libxmms-pulse.*a
rm -f %{buildroot}%{_libdir}/xmms/Output/libxmms-pulse.*a

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog LICENSE README doc/README.html doc/style.css
%{_libdir}/xmms/Output/libxmms-pulse.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- fix option of configure

* Mon Mar 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-1m)
- initial package for music freaks using Momonga Linux

