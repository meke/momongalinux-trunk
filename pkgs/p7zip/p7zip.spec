%global momorel 3

Summary:	p7zip is a port of the Windows programs 7z.exe and 7za.exe
Name:		p7zip
Version:	9.20.1
Release:	%{momorel}m%{?dist}
License:	LGPL
Group:		Applications/Archiving
URL:		http://p7zip.sf.net/
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}_%{version}_src_all.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
p7zip is a port of 7za.exe for POSIX systems like Unix (Linux, Solaris,
OpenBSD, FreeBSD, Cygwin, ...), MacOS X and BeOS.
7za.exe is the command line version of 7-zip, see http://www.7-zip.org/.
7-Zip is a file archiver with highest compression ratio.
Since 4.10, p7zip (like 7-zip) supports little-endian and big-endian machines.

%package plugins
Summary: Additional plugins for p7zip
Group: Applications/Archiving
Requires: %{name} = %{version}

%description plugins
Additional plugins that can be used with 7z to extend its abilities.
This package contains also a virtual file system for Midnight Commander.

%prep
%setup -q -n %{name}_%{version}

### Create wrapper scripts, as 7zCon.sfx and Codecs/Formats need to be in the
### same directory as the binaries, and we don't want them in %{_bindir}.
%{__cat} << 'EOF' > 7za.sh
#!/bin/sh
exec %{_libexecdir}/p7zip/7za $@
EOF

%{__cat} << 'EOF' > 7z.sh
#!/bin/sh
exec %{_libexecdir}/p7zip/7z $@
EOF

%build
%ifarch %{ix86}
cp makefile.linux_x86_asm_gcc_4.X makefile.machine
%endif
%ifarch x86_64
cat makefile.linux_amd64 | sed 's/-m32//' > makefile.machine
%endif
%ifarch ppc ppc64 alpha
cp makefile.linux_x86_ppc_alpha_gcc_4.X makefile.machine
%endif
perl -p -i -e 's|\-O2|%{optflags}|' makefile.machine

### Use optflags
%{__perl} -pi -e 's|^ALLFLAGS=.*|ALLFLAGS=-Wall %{optflags} -fPIC \\|g' \
    makefile.machine

%{__make} %{?_smp_mflags} 7z 7za sfx
find DOCS -type f | xargs chmod 644

%install 
%{__rm} -rf %{buildroot}

### Install binaries (7za, 7z, 7zCon.sfx and Codecs/Formats)
%{__mkdir_p} %{buildroot}%{_libexecdir}/p7zip/
%{__cp} -a bin/* %{buildroot}%{_libexecdir}/p7zip/

### Install wrapper scripts
%{__install} -D -m0755 7z.sh %{buildroot}%{_bindir}/7z
%{__install} -D -m0755 7za.sh %{buildroot}%{_bindir}/7za

chmod 0755 %{buildroot}%{_bindir}/*

%clean
rm -rf %{buildroot}

%files 
%defattr(-, root, root, 0755) 
%doc ChangeLog README TODO DOCS/*
%{_bindir}/7za
%dir %{_libexecdir}/p7zip/
%{_libexecdir}/p7zip/7za
%{_libexecdir}/p7zip/7zCon.sfx

%files plugins
%defattr(-, root, root, 0755) 
%doc contrib/
%{_bindir}/7z
%{_libexecdir}/p7zip/7z
%{_libexecdir}/p7zip/7z.so
%{_libexecdir}/p7zip/Codecs/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.20.1-3m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.20.1-2m)
- fix build problem on i686

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.20.1-1m)
- update to 9.20.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.13-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9.13-1m)
- update to 9.13

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.04-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (9.04-1m)
- update to 9.04

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.65-1m)
- update to 4.65

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.61-2m)
- rebuild against rpm-4.6

* Mon Dec  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.61-1m)
- update to 4.61

* Sat Jun 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.58-1m)
- update to 4.58

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.57-3m)
- rebuild against gcc43

* Sun Jan  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.57-2m)
- add patch for gcc43

* Mon Dec 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.57-1m)
- [SECURITY] CVE-2008-6536
- update to 4.57

* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.55-1m)
- update to 4.55

* Thu Sep  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.53-1m)
- update to 4.53
- do not use %%NoSource macro

* Sat Jul 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.51-1m)
- update to 4.51

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.47-1m)
- update to 4.47
- remove unused patch

* Sun Feb 11 2007 NARITA Koichi <pulsar@moomnga-linux.org>
- (4.44-1m)
- update to 4.44

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (4.43-1m)
- update to 4.43

* Sun Jun 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.42-1m)
- update to 4.42
- do not apply patch0 because no longer needed

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.30-1m)
- update to 4.30
- add gcc-4.1 patch.
-- p7zip-4.30-gcc41.patch

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.29-1m)
- update to 4.29
- separate package import from dag method

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.14.01-3m)
- enable 64bit binary for x86_64.

* Thu Mar 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.14.01-2m)
- fix DOCS permissions
- change bin permissions

* Mon Mar 14 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.14.01-1m)
  Imported
