%global momorel 1
%define tarball xf86-video-mga
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 mga video driver
Name:      xorg-x11-drv-mga
Version:   1.6.3
Epoch: 1
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0

Patch0: mga-1.4.5-no-hal-advertising.patch
Patch2: mga-1.4.12-bigendian.patch

License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 mga video driver.

%prep
%setup -q -n %{tarball}-%{version}
%patch0 -p1 -b .hal
%patch2 -p1 -b .bigendian

%build
autoreconf -v --install || exit 1
%configure --disable-static --disable-dri
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/mga_drv.so
%{_mandir}/man4/mga.4*

%changelog
* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.6.3-1m)
- update to 1.6.3

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.6.2-1m)
- update to 1.6.2

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.6.1-1m)
- update to 1.6.1

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.5.0-1m)
- update to 1.5.0

* Mon Jan 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.4.13-5m)
- add patch
-- support many hardware 
-- remove hal support
- disable dri

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.4.13-4m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.4.13-3m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.4.13-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.13-1m)
- update to 1.4.13

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.4.12-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.4.12-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.4.12-2m)
- add epoch to %%changelog

* Thu May 20 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.4.12-1m)
- version up 1.4.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.4.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.4.11-1m)
- version down to 1.4.11
- Epoch:0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.100-7m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.100-6m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.100-5m)
- add mga-1.4.7-death-to-cfb.patch

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.100-4m)
- update 1.9.100-20080504
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.100-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.100-2m)
- %%NoSource -> NoSource

* Mon Oct 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.100-1m)
- update 1.9.100

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-2m)
- rebuild against xorg-x11-server-1.4

* Fri Sep  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-1m)
- update 1.4.7

* Fri Aug 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6.1-3m)
- add G200SE patch
-- G200SE is used being Server widely

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6.1-2m)
- update mga.xinf

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.6.1-1m)
- update to 1.4.6.1

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Sun Nov 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1.3-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1.3-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.2.1.3-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.2.1.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.2.1.3-1
- Updated xorg-x11-drv-mga to version 1.2.1.3 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.2.1.2-1
- Updated xorg-x11-drv-mga to version 1.2.1.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.2.1-1
- Updated xorg-x11-drv-mga to version 1.2.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.2.0.1-1
- Updated xorg-x11-drv-mga to version 1.2.0.1 from X11R7 RC1
- Fix *.la file removal.
- Remove hardware specific {_bindir}/stormdwg utility.

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 1.1.2-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86, x86_64, ia64, ppc

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.1.2-0
- Initial spec file for mga video driver generated automatically
  by my xorg-driverspecgen script.
