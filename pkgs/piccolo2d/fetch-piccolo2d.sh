#! /bin/sh

svnrev=702

mkdir temp
cd temp

svn export -r $svnrev http://piccolo2d.googlecode.com/svn/piccolo2d.java/trunk piccolo2d.java

tar cjf piccolo2d-1.3-svn$svnrev.tar.bz2 piccolo2d.java

