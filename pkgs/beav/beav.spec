%global momorel 21
Summary: binary editor with emacs-like keybindings
Name: beav
Version: 1.40
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: beav-1.40.tar.bz2
#Source0: ftp://ftp.cdrom.com/pub/simtelnet/msdos/binaryed/beav140s.zip
Patch0:beav-1.40-glibc.patch
Patch1:beav-1.40-linux.patch
Patch2: beav-1.40-remove-wrong-prototypes.patch
Prefix: /usr
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel >= 5.6-10m

%description
beav is an editor that brings the features of a powerful full screen
editor to the editing of binary files.  You can edit a file in hex,
ASCII, EBCDIC, octal, decimal, and binary.  You can search or search
and replace in any of these modes.  Data can be displayed in byte,
word, or double word formats.  While displaying words or double words
the data can be displayed in Intel's or Motorola's byte swap format.
Data of any length can be inserted at any point in the file.  The
source of this data can be the keyboard, another buffer, of a file.
Any data that is being displayed can be sent to a printer in the
displayed format.  Files that are bigger than memory can be handled.

%prep
%setup -q
%patch1 -p1 -b glibc 
%patch0 -p1 -b linux
%patch2 -p1

# use ncueses
sed -i "s/-ltermcap/-lncurses/g" makefile.*

%build
make -f makefile.uxv CC="gcc %{optflags}"

%install
mkdir -p %{buildroot}/usr/bin
install -s -m 0755 beav %{buildroot}/usr/bin/

%clean 
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/bin/beav

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.40-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.40-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40-19m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.40-18m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40-16m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.40-15m)
- rebuild against gcc43

* Mon Feb 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40-14m)
- no use libtermcap

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.40-13m)
- rebuild against libtermcap-2.0.8-38m.

* Wed Nov 24 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.40-12m)
- removed wrong prototype declarations

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.40-11m)
- rebuild against new environment.

* Tue Nov 26 2002 zunda <zunda@freeshell.org>
- kossori
- BuildPreReq libtermcap-devel

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.40-10k)
- Source0 not found.
  NoSource 0 statement was removed.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Mar  4 2000 Toru Hoshina <t@kondara.orb>
- I hate unzip :-P

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Apr 11 1999 Toru Hoshina<hoshina@best.com>
- rebuild against rawhide 1.3.3.

* Wed Oct 06 1998 Michael Maher <mike@redhat.com>
- built this thing for 5.2

* Mon May 18 1998 Michael Maher <mike@redhat.com>
- Rebuilt package and added buildroots. 

* Wed Nov 26 1997 Otto Hammersmith <otto@redhat.com>
- built for 5.0


* Mon Apr 28 1997 Michael Fulbright <msf@redhat.com>
- Updated to version 1.40
- Is now the ugliest spec file I've ever written. I just didnt want to
  break up the original .zip file the sources came in. Live with it
  pink boy...
