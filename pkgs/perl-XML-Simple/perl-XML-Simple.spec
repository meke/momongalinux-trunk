%global         momorel 8

Name:           perl-XML-Simple
Version:        2.20
Release:        %{momorel}m%{?dist}
Summary:        Easily read/write XML (esp config files)
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-Simple/
Source0:        http://www.cpan.org/authors/id/G/GR/GRANTM/XML-Simple-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-XML-NamespaceSupport >= 1.04
BuildRequires:  perl-XML-SAX >= 0.15
BuildRequires:  perl-XML-SAX-Expat
Requires:       perl-XML-NamespaceSupport >= 1.04
Requires:       perl-XML-SAX >= 0.15
Requires:       perl-XML-SAX-Expat
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The XML::Simple module provides a simple API layer on top of an underlying
XML parsing module (either XML::Parser or one of the SAX2 parser modules).
Two functions are exported: XMLin() and XMLout(). Note: you can explicity
request the lower case versions of the function names: xml_in() and
xml_out().

%prep
%setup -q -n XML-Simple-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/XML/Simple.pm
%{perl_vendorlib}/XML/Simple
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-15m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-8m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-7m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-5m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18-2m)
- %%NoSource -> NoSource

* Thu Aug 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-1m)
- update to 2.18

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17-1m)
- update to 2.17

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.16-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-1m)
- update to 2.16

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.14-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.14-1m)
- version up to 2.14
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.12-2m)
- rebuild against perl-5.8.5

* Mon May 17 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.12-1m)
- version up

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (2.09-4m)
- revised spec for rpm 4.2.

* Thu Dec 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.09-3m)
- comment out 'make test' for rebuild

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.09-2m)
- rebuild against perl-5.8.2

* Sun Nov  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.09-1m)

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.08-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.08-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Jul  8 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (2.08-1m)
- Update to 2.08

* Fri Jan 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.03-1m)
- import to momonga
