%global momorel 10

Summary: Library to render text and graphic overlays onto video images
Name: libucil
Version: 0.9.8
Release: %{momorel}m%{?dist}
License: GPLv3
Group: System Environment/Libraries
URL: http://www.unicap-imaging.org/
Source0: http://www.unicap-imaging.org/downloads/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: gettext
BuildRequires: glib2-devel
# keep for new version
BuildRequires: gtk-doc
BuildRequires: intltool
BuildRequires: libogg-devel
BuildRequires: libpng-devel
BuildRequires: libtheora-devel
BuildRequires: libunicap-devel
BuildRequires: libvorbis-devel
BuildRequires: perl
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig

%description
Unicap provides a uniform interface to video capture devices. It allows
applications to use any supported video capture device via a single API.
The related ucil library provides easy to use functions to render text
and graphic overlays onto video images.

%package devel
Summary: Header files and libraries from libucil
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libucil.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/unicap/ucil*.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-10m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-7m)
- full rebuild for mo7 release

* Mon Mar  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8-1m)
- initial package for opencv-2.0.0
- disable gtk-doc for the moment
