%global momorel 1

Summary: DVD authoring tool
Name: dvdstyler
Version: 2.7.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://www.dvdstyler.de/
Source0: http://dl.sourceforge.net/sourceforge/dvdstyler/DVDStyler-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dvdauthor
Requires: mjpegtools
Requires: netpbm-progs
Requires: mpgtx
Requires: genisoimage
Requires: dvd+rw-tools
BuildRequires: wxGTK-devel >= 2.8.10-7m
BuildRequires: wxsvg-devel >= 1.2
BuildRequires: libgnomeui-devel
BuildRequires: gettext
BuildRequires: dvdauthor
BuildRequires: mjpegtools
BuildRequires: netpbm-progs
BuildRequires: mpgtx
BuildRequires: genisoimage
BuildRequires: dvd+rw-tools
BuildRequires: libjpeg-devel >= 8a
BuildRequires: ffmpeg-devel >= 2.2
BuildRequires: systemd-devel >= 187

%description
DVDStyler is a DVD authoring System.

%prep
%setup -q -n DVDStyler-%{version}

%build
%configure
%make

%install
%{__rm} -rf %{buildroot} _doc
mkdir -p %{buildroot}%{_datadir}/gnome/apps/Multimedia
%{__make} install DESTDIR=%{buildroot}
%find_lang %{name}
# Put docs back where we'll include them nicely
%{__mv} %{buildroot}%{_datadir}/doc/dvdstyler _doc


%clean
%{__rm} -rf %{buildroot}


%files -f %{name}.lang
%defattr(-, root, root, 0755)
%doc _doc/*
%{_bindir}/dvdstyler
%{_datadir}/%{name}
%{_datadir}/pixmaps/*
%{_datadir}/applications/*.desktop
%{_mandir}/man1/dvdstyler.1*

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.1-1m)
- update 2.7.1

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-1m)
- update 2.5.1

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-3m)
- rebuild against systemd-187

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-2m)
- rebuild against ffmpeg-0.7.0-0.20110705

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-1m)
- update 1.8.3-20110513

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3-5m)
- full rebuild for mo7 release

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.3-4m)
- explicitly link libjpeg

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.3-3m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.3-1m)
- update to 1.7.3, we can build with wxsvg-1.0

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5-8m)
- rebuild against libjpeg-7

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-7m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-4m)
- %%NoSource -> NoSource

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5-3m)
- rebuild against wxGTK-2.8.6 and wxsvg-1.0-0.7.4m

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5-2m)
- good-bye cdrtools and welcome cdrkit

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5 official release
- change source URI

* Sat Apr 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-0.7.3m)
- add Patch0: dvdstyler-1.5b7-install-desktop-file.patch

* Sat Apr 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-0.7.2m)
- move dvdstyler.desktop location

* Mon Apr 09 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5-0.7.1m)
- import from freshrpms.net

* Fri Jan 19 2007 Matthias Saou <http://freshrpms.net/> 1.5-0.1.b7
- Initial RPM release.

