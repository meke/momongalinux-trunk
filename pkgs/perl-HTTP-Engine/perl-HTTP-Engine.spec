%global         momorel 9

Name:           perl-HTTP-Engine
Version:        0.03005
Release:        %{momorel}m%{?dist}
Summary:        Web Server Gateway Interface and HTTP Server Engine Drivers (Yet Another Catalyst::Engine)
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTTP-Engine/
Source0:	http://www.cpan.org/authors/id/Y/YA/YAPPO/HTTP-Engine-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-Any-Moose >= 0.08
BuildRequires:  perl-CGI-Simple
BuildRequires:  perl-Class-Method-Modifiers
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-FCGI >= 0.67
BuildRequires:  perl-HTTP-Body >= 1.05
BuildRequires:  perl-HTTP-Headers-Fast >= 0.09
BuildRequires:  perl-HTTP-Request-AsCGI
BuildRequires:  perl-HTTP-Server-Simple >= 0.35
BuildRequires:  perl-IO-stringy
BuildRequires:  perl-Mouse >= 0.21
BuildRequires:  perl-MouseX-Types >= 0.01
BuildRequires:  perl-POE >= 1.0003
BuildRequires:	perl-Plack
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-SharedFork >= 0.04
BuildRequires:  perl-Test-TCP >= 0.04
BuildRequires:  perl-URI >= 1.36
BuildRequires:  perl-YAML
Requires:       perl-Any-Moose >= 0.08
Requires:       perl-Cgi-Simple >= 1.103
Requires:       perl-Class-Method-Modifiers
Requires:       perl-FCGI >= 0.67
Requires:       perl-HTTP-Body >= 1.05
Requires:       perl-HTTP-Headers-Fast >= 0.09
Requires:       perl-HTTP-Server-Simple >= 0.35
Requires:       perl-IO-stringy >= 2.11
Requires:       perl-Mouse >= 0.21
Requires:       perl-MouseX-Types >= 0.01
Requires:       perl-POE >= 1.0003
Requires:	perl-Plack
Requires:       perl-URI >= 1.36
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
HTTP::Engine abstracts handling the input and output of various web server
environments, including CGI, mod_perl and FastCGI.

%prep
%setup -q -n HTTP-Engine-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%if %{do_test}
%check
unset http_proxy
make test ||:
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%{perl_vendorlib}/HTTP/Engine*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-2m)
- rebuild against perl-5.16.0

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03005-1m)
- update to 0.03005

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-11m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.03004-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.03004-7m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.03004-6m)
- unset http_proxy before test

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.03004-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-2m)
- rebuild against perl-5.12.0

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03004-1m)
- update to 0.03004

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03003-1m)
- update to 0.03003

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.03002-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03002-1m)
- update to 0.03002

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03001-1m)
- update to 0.03001

* Sat Sep 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02004-2m)
- add do_test option to disable %%check

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02004-1m)
- update to 0.02004

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02002-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02002-2m)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02002-1m)
- update to 0.02002

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02001-1m)
- update to 0.02001

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-3m)
- remove duplicate directories

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-2m)
- modify BuildRequires

* Thu May 28 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.1.8-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
