%global momorel 7

%define _pkglibdir %{_libdir}/nx
%define _pkgdatadir %{_datadir}/nx
%define _pkglibexecdir %{_libexecdir}/nx

Summary:	Free Software (GPL) Implementation of the NX Server
Name:		freenx
Version:	0.7.1
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/Internet
URL:		http://freenx.berlios.de/
Source0:	http://download.berlios.de/freenx/%{name}-%{version}.tar.gz
Source1:	freenx.logrotate
NoSource:	0
Patch0:		freenx-0.7.1-sharing_and_multimedia_fixes.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	nx
Requires:	openssh-server 
Requires:	nc 
Requires:	expect 
Requires:	which 
Requires:	perl
Requires:	xorg-x11-server-Xorg 
Requires:	xorg-x11-apps

%description
NX is an exciting new technology for remote display. It provides near
local speed application responsiveness over high latency, low
bandwidth links. The core libraries for NX are provided by NoMachine
under the GPL. FreeNX is a GPL implementation of the NX Server.

%prep
%setup -q
%patch0 -p1 -b .sharing_and_multimedia

sed -i -e's,\$NX_DIR/bin,%{_pkglibexecdir},g'\
  -e's,\$NX_DIR/lib,%{_pkglibdir},g'\
  nxloadconfig nxserver
sed -i -e's,^NX_LOGFILE=.*,NX_LOGFILE=/var/log/nx/nxserver.log,' \
  nxloadconfig

%build
make -C nxserver-helper CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_pkglibexecdir}
mkdir -p %{buildroot}/etc/nxserver
mkdir -p %{buildroot}%{_pkglibexecdir}

install -p -m 0755 nxkeygen nxprint \
  %{buildroot}%{_pkglibexecdir}

mkdir -p %{buildroot}/var/lib/nxserver/home/.ssh
ln -s /etc/nxserver/server.id_dsa.pub.key \
  %{buildroot}/var/lib/nxserver/home/.ssh/authorized_keys
chmod 0700 %{buildroot}/var/lib/nxserver/home{,/.ssh}

mkdir -p %{buildroot}/var/lib/nxserver/db/closed
mkdir -p %{buildroot}/var/lib/nxserver/db/running
mkdir -p %{buildroot}/var/lib/nxserver/db/failed
chmod -R 0700 %{buildroot}/var/lib/nxserver

mkdir -p %{buildroot}/var/log/nx
chmod 0700 %{buildroot}/var/log/nx

mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
cp -pr %SOURCE1 %{buildroot}%{_sysconfdir}/logrotate.d/freenx

%clean
rm -rf %{buildroot}

%pre
%{_sbindir}/useradd -r -d /var/lib/nxserver/home -s %{_pkglibexecdir}/nxserver nx 2>/dev/null \
  || %{_sbindir}/usermod -d /var/lib/nxserver/home -s %{_pkglibexecdir}/nxserver nx 2>/dev/null || :

%post
if test ! -e /etc/nxserver/users.id_dsa; then
  %{_bindir}/ssh-keygen -q -t dsa -N "" -f /etc/nxserver/users.id_dsa
fi

if ! test -e /etc/nxserver/client.id_dsa.key -a -e /etc/nxserver/server.id_dsa.pub.key; then
  %{_bindir}/ssh-keygen -q -t dsa -N "" -f /etc/nxserver/local.id_dsa
  mv -f /etc/nxserver/local.id_dsa /etc/nxserver/client.id_dsa.key
  mv -f /etc/nxserver/local.id_dsa.pub /etc/nxserver/server.id_dsa.pub.key
fi

echo -n "127.0.0.1 " > /var/lib/nxserver/home/.ssh/known_hosts
# We do depend on openssh-server, but package installation != key
# creation time. See also Fedora bug #235592
cat /etc/ssh/ssh_host_rsa_key.pub >> /var/lib/nxserver/home/.ssh/known_hosts 2>/dev/null
chown nx:root /var/lib/nxserver/home/.ssh/known_hosts

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog CONTRIB
%doc nxsetup
%{_pkglibexecdir}/*
%defattr(-,nx,root,-)
/etc/nxserver
/var/lib/nxserver
/var/log/nx
%config(noreplace) %{_sysconfdir}/logrotate.d/freenx

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against rpm-4.6

* Sun Nov  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-2m)
- remove conflicting files with freenx-server

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- import from Fedora devel

* Mon Dec 31 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.7.1-4
- Apply Jeffrey J. Kosowsky's patches to enable multimedia and
  file/print sharing support (Fedora bug #216802).
- Silence %%post output, when openssh's server has never been started
  before (Fedora bug #235592).
- Add dependency on which (Fedora bug #250343).

* Mon Dec 10 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.1-3
- Fix syntax error in logrotate file, BZ 418221.

* Mon Nov 19 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.1-2
- Added logrotate, BZ 379761.

* Mon Nov 19 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.1-1
- Update to 0.7.1, many bugfixes, BZ 364751, 373771.

* Sun Sep 23 2007 Ville Skytta <ville.skytta at iki.fi> - 0.7.0-2
- Do not try to set up KDE_PRINTRC if ENABLE_KDE_CUPS is not 1, deal better
  with errors when it is (#290351).

* Thu Sep 6 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.0-1
- CM = Christian Mandery mail@chrismandery.de,  BZ 252976
- Version bump to 0.7.0 upstream release (CM)
- Fixed download URL (didn't work, Berlios changed layout). (CM)
- Changed license field from GPL to GPLv2 in RPM. (CM)
- Fixed release.

* Mon Feb 19 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.6.0-9
- Update to 0.6.0.

* Sat Sep 17 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.4.4.

* Sat Jul 30 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.4.2.

* Sat Jul  9 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.4.1.

* Tue Mar 22 2005 Rick Stout <zipsonic[AT]gmail.com> - 0:0.3.1
- Updated to 0.3.1 release

* Tue Mar 08 2005 Rick Stout <zipsonic[AT]gmail.com> - 0:0.3.0
- Updated to 0.3.0 release
- Removed home directory patch as it is now default

* Mon Feb 14 2005 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.8
- Updated to 0.2.8 release
- Fixes some security issues
- Added geom-fix patch for windows client resuming issues

* Thu Dec 02 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:0.2.7
- Fixed package removal not removing the var session directories

* Tue Nov 23 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.7
- Updated to 0.2.7 release
- fixes some stability issues with 0.2.6

* Fri Nov 12 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:0.2.6
- Fixed a problem with key backup upon removal

* Fri Nov 12 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.6
- Updated to 0.2.6 release
- Changed setup to have nx user account added as a system account.
- Changed nx home directory to /var/lib/nxserver/nxhome

* Thu Oct 14 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.5
- updated package to 0.2.5 release
- still applying patch for netcat and useradd

* Fri Oct 08 2004 Rick Stout <zipsonic[AT]gmail.com> - 3:0.2.4
- Added nxsetup functionality to the rpm
- patched nxsetup (fnxncuseradd) script for occasional path error.
- Added patch (fnxncuseradd) to resolve newer client connections (netcat -> nc)
- Changed name to be more friendly (lowercase)
- Added known dependencies

* Thu Sep 30 2004 Rick Stout <zipsonic[AT]gmail.com> - 2:0.2.4
- Patch (fnxpermatch) to fix permissions with key generation

* Wed Sep 29 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:0.2.4
- Initial Fedora RPM release.
- Updated SuSE package for Fedora
