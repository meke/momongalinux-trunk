%global         momorel 3
%global boost_version 1.55.0
%{!?python_sitelib:%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch:%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

#define ENABLE_TESTS -DENABLE_TESTS:BOOL=ON

Name:           avogadro
Version:        1.1.0
Release:        %{momorel}m%{?dist}
Summary:        An advanced molecular editor for chemical purposes
Group:          Applications/Editors
License:        GPLv2
URL:            http://avogadro.openmolecules.net/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
# Fix qmake mkspecs installation directory (broken in 1.0.3)
Patch0:         %{name}-%{version}-mkspecs-dir.patch
# Remove -Wl,-s from the compiler flags, fixes -debuginfo (#700080)
Patch1:         %{name}-%{version}-no-strip.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake >= 2.8.5-1m
BuildRequires:  qt-devel >= 4.7.4-3m
BuildRequires:  eigen2-devel >= 2.0.3
BuildRequires:  openbabel-devel >= 2.2.2
BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  glew-devel >= 1.10.0
BuildRequires:  desktop-file-utils
BuildRequires:  docbook-utils
BuildRequires:  sip-devel
BuildRequires:  numpy
Requires:       %{name}-libs = %{version}-%{release}

%description
An advanced molecular editor designed for 
cross-platform use in computational chemistry,
molecular modeling, bioinformatics, materials science,
and related areas, which offers flexible rendering and
a powerful plugin architecture.

%package libs
Summary:        Shared libraries for Avogadro
Group:          System Environment/Libraries
Requires:       sip

%description libs
This package contains the shared libraries for the
molecular editor Avogadro.

%package devel
Summary:        Development files for Avogadro
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}
# all this stuff is in the link interface in AvogadroLibraryDeps.cmake
Requires:       qt-devel
Requires:       glew-devel
Requires:       python-devel
Requires:       boost-devel

%description devel
This package contains files to develop applications using 
Avogadros libraries.

%prep
%setup -q
%patch0 -p1 -b .mkspecs-dir
%patch1 -p1 -b .no-strip

# nuke unpatched copy, use working version included in cmake instead -- Rex
rm -f cmake/modules/FindPythonLibs.cmake

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} %{?ENABLE_TESTS} -DENABLE_GLSL:BOOL=ON -DENABLE_PYTHON:BOOL=ON  \
  %{_cmake_skip_rpath} \
	-DPYTHON_INCLUDE_DIR:PATH=%{_includedir}/python2.7 \
	-DPYTHON_LIBRARY:FILEPATH=%{_libdir}/libpython2.7.so \
	..
popd
make %{?_smp_mflags} -C %{_target_platform}


%install
rm -rf $RPM_BUILD_ROOT
make install/fast DESTDIR=$RPM_BUILD_ROOT -C %{_target_platform}
sed -i -e 's!/usr//usr/!/usr/!g' \
  $RPM_BUILD_ROOT%{_libdir}/avogadro/AvogadroConfig.cmake

%find_lang libavogadro --with-qt --without-mo
%find_lang avogadro --with-qt --without-mo

%check
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/avogadro.desktop
# all of these currently require an active X session, so will fail
# in mock
%{?ENABLE_TESTS:make test -C %{_target_platform} ||:}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files -f avogadro.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING
%{_bindir}/avogadro
%{_bindir}/avopkg
%{_datadir}/avogadro/builder/
%{_datadir}/avogadro/crystals/
%{_datadir}/avogadro/fragments/
%{_datadir}/pixmaps/avogadro-icon.png
%{_datadir}/applications/avogadro.desktop
%{_mandir}/man1/avogadro.1*
%{_mandir}/man1/avopkg.1*
# find_lang doesn't support ca@valencia for qm files
%lang(ca@valencia) %{_datadir}/avogadro/i18n/avogadro_ca@valencia.qm

%files devel
%defattr(-,root,root,-)
%{_includedir}/avogadro/
%{_libdir}/libavogadro.so
%{_libdir}/libavogadro_OpenQube.so
%{_libdir}/avogadro/*.cmake
%{_libdir}/avogadro/1_1/*.cmake
%{_libdir}/pkgconfig/avogadro.pc
# Check at some point if these have been upstreamed, as of 2.8.0 they haven't.
# KDE ships copies of the same stuff, but this package is Qt-only. -- Kevin
%{_libdir}/avogadro/1_1/cmake/
%{_qt4_prefix}/mkspecs/features/avogadro.prf

%files libs -f libavogadro.lang
%defattr(-,root,root,-)
%{python_sitearch}/Avogadro.so
%{_datadir}/libavogadro/
%dir %{_datadir}/avogadro/
%dir %{_datadir}/avogadro/i18n/
%{_libdir}/libavogadro.so.1*
%{_libdir}/libavogadro_OpenQube.so.0*
%dir %{_libdir}/avogadro/
%dir %{_libdir}/avogadro/1_1/
%{_libdir}/avogadro/1_1/colors/
%{_libdir}/avogadro/1_1/extensions/
%{_libdir}/avogadro/1_1/engines/
%{_libdir}/avogadro/1_1/tools/
# find_lang doesn't support ca@valencia for qm files
%lang(ca@valencia) %{_datadir}/avogadro/i18n/libavogadro_ca@valencia.qm


%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild against boost-1.55.0

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-2m)
- rebuild against glew-1.10.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0
- rebuild against boost-1.52.0

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-7m)
- rebuild against glew-1.9.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-6m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.0.3-5m)
- rebuild for boost 1.50.0

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-4m)
- rebuild against glew-1.7.0

* Sun Dec 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-3m)
- rebuild against boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for boost

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- import From fedora for new kdeedu-4.7.0

* Thu Jul 21 2011 Rex Dieter <rdieter@fedoraproject.org> 1.0.3-4
- rebuild (boost)

* Mon Jun 20 2011 ajax@redhat.com - 1.0.3-3
- Rebuild for new glew soname

* Wed Apr 27 2011 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.3-2
- remove -Wl,-s from the compiler flags, fixes -debuginfo (#700080)

* Tue Apr 26 2011 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.3-1
- update to 1.0.3
- drop all patches (fixed upstream)
- fix qmake mkspecs installation directory (broken in 1.0.3)

* Fri Apr 08 2011 Jaroslav Reznik <jreznik@redhat.com> - 1.0.1-15
- rebuild for new boost (1.46.1)

* Tue Mar 22 2011 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-14
- fix forcefield extension with OpenBabel 2.3.0 (#680292, upstream patch)
- fix autooptimization tool with OpenBabel 2.3.0 (#680292, patch by lg)

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Feb 07 2011 Thomas Spura <tomspur@fedoraproject.org> - 1.0.1-12
- rebuild for new boost

* Thu Nov 25 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-11
- rebuild for new openbabel (2.3.0)

* Tue Nov 23 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-10
- fix crash with current SIP and Python 2.7 (#642248)

* Wed Sep 29 2010 jkeating - 1.0.1-9
- Rebuilt for gcc bug 634757

* Tue Sep 14 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-8
- fix FTBFS with SIP 4.11 (Gentoo#335644)

* Thu Sep 09 2010 Rex Dieter <rdieter@fedoraproject.org> - 1.0.1-7
- rebuild(sip)

* Sun Aug 22 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-6
- -devel: Requires: glew-devel python-devel boost-devel

* Sun Aug 22 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-5
- use CMAKE_SKIP_RPATH because we end up with rpaths otherwise
- disable tests because they don't work in Koji anyway

* Sun Aug 22 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.1-4
- fix paths in AvogadroConfig.cmake
- fix packaging of translations

* Wed Aug 04 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.0.1-3
- Rebuild for Boost soname bump

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 1.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Mon May 24 2010 Rex Dieter <rdieter@fedoraproject.org> - 1.0.1-1
- avogadro-1.0.1
- -devel: move cmake-related files here
- -devel: Req: qt4-devel
- %%files: track lib sonames
- %%check section

* Wed Feb 10 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.0-6
- reenable plugin builds and libs subpackage
- fix their build with sip 4.10

* Fri Jan 08 2010 Sebastian Dziallas <sebastian@when.com> - 1.0.0-5
- disable plugin builds and libs subpackage

* Thu Jan 07 2010 Rex Dieter <rdieter@fedoraproject.org> - 1.0.0-4
- rebuild (sip)

* Mon Nov 23 2009 Rex Dieter <rdieter@fedoraproject.org> - 1.0.0-3 
- rebuild (for qt-4.6.0-rc1, f13+)

* Mon Nov 16 2009 Rex Dieter <rdieter@fedoraproject.org> - 1.0.0-2 
- -libs: Requires: sip-api(%%_sip_api_major) >= %%_sip_api (#538124)
- Requires: %%{name}-libs ...

* Thu Oct 29 2009 Sebastian Dziallas <sebastian@when.com> 1.0.0-1
- update to new upstream release

* Tue Oct 20 2009 Rex Dieter <rdieter@fedoraproject.org> 0.9.8-3 
- rebuild (eigen2)

* Sat Sep 26 2009 Sebastian Dziallas <sebastian@when.com> 0.9.8-2
- fix typo in file list

* Sat Sep 26 2009 Sebastian Dziallas <sebastian@when.com> 0.9.8-1
- update to new upstream release
- enable python and glsl support again

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Jun 06 2009 Sebastian Dziallas <sebastian@when.com> 0.9.6-1
- new upstream release to fix issue with qt 4.5.0 and earlier

* Wed Jun 03 2009 Sebastian Dziallas <sebastian@when.com> 0.9.5-3
- remove remaining python parts
- fix files section finally

* Wed Jun 03 2009 Sebastian Dziallas <sebastian@when.com> 0.9.5-2
- disable python and glsl for now
- fix files section

* Wed Jun 03 2009 Sebastian Dziallas <sebastian@when.com> 0.9.5-1
- update to new release

* Tue May 12 2009 Sebastian Dziallas <sebastian@when.com> 0.9.4-1
- update to new release

* Fri Apr 10 2009 Sebastian Dziallas <sebastian@when.com> 0.9.3-1
- update to new release

* Sat Mar 21 2009 Sebastian Dziallas <sebastian@when.com> 0.9.2-2
- get desktop file properly installed

* Sat Mar 14 2009 Sebastian Dziallas <sebastian@when.com> 0.9.2-1
- update to new release

* Wed Feb 25 2009 Sebastian Dziallas <sebastian@when.com> 0.9.1-1
- update to new release

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 22 2009 Sebastian Dziallas <sebastian@when.com> 0.9.0-1
- update to new release

* Sun Sep 14 2008 Sebastian Dziallas <sebastian@when.com> 0.8.1-6
- add handbook to docs

* Sun Sep 14 2008 Sebastian Dziallas <sebastian@when.com> 0.8.1-5
- add build requirements and fix group names

* Wed Aug 13 2008 Sebastian Dziallas <sebastian@when.com> 0.8.1-4
- fix typos

* Sat Aug  9 2008 Sebastian Dziallas <sebastian@when.com> 0.8.1-3
- reorganize spec file

* Sat Aug  9 2008 Sebastian Dziallas <sebastian@when.com> 0.8.1-2
- rename shared library package and structure spec file

* Sun Aug  3 2008 Sebastian Dziallas <sebastian@when.com> 0.8.1-1
- initial pacakging release based on PackMan release
