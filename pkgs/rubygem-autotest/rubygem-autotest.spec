# Generated from autotest-4.4.6.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname autotest

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: This is a stub gem to fix the confusion caused by autotest being part of the ZenTest suite
Name: rubygem-%{gemname}
Version: 4.4.6
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://www.zenspider.com/ZSS/Products/ZenTest/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(ZenTest) >= 4.4.1
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
This is a stub gem to fix the confusion caused by autotest being part
of the ZenTest suite.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.6-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.6-1m)
- update 4.4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.2-1m)
- update 4.4.2

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.1-2m)
- add program suffix
-- FIX ME
 
* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.1-1m)
- Initial package for Momonga Linux
 
