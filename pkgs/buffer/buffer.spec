%global momorel 11

Summary: general purpose buffer programm
Name: buffer

Version: 1.19
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: http://sunsite.org.uk/public/public/packages/buffer/%{name}-%{version}.shar
Patch0: buffer-1.19.patch
Patch1: buffer-1.19-gcc34.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://sunsite.org.uk/public/public/packages/buffer/

%description
This is a program designed to speed up writing tapes on remote tape
drives. After startup it splits itself into two processes.  The first 
process reads (and reblocks) from stdin into a shared memory buffer.  
The second writes from the shared memory buffer to stdout.  Doing it this way
means that the writing side effectly sits in a tight write loop and
doesn't have to wait for input.  Similarly for the input side.  It is
this waiting that slows down other reblocking processes, like dd.


%prep
%setup -q -c %{name}-%{version} -T
sh %{SOURCE0}

%patch0 -p1
%patch1 -p1 -b .gcc34

%build
make CFLAGS="%{optflags} -Wall"

%install
rm -rf %{buildroot}
install -m 755 -D buffer %{buildroot}%{_bindir}/buffer
install -m 644 -D buffer.man %{buildroot}%{_mandir}/man1/buffer.1

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README
%{_mandir}/man1/buffer.1*
%{_bindir}/buffer

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19-6m)
- rebuild against gcc43

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19-5m)
- add Patch1: buffer-1.19-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Tue Jul 20 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19-4m)
- NoSource: 0

* Fri Jul 16 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.19-3m)
- I Need This.

* Sat Nov 15 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (1.19-2m)
- change Source: URL
- change %%setup option
- add buffer-1.19.patch from RedHat9
- change Summary and %%escription
- use %%{_bindir}, %%{_mandir}
- change URL:
- remove "make clean"
- add CFLAGS to make
- change most of %%install section

* Fri Nov 14 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.19-1m)
- First Relese

