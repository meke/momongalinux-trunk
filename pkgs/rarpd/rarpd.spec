%global momorel 8

Summary: The RARP daemon.
Name: rarpd
Version: ss981107
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
Source0: ftp://ftp.inr.ac.ru/ip-routing/dhcp.bootp.rarp/rarpd-%{version}.tar.gz
Source1: rarpd.service
Source2: LICENSE
Patch0: rarpd-%{version}.patch
Patch1: rarpd-fd-leak.patch
Patch2: rarpd-sprintf.patch
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
BuildRequires: systemd-units
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
RARP (Reverse Address Resolution Protocol) is a protocol which allows
individual devices on an IP network to get their own IP addresses from the
RARP server.  Some machines (e.g. SPARC boxes) use this protocol instead
of e.g. DHCP to query their IP addresses during network bootup.
Linux kernels up to 2.2 used to provide a kernel daemon for this service,
but since 2.3 kernels it is served by this userland daemon.

You should install rarpd if you want to set up a RARP server on your
network.

%prep
%setup -q -n rarpd
%patch0 -p1 -b .ss981107
%patch1 -p1 -b .fd-leak
%patch2 -p1 -b .sprintf

%build
%ifarch s390 s390x
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
%else
export CFLAGS="$RPM_OPT_FLAGS -fpic"
%endif
export LDFLAGS="-pie -Wl,-z,relro,-z,now"
make CFLAGS="$CFLAGS"

cp %{SOURCE2} .

%install
mkdir -p ${RPM_BUILD_ROOT}%{_unitdir}
mkdir -p ${RPM_BUILD_ROOT}%{_sbindir}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8

install -m 644 %{SOURCE1} ${RPM_BUILD_ROOT}%{_unitdir}/rarpd.service
install -m 755 rarpd ${RPM_BUILD_ROOT}%{_sbindir}/rarpd
install -m 644 rarpd.8 ${RPM_BUILD_ROOT}%{_mandir}/man8/rarpd.8

%post
%systemd_post rarpd.service

%preun
%systemd_postun_with_restart rarpd.service

%postun
%systemd_postun_with_restart rarpd.service

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%doc README
%{_unitdir}/*
%{_sbindir}/rarpd
%{_mandir}/man8/*

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (ss981107-8m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (ss981107-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (ss981107-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (ss981107-5m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (ss981107-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (ss981107-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (ss981107-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (ss981107-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - ss981107-28
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - ss981107-27
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - ss981107-26.1
- Autorebuild for GCC 4.3

* Wed Aug 29 2007 Jiri Moskovcak <jmoskovc@redhat.com> - ss981107-25.1
- renamed patch file

* Wed Aug 29 2007 Jiri Moskovcak <jmoskovc@redhat.com> - ss981107-25
- Modified init script to allow user to set the rarpd options via /etc/sysconfig/rarpd
- Resolves: #218998

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - ss981107-24
- Rebuild for selinux ppc32 issue.

* Mon Jul 23 2007 Jiri Moskovcak <jmoskovc@redhat.com> - ss981107-23
- Init script rewrite to comply with LSB.
- Resolves: #247042

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - ss981107-22.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - ss981107-22.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - ss981107-22.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 26 2005 Phil Knirsch <pknirsch@redhat.com> ss981107-22
- Fixed and optimized loop with sprintf usage
- Added missing stdlib.h include

* Thu Jul 14 2005 Phil Knirsch <pknirsch@redhat.com> ss981107-21
- Fix for leak socket descriptors (#162000)

* Wed Mar 02 2005 Phil Knirsch <pknirsch@redhat.com> ss981107-20
- bump release and rebuild with gcc 4

* Fri Feb 18 2005 Phil Knirsch <pknirsch@redhat.com> ss981107-19
- rebuilt

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com> ss981107-18
- rebuilt

* Wed May 12 2004 Phil Knirsch <pknirsch@redhat.com> ss981107-17
- Enabled PIE compilation and linking.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> ss981107-13
- rebuild on all arches

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Mar 15 2001 Trond Eivind Glomsrod <teg@redhat.com>
- more generic i18n (#26555)

* Mon Feb  5 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- i18nize init scripts (#26086)

* Thu Aug 17 2000 Jeff Johnson <jbj@redhat.com>
- summaries from specspo.
- typo in init script (#16450).

* Thu Jul 20 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Preston Brown <pbrown@redhat.com>
- move initscript, condrestart magic

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Fri Jun 16 2000 Bill Nottingham <notting@redhat.com>
- don't run by default

* Fri Apr  7 2000 Jakub Jelinek <jakub@redhat.com>
- initial package
