%global         momorel 1

Summary:        Command line tool for setting up authentication from network services
Name:           authconfig
Version:        6.2.8
Release:        %{momorel}m%{?dist}
License:        GPLv2+
ExclusiveOS:    Linux
Group:          System Environment/Base
URL:            https://fedorahosted.org/authconfig
Source0:        https://fedorahosted.org/releases/a/u/%{name}/%{name}-%{version}.tar.bz2
## following line should be revived ASAP
NoSource:       0 
Patch1: authconfig-6.2.6-gdm-nolastlog.patch
Patch2: authconfig-6.2.8-no-gnome-screensaver.patch
Patch3: authconfig-6.2.8-wait-for-card.patch
Patch4: authconfig-6.2.8-translation-updates.patch
Patch5: authconfig-6.2.8-norestart.patch
Patch6: authconfig-6.2.8-notraceback.patch
Patch7: authconfig-6.2.8-restorecon.patch
Patch8: authconfig-6.2.8-sssd-enable.patch
Patch9: authconfig-6.2.8-translation-updates-2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       newt-python
Requires:       pam >= 0.99.10.0
Requires:       python
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  glib-devel
BuildRequires:  intltool
BuildRequires:  perl-XML-Parser
BuildRequires:  python
BuildRequires:  python-devel >= 2.7
Conflicts:      pam_krb5 < 1.49
Conflicts:      samba-common < 3.0
Conflicts:      samba-client < 3.0
Conflicts:      nss_ldap < 254

%description 
Authconfig is a command line utility which can configure a workstation
to use shadow (more secure) passwords.  Authconfig can also configure a
system to be a client for certain networked user information and
authentication schemes.

%package gtk
Summary: Graphical tool for setting up authentication from network services
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}, pygtk-libglade >= 2.4.0
Requires: usermode-gtk

%description gtk
Authconfig-gtk is a GUI program which can configure a workstation
to use shadow (more secure) passwords.  Authconfig-gtk can also configure
a system to be a client for certain networked user information and
authentication schemes.

%prep
%setup -q
%patch1 -p1 -b .nolastlog
%patch2 -p1 -b .no-gnome-screensaver
%patch3 -p1 -b .card
%patch4 -p1 -b .translations
%patch5 -p1 -b .norestart
%patch6 -p1 -b .notraceback
%patch7 -p1 -b .restorecon
%patch8 -p1 -b .sssd-enable
%patch9 -p1 -b .translations2

intltoolize --force --copy
autoreconf -fi

%build
CFLAGS="%{optflags} -fPIC"; export CFLAGS
%configure
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_libdir}/python*/site-packages/acutilmodule.a
rm -f %{buildroot}/%{_libdir}/python*/site-packages/acutilmodule.la
rm -f %{buildroot}/%{_datadir}/%{name}/authconfig-tui.py
ln -s authconfig.py %{buildroot}/%{_datadir}/%{name}/authconfig-tui.py

%find_lang %{name}
find %{buildroot}%{_datadir} -name "*.mo" | xargs ./utf8ify-mo

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING NOTES TODO README.samba3
%ghost %config(noreplace) %{_sysconfdir}/sysconfig/authconfig
%ghost %config(noreplace) %{_sysconfdir}/pam.d/system-auth-ac
%ghost %config(noreplace) %{_sysconfdir}/pam.d/password-auth-ac
###%ghost %config(noreplace) %{_sysconfdir}/pam.d//postlogin-ac
%{_sysconfdir}/pam.d//postlogin-ac
%ghost %config(noreplace) %{_sysconfdir}/pam.d/fingerprint-auth-ac
%ghost %config(noreplace) %{_sysconfdir}/pam.d/smartcard-auth-ac
%{_sbindir}/cacertdir_rehash
%{_sbindir}/authconfig
%{_sbindir}/authconfig-tui
%{_mandir}/man8/*
%{_mandir}/man5/*
%{_libdir}/python*/site-packages/acutilmodule.so
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/authconfig.py*
%{_datadir}/%{name}/authconfig-tui.py*
%{_datadir}/%{name}/authinfo.py*
%{_datadir}/%{name}/shvfile.py*
%{_datadir}/%{name}/dnsclient.py*
%{_datadir}/%{name}/msgarea.py*
%attr(700,root,root) %dir %{_localstatedir}/lib/%{name}

%files gtk
%defattr(-,root,root,-)
%{_bindir}/authconfig
%{_bindir}/authconfig-tui
%{_bindir}/authconfig-gtk
%{_bindir}/system-config-authentication
%{_sbindir}/authconfig-gtk
%{_sbindir}/system-config-authentication
%{_datadir}/%{name}/authconfig.glade
%{_datadir}/%{name}/authconfig-gtk.py*
%config(noreplace) %{_sysconfdir}/pam.d/authconfig-gtk
%config(noreplace) %{_sysconfdir}/pam.d/system-config-authentication
%config(noreplace) %{_sysconfdir}/security/console.apps/authconfig-gtk
%config(noreplace) %{_sysconfdir}/security/console.apps/system-config-authentication
%config(noreplace) %{_sysconfdir}/pam.d/authconfig
%config(noreplace) %{_sysconfdir}/pam.d/authconfig-tui
%config(noreplace) %{_sysconfdir}/security/console.apps/authconfig
%config(noreplace) %{_sysconfdir}/security/console.apps/authconfig-tui
%{_datadir}/applications/*
%{_iconsdir}/hicolor/*/apps/*.png

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.8-1m)
- version 6.2.8

* Sat Jun  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.2.6-1m)
- version 6.2.6
- import two patches from fedora
- no NoSource for the moment

* Mon Nov  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.2.4-1m)
- update to 6.2.4

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2.2-1m)
- update to 6.2.2

* Sun Oct  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.1.16-1m)
- update 6.1.16

* Mon Jun 27 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.1.14-2m)
- modify spec to enable console login

* Fri Jun 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.1.14-1m)
- update 6.1.14

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.1.13-1m)
- update 6.1.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1.4-2m)
- full rebuild for mo7 release

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.4-1m)
- update to 6.1.4

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4.10-3m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.10-1m)
- sync with Fedora 11 (5.4.10-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.3-5m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4.3-4m)
- rebuild against python-2.6.1

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.3-3m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Sep  4 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.4.3-2m)
- add temporary directory (%%{_localstatedir}/lib/%%{name})

* Wed Jul  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.3-1m)
- update to 5.4.3
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.3.13-3m)
- rebuild against gcc43

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.3.13-2m)
- add intltoolize (intltool-0.36.0)

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.3.13-1m)
- update 5.3.13

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.2.3-4m)
- delete pyc pyo

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2.3-3m)
- rebuild against python-2.5

* Fri Sep 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.2.3-2m)
- remove category (Application) (modify patch0)

* Sat May 13 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2.3-1m)
- version up

* Sat Dec 17 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.13-3m)
- use autoreconf. 

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.6.13-2m)
- rebuild against python-2.4

* Mon Jun 27 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.13-1m)
- update 4.6.13

* Sun Mar 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.5-2m)
- revise Patch0: authconfig-4.6.5-desktop.diff

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (4.6.5-1m)
- ver up. Sync with FC3(4.6.5-3.1).
-- add patch0(authconfig-4.6.1-desktop.diff)
-- change desktop categories and execute command

* Sat Feb 19 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.6.1-4m)
- add patch0(authconfig-4.6.1-desktop.diff)
- change desktop categories and execute command

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.6.1-3m)
- rebuild against python2.3

* Sat May 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-2m)
- add BuildPrereq: desktop-file-utils for use desktop-file-install

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (4.6.1-1m)
- import from Fedora.

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.3.7-5m)
- rebuild against for glib-2.4.0

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (4.3.7-4m)
- revised spec for enabling rpm 4.2.

* Fri Mar  5 2004 Toru Hoshina <t@momonga-linux.org>
- (4.3.7-3m)
- rebuild against newt-0.51.6-1m

* Fri Dec 19 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.7-2m)
- rebuild against newt

* Thu Oct 16 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.7-1m)
- upgrade to 4.3.7

* Wed May 15 2002 Shingo Akagaki <dora@kondara.org>
- (4.2.8-4k)
- remove -gtk package

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (4.2.8-2k)
- uum...

* Thu Apr 18 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.8-4
- add missing translations back in
- convert .mo files at install-time

* Mon Apr 15 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.8-3
- refresh translations

* Wed Apr 10 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.8-2
- actually add the .desktop files

* Tue Apr  9 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.8-1
- refresh translations
- destroy the python object correctly

* Tue Mar 26 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.7-2
- add the .desktop file

* Mon Mar 25 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.7-1
- rework the auth stack logic to require all applicable auth modules

* Fri Mar  1 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.6-1
- allow pam_krb5afs to be used for account management, too

* Mon Feb 25 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.5-3
- refresh translations

* Fri Feb 22 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.5-2
- refresh translations

* Tue Feb 12 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.5-1
- actually free authInfo structures when asked to
- use pam_krb5's account management facilities
- conflict with versions of pam_krb5 which don't offer account management

* Mon Feb  4 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.4-1
- add python bindings for the back-end
- redo the gui so that it exercises the python bindings
- take a shot at getting authconfig to work in a firstboot container

* Thu Jan 31 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.3-4
- rebuild again

* Wed Jan 30 2002 Tim Powers <timp@redhat.com> 4.2.3-3
- rebuilt against new glib

* Wed Jan 23 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.3-2
- rebuild in new environment

* Thu Jan 10 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.3-1
- add some more experimental options
- clean up the glade files a bit
- don't destroy a garbage pointer on main cancel, destroy the main dialog

* Thu Jan  3 2002 Nalin Dahyabhai <nalin@redhat.com> 4.2.2-2
- bump release and rebuild

* Thu Dec 20 2001 Nalin Dahyabhai <nalin@redhat.com> 4.2.2-1
- make setting of experimental options only possible through
  /etc/sysconfig/authconfig, to keep accidents from happening
- add some more support for experimental stuff

* Tue Dec 11 2001 Nalin Dahyabhai <nalin@redhat.com> 4.2.1-1
- fix setting of LDAP TLS option in authconfig-gtk
- change Apply to Ok, Close to Cancel, because that's how they work

* Tue Dec 11 2001 Nalin Dahyabhai <nalin@redhat.com> 4.2-2
- add the glade XML file to the -gtk subpackage (fix from katzj)

* Mon Dec 10 2001 Nalin Dahyabhai <nalin@redhat.com> 4.2-1
- port to glib2
- move post code to the back-end
- add a libglade GUI in a -gtk subpackage
- set up to use userhelper

* Tue Nov 27 2001 Nalin Dahyabhai <nalin@redhat.com>
- remove pam_winbind from the list of session modules, because it doesn't
  provide a session-management interface

* Mon Sep 24 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.20-1
- make pam_localuser sufficient after pam_unix in account management, to allow
  local users in even if network connections to the LDAP directory are down (the
  network users should fail when pam_ldap returns a system error)

* Thu Sep  6 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.19-1
- translation refresh

* Tue Aug 28 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix assertion error hitting glib (#51798)
- allow multiple ldap servers to be specified (#49864)

* Fri Aug 24 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.18-1
- pam_ldap shouldn't be a mandatory module (#52531)
- refresh translations

* Thu Aug 23 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.17-1
- make pam_ldap required for account management when ldapauth is enabled
  (this requires pam_ldap 114 or later, but simplifies things)
- more translation updates

* Wed Aug 22 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.16-1
- warn about nscd the same way we do about nss_ldap and pam_krb5
- reorder some internal code so that it's easier to maintain
- change help string about the --probe option to make it clearer that using
  it doesn't actually set any options
- update translations from CVS

* Tue Aug 21 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.15-1
- set "pam_password md5" instead of "pam_password crypt" in ldap.conf if MD5
  is enabled

* Mon Aug 20 2001 Nalin Dahyabhai <nalin@redhat.com> 4.1.14-1
- right justify labels, and remove padding

* Fri Aug 17 2001 Nalin Dahyabhai <nalin@redhat.com>
- update translations from CVS, fixing #51873

* Thu Aug 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- set "pam_password crypt" in ldap.conf if not previously set
- update translations

* Mon Aug  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- don't mess with krb4 config files if we have no realm
- update translations

* Mon Jul 30 2001 Nalin Dahyabhai <nalin@redhat.com>
- use USESHADOW, USENIS, USEHESIOD, and USESMBAUTH variables to
  /etc/sysconfig/authconfig
- update translations

* Mon Jul  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- add "type=" to the list of arguments set up for pam_cracklib
- also modify /etc/krb.conf when configuring Kerberos (for compatibility)
- add --enablecache and --disablecache, which duplicates some of ntsysv's
  functionality, but it belongs here, too
- bravely try to carry on if bad options are passed in during kickstart

* Mon Jun 25 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix man page reference to file (/etc/sysconfig/authconfig, not auth) (#43344)
- own /etc/sysconfig/authconfig (#43344)
- fix spelling errors in Japanese message files (#15984)

* Tue Jun 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- rename --{enable,disable}smb to --{enable,disable}smbauth

* Thu May 31 2001 Nalin Dahyabhai <nalin@redhat.com>
- add --probe option to guess at LDAP and Kerberos configuration using DNS
- add preliminary support for SMB authentication

* Wed Feb 14 2001 Preston Brown <pbrown@redhat.com>
- final translation update.
- langify

* Mon Feb 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- errors connecting to LDAP also trigger service_err returns, so ignore on
  those as well

* Fri Feb  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- handle the case where the user doesn't specify a Kerberos realm, but
  enables it anyway
- update translations

* Wed Feb  7 2001 Nalin Dahyabhai <nalin@redhat.com>
- remove pam_access from the default configuration -- swat, pop, imap, etc.
  don't define a tty and pam_access bails if one isn't set

* Tue Feb  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- ignore on errors connecting to LDAP servers when doing LDAP account mgmt
  (probably less secure, but it allows root to log in when a wrong server
  name has been specified or the server is down)

* Mon Feb  5 2001 Nalin Dahyabhai <nalin@redhat.com>
- make account management in system-auth be an AND operation, but ignore
  user_unknown status for pam_ldap account management so that local root
  can log in (#26029)
- add pam_access and pam_env (#16170) to default configuration

* Tue Jan 24 2001 Preston Brown <pbrown@redhat.com>
- final translation update before Beta

* Tue Jan 24 2001 Nalin Dahyabhai <nalin@redhat.com>
- update translations
- make the entry fields on the second screen just a *little* bit smaller

* Fri Jan 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- really fix #23016 this time
- add buildprereqs on pam-devel, newt-devel, and glib-devel

* Wed Jan 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- match nss_ldap change of flag definitions for "ssl" flag ("on"=>"start_tls")
- change the "nothing-enabled" default so that we don't mistakenly think that
  NIS is enabled later on when it isn't supposed to be (#23327)
- only enable LDAP-related entry stuff on the appropriate screens (#23328)

* Sat Dec 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- make the copyright message translateable (#23016)

* Fri Dec 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- split the one big help message into multiple help messages (#23017)

* Wed Dec 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't write out configuration files for NIS, LDAP, Kerberos, Hesiod unless
  they're enabled when the user quits (we always write NSS, PAM, network)

* Fri Dec  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- make the internal code reflect the external use of "tls" instead of "ssl"

* Thu Dec  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- add support for toggling TLS on and off in /etc/ldap.conf

* Wed Nov 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't bother with USESHADOW; testing for /etc/shadow is sufficient
- use newtGrids to make NLS text fit (mostly)
- also edit "hosts:" to make sure it's there if nsswitch.conf is gone or broken
- preserve use of "db" and "nisplus" sources, even though we don't set them up

* Mon Nov 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- add the "nis" flag to pam_unix when NIS is enabled

* Wed Oct  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- read/write to /etc/syconfig/authconfig for PAM setup information

* Tue Aug 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't set "shadow" or "md5" for authentication with pam_unix, they're
  not needed (remove for clarity)
- add an authInfoCopy() routine to authinfo.c

* Mon Aug 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- edit /etc/openldap/ldap.conf in addition to /etc/ldap.conf

* Thu Aug 24 2000 Erik Troan <ewt@redhat.com>
- updated it and es translations

* Sun Aug 20 2000 Matt Wilson <msw@redhat.com>
- new translations

* Wed Aug  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- merge in new translations

* Tue Aug  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- add better error reporting for when Bill runs this on a read-only filesystem

* Fri Aug  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- change nss order from (hesiod,ldap,nis) to (nis,ldap,hesiod) in case shadow
  is in use
- kick nscd when we quit if it's running (and obey --nostart)

* Mon Jul 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- silently support the broken_shadow and bigcrypt flags for pam_unix
- only shut down ypbind if /var/run/ypbind exists

* Thu Jul 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- break some translations again

* Wed Jul 26 2000 Matt Wilson <msw@redhat.com>
- new translations for de fr it es

* Fri Jul 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix bug parsing NIS server names when there aren't any

* Thu Jul 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- also modify the services, protocols, and automount in nsswitch.conf
- add netgroups, too (#13824)

* Tue Jun 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- add --disable options
- try to not mess with ypbind if it isn't installed

* Tue Jun 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweak chkconfig magic for ypbind to work better
- turn on portmap when ypbind is enabled

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- only do chkconfig magic on ypbind if the ypbind init script exists

* Tue Jun 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix multiple-blank-line problem
- verify that NISDOMAIN is recorded properly in /etc/sysconfig/network

* Sat Jun 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- add calls to pam_limits in shared session stack

* Wed Jun  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix multiple realms section bug
- close all files we open
- bail on errors, even when we can see the file
- use RPM_OPT_FLAGS
- support multiple NIS servers
- warn if needed files aren't there

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix for false-matching beginnings of realm subsections
- FHS fixes

* Thu Jun  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- move default system-auth configuration to pam package

* Wed May 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- add default system-auth configuration

* Tue May 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix the uncommented comment problem
- pam_krb5 doesn't provide account management
- base DN can have spaces in it
- use pam_krb5afs for krb5 if /afs is readable
- add the tokens flag to pam_krb5afs
- break (user info and auth setup) into two screens

* Fri May 26 2000 Nalin Dahyabhai <nalin@redhat.com>
- finish LDAP support
- add Kerberos 5 support
- add Hesiod support
- migrate PAM config file logic to new method

* Wed Mar 08 2000 Cristian Gafton <gafton@redhat.com>
- rebuild for release

* Wed Feb 16 2000 Preston Brown <pbrown@redhat.com>
- disable LDAP, update man page.

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- beginning of /etc/pam.d writing, better man page, broadcast on by default.
- strip man page.

* Tue Jan 11 2000 Preston Brown <pbrown@redhat.com>
- support for LDAP authentication added.

* Tue Sep 21 1999 Matt Wilson <msw@redhat.com>
- updated man page

* Mon Sep 20 1999 Matt Wilson <msw@redhat.com>
- set up shadowed /etc/group

* Mon Aug  2 1999 Matt Wilson <msw@redhat.com>
- rebuilt against newt 0.50

* Mon Apr 19 1999 Cristian Gafton <gafton@redhat.com>
- release for Red Hat Linux 6.0

* Thu Apr 01 1999 Preston Brown <pbrown@redhat.com>
- don't report errors about NIS fields not being filled in if not enabled

* Fri Mar 26 1999 Preston Brown <pbrown@redhat.com>
- fix typo
- change domainname at nis start and stop

* Tue Mar 23 1999 Preston Brown <pbrown@redhat.com>
- fixed man page

* Wed Mar 17 1999 Matt Wilson <msw@redhat.com>
- fixed rewriting /etc/yp.conf
- restarts ypbind so that new changes take effect

* Mon Mar 15 1999 Matt Wilson <msw@redhat.com>
- just make the NIS part of configuration grayed out if NIS is not installed

* Tue Mar 09 1999 Preston Brown <pbrown@redhat.com>
- static buffer sizes increased.

* Tue Mar  9 1999 Matt Wilson <msw@redhat.com>
- removed build opts because of problems on alpha

* Mon Feb  8 1999 Matt Wilson <msw@redhat.com>
- Don't rewrite ypbind.conf if you're not configuring NIS

* Mon Feb  8 1999 Matt Wilson <msw@redhat.com>
- Don't configure NIS if /etc/ypbind.conf does not exist.

* Sat Feb  6 1999 Matt Wilson <msw@redhat.com>
- changed "/sbin/chkconfig --add ypbind" to
  "/sbin/chkconfig --level 345 ypbind on"
- added checks for null nis domains and servers if nis is enabled or if
  not using broadcast.
- added newt entry filter for spaces in domains

* Sat Feb  6 1999 Matt Wilson <msw@redhat.com>
- changed command line options to match user interface
- added --help

* Thu Feb  4 1999 Matt Wilson <msw@redhat.com>
- Rewrote UI to handle geometry management properly
- MD5 passwords do not require shadow passwords, so made them independent

* Wed Feb 03 1999 Preston Brown <pbrown@redhat.com>
- initial spec file
