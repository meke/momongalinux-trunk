%global momorel 2

###############################################################################
###############################################################################
##
##  Copyright (C) 2004-2011 Red Hat, Inc.  All rights reserved.
##
##  This copyrighted material is made available to anyone wishing to use,
##  modify, copy, or redistribute it subject to the terms and conditions
##  of the GNU General Public License v.2.
##
###############################################################################
###############################################################################

# keep around ready for later user
## global alphatag git0a6184070

Name: fence-agents
Summary: Fence Agents for Red Hat Cluster
Version: 3.1.7
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Base
URL: http://sourceware.org/cluster/wiki/
Source0: https://fedorahosted.org/releases/f/e/fence-agents/%{name}-%{version}.tar.xz
NoSource: 0

## Runtime deps
Requires: sg3_utils telnet openssh-clients
Requires: pexpect python-pycurl python-suds net-snmp-utils
Requires: perl-Net-Telnet

# This is required by fence_virsh. Per discussion on fedora-devel
# switching from package to file based require.
Requires: libvirt-client

# This is required by fence_ipmilan. it appears that the packages
# have changed Requires around. Make sure to get the right one.
Requires: OpenIPMI

## Setup/build bits

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

# Build dependencies
BuildRequires: perl python
BuildRequires: glibc-devel
BuildRequires: nss-devel nspr-devel
BuildRequires: libxslt pexpect python-pycurl python-suds

# explicitly provides fence_na.lib to be quiet mph-get-check
Provides: %{_datadir}/fence/fence_na.lib

%prep
%setup -q -n %{name}-%{version}

%build
%{configure}
CFLAGS="$(echo '%{optflags}')" make %{_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

## tree fix up
# fix libfence permissions
chmod 0755 %{buildroot}%{_datadir}/fence/*.py
# remove docs
rm -rf %{buildroot}/usr/share/doc/fence-agents

%clean
rm -rf %{buildroot}

%description
Red Hat Fence Agents is a collection of scripts to handle remote
power management for several devices.

%files 
%defattr(-,root,root,-)
%doc doc/COPYING.* doc/COPYRIGHT doc/README.licence
%config(noreplace) %{_sysconfdir}/cluster/fence_na.conf
%{_sbindir}/fence*
%{_datadir}/fence
%{_datadir}/cluster
%{_mandir}/man8/fence*

%changelog
* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.7-1m)
- version up 3.1.7

* Wed Jul  6 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.3-2m)
- explicitly provides fence_na.lib to be quiet mph-get-check

* Tue Jul  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.3-1m)
- version up 3.1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.7-4m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.7-3m)
- add BuildRequires

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.7-2m)
- explicitly link libnspr4

* Fri Jan 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7, sync with Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-0.1.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-0.1.2m)
- fix build failure with newer pacemaker

* Tue May 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-0.1.1m)
- import from Fedora 11

* Tue Mar 24 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-10.rc1
- New upstream release.
- Cleanup BuildRequires to avoid to pull in tons of stuff when it's not
  required.
- Update BuildRoot usage to preferred versions/names.
- Stop shipping powermib. Those are not required for operations anymore.

* Thu Mar 12 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-9.beta1
- Fix arch check for virt support.
- Drop unrequired BuildRequires.
- Drop unrequired Requires: on perl.

* Mon Mar  9 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-8.beta1
- New upstream release.
- Update corosync/openais BuildRequires and Requires.

* Fri Mar  6 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-7.alpha7
- New upstream release.
- Drop fence_scsi init stuff that's not required anylonger.

* Tue Mar  3 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-6.alpha6
- New upstream release.

* Tue Feb 24 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-5.alpha5
- Fix directory ownership.

* Tue Feb 24 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-4.alpha5
- Drop Conflicts with cman.

* Mon Feb 23 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-3.alpha5
- New upstream release. Also address comments from first package review.

* Thu Feb 19 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-2.alpha4
- Add comments on how to build this package.
- Update build depends on new corosynclib and openaislib.

* Thu Feb  5 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-1.alpha4
- New upstream release.
- Fix datadir/fence directory ownership.
- Update BuildRequires: to reflect changes in corosync/openais/cluster
  library split.

* Tue Jan 27 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-1.alpha3
- Initial packaging
