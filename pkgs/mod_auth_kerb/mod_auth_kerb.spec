%global momorel 7

Summary: Kerberos authentication module for HTTP
Name: mod_auth_kerb
Version: 5.4
Release: %{momorel}m%{?dist}
License: Modified BSD and MIT
Group: System Environment/Daemons
URL: http://modauthkerb.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/modauthkerb/%{name}-%{version}.tar.gz
NoSource: 0
Source1: auth_kerb.conf
Patch1: mod_auth_kerb-5.4-rcopshack.patch
Patch2: mod_auth_kerb-5.4-fixes.patch
Patch3: mod_auth_kerb-5.4-s4u2proxy.patch
Patch4: mod_auth_kerb-5.4-httpd24.patch
Patch5: mod_auth_kerb-5.4-delegation.patch
Patch6: mod_auth_kerb-5.4-cachedir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: httpd-devel >= 2.4.3, krb5-devel
Requires: httpd-mmn = %(cat %{_includedir}/httpd/.mmn || echo missing)

%description
mod_auth_kerb is module for the Apache HTTP Server designed to
provide Kerberos authentication over HTTP.  The module supports the
Negotiate authentication method, which performs full Kerberos
authentication based on ticket exchanges.

%prep
%setup -q -n %{name}-%{version}
%patch1 -p1 -b .rcopshack
%patch2 -p1 -b .fixes
%patch3 -p1 -b .s4u2proxy
%patch4 -p1 -b .httpd24
%patch5 -p1 -b .delegation
%patch6 -p1 -b .cachedir

%build
%configure --without-krb4 --with-krb5=%{_prefix} \
        --with-apache=%{_prefix}
make %{?_smp_mflags} 

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/httpd/modules \
         $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -m 755 src/.libs/mod_auth_kerb.so \
        $RPM_BUILD_ROOT%{_libdir}/httpd/modules/mod_auth_kerb.so
install -m 644 $RPM_SOURCE_DIR/auth_kerb.conf \
        $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/auth_kerb.conf.dist

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README
%config(noreplace) %{_sysconfdir}/httpd/conf.d/auth_kerb.conf.dist
%{_libdir}/httpd/modules/*.so

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4-7m)
- rebuild against httpd-2.4.3

* Sun Aug  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.4-6m)
- build fix (set krb5 direcroty)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4-1m)
- update to 5.4-1m
-- import patches from Fedora 11 (5.4-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.3-3m)
- rebuild against gcc43

* Sat Jun  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.3-2m)
- set PATH to %%{_sbindir}

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.3-1m)
- import from Fedora

* Wed Nov 29 2006 Joe Orton <jorton@redhat.com> 5.3-3
- fix r->user caching (Enrico Scholz, #214207)
- update to 5.3 (CVE-2006-5989, #215443)

* Sun Oct 01 2006 Jesse Keating <jkeating@redhat.com> - 5.1-3
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Sep 19 2006 Joe Orton <jorton@redhat.com> 5.1-2
- update to 5.1

* Thu Aug  3 2006 Joe Orton <jorton@redhat.com> 5.0-10
- fix segfault at startup (#201145)

* Thu Jul 20 2006 Joe Orton <jorton@redhat.com> 5.0-9
- add Russ Allbery's fix for disabling replay cache with krb15

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 5.0-8.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 5.0-8.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 5.0-8.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Dec  5 2005 Joe Orton <jorton@redhat.com> 5.0-8
- rebuild for httpd 2.2

* Wed Sep 21 2005 Joe Orton <jorton@redhat.com> 5.0-7
- fix build without /usr/sbin in $PATH (Roozbeh Pournader, #168212)

* Tue May 10 2005 Joe Orton <jorton@redhat.com> 5.0-6
- update to 5.0rc6
- don't force CC=gcc4

* Fri Mar  4 2005 Joe Orton <jorton@redhat.com> 5.0-3
- fix build with GCC 4
- only add "auth_kerb_module" symbol to dynamic symbol table

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Apr  8 2004 Joe Orton <jorton@redhat.com> 5.0-0.rc4.5
- remove static globals
- add SSLRequireSSL

* Mon Mar 29 2004 Joe Orton <jorton@redhat.com> 5.0-0.rc4.3
- support mutual authentication (Nalin Dahyabhai)
- once authentication returns COMPLETE, cache name for the duration
  of the connection

* Thu Mar 25 2004 Joe Orton <jorton@redhat.com> 5.0-0.rc4.2
- add example config file

* Wed Mar 24 2004 Joe Orton <jorton@redhat.com> 5.0-0.rc4.1
- update to mod_auth_kerb.c from HEAD to get workaround for
  "Request is a replay" errors

* Tue Mar 23 2004 Joe Orton <jorton@redhat.com> 
- Initial build.
