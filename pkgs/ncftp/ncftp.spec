%global momorel 2
%define _ipv6 0

Summary: An improved FTP client
Name: ncftp
Version: 3.2.5
Release: %{momorel}m%{?dist}
Prefix: %{_prefix}
License: Artistic
Group: Applications/Internet
URL: http://www.ncftpd.com/ncftp/
Source0: ftp://ftp.ncftp.com/ncftp/ncftp-%{version}-src.tar.bz2
NoSource: 0
Patch0: ncftp-v6.patch.bz2
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Ncftp is an improved FTP client.  Ncftp's improvements include support
for command line editing, command histories, recursive gets, automatic
anonymous logins and more.

Install ncftp if you use FTP to transfer files and you'd like to try
some of ncftp's additional features.

%prep
%setup -q
%if %{_ipv6}
%patch0 -p1 -b .v6
%endif

%build
#CFLAGS="%{optflags}" ./configure --prefix=%{prefix}
#autoconf --localdir=./autoconf
%if %{_ipv6}
%configure --enable-signals --enable-ipv6
%else
%configure --enable-signals
%endif
# cat %{PATCH1} | patch -p1 --suffix .configure
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/{bin,share/man/man1}

make prefix=%{buildroot}/usr BINDIR=%{buildroot}/usr/bin mandir=%{buildroot}%{_mandir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.txt doc
%{_bindir}/ncftp
%{_bindir}/ncftpget
%{_bindir}/ncftpput
%{_bindir}/ncftpbatch
%{_bindir}/ncftpls
%{_bindir}/ncftpbookmarks
%{_bindir}/ncftpspooler
%{_mandir}/man1/ncftp.1*
%{_mandir}/man1/ncftpget.1*
%{_mandir}/man1/ncftpput.1*
%{_mandir}/man1/ncftpbatch.1*
%{_mandir}/man1/ncftpspooler.1*
%{_mandir}/man1/ncftpls.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-2m)
- rebuild for new GCC 4.6

* Mon Feb  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.4-2m)
- full rebuild for mo7 release

* Thu May 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-2m)
- rebuild against rpm-4.6

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.1-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0
- use _bindir MACRO in %%files section
- revise %%files section (delete ncftpbench and add ncftpbatch)

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.1.9-1m)
- update to 3.1.9

* Sat Aug 14 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (3.1.8-2m)
- remove ncftp.desktop

* Tue Jul 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.8-1m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Sat Jan 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.7-1m)

* Fri Aug 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.6-1m)
- minor security fixes

* Mon Oct 14 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Thu Jul  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.4-1m)

* Tue Apr  2 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.1.3-2k)

* Thu Feb  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1.2-2k)
  update to 3.1.2

* Thu Dec 27 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1.1-2k)

* Thu Dec 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1.0-2k)

* Mon Oct 29 2001 TABUCHI Takaaki <tab@kondara.org>
- (3.0.4-2k)
- version up to 3.0.4
- add /usr/bin/ncftpspooler at %files
- comment out 'cat %{PATCH1} ...', patch1 is not found.
- ipv6 patch is broken.

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag
- fix ChangeLog to changelog

* Mon Aug 20 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (3.0.3-2k)

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.2-6k)
- errased IPv6 function with %{_ipv6} macro

* Fri Oct 20 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- remove configure patch

* Fri Oct 20 2000 Motonobu Ichimura <famao@kondara.org>
- up to 3.0.2
- modified kame pacth for 3.0.2

* Fri Oct 13 2000 Motonobu Ichimura <famao@kondara.org>
- up to ncftp-301-v6-20000906.diff.gz (http://www.kame.net)
- add ncftp-ipv6-config.patch 'cause of configure error (cannot autoconf)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed Jun 28 2000 Motonobu Ichimura <famao@kondara.org>
- merged ;-)

* Wed May 31 2000 KUSUNOKI Masanori <masanori@linux.or.jp>
- Enable IPv6

* Sat Apr 8 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- 3.01

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Wed Feb 16 2000 Shingo Akagaki <dora@kondara.org>
- fix .desktop entry

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-ncftp-20000115

* Sun Nov 28 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-ncftp-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Oct  4 1999 Bill Nottingham <notting@redhat.com>
- update to 3.0b21
- fix really broken desktop entry - where did that come from?

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- 3.0b19

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Wed Feb 24 1999 Bill Nottingham <notting@redhat.com>
- return of wmconfig

* Tue Feb 23 1999 Bill Nottingham <notting@redhat.com>
- 3.0b18

* Fri Feb 12 1999 Bill Nottingham <notting@redhat.com>
- 3.0b17

* Wed Dec  2 1998 Bill Nottingham <notting@redhat.com>
- 3.0b16

* Wed Nov 18 1998 Bill Nottingham <notting@redhat.com>
- add docs

* Thu Nov  5 1998 Bill Nottingham <notting@redhat.com>
- update to 3.0beta15

* Thu Aug 13 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- compiled for Manhattan

* Fri Mar 20 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.4.3 for security reasons

* Wed Nov 05 1997 Donnie Barnes <djb@redhat.com>
- added wmconfig entry

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- fixed the spec file

* Fri Oct 10 1997 Erik Troan <ewt@redhat.com>
- updated to ncftp 2.4.2

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Mar 25 1997 Donnie Barnes <djb@redhat.com>
- Rebuild as Sun version didn't work.
