%global momorel 7
%global qtver 4.8.2
%global kdever 4.9.0
%global kdelibsrel 2m
%global pykde4rel 1m

Summary: Wake On Lan front end for KDE
Name: kwakeonlan
Version: 0.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://dev.sharpley.org.uk/page/software/kwakeonlan
Source0: http://dev.sharpley.org.uk/pages/software/%{name}/%{name}%{version}.tar.gz
Source1: %{name}.desktop
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: PyKDE4 >= %{kdever}-%{pykde4rel}
Requires: oxygen-icon-theme
BuildRequires: coreutils

%description
KWakeOnLan is a graphical front end for sending Wake On Lan packets to your computer.
WOL packets cause a target computer to wake up.
This appliation allows you to wake any computer from Mac address,
and also to store that address for future waking.

%prep
%setup -q -n %{name}%{version}

%build
# nothing to do...

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing...
mkdir -p %{buildroot}%{_kde4_bindir}
mkdir -p %{buildroot}%{_kde4_datadir}/applications/kde4
mkdir -p %{buildroot}%{_kde4_appsdir}/%{name}

# install kwakeonlan
install -m 755 %{name}.py %{buildroot}%{_kde4_appsdir}/%{name}/
install -m 644 %{name}.ui %{buildroot}%{_kde4_appsdir}/%{name}/

# install _kde4_bindir/kwakeonlan
cat <<EOT > %{buildroot}%{_kde4_bindir}/%{name}
#!/bin/sh
cd %{_kde4_appsdir}/%{name}
./%{name}.py
EOT
chmod 755 %{buildroot}%{_kde4_bindir}/%{name}

# install desktop file
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_datadir}/applications/kde4/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-7m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-4m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-1m)
- initial package for WOL freaks using Momonga Linux
