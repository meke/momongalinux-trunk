%global momorel 12

%global fontname oflb-goudy-bookletter-1911
%global fontconf 61-%{fontname}.conf
%global fontforge_ver 20110222
%global fontforge_rel 2m.mo7

Name:		%{fontname}-fonts
Summary:	Clean serif font based on Kennerly Old Style
Version:	20080206
Release:	%{momorel}m%{?dist}
License:	Public Domain
Group:		User Interface/X
Source0:	http://openfontlibrary.org/people/chemoelectric/chemoelectric_-_Goudy_Bookletter_1.zip
Source1:	%{name}-fontconfig.conf
Source2:	GoudyBookletter1911.otf
URL:		http://openfontlibrary.org/media/files/chemoelectric/221
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	coreutils
BuildRequires:	fontpackages-devel
BuildRequires:  fontforge >= %{fontforge_ver}-%{fontforge_rel}
Requires:	fontpackages-filesystem
BuildArch:	noarch

%description
Based on the roman of Frederic Goudy's Kennerley Old Style (designed and cut in
1911 for a limited edition of "The Door in the Wall and Other Stories" by H G
Wells, published by Mitchell Kennerley). The letters, though not condensed, may
seem to fit together like pieces of a jigsaw puzzle, giving text an unusually
solid appearance.

%prep
%setup -q -c -n %{name}
sed -i 's|/home/trashman/bin/fontforge|/usr/bin/fontforge|g' make-otf.py

%build
# broken...
# ./generate-it.sh

install -m 644 %{SOURCE2} .

%install
rm -rf %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}
install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} %{buildroot}%{_fontconfig_confdir}
install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.otf

%changelog
* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20080206-12m)
- use ">= " instead of "=" to specify fontfoege version

* Sat Apr 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20080206-11m)
- import GoudyBookletter1911.otf from Fedora, fontforge's python support is broken

* Fri Apr 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20080206-10m)
- rebuild against fontforge-20110222-2m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080206-9m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20080206-8m)
- rebuild against fontforge-20090923-7m

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080206-7m)
- rebuild for new GCC 4.5

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20080206-6m)
- [BUILD FIX] specify Release of fontforge

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20080206-5m)
- full rebuild for mo7 release

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20080206-4m)
- specify fontforge version

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080206-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080206-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20080206-1m)
- import from Fedora

* Tue Mar 31 2009 Tom "spot" Callaway <tcallawa@redhat.com> 20080206-2
- rename package to oflb-goudy-bookletter-1911-fonts
- drop common_desc (unnecessary)
- fix fontconfig file
- use %%global rather than %%define
- continue to annoy people diffing spec files

* Sun Mar 29 2009 Tom "spot" Callaway <tcallawa@redhat.com> 20080206-1
- Initial package for Fedora
