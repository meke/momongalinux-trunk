%global momorel 3

%if %{?WITH_SELINUX:0}%{!?WITH_SELINUX:1}
%define WITH_SELINUX 1
%endif
Summary: A GNU file archiving program
Name: tar
Version: 1.26
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Archiving
URL: http://www.gnu.org/software/tar/
Source0: ftp://ftp.gnu.org/pub/gnu/tar/tar-%{version}.tar.xz
NoSource: 0
Source1: ftp://ftp.gnu.org/pub/gnu/tar/tar-%{version}.tar.xz.sig
#Manpage for tar and gtar, a bit modified help2man generated manpage
Source2: tar.1
#Stop issuing lone zero block warnings
Patch1: tar-1.14-loneZeroWarning.patch
#Fix extracting sparse files to a filesystem like vfat,
#when ftruncate may fail to grow the size of a file.(#179507)
Patch2: tar-1.15.1-vfatTruncate.patch
#Add support for selinux, acl and extended attributes
Patch3: tar-1.24-xattrs.patch
#change inclusion defaults of tar to "--wildcards --anchored
#--wildcards-match-slash" for compatibility reasons (#206841)
Patch4: tar-1.17-wildcards.patch
#ignore errors from setting utime() for source file
#on read-only filesystem (#500742)
Patch5: tar-1.22-atime-rofs.patch
#oldarchive option was not working(#594044)
Patch6: tar-1.23-oldarchive.patch
#temporarily disable sigpipe.at patch (fails at build in koji, passes manually)
Patch7: tar-sigpipe.patch

# build fix 'gets' undeclared here
Patch8: tar-1.26-stdio-gets.patch

Patch100: tar-1.24-bzip2_lzop_lzma.patch

Requires(post): info
Requires(preun): info
BuildRequires: autoconf automake gzip texinfo gettext libacl-devel gawk rsh
%if %{WITH_SELINUX}
BuildRequires: libselinux-devel
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info

%description
The GNU tar program saves many files together in one archive and can
restore individual files (or all of the files) from that archive. Tar
can also be used to add supplemental files to an archive and to update
or list files in the archive. Tar includes multivolume support,
automatic archive compression/decompression, the ability to perform
remote archives, and the ability to perform incremental and full
backups.

If you want to use tar for remote backups, you also need to install
the rmt package.

%prep
%setup -q
%patch1 -p1 -b .loneZeroWarning
%patch2 -p1 -b .vfatTruncate
%patch3 -p1 -b .xattrs
%patch4 -p1 -b .wildcards
%patch5 -p1 -b .rofs
%patch6 -p1 -b .oldarchive
%patch7 -p1 -b .fail
%patch8 -p1 -b .gets

%patch100 -p1

autoreconf

%build
%configure --bindir=/bin --libexecdir=/sbin \
%if %{WITH_SELINUX}
  --enable-selinux
%endif
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} bindir=/bin libexecdir=/sbin install

ln -s tar ${RPM_BUILD_ROOT}/bin/gtar
rm -f $RPM_BUILD_ROOT/%{_infodir}/dir
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man1
install -c -p -m 0644 %{SOURCE2} ${RPM_BUILD_ROOT}%{_mandir}/man1
ln -s tar.1 ${RPM_BUILD_ROOT}%{_mandir}/man1/gtar.1

# XXX Nuke unpackaged files.
rm -f ${RPM_BUILD_ROOT}/sbin/rmt

%find_lang %{name}

%check
rm -f %{buildroot}/test/testsuite
make check

%clean
rm -rf %{buildroot}

%post
if [ -f %{_infodir}/tar.info.gz ]; then
   /sbin/install-info %{_infodir}/tar.info %{_infodir}/dir || :
fi

%preun
if [ $1 = 0 ]; then
   if [ -f %{_infodir}/tar.info.gz ]; then
      /sbin/install-info --delete %{_infodir}/tar.info %{_infodir}/dir || :
   fi
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS ChangeLog ChangeLog.1 NEWS README THANKS TODO
%ifos linux
/bin/tar
/bin/gtar
%{_mandir}/man1/tar.1*
%{_mandir}/man1/gtar.1*
%else
%{_bindir}/*
%{_libexecdir}/*
%{_mandir}/man*/*
%endif

%{_infodir}/tar.info*

%changelog
* Sun Sep 29 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.29-3m)
- add Patch8: tar-1.26-stdio-gets.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-2m)
- rebuild for new GCC 4.6

* Sun Mar 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-2m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25
- remove unused patches
- import old archive patch from Fedora devel

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- [SECURITY] CVE-2010-0624 (fixed in 1.23)
- update to 1.24

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.22-7m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.22-6m)
- apply tar-1.17-bzip2_lzop_lzma.patch

* Sun Aug 22 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.22-5m)
- import patch from Fedora13

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-4m)
- [SECURITY] CVE-2010-0624
- import a security patch from Fedora 13 (2:1.22-16)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-2m)
- import a patch for -D_FORTIFY_SOURCE=2 from Rawhide (2:1.22-8)

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-1m)
- update to 1.22
-- support xz

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20-4m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20-3m)
- update Patch3,4 for fuzz=0
- License: GPLv3+

* Mon Dec 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20-2m)
- version down 1.20.
- failed test56

* Mon Dec 29 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.21-2m)
- fix %%check failure

* Mon Dec 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.21-1m)
- update to 1.21

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20, almost sync with Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-5m)
- rebuild against gcc43

* Fri Feb 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-4m)
- support gcc-4.3

* Mon Jan 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-3m)
- lzma support: update Patch10: tar-1.17-bzip2_lzop_lzma.patch

* Fri Dec 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-2m)
- extension is seen and operation is changed 

* Wed Dec 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- version down to 1.17
- lucene can not build using tar-1.19
- sync with Fedora devel

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Sun Nov  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.1-5m)
- [SECURITY] CVE-2007-4476
- add Patch9: tar-1.15.1-safer_name_suffix.patch

* Sat Aug 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.15-1-4m)
- [SECURITY] CVE-2007-4131tar directory traversal vulnerability
- add Patch8: tar-1.15.1-dot_dot_vuln.patch

* Sat Dec  2 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1-3m)
- [SECURITY] CVE-2006-6097
- add Patch7: tar-1.15.1-mangling.patch

* Wed Mar  8 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.15.1-2m)
- [SECURITY] CVE-2006-0300
-  add Patch6: tar-1.15.1-heapOverflow.patch

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.15.1-1m)
-  update to 1.15.1(based FC dev.)

* Wed Aug 18 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.14-2m)
- update Patch6: tar-1.14-nolibrt.patch

* Fri May 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14-1m)
- update to 1.14
- comment out Patch1: tar-1.13.90-sock.patch
- update Patch6: tar-1.14-nolibrt.patch
- Is is need configure --enable-backup-scripts? now not enable

* Sat Apr 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13.93-1m)
- update to 1.13.93

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.13.90-2m)
- revised spec for rpm 4.2.

* Sat Mar 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13.93-1m)
- update to 1.13.93

* Sun Nov 16 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13.90-1m)
- update to 1.13.90
- update patch1
- tmp comment out patch2 autoconf253 patch
- update patch6
- tmp comment out patch7
- update patch8
- coment out patch9 since included tarball
- tmp comment out patch11
- old patch10 + lzop patch = new patch10
- use rewrite bzip2_lzop patch, and delete bzip2andlzop patch

* Tue Oct  1 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.13.25-6m)
- [security] add tar-dots.patch.
  this patch fixes the following vulnerability.
  http://rhn.redhat.com/errata/RHSA-2002-096.html

* Fri Jul 26 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.13.25-5m)
- import from rawhide patch and source file

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.13.25-4k)
- Prereq: /sbin/install-info -> info

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (1.13.25-2k)

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Wed Jan 17 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.13.19

* Tue Nov  7 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.13.18

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul  7 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Mar  7 2000 MATSUDA, Daiki <dyky@df-usa.com>
- requires gzip and bzip2

* Wed Mar  1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_tar-20000115

* Wed Feb 20 2000 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P
- add barterly patch in order to handle bz2 automatically.

* Wed Feb  9 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix the exclude bug (#9201)

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- fix description
- fix fnmatch build problems

* Sun Jan  9 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.13.17
- remove dotbug patch (fixed in base)
- update download URL

* Fri Jan  7 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix a severe bug (tar xf any_package_containing_. would delete the
  current directory)

* Wed Jan  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.3.16
- unset LINGUAS before running configure

* Tue Nov  9 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.13.14
- Update man page to know about -I / --bzip
- Remove dependancy on rmt - tar can be used for anything local
  without it.

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 1.13.11.

* Wed Aug 18 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.13.9.

* Thu Aug 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.13.6.
- support -y --bzip2 options for bzip2 compression (#2415).

* Fri Jul 23 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.13.5.

* Tue Jul 13 1999 Bill Nottingham <notting@redhat.com>
- update to 1.13

* Sat Jun 26 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.12.64014.
- pipe patch corrected for remote tars now merged in.

* Sun Jun 20 1999 Jeff Johnson <jbj@redhat.com>
- update to tar-1.12.64013.
- subtract (and reopen #2415) bzip2 support using -y.
- move gtar to /bin.

* Tue Jun 15 1999 Jeff Johnson <jbj@redhat.com>
- upgrade to tar-1.12.64011 to
-   add bzip2 support (#2415)
-   fix filename bug (#3479)

* Mon Mar 29 1999 Jeff Johnson <jbj@redhat.com>
- fix suspended tar with compression over pipe produces error (#390).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Mon Mar 08 1999 Michael Maher <mike@redhat.com>
- added patch for bad name cache. 
- FIXES BUG 320

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Fri Dec 18 1998 Preston Brown <pbrown@redhat.com>
- bumped spec number for initial rh 6.0 build

* Tue Aug  4 1998 Jeff Johnson <jbj@redhat.com>
- add /usr/bin/gtar symlink (change #421)

* Tue Jul 14 1998 Jeff Johnson <jbj@redhat.com>
- Fiddle bindir/libexecdir to get RH install correct.
- Don't include /sbin/rmt -- use the rmt from dump.
- Turn on nls.

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 16 1997 Donnie Barnes <djb@redhat.com>
- updated from 1.11.8 to 1.12
- various spec file cleanups
- /sbin/install-info support

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Thu May 29 1997 Michael Fulbright <msf@redhat.com>
- Fixed to include rmt
