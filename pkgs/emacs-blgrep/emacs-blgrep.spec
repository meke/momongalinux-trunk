%global momorel 23
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Block Grep in Emacs
Name: emacs-blgrep
Version: 0.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Editors
URL: http://pop-club.hp.infoseek.co.jp/emacs/blgrep/
Source0: http://pop-club.hp.infoseek.co.jp/emacs/blgrep/blgrep-%{version}.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: blgrep-emacs
Obsoletes: blgrep-xemacs

Obsoletes: elisp-blgrep
Provides: elisp-blgrep

%description
Block Grep in Emacs.

%prep
%setup -q -n blgrep-%{version}

%build
%{__make} EMACS=emacs \
	  prefix=%{_prefix} \
	  datadir=%{_datadir} \
	  lispdir=%{e_sitedir}/blgrep

%install
rm -rf %{buildroot}

%{__make} EMACS=emacs \
	  prefix=%{buildroot}%{_prefix} \
	  datadir=%{buildroot}%{_datadir} \
	  lispdir=%{buildroot}%{e_sitedir}/blgrep install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog NEWS README README.ja
%{e_sitedir}/blgrep

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-23m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-22m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2-21m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-20m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-19m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-17m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-16m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-15m)
- merge blgrep-emacs to elisp-blgrep
- kill blgrep-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-13m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-12m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-11m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-10m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-9m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-8m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-6m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc43

* Wed Aug 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-1m)
- initial packaging
