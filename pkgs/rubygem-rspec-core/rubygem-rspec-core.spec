# Generated from rspec-core-2.9.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname rspec-core

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: rspec-core-2.9.0
Name: rubygem-%{gemname}
Version: 2.9.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: MIT
URL: http://github.com/rspec/rspec-core
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
BDD for Ruby. RSpec runner and example groups.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

mv %{buildroot}%{geminstdir}/exe %{buildroot}%{geminstdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/autospec
%{_bindir}/rspec
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.0-1m)
- update 2.9.0

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update 2.7.1

* Tue Sep  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-4m)
- add BuildRequires
- fix momorel

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.4-3m)
- cleanup spec

* Thu Sep  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.4-2m)
- fix %%files

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-1m)
- Initial package for Momonga Linux
