%global momorel 2
# Patch version?
%global snaprel %{nil}

# NOTE:  Try not to release new versions to released versions of Fedora
# You need to recompile all users of HDF5 for each version change
Name: hdf5
Version: 1.8.12
Release: %{momorel}m%{?dist}
Summary: A general purpose library and file format for storing scientific data
License: BSD
Group: System Environment/Libraries
URL: http://www.hdfgroup.org/HDF5/

Source0: http://www.hdfgroup.org/ftp/HDF5/releases/hdf5-%{version}%{?snaprel}/src/hdf5-%{version}%{?snaprel}.tar.bz2
NoSource: 0
Source1: h5comp
# For man pages
Source2: http://ftp.us.debian.org/debian/pool/main/h/hdf5/hdf5_%{version}-7.debian.tar.gz
NoSource: 2
Patch0: hdf5-LD_LIBRARY_PATH.patch
Patch1: hdf5-1.8.8-tstlite.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=925545
Patch2: hdf5-aarch64.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: krb5-devel, openssl-devel, zlib-devel, gcc-gfortran, time

%{?include_specopt}
%{?!with_mpich: %global with_mpich 1}
%{?!with_openmpi: %global with_openmpi 1}
%{?!runtest: %global runtest 0}

%if 0%{runtest}
# Needed for mpi tests
BuildRequires: openssh-clients
%endif

%if 0%{?rhel}
%ifarch ppc64
# No mpich2 on ppc64 in EL
%global with_mpich 0
%endif
%endif
%ifarch s390 s390x
# No openmpi on s390(x)
%global with_openmpi 0
%endif

%if %{with_mpich}
%global mpi_list mpich
%endif
%if %{with_openmpi}
%global mpi_list %{?mpi_list} openmpi
%endif

%description
HDF5 is a general purpose library and file format for storing scientific data.
HDF5 can store two primary objects: datasets and groups. A dataset is
essentially a multidimensional array of data elements, and a group is a
structure for organizing objects in an HDF5 file. Using these two basic
objects, one can create and store almost any kind of scientific data
structure, such as images, arrays of vectors, and structured and unstructured
grids. You can also mix and match them in HDF5 files according to your needs.


%package devel
Summary: HDF5 development files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: zlib-devel

%description devel
HDF5 development headers and libraries.


%package static
Summary: HDF5 static libraries
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
HDF5 static libraries.


%if %{with_mpich}
%package mpich
Summary: HDF5 mpich libraries
Group: Development/Libraries
Requires: mpich
BuildRequires: mpich-devel
Provides: %{name}-mpich2 = %{version}-%{release}
Obsoletes: %{name}-mpich2 < 1.8.11-4

%description mpich
HDF5 parallel mpich libraries


%package mpich-devel
Summary: HDF5 mpich development files
Group: Development/Libraries
Requires: %{name}-mpich%{?_isa} = %{version}-%{release}
Requires: mpich
Provides: %{name}-mpich2-devel = %{version}-%{release}
Obsoletes: %{name}-mpich2-devel < 1.8.11-4

%description mpich-devel
HDF5 parallel mpich development files


%package mpich-static
Summary: HDF5 mpich static libraries
Group: Development/Libraries
Requires: %{name}-mpich-devel%{?_isa} = %{version}-%{release}
Provides: %{name}-mpich2-static = %{version}-%{release}
Obsoletes: %{name}-mpich2-static < 1.8.11-4

%description mpich-static
HDF5 parallel mpich static libraries
%endif


%if %{with_openmpi}
%package openmpi
Summary: HDF5 openmpi libraries
Group: Development/Libraries
Requires: openmpi
BuildRequires: openmpi-devel >= 1.7.3

%description openmpi
HDF5 parallel openmpi libraries


%package openmpi-devel
Summary: HDF5 openmpi development files
Group: Development/Libraries
Requires: %{name}-openmpi%{_isa} = %{version}-%{release}
Requires: openmpi-devel

%description openmpi-devel
HDF5 parallel openmpi development files


%package openmpi-static
Summary: HDF5 openmpi static libraries
Group: Development/Libraries
Requires: %{name}-openmpi-devel%{?_isa} = %{version}-%{release}

%description openmpi-static
HDF5 parallel openmpi static libraries
%endif


%prep
#setup -q -n %{name}-%{version}%{?snaprel}
%setup -q -a 2
%patch0 -p1 -b .LD_LIBRARY_PATH
%ifarch ppc64 s390x
# the tstlite test fails with "stack smashing detected" on these arches
%patch1 -p1 -b .tstlite
%endif
%patch2 -p1 -b .aarch64
#This should be fixed in 1.8.7
find \( -name '*.[ch]*' -o -name '*.f90' -o -name '*.txt' \) -exec chmod -x {} +


%build
#Do out of tree builds
%global _configure ../configure
#Common configure options
%global configure_opts \\\
  --disable-silent-rules \\\
  --enable-fortran \\\
  --enable-fortran2003 \\\
  --enable-hl \\\
  --enable-shared \\\
%{nil}
# --enable-cxx and --enable-parallel flags are incompatible
# --with-mpe=DIR          Use MPE instrumentation [default=no]
# --enable-cxx/fortran/parallel and --enable-threadsafe flags are incompatible

#Serial build
export CC=gcc
export CXX=g++
export F9X=gfortran
export CFLAGS="${RPM_OPT_FLAGS/O2/O0}"
mkdir build
pushd build
ln -s ../configure .
%configure \
  %{configure_opts} \
  --enable-cxx
make
popd

#MPI builds
export CC=mpicc
export CXX=mpicxx
export F9X=mpif90
for mpi in %{mpi_list}
do
  mkdir $mpi
  pushd $mpi
  module load mpi/$mpi-%{_arch}
  ln -s ../configure .
  %configure \
    %{configure_opts} \
    --enable-parallel \
    --libdir=%{_libdir}/$mpi/lib \
    --bindir=%{_libdir}/$mpi/bin \
    --sbindir=%{_libdir}/$mpi/sbin \
    --includedir=%{_includedir}/$mpi-%{_arch} \
    --datarootdir=%{_libdir}/$mpi/share \
    --mandir=%{_libdir}/$mpi/share/man
  make
  module purge
  popd
done


%install
make -C build install DESTDIR=%{buildroot}
rm %{buildroot}/%{_libdir}/*.la
for mpi in %{mpi_list}
do
  module load mpi/$mpi-%{_arch}
  make -C $mpi install DESTDIR=%{buildroot}
  rm %{buildroot}/%{_libdir}/$mpi/lib/*.la
  module purge
done
#Fortran modules
mkdir -p %{buildroot}%{_fmoddir}
mv %{buildroot}%{_includedir}/*.mod %{buildroot}%{_fmoddir}
#Fixup example permissions
find %{buildroot}%{_datadir} \( -name '*.[ch]*' -o -name '*.f90' \) -exec chmod -x {} +

#Fixup headers and scripts for multiarch
%ifarch x86_64 ppc64 ia64 s390x sparc64 alpha
sed -i -e s/H5pubconf.h/H5pubconf-64.h/ %{buildroot}%{_includedir}/H5public.h
mv %{buildroot}%{_includedir}/H5pubconf.h \
   %{buildroot}%{_includedir}/H5pubconf-64.h
for x in h5c++ h5cc h5fc
do
  mv %{buildroot}%{_bindir}/${x} \
     %{buildroot}%{_bindir}/${x}-64
  install -m 0755 %SOURCE1 %{buildroot}%{_bindir}/${x}
done
%else
sed -i -e s/H5pubconf.h/H5pubconf-32.h/ %{buildroot}%{_includedir}/H5public.h
mv %{buildroot}%{_includedir}/H5pubconf.h \
   %{buildroot}%{_includedir}/H5pubconf-32.h
for x in h5c++ h5cc h5fc
do
  mv %{buildroot}%{_bindir}/${x} \
     %{buildroot}%{_bindir}/${x}-32
  install -m 0755 %SOURCE1 %{buildroot}%{_bindir}/${x}
done
%endif
# rpm macro for version checking
mkdir -p %{buildroot}%{_sysconfdir}/rpm
cat > %{buildroot}%{_sysconfdir}/rpm/macros.hdf5 <<EOF
# HDF5 version is
%_hdf5_version	%{version}
EOF

# Install man pages from debian
mkdir -p %{buildroot}%{_mandir}/man1
cp -p debian/man/*.1 %{buildroot}%{_mandir}/man1/


%check
%if 0%{runtest}
make -C build check
# disable parallel tests on s390(x) - something gets wrong in DNS resolver in glibc
# they are passed when run manually in mock
%ifnarch s390 s390x
export HDF5_Make_Ignore=yes
for mpi in %{mpi_list}
do
  module load mpi/$mpi-%{_arch}
  make -C $mpi check
  module purge
done
%endif
%endif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc COPYING MANIFEST README.txt release_docs/RELEASE.txt
%doc release_docs/HISTORY*.txt
%{_bindir}/gif2h5
%{_bindir}/h52gif
%{_bindir}/h5copy
%{_bindir}/h5debug
%{_bindir}/h5diff
%{_bindir}/h5dump
%{_bindir}/h5import
%{_bindir}/h5jam
%{_bindir}/h5ls
%{_bindir}/h5mkgrp
%{_bindir}/h5perf_serial
%{_bindir}/h5repack
%{_bindir}/h5repart
%{_bindir}/h5stat
%{_bindir}/h5unjam
%{_libdir}/*.so.*
%{_mandir}/man1/gif2h5.1*
%{_mandir}/man1/h52gif.1*
%{_mandir}/man1/h5copy.1*
%{_mandir}/man1/h5diff.1*
%{_mandir}/man1/h5dump.1*
%{_mandir}/man1/h5import.1*
%{_mandir}/man1/h5jam.1*
%{_mandir}/man1/h5ls.1*
%{_mandir}/man1/h5mkgrp.1*
%{_mandir}/man1/h5perf_serial.1*
%{_mandir}/man1/h5repack.1*
%{_mandir}/man1/h5repart.1*
%{_mandir}/man1/h5stat.1*
%{_mandir}/man1/h5unjam.1*

%files devel
%{_sysconfdir}/rpm/macros.hdf5
%{_bindir}/h5c++*
%{_bindir}/h5cc*
%{_bindir}/h5fc*
%{_bindir}/h5redeploy
%{_includedir}/*.h
%{_libdir}/*.so
%{_libdir}/*.settings
%{_fmoddir}/*.mod
%{_datadir}/hdf5_examples/
%{_mandir}/man1/h5c++.1*
%{_mandir}/man1/h5cc.1*
%{_mandir}/man1/h5fc.1*
%{_mandir}/man1/h5redeploy.1*

%files static
%{_libdir}/*.a

%if %{with_mpich}
%files mpich
%doc COPYING MANIFEST README.txt release_docs/RELEASE.txt
%doc release_docs/HISTORY*.txt
%{_libdir}/mpich/bin/gif2h5
%{_libdir}/mpich/bin/h52gif
%{_libdir}/mpich/bin/h5copy
%{_libdir}/mpich/bin/h5debug
%{_libdir}/mpich/bin/h5diff
%{_libdir}/mpich/bin/h5dump
%{_libdir}/mpich/bin/h5import
%{_libdir}/mpich/bin/h5jam
%{_libdir}/mpich/bin/h5ls
%{_libdir}/mpich/bin/h5mkgrp
%{_libdir}/mpich/bin/h5redeploy
%{_libdir}/mpich/bin/h5repack
%{_libdir}/mpich/bin/h5perf
%{_libdir}/mpich/bin/h5perf_serial
%{_libdir}/mpich/bin/h5repart
%{_libdir}/mpich/bin/h5stat
%{_libdir}/mpich/bin/h5unjam
%{_libdir}/mpich/bin/ph5diff
%{_libdir}/mpich/lib/*.so.*

%files mpich-devel
%{_includedir}/mpich-%{_arch}
%{_libdir}/mpich/bin/h5pcc
%{_libdir}/mpich/bin/h5pfc
%{_libdir}/mpich/lib/lib*.so
%{_libdir}/mpich/lib/lib*.settings

%files mpich-static
%{_libdir}/mpich/lib/*.a
%endif

%if %{with_openmpi}
%files openmpi
%doc COPYING MANIFEST README.txt release_docs/RELEASE.txt
%doc release_docs/HISTORY*.txt
%{_libdir}/openmpi/bin/gif2h5
%{_libdir}/openmpi/bin/h52gif
%{_libdir}/openmpi/bin/h5copy
%{_libdir}/openmpi/bin/h5debug
%{_libdir}/openmpi/bin/h5diff
%{_libdir}/openmpi/bin/h5dump
%{_libdir}/openmpi/bin/h5import
%{_libdir}/openmpi/bin/h5jam
%{_libdir}/openmpi/bin/h5ls
%{_libdir}/openmpi/bin/h5mkgrp
%{_libdir}/openmpi/bin/h5perf
%{_libdir}/openmpi/bin/h5perf_serial
%{_libdir}/openmpi/bin/h5redeploy
%{_libdir}/openmpi/bin/h5repack
%{_libdir}/openmpi/bin/h5repart
%{_libdir}/openmpi/bin/h5stat
%{_libdir}/openmpi/bin/h5unjam
%{_libdir}/openmpi/bin/ph5diff
%{_libdir}/openmpi/lib/*.so.*

%files openmpi-devel
%defattr(-,root,root,-)
%{_includedir}/openmpi-%{_arch}/*
%{_libdir}/openmpi/bin/h5pcc
%{_libdir}/openmpi/bin/h5pfc
%{_libdir}/openmpi/lib/lib*.so
%{_libdir}/openmpi/lib/lib*.settings

%files openmpi-static
%defattr(-,root,root,-)
%{_libdir}/openmpi/lib/*.a
%endif

%changelog
* Wed Jan 15 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.12-2m)
- update Source2

* Tue Jan 14 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.12-1m)
- update to 1.8.12
- switch to use mpich instead of mpich2
- disable %%check by default because it takes a very long time

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.10-2m)
- rebuild against mpich2-1.5

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.10-1m)
- update to 1.8.10
- rebuild against openmpi-1.6.3

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.9-2m)
- rebuild against mpich2-1.5rc1

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.9-1m)
- rebuild against openmpi-1.6
- update to 1.8.9

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.8-1m)
- update to 1.8.8
- add mich2 and openmpi sub packages
- sync with Fedora devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.4.patch1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.4.patch1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4.patch1-3m)
- full rebuild for mo7 release

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.4.patch1-2m)
- change CFLAGS from "$RPM_OPT_FLAGS -fno-strict-aliasing" to
  "${RPM_OPT_FLAGS/O2/O0}" because tests fail on x86_64

* Wed Mar  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.4.patch1-1m)
- update to 1.8.4-patch1
-- drop hdf5-1.8.3-detect.patch and hdf5-1.8.1-filter-as-option.patch
- revise spec
-- add REMOVE.PLEASE to avoid miss compilation
-- delete --with-ssl, which is no longer available

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3
-- import patches from Fedora 11 (1.8.3-1)
- enable fortran
- run test

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-3m)
- use -O1 when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-2m)
- rebuild against rpm-4.6

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2
-- remove configure option --enable-threadsafe, since --enable-cxx and
   --enable-threadsafe are incompatible (also --enable-fortran and --enable-parallel)
-- update Patch1,2
-- import and update Patch4 from Fedora devel (1.8.1-3)
-- drop Patch13, merged upstream
- add specopt runtest to run make check
  the default value is 0

* Wed Nov 12 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0-2m)
- delete -enable-fortran option from configure for fix build
- comment out about include/*.mod and h5fc

* Mon Apr 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0
- resync with Fedora
- * Fri Feb 29 2008 Orion Poplawski <orion@cora.nwra.com> 1.8.0-1
- - Update to 1.8.0, drop upstreamed patches
- - Update signal patch
- - Move static libraries into -static sub-package
- - Make -devel multiarch (bug #341501)
- * Wed Feb  6 2008 Orion Poplawski <orion@cora.nwra.com> 1.6.6-7
- - Add patch to fix strict-aliasing
- - Disable production mode to enable debuginfo
- * Tue Feb  5 2008 Orion Poplawski <orion@cora.nwra.com> 1.6.6-6
- - Add patch to fix calling free() in H5PropList.cpp
- * Tue Feb  5 2008 Orion Poplawski <orion@cora.nwra.com> 1.6.6-5
- - Add patch to support s390 (bug #431510)
- * Mon Jan  7 2008 Orion Poplawski <orion@cora.nwra.com> 1.6.6-4
- - Add patches to support sparc (bug #427651)
- * Tue Dec  4 2007 Orion Poplawski <orion@cora.nwra.com> 1.6.6-3
- - Rebuild against new openssl
- * Fri Nov 23 2007 Orion Poplawski <orion@cora.nwra.com> 1.6.6-2
- - Add patch to build on alpha (bug #396391)
- * Wed Oct 17 2007 Orion Poplawski <orion@cora.nwra.com> 1.6.6-1
- - Update to 1.6.6, drop upstreamed patches
- - Explicitly set compilers
- * Fri Aug 24 2007 Orion Poplawski <orion@cora.nwra.com> 1.6.5-9
- - Update license tag to BSD
- - Rebuild for BuildID
- * Wed Aug  8 2007 Orion Poplawski <orion@cora.nwra.com> 1.6.5-8
- - Fix memset typo
- - Pass mode to open with O_CREAT
- * Mon Feb 12 2007 Orion Poplawski <orion@cora.nwra.com> 1.6.5-7
- - New project URL
- - Add patch to use POSIX sort key option
- - Remove useless and multilib conflicting Makefiles from html docs
-   (bug #228365)
- - Make hdf5-devel own %{_docdir}/%{name}

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.5-5m)
- %%NoSource -> NoSource

* Sat Jan  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.5-4m)
- add patch for gcc43

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.5-3m)
- change Source URL

* Wed Mar 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.5-2m)
- update hdf5-1.6.5-flags.patch
--it becomes same config as gcc-4.1
-- gcc-4.2 was used "-O3"...

* Wed May 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.5-1m)
- import to Momonga for octave and octave-forge
- based on the package in Fedora Extras (1.6.5-5fc)

* Wed Mar 15 2006 Orion Poplawski <orion@cora.nwra.com> 1.6.5-5
- Change rpath patch to not need autoconf
- Add patch for libtool on x86_64
- Fix shared lib permissions

* Mon Mar 13 2006 Orion Poplawski <orion@cora.nwra.com> 1.6.5-4
- Add patch to avoid HDF setting the compiler flags

* Mon Feb 13 2006 Orion Poplawski <orion@cora.nwra.com> 1.6.5-3
- Rebuild for gcc/glibc changes

* Wed Dec 21 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.5-2
- Don't ship h5perf with missing library

* Wed Dec 21 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.5-1
- Update to 1.6.5

* Wed Dec 21 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-9
- Rebuild

* Wed Nov 30 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-8
- Package fortran files properly
- Move compiler wrappers to devel

* Fri Nov 18 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-7
- Add patch for fortran compilation on ppc

* Wed Nov 16 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-6
- Bump for new openssl

* Tue Sep 20 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-5
- Enable fortran since the gcc bug is now fixed

* Tue Jul 05 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-4
- Make example scripts executable

* Wed Jul 01 2005 Orion Poplawski <orion@cora.nwra.com> 1.6.4-3
- Add --enable-threads --with-pthreads to configure
- Add %check
- Add some %docs
- Use %makeinstall
- Add patch to fix test for h5repack
- Add patch to fix h5diff_attr.c

* Mon Jun 27 2005 Tom "spot" Callaway <tcallawa@redhat.com> 1.6.4-2
- remove szip from spec, since szip license doesn't meet Fedora standards

* Sun Apr 3 2005 Tom "spot" Callaway <tcallawa@redhat.com> 1.6.4-1
- inital package for Fedora Extras
