%global momorel 6

%define git_head cc6da5b
%define checkout 20081003
%define alphatag %{checkout}git%{git_head}

Name:           echo-icon-theme
Version:        0.3.89.0
Release:        0.11.%{alphatag}.%{momorel}m%{?dist}
Summary:        Echo icon theme

Group:          User Interface/Desktops
License:        "Creative Commons Attribution-ShareAlike 3.0"
URL:            http://fedoraproject.org/wiki/Artwork/EchoDevelopment
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  icon-naming-utils >= 0.8.7
BuildRequires:  autoconf automake
Requires(post): gtk2 >= 2.6.0
Requires(postun): gtk2 >= 2.6.0
#Requires:       gnome-icon-theme
Requires:       gnome-themes

%description
This package contains the Echo icon theme.

%prep
%setup -q

%build
#%%{_bindir}/autoreconf --install
%configure

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

touch %{buildroot}%{_datadir}/icons/Echo/icon-theme.cache

%post
touch --no-create %{_datadir}/icons/Echo || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/Echo || :

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog AUTHORS
%{_datadir}/icons/Echo
%ghost %{_datadir}/icons/Echo/icon-theme.cache

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.89.0-0.11.20081003gitcc6da5b.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.89.0-0.11.20081003gitcc6da5b.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.89.0-0.11.20081003gitcc6da5b.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.89.0-0.11.20081003gitcc6da5b.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.89.0-0.11.20081003gitcc6da5b.2m)
- rebuild against rpm-4.6

* Tue Nov 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.89.0-0.11.20081003gitcc6da5b.1m)
- New git snapshot sync with fedora

* Sun May 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.89.0-0.1.git51c57605.1m)
- import from Fedora to Momonga

* Sat May 10 2008 Martin Sourada <martin.sourada@gmail.com> - 0.3.89.0-0.1.git51c57605
- New git snapshot
- Introduce Autotools into Echo icon theme
- Use icon-naming-utils to create symlinks
- New icons

* Mon Jan 7 2008 Luya Tshimbalanga <luya@fedoraproject.org> - 0.3.1-2
- Fixes rhbz #244512
- Adds new icons

* Sat Nov 17 2007 Martin Sourada <martin.sourada@gmail.com> - 0.3.1-1
- add missing requires
- new version
- fixes rhbz #333231
- adds new icons

* Thu Oct 04 2007 Martin Sourada <martin.sourada@gmail.com> - 0.3-3.git
- Fix big icons issue

* Wed Oct  3 2007 Matthias Clasenb <mclasen@redhat.com> - 0.3-2.git
- Drop the redhat-artwork dependency  (#309661)

* Tue Oct 02 2007 Luya Tshimbalanga <luya_tfz@thefinalzone.com> - 0.3-1.git
- Newest snapshot
- Included cc-by-sa 3.0 file
- Dropped patch contexts.patch

* Wed Sep 26 2007 Luya Tshimbalanga <luya_tfz@thefinalzone.com> - 0.2.5.20070427wiki
- Updated license to cc-by-sa 3.0

* Mon Jun 04 2007 Luya Tshimbalanga <luya_tfz@thefinalzone.com> - 0.2.4.20070427wiki
- New snapshot

* Mon May 21 2007 Matthias Clasen <mclasen@redhat.com> - 0.2.3.20070419wiki
- Fix context information in index.theme (#217832)

* Thu Apr 19 2007 Matthias Clasen <mclasen@redhat.com> - 0.2.2.20070419wiki
- Drop scalable images again

* Tue Apr 17 2007 Matthias Clasen <mclasen@redhat.com> - 0.2.2.20070417wiki
- New snapshot
- Include scalable images

* Tue Mar 27 2007 Matthias Clasen <mclasen@redhat.com> - 0.2-2.20070326wiki
- Fix n-v-r ordering problem

* Mon Mar 26 2007 Matthias Clasen <mclasen@redhat.com> - 0.2-2.20070326wiki
- New snapshot

* Fri Feb 23 2007 Matthias Clasen <mclasen@redhat.com> - 0.2-2.20070223wiki
- New snapshot
- Fix some scriptlet issues
- Own the icon cache

* Tue Feb  6 2007 Matthias Clasen <mclasen@redhat.com> - 0.2-1.20070206wiki
- New snapshot

* Mon Feb  5 2007 Matthias Clasen <mclasen@redhat.com> - 0.1-7
- Neuter macros in %%changelog

* Thu Oct 26 2006 David Zeuthen <davidz@redhat.com> - 0.1-6
- Make this package own %%{_datadir}/icons/Echo
- Preserve timestamps
- Keep %%build around to document it's intentionally left empty
- Use %%{buildroot} instead of $RPM_BUILD_ROOT

* Thu Oct 26 2006 Luya Tshimbalanga <luya_tfz@thefinalzone.com> - 0.1-5
- Renamed the spec file to respect Packaging Guideline
- Included URL for source
- Cleaned up
- Ajusted permissions
- Removed unneeded build script 

* Wed Oct 25 2006 Luya Tshimbalanga <luya_tfz@thefinalzone.com> - 0.1-2
- Minor fixes

* Mon Oct 23 2006 Christopher Aillon <caillon@redhat.com> - 0.1-1
- Initial RPM.
