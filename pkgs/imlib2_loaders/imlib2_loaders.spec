%global momorel 11

Summary: Additional image loaders for Imlib2
Name: imlib2_loaders
Version: 1.4.1.000
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/pages/imlib2.html
Source0: http://download.enlightenment.org/snapshots/2008-01-25/%{name}-%{version}.tar.bz2
Nosource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: imlib2-devel >= 1.4.1.000-1m
BuildRequires: eet >= 1.2.3
Obsoletes: edb edb-ed edb-devel

%description
This package contains additional image loaders for Imlib2,
which for some reason (such as license issues) are not
distributed with Imlib2 directly.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/imlib2/loaders/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1.000-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1.000-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1.000-9m)
- full rebuild for mo7 release

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.1.000-8m)
- rebuild against new EFL

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1.000-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1.000-6m)
- rebuild against new EFL

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.1.000-5m)
- rebuild against new EFL

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.1.000-4m)
- rebuild against edb deletion

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.4.1.000-3m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1.000-2m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1.000-1m)
- merge from T4R
-
- changelog is below
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (1.4.1.000-1m)
- - update to 1.4.1.000

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2.001-3m)
- rebuild against gcc43

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2.001-2m)
- delete libtool library

* Sun Sep 24 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.2.001-1m)
- update to 1.2.2.001
- change Source0 url

* Sun Jan  9 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0

* Sun Aug 12 2001 Tom Gilbert <tom@linuxbrit.co.uk>
- Added db loader.

* Sat Aug 11 2001 Mark Bainter <mark-e@cymry.org>
- Corrected web URL.  
- Changed Copyright to mixed, since not all items
  in this package will be BSD Licensed.

* Wed Jul 04 2001 Mark Bainter <mark-e@cymry.org>
- Corrected ftp location.

* Wed Jun 28 2001 Mark Bainter <mark-e@cymry.org>
- Made package relocateable.

* Wed Jun 13 2001 Mark Bainter <mark-e@cymry.org>
- Cleanup and adjustments to reflect new ownership

* Tue May 22 2001 Christian Kreibich <cK@whoop.org>
- Initial spec file
