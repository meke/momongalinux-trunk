%global momorel 5
%define tarname mx
Summary: a widget toolkit using Clutter
Name: libmx
Version: 1.3.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: User Interface/Desktops
URL: http://www.clutter-project.org
Source0: http://source.clutter-project.org/sources/%{tarname}/1.3/%{tarname}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: startup-notification-devel
BuildRequires: dbus-glib-devel
BuildRequires: glade-devel
BuildRequires: gtk2-devel
BuildRequires: cogl-devel >= 1.10

%description
Mx is a widget toolkit using Clutter that provides a set of standard interface
elements, including buttons, progress bars, scroll bars and others. It also
implements some standard  managers. One other interesting feature is the
possibility setting style properties from a CSS format file.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{tarname}-%{version}

%build
%configure --enable-gtk-doc --enable-introspection=yes --with-glade3
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang mx-1.0

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f mx-1.0.lang
%defattr(-, root, root)
%doc AUTHORS COPYING.LIB ChangeLog NEWS README
%{_bindir}/mx-create-image-cache
%{_libdir}/libmx-1.0.so.*
%{_libdir}/libmx-gtk-1.0.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/Mx-1.0.typelib
%{_libdir}/girepository-1.0/MxGtk-1.0.typelib
%{_datadir}/mx

%files devel
%defattr(-, root, root)
%{_includedir}/mx-1.0
%{_libdir}/*.so
%{_libdir}/pkgconfig/mx-1.0.pc
%{_libdir}/pkgconfig/mx-gtk-1.0.pc
%{_datadir}/gtk-doc/html/mx-gtk
%{_datadir}/gir-1.0/Mx-1.0.gir
%{_datadir}/gir-1.0/MxGtk-1.0.gir

%doc %{_datadir}/gtk-doc/html/mx

%changelog
* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- do not use glade3-*-devel, which are now replaced by glade-devel

* Sat Jul  7 2012 Daniel Mclellan <daniel.mclellan@gmail.com>
- (1.3.1-4m)
- rebuild for glade3-libgladeui-devel

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-3m)
- rebuild for glade-devel

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-2m)
- rebuild for cogl-1.10.2

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.1-1m)
-update to 1.3.1

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-2m)
- rebuild against cogl-1.8.0

* Sat Aug  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
-update to 1.3.0

* Tue Apr 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- initial build
