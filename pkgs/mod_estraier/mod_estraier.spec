%global momorel 15

Name: mod_estraier
Summary: mod_estraier is an apache module that registers web pages processed by the apache and search from them using the node API of Hyper Estraier
Version: 0.3.2
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/modestraier/%{name}-%{version}.tar.gz 
NoSource: 0
License: GPLv2
URL: http://modestraier.sourceforge.net/index.ja.html
Group: System Environment/Daemons
BuildRequires: httpd-devel >= 2.4.3 hyperestraier-devel qdbm-devel >= 1.8.77
Requires: qdbm >= 1.8.77
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
mod_estraier is an apache module that registers web pages processed by the apache and search from them using the node API of Hyper Estraier

%prep
%setup -q -n %{name}

%build
%configure --with-apxs=%{_httpd_apxs}
#  --with-tidy=%{_bindir}/tidy


%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/httpd/modules
%{_httpd_apxs} -i -S LIBEXECDIR=%{buildroot}%{_libdir}/httpd/modules mod_estraier.la
%{_httpd_apxs} -i -S LIBEXECDIR=%{buildroot}%{_libdir}/httpd/modules mod_estraier_search.la
%{_httpd_apxs} -i -S LIBEXECDIR=%{buildroot}%{_libdir}/httpd/modules mod_estraier_cache.la

mkdir -p %{buildroot}%{_datadir}/mod_estraier/tmpl
cp tmpl/* %{buildroot}%{_datadir}/mod_estraier/tmpl

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc ChangeLog README README.ja TODO doc/recipes.conf
%{_libdir}/httpd/modules/*
%{_datadir}/mod_estraier/tmpl/

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-15m)
- rebuild against httpd-2.4.3

* Mon Mar 19 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-14m)
- rebuild against hyperestraier 1.4.13-11m
  
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-9m)
- fix BuildRequires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-7m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-6m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-5m)
- rebuild against qdbm-1.8.77

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-4m)
- rebuild against qdbm-1.8.75

* Wed Dec  6 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.3.2-3m)
- add tmplete

* Tue Dec  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-2m)
- add --with-apxs=%%{_sbindir}/apxs to configure

* Tue Dec  5 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.3.2-1m)
- initial commit
