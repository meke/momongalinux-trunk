%global momorel 1

Summary: This is the distribution of enhanced less. less - opposite of more
Name: less

Version: 458
Release: %{momorel}m%{?dist}
License: GPLv3+
URL: http://www.greenwoodsoftware.com/less/
Group: Applications/Text

Source0: http://www.greenwoodsoftware.com/less/%{name}-%{version}.tar.gz
NoSource: 0
Source1: lesspipe.sh
Source2: less.sh
Source3: less.csh
Patch1:	less-443-Foption.patch
Patch4: less-394-time.patch
Patch5: less-418-fsync.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires(prep): gzip, sharutils
BuildRequires(prep,build,install): coreutils
BuildRequires(build,install): make
BuildRequires(build): ncurses-devel
BuildRequires: pcre-devel >= 8.31
BuildRequires: autoconf

Requires: chkconfig  >= 1.3.6-1m
Requires: grep
Provides: pager

%description
The less utility is a text file browser that resembles more, but has
more capabilities.  Less allows you to move backwards in the file as
well as forwards.  Since less doesn't have to read the entire input file
before it starts, less starts up more quickly than text editors (for
example, vi). 

You should install less because it is a basic utility for viewing text
files, and you'll use it frequently.

%prep
%setup -q
%patch1 -p1 -b .Foption
%patch4 -p1 -b .time
%patch5 -p1 -b .fsync

chmod -R a+w *
chmod 644 lessecho.c lesskey.c version.c LICENSE

%build
%configure --with-regex=pcre
make CC="gcc %{optflags} -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64" datadir=%{_docdir}

%install
rm -rf %{buildroot}
%makeinstall
strip -R .comment %{buildroot}/%{_bindir}/less
mkdir -p %{buildroot}/etc/profile.d
install -p -c -m 755 %{SOURCE1} %{buildroot}/%{_bindir}
install -p -c -m 755 %{SOURCE2} %{buildroot}/etc/profile.d
install -p -c -m 755 %{SOURCE3} %{buildroot}/etc/profile.d
ls -la %{buildroot}/etc/profile.d

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE
/etc/profile.d/*
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Thu Apr 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (458-1m)
- update to 458

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (451-1m)
- update to 451

* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (444-2m)
- rebuild against pcre-8.31

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (444-1m)
- update to 444

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (443-1m)
- update to 443

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (436-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (436-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (436-4m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (436-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (436-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (436-1m)
- update to 436

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (429-1m)
- update to 429

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (424-3m)
- update lesspipe.sh for xz

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (424-2m)
- rebuild against rpm-4.6

* Sun Jul 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (424-1m)
- update 424

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (416-4m)
- rebuild against gcc43

* Fri Feb 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (416-3m)
- add lzma hack about Source1: lesspipe.sh

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (416-2m)
- %%NoSource -> NoSource

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (416-1m)
- sync with fc-devel (less-416-1_fc9)
- removed "Provides: pager"

* Mon May 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (394-1m)
- update to 394 (sync with FC)
- import some patches from FC

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (382-1m)
- sync with FC3(382-4).

* Wed Apr  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (358-20m)
- fix dependency

* Thu Nov 06 2003 Kenta MURATA <muraken2@nifty.com>
- (358-19m)
- pretty spec file.

* Wed Oct 29 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (358-18m)
- revise %%post and %%postun

* Tue Oct 28 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (358-17m)
- revise %%postun

* Thu Oct 23 2003 zunda <zunda at freeshell.org>
- (358-16m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Jan 25 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (358-15m)
- add %{_libdir}/less/less-pager for pager
   define LESS_PAGER if you'd like to override the default options for less-pager
- revice %%post and %%postun

* Fri Nov 29 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (358-14m)
- add version at requires chkconfig for alternatives

* Tue Nov 19 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (358-13m)
- adopt the alternatives system (requries chkconfig, provides pager).

* Tue Jan 22 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (358-12k)
- Should BuildPreReq: sharutils(uudecode).

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (358-10k)
- add locale name "ja_JP.EUC-JP" instead of eucjp.

* Sat May  5 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (358-8k)
- add locale name "ja_JP.eucjp" to charset.c

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (332-7k)
- change spec file with macro for compatibility

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Jun 11 2000 MATSUDA, Daiki <dyky@df-usa.com>
- change lespipe.sh enable bzip2

* Sat Apr  8 2000 Shingo Akagaki <dora@kondara.org>
- bug fix

* Tue Feb 29 2000 Shingo Akagaki <dora@kondara.org>
- japanese help message use only ja locale 

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- version up to man-pages-ja-GNU_less-20000115

* Thu Nov 25 1999 Masako Hattori <maru@kondara.org>
- version up to man-pages-ja-GNU_less-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Nov  3 1999 Tenkou N. Hattori <tnh@kondara.org>
- add man-pages-ja.

* Sun Sep 19 1999 Toru Hoshina<hoshina@best.com>
- rename to less-332.iso242.

* Tue Mar 16 1999 Toru Hoshina<hoshina@best.com>
- rebuild against rawhide 1.2.9
