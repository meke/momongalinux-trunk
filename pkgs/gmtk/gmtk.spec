%global         momorel 1

Name:           gmtk
Summary:        gnome-mplayer toolkit
Version:        1.0.7
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          System Environment/Libraries
URL:            http://code.google.com/p/gmtk/
Source0:        http://gmtk.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  alsa-lib-devel >= 0.7.0
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  glib2-devel
BuildRequires:  gtk2-devel
BuildRequires:  libgpod-devel >= 0.7.0
BuildRequires:  pkgconfig
BuildRequires:  pulseaudio-libs-devel

%description
gmtk - gnome-mplayer toolkit
Includes: libgmlib - a set of functions that support non-graphical operations
          libgmtk - a set of gtk widgets to use with gnome-mplayer 

%package        devel
Summary:        development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%__rm -rf --preserve-root %{buildroot}
%setup -q

%build
%configure \
    --disable-static \
    --with-gio \
    --with-alsa \
    --with-pulseaudio

%make

%install
%__rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -exec rm -f {} ';'
## delete duplicated documents
%__rm -rf %{buildroot}%{_docdir}/%{name}

%clean
%__rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/libgmlib.so.*
%{_libdir}/libgmtk.so.*
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/libgmlib.so
%{_libdir}/libgmtk.so
%{_libdir}/pkgconfig/gmlib.pc
%{_libdir}/pkgconfig/gmtk.pc

%changelog
* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- initial build for Momonga Linux