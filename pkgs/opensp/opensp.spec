%global momorel 11

Summary: SGML and XML parser
Name: opensp
Version: 1.5.2
Release: %{momorel}m%{?dist}
Requires: sgml-common >= 0.5
URL: http://openjade.sourceforge.net/
Source: http://download.sourceforge.net/openjade/OpenSP-%{version}.tar.gz
Patch0: opensp-multilib.patch
Patch1: opensp-nodeids.patch
Patch2: opensp-sigsegv.patch
License: MIT/X
Group: Applications/Text
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: xmlto, jadetex

%description
OpenSP is an implementation of the ISO/IEC 8879:1986 standard SGML
(Standard Generalized Markup Language). OpenSP is based on James
Clark's SP implementation of SGML. OpenSP is a command-line
application and a set of components, including a generic API.

%package devel
Summary: Files for developing applications that use OpenSP
Requires: %{name} = %{version}-%{release}
Group: Development/Libraries

%description devel
Header files and libtool library for developing applications that use OpenSP.

%prep
%setup -q -n OpenSP-%{version}
%patch0 -p1 -b .multilib
%patch1 -p1 -b .nodeids
%patch2 -p1 -b .sigsegv

%build
%configure --disable-dependency-tracking --disable-static --enable-http \
 --enable-default-catalog=%{_sysconfdir}/sgml/catalog \
 --enable-default-search-path=%{_datadir}/sgml:%{_datadir}/xml
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

# Fix up libtool libraries
find $RPM_BUILD_ROOT -name '*.la' | \
  xargs perl -p -i -e "s|-L$RPM_BUILD_DIR[\w/.-]*||g"

# oMy, othis ois osilly.
for file in nsgmls sgmlnorm spam spent sx ; do
   ln -s o$file $RPM_BUILD_ROOT%{_bindir}/$file
   echo ".so man1/o${file}.1" > $RPM_BUILD_ROOT%{_mandir}/man1/${file}.1
done

#
# Rename sx to sgml2xml.
mv $RPM_BUILD_ROOT%{_bindir}/sx $RPM_BUILD_ROOT%{_bindir}/sgml2xml
mv $RPM_BUILD_ROOT%{_mandir}/man1/{sx,sgml2xml}.1

#
# Clean out (installed) redundant copies of the docs and DTDs.
rm -rf $RPM_BUILD_ROOT%{_docdir}/OpenSP
rm -rf $RPM_BUILD_ROOT%{_datadir}/OpenSP

#rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la

%find_lang sp5

%check
make check || : # failures as of 1.5.2pre1 :(

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f sp5.lang
%defattr(-,root,root)
%doc doc/*.htm
%doc docsrc/releasenotes.html
%doc AUTHORS BUGS COPYING ChangeLog NEWS README
%doc pubtext/opensp-implied.dcl
%{_bindir}/*
%{_libdir}/libosp.so.*
%{_mandir}/man1/*.1*

%files devel
%defattr(-,root,root)
%{_includedir}/OpenSP/
%{_libdir}/libosp.so
%{_libdir}/libosp.la

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-9m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-6m)
- define __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.2-4m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-3m)
- import fedora patch

* Sun Apr 22 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.2-2m)
- revive .la files

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.2-1m)
- import from fc to Momonga
- delete .la files

* Mon Feb 12 2007 Tim Waugh <twaugh@redhat.com> 1.5.2-4
- Fixed build root.
- Give IDs to nodes in the release notes source to prevent releasenotes.html
  having multilib conflicts (bug #228320).

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.5.2-3.1
- rebuild

* Tue Jun 13 2006 Tim Waugh <twaugh@redhat.com> 1.5.2-3
- Fixed multilib fix (bug #194702).

* Fri May 26 2006 Tim Waugh <twaugh@redhat.com> 1.5.2-2
- Fixed multilib devel conflicts (bug #192741).

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.5.2-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.5.2-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Jan  5 2006 Tim Waugh <twaugh@redhat.com>  1.5.2-1
- 1.5.2.

* Tue Dec 14 2005 Tim Waugh <twaugh@redhat.com>  1.5.1-2
- Backported patch from 1.5.2pre1 to fix ArcEngine crash.

* Tue Dec 13 2005 Tim Waugh <twaugh@redhat.com>  1.5.1-1
- Back down to 1.5.1 for now.
- Fixes for GCC4.1.

* Sun Dec  4 2005 Ville Skytta <ville.skytta at iki.fi> - 1.5.2-0.1.pre1
- Fix build dependencies.
- Require exact version of main package in -devel.
- Build with dependency tracking disabled.
- Add %%{_datadir}/xml to default search path.
- Run test suite during build.
- Add URL tag.
- Use %%find_lang.
- Cosmetic improvements.

* Tue Nov 29 2005 Terje Bless <link@pobox.com> 1.5.2-0.pre1
- New package OpenSP.

