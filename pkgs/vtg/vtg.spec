%global momorel 2
%global major 0.12

Summary: Vala Toys for gEdit
Name: vtg
Version: %{major}.1
Release: %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License: GPLv2
Group: Applications/Editors
Source0: http://vtg.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: ja.po
Patch0: vtg-0.5.0-ja.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://code.google.com/p/vtg/

BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: gtk3-devel
BuildRequires: vala-devel >= 0.12.0
BuildRequires: gedit-devel >= 3.0.2
BuildRequires: GConf2-devel
BuildRequires: gtksourcecompletion-devel
Requires: gedit

%description
Vala Toys for gEdit (Vtg) is an experimental collection of plugins
that extend the gEdit editor to make it a better programmer editor
targetted especially for the Vala language.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{name}-%{version}%{?beta_tag:_%{beta_tag}}
cp %{SOURCE1} po
%patch0 -p1 -b .ja

%build
%configure --enable-silent-rules --disable-static LIBS=-lm
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING-GPL COPYING-LGPL ChangeLog ChangeLog.pre-2011
%doc MAINTAINERS NEWS README THANKS TODO
%{_bindir}/vala-gen-project
#%%{_bindir}/vsc-shell
%{_libdir}/libafrodite-%{major}.so.*
%exclude %{_libdir}/*.la

# fix me, plugins are arch depending ;-P
#%{_libdir}/gedit/plugins/*
%{_datadir}/gedit/plugins/*

%{_mandir}/man1/vala-gen-project.1.*
%{_datadir}/%{name}
%{_datadir}/GConf/gsettings/vala-toys.convert
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.vala-toys.gschema.xml
%{_datadir}/vala/vapi/afrodite-%{major}.vapi
%exclude %{_datadir}/doc/%{name}

%files devel
%defattr(-,root,root)
%dir %{_includedir}/afrodite-%{major}
%{_includedir}/afrodite-%{major}/afrodite.h
%dir %{_includedir}/gen-project-1.0
%{_includedir}/gen-project-1.0/gen-project.h
%{_libdir}/libafrodite-%{major}.so
%{_libdir}/pkgconfig/libafrodite-%{major}.pc

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-2m)
- rebuild for glib 2.33.2

* Tue Aug 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Wed Jul 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-0.4.1m)
- update to 0.12.0_beta4

* Sat Apr 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.1-1m)
- update to 0.11.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.0-2m)
- rebuild for new GCC 4.5

* Mon Sep 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3
- To main

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0.020100423-4m)
- full rebuild for mo7 release

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (0.7.0.020100423-3m)
- add %%dir

* Wed Apr 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0.020100423-2m)
- delete conflict file

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0.020100423-1m)
- update svn version

* Sat Nov 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 31 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.0-2m)
- add BR version, vala-devel >= 0.7.7

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initial build
