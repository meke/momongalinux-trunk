%global momorel 10

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _gcj_support 0

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define base_name       standard
%define short_name      taglibs-%{base_name}
%define jversion	1.1.1

%define section         free

Name:           jakarta-taglibs-standard
Version:        1.1.1
Release:        7jpp.%{momorel}m%{?dist}
Epoch:          0
Summary:        An open-source implementation of the JSP Standard Tag Library
License:        Apache
Group:          Development/Libraries
URL:            http://jakarta.apache.org/taglibs/
Source:         http://www.apache.org/dist/jakarta/taglibs/standard/source/jakarta-taglibs-standard-%{jversion}-src.tar.gz
Patch0:		jakarta-taglibs-standard-%{version}-build.patch
Patch1:		DataSourceWrapper_for_java6.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRequires:  jpackage-utils >= 0:1.5.30
BuildRequires:  ant
BuildRequires:  servletapi5 >= 0:5.0.16
BuildRequires:  tomcat5-jsp-2.0-api >= 0:5.0.16
BuildRequires:  xalan-j2 >= 2.6.0
Requires:       servletapi5 >= 0:5.0.16
Requires:       tomcat5-jsp-2.0-api >= 0:5.0.16
Requires:       xalan-j2 >= 2.6.0

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
This directory contains releases for the 1.1.x versions of the Standard
Tag Library, Jakarta Taglibs's open-source implementation of the JSP
Standard Tag Library (JSTL), version 1.1. JSTL is a standard under the
Java Community Process.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
BuildRequires:  java-javadoc
Requires(post): coreutils
Requires(postun): coreutils

%description    javadoc
Javadoc for %{name}.


%prep
%setup -q -n %{name}-%{jversion}-src
%patch0
%patch1
cat > build.properties <<EOBP
ant.build.javac.source=1.4
build.dir=build
dist.dir=dist
servlet24.jar=$(build-classpath servletapi5)
jsp20.jar=$(build-classpath jspapi)
jaxp-api.jar=$(build-classpath xalan-j2)
EOBP

%build

ant \
  -Dfinal.name=%{short_name} \
  -Dj2se.javadoc=%{_javadocdir}/java \
  -f standard/build.xml \
  dist


%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p standard/dist/standard/lib/jstl.jar $RPM_BUILD_ROOT%{_javadir}/jakarta-taglibs-core-%{version}.jar
cp -p standard/dist/standard/lib/standard.jar $RPM_BUILD_ROOT%{_javadir}/jakarta-taglibs-standard-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|jakarta-||g"`; done)
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr standard/dist/standard/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name} # ghost symlink


%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT


%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
  rm -f %{_javadocdir}/%{name}
fi

%post
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc standard/README_src.txt standard/README_bin.txt standard/dist/doc/doc/standard-doc/*.html
%{_javadir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/jakarta-taglibs-core-1.1.1.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/jakarta-taglibs-standard-1.1.1.jar.*
%endif

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}
%ghost %doc %{_javadocdir}/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-7jpp.10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-7jpp.9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-7jpp.8m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-7jpp.7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.1.1-7jpp.6m)
- add JDBC4.0 API method to build with OpenJDK

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-7jpp.5m)
- ant.build.javac.source=1.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-7jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-7jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-7jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.1-7jpp.1m)
- import from Fedora

* Thu Aug 10 2006 Matt Wringe <mwringe at redhat.com> 0:1.1.1-7jpp.1
- Merge with upstream version
 - Add missing javadoc postun
 - Add missing javadoc requires

* Thu Aug 10 2006 Karsten Hopp <karsten@redhat.de> 1.1.1-6jpp_3fc
- Requires(post): coreutils

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:1.1.1-6jpp_2fc
- Rebuilt

* Thu Jul 20 2006 Matt Wringe <mwringe at redhat.com> 0:1.1.1-6jpp_1fc
- Merge with upstream version
- Natively compile package

* Thu Jul 20 2006 Matt Wringe <mwringe at redhat.com> 0:1.1.1-6jpp
- Add conditional native compilation
- Add missing BuildRequires and Requires for tomcat5-jsp-2.0-api and xalan-j2
  (from Deepak Bhole <dbhole at redhat.com>)

* Thu Apr 27 2006 Fernando Nasser <fnasser@redhat.com> 0:1.1.1-5jpp
- First JPP 1.7 build

* Fri Oct 22 2004 Fernando Nasser <fnasser@redhat.com> 0:1.1.1-4jpp
- Rebuild to replace incorrect patch file

* Fri Oct 22 2004 Fernando Nasser <fnasser@redhat.com> 0:1.1.1-3jpp
- Remove hack for 1.3 Java that would break building with an IBM SDK.

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.1.1-2jpp
- Rebuild with ant-1.6.2

* Tue Jul 27 2004 Kaj J. Niemi <kajtzu@fi.basen.net> 0:1.1.1-1jpp
- 1.1.1

* Tue Feb 17 2004 Kaj J. Niemi <kajtzu@fi.basen.net> 0:1.1.0-1jpp
- 1.1.0 final

* Wed Jan 22 2004 David Walluck <david@anti-microsoft.org> 0:1.1.0-0.B1.2jpp
- change URL
- fix description

* Fri Jan  9 2004 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:1.1.0-0.B1.1jpp
- First build for JPackage

* Mon Dec 22 2003 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:1.1.0-0.B1.1
- First build
- Skip examples for now

