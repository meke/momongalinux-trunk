%global momorel 1
%global qtver 4.8.3
%global kdever 4.9.2
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global cmakever 2.6.2
%global cmakerel 1m
%global sourcedir stable/%{name}/%{version}/src

Summary: Very powerful Quake style Konsole
Name: yakuake
Version: 2.9.9
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://yakuake.kde.org/
Group: User Interface/X
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
Patch1: %{name}-2.9.3-replace-shortcut.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: libpng-devel
BuildRequires: libjpeg-devel

%description
A KDE konsole which looks like those found in Quake.

%prep
%setup -q

%patch1 -p1 -b .f10

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}
%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog KDE4FAQ NEWS README TODO
%{_kde4_bindir}/yakuake
%{_kde4_datadir}/applications/kde4/*.desktop
%{_kde4_datadir}/config/yakuake.knsrc
%{_kde4_appsdir}/kconf_update/yakuake*
%{_kde4_appsdir}/yakuake
%{_kde4_iconsdir}/*/*/*/*

%changelog
* Fri Oct 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.9-1m)
- update to 2.9.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.8-2m)
- rebuild for new GCC 4.6

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.8-1m)
- update to 2.9.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.7-4m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.7-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.7-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.7-1m)
- update to 2.9.7

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.6-4m)
- rebuild against qt-4.6.3-1m

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.6-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.6-1m)
- update to 2.9.6

* Wed May 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.5-1m)
- update to 2.9.5
- specify cmake version to use %%{cmake_kde4} macro

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.4-2m)
- rebuild against rpm-4.6

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.4-1m)
- version 2.9.4

* Sun Jul 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.3-2m)
- set toggle-window-state=F10
  to avoid conflicting with kerry and keep compatibility for upgrading from STABLE_4 

* Fri Jun  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.3-1m)
- update to 2.9.3

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-2m)
- rebuild against qt-4.4.0-1m

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-1m)
- update to 2.9.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.1-2m)
- rebuild against gcc43

* Thu Mar 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.1-1m)
- version 2.9.1

* Sun Mar  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9-1m)
- update to 2.9

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9-0.1.2m)
- re-add desktop.patch

* Sat Jan 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9-0.1.1m)
- update to 2.9-beta1 for KDE4

* Wed Oct  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-1m)
- version 2.8
- remove merged Japanese translation
- update desktop.patch
- remove getSessionName.diff
- License: GPLv2

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.5-6m)
- apply yakuake-getSessionName.diff
- https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=212862

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.5-5m)
- replace Source0

* Sat Oct 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.5-4m)
- import new admin/* from svn.kde.org for new autotools

* Sun Sep 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.7.5-3m)
- remove category TerminalEmulator

* Tue Jun 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.5-2m)
- modify initial settings

* Tue Jun 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.5-1m)
- initial package for Momonga Linux
- import yakuake-ja.po from SVN (kde.org)
- add desktop.patch
- Summary and %%description are imported from cooker
