%global momorel 1

Summary: NFS utilities and supporting clients and daemons for the kernel NFS server
Name: nfs-utils
URL: http://sourceforge.net/projects/nfs
Version: 1.2.9
Release: %{momorel}m%{?dist}

# group all 32bit related archs
%define all_32bit_archs i386 i686 athlon ppc sparcv9

Source0: http://dl.sourceforge.net/sourceforge/nfs/nfs-utils-%{version}.tar.bz2
NoSource: 0

Source9: id_resolver.conf
Source10: nfs.sysconfig
Source11: nfs-lock.service
Source12: nfs-secure.service
Source13: nfs-secure-server.service
Source14: nfs-server.service
Source15: nfs-blkmap.service
Source16: nfs-rquotad.service
Source17: nfs-mountd.service
Source18: nfs-idmap.service
Source19: nfs.target
%define nfs_services %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} %{SOURCE15} %{SOURCE16} %{SOURCE17} %{SOURCE18} %{SOURCE19}
#
# Services that need to be restarted.
#
%define nfs_start_services %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} %{SOURCE15} %{SOURCE18} %{SOURCE19}

Source20: var-lib-nfs-rpc_pipefs.mount
Source21: proc-fs-nfsd.mount
%define nfs_automounts %{SOURCE20} %{SOURCE21}

Source50: nfs-lock.preconfig
Source51: nfs-server.preconfig
Source52: nfs-server.postconfig
%define nfs_configs %{SOURCE50} %{SOURCE51} %{SOURCE52}

Patch001: nfs-utils-1.2.10-rc1.patch

Patch100: nfs-utils-1.2.1-statdpath-man.patch
Patch101: nfs-utils-1.2.1-exp-subtree-warn-off.patch
Patch102: nfs-utils-1.2.3-sm-notify-res_init.patch
Patch103: nfs-utils-1.2.5-idmap-errmsg.patch

Group: System Environment/Daemons
Provides: exportfs    = %{version}-%{release}
Provides: nfsstat     = %{version}-%{release}
Provides: showmount   = %{version}-%{release}
Provides: rpcdebug    = %{version}-%{release}
Provides: rpc.idmapd  = %{version}-%{release}
Provides: rpc.mountd  = %{version}-%{release}
Provides: rpc.nfsd    = %{version}-%{release}
Provides: rpc.statd   = %{version}-%{release}
Provides: rpc.gssd    = %{version}-%{release}
Provides: rpc.svcgssd = %{version}-%{release}
Provides: mount.nfs   = %{version}-%{release}
Provides: mount.nfs4  = %{version}-%{release}
Provides: umount.nfs  = %{version}-%{release}
Provides: umount.nfs4 = %{version}-%{release}
Provides: sm-notify   = %{version}-%{release}
Provides: start-statd = %{version}-%{release}

License: MIT and GPLv2 and GPLv2+
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rpcbind, sed, gawk, sh-utils, fileutils, textutils, grep
Requires: modutils >= 2.4.26-9
BuildRequires: nfs-utils-lib-devel >= 1.1.5
BuildRequires: libevent-devel >= 2.0.10 libgssglue-devel >= 0.3
BuildRequires: krb5-libs >= 1.4 autoconf >= 2.57 openldap-devel >= 2.2
BuildRequires: automake, libtool, keyutils-libs-devel, glibc-headers
BuildRequires: tcp_wrappers-devel, e2fsprogs-devel, krb5-devel
BuildRequires: perl
BuildRequires: libtirpc-devel
BuildRequires: libmount-devel
Requires(pre): shadow-utils >= 4.0.3-25
Requires(pre): chkconfig util-linux
Requires(pre): nfs-utils-lib >= 1.1.1 libevent >= 2.0.10 libgssglue
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
The nfs-utils package provides a daemon for the kernel NFS server and
related tools, which provides a much higher level of performance than the
traditional Linux NFS server used by most users.

This package also contains the showmount program.  Showmount queries the
mount daemon on a remote host for information about the NFS (Network File
System) server on the remote host.  For example, showmount can display the
clients which are mounted on that host.

This package also contains the mount.nfs and umount.nfs program.

%prep
%setup -q

%patch001 -p1

%patch100 -p1
%patch101 -p1
%patch102 -p1
%patch103 -p1

# Remove .orig files
find . -name "*.orig" | xargs rm -f

%build

%ifarch s390 s390x sparcv9 sparc64
PIE="-fPIE"
%else
PIE="-fpie"
%endif
export PIE

sh -x autogen.sh

CFLAGS="`echo $RPM_OPT_FLAGS $ARCH_OPT_FLAGS $PIE -D_FILE_OFFSET_BITS=64`"
%configure \
    CFLAGS="$CFLAGS" \
    CPPFLAGS="$DEFINES" \
    LDFLAGS="-pie" \
    --enable-mountconfig \
    --enable-ipv6 \
        --with-statdpath=/var/lib/nfs/statd \
        --enable-libmount-mount

make %{?_smp_mflags} all

%install
rm -rf %{buildroot}
mkdir -p $RPM_BUILD_ROOT%/sbin
mkdir -p $RPM_BUILD_ROOT%{_sbindir}
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_unitdir}/nfs.target.wants
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}/%{name}/scripts
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/request-key.d
mkdir -p $RPM_BUILD_ROOT/lib/modprobe.d/
make DESTDIR=$RPM_BUILD_ROOT install
install -s -m 755 tools/rpcdebug/rpcdebug $RPM_BUILD_ROOT%{_sbindir}
install -m 644 utils/mount/nfsmount.conf  $RPM_BUILD_ROOT%{_sysconfdir}
install -m 644 %{SOURCE9} $RPM_BUILD_ROOT%{_sysconfdir}/request-key.d
install -m 644 %{SOURCE10} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/nfs

for service in %{nfs_services} ; do
        install -m 644 $service $RPM_BUILD_ROOT%{_unitdir}
done

for service in %{nfs_automounts} ; do
        install -m 644 $service $RPM_BUILD_ROOT%{_unitdir}
done
for config in %{nfs_configs} ; do
        install -m 755 $config $RPM_BUILD_ROOT%{_libexecdir}/%{name}/scripts
done

cd $RPM_BUILD_ROOT%{_unitdir}
ln -s nfs-idmap.service rpcidmapd.service
ln -s nfs-lock.service nfslock.service
ln -s nfs-secure-server.service rpcsvcgssd.service
ln -s nfs-secure.service rpcgssd.service
ln -s nfs-server.service nfs.service

mkdir -p $RPM_BUILD_ROOT%{_sharedstatedir}/nfs/rpc_pipefs

touch $RPM_BUILD_ROOT%{_sharedstatedir}/nfs/rmtab
mv $RPM_BUILD_ROOT%{_sbindir}/rpc.statd $RPM_BUILD_ROOT/sbin

mkdir -p $RPM_BUILD_ROOT%{_sharedstatedir}/nfs/statd/sm
mkdir -p $RPM_BUILD_ROOT%{_sharedstatedir}/nfs/statd/sm.bak
mkdir -p $RPM_BUILD_ROOT%{_sharedstatedir}/nfs/v4recovery
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/exports.d


%clean
rm -rf %{buildroot}

%pre
# move files so the running service will have this applied as well
for x in gssd svcgssd idmapd ; do
    if [ -f /var/lock/subsys/rpc.$x ]; then
                mv /var/lock/subsys/rpc.$x /var/lock/subsys/rpc$x
    fi
done

/usr/sbin/useradd -l -c "RPC Service User" -r \
                -s /sbin/nologin -u 29 -d /var/lib/nfs rpcuser 2>/dev/null || :
/usr/sbin/groupadd -g 29 rpcuser 2>/dev/null || :

# Using the 16-bit value of -2 for the nfsnobody uid and gid
%define nfsnobody_uid   65534

# Create nfsnobody gid as long as it does not already exist
cat /etc/group | cut -d':' -f 1 | grep --quiet nfsnobody 2>/dev/null
if [ "$?" -eq 1 ]; then
    /usr/sbin/groupadd -g %{nfsnobody_uid} nfsnobody 2>/dev/null || :
else
    /usr/sbin/groupmod -g %{nfsnobody_uid} nfsnobody 2>/dev/null || :
fi

# Create nfsnobody uid as long as it does not already exist.
cat /etc/passwd | cut -d':' -f 1 | grep --quiet nfsnobody 2>/dev/null
if [ "$?" -eq 1 ]; then
    /usr/sbin/useradd -l -c "Anonymous NFS User" -r -g %{nfsnobody_uid} \
                -s /sbin/nologin -u %{nfsnobody_uid} -d /var/lib/nfs nfsnobody 2>/dev/null || :
else

   /usr/sbin/usermod -u %{nfsnobody_uid} -g %{nfsnobody_uid} nfsnobody 2>/dev/null || :
fi

%post
if [ $1 -eq 1 ]; then
        # Package install,
        /bin/systemctl enable nfs.target >/dev/null 2>&1 || :
        /bin/systemctl enable nfs-lock.service >/dev/null 2>&1 || :
        /bin/systemctl start nfs-lock.service >/dev/null 2>&1 || :
else
        # Package upgrade
        if /bin/systemctl --quiet is-enabled nfs.target ; then
                /bin/systemctl reenable nfs.target >/dev/null 2>&1 || :
        fi
        if /bin/systemctl --quiet is-enabled nfs-lock.service ; then
                /bin/systemctl reenable nfs-lock.service >/dev/null 2>&1 || :
        fi
fi
# Make sure statd used the correct uid/gid.
chown -R rpcuser:rpcuser /var/lib/nfs/statd

%preun
if [ $1 -eq 0 ]; then
        # Package removal, not upgrade
        for service in %(sed 's!\S*/!!g' <<< '%{nfs_start_services}') ; do
                %systemd_preun $service
        done
    /usr/sbin/userdel rpcuser 2>/dev/null || :
    /usr/sbin/groupdel rpcuser 2>/dev/null || :
    /usr/sbin/userdel nfsnobody 2>/dev/null || :
    /usr/sbin/groupdel nfsnobody 2>/dev/null || :
    rm -rf /var/lib/nfs/statd
    rm -rf /var/lib/nfs/v4recovery
fi

%postun
if [ $1 -ge 1 ]; then
        # Package upgrade, not uninstall
        for service in %(sed 's!\S*/!!g' <<< '%{nfs_start_services}') ; do
                /bin/systemctl try-restart $service >/dev/null 2>&1 || :
        done
fi
/bin/systemctl --system daemon-reload >/dev/null 2>&1 || :


%triggerun -- nfs-utils < 1.2.4-2
/bin/systemctl enable nfs-idmap.service >/dev/null 2>&1 || :
/bin/systemctl enable nfs-lock.service >/dev/null 2>&1 || :
if /sbin/chkconfig --level 3 nfs ; then
       /bin/systemctl enable nfs-server.service >/dev/null 2>&1 || :
fi
if /sbin/chkconfig --level 3 rpcgssd ; then
       /bin/systemctl enable nfs-secure.service >/dev/null 2>&1 || :
fi
if /sbin/chkconfig --level 3 rpcsvcgssd ; then
       /bin/systemctl enable nfs-secure-server.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root)
%config(noreplace) /etc/sysconfig/nfs
%config(noreplace) /etc/nfsmount.conf
%dir %{_sysconfdir}/exports.d
%dir %{_sharedstatedir}/nfs/v4recovery
%dir %{_sharedstatedir}/nfs/rpc_pipefs
%dir %{_sharedstatedir}/nfs
%dir %{_libexecdir}/%{name}/scripts
%dir %{_libexecdir}/%{name}
%dir %attr(700,rpcuser,rpcuser) %{_sharedstatedir}/nfs/statd
%dir %attr(700,rpcuser,rpcuser) %{_sharedstatedir}/nfs/statd/sm
%dir %attr(700,rpcuser,rpcuser) %{_sharedstatedir}/nfs/statd/sm.bak
#%%config(noreplace) %attr(644,rpcuser,rpcuser) %{_sharedstatedir}/nfs/state
#%%config(noreplace) %{_sharedstatedir}/nfs/xtab
#%%config(noreplace) %{_sharedstatedir}/nfs/etab
%config(noreplace) %attr(644,rpcuser,rpcuser) /var/lib//nfs/state
%config(noreplace) /var/lib/nfs/xtab
%config(noreplace) /var/lib/nfs/etab
%config(noreplace) /var/lib/nfs/rmtab
%config(noreplace) %{_sharedstatedir}/nfs/rmtab
%config(noreplace) %{_sysconfdir}/request-key.d/id_resolver.conf
%doc linux-nfs/ChangeLog linux-nfs/KNOWNBUGS linux-nfs/NEW linux-nfs/README
%doc linux-nfs/THANKS linux-nfs/TODO
/sbin/rpc.statd
/sbin/osd_login
%{_sbindir}/exportfs
%{_sbindir}/nfsstat
%{_sbindir}/rpcdebug
%{_sbindir}/rpc.mountd
%{_sbindir}/rpc.nfsd
%{_sbindir}/showmount
%{_sbindir}/rpc.idmapd
%{_sbindir}/rpc.gssd
%{_sbindir}/rpc.svcgssd
%{_sbindir}/sm-notify
%{_sbindir}/start-statd
%{_sbindir}/mountstats
%{_sbindir}/nfsiostat
%{_sbindir}/nfsidmap
%{_sbindir}/blkmapd
%{_sbindir}/nfsdcltrack
%{_mandir}/*/*
%{_unitdir}/*
%{_libexecdir}/%{name}/scripts/*

%attr(4755,root,root)   /sbin/mount.nfs
/sbin/mount.nfs4
/sbin/umount.nfs
/sbin/umount.nfs4

%changelog
* Fri Nov 22 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.9-1m)
- update 1.2.9

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-5m)
- add BuildRequires

* Fri Mar 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.5-4m)
- import some fixes and changes from fedora's nfs-utils-1.2.5-12

* Mon Dec  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-3m)
- import some fixes and changes from fedora's nfs-utils-1.2.5-6
-- rhbz#748275, rhbz#754496, rhbz#748275, rhbz#746497

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.5-2m)
- update nfs-server.service

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.5-1m)
- udpate 1.2.5

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-4m)
- update systemd serivice files

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-3m)
- support systemd

* Thu Jul 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-2m)
- fix build failure

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update 1.2.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-4m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-3m)
- rebuild against libevent-2.0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-2m)
- rebuild for new GCC 4.5

* Fri Oct  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- fix nfsd's v3 support bug
-- see http://developer.momonga-linux.org/kagemai/guest.cgi?action=view_report&id=300&project=momongaja

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2
- sync with Fedora devel

* Tue Jun  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-5m)
- add BuildRequires

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-4m)
- apply glibc212 patch

* Tue Nov 17 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.1-3m)
- added from fc devel
  Patch200: nfs-utils-1.2.0-v4root-rel8.patch
  Patch201: nfs-utils-1.2.1-nfsd-bootfail.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Nov  9 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.1-1m)
- sync with fc devel except init files

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.6-4m)
- stop daemon (rpcgssdm, rpcidmapd)

* Sat Jul 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-3m)
- stop auto-start of nfslock

* Wed May 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-2m)
- remove %%{epoch}

* Sat May 16 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.6-1m)
- sync with fc devel
- removed patches 107-117

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-4m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-3m)
- [SECURITY] http://secunia.com/advisories/33545/
- Fedora 9 Update: nfs-utils-1.1.2-9.fc9
- https://www.redhat.com/archives/fedora-package-announce/2009-January/msg00526.html
-
- merge fedora 1.1.4-12
- add Patch107: nfs-utils-1.1.4-mount-inet6-support.patch
- add Patch108: nfs-utils-1.1.4-svcgssd-expiration.patch
- add Patch109: nfs-utils-1.1.4-mount-po_get_numeric.patch
- add Patch110: nfs-utils-1.1.4-sm-notify-freeaddrinfo.patch
- add Patch111: nfs-utils-1.1.4-statd-xunlink.patch
- add Patch112: nfs-utils-1.1.4-tcpwrapper-update.patch
- add Patch113: nfs-utils-1.1.4-tcpwrap-warn.patch
- add Patch114: nfs-utils-1.1.4-gssd-verbosity.patch
- add Patch115: nfs-utils-1.1.4-mount-addrconfig.patch
- add Patch116: nfs-utils-1.1.4-configure-uuid.patch
- add Patch117: nfs-utils-1.1.4-configure-tirpc.patch

* Thu Dec 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-2m)
- merge fixes and changes from fc-devel (nfs-utils-1_1_4-6_fc11)

* Tue Oct 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- [SECURITY] CVE-2008-4552
- update to 1.1.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-3m)
- rebuild against gcc43

* Mon Mar 31 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-2m)
- add BuildRequires: nfs-utils-lib # for checking for nfs4_init_name_mapping in -lnfsidmap... no

* Sat Mar 22 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2
- patchs copied from fc devel:
  update Patch06: nfs-utils-1.1.0-exportfs-man-update.patch
  add    Patch07: nfs-utils-1.1.2-multi-auth-flavours.patch

* Sun Mar  2 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1.1-2m)
- require newer version of nfs-utils-libs

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1
- use libgssglue
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-7m)
- %%NoSource -> NoSource

* Sat Jan  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-6m)
- rebuild against libevent-1.4.1-0.1.1m
- import some patches from Fedora devel

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-5m)
- rebuild against libevent-1.3e

* Fri Sep 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-4m)
- rebuild against libevent-1.3d

* Thu Aug 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- added condstop to all the initscripts again

* Thu Aug 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- sync with fc-devel (nfs-utils-1_1_0-3_fc8)
-- update init scripts

* Thu Aug 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0 (sync with FC-devel)
- rebuild against libevent-1.3c

* Wed Aug  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-5m)
- fixed bugs in %%{_initscriptdir}/{nfs,nfslock} so that %%postun will not fail.

* Wed Jun 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.12-4m)
- stop auto start

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.12-3m)
- rebuild against libevent-1.3b

* Wed May 30 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-2m)
- sync fc-devel (nfs-utils-1_0_12-7_fc7)

* Mon May 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-1m)
- sync fc-devel 1.0.12

* Thu Feb 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-2m)
- sync fc-devel
-- add cache FS patch

* Wed Feb 21 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.10-1m)
- up to 1.0.10
- rebuild against libevent 1.3a
- rebuild against nfs-utils-libs 1.0.10

* Fri Jul 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-2m)
- stop all daemon for stable release

* Fri Jun  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.8-1m)
- sync with fc

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.7-1m)
- sync with fc-devel
- enable nfs v4

* Thu Jan 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.6-8m)
- build fix for rquotad

* Mon Jul 25 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.6-7m)
- modify nfs.init

* Wed Jul 20 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.6-6m)
- revert 1.0.6-2m

* Sun Apr 17 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.6-5m)
- stop rpcidmapd rpcgssd nfslock

* Sat Apr  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.6-4m)
- ummm, who's gonna mount /var/lib/nfs/rpc_pipefs?

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.6-3m)
- sync with FC3(1.0.6-52).
- nfs-utils-1.0.6-statddos_quotabuf.patch and
  nfs-utils-1.0.6-auth_reload.patch is applied as another name.

* Wed Dec  8 2004 TAKAHASHI Tamotsu <tamo>
- (1.0.6-2m)
- apply some of UBUNTU patches
 - Patch0: nfs-utils-1.0.6-statddos_quotabuf.patch
  (CAN-2004-1014: ignore SIGPIPE to fix a DoS vulnerability)
  (CAN-2004-0946: do not use memcpy to fix a buffer overflow on 64 bit platforms)
 - Patch1: nfs-utils-1.0.6-auth_reload.patch
  (call auth_reload more often)

* Sun Aug 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.6-1m)
- updated to 1.0.6

* Wed Jul 21 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.3-5m)
- add uid/gid

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.3-4m)
- revised spec. rpc.rquotad belongs quota package.

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.3-3m)
- revised spec for rpm 4.2.

* Wed Jul 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.3-2m)
- add nfsstat-overflow.patch

* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.3-1m)
  update to 1.0.3

* Wed Oct 24 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.3.3-8k)
- modify rpc.statd location to /sbin on init script

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (0.3.3-6k)
- require quota 3.0.1
- move rpc.statd to /sbin.

* Fri Oct 12 2001 Toru Hoshina <t@kondara.org>
- (0.3.3-4k)
- revised nfslock.init to use /usr/sbin/rpc.statd instead of /sbin...

* Sat Sep 29 2001 Toru Hoshina <t@kondara.org>
- (0.3.3-2k)
- nfs.statd...

* Fri Apr 13 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.3.1-2k)
- backport from Jirai

* Fri Apr 13 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.3.1-3k)
- upgrade to 0.3.1
- change source URL and URL: tag.
- remove rpc.statd, rpc.lockd (kernels >= 2.2.18+ have automatic NFS lock)
- enable NFSv3

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.1-9k)
- rebuild against glibc-2.2.2 and add nfs-utils.glibc222.time.patch

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.1-7k)
- rebuild againt rpm-3.0.5-39k

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Sun Nov 19 2000 AYUHANA Tomonori <l@kondara.org>
- (0.2.1-1k)
- upgrade 0.1.9.1-3k to 0.2.1-1k
- remove Distribution
- obey FHS

* Wed Jul 19 2000 AYUHANA Tomonori <l@kondara.org>
- (0.1.9.1-1k)
- ported from RedHat-6.2 Updates
- Be a NoSrc :-P
- rebuild against glibc-2.1.91, X-4.0, rpm-3.0.5
- SPEC fixed ( Distribution, BuildRoot, URL, Source )
- use %configure
- add SBINDIR and MAN*DIR at make install

* Mon Jul 17 2000 Matt Wilson <msw@redhat.com>
- 0.1.9.1
- remove patch0, has been integrated upstream

* Wed Feb  9 2000 Bill Nottingham <notting@redhat.com>
- the wonderful thing about triggers, is triggers are wonderful things...

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- switch to nfs-utils as the base tree
- fix the statfs patch for the new code base
- single package that obsoletes everything we had before (if I am to keep
  some traces of my sanity with me...)

* Mon Jan 17 2000 Preston Brown <pbrown@redhat.com>
- use statfs syscall instead of stat to determinal optimal blksize
