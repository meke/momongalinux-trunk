%global momorel 6

Summary: waei
Name: gwaei
License: GPLv3
Group: Applications/Text
Version: 1.4.0
Release: %{momorel}m%{?dist}
URL: http://gwaei.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk2-devel
BuildRequires: GConf2-devel
BuildRequires: libcurl-devel
BuildRequires: libgnome-devel
BuildRequires: libgnomeui-devel
BuildRequires: glib2-devel
Requires(pre): hicolor-icon-theme gtk2

%description
gWaei is a Japanese-English dictionary program for the GNOME
desktop. It is made to be a modern drop in replacement for Gjiten with
many of the same features. The dictionary files it uses are from Jim
Breen's WWWJDIC project and are installed separately through the
program.
#'

%prep
%setup -q

%build
%configure --disable-schemas-install --disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/gwaei.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gwaei.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gwaei.schemas \
	> /dev/null || :
fi

%files
%defattr(-,root,root)
%doc COPYING README
%{_sysconfdir}/gconf/schemas/gwaei.schemas
%{_bindir}/%{name}
%{_bindir}/waei
%{_libdir}/gwaei/kpengine
%{_datadir}/%{name}
%{_datadir}/doc/%{name}
%{_datadir}/applications/gwaei.desktop
%{_datadir}/gnome/help/%{name}
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/locale/*/*/*
%{_datadir}/omf/gwaei/gwaei-C.omf
%{_mandir}/man1/gwaei.1.*
%{_mandir}/man1/waei.1.*


%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Mon May 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Tue Mar 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.4-1m)
- update to 0.15.4

* Tue Mar 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.3-1m)
- update to 0.15.3

* Fri Mar 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.2-1m)
- update to 0.15.2

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0

* Tue Jan 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Sat Jan 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.2-1m)
- update to 0.13.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.1-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.1

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.0

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0
-- please ignore install waring (first time only)

* Tue Dec  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Sat Nov 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Wed Nov 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7-1m)
- initial build
