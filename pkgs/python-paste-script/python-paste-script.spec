%global momorel 7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

Name:           python-paste-script
Version:        1.7.3
Release:        %{momorel}m%{?dist}
Summary:        A pluggable command-line frontend
Group:          System Environment/Libraries
License:        MIT
URL:            http://pythonpaste.org/script
Source0:        http://cheeseshop.python.org/packages/source/P/PasteScript/PasteScript-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel
Requires:       python-paste >= 1.3, python-paste-deploy, python-cheetah

%description
Paster is pluggable command-line frontend, including commands to setup package
file layouts

Built-in features:

 * Creating file layouts for packages.
   For instance a setuptools-ready file layout.
 * Serving up web applications, with configuration based on paste.deploy


%prep
%setup -q -n PasteScript-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --single-version-externally-managed \
                             --skip-build -O1 --root=%{buildroot}

echo '%defattr (0644,root,root,0755)' > pyfiles
find %{buildroot}%{python_sitelib}/paste/script -type d | \
    sed 's:%{buildroot}\(.*\):%dir \1:' >> pyfiles
find %{buildroot}%{python_sitelib}/paste/script -not -type d | \
    sed 's:%{buildroot}\(.*\):\1:' >> pyfiles


%clean
rm -rf %{buildroot}


%files -f pyfiles
%defattr(-,root,root,-)
%doc docs/*
%{python_sitelib}/PasteScript-%{version}-py%{pyver}*
%{_bindir}/paster


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.3-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.3-1m)
- update to 1.7.3

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.2-2m)
- rebuild against python-2.6.1-2m

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.2-1m)
- version up 1.6.2
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Sat Mar  3 2007 Luke Macken <lmacken@redhat.com> - 1.1-1
- 1.1

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> - 1.0-4
- Add python-devel to BuildRequires
- Python 2.5 fixes
- 1.0

* Sun Sep 17 2006 Luke Macken <lmacken@redhat.com> - 0.9.8-1
- 0.9.8

* Sun Sep  3 2006 Luke Macken <lmacken@redhat.com> - 0.9-5
- Rebuild for FC6

* Mon Aug 21 2006 Luke Macken <lmacken@redhat.com> - 0.9-4
- Include .pyo files instead of ghosting them.

* Sat Jul 29 2006 Luke Macken <lmacken@redhat.com> - 0.9-3
- Require python-paste-deploy

* Wed Jul 26 2006 Luke Macken <lmacken@redhat.com> - 0.9-2
- Rename to python-paste-script
- Use consistent buildroot variables
- Fix docs inclusion

* Mon Jul 10 2006 Luke Macken <lmacken@redhat.com> - 0.9-1
- Initial package
