%global momorel 19

Summary: Galago plugin for evolution-data-server
Name: eds-feed
Version: 0.5.0
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
License: GPLv2+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.12.3
BuildRequires: libgalago-devel >= 0.5.2
BuildRequires: libxml2-devel >= 2.6.26
BuildRequires: evolution-data-server-devel >= 3.1

Patch0: eds-feed-0.5.0-eds.patch

%description
galago for evolution-data-server (eds)

%prep
%setup -q
%patch0 -p1 -b .eds

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%dir %{_libdir}/galago
%{_libdir}/galago/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-19m)
- rebuild for glib 2.33.2

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-18m)
- rebuild against evolution-data-server-3.1.91

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-17m)
- rebuild against evolution-data-server-3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-15m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-14m)
- rebuild atainst evolution-data-server-2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-13m)
- full rebuild for mo7 release

* Tue Jun 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-12m)
- rebuild against evolution-data-server-devel-2.30.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-9m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-8m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-7m)
- rebuild against evolution-data-server-2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-5m)
- %%NoSource -> NoSource

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-4m)
- rebuild against evolution-data-server-1.10.0

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-3m)
- rebuild against libgalago-0.5.2

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-2m)
- rebuild against evolution-data-server-1.8.0
-- add patch0 (struct member change)

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initila build
