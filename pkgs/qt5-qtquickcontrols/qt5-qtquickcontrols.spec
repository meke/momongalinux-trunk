%global momorel 1
%global qt_module qtquickcontrols

# define to build docs, need to undef this for bootstrapping
# where qt5-qttools builds are not yet available
%define docs 1

Name:           qt5-%{qt_module}
Summary:        Qt5 - module with set of QtQuick controls
Version:        5.2.1
Release:        %{momorel}m%{?dist}

License:        "BSD and (LGPLv2 with exceptions or GPLv3 with exceptions) and GFDL"
Group:          System Environment/Libraries
Url:            http://qt-project.org/
Source0: http://download.qt-project.org/official_releases/qt/5.2/%{version}/submodules/%{qt_module}-opensource-src-%{version}.tar.xz
NoSource: 0
BuildRequires:  qt5-qtbase-devel >= %{version}
BuildRequires:  qt5-qtbase-static >= %{version}
BuildRequires:  qt5-qtdeclarative-devel >= %{version}

%{?_qt5_version:Requires: qt5-qtbase%{?_isa} >= %{_qt5_version}}

%description
The Qt Quick Controls module provides a set of controls that can be used to
build complete interfaces in Qt Quick.

%if 0%{?docs}
%package doc
Summary: API documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
# for qhelpgenerator
BuildRequires: qt5-qttools-devel
BuildArch: noarch

%description doc
%{summary}.
%endif

%package examples
Summary: Programming examples for %{name}
Group: Documentation
Requires: %{name}%{?_isa} = %{version}-%{release}

%description examples
%{summary}.

%prep
%setup -q -n %{qt_module}-opensource-src-%{version}%{?pre:-%{pre}}

%build
%{_qt5_qmake}
make %{?_smp_mflags}

%if 0%{?docs}
make %{?_smp_mflags} docs
%endif

%install
rm -rf --preserve-root %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%if 0%{?docs}
make install_docs INSTALL_ROOT=%{buildroot}
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
# better to own this elsewhere? qt5-qtbase? -- rex
%dir %{_qt5_archdatadir}/qml
%{_qt5_archdatadir}/qml/QtQuick/
%doc LICENSE.FDL
%doc LICENSE.LGPL
%doc LICENSE.GPL
%doc LGPL_EXCEPTION.txt
%doc header.BSD

%if 0%{?docs}
%files doc
%defattr(-,root,root,-)
%{_qt5_docdir}/qtquickcontrols.qch
%{_qt5_docdir}/qtquickcontrols/
%{_qt5_docdir}/qtquicklayouts.qch
%{_qt5_docdir}/qtquicklayouts/
%endif

# ready -examples, seems 'make install' doesn't currently do anything in examples/
%if 0
#if 0%{?_qt5_examplesdir:1}
%files examples
%defattr(-,root,root,-)
%{_qt5_examplesdir}/
%endif

%changelog
* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- import from Fedora

* Thu Feb 06 2014 Rex Dieter <rdieter@fedoraproject.org> 5.2.1-1
- 5.2.1

* Mon Jan 27 2014 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-2
- ready -examples subpkg

* Thu Dec 12 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-1
- 5.2.0

* Mon Dec 02 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.10.rc1
- 5.2.0-rc1

* Mon Nov 11 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.5.beta1
- rebuild (arm/qreal)

* Thu Oct 24 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.4.beta1
- 5.2.0-beta1

* Thu Oct 24 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.3.alpha
- bootstrap ppc

* Thu Oct 24 2013 Lubomir Rintel <lkundrak@v3.sk> - 5.2.0-0.2.alpha
- Bulk sad and useless attempt at consistent SPEC file formatting

* Wed Oct 02 2013 Rex Dieter <rdieter@fedoraproject.org> - 5.2.0-0.1.alpha
- 5.2.0-alpha
- tidy dir ownership
- -doc subpkg

* Mon Sep 23 2013 Lubomir Rintel <lkundrak@v3.sk> - 5.1.1-2
- Drop unused devel package (Rex Dieter, #1008527)

* Wed Sep 11 2013 Lubomir Rintel <lkundrak@v3.sk> - 5.1.1-1
- Initial packaging
