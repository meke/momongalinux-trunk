%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global sopranover 2.9.4
%global qimageblitzver 0.0.6

%global moz_pluginsdir %{_kde4_libdir}/mozilla/plugins

Name: krdc
Summary: KRDC - Remote Desktop Client
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
Source1: krdc-icons.tar.bz2
Patch0: kdenetwork-4.10.4-krdc_icon.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
# kopete/yahoo
Requires(hint): jasper
## kppp
Requires: ppp
## krdc
Requires: rdesktop
BuildRequires: boost-devel >= 1.50.0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: desktop-file-utils
BuildRequires: freenx-client-devel >= 1.0
BuildRequires: freerdp-devel
BuildRequires: giflib-devel
BuildRequires: jasper-devel
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kdepimlibs-devel >= %{version}
BuildRequires: kde-workspace-devel >= %{version}
BuildRequires: kde-baseapps-devel
BuildRequires: libgadu-devel >= 1.8.0
BuildRequires: libidn-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libktorrent-devel >= 1.3
BuildRequires: libmsn-devel >= 4.1
BuildRequires: libotr-devel
BuildRequires: libv4l-devel
BuildRequires: libvncserver-devel >= 0.9.9
BuildRequires: libxslt-devel
BuildRequires: libxml2-devel
BuildRequires: linphone-devel >= 3.4.3
BuildRequires: meanwhile-devel
BuildRequires: mozilla-filesystem
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openslp-devel
BuildRequires: ortp-devel >= 0.15.0
BuildRequires: pcre-devel
BuildRequires: qca2-devel
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: speex-devel
BuildRequires: sqlite-devel
BuildRequires: telepathy-qt4-devel >= 0.1.8
BuildRequires: webkitpart-devel >= 0.0.4
BuildRequires: openssl-devel >= 1.0.1c
BuildRequires: libmms-devel
BuildRequires: libsrtp-devel

Obsoletes: kdenetwork-krdc < %{version}-%{release}
Conflicts: kdenetwork < 4.10.90

%description
KRDC is a client application that allows you to view or even control the desktop session
on another machine that is running a compatible server. VNC and RDP is supported.

%package libs
Summary: Runtime libraries for %{name}
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: kdenetwork-krdc-libs < %{version}-%{release}

%description libs
%{summary}.

%package devel
Summary: Developer files for %{name}
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel
Obsoletes: kdenetwork-krdc-devel < %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q -a 1 -n %{name}-%{version}
%patch0 -p2 -b .krdc_icon

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
export CXXFLAGS=-DGLIB_COMPILATION
%{cmake_kde4} \
	  ../
popd

make -C %{_target_platform}/doc
make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING*
%{_kde4_bindir}/krdc
%{_kde4_bindir}/krdc_rfb_approver
%{_kde4_libdir}/kde4/kcm_krdc*.so
%{_kde4_libdir}/kde4/krdc_*.so
%{_kde4_appsdir}/krdc
%{_kde4_appsdir}/krdc_rfb_approver
%{_kde4_datadir}/applications/kde4/krdc.desktop
%{_kde4_datadir}/kde4/services/krdc_*.desktop
%{_kde4_datadir}/kde4/services/rdp.protocol
%{_kde4_datadir}/kde4/services/vnc.protocol
%{_kde4_datadir}/kde4/servicetypes/krdc*.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/smb2rdc.desktop
%{_kde4_datadir}/config.kcfg/krdc.kcfg
%{_kde4_datadir}/telepathy/clients/krdc_rfb_approver.client
%{_kde4_datadir}/telepathy/clients/krdc_rfb_handler.client
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.krdc_rfb_approver.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.krdc_rfb_handler.service
%{_kde4_datadir}/doc/HTML/en/krdc

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libkrdccore.so.4*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/krdc
%{_kde4_libdir}/libkrdccore.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- initial build for Momonga Linux
