%global momorel 1

Summary: DNS resolver library for both synchronous and asynchronous DNS queries
Name: udns
Version: 0.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.corpit.ru/mjt/udns.html
Source: http://www.corpit.ru/mjt/udns/udns-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
udns is a resolver library for C (and C++) programs, and a collection
of useful DNS resolver utilities.

%package devel
Summary: Header files, libraries and development documentation for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%prep
%setup -q

%build
CFLAGS="%{optflags}" ./configure --enable-ipv6
%{__make} %{?_smp_mflags} all sharedlib

%install
%{__rm} -rf %{buildroot}
%{__install} -Dp -m0755 libudns.so.0 %{buildroot}%{_libdir}/libudns.so.0
%{__ln_s} -f libudns.so.0 %{buildroot}%{_libdir}/libudns.so
%{__install} -Dp -m0755 dnsget %{buildroot}%{_bindir}/dnsget
%{__install} -Dp -m0444 dnsget.1 %{buildroot}%{_mandir}/man1/dnsget.1

%{__install} -Dp -m0444 udns.3 %{buildroot}%{_mandir}/man3/udns.3
%{__install} -Dp -m0644 udns.h %{buildroot}%{_includedir}/udns.h

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc COPYING.LGPL NEWS NOTES TODO
%doc %{_mandir}/man1/dnsget.1*
%{_bindir}/dnsget
%{_libdir}/libudns.so.*

%files devel
%defattr(-, root, root, 0755)
%doc %{_mandir}/man3/udns.3*
%{_includedir}/udns.h
%{_libdir}/libudns.so

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-1m)
- update to 0.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.9-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul  2 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.9-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.9-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Sep 14 2008 Adrian Reber <adrian@lisas.de> - 0.0.9-3
- removed rblcheck binary to resolve conflict with package rblcheck

* Wed Jul 16 2008 Adrian Reber <adrian@lisas.de> - 0.0.9-2
- removed static library
- added correct optflags
- fixed license tag

* Thu Nov 22 2007 Dag Wieers <dag@wieers.com> - 0.0.9-1 - +/
- Initial package. (using DAR)
