; ~/.xemacs/init.el
;

;;;
;;; font / coding-system
;;;

; language and coding-system
(set-language-environment 'Japanese)
(set-default-coding-systems 'euc-jp-unix)
(set-terminal-coding-system 'euc-jp-unix)
(set-keyboard-coding-system 'euc-jp-unix)

; font setting
(if (equal window-system 'x)
    (progn
      (set-face-font 'default '("-shinonome-gothic-medium-r-normal--14-*"
				"-efont-*-medium-r-normal--14-*"
				"-*-fixed-medium-r-normal--14-*"
				)
		     'global '(mule-fonts) 'prepend)
      (set-face-font 'bold '(
			     "-shinonome-gothic-bold-r-normal--14-*"
			     "-efont-*-bold-r-*-*-14-*"
			     "-*-fixed-bold-r-normal--14-*"
			     )
		     'global '(mule-fonts) 'prepend)
      (set-face-font 'bold-italic '(
				    "-shinonome-gothic-bold-i-normal--14-*"
				    "-efont-*-bold-i-*-*-14-*"
				    "-*-fixed-bold-i-normal--14-*"
				    )
		     'global '(mule-fonts) 'prepend)
      (set-face-font 'italic '(
			       "-shinonome-gothic-medium-i-normal--14-*"
			       "-efont-*-medium-i-*-*-14-*"
			       "-*-fixed-medium-i-normal--14-*"
			       )
		     'global '(mule-fonts) 'prepend)
      ))

; add a little space between lines
(set-specifier minimum-line-ascent 0 nil 'x)
(set-specifier minimum-line-descent 3 nil 'x)

; dirty solution for jisx0208-1978
(set-charset-registry 'japanese-jisx0208-1978 "jisx0208")
(set-charset-registry 'japanese-jisx0208 "jisx0208")

; jisx0213-2000
(if (not(featurep 'utf-2000))
    (progn
      (make-charset
       'japanese-jisx0213-1
       "JIS X 0213:2000 Plain 1"
       '(registry "jisx0213\\(\\.2000\\)-1"
		  dimension 2 chars 94 final ?O graphic 0))
      (make-charset
       'japanese-jisx0213-2
       "JIS X 0213:2000 Plain 2"
       '(registry "jisx0213\\(\\.2000\\)-2"
		  dimension 2 chars 94 final ?P graphic 0))))
(make-coding-system 
 'iso-2022-jp-3
 'iso2022
 "ISO 2022 based 7bit encoding for JIS X 0213 (MIME:ISO-2022-JP-3)"
 '(mnemonic "ISO7/JP3"))

;;;
;;; miscellaneous setting
;;;

; auto-save files should be saved in ...
(setq auto-save-list-file-prefix (expand-file-name "~/.autosave/"))

; or you may disable auto-save funcion
(add-hook 'window-setup-hook
	  (lambda ()
	    (setq auto-save-list-file-name nil)))

; display a line number
(line-number-mode 1)

; no gutter please
(if (featurep 'gutter)
    (set-specifier default-gutter-visible-p nil))


; no progress-bar please
(setq progress-display-use-echo-area t)
(setq progress-feedback-use-echo-area t)

; no dialog box please
(setq use-dialog-box nil)

; warning level setting
(setq display-warning-suppressed-classes '(warning))

; additional load-path
(setq load-path
      (append '("~/lisp/xemacs")
	      load-path))

; wheel mouse please
(setq mwheel-follow-mouse t)
(mwheel-install)

; keep minibuffer histories please
(savehist-load)

; eldoc-mode is very useful for elisper
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)

; function list please
(require 'func-menu)
(add-hook 'find-file-hooks 'fume-add-menubar-entry)
(define-key global-map "\C-cl" 'fume-list-functions)
(define-key global-map "\C-cg" 'fume-prompt-function-goto)

; with gnuserv, you can use 'gnuclient' as EDITOR environment
(gnuserv-start)
