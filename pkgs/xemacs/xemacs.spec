%global momorel 1
# TODO: review desktop entry associations (does text/* work?)
# TODO: zero-length /usr/share/xemacs-21.5-b26/lisp/dump-paths.el
# TODO: non-ASCII in buffer tabs

%bcond_with     gtk
%bcond_with     wnn
%bcond_without  xaw3d
%bcond_with     xfs
%bcond_without  mule
%bcond_without  nox
%bcond_without  xim
%ifarch ia64
# no-expdyn-ia64 patch, https://bugzilla.redhat.com/show_bug.cgi?id=106744#c39
%bcond_with     modules
%else
%bcond_without  modules
%endif

#global snap    20090311hg4626
%global majver  21.5

Name:           xemacs
Version:        21.5.34
Release: %{momorel}m%{?dist}
Summary:        Different version of Emacs

Group:          Applications/Editors
License:        GPLv3+
URL:            http://www.xemacs.org/
%if 0%{?snap:1}
Source0:        %{name}-%{snap}.tar.xz
%else
Source0:        http://ftp.xemacs.org/xemacs-%{majver}/xemacs-%{version}.tar.gz
NoSource:	0
%endif
Source1:        %{name}.png
Source2:        xemacs.desktop
Source3:        dotxemacs-init.el
Source4:        default.el
Source5:        xemacs-sitestart.el
Source6:        gnuclient.desktop

# Fedora-specific.  Don't run the check-features Makefile target.  It checks
# that necessary packages are installed, but they aren't installed while
# building an RPM.
Patch0:         %{name}-21.5.25-mk-nochk-features.patch
# Experimental patch.  Fix WNN support.  This patch is no longer sufficient.
# WNN still doesn't work.
Patch1:         %{name}-21.5.25-wnnfix-128362.patch
# Fedora-specific.  Don't force ISO-8859 fonts.
Patch2:         %{name}-21.5.34-utf8-fonts.patch
# Experimental patch, to be sent upstream eventually.  Don't use
# -export-dynamic on IA64; leads to segfaults due to function pointer issues.
Patch3:         %{name}-21.5.27-no-expdyn-ia64-106744.patch
# Fedora-specific.  Default to courier instead of lucidatypewriter.
Patch4:         %{name}-21.5.28-courier-default.patch
# Fedora-specific.  Recognize the Fedora X server.
Patch5:         %{name}-21.5.29-x-server.patch

BuildRequires:  autoconf
BuildRequires:  sed >= 3.95
BuildRequires:  texinfo
BuildRequires:  ncurses-devel
BuildRequires:  gpm-devel
BuildRequires:  pam-devel
BuildRequires:  zlib-devel
BuildRequires:  libjpeg-devel
BuildRequires:  compface-devel
BuildRequires:  libpng-devel
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  giflib-devel
BuildRequires:  desktop-file-utils
%if %{with mule}
BuildRequires:  Canna-devel
%if %{with wnn}
BuildRequires:  FreeWnn-devel
%endif # wnn
%endif # mule
BuildRequires:  xmkmf
BuildRequires:  libXau-devel
BuildRequires:  libXpm-devel
BuildRequires:  libXt-devel >= 1.1.3
BuildRequires:  alsa-lib-devel
BuildRequires:  db4-devel
BuildRequires:  libdb-devel >= 5.3.15
BuildRequires:  gmp-devel
%if %{with gtk}
BuildRequires:  gtk2-devel
BuildRequires:  libglade-devel
%else  # gtk
BuildRequires:  xorg-x11-xbitmaps
%if %{with xaw3d}
BuildRequires:  Xaw3d-devel
%else  # xaw3d
BuildRequires:  neXtaw-devel
%endif # xaw3d
%endif # gtk
BuildRequires:  libXft-devel
# Note: no xemacs-packages-extra dependency here, need main pkg to build it.
Requires:       xemacs-packages-base >= 20060510
Requires:       %{name}-common = %{version}-%{release}
Requires:       xorg-x11-fonts-ISO8859-1-75dpi
Requires:       xorg-x11-fonts-ISO8859-1-100dpi
Requires:       xorg-x11-fonts-misc
Requires(post): chkconfig
Requires(post): coreutils
Requires(postun): chkconfig
Requires(postun): coreutils
Provides:       xemacs(bin) = %{version}-%{release}

%global xver    %(echo %{version} | sed -e 's/\\.\\([0-9]\\+\\)$/-b\\1/')
%global xbuild  %(echo %{_build} | sed -e 's/^\\([^-]*-[^-]*-[^-]*\\).*/\\1/')

%description
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains XEmacs built for X Windows%{?with_mule: with MULE support}.

%package        common
Summary:        Byte-compiled lisp files and other common files for XEmacs
Group:          Applications/Editors
Requires:       %{name}-filesystem = %{version}-%{release}
Requires(post): chkconfig
Requires(preun): chkconfig

%description    common
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains byte-compiled lisp and other common files for XEmacs.

%package        nox
Summary:        Different version of Emacs built without X Windows support
Group:          Applications/Editors
# Note: no xemacs-packages* dependencies here, we need -nox to build the
# base package set.
Requires:       %{name}-common = %{version}-%{release}
Requires(post): chkconfig
Requires(post): coreutils
Requires(postun): chkconfig
Provides:       xemacs(bin) = %{version}-%{release}

%description    nox
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains XEmacs built without X Windows support.

%package        xft
Summary:        Different version of Emacs built with Xft/fontconfig support
Group:          Applications/Editors
Requires:       %{name}-common = %{version}-%{release}
Requires:       xemacs-packages-base >= 20060510
Requires(post): chkconfig
Requires(post): coreutils
Requires(postun): chkconfig
Provides:       xemacs(bin) = %{version}-%{release}

%description    xft
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains XEmacs built with Xft and fontconfig support.

%package        el
Summary:        Emacs lisp source files for XEmacs
Group:          Development/Libraries
Requires:       %{name}-filesystem = %{version}-%{release}

%description    el
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains the lisp source files for XEmacs, mainly of
interest when developing or debugging XEmacs itself.

%package        info
Summary:        XEmacs documentation in GNU texinfo format
Group:          Documentation
%if 0%{?fedora} >= 10
BuildArch:      noarch
%endif
Requires(post): info
Requires(preun): info

%description    info
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains XEmacs documentation in GNU texinfo format.

%package        devel
Summary:        Development files for XEmacs
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains XEmacs development support files.

%package        filesystem
Summary:        XEmacs filesystem layout
Group:          Applications/Editors
%if 0%{?fedora} >= 10
BuildArch:      noarch
%endif

%description    filesystem
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains directories that are required by other packages that
add functionality to XEmacs.

%prep
%setup -q -n %{name}-%{?snap:beta}%{!?snap:%{version}}
find . -type f -name "*.elc" -o -name "*.info*" | xargs rm -f
rm -f configure.in
sed -i -e /tetris/d lisp/menubar-items.el
%patch0
%patch1
%patch2
%patch3
%patch4
%patch5

sed -i -e 's/"lib"/"%{_lib}"/' lisp/setup-paths.el

autoconf --force # for DESTDIR and NO-EXPDYN-IA64 patches

for f in man/lispref/mule.texi man/xemacs-faq.texi CHANGES-beta ; do
    iconv -f iso-8859-1 -t utf-8 -o $f.utf8 $f
    touch -r $f $f.utf8
    mv -f $f.utf8 $f
done


%build
%if %{with gtk}
CFLAGS="$CFLAGS $(pkg-config libglade --cflags) -fno-strict-aliasing"
%else
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
%endif
export CFLAGS
export EMACSLOADPATH=$PWD/lisp:$PWD/lisp/mule

# The --with-*dir args can probably go away in the future if/when upstream
# configure learns to honor standard autofoo dirs better.
common_options="
    --mandir=%{_mandir}/man1
    --with-archlibdir=%{_libdir}/xemacs-%{xver}/%{xbuild}
%if %{with modules}
    --with-moduledir=%{_libdir}/xemacs-%{xver}/%{xbuild}/modules
%endif
    --with-lispdir=%{_datadir}/xemacs-%{xver}/lisp
    --with-etcdir=%{_datadir}/xemacs-%{xver}/etc
    --with-system-packages=%{_datadir}/xemacs
    --without-msw
%if %{with mule}
    --with-mule
%endif
    --with-clash-detection
    --with-database=berkdb
    --without-ldap
    --without-postgresql
    --with-mail-locking=lockf
    --with-pop
    --without-hesiod
%ifarch alpha ia64 ppc64 x86_64
    --with-system-malloc
%endif
    --with-pdump
%if ! %{with modules}
    --without-modules
%endif
    --with-debug
    --with-error-checking=none
    --enable-bignum=gmp
    --with-union-type
"

%if %{with nox}
# build without X
%configure $common_options \
    --with-docdir=%{_libdir}/xemacs-%{xver}/doc-nox \
    --with-sound=none \
    --with-xim=no \
    --without-canna \
    --without-wnn \
    --without-x
make EMACSDEBUGPATHS=yes # toplevel parallel make fails
mv lib-src/DOC{,-nox}
mv src/xemacs{,-nox-%{xver}}
mv lib-src/config.values{,-nox}
mv Installation{,-nox}
# grab these from -nox, the X ones may have deps on ALSA, X, etc
for file in {e,oo}tags gnuserv {fake,move}mail ; do
    mv lib-src/$file{,-mindep}
done
%endif # nox

# build with Xft
%configure $common_options \
    --with-docdir=%{_libdir}/xemacs-%{xver}/doc-xft \
    --with-sound=nonative,alsa \
    --with-xft=all \
%if %{with gtk}
    --with-gtk \
    --with-gnome \
%else
    --with-athena=%{?with_xaw3d:3d}%{!?with_xaw3d:next} \
    --with-menubars=lucid \
    --with-widgets=athena \
    --with-dialogs=athena \
    --with-scrollbars=lucid \
    --with-xim=%{?with_xim:xlib}%{!?with_xim:no} \
%endif
%if ! %{with wnn}
    --without-wnn
%endif
make EMACSDEBUGPATHS=yes # toplevel parallel make fails
mv lib-src/DOC{,-xft}
mv src/xemacs{,-xft-%{xver}}
mv lib-src/config.values{,-xft}
mv Installation{,-xft}

# build with X
%configure $common_options \
    --with-docdir=%{_libdir}/xemacs-%{xver}/doc \
    --with-sound=nonative,alsa \
%if %{with xft}
    --with-xft=all \
%else
%if %{with xfs}
    --with-xfs \
%endif
%endif
%if %{with gtk}
    --with-gtk \
    --with-gnome \
%else
    --with-athena=%{?with_xaw3d:3d}%{!?with_xaw3d:next} \
    --with-menubars=lucid \
    --with-widgets=athena \
    --with-dialogs=athena \
    --with-scrollbars=lucid \
    --with-xim=%{?with_xim:xlib}%{!?with_xim:no} \
%endif
%if ! %{with wnn}
    --without-wnn
%endif

make EMACSDEBUGPATHS=yes # toplevel parallel make fails

cat << \EOF > xemacs.pc
prefix=%{_prefix}
%if %{with modules}
includedir=%{_libdir}/xemacs-%{xver}/%{xbuild}/include
sitemoduledir=%{_libdir}/xemacs/site-modules
%endif
sitestartdir=%{_datadir}/xemacs/site-packages/lisp/site-start.d
sitepkglispdir=%{_datadir}/xemacs/site-packages/lisp

Name: xemacs
Description: Different version of Emacs
Version: %{version}
%if %{with modules}
Cflags: -I${includedir}
%endif
EOF

cat > macros.xemacs << EOF
%%_xemacs_version %{majver}
%%_xemacs_ev %{?epoch:%{epoch}:}%{version}
%%_xemacs_evr %{?epoch:%{epoch}:}%{version}-%{release}
%%_xemacs_sitepkgdir %{_datadir}/xemacs/site-packages
%%_xemacs_sitelispdir %{_datadir}/xemacs/site-packages/lisp
%%_xemacs_sitestartdir %{_datadir}/xemacs/site-packages/lisp/site-start.d
%%_xemacs_bytecompile /usr/bin/xemacs -q -no-site-file -batch -eval '(push "." load-path)' -f batch-byte-compile
%if %{with modules}
%%_xemacs_includedir %{_libdir}/xemacs-%{xver}/%{xbuild}/include
%%_xemacs_sitemoduledir %{_libdir}/xemacs/site-modules
%endif
EOF

%install
%if %{with nox}
# restore binaries with less dependencies; note: no -p nor move
for file in lib-src/*-mindep ; do cp $file ${file%%-mindep} ; done
%endif

make install DESTDIR=$RPM_BUILD_ROOT

%if %{with nox}
# install nox files
echo ".so man1/xemacs.1" > $RPM_BUILD_ROOT%{_mandir}/man1/xemacs-nox.1
install -pm 755 src/xemacs-nox-%{xver} $RPM_BUILD_ROOT%{_bindir}
ln -s xemacs-nox-%{xver} $RPM_BUILD_ROOT%{_bindir}/xemacs-nox
install -dm 755 $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/doc-nox
install -pm 644 lib-src/DOC-nox \
    $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/doc-nox/DOC
install -pm 644 lib-src/config.values-nox \
    $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/doc-nox/config.values
%endif # nox

# install xft files
echo ".so man1/xemacs.1" > $RPM_BUILD_ROOT%{_mandir}/man1/xemacs-xft.1
install -pm 755 src/xemacs-xft-%{xver} $RPM_BUILD_ROOT%{_bindir}
ln -s xemacs-xft-%{xver} $RPM_BUILD_ROOT%{_bindir}/xemacs-xft
install -dm 755 $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/doc-xft
install -pm 644 lib-src/DOC-xft \
    $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/doc-xft/DOC
install -pm 644 lib-src/config.values-xft \
    $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/doc-xft/config.values

# these clash with GNU Emacs
mv $RPM_BUILD_ROOT%{_bindir}/etags{,.xemacs}
rm -f $RPM_BUILD_ROOT%{_bindir}/{ctags,rcs-checkin,b2m}
mv $RPM_BUILD_ROOT%{_mandir}/man1/etags{,.xemacs}.1
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/ctags.1

# these clash with other packages
rm -f $RPM_BUILD_ROOT%{_infodir}/info*
rm -f $RPM_BUILD_ROOT%{_infodir}/standards*
rm -f $RPM_BUILD_ROOT%{_infodir}/termcap*
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

desktop-file-install --mode=644 --dir=$RPM_BUILD_ROOT%{_datadir}/applications \
    %{SOURCE2}

desktop-file-install --mode=644 --dir=$RPM_BUILD_ROOT%{_datadir}/applications \
    %{SOURCE6}

# site-start.el
install -dm 755 \
    $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/lisp/site-start.d
install -pm 644 %{SOURCE5} \
    $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/lisp/site-start.el

# default.el
install -pm 644 %{SOURCE4} $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/lisp

# default user init file
install -Dpm 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/skel/.xemacs/init.el

# icon
install -Dpm 644 %{SOURCE1} \
    $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/apps/xemacs.png

# macro file
install -Dpm 644 macros.xemacs $RPM_BUILD_ROOT%{_sysconfdir}/rpm/macros.xemacs

# Empty directories for external packages to use
mkdir -m 0755 $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/etc
mkdir -m 0755 $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/info
mkdir -m 0755 $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/lib-src
mkdir -m 0755 $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/man
mkdir -m 0755 $RPM_BUILD_ROOT%{_datadir}/xemacs/site-packages/pkginfo

# make sure nothing is 0400
chmod -R a+rX $RPM_BUILD_ROOT%{_prefix}
chmod a+x $RPM_BUILD_ROOT%{_datadir}/xemacs-%{xver}%{_sysconfdir}/xemacs-fe.sh

# clean up unneeded stuff (TODO: there's probably much more)
find $RPM_BUILD_ROOT%{_prefix} -name "*~" | xargs -r rm
rm $RPM_BUILD_ROOT%{_libdir}/xemacs-%{xver}/%{xbuild}/gzip-el.sh
rm $RPM_BUILD_ROOT{%{_bindir}/gnuattach,%{_mandir}/man1/gnuattach.1}
cd $RPM_BUILD_ROOT%{_datadir}/xemacs-%{xver}/etc
rm -r InstallGuide tests XKeysymDB *.1
cd -

# separate files
rm -f *.files base-files el-files info-files
echo "%%defattr(-,root,root,-)" > base-files
echo "%%defattr(-,root,root,-)" > el-files
echo "%%defattr(-,root,root,-)" > info-files

find $RPM_BUILD_ROOT%{_datadir}/xemacs{-%{xver},/site-packages/lisp/*} \
  \( -type f -not -name '*.el' -fprint base-non-el.files \) -o \
  \( -type d -name info -fprint info.files -prune \) -o \
  \( -type d -not -name site-start.d -fprintf dir.files "%%%%dir %%p\n" \) -o \
  \( -name '*.el' \( -exec test -e '{}'c \; -fprint el-bytecomped.files -o \
     -fprint base-el-not-bytecomped.files \) \)
sed -i -e "s|$RPM_BUILD_ROOT||" *.files

# make site-packages lisp files config files
sed -i -e 's|^\(.*/site-packages/lisp/.*\)$|%%config(noreplace) \1|' \
  base-el-not-bytecomped.files

# combine the file lists
cat base-*.files dir.files >> base-files
cat el-*.files >> el-files
cat info.files >> info-files

install -Dpm 644 xemacs.pc $RPM_BUILD_ROOT%{_libdir}/pkgconfig/xemacs.pc


%post
%{_sbindir}/alternatives --install %{_bindir}/xemacs xemacs \
    %{_bindir}/xemacs-%{xver} 80
touch --no-create %{_datadir}/icons/hicolor &>/dev/null

%postun
[ -e %{_bindir}/xemacs-%{xver} ] || \
%{_sbindir}/alternatives --remove xemacs %{_bindir}/xemacs-%{xver}
if [ $1 -eq 0 ] ; then
    update-desktop-database %{_datadir}/applications &>/dev/null
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null
fi

%posttrans
update-desktop-database %{_datadir}/applications &>/dev/null
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%post nox
%{_sbindir}/alternatives --install %{_bindir}/xemacs xemacs \
    %{_bindir}/xemacs-nox-%{xver} 40 || :

%postun nox
[ -e %{_bindir}/xemacs-nox-%{xver} ] || \
%{_sbindir}/alternatives --remove xemacs %{_bindir}/xemacs-nox-%{xver} || :

%post xft
%{_sbindir}/alternatives --install %{_bindir}/xemacs xemacs \
    %{_bindir}/xemacs-xft-%{xver} 40 || :

%postun xft
[ -e %{_bindir}/xemacs-xft-%{xver} ] || \
%{_sbindir}/alternatives --remove xemacs %{_bindir}/xemacs-xft-%{xver} || :

%post common
%{_sbindir}/alternatives --install %{_bindir}/etags etags \
    %{_bindir}/etags.xemacs 40 || :

%preun common
[ $1 -ne 0 ] || \
%{_sbindir}/alternatives --remove etags %{_bindir}/etags.xemacs || :

%post info
for file in xemacs cl internals lispref new-users-guide ; do
    /sbin/install-info %{_infodir}/$file.info %{_infodir}/dir
done
:

%preun info
if [ $1 -eq 0 ] ; then
    for file in xemacs cl internals lispref new-users-guide ; do
        /sbin/install-info --delete %{_infodir}/$file.info %{_infodir}/dir
    done
fi
:


%files
%defattr(-,root,root,-)
%doc Installation
# gnuclient needs X libs, so not in -common
%{_bindir}/gnuclient
%{_bindir}/gnudoit
%ghost %{_bindir}/xemacs
%{_bindir}/xemacs-%{xver}
%{_libdir}/xemacs-%{xver}/doc/
%if %{with modules}
%if %{with mule}
%{_libdir}/xemacs-%{xver}/%{xbuild}/modules/canna_api.ell
%endif
%endif
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/gnuclient.desktop
%{_datadir}/icons/hicolor/48x48/apps/xemacs.png
%{_mandir}/man1/gnuclient.1*
%{_mandir}/man1/gnudoit.1*

%if %{with nox}
%files nox
%defattr(-,root,root,-)
%doc Installation-nox
%ghost %{_bindir}/xemacs
%{_bindir}/xemacs-nox
%{_bindir}/xemacs-nox-%{xver}
%{_libdir}/xemacs-%{xver}/doc-nox/
%{_mandir}/man1/xemacs-nox.1*
%endif

%files xft
%defattr(-,root,root,-)
%doc Installation-xft
%ghost %{_bindir}/xemacs
%{_bindir}/xemacs-xft
%{_bindir}/xemacs-xft-%{xver}
%{_libdir}/xemacs-%{xver}/doc-xft/
%{_mandir}/man1/xemacs-xft.1*

%files common -f base-files
%defattr(-,root,root,-)
%doc INSTALL README COPYING PROBLEMS CHANGES-beta etc/NEWS etc/TUTORIAL
%{_bindir}/etags.xemacs
%{_bindir}/ootags
%{_bindir}/xemacs-script
%dir %{_libdir}/xemacs-%{xver}/
%dir %{_libdir}/xemacs-%{xver}/%{xbuild}/
%{_libdir}/xemacs-%{xver}/%{xbuild}/[acdfghprsvwy]*
%{_libdir}/xemacs-%{xver}/%{xbuild}/m[am]*
%{_libdir}/xemacs-%{xver}/%{xbuild}/movemail
%if %{with modules}
%{_libdir}/xemacs/
%dir %{_libdir}/xemacs-%{xver}/%{xbuild}/modules/
%{_libdir}/xemacs-%{xver}/%{xbuild}/modules/auto-autoloads.elc
%endif
%config(noreplace) %{_sysconfdir}/rpm/macros.xemacs
%config(noreplace) %{_sysconfdir}/skel/.xemacs/
%{_mandir}/man1/etags.xemacs.1*
%{_mandir}/man1/gnuserv.1*
%{_mandir}/man1/xemacs.1*

%files el -f el-files
%defattr(-,root,root,-)
%if %{with modules}
%{_libdir}/xemacs-%{xver}/%{xbuild}/modules/auto-autoloads.el
%endif

%files info -f info-files
%defattr(-,root,root,-)
%doc COPYING
%{_infodir}/*.info*

%files devel
%defattr(-,root,root,-)
%if %{with modules}
%{_bindir}/ellcc
%{_libdir}/xemacs-%{xver}/%{xbuild}/include/
%endif
%{_libdir}/pkgconfig/xemacs.pc

%files filesystem
%defattr(-,root,root,-)
%dir %{_datadir}/xemacs
%dir %{_datadir}/xemacs/site-lisp
%dir %{_datadir}/xemacs/site-packages
%dir %{_datadir}/xemacs/site-packages/etc
%dir %{_datadir}/xemacs/site-packages/info
%dir %{_datadir}/xemacs/site-packages/lib-src
%dir %{_datadir}/xemacs/site-packages/lisp
%dir %{_datadir}/xemacs/site-packages/lisp/site-start.d
%dir %{_datadir}/xemacs/site-packages/man
%dir %{_datadir}/xemacs/site-packages/pkginfo

%changelog
* Thu May 22 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (21.5.34-1m)
- update to 21.5.34

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.5.31-6m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (21.5.31.5m)
- rebuild against libtiff-4.0.1

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (21.5.31-4m)
- rebuild against libdb-5.3.15

* Sun Mar 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.5.31-3m)
- fix momonga BTS #426
- rebuild against libXt-1.1.3

* Mon Sep 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.5.31-2m)
- use --with-system-malloc on x86_64

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.5.31-1m)
- update to 21.5.31
- reimport from fedora's xemacs.spec

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.5.29-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.5.29-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (21.5.29-6m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.29-5m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.29-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (21.5.29-2m)
- rebuild against libjpeg-7

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.29-2m)
- [SECURITY] CVE-2009-2688
- import a security patch (Patch100) from Gentoo

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.29-1m)
- update to 21.5.29

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.28-4m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.28-3m)
- rebuild against gpm-1.20.5

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.28-2m)
- fix install-info

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.5.28-1m)
- update to 21.5.28 (based on Fedora)

* Sat Dec  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.4.20-4m)
- fixed alternatives

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.4.20-3m)
- revised alternatives

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (21.4.20-2m)
- rebuild against db4-4.6.21

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21.4.20-1m)
- update to 21.4.20

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (21.4.19-7m)
- add BuildPreReq: xorg-x11-xbitmaps

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (21.4.19-6m)
- rebuild against db-4.5

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (21.4.19-5m)
- rebuild against openmotif-2.3.0-beta2

* Sun Nov  5 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.4.19-4m)
- fix alternatives

* Mon Jul 10 2006 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.19-3m)
- rebuild without libwnn6

* Mon Jul 10 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21.4.19-2m)
- ctags and etags are now handled by alternatives.

* Sat Feb  4 2006 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.19-1m)
- version up
- remove 'xemacs-21-cursor.patch' that seems to be merged

* Mon Jan  9 2006 Masahiro takahata <takahata@momonga-linux.org>
- (21.4.17-4m)
- rebuild against db4.3

* Tue Oct 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (21.4.17-3m)
- enable ia64

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (21.4.17-2m)
- elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Thu Feb 10 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (21.4.17-1m)
  update to 21.4.17

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (21.4.16-4m)
- enable x86_64.
- alpha ia64 ppc64 specific configure args from fc3.

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (21.4.16-3m)
- import new xemacs.desktop from Fedora Core

* Sun Jan  9 2005 Toru Hoshina <t@momonga-linux.org>
- (21.4.16-2m)
- rebuild against libwnn6-3.0-17m. libwnn6's SONAME was changed.

* Sun Jan  9 2005 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.16-1m)
- version up

* Sat Dec  4 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (21.4.15-4m)
- rebuild against openmotif-2.2.4

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.15-3m)
- rebuild against db4.2

* Wed Mar 03 2004 mutecat <mutecat@momonga-linux.org>
- (21.4.15-2m)
- add xemacs-21.4.15-ppc.patch

* Mon Feb 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.15-1m)

* Wed Dec 24 2003 Kenta MURATA <muraken2@nifty.com>
- (21.4.14-8m)
- fix alternative again (gnuattach, gnudoit).

* Tue Dec 23 2003 Kenta MURATA <muraken2@nifty.com>
- (21.4.14-7m)
- fix alternative again.

* Tue Dec 23 2003 Kenta MURATA <muraken2@nifty.com>
- (21.4.14-6m)
- gnuclient is alternatived for gnuserv-emacs.
- fix alternative.

* Wed Dec 17 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (21.4.14-5m)
- fix alternative at %%postun

* Tue Dec 16 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (21.4.14-4m)
- add patch12 for stack overflow on regex.c

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (21.4.14-3m)
- disable faces
- - delete faces-devel from BuildPreReq
- - set --without-xface at configure
- - reported by ToshiOkada [Momonga-devel.ja:02282]
- s/%%define/%%global/g

* Fri Oct  3 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (21.4.14-2m)
- adapt alternatives for b2m and rcs-checkin.

* Fri Sep 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.14-1m)
- remove 'xemacs-21.4.13-EmacsFrame-fontlock.patch' since it was merged

* Sat Jul 12 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (21.4.13-2m)
- add 'xemacs-21.4.13-EmacsFrame-fontlock.patch' from rawhide

* Fri May 30 2003 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.13-1m)

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (21.4.12-2m)
- revise %%post and %%preun

* Fri Jan 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.12-1m)
- the 21.4 series is now officially the stable branch of XEmacs

* Mon Jan  6 2003 Junichiro Kita <kita@momonga-linux.org>
- (21.4.11-2m)
- install-info emodules.info

* Sun Jan  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.11-1m)

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.10-1m)

* Wed Oct  2 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.9-2m)
- remove 'xemacs-21.4.6-adhoc.patch'

* Sun Sep 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.9-1m)

* Tue Aug  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (21.4.8-7m)
- disabling etags symlinks

* Thu Aug  1 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.8-6m)
- Momonga

* Wed Jul 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.8-5m)
- apply 'xemacs-21.4.6-adhoc.patch' on ppc too

* Tue Jun 11 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (21.4.8-4k)
- SEGV....(T_T)

* Tue May 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.8-2k)

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (21.4.7-4k)
- rebuild against openmotif-2.2.2-2k.
- tu-ka, katte ni tukawarete simau...

* Mon May  6 2002 Tsutomu Yasuda <tom@kondara.org>
- (21.4.7-2k)
  update to 21.4.7

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (21.4.6-18k)
- rebuild against libpng 1.2.2.

* Wed Mar 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.6-16k)
- remove 'Provides: xemacs'

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (21.4.6-14k)
- rebuild against for db3,4

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (21.4.6-12k)
- revise BuildPreReq:

* Mon Feb 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.6-10k)
- never handle ctags and ctags.1

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (21.4.6-8k)
- autoconf 1.5

* Thu Dec 27 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.6-6k)
- add an adhoc patch so as to avoid builing failure in Alpha architecture

* Tue Dec 25 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.4.6-4k)
- change build option to lucid from athena

* Fri Dec 21 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.6-2k)
- remove coding-magic patch
- remove read-key-sequence patch

* Wed Dec 19 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.5-20k)
- remove dotemacs

* Tue Dec 11 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.5-18k)
- add dotemacs-0.8.tar.gz

* Tue Dec 11 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.5-16k)
- add a config-sample

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (21.4.5-14k)
- Patch10: xemacs-21.1.14-xfs.patch (from rawhide) is disabled at this time.

* Fri Nov 23 2001  Hidetomo Machi <mcHT@kondara.org>
- (21.4.5-12k)
- Patch10: xemacs-21.1.14-xfs.patch (from rawhide)
- Patch11: xemacs-21-cursor.patch (from rawhide)
- grub /var/lock/xemacs and some info files
- renewal xdnd-xsmp patch

* Mon Nov 12 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.4.5-10k)
- add xemacs-read-key-sequece.patch
  see <stitchuv2o.fsf@geo.f.axe-inc.co.jp> xemacs-beta-ja@xemacs.org
- arrange patch number
- add /usr/lib/xemacs/site-modules to %dir

* Tue Oct 30 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.5-8k)
- fix installing xemacs-editclient 644 -> 0555

* Mon Oct 29 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.5-6k)
- add BuildPreReq:
- add coding-magic.patch from ftp.jpl.org
  see <yosuzo7r8kmj.fsf@jpl.org> xemacs-beta-ja@xemacs.org
- add Tabs-I18N from emacs-w3m ML

* Wed Oct 24 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (21.4.5-4k)
  fix source uri

* Wed Oct 24 2001 Hidetomo Machi <>
- (21.4.5-2k)
- modify spec (see cvs diff ;p)

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (21.4.4-8k)
- rebuild against libpng 1.2.0.

* Fri Oct  5 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.4-6k)
- add '--with-tty=yes'

* Fri Sep 14 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.4.4-4k)
- add '--without-msw' to avoid building problem on a system with Wine

* Sat Jun 28 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.4-3k)
- updated to 21.4.4

* Sat Jun 02 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.3-3k)
- updated to 21.4.3

* Thu May 10 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.2-3k)
- updated to 21.4.2

* Sat Apr 28 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.1-3k)
- updated to 21.4.1

* Tue Apr 17 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.4.0-3k)
- updated to 21.4.0

* Mon Apr 16 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.47-3k)
- updated to 21.2.47
- new variable %{relver}

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (21.2.46-5k)
- rebuild against audiofile-0.2.1.

* Wed Mar 21 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.46-3k)
- updated to 21.2.46
- modified xemacs-21.2.43.xdnd-xsmp.patch
  renamed  xemacs-21.2.46.xdnd-xsmp.patch
  but still not working.

* Tue Feb 27 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.45-3k)
- updated to 21.2.45

* Fri Feb 09 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.44-3k)
- updated to 21.2.44

* Mon Jan 29 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.43-3k)
- updated to 21.2.43
- modified xemacs-21.2.35.xdnd-xsmp.patch
  renamed  xemacs-21.2.43.xdnd-xsmp.patch

* Sun Jan 21 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.42-3k)
- updated to 21.2.42

* Wed Jan 17 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.41-3k)
- updated to 21.2.41

* Tue Jan 09 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.40-5k)
- add option --mail-locking=flock

* Mon Jan 08 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.40-3k)
- updated to 21.2.40

* Mon Jan 01 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.2.39-3k)
- updated to 21.2.39

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (21.2.38-7k)
- rebuild against audiofile-0.2.0.

* Fri Dec 15 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- --with-postgresql=no

* Tue Dec 05 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.38-3k)
- updated to 21.2.38

* Wed Nov 15 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.37-1k)
- updated to 21.2.37
- removed xemacs-21.2.36-ccl.patch

* Thu Oct 19 2000 Hidetomo Machi <mcHT@kondara.org>
- (21.2.36-9k)
- modify specfile (for FHS)

* Sun Oct 08 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.36-7k)
- renamed to xemacs-21.2.36 (from xemacs-beta-21.2.36-5k)
- remove option     "--debug"
- change Copyright to License

* Sun Oct 08 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.36-5k)
- add xemacs-21.2.36-ccl.patch from xemacs-beta ML
- add xemacs-mule-wnn.patch again

* Sat Oct  7 2000 Kazuhiko <kazuhiko@fdiary.net>
- Obsoletes: xemacs-utf-2000
- add option "--with-widgets=athena"
- remove xemacs-mule-wnn.patch
- fix wnn6 header path automatically

* Wed Oct 04 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.36-1k)
- updated to 21.2.36
- removed xemacs-21.2.35+.patch diff with cvs.

* Fri Sep 08 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.35-3k)
- add xemacs-21.2.35+.patch diff with cvs.

* Fri Sep 08 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.2.35-2k)
- add xdnd-xsmp.patch modified from kondara.patch
- add option "--with-xdnd"

* Fri Jul 21 2000 Toshiro HIKITA <toshi@sodan.org>
- New Branch XEmacs-Beta 21.2.34 (from Kondara-21.1.11-1k)
- remove kondara.patch
- remove option  "--lockdir=/var/lock/xemacs" "--with-xdnd"
- remove install option "lockdir=/var/lock/xemacs"
- remove Source files "-elc.tar.gz" "-info.tar.gz"
- add mule-wnn.patch
- adopt filenames
- new variable %{betaver}
- remove files /var/lock/xemacs
- add option     "--debug"


* Wed Jul 19 2000 Toshiro HIKITA <toshi@sodan.org>
- update to 21.1.11

* Wed Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed Jun 28 2000 Hidetomo Machi <mcHT@kondara.org>
- modify xemacs.desktop

* Mon Jun 26 2000 Yoshiki Hayashi <penny@sodan.org>
- update to 21.1.10a.

* Wed Jun  7 2000 Yoshiki Hayashi <penny@sodan.org>
- update to 21.1.10

* Wed Mar 29 2000 Akira Higuchi <a@kondara.org>
- fixed a compilation problem in xsmp.c

* Fri Mar 10 2000 Hidetomo Machi <mcHT@kondara.org>
- avoid to conflict with emacs packages's files (look this spec)
- move lockdir to /var/lock/xemacs from /usr/lib/xemacs/lock
- add option --with-pop

* Wed Feb 23 2000 Hidetomo Machi <mcHT@kondara.org>
- include /usr/lib/xemacs directory

* Sun Feb 21 2000 Hidetomo Machi <mcHT@kondara.org>
- add some directory for Kondara's addon elisp packages
- gzip *.el file

* Sun Feb 20 2000 Akira Higuchi <a@kondara.org>
- Added support for "-unmapped" option in xemacs-editclient
- Specify "-unmapped" option in SmRestartCommand if no frame is visible
- Some minor fixes

* Fri Feb 18 2000 Akira Higuchi <a@kondara.org>
- Fixed a drawing bug in menubar
- Got rid of the annoying "XOpenIM failed" messages

* Wed Feb 16 2000 Akira Higuchi <a@kondara.org>
- Some minor fixes

* Mon Feb 14 2000 Akira Higuchi <a@kondara.org>
- Updated to 21.1.9

* Sun Feb 13 2000 Akira Higuchi <a@kondara.org>
- Bug fixes in dialog.el, gnuserv.el, lwlib-Xaw.c, and xsmp.c
- Moved xsmp-save-yourself to xsmp.el

* Fri Feb 11 2000 Akira Higuchi <a@kondara.org>
- Bug fix in the iso-2022 decoder in file-coding.c

* Wed Feb  9 2000 Akira Higuchi <a@kondara.org>
- Added support for XSMP session management
- Some fixes in xemacs-editclient script
- Bug fixes in gnuserv.el
- Added xemacs.desktop file for the gnome desktop

* Sat Jan  8 2000 Tenkou N. Hattori <tnh@kondara.org>
- with-{canna,wnn,wnn6,xim}.
- without-ldap.

* Wed Dec 22 1999 Akira Higuchi <a@kondara.org>
- Fixed the bug that gnuserv sometimes loses request from client
- Send XdndFinished even when we get an incorrect SelectionNotify event
- Added xemacs-editclient script

* Fri Dec 17 1999 Akira Higuchi <a@kondara.org>
- Added XDND drag-n-drop support (not yet complete, though)
- Fixed the problem that compilation of xemacs-Wnn fails when FreeWnn-devel is installed
- Strip binaries
- Group is Applications/Editors
- Provides xemacsen
