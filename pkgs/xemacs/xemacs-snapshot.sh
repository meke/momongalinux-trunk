#!/bin/bash

set -e

if [ -z "$1" -o $# -ne 1 ]; then
  echo "Usage: $0 <hg-revision>"
  exit 2
fi

rev="$1"
tmp=$(mktemp -d)
tarball="xemacs-$(date +%Y%m%d)hg$rev.tar"

trap cleanup EXIT
cleanup() {
    set +e
    [ -z "$tmp" -o ! -d "$tmp" ] || rm -rf "$tmp"
}

unset CDPATH
pwd=$(pwd)
now=$(date +%Y%m%d)

cd "$tmp"
hg clone http://hg.debian.org/hg/xemacs/xemacs-beta
cd xemacs-beta
hg update -r "$rev"
# Hack to avoid needing .hg/ around but still getting xemacs-extra-name right
hg identify | cut -d " " -f 1 >> version.sh.in # see configure.ac
rm -r .hg*
cd ..
tar cf "$pwd/$tarball" xemacs-beta
xz -f "$pwd/$tarball"
cd - >/dev/null
