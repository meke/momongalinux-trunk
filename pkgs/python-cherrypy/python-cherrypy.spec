%global momorel 7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-cherrypy
Version:        3.1.2
Release:        %{momorel}m%{?dist}
Summary:        A pythonic, object-oriented web development framework
Group:          Development/Libraries
License:        BSD
URL:            http://www.cherrypy.org/
Source0:        http://download.cherrypy.org/cherrypy/%{version}/CherryPy-%{version}.tar.gz
NoSource:       0
Patch0:         python-cherrypy-tutorial-doc.patch
Patch1:         python-cherrypy-changederrormessage.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel
BuildRequires:  tidy

%description
CherryPy allows developers to build web applications in much the same way 
they would build any other object-oriented Python program. This usually 
results in smaller source code developed in less time.

%prep
%setup -q -n CherryPy-%{version}
%patch0 -p1
%patch1 -p1 -b .errmsg

%{__sed} -i 's/\r//' README.txt cherrypy/tutorial/README.txt cherrypy/tutorial/tutorial.conf

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT

%check
cd cherrypy/test
# The tidy tool test needs tidy in the test dir
ln -s %{_bindir}/tidy tidy
%{__python} test.py --dumb

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README.txt
%doc cherrypy/tutorial
%{python_sitelib}/*
%attr(755,root,root) %{_bindir}/cherryd

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.2-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-4m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-3m)
- pass the tests

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May  5 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.2-1m)
- updated to 3.1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.3-2m)
- rebuild against python-2.6.1-2m

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.3-1m)
- version up 3.0.3
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.1-1m)
- import from Fedora

* Mon Feb 19 2007 Luke Macken <lmacken@redhat.com> 2.2.1-6
- Disable regression tests until we can figure out why they
  are dying in mock.

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 2.2.1-5
- Add python-devel to BuildRequires

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 2.2.1-4
- Rebuild for python 2.5

* Mon Sep 18 2006 Luke Macken <lmacken@redhat.com> 2.2.1-3
- Rebuild for FC6
- Include pyo files instead of ghosting them

* Thu Jul 13 2006 Luke Macken <lmacken@redhat.com> 2.2.1-2
- Rebuild

* Thu Jul 13 2006 Luke Macken <lmacken@redhat.com> 2.2.1-1
- Update to 2.2.1
- Remove unnecessary python-abi requirement

* Sat Apr 22 2006 Gijs Hollestelle <gijs@gewis.nl> 2.2.0-1
- Update to 2.2.0

* Wed Feb 22 2006 Gijs Hollestelle <gijs@gewis.nl> 2.1.1-1
- Update to 2.1.1 (Security fix)

* Tue Nov  1 2005 Gijs Hollestelle <gijs@gewis.nl> 2.1.0-1
- Updated to 2.1.0

* Sat May 14 2005 Gijs Hollestelle <gijs@gewis.nl> 2.0.0-2
- Added dist tag

* Sun May  8 2005 Gijs Hollestelle <gijs@gewis.nl> 2.0.0-1
- Updated to 2.0.0 final
- Updated python-cherrypy-tutorial-doc.patch to match new version

* Wed Apr  6 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 2.0.0-0.2.b
- Removed CFLAGS

* Wed Mar 23 2005 Gijs Hollestelle <gijs[AT]gewis.nl> 2.0.0-0.1.b
- Initial Fedora Package
