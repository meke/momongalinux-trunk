%global momorel 4

Summary:     Work out BuildRequires for rpmbuild automatically
Name:        auto-buildrequires
Version:     1.1
Release:     %{momorel}m%{?dist}
License:     GPLv2+
Group:       Development/Tools
URL:         http://et.redhat.com/~rjones/auto-buildrequires/
Source0:     http://et.redhat.com/~rjones/auto-buildrequires/files/%{name}-%{version}.tar.gz
NoSource:    0
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root

Requires:    rpm-build
Requires:    perl-String-ShellQuote

%description
Auto-BuildRequires is a simple set of scripts for automatically suggesting 
BuildRequires lines for programs.


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/auto-br
%{_bindir}/auto-br-rpmbuild
%{_libexecdir}/auto-br-analyze.pl
%{_libexecdir}/%{name}-preload.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- import from Fedora 11

* Mon Mar  9 2009 Richard W.M. Jones <rjones@redhat.com> - 1.0-3
- New upstream version 1.0:
  . Fixes 32-bit platforms.
  . Add a missing runtime requires for a Perl library.
- High release number is so that we are larger than the release number
  in the upstream specfile.

* Fri Mar  6 2009 Orcan Ogetbil <oget[DOT]fedora[AT]gmail[DOT]com> - 0.9-2
- Prepared the SPEC file for Review Request submission

* Fri Mar  6 2009 Richard Jones <rjones@redhat.com> - 0.9-1
- Imported to git and rebuilt.

* Thu Nov  6 2008 Richard Jones <rjones@redhat.com> - 0.1-2
- Initial build.
