%global momorel 22
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: Romaji Hiragana conversion library
Name: suikyo
Version: 2.1.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://taiyaki.org/suikyo/
Source: http://prime.sourceforge.jp/src/%{name}-%{version}.tar.gz
NoSource: 0
Group: Development/Libraries
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
BuildRequires: ruby18

Obsoletes: suikyo-ruby < 2.1.0-18m

%description
Suikyo is a Romaji Hiragana conversion library based on
DFA (Deterministic finate state) automaton.

%package -n emacs-suikyo
Summary: emacs binding of suikyo
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
Obsoletes: suikyo-emacs
Obsoletes: suikyo-xemacs
Obsoletes: elisp-suikyo
Provides: elisp-suikyo

%description -n emacs-suikyo
suikyo-emacs is a emacs binding of suikyo.

%package ruby18
Summary: ruby18 binding of suikyo
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ruby18

%description ruby18
suikyo-ruby18 is a ruby binding of suikyo.

%prep
rm -rf %{buildroot}

%setup -q

%build
%configure rubydir=%{ruby18_sitelibdir}
%make

cd elisp/src
emacs -no-site-file -q -batch -f batch-byte-compile *.el
for i in *.el; do
  cp $i $i.emacs
done
for i in *.elc; do
  mv $i $i.emacs
done
cd ../..

%install
(cd conv-table && make SUIKYO_TABLE_DIR=%{buildroot}%{_datadir}/suikyo/conv-table install)
(cd ruby && echo "install:" > doc/Makefile && make prefix=%{buildroot}%{_prefix} rubydir=%{buildroot}%{ruby18_sitelibdir} install)

cd elisp/src

mkdir -p %{buildroot}%{_emacs_sitelispdir}/suikyo

for i in *.el*.emacs; do
  install -c -m 0644 $i %{buildroot}%{_emacs_sitelispdir}/suikyo/${i%%.emacs}
done

mkdir -p %{buildroot}%{_emacs_sitestartdir}
install -c -m 0644 ../etc/init-suikyo.el %{buildroot}%{_emacs_sitestartdir}

cd ../..

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_datadir}/suikyo
%doc ChangeLog
%doc contrib/uniq.rb
%doc doc/index.html

%files -n emacs-suikyo
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/suikyo
%{_emacs_sitelispdir}/suikyo
%{_emacs_sitestartdir}/init-suikyo.el
%doc elisp/ChangeLog

%files ruby18
%defattr(-,root,root,-)
%{ruby18_sitelibdir}/suikyo
%doc ruby/ChangeLog
%doc ruby/doc/index.html

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-22m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-22m)
- rename elisp-suikyo to emacs-suikyo

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-19m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-18m)
- build with ruby18

* Fri Aug  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-17m)
- fix build on x86_64

* Wed Aug  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-16m)
- rebuild against ruby-1.9.2

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-15m)
- rebuild against emacs-23.2

* Fri Apr 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-14m)
- add rm -rf %%{buildroot}%{%sitepdir}

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-13m)
- rename suikyo-emacs to elisp-suikyo
- kill suikyo-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-11m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-10m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-9m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-8m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-7m)
- rebuild against xemacs-21.5.29

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-6m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-5m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-3m)
- rebuild against gcc43

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.0-2m)
- /usr/lib/ruby

* Wed Mar 30 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.1.0-1m)
- up to 2.1.0

* Tue Mar  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1.2-1m)
- version 2.0.1.2

* Wed Dec 15 2004 TAKAHASHI Tamotsu <tamo>
- (2.0.0-1m)

* Sat Nov 27 2004 TAKAHASHI Tamotsu <tamo>
- (1.3.3-3m)
- add defattr(-,root,root) to each subpackage

* Thu Nov 25 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.3-2m)
- rebuild against emacs-21.3.50 (nen no tame...)

* Tue Aug 31 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.3-1m)
- version update to 1.3.3

* Wed Mar  3 2004 kourin <kourin@fh.freeserve.ne.jp>
- (1.3.2-1m)
- version up.

* Wed Dec 31 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.2.1-1m)
- version up.

* Tue Dec 16 2003 Masaki Yatsu <yatsu@digital-genes.com>
- (1.2.0-1)
- version up.

* Wed May 28 2003 Masaki Yatsu <yatsu@borogrammers.net>
- (1.1.2-2 / 1.1.2-0pa2)
- make suikyo sub directory in Emacs-Lisp directory

* Sun May 18 2003 Masaki Yatsu <yatsu@borogrammers.net>
- (1.1.2-1 / 1.1.2-0pa1)
- version up

* Tue May 13 2003 Masaki Yatsu <yatsu@borogrammers.net>
- (1.1.1-0pa2)
- correct suikyo-table-path

* Tue May 13 2003 Masaki Yatsu <yatsu@borogrammers.net>
- (1.1.1-0pa1)
- first build
