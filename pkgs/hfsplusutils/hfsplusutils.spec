%global momorel 9

Summary: Tools for reading Macintosh HFS+ volumes
Name: hfsplusutils
Version: 1.0.4
Release: %{momorel}m%{?dist}

License: GPLv2+
Group: Applications/File
URL: ftp://ftp.penguinppc.org/users/hasi/hfsplus_1.0.4.src.tar.bz2
Source0: ftp://ftp.penguinppc.org/users/hasi/hfsplus_%{version}.src.tar.bz2
Patch0: hfsplusutils-1.0.4-nullisnotachar.patch
Patch1: hfsplusutils-1.0.4-errno.patch
Patch2: hfsplusutils-1.0.4-includes.patch
Patch3: hfsplusutils-1.0.4-gcc4.patch
Patch4: hfsplusutils-1.0.4-memset.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake14, autoconf, libtool

%description
This package is a set of tools that allow access to HFS+ formatted
volumes. HFS+ is a modernized version of Apple Computers HFS
Filesystem.

%prep
%setup -q -n hfsplus-%{version}

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1

sed -i -e 's/^CFLAGS\s/AM_CFLAGS /' {libhfsp/src,src}/Makefile.am

%build
# Oh autocrap how we hate thee
autoreconf -vfi
%configure --disable-shared --disable-dependency-tracking
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} SUBDIRS=src transform="" install
ln -sf hpfsck %{buildroot}/%{_bindir}/fsck.hfsplus
# Remove zero-length doc file
rm -f mail/extents.diff
mkdir -p %{buildroot}/%{_mandir}/man1
bzip2 -c < doc/man/hfsp.man > %{buildroot}/%{_mandir}/man1/hfsp.1.bz2
for a in hpcd hpcopy hpfsck hpls hpmkdir hpmount hppwd hprm hpumount fsck.hfsplus ; do
	ln -sf hfsp.1.bz2 %{buildroot}/%{_mandir}/man1/$a.1.bz2
done

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/hpcd
%{_bindir}/hpcopy
%{_bindir}/hpfsck
%{_bindir}/fsck.hfsplus
%{_bindir}/hpls
%{_bindir}/hpmkdir
%{_bindir}/hpmount
%{_bindir}/hppwd
%{_bindir}/hprm
%{_bindir}/hpumount
%{_mandir}/man1/*
%doc doc/*.html
%doc mail
%doc AUTHORS NEWS README


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-8m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-7m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-4m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-3m)
- separate hfsplus-tool
- add Patch4: hfsplusutils-1.0.4-memset.patch

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc43

* Sun Mar 19 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.4-1m)
- import from fedora extra devel.
- add working fsck.hfsplus

* Tue Mar  7 2006 David Woodhouse <dwmw2@infradead.org> 1.0.4-6
- Rebuild

* Sat Apr 30 2005 David Woodhouse <dwmw2@infradead.org> 1.0.4-5
- Fix pointer abuse which GCC 4 doesn't like. Yes, the workaround
  is ugly as sin, but it is at least valid C. Apart from the bit 
  about arithmetic on void *, but GCC is happy with that :)

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Tue Feb 22 2005 David Woodhouse <dwmw2@infradead.org> 1.0.4-3
- Include man page
- Remove zero-length diff file in documentation
- BuildRequires for all the auto* mess

* Mon Feb 21 2005 David Woodhouse <dwmw2@infradead.org> 1.0.4-2
- Include fsck.hfsplus symlink
- Fix kernel header inclusion

* Sun Apr 11 2004 David Woodhouse <dwmw2@redhat.com> 1.0.4-1
- Initial build.

