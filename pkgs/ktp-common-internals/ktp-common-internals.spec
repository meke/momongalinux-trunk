%global        momorel 1
%global        base_name kde-telepathy
%global        unstable 0
%if 0%{unstable}
%global        release_dir unstable
%else
%global        release_dir stable
%endif
%global        kdever 4.13.0
%global        ftpdirver 0.8.1
%global        sourcedir %{release_dir}/%{base_name}/%{ftpdirver}/src
%global        qtver 4.8.5
%global        qtrel 1m
%global        cmakever 2.8.5
%global        cmakerel 2m
%global        telepathy_logger_qt_version 0.8.0

Name:          ktp-common-internals
Summary:       Common internals for KDE Telepathy
Version:       %{ftpdirver}
Release:       %{momorel}m%{?dist}
License:       LGPLv2+
Group:         System Environment/Libraries
URL:           http://www.kde.org
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:      0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: telepathy-qt4-devel >= 0.9.3
BuildRequires: telepathy-logger-qt-devel >= %{telepathy_logger_qt_version}
Requires:      telepathy-logger-qt >= %{telepathy_logger_qt_version}

%description
%[summary}.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: telepathy-qt4-devel

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc COPYING
%{_kde4_bindir}/ktp-debugger
%{_kde4_libdir}/*.so.*
%{_kde4_libdir}/kde4/imports/org/kde/telepathy
%{_kde4_libdir}/kde4/ktploggerplugin_tplogger.so
%{_kde4_datadir}/kde4/services/ktploggerplugin_tplogger.desktop
%{_kde4_datadir}/kde4/servicetypes/ktp_logger_plugin.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/*.mo
%{_kde4_appsdir}/katepart/syntax/ktpdebugoutput.xml
%{_kde4_appsdir}/ktelepathy
%{_kde4_iconsdir}/hicolor/*/*/*.png
%{_kde4_iconsdir}/hicolor/scalable/apps/*.svgz

%files devel
%{_kde4_includedir}/KTp
%{_kde4_libdir}/*.so

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Wed Mar 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Wed Feb 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.80-1m)
- update to 0.7.80

* Fri Nov 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-2m)
- revise source URI

* Mon Oct 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Thu Sep 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.80-1m)
- update to 0.6.80

* Wed Aug  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Apr  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-2m)
- remove patch

* Tue Dec 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sun Sep  2 2012 NARITA Koichi ,pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Fri Jul 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Sun May  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- initial build for Momonga Linux
