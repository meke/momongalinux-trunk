%global momorel 14
%global scim_binary_version 1.4.0

Summary: Smart Common Input Method platform
Name: scim
Version: 1.4.9
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.scim-im.org/
Group: User Interface/X
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: xim.d-SCIM
Patch0: %{name}-1.4.8-configs.patch
Patch7: %{name}_panel_gtk-emacs-cc-style.patch
Patch9: %{name}_panel_gtk-icon-size-fixes.patch
Patch11: %{name}-1.4.5-panel-menu-fixes.patch
Patch14: %{name}_x11_frontend-ic-focus-LTC27940-215953.patch
Patch15: %{name}-gtkimm-default-snooper-off-213796.patch
Patch17: %{name}-1.4.5-no-rpath-libdir.patch
Patch19: %{name}-1.4.7-remove-locale.patch
Patch20: %{name}-1.4.7-fix-fallback.patch
Patch21: %{name}-1.4.7-fix-capslock.patch
Patch22: %{name}-1.4.7-fix-gdm.patch
Patch23: %{name}-1.4.7-remove-help-frame.patch
Patch25: %{name}-1.4.7-timeout.patch
Patch26: %{name}-1.4.7-trayicon.patch
Patch27: %{name}-1.4.7-menu-pos.patch
Patch28: %{name}-1.4.7-xim-wrong-format.patch
Patch29: %{name}-1.4.7-bz462820.patch
Patch30: %{name}-1.4.7-imdkit-read-property-properly.patch
Patch31: %{name}-1.4.7-syslibltdl.patch
Patch32: %{name}-1.4.8-fix-dlopen.patch
Patch40: %{name}-1.4.8-revert-r114.patch
Patch50: %{name}-1.4.9-mozc-setup.patch
Patch100: %{name}-1.4.9-gtk-im-module-fake-event.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): gtk2 >= 2.20.1-5m
Requires(postun): gtk2 >= 2.20.1-5m
Requires: GConf2 >= 2.0.0
Requires: glib2 >= 2.0.0
Requires: pango >= 1.0.0
BuildRequires: GConf2-devel >= 2.0.0
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: doxygen
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel >= 2.12.3
BuildRequires: gtk2-devel >= 2.10.3
BuildRequires: libX11-devel
BuildRequires: libXcursor-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libtool
BuildRequires: pango-devel >= 1.14.3
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig >= 0.12
Obsoletes: skim
Obsoletes: skim-honoka

%description
Smart Common Input Method platform, in short SCIM, is a development
platform to make Input Method developer life easier. It honors a
very clear architecture and provides a pretty simple and powerful
programming interface.

%package devel
Summary: Development tools for SCIM applications.
Group: Development/Libraries
Requires: %{name} = %{version}
Requires: pkgconfig >= 0.12
Obsoletes: skim-devel

%description devel
scim-devel package contains the header files and static libraries to
develop a SCIM applications.

%prep
%setup -q
%patch0 -p1 -b .config~

%patch7 -p1 -b .7-emacs-ccmode~
## DO NOT APPLY Patch9, icon is too small (ichiro@scim-1.4.7-5m)
# %%patch9 -p1 -b .9-icon-size~
%patch11 -p1 -b .11-factory-menu~
%patch14 -p1 -b .14-xim-focus~
%patch15 -p1 -b .15-key-snooper~
%patch17 -p1 -b .17-rpath~
%patch19 -p1 -b .19-remove-locale~
%patch20 -p1 -b .20-fix-fallback
%patch21 -p1 -b .21-fix-capslock
%patch22 -p1 -b .22-fix-gdm
%patch23 -p1 -b .23-help-frame
%patch25 -p1 -b .25-timeout
%patch26 -p1 -b .26-trayicon
%patch27 -p1 -b .27-menu-pos
%patch28 -p1 -b .28-xim-wrong-format
%patch29 -p1 -b .29-bz462820
%patch30 -p1 -b .30-bz466657
%patch31 -p1 -b .31-sysltdl
## DO NOT APPLY Patch32, KDE does not work (ichiro@scim-1.4.9-2m)
# %%patch32 -E -p1 -b .fix-dlopen

%patch40 -p0 -b .revert-r114~

%patch50 -p1 -b .mozc-setup

%patch100 -p1 -b .fake-event

%build
# Patch17 needs bootstrap
./bootstrap
%configure --enable-ld-version-script
%make

# build documentation
make -C docs html

%install
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install configuration file for sdr
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xim.d
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xinit/xim.d/SCIM

%{find_lang} %{name}

%clean
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
%{_bindir}/update-gtk-immodules %{_host} || :

%postun
/sbin/ldconfig
%{_bindir}/update-gtk-immodules %{_host} || :

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README THANKS TODO
%{_sysconfdir}/X11/xinit/xim.d/SCIM
%config %{_sysconfdir}/%{name}/config
%config %{_sysconfdir}/%{name}/global
%{_bindir}/*
%{_libdir}/lib%{name}-1.0.so.*
%{_libdir}/lib%{name}-gtkutils-1.0.so.*
%{_libdir}/lib%{name}-x11utils-1.0.so.*
%exclude %{_libdir}/*.la

%{_libdir}/gtk-2.0/immodules/im-%{name}.so
%{_libdir}/gtk-2.0/immodules/im-%{name}.la

%dir %{_libdir}/%{name}-1.0
%{_libdir}/%{name}-1.0/%{name}-launcher
%{_libdir}/%{name}-1.0/%{name}-panel-gtk
%{_libdir}/%{name}-1.0/%{name}-helper-launcher
%{_libdir}/%{name}-1.0/%{name}-helper-manager
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}/Config
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Config/*.so
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Config/*.la
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}/Filter
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Filter/*.so
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Filter/*.la
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}/FrontEnd
%{_libdir}/%{name}-1.0/%{scim_binary_version}/FrontEnd/*.so
%{_libdir}/%{name}-1.0/%{scim_binary_version}/FrontEnd/*.la
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}/IMEngine
%{_libdir}/%{name}-1.0/%{scim_binary_version}/IMEngine/*.so
%{_libdir}/%{name}-1.0/%{scim_binary_version}/IMEngine/*.la
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}/SetupUI
%{_libdir}/%{name}-1.0/%{scim_binary_version}/SetupUI/*.so
%{_libdir}/%{name}-1.0/%{scim_binary_version}/SetupUI/*.la
%dir %{_libdir}/%{name}-1.0/%{scim_binary_version}/Helper
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Helper/*.so
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Helper/*.la
%{_datadir}/applications/%{name}-setup.desktop
%{_datadir}/control-center-2.0/capplets/%{name}-setup.desktop
%{_datadir}/pixmaps/%{name}-setup.png
%{_datadir}/%{name}

%files devel
%defattr(-, root, root)
%doc docs/html docs/developers
%{_libdir}/lib%{name}-1.0.a
%{_libdir}/lib%{name}-1.0.so
%{_libdir}/lib%{name}-gtkutils-1.0.a
%{_libdir}/lib%{name}-gtkutils-1.0.so
%{_libdir}/lib%{name}-x11utils-1.0.a
%{_libdir}/lib%{name}-x11utils-1.0.so
%{_libdir}/gtk-2.0/immodules/im-%{name}.a
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/%{name}-gtkutils.pc
%{_libdir}/pkgconfig/%{name}-x11utils.pc
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Config/*.a
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Filter/*.a
%{_libdir}/%{name}-1.0/%{scim_binary_version}/FrontEnd/*.a
%{_libdir}/%{name}-1.0/%{scim_binary_version}/IMEngine/*.a
%{_libdir}/%{name}-1.0/%{scim_binary_version}/SetupUI/*.a
%{_libdir}/%{name}-1.0/%{scim_binary_version}/Helper/*.a
%{_includedir}/%{name}-1.0

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.9-14m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9-13m)
- remove lines for skim from Source1: xim.d-SCIM

* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.9-12m)
- Obsoletes: skim(-devel)
- Obsoletes: skim-honoka

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.9-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.9-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.9-9m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-8m)
- add a workaround for mozc-setup.so
-- if mozc-setup.so is in SetupUI, scim-setup causes segfault

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9-7m)
- modify %%post and %%postun for new gtk2

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.9-6m)
- add some la file

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9-5m)
- change PreReq to Requires(post) and Requires(postun)

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.9-4m)
- add scim-1.4.9-gtk-im-module-fake-event.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9-2m)
- remove scim-1.4.8-fix-dlopen.patch, KDE does not work with this patch

* Fri May 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.9-1m)
- update 1.4.9

* Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.8-2m)
- re-update to 1.4.8
-- revert r114 changes of src/scim_helper_manager_server.cpp
-- drop Patch8,12, probably not needed

* Wed Feb 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-9m)
- rollback to 1.4.7 (due to segfault scim-helper-man)
-- apply Patch8,12,25-30

* Wed Feb 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8
-- import Patch25-31 from Rawhide (1.4.8-1)
-- drop Patch8,12, probably not needed

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-8m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-7m)
- update Patch17 for fuzz=0
- License: LGPLv2+

* Wed Aug 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-6m)
- move gtk2 from Requires to PreReq

* Mon May 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-5m)
- remove scim_panel_gtk-icon-size-fixes.patch
- because patched icon in tray is too small

* Mon May 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-4m)
- Import Fedora Patch (Patch7 - Patch24)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.7-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-2m)
- %%NoSource -> NoSource

* Mon Jul  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-1m)
- version 1.4.7

* Tue Apr 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.6-1m)
- version 1.4.6
- update configs.patch

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-4m)
- BuildRequires: freetype2-devel -> freetype-devel

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.5-3m)
- delete libtool library

* Mon Oct 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-2m)
- remove unused directories
- do not use bootstrap for stable release

* Sun Oct 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-1m)
- update 1.4.5

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-0.20060301.5m)
- rebuild against glib-2.12.3 gtk+2.10.3 pango-1.14.3

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.4-0.20060301.4m)
- rebuild against expat-2.0.0-1m

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-0.20060301.3m)
- update configs.patch (change initial settings)
  remove /FrontEnd/SharedInputMethod = true

* Sun Mar 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-0.20060301.2m)
- update configs.patch (change initial settings)
  remove Control+space from /Hotkeys/FrontEnd/Trigger
  for emacs users

* Sat Mar 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-0.20060301.1m)
- update to 20060301 cvs snapshot (use opensuse tar-ball)
 +* Sat Mar 04 2006 - zsu@suse.de
 +- Upgrade to the latest cvs head to fix the following issues [#154308]
 +- Fixed a NULL pointer bug in scim_imengine_setup.cpp, which causes
 +  GTK-CRITICAL issue when launching scim-setup.
 +- Rename language name "Panjabi" to "Punjabi".
 +- Add 4 new languange names that required by m17n 1.3.0
 +- Fixed a bug in rawcode engine which prevents from inputting some
 +  code point.
- import configs.patch from opensuse
 +* Fri Aug 19 2005 - mfabian@suse.de
 +- Bugzilla #105319: make /FrontEnd/X11/Dynamic = false the
 +  default again to make sure that Compose works always.
 +- Bugzilla #105193: add "Zenkaku_Hankaku" to
 +  /Hotkeys/FrontEnd/Trigger (this is the trigger key used MS IME
 +  for Japanese).
- revise %%files
- BuildRequires: doxygen automake autoconf

* Tue Feb 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-3m)
- remove "-fno-strict-aliasing" from CFLAGS and CXXFLAGS
- add --enable-ld-version-script to configure

* Fri Jan 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-2m)
- add "-fno-strict-aliasing" to CFLAGS and CXXFLAGS for gcc-4.0.2-3m
  gcc-4.1.0-0.8m works fine without "-fno-strict-aliasing", don't forget 

* Fri Jan 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-1m)
- version 1.4.4

* Sat Dec 31 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-1m)
- version 1.4.3

* Tue Aug 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- version 1.4.2

* Thu Aug 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-1m)
- version 1.4.1

* Sat May 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-1m)
- version 1.2.3 (minor bugfix release)

* Tue Apr 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-1m)
- version 1.2.2 (minor bugfix release)

* Sat Mar 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-1m)
- version 1.2.1 (minor bugfix release)

* Fri Mar 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-4m)
- modify Source1: xim.d-SCIM
  use ':' instead of 'skim -d'

* Thu Mar 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-3m)
- modify xim.d/SCIM for skim

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-2m)
- add xim.d/SCIM

* Thu Mar 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- updated to 1.2.0

* Mon Nov 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.1-1m)
- updated to 1.0.1

* Wed Sep 29 2004 kourin <kourin@fh.freeserve.ne.jp>
- (1.0.0-2m)
- minor fix
 
* Tue Sep 21 2004 meke <meke@apost.plala.or.jp>
- (1.0.0-1m)
- import VineSeedPlus
- fix build.

* Wed Sep  8 2004 IWAI, Masaharu <iwai@alib.jp> 1.0.0-0vl1
- initial build

