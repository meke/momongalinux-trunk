%global momorel 6

Name:           bcm43xx-fwcutter
Version:        006
Release:        %{momorel}m%{?dist}
Summary:        Firmware extraction tool for Broadcom wireless driver

Group:          System Environment/Base
License:        GPLv2+
URL:            http://bcm43xx.berlios.de/
Source0:        http://download.berlios.de/bcm43xx/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:	README.Fedora
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package contains the 'bcm43xx-fwcutter' tool which is used to
extract firmware for the Broadcom network devices, from the official
Windows, MacOS or Linux drivers.

See the README.Fedora file shipped in the package's documentation for
instructions on using this tool.

%prep
%setup -q
sed -i -e 's/-O2/$(RPM_OPT_FLAGS)/' Makefile


%build
make
cp %{SOURCE1} .

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m0755 bcm43xx-fwcutter $RPM_BUILD_ROOT%{_bindir}/bcm43xx-fwcutter
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
install -m0644 bcm43xx-fwcutter.1 $RPM_BUILD_ROOT%{_mandir}/man1

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/bcm43xx-fwcutter
%{_mandir}/man1/*
%doc README README.Fedora COPYING

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (006-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (006-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (006-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (006-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (006-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (006-1m)
- import from Fedora

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 006-4
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 David Woodhouse <dwmw2@infradead.org> 006-3
- Update licence

* Wed Aug 22 2007 David Woodhouse <dwmw2@infradead.org> 006-2
- Rebuild

* Thu Mar 22 2007 David Woodhouse <dwmw2@infradead.org> 006-1
- Update to 006
- Remove obsolete modprobe.bcm43xx

* Sat Oct 14 2006 David Woodhouse <dwmw2@infradead.org> 005-1
- Update to 005

* Mon Sep 11 2006 David Woodhouse <dwmw2@infradead.org> 004-2
- Rebuild

* Fri Mar 31 2006 David Woodhouse <dwmw2@infradead.org> 004-1
- Update to 004

* Thu Mar 23 2006 David Woodhouse <dwmw2@infradead.org> 003-2
- Package review. Use $RPM_OPT_FLAGS, ship man page, etc.
- Complete documentation, add sample bcm43xx.modprobe file

* Wed Mar 22 2006 David Woodhouse <dwmw2@infradead.org> 003-1
- Initial build.

