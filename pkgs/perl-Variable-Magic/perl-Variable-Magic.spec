%global         momorel 3

Name:           perl-Variable-Magic
Version:        0.53
Release:        %{momorel}m%{?dist}
Summary:        Associate user-defined magic to variables from Perl
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Variable-Magic/
Source0:        http://www.cpan.org/authors/id/V/VP/VPIT/Variable-Magic-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.008
BuildRequires:  perl-base
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-XSLoader
Requires:       perl-base
Requires:       perl-XSLoader
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Magic is Perl's way of enhancing variables. This mechanism lets the user
add extra data to any variable and hook syntactical operations (such as
access, assignment or destruction) that can be applied to it. With this
module, you can add your own magic to any variable without having to write
a single line of XS.

%prep
%setup -q -n Variable-Magic-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorarch}/auto/Variable
%{perl_vendorarch}/Variable*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-2m)
- rebuild against perl-5.18.2

* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.16.3

* Mon Nov  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-2m)
- rebuild against perl-5.16.2

* Sun Aug 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.45-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.45-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-2m)
- rebuild against perl-5.12.2

* Sat Sep 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- rebuild against perl-5.12.0
- update to 0.41

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Sat Sep 26 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.37-1m)
- update to 0.37 (missing old src)

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Fri Jun 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- Specfile autogenerated by cpanspec 1.77 for Momonga Linux.
