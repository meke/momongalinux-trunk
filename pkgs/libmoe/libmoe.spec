%global momorel 7
Summary: Functions to handle multiple octets character encoding scheme
Name: libmoe
Version: 1.5.8
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Libraries
Source: http://pub.ks-and-ks.ne.jp/prog/pub/%{name}-%{version}.tar.gz
#Source: http://pub.ks-and-ks.ne.jp/prog/pub/libmoe-devel.tar.gz
NoSource: 0
URL: http://pub.ks-and-ks.ne.jp/prog/libmoe/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: libiso2mb

%description
The main functionalities are to calculate from a character encoded in multiple
octet, a non-negative integer, which is called Universal Code Point (UCP) for
convinience of description in this document, including complete information
about coded character set containing the character and codepoint of the
character in the set, and to reproduce the orignal octet sequence from the
integer.

%prep

%setup -q
#%%setup -q -n %{name}-devel

%build
make CF="%{optflags} -fpic -I."

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=%{_prefix} \
	     LIBSODIR=%{_libdir} \
	     MANDIR=%{_mandir} \
	     MANCOMPR='cat' MANX=''

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-, root, root)
%doc ChangeLog libmoe.shtml
%{_bindir}/*
%{_includedir}/moe
%{_libdir}/libmoe*
%{_libexecdir}/moe
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.8-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.8-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.8-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.8-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.8-2m)
- rebuild against gcc43

* Tue Feb  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.8-1m)
- version up
- remove libmoe-1.5.6-gcc34.patch

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.5.6-3m)
- enable x86_64.

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.6-2m)
- add Patch1: libmoe-1.5.6-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Fri Aug 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.6-1m)

* Tue Aug 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.5-1m)

* Sun Apr 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.3-2k)

* Tue Feb 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.2-2k)

* Sat Feb  2 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.0-2k)

* Mon Dec 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.4.1-2k)

* Tue Dec  4 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.4.0-2k)

* Sun Jun  2 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.3.9-2k)
- modify BuildRoot

* Sat Jun  2 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.9-3k)

* Wed May 30 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.8-3k)

* Sun May 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.5-3k)

* Thu Apr 26 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.4-3k)

* Tue Mar 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.0-3k)

* Sat Feb 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-3k)

* Fri Feb  9 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.0-3k)

* Fri Jan 26 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.1.1-3k)

* Wed Jan 24 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.1.0-3k)

* Thu Jan 18 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.0.11-3k)

