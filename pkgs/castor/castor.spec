%global momorel 11
%global _gcj_support 0

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

Summary:        An open source data binding framework for Java
Name:           castor
Version:        0.9.5
Release:        1jpp.%{momorel}m%{?dist}
Epoch:          0
Group:          Development/Libraries
License:        BSD
URL:            http://castor.codehaus.org/
Source0: http://dist.codehaus.org/castor/%{version}/castor-%{version}-src.tgz 
NoSource: 0
Patch0:         example-servletapi4.patch
Patch1:         example-servletapi5.patch
Patch2:         castor-build-xml.patch
Patch3:         ClobImpl_for_java6.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%if ! %{gcj_support}
BuildArch:      noarch
%endif
Requires:       adaptx
Requires:       jdbc-stdext
Requires:       jndi
Requires:       jta
Requires:       ldapjdk
Requires:       log4j
Requires:       oro
Requires:       regexp
Requires:       xerces-j2
BuildRequires:  adaptx
BuildRequires:  log4j
BuildRequires:  ant
BuildRequires:  jdbc-stdext
BuildRequires:  jndi
BuildRequires:  jpackage-utils >= 0:1.5.16
BuildRequires:  jta
BuildRequires:  ldapjdk
BuildRequires:  oro
BuildRequires:  regexp
BuildRequires:  xerces-j2

%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description
Castor is an open source data binding framework for Java. It's basically
the shortest path between Java objects, XML documents and SQL tables.
Castor provides Java to XML binding, Java to SQL persistence, and more.

%package demo
Group:          Development/Libraries
Summary:        Demo for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       servletapi5
BuildRequires:  servletapi5 

%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description demo
Demonstrations and samples for %{name}.

%package test
Group:          Development/Libraries
Summary:        Tests for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       junit
BuildRequires:  junit

%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description test
Tests for %{name}.

%package xml
Group:          Development/Libraries
Summary:        XML support for %{name}.
Requires:       %{name} = %{epoch}:%{version}-%{release}

%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description xml
XML support for Castor

%package javadoc
Group:          Documentation
Summary:        Javadoc for %{name}

%description javadoc
Javadoc for %{name}.

%package doc
Summary:        Documentation for %{name}
Group:          Documentation

%description doc
Documentation for %{name}.

%prep
%setup -q
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;
perl -p -i -e 's|org.apache.xerces.utils.regex|org.apache.xerces.impl.xpath.regex|g;' \
src/main/org/exolab/castor/util/XercesRegExpEvaluator.java
find . -name "*.java" -exec perl -p -i -e 's|assert\(|assertTrue\(|g;' {} \;
find . -name "*.java" -exec perl -p -i -e 's|_test.name\(\)|_test.getName\(\)|g;' {} \;
find src/doc -name "*.xml" -exec perl -p -i -e 's|\222|&#x92;|g;' {} \;
%patch0
%patch1
%patch2
%patch3

# Fix for wrong-file-end-of-line-encoding problem
for i in `find src/doc -iname "*.css"`; do sed -i 's/\r//' $i; done
for i in `find src/doc -iname "*.xsd"`; do sed -i 's/\r//' $i; done
for i in `find src/doc -iname "*.dtd"`; do sed -i 's/\r//' $i; done
for i in `find src/doc -iname "*.pdf"`; do sed -i 's/\r//' $i; done
for i in `find src/doc -iname "*.htm"`; do sed -i 's/\r//' $i; echo "" >> $i; done
sed -i 's/\r//' src/etc/README
sed -i 's/\r//' src/etc/LICENSE
sed -i 's/\r//' src/etc/CHANGELOG
sed -i 's/Class-Path: xerces.jar jdbc-se2.0.jar jndi.jar jta1.0.1.jar//' src/etc/MANIFEST.MF

%build
export CLASSPATH=%(build-classpath adaptx jdbc-stdext jndi jta junit ldapjdk oro regexp servletapi5 xerces-j2)
ant -Dant.build.javac.source=1.4 -buildfile src/build.xml jar
ant -Dant.build.javac.source=1.4 -buildfile src/build.xml examples
ant -Dant.build.javac.source=1.4 -buildfile src/build.xml CTFjar
ant -Dant.build.javac.source=1.4 -buildfile src/build.xml javadoc

%install
rm -rf $RPM_BUILD_ROOT

# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 dist/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
install -m 644 dist/%{name}-%{version}-xml.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-xml-%{version}.jar
install -m 644 dist/CTF-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-tests-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# examples (demo)
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/examples
cp -pr build/examples/* $RPM_BUILD_ROOT%{_datadir}/%{name}/examples

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/doc/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# do this last, since it will delete all build directories
export CLASSPATH=%(build-classpath log4j adaptx)
ant -buildfile src/build.xml doc

# like magic
%jpackage_script org.exolab.castor.builder.SourceGenerator %{nil} %{nil} xerces-j2:%{name} %{name}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm --exclude %{_datadir}/%{name}/examples/webapp-example-castor.war --exclude %{_datadir}/%{name}/examples/webapp/WEB-INF/lib/castor-0.9.5.jar
%endif

%clean
rm -rf $RPM_BUILD_ROOT


%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%post test
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun test
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%post xml
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun xml
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%post demo
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun demo
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc src/etc/{CHANGELOG,LICENSE,README}
%attr(0755,root,root) %{_bindir}/%{name}
%{_javadir}/%{name}-%{version}.jar
%{_javadir}/%{name}.jar
%dir %{_datadir}/%{name}

%if %{gcj_support}
%dir %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/castor-0.9.5.jar.*
%endif

%files demo
%defattr(0644,root,root,0755)
%{_datadir}/%{name}/examples

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/examples.*
%endif

%files test
%defattr(0644,root,root,0755)
%{_javadir}/%{name}-tests-%{version}.jar
%{_javadir}/%{name}-tests.jar

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/castor-tests-0.9.5.jar.*
%endif

%files xml
%defattr(0644,root,root,0755)
%{_javadir}/%{name}-xml-%{version}.jar
%{_javadir}/%{name}-xml.jar

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

%files doc
%defattr(0644,root,root,0755)
%doc build/doc/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-1jpp.11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-1jpp.10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-1jpp.9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1jpp.8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.9.5-1jpp.7m)
- add JDBC4.0 API method to build with OpenJDK

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1jpp.6m)
- build with -Dant.build.javac.source=1.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1jpp.5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-1jpp.4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-1jpp.3m)
- %%NoSource -> NoSource

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-1jpp.2m)
- modify %%files

* Thu Jun  7 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-1jpp.1m)
- import from Fedora

* Wed Apr 18 2007 Permaine Cheung <pcheung@redhat.com> - 0:0.9.5-1jpp.8
- Update spec file as per fedora review process.

* Thu Aug 03 2006 Deepak Bhole <dbhole@redhat.com> - 0:0.9.5-1jpp.7
- Added missing requirements.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:0.9.5-1jpp_6fc
- Rebuilt

* Thu Jul  20 2006 Deepak Bhole <dbhole@redhat.com> - 0:0.9.5-1jpp_5fc
- Added conditional native compilation.
- Added missing BR/R for log4j.

* Thu Jun  8 2006 Deepak Bhole <dbhole@redhat.com> - 0:0.9.5-1jpp_4fc
- Updated project URL -- fix for Bug #180586

* Wed Mar  8 2006 Rafael Schloming <rafaels@redhat.com> - 0:0.9.5-1jpp_3fc
- excluded s390[x] and ppc64 due to eclipse

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:0.9.5-1jpp_2fc
- stop scriptlet spew

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Jun 16 2005 Gary Benson <gbenson@redhat.com> 0:0.9.5-1jpp_1fc
- Build into Fedora.

* Fri Jun 10 2005 Gary Benson <gbenson@redhat.com>
- Remove jarfiles and classfiles from the tarball.

* Thu Jun  2 2005 Gary Benson <gbenson@redhat.com>
- Fix up (alleged) invalid characters in the documentation.

* Fri Jul 23 2004 Fernando Nasser <fnasser@redhat.com> 0:0.9.5-1jpp_3rh
- use servletapi5 instead of servletapi4

* Thu Mar 11 2004 Frank Ch. Eigler <fche@redhat.com> 0:0.9.5-1jpp_2rh
- try servletapi4 instead of servletapi3
- add example-servletapi4 patch

* Thu Mar  4 2004 Frank Ch. Eigler <fche@redhat.com> 0:0.9.5-1jpp_1rh
- RH vacuuming

* Tue Sep 09 2003 David Walluck <david@anti-microsoft.org> 0:0.9.5-1jpp
- 0.9.5

* Fri May 16 2003 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net> 0:0.9.4.3-2jpp
- use same lsapjdk package as tyrex

* Sat May 10 2003 David Walluck <david@anti-microsoft.org> 0:0.9.4.3-1jpp
- release
