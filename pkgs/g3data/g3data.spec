%global momorel 7

Summary:        Program for extracting the data from scanned graphs
Name:           g3data
Version:        1.5.3
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Engineering
URL:            http://www.frantz.fi/software/g3data.php
Source0:        http://www.frantz.fi/software/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        %{name}.desktop
Source2:        %{name}-icon.xpm
Source3:        %{name}-icon.svg
Source4:        %{name}-icon.png
Patch0:         %{name}-Makefile.patch
Patch1:         %{name}-1.5.3-sgmlspl.patch
Patch2:         %{name}-%{version}-linking.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk2-devel
BuildRequires:  desktop-file-utils
BuildRequires:  docbook-utils
BuildRequires:  opensp

%description
g3data is used for extracting data from graphs. In publications
graphs often are included, but the actual data is missing, g3data
makes this extracting process much easier.

%prep
%setup -q

%patch0
%patch1 -p1 -b .sgmlspl~
%patch2 -p1 -b .link

%build
export CPPFLAGS="$RPM_OPT_FLAGS"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_datadir}/man/man1
mkdir -p %{buildroot}/%{_datadir}/pixmaps/%{name}

make \
prefix=%{?buildroot:%{buildroot}}%{_prefix} \
bindir=%{?buildroot:%{buildroot}}%{_bindir} \
datadir=%{?buildroot:%{buildroot}}%{_datadir} \
mandir=%{?buildroot:%{buildroot}}%{_mandir} \
install

chmod a-x %{buildroot}%{_mandir}/man1/%{name}.1.gz

cp %{SOURCE2}  %{buildroot}/%{_datadir}/pixmaps/
cp %{SOURCE2}  %{buildroot}/%{_datadir}/pixmaps/%{name}/
cp %{SOURCE3}  %{buildroot}/%{_datadir}/pixmaps/%{name}/
cp %{SOURCE4}  %{buildroot}/%{_datadir}/pixmaps/%{name}/

mkdir -p %{buildroot}/%{_datadir}/applications
desktop-file-install --vendor=                \
  --dir=%{buildroot}/%{_datadir}/applications \
  %{SOURCE1}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.TEST test1.png test1.values test2.png test2.values gpl.txt
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}
%{_datadir}/pixmaps/%{name}-icon.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3-5m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-4m)
- fix build

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-3m)
- fix up desktop file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.1-1m)
- import to Momonga from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.5.1-8
- Autorebuild for GCC 4.3

* Tue Aug 21 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-7
- Bump for rebuild

* Fri Aug 03 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-6
- add GPLv2+ license tag string for updated fedora licensing policy

* Mon Mar 26 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-5
- fix unowned directory oops

* Wed Mar 21 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-4
- clean up of buildrequires and ensure use of RPM_OPT_FLAGS

* Sun Mar 18 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-3
- Minor pixmap location fix
- Updated upstream url locations

* Sat Mar 17 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-2
- Added desktop file and icon

* Fri Mar 16 2007 Jef Spaleta <jspaleta@gmail.com> 1.5.1-1
- Initial package meant for submission into Fedora Extras

