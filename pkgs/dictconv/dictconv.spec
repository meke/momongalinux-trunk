%global momorel 5

Summary: Dictionary Converter
Name: dictconv
Version: 0.2
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://ktranslator.sourceforge.net/
Group: Applications/Text
Source0: http://dl.sourceforge.net/sourceforge/ktranslator/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-gcc43.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: libxml2-devel

%description
The program detects the type of dictionary by its extension:

.bgl for Babylon glossaries
.dct for Sdictionary dictionaries.
.dic for plain text dictionaries
.ifo for StarDict dictionaries
.index for DICT dictionaries
.tei for Freedict dictionaries (XML format)

Currently, it supports converting from Babylon glossaries,
Freedict dictionaries, Sdictionary dictionaries and Stardict dictionaries
to DICT dictionaries, plain text dictionaries and StarDict dictionaries.
More file types will be added in new versions. When converting to StarDict
and DICT, the .dict file is not compressed.
This feature will be added in future.

%prep
%setup -q

%patch0 -p1 -b .gcc43

autoreconf -fi

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/dictconv

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-1m)
- initial package for qtrans-0.2.1.4
- Summary, %%description and gcc43.patch are imported from dictconv-0.2-0.pm.1.src.rpm
