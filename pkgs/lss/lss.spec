%global momorel 5

Summary: LaTeX Symbol Selector
Name: lss
Version: 0.1.6
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Engineering
URL: http://clayo.org/lss/
Source0: http://clayo.org/lss/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel, libxml2-devel

%description
LSS is a symbol browser to help creating LaTeX documents with many math symbols. 
All symbols are grouped into categories and user can copy symbol name to
system-wide clipboard (or insert it directly to first running copy of gVIM)
by selecting symbol icon from list.

%prep
%setup -q

%build
%configure
%make


%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files                                           
%defattr(-, root, root)                          
%doc AUTHORS COPYING*
%{_bindir}/lss
%{_datadir}/applications/lss.desktop
%{_datadir}/locale/pl/LC_MESSAGES/lss.mo
%{_datadir}/pixmaps/lss.svg
%{_datadir}/pixmaps/lss.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.6-1m)
- version up 0.1.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.3-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-2m)
- %%NoSource -> NoSource

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-1m)
- update to 0.1.3

* Fri Feb 23 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.2-1m)
- update to 0.1.2

* Thu Jan 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Tue Jan 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.0-1m)
- first release


