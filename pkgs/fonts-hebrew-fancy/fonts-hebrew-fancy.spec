%global momorel 8

%define culmus_version 0.101

Name: fonts-hebrew-fancy
Version: 0.20051122
Release: %{momorel}m%{?dist}
License: GPL
Source0: http://culmus.sourceforge.net/fancy/comix.tar.gz
Source1: http://culmus.sourceforge.net/fancy/dorian.tar.gz
Source2: http://culmus.sourceforge.net/fancy/gan.tar.gz
Source3: http://culmus.sourceforge.net/fancy/gladia.tar.gz
Source4: http://culmus.sourceforge.net/fancy/ktav-yad.tar.gz
Source5: http://culmus.sourceforge.net/fancy/ozrad.tar.gz
Source6: http://culmus.sourceforge.net/fancy/anka.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Group: User Interface/X
Summary: Fancy fonts for Hebrew
URL: http://culmus.sourceforge.net/fancy
Requires: fontpackages-filesystem

%description
The fonts-hebrew-fancy package contains fancy (non-standard) Hebrew fonts 
from the Culmus project by Maxim Iorsh.

%prep
%setup -c
%setup -T -D -a 1
%setup -T -D -a 2
%setup -T -D -a 3
%setup -T -D -a 4
%setup -T -D -a 5
%setup -T -D -a 6

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_datadir}/fonts/hebrew-fancy
cp *.pfa *.afm $RPM_BUILD_ROOT%{_datadir}/fonts/hebrew-fancy
# %%ghost the fonts.cache-1 file
touch $RPM_BUILD_ROOT%{_datadir}/fonts/hebrew-fancy/fonts.cache-{1,2}

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ -x %{_bindir}/fc-cache ]; then
  %{_bindir}/fc-cache %{_datadir}/fonts/hebrew-fancy
fi

%postun
if [ "$1" = "0" ]; then
  if [ -x %{_bindir}/fc-cache ]; then
    %{_bindir}/fc-cache %{_datadir}/fonts
  fi
fi

%files
%defattr(-,root,root,-)
%doc GNU-GPL
%dir %{_datadir}/fonts/hebrew-fancy
%{_datadir}/fonts/hebrew-fancy/*.pfa
%{_datadir}/fonts/hebrew-fancy/*.afm
%ghost %verify(not md5 size mtime) %{_datadir}/fonts/hebrew-fancy/fonts.cache-1
%ghost %verify(not md5 size mtime) %{_datadir}/fonts/hebrew-fancy/fonts.cache-2

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20051122-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20051122-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20051122-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20051122-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20051122-4m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20051122-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20051122-2m)
- rebuild against gcc43

* Sun Jun 10 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20051122-1m)
- import from Fedora

* Mon May 14 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 0.20051122-2
- robust scriptlets (#238917)

* Sat Sep 16 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.20051122-1
- Rebuild for Fedora Extras 6
- Make version number unrelated to the main Culmus version number. Bug #176051
* Tue Nov 22 2005 Dan Kenigsberg <danken@cs.technion.ac.il> - 0.101-2
- Clean things according to the review of mpeters AT mac.com
* Tue Nov 22 2005 Dan Kenigsberg <danken@cs.technion.ac.il> - 0.101-1
- Take fonts-hebrew.spec and change it for the fancy fonts.
