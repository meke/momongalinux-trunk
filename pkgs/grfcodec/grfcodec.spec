%global momorel 1
#global prever r821

Name:           grfcodec
Version:        6.0.2
Release: %{momorel}m%{?dist}
Summary:        A suite of programs to modify Transport Tycoon Deluxe's GRF files
Group:          Development/Tools
License:        GPLv2+
URL:            http://dev.openttdcoop.org/projects/grfcodec
Source0:        http://binaries.openttd.org/extra/grfcodec/%{version}/grfcodec-%{version}-source.tar.gz
NoSource: 0
#Source0:        http://binaries.openttd.org/extra/grfcodec-nightly/%{prever}/grfcodec-nightly-%{prever}-source.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  boost-devel libpng-devel
Obsoletes:      nforenum < 4.0.0-2
Provides:       nforenum = 4.0.0-2


%description
A suite of programs to modify Transport Tycoon Deluxe's GRF files.


%prep
%setup -q


%build
cat << EOF >> Makefile.local
STRIP=true
V=1
CXXFLAGS=%{optflags}
prefix=%{_prefix}
DO_NOT_INSTALL_DOCS=1
DO_NOT_INSTALL_CHANGELOG=1
DO_NOT_INSTALL_LICENSE=1
EOF

%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc changelog.txt COPYING
%doc docs/*.txt
%{_bindir}/grf*
%{_bindir}/nforenum
%{_mandir}/man1/grf*.1.bz2
%{_mandir}/man1/nforenum.1.bz2


%changelog
* Sat Apr 20 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (6.0.2-1m)
- update to 6.0.2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0.0-3m)
- rebuild for boost

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0.0-2m)
- reimport from fedora

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (5.1.2-2m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.2-2m)
- rebuild for boost-1.48.0

* Sat Dec 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (5.1.2-1m)
- update to 5.1.2

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (5.1.1-2m)
- rebuild for boost

* Thu Jun 23 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (5.1.1-1m)
- update to 5.1.1
 
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.0-2m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.0-2m)
- rebuild against boost-1.46.1

* Thu Mar 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (5.1.0-1m)
- update to 5.1.0
- change source URI.
- NoSource: 0
- now includes nforenum

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.11-0.2305.3m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.11-0.2305.3m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.11-0.2305.3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.11-0.2305.3m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.11-0.2305.2m)
- fix version

* Sun Feb 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.9.11-0.2305.1m)
- Import from Fedora.
- update to r2305.

* Sun May 03 2009 Iain Arnell <iarnell@gmail.com> 0.9.11-0.2.r2111
- fix license tag (GPLv2+)
- don't pass -O3 to gcc
- doesn't BR subversion

* Sat May 02 2009 Iain Arnell <iarnell@gmail.com> 0.9.11-0.1.r2111
- initial release
