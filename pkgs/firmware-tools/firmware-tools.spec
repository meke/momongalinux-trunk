%global momorel 1

%define major 2
%define minor 1
%define micro 14
%define extra %{nil}
%define release_version %{major}.%{minor}.%{micro}%{extra}

# per fedora python packaging guidelines
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           firmware-tools
Version:        %{release_version} 
Release:        %{momorel}m%{?dist}
Summary:        Scripts and tools to manage firmware and BIOS updates

Group:          Applications/System
License:        GPLv2+ or "OSL 2.1"
URL:            http://linux.dell.com/libsmbios/download/
Source0:        http://linux.dell.com/libsmbios/download/%{name}/%{name}-%{version}/%{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# SUSE doesnt have noarch python, so for SUSE, always build arch-dependent
%if ! 0%{?suse_version}
BuildArch:      noarch
%endif

BuildRequires:  python-devel, rpm-python
Requires: rpm-python, pciutils
Provides: firmware_inventory(pci) = 0:%{release_version}

# packages that dont conform to latest ABI
Conflicts: firmware_addon_dell < 0:2.1.0
Conflicts: dell-dup < 0:1.1.0

%description
The firmware-tools project provides tools to inventory hardware and a plugin
architecture so that different OEM vendors can provide different inventory
components. It is intended to tie to the package system to enable seamless
installation of updated firmware via your package manager, as well as provide
a framework for BIOS and firmware updates.


%prep
%setup -q


%build
# this line lets us build an RPM directly from a git tarball
[ -e ./configure ] || \
    RELEASE_MAJOR=%{major}  \
    RELEASE_MINOR=%{minor}  \
    RELEASE_MICRO=%{micro}  \
    RELEASE_EXTRA=%{extra}  \
    ./autogen.sh --no-configure

# fix problems when buildsystem time is out of sync. ./configure will
# fail if newly created files are older than the packaged files.
# this should normally be a no-op on proper buildsystems.
touch configure
find . -type f -newer configure -print0 | xargs -r0 touch

%configure
make -e %{?_smp_mflags}

%check
make -e %{?_smp_mflags} check

%install
# Fedora Packaging guidelines
rm -rf $RPM_BUILD_ROOT
# SUSE Packaging rpmlint
mkdir $RPM_BUILD_ROOT

make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/firmware/firmware.d/
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/firmware

# backwards compatibility symlinks
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
ln -s firmwaretool $RPM_BUILD_ROOT/%{_sbindir}/inventory_firmware
ln -s firmwaretool $RPM_BUILD_ROOT/%{_sbindir}/bootstrap_firmware
ln -s firmwaretool $RPM_BUILD_ROOT/%{_sbindir}/update_firmware
ln -s %{_sbindir}/firmwaretool $RPM_BUILD_ROOT/%{_bindir}/update_firmware


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING-GPL COPYING-OSL COPYING.LIB README
%{python_sitelib}/*
%attr(0755,root,root) %{_sbindir}/*
%attr(0755,root,root) %{_bindir}/*
%{_datadir}/firmware-tools/
%dir %{_sysconfdir}/firmware
%dir %{_sysconfdir}/firmware/firmware.d
%config(noreplace) %{_sysconfdir}/firmware/firmware.conf
%{_datadir}/firmware/


%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.14-1m)
- update 2.1.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.9-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.9-1m)
- sync with Fedora 13 (2.1.9-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.6-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.6-2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.5.6-2.1
- Rebuild for Python 2.6

* Thu Aug 23 2007 Michael E Brown <michael_e_brown at dell.com> - 1.5.6-1
- rebase to upstream release

* Fri Aug 17 2007 Michael E Brown <michael_e_brown at dell.com> - 1.5.5-1
- rebase to upstream release

* Fri Aug 17 2007 Michael E Brown <michael_e_brown at dell.com> - 1.4.2-1
- rebase to upstream release

* Tue May 1 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.6-1
- disable empty debuginfo package

* Tue Mar 20 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.5-1
- Remove python-abi dep for RHEL3 (it was broken)

* Fri Mar 16 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.4-1
- fix typo in sitelib path -- only for RHEL3 build

* Wed Mar 14 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.3-1
- create and own {_sysconfdir}/firmware/firmware.d/ for plugins.
- Fedora review changes

* Mon Mar 12 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.0-1
- Fedora-compliant packaging changes.
