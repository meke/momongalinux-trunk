%global momorel 15
%global ocamlver 3.12.1

%global debug_package %{nil}

Summary: Functional Constraint Library implemented in Objective Caml
Name: ocaml-facile
Version: 1.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source0: http://www.recherche.enac.fr/opti/facile/distrib/facile-%{version}.tar.gz 
NoSource: 0
URL: http://www.recherche.enac.fr/opti/facile/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: facile = %{version}-%{release}
Obsoletes: facile
BuildRequires: ocaml >= %{ocamlver}
Requires: ocaml-runtime >= %{ocamlver}

%description
FaCiLe is a constraint programming library on integer and integer set 
finite domains written in OCaml. It offers all usual facilities to create and 
manipulate finite domain variables, arithmetic expressions and constraints 
(possibly non-linear), built-in global constraints (difference, cardinality, 
sorting etc.) and search and optimization goals. FaCiLe allows as well to 
build easily user-defined constraints and goals (including recursive ones), 
making pervasive use of OCaml higher-order functionals to provide a simple 
and flexible interface for the user. As FaCiLe is an OCaml library and not 
"yet another language", the user benefits from type inference and strong 
typing discipline, high level of abstraction, modules and objects system, 
as well as native code compilation efficiency, garbage collection and replay 
debugger, all features of OCaml (among many others) that allow to prototype 
and experiment quickly: modeling, data processing and interface are 
implemented with the same powerful and efficient language. For a more 
complete description, you may consult the preface and foreword of the online 
documentation

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: facile = %{version}-%{release}
Obsoletes: facile

%description devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
rm -rf %{buildroot}

%setup -q -n facile-%{version}

%build
./configure

%make
make check

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/ocaml/facile
cp src/facile.cmi src/facile.cma src/facile.cmxa src/facile.a %{buildroot}%{_libdir}/ocaml/facile
chmod a+r %{buildroot}%{_libdir}/ocaml/facile/facile.cmi
chmod a+r %{buildroot}%{_libdir}/ocaml/facile/facile.cma
chmod a+r %{buildroot}%{_libdir}/ocaml/facile/facile.cmxa
chmod a+r %{buildroot}%{_libdir}/ocaml/facile/facile.a

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE README
%{_libdir}/ocaml/facile/facile.cmi
%{_libdir}/ocaml/facile/facile.cma

%files devel
%defattr(-,root,root)
%doc LICENSE README
%{_libdir}/ocaml/facile/facile.a
%{_libdir}/ocaml/facile/facile.cmxa

%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-15m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-12m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-11m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-9m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-8m)
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-7m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-6m)
- rebuild against ocaml-3.10.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-5m)
- %%NoSource -> NoSource

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against ocaml-3.10.1-1m

* Mon Oct  1 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- disable debug_package

* Sat Sep 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rename to ocaml-facile and split packages
- rebuild against ocaml-3.10.0-1m

* Mon Nov 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- import from Vine Seed

* Thu Mar 23 2006 AKIYAMA Kazuhito <akiyama@yb3.so-net.ne.jp> 1.1-0vl4
- rebuild with ocaml-3.09.1 on VineSeed

* Thu Mar 23 2006 AKIYAMA Kazuhito <akiyama@yb3.so-net.ne.jp> 1.1-0vl3
- rebuild with ocaml-3.09.1 on Vine3.2

* Mon Feb  6 2006 AKIYAMA Kazuhito <akiyama@yb3.so-net.ne.jp> 1.1-0vl2
- rebuild for VineSeed

* Mon Feb  6 2006 AKIYAMA Kazuhito <akiyama@yb3.so-net.ne.jp> 1.1-0vl1
- initial release
