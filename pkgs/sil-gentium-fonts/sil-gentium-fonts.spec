%global momorel 7

%define fontname sil-gentium
%define archivename ttf-sil-gentium
%define common_desc \
SIL Gentium ("belonging to the nations" in Latin) is a Unicode typeface family\
designed to enable the many diverse ethnic groups around the world who use\
the Latin script to produce readable, high-quality publications. It supports\
a wide range of Latin-based alphabets.


Name:           %{fontname}-fonts
Version:        1.02
Release:        %{momorel}m%{?dist}
Summary:        SIL Gentium fonts

Group:          User Interface/X
License:        OFL
URL:            http://scripts.sil.org/Gentium_linux
# Source0 can be downloaded from the above URL, search for "tar.gz"
Source0:        %{archivename}_1.0.2.tar.gz
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel

Requires:       %{name}-common = %{version}-%{release}

# Obsoleting and providing the old RPM name
Obsoletes:      gentium-fonts < 1.02-7

%description
%common_desc

This package consists of the main SIL Gentium family.


%_font_pkg Gen[RI]*.ttf


%package common
Summary:        Common files of SIL Gentium fonts
Group:          User Interface/X
Requires:       fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.


%package -n %{fontname}-alt-fonts
Summary:        SIL GentiumAlt fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-alt-fonts
%common_desc

This package consists of the SIL GentiumAlt family. GentiumAlt is a
alternative version of Gentium with flatter diacratics, to make it more
suitable for languages that use stacking diacratics, like Vietnamese. There
is no problem with having both Gentium and GentiumAlt installed at the same
time.


%_font_pkg -n alt GenA*.ttf


%prep
%setup -q -n %{archivename}-%{version}

# Convert GENTIUM-FAQ from MacRoman
iconv --from=MACINTOSH --to=UTF-8 GENTIUM-FAQ > GENTIUM-FAQ.new
touch -c -r GENTIUM-FAQ GENTIUM-FAQ.new
mv GENTIUM-FAQ.new GENTIUM-FAQ


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc FONTLOG GENTIUM-FAQ OFL OFL-FAQ QUOTES README

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.02-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.02-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.02-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 29 2009 Roozbeh Pournader <roozbeh@gmail.com> 1.02-8
- Better description for GentiumAlt subpackage, remove
  Provides (Nicolas Mailhot)

* Tue Jan 27 2009 Roozbeh Pournader <roozbeh@gmail.com> 1.02-7
- Update to new F11 fonts policy, including renaming and splitting

* Mon Jul 21 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.02-6
- fix license tag

* Thu Nov 09 2006 Roozbeh Pournader <roozbeh@farsiweb.info> 1.02-5
- Remove ghost-ed fontconfig caches (fontconfig mechanism is changed)

* Tue Sep 19 2006 Kevin Fenzi <kevin@tummy.com> 1.02-4
- Rebuild for Fedora Extras 6
- Add dist tag

* Mon Feb 13 2006 Roozbeh Pournader <roozbeh@farsiweb.info> 1.02-3
- Rebuild for Fedora Extras 5

* Wed Dec 21 2005 Roozbeh Porunader <roozbeh@farsiweb.info> 1.02-2
- Added comment to Source0 about where to get the file
- Added Provides and Obsoletes for upsteam RPM name

* Mon Dec 19 2005 Roozbeh Pournader <roozbeh@farsiweb.info> 1.02-1
- Initial packaging, borrowing many things from dejavu-fonts
