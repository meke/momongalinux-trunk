%global momorel 1
%global qtver 4.8.5
%global kdever 4.12.2
%global kdelibsrel 1m
%global contents_id 29024

Summary: A frontend to various audio converters
Name: soundkonverter
Version: 2.0.91
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.kde-apps.org/content/show.php?content=%{content_id}
#Source0: http://kde-apps.org/CONTENT/content-files/#{contents_id}-#{name}-#{version}.tar.gz
Source0: https://github.com/HessiJames/%{name}/archive/v%{version}.tar.gz
NoSource: 0
Source2: %{name}rc
Patch0: %{name}-2.0.90-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: TiMidity++
Requires: cdparanoia
Requires: faac
Requires: faad2
Requires: flac
Requires: ffmpeg
Requires: fluidsynth
Requires: icedax
Requires: lame
Requires: mplayer
Requires: speex
Requires: vorbis-tools
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cdparanoia-devel
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: desktop-file-utils >= 0.16
BuildRequires: fluidsynth-devel
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: libxml2
BuildRequires: taglib-devel
BuildRequires: kdemultimedia-devel

%description
soundKonverter is frontend to various audio converters.

It is extendable by plugins and supports many backends:
lame, oggenc, flac, musepack, faac, mplayer, ffmpeg - just to mention a few.
This way you can convert between various audio formats:
mp3, ogg, flac, mpc, aac, m4a, rm and wma, avi, flv (the latter reading only) for example.
soundKonverter supports reading and writing tags for many formats, so the tags are preserved when converting files.

The key features are:
- Audio conversion
- Replay Gain calculation
- CD ripping

soundKonverter comes with an Amarok script that allows you to easily transcode files when transfering to your media device.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ../src
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop files
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --add-category=KDE \
  --add-category=Qt \
  --remove-category=CD \
  %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

# # install soundkonverterrc
# mkdir -p %%{buildroot}%%{_datadir}/config
# install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/config/

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png
%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc src/CHANGELOG src/INSTALL src/README
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/%{name}_codec_aften.so
%{_kde4_libdir}/kde4/%{name}_codec_faac.so
%{_kde4_libdir}/kde4/%{name}_codec_ffmpeg.so
%{_kde4_libdir}/kde4/%{name}_codec_flac.so
%{_kde4_libdir}/kde4/%{name}_codec_flake.so
%{_kde4_libdir}/kde4/%{name}_codec_fluidsynth.so
%{_kde4_libdir}/kde4/%{name}_codec_lame.so
%{_kde4_libdir}/kde4/%{name}_codec_libav.so
%{_kde4_libdir}/kde4/%{name}_codec_mac.so
%{_kde4_libdir}/kde4/%{name}_codec_mplayer.so
%{_kde4_libdir}/kde4/%{name}_codec_musepack.so
%{_kde4_libdir}/kde4/%{name}_codec_neroaac.so
%{_kde4_libdir}/kde4/%{name}_codec_opustools.so
%{_kde4_libdir}/kde4/%{name}_codec_shorten.so
%{_kde4_libdir}/kde4/%{name}_codec_speex.so
%{_kde4_libdir}/kde4/%{name}_codec_timidity.so
%{_kde4_libdir}/kde4/%{name}_codec_ttaenc.so
%{_kde4_libdir}/kde4/%{name}_codec_twolame.so
%{_kde4_libdir}/kde4/%{name}_codec_vorbistools.so
%{_kde4_libdir}/kde4/%{name}_codec_wavpack.so
%{_kde4_libdir}/kde4/%{name}_filter_normalize.so
%{_kde4_libdir}/kde4/%{name}_filter_sox.so
%{_kde4_libdir}/kde4/%{name}_replaygain_mp3gain.so
%{_kde4_libdir}/kde4/%{name}_replaygain_musepackgain.so
%{_kde4_libdir}/kde4/%{name}_replaygain_aacgain.so
%{_kde4_libdir}/kde4/%{name}_replaygain_metaflac.so
%{_kde4_libdir}/kde4/%{name}_replaygain_vorbisgain.so
%{_kde4_libdir}/kde4/%{name}_replaygain_wvgain.so
%{_kde4_libdir}/kde4/%{name}_ripper_cdparanoia.so
%{_kde4_libdir}/kde4/%{name}_ripper_icedax.so
%{_kde4_libdir}/lib%{name}core.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/solid/actions/soundkonverter-rip-audiocd.desktop
%{_kde4_appsdir}/%{name}
# %%{_kde4_configdir}/%%{name}rc
# %%{_kde4_docdir}/HTML/*/%%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}-replaygain.png
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_datadir}/kde4/services/%{name}_codec_aften.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_faac.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_ffmpeg.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_flac.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_flake.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_fluidsynth.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_lame.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_libav.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_mac.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_mplayer.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_musepack.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_neroaac.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_opustools.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_shorten.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_speex.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_timidity.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_ttaenc.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_twolame.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_vorbistools.desktop
%{_kde4_datadir}/kde4/services/%{name}_codec_wavpack.desktop
%{_kde4_datadir}/kde4/services/%{name}_filter_normalize.desktop
%{_kde4_datadir}/kde4/services/%{name}_filter_sox.desktop
%{_kde4_datadir}/kde4/services/%{name}_replaygain_aacgain.desktop
%{_kde4_datadir}/kde4/services/%{name}_replaygain_mp3gain.desktop
%{_kde4_datadir}/kde4/services/%{name}_replaygain_metaflac.desktop
%{_kde4_datadir}/kde4/services/%{name}_replaygain_musepackgain.desktop
%{_kde4_datadir}/kde4/services/%{name}_replaygain_vorbisgain.desktop
%{_kde4_datadir}/kde4/services/%{name}_replaygain_wvgain.desktop
%{_kde4_datadir}/kde4/services/%{name}_ripper_cdparanoia.desktop
%{_kde4_datadir}/kde4/services/%{name}_ripper_icedax.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_codecplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_filterplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_replaygainplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_ripperplugin.desktop
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Feb 18 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.91-1m)
- update to 2.0.91

* Thu Dec 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.90-1m)
- update to 2.0.90

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Wed Jun  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sun May  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sun Mar 10 2013 NARITA Koichi <puldar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Feb 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.90-1m)
- update to 1.9.90 (2.0.0 rc1)

* Mon Jan 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.80-1m)
- update to 1.9.80 (2.0.0 beta1)

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Tue Jun 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.90-1m)
- update to 1.4.90 (1.5.0rc1)

* Sun Mar 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Wed Mar 14 2012 NARITA Koichi <pulasr@momonga-linux.org>
- (1.3.90-1m)
- update to 1.3.90 (1.4.0rc1)

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Tue Nov 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-2m)
- modify %%files to avoid conflicting

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.90-1m)
- update to 1.1.90 (1.2.0rc1)

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.90-1m)
- update to 1.0.90 (1.1.0rc1)

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-1m)
- update to 1.0.0rc3 (0.9.95)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.94-2m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.94-1m)
- update to 1.0.0rc2 (0.9.94)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.93-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.93-1m)
- update to 1.0.0rc1 (0.9.93)

* Sun Oct 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.92-2m)
- add BuildRequires

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.92-1m)
- update to 1.0.0beta3 (0.9.92)

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.91-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.91-3m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.91-2m)
- use desktop-file-install instead of sed

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.91-1m)
- update to 1.0.0beta2 (0.9.91)

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.90-4m)
- build fix with desktop-file-utils-0.16

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.90-3m)
- rebuild against qt-4.6.3-1m

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.90-2m)
- replace tar-ball
- remove merged libsoundkonvertercore-install.patch
- good-bye KDE3 and hello KDE4

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.11-3m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Mon May  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.90-1m)
- update to 1.0.0beta1
- remove Source1: old-soundkonverter-kde3 and remove install icon section
- update desktop.patch
- apply libsoundkonvertercore-install.patch to rosolve dependency

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.11-2m)
- use BuildRequires

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.2.1m)
- update to 1.0.0alpha2

* Wed Jan  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.1.2m)
- fix up icon install section

* Mon Dec  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.1.1m)
- update to version 1.0.0alpha1
- update desktop.patch
- clean up old patches
- set NoSource: 0 again
- good-bye KDE3

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.11-1m)
- version 0.3.11
- unset NoSource: 0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.10-1m)
- update to 0.3.10
- userscript.sh is installed to /usr/share/apps/soundkonverter

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.9-5m)
- fix build with new automake

* Fri Mar 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.9-4m)
- remove klineedit workaround for gcc44

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.9-3m)
- modify configure when gcc44
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.9-2m)
- rebuild against rpm-4.6

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.9-1m)
- update to 0.3.9

* Wed Jun 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-2m)
- rebuild against qt3

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.7-0.1.2m)
- rebuild against gcc43

* Fri Mar  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-0.1.1m)
- version 0.3.7-beta

* Fri Feb 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Fri Feb 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Tue Sep  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- version 0.3.6
- do not use %%NoSource macro

* Sat Aug  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.4-2m)
- replace Source0 to new tar-ball

* Mon Jul 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.4-1m)
- version 0.3.4

* Tue Jun 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-4m)
- change Requires from kdebase to kdelibs

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-3m)
- set ripper=cdda2wav to avoid crashing at initial start up

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-2m)
- good-bye cdrtools and welcome cdrkit

* Tue May 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-1m)
- version 0.3.3

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- version 0.3.2

* Sat Dec 16 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.95-1m)
- initial build for Momonga Linux 3
