%global momorel 7

Summary:        VTE based, super lightweight terminal emulator
Name:           evilvte
Version:        0.4.5
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          User Interface/X
URL:            http://www.calno.com/evilvte/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        http://www.calno.com/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        %{name}.desktop
Requires:       vte028
BuildRequires:  vte028-devel pkgconfig
BuildRequires:  desktop-file-utils
BuildRequires:  xorg-x11-server-devel glib2-devel gtk2-devel

%description
VTE based, super lightweight terminal emulator.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category TerminalEmulator \
  --add-category System \
  --add-category GTK \
  %{SOURCE1}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changelog LICENSE gpl-2.0.txt
%{_bindir}/evilvte
%{_bindir}/showvte
%{_datadir}/pixmaps/evilvte.*
%{_datadir}/applications/evilvte.desktop
%{_mandir}/man1/evilvte.1*
%{_mandir}/man1/showvte.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-7m)
- rebuild for glib 2.33.2

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-6m)
- fix BuildRequires and Requires; use vte028

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-3m)
- full rebuild for mo7 release

* Thu Dec  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-2m)
- fix up evilvte.desktop
- fix %%description
- use optflags
- enable parallel build
- add LICENSE and gpl-2.0.txt to %%doc
- add URL

* Thu Nov 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.5-1m)
- initial package for Momonga
