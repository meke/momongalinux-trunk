%global momorel 3
Summary: A network diagnostic tool
Name: mtr
Version: 0.82
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2+
URL: http://www.BitWizard.nl/mtr
Source:	ftp://ftp.bitwizard.nl/mtr/%{name}-%{version}.tar.gz
NoSource: 0
Source1: xmtr.consolehelper
Source2: xmtr.pam
Source3: net-x%{name}.desktop

Patch1: mtr-crash-in-xml-mode.patch
Patch2: mtr-xml-format-fixes.patch
Patch3: mtr081-rh703549.patch

BuildRequires: ncurses-devel gtk2-devel desktop-file-utils

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Mtr is a network diagnostic tool that combines ping and traceroute
into one program. Mtr provides two interfaces: an ncurses interface,
useful for using Mtr from a telnet session; and a GTK+ interface for X
(provided in the mtr-gtk package).

%package gtk
Summary: The GTK+ interface for mtr
Group: Applications/Internet
Requires: mtr = %{version}-%{release}
Requires: usermode >= 1.37

%description gtk
The mtr-gtk package provides the GTK+ interface for the mtr network
diagnostic tool.

%prep
%setup -q

%patch1 -p1
%patch2 -p1
%patch3 -p1 -b .rh703549

## parallel build does not work properly...
%build
%configure --with-gtk
make  && mv mtr xmtr && make distclean
%configure --without-gtk
make 

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_sbindir}
mkdir -p %{buildroot}/%{_datadir}/pixmaps
mkdir -p %{buildroot}/%{_mandir}/man8
install -m 0755 mtr %{buildroot}/%{_sbindir}/mtr
make DESTDIR=%{buildroot} install

install -m 0755 xmtr %{buildroot}/%{_bindir}/xmtr.bin
install -m 644 img/mtr_icon.xpm %{buildroot}/usr/share/pixmaps
mkdir -p %{buildroot}/%{_sysconfdir}/security/console.apps
install -m 644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/security/console.apps/xmtr
mkdir -p %{buildroot}/%{_sysconfdir}/pam.d
install -m 644 %{SOURCE2} %{buildroot}/etc/pam.d/xmtr
mkdir -p  %{buildroot}/%{_bindir}
ln -fs consolehelper %{buildroot}/%{_bindir}/xmtr

mkdir -p %{buildroot}/%{_datadir}/applications

desktop-file-install --vendor=					\
	--dir=%{buildroot}/%{_datadir}/applications		\
	%{SOURCE3}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING FORMATS NEWS README SECURITY
%attr(0755,root,root) %caps(cap_net_raw=pe) %{_sbindir}/mtr
%{_mandir}/man8/*

%files gtk
%defattr(-,root,root,-)
%{_datadir}/applications/net-x%{name}.desktop
%attr(0755,root,root) %{_bindir}/xmtr.bin
%{_bindir}/xmtr
%config(noreplace) %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/*
%{_datadir}/pixmaps/mtr_icon.xpm

%changelog
* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-3m)
- remove fedora from vendor (desktop-file-install)

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-2m)
- do not use %%make macro

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.82-1m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.75-6m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-3m)
- %%define __libtoolize :

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-2m)
- specify prefix at make install

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-1m)
- update to 0.75
-- import Patch0 from Fedora 11 (2:0.75-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.72-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.72-4m)
- rebuild against gcc43

* Mon Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.72-3m)
- no use libtermcap

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.72-2m)
- update xmtr.pam for new pam
- revise xmtr.consolehelper
- goo-bye gtk+1, and hello gtk2
- import mtr-0.69-CVE-2002-0497.patch from Fedora
- import mtr-0.69-format.patch from Fedora
- import mtr-0.71-underflow.patch from Fedora

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.71-1m)
- update to 0.71
- use %%NoSource

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.67-4m)
- revised installdir

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.67-3m)
- rebuild against ncurses-5.4-1m, libtermcap-2.0.8-38m.

* Thu Feb 24 2005 Toru Hoshina <t@momonga-linux.org>
- (0.67-2m)
- auto* fix.

* Thu Dec 16 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.67-1m)
- up to 0.67

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.52-3m)
- import from FC1.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Nov 29 2002 Phil Knirsch <pknirsch@redhat.com> 0.52-1
- Update to latest upstream version (0.52).

* Tue Nov 12 2002 Nalin Dahyabhai <nalin@redhat.com> 0.49-9
- Remove absolute paths from the PAM configuration, ensuring that the modules
  for the right arch get used on multilib systems.
- Remove Icon:.

* Tue Sep 24 2002 Bernhard Rosenkraenzer <bero@redhat.com> 0.49-7a
- Fix build on s390x

* Mon Aug 19 2002 Phil Knirsch <pknirsch@redhat.com> 0.49-7
- Fixed consolehelper support.

* Wed Aug 07 2002 Phil Knirsch <pknirsch@redhat.com> 0.49-6
- Desktop file fixes (#69550).

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 0.49-5
- automated rebuild

* Tue Jun 18 2002 Phil Knirsch <pknirsch@redhat.com> 0.49-4
- Added consolehelper support to xmtr.

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed May 22 2002 Phil Knirsch <pknirsch@redhat.com> 0.49-2
- Fixed autoFOO problems.

* Fri Mar 08 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- 0.49 update

* Thu Mar 07 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- 0.48 update

* Mon Jun 25 2001 Preston Brown <pbrown@redhat.com>
- 0.44 bugfix release
- fix display of icon in .desktop entry

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Mon Feb 12 2001 Preston Brown <pbrown@redhat.com>
- don't advertise gtk support in non-gtk binary (#27172)

* Fri Oct 20 2000 Bill Nottingham <notting@redhat.com>
- fix autoconf check for resolver functions

* Fri Jul 21 2000 Bill Nottingham <notting@redhat.com>
- fix group

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul  6 2000 Bill Nottingham <notting@redhat.com>
- fix setuid bit
- remove symlink
- force build of non-gtk version

* Mon Jun 19 2000 Preston Brown <pbrown@redhat.com>
- disable SUID bits
- desktop entry

* Mon Jun 19 2000 Than Ngo <than@redhat.de>
- FHS fixes

* Fri May 26 2000 Preston Brown <pbrown@redhat.com>
- adopted for Winston

* Thu Aug 19 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.41-1]
- Added afr's patch to allow disabeling of gtk without Robn's hack. 
- Made report mode report the newly added extra resolution. 

* Wed Aug 18 1999 Ryan Weaver <ryanw@infohwy.com>
- renamed mtr-gtk to xmtr
- added symlink from /usr/bin/mtr to /usr/sbin/mtr

  [mtr-0.40-1]
- Fixed some problems with HPUX and SunOS.
- Included Olav Kvittem's patch to do packetsize option.
- Made the timekeeping in micro seconds.

* Wed Jun 10 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.39-1]
- Updated to version 0.39.

* Wed Jun  9 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.38-1]
- Updated to version 0.38.

* Thu Apr 15 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.37-2]
- Changed RPM headers to conform to Red Hat Contrib|Net specs.

* Mon Apr 12 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.37-1]
- v0.37
- Added Bill Bogstad's "show the local host & time" patch. 
- Added R. Sparks' show-last-ping patch, submitted by Philip Kizer.

- v0.36
- Added Craigs change-the-interval-on-the-fly patch.
- Added Moritz Barsnick's "do something sensible if host not found" 
  patch.
- Some cleanup of both Craigs and Moritz' patches.

* Wed Apr  7 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.35-1]
- v0.35
- Added Craig Milo Rogers pause/resume for GTK patch.
- Added Craig Milo Rogers cleanup of "reset". (restart at the beginning)
- Net_open used to send a first packet. After that the display-driver
  got a chance to distort the timing by taking its time to initialize.

* Mon Apr  5 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.34-1]
- v0.34
- Added Matt's nifty "use the icmp unreachables to do the timing" patch.
- Added Steve Kann's pause/resume patch.

* Wed Mar 10 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.33-1]
- v0.33
- Fixed the Linux glibc resolver problems.
- Fixed the off-by-one problem with -c option.

* Mon Mar  8 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.32-1]
- v0.32
- Fixed the FreeBSD bug detection stuff.

* Fri Mar  5 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.31-1]
- v0.31
- Fixed a few documentation issues. -- Matt
-  Changed the autoconf stuff to find the resolver library on
     Solaris. -- REW
-  Cleaned up the autoconf.in file a bit. -- Matt.

* Thu Mar  4 1999 Ryan Weaver <ryanw@infohwy.com>
  [mtr-0.30-1]
- Build gtk version against gtk+-1.2.0
- v0.30
- Fixed a typo in the changelog (NEWS) entry for 0.27. :-)
- added use of "MTR_OPTIONS" environment variable for defaults.
