# Generated from em-http-request-1.0.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname em-http-request

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: EventMachine based, async HTTP Request client
Name: rubygem-%{gemname}
Version: 1.0.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/igrigorik/em-http-request
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(eventmachine) >= 1.0.0.beta.4
Requires: rubygem(addressable) >= 2.2.3
Requires: rubygem(http_parser.rb) >= 0.5.3
Requires: rubygem(em-socksify) 
Requires: rubygem(cookiejar) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
EventMachine based, async HTTP Request client


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-1m)
- update 1.0.2

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update 1.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.14-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.14-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.14-1m)
- update 0.2.14

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.10-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.10-1m)
- Initial package for Momonga Linux
