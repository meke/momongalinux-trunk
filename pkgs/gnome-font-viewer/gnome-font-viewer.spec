%global momorel 1
Name:           gnome-font-viewer
Version:        3.6.0
%global         release_version %(echo %{version} | awk -F. '{print $1"."$2}')
Release: %{momorel}m%{?dist}
Summary:        Utility for previewing fonts for GNOME

Group:          Applications/System
License:        GPLv2+
#No URL for the package specifically, as of now
URL:            http://www.gnome.org/gnome-3/
Source:  	http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gnome-desktop3-devel
BuildRequires:  gtk3-devel
BuildRequires:  intltool
BuildRequires:  desktop-file-utils

Obsoletes: gnome-utils < 3.3
Obsoletes: gnome-utils-devel < 3.3
Obsoletes: gnome-utils-libs < 3.3

%description
Use gnome-font-viewer, the Font Viewer, to preview fonts and display
information about a specified font. You can use the Font Viewer to display the
name, style, type, size, version and copyright of the font.

%prep
%setup -q


%build
%configure --disable-silent-rules
%make 


%install
make install DESTDIR=%{buildroot}

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name} --with-gnome

%post
update-desktop-database &> /dev/null || :

%postun
update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%doc COPYING NEWS

%{_bindir}/%{name}
%{_bindir}/gnome-thumbnail-font
%{_datadir}/applications/%{name}.desktop
%{_datadir}/thumbnailers/%{name}.thumbnailer


%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 02 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- import from fedora

