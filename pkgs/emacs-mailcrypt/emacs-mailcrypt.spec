%global momorel 36
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global realname mailcrypt

Summary: An PGP Interface on Emacs
Name: emacs-%{realname}
Version: 3.5.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://dl.sourceforge.net/sourceforge/%{realname}/%{realname}-%{version}.tar.gz
Nosource: 0
URL: http://mailcrypt.sourceforge.net/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: tetex
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: mailcrypt-emacs

Obsoletes: elisp-mailcrypt
Provides: elisp-mailcrypt

%description
Mailcrypt is an Emacs Lisp package which provides a simple interface
to public key cryptography with PGP [and now GnuPG!]. Mailcrypt makes
strong cryptography a fully integrated part of your normal mail and
news handling environment, and is an important part of a balanced
breakfast.

%prep
%setup -q -n %{realname}-%{version}

%build
%configure --prefix=%{_prefix}
make EMACS=emacs \
     EMACSFLAGS="-q -no-site-file" \
     prefix=%{_prefix} \
     lispdir=%{e_sitedir}/%{realname} \
     infodir=%{_infodir}

#make html
make timer.el

%install
rm -rf %{buildroot}
make EMACS=emacs \
     EMACSFLAGS="-q -no-site-file" \
     prefix=%{buildroot}%{_prefix} \
     lispdir=%{buildroot}%{e_sitedir}/%{realname} \
     infodir=%{buildroot}%{_infodir} install

# remove
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info -- %{_infodir}/mailcrypt.info.* %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info -- %{_infodir}/mailcrypt.info.* %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc ANNOUNCE COPYING INSTALL NEWS ONEWS README README.gpg
%doc ChangeLog ChangeLog.1 LCD-entry
#%%doc mailcrypt/*.html
%{e_sitedir}/%{realname}
%{_infodir}/mailcrypt.info*.*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-36m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-35m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.5.8-34m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-33m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.8-32m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-31m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-30m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-29m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-28m)
- merge mailcrypt-emacs to elisp-mailcrypt

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-27m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.8-26m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.8-25m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-24m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-23m)
- rebuild against emacs-23.0.94
- drop html manual since we can not generate htmls by texi2html-1.82

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-22m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-21m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-20m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-19m)
- rebuild against emacs-22.3

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-18m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.8-17m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-16m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-15m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-14m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-13m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-12m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-11m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-10m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-9m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-8m)
- rebuild against emacs-22.0.90

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.8.5-7m)
- telia -> jaist

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-6m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.8.5-5m)
- rebuild against emacs-21.3.50

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (3.8.5-4m)
- revised spec for enabling rpm 4.2.

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.5-3m)
- rebuild against emacs-21.3

* Mon Nov 11 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (3.8.5-2m)
- add BuildPrereq: tetex

* Sat Nov  2 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (3.8.5-1m)
- create spec for FSF Emacs.
