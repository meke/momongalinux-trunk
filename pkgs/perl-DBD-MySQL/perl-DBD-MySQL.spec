%global         momorel 2

Name:           perl-DBD-MySQL
Version:        4.027
Release:        %{momorel}m%{?dist}
Summary:        MySQL driver for the Perl5 Database Interface (DBI)
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBD-mysql/
Source0:        http://www.cpan.org/authors/id/C/CA/CAPTTOFU/DBD-mysql-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-DBI >= 1.08
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  mariadb >= 5.5.30
BuildRequires:  mariadb-devel >= 5.5.30
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  zlib-devel
Requires:       perl-Data-Dumper
Requires:       perl-DBI >= 1.08
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
# for automatic upgrade
Provides:       perl-DBD-mysql = %{version}-%{release}
Obsoletes:      perl-DBD-mysql

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DBD::mysql is the Perl5 Database Interface driver for the MySQL database.
In other words: DBD::mysql is an interface between the Perl programming
language and the MySQL programming API that comes with the MySQL relational
database management system. Most functions provided by this programming API
are supported. Some rarely used functions are missing, mainly because noone
ever requested them. :-)

%prep
%setup -q -n DBD-mysql-%{version}

%build
LIBS=`mysql_config --libs`
%{__perl} Makefile.PL -ssl -libs="$LIBS -lmysqld" INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
NEED_STOP=0
if [ ! -e /var/run/mysqld/mysqld.pid ] ; then
    sudo systemctl start mysqld.service
    NEED_STOP=1
fi
make test
if [ $NEED_STOP -eq 1 ] ; then
    sudo systemctl stop mysqld.service
fi
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.pod ChangeLog TODO
%{_mandir}/man?/*
%{perl_vendorarch}/DBD/mysql
%{perl_vendorarch}/DBD/mysql.pm
%{perl_vendorarch}/DBD/README.pod
%{perl_vendorarch}/auto/DBD/mysql
%{perl_vendorarch}/Bundle/DBD/mysql.pm

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.027-2m)
- rebuild against perl-5.20.0

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.027-1m)
- update to 4.027

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.026-1m)
- update to 4.026
- rebuild against perl-5.18.2

* Wed Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.025-1m)
- update to 4.025

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.024-1m)
- update to 4.024

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.023-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.023-3m)
- rebuild against perl-5.18.0

* Thu Apr 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.023-2m)
- rebuild against mariadb-5.5.30

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.023-1m)
- update to 4.023

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.022-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.022-2m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.022-1m)
- update to 4.022

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.021-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.021-1m)
- update to 4.021
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.020-2m)
- rebuild against perl-5.14.2

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.020-1m)
- update to 4.020

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.019-2m)
- rebuild against perl-5.14.1

* Mon May  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.019-1m)
- update to 4.019

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.018-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.018-4m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.018-3m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.018-2m)
- rebuild for new GCC 4.5

* Wed Oct 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.018-1m)
- update to 4.018

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.017-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.017-1m)
- update to 4.017

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.016-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.016-1m)
- update to 4.016

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.015-1m)
- update to 4.015

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.014-2m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.014-1m)
- update to 4.014

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.013-5m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.013-4m)
- rebuild against openssl-1.0.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.013-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.013-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.013-1m)
- update to 4.013

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.012-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.012-3m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.012-2m)
- rename perl-DBD-mysql to perl-DBD-MySQL

* Fri Jun 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.012-1m)
- update to 4.012

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.011-1m)
- update to 4.011

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.010-4m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.010-3m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.010-2m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.010-1m)
- update to 4.010

* Thu Oct 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.009-1m)
- update to 4.009

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.008-1m)
- update to 4.008

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.007-2m)
- rebuild against openssl-0.9.8h-1m

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.007-1m)
- update to 4.007

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.006-2m)
- rebuild against gcc43

* Thu Dec 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.006-1m)
- update to 4.006

* Sat Jun  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.005-1m)
- update to 4.005

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.004-3m)
- use vendor

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.004-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Mar 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.004-1m)
- update t0 4.004

* Sun Mar  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.003-1m)
- update to 4.003

* Sat Mar  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.002-1m)
- update to 4.002

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.001-1m)
- update to 4.001

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00-1m)
- update to 4.00

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0008-1m)
- update to 3.0008

* Tue Oct 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0007-2m)
- add Provides and Obsoletes for automatic upgrade

* Mon Oct 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0007-1m)
- rename perl-DBD-MySQL to perl-DBD-mysql

* Sun Sep 17 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.0007-1m)
- update to 3.0007

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.0006-1m)
- update to 3.0006

* Fri Jun  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.0004-1m)
- update to 3.0004

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.0003-2m)
- rebuild against mysql 5.0.22-1m

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.0003-1m)
- update to 3.0003

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0002-3m)
- built against perl-5.8.8

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.0002-2m)
- rebuild against for MySQL-4.1.14

* Sun Jul 31 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.0002-1m)
- update to 3.0002
  Release of prepared statement code
- revised URL

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.9008-1m)
- version up to 2.9008
- built against perl-5.8.7

* Mon May 16 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.9006-1m)
- update to 2.9006
- add TODO to %%doc

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.9003-5m)
- rebuild against perl-5.8.5
- rebuild against perl-DBI 1.38-4m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.9003-4m)
- remove Epoch from BuildRequires

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9003-3m)
- rebuild against perl-5.8.1

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.9003-2m)
- rebuild against perl-5.8.1

* Fri Oct 31 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.9003-1m)
- update to 2.9003
- fix License: preamble

* Tue Sep  9 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.9002-1m)
- update to 2.9002
- use %%{momorel}, %%NoSource
- remove /usr/bin/dbimon
- fix %%files

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.2219-7m)
- rebuild against for MySQL-4.0.14-1m

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2219-6m)
- rebuild against perl-5.8.0

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2219-5m)
- fix %files

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2219-4m)
- fix man install path

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2219-3m)
- remove BuildRequires: gcc2.95.3

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (1.2219-2k)
- version 1.2219

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.2216-6k)
- rebuild against for perl-5.6.1

* Mon Sep 17 2001 Toru Hoshina <toru@df-usa.com>
- (1.2216-4k)
- no more fixpack...

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (1.2216-2k)
- merge from rawhide. based on 1.2216-4.
