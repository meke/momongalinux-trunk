%global momorel 17
%global qtver 3.3.7
%global qtdir %{_libdir}/qt-%{qtver}

Summary: Exscalibar Framework
Name: exscalibar
Version: 1.0.4
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.sf.net/projects/exscalibar
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/exscalibar/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-%{version}-set-flags.patch
Patch1: exscalibar-1.0.4-x86_64.patch
Patch10: exscalibar-1.0.4-no_virtual.patch
Patch11: exscalibar-1.0.4-gcc43.patch
Patch12: exscalibar-1.0.4-gcc44.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt3-devel = %{qtver}
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libsndfile-devel, libmad, libvorbis-devel
BuildRequires: alsa-lib-devel, jack-devel
BuildRequires: pkgconfig

%description
Exscalibar is an EXtendable, SCalable Architecture for Live, Interactive or Batch-orientated Audio-signal Refinement. 

%package devel
Summary: Headers for developing programs that will use Exscalibar
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Headers for developing programs that will use Exscalibar.

%prep
%setup -q

%patch0 -p1 -b .set-flags
%ifarch x86_64 ppc64 ia64 alpha sparc64
%patch1 -p1 -b .x86_64
%endif

%patch10 -p1 -b .no_virtual
%patch11 -p1 -b .gcc43~
%patch12 -p1 -b .gcc44~

%build
export QTDIR=%{qtdir}

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure

# make -j2 doesn't work (1.0.4-1m)
# make %{?_smp_mflags}
make

%install
export QTDIR=%{qtdir}
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%if %{_lib} == "lib64"
perl -p -i -e 's|/usr/lib/pkgconfig/|/usr/lib64/pkgconfig/|g' system/Makefile
%endif
make install INSTALL_ROOT=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc CHANGELOG COPYING INSTALL README
%{qtdir}/bin/exinfo
%{qtdir}/bin/nite
%{qtdir}/bin/nodeserver
%{qtdir}/lib/lib*.so.*
%{qtdir}/plugins/geddei

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/exscalibar.pc
%{qtdir}/include/geddei
%{qtdir}/include/geddeiprocessors
%{qtdir}/include/qtextra
%{qtdir}/include/rgeddei
%{qtdir}/include/Geddei
%{qtdir}/include/SignalTypes
%{qtdir}/include/rGeddei
%{qtdir}/lib/lib*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-15m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-14m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-12m)
- rebuild against libjpeg-7

* Sun Jan 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-11m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-10m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-9m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-8m)
- rebuild against gcc43

* Sun Feb 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-7m)
- add gcc43 patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-6m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-5m)
- rebuild against libvorbis-1.2.0-1m

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-4m)
- fix gcc-4.2. add exscalibar-1.0.4-no_virtual.patch

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-3m)
- rebuild against qt-3.3.7

* Thu Feb 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.4-2m)
- update spec for x86_64

* Tue Feb 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-1m)
- initial package for amarok-1.4
