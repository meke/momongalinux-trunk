%global momorel 2

Summary: bmemcached is a C and C++ client library 
Name: libmemcached 
Version: 0.48
Release: %{momorel}m%{?dist}
URL: http://libmemcached.org/
Source0: http://launchpad.net/libmemcached/1.0/%{version}/+download/libmemcached-%{version}.tar.gz
NoSource: 0
License: BSD
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libevent-devel >= 2.0.10
BuildRequires: memcached

%description
libmemcached is a C and C++ client library to the memcached server
(http://danga.com/memcached).  It has been designed to be light on
memory usage, thread safe, and provide full access to server side
methods.

%package devel
Summary: Files for development using %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the headers and pkg-config file for
development of programs using %{name}.

%prep
%setup -q

%build
export PATH=%{_sbindir}:$PATH
%configure --program-transform-name=''
%make

%install
make DESTDIR=%{buildroot} install
%{__make} install  DESTDIR="%{buildroot}" AM_INSTALL_PROGRAM_FLAGS="" transform-program-name=""

rm -f %{buildroot}%{_libdir}/*.la

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README THANKS TODO ChangeLog
%{_bindir}/memcapable
%{_bindir}/memcat
%{_bindir}/memcp
%{_bindir}/memdump
%{_bindir}/memerror
%{_bindir}/memflush
%{_bindir}/memrm
%{_bindir}/memslap
%{_bindir}/memstat
%{_libdir}/libhashkit.so.*
%{_libdir}/libmemcached.so.*
%{_libdir}/libmemcachedprotocol.so.*
%{_libdir}/libmemcachedutil.so.*
%{_mandir}/man1/memcapable.1*
%{_mandir}/man1/memcat.1*
%{_mandir}/man1/memcp.1*
%{_mandir}/man1/memdump.1*
%{_mandir}/man1/memerror.1*
%{_mandir}/man1/memflush.1*
%{_mandir}/man1/memrm.1*
%{_mandir}/man1/memslap.1*
%{_mandir}/man1/memstat.1*

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/libmemcached.pc
%{_libdir}/libhashkit.so
%{_libdir}/libmemcached.so
%{_libdir}/libmemcachedprotocol.so
%{_libdir}/libmemcachedutil.so
%{_includedir}/libhashkit
%{_includedir}/libmemcached
%{_mandir}/man3/hashkit_*.3*
%{_mandir}/man3/libmemcached*.3*
%{_mandir}/man3/memcached_*.3*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48
- rebuild against libevent-2.0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (0.26-5m)
- add %%dir

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-3m)
- do not specify man file compression format

* Thu Feb  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.26-2m)
- set PATH to %%{_sbindir} (to enable build)
- BuildRequires: memcached

* Thu Feb 5 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.26-1m)
- inital build
