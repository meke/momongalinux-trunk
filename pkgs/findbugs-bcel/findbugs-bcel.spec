%global momorel 5

# This is a version of BCEL that has been modified by the findbugs team.  They
# added some new functionality and also did some performance optimizations of
# the base code.  I am not producing a new manual, since we already have a
# bcel-manual package and the findbugs team did not patch the manual.  However,
# the javadoc package is necessary to show the changes in the API created by
# the findbug team's work.

%define findbugsver 1.3.8
%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name:           findbugs-bcel
Version:        5.2
Release:        %{momorel}m%{?dist}
Summary:        Byte Code Engineering Library with findbugs extensions

Group:          Development/Libraries
License:        "ASL 2.0"
URL:            http://jakarta.apache.org/bcel/
Source0:        http://www.apache.org/dist/jakarta/bcel/source/bcel-%{version}-src.tar.gz
NoSource:       0
# This patch is available in the findbugs release, in src/patches/bcel.diff.
Source1:        bcel.diff
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  java-devel, jpackage-utils
Requires:       java, jpackage-utils

%if %{with_gcj}
BuildRequires:  java-gcj-compat-devel >= 1.0.31
Requires(post): java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch:      noarch
%endif

%description
This is a version of Apache's Byte Code Engineering Library (BCEL) that has
been modified by the findbugs developers.  The modifications add some new
functionality, and also introduce a number of performance optimizations to
address findbugs performance problems.  Some of the performance optimizations
induce API changes, so this version of BCEL is not compatible with the vanilla
upstream version.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
%{summary}.

%prep
%setup -q -n bcel-%{version}
%{__patch} -p7 -s < %{SOURCE1}

%build
# The ant and maven builds both try to download unneeded jars
mkdir classes
find src/java -type f -name '*.java' | \
xargs javac -g -d classes -source 1.5 -encoding ISO8859-1
cd classes
jar cf findbugs-bcel-%{findbugsver}.jar org
cd ..

mkdir javadoc
find src/java -type f -name '*.java' | xargs javadoc -sourcepath src/java \
  -classpath classes -source 1.5 -encoding ISO8859-1 -d javadoc 

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p classes/findbugs-bcel-%{findbugsver}.jar $RPM_BUILD_ROOT%{_javadir}
ln -s findbugs-bcel-%{findbugsver}.jar $RPM_BUILD_ROOT%{_javadir}/findbugs-bcel.jar

mkdir -p $RPM_BUILD_ROOT%{_javadocdir}
cp -a javadoc $RPM_BUILD_ROOT%{_javadocdir}/findbugs-bcel-%{findbugsver}
ln -s findbugs-bcel-%{findbugsver} $RPM_BUILD_ROOT%{_javadocdir}/findbugs-bcel

%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]; then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]; then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(-,root,root,-)
%doc LICENSE.txt NOTICE.txt README.txt
%{_javadir}/findbugs-bcel*
%if %{with_gcj}
%{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/findbugs-bcel*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-1m)
- import from Fedora 11 for findbugs

* Tue Mar 17 2009 Jerry James <loganjerry@gmail.com> - 5.2-1.3.8
- Update to the findbugs 1.3.8 version of the BCEL patch
- The BCEL patch now applies cleanly, so drop workaround code

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.2-1.3.7.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 13 2009 Jerry James <loganjerry@gmail.com> - 5.2-1.3.7.1
- Add '-source 1.5' to the javac and javadoc invocations

* Fri Jan  2 2009 Jerry James <loganjerry@gmail.com> - 5.2-1.3.7
- Update to the findbugs 1.3.7 version of the BCEL patch

* Mon Dec  8 2008 Jerry James <loganjerry@gmail.com> - 5.2-1.3.6
- Update to the findbugs 1.3.6 version of the BCEL patch

* Thu Sep 18 2008 Jerry James <loganjerry@gmail.com> - 5.2-1.3.5
- Initial RPM
