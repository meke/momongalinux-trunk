%global momorel 7

Summary: The Point-to-Point Protocol daemon
Name: ppp
Version: 2.4.5
Release: %{momorel}m%{?dist}
License: BSD and LGPLv2+ and GPLv2+ and Public Domain
Group: System Environment/Daemons
URL: http://www.samba.org/ppp
Source0: ftp://ftp.samba.org/pub/ppp/ppp-%{version}.tar.gz
NoSource: 0
Source1: ppp-2.3.5-pamd.conf
Source2: ppp.logrotate
Source3: ppp-tmpfs.conf
Patch0: ppp-2.4.3-make.patch
Patch1: ppp-2.3.6-sample.patch
Patch2: ppp-2.4.2-libutil.patch
Patch3: ppp-2.4.1-varargs.patch
Patch4: ppp-2.4.4-lib64.patch
Patch7: ppp-2.4.2-pie.patch
Patch8: ppp-2.4.3-fix.patch
Patch9: ppp-2.4.3-fix64.patch
Patch11: ppp-2.4.2-change_resolv_conf.patch
Patch13: ppp-2.4.4-no_strip.patch
Patch17: ppp-2.4.2-pppoatm-make.patch
Patch19: ppp-2.4.3-local.patch
Patch20: ppp-2.4.3-ipv6-accept-remote.patch
Patch22: ppp-2.4.4-cbcp.patch
Patch23: ppp-2.4.2-dontwriteetc.patch
Patch24: ppp-2.4.4-fd_leak.patch
Patch25: ppp-2.4.5-var_run_ppp.patch
Patch26: ppp-2.4.5-manpg.patch
Patch27: ppp-2.4.5-eaptls-mppe-0.99.patch
Patch28: ppp-2.4.5-ppp_resolv.patch
Patch29: ppp-2.4.5-man.patch
Patch30: ppp-2.4.5-eth.patch
Patch31: ppp-2.4.5-lock.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pam-devel, libpcap-devel >= 1.1.1
Requires: glibc >= 2.0.6, pam, logrotate, libpcap >= 0.8.3-6, systemd-units

%description
The ppp package contains the PPP (Point-to-Point Protocol) daemon and
documentation for PPP support. The PPP protocol provides a method for
transmitting datagrams over serial point-to-point links. PPP is
usually used to dial in to an ISP (Internet Service Provider) or other
organization over a modem and phone line.

%package devel
Summary: Headers for ppp plugin development
Group: Development/Libraries

%description devel
This package contains the header files for building plugins for ppp.

%prep
%setup  -q
%patch0 -p1 -b .make
%patch1 -p1 -b .sample
# patch 2 depends on the -lutil in patch 0
%patch2 -p1 -b .libutil
%patch3 -p1 -b .varargs
# patch 4 depends on the -lutil in patch 0
%patch4 -p1 -b .lib64
%patch7 -p1 -b .pie
%patch8 -p1 -b .fix
%patch9 -p1 -b .fix64
%patch11 -p1 -b .change_resolv_conf
%patch13 -p1 -b .no_strip
%patch17 -p1 -b .atm-make
%patch19 -p1 -b .local
%patch20 -p1 -b .ipv6cp
%patch22 -p1 -b .cbcp
%patch23 -p1 -b .dontwriteetc
%patch24 -p1 -b .fd_leak
%patch25 -p1 -b .var_run_ppp
%patch26 -p1 -b .manpg
%patch27 -p1 -b .eaptls
%patch28 -p1 -b .ppp_resolv
%patch29 -p1 -b .man
# fixes bz#682381 - hardcodes eth0
%patch30 -p1 -b .eth
# fixes bz#708260 - SELinux is preventing access on the file LCK..ttyUSB3
%patch31 -p1 -b .lock

rm -f scripts/*.local
rm -f scripts/*.change_resolv_conf
rm -f scripts/*.usepeerdns-var_run_ppp_resolv
rm -f scripts/*.ppp_resolv
find . -type f -name "*.sample" | xargs rm -f 

rm -f include/linux/if_pppol2tp.h

%build
#find . -name 'Makefile*' -print0 | xargs -0 perl -pi.no_strip -e "s: -s : :g"
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -fPIC -Wall"
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
export INSTROOT=%{buildroot}
make install install-etcppp

chmod -R a+rX scripts
find scripts -type f | xargs chmod a-x
chmod 0755 %{buildroot}/%{_libdir}/pppd/%{version}/*.so
mkdir -p %{buildroot}/etc/pam.d
install -m 644 %{SOURCE1} %{buildroot}/etc/pam.d/ppp

# Provide pointers for people who expect stuff in old places
mkdir -p %{buildroot}/var/log/ppp
mkdir -p %{buildroot}/var/run/ppp

install -d -m 755 %{buildroot}%{_sysconfdir}/tmpfiles.d
install -p -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/tmpfiles.d/ppp.conf

# Logrotate script
mkdir -p %{buildroot}/etc/logrotate.d
install -m 644 %{SOURCE2} %{buildroot}/etc/logrotate.d/ppp

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_sbindir}/chat
%{_sbindir}/pppd
%{_sbindir}/pppdump
%{_sbindir}/pppoe-discovery
%{_sbindir}/pppstats
%{_mandir}/man8/chat.8*
%{_mandir}/man8/pppd.8*
%{_mandir}/man8/pppdump.8*
%{_mandir}/man8/pppd-radattr.8*
%{_mandir}/man8/pppd-radius.8*
%{_mandir}/man8/pppstats.8*
%{_mandir}/man8/pppoe-discovery.8*
%{_libdir}/pppd
%dir /var/run/ppp
%attr(700, root, root) %dir /var/log/ppp
%config %{_sysconfdir}/tmpfiles.d/ppp.conf
%config(noreplace) %{_sysconfdir}/ppp/eaptls-client
%config(noreplace) %{_sysconfdir}/ppp/eaptls-server
%config(noreplace) /etc/ppp/chap-secrets
%config(noreplace) /etc/ppp/options
%config(noreplace) /etc/ppp/pap-secrets
%config(noreplace) /etc/pam.d/ppp
%config(noreplace) /etc/logrotate.d/ppp
%doc FAQ README README.cbcp README.linux README.MPPE README.MSCHAP80 README.MSCHAP81 README.pwfd README.pppoe scripts sample

%files devel
%defattr(-,root,root)
%{_includedir}/pppd
%doc PLUGINS

%changelog
* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.5-8m)
- add tmpfiles.d conf

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.5-6m)
- rebuild for new GCC 4.5

* Wed Sep  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.5-5m)
- [BUILD FIX] fix build with new kernel

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.5-4m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.5-3m)
- release %%dir /etc/ppp, it's already provided by initscripts

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.5-2m)
- import patch ppp-2.4.5-ppp_resolv.patch

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.5-1m)
- version up 2.4.5
- import patch and sync Fedora

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-11m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-9m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-8m)
- update Patch3,7,17,19 for fuzz=0

* Tue May  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.4-7m)
- release %%{_sysconfdir}/ppp
- it's already provided by initscripts
- however, we are going to switch starting up system to upstart
- %%{_sysconfdir}/ppp should be checked again

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.4-6m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.4-5m)
- rebuild against gcc43

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.4-4m)
- update ppp-2.3.5-pamd.conf for new pam

* Thu Jun 07 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.4-3m)
- rebuild against libpcap-0.9.5 (restrict by version)

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-2m)
- rebuild against libpcap-0.9.5

* Mon Nov 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4 (sync with FC-devel ppp-2.4.4-1.src.rpm)

* Mon Jul 11 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.3-2m)
- [SECURITY] CVE-2006-2194

* Tue Jun 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3 (sync FC-devel)

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2-6m)
- delete duplicate dir

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.2-5m)
- revised docdir permission
- revised specfile

* Thu Feb 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.4.2-4m)
- remove obsolete %%post code.

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.4.2-3m)
- enable x86_64.

* Sun Jan  8 2005 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- force using autoheader-2.13 to build radiusclient.

* Sun Jul 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.2-1m)
- Use source tar ball
- add Patch111 ppp-2.4.2-nobpf-3.patch 
 
* Sun May 16 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.4.2-0.20030715.5m)
- update mppe-mppc patch
  See http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=36

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-0.20030715.4m)
- revised spec for rpm 4.2.

* Tue Nov  4 2003 zunda <zunda at freeshell.org>
- (2.4.2-0.20030715.3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Sep 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.2-0.20030715.2m)
- install plugin files to /etc/ppp/plugins

* Mon Sep 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.2-0.20030715.1m)

* Fri Apr 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-21m)
- add Requires: zlib >= 1.1.4-5m

* Wed Apr  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.4.1-20m)
- for installer

* Sat Nov 16 2002 Kenta MURATA <muraken@momonga-linux.org>
- (2.4.1-19m)
- configuration files were noreplace.

* Mon Nov 04 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.1-18m)
- added radiusclient files to %files section.

* Wed Oct 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.1-17m)
- cvs snapshot

* Mon Apr 29 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.4.1-16k)
- remove 'locate' dependency

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.1-14k)
- make pppoe patch and use original tar ball.

* Thu Apr 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.1-12k)
- kernel-mode pppoe

* Tue Mar 26 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4.1-10k)
- added PPTP support (merged from http://mirror.binarix.com/ppp-mppe/)
- add mppe encrypion patch
- add patch for MSCHAPv2
- add alias ppp ppp_generic to /etc/modules.conf
- now, we can connect this from Windows clients with MSCHAPv2 and 128bit
  encryption. (needs pptpd package.)

* Tue Mar 12 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.4.1-8k)
- fixed zlib double free bug

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.4.1-6k)
- updated Source0 URL.

* Mon Aug  6 2001 Toru Hoshina <toru@df-usa.com>
- (2.4.1-4k)
- fixed system-auth issue.

* Sat Apr 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.4.1-2k)
- update to 2.4.1 for kernel-2.4.x

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.3.11-4).

* Mon Mar 06 2000 Nalin Dahyabhai <nalin@redhat.com>
- reaper bugs verified as fixed
- check pam_open_session result code (bug #9966)

* Mon Feb 07 2000 Nalin Dahyabhai <nalin@redhat.com>
- take a shot at the wrong reaper bugs (#8153, #5290)

* Thu Feb 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- free ride through the build system (release 2)

* Tue Jan 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- Update to 2.3.11

* Sat Nov 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- Better fix for both problems

* Fri Nov 05 1999 Michael K. Johnson <johnsonm@redhat.com>
- fix for double-dial problem
- fix for requiring a controlling terminal problem

* Sun Sep 19 1999 Preston Brown <pbrown@redhat.com>
- 2.3.10 bugfix release

* Fri Aug 13 1999 Michael K. Johnson <johnsonm@redhat.com>
- New version 2.3.9 required for kernel 2.3.13 and will be required
  for new initscripts.  auth patch removed; 2.3.9 does the same thing
  more readably than the previous patch.

* Thu Jun 24 1999 Cristian Gafton <gafton@redhat.com>
- add pppdump

* Fri Apr 09 1999 Cristian Gafton <gafton@redhat.com>
- force pppd use the glibc's logwtmp instead of implementing its own

* Wed Apr 01 1999 Preston Brown <pbrown@redhat.com>
- version 2.3.7 bugfix release

* Tue Mar 23 1999 Cristian Gafton <gafton@redhat.com>
- version 2.3.6

* Mon Mar 22 1999 Michael Johnson <johnsonm@redhat.com>
- auth patch

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Thu Jan 07 1999 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Fri Jun  5 1998 Jeff Johnson <jbj@redhat.com>
- updated to 2.3.5.

* Tue May 19 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Fri May  8 1998 Jakub Jelinek <jj@ultra.linux.cz>
- make it run with kernels 2.1.100 and above.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Mar 18 1998 Cristian Gafton <gafton@redhat.com>
- requires glibc 2.0.6 or later

* Wed Mar 18 1998 Michael K. Johnson <johnsonm@redhat.com>
- updated PAM patch to not turn off wtmp/utmp/syslog logging.

* Wed Jan  7 1998 Cristian Gafton <gafton@redhat.com>
- added the /etc/pam.d config file
- updated PAM patch to include session support

* Tue Jan  6 1998 Cristian Gafton <gafton@redhat.com>
- updated to ppp-2.3.3, build against glibc-2.0.6 - previous patches not
  required any more.
- added buildroot
- fixed the PAM support, which was really, completely broken and against any
  standards (session support is still not here... :-( )
- we build against running kernel and pray that it will work
- added a samples patch; updated glibc patch

* Thu Dec 18 1997 Erik Troan <ewt@redhat.com>
- added a patch to use our own route.h, rather then glibc's (which has 
  alignment problems on Alpha's) -- I only applied this patch on the Alpha,
  though it should be safe everywhere

* Fri Oct 10 1997 Erik Troan <ewt@redhat.com>
- turned off the execute bit for scripts in /usr/doc

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Mar 25 1997 Erik Troan <ewt@redhat.com>
- Integrated new patch from David Mosberger
- Improved description


