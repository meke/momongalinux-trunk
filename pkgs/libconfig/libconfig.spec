%global momorel 1

Name:			libconfig
Summary: 		C/C++ configuration file library
Version:		1.4.8
Release:		%{momorel}m%{?dist}
License:		LGPLv2+
Group:			System Environment/Libraries
Source0:		http://www.hyperrealm.com/libconfig/libconfig-%{version}.tar.gz
NoSource:               0
URL:			http://www.hyperrealm.com/libconfig/
BuildRoot:		%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:		texinfo-tex

%description
Libconfig is a simple library for manipulating structured configuration 
files. This file format is more compact and more readable than XML. And 
unlike XML, it is type-aware, so it is not necessary to do string parsing 
in application code.

%package devel
Summary:		Development files for libconfig
Group:			Development/Libraries
Requires:		%{name} = %{version}-%{release}
Requires:		pkgconfig
Requires(post):		/sbin/install-info
Requires(preun):	/sbin/install-info

%description devel
Development libraries and headers for developing software against 
libconfig.

%prep
%setup -q
iconv -f iso-8859-1 -t utf-8 -o AUTHORS{.utf8,}
mv AUTHORS{.utf8,}

%build
%configure --disable-static
make %{?_smp_mflags}
make pdf

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install install-pdf
rm -rf $RPM_BUILD_ROOT%{_libdir}/*.la
rm -rf $RPM_BUILD_ROOT%{_infodir}/dir
# Prevent multilib conflicts
sed -i '/^\/CreationDate/d' $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}/libconfig.pdf
sed -i '/^\/ModDate/d' $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}/libconfig.pdf
sed -i '/^\/ID /d' $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}/libconfig.pdf

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :

%postun -p /sbin/ldconfig

%preun devel
if [ $1 = 0 ]; then
	/sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING.LIB README
%{_libdir}/libconfig*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/libconfig*
%{_libdir}/libconfig*.so
%{_libdir}/pkgconfig/libconfig*.pc
%{_defaultdocdir}/%{name}/
%{_infodir}/libconfig.info*

%changelog
* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.8-1m)
- update 1.4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Fri May 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.1-1m)
- import from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.2.1-2
- Autorebuild for GCC 4.3

* Tue Feb 19 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.2.1-1
- bump to 1.2.1

* Fri Nov 30 2007 Tom "spot" Callaway <tcallawa@redhat.com> 1.2-4
- nuke %%{_infodir}/dir, we handle it in %%post

* Fri Nov 30 2007 Tom "spot" Callaway <tcallawa@redhat.com> 1.2-3
- move all docs to devel
- move scriptlets around to match
- move requires around to match

* Fri Nov 30 2007 Tom "spot" Callaway <tcallawa@redhat.com> 1.2-2
- BR: texinfo-tex (not Requires)

* Fri Nov 30 2007 Tom "spot" Callaway <tcallawa@redhat.com> 1.2-1
- Initial package for Fedora
