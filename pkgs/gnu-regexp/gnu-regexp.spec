%global momorel 6

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:           gnu-regexp
Version:        1.1.4
Release:        10jpp.%{momorel}m%{?dist}
Summary:        Java NFA regular expression engine implementation
License:        LGPL
Source0:        ftp://ftp.tralfamadore.com/pub/java/gnu.regexp-%{version}.tar.gz
Source1:        %{name}.build.xml
BuildRequires:  ant
BuildRequires:  gnu-getopt
BuildRequires:  jpackage-utils >= 0:1.6
URL:            http://nlp.stanford.edu/nlp/javadoc/gnu-regexp-docs/
Group:          Development/Libraries
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       gnu.regexp = %{version}-%{release}
Obsoletes:      gnu.regexp < %{version}-%{release}

%description
The gnu.regexp package is a pure-Java implementation of a traditional
(non-POSIX) NFA regular expression engine. Its syntax can emulate many
popular development tools, including awk, sed, emacs, perl and grep. For
a relatively complete list of supported and non-supported syntax, refer
to the syntax and usage notes.

%package demo
Summary:        Demo for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       gnu.getopt
Group:          Development/Libraries
Provides:       gnu.regexp-demo = %{version}-%{release}
Obsoletes:      gnu.regexp-demo < %{version}-%{release}

%description demo
Demonstrations and samples for %{name}.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Provides:       gnu.regexp-javadoc = %{version}-%{release}
Obsoletes:      gnu.regexp-javadoc < %{version}-%{release}

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n gnu.regexp-%{version}
%__cp -a %{SOURCE1} build.xml
# remove all binary libs
find . -name "*.jar" -exec %__rm -f {} \;

%build
export CLASSPATH=$(build-classpath gnu.getopt)
%ant jar javadoc

%install
%__rm -rf %{buildroot}

# jars
%__mkdir_p %{buildroot}%{_javadir}
%__cp -a build/lib/gnu.regexp.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}*; do %__ln_s ${jar} `echo $jar| sed "s|-%{version}||g"`; done
%__ln_s %{name}.jar gnu.regexp.jar)

# demo
%__mkdir_p %{buildroot}%{_datadir}/%{name}/gnu/regexp/util
%__cp -a build/classes/gnu/regexp/util/*.class \
  %{buildroot}%{_datadir}/%{name}/gnu/regexp/util

# javadoc
%__mkdir_p %{buildroot}%{_javadocdir}/%{name}-%{version}
%__cp -a build/api/* %{buildroot}%{_javadocdir}/%{name}-%{version}
(cd %{buildroot}%{_javadocdir} && %__ln_s %{name}-%{version} %{name})

%clean
%__rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%doc COPYING COPYING.LIB README TODO docs/*.html
%{_javadir}/*

%files demo
%defattr(0644,root,root,0755)
%{_datadir}/%{name}

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-10jpp.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-10jpp.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-10jpp.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-10jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-10jpp.2m)
- rebuild against rpm-4.6

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-10jpp.1m)
- import from Fedora to Momonga

* Wed Oct 17 2007 Deepak Bhole <dbhole@redhat.com> 1.1.4-10jpp.3
- Resolve bz# 245270: Fixed URL

* Sun Jun 03 2007 Florian La Roche <laroche@redhat.com> - 1.1.4-10jpp.2
- remove Distribution: tag from .spec file

* Mon Feb 12 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.4-10jpp.1
- Update per Fedora review process
- Resolves rhbz#227057

* Thu May 04 2006 Ralph Apel <r.apel at r-apel.de> 0:1.1.4-10jpp
- First JPP-1.7 release
- Change name to gnu-regexp, Provide/Obsolete gnu.regexp
- Still provide gnu.regexp.jar as symlink

* Wed Feb 16 2005 Ralph Apel <r.apel at r-apel.de> 0:1.1.4-9jpp
- Build with javac 1.4.2

* Sun Feb 06 2005 David Walluck <david@jpackage.org> 0:1.1.4-8jpp
- add non-versioned javadoc link
- modernize spec

* Mon Aug 23 2004 Ralph Apel <r.apel at r-apel.de> 0:1.1.4-7jpp
- Build with ant-1.6.2

* Sun Sep 28 2003 David Walluck <david@anti-microsoft.org> 0:1.1.4-6jpp
- add Distribution and Vendor tags

* Thu Mar 27 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.1.4-5jpp
- Adapted for JPackage 1.5.

* Sat Feb 01 2003 David Walluck <david@anti-microsoft.org> 1.1.4-4jpp
- remove vendor tag

* Sat Feb 01 2003 David Walluck <david@anti-microsoft.org> 1.1.4-3jpp
- move gnu.regexp.util classes to demo package in order to remove the
  gnu.getopt dependency from the main package
- remove bzip2 compression on build script

* Sat Jan 19 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.4-2jpp
- versioned dir for javadoc
- no dependencies for javadoc package
- additional sources in individual archives
- section macro

* Sat Dec 8 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.4-1jpp
- first JPackage release
