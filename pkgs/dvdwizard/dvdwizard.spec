%global momorel 8

Summary: a set of bash-scripts for authoring Video-DVDs
Name:    dvdwizard
Version: 0.5.1
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPL
URL:     http://dvdwizard.wershofen.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Requires: dvdauthor ffmpeg ImageMagick mjpegtools transcode
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The general idea behind dvdwizard was to automate the creation of a full-featured Video-DVD structure from a given MPEG-file and make it as easy as possible. On the other hand, it should be flexible enough to let the more experienced user adjust the looks of the DVD to his or her liking.

The result is a set of bash scripts, which act as a command-line frontend to dvdauthor , which is one of the two core applications within dvdwizard. dvdauthor creates a Video-DVD-compliant directory structure, suitable to be watched directly with a software DVD-Player like Xine or to be burned on DVD and watched on TV with a hardware player.

The other core application is ImageMagick , which offers a wast variety of tools to create and manipulate graphical data. Within dvdwizard, ImageMagick is used to create menu backgrounds and button masks without any user interaction.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sysconfdir}
for script in dvdwizard mk_vmgm mk_vtsm mk_vtsm_lang mk_vtsm_info dvdwizardrc chaptercheck dvdcpics dvdtguess mpgprobe txt2png; do
    install -m 755 ${script} %{buildroot}%{_bindir}
done
install -m 644 dvdwizard.conf.sample %{buildroot}%{_sysconfdir}/dvdwizard.conf


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGELOG CONTRIB COPYING README TODO TOOLS
%{_bindir}/*
%config  %{_sysconfdir}/dvdwizard.conf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-2m)
- %%NoSource -> NoSource

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5.1-1m)
- import to Momonga
