%global momorel 5

# Use rpmbuild --without gcj to disable native bits
%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name:          writer2latex
Version:       1.0.2
Release:       %{momorel}m%{?dist}
Summary:       Document Converter from ODT to LaTeX
License:       LGPLv2
Url:           http://writer2latex.sourceforge.net/
Source0:       http://writer2latex.svn.sourceforge.net/viewvc/writer2latex/tags/%{version}.tar.gz
Patch0:        writer2latex-1.0.2-momonga.patch
BuildRequires: ant, openoffice.org-core
Group:         Applications/File
Buildroot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:      jaxp_parser_impl, jaxp_transform_impl
%if %{with_gcj}
BuildRequires: java-gcj-compat-devel >= 1.0.31
Requires(post): java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch: noarch
%endif

%description
Writer2LaTeX is a utility written in java. It converts OpenOffice.org documents
- in particular documents containing formulas - into other formats. It is
actually a collection of four converters, i.e.:
1) Writer2LaTeX converts documents into LaTeX 2e format for high quality
   typesetting.
2) Writer2BibTeX extracts bibliographic data from a document and stores it in
   BibTeX format (works together with Writer2LaTeX).
3) Writer2xhtml converts documents into XHTML 1.0 or XHTML 1.1+MathML 2.0 with
   CSS2.
4) Calc2xhtml is a companion to Writer2xhtml that converts OOo Calc documents
   to XHTML 1.0 with CSS2 to display your spreadsheets on the web.

%package javadoc
Summary:     Javadoc for %{name}
Group:       Documentation
%if %{with_gcj}
BuildArch: noarch
%endif

%description javadoc
Javadoc for %{name}.

%package -n openoffice.org-writer2latex
Summary:          OpenOffice.org Writer To LateX Converter
Group:            Applications/Productivity
Requires:         openoffice.org-core
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core

%package -n openoffice.org-writer2xhtml
Summary:          OpenOffice.org Writer to xhtml Converter
Group:            Applications/Productivity
Requires:         openoffice.org-core
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core

%description -n openoffice.org-writer2latex
Document Converter Extension for OpenOffice.org to provide 
LaTeX and BibTeX export filters.

%description -n openoffice.org-writer2xhtml
Document Converter Extension for OpenOffice.org to provide 
XHTML export filters.

%prep
%setup -q -n %{version}
%patch0 -p1 -b .momonga
sed -i -e "s|<property name=\"OFFICE_CLASSES\" location=\"/usr/share/java/openoffice\" />|<property name=\"OFFICE_CLASSES\" location=\"%{_libdir}/openoffice.org3.0/basis3.3/program/classes\" />|" build.xml
sed -i -e "s|<property name=\"URE_CLASSES\" location=\"/usr/share/java/openoffice\" />|<property name=\"URE_CLASSES\" location=\"%{_libdir}/openoffice.org3.0/ure/share/java\" />|" build.xml

%build
ant jar javadoc oxt

%install
rm -rf $RPM_BUILD_ROOT
# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -p -m 644 target/lib/%{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} ${jar/-%{version}/}; done)
# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -p -r target/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
pushd $RPM_BUILD_ROOT%{_javadocdir}
ln -s %{name}-%{version} %{name}
popd
# OOo extensions
install -d -m 755 $RPM_BUILD_ROOT%{_libdir}/openoffice.org3.0/extensions/writer2latex.oxt
unzip target/lib/writer2latex.oxt -d $RPM_BUILD_ROOT%{_libdir}/openoffice.org3.0/extensions/writer2latex.oxt
install -d -m 755 $RPM_BUILD_ROOT%{_libdir}/openoffice.org3.0/extensions/writer2xhtml.oxt
unzip target/lib/writer2xhtml.oxt -d $RPM_BUILD_ROOT%{_libdir}/openoffice.org3.0/extensions/writer2xhtml.oxt
%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc source/distro/History.txt source/distro/COPYING.TXT source/distro/Readme.txt source/distro/doc/user-manual.odt
%{_javadir}/*
%if %{with_gcj}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}
%{_javadocdir}/%{name}-%{version}

%files -n openoffice.org-writer2latex
%defattr(0644,root,root,0755)
%{_libdir}/openoffice.org3.0/extensions/writer2latex.oxt

%files -n openoffice.org-writer2xhtml
%defattr(0644,root,root,0755)
%{_libdir}/openoffice.org3.0/extensions/writer2xhtml.oxt

%changelog
* Thu Apr 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-5m)
- fix build with libreoffice-3.3.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2 and momonganize

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.2-4m)
- use %%{ooodir}/program/unopkg instead of /usr/bin/unopkg
- rename org.openoffice.da.writer2latex.oxt to writer2latex.oxt

* Tue Jul 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.2-3m)
- revise for Momonga
-- --link option of unopkg is Fedora specific

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.2-2m)
- revise for OOo3

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.2-1m)
- sync with Fedora 11 (0.5.0.2-6) and momonganize for OOo2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-2m)
- rebuild against rpm-4.6

* Wed May 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5-1m)
- import to Momonga from Fedora

* Wed Mar 26 2008 Caolan McNamara <caolanm@redhat.com> 0.5-3
- tweak for guidelines

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.5-2
- Autorebuild for GCC 4.3

* Sun Dec 1 2007 Caolan McNamara <caolanm@redhat.com> 0.5-1
- initial version
