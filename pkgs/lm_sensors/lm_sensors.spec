%global momorel 2

Summary: Hardware Health Monitoring Tools
Name: lm_sensors
Version: 3.3.4
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: Applications/System
URL: http://www.lm-sensors.org/
Source0: http://dl.lm-sensors.org/lm-sensors/releases/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: lm_sensors.sysconfig
# these 2 were taken from PLD-linux, Thanks!
Source2: sensord.sysconfig 
Source3: lm_sensors-modprobe
Source4: lm_sensors-modprobe-r

Patch0: lm_sensors-3.3.4-sensors-detect-null-input.patch
Patch1: lm_sensors-3.3.4-lm_sensors-service-modprobe-warnings.patch
Patch2: lm_sensors-3.3.4-man-sensors-conf-conv.patch
Patch3: lm_sensors-3.3.4-sensors-detect-ppc-missing-vendor_id.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(preun): chkconfig
Requires: %{name}-libs = %{version}-%{release}
Requires: dmidecode
BuildRequires: flex
BuildRequires: gawk
# lm_sensors-2.7.0 requires i2c-2.7.0 patched kernel
BuildRequires: kernel-headers >= 2.4.22-28m
BuildRequires: libsysfs-devel
BuildRequires: rrdtool-devel >= 1.3.0
ExclusiveArch: alpha %{ix86} x86_64

%description
The lm_sensors package includes a collection of modules for general SMBus
access and hardware monitoring.  NOTE: this requires special support which
is not in standard 2.2-vintage kernels.

%package libs
Summary: Lm_sensors core libraries
Group: System Environment/Libraries

%description libs
Core libraries for lm_sensors applications.

%package devel
Summary: Development files for programs which will use lm_sensors.
Group: Development/System
Requires: %{name} = %{version}-%{release}

%description devel
The lm_sensors-devel package includes a header files and libraries for use
when building applications that make use of sensor data.

%package sensord
Summary: Daemon that periodically logs sensor readings
Group: System Environment/Daemons
Requires(post): chkconfig
Requires(preun): chkconfig
Requires: %{name} = %{version}-%{release}

%description sensord
Daemon that periodically logs sensor readings to syslog or a round-robin
database, and warns of sensor alarms.

%prep
%setup -q

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

mv prog/init/README prog/init/README.initscripts
chmod -x prog/init/fancontrol.init

%build
export CFLAGS="%{optflags}"

make PREFIX=%{_prefix} \
	LIBDIR=%{_libdir} \
	MANDIR=%{_mandir} \
	EXLDFLAGS= \
	PROG_EXTRA=sensord user

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make PREFIX=%{_prefix} \
	LIBDIR=%{_libdir} \
	MANDIR=%{_mandir} PROG_EXTRA=sensord \
	DESTDIR=%{buildroot} user_install

rm $RPM_BUILD_ROOT%{_libdir}/libsensors.a

# Remove userland kernel headers, belong in glibc-kernheaders.
rm -rf %{buildroot}%{_includedir}/linux

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sensors.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/lm_sensors
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/sensord

# service files
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
install -pm 644 prog/init/sensord.service    $RPM_BUILD_ROOT%{_unitdir}
install -pm 644 prog/init/lm_sensors.service $RPM_BUILD_ROOT%{_unitdir}
install -pm 644 prog/init/fancontrol.service $RPM_BUILD_ROOT%{_unitdir}

# customized modprobe calls
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}/%{name}
install -pm 755 %{SOURCE3} $RPM_BUILD_ROOT%{_libexecdir}/%{name}/modprobe
install -pm 755 %{SOURCE4} $RPM_BUILD_ROOT%{_libexecdir}/%{name}/modprobe-r

# Note non standard systemd scriptlets, since reload / stop makes no sense
# for lm_sensors
%triggerun -- lm_sensors < 3.3.1-2m
if [ -L /etc/rc3.d/S26lm_sensors ]; then
    /bin/systemctl enable lm_sensors.service >/dev/null 2>&1 || :
fi
/sbin/chkconfig --del lm_sensors

%post
%systemd_post lm_sensors.service

%preun
%systemd_preun lm_sensors.service

%postun
%systemd_postun_with_restart lm_sensors.service

# ==== sensord ===

%post sensord 
%systemd_post sensord.service

%preun sensord
%systemd_preun sensord.service

%postun sensord
%systemd_postun_with_restart sensord.service

# ===== libs =====

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%doc CHANGES CONTRIBUTORS COPYING doc README*
%doc prog/init/fancontrol.init prog/init/README.initscripts
%config(noreplace) %{_sysconfdir}/sensors3.conf
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_sbindir}/*
%{_unitdir}/lm_sensors.service
%{_unitdir}/fancontrol.service
%{_libexecdir}/%{name}/modprobe*
%config(noreplace) %{_sysconfdir}/sysconfig/lm_sensors
%exclude %{_sbindir}/sensord
%exclude %{_mandir}/man8/sensord.8.*

%files libs
%{_libdir}/*.so.*

%files devel
%{_includedir}/sensors
%{_libdir}/lib*.so
%{_mandir}/man3/*

%files sensord
%doc prog/sensord/README
%{_sbindir}/sensord
%{_mandir}/man8/sensord.8.*
%config(noreplace) %{_sysconfdir}/sysconfig/sensord
%{_unitdir}/sensord.service

%changelog
* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.4-2m)
- add any patch

* Mon Sep 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.4-1m)
- version 3.3.4

* Wed Oct 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-1m)
- version 3.3.2

* Fri Feb 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-2m)
- support systemd

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-1m)
- update 3.3,1

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.0-1m)
- update 3.3,0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-4m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-3m)
- split package libs
- import lm85.patch from Fedora
 +* Wed Mar 31 2010 Nikola Pajkovsky <npajkovs@redhat.com> - 3.1.2-2
 +- patch lm_sensors-3.1.2-lm85.patch add into sensors-detect driver lm85
 +- Resolved: 578527 - sensors-detect fails to detect
- clean up old patches

* Sun Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-2m)
- change License to GPLv2+ and LGPLv2+

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-1m)
- version 3.1.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.1-1m)
- version 3.1.1

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-1m)
- version 3.1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-2m)
- rebuild against rpm-4.6

* Thu Dec 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.3-1m)
- version 3.0.3

* Fri Jul 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.2-2m)
- rebuild against rrdtool

* Tue May 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-1m)
- version 3.0.2
- add a package sensord

* Thu Apr 15 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.10.3-5m)
- add applesmc patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.3-4m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.10.3-3m)
- rebuild against perl-5.10.0-1m

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.3-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Mon Apr 16 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Mon Mar 19 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.1-3m)
- add non init patch

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.1-2m)
- separate devel package for xfce4-sensors-plugin

* Fri Jan 12 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1
- update Patch5: lm_sensors-2.10.1-local.patch

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.0-3m)
- rebuild against sysfsutils
- change BuildRequires: libsysfs-devel

* Mon Jul 31 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.10.0-2m)
- remove Patch1: lm_sensors-2.5.5-glibc22.patch

* Fri Feb 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.10.0-1m)
- version up
- remove Patch24: lm_sensors-2.9.0-init.patch
- remove Patch25: lm_sensors-2.9.0-bmcsensors.patch

* Thu Feb 24 2005 Toru Hoshina <t@momonga-linux.org>
- (2.9.0-1m)
- version up.
- based on [Momonga-devel.ja:02992] Thanks to M Sato <m.sato@screen.co.jp>

* Sun Feb  6 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.8.7-4m)
- enable x86_64.

* Wed Jan 19 2005 mutecat <mutecat@momonga-linux.org>
- (2.8.7-3m)
- remove lm_sensors.ppc.patch

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.7-2m)
- stop daemon

* Mon Jun 21 2004 muradaikan <muradaikan@momonga-linux.org>
- (2.8.7-1m)
- version update to 2.8.7
- change spec to build against linux kernel 2.6.x
     make    	  -> make user
     make install -> make user_install

* Mon May 17 2004 mutecat <mutecat@momonga-linux.org>
- (2.8.4-3m)
- change lm_sensors.ppc.patch

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.8.4-2m)
- revised spec for enabling rpm 4.2.

* Fri Feb 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.8.4-1m)
- update to 2.8.4

* Thu Nov 20 2003 zunda <zunda at freeshell.org>
- (2.8.1-2m)
- lm_sensors-2.8.1.sensors-path.patch corrects the path from
  /usr/local/bin/sensors to /usr/bin/sensors

* Mon Nov 17 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.8.1-1m)
- update to 2.8.1
- replace my old e-mail addresses in this spec file
- add more files to %%files

* Sun Feb  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.7.0-1m)
- update to 2.7.0
- change URL
- move i2c-mkdev.sh from /dev to %%{_sbindir}
- add %%{_initscriptdir}/lm_sensors

* Sat Jul 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.4-1m)
- minor feature enhancements

* Sun May 26 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.6.3-3k)
- revise Source0:

* Sun Mar 31 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.6.3-2k)
- update to 2.6.3

* Tue Nov 20 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.6.2-2k)
- update to 2.6.2

* Mon Sep  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.6.1-2k)
- update to 2.6.1

* Wed Aug 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.6.1-0.020010814002k)
- update to 2.6.1-20010814(CVS) for kernel-2.4.8

* Sun Jul 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.6.0-2k)
- update to 2.6.0

* Tue Mar 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.5.5-2k)
- update to 2.5.5

* Tue Mar 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.5.4-6k)
- fixed for ppc

* Tue Dec 19 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.5.4-5k)
- added alpha.prog.dump.isadump.c.patch for Alpha
- added i2c-mkdev.sh

* Mon Dec 18 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.5.4-3k)
- Initial Kondarization
