%global momorel 7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
# NOTE: This package contains only C header files and pkg-config *.pc files,
# and does not contain any ELF binaries or DSOs, so we disable debuginfo
# generation.
%define debug_package %{nil}

Summary: X.Org X11 Protocol headers
Name: xorg-x11-proto-devel
Version: 7.8
Release: 0.%{momorel}m%{?dist}
License: "The Open Group License"
Group: Development/System
URL: http://www.x.org/
%define sourceurl	http://xorg.freedesktop.org/releases/individual/proto
Source0: %{sourceurl}/bigreqsproto-1.1.2.tar.bz2 
NoSource: 0
Source1: %{sourceurl}/compositeproto-0.4.2.tar.bz2 
NoSource: 1
Source2: %{sourceurl}/damageproto-1.2.1.tar.bz2 
NoSource: 2
Source3: %{sourceurl}/dmxproto-2.3.1.tar.bz2 
NoSource: 3
Source32: %{sourceurl}/dri2proto-2.8.tar.bz2
NoSource: 32
Source4: %{sourceurl}/evieext-1.1.1.tar.bz2 
NoSource: 4
Source5: %{sourceurl}/fixesproto-5.0.tar.bz2 
NoSource: 5
Source6: %{sourceurl}/fontcacheproto-0.1.3.tar.bz2 
NoSource: 6
Source7: %{sourceurl}/fontsproto-2.1.2.tar.bz2 
NoSource: 7
Source8: %{sourceurl}/glproto-1.4.17.tar.bz2 
NoSource: 8
Source9: %{sourceurl}/inputproto-2.3.1.tar.bz2 
NoSource: 9
Source10: %{sourceurl}/kbproto-1.0.6.tar.bz2 
NoSource: 10
Source12: %{sourceurl}/printproto-1.0.5.tar.bz2 
NoSource: 12
Source13: %{sourceurl}/randrproto-1.4.0.tar.bz2 
NoSource: 13
Source14: %{sourceurl}/recordproto-1.14.2.tar.bz2 
NoSource: 14
Source15: %{sourceurl}/renderproto-0.11.1.tar.bz2 
NoSource: 15
Source16: %{sourceurl}/resourceproto-1.2.0.tar.bz2 
NoSource: 16
Source17: %{sourceurl}/scrnsaverproto-1.2.2.tar.bz2 
NoSource: 17
Source18: %{sourceurl}/trapproto-3.4.3.tar.bz2 
NoSource: 18
Source19: %{sourceurl}/videoproto-2.3.1.tar.bz2 
NoSource: 19
Source20: http://xcb.freedesktop.org/dist/xcb-proto-1.10.tar.bz2 
NoSource: 20
Source21: %{sourceurl}/xcmiscproto-1.2.2.tar.bz2 
NoSource: 21
Source22: %{sourceurl}/xextproto-7.2.1.tar.bz2 
NoSource: 22
Source23: %{sourceurl}/xf86bigfontproto-1.2.0.tar.bz2 
NoSource: 23
Source24: %{sourceurl}/xf86dgaproto-2.1.tar.bz2 
NoSource: 24
Source25: %{sourceurl}/xf86driproto-2.1.1.tar.bz2 
NoSource: 25
Source26: %{sourceurl}/xf86miscproto-0.9.3.tar.bz2 
NoSource: 26
Source27: %{sourceurl}/xf86rushproto-1.1.2.tar.bz2 
NoSource: 27
Source28: %{sourceurl}/xf86vidmodeproto-2.3.1.tar.bz2 
NoSource: 28
Source29: %{sourceurl}/xineramaproto-1.2.1.tar.bz2 
NoSource: 29
Source30: %{sourceurl}/xproto-7.0.24.tar.bz2 
NoSource: 30
Source31: %{sourceurl}/xproxymanagementprotocol-1.0.3.tar.bz2 
NoSource: 31
Source33: %{sourceurl}/dri3proto-1.0.tar.bz2
NoSource: 33
Source34: %{sourceurl}/presentproto-1.0.tar.bz2
NoSource: 34

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
BuildRequires: xmlto-tex
Requires: python >= 2.7
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

%description
X.Org X11 Protocol headers

%prep
%setup -q -c %{name}-%{version} -a1 -a2 -a3 -a4 -a5 -a6 -a7 -a8 -a9 -a10 -a12 -a13 -a14 -a15 -a16 -a17 -a18 -a19 -a20 -a21 -a22 -a23 -a24 -a25 -a26 -a27 -a28 -a29 -a30 -a31 -a32 -a33 -a34

%build

# Proceed through each proto package directory, building them all
for dir in $(ls -1) ; do
	pushd $dir
	%configure
	make %{?_smp_mflags}
	popd
done

%install
rm -rf --preserve-root %{buildroot}
for dir in $(ls -1) ; do
	pushd $dir
	%makeinstall
	popd
done

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%dir %{_includedir}/GL
%{_includedir}/GL/glxint.h
%{_includedir}/GL/glxmd.h
%{_includedir}/GL/glxproto.h
%{_includedir}/GL/glxtokens.h
%dir %{_includedir}/GL/internal
%{_includedir}/GL/internal/glcore.h
%{_includedir}/X11/DECkeysym.h
%{_includedir}/X11/HPkeysym.h
%dir %{_includedir}/X11/PM
%{_includedir}/X11/PM/PM.h
%{_includedir}/X11/PM/PMproto.h
%{_includedir}/X11/Sunkeysym.h
%{_includedir}/X11/X.h
%{_includedir}/X11/XF86keysym.h
%{_includedir}/X11/XWDFile.h
%{_includedir}/X11/Xalloca.h
%{_includedir}/X11/Xarch.h
%{_includedir}/X11/Xatom.h
%{_includedir}/X11/Xdefs.h
%{_includedir}/X11/Xfuncproto.h
%{_includedir}/X11/Xfuncs.h
%{_includedir}/X11/Xmd.h
%{_includedir}/X11/Xos.h
%{_includedir}/X11/Xos_r.h
%{_includedir}/X11/Xosdefs.h
%{_includedir}/X11/Xpoll.h
%{_includedir}/X11/Xproto.h
%{_includedir}/X11/Xprotostr.h
%{_includedir}/X11/Xthreads.h
%{_includedir}/X11/Xw32defs.h
%{_includedir}/X11/Xwindows.h
%{_includedir}/X11/Xwinsock.h
%{_includedir}/X11/ap_keysym.h
%dir %{_includedir}/X11/dri
%{_includedir}/X11/dri/xf86dri.h
%{_includedir}/X11/dri/xf86driproto.h
%{_includedir}/X11/dri/xf86dristr.h
%dir %{_includedir}/X11/extensions
# %{_includedir}/X11/extensions/MITMisc.h
%{_includedir}/X11/extensions/Print.h
%{_includedir}/X11/extensions/Printstr.h
%{_includedir}/X11/extensions/EVI.h
%{_includedir}/X11/extensions/EVIproto.h
%{_includedir}/X11/extensions/XI.h
%{_includedir}/X11/extensions/XI2.h
%{_includedir}/X11/extensions/XI2proto.h
# %{_includedir}/X11/extensions/XInput.h
%{_includedir}/X11/extensions/XIproto.h
%{_includedir}/X11/extensions/XKB.h
%{_includedir}/X11/extensions/XKBgeom.h
%{_includedir}/X11/extensions/XKBproto.h
%{_includedir}/X11/extensions/XKBsrv.h
%{_includedir}/X11/extensions/XKBstr.h
# %{_includedir}/X11/extensions/XLbx.h
%{_includedir}/X11/extensions/XResproto.h
# %{_includedir}/X11/extensions/XShm.h
# %{_includedir}/X11/extensions/XTest.h
%{_includedir}/X11/extensions/ag.h
%{_includedir}/X11/extensions/agproto.h
# %{_includedir}/X11/extensions/Xagstr.h
# %{_includedir}/X11/extensions/Xcup.h
# %{_includedir}/X11/extensions/Xcupstr.h
# %{_includedir}/X11/extensions/Xdbe.h
# %{_includedir}/X11/extensions/Xdbeproto.h
# %{_includedir}/X11/extensions/Xext.h
%{_includedir}/X11/extensions/Xeviestr.h
# %{_includedir}/X11/extensions/Xge.h
# %{_includedir}/X11/extensions/Xinerama.h
%{_includedir}/X11/extensions/Xv.h
%{_includedir}/X11/extensions/XvMC.h
%{_includedir}/X11/extensions/XvMCproto.h
%{_includedir}/X11/extensions/Xvproto.h
%{_includedir}/X11/extensions/bigreqstr.h
%{_includedir}/X11/extensions/bigreqsproto.h
%{_includedir}/X11/extensions/composite.h
%{_includedir}/X11/extensions/compositeproto.h
%{_includedir}/X11/extensions/cup.h
%{_includedir}/X11/extensions/cupproto.h
%{_includedir}/X11/extensions/damageproto.h
%{_includedir}/X11/extensions/damagewire.h
%{_includedir}/X11/extensions/dbe.h
%{_includedir}/X11/extensions/dbeproto.h
# %{_includedir}/X11/extensions/dmxext.h
%{_includedir}/X11/extensions/dmx.h
%{_includedir}/X11/extensions/dmxproto.h
%{_includedir}/X11/extensions/dpmsconst.h
%{_includedir}/X11/extensions/dpmsproto.h
%{_includedir}/X11/extensions/dri2proto.h
%{_includedir}/X11/extensions/dri2tokens.h
%{_includedir}/X11/extensions/dri3proto.h
%{_includedir}/X11/extensions/evieproto.h
%{_includedir}/X11/extensions/ge.h
%{_includedir}/X11/extensions/geproto.h
%{_includedir}/X11/extensions/fontcache.h
%{_includedir}/X11/extensions/fontcacheP.h
%{_includedir}/X11/extensions/fontcachstr.h
%{_includedir}/X11/extensions/lbx.h
%{_includedir}/X11/extensions/lbxproto.h
%{_includedir}/X11/extensions/mitmiscconst.h
%{_includedir}/X11/extensions/mitmiscproto.h
%{_includedir}/X11/extensions/multibufconst.h
%{_includedir}/X11/extensions/multibufproto.h
# %{_includedir}/X11/extensions/panoramiXext.h
%{_includedir}/X11/extensions/panoramiXproto.h
%{_includedir}/X11/extensions/presentproto.h
%{_includedir}/X11/extensions/presenttokens.h
%{_includedir}/X11/extensions/randr.h
%{_includedir}/X11/extensions/randrproto.h
# %{_includedir}/X11/extensions/record.h
%{_includedir}/X11/extensions/recordconst.h
%{_includedir}/X11/extensions/recordproto.h
%{_includedir}/X11/extensions/recordstr.h
%{_includedir}/X11/extensions/render.h
%{_includedir}/X11/extensions/renderproto.h
%{_includedir}/X11/extensions/saver.h
%{_includedir}/X11/extensions/saverproto.h
%{_includedir}/X11/extensions/secur.h
%{_includedir}/X11/extensions/securproto.h
%{_includedir}/X11/extensions/shapeconst.h
%{_includedir}/X11/extensions/shapeproto.h
%{_includedir}/X11/extensions/shapestr.h
%{_includedir}/X11/extensions/shm.h
%{_includedir}/X11/extensions/shmproto.h
%{_includedir}/X11/extensions/shmstr.h
%{_includedir}/X11/extensions/syncconst.h
%{_includedir}/X11/extensions/syncproto.h
%{_includedir}/X11/extensions/syncstr.h
%{_includedir}/X11/extensions/vldXvMC.h
%{_includedir}/X11/extensions/xcmiscproto.h
%{_includedir}/X11/extensions/xcmiscstr.h
%{_includedir}/X11/extensions/xf86bigfont.h
%{_includedir}/X11/extensions/xf86bigfproto.h
%{_includedir}/X11/extensions/xf86bigfstr.h
%{_includedir}/X11/extensions/xf86dga.h
# %{_includedir}/X11/extensions/xf86dga1.h
%{_includedir}/X11/extensions/xf86dga1const.h
%{_includedir}/X11/extensions/xf86dga1proto.h
%{_includedir}/X11/extensions/xf86dga1str.h
%{_includedir}/X11/extensions/xf86dgaconst.h
%{_includedir}/X11/extensions/xf86dgaproto.h
%{_includedir}/X11/extensions/xf86dgastr.h
%{_includedir}/X11/extensions/xf86misc.h
%{_includedir}/X11/extensions/xf86mscstr.h
%{_includedir}/X11/extensions/xf86rush.h
%{_includedir}/X11/extensions/xf86rushstr.h
# %{_includedir}/X11/extensions/xf86vmode.h
%{_includedir}/X11/extensions/xf86vm.h
%{_includedir}/X11/extensions/xf86vmproto.h
%{_includedir}/X11/extensions/xf86vmstr.h
%{_includedir}/X11/extensions/xfixesproto.h
%{_includedir}/X11/extensions/xfixeswire.h
%{_includedir}/X11/extensions/xtestconst.h
%{_includedir}/X11/extensions/xtestext1const.h
%{_includedir}/X11/extensions/xtestext1proto.h
%{_includedir}/X11/extensions/xtestproto.h
%{_includedir}/X11/extensions/xtrapbits.h
%{_includedir}/X11/extensions/xtrapddmi.h
%{_includedir}/X11/extensions/xtrapdi.h
%{_includedir}/X11/extensions/xtrapemacros.h
%{_includedir}/X11/extensions/xtraplib.h
%{_includedir}/X11/extensions/xtraplibp.h
%{_includedir}/X11/extensions/xtrapproto.h

%dir %{_includedir}/X11/fonts
%{_includedir}/X11/fonts/FS.h
%{_includedir}/X11/fonts/FSproto.h
%{_includedir}/X11/fonts/font.h
%{_includedir}/X11/fonts/fontproto.h
%{_includedir}/X11/fonts/fontstruct.h
%{_includedir}/X11/fonts/fsmasks.h
%{_includedir}/X11/keysym.h
%{_includedir}/X11/keysymdef.h
%{_libdir}/pkgconfig/bigreqsproto.pc
%{_libdir}/pkgconfig/compositeproto.pc
%{_libdir}/pkgconfig/damageproto.pc
%{_libdir}/pkgconfig/dmxproto.pc
%{_libdir}/pkgconfig/dri2proto.pc
%{_libdir}/pkgconfig/dri3proto.pc
%{_libdir}/pkgconfig/evieproto.pc
%{_libdir}/pkgconfig/fixesproto.pc
%{_libdir}/pkgconfig/fontcacheproto.pc
%{_libdir}/pkgconfig/fontsproto.pc
%{_libdir}/pkgconfig/glproto.pc
%{_libdir}/pkgconfig/inputproto.pc
%{_libdir}/pkgconfig/kbproto.pc
%{_libdir}/pkgconfig/presentproto.pc
%{_libdir}/pkgconfig/printproto.pc
%{_libdir}/pkgconfig/randrproto.pc
%{_libdir}/pkgconfig/recordproto.pc
%{_libdir}/pkgconfig/renderproto.pc
%{_libdir}/pkgconfig/resourceproto.pc
%{_libdir}/pkgconfig/scrnsaverproto.pc
%{_libdir}/pkgconfig/trapproto.pc
%{_libdir}/pkgconfig/videoproto.pc
%{_libdir}/pkgconfig/xcb-proto.pc
%{_libdir}/pkgconfig/xcmiscproto.pc
%{_libdir}/pkgconfig/xextproto.pc
%{_libdir}/pkgconfig/xf86bigfontproto.pc
%{_libdir}/pkgconfig/xf86dgaproto.pc
%{_libdir}/pkgconfig/xf86driproto.pc
%{_libdir}/pkgconfig/xf86miscproto.pc
%{_libdir}/pkgconfig/xf86rushproto.pc
%{_libdir}/pkgconfig/xf86vidmodeproto.pc
%{_libdir}/pkgconfig/xineramaproto.pc
%{_libdir}/pkgconfig/xproto.pc
%{_libdir}/pkgconfig/xproxymngproto.pc
%{_datadir}/xcb/bigreq.xml
%{_datadir}/xcb/composite.xml
%{_datadir}/xcb/damage.xml
%{_datadir}/xcb/dpms.xml
%{_datadir}/xcb/dri2.xml
%{_datadir}/xcb/dri3.xml
%{_datadir}/xcb/ge.xml
%{_datadir}/xcb/glx.xml
%{_datadir}/xcb/present.xml
%{_datadir}/xcb/randr.xml
%{_datadir}/xcb/record.xml
%{_datadir}/xcb/render.xml
%{_datadir}/xcb/res.xml
%{_datadir}/xcb/screensaver.xml
%{_datadir}/xcb/shape.xml
%{_datadir}/xcb/shm.xml
%{_datadir}/xcb/sync.xml
%{_datadir}/xcb/xc_misc.xml
%{_datadir}/xcb/xcb.xsd
%{_datadir}/xcb/xevie.xml
%{_datadir}/xcb/xf86dri.xml
%{_datadir}/xcb/xf86vidmode.xml
%{_datadir}/xcb/xfixes.xml
%{_datadir}/xcb/xinerama.xml
%{_datadir}/xcb/xinput.xml
%{_datadir}/xcb/xkb.xml
%{_datadir}/xcb/xprint.xml
%{_datadir}/xcb/xproto.xml
%{_datadir}/xcb/xselinux.xml
%{_datadir}/xcb/xtest.xml
%{_datadir}/xcb/xv.xml
%{_datadir}/xcb/xvmc.xml

%{_datadir}/doc/bigreqsproto
%{_datadir}/doc/dri2proto
%{_datadir}/doc/dri3proto
%{_datadir}/doc/fontsproto
%{_datadir}/doc/inputproto
%{_datadir}/doc/kbproto/
%{_datadir}/doc/presentproto
%{_datadir}/doc/recordproto
%{_datadir}/doc/resourceproto
%{_datadir}/doc/scrnsaverproto
%{_datadir}/doc/videoproto
%{_datadir}/doc/xcmiscproto
%{_datadir}/doc/xextproto

%{_datadir}/doc/compositeproto/compositeproto.txt
%{_datadir}/doc/damageproto/damageproto.txt
%{_datadir}/doc/fixesproto/fixesproto.txt
%{_datadir}/doc/randrproto/randrproto.txt
%{_datadir}/doc/renderproto/renderproto.txt
%{_datadir}/doc/xproxymanagementprotocol/PM_spec

%{_datadir}/doc/xproto

%{python_sitelib}/xcbgen/

%{_mandir}/man7/Xprint.7.*

%changelog
* Tue Jun 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8-0.7m)
- update inputproto

* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8-0.6m)
- update glproto

* Thu Mar 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8-0.5m)
- update xcbproto

* Thu Mar 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8-0.4m)
- add presentproto and dri3proto

* Sun Apr 14 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.8-0.3m)
- update xproto and inputproto

* Mon Oct 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.8-0.2m)
- update xcb-proto-1.8

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.8-0.1m)
- update dri2proto-2.8 glproto-1.4.16 randrproto-1.4.0

* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-1m)
- update 7.7 (version only)

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-0.9m)
- update bigreqsproto-1.1.2

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.8m)
- update fontsproto-2.1.2, fontsproto-2.1.2, recordproto-1.14.2
-        scrnsaverproto-1.2.2, xextproto-7.2.1, xcb-proto-1.7.1

* Sat Mar 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.7m)
- update xproto-7.0.23

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.6m)
- update inputproto-2.2

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.5m)
- update inputproto-2.1.99.6

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.4m)
- update glproto-1.4.15

* Fri Jan 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.3m)
- update xcb-proto-1.7

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.2m)
- update inputproto-2.1.99.5

* Fri Jan  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-0.1m)
- update inputproto-2.1.99.4
-- support multi-touch interface

* Tue Dec 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-11m)
- add BuildRequires

* Mon Dec 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-9m)
- inputproto-2.1

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-9m)
- dri2proto-2.6 glproto-1.4.14

* Sat Jun 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-8m)
- update xproto-7.0.22

* Sat Jun 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-7m)
- build fix mesa-7.10.3 (add patch0, 1)

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.6-6m)
- update inputproto-2.0.2 resourceproto-1.2.0
- dri2proto-2.4 glproto-1.4.13

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.6-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-4m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.6-3m)
- update to
- fixesproto-5.0.0 xextproto-7.2.0 xproto-7.0.21

* Thu Jan  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-2m)
- update to
-- dmxproto-2.3.1 evieext-1.1.1 printproto-1.0.5
-- xf86driproto-2.1.1 xf86vidmodeproto-2.3.1 xineramaproto-1.2.1

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-1m)
- update to 7.6
- update to renderproto-0.11.1

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-18m)
- update to xproto-7.0.20 resourceproto-1.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-17m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-16m)
- update to inputprot-2.0.1

* Wed Nov  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-15m)
- update xproto-7.0.19

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-14m)
- update many proto packages

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.5-13m)
- full rebuild for mo7 release

* Fri May 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-12m)
- update dri2proto-2.3
- update xf86miscproto-0.9.3
- update xproxymanagementprotocol

* Wed May 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.5-11m)
- update xproto-7.0.17

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-10m)
- update glproto-1.4.11
- update dri2proto-2.2

* Thu Jan 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-9m)
- update xcb-proto-1.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-1m)
- update to xorg 7.5
- update kbproto-1.0.4

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.6m)
- update xproto-7.0.16, xf86driproto-2.1.0, xf86bigfontproto-1.2.0
-- xextproto-7.1.1, xcmiscproto-1.2.0, videoproto-2.3.0
-- scrnsaverproto-1.2.0, resourceproto-1.1.0, renderproto-0.11
-- fontsproto-2.1.0, fixesproto-4.1, evieext-1.1.0
-- evieext-1.1.0, damageproto-1.2.0, bigreqsproto-1.1.0 
-- xineramaproto-1.2, randrproto-1.3.1, xf86vidmodeproto-2.3
-- xf86dgaproto-2.1, recordproto-1.14, inputproto, 
-- fontcacheproto-0.1.3, fixesproto-4.1.1, dmxproto-2.3
-- compositeproto-0.4.1

* Sat Jul  4 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.5m)
- update xcb-proto

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.4m)
- update dri2proto-2.1, glproto-1.4.10, inputproto-1.5.1

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.3m)
- update dri2proto-2.0

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.2m)
- update randrproto-1.3.0, xproto-7.0.15

* Mon Feb 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.1m)
- update dri2proto-1.99.3
- update xproto-7.0.14
- update xextproto-7.0.5
- update randrproto-1.2.99.3
- update inputproto-1.5.0


* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.4-11m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.4-10m)
- rebuild agaisst python-2.6.1-1m

* Mon Aug  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-9m)
- update inputproto-1.4.4

* Wed Jul 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-8m)
- version up xcbproto-1.2

* Thu Jul  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.4-7m)
- version up randrproto-1.2.2

* Wed Jun 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.4-6m)
- version down xextproto-7.0.2

* Fri May 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.4-5m)
- version up xproto-7.0.13
- version up xextproto-7.0.3

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4-3m)
- add dri2proto-1.1
- update xf86driproto-2.0.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.4-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4-1m)
- update printproto-1.0.4, xproto-7.0.12
-        inputproto-1.4.3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-7m)
- %%NoSource -> NoSource

* Thu Nov  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-6m)
- updated xcb-proto-1.1

* Sat Oct 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-5m)
- updated glproto-1.4.9

* Mon Sep 24 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3-4m)
- updated renderproto-0.9.3-fix.patch

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3-3m)
- added renderproto-0.9.3-fix.patch

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.3-2m)
- update xproto-7.0.11

* Tue Sep 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-1m)
- update inputproto-1.4.2.1, renderproto-0.9.3, xf86dgaproto-2.0.3

* Wed Jul  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2-8m)
- update compositeproto-0.4

* Sat Mar 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.2-7m)
- version down inputproto-1.3.2

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.2-6m)
- version up inputproto-1.4.1

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2-5m)
- version up randrproto-1.2.1

* Sun Jan 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.2-4m)
- version down inputproto-1.3.2
- version down randrproto-1.1.2

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.2-3m)
- update damageproto-1.1.0
- update inputproto-1.4
- update randrproto-1.2.0
- update xproto-7.0.10

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2-2m)
- update xcb-proto-1.0

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2-1m)
- update xproto-7.0.9, glproto-1.4.8
- add    xcb-proto-0.9.93

* Sat Jun  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-3m)
- refind xproto-7.0.5

* Fri Jun  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-2m)
- update xproto-7.0.7

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-1m)
- version up X11R7.1
- update glproto-1.4.7

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-10m)
- update xproto-7.0.5

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-9m)
- update Xorg-7.1RC1
- compositeproto-0.3.1
- fixesproto-4.0
- scrnsaverproto-1.1.0

* Fri Mar 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-8m)
- update glproto-1.4.6. (remove NoSource)
- - need xgl.

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-7m)
- To trunk

* Thu Mar  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-6.1m)
- version down glproto-1.4.3
-- comment out patch

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-6m)
- Commentout Obsolete
- Commentout Requires: mesa-libGL-devel

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-5m)
- import to Momonga

* Wed Feb 22 2006 Jeremy Katz <katzj@redhat.com> - 7.0-5
- require mesa-libGL-devel since it's needed by some of the headers

* Sun Feb 19 2006 Ray Strode <rstrode@redhat.com> 7.0-4
- Add back part of glproto-texture-from-drawable patch that didn't
  get integrated for some reason

* Thu Feb 16 2006 Mike A. Harris <mharris@redhat.com> 7.0-3
- Update to glproto-1.4.4
- Drop glproto-texture-from-drawable patch, which is integrated now.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> 7.0-2.3
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 7.0-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Jan 27 2006 Kristian Hogsberg <krh@redhat.com> 7.0-2
- Add glproto-texture-from-drawable.patch to add opcodes and tokens
  for GLX_texture_from_drawable extension.

* Fri Dec 23 2005 Mike A. Harris <mharris@redhat.com> 7.0-1
- Update to damageproto-1.0.3, glproto-1.4.3, xf86driproto-2.0.2 from the
  X11R7.0 final release.
- Bump package version to 7.0 to match the X11R7 version, for no particularly
  strong reason other than "it feels good".

* Thu Dec 15 2005 Mike A. Harris <mharris@redhat.com> 0.99.4-1
- Update all proto tarballs to the RC4 release.

* Wed Dec  7 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Update to printproto-1.0.2, trapproto-3.4.2, xproto-7.0.3 from the
  X11R7 RC3 release.

* Mon Nov 21 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-1" to attempt to
  workaround bug( #173384).

* Thu Nov 17 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Change Conflicts to "Obsoletes: XFree86-devel, xorg-x11-devel"

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Update to X11R7 RC2 release, picking up new xproto-7.0.2.

* Thu Oct 20 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- This package contains only C header files and pkg-config *.pc files,
  and does not contain any ELF binaries or DSOs, so we disable debuginfo
  generation.

* Thu Oct 20 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Update all tarballs to X11R7 RC1 release.
- Remove panoramixproto, as it is now known as xineramaproto.
- Remove glu.h and glx.h from file manifest, as they're provided by Mesa.
- Added {_includedir}/GL/internal/dri_interface.h to file manifest.
#'

* Sun Oct  2 2005 Mike A. Harris <mharris@redhat.com> 0.0.1-3
- Use Fedora-Extras style BuildRoot
- Invoke make with _smp_mflags
- Add full URLs to SourceN lines

* Mon Sep 12 2005 Kristian Hogsberg <krh@redhat.com> 0.0.1-2
- Update to 20050912 cvs snapshot of kbproto.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.0.1-1
- Changed for loop in build section to use "ls -1" instead of find "*proto*"
  when going through protocol dirs, as "evieext" just *HAD* to be named
  something completely different from everything else, in true X.Org
  inconsistency fashion.
- Added Conflicts with XFree86-devel and xorg-x11-devel.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.0.1-0
- Initial build of xproto from X.Org modular CVS checkout and "make dist".
- Since there are no upstream tarballs yet, and "make dist" generates
  a "7.0" non-beta final version, I changed the version to 0.0 as I've no
  idea what the intention is and want to avoid using Epoch later.
