%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}


Name:		b43-tools
Version:	018
Release:	%{momorel}m%{?dist}
Summary:	Tools for the Broadcom 43xx series WLAN chip
Group:		System Environment/Base
# assembler — GPLv2
# debug — GPLv3
# disassembler — GPLv2
# ssb_sprom — GPLv2+
License:	GPLv2 and GPLv2+ and GPLv3
URL:		http://bu3sch.de/gitweb?p=b43-tools.git;a=summary
# git clone git://git.bu3sch.de/b43-tools.git
# cd b43-tools
# git-archive --format=tar --prefix=%{name}-%{version}/ b43-fwcutter-%{version} | bzip2 > ../%{name}-%{version}.tar.bz2
Source0:	%{name}-%{version}.tar.bz2
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	bison
BuildRequires:	flex
BuildRequires:	python-devel >= 2.7


%description
Tools for the Broadcom 43xx series WLAN chip.


%prep
%setup -q
install -p -m 0644 assembler/COPYING COPYING.assembler
install -p -m 0644 assembler/README README.assembler
install -p -m 0644 debug/COPYING COPYING.debug
install -p -m 0644 debug/README README.debug
install -p -m 0644 disassembler/COPYING COPYING.disassembler
install -p -m 0644 ssb_sprom/README README.ssb_sprom
install -p -m 0644 ssb_sprom/COPYING COPYING.ssb_sprom

%build
CFLAGS="%{optflags}" make %{?_smp_mflags} -C assembler
CFLAGS="%{optflags}" make %{?_smp_mflags} -C disassembler
CFLAGS="%{optflags}" make %{?_smp_mflags} -C ssb_sprom
cd debug && python install.py build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p %{buildroot}%{_bindir}
install -p -m 0755 assembler/b43-asm %{buildroot}%{_bindir}
install -p -m 0755 assembler/b43-asm.bin %{buildroot}%{_bindir}
install -p -m 0755 disassembler/b43-dasm %{buildroot}%{_bindir}
install -p -m 0755 disassembler/b43-ivaldump %{buildroot}%{_bindir}
install -p -m 0755 disassembler/brcm80211-fwconv %{buildroot}%{_bindir}
install -p -m 0755 disassembler/brcm80211-ivaldump %{buildroot}%{_bindir}
install -p -m 0755 ssb_sprom/ssb-sprom %{buildroot}%{_bindir}
cd debug && python install.py install --skip-build --root %{buildroot}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README.* COPYING.*
%{_bindir}/b43-asm
%{_bindir}/b43-asm.bin
%{_bindir}/b43-beautifier
%{_bindir}/b43-dasm
%{_bindir}/b43-fwdump
%{_bindir}/b43-ivaldump
%{_bindir}/brcm80211-fwconv
%{_bindir}/brcm80211-ivaldump
%{_bindir}/ssb-sprom
%{python_sitelib}/*


%changelog
* Mon Oct 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (018-1m)
- version 018

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.git20100419.5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.git20100419.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.git20100419.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.git20100419.2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0-0.git20100419.1m)
- import from Fedora
- update from git

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.4.git20090125
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue May 19 2009 Peter Lemenkov <lemenkov@gmail.com> 0-0.3.git20090125
- Corrected 'License' field
- Since now ssb_sprom honours optflags

* Sat Apr  4 2009 Peter Lemenkov <lemenkov@gmail.com> 0-0.2.git20090125
- Added missing BuildRequire

* Sat Mar 14 2009 Peter Lemenkov <lemenkov@gmail.com> 0-0.1.git20090125
- Initial package for Fedora

