%global momorel 4

Summary:        An arbitrary precision integer and multiple precision floatingpoint calculator
Name:           genius
Version:        1.0.12
Release:        %{momorel}m%{?dist}
Group:          Applications/Engineering
License:        GPL
URL:            http://www.jirka.org/genius.html
Source0:        ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.0/%{name}-%{version}.tar.bz2
NoSource:       0
Requires:       libgnomeui gtksourceview
Requires:       gmp ncurses readline
BuildRequires:  desktop-file-utils
BuildRequires:  vte028-devel >= 0.20.5
BuildRequires:  readline-devel
BuildRequires:  gmp-devel >= 5.0.0
BuildRequires:  mpfr-devel >= 3.0.0
BuildRequires:  popt
BuildRequires:  pkgconfig
BuildRequires:  intltool
BuildRequires:  gtksourceview-devel
BuildRequires:  libgnomeui-devel
BuildRequires:  desktop-file-utils
BuildRequires:  rarian
BuildRequires:  flex
BuildRequires:  gettext
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Genius is a general purpose calculator program similiar in some
aspects to BC, Matlab, Maple or Mathematica. It is useful both as a
simple calculator and as a research or educational tool. The syntax is
very intuitive and is designed to mimic how mathematics is usually
written. GEL is the name of its extention language, it stands for
Genius Extension Language, clever isn't it? In fact, many of the
standard genius functions are written in GEL itself.
#'

%package devel
Summary:        Development files for Genius
Group:          Applications/Engineering
Requires:       %{name} = %{version}-%{release}

%description devel
Development files for Genius.


%package -n gnome-genius
Summary:        GNOME frontend for Genius
Group:          Applications/Engineering
Requires:       %{name} = %{version}-%{release}
Requires(post): scrollkeeper
Requires(postun): scrollkeeper

%description -n gnome-genius
GNOME frontend for Genius.


%prep
#'
%setup -q
sed -i "s|Mime-Type|MimeType|" src/gnome-genius.desktop*
find -name \*.c | xargs chmod 0644

%build
%configure --disable-static --disable-update-mimedb --disable-scrollkeeper CFLAGS="-I%{_includedir}/ncurses"
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

desktop-file-install \
    --vendor "" \
    --add-category Science \
    --add-category Math \
    --remove-category Education \
    --remove-category Utility \
    --remove-category Office \
    --remove-category Scientific \
    --dir %{buildroot}%{_datadir}/applications \
    %{buildroot}%{_datadir}/applications/gnome-genius.desktop


rm -rf %{buildroot}%{_libdir}
rm -rf %{buildroot}%{_datadir}/application-registry
rm -rf %{buildroot}%{_datadir}/mime-info

%find_lang %{name}

%post -n gnome-genius
update-mime-database %{_datadir}/mime > /dev/null 2>&1 || :
update-desktop-database > /dev/null 2>&1 ||:
rarian-sk-update
gtk-update-icon-cache -qf /usr/share/icons/hicolor &> /dev/null || :


%postun -n gnome-genius
if [ $1 -eq 0 ]; then
  update-mime-database %{_datadir}/mime > /dev/null 2>&1 || :
  update-desktop-database > /dev/null 2>&1 ||:
  rarian-sk-update
  gtk-update-icon-cache -qf /usr/share/icons/hicolor &> /dev/null || :
fi

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc README INSTALL* ChangeLog AUTHORS COPYING NEWS TODO
%{_bindir}/genius
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/genius

%files -n gnome-genius
%defattr(-,root,root,-)
%{_bindir}/gnome-genius
%{_libexecdir}/*
%{_datadir}/icons/*/*/*/*
%{_datadir}/applications/*.desktop
%{_datadir}/mime/packages/*
%{_datadir}/gnome/help/genius
%{_datadir}/omf/genius

%changelog
* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-4m)
- fix BuildRequires; use vte028

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.12-1m)
- update 1.0.12

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-7m)
- rebuild against readline6

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-6m)
- fix build error

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-5m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-2m)
- rebuild against vte-0.20.4

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-5m)
- rebuild against rpm-4.6

* Fri Oct 31 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-4m)
- add configure --disable-update-mimedb --disable-scrollkeeper

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-3m)
- fix delete file name

* Tue Jul  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-2m)
- delete unpackaged files

* Mon Apr 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.2-1m)
- import to Momonga 

