%global         momorel 1

Name:           perl-Module-Pluggable
Version:        5.1
Release:        %{momorel}m%{?dist}
Epoch:          20
Summary:        Automatically give your module the ability to have plugins
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Module-Pluggable/
Source0:        http://www.cpan.org/authors/id/S/SI/SIMONW/Module-Pluggable-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Cwd >= 3.00
BuildRequires:  perl-if
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple >= 0.62
Requires:       perl-Cwd >= 3.00
Requires:       perl-if
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Provides a simple but, hopefully, extensible way of having 'plugins' for
your module. Obviously this isn't going to be the be all and end all of
solutions but it works for me.

%prep
%setup -q -n Module-Pluggable-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json README
%{perl_vendorlib}/Devel/InnerPackage.pm
%{perl_vendorlib}/Module/Pluggable.pm
%{perl_vendorlib}/Module/Pluggable
%{_mandir}/man3/*

%changelog
* Sun Jun 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:5.1-1m)
- perl-Module-Pluggable was removrd from perl core libraries
