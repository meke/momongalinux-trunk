%global momorel 11

Summary: X11 based presentation tool
Name: MagicPoint
Version: 1.13a
Release: %{momorel}m%{?dist}
License: BSD
# Copyright (C) 1997 and 1998 WIDE Project.  All rights reserved.
Group: Applications/Publishing
URL: http://member.wide.ad.jp/wg/mgp/
Source: ftp://sh.wide.ad.jp/WIDE/free-ware/mgp/magicpoint-%{version}.tar.gz
NoSource: 0
#Patch1: magicpoint-1.10a.xft2.patch
Patch2: magicpoint-1.09a.mgp2html.patch
Patch5: magicpoint-1.10a.imlib.patch
BuildRequires: freetype-devel, imlib-devel
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: fonts-japanese
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes:      mgp < %{version}-%{release}, magicpoint < %{version}-%{release}
Provides:       mgp = %{version}-%{release}, magicpoint = %{version}-%{release}

%description
MagicPoint is an X11 based presentation tool.  It is designed to make
simple presentations easy while to make complicated presentations
possible.  Its presentation file (whose suffix is typically .mgp) is
just text so that you can create presentation files quickly with your
favorite editor (e.g. Emacs).

%prep

%setup -q -n magicpoint-%{version}
%patch2 -p4 -b .mgp2html
%patch5 -p1 -b .imlib~

autoconf
%build
#./configure --with-vfontcap=/etc/vfontcap --with-x --enable-imlib
./configure --disable-freetype --disable-vflib --enable-imlib \
    --x-includes=%{_includedir} --x-libraries=%{_libdir}
xmkmf -a
make clean
make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}/usr/bin/
make DESTDIR="%{buildroot}" install
make DESTDIR="%{buildroot}" install.man
install -c -m 0755 contrib/mgp2latex.pl %{buildroot}%{_bindir}/mgp2latex
install -c -m 0755 contrib/mgp2html.pl %{buildroot}%{_bindir}/mgp2html
install -c -m 0755 contrib/tex2eps.sh %{buildroot}%{_bindir}/mgp-tex2eps
install -c -m 0755 contrib/eqn2eps.sh %{buildroot}%{_bindir}/mgp-eqn2eps

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc COPYRIGHT* FAQ README* RELNOTES SYNTAX TODO.jp USAGE* sample
%{_bindir}/mgp2latex
%{_bindir}/mgp2html
%{_bindir}/mgp-tex2eps
%{_bindir}/mgp-eqn2eps
%{_bindir}/mgp
%{_bindir}/mgp2ps
%{_bindir}/mgpembed
%{_bindir}/mgpnet
%{_bindir}/xwintoppm
%{_prefix}/lib/X11/mgp/
%{_mandir}/man1/*

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13a-11m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13a-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13a-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13a-8m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13a-7m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13a-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13a-5m)
- rebuild against libjpeg-7

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.13a-4m)
- rename MagicPoint

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13a-3m)
- rebuild against rpm-4.6

* Thu Apr 29 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- fix BuildPreReq

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13a-2m)
- rebuild against gcc43

* Thu Feb 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13a-1m)
- update to 1.13a

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12a-2m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Fri Feb  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12a-1m)
- update to 1.12a

* Tue Mar 28 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.11b-4m)
- %%{_libdir}/X11 -> %%{_prefix}/lib/

* Tue Mar 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11b-3m)
- change install direcroty (/usr/X11R6 -> /usr)

* Mon Feb 14 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11b-2m)
- disable freetype-1 and VFlib2

* Mon Sep 27 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11b-1m)
- update to 1.11b

* Wed Sep 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11a-1m)
- update to 1.11a
- delete included patch1

* Thu Oct  9 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.10a-2m)
- add magicpoint-1.10a.xft2.patch ([mgp-users-jp 01341] http://www.mew.org/ml/mgp-users-jp/msg01335.html)
- adapt the License: preamble for the Momonga Linux license expression unification policy (draft)

* Wed Jun 25 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.10a-1m)
- update to 1.10a

* Wed Feb 26 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.09a-12m)
- rebuild against VFlib2-2.25.6-1m
- change License to BSD
- move License's body to Copyright and comment out
- delete duplicate BuildRoot:

* Sun Aug 30 2002 Kenta MURATA <muraken2@nifty.com>
- (1.09a-11m)
- tex2eps and eqn2eps join to filelist.
- enable imlib.

* Sun Jun 23 2002 zunda <zunda@kondara.org>
- (1.09a-10k)
- patch from mgp-users-jp 00948 : contrib/mgp2html.pl.in
- patch from mgp-users-jp 00949 : mgpembed.pl.in
- patch from mgp-users-jp 00950 : print.c

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.09a-8k)
- rebuild against libpng 1.2.2.

* Sun Feb 17 2002 zunda <zunda@kondara.org>
- (1.09a-6k)
- magicpoint-1.09a.xsystem.patch for appropriate %xsystem window location

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (1.09a-4k)
- rebuild against libpng 1.2.0.

* Wed Oct 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.09a-2k)
- add BuildRoot tag

* Wed Aug  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- 1.0.7a-2k
- change from %patch2 -p1 to %patch -p0

* Wed Jul 26 2000 Kazuhiko <kazuhiko@kondara.org>
- 1.0.7a
- fix BuildPreReq (add freetype-devel)

* Tue Feb 8 2000 Toru Hoshina <t@kondara.org>
- freetype_charset16 seemd to be bad on gcc 2.95.2?

* Tue Feb 8 2000 Norihito Ohmori <nono@kondara.org>
- fix Reuiqres: and BuildPreReq:

* Sun Jan  30 2000 Takaaki Tabuchi <tab@kondara.org>
- add configure for sparc
- add -q flag at %setup

* Wed Dec  8 1999 Akira Higuchi <a@kondara.org>
- fixed the bug that the argument for CTL_SIZE was treated as of type int
- added BuildPreReq tag

* Mon Nov  8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Oct 11 1999 Toru Hoshina <t@kondara.org>
- rebuild for FreeType-1.3 with barterly patch :-P

* Wed Jul 30 1999 Norihito Ohmori <ohmori@flatout.org>
- update to version 1.06a

* Thu Jun 24 1999 Norihito Ohmori <ohmori@flatout.org>
- update to version 1.05a

* Fri Mar  5 1999 MATSUMOTO Shoji <vine@flatout.org>
- use VFlib (for mgp2ps)
- replace freetype&vflib libs dir patch to vine.patch (configure)

* Thu Feb  4 1999 ZUKERAN, shin <shin@ryukyu.ad.jp>
- add patch for freetype & vflib library's directoty (/usr/lib/lib).

* Tue Oct 27 1998 ZUKERAN, shin <zukkun@opus.or.jp>
- change package name to 'magicpoint-<version>.rpm'.
