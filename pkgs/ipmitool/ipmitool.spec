%global momorel 1


Name:         ipmitool
Summary:      Utility for IPMI control
Version:      1.8.13
Release:      %{momorel}m%{?dist}
License:      BSD
Group:        System Environment/Base
URL:          http://ipmitool.sourceforge.net/
Source0:      http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource:     0
Source1:      openipmi-ipmievd.sysconf
Source2:      ipmievd.service
Source3:      exchange-bmc-os-info.service
Source4:      exchange-bmc-os-info.sysconf
Source5:      set-bmc-url.sh
Source6:      exchange-bmc-os-info

Buildroot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0 readline-devel ncurses-devel
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Obsoletes: OpenIPMI-tools

Patch1: ipmitool-1.8.10-ipmievd-init.patch
Patch2: ipmitool-1.8.10-ipmievd-condrestart.patch
Patch3: ipmitool-1.8.11-remove-umask0.patch
# various threads. still pending.
Patch4: cxoem-jb-cx6.patch
# pending
#Patch5: ipmitool-1.8.12-fips.patch
# pending
#Patch6: ipmitool-1.8.12-fipsman.patch
# pending https://sourceforge.net/p/ipmitool/bugs/280/
Patch7: ipmitool-1.8.13-dualbridgedoc.patch

%description
This package contains a utility for interfacing with devices that support
the Intelligent Platform Management Interface specification.  IPMI is
an open standard for machine health, inventory, and remote power control.

This utility can communicate with IPMI-enabled devices through either a
kernel driver such as OpenIPMI or over the RMCP LAN protocol defined in
the IPMI specification.  IPMIv2 adds support for encrypted LAN
communications and remote Serial-over-LAN functionality.

It provides commands for reading the Sensor Data Repository (SDR) and
displaying sensor values, displaying the contents of the System Event
Log (SEL), printing Field Replaceable Unit (FRU) information, reading and
setting LAN configuration, and chassis power control.

%package -n bmc-snmp-proxy
Requires: net-snmp
Requires: exchange-bmc-os-info
Requires:OpenIPMI-modalias
BuildArch: noarch
Summary: Reconfigure SNMP to include host SNMP agent within BMC
%description -n bmc-snmp-proxy
Given a host with BMC, this package would extend system configuration
of net-snmp to include redirections to BMC based SNMP.


%package -n exchange-bmc-os-info
Requires: hostname
Requires: ipmitool OpenIPMI
Requires:OpenIPMI-modalias
BuildArch: noarch
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

Summary: Let OS and BMC exchange info

%description -n exchange-bmc-os-info
Given a host with BMC, this package would pass the hostname &
OS information to the BMC and also capture the BMC ip info
for the host OS to use.


%prep

%setup -q
%patch1 -p1 -b .ipmievd-init
%patch2 -p0 -b .condrestart
%patch3 -p1 -b .umask
%patch4 -p1 -b .cxoem
#patch5 -p0 -b .fips
#patch6 -p0 -b .fipsman
%patch7 -p1 -b .dualbridgedoc

for f in AUTHORS ChangeLog; do
    iconv -f iso-8859-1 -t utf8 < ${f} > ${f}.utf8
    mv ${f}.utf8 ${f}
done

%build
# --disable-dependency-tracking speeds up the build
# --enable-file-security adds some security checks
# --disable-intf-free disables FreeIPMI support - we don't want to depend on
#   FreeIPMI libraries, FreeIPMI has its own ipmitoool-like utility.

# begin: release auto-tools
# Used to be needed by aarch64 support, now only cxoem patch makefiles are left.
aclocal
libtoolize --automake --copy
autoheader
automake --foreign --add-missing --copy
aclocal
autoconf
automake --foreign
# end: release auto-tools

%configure --disable-dependency-tracking --enable-file-security --disable-intf-free
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'

install -Dpm 644 %{SOURCE2} %{buildroot}%{_unitdir}/ipmievd.service
install -Dpm 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/ipmievd
install -Dm 644 %{SOURCE3} %{buildroot}%{_unitdir}/exchange-bmc-os-info.service
install -Dm 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/exchange-bmc-os-info
install -Dm 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/profile.d/set-bmc-url.sh
install -Dm 755 %{SOURCE6} %{buildroot}%{_libexecdir}/exchange-bmc-os-info


install -Dm 644 contrib/bmc-snmp-proxy.sysconf %{buildroot}%{_sysconfdir}/sysconfig/bmc-snmp-proxy
install -Dm 644 contrib/bmc-snmp-proxy.service %{buildroot}%{_unitdir}/bmc-snmp-proxy.service
install -Dm 755 contrib/bmc-snmp-proxy         %{buildroot}%{_libexecdir}/bmc-snmp-proxy

%clean
rm -rf %{buildroot}

%post
%systemd_post ipmievd.service

%preun
%systemd_preun ipmievd.service

%postun
%systemd_postun_with_restart ipmievd.service

%post -n exchange-bmc-os-info
%systemd_post exchange-bmc-os-info.service

%preun -n exchange-bmc-os-info
%systemd_preun exchange-bmc-os-info.service

%postun -n exchange-bmc-os-info
%systemd_postun_with_restart exchange-bmc-os-info.service

%triggerun -- ipmievd < 1.8.11-10m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply ipmievd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save ipmievd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del ipmievd >/dev/null 2>&1 || :
/bin/systemctl try-restart ipmievd.service >/dev/null 2>&1 || :


%files
%config(noreplace) %{_sysconfdir}/sysconfig/ipmievd
%{_unitdir}/ipmievd.service
%{_bindir}/*
%{_sbindir}/*
%{_mandir}/man*/*
%doc %{_datadir}/doc/ipmitool
%{_datadir}/ipmitool

%files -n exchange-bmc-os-info
%config(noreplace) %{_sysconfdir}/sysconfig/exchange-bmc-os-info
%{_sysconfdir}/profile.d/set-bmc-url.sh
%{_unitdir}/exchange-bmc-os-info.service
%{_libexecdir}/exchange-bmc-os-info

%files -n bmc-snmp-proxy
%config(noreplace) %{_sysconfdir}/sysconfig/bmc-snmp-proxy
%{_unitdir}/bmc-snmp-proxy.service
%{_libexecdir}/bmc-snmp-proxy

%changelog
* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.13-1m)
- update 1.8.13

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.11-10m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.11-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.11-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.11-7m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.11-6m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.11-5m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.11-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.11-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.11-2m)
- add transform='s,x,x,'

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.11-1m)
- import from Fedora 11
- %%define __libtoolize :

* Thu Feb 26 2009 Jan Safranek <jsafrane@redhat.com> 1.8.11-1
- updated to new version

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.10-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> 1.8.10-3
- rebuild with new openssl

* Tue Oct 14 2008 Jan Safranek <jsafrane@redhat.com> 1.8.10-2
- fix issues found during package review:
  - clear Default-Start: line in the init script, the service should be 
    disabled by default
  - added Obsoletes: OpenIPMI-tools
  - compile with --disable-dependency-tracking to speed things up
  - compile with --enable-file-security
  - compile with --disable-intf-free, don't depend on FreeIPMI libraries
    (FreeIPMI has its own ipmitool-like utility)

* Mon Oct 13 2008 Jan Safranek <jsafrane@redhat.com> 1.8.10-1
- package created, based on upstream .spec file
