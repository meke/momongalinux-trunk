%global         momorel 23

Name:           perl-DateTime-Util-Calc
Version:        0.13002
Release:        %{momorel}m%{?dist}
Summary:        DateTime Calculation Utilities
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Util-Calc/
Source0:        http://www.cpan.org/authors/id/D/DM/DMAKI/DateTime-Util-Calc-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-test.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.1
BuildRequires:  perl-DateTime
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Math-BigInt
BuildRequires:  perl-Math-BigInt-FastCalc
BuildRequires:  perl-Math-BigInt-GMP
BuildRequires:  perl-Math-BigInt-Pari
BuildRequires:  perl-Math-Complex
BuildRequires:  perl-Test-Simple
Requires:       perl-DateTime
Requires:       perl-Math-BigInt
Requires:       perl-Math-BigInt-FastCalc
Requires:       perl-Math-BigInt-GMP
Requires:       perl-Math-BigInt-Pari
Requires:       perl-Math-Complex
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module contains some common calculation utilities that are required to
perform datetime calculations, specifically from "Calendrical Calculations"
-- they are NOT meant to be general purpose.

%prep
%setup -q -n DateTime-Util-Calc-%{version}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE
%{perl_vendorlib}/*/*/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-23m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-22m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-21m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-20m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-19m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-18m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-17m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-16m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-15m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13002-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13002-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13002-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-8m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-7m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13002-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-5m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13002-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13002-3m)
- rebuild against gcc43

* Mon Mar 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-2m)
- rebuild against perl-5.10.0-5m

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13002-1m)
- update to 0.13002

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.13-3m)
- use vendor

* Wed Feb  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-2m)
- add BuildRequires: perl-Math-Complex
- add Requires: perl-Math-Complex

* Wed Feb  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Tue Feb  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12
- add BuildRequires: perl-Math-Pari and perl-Math-FastCalc
- add Requires: perl-Math-Pari and perl-Math-FastCalc

* Wed Nov 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-2m)
- delete dupclicate directory

* Tue Nov 28 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
