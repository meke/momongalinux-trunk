%global momorel 32
%global emacsver %{_emacs_version}
%global realname css-mode
%global e_sitedir %{_emacs_sitelispdir}

Summary: CSS edit mode for Emacs
Name: emacs-%{realname}
Version: 0.11
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source0: http://www.garshol.priv.no/download/software/%{realname}/%{realname}.el 
NoSource: 0
Patch0: css-mode-custom.patch
URL: http://www.garshol.priv.no/download/software/%{realname}/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-c-comment-edit >= 1.02-22m
Requires: emacs >= %{emacsver}
Requires: emacs-c-comment-edit >= 1.02-22m
Obsoletes: css-mode-emacs

Obsoletes: elisp-css-mode
Provides: elisp-css-mode

%description
CSS edit mode for Emacs.

%prep
%{__rm} -rf %{buildroot}

%setup -qTc
%{__cp} %{S:0} .
%patch0 -p0 -b .custom

%build
emacs -q -batch -f batch-byte-compile %{realname}.el

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} -m 0755 %{buildroot}/%{e_sitedir}/%{realname}
%{__install} -m 0644 %{realname}.el %{buildroot}/%{e_sitedir}/%{realname}/%{realname}.el
%{__install} -m 0644 %{realname}.elc %{buildroot}/%{e_sitedir}/%{realname}/%{realname}.elc

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{e_sitedir}/%{realname}
%{e_sitedir}/%{realname}/%{realname}.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-32m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-31m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11-30m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-29m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-28m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-27m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-26m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-25m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-24m)
- merge css-mode-emacs to elisp-css-mode

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-23m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-22m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-21m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-20m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-19m)
- rebuild against emacs-23.0.94

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-18m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-17m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-16m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-15m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-14m)
- rebuild against emacs-22.3

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-13m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-11m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-10m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-2m)
- rebuild against emacs-22.0.90

* Sat Dec 24 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.11-1m)
- Initial specfile
