%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-camlidl
Version:        1.05
Release:        %{momorel}m%{?dist}
Summary:        Stub code generator and COM binding for Objective Caml

Group:          Development/Libraries
License:        "QPL" and "LGPLv2 with exceptions"
URL:            http://caml.inria.fr/pub/old_caml_site/camlidl/
Source0:        http://caml.inria.fr/pub/old_caml_site/distrib/bazar-ocaml/camlidl-%{version}.tar.gz
NoSource:       0
Source1:        http://caml.inria.fr/pub/old_caml_site/distrib/bazar-ocaml/camlidl-%{version}.doc.pdf
NoSource:       1
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-ocamldoc


%description
CamlIDL is a stub code generator and COM binding for Objective Caml.

CamlIDL comprises two parts:

* A stub code generator that generates the C stub code required for
  the Caml/C interface, based on an MIDL specification. (MIDL stands
  for Microsoft's Interface Description Language; it looks like C
  header files with some extra annotations, plus a notion of object
  interfaces that look like C++ classes without inheritance.)

* A (currently small) library of functions and tools to import COM
  components in Caml applications, and export Caml code as COM
  components.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n camlidl-%{version}
sed -e 's|^OCAMLLIB=.*|OCAMLLIB=%{_libdir}/ocaml|' \
    -e 's|^BINDIR=.*|BINDIR=%{_bindir}|' \
    < config/Makefile.unix \
    > config/Makefile
cp %{SOURCE1} .


%build
make all


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/%{_libdir}/ocaml
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/ocaml/caml
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/ocaml/stublibs
mkdir -p $RPM_BUILD_ROOT/%{_bindir}

make OCAMLLIB=$RPM_BUILD_ROOT/%{_libdir}/ocaml \
     BINDIR=$RPM_BUILD_ROOT/%{_bindir} \
     install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/*.*
%if %opt
%exclude %{_libdir}/ocaml/*.a
%exclude %{_libdir}/ocaml/*.cmxa
%endif
%{_bindir}/camlidl


%files devel
%defattr(-,root,root,-)
%doc LICENSE README Changes camlidl-%{version}.doc.pdf tests
%if %opt
%{_libdir}/ocaml/*.a
%{_libdir}/ocaml/*.cmxa
%endif
%{_libdir}/ocaml/caml/*.h


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-9m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.05-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.05-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.05-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.05-5
- Rebuild for OCaml 3.10.2.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.05-4
- Added tests subdirectory to the documentation.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.05-3
- Removed -doc subpackage and placed documentation in -devel.

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 1.05-2
- Rebuild for ppc64.

* Wed Feb 20 2008 Richard W.M. Jones <rjones@redhat.com> - 1.05-1
- Initial RPM release.
