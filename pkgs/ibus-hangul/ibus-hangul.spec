%global momorel 2

%global require_ibus_version 1.4.99.20121006
%global require_libhangul_version 0.1.0

Summary: The Hangul engine for IBus input platform
Name: ibus-hangul
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://code.google.com/p/ibus/
Group: System Environment/Libraries
Source0: http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext-devel
BuildRequires: ibus-devel >= %{require_ibus_version}
BuildRequires: libhangul-devel >= %{require_libhangul_version}
BuildRequires: libtool
BuildRequires: pkgconfig
Requires: ibus >= %{require_ibus_version}
Requires: libhangul >= %{require_libhangul_version}

%description
The Hangul engine for IBus platform. It provides Korean input method from
libhangul.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la file
rm -f %{buildroot}%{python_sitearch}/_hangul.la

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/ibus-setup-hangul
%{_libdir}/ibus-hangul/setup/hangul_keyboard_list
%{_libexecdir}/ibus-engine-hangul
%{_datadir}/applications/ibus-setup-hangul.desktop
%{_datadir}/ibus-hangul
%{_datadir}/ibus/component/*

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-2m)
- rebuild against ibus-1.4.99.20121006

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-4m)
- rebuildagainst libhangul-0.1.0

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-3m)
- rebuild for ibus-1.3.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0.20100329-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0.20100329-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0.20100329-1m)
- update 1.3.0.20100329-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090617-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090617-1m)
- update to 1.2.0.20090617

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.20090328.1m)
- version 1.1.0

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080819.1m)
- initial package for Momonga Linux
