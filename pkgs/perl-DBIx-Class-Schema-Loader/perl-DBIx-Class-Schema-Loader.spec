%global         momorel 1

Name:           perl-DBIx-Class-Schema-Loader
Version:        0.07040
Release:        %{momorel}m%{?dist}
Summary:        Dynamic definition of a DBIx::Class::Schema
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBIx-Class-Schema-Loader/
Source0:        http://www.cpan.org/authors/id/I/IL/ILMARI/DBIx-Class-Schema-Loader-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Carp-Clan
BuildRequires:  perl-Class-Accessor-Grouped >= 0.10008
BuildRequires:  perl-Class-C3-Componentised >= 1.0008
BuildRequires:  perl-Class-Inspector >= 1.27
BuildRequires:  perl-Class-Unload >= 0.07
BuildRequires:  perl-Config-Any
BuildRequires:  perl-Config-General
BuildRequires:  perl-Data-Dump >= 1.06
BuildRequires:  perl-DBD-SQLite >= 1.29
BuildRequires:  perl-DBIx-Class >= 0.08127
BuildRequires:  perl-DBIx-Class-IntrospectableM2M
BuildRequires:  perl-Digest-MD5 >= 2.36
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Path >= 2.07
BuildRequires:  perl-File-Temp >= 0.16
BuildRequires:  perl-Hash-Merge >= 0.12
BuildRequires:  perl-Lingua-EN-Inflect-Number >= 1.1
BuildRequires:  perl-Lingua-EN-Inflect-Phrase >= 0.15
BuildRequires:  perl-Lingua-EN-Tagger >= 0.23
BuildRequires:  perl-List-MoreUtils >= 0.32
BuildRequires:  perl-Moose >= 1.12
BuildRequires:  perl-MooseX-MarkAsMethods >= 0.13
BuildRequires:  perl-MooseX-NonMoose >= 0.16
BuildRequires:  perl-MRO-Compat >= 0.09
BuildRequires:  perl-namespace-autoclean >= 0.09
BuildRequires:  perl-namespace-clean >= 0.23
BuildRequires:  perl-Pod-Simple >= 3.22
BuildRequires:  perl-Scope-Guard >= 0.20
BuildRequires:  perl-String-CamelCase >= 0.02
BuildRequires:  perl-String-ToIdentifier-EN >= 0.05
BuildRequires:  perl-Sub-Name
BuildRequires:  perl-Test-Exception >= 0.31
BuildRequires:  perl-Test-Pod >= 1.14
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Test-Warn >= 0.21
BuildRequires:  perl-Try-Tiny
Requires:       perl-Carp-Clan
Requires:       perl-Class-Accessor-Grouped >= 0.10008
Requires:       perl-Class-C3-Componentised >= 1.0008
Requires:       perl-Class-Inspector >= 1.27
Requires:       perl-Class-Unload >= 0.07
Requires:       perl-Data-Dump >= 1.06
Requires:       perl-DBIx-Class >= 0.08127
Requires:       perl-Digest-MD5 >= 2.36
Requires:       perl-Hash-Merge >= 0.12
Requires:       perl-Lingua-EN-Inflect-Number >= 1.1
Requires:       perl-Lingua-EN-Inflect-Phrase >= 0.02
Requires:       perl-Lingua-EN-Tagger >= 0.23
Requires:       perl-List-MoreUtils >= 0.32
Requires:       perl-MRO-Compat >= 0.09
Requires:       perl-namespace-clean >= 0.23
Requires:       perl-Scope-Guard >= 0.20
Requires:       perl-String-CamelCase >= 0.02
Requires:       perl-String-ToIdentifier-EN >= 0.05
Requires:       perl-Sub-Name
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DBIx::Class::Schema::Loader automates the definition of a
DBIx::Class::Schema by scanning database table definitions and setting up
the columns, primary keys, and relationships.

%prep
%setup -q -n DBIx-Class-Schema-Loader-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(DBIx::Class::Schema::Loader::Utils)/d'

EOF
%define __perl_requires %{_builddir}/DBIx-Class-Schema-Loader-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test ||:
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/dbicdump
%{perl_vendorlib}/DBIx/Class/Schema/Loader
%{perl_vendorlib}/DBIx/Class/Schema/Loader.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07040-1m)
- rebuild against perl-5.20.0
- update to 0.07040

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07039-1m)
- update to 0.07039
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07038-1m)
- update to 0.07038

* Sun Nov 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07037-1m)
- update to 0.07037

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07036-2m)
- rebuild against perl-5.18.1

* Tue Jul  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07036-1m)
- update to 0.07036

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07035-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07035-2m)
- rebuild against perl-5.16.3

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07035-1m)
- update to 0.07035

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07033-2m)
- rebuild against perl-5.16.2

* Tue Sep 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07033-1m)
- update to 0.07033

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07031-1m)
- update to 0.07031

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07028-1m)
- update to 0.07028

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07027-1m)
- update to 0.07027

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07026-1m)
- update to 0.07026

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07025-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07025-1m)
- update to 0.07025
- rebuild against perl-5.16.0

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07020-1m)
- update to 0.07020

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07019-1m)
- update to 0.07019

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07018-1m)
- update to 0.07018

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07017-1m)
- update to 0.07017

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07015-1m)
- update to 0.07015

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07014-1m)
- update to 0.07014

* Fri Nov 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07013-1m)
- update to 0.07013

* Fri Nov 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07012-1m)
- update to 0.07012

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07011-1m)
- update to 0.07011

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07010-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07010-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07010-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.07010-2m)
- rebuild for new GCC 4.6

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07010-1m)
- update to 0.07010

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07009-2m)
- revise some BuildRequires and Requires

* Fri Feb 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07009-1m)
- update to 0.07009

* Fri Feb 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07008-1m)
- update to 0.07008

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07006-1m)
- update to 0.07006

* Thu Jan 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07005-1m)
- update to 0.07005

* Mon Jan 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07004-1m)
- update to 0.07004

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07003-1m)
- update to 0.07003

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.07002-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07002-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07002-1m)
- update to 0.07002

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.07001-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07001-1m)
- update to 0.07001

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07000-1m)
- update to 0.07000

* Fri Jun  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04006-7m)
- modify %%files to aviod conflicting with perl-DBIx-Class

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04006-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04006-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.04006-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04006-3m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.04006-2m)
- modify BuildRequires:  perl-DBD-MySQL

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04006-1m)
- update to 0.04006

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.04005-2m)
- rebuild against rpm-4.6

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04005-1m)
- update to 0.04005

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.04004-2m)
- rebuild against gcc43

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04004-1m)
- update to 0.04004

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04003-1m)
- update to 0.04003
- do not use %%NoSource macro

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04002-1m)
- update to 0.04002

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04001-1m)
- update to 0.04001

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03012-1m)
- update to 0.03012

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.03009-2m)
- use vendor

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03009-1m)
- update to 0.03009

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03008-1m)
- update to 0.03008

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.03007-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
