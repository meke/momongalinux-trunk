%global momorel 5

Summary: A pure ruby library for access to mp3 files (internal infos and tags)
Name: ruby-mp3info
Version: 0.6.13
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://rubyforge.org/projects/ruby-mp3info/
Source0: http://rubyforge.org/frs/download.php/57443/ruby-mp3info-%{version}.tgz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2

%description
This extension provides freedb functionality for Ruby.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q

%build
ruby install.rb config --prefix=%{_prefix} --site-ruby=%{ruby_libdir}
ruby install.rb setup

%install
[ -d %{buildroot} -a "%{buildroot}" != "" ] && rm -rf %{buildroot}
ruby install.rb install --prefix=%{buildroot}

%clean
[ -d %{buildroot} -a "%{buildroot}" != "" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc History.txt README.txt
%{ruby_libdir}/mp3info.rb
%{ruby_libdir}/mp3info/*

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.13-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.13-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.13-1m)
- update 0.6.13

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-2m)
- rebuild against gcc43

* Mon Jul 17 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5-1m)
- First release to momonga
