%global momorel 1
%global srcname httplib2
%global pythonver 2.7

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A Python HTTP library
Name: python-httplib2
Version: 0.6.0
Release: %{momorel}m%{?dist}
License: MIT/X
URL: http://bitworking.org/projects/httplib2/
Group: Development/Libraries
Source0: http://httplib2.googlecode.com/files/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
BuildRequires: python-devel >= %{pythonver}

%description
A comprehensive python HTTP client library, which supports many
features left out of other HTTP libraries.

%prep
%setup -q -n %{srcname}-%{version}

%build
CFLAGS="%{optflags}" python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install --prefix=%{_prefix} --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README
%{python_sitelib}/httplib2
%{python_sitelib}/%{srcname}*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-1m)
- update 0.6.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.0-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.2.0-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-2m)
- rebuild against gcc43

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-1m)
- initial package for libopensync-plugin-google-calendar
- Summary and %%description are imported from opensuse
