%global momorel 7

Summary: Reads and writes data across network connections using TCP or UDP.
Name: nc
Version: 1.84
Release: %{momorel}m%{?dist}
Source0: %{name}-%{version}.tar.bz2
Patch0: nc-1.84-glib.patch
Patch1: nc-1.78-pollhup.patch
Patch2: nc-1.82-reuseaddr.patch
Patch3: nc-gcc_signess.patch
License: GPL
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The nc package contains Netcat (the program is actually nc), a simple
utility for reading and writing data across network connections, using
the TCP or UDP protocols. Netcat is intended to be a reliable back-end
tool which can be used directly or easily driven by other programs and
scripts.  Netcat is also a feature-rich network debugging and
exploration tool, since it can create many different connections and
has many built-in capabilities.

You may want to install the netcat package if you are administering a
network and you'd like to use its debugging and network exploration
capabilities.

%prep
%setup -q -n nc
%patch0 -p1 -b .glib
%patch1 -p1 -b .pollhup
%patch2 -p1 -b .reuseaddr
%patch3 -p1 -b .gcc

%build
gcc $RPM_OPT_FLAGS -Werror `pkg-config --cflags --libs glib-2.0` netcat.c atomicio.c socks.c -o nc
# XXX Make linux is supported, but it makes a static binary. 
# make CFLAGS="$RPM_OPT_FLAGS" generic

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_bindir}
install -m 755 nc %{buildroot}%{_bindir}
install -d %{buildroot}%{_mandir}/man1
install -m 644 nc.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README scripts
%{_bindir}/nc
%{_mandir}/man1/nc.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.84-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.84-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.84-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.84-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.84-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.84-2m)
- rebuild against gcc43

* Wed May 31 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.84-1m)
- update to 1.84 (sync with FC)

* Wed Jun  9 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-5m)
- import patch from FC2

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.10-4k)
- s/Copyright/License/

* Thu Oct 18 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.10-2k)
- from rawhide nc-1.10-11.src.rpm

* Tue May 15 2001 Bill Nottingham <notting@redhat.com>
- add patch to fix timeouts (#40689, <jdp@ll.mit.edu>)

* Fri Oct 20 2000 Bill Nottingham <notting@redhat.com>
- include reslov.h for res_init prototype

* Fri Aug 11 2000 Jeff Johnson <jbj@redhat.com>
- add shutdown to fix obscure half-close connection case (#9324).

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jun 14 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- add man page

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Tue Jan 12 1999 Cristian Gafton <gafton@redhat.com>
- make it build on the arm

* Tue Dec 29 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0
