%global momorel 3
# From configure.ac OCTAVE_API_VERSION_NUMBER
%global octave_api_num 48
%global octave_api api-v%{octave_api_num}+

Name:           octave
Version:        3.6.4
Release: %{momorel}m%{?dist}
Summary:        A high-level language for numerical computations
Group:          Applications/Engineering
License:        GPLv3+
Source0:        ftp://ftp.gnu.org/gnu/octave/octave-%{version}.tar.bz2
NoSource:	0
Source1:        macros.octave
# Fix building packages from directories
Patch2:         octave-3.4.0-pkgbuilddir.patch

URL:            http://www.octave.org
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Provides:       octave(api) = %{octave_api}

### include local configuration
%{?include_specopt}
### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpm/specopt/octave.specopt and edit it.
%{?!do_test: %global do_test 0}

BuildRequires:  bison 
BuildRequires:  flex 
BuildRequires:  less 
BuildRequires:  tetex 
BuildRequires:  gcc-gfortran 
BuildRequires:  atlas-devel 
BuildRequires:  ncurses-devel 
BuildRequires:  zlib-devel 
BuildRequires:  hdf5-devel >= 1.8.12
BuildRequires:  texinfo 
BuildRequires:  qhull-devel
BuildRequires:  readline-devel 
BuildRequires:  glibc-devel 
BuildRequires:  fftw-devel 
BuildRequires:  gperf 
BuildRequires:  ghostscript
BuildRequires:  curl-devel 
BuildRequires:  pcre-devel >= 8.31
BuildRequires:  texinfo-tex 
BuildRequires:  arpack-devel 
BuildRequires:  libX11-devel
BuildRequires:  suitesparse-devel 
BuildRequires:  glpk-devel >= 4.51
BuildRequires:  gnuplot 
BuildRequires:  desktop-file-utils
BuildRequires:  GraphicsMagick-c++-devel 
BuildRequires:  fltk-devel  >= 1.3.2
BuildRequires:  ftgl-devel qrupdate-devel
BuildRequires:  texlive-dvipsk

Requires:        gnuplot gnuplot-common less info texinfo 
Requires(post):  info
Requires(preun): info

%description
GNU Octave is a high-level language, primarily intended for numerical
computations. It provides a convenient command line interface for
solving linear and nonlinear problems numerically, and for performing
other numerical experiments using a language that is mostly compatible
with Matlab. It may also be used as a batch-oriented language. Octave
has extensive tools for solving common numerical linear algebra
problems, finding the roots of nonlinear equations, integrating
ordinary functions, manipulating polynomials, and integrating ordinary
differential and differential-algebraic equations. It is easily
extensible and customizable via user-defined functions written in
Octave's own language, or using dynamically loaded modules written in
C++, C, Fortran, or other languages.


%package devel
Summary:        Development headers and files for Octave
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       readline-devel fftw-devel hdf5-devel zlib-devel
Requires:       atlas-devel gcc-c++ gcc-gfortran

%description devel
The octave-devel package contains files needed for developing
applications which use GNU Octave.


%package doc
Summary:        Documentation for Octave
Group:          Documentation
BuildArch:      noarch

%description doc
This package contains documentation for Octave.

%prep
%setup -q
%patch2 -p1 -b .pkgbuilddir

# Check that octave_api is set correctly
if ! grep -q '^OCTAVE_API_VERSION_NUMBER="%{octave_api_num}"' configure.ac
then
  echo "octave_api variable in spec does not match configure.ac"
  exit 1
fi

# Check permissions
find -name *.cc -exec chmod 644 {} \;

%build
%global enable64 no
export F77=gfortran
# TODO: arpack (and others?) appear to be bundled in libcruft.. 
#   --with-arpack is not an option anymore
#   gl2ps.c is bundled.  Anything else?
%configure --enable-shared --disable-static --enable-64=%enable64 \
 --with-blas="-L%{_libdir}/atlas -lf77blas -latlas" --with-qrupdate \
 --with-lapack="-L%{_libdir}/atlas -llapack" \
 --with-amd --with-umfpack --with-colamd --with-ccolamd --with-cholmod \
 --with-cxsparse

make OCTAVE_RELEASE="Momonga %{version}-%{release}"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_infodir}/dir

# Make library links
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/octave/%{version}" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/octave-%{_arch}.conf

# Remove RPM_BUILD_ROOT from ls-R files
perl -pi -e "s,%{buildroot},," %{buildroot}%{_libdir}/%{name}/ls-R
perl -pi -e "s,%{buildroot},," %{buildroot}%{_datadir}/%{name}/ls-R
# Make sure ls-R exists
touch %{buildroot}%{_datadir}/%{name}/ls-R

# Create desktop file
rm %{buildroot}%{_datadir}/applications/www.octave.org-octave.desktop
desktop-file-install --vendor= --remove-category Development --add-category "Education" \
  --add-category "DataVisualization" --add-category "NumericalAnalysis" --add-category "Engineering" --add-category "Physics" \
  --dir %{buildroot}%{_datadir}/applications doc/icons/octave.desktop

# Create directories for add-on packages
HOST_TYPE=`%{buildroot}%{_bindir}/octave-config -p CANONICAL_HOST_TYPE`
mkdir -p %{buildroot}%{_libdir}/%{name}/site/oct/%{octave_api}/$HOST_TYPE
mkdir -p %{buildroot}%{_libdir}/%{name}/site/oct/$HOST_TYPE
mkdir -p %{buildroot}%{_datadir}/%{name}/packages
mkdir -p %{buildroot}%{_libdir}/%{name}/packages
touch %{buildroot}%{_datadir}/%{name}/octave_packages

# work-around broken pre-linking (bug 524493)
#install -d %{buildroot}%{_sysconfdir}/prelink.conf.d
#echo "-b %{_bindir}/octave-%{version}" > %{buildroot}%{_sysconfdir}/prelink.conf.d/octave.conf

# Fix multilib installs
for include in config defaults oct-conf
do
   mv %{buildroot}%{_includedir}/%{name}-%{version}/%{name}/${include}.h \
      %{buildroot}%{_includedir}/%{name}-%{version}/%{name}/${include}-%{__isa_bits}.h
   cat > %{buildroot}%{_includedir}/%{name}-%{version}/%{name}/${include}.h <<EOF
#if ! defined __OCTAVE_${include}_H_INCLUDED__
# define __OCTAVE_${include}_H_INCLUDED__
# include <bits/wordsize.h>

# if __WORDSIZE == 32
#  include "${include}-32.h"
# elif __WORDSIZE == 64
#  include "${include}-64.h"
# else
#  error "Unknown word size"
# endif

#endif
EOF
done
#for script in octave-config mkoctfile
#do
#  mv %{buildroot}%{_bindir}/${script} %{buildroot}%{_bindir}/${script}-%{version}
#  ln -fs ${script}-%{version} %{buildroot}%{_bindir}/${script}
#done
for script in octave-config-%{version} mkoctfile-%{version}
do
   mv %{buildroot}%{_bindir}/${script} %{buildroot}%{_libdir}/%{name}/%{version}/${script}
   cat > %{buildroot}%{_bindir}/${script} <<EOF
#!/bin/bash
ARCH=\$(uname -m)

case \$ARCH in
x86_64 | ia64 | s390x) LIB_DIR=/usr/lib64
                       SECONDARY_LIB_DIR=/usr/lib
                       ;;
* )
                       LIB_DIR=/usr/lib
                       SECONDARY_LIB_DIR=/usr/lib64
                       ;;
esac

if [ ! -x \$LIB_DIR/%{name}/%{version}/${script} ] ; then
  if [ ! -x \$SECONDARY_LIB_DIR/%{name}/%{version}/${script} ] ; then
    echo "Error: \$LIB_DIR/%{name}/%{version}/${script} not found"
    if [ -d \$SECONDARY_LIB_DIR ] ; then
      echo "   and \$SECONDARY_LIB_DIR/%{name}/%{version}/${script} not found"
    fi
    exit 1
  fi
  LIB_DIR=\$SECONDARY_LIB_DIR
fi
exec \$LIB_DIR/%{name}/%{version}/${script} "\$@"
EOF
   chmod +x %{buildroot}%{_bindir}/${script}
done
# remove timestamp from doc-cache
sed -i -e '/^# Created by Octave/d' %{buildroot}%{_datadir}/%{name}/%{version}/etc/doc-cache

# rpm macros
mkdir -p %{buildroot}%{_sysconfdir}/rpm
cp -p %SOURCE1 %{buildroot}%{_sysconfdir}/rpm/


%if %{do_test}
%check
# TODO - Fix this:
#  src/DLD-FUNCTIONS/md5sum.cc ............................*** stack smashing detected ***: /builddir/build/BUILD/octave-3.3.54/src/.libs/lt-octave terminated
make check || :
%endif


%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
/sbin/install-info --info-dir=%{_infodir} --section="Programming" \
        %{_infodir}/octave.info || :

%preun
if [ "$1" = "0" ]; then
   /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/octave.info || :
fi

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS ChangeLog* COPYING NEWS* README
# FIXME: Create an -emacs package that has the emacs addon
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/octave-*.conf
%{_sysconfdir}/rpm/macros.octave
%{_bindir}/octave*
%{_libdir}/octave/
%{_libexecdir}/octave/
%{_mandir}/man1/octave*.1.*
%{_infodir}/liboctave.info*
%{_infodir}/octave.info*
%{_infodir}/OctaveFAQ.info*
%{_datadir}/applications/octave.desktop
# octave_packages is %ghost, so need to list everything else separately
%dir %{_datadir}/octave
%{_datadir}/octave/%{version}/
%{_datadir}/octave/ls-R
%ghost %{_datadir}/octave/octave_packages
%{_datadir}/octave/packages/
%{_datadir}/octave/site/
#%{_sysconfdir}/prelink.conf.d/octave.conf

%files devel
%defattr(-,root,root,-)
%{_bindir}/mkoctfile
%{_bindir}/mkoctfile-%{version}
%{_includedir}/octave-%{version}/
%{_mandir}/man1/mkoctfile.1.*

%files doc
%defattr(-,root,root,-)
%doc doc/liboctave/liboctave.html doc/liboctave/liboctave.pdf
%doc doc/faq/OctaveFAQ.pdf doc/refcard/*.pdf
%doc examples/


%changelog
* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-3m)
- rebuild against hdf5-1.8.12

* Sun Jun 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-2m)
- rebuild against glpk-4.51

* Sun May  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-1m)
- update to 3.6.4
- rebuild against glpk-4.48

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.3-1m)
- update to 3.6.3

* Sun Dec 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.2-3m)
- rebuild against fltk-1.3.2

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.2-2m)
- rebuild against pcre-8.31

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1
- removed 
   Patch3: octave-3.4.2-tar-fix.patch
   Patch4: octave-3.4.2-curl.patch

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.2-3m)
- rebuild against hdf5-1.8.8

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-2m)
- fix build failure; add patch for curl 7.21.7

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-1m)
- update 3.4.2
-- fix some minor installation bugs
- update /etc/rpm/macro.octave
- add patch to fix tar.m

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-1m)
- update 3.4.1
- merge with fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-13m)
- rebuild for new GCC 4.6

* Tue Feb  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-12m)
- fix release in changelog

* Fri Feb  4 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-11m)
- add patch for GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-10m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-9m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.4-8m)
- full rebuild for mo7 release

* Thu Jun  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-7m)
- add Requires

* Wed May  5 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-6m)
- fix imread.m crash bug

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.4-5m)
- rebuild against readline6

* Fri Apr 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-4m)
- add BuildRequires

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.4-3m)
- use Requires

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-2m)
- sync with Fedora devel

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3
- fix %%post and %%postun error in octave-forge

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.5-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.5-2m)
- add Provides: octave(api)

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-5m)
- rebuild against rpm-4.6

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-4m)
- rebuild against hdf5-1.8.2

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-3m)
- rebuild against ImageMagick-6.4.2.1

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-2m)
- install icons for qtoctave

* Wed Apr 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-1m)
- update 3.0.1

* Mon Apr 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.0-5m)
- rebuild against hdf5-1.8.0

* Mon Apr 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.0-4m)
- add octave-3.0.0-gcc43.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.0-2m)
- %%NoSource -> NoSource

* Tue Dec 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.0-1m)
- update 3.0.0

* Mon Nov 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.12-2m)
- added a patch for gcc43
- added %%check section

* Thu May 31 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.9.12-1m)
- update to 2.9.12

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.9.9-1m)
- update to octave-2.9.9/octave-forge-2006.07.09
- sync with octave-2.9.9-1.src.rpm in Fedora Extra
- (changelog in Fedora)
- * Mon Oct  3 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.9-1
- - New release. Remove old patch.
- 
- * Fri Sep 15 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.8-2
- - Fix this bug:
-   https://www.cae.wisc.edu/pipermail/bug-octave/2006-September/000687.html
- 
- * Fri Aug 25 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.8-1
- - New release. Remove old patch. This fixes bug #203676.
- 
- * Tue Aug 15 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.7-3
- - Add ghostscript as a build dependency.
- 
- * Tue Aug 15 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.7-2
- - Update patch to fix several small bugs, including #201087.
- 
- * Fri Jul 28 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.7-1
- - New release. Remove old patches and add one new one.
- 
- * Tue Jul 11 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.6-2
- - Patch for some erroneous warnings and a file path bug.
- 
- * Mon Jul 10 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.6-1
- - New release. Remove old patches.
- - Disable 64-bit extensions (some libraries don't support 64-bit indexing yet).
- - Add gcc-gfortran to -devel dependencies (mkoctfile fails without it).
- - Move octave-bug and octave-config from devel to main package.
- - Fix categorization of info files (bug 196760).
- 
- * Wed Apr 27 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.5-6
- - Add patch for bug #190481
- - Manual stripping of .oct files is no longer necessary.
- 
- * Wed Apr 19 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.5-5
- - Add new patch to configure script (breaks octave-forge without it).
- 
- * Fri Mar 24 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.5-4
- - Change patch again (suggested by the author on Octave mailing list).
- 
- * Fri Mar 24 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.5-3
- - Fix broken patch.
- 
- * Fri Mar 24 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.5-2
- - Add more changes to sparse patch.
- 
- * Thu Mar 23 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.5-1
- - New upstream release; remove old patches; add sparse patch.
- - Add gcc-c++ as dependency for devel package.
- - Add more docs; cleanup extra files in docs.
- - Simplify configure command.
- - Install desktop file.
- 
- * Fri Feb 24 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-8
- - Rebuild for new hdf5.
- - Remove obsolete configure options.
- - Make sure /usr/libexec/octave is owned by octave.
- 
- * Wed Feb 15 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-7
- - Rebuild for Fedora Extras 5.
- 
- * Wed Feb  1 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-6
- - Change dependency from fftw3 to fftw.
- 
- * Thu Jan 26 2006 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-5
- - Rebuild for new release of hdf5.
- 
- * Mon Dec 19 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-4
- - Rebuild for gcc 4.1.
- 
- * Thu Dec  1 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-3
- - Make sure patch applies correctly before building!
- 
- * Thu Dec  1 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-2
- - Patch to enable compilation on x86_64.
- 
- * Fri Nov 11 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.4-1
- - New upstream release.
- - Patch to make sure all headers are included in -devel.
- - PKG_ADD file now needs $RPM_BUILD_ROOT stripped from it.
- - Cleanup errors in dependencies.
- 
- * Tue Oct 25 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-6
- - Add lapack-devel and blas-devel dependencies to devel package.
- 
- * Mon Oct 03 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-5
- - Change umfpack-devel dependency to the new ufsparse-devel package.
- 
- * Thu Sep 22 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-5
- - Change lapack and blas dependencies to lapack-devel and blas-devel
- 
- * Mon Aug 08 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-4
- - Cleanup: remove redefinition of __libtoolize, ExcludeArch of two platforms,
-   old s390 workarounds, and LC_ALL setting. None of these appear to be
-   necessary any longer, even if the platforms were supported.
- - Add --enable-64 to configure to enable 64-bit array indexing on x86_64.
- - Add support for GLPK (new build dependency and CPPFLAGS for configure).
- 
- * Wed Jul 27 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-3
- - Add fftw3-devel to dependencies for devel
- 
- * Tue Jul 26 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-2
- - Add dependencies (hdf5-devel and zlib-devel) for devel
- 
- * Tue Jul 26 2005 Quentin Spencer <qspencer@users.sourceforge.net> 2.9.3-1
- - Move to new 2.9.x development tree.
- - Add umfpack-devel as new build dependency.

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.73-4m)
- rebuild against readline-5.0

* Wed May 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.73-3m)
- enable hdf5 support

* Thu Apr 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.73-2m)
- Added gcc41 patch

* Thu Apr 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.73-1m)
- update to 2.1.73

* Sun Feb 19 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.72-2m)
- rebuild against gcc-4.1

* Thu Jan 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.1.72-1m)
- update to 2.1.72
- gcc-4.1 build fix

* Thu Jun 30 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.71-1m)
- update to 2.1.71
- import octave-2.1.71-save.patch from FC
- split devel package

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.57-4m)
- rebuild against gcc-c++-3.4.1

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.57-3m)
- remove Epoch

* Sun Jun 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.57-2m)
- no nosource0

* Wed Jun 09 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.57-1m)
- update to 2.1.57
- sync with FC2 package

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.36-7m)
- revised spec for rpm 4.2.

* Mon Jul  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.36-6m)
- rebuild against rpm-4.0.4-52m

* Tue Feb 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.36-5m)
- change URI of acx_{blas,lapack}.m4 (Source1,2) to
  http://ac-archive.sourceforge.net/Installed_Packages/acx_{blas,lapack}.m4
  reported by Imuta Takeshi <imuchin777--at--m3.dion.ne.jp>([Momonga-devel.ja:01372]), thakns!

* Sun Dec  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.36-4m)
- use autoconf-2.53

* Tue Dec 03 2002 Kenta MURATA <muraken@momonga-linux.org>
- (2.1.36-3m)
- BuildPreReq: readline-devel >= 4.2

* Tue Nov 19 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (2.1.36-2m)
- require pager instead of less.

* Tue Oct  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
-(2.1.36-1m)
- update to 2.1.36
- sync with redhat
  add octave-2.1.36-gcc31.patch
  add -fno-use-cxa-atexit to CXXFLAGS
  get rid of 0 size doc files
- stop 'strip'

* Wed Jun 20 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add more dependencies in BuildPrereq (#45184)

* Fri Jun 08 2001 Trond Eivind Glomsrod <teg@redhat.com>
- No longer exclude ia64

* Mon Apr 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.34

* Tue Mar 27 2001 Trond Eivind Glomsrod <teg@redhat.com>
- set LC_ALL to POSIX before building, otherwise the generated paths.h is bad

* Wed Jan 10 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.33

* Mon Jan 08 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- do not require compat-egcs-c++, but gcc-c++
- add some libtoolize calls to add newest versions

* Fri Dec 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.32, no longer use CVS as our needed fixes are in now
- add Prereq for info

* Thu Dec 07 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use a development version, as they have now been fixed
  to compile with the our current toolchain.

* Thu Aug 24 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.0.16, with compat C++ compiler and new C and f77 compilers
  The C++ code is too broken for our new toolchain (C++ reserved
  words used as enums and function names, arcane macros), but
  plotting works here and not in the beta (#16759)
- add epoch to upgrade the betas

* Tue Jul 25 2000 Jakub Jelinek <jakub@redhat.com>
- make sure #line commands are not output within macro arguments

* Wed Jul 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.31

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- no longer disable optimizations, sparc excepted

* Tue Jul  4 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Mon Jul  3 2000 Matt Wilson <msw@redhat.com>
- added missing %% before {_infodir} in the %%post 

* Sat Jun 09 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.30 - the old version contains invalid C++ code
  accepted by older compilers.

* Sat Jun 09 2000 Trond Eivind Glomsrod <teg@redhat.com>
- disable optimization for C++ code

* Fri Jun 08 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add "Excludearch: " for Alpha - it triggers compiler bugs

* Fri Jun 08 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%configure, %%makeinstall, %{_infodir}. %{_mandir}
- remove prefix

* Tue May 09 2000 Trond Eivind Glomsrod <teg@redhat.com>
- upgraded to 2.0.16
- removed "--enable-g77" from the configure flags - let autoconf find it

* Thu Jan 20 2000 Tim Powers <timp@redhat.com>
- bzipped source to conserve space.

* Thu Jan 13 2000 Jeff Johnson <jbj@redhat.com>
- update to 2.0.15.

* Tue Jul 20 1999 Tim Powers <timp@redhat.com>
- rebuit for 6.1

* Wed Apr 28 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.0.14.

* Fri Oct 23 1998 Jeff Johnson <jbj@redhat.com>
- update to 2.0.13.90

* Thu Jul  9 1998 Jeff Johnson <jbj@redhat.com>
- repackage in powertools.

* Thu Jun 11 1998 Andrew Veliath <andrewtv@usa.net>
- Add %attr, build as user.

* Mon Jun 1 1998 Andrew Veliath <andrewtv@usa.net>
- Add BuildRoot, installinfo, require gnuplot, description from
  Octave's web page, update to Octave 2.0.13.
- Adapt from existing spec file.

* Tue Dec  2 1997 Otto Hammersmith <otto@redhat.com>
- removed libreadline stuff from the file list

* Mon Nov 24 1997 Otto Hammersmith <otto@redhat.com>
- changed configure command to put things in $RPM_ARCH-rehat-linux, 
  rather than genereated one... was causing problems between building 
  on i686 build machine.

* Mon Nov 17 1997 Otto Hammersmith <otto@redhat.com>
- moved buildroot from /tmp to /var/tmp

* Mon Sep 22 1997 Mike Wangsmo <wanger@redhat.com>
- Upgraded to version 2.0.9 and built for glibc system

* Thu May 01 1997 Michael Fulbright <msf@redhat.com>
- Updated to version 2.0.5 and changed to build using a BuildRoot
