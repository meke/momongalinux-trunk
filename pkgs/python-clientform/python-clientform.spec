%global momorel 5
%global srcname ClientForm
%global pythonver 2.7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Python module for client-side HTML forms
Name: python-clientform
Version: 0.2.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Development/Languages
URL: http://wwwsearch.sourceforge.net/ClientForm/
Source0: http://wwwsearch.sourceforge.net/%{srcname}/src/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
BuildRequires: coreutils
BuildRequires: python-devel >= %{pythonver}
BuildRequires: python-setuptools
BuildArch: noarch 

%description
ClientForm is a Python module for handling HTML forms on the client side,
useful for parsing HTML forms, filling them in and returning the completed
forms to the server.  It developed from a port of Gisle Aas' Perl module
HTML::Form, from the libwww-perl library, but the interface is not the same.

%prep
%setup -q -n %{srcname}-%{version}

%build
python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install -O1 --skip-build --single-version-externally-managed --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING.txt COPYRIGHT.txt ChangeLog.txt GeneralFAQ.html 
%doc INSTALL.txt README.html README.txt examples
%{python_sitelib}/%{srcname}-%{version}-py?.?.egg-info
%{python_sitelib}/%{srcname}.py*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.7-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.7-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.7-1m)
- initial package for python-mechanize
