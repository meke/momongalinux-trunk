;;; for Gnus (~/.gnus)                                 -*- emacs-lisp -*-
; -----------------------------------------------------------------------

(add-hook 'gnus-message-setup-hook
          (lambda ()
            (define-key (current-local-map) "\C-x4x"
              'select-xface)))
(add-hook 'gnus-message-setup-hook 'x-face-insert))
(eval-after-load "gnus-sum"
  '(define-key gnus-summary-mode-map "\C-x4s" 'x-face-save))
(define-key message-mode-map "\C-x4i" 'x-face-insert)
(define-key message-mode-map "\M-t" 'x-face-show)
