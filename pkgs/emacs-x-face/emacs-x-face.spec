%global momorel 36
%global emacsver %{_emacs_version}
%global selectver 0.15
%global e_sitedir %{_emacs_sitelispdir}

Summary: X-Face Utilities for Emacs
Name: emacs-x-face
Version: 1.3.6.24
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: ftp://ftp.jpl.org/pub/elisp/x-face-%{version}.tar.gz
Source1: ftp://ftp.jpl.org/pub/elisp/select-xface-%{selectver}.tar.gz
#
Source2: ftp://ftp.jpl.org/pub/elisp/x-face-e21.el.gz
# size and time stamp "21930 Mar  6 01:02"
#
Source3: ftp://ftp.jpl.org/pub/elisp/make-gray-x-face.el
# size and time stamp "3888 Mar  7  2002"
#
Source10: dot.emacs
Source12: dot.mew
Source13: dot.gnus
Source14: dot.wl
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires: compface
Requires: netpbm-progs
Obsoletes: x-face-emacs
Obsoletes: x-face-xemacs

Obsoletes: elisp-x-face
Provides: elisp-x-face

%description
X-Face utilities for Emacs.

%prep
%setup -q -c -a 1

%build
rm -rf %{buildroot}

cd x-face-%{version}

mkdir -p %{buildroot}%{e_sitedir}
(cd %{buildroot}%{e_sitedir}
install -m644 %{SOURCE2} .
gunzip x-face-e21.el.gz
emacs -q -batch -f batch-byte-compile x-face-e21.el
)

cd ../select-xface-%{selectver}
cp %{SOURCE3} .
emacs -q -no-site-file -batch -f batch-byte-compile \
  select-xface.el make-gray-x-face.el
install -m 644 select-xface.el* make-gray-x-face.el* \
  %{buildroot}%{e_sitedir}/

%install
rm -f x-face-%{version}/{Makefile,eval.el.in,*.el*}
rm -f select-xface-%{selectver}/*.el*

mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -c -m 644 %{SOURCE10} %{SOURCE12} %{SOURCE13} %{SOURCE14} \
    %{buildroot}%{_datadir}/config-sample/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc x-face-%{version}
%doc select-xface-%{selectver}
%{_datadir}/config-sample/%{name}
%{e_sitedir}/*.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.6.24-36m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.6.24-35m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.6.24-34m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.6.24-33m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-32m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.6.24-31m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.6.24-30m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-29m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-28m)
- merge x-face-emacs to elisp-x-face
- kill x-face-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-27m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-26m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-25m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-24m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-23m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-22m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-21m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-20m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-19m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-18m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-17m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-16m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-15m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-14m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-13m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-12m)
- rebuild against emacs-22.0.96

* Sun Mar 11 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-11m)
- add Requires: netpbm-progs

* Sat Mar 10 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6.24-10m)
- be free
- change Requires from faces* to compface
  in %%package -n {x-face-emacs,x-face-xemacs} sections
- update x-face-e21-version to 0.129

* Sun Mar 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-9m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-8m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-7m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-6m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-5m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-4m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.6.24-3m)
- change Source2 md5sum.
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.6.24-2m)
- rebuild against emacs-21.3.50

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6.24-1m)
- update to 1.3.6.24, this version include security fix at 1.3.6.23
  reported by Tatsuya Kinoshita [Momonga-devel.ja:02291]
- update x-face-e21.el.gz

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6-20-10m)
- rebuild against emacs-21.3

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6-20-9m)
- add BuildPreReq: xemacs-sumo

* Fri Aug 30 2002 Hidetomo Machi <mcht@momonga-linux.org>
- (1.3.6-20-8m)
- update x-face-e21 (version 0.118)

* Mon Apr  1 2002 Junichiro Kita <kita@kondara.org>
- (1.3.6.20-6k)
- update dot.wl

* Tue Mar 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.3.6.20-4k)
- rebuild against emacs-21.2

* Sun Mar 17 2002 Toru Hoshina <t@kondara.org>
- (1.3.6.20-2k)

* Sun Mar 10 2002 Hidetomo Machi <mcHT@kondara.org>
- (1.3.6.19-4k)
- update x-face-e21.el (Revised: 2002/03/07)
- add make-gray-x-face.el

* Tue Feb 26 2002 Hidetomo Machi <mcHT@kondara.org>
- (1.3.6.19-2k)
- update 1.3.6.19
- update select-xface-0.15
- add `faces-xface' to Requires in x-face-emacs

* Thu Feb 21 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.3.6.16-2k)
- update 1.3.6.16
- update x-face-e21.el
- remove x-face-e21.diff

* Sun Feb 17 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.3.6.15-2k)
- update 1.3.6.15
- update x-face-e21.el
- add x-face-e21.diff posted @ Elips and WL
  <m34rke8w8s.wl@gohome.org>

* Wed Dec 19 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.6.13-16k)
- revise config-sample

* Sat Dec  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3.6.13-14k)
- add config-sample

* Fri Nov  9 2001 Toru Hoshina <t@kondara.org>
- (1.3.6.13-8k)
- don't require both emacs and xemacs on top.

* Wed Nov  7 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.3.6.13-6k)
- replace new x-face-e21.el.gz

* Tue Oct 30 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.3.6.13-4k)
- add x-face-e21.el for Emacs21

* Mon Oct 22 2001 Hidetomo Machi <mcHT@kondara.org>
- (elisp-x-face-1.3.6.13-2k)
- merge x-face-emacs and x-face-xemacs

* Wed Oct 25 2000 Hidetomo Machi <mcHT@kondara.org>
- (1.3.6.10-9k)
- modify specfile (License)
- include select-xface ;)

* Fri Jul 21 2000 Toshiro HIKITA <toshi@sodan.org>
- imported from x-face-emacs-1.3.6.10-6k
- Adopt to XEmacs

* Wed Jul  5 2000 SAKA Toshihide <saka@yugen.org>
- Add the summary and the description. You may need more description
  about this package, also may I :-(

* Sun Jul  2 2000 SAKA Toshihide <saka@yugen.org>
- Released for GNU Emacs 20.7

* Mon Feb 28 2000 SAKA Toshihide <saka@yugen.org>
- Released for GNU Emacs 20.6.

* Thu Feb 24 2000 SAKA Toshihide <saka@yugen.org>
- Version up: 1.3.6.9 ==> 1.3.6.10

* Sun Jan  8 2000 SAKA Toshihide <saka@yugen.org>
- 1st release.
