; ~/.xemacs/init.el (for xemacs)                       -*- emacs-lisp -*-

;; ----------------------------------------------------------------------
;; autoloads
;; ----------------------------------------------------------------------

(autoload 'x-face-encode "x-face"
  "Generate X-Face string(s) from xbm file." t)
(autoload 'x-face-insert "x-face"
  "Insert X-Face fields." t)
(autoload 'x-face-save "x-face"
  "Save X-Face fields to files." t)
(autoload 'x-face-view "x-face"
  "View X-Face fields." t)
(autoload 'x-face-ascii-view "x-face"
  "View X-Face fields as ascii pictures." t)
(autoload 'x-face-xmas-display-x-face "x-face"
  "Display X-Face fields as XEmacs-glyph." t)
(autoload 'x-face-xmas-force-display-x-face "x-face"
  "Display X-Face fields compulsorily as XEmacs glyph." t)
(autoload 'x-face-xmas-remove-x-face-glyph "x-face"
  "Remove X-Face images and some text-properties." t)

;; ----------------------------------------------------------------------
;; variables
;; ----------------------------------------------------------------------

; uncomment the next line to disable splash
; (setq x-face-inhibit-loadup-splash t)
; Name of the directory where the image files are existed.
(setq x-face-image-file-directory "~/.x-faces")
; Name of the directory where the image files will be saved in.
(setq x-face-image-file-directory-for-save "~/.x-faces")
; Default xbm file name of user's face.
(setq x-face-default-xbm-file "YourFace.xbm")

(setq select-xface-directory "~/.x-faces")
(setq select-xface-add-x-face-version-header t)

;; ----------------------------------------------------------------------
;; load libraries
;; ----------------------------------------------------------------------
(require 'select-xface)
(require 'x-face)

;; replace highlight-headers (for XEmacs only)
(if (not window-system)
  (x-face-xmas-replace-highlight-headers))
