;;; for Wanderlust (~/.wl or ~/.wl.el)                 -*- emacs-lisp -*-
; -----------------------------------------------------------------------

(add-hook 'mail-mode-hook
          (lambda ()
            (define-key (current-local-map) "\C-x4x"
              'select-xface)))
(add-hook 'wl-mail-setup-hook 'x-face-insert)
;; If you use `wl-draft-insert-x-face-field' instead of
;; `x-face-insert' for inserting an X-Face, you can highlight
;; it as an image with the setting of the following hook:
(add-hook 'wl-draft-insert-x-face-field-hook
	  (lambda nil
	    (x-face-insert wl-x-face-file)))
(when window-system
  (cond
   ((featurep 'xemacs)
    ;; for XEmacs
    (require 'x-face)
    (setq wl-highlight-x-face-function 'x-face-xmas-wl-display-x-face))
   (t
    ;; for Emacs
    (require 'x-face-e21)
    (setq wl-highlight-x-face-function 'x-face-decode-message-header))))
(define-key wl-summary-mode-map "\C-x4s" 'x-face-save)
(define-key wl-draft-mode-map "\C-x4i" 'x-face-insert)
;; "\M-t" key is reserved for wl command.
(define-key wl-draft-mode-map "\M-\C-t" 'x-face-show)
