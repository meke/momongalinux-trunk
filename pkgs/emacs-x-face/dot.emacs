; ~/.emacs (for emacs)                                 -*- emacs-lisp -*-

;; ----------------------------------------------------------------------
;; autoloads
;; ----------------------------------------------------------------------

(autoload 'x-face-decode-message-header "x-face-e21")
(autoload 'x-face-insert "x-face-e21" nil t)
(autoload 'x-face-save "x-face-e21" nil t)
(autoload 'x-face-show "x-face-e21" nil t)
(autoload 'x-face-turn-off "x-face-e21")

;; ----------------------------------------------------------------------
;; variables
;; ----------------------------------------------------------------------

; uncomment the next line to disable splash
; (setq x-face-inhibit-loadup-splash t)
; Name of the directory where the image files are existed.
(setq x-face-image-file-directory "~/.x-faces")
; Name of the directory where the image files will be saved in.
(setq x-face-image-file-directory-for-save "~/.x-faces")
; Default xbm file name of user's face.
(setq x-face-default-xbm-file "YourFace.xbm")

(setq select-xface-directory "~/.x-faces")
(setq select-xface-add-x-face-version-header t)

; Show X-Face images when `x-face-insert' is done.
(setq x-face-auto-image t)

;; ----------------------------------------------------------------------
;; load libraries
;; ----------------------------------------------------------------------
(require 'select-xface)
(require 'x-face-e21)

; If you show X-Face images in the message sending buffer,
; it is STRONGLY recommended that you remove images from the
; buffer before sending a message.  However, it seems not to
; be required for Gnusae so far.  The following lines are for
; SEMI and Mew.  The latter can be put into .mew file instead.
(add-hook 'mime-edit-translate-hook 'x-face-turn-off)
(add-hook 'mew-make-message-hook 'x-face-turn-off)
