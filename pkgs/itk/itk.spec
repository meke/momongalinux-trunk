%global momorel 6

%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %define tcl_sitearch %{_libdir}/tcl%{tcl_version}}

Name:           itk
Version:        3.4
Release:        %{momorel}m%{?dist}
Summary:        Object oriented extensions to Tk

Group:          Development/Libraries
License:        "TCL"
URL:            http://incrtcl.sourceforge.net/itcl/
# cvs -d:pserver:anonymous@incrtcl.cvs.sourceforge.net:/cvsroot/incrtcl export -D 2007-12-31 -d itk-20071231cvs itk
# tar czf itk-20071231cvs.tgz ./itk-20071231cvs
Source0:        itk-20071231cvs.tgz
Patch0:         itk-3.4-libdir.patch
Patch1:         itk-3.4-tclbindir.patch
Patch2:         itk-3.4-soname.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:  tcl(abi) = 8.5 itcl tk
BuildRequires:  tk-devel itcl-devel

%description
[incr Tk] is Tk extension that provides object-oriented features that are
missing from the Tk extension to Tcl.  The OO features provided by itk are
useful for building megawidgets.

%package devel
Summary:  Development headers and libraries for linking against itk
Group: Development/Libraries
Requires:       %{name} = %{version}-%{release}
%description devel
Development headers and libraries for linking against itk.

%prep
%setup -q -n itk-20071231cvs
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_libdir}/*.so
%dir %{tcl_sitearch}/itk%{version}
%{tcl_sitearch}/%{name}%{version}/*.tcl
%{tcl_sitearch}/%{name}%{version}/*.itk
%{tcl_sitearch}/%{name}%{version}/tclIndex
%{_mandir}/mann/*.*
%doc license.terms

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
# What happened to itk's stub library and itkConfig.sh?

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4-4m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4-1m)
- import from Fedora to plplot->gdl

* Sat Feb 9 2008 Wart <wart at kobold.org> - 3.4-2
- Rebuild for gcc 4.3
- Add patch to add soname to library

* Fri Jan 11 2008 Wart <wart at kobold.org> - 3.4-1
- Update to latest CVS head for tcl 8.5 compatibility

* Wed Dec 19 2007 Wart <wart at kobold.org> - 3.3-0.8.RC1
- Move libitk shared library to %%{_libdir} so that applications
  linked against itk can find it. (BZ #372791)

* Sun Aug 19 2007 Wart <wart at kobold.org> - 3.3-0.7.RC1
- License tag clarification
- Better download URL
- Clean up %%files section

* Wed Mar 28 2007 Wart <wart at kobold.org> - 3.3-0.6.RC1
- Rebuild for tcl 8.4 downgrade

* Thu Feb 8 2007 Wart <wart at kobold.org> - 3.3-0.5.RC1
- Rebuild for tcl 8.5a5

* Mon Aug 28 2006 Wart <wart at kobold.org> - 3.3-0.4.RC1
- Rebuild for Fedora Extras

* Thu Jun 1 2006 Wart <wart at kobold.org> - 3.3-0.3.RC1
- Fixed Requires: for -devel subpackage

* Wed Jan 11 2006 Wart <wart at kobold.org> - 3.3-0.2.RC1
- Fix quoting bug that is exposed by bash >= 3.1

* Mon Jan 9 2006 Wart <wart at kobold.org> - 3.3-0.1.RC1
- New itk package from newly separated itk upstream sources.
