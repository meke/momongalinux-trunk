%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-newt
Version:        0.9
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for using newt text mode window system

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://et.redhat.com/~rjones/ocaml-newt/
Source0:        http://et.redhat.com/~rjones/ocaml-newt/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  chrpath
BuildRequires:  ocaml-camlidl-devel >= 1.05-9m
#BuildRequires:  newt-devel > 0.52.7
BuildRequires:  newt-devel


%description
This is a set of OCaml bindings to newt.

The newt windowing system is a terminal-based window and widget
library designed for writing applications with a simple, but
user-friendly, interface.  While newt is not intended to provide the
rich feature set advanced applications may require, it has proven to
be flexible enough for a wide range of applications (most notably, Red
Hat's installation process).


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       newt-devel


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q


%build
./configure --libdir=%{_libdir}
# Dependencies are broken in the upstream package.
make newt_int.mli
rm -f .depend
make depend

make all
%if %opt
make opt
%endif
make doc


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install

strip $OCAMLFIND_DESTDIR/stublibs/dll*.so
chrpath --delete $OCAMLFIND_DESTDIR/stublibs/dll*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/ocaml/newt
%if %opt
%exclude %{_libdir}/ocaml/newt/*.a
%exclude %{_libdir}/ocaml/newt/*.cmxa
%exclude %{_libdir}/ocaml/newt/*.cmx
%endif
%exclude %{_libdir}/ocaml/newt/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc COPYING.LIB README examples/*.ml html
%if %opt
%{_libdir}/ocaml/newt/*.a
%{_libdir}/ocaml/newt/*.cmxa
%{_libdir}/ocaml/newt/*.cmx
%endif
%{_libdir}/ocaml/newt/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-9m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9-2
- Rebuild for OCaml 3.10.2

* Tue Mar 18 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9-1
- New upstream release 0.9.

* Tue Mar 18 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7-1
- New upstream release 0.7.
- Move configure into the build section.

* Fri Mar 14 2008 Richard W.M. Jones <rjones@redhat.com> - 0.6-1
- New upstream release 0.6.

* Thu Mar 13 2008 Richard W.M. Jones <rjones@redhat.com> - 0.4-1
- Initial RPM release.
