%global momorel 1

%{!?python:%define python python}
%{!?python_sitearch: %define python_sitearch %(%{python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           %{python}-twisted-web
Version:        11.0.0
Release:        %{momorel}m%{?dist}
Summary:        Twisted web client and server, programmable in Python
Group:          Development/Libraries
License:        MIT
URL:            http://twistedmatrix.com/trac/wiki/TwistedWeb
Source0:        http://twistedmatrix.com/Releases/Web/11.0/TwistedWeb-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  %{python}-twisted-core >= 11.0.0
BuildRequires:  %{python}-devel >= 2.7
Requires:       %{python}-twisted-core >= 11.0.0
Requires:       SOAPpy

# a noarch-turned-arch package should not have debuginfo
%define debug_package %{nil}

%description
Twisted is an event-based framework for internet applications.

Twisted Web is a complete web server, aimed at hosting web applications using
Twisted and Python, but fully able to serve static pages too.

%prep
%setup -q -n TwistedWeb-%{version}

# Remove spurious shellbangs
sed -i -e '/^#! *\/usr\/bin\/python/d' twisted/web/test/test_cgi.py
sed -i -e '/^#! *\/usr\/bin\/python/d' twisted/web/test/test_distrib.py

%build
%{python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT

# This is a pure python package, but extending the twisted namespace from
# python-twisted-core, which is arch-specific, so it needs to go in sitearch
%{python} setup.py install -O1 --skip-build \
    --install-purelib %{python_sitearch} --root $RPM_BUILD_ROOT

# See if there's any egg-info
if [ -f $RPM_BUILD_ROOT%{python_sitearch}/Twisted*.egg-info ]; then
    echo $RPM_BUILD_ROOT%{python_sitearch}/Twisted*.egg-info |
        sed -e "s|^$RPM_BUILD_ROOT||"
fi > egg-info

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ -x %{_libexecdir}/twisted-dropin-cache ]; then
    %{_libexecdir}/twisted-dropin-cache || :
fi

%postun
if [ -x %{_libexecdir}/twisted-dropin-cache ]; then
    %{_libexecdir}/twisted-dropin-cache || :
fi

%files -f egg-info
%defattr(-,root,root,-)
%doc LICENSE NEWS README doc/*
%{python_sitearch}/twisted/plugins/twisted_web.py*
%{python_sitearch}/twisted/web/

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.0-1m)
- update 11.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.2.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2.0-1m)
- import from Fedora 11

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 8.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 29 2008 Matthias Saou <http://freshrpms.net/> 8.2.0-1
- Update to 8.2.0.
- Change back spec cosmetic details from Paul's to Thomas' preference.

* Tue Dec 23 2008 Matthias Saou <http://freshrpms.net/> 8.1.0-2
- Update to 8.1.0.
- Merge back changes from Paul Howarth.
- Make sure the scriplets never return a non-zero exit status.

* Sun Nov 30 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.7.0-5
- Fix locations for Python 2.6

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.7.0-4
- Rebuild for Python 2.6

* Fri Mar 07 2008 Jesse Keating <jkeating@redhat.com> - 0.7.0-3
- Handle egg issue, drop the pyver stuff.

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.7.0-2
- Autorebuild for GCC 4.3

* Fri Aug 31 2007 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.7.0-1
- updated to new version
- updated core requires
- removed websetroot

* Fri Dec 22 2006 Jef Spaleta <jspaleta@gmail.com>
- 0.6.0-4
- Docs and Url fixes as suggested in the review
- Added BR for python-devel for python 2.5, merry Christmas!

* Wed Nov 01 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.6.0-3
- remove shebang lines from unit test files

* Tue Sep 26 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.6.0-2
- no longer ghost .pyo files
- rebuild dropin.cache

* Wed Jun 07 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.6.0-1
- update to new release
- remove NoArch
- change dep to SOAPpy as it is called in extras

* Tue Aug 23 2005 Jeff Pitman <symbiont+pyvault@berlios.de> 0.5.0-2
- disttag

* Wed Mar 16 2005 Jeff Pitman <symbiont+pyvault@berlios.de> 0.5.0-0.1.a3
- upstream release

* Sat Mar 12 2005 Jeff Pitman <symbiont+pyvault@berlios.de> 0.5.0-0.1.a2
- prerelease; FE versioning

* Mon Feb 07 2005 Jeff Pitman <symbiont+pyvault@berlios.de> 0.1.0-1
- prep for split

