%global         momorel 1

Name:           pari-seadata
Version:        20090618
Release:        %{momorel}m%{?dist}
Summary:        PARI/GP Computer Algebra System modular polynomials
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://pari.math.u-bordeaux.fr/packages.html
Source0:        http://pari.math.u-bordeaux.fr/pub/pari/packages/seadata.tgz
NoSource:       0
BuildArch:      noarch

%description
This package contains the optional PARI package seadata, which provides the
modular polynomials for prime level up to 500 needed by the GP functions
ellap and ellsea.

These polynomials were extracted from the ECHIDNA databases available at
<http://echidna.maths.usyd.edu.au/kohel/dbs/> and computed by David R. Kohel
at the University of Sydney.

%prep
%setup -cq
mv data/seadata/README .

%build

%install
mkdir -p %{buildroot}%{_datadir}/pari/
cp -a data/seadata %{buildroot}%{_datadir}/pari/
%{_fixperms} %{buildroot}%{_datadir}/pari/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{_datadir}/pari/seadata

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20090618-1m)
- import from Fedora

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090618-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090618-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090618-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jun  1 2012 Paul Howarth <paul@city-fan.org> - 20090618-3
- Drop conflict with old versions of pari, which cannot use this package but
  aren't broken by it either

* Wed May 23 2012 Paul Howarth <paul@city-fan.org> - 20090618-2
- Add dist tag
- Use %%{_fixperms} to ensure packaged files have sane permissions
- At least version 2.4.3 of pari is required to use this data, so conflict
  with older versions; can't use a versioned require here as it would lead to
  circular build dependencies with pari itself

* Fri May 18 2012 Paul Howarth <paul@city-fan.org> - 20090618-1
- Initial RPM package
