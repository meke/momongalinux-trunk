%global momorel 1

%global with_python3 1

%global srcname setuptools

Name:           python-setuptools
Version:        2.0
Release:        %{momorel}m%{?dist}
Summary:        Easily build and distribute Python packages

Group:          Applications/System
License:        "Python" or "ZPLv2.0"
URL:            http://pypi.python.org/pypi/distribute
Source0:        http://pypi.python.org/packages/source/s/%{srcname}/%{srcname}-%{version}.tar.gz
NoSource:       0
Source1:        psfl.txt
Source2:        zpl.txt

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
Requires: python-backports-ssl_match_hostname
BuildRequires: python-backports-ssl_match_hostname
BuildRequires:  python2-devel >= 2.7
BuildRequires:  python3-devel >= 3.4

# Legacy: We removed this subpackage once easy_install no longer depended on
# python-devel
Provides: python-setuptools-devel = %{version}-%{release}
Obsoletes: python-setuptools-devel < 0.6.7-1

# Provide this since some people will request distribute by name
Provides: python-distribute = %{version}-%{release}

%description
Setuptools is a collection of enhancements to the Python distutils that allow
you to more easily build and distribute Python packages, especially ones that
have dependencies on other packages.

This package contains the runtime components of setuptools, necessary to
execute the software that requires pkg_resources.py.

This package contains the distribute fork of setuptools.
%if 0%{?with_python3}
%package -n python3-setuptools
Summary:        Easily build and distribute Python 3 packages
Group:          Applications/System

%description -n python3-setuptools
Setuptools is a collection of enhancements to the Python 3 distutils that allow
you to more easily build and distribute Python 3 packages, especially ones that
have dependencies on other packages.

This package contains the runtime components of setuptools, necessary to
execute the software that requires pkg_resources.py.

This package contains the distribute fork of setuptools.
%endif # with_python3

%prep
%setup -q -n %{srcname}-%{version}

find -name '*.txt' -exec chmod -x \{\} \;
find . -name '*.orig' -exec rm \{\} \;

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
pushd %{py3dir}
for file in setuptools/command/easy_install.py ; do
    sed -i '1s|^#!python|#!%{__python3}|' $file
done
popd
%endif # with_python3

for file in setuptools/command/easy_install.py ; do
    sed -i '1s|^#!python|#!%{__python}|' $file
done

%build

CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build
popd
%endif # with_python3

%install
rm -rf %{buildroot}

# Must do the python3 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python2 version
# to be the default for now).
# Change to defaulting to python3 version in F22
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}

rm -rf %{buildroot}%{python3_sitelib}/setuptools/tests

install -p -m 0644 %{SOURCE1} %{SOURCE2} %{py3dir}
find %{buildroot}%{python3_sitelib} -name '*.exe' | xargs rm -f
chmod +x %{buildroot}%{python3_sitelib}/setuptools/command/easy_install.py
popd
%endif # with_python3

%{__python} setup.py install --skip-build --root %{buildroot}

rm -rf %{buildroot}%{python_sitelib}/setuptools/tests

install -p -m 0644 %{SOURCE1} %{SOURCE2} .
find %{buildroot}%{python_sitelib} -name '*.exe' | xargs rm -f
chmod +x %{buildroot}%{python_sitelib}/setuptools/command/easy_install.py

%check
%{__python} setup.py test

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py test
popd
%endif # with_python3

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc *.txt docs
%{python_sitelib}/*
%{_bindir}/easy_install
%{_bindir}/easy_install-2.*

%if 0%{?with_python3}
%files -n python3-setuptools
%defattr(-,root,root,-)
%doc psfl.txt zpl.txt docs
%{python3_sitelib}/*
%{_bindir}/easy_install-3.*
%endif # with_python3

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.27-1m)
- update to 0.6.27
- import two patches from fedora

* Wed Jan 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.21-4m)
- update sources (maybe source was replaced)

* Mon Sep 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.21-3m)
- modify %%files

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.21-2m)
- rebuild against python-3.2

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.21-1m)
- update to 0.6.21

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.16-1m)
- update to 0.6.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-1m)
- update to 0.6.14
- create python3 subpackage

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6c9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6c9-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6c9-2m)
- rebuild against python-2.6.1-2m

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6c9-1m)
- update to 0.6c9

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6c7-4m)
- add patch0 for Pyrex-0.9.8.4

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6c7-4m)
- sync Fedora

* Wed Apr 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6c7-3m)
- split packages. python-setuptools/python-setuptools-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6c7-2m)
- rebuild against gcc43

* Fri Nov  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6c7-1m)
- version 0.6c7

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6c5-1m)
- version 0.6c5

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6b3-3m)
- rebuild against python-2.5-9m

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6b3-2m)
- rebuild against python-2.5

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6b3-1m)
- import from fedora extra

* Wed Jun 28 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.6b3-1
- Taking over from Ignacio
- Version 0.6b3
- Ghost .pyo files in sitelib
- Add license files
- Remove manual python-abi, since we're building FC4 and up
- Kill .exe files

* Wed Feb 15 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.6a10-1
- Upstream update

* Mon Jan 16 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.6a9-1
- Upstream update

* Sat Dec 24 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.6a8-1
- Initial RPM release
