%global momorel 7
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-zip
Version:        1.04
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for reading and writing zip, jar and gzip files

Group:          Development/Libraries
License:        "LGPLv2 with exceptions"
URL:            http://pauillac.inria.fr/~xleroy/software.html
Source0:        http://caml.inria.fr/distrib/bazar-ocaml/camlzip-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  zlib-devel >= 1.1.3
BuildRequires:  chrpath


%description
This Objective Caml library provides easy access to compressed files
in ZIP and GZIP format, as well as to Java JAR files. It provides
functions for reading from and writing to compressed files in these
formats.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n camlzip-%{version}


%build
make all
%if %opt
make allopt
%endif
strip dll*.so
chrpath --delete dll*.so

cat > META <<EOF
name = "%{name}"
version = "%{version}"
description = "%{description}"
requires = "unix"
archive(byte) = "zip.cma"
archive(native) = "zip.cmxa"
EOF


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/ocaml/zip
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/ocaml/stublibs

export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml

ocamlfind install zip *.cma *.cmxa *.a *.cmx *.cmi *.mli dll*.so META


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/zip
%if %opt
%exclude %{_libdir}/ocaml/zip/*.a
%exclude %{_libdir}/ocaml/zip/*.cmxa
%exclude %{_libdir}/ocaml/zip/*.cmx
%endif
%exclude %{_libdir}/ocaml/zip/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc LICENSE Changes README
%if %opt
%{_libdir}/ocaml/zip/*.a
%{_libdir}/ocaml/zip/*.cmxa
%{_libdir}/ocaml/zip/*.cmx
%endif
%{_libdir}/ocaml/zip/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.04-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-3m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-2m)
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.03-4
- Rebuild for OCaml 3.10.2

* Mon Mar 31 2008 Richard W.M. Jones <rjones@redhat.com> - 1.03-3
- Add unix as a dependency to the META-file (rhbz #439652).

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 1.03-2
- Rebuild for ppc64.

* Fri Feb 22 2008 Richard W.M. Jones <rjones@redhat.com> - 1.03-1
- Initial RPM release.
