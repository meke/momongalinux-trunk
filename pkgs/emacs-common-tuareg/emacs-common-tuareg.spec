%global momorel 3
%define pkg tuareg
%define pkgname Tuareg-mode

# If the emacs-el package has installed a pkgconfig file, use that to
# determine install locations and Emacs version at build time,
# otherwise set defaults.
%if %($(pkg-config emacs) ; echo $?)
%define emacs_version 22.1
%define emacs_lispdir %{_datadir}/emacs/site-lisp
%define emacs_startdir %{_datadir}/emacs/site-lisp/site-start.d
%else
%define emacs_version %(pkg-config emacs --modversion)
%define emacs_lispdir %(pkg-config emacs --variable sitepkglispdir)
%define emacs_startdir %(pkg-config emacs --variable sitestartdir)
%endif

# If the xemacs-devel package has installed a pkgconfig file, use that
# to determine install locations and Emacs version at build time,
# otherwise set defaults.
%if %($(pkg-config xemacs) ; echo $?)
%define xemacs_version 21.5
%define xemacs_lispdir %{_datadir}/xemacs/site-lisp
%define xemacs_startdir %{_datadir}/xemacs/site-lisp/site-start.d
%else
%define xemacs_version %(pkg-config xemacs --modversion)
%define xemacs_lispdir %(pkg-config xemacs --variable sitepkglispdir)
%define xemacs_startdir %(pkg-config xemacs --variable sitestartdir)
%endif


Name:           emacs-common-%{pkg}
Version:        2.0.4
Release: %{momorel}m%{?dist}
Summary:        Emacs and XEmacs mode for editing ocaml

Group:          Development/Libraries
License:        GPLv2+
URL:            http://forge.ocamlcore.org/projects/tuareg/
Source0:        http://forge.ocamlcore.org/frs/download.php/514/tuareg-2.0.4.tgz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  emacs, emacs-el
BuildRequires:  xemacs, xemacs-devel

# Needs caml-types.el in order to use *.annot files properly.
# In Debian this is merely a recommendation, to avoid pulling
# in all of OCaml just to edit an OCaml file.
Requires:    emacs-ocaml-mode

%description
Tuareg is an OCaml mode for GNU Emacs and XEmacs.  It handles
automatic indentation of Objective Caml and Caml Light code.  Key
parts of the code are highlighted using Font-Lock.  Support to run an
interactive Caml toplevel and debbuger is provided.

This package contains the common files.  Install emacs-%{pkg} to get
the complete package.


%package -n emacs-%{pkg}
Summary:        Compiled elisp files to run %{pkgname} under GNU Emacs
Group:          Development/Libraries
Requires:       emacs(bin) >= %{emacs_version}
Requires:       emacs-common-%{pkg} = %{version}-%{release}

%description -n emacs-%{pkg}
Tuareg is an OCaml mode for GNU Emacs and XEmacs.  It handles
automatic indentation of Objective Caml and Caml Light code.  Key
parts of the code are highlighted using Font-Lock.  Support to run an
interactive Caml toplevel and debbuger is provided.

Install this package if you need to edit OCaml code in Emacs.


%package -n emacs-%{pkg}-el
Summary:        Elisp source files for %{pkgname} under GNU Emacs
Group:          Development/Libraries
Requires:       emacs-%{pkg} = %{version}-%{release}
Obsoletes:	elisp-tuareg
Provides:	elisp-tuareg

%description -n emacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under GNU
Emacs. You do not need to install this package to run
%{pkgname}. Install the emacs-%{pkg} package to use %{pkgname} with
GNU Emacs.


%package -n xemacs-%{pkg}
Summary:        Compiled elisp files to run %{pkgname} under XEmacs
Group:          Development/Libraries
Requires:       xemacs(bin) >= %{xemacs_version}
Requires:       emacs-common-%{pkg} = %{version}-%{release}

%description -n xemacs-%{pkg}
Tuareg is an OCaml mode for GNU Emacs and XEmacs.  It handles
automatic indentation of Objective Caml and Caml Light code.  Key
parts of the code are highlighted using Font-Lock.  Support to run an
interactive Caml toplevel and debbuger is provided.

Install this package if you need to edit OCaml code in XEmacs.


%package -n xemacs-%{pkg}-el
Summary:        Elisp source files for %{pkgname} under XEmacs
Group:          Development/Libraries
Requires:       xemacs-%{pkg} = %{version}-%{release}

%description -n xemacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under
XEmacs. You do not need to install this package to run
%{pkgname}. Install the xemacs-%{pkg} package to use %{pkgname} with
XEmacs.


%prep
%setup -q -n %{pkg}-%{version}


%build
make


%install
rm -rf $RPM_BUILD_ROOT

# Install twice, for emacs and xemacs.
mkdir -p $RPM_BUILD_ROOT/%{emacs_lispdir}/%{pkg}
make install DEST=$RPM_BUILD_ROOT/%{emacs_lispdir}/%{pkg}
mkdir -p $RPM_BUILD_ROOT/%{xemacs_lispdir}/%{pkg}
make install DEST=$RPM_BUILD_ROOT/%{xemacs_lispdir}/%{pkg}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING HISTORY README append-tuareg.el custom-tuareg.el


%files -n emacs-%{pkg}
%defattr(-,root,root,-)
%doc COPYING
%{emacs_lispdir}/%{pkg}/*.elc
%{emacs_lispdir}/%{pkg}/version
#%{emacs_startdir}/*.el
%dir %{emacs_lispdir}/%{pkg}


%files -n emacs-%{pkg}-el
%defattr(-,root,root,-)
%doc COPYING
%{emacs_lispdir}/%{pkg}/*.el


%files -n xemacs-%{pkg}
%defattr(-,root,root,-)
%doc COPYING
%{xemacs_lispdir}/%{pkg}/*.elc
%{xemacs_lispdir}/%{pkg}/version
#%{xemacs_startdir}/*.el
%dir %{xemacs_lispdir}/%{pkg}


%files -n xemacs-%{pkg}-el
%defattr(-,root,root,-)
%doc COPYING
%{xemacs_lispdir}/%{pkg}/*.el


%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-3m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-2m)
- revise requires tag

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-1m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.1-7m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-6m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-5m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-3m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-2m)
- rebuild against emacs-23.2

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45.6-7m)
- merge tuareg-emacs to elisp-tuareg

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.45.6-5m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.45.6-4m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45.6-3m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45.6-2m)
- rebuild against emacs-23.0.94

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45.6-1m)
- initial packaging
