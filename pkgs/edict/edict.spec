%global momorel 7

%define v_year 2003
%define v_month 08
%define v_day 19

Summary: Japanese-English Electronic Dictionaru File
Name: edict
Version: 1.0
Release: 0.%{v_year}%{v_month}%{v_day}.%{momorel}m%{?dist}
Source0: ftp://ftp.cc.monash.edu.au/pub/nihongo/edict.gz
Source1: ftp://ftp.cc.monash.edu.au/pub/nihongo/edict_doc.txt
Source2: ftp://ftp.cc.monash.edu.au/pub/nihongo/edict_doc.html
Source3: http://www.csse.monash.edu.au/groups/edrdg/licence.html
License: see "licence.html"
Group: Applications/Text
Url: http://www.dgs.monash.edu.au/~jwb/edict.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
Japanese-English Electronic Dictionaru File

%prep
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/share/dict

cp %{SOURCE0} %{SOURCE1} %{SOURCE2} %{SOURCE3} . 
gzip -d edict.gz
perl -p -i -e "s,50\x5c\x0b,b," edict

%install
cp edict %{buildroot}/usr/share/dict
install -m644 edict %{buildroot}/usr/share/dict

%clean
rm -rf %{buildroot}
rm $RPM_BUILD_DIR/edict

%files
%defattr(-,root,root)
/usr/share/dict/edict
%doc edict_doc.txt edict_doc.html licence.html

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.20030819.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.20030819.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.20030819.5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20030819.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20030819.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.20030819.2m)
- rebuild against gcc43
 
* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (kossori)
- License: see "licence.html" instead of "see licence.html"

* Sun Oct 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.20030819.1m)

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-0.20020426.3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Apr 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0-0.020020426002k)
- rebuild against updated sources

* Fri Jan 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0-0.020020118002k)
- obey FHS (move to /usr/share/dict)
- rebuild against updated sources

* Mon Nov 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.0-0.020010615004k)
- rebuild against updated sources

* Thu Jul 05 2001 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- include source file.
- up to 2001/06/15 version.

* Thu Jun 22 2000 Uechi Yasumasa <uh@kondara.org>
- V00-001-3k
- add documents and fix version

* Wed Apr 05 2000 Chiaki Nakata <hanibi@kondara.org>
- add remove edict in %clean section.

* Wed Apr 05 2000 Chiaki Nakata <hanibi@kondara.org>
- First release in Kondara MNU/Linux
