%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name:          kfilemetadata
Summary:       A library for extracting file metadata
Version:       %{kdever}
Release:       %{momorel}m%{?dist}
License:       GPLv2+ and LGPLv2+
Group:         System Environment/Libraries
URL:           https://projects.kde.org/projects/kde/kdelibs/kactivities
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:      0
BuildRequires: ebook-tools-devel
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: qmobipocket-devel >= 4.12.97-2m
BuildRequires: exiv2-devel >= 0.20
BuildRequires: poppler-qt4-devel
BuildRequires: taglib-devel
Requires:      kdelibs >= %{version}

%description
%{summary}.

%package  devel
Summary:       Developer files for %{name}
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Requires:      kdelibs-devel

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_kde4_libdir}/libkfilemetadata.so.4*
%{_kde4_libdir}/kde4/kfilemetadata_epubextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_exiv2extractor.so
%{_kde4_libdir}/kde4/kfilemetadata_ffmpegextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_mobiextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_odfextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_office2007extractor.so
%{_kde4_libdir}/kde4/kfilemetadata_officeextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_plaintextextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_popplerextractor.so
%{_kde4_libdir}/kde4/kfilemetadata_taglibextractor.so
%{_kde4_datadir}/kde4/services/kfilemetadata_epubextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_exiv2extractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_ffmpegextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_mobiextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_odfextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_office2007extractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_officeextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_plaintextextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_popplerextractor.desktop
%{_kde4_datadir}/kde4/services/kfilemetadata_taglibextractor.desktop
%{_kde4_datadir}/kde4/servicetypes/kfilemetadataextractor.desktop

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/kfilemetadata
%{_kde4_libdir}/libkfilemetadata.so
%{_kde4_libdir}/cmake/KFileMetaData

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Mon Mar 31 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-3m)
- remove cmake option
- specify qmobipocket version and release

* Sun Mar 30 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.12.97-2m)
- set QMOBIPOCKET_INCLUDE_DIR to enable build on i686 for the moment

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- initial build (KDE 4.13 RC)
