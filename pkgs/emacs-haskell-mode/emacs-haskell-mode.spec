%global momorel 8
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: An Emacsen major mode for editing Haskell source code
Name: emacs-haskell-mode
Version: 2.8.0
Release: %{momorel}m%{?dist}
Source0: http://projects.haskell.org/haskellmode-emacs/haskell-mode-%{version}.tar.gz
NoSource: 0
License: GPLv3+
Group: Applications/Editors
URL: http://haskell.org/haskell-mode/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: haskell-mode-emacs
Obsoletes: haskell-mode-xemacs

Obsoletes: elisp-haskell-mode
Provides: elisp-haskell-mode

%description
Haskell-mode is a major Emacs mode for editing Haskell source code.
It provides syntax highlighting and automatic indentation and comes
with inf-haskell which allows interaction with an inferior Haskell
interactive loop such as the one of Hugs or GHCi.

%prep
%setup -q -n haskell-mode-%{version}

%build
for el in *.el;
do
    emacs -batch -q -no-site-file \
          -eval '(setq load-path (cons "." load-path))' \
          -f batch-byte-compile ${el}
done

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{e_sitedir}/haskell
%{__install} -m 0644 *.el *.elc %{buildroot}%{e_sitedir}/haskell

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc NEWS README fontlock.hs indent.hs
%{e_sitedir}/haskell

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-8m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.8.0-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.0-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.0-2m)
- full rebuild for mo7 release

* Sat Jul  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-2m)
- merge haskell-mode-emacs to elisp-haskell-mode

* Thu Dec  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Fri Nov  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2
- good-bye haskell-mode-xemacs

* Tue Oct 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-13m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-12m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-11m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-10m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-9m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-8m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-6m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-2m)
- rebuild against gcc43

* Wed Dec 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-1m)
- update to 2.4
- License: GPLv3

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-4m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-3m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-2m)
- rebuild against emacs-22.0.96

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-2m)
- rebuild against emacs-22.0.90

* Tue Aug 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-1m)
- initial package
