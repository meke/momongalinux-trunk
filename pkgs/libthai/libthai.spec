%global momorel 4

%define datrie_version 0.2.3
%define datrie libdatrie-%{datrie_version}

Summary: Thai language support routines
Name: libthai
Version: 0.1.14
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source0: ftp://linux.thai.net/pub/thailinux/software/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: ftp://linux.thai.net/pub/thailinux/software/%{name}/libdatrie-%{datrie_version}.tar.gz
NoSource: 1
Patch0: libthai-0.1.12-libdatrie-0.2.2-static-build.patch
Patch1: libthai-0.1.9-doxygen-segfault.patch
Patch2: libthai-0.1.9-multilib.patch
Patch3: 01_ftbfs-manpages.patch
URL: http://linux.thai.net
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: doxygen
# we edit the Makefile.am's
BuildRequires: automake
BuildRequires: autoconf
BuildRequires: libtool

%description
LibThai is a set of Thai language support routines aimed to ease
developers' tasks to incorporate Thai language support in their applications.
It includes important Thai-specific functions e.g. word breaking, input and
output methods as well as basic character and string supports.

%package devel
Summary: Thai language support routines
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The libthai-devel package includes the header files and developer docs 
for the libthai package.

Install libthai-devel if you want to develop programs which will use
libthai.

%prep
%setup -q -n %{name}-%{version} -a 1
mv libdatrie-%{datrie_version} libdatrie
%patch0 -p1 -b .static-build
%patch1 -p1 -b .doxygen-segfault
%patch2 -p1 -b .multilib
%patch3 -p1 -b .kill-manpages

%build

# libthai depends on this library called libdatrie.  libdatrie is a
# data-structure implementaiton that the author of libthai ripped out of it.
# However, since libthai is the only user of that code, there's no reason to
# 1) package it separately, 2) use it as a shared library.
# So, we compile it as a libtool convenience library and include in libthai.so,
# and use symbol hiding to hide them (and other internal symbols).
#
# The patch takes care of making datrie into a convenience lib and making sure
# libthai will include it (and hide its symbols), and the exports make sure
# libthai finds the uninstalled libdatrie.  We need to call automake, since
# the patch modifies a few Makefile.am's.

{
  pushd libdatrie
  mkdir m4
  autoreconf -i -f
  %configure
  make
  popd
}

export DATRIE_CFLAGS="-I$PWD/libdatrie"
export DATRIE_LIBS="$PWD/libdatrie/datrie/libdatrie.la"
export PATH="$PWD/libdatrie/tools:$PATH"

autoreconf -i -f

%configure --disable-static
make

%install
rm -rf %{buildroot}

%makeinstall

# move installed doc files back to build directory to package them
# in the right place
mkdir installed-docs
mv %{buildroot}%{_docdir}/libthai/* installed-docs
rmdir %{buildroot}%{_docdir}/libthai

rm %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc README AUTHORS COPYING ChangeLog TODO
%{_libdir}/lib*.so.*
%{_datadir}/libthai

%files devel
%defattr(-, root, root)
%doc installed-docs/*
%{_includedir}/thai
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
#%%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.14-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.14-2m)
- full rebuild for mo7 release

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.14-1m)
- update to 0.1.14
- remove broken manpages (Patch3)

* Sun Jan 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.13-1m)
- [SECURITY] CVE-2009-4012
- update to 0.1.13

* Sat Dec 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.12-1m)
- sync with Rawhide (0.1.12-2)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.9-3m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.9-1m)
- re-sync with Fedora

* Mon Mar 17 2008 Matthias Clasen <mclasen@redhat.com> - 0.1.9-4
- Attempt to fix multilib conflict

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.9-3
- Autorebuild for GCC 4.3

* Tue Nov 13 2007 Behdad Esfahbod <besfahbo@redhat.com> 0.1.9-2
- Add %{name}-%{version}-doxygen-segfault.patch to workaround doxygen segfault

* Tue Aug 28 2007 Behdad Esfahbod <besfahbo@redhat.com> 0.1.9-1
- Update to 0.1.9
- Adjust patch

* Thu Aug 23 2007 Adam Jackson <ajax@redhat.com> - 0.1.7-6
- Rebuild for build ID

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.7-1m)
- import from fc to Momonga

* Tue Jan 22 2007 Behdad Esfahbod <besfahbo@redhat.com> 0.1.7-5
- Export _th_*_tbl symbols too.  They are accessed by some of the macros.

* Tue Jan 17 2007 Behdad Esfahbod <besfahbo@redhat.com> 0.1.7-4
- Patch libthai.pc.in to not require datrie.

* Tue Jan 16 2007 Matthias Clasen <mclasen@redhat.com> 0.1.7-3
- Miscellaneous fixes
 
* Tue Jan 16 2007 Behdad Esfahbod <besfahbo@redhat.com> 0.1.7-2
- Apply comments from Matthias Clasen (#222611) 
- devel summary improvement
- devel require pkgconfig
- configure --disable-static
- Add comments about the voodoo
- Install docs in the right place

* Sun Jan 14 2007 Behdad Esfahbod <besfahbo@redhat.com> 0.1.7-1
- Initial package based on package by Supphachoke Suntiwichaya
  and Kamthorn Krairaksa for the OLPC.
