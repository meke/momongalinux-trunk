%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0

Name:           libxfce4ui
Version:        4.11.1
Release:        %{momorel}m%{?dist}
Summary:        Commonly used Xfce widgets

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://xfce.org/
Source0:	http://archive.xfce.org/src/xfce/%{name}/4.11/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= 2.12.0
BuildRequires:  gtk2-devel >= 2.12.0
BuildRequires:  libSM-devel
# FIXME: replace 4.6.0 with %{xfceversion} or %{version} once this is stable
BuildRequires:  libxfce4util-devel >= %{xfce4ver}
BuildRequires:  xfconf-devel
BuildRequires:  startup-notification-devel >= 0.4
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  glade-devel

# FIXME: remove after libxfcegui4 -> libxfce4ui transition
Requires:       libxfcegui4

# do this later
#Provides:       libxfcegui4 = %{version}
#Obsoletes:      libxfcegui4 < %{version}

%description
Libxfce4ui is used to share commonly used Xfce widgets among the Xfce
applications.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk2-devel
Requires:       libxfce4util-devel
Requires:       glade-devel
Requires:       gtk-doc
Requires:       pkgconfig

# do this later
#Provides:       libxfcegui4-devel = %{version}
#Obsoletes:      libxfcegui4-devel < %{version}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure --enable-gtk-doc --disable-static
# Remove rpaths
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# The LD_LIBRARY_PATH hack is needed for --enable-gtk-doc
# because lt-libxfce4ui-scan is linked against libxfce4ui
export LD_LIBRARY_PATH=$( pwd )/libxfce4ui/.libs

make %{?_smp_mflags} V=1


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
%find_lang %{name}

# to fix the file conflict, remove after libxfcegui4 -> libxfce4ui transition
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/xdg/xfce4

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README THANKS
# FIXME: re-enable after libxfcegui4 -> libxfce4ui transition
#%config(noreplace) %{_sysconfdir}/xdg/xfce4
%{_bindir}/xfce4-about
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc TODO
%{_includedir}/xfce4/libxfce4kbd-private-2
%{_includedir}/xfce4/libxfce4kbd-private-3
%{_includedir}/xfce4/libxfce4ui-1
%{_includedir}/xfce4/libxfce4ui-2
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%doc %{_datadir}/gtk-doc/html/%{name}/
%{_datadir}/applications/xfce4-about.desktop
%{_datadir}/icons/hicolor/48x48/apps/xfce4-logo.png


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.1-1m)
- update to 4.11.1

* Sun Jan  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to 4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to versin 4.10.0

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9.0-3m)
- rebuild for glade-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9.0-2m)
- rebuild for glib 2.33.2

* Wed Mar 21 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.9.0-1m)
- update to 4.9 for exo
- update Patch0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8.0-2m)
- goodbye glade3 modules

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update to xfce4 4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.5-1m)
- update to 4.7.5
- update to xfce4 4.8pre2
- update Patch0:         libxfce4ui-4.7.5-keyboard-shortcuts.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.2-3m)
- full rebuild for mo7 release

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.2-2m)
- modify %%files

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.2-1m)
- import from Fedora to Momonga

* Tue Jul 27 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.2-2
- Fix file conflict with libxfce4gui (#618719)

* Fri May 21 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.2-1
- Update to 4.7.2

* Wed May 19 2010 Kevin Fenzi <kevin@tummy.com> - 4.7.1-3
- Rebuild for new glade version. 

* Tue Jan 12 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.1-2
- Fix license tag
- Build gtk-doc

* Tue Jan 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.1-1
- Initial spec file, based on libxfcegui4.spec

