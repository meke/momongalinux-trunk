%global momorel 9
%global srcname textext
%global inkxdir %{_datadir}/inkscape/extensions

Summary: Inkscape extension to add editable LaTeX text objects to a document
Name:    inkscape-textext
Version: 0.3.3
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: BSD
URL:     http://www.elisanet.fi/ptvirtan/software/textext/
Source0: http://www.elisanet.fi/ptvirtan/software/textext/textext-%{version}.tar.gz
Patch0: textext-0.3.3-menu.patch
Patch1: textext-0.3.3-useplatex.patch
Requires: inkscape pdf2svg pygtk2 tetex-ptex >= 3.1.10-16m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Inkscape is a great program. But if you try to make a conference poster with it, you'll see that what is missing is scientific typesetting.

Some solutions, such as Inklatex, have been written, but generally these do not allow editing the generated objects afterwards. Textext aims to cover this need, by adding re-editable LaTeX objects to Inkscape's repertoire.

Notice: Textext in this package is modified from the original one to use platex+dvipdfmx instead of pdflatex.

%prep
#'
%setup -q -c
%patch0 -p0 -b .menu
%patch1 -p0 -b .platex

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{inkxdir}
install -m 644 textext.inx %{buildroot}%{inkxdir}
install -m 755 inkex45.py %{buildroot}%{inkxdir}
install -m 755 textext.py %{buildroot}%{inkxdir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{inkxdir}/*

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-9m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-4m)
- rebuild against rpm-4.6

* Sun Apr 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.3-3m)
- move platex2pdf command to tetex-ptex

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-2m)
- rebuild against gcc43

* Fri Mar 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.3-1m)
- import to Momonga
