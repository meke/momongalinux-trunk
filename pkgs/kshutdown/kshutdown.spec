%global momorel 1
%global qtver 4.8.5
%global kdever 4.11.4
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global betaver beta
%global prever 1

Summary: KShutDown is an advanced shut down utility for KDE
Name: kshutdown
Version: 3.1
Release: %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License: GPLv2
URL: http://kshutdown.sourceforge.net/
Group: Applications/System
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-source-%{version}%{?betaver:%{betaver}}.zip
NoSource: 0
Patch0: %{name}-3.0beta3-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: kdebase-workspace-devel

%description
KShutDown is an advanced shut down utility for KDE.
Features:
- Turn Off Computer (logout and halt the system)
- Restart Computer (logout and reboot the system)
- Lock Session (lock the screen using a screen saver)
- End Current Session (end the current KDE session and logout the user)
- Extras (additional, user commands)
- Time and delay options
- Command line and DCOP support
- System tray and panel applet
- Visual and sound notifications
- KDE Kiosk support
- And more...

%prep
%setup -q -n %{name}-%{version}%{?betaver:%{betaver}}

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-category Utility \
  --add-category System \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc ChangeLog LICENSE README.html TODO
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_appsdir}/%{name}
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_kde4_iconsdir}/*/*/*/%{name}.svgz

%changelog
* Mon Dec  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-0.1.1m)
- update to 3.1beta

* Tue Jul  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Mon Jun 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.7.1m)
- update to 3.0beta8

* Thu Apr 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.6.1m)
- update to 3.0beta7

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.5.1m)
- update to 3.0beta6

* Tue Nov 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.4.1m)
- update to 3.0beta5

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.3.1m)
- update to 3.0beta4

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.2.1m)
- update to 3.0beta3

* Sat Dec 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.1.1m)
- update to 3.0beta2

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-2m)
- rebuild with new KDE 4.8

* Sat Apr 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-1m)
- update to 2.0 official release

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.13.2m)
- rebuild for new GCC 4.6

* Thu Feb 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.13.1m)
- update to 2.0 Beta 12

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.12.2m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.12.1m)
- update to 2.0 Beta 11
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-0.11.2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.11.1m)
- update to 2.0 Beta 10

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.10.2m)
- rebuild against qt-4.6.3-1m

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.10.1m)
- update to 2.0 Beta 9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.9.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.9.1m)
- update to 2.0 Beta 8

* Wed Aug 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.8.1m)
- update to 2.0beta7

* Wed Apr 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.7.1m)
- update to version 2.0 Beta 6

* Thu Apr  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.6.1m)
- update to version 2.0 Beta 5
- update desktop.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.5.2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.5.1m)
- update to 2.0beta4

* Tue Dec  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.4.1m)
- update to 2.0beta3

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.3.1m)
- update to 2.0beta2

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.2.1m)
- update to 2.0beta1

* Wed Jun  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.1.3m)
- add BuildPreReq: kdebase-workspace-devel

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.1.2m)
- rebuild against qt-4.4.0-1m

* Fri Apr 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.1.1m)
- update to version 2.0 Alpha 5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-0.20080123.2m)
- rebuild against gcc43

* Wed Jan 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080123.1m)
- update to 20080123 svn snapshot for KDE4
- update desktop.patch

* Tue Nov 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- version 1.0.2

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- import large icons from Fedora

* Thu Jul 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1

* Mon Apr 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-0.1.1m)
- initial package for Momonga Linux
