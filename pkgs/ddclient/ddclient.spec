%global momorel 1

Summary: ddclient is a Perl client for updating DynDNS information
Name: ddclient
Version: 3.8.2
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPL
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: ddclient.rwtab
Source2: ddclient.service
Source3: ddclient.sysconfig
URL: http://ddclient.sourceforge.net/
BuildArch: noarch
Requires(post): chkconfig
Requires(postun): initscripts chkconfig
Requires: perl
Requires: perl-IO-Socket-SSL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ddclient is a Perl client used to update dynamic DNS entries for accounts
on many dynamic DNS services.

%prep
%setup -q -n %{name}-%{version}

# Move pid file location for running as non-root.
sed -e 's|/var/run/ddclient.pid|%{_localstatedir}/run/%{name}/%{name}.pid|' -i sample-etc_ddclient.conf

# Send less mail by default, eg. not on every shutdown.
sed -e 's|^mail=|#mail=|' -i sample-etc_ddclient.conf

# http://sourceforge.net/forum/forum.php?forum_id=706446
sed -e 's|"3\.7\.1"|"3.7.2"|' -i %{name}

# Backwards compatibility from pre-3.6.6-1
sed -e 's|/etc/ddclient/|%{_sysconfdir}/|' -i %{name}

%build

%install
mkdir -p %{buildroot}{%{_sbindir},%{_sysconfdir}/{rwtab.d,sysconfig}}
mkdir -p %{buildroot}%{_unitdir}
install -p -m 755 %{name} %{buildroot}%{_sbindir}/%{name}
install -p -m 600 sample-etc_ddclient.conf %{buildroot}%{_sysconfdir}/%{name}.conf
install -p -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/rwtab.d/%{name}
install -p -m 755 %{SOURCE2} %{buildroot}%{_unitdir}/%{name}.service
install -p -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
mkdir -p %{buildroot}%{_localstatedir}/{cache,run}/%{name}
touch %{buildroot}%{_localstatedir}/cache/%{name}/%{name}.cache

# Correct permissions for later usage in %%doc
chmod 644 sample-*

%clean
rm -rf %{buildroot}

%pre
getent group %{name} > /dev/null || %{_sbindir}/groupadd -r %{name}
getent passwd %{name} > /dev/null || %{_sbindir}/useradd -r -g %{name} -d %{_localstatedir}/cache/%{name} -s /sbin/nologin -c "Dynamic DNS Client" %{name}
exit 0

%post
#/sbin/chkconfig --add %{name}
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
#if [ $1 -eq 0 ]; then
#  /sbin/service %{name} stop > /dev/null 2>&1 || :
#  /sbin/chkconfig --del %{name}
#fi
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable ddclient.service > /dev/null 2>&1 || :
    /bin/systemctl stop ddclient.service > /dev/null 2>&1 || :
fi


%postun
#if [ $1 -ne 0 ]; then
#  /sbin/service %{name} condrestart > /dev/null 2>&1 || :
#fi
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart ddclient.service >/dev/null 2>&1 || :
fi

%triggerun -- ddclient < 3.8.1-1
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply ddclient
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save ddclient >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del ddclient >/dev/null 2>&1 || :
/bin/systemctl try-restart ddclient.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc README* COPYING COPYRIGHT Changelog sample-etc_ppp_ip-up.local
%doc sample-etc_dhclient-exit-hooks sample-etc_cron.d_ddclient
%doc sample-ddclient-wrapper.sh sample-etc_dhcpc_dhcpcd-eth0.exe
%{_unitdir}/%{name}.service
%attr(600,%{name},%{name}) %config(noreplace) %{_sysconfdir}/%{name}.conf
%config(noreplace) %{_sysconfdir}/rwtab.d/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%{_sbindir}/%{name}
%attr(0700,%{name},%{name}) %dir %{_localstatedir}/cache/%{name}/
%attr(0600,%{name},%{name}) %ghost %{_localstatedir}/cache/%{name}/%{name}.cache
%attr(0755,%{name},%{name}) %dir %{_localstatedir}/run/%{name}/

%changelog
* Fri Apr 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.2-1m)
- update to 3.8.2

* Fri Mar 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.3-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.3-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.3-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.3-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.7.3-2m)
- rebuild against perl-5.10.0-1m

* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.3-1m)
- update to 3.7.3
- do not use %%NoSource macro

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.1-1m)
- update to 3.7.1
- modify patch

* Tue May 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.6.7-2m)
- update patch (sorry)

* Tue May 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.6.7-1m)
- update to 3.6.7
- change URL

* Sat Jul  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.6.3-2m)
- stop daemon

* Sun Jul 13 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.3-1m)
- update to 3.6.3
- mv sample-etc_dhclient-enter-hooks sample-etc_dhclient-exit-hooks
- change Source0 and URL (thanks to SIVA [Momonga-devel.ja:01632])
- No Nosource

* Sat Apr 20 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.6.2-2k)
- Kondara Project start.
- made from ddclient 3.6.1-4cl ( http://snapshot.conectiva.com/SRPMS/Networking/ddclient.html ) at Conectiva Linux SRPM packages (http://snapshot.conectiva.com/SRPMS/)

* Sat Jan 05 2002 Rodrigo Missiaggia <rodrigom@conectiva.com>
+ ddclient-3.6.1-4cl
- Changed chkconfig to 99 01 in /etc/rc.d/init.d/ddclient
- Changed echo to gprintf in /etc/rc.d/init.d/ddclient

* Sat Dec 29 2001 Roberto Selbach Teixeira <maragato@conectiva.com>
+ ddclient-3.6.1-3cl
- don't replace configuration file
- config file not world readable

* Fri Dec 28 2001 Roberto Selbach Teixeira <maragato@conectiva.com>
+ ddclient-3.6.1-2cl
- added patch for Alcatel Speed Touch Pro

* Mon Aug 27 2001 Claudio Matsuoka <claudio@conectiva.com>
+ ddclient-2.3.7-4cl
- dependency to perl changed to perl-bin (closes: #4271)

* Wed Nov  8 2000 Claudio Matsuoka <claudio@conectiva.com>
+ ddclient-2.3.7-3cl
- added Steve Greenland's man page for ddclient.

* Tue Nov  7 2000 Andreas Hasenack <andreas@conectiva.com>
+ ddclient-2.3.7-2cl
- do not restart samba
- do not use some strange proxy by default
- fixed dynamic IP detection (as long as ifconfig's output does not change
  in the future...)
- the ddupdate script no longer tries to use ddclient in /usr/local
- ddclient and ddupdate now are 755 instead of 700

* Tue Nov  7 2000 Claudio Matsuoka <claudio@conectiva.com>
+ ddclient-2.3.7-1cl
- package created
