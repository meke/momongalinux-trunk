%global momorel 1

%define pkgname xauth

Summary: X.Org X11 X authority utilities
Name: xorg-x11-%{pkgname}
Version: 1.0.9
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
Source10: mkxauth
Source11: mkxauth.man
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXext-devel
BuildRequires: libXmu-devel

Obsoletes: XFree86-xauth, mkxauth
# NOTE: xauth moved from the XFree86 package to XFree86-xauth in
# XFree86-4.2.0-50.11, so this Conflicts line is required for upgrades
# from RHL 8 and older, and RHEL 2.1 to work properly when upgrading to
# a newer OS release.
Conflicts: XFree86 < 4.2.0-50.11

%description
xauth is used to edit and display the authorization information
used in connecting to an X server.

%prep
%setup -q -n xauth-%{version}

%build
%configure
make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
# Install mkxauth
{
   install -m 755 %{SOURCE10} $RPM_BUILD_ROOT%{_bindir}/
   install -m 644 %{SOURCE11} $RPM_BUILD_ROOT%{_mandir}/man1/mkxauth.1
}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/xauth
%{_bindir}/mkxauth
%{_mandir}/man1/xauth.1*
%{_mandir}/man1/mkxauth.1*

%changelog
* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-2m)
- rebuild against gcc43

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- %%NoSource -> NoSource

* Wed Jan 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-1
- Updated to xauth 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1:1.0.0-1
- Updated to xauth 1.0.0 from X11R7 RC4
- Changed manpage dir from man1x to man1 to match upstream default.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.2-1
- Updated to xauth 0.99.2 from X11R7 RC2
- Added Epoch 1 to package, to be able to change the version number from the
  X11R7 release number to the actual twm version.
- Rename mkxauth manpage to mkxauth.1x

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-3
- Updated to xauth 0.99.1 from X11R7 RC1
- Change manpage location to 'man1x' in file manifest

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-2
- Use Fedora-Extras style BuildRoot tag
- Update BuildRequires to use new library package names

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-1
- Initial build.
