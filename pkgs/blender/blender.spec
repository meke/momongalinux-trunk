%global momorel 3
%global geckover 1.9.2
%global srcver 2.69
%global pythonver 3.4
%global __python %{_bindir}/python3
%global boost_version 1.55.0

Summary: 3D modeling, animation, rendering and post-production
Name: blender
Version: 2.69
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.blender.org/
#Source0: http://download.blender.org/source/%{name}-%{version}.tar.gz
Source0: http://download.blender.org/source/%{name}-%{srcver}.tar.gz
NoSource: 0
#Source1: %%{name}.png
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: VLGothic-fonts
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: OpenImageIO-devel
BuildRequires: SDL-devel >= 1.2.7-11m
#BuildRequires: xulrunner-devel >= %{geckover}
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: freeglut-devel
BuildRequires: freetype-devel
BuildRequires: ftgl
BuildRequires: gettext-devel >= 0.18.1.1
BuildRequires: ilmbase >= 1.0.3
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: openmpi-devel
BuildRequires: glew-devel >= 1.10.0
BuildRequires: openjpeg-devel >= 1.5.0
BuildRequires: cmake >= 2.8
BuildRequires: zlib-devel
BuildRequires: xz-devel
BuildRequires: ffmpeg-devel
BuildRequires: python3-devel >= %{pythonver}

%description
Blender is an open source software for 3D modeling, animation, rendering,
post-production, interactive creation and playback.

%prep
#%setup -q -n %{name}-%{version}
%setup -q -n %{name}-%{srcver}

## workaround...
#pushd extern/xvidcore/src
#find . -type d -exec cp -v nasm.inc '{}' \;
#popd

%build
# for ftfont build
#ln -s %%{_includedir}/freetype2/freetype source/%%{name}/ftfont/
#ln -s %%{_includedir}/ft2build.h source/%%{name}/ftfont/

if [ -e build ]; then
  echo "Directory 'build' already exists."
  exit 1
fi

%{__mkdir} build
pushd build
#cmake .. -DPYTHON_VERSION=%{pythonver} \
#         -DPYTHON_INCLUDE_DIR=%{_includedir}/python%{pythonver}mu \
#         -DPYTHON_LIBRARY='' \
#         -DBLENDER_LINK_LIBS=python%{pythonver}mu \
#         -DFFMPEG_INCLUDE_DIRS=%{_includedir}/ffmpeg \
#         -DWITH_CODEC_FFMPEG=ON -DWITH_SDL=ON -DWITH_FFTW3=ON \
##         -DCMAKE_INSTALL_PREFIX=%{_prefix} \
#         -DWITH_PYTHON_INSTALL=OFF \
#         -DWITH_BUILTIN_GLEW=OFF -DWITH_INSTALL_PORTABLE=OFF

export CFLAGS="$RPM_OPT_FLAGS -fPIC -funsigned-char -fno-strict-aliasing"
export CXXFLAGS="$CFLAGS"
cmake .. -DCMAKE_INSTALL_PREFIX=%{_prefix} \
 -DCMAKE_SKIP_RPATH=ON \
 -DBUILD_SHARED_LIBS=OFF \
 -DWITH_FFTW3:BOOL=ON \
 -DWITH_JACK:BOOL=ON \
 -DWITH_CODEC_SNDFILE:BOOL=ON \
 -DWITH_IMAGE_OPENJPEG:BOOL=ON \
 -DWITH_OPENCOLLADA:BOOL=ON \
 -DOPENCOLLADA=%{_includedir} \
 -DPYTHON_VERSION=%{pythonver} \
 -DPYTHON_INCLUDE_DIR=%{_includedir}/python%{pythonver}m \
 -DWITH_PYTHON:BOOL=ON \
 -DWITH_PYTHON_INSTALL:BOOL=OFF \
 -DWITH_CODEC_FFMPEG:BOOL=ON \
 -DWITH_GAMEENGINE:BOOL=ON \
 -DWITH_CXX_GUARDEDALLOC:BOOL=OFF \
 -DWITH_BUILTIN_GLEW=OFF \
 -DWITH_INSTALL_PORTABLE=OFF \
 -DWITH_PYTHON_SAFETY=ON \
 -DWITH_PLAYER=ON

%make

popd
# %%{__make}

%install
%{__rm} --preserve-root -fr %{buildroot}

pushd build
make install DESTDIR=%{buildroot}

#pushd ../install/linux2/


#mkdir -p %%{buildroot}%%{_bindir}
#install %%{name} %%{buildroot}%%{_bindir}/
#
#cp -p *.txt ../../%%{name}-%%{version}/
#
#rm -rf %%{buildroot}%%{_datadir}/%%{name}/locale
#mkdir -p %%{buildroot}%%{_datadir}
#cp -a .%%{name}/locale %%{buildroot}%%{_datadir}/

## install Blanguages file
# cp -f ./bin/.%{name}/.Blanguages %%{buildroot}%%{_datadir}/%%{name}/Blanguages
## make symlink of default fonts

#%%{__rm} -f %%{buildroot}%%{_datadir}/%%{name}/%%{version}/.bfont.ttf
#ln -s %%{_datadir}/fonts/vlgothic/VL-Gothic-Regular.ttf %%{buildroot}%%{_datadir}/%%{name}/%%{version}/.bfont.ttf

# install icon
#mkdir -p %%{buildroot}%%{_datadir}/pixmaps
#install -m 644 %%{SOURCE1} %%{buildroot}%%{_datadir}/pixmaps/
#
## install desktop file
#cat >%%{name}.desktop<<EOF
#[Desktop Entry]
#Name=%%{name}
#Terminal=false
#Type=Application
#Description=3D Modeler
#Exec=%%{name} -w
#Icon=%%{name}
#Categories=Graphics;
#EOF
#
#mkdir -p %%{buildroot}%%{_datadir}/applications
#install -m 644 %%{name}.desktop %%{buildroot}%%{_datadir}/applications/

popd 

#%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && %__rm -fr %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

#%%files -f %{name}.lang
%files
%defattr(-,root,root,-)
%doc COPYING
#%%doc GPL-license.txt Python-license.txt copyright.txt release_*.txt
%{_bindir}/%{name}
%{_bindir}/%{name}player
%{_bindir}/%{name}-thumbnailer.py
%{_datadir}/applications/%{name}.desktop
# %%{_datadir}/%%{name}/Blanguages
%dir %{_datadir}/%{name}/%{version}
#%{_datadir}/%{name}/%{version}/.bfont.ttf
#%{_datadir}/%{name}/%{version}/.Blanguages
%dir %{_datadir}/%{name}/%{version}/datafiles
%dir %{_datadir}/%{name}/%{version}/datafiles/colormanagement
%dir %{_datadir}/%{name}/%{version}/datafiles/colormanagement/*
%dir %{_datadir}/%{name}/%{version}/datafiles/fonts
%dir %{_datadir}/%{name}/%{version}/datafiles/fonts/*
%dir %{_datadir}/%{name}/%{version}/datafiles/locale
%dir %{_datadir}/%{name}/%{version}/datafiles/locale/*
%{_datadir}/%{name}/%{version}/datafiles/colormanagement
%{_datadir}/%{name}/%{version}/datafiles/locale
%{_datadir}/%{name}/%{version}/scripts
%{_datadir}/icons/*/*/apps/%{name}.png
%{_datadir}/icons/*/*/apps/%{name}.svg
%{_mandir}/man1/%{name}.1*
%docdir %{_docdir}/%{name}
%{_docdir}/%{name}

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.69-3m)
- rebuild against ffmpeg

* Sat Jan 11 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.69-2m)
- add BuildRequires
- rebuild against boost-1.55.0

* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.69-1m)
- update 2.69

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.63-8m)
- rebuild against glew-1.10.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.63-7m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.63-6m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.63-5m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.63-4m)
- rebuild for glew-1.9.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.63-3m)
- rebuild for boost

* Fri Jul 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.63-2m)
- rebuild against boost-1.50.0

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.63-1m)
- update to 2.63a

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.61-3m)
- rebuild against libtiff-4.0.1
- rebuild against openjpeg-1.5.0

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.61-2m)
- rebuild against glew-1.7.0

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.61-1m)
- update to 2.61

* Mon Oct 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.60-1m)
- update to 2.60

* Wed Sep 21 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (2.59-1m)
- update to 2.59
- add ffmpeg include dir patch
- use cmake for build

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.49b-6m)
- rebuild against python-2.7

* Fri Apr 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.49b-5m)
- add gcc46.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49b-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49b-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.49b-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.49b-1m)
- update to 2.49b

* Sat Jul  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.49a-9m)
- rebuild against gettext-0.18.1.1

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.49a-8m)
- fix the path to VL-Gothic-Regular.ttf

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.49a-7m)
- fix up desktop file

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.49a-6m)
- rebuild against gettext-0.18

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.49a-5m)
- rebuild against libjpeg-8a

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.49a-4m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.49a-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.49a-2m)
- rebuild against libjpeg-7

* Mon Jul  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.49a-1m)
- update 2.49a

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48a-3m)
- rebuild against xulrunner-1.9.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48a-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.48a-1m)
- update 2.48a
- add patches
  Patch0: blender-sanitize-sys.path-48a.patch
  Patch1: blender-2.48a-python26.patch

* Fri Nov  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.47-2m)
- [SECURITY] CVE-2008-4863
- import a security patch from Debian unstable (2.46+dfsg-6)

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.47-1m)
- update 2.47

* Tue Sep 02 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.46-2m)
- use VLGothic-fonts

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.46-1m)
- [SECURITY] CVE-2008-1102
- update 2.46

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.45-8m)
- rebuild against firefox-3

* Mon Apr 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.45-7m)
- support scons-0.98.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.45-6m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.45-5m)
- rebuild against OpenEXR-1.6.1

* Wed Feb 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.45-4m)
- import gcc43 patch from Fedora devel

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.45-3m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.45-2m)
- rebuild against gettext-0.17

* Tue Oct 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.45-1m)
- update 2.45

* Tue May 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.44-1m)
- update 2.44

* Sun Apr 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.42a-4m)
- rebuild against gettext-0.16.1

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.42a-3m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.42a-2m)
- require higher version of scons

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.42a-1m)
- update 2.42a

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.42-5m)
- rebuild against OpenEXR-1.4.0

* Mon Nov 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.42-5m)
- copy from momonga's trunk (r12695)
- use firefox-devel instead of mozilla-devel

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.42-4m)
- rebuild against freeglut

* Thu Sep 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.42-3m)
- rebuild against gettext-0.15

* Sun Jul 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.42-2m)
- WITH_BF_OPENAL='false'

* Sun Jul 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.42-1m)
- update 2.42

* Mon Dec 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.40-1m)
- update 2.40
- BuildPrereq: scons >= scons-0.96.91
- BuildPrereq: mozilla-devel

* Mon Nov  7 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.32-11m)
- add gcc4 patch.
- Patch8: blender-2.32-gcc4.patch

* Sat Nov  5 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.32-10m)
- rebuild against python-2.4.2-3m

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.32-9m)
- rebuild against SDL-1.2.7-11m

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.32-8m)
- rebuild against SDL

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.32-7m)
- enable x86_64.

* Sat Dec 25 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.32-6m)
- add FTGL patch

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.32-5m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++
- add gcc34 patch

* Mon Aug 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32-4m)
- remove unnecessary desktop file

* Mon Aug 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.32-3m)
- rebuild against DirectFB-0.9.21

* Thu Jul  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.32-2m)
- use sazanami instead of kochi

* Mon Feb 16 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.32-1m)
- update to 2.32
- revise Patch1

* Wed Jan 14 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.31a-3m)
- use %%find_lang macro
- use %%NoSource macro
- use autoconf-1.7
    See doc/autoconfig.txt
- and remove blender-2.31a-intern_ghost_automake.patch

* Wed Jan 14 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.31a-2m)
- enable international menu (needs ftgl libraries)
- - add Patch1,2 to build blender with source/blender/src/ftfont module 
    and use automake, autoconf
- - new Patch0 (patching Makefile.am)
- - add Patch3 to change default font and .Blanguage file path
- - - set default font path to /usr/share/blender/fonts
- - - set default font to /usr/share/blender/fonts/bfonts.ttf and 
      make it symlink to /usr/share/fonts/truetype/japanese/kochi-gothic.ttf)
- - - change name of .Blanguage file to /usr/share/blender/Blanguage
- - use latest ja.po at http://uniside.s37.xrea.com/ 
- - - (add Patch4 to remove duplicated entries)
- add some BuildRequires
- add desktop file

* Fri Jan 9 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.31a-1m)
- initial momonga rpm


