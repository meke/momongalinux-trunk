%global momorel 8

Name:		cdlabelgen
Summary:	Generates frontcards and traycards for inserting in CD jewelcases.
Version:	4.1.0
Release:	%{momorel}m%{?dist}
Source0:        http://www.aczoom.com/pub/tools/%{name}-%{version}.tgz
NoSource:	0
URL:		http://www.aczoom.com/tools/cdinsert/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License:	GPL
Group:		Applications/Publishing
BuildArch:	noarch

%description
Cdlabelgen is a utility which generates frontcards and traycards (in
PostScript(TM) format) for CD jewelcases.

%prep
%setup -q

%build
pod2man cdlabelgen.pod > cdlabelgen.1
pod2man cdlabelgen.pod > cdlabelgen.html

%install
mkdir -p %{buildroot}{%{_bindir},%{_datadir}/cdlabelgen,%{_mandir}/man1}
install -m 755 cdlabelgen %{buildroot}%{_bindir}
install -m 644 postscript/* %{buildroot}%{_datadir}/cdlabelgen
install -m 644 cdlabelgen.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc ChangeLog README INSTALL INSTALL.WEB cdinsert.pl
%defattr(-,root,root,-)
%{_bindir}/cdlabelgen
%{_datadir}/cdlabelgen
%{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.0-8m)
- rebuild for new GCC 4.6

* Sun Feb 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-7m)
- source was changed again

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.0-5m)
- full rebuild for mo7 release

* Sun Jan 17 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.1.0-4m)
- src changed

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.0-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.0-2m)
- %%NoSource -> NoSource

* Mon Dec 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.6.0-1m)
- up to 3.6.0
- change URI

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.0-2m)
- revised docdir permission

* Sun May 30 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.0-1m)
- verup

* Sat Oct  4 2003 Toru Hoshina <t@momonga-linux.org>
- (2.6.1-2m)
- tar ball was revised at primary site.

* Wed Jul 30 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Fri Mar 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.5-1m)
- version 0.2.5

* Wed May 1 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.5-8k)
- Fix source URL

* Tue Jan 18 2000 Yoshimitsu Okamoto <umasan@wc4.so-net.ne.jp>
- Remake to No SRC.

* Mon Aug 23 1999 Preston Brown <pbrown@redhat.com>
- adopted for Powertools 6.1.
