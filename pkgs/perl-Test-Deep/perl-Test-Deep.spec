%global         momorel 3

Name:           perl-Test-Deep
Version:        0.112
Release:        %{momorel}m%{?dist}
Summary:        Extremely flexible deep comparison
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Test-Deep/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Test-Deep-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Scalar-Util >= 1.09
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-NoWarnings >= 0.02
BuildRequires:  perl-Test-Tester >= 0.04
Requires:       perl-Scalar-Util >= 1.09
Requires:       perl-Test-Simple
Requires:       perl-Test-NoWarnings >= 0.02
Requires:       perl-Test-Tester >= 0.04
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
If you don't know anything about automated testing in Perl then you should
probably read about Test::Simple and Test::More before preceding.
Test::Deep uses the Test::Builder framework.

%prep
%setup -q -n Test-Deep-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%{perl_vendorlib}/Test/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-2m)
- rebuild against perl-5.18.2

* Sun Dec 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-1m)
- update to 0.112

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110-1m)
- update to 0.110
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.109-1m)
- update to 0.109

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.108-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.108-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.108-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.108-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.108-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.108-1m)
- update to 0.108

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.107-1m)
- update to 0.107

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.106-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.106-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.106-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.106-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.106-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.106-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.106-1m)
- update to 0.106

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.104-1m)
- update to 0.104

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.103-2m)
- rebuild against rpm-4.6

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103-1m)
- update to 0.103

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.102-1m)
- update to 0.102

* Wed May 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.101-1m)
- update to 0.101

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100-1m)
- update to 0.100

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.099-3m)
- rebuild against gcc43

* Mon Oct 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.099-2m)
- update to 0.099

* Sun Oct 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.099-1m)
- update to 0.099_a1

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.098-1m)
- update to 0.098

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.096-2m)
- use vendor

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.096-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
