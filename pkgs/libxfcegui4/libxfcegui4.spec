%global momorel 1

%global xfcever 4.10
%global xfce4ver 4.10.0

Name: 		libxfcegui4
Version: 	4.10.0
Release:	%{momorel}m%{?dist}
Summary: 	GTK widgets for Xfce

Group: 		Development/Libraries
License:	LGPLv2+
URL: 		http://www.xfce.org/
Source0:	http://archive.xfce.org/src/archive/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:       0
#Patch0:	%{name}-4.4.2-xfce-exec-use-thunar.patch
Patch10:	libxfcegui4-libtool.patch
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  dbh-devel
BuildRequires: 	gtk2-devel
BuildRequires:  libxml2-devel >= 2.7.2
BuildRequires: 	pkgconfig
BuildRequires:	libxfce4util-devel >= %{xfce4ver}
#BuildRequires:	libxfce4util-devel >= 4.8.1
#BuildRequires:	xfconf-devel >= %{xfce4ver}
#BuildRequires:  xfce4-dev-tools >= %{xfce4ver}
BuildRequires:	glade-devel
BuildRequires:  gtk-doc
BuildRequires:  imake
BuildRequires:  libXt-devel
BuildRequires:  gettext
BuildRequires:  intltool

Requires: gtk2 >= 2.20
Requires: gnome-icon-theme
#Requires: xfce4-session >= %{xfce4ver}
# xfce4-sessin has %%dir %%{_sysconfdir}/xdg/xfce4/xfconf/xfce-perchannel-xml

%description
The package includes various gtk widgets for Xfce.

%package devel
Summary:	developpment tools for libxfcegui4 library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	gtk2-devel
Requires:	libxfce4util-devel
Requires:	glade-devel
Requires:	pkgconfig
Requires:	gtk-doc

%description devel
This package includes the static libraries and header files you will need
to compile applications for Xfce.

%prep
%setup -q
#%%patch0 -p1 -b .thunar
%patch10 -p1

libtoolize --force --copy
aclocal
autoconf
automake

autoreconf -vfi

%build
%configure --enable-xinerama --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -name "*.la" -delete

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING 
%{_libdir}/lib*.so.*
%{_libdir}/libglade/2.0/libxfce4.so
%{_datadir}/icons/hicolor/*/*/*.png
%exclude %{_datadir}/icons/hicolor/scalable/apps/xfce-filemanager.svg
# this file has gnome-icon-theme

%files devel
%defattr(-, root,root,-)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/xfce4/*
%doc %{_datadir}/gtk-doc/html/libxfcegui4/

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0
- add Source0 from fedora
- build against xfce4-4.10.0

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-4m)
- fix requires

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-3m)
- add BuildRequires
- change source url

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8.0-2m)
- goodbye glade3 modules

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.0-1m)
- update
- tmp comment out Patch1:		libxfcegui4-4.6.0-keyboard-shortcuts.patch
- tmp comment out Patch11:        libxfcegui4-4.6.4-autoconf.patch
- fix %%files

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.4-3m)
- rebuild for new GCC 4.5

* Thu Aug 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.4-2m)
- add BuildRequires

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.4-1m)
- update
- update Patch11

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-5m)
- revise BuildRequires

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.1-4m)
- add patch11 build fix

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-2m)
- arrange files

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.5.93-3m)
- rebuild against glade3-3.5.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update

* Sat Nov 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-2m)
- change BuildRequires: glade3 to glade3-devel

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update
- add --enable-final --enable-gtk-doc at %%configure
- add patch0
- delete --enable-final not exist in config
- files %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Wed Jan 31 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-2m)
- exclude %%{_datadir}/icons/hicolor/scalable/apps/xfce-filemanager.svg
  use gnome-icon-theme
- add Requires: gnome-icon-theme

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-2m)
- delete libtool library
- rebuild against libxfce4util-4.2.3.2-2m

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.2.2-2m)
- apply taskbar overflow patch <http://bugs.gentoo.org/show_bug.cgi?id=102912>

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)
- minor bugfixes

* Sun Jul 11 2004 Ryu SASAOKA <ryu@momonga-linux.org>
-(4.0.5-2m)
- add xfce4-icon-size.patch

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
-(4.0.5-1m)
- version update to 4.0.5.

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-3m)
- rebuild against for gtk+-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-2m)
- rebuild against for libxml2-2.6.8

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.4-1m)

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- version up to 4.0.3

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-1m)
- version up to 4.0.1

* Fri Sep 26 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (4.0.0-1m)
- version up

* Sat Sep 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.99.4-1m)
- XFce4 RC4 release
