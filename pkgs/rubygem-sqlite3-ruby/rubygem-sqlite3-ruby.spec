%global momorel 1

# Generated from sqlite3-ruby-1.2.5.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname sqlite3
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: This module allows Ruby programs to interface with the SQLite3 database engine (http://www.sqlite.org)
Name: rubygem-%{gemname}-ruby
Version: 1.3.8
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://sqlite3-ruby.rubyforge.org
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(mocha) >= 0
Requires: rubygem(rake-compiler) >= 0.5.0
Requires: rubygem(hoe) >= 2.3.2
BuildRequires: rubygems
BuildRequires: ruby >= 1.9.2
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem(%{gemname}-ruby) = %{version}
Provides: ruby-sqlite3 = %{version}-%{release}
Obsoletes: ruby-sqlite3 < %{version}-%{release}

%description
This module allows Ruby programs to interface with the SQLite3
database engine (http://www.sqlite.org).  You must have the
SQLite engine installed in order to build this module.
Note that this module is NOT compatible with SQLite 2.x.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/API_CHANGES.rdoc
%doc %{geminstdir}/ChangeLog.cvs
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/CHANGELOG.rdoc
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README.rdoc
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Aug 19 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.5-1m)
- update to 1.3.5

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-2m)
- Provides: rubygem(%%{gemname}-ruby) = %%{version}

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4
-- gemname has been renamed to "sqlite3"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-2m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-1m)
- update 1.3.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Mon Mar 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5 and use gem2rpm

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-3m)
- rebuild against gcc43

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-2m)
- Provides: ruby-sqlite

* Sun Feb  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.0-3m)
- rebuild against ruby-1.8.6-4m

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.0-2m)
- depend sqlite3 -> sqlite

* Mon Dec 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.0-1m)
- initial import to Momonga
