%global momorel 4

%global short_name commons-modeler

# TODO - Drop "jakarta-" from the package name
Name:          jakarta-%{short_name}
Version:       2.0.1
Release:       %{momorel}m%{?dist}
Summary:       Model MBeans utility classes
Group:         Development/Libraries
License:       "ASL 2.0"
URL:           http://commons.apache.org/modeler/

Source0:       http://archive.apache.org/dist/commons/modeler/source/%{short_name}-%{version}-src.tar.gz

# a maven2 pom is not supplied, so supply one based on the one from source control:
#  $ svn export -r 678495 http://svn.apache.org/repos/asf/commons/proper/modeler/trunk/pom.xml
Source1:       pom.xml

BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch

BuildRequires: java-devel >= 1.6.0
BuildRequires: jpackage-utils
BuildRequires: jakarta-commons-beanutils >= 1.6
BuildRequires: jakarta-commons-collections >= 2.1
BuildRequires: jakarta-commons-digester >= 1.4.1
BuildRequires: jakarta-commons-logging >= 1.0.4
BuildRequires: xml-commons-apis
BuildRequires: maven2-plugin-antrun
BuildRequires: maven2-plugin-assembly
BuildRequires: maven2-plugin-compiler
BuildRequires: maven2-plugin-idea
BuildRequires: maven2-plugin-install
BuildRequires: maven2-plugin-jar
BuildRequires: maven2-plugin-javadoc
BuildRequires: maven2-plugin-resources
BuildRequires: maven-doxia-sitetools
BuildRequires: maven-plugin-bundle
BuildRequires: maven-surefire-maven-plugin
BuildRequires: maven-surefire-provider-junit
Requires:      java
Requires:      jpackage-utils
Requires:      jakarta-commons-beanutils >= 1.6
Requires:      jakarta-commons-collections >= 2.1
Requires:      jakarta-commons-digester >= 1.4.1
Requires:      jakarta-commons-logging >= 1.0.4
Requires:      xml-commons-apis

Provides:      %{short_name} = %{version}-%{release}
Obsoletes:     %{short_name} < %{version}-%{release}

%description
Commons Modeler makes the process of setting up JMX (Java Management
Extensions) MBeans easier by configuring the required meta data using an
XML descriptor. In addition, Modeler provides a factory mechanism to
create the actual Model MBean instances.

%package javadoc
Summary:       Javadocs for %{name}
Group:         Documentation
Requires:      %{name} = %{version}-%{release}

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{short_name}-%{version}-src

cp -p %{SOURCE1} pom.xml

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

# TODO: disable tests for now because they need internet access -- they run fine locally
mvn-jpp -Dmaven.repo.local=$MAVEN_REPO_LOCAL -Dmaven.test.skip=true install javadoc:javadoc

%install
rm -rf %{buildroot}

# jars
install -pD -T target/%{short_name}-%{version}.jar \
  %{buildroot}%{_javadir}/%{short_name}-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|%{short_name}|%{name}|g"`; done)
(cd %{buildroot}%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadocs
mkdir -p %{buildroot}%{_javadocdir}/%{name}-%{version}
cp -pr target/site/apidocs/* %{buildroot}%{_javadocdir}/%{name}-%{version}
(cd %{buildroot}%{_javadocdir} && ln -sf %{name}-%{version} %{name})

# pom
install -pD -T -m 644 pom.xml %{buildroot}%{_mavenpomdir}/JPP-%{short_name}.pom
%add_to_maven_depmap %{short_name} %{short_name} %{version} JPP %{short_name}

%clean
rm -rf %{buildroot}

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc LICENSE.txt NOTICE.txt RELEASE-NOTES*
%{_mavendepmapfragdir}/*
%{_mavenpomdir}/*
%{_javadir}/*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- full rebuild for mo7 release

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-1m)
- sync with Fedora 13 (2.0.1-5)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-3jpp.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-3jpp.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-3jpp.2m)
- rebuild against gcc43

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-3jpp.1m)
- import from Fedora

* Wed Mar 21 2007 Matt Wringe <mwringe@redhat.com> - 0:2.0-3jpp.1
- Merge with lastest jpp version

* Wed Mar 21 2007 Matt Wringe <mwringe@redhat.com> - 0:2.0-3jpp
- Make provides and obsoletes versioned
- Fix eol encoding on various files

* Tue Mar 12 2007 Jason Corley <jason.corley@gmail.com> - 0:2.0-2jpp
- respin to upload to main repo
- fix rpmlint warnings

* Tue Feb 27 2007 Jason Corley <jason.corley@gmail.com> - 0:2.0-1jpp
- change copyright text to have current year
- minor aesthetic cleanups
- update to 2.0

* Thu Aug 10 2006 Matt Wringe <mwringe at redhat.com> - 0:1.1-8jpp.1
- Merge with upstream version:
 - Add missing javadoc requires
 - Add patches to properly set the classpath in build.xml and
   build.properties.default

* Thu Aug 10 2006 Karsten Hopp <karsten@redhat.de> 1.1-7jpp_3fc
- Requires(post/postun): coreutils

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:1.1-7jpp_2fc
- Rebuilt

* Wed Jul 19 2006 Matt Wringe <mwringe at redhat.com> - 0:1.1-7jpp_1fc
- Merged with upstream version

* Wed Jul 19 2006 Matt Wringe <mwringe at redhat.com> - 0:1.1-7jpp
- Removed separate definition of name, version and release

* Mon Jul 17 2006 Matt Wringe <mwringe at redhat.com> - 0:1.1-6jpp
- Added contional native compilation.

* Wed Apr 26 2006 Fernando Nasser <fnasser@redhat.com> - 0:1.1-5jpp
- First JPP 1.7 build

* Thu Feb 24 2005 David Walluck <david@jpackage.org> 0:1.1-4jpp
- add missing epochs to dependencies
- add non-versioned javadoc symlink
- fix file permissions

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.1-3jpp
- Rebuild with ant-1.6.2
* Thu Oct 09 2003 Henri Gomez <hgomez at users.sourceforge.net> 0:1.1-2jpp
- fix junit typo (reported by Kaj J. Niemi)

* Wed Oct 08 2003 Henri Gomez <hgomez at users.sourceforge.net> 0:1.1-1jpp
- modeler 1.1

* Tue Mar 25 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> 1.0-6jpp
- for jpackage-utils 1.5

* Thu Feb 27 2003 Henri Gomez <hgomez@users.sourceforge.net> 1.0-5jpp
- fix ASF license

* Thu Feb 27 2003 Henri Gomez <hgomez@users.sourceforge.net> 1.0-4jpp
- fix missing packager tag
- fix new mx4j jar name (mx4j-jmx.jar)

* Fri Jul 12 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0-3jpp
- updated requires for jaxp_parser_impl and xml-commons-apis

* Mon Jun 10 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0-2jpp
- use sed instead of bash 2.x extension in link area to make spec compatible
  with distro using bash 1.1x

* Fri Jun 07 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0-1jpp 
- 1.0
- added short names in %%{_javadir}, as does jakarta developpers
- first jPackage release
