%global momorel 3

%define bescachedir %{_localstatedir}/cache/%{name}
%define bespkidir %{_sysconfdir}/pki/%{name}
%define beslogdir %{_localstatedir}/log/%{name}
%define besuser %{name}
%define besgroup %{name}

Name:           bes
Version:        3.9.0
Release:        %{momorel}m%{?dist}
Summary:        Back-end server software framework for OPeNDAP

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.opendap.org/download/BES.html
Source0:        http://www.opendap.org/pub/source/bes-%{version}.tar.gz
NoSource:       0
Patch2:         %{name}-3.8.4-openssl.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libdap-devel >= 3.11.0
BuildRequires:  readline-devel
# needed by ppt
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  doxygen graphviz
BuildRequires:  pkgconfig
Requires:       bzip2 gzip
Requires(pre):  shadow-utils

%description
BES is a new, high-performance back-end server software framework for 
OPeNDAP that allows data providers more flexibility in providing end 
users views of their data. The current OPeNDAP data objects (DAS, DDS, 
and DataDDS) are still supported, but now data providers can add new data 
views, provide new functionality, and new features to their end users 
through the BES modular design. Providers can add new data handlers, new 
data objects/views, the ability to define views with constraints and 
aggregation, the ability to add reporting mechanisms, initialization 
hooks, and more.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       libdap-devel >= 3.7.10
# in bes-config --libs
Requires:       openssl-devel zlib-devel
# for the /usr/share/aclocal directory ownership
Requires:       automake
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        doc
Summary:        Documentation of the OPeNDAP BES
Group:          Documentation

%description    doc
Documentation of OPeNDAP BES.

%prep
%setup -q
%patch2 -p1 -b .openssl100
chmod a-x dispatch/BESStreamResponseHandler*

%build
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}

make docs
rm -rf __distribution_docs
cp -pr docs __distribution_docs
mv __distribution_docs/html __distribution_docs/api-html
# .map and .md5 files are of dubious use
rm __distribution_docs/api-html/*.map
rm __distribution_docs/api-html/*.md5
chmod a-x __distribution_docs/BES_*.doc

sed -i.dist -e 's:=/tmp:=%{bescachedir}:' \
  -e 's:=.*/bes.log:=%{beslogdir}/bes.log:' \
  -e 's:=/full/path/to/serverside/certificate/file.pem:=%{bespkidir}/cacerts/file.pem:' \
  -e 's:=/full/path/to/serverside/key/file.pem:=%{bespkidir}/public/file.pem:' \
  -e 's:=/full/path/to/clientside/certificate/file.pem:=%{bespkidir}/cacerts/file.pem:' \
  -e 's:=/full/path/to/clientside/key/file.pem:=%{bespkidir}/public/file.pem:' \
  -e 's:=user_name:=%{besuser}:' \
  -e 's:=group_name:=%{besgroup}:' \
  dispatch/bes/bes.conf

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
mkdir -p %{buildroot}%{bescachedir}
mkdir -p %{buildroot}%{bespkidir}/{cacerts,public}
mkdir -p %{buildroot}%{beslogdir}
mv %{buildroot}%{_bindir}/bes-config-pkgconfig %{buildroot}%{_bindir}/bes-config

%clean
rm -rf %{buildroot}

%pre
getent group %{besgroup} >/dev/null || groupadd -r %{besgroup}
getent passwd %{besuser} >/dev/null || \
useradd -r -g %{besuser} -d %{beslogdir} -s /sbin/nologin \
    -c "BES daemon" %{besuser}
exit 0

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog NEWS README
%dir %{_sysconfdir}/bes/
%config(noreplace) %{_sysconfdir}/bes/bes.conf
%config(noreplace) %{_sysconfdir}/bes/modules/dap.conf
%dir %{_datadir}/bes/
%{_datadir}/bes/*.html
%{_datadir}/bes/*.txt
%{_datadir}/bes/*.xml
%{_bindir}/beslistener
%{_bindir}/besd
%{_bindir}/besdaemon
%{_bindir}/besctl
%{_bindir}/hyraxctl
%{_bindir}/besregtest
%{_bindir}/bescmdln
%{_bindir}/besstandalone
%{_libdir}/*.so.*
%{_libdir}/bes/
%{bescachedir}
%{bespkidir}/
%attr (-,%{besuser},%{besgroup}) %{beslogdir}

%files devel
%defattr(-,root,root,-)
%doc __distribution_docs/BES_*.doc
%{_bindir}/besCreateModule
%{_bindir}/bes-config
%{_includedir}/bes/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/bes/templates/
%{_datadir}/aclocal/bes.m4

%files doc
%defattr(-,root,root,-)
%doc __distribution_docs/api-html/

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.9.0-3m)
- rebuild against graphviz-2.36.0-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.0-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.0-1m)
- rebuild against libdap-3.11.0
- update to 3.9.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.4-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.4-1m)
- rebuild against libdap-3.10.2
- update to 3.8.4

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.2-8m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-7m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-6m)
- apply openssl100 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-4m)
- rebuild against openssl-0.9.8k

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2
- rebuild against libdap-3.8.2

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-2m)
- BuildRequires: libdap-devel >= 3.8.0

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1
- go to main

* Sun May 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.3-1m)
- import from Fedora to Momonga

* Wed Jan  2 2008 Patrice Dumas <pertusus@free.fr> 3.5.3-3
- Add Requires openssl-devel and zlib-devel since it is in bes-config --libs

* Mon Dec 17 2007 Patrice Dumas <pertusus@free.fr> 3.5.3-2
- update to 3.5.3

* Thu Dec 06 2007 Release Engineering <rel-eng at fedoraproject dot org> - 3.5.1-4
 - Rebuild for deps

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 3.5.1-3
- Rebuild for selinux ppc32 issue.

* Fri Jun  8 2007 Patrice Dumas <pertusus@free.fr> 3.5.1-2
- BuildRequires graphviz

* Sun Jun  3 2007 Patrice Dumas <pertusus@free.fr> 3.5.1-1
- update to 3.5.1

* Fri Dec  8 2006 Patrice Dumas <pertusus@free.fr> 3.2.0-2
- set License to LGPL

* Fri Dec  8 2006 Patrice Dumas <pertusus@free.fr> 3.2.0-2
- add BuildRequires for readline-devel and openssl-devel

* Sat Jul 22 2006 Patrice Dumas <pertusus@free.fr> 3.2.0-1
- initial packaging
