%global momorel 1

Summary: Advanced Linux Sound Architecture (ALSA) - Library
Name: alsa-lib
Version: 1.0.28
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.alsa-project.org/
Source0: ftp://ftp.alsa-project.org/pub/lib/%{name}-%{version}.tar.bz2
NoSource: 0
Source10: asound.conf
Patch0: %{name}-1.0.24.1-remove-gid.patch
Patch1: %{name}-1.0.14-glibc-open.patch
Patch2: %{name}-1.0.16-no-dox-date.patch
## upstream patches
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: coreutils
BuildRequires: autoconf >= 2.58
BuildRequires: automake
BuildRequires: doxygen
BuildRequires: pkgconfig
BuildRequires: python >= 2.7

%description
This document is probably old!!
Advanced Linux Sound Architecture (ALSA) - Library

Features
========

* general
  - modularized architecture with support for 2.0 and latest 2.1 kernels
  - support for versioned and exported symbols
  - full proc filesystem support - /proc/sound
* ISA soundcards
  - support for 128k ISA DMA buffer
* mixer
  - new enhanced API for applications
  - support for unlimited number of channels
  - volume can be set in three ways (percentual (0-100), exact and decibel)
  - support for mute (and hardware mute if hardware supports it)
  - support for mixer events
    - this allows two or more applications to be synchronized
* digital audio (PCM)
  - new enhanced API for applications
  - full real duplex support
  - full duplex support for SoundBlaster 16/AWE soundcards
  - digital audio data for playback and record should be read back using
    proc filesystem
* OSS/Lite compatibility
  - full mixer compatibity
  - full PCM (/dev/dsp) compatibility

%package devel
Summary: Static libraries and header files from the ALSA library.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The Advanced Linux Sound Architecture (ALSA) provides audio and MIDI
functionality to the Linux operating system.

This package includes the ALSA development libraries for developing
against the ALSA libraries and interfaces.

%prep
%setup -q

%patch0 -p1 -b .gid
%patch1 -p1 -b .glibc-open
%patch2 -p1 -b .no-dox-date

# preparing
libtoolize -c -f
aclocal
automake --add-missing
autoconf

%build
%configure --disable-aload --disable-alisp
%make
make doc

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'

mkdir -p %{buildroot}/%{_lib}

# install global configuration file
mkdir -p %{buildroot}%{_sysconfdir}
install -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/

# DO NOT REMOVE la files
# find %%{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING ChangeLog INSTALL TODO doc/asoundrc.txt
%config %{_sysconfdir}/asound.conf
%{_libdir}/libasound.so.*
%{_bindir}/aserver
%{_libdir}/alsa-lib
%{_datadir}/alsa

%files devel
%defattr(-,root,root)
%doc MEMORY-LEAK NOTES doc/doxygen
%{_includedir}/alsa
%{_includedir}/sys/asoundlib.h
%{_libdir}/pkgconfig/alsa.pc
%exclude %{_libdir}/libasound.la
%{_libdir}/libasound.so
%{_datadir}/aclocal/alsa.m4

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.28-1m)
- version 1.0.28

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.27.2-2m)
- support UserMove env

* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.27.2-1m)
- version 1.0.27.2
- update 1.0.27.2

* Fri May 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.27.1-1m)
- version 1.0.27.1
- remove merged upstream patch

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.27-2m)
- apply upstream bug fix patch

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.27-1m)
- update to 1.0.27

* Thu Sep 27 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.26-1m)
- update to 1.0.26

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.25-1m)
- version 1.0.25
- remove static link library (libasound.a)

* Sat Jun 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24.1-1m)
- version 1.0.24.1
- update remove-gid.patch

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.23-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.23-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.23-1m)
- version 1.0.23

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.22-2m)
- use Requires

* Thu Dec 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.22-1m)
- version 1.0.22

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-1m)
- version 1.0.20

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.19-1m)
- version 1.0.19
-- need pulseaudio

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.18-4m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.18-3m)
- update Patch0 for fuzz=0

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.18-2m)
- rebuild against python-2.6.1-2m

* Fri Dec  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-1m)
- version 1.0.18
- move configuration files from %%{_sysconfdir}/alsa to %%{_datadir}/alsa again
- disable /dev/aload device checking
- import asound.conf from Fedora
- clean up patches
- License: LGPLv2+

* Tue Sep 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17a-2m)
- import 2 upstream patches from F9-updates

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17a-1m)
- version 1.0.17a
- remove merged patches

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-1m)
- version 1.0.17
- import 4 patches from Fedora
 +* Mon Jul 21 2008 Jaroslav Kysela <jkysela@redhat.com> - 1.0.17-2
 +- added four patches from upstream (to better support pulseaudio)

* Wed Apr 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.16-1m)
- version 1.0.16
- import no-dox-date.patch from Fedora
 +* Thu Apr  3 2008 Jim Radford <radford@blackbean.org> - 1.0.16-3
 +- Fix multilib doxygen conflicts

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.15-2m)
- rebuild against gcc43

* Thu Oct 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.15-1m)
- version 1.0.15
- import glibc-open.patch and pulse-default.patch from Fedora

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14a-3m)
- revive la files

* Sat Jul  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14a-2m)
- remove Requires: alsa-driver for new kernel

* Tue Jun 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14a-1m)
- version 1.0.14a

* Wed Jun  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-1m)
- version 1.0.14

* Sat May  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-0.4.1m)
- update to 1.0.14rc4

* Sat Apr  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-0.3.1m)
- update to 1.0.14rc3
- update gid.patch
- clean up sepc file

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.13-2m)
- delete libtool library

* Mon Oct 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.13-1m)
- update to 1.0.13
- update gid.patch

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.11-2m)
- specify automake version 1.9

* Thu May 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.11-1m)
- update to 1.0.11
- update gid.patch

* Mon Apr 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.10-2m)
- update to 1.0.10 again
- add alsa-lib-1.0.10-remove-gid.patch (fix dmix issue)
- change License: from GPL to LGPL
- clean up spec file

* Sun Jan 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-2m)
- rewind for trouble shooting with gcc-4.1.0 (dmix issue)

* Sat Jan 21 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10

* Fri Aug 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Sun Feb 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-2m)
- move libraries and data to root fs, needed at boot time

* Fri Jan 14 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Tue Sep 14 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.7-0.1.1m)
- update to 1.0.7-rc1.

* Mon Aug 16 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Tue Aug 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0.5-3m)
- use autoconf instead of autoconf-2.58

* Sun Jun 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-2m)
- delete exclude %%{_libdir}/libasound.la for gst-plugins

* Thu Jun 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Thu Apr 29 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- make static lib together
- separate devel packages
- use fedora 1.0.3a spec files list

* Fri Apr 09 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-1m)
- update ALSA to 1.0.4

* Wed Mar 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.3b-1m)
- fixes SIGSEGV problem for dmix plugin

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.3a-2m)
- revised spec for enabling rpm 4.2.

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.3a-1m)

* Tue Feb 03 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- update ALSA to 1.0.2

* Wed Jan 21 2004 Kenta MURATA <muraken2@nifty.com>
- (1.0.1-2m)
- BuildRequires: autoconf >= 2.58.

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.1-1m)

* Fri Dec 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-0.2.1m)
- update to 1.0.0rc2

* Wed Oct 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8-1m)

* Fri Oct 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.7-2m)
- rebuild against alsa-0.9.7c

* Fri Oct  3 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.7-1m)
- update to 0.9.7

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.9.6-2m)
- add alsa.pc

* Thu Jul 31 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6
- use %%NoSource, no need :

* Mon Jul 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Sat Jun 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.4-3m)
  fix requires.

* Sat Jun 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-2m)
- change BuildRequires and Requires: kernel-headers
- thnaks to NARITA Koichi [Momonga-devel.ja:01741]

* Fri Jun 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Sun May 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3
- update BuildRequires: kernel-headers >= 2.4.21-0.3.3m
- update Requires: kernel-headers >= 2.4.21-0.3.3m

* Mon Mar 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2
- update BuildRequires: kernel-headers >= 2.4.20-38m
- update Requires: kernel-headers >= 2.4.20-38m

* Fri Mar 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.1-1m)
  update to 0.9.1

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.0-0.8c.1m)
  update to 0.9.0rc8c

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.0-0.7.2m)
- BuildRequires: kernel-headers >= 2.4.20-21m
- Requires: kernel-headers >= 2.4.20-21m

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.0-0.7.1m)
- update to rc7

* Tue Jan 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.6.2m)
- transform='s,x,x,' in %makeinstall

* Tue Nov 19 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.0-0.6.1m)
- 0.9.0rc6

* Thu Oct 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.5.1m)
- 0.9.0rc5

* Sat Sep 21 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-0.3.3m)
- change Requires: kernel-headers >= 2.4.18-119m
  alsa-0.9 was integrated into kernel at kernel-2.4.18-119m
  alsa-0.9(asound.h) was moved include/linux to include/sound
  alsa-driver package was obsoleted

* Mon Aug 26 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.9.0-0.3.2m)
- we forgot about /usr/share/alsa directory in %files!
- also inlcluded /usr/include/sys/asoundlib.h so other
  builds will detect alsa correctly

* Thu Aug 22 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.9.0-0.3.1m)
- version 0.9.0 rc3
- based on kaz-san's patch devel.ja:00342

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5.10b-3k)
- update to 0.5.10b

* Sun Dec 10 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5.10-3k)
- update to 0.5.10

* Thu Aug 10 2000 AYUHANA Tomonori <l@kondara.org>
- (0.5.9-1k)
- Version up 0.5.8 -> 0.5.9

* Tue Jun  6 2000 AYUHANA Tomonori <l@kondara.org>
- Version up 0.5.7 -> 0.5.8
- add -q at %setup
- use %configure

* Thu Apr  9 2000 Toru Hoshina <t@kondara.org>
- Version up 0.5.6 -> 0.5.7

* Wed Mar 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- Version up 0.5.5 -> 0.5.6
- Provides libasound-0.5.5.so for compatiblity

* Thu Mar  2 2000 Toru Hoshina <t@kondara.org>
- Version up 0.5.4 -> 0.5.5

* Sun Feb 28 2000 Toru Hoshina <t@kondara.org>
- Version up 0.4.1e -> 0.5.4

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon May 28 1998 Helge Jensen <slog@slog.dk>
- Made SPEC file
