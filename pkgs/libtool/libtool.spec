%global momorel 3
# libtool-1.5.x and 2.4.x must be rebuilt when gcc's version is changed.
%global default_gcc_version 4.6.4

Summary: The GNU libtool, which simplifies the use of shared libraries
Name: libtool
%{?include_specopt}
%{?!do_test: %global do_test 1}
%{?!gcc_version: %global gcc_version %{default_gcc_version}}
Version: 2.4.2
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+ and GFDL
Group: Development/Tools
URL: http://www.gnu.org/software/libtool/libtool.html
Source0: http://ftp.gnu.org/gnu/libtool/libtool-%{version}.tar.gz
NoSource: 0
Patch1: libtool-2.2.8-multilib.patch
Requires: autoconf automake m4 perl
Requires(post): info
Requires(preun): info
BuildRequires: autoconf automake texinfo
# make sure we can configure all supported langs
Buildrequires: gcc gcc-c++ libstdc++-devel gcc-gfortran gcc-java
Requires: %{name}-ltdl = %{version}-%{release}, mktemp
# libtool-1.5.24 requires gcc-%%{gcc_version}
Requires: gcc = %{gcc_version}
Buildrequires: gcc = %{gcc_version}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# rename libtool_1_5 -> libtool
Obsoletes: libtool_1_5
Provides:  libtool_1_5

%description
GNU Libtool is a set of shell scripts which automatically configure UNIX and
UNIX-like systems to generically build shared libraries. Libtool provides a
consistent, portable interface which simplifies the process of using shared
libraries.

If you are developing programs which will use shared libraries, but do not use
the rest of the GNU Autotools (such as GNU Autoconf and GNU Automake), you
should install the libtool package.

The libtool package also includes all files needed to integrate the GNU
Portable Library Tool (libtool) and the GNU Libtool Dynamic Module Loader
(ltdl) into a package built using the GNU Autotools (including GNU Autoconf
and GNU Automake).

This package includes a modification from the original GNU Libtool to allow
support for multi-architecture systems, such as the AMD64 Opteron and the
Intel 64-bit Xeon.

%package ltdl
Summary:        Runtime libraries for GNU Libtool Dynamic Module Loader
Group:          System Environment/Libraries
Provides:       libtool-libs = %{version}-%{release}
Obsoletes:      libtool-libs
License:        LGPLv2+

# rename libtool_1_5 -> libtool
Obsoletes: libtool_1_5-ltdl
Provides: libtool_1_5-ltdl

%description ltdl
The libtool-ltdl package contains the GNU Libtool Dynamic Module Loader, a
library that provides a consistent, portable interface which simplifies the
process of using dynamic modules.

These runtime libraries are needed by programs that link directly to the
system-installed ltdl libraries; they are not needed by software built using
the rest of the GNU Autotools (including GNU Autoconf and GNU Automake).

%package ltdl-devel
Summary:        Tools needed for development using the GNU Libtool Dynamic Module Loader
Group:          Development/Libraries
Requires:       %{name}-ltdl = %{version}-%{release}
License:        LGPLv2+

# rename libtool_1_5 -> libtool
Obsoletes: libtool_1_5-ltdl-devel
Provides: libtool_1_5-ltdl-devel

%description ltdl-devel
Static libraries and header files for development with ltdl.

%prep
%setup -q -n %{name}-%{version}
%patch1 -p1 -b .multilib

%build
./bootstrap

export CC=gcc
export CXX=g++
export F77=gfortran
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
sed -e 's/pkgdatadir="\\${datadir}\/\$PACKAGE"/pkgdatadir="\\${datadir}\/\${PACKAGE}"/' configure > configure.tmp; mv -f configure.tmp configure; chmod a+x configure
./configure --prefix=%{_prefix} --exec-prefix=%{_prefix} --bindir=%{_bindir} --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} --datadir=%{_datadir} --includedir=%{_includedir} --libdir=%{_libdir} --libexecdir=%{_libexecdir} --localstatedir=%{_localstatedir} --mandir=%{_mandir} --infodir=%{_infodir} 
# it seems not to be smp-safe
make

%if %{do_test}
%check
make check VERBOSE=yes > make_check.log 2>&1 || (cat make_check.log && false)
%endif

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/libtool.info %{_infodir}/dir

%post ltdl -p /sbin/ldconfig

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/libtool.info %{_infodir}/dir
fi

%postun ltdl -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README THANKS TODO ChangeLog
%{_infodir}/libtool.info*
%{_bindir}/libtool
%{_bindir}/libtoolize
%{_datadir}/aclocal/*.m4
%{_datadir}/libtool
%{_mandir}/man1/libtool.1.*
%{_mandir}/man1/libtoolize.1.*

%files ltdl
%defattr(-,root,root)
%doc libltdl/COPYING.LIB libltdl/README
%{_libdir}/libltdl.so.*

%files ltdl-devel
%defattr(-,root,root)
%{_libdir}/libltdl.a
%{_libdir}/libltdl.la
%{_libdir}/libltdl.so
%{_includedir}/ltdl.h
%dir %{_includedir}/libltdl
%{_includedir}/libltdl/lt_dlloader.h
%{_includedir}/libltdl/lt_error.h
%{_includedir}/libltdl/lt_system.h

%changelog
* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-3m)
- support gcc-4.6.4

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-2m)
- support gcc-4.6.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Jul  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-7m)
- rebuild for gcc-4.6.2 series

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-6m)
- rebuild for new GCC 4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-5m)
- set %%gcc_version to 4.6.1

* Tue Feb  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- %%gcc_version is now configurable. use specopt if you want

* Wed Jan 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4-3m)
- set gcc_version to 4.5.2

* Sat Nov 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-2m)
- set gcc_version to 4.5.1

* Thu Sep 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.10-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Tue Jun  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.8-1m)
- update to 2.2.8

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.6b-5m)
- add %%dir

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6b-4m)
- rebuild against gcc-4.4.4

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.6b-3m)
- use Requires

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.6b-2m)
- rebuild against gcc-4.4.3

* Fri Dec 11 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.6b-1m)
- update to 2.2.6b (CVE-2009-3736)

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6a-7m)
- rebuild against gcc-4.4.2

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6a-6m)
- rebuild against gcc-4.4.1

* Tue Sep 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.6a-5m)
- rebuild against gcc-4.3.4-2m

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.6a-4m)
- do_test = 1 again

* Sat Jul 11 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.6a-3m)
- always do_test = 0

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6a-2m)
- fix install-info

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.6a-1m)
- update to 2.2.6a
-- merge from libtool22

* Sun Mar 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.26-6m)
- revise install-info

* Fri Feb 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.26-5m)
- rebuild against gcc-4.3.3-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.26-4m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.26-3m)
- fix BuildReq

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.26-2m)
- back to 1.5.26

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.4-1m)
- update to 2.2.4

* Thu Jan  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.26-1m)
- update to 1.5.26

* Mon Oct 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.24-9m)
- set gcc_version to 4.3.2

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.24-8m)
- Req gcc-4.3.1 only

* Thu Sep  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.24-7m)
- Req upper gcc-4.3.1

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.24-6m)
- add Requires: %%{gcc_version} 
-- libtool-1.5.x contains some hard-coded gcc's version in /usr/bin/libtool
   and can not work properly when gcc's version is changed

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.24-5m)
- rebuild against gcc43

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.24-4m)
- back to 1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update 2.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.24-3m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.24-2m)
- rebuild against perl-5.10.0-1m

* Sat Jul 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.24-1m)
- update to 1.5.24

* Sun Nov 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.22-3m)
- use libtool.specopt condition for %%check

* Thu Apr  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.22-2m)
- revised packages owner

* Sat Jan  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.22-1m)
- sync with fc-devel

* Sat Jun 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.18-1m)
- sync with FC

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.10-4m)
- sync with FC

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (1.5.10-3m)
- relink shouldn't link .la from /usr/lib because it's not installed yet...

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.5.10-2m)
- enable x86_64.

* Sun Dec 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.10-1m)
- update to the 1.5.10 bugfix release

* Thu Aug 26 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.5-5m)
- rebuild against gcc-3.4.1 (add BuildPrereq: gcc)

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.5-4m)
- revised spec for enabling rpm 4.2.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.5-3m)
- put source into repository

* Tue Jul  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-2m)
- rebuild against rpm-4.0.4-52m

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-1m)
- version 1.5

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.4.2-4k)

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.2-2k)
- version 1.4.2

* Fri Jan 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.4-6k)
- port from Jirai

* Fri Sep 28 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.4-7k)
- add library search path for rpm build time

* Thu Jul  5 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.4

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Thu Jul 13 2000 Elliot Lee <sopwith@redhat.com>
- Fix recognition of ^0[0-9]+$ as a non-negative integer.

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jul  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- patch to use mktemp to create the tempdir
- use %%configure after defining __libtoolize to /bin/true

* Mon Jul  3 2000 Matt Wilson <msw@redhat.com>
- subpackage libltdl into libtool-libs

* Sun Jun 18 2000 Bill Nottingham <notting@redhat.com>
- running libtoolize on the libtool source tree ain't right :)

* Mon Jun  5 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Thu Jun  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.3.5.

* Fri Mar  3 2000 Jeff Johnson <jbj@redhat.com>
- add prereqs for m4 and perl inorder to run autoconf/automake.

* Mon Feb 28 2000 Jeff Johnson <jbj@redhat.com>
- functional /usr/doc/libtool-*/demo by end-user %post procedure (#9719).

* Wed Dec 22 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.3.4.

* Mon Dec  6 1999 Jeff Johnson <jbj@redhat.com>
- change from noarch to per-arch in order to package libltdl.a (#7493).

* Thu Jul 15 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.3.3.

* Mon Jun 14 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.3.2.

* Tue May 11 1999 Jeff Johnson <jbj@redhat.com>
- explicitly disable per-arch libraries (#2210)
- undo hard links and remove zero length file (#2689)

* Sat May  1 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.3.

* Fri Mar 26 1999 Cristian Gafton <gafton@redhat.com>
- disable the --cache-file passing to ltconfig; this breaks the older
  ltconfig scripts found around.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.2f

* Tue Mar 16 1999 Cristian Gafton <gafton@redhat.com>
- completed arm patch
- added patch to make it more arm-friendly
- upgrade to version 1.2d

* Thu May 07 1998 Donnie Barnes <djb@redhat.com>
- fixed busted group

* Sat Jan 24 1998 Marc Ewing <marc@redhat.com>
- Update to 1.0h
- added install-info support

* Tue Nov 25 1997 Elliot Lee <sopwith@redhat.com>
- Update to 1.0f
- BuildRoot it
- Make it a noarch package
