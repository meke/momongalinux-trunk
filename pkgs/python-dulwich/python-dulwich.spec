%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define debug_package %{nil}

Name: python-dulwich
Summary: Pure-Python git library
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Libraries
URL: http://samba.org/~jelmer/dulwich/
# and see https://launchpad.net/dulwich
Source0: http://samba.org/~jelmer/dulwich/dulwich-%{version}.tar.gz
NoSource: 0

BuildRequires: python-devel >= 2.7

%description
Dulwich is a pure-Python implementation of the git file formats and
protocols.

%prep
%setup -q -n dulwich-%{version}

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --root %{buildroot} --skip-build

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING HACKING NEWS README
%doc docs/performance.txt docs/protocol.txt
%{_bindir}/dul-daemon
%{_bindir}/dul-web
%{_bindir}/dulwich
%{python_sitearch}/dulwich/
%{python_sitearch}/dulwich-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update to 0.7.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-2m)
- full rebuild for mo7 release

* Sat Jan  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-1m)
- initial packaging for bzr-git
