%global         momorel 1

Summary:        A library that hides the complexity of using the SIP protocol
Name:           libeXosip2
Version:        3.6.0
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          System Environment/Libraries
URL:            http://savannah.nongnu.org/projects/eXosip
Source0:        http://download.savannah.nongnu.org/releases/exosip/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ortp-devel >= 0.16.3
BuildRequires:  libosip2-devel >= %{version}
BuildRequires:  doxygen
BuildRequires:  openssl-devel >= 1.0.0

%description
A library that hides the complexity of using the SIP protocol for
mutlimedia session establishement. This protocol is mainly to be used
by VoIP telephony applications (endpoints or conference server) but
might be also useful for any application that wish to establish
sessions like multiplayer games.

%package devel
Group: Development/Libraries
Summary: Development files for libeXosip2
Requires: %{name} = %{version}-%{release}
Requires: libosip2-devel

%description devel
Development files for libeXosip2.

%prep
%setup -q

%build
%configure --disable-static
make %{_smp_mflags}
make doxygen

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/%{name}.la

mkdir -p %{buildroot}%{_mandir}/man3
cp help/doxygen/doc/man/man3/*.3* %{buildroot}%{_mandir}/man3

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README

%{_bindir}/sip_reg
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%doc help/doxygen/doc/html help/doxygen/doc/latex

%{_includedir}/eXosip2
%{_libdir}/%{name}.so
%{_mandir}/man3/*.3*

%changelog
* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0 (rebuild against libosip2-3.6.0)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.0-2m)
- rebuild for new GCC 4.6

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0
- rebuild against libosip2-3.5.0 and ortp-0.16.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.0-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.0-3m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-2m)
- delete __libtoolize hack

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-1m)
- import from Fedora devel and update to 3.3.0

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 14 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.1.0-1
- Update to 3.1.0

* Tue Feb  5 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0.3-3
- Apply patch from Adam Tkac that fixes compilation with GCC 4.3.

* Mon Feb  4 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0.3-2
- Update to new patchlevel release.

* Tue Aug 28 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0.3-1
- Update to 3.0.3

* Mon Aug 28 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 2.2.3-3
- Bump release and rebuild.

* Mon Jun  5 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 2.2.3-2
- Add BR for doxygen.

* Mon May 29 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 2.2.3-1
- Update to 2.2.3

* Mon Feb 20 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 2.2.2-4
- Bump release for rebuild.

* Mon Feb 13 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 2.2.2-3
- Bump release and rebuild for gcc 4.1 and glibc.

* Wed Jan  4 2006 Jeffrey C. Ollie <jeff@max1.ocjtech.us> - 2.2.2
- Update to 2.2.2.
- Bump release because forgot to upload new source.

* Sat Oct 29 2005 Jeffrey C. Ollie <jcollie@lt16585.campus.dmacc.edu> - 1.9.1-0.5.pre17
- Update to next prerelease.

* Mon Oct 24 2005 Jeffrey C. Ollie <jcollie@lt16585.campus.dmacc.edu> - 1.9.1-0.4.pre16
- Remove INSTALL from %doc - not needed in an RPM package

* Sun Oct 23 2005 Jeffrey C. Ollie <jcollie@lt16585.campus.dmacc.edu> - 1.9.1-0.3.pre16
- Added -n to BuildRoot
- BR libosip2-devel for -devel subpackage.

* Sun Oct 16 2005 Jeffrey C. Ollie <jcollie@lt16585.campus.dmacc.edu> - 1.9.1-0.2.pre16
- Changed BuildRoot to match packaging guidelines.
- Remove extraneous %dir in -devel %files
- Replace %makeinstall with proper invocation.

* Fri Oct 14 2005 Jeffrey C. Ollie <jcollie@lt16585.campus.dmacc.edu> - 1.9.1-0.1.pre16
- Initial build.

