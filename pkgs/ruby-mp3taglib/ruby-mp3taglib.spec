%global momorel 10

Summary: MP3 taglib for ruby
Name: ruby-mp3taglib
Version: 0.6
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://rubyforge.org/projects/mp3taglib/
Source0: http://rubyforge.org/frs/download.php/68/mp3taglib-%{version}.tar.gz
NoSource: 0
Patch0: ruby-mp3taglib-0.6-nonstatic.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: id3lib-devel
Requires: id3lib

%description
This extension provides freedb functionality for Ruby.

%prep
%setup -q -n mp3taglib-%{version}
%patch0 -p1

%build
ruby extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README example.rb
%{ruby_sitearchdir}/*so
%{ruby_sitelibdir}/*rb

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-8m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6-7m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-3m)
- rebuild against gcc43

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6-2m)
- rebuild against ruby-1.8.6-4m

* Thu Jun 29 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6-1m)
- First release to momonga
