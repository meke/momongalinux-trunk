%global momorel 1
Summary: polkit Authorization Framework
Name: polkit
Version: 0.112
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.freedesktop.org/wiki/Software/polkit
Source0: http://www.freedesktop.org/software/polkit/releases/%{name}-%{version}.tar.gz
NoSource: 0
Group: System Environment/Libraries
BuildRequires: glib2-devel >= 2.30.0
BuildRequires: expat-devel
BuildRequires: pam-devel
BuildRequires: gtk-doc
BuildRequires: intltool
BuildRequires: gobject-introspection-devel
BuildRequires: systemd-devel
BuildRequires: js-devel

Requires: ConsoleKit
Requires: dbus

Requires(pre): shadow-utils
Requires(post): /sbin/ldconfig, systemd
Requires(preun): systemd
Requires(postun): /sbin/ldconfig, systemd

Obsoletes: PolicyKit <= 0.10
Provides: PolicyKit = 0.11

# polkit saw some API/ABI changes from 0.96 to 0.97 so require a
# sufficiently new polkit-gnome package
Conflicts: polkit-gnome < 0.97

Obsoletes: polkit-desktop-policy < 0.107
Provides: polkit-desktop-policy = %{version}

%description
polkit is a toolkit for defining and handling authorizations.  It is
used for allowing unprivileged processes to speak to privileged
processes.

%package devel
Summary: Development files for polkit
Group: Development/Libraries
Requires: %name = %{version}-%{release}
Requires: %name-docs = %{version}-%{release}
Requires: glib2-devel
Obsoletes: PolicyKit-devel <= 0.10
Provides: PolicyKit-devel = 0.11

%description devel
Development files for polkit.

%package docs
Summary: Development documentation for polkit
Group: Development/Libraries
Requires: %name-devel = %{version}-%{release}
Obsoletes: PolicyKit-docs <= 0.10
Provides: PolicyKit-docs = 0.11
BuildArch: noarch

%description docs
Development documentation for polkit.

%prep
%setup -q

%build

# we can't use _hardened_build here, see
# https://bugzilla.redhat.com/show_bug.cgi?id=962005
export CFLAGS='-fPIC %optflags'
export LDFLAGS='-pie -Wl,-z,now -Wl,-z,relro'

%configure --enable-gtk-doc \
        --disable-static \
        --enable-introspection \
        --disable-examples \
        --enable-libsystemd-login=yes
make V=1

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/polkit-1/extensions/*.la

%find_lang polkit-1

%pre
getent group polkitd >/dev/null || groupadd -r polkitd
getent passwd polkitd >/dev/null || useradd -r -g polkitd -d / -s /sbin/nologin -c "User for polkitd" polkitd
exit 0

%post
/sbin/ldconfig
# The implied (systemctl preset) will fail and complain, but the macro hides
# and ignores the fact.  This is in fact what we want, polkit.service does not
# have an [Install] section and it is always started on demand.
%systemd_post polkit.service

%preun
%systemd_preun polkit.service

%postun
/sbin/ldconfig
# Not %%systemd_postun_with_restart - let's err on the side of safety, and keep
# the daemon, with its temporary authorizations and agent registrations, running
# after the upgrade as well; it would be unfortunate if the upgrade tool failed
# because a component can't handle polkitd losing state.
%systemd_postun

%files -f polkit-1.lang
%defattr(-,root,root,-)
%doc COPYING NEWS README
%{_libdir}/lib*.so.*
%{_datadir}/man/man1/*
%{_datadir}/man/man8/*
%{_datadir}/dbus-1/system-services/*
%{_unitdir}/polkit.service
%dir %{_datadir}/polkit-1/
%dir %{_datadir}/polkit-1/actions
%attr(0700,polkitd,root) %dir %{_datadir}/polkit-1/rules.d
%{_datadir}/polkit-1/actions/org.freedesktop.policykit.policy
%dir %{_sysconfdir}/polkit-1
%{_sysconfdir}/polkit-1/rules.d/50-default.rules
%attr(0700,polkitd,root) %dir %{_sysconfdir}/polkit-1/rules.d
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.PolicyKit1.conf
%{_sysconfdir}/pam.d/polkit-1
%{_bindir}/pkaction
%{_bindir}/pkcheck
%{_bindir}/pkttyagent
%dir %{_prefix}/lib/polkit-1
%{_prefix}/lib/polkit-1/polkitd
%{_libdir}/girepository-1.0/*.typelib

# see upstream docs for why these permissions are necessary
%attr(4755,root,root) %{_bindir}/pkexec
%attr(4755,root,root) %{_prefix}/lib/polkit-1/polkit-agent-helper-1

%files devel
%defattr(-,root,root,-)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir
%{_includedir}/*

%files docs
%defattr(-,root,root,-)
%{_datadir}/gtk-doc

%changelog
* Wed Oct  2 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.112-1m)
- update to 0.112
- sync with fc

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.107-1m)
- reimport from fedora
-- this version will improve gdm's behavior

* Fri Jul 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.105-3m)
- fix %%files to avoid conflicting

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.105-2m)
- rebuild for glib 2.33.2

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.105-1m)
- update 0.105
-- fixed resource leak

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.104-1m)
- update 0.104

* Sat Sep 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.102-2m)
- make /etc/polkit/localauthority

* Mon Aug  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.102-1m)
- update to 0.102

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.101-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.101-1m)
- update to 0.101

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96-3m)
- full rebuild for mo7 release

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.96-2m)
- add %%dir

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.96-1m)
- update to 0.96

* Thu Dec 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.94-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.94-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.94-1m)
- import from Fedora (but 0.94)

* Mon Sep 14 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.2
- Refine how Obsolete: is used and also add Provides: (thanks Jesse
  Keating and nim-nim)

* Mon Sep 14 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.1
- Add bugfix for polkit_unix_process_new_full() (thanks Bastien Nocera)
- Obsolete old PolicyKit packages

* Sun Sep 13 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913
- Update to git snapshot
- Drop upstreamed patches
- Turn on GObject introspection
- Don't delete desktop_admin_r and desktop_user_r groups when
  uninstalling polkit-desktop-policy

* Fri Sep 11 2009 David Zeuthen <davidz@redhat.com> - 0.94-4
- Add some patches from git master
- Sort pkaction(1) output
- Bug 23867 – UnixProcess vs. SystemBusName aliasing

* Thu Aug 13 2009 David Zeuthen <davidz@redhat.com> - 0.94-3
- Add desktop_admin_r and desktop_user_r groups along with a first cut
  of default authorizations for users in these groups.

* Wed Aug 12 2009 David Zeuthen <davidz@redhat.com> - 0.94-2
- Disable GObject Introspection for now as it breaks the build

* Wed Aug 12 2009 David Zeuthen <davidz@redhat.com> - 0.94-1
- Update to upstream release 0.94

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.93-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 20 2009 David Zeuthen <davidz@redhat.com> - 0.93-2
- Rebuild

* Mon Jul 20 2009 David Zeuthen <davidz@redhat.com> - 0.93-1
- Update to 0.93

* Tue Jun 09 2009 David Zeuthen <davidz@redhat.com> - 0.92-3
- Don't make docs noarch (I *heart* multilib)
- Change license to LGPLv2+

* Mon Jun 08 2009 David Zeuthen <davidz@redhat.com> - 0.92-2
- Rebuild

* Mon Jun 08 2009 David Zeuthen <davidz@redhat.com> - 0.92-1
- Update to 0.92 release

* Wed May 27 2009 David Zeuthen <davidz@redhat.com> - 0.92-0.git20090527
- Update to 0.92 snapshot

* Mon Feb  9 2009 David Zeuthen <davidz@redhat.com> - 0.91-1
- Initial spec file.
