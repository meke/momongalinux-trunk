%global         momorel 2

Name:           perl-Email-Simple
Version:        2.203
Release:        %{momorel}m%{?dist}
Summary:        Simple parsing of RFC2822 message format and headers
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Email-Simple/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Email-Simple-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Email-Date-Format
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-Email-Date-Format
Requires:       perl-Test-Simple >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Email-Simple-Creator <= 1.424
Provides:       perl-Email-Simple-Creator = 1.424

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Email::Simple is the first deliverable of the "Perl Email Project." The
Email:: namespace was begun as a reaction against the increasing complexity
and bugginess of Perl's existing email modules. Email::* modules are meant
to be simple to use and to maintain, pared to the bone, fast, minimal in
their external dependencies, and correct.

%prep
%setup -q -n Email-Simple-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/Email/Simple*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.203-2m)
- rebuild against perl-5.20.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.203-1m)
- update to 2.203

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.202-2m)
- rebuild against perl-5.18.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.202-1m)
- update to 2.202

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.201-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.201-2m)
- rebuild against perl-5.18.0

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.201-1m)
- update to 2.201

* Wed Apr 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.103-1m)
- update to 2.103

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.102-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.102-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.102-2m)
- rebuild against perl-5.16.1

* Sat Jul 14 2012 nARITA Koichi <pulsar@momonga-linux.org>
- (2.102-1m)
- update to 2.102

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.101-2m)
- rebuild against perl-5.16.0

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.101-1m)
- update to 2.101

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-15m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.100-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.100-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.100-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-8m)
- rebuild against perl-5.12.1

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-7m)
- enable test

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-6m)
- rebuild against perl-5.12.0
- disable test for a while...

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-5m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.100-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.100-3m)
- Obsoletes and Provides perl-Email-Simple-Creator

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-2m)
- remove conflicting man page with perl-Email-Simple-Creator

* Wed Nov 04 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.100-1m)
- update to 2.100
- Specfile re-generated by cpanspec 1.78 for Momonga Linux.

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.005-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.005-1m)
- update to 2.005

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.004-2m)
- rebuild against rpm-4.6

* Fri Jun 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.004-1m)
- update to 2.004

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.003-2m)
- rebuild against gcc43

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.003-1m)
- update to 2.003

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.002-1m)
- update to 2.002

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.999-2m)
- use vendor

* Wed Mar 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.999-1m)
- update to 1.999

* Fri Feb  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.998-1m)
- update to 1.998

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.996-1m)
- update to 1.996

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.995-1m)
- update to 1.995

* Fri Sep 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.990-1m)
- spec file was autogenerated
