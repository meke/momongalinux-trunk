%global momorel 4
%global         qtver 4.7.1

Name:           qgoogletranslator
Version:        1.2.1
Release:        %{momorel}m%{?dist}
Summary:        Google Translator for Qt
Group:          Applications/Text
License:        LGPLv3
URL:            http://code.google.com/p/qgt/
Source0:        http://qgt.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:        %{name}.desktop
Source2:        translate_robot.png
Patch0:         %{name}-%{version}-linking.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  doxygen
Requires:       qt >= %{qtver}

%description
Tiny and powerful gui for google translate(tm) service.

%prep
%setup -q
%patch0 -p1 -b .linking

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install -C %{_target_platform} DESTDIR=%{buildroot}

desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}
install -D -p %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/qgoogletranslator.png

%clean
rm -rf %{buildroot}

%post
update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-,root,root,-)
%doc LICENSE.txt
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/%{name}.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-2m)
- specify PATH for Qt4

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- initial build for Momonga Linux
