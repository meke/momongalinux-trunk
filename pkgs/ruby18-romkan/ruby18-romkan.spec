%global momorel 14
%global origname ruby-romkan
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: A Romaji <-> Kana conversion library for Ruby
Name: ruby18-romkan
Version: 0.4
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Ruby
URL: http://namazu.org/~satoru/ruby-romkan/
Source0: http://namazu.org/~satoru/ruby-romkan/%{origname}-%{version}.tar.gz 
NoSource: 0
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18
Requires: ruby18

%description
Ruby/Romkan is a Romaji <-> Kana conversion library for Ruby. It can
convert a Japanese Romaji string to a Japanese Kana string or vice
versa.

%prep
%setup -q -n %{origname}-%{version}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{ruby18_sitelibdir}
install -m 644 romkan.rb %{buildroot}%{ruby18_sitelibdir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog *.rd test.*
%{ruby18_sitelibdir}/romkan.rb

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-12m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-11m)
- copy from ruby-romkan and build with ruby18

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-10m)
- add Magic Comment. need ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-5m)
- %%NoSource -> NoSource

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4-4m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4-3m)
- rebuild against ruby-1.8.0.

* Thu Feb 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.4-2k)

* Fri Jul 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.3-2k)
