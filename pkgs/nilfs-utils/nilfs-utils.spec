%global momorel 1
%global enable_debug 1
%global _sbindir /sbin

Name: nilfs-utils
Version: 2.2.0
Release: %{momorel}m%{?dist}
Summary: Utilities for NILFS filesystems
License: GPLv2+
Group: System Environment/Base
URL: http://www.nilfs.org/
Source0: http://www.nilfs.org/download/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: nilfs-utils-2.0.6-no_ldconfig.patch
Patch1: nilfs-utils-2.0.14-no_chown.patch
BuildRequires: readline-devel >= 5.0
BuildRequires: libmount-devel
BuildRequires: libuuid-devel
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Utilities for manipulating NILFS filesystems.

%package devel
Summary: Development libraries and headers for developing nilfs tools.
Group: Development/Libraries

%description devel
Development libraries and headers for developing nilfs tools.

%prep
%setup -q -n %{name}-%{version}

#%%patch0 -p1 -b .no_ldconfig
#%%patch1 -p1 -b .no_chown

%build
%configure \
%if %{enable_debug}
        --enable-debug \
%else
        --disable-debug \
%endif
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "*.a" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL NEWS README
%{_bindir}/chcp
%{_bindir}/dumpseg
%{_bindir}/lscp
%{_bindir}/lssu
%{_bindir}/mkcp
%{_bindir}/rmcp
%{_libdir}/libnilfs.so.0*
%{_libdir}/libnilfscleaner.so.0*
%{_libdir}/libnilfsgc.so.0*
%{_sbindir}/mkfs.nilfs2
%{_sbindir}/mount.nilfs2
%{_sbindir}/nilfs-clean
%{_sbindir}/nilfs_cleanerd
%{_sbindir}/nilfs-tune
%{_sbindir}/nilfs-resize
%{_sbindir}/umount.nilfs2
%{_sysconfdir}/nilfs_cleanerd.conf
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root)
%{_includedir}/nilfs.h
%{_includedir}/nilfs_cleaner.h
%{_includedir}/nilfs2_fs.h
%{_libdir}/libnilfs.so
%{_libdir}/libnilfscleaner.so
%{_libdir}/libnilfsgc.so

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-1m)
- update 2.2.0

* Wed May  1 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-1m)
- update 2.1.4

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.1-1m)
- update 2.1.1

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-1m)
- update 2.1.0

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-0.2m)
- add BuildRequires

* Tue Aug 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-0.1m)
- update 2.1.0-rc2

* Sat May 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.23-1m)
- update 2.0.23

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.21-2m)
- rebuild for new GCC 4.6

* Wed Feb  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.21-1m)
- update 2.0.21

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.20-2m)
- rebuild for new GCC 4.5

* Tue Nov  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.20-1m)
- update 2.0.20

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.18-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.18-1m)
- update 2.0.18

* Mon Mar 15 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.17-1m)
- update 2.0.17

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.15-3m)
- apply glibc212 patch

* Thu Jan 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.15-2m)
- remove no-ld-config patch

* Thu Jan 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.15-1m)
- update 2.0.15

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.14-1m)
- [SECURITY] CVE-2009-2657
- update 2.0.14

* Tue Jul  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.13-2m)
- add cleanerd-open-failure.patch.bz2

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.13-1m)
- update 2.0.13

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.12-1m)
- update 2.0.12

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-4m)
- update sources, maybe tarball was changed...

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-3m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-2m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Sat Oct 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.6-1m)
- update 2.0.6
-- fixed a cleaner bug causing kernel panic for
-- more than 512 snapshots.

* Thu Jul 24 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.5-1m)
- update 2.0.5

* Tue Jul  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-1m)
- update 2.0.4

* Mon May  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-1m)
- update 2.0.3

* Sun Apr 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1-release

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-2m)
- rebuild against gcc43

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0-release

* Sat Feb 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.9.1m)
- update 2.0.0-test9

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.8.2m)
- %%NoSource -> NoSource

* Wed Jan  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.8.1m)
- update 2.0.0-test8

* Sun Dec 30 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-0.7.3m)
- re-replaced Source...

* Thu Dec  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.7.2m)
- replaced Source...

* Fri Nov 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.7.1m)
- update 2.0.0-test7

* Wed Nov  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.6.1m)
- update 2.0.0-test6

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.3.1m)
- update 2.0.0-test3

* Wed Jul  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.2.1m)
- Initial Commit
