%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global qimageblitver 0.0.6
%global sopranover 2.9.4
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global icons_svg 1

Name: oxygen-icons
Summary: K Desktop Environment - Oxygen icon theme
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires(pre): coreutils
Requires(pre): gtk2
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{kdever}-%{kdepimrel}
BuildRequires: OpenEXR-devel
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: coreutils
BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: pulseaudio-libs-devel
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
BuildRequires: sed
BuildRequires: soprano-devel >= %{sopranover}

Obsoletes:	oxygen-icon-theme < %{version}
Provides:	oxygen-icon-theme = %{version}-%{release}
Obsoletes:	oxygen-icon-theme-scalable < %{version}

%description
Oxygen icon theme for the K Desktop Environment.

%if %{icons_svg}
%package scalable
Summary: Oxygen icon theme, scalable(svg) icons
Group: User Interface/Desktops
Requires(pre): coreutils
Requires(pre): gtk2
Requires: %{name} = %{version}-%{release}
Provides: oxygen-icon-theme-scalable = %{version}-%{release}

%description scalable
%{summary}.
%endif

%prep
%setup -q -n %{name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%if 0%{?icons_svg}
mkdir -p %{buildroot}%{_kde4_iconsdir}/oxygen/scalable
for icon in `find scalable -name "*.svg"`; do
  install -m 0644 -D "$icon" "%{buildroot}%{_kde4_iconsdir}/oxygen/$icon"
done
for icon in `find scalable -name "*.svgz"`; do
  install -m 0644 -D "$icon" "%{buildroot}%{_kde4_iconsdir}/oxygen/$icon"
done

# remove duplicate icon with kdebase
rm -r %{buildroot}%{_kde4_iconsdir}/oxygen/scalable/apps/clock.svgz

# remove duplicate icon with kdeplasma-addons
rm -r %{buildroot}%{_kde4_iconsdir}/oxygen/scalable/apps/accessories-dictionary.svgz

find %{buildroot}%{_kde4_iconsdir}/oxygen/scalable -name "*.svgz" > svg.list
sed -e "s|^%{buildroot}||" svg.list > svg-include.list
sed -e "s|^%{buildroot}|%exclude |" svg.list > svg-exclude.list
%endif

# remove duplicate icon with kdepim
rm -r %{buildroot}%{_kde4_iconsdir}/oxygen/16x16/status/meeting-organizer.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
fi

%if %{icons_svg}
%post scalable
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%posttrans scalable
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun scalable
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
fi
%endif

%files %{?icons_svg:-f svg-exclude.list}
%defattr(-,root,root,-)
%doc AUTHORS CONTRIBUTING COPYING
%{_kde4_iconsdir}/oxygen/*

%if %{icons_svg}
%files scalable -f svg-include.list
%defattr(-,root,root,-)
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89
- switch to noarch

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88 with KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.85-3m)
- include missing icons for KDE 4.2.3

* Fri May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.85-2m)
- remove duplicate icon(s)

* Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.85-1m)
- update to 4.2.85

* Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.71-1m)
- update to 4.2.71

* Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.70-1m)
- update to 4.2.70

* Sat Apr 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.69-1m)
- update to 4.2.69

* Wed Apr  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.68-1m)
- update to 4.2.68

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.67-1m)
- update to 4.2.67
- initial build for Momonga Linux
