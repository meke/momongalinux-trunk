%global momorel 4

%global git_commit e0fdedc
%global cluster olabini

# Prevent brp-java-repack-jars from being run.
%define __jar_repack %{nil}

Name:           jvyamlb
Version:        0.2.5
Release:        %{momorel}m%{?dist}
Summary:        YAML processor for JRuby

Group:          Development/Libraries
License:        MIT
URL:            http://github.com/%{cluster}/%{name}
Source0:        %{url}/tarball/%{version}/%{cluster}-%{name}-%{git_commit}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  ant
BuildRequires:  ant-junit
BuildRequires:  bytelist
BuildRequires:  java-devel
BuildRequires:  jcodings
BuildRequires:  joda-time
BuildRequires:  jpackage-utils
BuildRequires:  junit

Requires:       bytelist
Requires:       java
Requires:       jcodings
Requires:       joda-time
Requires:       jpackage-utils


%description
YAML processor extracted from JRuby.


%prep
%setup -q -n %{cluster}-%{name}-%{git_commit}

find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;

build-jar-repository -s -p lib joda-time bytelist jcodings


%build
%ant


%install
%__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_javadir}

%__cp -p lib/%{name}-%{version}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
pushd %{buildroot}%{_javadir}/
  %__ln_s %{name}-%{version}.jar %{name}.jar
popd


%check
%ant test


%clean
%__rm -rf %{buildroot}


%files
%defattr(644,root,root,755)
%{_javadir}/*
%doc LICENSE README CREDITS


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-2m)
- full rebuild for mo7 release

* Fri Feb 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.5-1m)
- import from Fedora 13

* Tue Feb 09 2010 Victor G. Vasilyev <victor.vasilyev@sun.com> - 0.2.5-4
- Fix the clean up code in the prep section
- Fix typo

* Thu Jan 28 2010 Victor G. Vasilyev <victor.vasilyev@sun.com> - 0.2.5-3
- New upstream
- name-version.jar is also provided
- Use macros in all sections of the spec

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec 12 2008 Conrad Meyer <konrad@tylerc.org> - 0.2.5-1
- Newer version (0.2.5).

* Sat Jul 19 2008 Conrad Meyer <konrad@tylerc.org> - 0.2.2-1
- Newer version.

* Thu Apr 24 2008 Conrad Meyer <konrad@tylerc.org> - 0.1-2
- Add tests to check section.
- Don't include version in jar filename.

* Tue Apr 22 2008 Conrad Meyer <konrad@tylerc.org> - 0.1-1
- Initial RPM.
