%global momorel 3

Summary: programming language features to GNOME developers
Name: valadoc
Version: 0.20110929
Release: %{momorel}m%{?dist}
License: GPL
Group: Documentation
URL: http://live.gnome.org/Valadoc
# git clone git://git.gnome.org/valadoc
Source0: %{name}-20110929.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: graphviz-devel >= 2.36.0
BuildRequires: glib2-devel
BuildRequires: libgee-devel
BuildRequires: gdk-pixbuf2-devel
BuildRequires: vala-devel

%description
Valadoc is a documentation generator for generating API documentation
from Vala source code based on libvala.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{name}

%build
./autogen.sh
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog MAINTAINERS NEWS README THANKS
%{_bindir}/valadoc
%{_libdir}/libvaladoc.so.*
%exclude %{_libdir}/*.la
%{_libdir}/%{name}
%{_datadir}/%{name}
%{_mandir}/man1/%{name}.1.*

%files devel
%defattr(-, root, root)
%{_includedir}/valadoc-1.0.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/valadoc-1.0.pc
%{_datadir}/vala/vapi/valadoc-1.0.deps
%{_datadir}/vala/vapi/valadoc-1.0.vapi

%changelog
* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20110929-3m)
- rebuild against graphviz-2.36.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20110929-2m)
- rebuild for glib 2.33.2

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20110929-1m)
- update to 20110929 repository

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20110910-1m)
- update to 20110910 repository

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20110408-3m)
- rebuild against graphviz-2.28.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20110408-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20110408-1m)
- initial build
