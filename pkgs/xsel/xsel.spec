%global momorel 4

Name:           xsel
Version:        1.2.0
Release:        %{momorel}m%{?dist}
Summary:        Command line clipboard and X selection tool
Group:          Applications/System
License:        MIT
URL:            http://www.vergenet.net/~conrad/software/xsel/
Source0:        http://www.vergenet.net/~conrad/software/xsel/download/xsel-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libSM-devel libXt-devel libXext-devel

%description
XSel is a command line or script utility, similar to xclip, that can copy the
primary and secondary X selection, or any highlighted text, to or from a file,
stdin or stdout. It can also append to and delete the clipboard or buffer that
you would paste with the middle mouse button.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_mandir}/man1/xsel.1x*
%{_bindir}/xsel

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-1m)
- import from Fedora

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Aug 08 2008 Rakesh Pandit <rakesh@fedoraproject.org> - 1.2.0-4
- retag for F-8, missing spec file

* Sat Jul 26 2008 Rakesh Pandit <rakesh@fedoraproject.org> - 1.2.0-3
- file section to catch all man compressions & no compressions

* Sat Apr 19 2008 Henry Kroll <nospam[AT]thenerdshow.com> - 1.2.0-2
- Standardize build section, remove unnecessary CDEBUGFLAGS.
- Break up long lines, general cleanup.

* Sat Apr 12 2008 Henry Kroll <nospam[AT]thenerdshow.com> - 1.2.0-1
- Version upgrade
- Change license to MIT Old Style
- Fix URL, Requires, BuildRequires. 
- Change group to Applications/System re: xclip. Include keywords
 in the description to make it easier to find.

* Thu Mar 20 2008 Henry Kroll <nospam[AT]thenerdshow.com> - 1.1.0-1
- RPM test build
