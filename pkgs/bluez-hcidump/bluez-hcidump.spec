%global momorel 1

Summary: Bluetooth HCI protocol analyser
Name: bluez-hcidump
Version: 2.5
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source0: http://www.kernel.org/pub/linux/bluetooth/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.bluez.org/
Requires: glibc >= 2.2.4
Requires: bluez-libs >= 3.14
BuildRequires: glibc-devel >= 2.2.4
BuildRequires: bluez-libs-devel >= 4.00
BuildRequires: pkgconfig
ExcludeArch: s390 s390x

%description
Protocol analyser for Bluetooth traffic.

The BLUETOOTH trademarks are owned by Bluetooth SIG, Inc., U.S.A.

%prep

%setup -q

%build
%configure
%make

%Install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL ChangeLog NEWS README
%{_sbindir}/hcidump
%{_mandir}/man8/hcidump.8.*

%changelog
* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5-1m)
- update to 2.5

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-3m)
- update source (maybe source was replaced)

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-2m)
- [SECURITY] CVE-2011-3334

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1-1m)
- update 2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.42-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.42-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.42-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-3m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42-2m)
- rebuild against bluez-4.22

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.40-2m)
- rebuild against gcc43

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40-1m)
- update 1.40

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.33-2m)
- %%NoSource -> NoSource

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.33-1m)
- update 1.33

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16-2m)
- add gcc4 patch
- Patch0: bluez-hcidump-1.16-gcc4.patch

* Mon Feb 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.16-1m)
- import from FC

* Tue Jan 12 2005 David Woodhouse <dwmw2@redhat.com> 1.16-1
- update to bluez-hcidump 1.16

* Tue Sep 22 2004 David Woodhouse <dwmw2@redhat.com> 1.11-1
- update to bluez-hcidump 1.11

* Tue Aug 02 2004 David Woodhouse <dwmw2@redhat.com> 1.10-1
- update to bluez-hcidump 1.10

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed May 12 2004 David Woodhouse <dwmw2@redhat.com>
- update to bluez-hcidump 1.8

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Apr 25 2003 David Woodhosue <dwmw2@redhat.com> 1.5-2
- Fix get_unaligned() -- don't abuse kernel headers.

* Thu Apr 24 2003 David Woodhouse <dwmw2@redhat.com>
- Initial build
