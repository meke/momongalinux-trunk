%global         momorel 6

Name:           perl-Mail-DKIM
Version:        0.40
Release:        %{momorel}m%{?dist}
Summary:        Signs/verifies Internet mail with DKIM/DomainKey signatures
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Mail-DKIM/
Source0:        http://www.cpan.org/authors/id/J/JA/JASLONG/Mail-DKIM-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Crypt-OpenSSL-RSA >= 0.24
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MailTools
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-Net-DNS
BuildRequires:  perl-Test-Simple
Requires:       perl-Crypt-OpenSSL-RSA >= 0.24
Requires:       perl-Digest-SHA
Requires:       perl-MailTools
Requires:       perl-MIME-Base64
Requires:       perl-Net-DNS
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module implements the various components of the DKIM and DomainKeys
message-signing and verifying standards for Internet mail. It currently
tries to implement these specifications:

%prep
%setup -q -n Mail-DKIM-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog Changes HACKING.DKIM README TODO
%{perl_vendorlib}/*/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-2m)
- rebuild against perl-5.16.3

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-9m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-8m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-7m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.38-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.10.1

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Fri Mar 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33.6-1m)
- update to 0.33_6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32-2m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Tue Apr 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.30.1-2m)
- rebuild against gcc43

* Fri Jan 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30.1-1m)
- update to 0.30.1

* Fri Jan 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Fri Nov  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Sun Jul 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
