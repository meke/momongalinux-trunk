%global         momorel 3
%global         dirname ccfits
%global         cfitsiover 3.350

Name:           CCfits
Version:        2.4
Release:        %{momorel}m%{?dist}
Summary:        A C++ interface for cfitsio
Group:          Development/Libraries
License:        BSD
URL:            http://heasarc.gsfc.nasa.gov/docs/software/fitsio/ccfits
Source0:        http://heasarc.gsfc.nasa.gov/docs/software/fitsio/%{dirname}/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         CCfits-1.6-removerpath.patch
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cfitsio-devel >= %{cfitsiover}

%description
CCfits is an object oriented interface to the cfitsio library. It is designed 
to make the capabilities of cfitsio available to programmers working in C++. 
It is written in ANSI C++ and implemented using the C++ Standard Library 
with namespaces, exception handling, and member template functions.

%package devel
Summary:        Headers for developing programs that will use %{name}
Group:          Development/Libraries
Requires:       cfitsio-devel >= %{cfitsiover}
Requires:       %{name} = %{version}-%{release}

%description devel
These are the header files and libraries needed to develop a %{name} 
application.

%package docs
Summary:        Documentation for %{name}, includes full API docs
Group:          Documentation
BuildArch:      noarch
 
%description docs
This package contains the full API documentation for %{name}.

%prep
%setup -q -n CCfits
%patch0 -p1
rm -rf %{_builddir}/%{name}/html/*.pl

%build
%configure --disable-static --with-cfitsio=%{_prefix} --with-cfitsio-include=%{_includedir}/cfitsio
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make %{?_smp_mflags}  install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc License.txt
%{_libdir}/*so.*

%files devel
%defattr(-,root,root,-)
%doc CHANGES 
%exclude %{_bindir}/cookbook
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%exclude %{_libdir}/*.la
%{_includedir}/*

%files docs
%defattr(-,root,root,-)
%doc html

%changelog
* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-3m)
- rebuild against cfitsio-3.350

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-2m)
- rebuild against cfitsio-3.330

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-2m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-3m)
- full rebuild for mo7 release

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-2m)
- rebuild against cfitsio-3.250

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- import from Fedora devel

* Wed Jan 27 2010 Sergio Pascual <sergiopr at fedoraproject.org> 2.2-2
- Rebuilt to match new cfitsio 3.240
- Removed require pkgconfig
- Using upstream pkgconfig file
- Minor fixes

* Thu Sep 10 2009 Sergio Pascual <sergiopr at fedoraproject.org> - 2.2-1
- New upstream version, includes pkgconfig file

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 16 2009 Sergio Pascual <sergiopr at fedoraproject.org> - 2.1-4
- Noarch subpackage for docs

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb 06 2009 Sergio Pascual <sergiopr at fedoraproject.org> 2.1-2
- Rebuilt to match new cfitsio 3.130

* Wed Dec 03 2008 Sergio Pascual <sergiopr at fedoraproject.org> 2.1-1
- New upstream source
- Rebuilt needed to fix bz #474087

* Sat Feb 09 2008 Sergio Pascual <sergiopr at fedoraproject.org> 2.0-1
- New upstream source

* Wed Nov 13 2007 Sergio Pascual <sergiopr at fedoraproject.org> 1.8-1
- New upstream source

* Mon Aug 27 2007 Sergio Pascual <spr at astrax.fis.ucm.es> 1.7-1.1
- Rebuild for Fedora 8 by bad binutils in ppc32

* Sun Jul 22 2007 Sergio Pascual <spr at astrax.fis.ucm.es> 1.7-1
- New upstream source 1.7

* Thu Feb 01 2007 Sergio Pascual <spr at astrax.fis.ucm.es> 1.6-2
- Patch to fix the include directives of cfitsio in header files.

* Mon Dec 11 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.6-1
- New upstream version 1.6. Only compiles with cfitsio >= 3.020.

* Tue Sep 12 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.5-3
- Rebuild for FC6.

* Fri Jul 28 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.5-2
- Removed perl files in the documentation (bug #200517).

* Thu Jul 27 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.5-1
- New upstream source 1.5.
- Removed patch CCfits-1.4-g++4.patch (integrated in the source)
- Perl files in docs removed in the source.

* Wed Mar 08 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.4-4
- Removed explicit Buildrequires gcc-c++.

* Tue Mar 07 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.4-3
- Rebuilt with new upstream source.

* Mon Mar 06 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.4-2
- Removed Source with license file License.txt.

* Mon Feb 20 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.4-1
- Removed perl files in documentation.
- Changed license type to BSD.
- Main summary changed and trailing dots removed.
- Removed -rpath in the shared library.

* Thu Feb 16 2006 Sergio Pascual <spr at astrax.fis.ucm.es> 1.4-0
- Initial spec file.
