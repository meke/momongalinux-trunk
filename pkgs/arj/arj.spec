%global momorel 9

Name:           arj
Version:        3.10.22
Release:        %{momorel}m%{?dist}
Summary:        Archiver for .arj files
Group:          Applications/Archiving
License:        GPL
URL:            http://arj.sourceforge.net/
Source0:        http://dl.sf.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
# unarj.* from Debian.
Source1:        unarj.sh
Source2:        unarj.1
Patch0:         http://ftp.debian.org/debian/pool/main/a/%{name}/%{name}_%{version}-2.diff.gz
Patch1:         arj-3.10.22-custom-printf.patch
Patch2:         arj-3.10.22-quotes.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf
Provides:       unarj = %{version}-%{release}
Obsoletes:      unarj < 3

%description
This package is an open source version of the arj archiver.  This
version has been created with the intent to preserve maximum
compatibility and retain the feature set of original ARJ archiver as
provided by ARJ Software, Inc.

%prep
%setup -q
%patch0 -p1
for i in debian/patches/00*.patch; do
  patch -p1 < $i
done
pushd gnu
  autoconf
popd
%patch1 -p1 -b .glibc210
%patch2 -p1 -b .gcc44

%build
pushd gnu
  %configure
popd
# no %{?_smp_mflags}, arj is not parallel build clean, no stripping please
make ADD_LDFLAGS=""

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# rm the register remainders of arj's sharewares time
rm $RPM_BUILD_ROOT%{_bindir}/arj-register
rm $RPM_BUILD_ROOT%{_mandir}/man1/arj-register.1*
install -Dpm 644 resource/rearj.cfg.example \
  $RPM_BUILD_ROOT%{_sysconfdir}/rearj.cfg
install -pm 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/unarj
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_mandir}/man1/unarj.1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc ChangeLog* doc/COPYING doc/rev_hist.txt
%config(noreplace) %{_sysconfdir}/rearj.cfg
%{_bindir}/*arj*
%{_libdir}/arj/
%{_mandir}/man1/*arj*1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.22-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.22-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.22-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.22-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.22-5m)
- apply gcc44 and glibc210 patch from Rawhide (3.10.22-11)
-- http://sourceforge.net/tracker/?func=detail&aid=2853421&group_id=49820&atid=457566

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.22-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.22-3m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.10.22-2m)
- rebuild against gcc43

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.10.22-1m)
- import from fc-devel to Momonga

* Sat Sep  9 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 3.10.22-1
- initial FE submission based on a src.rpm by Ville Skytta
