%global         momorel 1

Name:           perl-Perl-MinimumVersion
Version:        1.37
Release:        %{momorel}m%{?dist}
Summary:        Find a minimum required version of perl for Perl code
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Perl-MinimumVersion/
Source0:        http://www.cpan.org/authors/id/N/NE/NEILB/Perl-MinimumVersion-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Find-Rule >= 0.32
BuildRequires:  perl-File-Find-Rule-Perl >= 1.04
BuildRequires:  perl-Params-Util >= 0.25
BuildRequires:  perl-PathTools >= 0.80
BuildRequires:  perl-Perl-Critic >= 1.104
BuildRequires:  perl-PPI >= 1.215
BuildRequires:  perl-Scalar-Util >= 1.20
BuildRequires:  perl-Test-More >= 0.47
BuildRequires:  perl-Test-Script >= 1.03
BuildRequires:  perl-version >= 0.76
Requires:       perl-File-Find-Rule >= 0.32
Requires:       perl-File-Find-Rule-Perl >= 1.04
Requires:       perl-Params-Util >= 0.25
Requires:       perl-Perl-Critic >= 1.104
Requires:       perl-PPI >= 1.215
Requires:       perl-Scalar-Util >= 1.20
Requires:       perl-version >= 0.76
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Perl::MinimumVersion takes Perl source code and calculates the minimum
version of perl required to be able to run it. Because it is based on PPI,
it can do this without having to actually load the code.

%prep
%setup -q -n Perl-MinimumVersion-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{_bindir}/perlver
%{perl_vendorlib}/Perl/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- rebuild against perl-5.20.0
- update to 1.37

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-2m)
- rebuild against perl-5.16.3

* Fri Jan 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Fri Nov 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Wed Nov 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-2m)
- rebuild for new GCC 4.6

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.26-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-2m)
- rebuild against perl-5.12.1

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-3m)
- rebuild against perl-5.12.0
- version down to 1.22

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Mon Jan 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 04 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
