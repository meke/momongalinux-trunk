%global momorel 6

Summary:	Trace a DNS record to its start of authority
Name:		dnstracer
Version:	1.9
Release:	%{momorel}m%{?dist}
Group:		Applications/Internet
License:	Modified BSD
URL:		http://www.mavetju.org/unix/general.php
Source0:	http://www.mavetju.org/download/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
dnstracer determines where a given Domain Name Server (DNS) gets
its information from, and follows the chain of DNS servers back to
the servers which know the data.

%prep
%setup -q

%build
autoconf -f
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES CONTACT LICENSE README
%{_bindir}/%{name}
%{_mandir}/man8/dnstracer.8*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-2m)
- run autoconf -f to avoid build failure on high resolution filesystem

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-1m)
- update to 1.9
- NO.TMPFS

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8-2m)
- rebuild against gcc43

* Wed Mar  2 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8-1m)
- version up

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6-2m)
- enable x86_64.

* Fri Jan 31 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (1.6-1m)
- version up

* Tue Jul 23 2002 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (1.5-2m)
- Fix Source URL

* Tue Jul 23 2002 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (1.5-1m)
- spec file was created newly


