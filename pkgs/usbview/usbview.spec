%global momorel 4

Name: usbview
Summary: USB topology and device viewer
Version: 1.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://www.kroah.com/linux-usb/
Source0: http://www.kroah.com/linux-usb/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: %{name}_icon.png
Source2: %{name}.desktop
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel

%description
USBView is a GTK program that displays the topography of the devices
that are plugged into the USB bus on a Linux machine. It also displays
information on each of the devices. This can be useful to determine if
a device is working properly or not.

%prep
%setup -q

%build
%configure
%make

%install
%{__rm} -rf %{buildroot}
%makeinstall

# install desktop icon
install -Dp -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/%{name}_icon.png

# install desktop file
desktop-file-install \
    --vendor= \
    --add-category="System" \
    --dir=%{buildroot}%{_datadir}/applications \
    %{SOURCE2}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING* README TODO
%{_bindir}/usbview
%{_mandir}/man8/usbview.8*
%{_datadir}/pixmaps/%{name}_icon.png
%{_datadir}/applications/%{name}.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- %%NoSource -> NoSource

* Thu Jun 30 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0-1m)
- Initial import.
