%global momorel 10

%global threads pthreads
%global optflags $(echo -n '%{optflags}' | sed -e 's/-fstack-protector//g')

Name: gauche
Summary: Scheme script interpreter with multibyte character handling

### include local configuration
%{?include_specopt}
### default configurations
# If you'd like to change these configurations, please copy them to
# ~/rpm/specopt/Gauche.specopt and edit it.
# encoding: utf8 or eucjp
%{?!encoding: %global encoding utf8}
%{?!runtest: %global runtest 0}

Version: 0.8.14
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/gauche/Gauche-%{version}.tgz
NoSource: 0
Patch0: Gauche-bzip2ed-info.patch
License: see "COPYING"
Group: Development/Languages
URL: http://www.shiro.dreamhost.com/scheme/gauche/
Requires: gdbm >= 1.8.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: Gauche = %{version}-%{release}
Obsoletes: Gauche < %{version}-%{release}

%description
Gauche is a Scheme interpreter conforming Revised^5 Report on
Algorithmic Language Scheme.  It is designed for rapid development
of daily tools like system management and text processing.
It can handle multibyte character strings natively.
This package is compiled with %{encoding} as the native character encoding.

%package %{encoding}
Summary: Scheme script interpreter with multibyte character handling
Group: Development/Languages
Provides: gauche libgauche.so
License: see "COPYING"
Requires: gauche-common
Provides: /usr/bin/gosh
Provides: Gauche-%{encoding} = %{version}-%{release}
Obsoletes: Gauche-%{encoding} < %{version}-%{release}

%description %{encoding}
Gauche is a Scheme interpreter conforming Revised^5 Report on
Algorithmic Language Scheme.  It is designed for rapid development
of daily tools like system management and text processing.
It can handle multibyte character strings natively.
This package is compiled with %{encoding} as the native character encoding.

%package common
Summary: Scheme script interpreter with multibyte character handling
Group: Development/Languages
License: see "COPYING"
Requires(post): info
Requires(preun): info
Provides: Gauche-common = %{version}-%{release}
Obsoletes: Gauche-common < %{version}-%{release}

%description common
Gauche is a Scheme interpreter conforming Revised^5 Report on
Algorithmic Language Scheme.  It is designed for rapid development
of daily tools like system management and text processing.
It can handle multibyte character strings natively.
This package includes common part that is independent from any
native character encoding.  You need either Gauche-eucjp or Gauche-utf8
package as well.

%package gdbm-%{encoding}
Summary: gdbm binding for Gauche Scheme system
Group: Development/Languages
License: GPL
Provides: gauche-gdbm
Requires: gdbm >= 1.8.0, gauche-%{encoding}
Provides: Gauche-gdbm-%{encoding} = %{version}-%{release}
Obsoletes: Gauche-gdbm-%{encoding} < %{version}-%{release}

%description gdbm-%{encoding}
This package adds gdbm binding to the Gauche Scheme system.

%prep
%setup -q -n Gauche-%{version}
%patch0 -p1

%build
## %%configure makes missmatch i586-momonga-linux-gnu and i686-pc-linux-gnu
#%%configure --enable-threads=%{threads} --enable-multibyte=%{encoding} \
#	   --enable-ipv6
#ERROR ./configure --enable-threads=%{threads} --enable-multibyte=%{encoding} --enable-ipv6

./configure --target=%{_target_platform} \
	    --prefix=%{_prefix} \
	    --libdir=%{_libdir} \
	    --mandir=%{_datadir}/man \
	    --infodir=%{_datadir}/info \
	    --enable-threads=%{threads} \
	    --enable-multibyte=%{encoding} \
	    --enable-ipv6

%ifarch %{ix86} x86_64
make OPTFLAGS="-fomit-frame-pointer"
%else
make
%endif

%if %{runtest}
make test
%endif

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_prefix}
make DESTDIR=%{buildroot} install-pkg
make DESTDIR=%{buildroot} install-doc

find %{buildroot} -type f -exec chmod u+w {} \;

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post %{encoding}
# creates slib catalog, if possible.
%{_bindir}/gosh -u slib -e "(require 'logical)" -e "(exit 0)" > /dev/null 2>&1 || echo

%post common
/sbin/install-info %{_infodir}/gauche-refe.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/gauche-refj.info %{_infodir}/dir || :

%preun common
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gauche-refe.info %{_infodir}/dir || :
  /sbin/install-info --delete %{_infodir}/gauche-refj.info %{_infodir}/dir || :
fi

%files common
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL INSTALL.eucjp NEWS README examples
%{_datadir}/aclocal/gauche.m4
%{_datadir}/gauche/%{version}
%exclude %{_datadir}/gauche/%{version}/lib/dbm/odbm.scm
%exclude %{_datadir}/gauche/%{version}/lib/dbm/ndbm.scm
%exclude %{_datadir}/gauche/%{version}/lib/dbm/gdbm.scm
%{_datadir}/info/gauche-refe.info*
%{_datadir}/info/gauche-refj.info*
%{_datadir}/man/man1/*

%files %{encoding}
%defattr(-,root,root)
%{_bindir}/gosh
%{_bindir}/gauche-config
%{_bindir}/gauche-cesconv
%{_bindir}/gauche-install
%{_bindir}/gauche-package
#%%{_libdir}/libgauche.a
%{_libdir}/libgauche.so*
%{_libdir}/gauche
%exclude %{_libdir}/gauche/%{version}/*/odbm-lib.so
%exclude %{_libdir}/gauche/%{version}/*/ndbm-lib.so
%exclude %{_libdir}/gauche/%{version}/*/gdbm-lib.so

%files gdbm-%{encoding}
%defattr(-,root,root)
%{_libdir}/gauche/%{version}/*/odbm-lib.so
%{_libdir}/gauche/%{version}/*/ndbm-lib.so
%{_libdir}/gauche/%{version}/*/gdbm-lib.so
%{_datadir}/gauche/%{version}/lib/dbm/odbm.scm
%{_datadir}/gauche/%{version}/lib/dbm/ndbm.scm
%{_datadir}/gauche/%{version}/lib/dbm/gdbm.scm

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.14-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.14-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.14-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.14-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.14-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.14-5m)
- fix up Provides and Obsoletes

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.14-4m)
- renamed from Gauche

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.14-3m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.14-2m)
- run install-info

* Sun Oct 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.14-1m)
- update to 0.8.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.13-2m)
- rebuild against gcc43

* Sat Feb 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.13-1m)
- update to 0.8.13

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.12-2m)
- %%NoSource -> NoSource

* Thu Nov  1 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.12-1m)
- update to 0.8.12

* Sat Aug 18 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.11-1m)
- update to 0.8.11

* Sun Apr 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.10-1m)
- update to 0.8.10

* Thu Feb 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.9-1m)
- update to 0.8.9

* Wed Nov 15 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Tue Oct 31 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.8.7-3m)
- add "Provides: /usr/bin/gosh" to package %%{encoding}

* Thu Aug 31 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-2m)
- delete duplicated file

* Tue Aug 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Fri Aug 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1
- delete Patch0: Gauche-makeverslink.patch
- change Gauche-bzip2ed-info.patch to Patch0
- merge Gauche-0.8.1/Gauche.spec
- set and use %%optflags
- change configure part

* Thu Dec 11 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Mon Nov 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.1-2m)
- rebuild against gdbm-1.8.0-19m

* Sun Aug  3 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.7.1-1m)
- update to 0.7.1
    spec is updated by thin ([devel.ja:01995])
- search and read bzip2'ed info file on gosh (Gauche-bzip2ed-info.patch)

* Mon Jul  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.7-4m)
- rebuild against rpm-4.0.4-52m

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.7-3m)
- remove ifarch
- use %%{?_smp_mflags}
- use %%configure
- %%files

* Sat Jun 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-2m)
- use rpm macros
- add NoSource: 0
- change ifarch to ix86

* Fri Jun 13 2003 thin<thin@outside.jp>
- (0.7-1m)
- import to momonga

* Fri May 30 2003 Shiro Kawai
- Gauche release 0.7

* Sun Mar 30 2003 Shiro Kawai
- Gauche release 0.6.8

* Thu Feb  7 2003 Shiro Kawai
- Gauche release 0.6.7.1

* Thu Feb  6 2003 Shiro Kawai
- Gauche release 0.6.7

* Fri Dec 14 2002 Shiro Kawai
- Gauche release 0.6.6

* Fri Nov 15 2002 Shiro Kawai
- Gauche release 0.6.5

* Mon Oct 14 2002 Shiro Kawai
- Gauche release 0.6.4

* Thu Sep 22 2002 Shiro Kawai
- Gauche release 0.6.3

* Thu Sep  2 2002 Shiro Kawai
- Gauche release 0.6.2

* Thu Jul 31 2002 Shiro Kawai
- Gauche release 0.6.1

* Thu Jul 18 2002 Shiro Kawai
- Gauche release 0.6

* Sun Jun 30 2002 Shiro Kawai
- Gauche release 0.5.7

* Fri Jun 14 2002 Shiro Kawai
- Gauche release 0.5.6

* Mon May 27 2002 Shiro Kawai
- Gauche release 0.5.5

* Sat May  5 2002 Shiro Kawai
- Gauche release 0.5.4

* Thu Apr 15 2002 Shiro Kawai
- Gauche release 0.5.3

* Thu Mar  7 2002 Shiro Kawai
- first package release

