%global momorel 6

Summary: Sounds for GNOME events.
Name: gnome-audio
Version: 2.22.2
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Multimedia
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.22/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
BuildArchitectures: noarch

%description
If you use the GNOME desktop environment, you may want to
install this package of complementary sounds.

%package extra
Summary: Optional Sounds for GNOME events.
Group: System Environment/Libraries

%description extra
This package contains extra sound files useful for customizing the
sounds that the GNOME desktop environment makes.

%prep
%setup -q

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc ChangeLog COPYING README
%{_datadir}/sounds/warning.wav
%{_datadir}/sounds/panel
%{_datadir}/sounds/gtk-events
%{_datadir}/sounds/shutdown1.wav
%{_datadir}/sounds/email.wav
%{_datadir}/sounds/login.wav
%{_datadir}/sounds/startup3.wav
%{_datadir}/sounds/info.wav
%{_datadir}/sounds/generic.wav
%{_datadir}/sounds/logout.wav
%{_datadir}/sounds/error.wav
%{_datadir}/sounds/question.wav

%files extra
%defattr(-, root, root)
%{_datadir}/sounds/card_shuffle.wav
%{_datadir}/sounds/phone.wav

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.2-2m)
- rebuild against rpm-4.6

* Wed May  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.0-2m)
- revised spec for enabling rpm 4.2.

* Tue Jul 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Thu Mar 29 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0
- K2K

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.0-11k)
- fixed for FHS

* Thu Mar  9 2000 Shingo Akagaki <dora@kondara.org>
- more fix

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- change %files section

* Tue Dec 21 1999 Shingo Akagaki <dora@kondara.org>
-  to be nosrc

* Fri Mar 19 1999 Michael Fulbright <drmike@redhat.com>
- switched generic and question sounds

* Thu Mar 18 1999 Michael Fulbright <drmike@redhat.com>
- redid defaults

* Thu Feb 25 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.0 - made extra subpackage

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.8

* Tue Jan 26 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.4

* Thu Dec 17 1998 Michael Fulbright <drmike@redhat.com>
- first pass at a spec file
