%global momorel 5

%define tog_pegasus_version 2.5.1
%define provider_dir %{_libdir}/cmpi

Name:           sblim-cmpi-base
Version:        1.5.9
Release:        %{momorel}m%{?dist}
Summary:        SBLIM CMPI Base Providers

Group:          Applications/System
License:        CPL
URL:            http://sblim.wiki.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/sblim/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  tog-pegasus-devel >= %{tog_pegasus_version}
Requires:       tog-pegasus >= %{tog_pegasus_version}

%description
SBLIM (Standards Based Linux Instrumentation for Manageability)
CMPI (Common Manageability Programming Interface) Base Providers
for System-Related CIM (Common Information Model) classes.

%package devel
Summary: SBLIM CMPI Base Providers Development Header Files
Group: Development/Libraries
BuildRequires: tog-pegasus-devel >= %{tog_pegasus_version}
Requires: %{name} = %{version}-%{release}

%description devel
SBLIM (Standards Based Linux Instrumentation for Manageability)
CMPI (Common Manageability Programming Interface) Base Provider
development header files and link libraries.

%package test
Summary: SBLIM CMPI Base Providers Test Cases
Group: Applications/System
BuildRequires: tog-pegasus-devel >= %{tog_pegasus_version}
Requires: %{name} = %{version}-%{release}
Requires: sblim-testsuite

%description test
SBLIM (Standards Based Linux Instrumentation for Manageability)
CMPI (Common Manageability Programming Interface) Base Provider
Testcase Files for the SBLIM Testsuite.

%prep

%setup -q

%build
%configure TESTSUITEDIR=%{_datadir}/sblim-testsuite \
           PROVIDERDIR=%{provider_dir} \
           CIMSERVER=pegasus
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
cp -fp *OSBase_UnixProcess.h $RPM_BUILD_ROOT/%{_includedir}/sblim
chmod 644 $RPM_BUILD_ROOT/%{_includedir}/sblim/*OSBase_UnixProcess.h
# remove unused libtool files
rm -f $RPM_BUILD_ROOT/%{_libdir}/*a
rm -f $RPM_BUILD_ROOT/%{_libdir}/cmpi/*a

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%docdir %{_datadir}/doc/%{name}-%{version}
%{_datadir}/doc/%{name}-%{version}
%{_datadir}/%{name}
%{_libdir}/*.so.*
%{provider_dir}/*.so*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so

%files test
%defattr(-,root,root,-)
%{_datadir}/sblim-testsuite/cim/Linux_*.cim
%{_datadir}/sblim-testsuite/system/linux/Linux_*
%{_datadir}/sblim-testsuite/system/linux/createKeyFiles.sh
%{_datadir}/sblim-testsuite/test-cmpi-base.sh

%pre
%define SCHEMA %{_datadir}/%{name}/Linux_Base.mof
%define REGISTRATION %{_datadir}/%{name}/Linux_Base.registration
# If upgrading, deregister old version
if [ $1 -gt 1 ]
then
  %{_datadir}/%{name}/provider-register.sh \
        -d -t pegasus \
        -m %{SCHEMA} \
        -r %{REGISTRATION} > /dev/null  2>&1 || :;
  # don't let registration failure when server not running fail upgrade!
fi

%post
/sbin/ldconfig
if [ $1 -ge 1 ]
then
# Register Schema and Provider - this is higly provider specific
  %{_datadir}/%{name}/provider-register.sh \
        -t pegasus \
        -m %{SCHEMA} \
        -r %{REGISTRATION} > /dev/null  2>&1 || :;
  # don't let registration failure when server not running fail install!
fi

%preun
# Deregister only if not upgrading 
if [ $1 -eq 0 ]
then
  %{_datadir}/%{name}/provider-register.sh \
        -d -t pegasus \
        -m %{SCHEMA} \
        -r %{REGISTRATION} > /dev/null  2>&1 || :;
  # don't let registration failure when server not running fail erase!
fi

%postun -p /sbin/ldconfig

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.9-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.9-1m)
- sync with Fedora 11 (1.5.7-3)
- update to 1.5.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-3m)
- rebuild against rpm-4.6

* Sun Sep 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-2m)
- modify %%files test

* Wed Sep 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.4-1m)
- import from Fedora

* Tue Dec 05 2006 Mark Hamzy <hamzy@us.ibm.com> 1.5.4-7
 - Ignore failures when running provider-register.sh.  cimserver may be down

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 1.5.4-6
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Thu Nov 10 2005  <mihajlov@de.ibm.com> - 1.5.4-3
- suppress error output in post scriptlets

* Wed Oct 27 2005  <mihajlov@de.ibm.com> - 1.5.4-2
- went back to original provider dir location as FC5 pegasus 2.5.1 support
  /usr/lib[64]/cmpi

* Wed Oct 12 2005  <mihajlov@de.ibm.com> - 1.5.4-1
- new spec file specifically for Fedora/RedHat

* Wed Jul 20 2005 Mark Hamzy <hamzy@us.ibm.com>	1.5.3-1
- initial support
