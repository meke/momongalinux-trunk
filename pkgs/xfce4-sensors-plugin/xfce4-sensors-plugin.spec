%global momorel 1

%global xfce4ver 4.10.0
%global major 1.2

Name: 		xfce4-sensors-plugin
Version: 	1.2.5
Release:	%{momorel}m%{?dist}
Summary: 	Sensors plugin for the Xfce panel
Group: 		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:        http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libnotify-devel >= 0.4
BuildRequires:	xfce4-dev-tools
BuildRequires:	gettext, perl-XML-Parser, lm_sensors-devel >= 3.0.2
Requires:	xfce4-panel >= %{xfce4ver}, lm_sensors >= 3.0.2, hddtemp
ExcludeArch:    s390 s390x ppc

%description
This plugin displays various hardware sensor values in the Xfce panel.

%package devel
Summary:        Development files for xfce4-sensors-plugin
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       libxfce4util-devel

%description devel
Static libraries and header files for the xfce4-sensors-plugin.

%prep
%setup -q

%build
# Xfce has its own autotools-running-script thingy, if you use autoreconf
# it'll fall apart horribly
xdt-autogen

%configure --disable-static --with-pathhddtemp=%{_bindir}/hddtemp LIBS="-lm"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir} transform='s,x,x,'
%find_lang %{name}

rm ${RPM_BUILD_ROOT}%{_libdir}/xfce4/modules/libxfce4sensors.la
desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --delete-original                                       \
        --add-only-show-in=XFCE                                 \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/xfce4-sensors.desktop

%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/*
%{_libdir}/xfce4/modules/libxfce4sensors.so.*
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/applications/xfce4-sensors.desktop
%{_datadir}/icons/hicolor/*/apps/xfce-sensors.png
%{_datadir}/icons/hicolor/scalable/apps/xfce-sensors.svg
%{_datadir}/xfce4/panel-plugins/*.desktop

%files devel
%defattr(-, root, root,-)
%{_libdir}/pkgconfig/libxfce4sensors-1.0.pc
%{_libdir}/xfce4/modules/libxfce4sensors.so


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.5-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-7m)
- rebuild against xfce4-4.8

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-5m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.99.6-5m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.99.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.99.6-3m)
- add only show in XFCE

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.99.6-2m)
- add autoreconf
-- need libtool-2.2.x

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.99.6-1m)
- update
- rebuild against xfce4-4.6
- separate devel sub package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.99.2-2m)
- rebuild against rpm-4.6

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.99.2-1m)
- update to version 0.10.99.2 for lm_sensors-3.0.2
- import lm_sensors3x.patch from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-4m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-1m)
- import to Momonga from fc

* Sun Jan 28 2007 Christoph Wickert <fedora christoph-wickert de> - 0.10.0-2
- Rebuild for XFCE 4.4.
- Update gtk-icon-cache scriptlets.

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 0.10.0-1
- Update to 0.10.0.

* Wed Sep 13 2006 Christoph Wickert <fedora christoph-wickert de> - 0.9.0-2
- Rebuild for XFCE 4.3.99.1.
- BR perl(XML::Parser).

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.9.0-1
- Update to 0.9.0 on XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.7.0-6
- Mass rebuild for Fedora Core 6.

* Tue Apr 11 2006 Christoph Wickert <fedora wickert at arcor de> - 0.7.0-5
- Require xfce4-panel.

* Sat Feb 18 2006 Christoph Wickert <fedora wickert at arcor de> - 0.7.0-4
- Rebuild for Fedora Extras 5.

* Sun Jan 22 2006 Christoph Wickert <fedora wickert at arcor de> - 0.7.0-3
- ExcludeArch ppc.

* Thu Dec 01 2005 Christoph Wickert <fedora wickert at arcor de> - 0.7.0-2
- Add libxfcegui4-devel BuildReqs.
- Fix %%defattr.

* Mon Nov 14 2005 Christoph Wickert <fedora wickert at arcor de> - 0.7.0-1
- Initial Fedora Extras version.
- Update to 0.7.0.
- disable-static instead of removing .a files.

* Fri Sep 23 2005 Christoph Wickert <fedora wickert at arcor de> - 0.6.2-1.fc4.cw
- Update to 0.6.2.
- Add libxml2 BuildReqs.

* Sat Jul 09 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.0-1.fc4.cw
- Rebuild for Core 4.

* Wed Apr 13 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.0-1.fc3.cw
- Initial RPM release.
