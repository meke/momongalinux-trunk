%global         momorel 4
%global boost_version 1.55.0

Name:           OpenImageIO
Version:        1.0.9
Release:        %{momorel}m%{?dist}
Summary:        Library for reading and writing images
Group:          Development/Libraries
License:        BSD
URL:            https://sites.google.com/site/openimageio/home
Source0:        https://github.com/OpenImageIO/oiio/archive/Release-%{version}.tar.gz
NoSource:       0
Source101:      FindTBB.cmake
Patch0:         OpenImageIO-1.0.0-use_external_tbb.patch
Patch1:         OpenImageIO-1.0.0-tbb_include.patch

BuildRequires:  cmake
BuildRequires:  qt-devel
BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  glew-devel >= 1.10.0
BuildRequires:  OpenEXR-devel >= 1.7.1
BuildRequires:  ilmbase-devel >= 1.0.3
BuildRequires:  python2-devel
BuildRequires:  txt2man
BuildRequires:  libpng-devel
BuildRequires:  libtiff-devel
BuildRequires:  openjpeg-devel
BuildRequires:  libwebp-devel
BuildRequires:  zlib-devel
BuildRequires:  jasper-devel
BuildRequires:  pugixml-devel
%ifarch x86_64
BuildRequires:  tbb-devel >= 4.1
%endif
BuildRequires:  hdf5-devel Field3D-devel
BuildRequires:  OpenColorIO-devel

# We don't want to provide private python extension libs
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$ 
%filter_setup
}


%description
OpenImageIO is a library for reading and writing images, and a bunch of related
classes, utilities, and applications. Main features include:
- Extremely simple but powerful ImageInput and ImageOutput APIs for reading and
  writing 2D images that is format agnostic.
- Format plugins for TIFF, JPEG/JFIF, OpenEXR, PNG, HDR/RGBE, Targa, JPEG-2000,
  DPX, Cineon, FITS, BMP, ICO, RMan Zfile, Softimage PIC, DDS, SGI,
  PNM/PPM/PGM/PBM, Field3d.
- An ImageCache class that transparently manages a cache so that it can access
  truly vast amounts of image data.
- A really nice image viewer, iv, also based on OpenImageIO classes (and so 
  will work with any formats for which plugins are available).


%package devel
Summary:        Documentation for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development files for package %{name}


%prep
%setup -q -n oiio-Release-%{version}
%patch0 -p1 -b .exttbb
%patch1 -p1 -b .tbbinc

# Install FindTBB.cmake
install %{SOURCE101} src/cmake/modules/

# Remove bundled pugixml
rm -f src/include/pugixml.hpp \
      src/include/pugiconfig.hpp \
      src/libutil/pugixml.cpp 

# Remove bundled tbb
rm -rf src/include/tbb

# Install test images
#rm -rf ../oiio-images && mkdir ../oiio-images && pushd ../oiio-images
#tar --strip-components=1 -xzf %{SOURCE1}


%build
rm -rf build/linux && mkdir -p build/linux && pushd build/linux
# CMAKE_SKIP_RPATH is OK here because it is set to FALSE internally and causes
# CMAKE_INSTALL_RPATH to be cleared, which is the desiered result.
%cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo \
       -DCMAKE_SKIP_RPATH:BOOL=TRUE \
       -DINCLUDE_INSTALL_DIR:PATH=/usr/include/%{name} \
       -DPYLIB_INSTALL_DIR:PATH=%{python_sitearch} \
       -DINSTALL_DOCS:BOOL=FALSE \
       -DUSE_EXTERNAL_PUGIXML:BOOL=TRUE \
%ifarch x86_64
       -DUSE_TBB:BOOL=TRUE \
       -DUSE_EXTERNAL_TBB=TRUE \
%else
       -DUSE_TBB:BOOL=FALSE \
%endif
%ifarch ppc ppc64
       -DNOTHREADS:BOOL=TRUE \
%endif
       ../../src

make %{?_smp_mflags}

%install
pushd build/linux
make DESTDIR=%{buildroot} install

# Move man pages to the right directory
mkdir -p %{buildroot}%{_mandir}/man1
cp -a doc/*.1 %{buildroot}%{_mandir}/man1

%check
#pushd build/linux && make test

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc CHANGES LICENSE
%{_bindir}/*
%{_libdir}/libOpenImageIO.so.*
%{python_sitearch}/OpenImageIO.so
%{_mandir}/man1/*

%files devel
%doc src/doc/*.pdf
%{_libdir}/libOpenImageIO.so
%{_includedir}/*

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-4m)
- rebuild against boost-1.55.0

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-3m)
- rebuild against glew-1.10.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-2m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- import from Fedora

* Fri Dec 28 2012 Richard W.M. Jones <rjones@redhat.com> - 1.0.9-3
- Rebuild, see
  http://lists.fedoraproject.org/pipermail/devel/2012-December/175685.html

* Thu Dec 13 2012 Adam Jackson <ajax@redhat.com> - 1.0.9-2
- Rebuild for glew 1.9.0

* Sat Sep 22 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.9-1
- Update to latest upstream release.

* Wed Aug  8 2012 David Malcolm <dmalcolm@redhat.com> - 1.0.8-2
- rebuild against boost-1.50

* Wed Aug 01 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.8-1
- Update to latest upstream release.

* Mon Jul 30 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.7-3
- Rebuild for updated libGLEW.

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Jul 09 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.7-1
- Update to latest upstream release.

* Thu Jun 28 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.6-1
- Update to latest upstream release.
- Fix linking against TBB which broke at some point.

* Tue Jun 12 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.5-1
- Update to latest upstream release.

* Mon May 07 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.4-2
- Rebuild for updated libtiff.
- Add OpenColorIO to build requirements.

* Thu May 03 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.4-1
- Update to latest upstream release.

* Tue Apr 24 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.3-1
- Update to latest upstream release.

* Fri Mar 02 2012 Richard Shaw <hobbes1069@gmail.com> - 1.0.0-1
- Update to latest upstream release.

* Thu Jan 05 2012 Richard Shaw <hobbes1069@gmail.com> - 0.10.4-1
- Update to 0.10.4.
- Rebuild for GCC 4.7.0.

* Fri Dec 02 2011 Richard Shaw <hobbes1069@gmail.com> - 0.10.3-1
- Build against TBB library.

* Sat Nov 05 2011 Richard Shaw <hobbes1069@gmail.com> - 0.10.3-1
- Update to 0.10.3
- Rebuild for libpng 1.5.
- Fixed bulding against tbb library.

* Thu Aug 27 2011 Tom Callaway <spot@fedoraproject.org> - 0.10.2-1
- Update to 0.10.2

* Thu Aug 04 2011 Richard Shaw <hobbes1069@gmail.com> - 0.10.1-2
- New upstream release.
- Fix private shared object provides with python library.

* Mon Jul 18 2011 Richard Shaw <hobbes1069@gmail.com> - 0.10.0-2
- Disabled use of the TBB library.
- Moved headers to named directory.

* Tue Jul 05 2011 Richard Shaw <hobbes1069@gmail.com> - 0.10.0-1
- Inital Release.
