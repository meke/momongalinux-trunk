%global momorel 2
%global pilot-link-version 0.12.3

%global with_eds 0

Summary: GNOME pilot programs
Name: gnome-pilot
Version: 2.91.93
Release: %{momorel}m%{?dist}
License: LGPLv2+ and GPLv2+
Group: Applications/Communications
Source0: ftp://ftp.gnome.org/pub/gnome/sources/%{name}/2.91/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pilot-link
Requires(post): GConf2 scrollkeeper
Requires(postun): scrollkeeper
BuildRequires: libgnome-devel >= 2.16.0
BuildRequires: libgnomeui-devel >= 2.16.1
BuildRequires: libglade2-devel >= 2.6.0
BuildRequires: ORBit2-devel >= 2.14.5
BuildRequires: libbonobo-devel >= 2.16.0
BuildRequires: gnome-panel-devel >= 3.0
BuildRequires: GConf2-devel >= 2.14.0
BuildRequires: pilot-link-devel >= 0.12.1-2m
BuildRequires: libxml2-devel >= 2.6.27
BuildRequires: gnome-vfs2-devel >= 2.16.3
BuildRequires: readline-devel >= 5.2
BuildRequires: openssl-devel >= 0.9.8b
BuildRequires: gob2
%if %{with_eds}
BuildRequires: evolution-data-server-devel >= 3.1
%endif
#ExcludeArch: s390 s390x

%description
gnome-pilot is a collection of programs and daemon for integrating
GNOME and the PalmPilot<tm> or other PalmOS<tm> devices.

%package devel
Summary: GNOME pilot libraries, includes, etc
Group: Development/Libraries
Requires: ORBit2-devel
Requires: pilot-link-devel >= %{pilot-link-version}
Requires: %name = %{version}
Requires: libgnomeui-devel

%description devel
gpilotd libraries and includes.

%prep
%setup -q

%build
%configure --enable-usb \
	--enable-network \
%if %{with_eds}
	--enable-eds-conduits \
%else
	--disable-eds-conduits \
%endif
	--disable-schemas-install \
	--disable-static \
	--disable-scrollkeeper
export tagname=CC
%make

%install
rm -rf --preserve-root %{buildroot}
export tagname=CC
%makeinstall LIBTOOL=%{_bindir}/libtool
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
SCHEMAS="pilot.schemas"
for S in $SCHEMAS; do
  gconftool-2 --makefile-install-rule /etc/gconf/schemas/$S > /dev/null
done
scrollkeeper-update

%postun
/sbin/ldconfig
scrollkeeper-update

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/pilot.schemas
%{_bindir}/*
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la
%dir %{_libdir}/gnome-pilot
%dir %{_libdir}/gnome-pilot/conduits
%{_libdir}/gnome-pilot/conduits/*
%{_libexecdir}/*
%{_mandir}/man*/*
%{_datadir}/%{name}
%{_datadir}/gnome-pilot/devices.xml
%{_datadir}/mime-info/palm.*
%{_datadir}/pixmaps/*.png
%{_datadir}/gnome/help/gnome-pilot
%{_datadir}/omf/gnome-pilot
%{_datadir}/applications/gpilotd-control-applet.desktop
%{_datadir}/dbus-1/services/org.gnome.GnomePilot.service
%{_datadir}/dbus-1/services/org.gnome.panel.applet.PilotAppletFactory.service
%{_datadir}/gnome-panel/4.0/applets/org.gnome.applets.PilotApplet.panel-applet

%files devel
%defattr(-, root, root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Tue Jul 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.91.93-2m)
- disable evolution support

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.91.93-1m)
- update to 2.91.93

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.91.92-1m)
- update to 2.91.92

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.17-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.17-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17

* Thu May 28 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.0.16-6m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-5m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-4m)
- update Patch8 for fuzz=0
- License: LGPLv2+ and GPLv2+

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.16-3m)
- rebuild against pilot-link 0.12.3

* Thu Jun 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.16-2m)
- remove broken menu entry
- https://bugzilla.redhat.com/show_bug.cgi?id=435118

* Sun Jun 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.15-4m)
- rebuild against gcc43

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.15-3m)
- add -maxdepth 1 to del .la

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.15-2m)
- delete libtool library

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15
- rebuild against pilot-link-0.12.1-1m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.13-5m)
- rebuild against expat-2.0.0-1m

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.13-4m)
- rebuild against readline-5.0

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.13-3m)
- rebuild against openssl-0.9.8a

* Sun Jan 01 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.13-2m)
- add %%global pilot-link-version 0.11.8

* Wed Nov 23 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.13-1m)
- import from FC

* Tue Aug 16 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.13-7.fc5
- rebuild (required by new cairo package)

* Tue Jun 28 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.13-6.fc5
- Regenerate patch 18 with a fix for a crash in the backup conduit (would crash whenever no modifications had occurred since the last sync)

* Tue Jun 28 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.13-5.fc5
- Fixed gnome-pilot-2.0.12-move-conduits-autotools.patch to set GNOME_PILOT_CONDUIT_SEARCH_PATH to libdir/gnome-pilot/conduits
  rather than share/gnome-pilot/conduits; should be able to find conduits now.
- Finished removing test conduit

* Mon Jun 27 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.13-4.fc5
- Introduce pilot-link-version macro; use to bump version to 1:0.12.0-0.pre2.0
- Update gnome-pilot-2.0.12-port-to-pilot-link-0.12.patch to use version by Mark G Adams (#161824; patch 11)
  In addition to the correct port to 0.12, this contains three patches in GNOME bugzilla bug 274032 (error handling, not closing socket on connection, fixed DB reading loop)
- Renabled backup conduit (was patch 12), applying two patches by Mark G Adams (#161799; patch 18 and patch 19)
- Applied patch by Mark G Adams to fix some issues identified using valgrind in the backup conduit (gnome bug 209130, patch 17)
- Applied patch by Mark G Adams to fix some cleanup of XML handling (gnome bug 309077, patch 16)
- Patched to fix a missing #include <pi-error.h> (patch 20)
- Remove test conduit

* Fri Apr 29 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.13-2
- move .desktop file from /usr/share/control-center-2.0/capplets to 
  /usr/share/applications (#149228)

* Mon Apr 11 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.13-1
- 2.0.13
- removed these patches (now upstream)
  - fix_warnings (#114281)
  - fix_desktop (added in 2.0.12-5)
  - libtool ( linkage of gpilotd)

* Wed Mar 16 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.12-9
- fixed missing $RPM_BUILD_ROOT in move of conduits files; do it in install stanza instead of build
- include the version in build root

* Wed Mar 16 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.12-8
- added various patches I missed to CVS

* Wed Mar 16 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.12-7
- move conduits file from /usr/share/gnome-pilot/conduits to /usr/LIB_DIR/gnome-pilot/conduits (bz #135304)
- initial attempt at porting to pilot-link 0.12 API
- disabled backup conduit for now

* Sun Mar 13 2005 Than Ngo <than@redhat.com> 2.0.12-6
- rebuild against pilot-link-0.12

* Fri Feb 25 2005 Than Ngo <than@redhat.com> 2.0.12-5
- fix broken desktop file

* Fri Oct 15 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.12-4
- use a higher-res icon for the panel applet (bz #135897)

* Tue Sep 21 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.12-3
- added fix for compile-time warnings (bugzilla 114281)

* Mon Sep 20 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Wed Sep 15 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.12-1
- Update from 2.0.11 to 2.0.12
- Removed patch to fix bx #131560; this is now in the upstream source

* Thu Sep  2 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.11-2
- added patch to fix bz #131560

* Fri Aug 27 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.11-1
- updated from 2.0.10 to 2.0.11
- added the new XML device information file to the package (which replaces a hardcoded array in the code)
- removed patch that added Treo 600 to that array; the Treo 600 is in the XML file
- removed patch for gcc 3.4 support: a version of this is now in the upstream tarball

* Thu Aug 19 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.10-10
- Changed BuildRequires from gnome-panel to gnome-panel-devel (bz #125033)

* Mon Jun 21 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.10-9
- fixes for gcc3.4

* Thu Jun 17 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.10-8
- apply 64-bit build fix (#121268)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt
- use smp_mflags

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Dec  2 2003 Thomas Woerner <twoerner@redhat.com> 2.0.10-5
- removed rpath (patched libtool, removed LIBTOOL=%%{_bindir}/libtool from 
  make call in %%build)

* Fri Nov 28 2003 Jeremy Katz <katzj@redhat.com> 
- call scrollkeeper update in %post (#111159)
- prereq scrollkeeper and GConf2
- -devel should require libgnomeui-devel (#111160)

* Sun Nov  2 2003 Jeremy Katz <katzj@redhat.com> 2.0.10-4
- updated Treo600 patch

* Thu Oct  9 2003 Jeremy Katz <katzj@redhat.com> 2.0.10-3
- add patch from Dax Kelson for Treo600 support (#106731)

* Thu Jul 17 2003 Jeremy Katz <katzj@redhat.com> 2.0.10-1
- 2.0.10

* Tue Jul 15 2003 Jeremy Katz <katzj@redhat.com> 2.0.9-8
- rebuild
- try nuking LIBTOOL=/usr/bin/libtool

* Fri Jun 20 2003 Jeremy Katz <katzj@redhat.com> 2.0.9-6
- rebuild

* Fri Jun 20 2003 Jeremy Katz <katzj@redhat.com> 2.0.9-5
- fix 64bit problems

* Wed Jun 5 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun  5 2003 Jeremy Katz <katzj@redhat.com> 2.0.9-3
- rebuild

* Mon May 19 2003 Jeremy Katz <katzj@redhat.com> 2.0.9-2
- bye bye test conduit
- remove some unneeded files
- make LIBTOOL=/usr/bin/libtool

* Mon May 19 2003 Jeremy Katz <katzj@redhat.com> 2.0.9-1
- 2.0.9

* Mon May  5 2003 Jeremy Katz <katzj@redhat.com> 2.0.8-1
- 2.0.8

* Mon May  5 2003 Jeremy Katz <katzj@redhat.com> 2.0.7-2
- include the docs

* Mon May  5 2003 Jeremy Katz <katzj@redhat.com> 2.0.7-1
- update to 2.0.7

* Wed Apr 30 2003 Jeremy Katz <katzj@redhat.com> 2.0.6-1
- update to 2.0.6
- drop all patches, unneeded with 2.0 (or will need redoing if not)
- fix file lists
- update build requires to match what's in configure.in

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Dec 12 2002 Jeremy Katz <katzj@redhat.com> 0.1.71-1
- update to 0.1.71, drop obsolete patches

* Fri Dec  6 2002 Jeremy Katz <katzj@redhat.com> 0.1.70-1
- update to 0.1.70, update patches

* Thu Nov  7 2002 Jeremy Katz <katzj@redhat.com> 0.1.69-1
- update to 0.1.69, drop unneeded patches

* Sat Nov  2 2002 Jeremy Katz <katzj@redhat.com> 0.1.67-2
- bump epoch on pilot-link buildrequires

* Sat Nov  2 2002 Jeremy Katz <katzj@redhat.com> 0.1.67-1
- update to 0.1.67

* Fri Oct 18 2002 Jeremy Katz <katzj@redhat.com> 0.1.65-11
- update conduits patch so that new pilots show up in the conduits tab

* Tue Oct  8 2002 Jeremy Katz <katzj@redhat.com> 0.1.65-10
- make default synctype for the pilot apply (#71104)
- add an epoch to the pilot-link requirement (#74575)
- fix applying settings and the try button from the previous patch

* Mon Oct  7 2002 Jeremy Katz <katzj@redhat.com> 0.1.65-9
- put the conduits configuration in the main capplet (#75345)

* Thu Aug 29 2002 Jeremy Katz <katzj@redhat.com> 0.1.65-8
- don't use a relative symlink for the desktop file (#72911)
- include control center desktop files so that capplets work (#71852)

* Thu Aug 22 2002 Than Ngo <than@redhat.com> 0.1.65-7
- rebuild against new pilot-link

* Tue Aug   6 2002 Than Ngo <than@redhat.com> 0.1.65-6
- rebuild against pilot-link-0.11.2

* Mon Jul 29 2002 Jeremy Katz <katzj@redhat.com> 0.1.65-5
- move desktop file to redhat-menus and create symlink here (#69426)

* Thu Jul 18 2002 Than Ngo <than@redhat.com> 0.1.65-4
- rebuild against pilot-link-0.11

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue May 14 2002 Jeremy Katz <katzj@redhat.com>
- update to 0.1.65

* Mon May 13 2002 Jeremy Katz <katzj@redhat.com>
- rebuild in new environment

* Sun Apr 28 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- do not build on mainframe

* Wed Apr 17 2002 Jeremy Katz <katzj@redhat.com>
- don't build the applet.  now we don't need gnome-core

* Thu Feb 21 2002 Jeremy Katz <katzj@redhat.com>
- go straight through the build system, do not pass go

* Fri Jan 25 2002 Jeremy Katz <katzj@redhat.com>
- rebuild in new environment

* Sun Jan 13 2002 Jeremy Katz <katzj@redhat.com>
- update to 0.1.64

* Mon Nov 26 2001 Jeremy Katz <katzj@redhat.com>
- initial build for Red Hat Linux
- lang'ify
- add build requires

* Tue Sep 11 2001 Eskil Heyn Olsen <eskil@eskil.dk>
- Removed the test conduit from rpm

* Wed Feb 17 1999 Eskil Heyn Olsen <deity@eskil.dk>
- Created the .spec file

