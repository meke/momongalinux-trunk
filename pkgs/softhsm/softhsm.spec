%global momorel 6

Summary: Software HSM
Name: softhsm
Version: 1.1.4
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Daemons
Source0: http://www.opendnssec.org/files/source/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.opendnssec.org/files/source/%{name}-%{version}.tar.gz
BuildRequires: botan-devel
BuildRequires: sqlite-devel >= 3.4.2

%description
Software HSM.

%prep
%setup -q

%build
%{configure} \
	--with-sqlite3=%{_bindir} \
	--with-botan=%{_libdir} \
%ifarch x86_64
	--enable-64bit \
	--localstatedir=%{_localstatedir}/lib \
%else
	--localstatedir=%{_localstatedir}/lib \
%endif
	LDFLAGS="`pkg-config sqlite3 --libs`"

%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install

find %{buildroot} -name "*.la" -delete

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && %{__rm} -rf "$RPM_BUILD_ROOT"

%files
%defattr(-,root,root,-)
%doc NEWS README

%config %{_sysconfdir}/softhsm.conf.sample
%config(noreplace) %{_sysconfdir}/softhsm.conf
%{_localstatedir}/lib/softhsm
%{_mandir}/man5/softhsm.conf.5.*
%{_mandir}/man1/softhsm.1.*
%{_mandir}/man1/softhsm-keyconv.1.*
%{_bindir}/softhsm
%{_bindir}/softhsm-keyconv
#%%{_libdir}/libsofthsm.la
%{_libdir}/libsofthsm.so
%{_libdir}/libsofthsm.a

%changelog
* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-6m)
- fix BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-4m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-3m)
- update CFLAGS and LDFLAGS for sqlite3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-1m)
- initial made for Momonga

