%global momorel 8
%global srcrel 71320
%global srcname Fotowall
%global qtver 4.7.0

Summary: FotoWall is a fun tool for creating wallpapaers
Name: fotowall
Version: 0.9
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.enricoros.com/opensource/fotowall/
Source0: http://fotowall.googlecode.com/files/%{srcname}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.8-desktop.patch
Patch1: Fotowall-0.9-use-libv4l1-videodev.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: desktop-file-utils

%description
FotoWall is a fun tool for creating wallpapaers by mixing some of your favorite pictures in a nice and smooth high resolution composition.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .use-libv4l1-videodev~

%build
qmake-qt4

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64}/apps
convert -scale 16x16 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
install -m 644 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/

# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc GPL_V2 README.markdown
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-8m)
- fix v4l issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-6m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-3m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-2m)
- touch up spec file

* Wed Dec  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-1m)
- version 0.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-1m)
- version 0.8.2
- update URL

* Sun Sep 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- version 0.8.1
- remove fix-build.patch

* Sat Sep 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- version 0.8
- update desktop.patch
- add fix-build.patch

* Tue Aug  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-1m)
- version 0.6.1

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-1m)
- version 0.5

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-1m)
- initial package for wallpaper freaks using Momonga Linux
