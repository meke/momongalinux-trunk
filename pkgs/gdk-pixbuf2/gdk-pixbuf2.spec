%global		momorel 1
Name:           gdk-pixbuf2
Version:        2.30.7
Release: 	%{momorel}m%{?dist}
Summary:        An image loading library

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.gt.org
#VCS:           git:git://git.gnome.org/gdk-pixbuf
Source0:        http://download.gnome.org/sources/gdk-pixbuf/2.30/gdk-pixbuf-%{version}.tar.xz
NoSource:	0
BuildRequires:  glib2-devel >= 2.31.16
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libtiff-devel
BuildRequires:  jasper-devel
BuildRequires:  libX11-devel
BuildRequires:  gobject-introspection-devel >= 0.9.3
# gdk-pixbuf does a configure time check which uses the GIO mime
# layer; we need to actually have the mime type database.
BuildRequires:  shared-mime-info
# Bootstrap requirements
BuildRequires: autoconf automake libtool gtk-doc
BuildRequires: gettext

# We also need MIME information at runtime
Requires: shared-mime-info

# gdk-pixbuf was included in gtk2 until 2.21.2
Conflicts: gtk2 <= 2.21.2

Requires(preun): %{_sbindir}/alternatives
Requires(posttrans): %{_sbindir}/alternatives

%description
gdk-pixbuf is an image loading library that can be extended by loadable
modules for new image formats. It is used by toolkits such as GTK+ or
clutter.

%package devel
Summary: Development files for gdk-pixbuf
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel

# gdk-pixbuf was included in gtk2 until 2.21.2
Conflicts: gtk2-devel <= 2.21.2

%description devel
This package contains the libraries and header files that are needed
for writing applications that are using gdk-pixbuf.


%prep
%setup -q -n gdk-pixbuf-%{version}

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; CONFIGFLAGS=--enable-gtk-doc; fi;
 %configure $CONFIGFLAGS             \
        --with-x11                   \
        --with-libjasper             \
        --with-included-loaders=png  )
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT    \
             RUN_QUERY_LOADER_TEST=false

# Remove unpackaged files
rm $RPM_BUILD_ROOT%{_libdir}/*.la
rm $RPM_BUILD_ROOT%{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders/*.la

touch $RPM_BUILD_ROOT%{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders.cache

(cd $RPM_BUILD_ROOT%{_bindir}
 mv gdk-pixbuf-query-loaders gdk-pixbuf-query-loaders-%{__isa_bits}
)

%find_lang gdk-pixbuf

%post
/sbin/ldconfig
gdk-pixbuf-query-loaders-%{__isa_bits} --update-cache || :
[ -L %{_bindir}/gdk-pixbuf-query-loaders ] || rm -f %{_bindir}/gdk-pixbuf-query-loaders
%{_sbindir}/alternatives \
  --install %{_bindir}/gdk-pixbuf-query-loaders \
  gdk-pixbuf-query-loaders \
  %{_bindir}/gdk-pixbuf-query-loaders-%{__isa_bits} %{__isa_bits}

%postun
/sbin/ldconfig
if [ $1 -gt 0 ]; then
  gdk-pixbuf-query-loaders-%{__isa_bits} --update-cache || :
fi
[ -e "%{_bindir}/gdk-pixbuf-query-loaders-%{__isa_bits}" ] || \
  %{_sbindir}/alternatives --remove gdk-pixbuf-query-loaders \
  gdk-pixbuf-query-loaders-%{__isa_bits}

%files -f gdk-pixbuf.lang
%doc AUTHORS COPYING NEWS
%{_libdir}/libgdk_pixbuf-2.0.so.*
%{_libdir}/libgdk_pixbuf_xlib-2.0.so.*
%{_libdir}/girepository-1.0
%dir %{_libdir}/gdk-pixbuf-2.0
%dir %{_libdir}/gdk-pixbuf-2.0/2.10.0
%dir %{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders
%{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders/*.so
%ghost %{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders.cache
%{_bindir}/gdk-pixbuf-query-loaders-%{__isa_bits}
%{_mandir}/man1/gdk-pixbuf-query-loaders.1*

%files devel
%{_includedir}/gdk-pixbuf-2.0
%{_libdir}/libgdk_pixbuf-2.0.so
%{_libdir}/libgdk_pixbuf_xlib-2.0.so
%{_libdir}/pkgconfig/gdk-pixbuf-2.0.pc
%{_libdir}/pkgconfig/gdk-pixbuf-xlib-2.0.pc
%{_bindir}/gdk-pixbuf-csource
%{_bindir}/gdk-pixbuf-pixdata
%{_datadir}/gir-1.0
%{_datadir}/gtk-doc/html/*
%{_mandir}/man1/gdk-pixbuf-csource.1*


%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.30.7-1m)
- update to 2.30.7

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.5-1m)
- update to 2.26.5

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.4-1m)
- update to 2.26.4

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.1-2m)
- fix BTS #438

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24.1-2m)
- rebuild against libtiff-4.0.1

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.1-1m)
- update to 2.24.1

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.23.5-1m)
- [SECURITY] CVE-2011-2485
-- https://bugzilla.redhat.com/show_bug.cgi?id=CVE-2011-2485

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.4-1m)
- update to 2.23.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.23.3-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.3-1m)
- update to 2.23.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.1-2m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- initial build 2.22.0
- copy from gdk-pixbuf

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.0-21m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22.0-20m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-19m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.0-18m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.22.0-16m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-15m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22.0-14m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (9.22.0-13m)
- delete libtool library

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (9.22.0-12m)
- delete document (gtk-doc >= 1.5 cannot create document)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.0-11m)
- revise for xorg-7.0
- revise init_xdisplay macro

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.0-10m)
- suppress AC_DEFUN warning.

* Sun Nov 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22.0-9m)
- change BuildRequires: xorg-x11-Xvfb

* Mon Jul  5 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.22.0-8m)
- bug fix %%init_xdisplay

* Sat Jul  3 2004 zunda <zunda at freeshell.org>
- (kossori)
- gives up when no free display found after 10 retries (for non-X machiens)

* Wed Apr 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.22.0-7m)
- export DISPLAY=:$XDISPLAY

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.22.0-6m)
- rebuild w/o db1

* Tue Mar 16 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.22.0-5m)
- import bugfix patches from rawhide

* Tue Mar 16 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.22.0-4m)
- use Xvfb

* Tue Sep 30 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.22.0-3m)
- rebuild against autoconf-2.57-1m,
  use /usr/bin/auto{conf,header} instead of /usr/bin/auto{conf,header}-2.56

* Sat Jun 21 2003 Kenta MURATA <muraken2@nifty.com>
- (0.22.0-2m)
- use newly version of libtool because old version can not resolve
  inter library dependencies.

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.22.0-1m)
- version 0.22.0

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.21.0-1m)
- version 0.21.0

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.20.0-1m)
- version 0.20.0

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.19.0-1m)
- version 0.19.0

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.18.0-3m)
- add buildprereq: gnome-libs-devel

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.18.0-2k)
- version 0.18.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.17.0-2k)
- version 0.17.0

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.16.0-4k)
- rebuild against libpng 1.2.2.

* Mon Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.16.0-2k)
- version 0.16.0

* Fri Dec 14 2001 Shingo Akagaki <dora@kondara.org>
- (0.14.0-2k)
- version 0.14.0

* Tue Nov 13 2001 Tsutomu Yasuda <tom@kondara.org>
- (0.13.0-4k)
  added configure option 

* Fri Nov  2 2001 Shingo Akagaki <dora@kondara.org>
- (0.13.0-2k)
- version 0.13.0

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (0.11.0-10k)
- rebuild against libpng 1.2.0.

* Thu May 24 2001 Shingo Akagaki <dora@kondara.org>
- enable mmx

* Wed May 23 2001 Shingo Akagaki <dora@kondara.org>
- change spec file

* Sun Apr 19 2001 Shingo Akagaki <dora@kondara.org>
- version 0.11.0

* Thu Apr  5 2001 Shingo Akagaki <dora@Kondara.org>
- fix release

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.10.1

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.9.0-5k)
- modified Docdir with macros

* Fri Sep 1 2000 Shingo Akagaki <dora@kondara.org>
- version 0.9.0

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 0.8.0

* Wed Apr 23 2000 Shingo Akagaki <dora@kondara.org>
- version 0.7.0

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file
- version 0.6.0

* Sun Jan 30 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Sat Jan 22 2000 Ross Golder <rossigee@bigfoot.com>

- Borrowed from gnome-libs to integrate into gdk-pixbuf source tree

