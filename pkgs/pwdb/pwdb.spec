%global momorel 9

Summary: The password database library
Name: pwdb
Version: 0.62
Release: %{momorel}m%{?dist}
License: GPL or BSD
Group: System Environment/Base
Source: pwdb-%{version}.tar.gz
Patch100: pwdb-0.62-buildid.patch
BuildRequires:	binutils >= 2.18
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The pwdb package contains libpwdb, the password database library.
Libpwdb is a library which implements a generic user information
database.  Libpwdb was specifically designed to work with Linux's PAM
(Pluggable Authentication Modules).  Libpwdb allows configurable
access to and management of security tools like /etc/passwd,
/etc/shadow and network authentication systems including NIS and
Radius.

%prep
%setup -q
rm default.defs
ln -s defs/redhat.defs default.defs
# checking out of the CVS sometimes preserves the setgid bit on
# directories...
chmod -R g-s .

%patch100 -p1 -b .buildid~

%build
make LD=gcc RPM_OPT_FLAGS="%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/{etc,lib,usr/include/pwdb}

make	INCLUDED=%{buildroot}/usr/include/pwdb \
	LIBDIR=%{buildroot}/lib \
	LDCONFIG=":" \
	install

install -m 644 conf/pwdb.conf %{buildroot}/etc/pwdb.conf

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Copyright doc/pwdb.txt doc/html
%config /etc/pwdb.conf
/usr/include/pwdb
/lib/libpwdb.a
/lib/libpwdb.so
/lib/libpwdb.so.%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.62-7m)
- full rebuild for mo7 release

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62-6m)
- revise for rpm48
-- s/%%{PACKAGE_VERSION}/%%{version}/

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-3m)
- rebuild against gcc43

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62-2m)
- add pwdb-0.62-buildid.patch
- add BuildRequires: binutils >= 2.18

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.62-1m)
  update to 0.62

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (0.61.2-2k)
- version up.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (0.61.1-2k)
- version up.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.61-0).
- be a NoSrc :-P

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix setting the password for passwordless accounts. Patch from Thomas
  Sailer

* Mon Jan 31 2000 Cristian Gafton <gafton@redhat.com>
- rebuild to fix dependencies
