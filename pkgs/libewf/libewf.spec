%global         momorel 5

Name:           libewf
Version:        20100226
Release:        %{momorel}m%{?dist}
Summary:        Library for the Expert Witness Compression Format (EWF)

Group:          System Environment/Libraries
License:        BSD
URL:            http://sourceforge.net/projects/libewf/
Source0:	http://dl.sourceforge.net/sourceforge/libewf/libewf-%{version}.tar.gz
NoSource:       0
Source1:        http://dl.sourceforge.net/sourceforge/libewf/mount_ewf-20090113.py
NoSource:       1
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  e2fsprogs-devel
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  zlib-devel
BuildRequires:  python-devel


%description
Libewf is a library for support of the Expert Witness Compression Format (EWF),
it support both the SMART format (EWF-S01) and the EnCase format (EWF-E01). 
Libewf allows you to read and write media information within the EWF files.

%package -n     ewftools
Summary:        Utilities for %{name}
Group:          Applications/System
Requires:       %{name} = %{version}-%{release}
Requires:       fuse-python >= 0.2
Requires:       disktype

%description -n ewftools
Several tools for reading and writing EWF files.
It contains tools to acquire, verify and export EWF files.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       zlib-devel
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static --enable-wide-character-type

# Remove rpath from libtool
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# clean unused-direct-shlib-dependencies
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

mkdir -p $RPM_BUILD_ROOT/sbin
install -pm 0755 %{SOURCE1} $RPM_BUILD_ROOT/sbin/mount.ewf
ln -s mount.ewf $RPM_BUILD_ROOT/sbin/umount.ewf

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README documents/
%{_libdir}/*.so.*

%files -n ewftools
%defattr(-,root,root,-)
%{_bindir}/ewf*
%{_mandir}/man1/*
/sbin/*.ewf

%files devel
%defattr(-,root,root,-)
%{_includedir}/libewf.h
%{_includedir}/libewf/
%{_libdir}/*.so
%{_libdir}/pkgconfig/libewf.pc
%{_mandir}/man3/*

%changelog
* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20100226-5m)
- change checksum of Source1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100226-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100226-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100226-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20100226-1m)
- update to 20100226

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100126-2m)
- rebuild against openssl-1.0.0

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100126-1m)
- update to 20100126

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080501-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080501-1m)
- reassign version 20080501
- sync with Fedora 11 (20080501-6)
-- build with --disable-static

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.20080501.3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.20080501.2m)
- rebuild against rpm-4.6

* Thu Jun 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20080501.1m)
- update to 20080501

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20070512.3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20070512.2m)
- rebuild against gcc43

* Thu Feb 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0-0.20070512.1m)
- import to Momonga from Fedora

* Sat Feb  9 2008 kwizart < kwizart at gmail.com > - 0.4.20070512
- Rebuild for gcc43

* Wed Dec  5 2007 kwizart < kwizart at gmail.com > - 0-3.20070512
- Rebuild against new openssl

* Mon Nov  5 2007 kwizart < kwizart at gmail.com > - 0-2.20070512
- Update License to BSD with advertising

* Fri Nov  2 2007 kwizart < kwizart at gmail.com > - 0-1.20070512
- Initial package for Fedora

