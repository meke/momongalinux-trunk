%global momorel 4

Name:           sysprof
Version:        1.1.6
Release:        %{momorel}m%{?dist}
Summary:        A system-wide Linux profiler
Group:          Development/System
License:        GPLv2+
URL:            http://www.daimi.au.dk/~sandmann/sysprof/
Source0:        http://www.daimi.au.dk/~sandmann/sysprof/sysprof-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source1:        sysprof.desktop

BuildRequires:  gtk2-devel >= 2.6
BuildRequires:  libglade2-devel
BuildRequires:  binutils-devel
BuildRequires:  desktop-file-utils

Requires:       kernel >= 2.6.31

ExclusiveArch:  %{ix86} x86_64

%description
Sysprof is a sampling CPU profiler for Linux that collects accurate,
high-precision data and provides efficient access to the sampled
calltrees.


%prep
%setup -q -n sysprof-%{version}

# Fix README
iconv --from=ISO-8859-1 --to=UTF-8 README > README.new && \
touch -r README README.new && \
mv README.new README


%build
%configure
make %{?_smp_mflags}


%install
rm -rf ${RPM_BUILD_ROOT}
make install DESTDIR=${RPM_BUILD_ROOT}

desktop-file-install                                            \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        %{SOURCE1}


%clean
rm -rf ${RPM_BUILD_ROOT}


%files
%defattr(-,root,root,-)
%doc NEWS README COPYING TODO AUTHORS
%{_sysconfdir}/udev/rules.d/60-sysprof.rules
%{_bindir}/sysprof
%{_bindir}/sysprof-cli
%{_datadir}/pixmaps/sysprof-*.png
%{_datadir}/sysprof/
%{_datadir}/applications/*.desktop


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-1m)
- import from Fedora 13

* Tue Jun  8 2010 Gianluca Sforna <giallu gmail com> - 1.1.6-1
- New upstream release

* Sun Sep 27 2009 Gianluca Sforna <giallu gmail com> - 1.1.2-3
- Incorporate suggestions from package review
- Require kernel 2.6.31
- Updated description

* Sat Sep 26 2009 Gianluca Sforna <giallu gmail com> - 1.1.2-1
- New upstream release

* Wed Apr  9 2008 Gianluca Sforna <giallu gmail com> - 1.0.9-1
- version update to 1.0.9

* Tue Aug 28 2007 Gianluca Sforna <giallu gmail com> 1.0.8-2
- update License field

* Thu Dec 21 2006 Gianluca Sforna <giallu gmail com> 1.0.8-1
- version update to 1.0.8

* Tue Nov 21 2006 Gianluca Sforna <giallu gmail com> 1.0.7-1
- version update to 1.0.7

* Wed Nov  1 2006 Gianluca Sforna <giallu gmail com> 1.0.5-1
- version update

* Sun Oct  8 2006 Gianluca Sforna <giallu gmail com> 1.0.3-6
- better to use ExclusiveArch %%{ix86} (thanks Ville)

* Thu Oct  5 2006 Gianluca Sforna <giallu gmail com> 1.0.3-5
- add ExclusiveArch to match sysprof-kmod supported archs

* Tue Oct  2 2006 Gianluca Sforna <giallu gmail com> 1.0.3-4
- add .desktop file

* Fri Sep 30 2006 Gianluca Sforna <giallu gmail com> 1.0.3-3
- versioned Provides
- add BR: binutils-devel

* Fri Sep 29 2006 Gianluca Sforna <giallu gmail com> 1.0.3-2
- own sysprof directory

* Thu Jun 22 2006 Gianluca Sforna <giallu gmail com> 1.0.3-1
- version update
- use standard %%configure macro

* Sun May 14 2006 Gianluca Sforna <giallu gmail com> 1.0.2-1
- Initial Version
