%global momorel 1
%global snapshot -20131009
%global realversion 1.5.0
%global _hardened_build 1

Name: libmbim
Summary: Support library for the Mobile Broadband Interface Model protocol
Version: %{?realversion}
Release: 0.%{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Libraries
URL: http://www.freedesktop.org/software/libmbim

# If snapshot is defined, source will be a snapshot of git from the
# master branch on the given date.  To re-generate the tarball:
#  git://anongit.freedesktop.org/libmbim/libmbim
#  cd libmbim
#  git reset --hard <commit from snapshot date>
#  ./autogen.sh
#  make distcheck
#
Source: %{name}-%{realversion}%{snapshot}.tar.xz

BuildRequires: glib2-devel
BuildRequires: pkgconfig
BuildRequires: automake autoconf intltool libtool
BuildRequires: python >= 2.7
BuildRequires: pkgconfig(gudev-1.0) >= 147


%description
This package contains the libraries that make it easier to use MBIM
functionality from applications that use glib.


%package devel
Summary: Header files for adding MBIM support to applications that use glib
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: glib2-devel%{?_isa}

%description devel
This package contains the header and pkg-config files for developing
applications using MBIM functionality from applications that use glib.

%package utils
Summary: Utilities to use the MBIM protocol from the command line
Requires: %{name}%{?_isa} = %{version}-%{release}
License: GPLv2+

%description utils
This package contains the utilities that make it easier to use MBIM
functionality from the command line.


%prep
# NOTE: the documentation is pre-generated and shipped in the dist tarball;
# it is not build during the RPM build but the pre-generated docs are simply
# installed as-is.
%setup -q -n %{name}-%{realversion}

%build
./autogen.sh
%configure --disable-static --disable-gtkdoc
V=1 make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make install INSTALL="install -p" DESTDIR=$RPM_BUILD_ROOT
%{__rm} -f $RPM_BUILD_ROOT%{_libdir}/*.la

%check
make check


%post	-p /sbin/ldconfig
%postun	-p /sbin/ldconfig


%files
%doc COPYING NEWS AUTHORS README
%{_libdir}/libmbim-glib.so.*

%files devel
%{_includedir}/libmbim-glib/
%{_libdir}/pkgconfig/mbim-glib.pc
%{_libdir}/libmbim-glib.so
%{_mandir}/man*/*

%files utils
%{_bindir}/mbimcli
%{_bindir}/mbim-network


%changelog
* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-0.1m)
- initial commit Momonga Linux

* Thu Aug 15 2013 Dan Williams <dcbw@redhat.com> - 1.5.0-1.20130815git
- Initial Fedora release

