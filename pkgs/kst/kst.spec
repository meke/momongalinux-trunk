%global momorel 3
# Requires completely version matching of cfitsio
%global cfitsiover 3.350
%global qtver 4.8.6
%global kdever 4.13.0
%global kdelibsrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global srcname Kst

Summary: A data viewing program for KDE
Name: kst
Version: 2.0.7
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Engineering
URL: http://kst.kde.org/
Source0: http://dl.sourceforge.net/project/%{name}/%{srcname}%20%{version}/%{srcname}-%{version}.tar.gz
NoSource: 0
Patch0: kst-properties.patch
Patch1: %{name}-%{version}-qreal.patch
Patch2: %{name}-%{version}-linker-options.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: cfitsio = %{cfitsiover}
Requires: netcdf
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cfitsio-devel = %{cfitsiover}
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: getdata-devel >= 0.8.5
BuildRequires: gettext
BuildRequires: gsl-devel
BuildRequires: libtool
## rebuild against hdf5-1.8.12
BuildRequires: matio-devel >= 1.5.2-2m
BuildRequires: muParser-devel
BuildRequires: ncurses-devel
BuildRequires: netcdf-cxx-devel >= 4.2
BuildRequires: readline-devel
Obsoletes    : %{name}-docs < %{version}

%description
Kst is a real-time data viewing and plotting tool with basic data analysis
functionality. Kst contains many powerful built-in features and is
expandable with plugins and extensions. Kst is a KDE application.

Main features of kst include:
  * Robust plotting of live "streaming" data.
  * Powerful keyboard and mouse plot manipulation.
  * Powerful plugins and extensions support.
  * Large selection of built-in plotting and data manipulation functions,
    such as histograms, equations, and power spectra.
  * Color mapping and contour mapping capabilities for three-dimensional data.
  * Monitoring of events and notifications support.
  * Filtering and curve fitting capabilities.
  * Convenient command-line interface.
  * Powerful graphical user interface.
  * Support for several popular data formats.
  * Multiple tabs or windows.

%package devel
Summary: Development libraries and headers for kst
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}

%description devel
Headers and libraries required when building against kst.

%package netcdf
Summary: netcdf datasource plugin for kst
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}

%description netcdf
A plugin allowing kst to open and read data in netcdf format.

%package fits
Summary: fits datasource plugin for kst
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}
# Hack because cfitsio won't run if it's internal library version
# doesn't perfectly match between installed library and compiled
# against library.  Meh.
Requires: cfitsio = %{cfitsiover}

%description fits
A plugin allowing kst to open and read data and images contained within
fits files.  This includes healpix encoded fits files, and lfiio data.

%package getdata
Summary: getdata datasource plugin for kst
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}

%description getdata
A plugin allowing kst to open and read data in getdata (dirfile) format.

%prep
%setup -q
%patch0 -p1 -b .properties
%patch1 -p1 -b .qreal
%patch2 -p1 -b .linker

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
# -Dkst_merge_files=1 is failing for now
# https://bugs.kde.org/show_bug.cgi?id=322289
%{cmake_kde4} \
    -Dkst_merge_files=0 \
    -Dkst_rpath=0 \
    -Dkst_install_prefix=%{_prefix} \
    -Dkst_install_libdir=%{_lib} \
    -Dkst_test=0 \
    -Dkst_release=1 \
    -Dkst_verbose=1 \
    ../cmake
popd

make %{?_smp_mflags} kst2 -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} SUID_ROOT="" install -C %{_target_platform}
rm -f %{buildroot}%{_bindir}/test_*
# omit deprecated kde3-era stuff
rm -frv %{buildroot}%{_datadir}/{applnk,mimelink}/


%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING* ChangeLog INSTALL* NEWS README RELEASE.NOTES
%{_kde4_bindir}/%{name}2
%{_kde4_libdir}/libkst2core.so.*
%{_kde4_libdir}/libkst2math.so.*
%{_kde4_libdir}/libkst2widgets.so.*
%dir %{_kde4_libdir}/kst2
%dir %{_kde4_libdir}/kst2/plugins
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_bin.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_chop.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_convolution_convolve.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_convolution_deconvolve.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_correlation_autocorrelation.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_correlation_crosscorrelation.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_crossspectrum.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_differentiation.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_effectivebandwidth.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_genericfilter.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_interpolations_akima.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_interpolations_akimaperiodic.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_interpolations_cspline.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_interpolations_csplineperiodic.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_interpolations_linear.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_interpolations_polynomial.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_linefit.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_lockin.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_noiseaddition.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_periodogram.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_phase.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_shift.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_statistics.so
%{_kde4_libdir}/kst2/plugins/libkst2_dataobject_syncbin.so
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_ascii.so
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_qimagesource.so
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_matlab.so
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_sampledatasource.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_butterworth_bandpass.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_butterworth_bandstop.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_butterworth_highpass.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_butterworth_lowpass.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_cumulativeaverage.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_cumulativesum.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_despike.so
%{_kde4_libdir}/kst2/plugins/libkst2_filters_differentiation.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_exponential_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_exponential_weighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_gaussian_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_gaussian_weighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_gradient_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_gradient_weighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_kneefrequency.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_linear_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_linear_weighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_lorentzian_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_lorentzian_weighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_polynomial_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_polynomial_weighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_sinusoid_unweighted.so
%{_kde4_libdir}/kst2/plugins/libkst2_fits_sinusoid_weighted.so
%{_kde4_datadir}/applications/%{name}2.desktop
%{_kde4_iconsdir}/hicolor/*/apps/*.*
%{_mandir}/man1/%{name}2.1.*

%files devel
%defattr(-,root,root,-)
%{_kde4_libdir}/libkst2app.a
%{_kde4_libdir}/libkst2core.so
%{_kde4_libdir}/libkst2math.so
%{_kde4_libdir}/libkst2widgets.so

%files fits
%defattr(-,root,root,-)
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_fitsimage.so

%files netcdf
%defattr(-,root,root,-)
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_netcdf.so

%files getdata
%defattr(-,root,root,-)
%{_kde4_libdir}/kst2/plugins/libkst2_datasource_dirfilesource.so

%changelog
* Fri May 16 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-3m)
- rebuild against getdata-0.8.5

* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-2m)
- rebuild against matio-1.5.2-2m (rebuild against hdf5-1.8.12)
- use netcdf-cxx instead of netcdf

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7
- rebuild against cfitsio-3.350

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-2m)
- rebuild against cfitsio-3.330

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6
- rebuild against cfitsio-3.310

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-2m)
- rebuild against cfitsio-3.290

* Fri Oct 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4
- Obsoletes: kst-docs

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-2m)
- remove invalid character from kstdata_netcdf.desktop

* Sat Aug 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-18m)
- rebuild against cfitsio-3.280

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-17m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-16m)
- rebuild against netcfd-4.1.2

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-15m)
- rebuild against cfitsio-3.270

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-13m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-12m)
- rebuild against netcdf-4.1.1

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-11m)
- rebuild against cfitsio-3.250

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0-10m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0-9m)
- rebuild against readline6

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0-8m)
- touch up spec file

* Thu Jan 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0-7m)
- rewind desktop-file-install section to 1.6.0-1m
  fix up duplicate menu entries and move kst.desktop to correct space again
- sort %%doc again

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-6m)
- build with getdata

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-5m)
- rebuild against cfitsio-3.240
- sync with Fedora devel (separate some subpackages)

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-4m)
- rebuild against cfitsio-3.230

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-2m)
- rebuild against cfitsio-3.210

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0
- rebuild against cfitsio-3.200

* Sun Jun 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-6m)
- rebuild against cfitsio-3.181-1m

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-5m)
- fix build with new automake

* Sun Apr 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-4m)
- rebuild against cfitsio-3.140

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-3m)
- rebuild against cfitsio-3.130

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0
- rebuild against cfitsio-3.100

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-3m)
- revise %%{_docdir}/HTML/*/kst/common

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- modify %%files for smart handling of a directory

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-1m)
- initial package for Momonga Linux
