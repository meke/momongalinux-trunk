%global momorel 8

Name:      plotutils
Version:   2.5
Release:   %{momorel}m%{?dist}
Summary:   GNU vector and raster graphics utilities and libraries

Group:     Applications/Productivity
License:   GPL
URL:       http://www.gnu.org/software/plotutils/
Source0: ftp://ftp.gnu.org/gnu/plotutils/plotutils-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:   flex
BuildRequires:   libpng-devel
BuildRequires:   xorg-x11-proto-devel
BuildRequires:   libX11-devel
BuildRequires:   libXaw-devel
BuildRequires:   libXt-devel
BuildRequires:   libXext-devel

Requires(post):  /sbin/install-info
Requires(post):  /sbin/ldconfig
Requires(preun): /sbin/install-info

%description
The GNU plotutils package contains software for both programmers and
technical users. Its centerpiece is libplot, a powerful C/C++ function
library for exporting 2-D vector graphics in many file formats, both
vector and raster. It can also do vector graphics animations. Besides
libplot, the package contains command-line programs for plotting
scientific data. Many of them use libplot to export graphics


%package devel
Summary:     Headers for developing programs that will use %{name}
Group:       Development/Libraries
Requires:    %{name} = %{version}-%{release}


%description devel
This package contains the header files needed for developing %{name}
applications


%prep
%setup -q
cp -f lib/fontlist.c graph/
cp -f lib/fontlist.c plot/
cp -f lib/fontlist.c pic2plot/
cp -f lib/fontlist.c plotfont/
cp -f lib/fontlist.c tek2plot/


%build
%configure --disable-static --enable-libplotter --enable-libxmi --enable-ps-fonts-in-pcl
%make


%install
rm -rf $RPM_BUILD_ROOT docs-to-include
make install DESTDIR=$RPM_BUILD_ROOT
mkdir docs-to-include
mv ${RPM_BUILD_ROOT}%{_datadir}/ode docs-to-include
mv ${RPM_BUILD_ROOT}%{_datadir}/pic2plot docs-to-include
mv ${RPM_BUILD_ROOT}%{_datadir}/libplot docs-to-include
mv ${RPM_BUILD_ROOT}%{_datadir}/tek2plot docs-to-include
find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'
rm -f $RPM_BUILD_ROOT%{_infodir}/dir


%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/install-info %{_infodir}/libxmi.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/plotutils.info %{_infodir}/dir || :
/sbin/ldconfig


%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/libxmi.info %{_infodir}/dir || :
    /sbin/install-info --delete %{_infodir}/plotutils.info %{_infodir}/dir || :
fi


%postun -p /sbin/ldconfig


%files
%defattr(-, root, root, -)
%doc AUTHORS COMPAT COPYING NEWS THANKS README PROBLEMS KNOWN_BUGS
%doc docs-to-include/*
%{_bindir}/graph
%{_bindir}/ode
%{_bindir}/double
%{_bindir}/plot
%{_bindir}/pic2plot
%{_bindir}/plotfont
%{_bindir}/spline
%{_bindir}/tek2plot
%{_libdir}/*.so.*
%{_mandir}/man1/*
%{_infodir}/*.info*


%files devel
%defattr(-, root, root, -)
%doc TODO
%{_includedir}/*.h
%{_libdir}/*.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-2m)
- %%NoSource -> NoSource

* Fri Jun 15 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.5-1m)
- Import for Bug Fix (from Fedora)
- - Import such that inkscape can import PS and EPS
- - This fix the bug that inkscape can't render LaTeX formula

* Mon Aug 28 2006 Denis Leroy <denis@poolshark.org> - 2.5-3
- FE6 Rebuild

* Thu Aug 10 2006 Denis Leroy <denis@poolshark.org> - 2.5-2
- Some reformatting, added ldconfig Req

* Wed Aug  9 2006 Denis Leroy <denis@poolshark.org> - 2.5-1
- Initial version

