%global pmacutilsver 2.1
%global ppc64utilsver 1.1
%global powerpcutilsver 1.0.3
%global ps3pfutilsver 1.0.9
%global momorel 8

Summary: Linux/PPC64 specific utilities
Name: ppc64-utils
Version: 0.11
Release: %{momorel}m%{?dist}
License: GPL and CPL
Group: System Environment/Base
Source0: powerpc-utils-%{powerpcutilsver}.tar.gz
Source1: addRamDisk.c
Source2: addSystemMap.c
Source3: mkzimage
Source5: pmac-utils-%{pmacutilsver}.tar.gz
Source6: zImage-boot.tar.gz
Source7: powerpc-utils-papr-%{powerpcutilsver}.tar.gz
Source8: ps3pf_utils-%{ps3pfutilsver}.tar.bz2
Source9: zImage-wrapper.tar.gz
Source10: ps3.dts
Source11: ps3.dtb
Patch0: pmac-utils-0.4-cell.patch
Patch1: ppc64-utils-1.1-64k.patch
Patch2: ps3pf_utils-1.0.9-types.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: ppc ppc64
Requires: binutils yaboot
BuildRequires: linuxdoc-tools
BuildRequires: librtas-devel
Provides: powerpc-utils = %{powerpcutilsver}
Provides: powerpc-utils-papr = %{powerpcutilsver}

%description
A collection of utilities for Linux on PPC64 platforms.

%prep
%setup -q -n powerpc-utils-%{powerpcutilsver} -a 5 -a 6 -a 7 -a 8 -a 9
cp %{SOURCE1} %{SOURCE2} %{SOURCE3} .
%patch0 -p0 -b .cell
%patch1 -p1 -b .64k

%patch2 -p0
%build
make
find -type f -exec perl -pi -e "s,/usr/sbin/ibmras,%{_sbindir},g" {} \;
cc -O -fno-builtin -DSTDC_HEADERS addRamDisk.c -o addRamDisk
cc -O -fno-builtin -DSTDC_HEADERS addSystemMap.c -o addSystemMap
make -C pmac-utils-%{pmacutilsver} clean nvsetenv nvsetenv.8
CFLAGS="$RPM_OPT_FLAGS" make -C powerpc-utils-papr-%{powerpcutilsver}
make -C zImage-boot zImage.stub
make -C zImage-wrapper CFLAGS="$RPM_OPT_FLAGS"
make -C ps3pf_utils-%{ps3pfutilsver}
# After Core/Extras merge, we can require dtc and...
# dtc -O dtb %{SOURCE10} -o ps3.dtb


%install
rm -rf $RPM_BUILD_ROOT
make LIB_DIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT install
mkdir -p $RPM_BUILD_ROOT/sbin
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man8
mkdir -p $RPM_BUILD_ROOT/usr/share/ppc64-utils
install -m 755 mkzimage $RPM_BUILD_ROOT/%{_bindir}
install -m 755 addRamDisk $RPM_BUILD_ROOT/sbin
install -m 755 addSystemMap $RPM_BUILD_ROOT/sbin
install -m 644 zImage-boot/zImage.lds $RPM_BUILD_ROOT/usr/share/ppc64-utils
install -m 644 zImage-boot/zImage.stub $RPM_BUILD_ROOT/usr/share/ppc64-utils
pushd pmac-utils-%{pmacutilsver}
install -m 755 nvsetenv $RPM_BUILD_ROOT/sbin
install -m 755 nvsetenv.8 $RPM_BUILD_ROOT/%{_mandir}/man8
popd
pushd powerpc-utils-papr-%{powerpcutilsver}
make LIB_DIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT install
rm -rf $RPM_BUILD_ROOT/etc/init.d
install -m 644 scripts/ibmvscsis.sh $RPM_BUILD_ROOT/usr/share/ppc64-utils
popd

make -C zImage-wrapper DESTDIR=$RPM_BUILD_ROOT install
make -C ps3pf_utils-%{ps3pfutilsver} DESTDIR=$RPM_BUILD_ROOT install
install -m0644 %{SOURCE10} %{SOURCE11} $RPM_BUILD_ROOT/usr/share/ppc64-utils/wrapper
rm -rf $RPM_BUILD_ROOT/%{_docdir}/packages

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README COPYRIGHT
/sbin/addRamDisk
/sbin/addSystemMap
/sbin/boot-game-os
/sbin/find-other-os-flash
/sbin/nvsetenv
/sbin/other-os-flash-util
%{_sbindir}/*
%{_bindir}/*
/usr/share/ppc64-utils/
%{_mandir}/man8/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-3m)
- rebuild against gcc43

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-2m)
- remerge from FC

* Thu Dec 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-1m)
- remerge from  FC

-* Mon Oct 30 2006 Paul Nasrat <pnasrat@redhat.com> - 0.11-1
-- Update powerpc-utils to 1.0.3 (#212181)
-
-* Mon Oct 16 2006 Paul Nasrat <pnasrat@redhat.com> - 0.10-1
-- Include powerpc-utils and powerpc-utils-papr (#184685)
-
-* Sun Oct 01 2006 Jesse Keating <jkeating@redhat.com> - 0.9-15
-- rebuilt for unwind info generation, broken in gcc-4.1.1-21
-
-* Thu Sep 21 2006 Peter Jones <pjones@redhat.com> - 0.9-14
-- Fix iSeries support in addRamDisk
-
-* Fri Jul 14 2006 Jesse Keating <jkeating@redhat.com> - 0.9-13
-- rebuild
-
-* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.9-12.2.1
-- bump again for double-long bug on ppc(64)
-
-* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.9-12.2
-- rebuilt for new gcc4.1 snapshot and glibc changes
-
-* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
-- rebuilt
-
-* Fri Nov 18 2005 David Woodhouse <dwmw2@redhat.com> - 0.9-12
-- Update zImage.stub from 2.6.15-rc1 kernel
-- Link zImage at 3MiB since the POWER5 doesn't like it at 8MiB
-#'
-
-* Tue Oct 25 2005 Paul Nasrat <pnasrat@redhat.com> - 0.9-11
-- Bump internal version above rhel4 ver
-- Pull in later ppc64-utils (#169035)
-
-* Tue Oct 25 2005 Paul Nasrat <pnasrat@redhat.com> - 0.7-13
-- Stop scanning nvram on broken content (#170418)
-
-* Tue Sep 27 2005 David Woodhouse <dwmw2@redhat.com> - 0.7-12
-- Work around old Pegasos II 'claim' method bug
-
-* Wed Sep 21 2005 David Woodhouse <dwmw2@redhat.com> - 0.7-11
-- Build zImage.stub 32-bit even on ppc64
-
-* Tue Sep 20 2005 David Woodhouse <dwmw2@redhat.com> - 0.7-10
-- Include zImage stub, capable of both ppc32 and ppc64 boot.
-
-* Tue Mar 15 2005 Paul Nasrat <pnasrat@redhat.com> - 0.7-9
-- Rebuild 
-
-* Wed Oct 20 2004 Jeremy Katz <katzj@redhat.com> - 0.7-8
-- add some more error handling to new mkzimage
-
-* Wed Oct 20 2004 Jeremy Katz <katzj@redhat.com> - 0.7-7
- New mkzimage script from dhowells that works with the zImage.stub in 
-  our 2.6 kernels
-  - use mktemp instead of predictable filenames in /tmp
-  - use the zImage.lds in the package instead of output by the script

* Sun Jan 30 2005 mutecat <mutecat@momonga-linux.org>
- (0.7-1m)
- import from FC3

* Wed Sep 22 2004 Jeremy Katz <katzj@redhat.com> - 0.7-5
- rebuild

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Mar  3 2004 Jeremy Katz <katzj@redhat.com> - 0.7-3
- rebuild

* Tue Nov  4 2003 Matt Wilson <msw@redhat.com> 0.7-2
- make clean before buildinig nvsetenv to nuke the old binary (#107943)

* Tue Sep 16 2003 Jeremy Katz <katzj@redhat.com> 0.7-1
- mkzimage bugfix

* Mon Aug 18 2003 Matt Wilson <msw@redhat.com> 0.6-1
- incorporate ppc64-utils 0.4 from IBM

* Mon Aug 11 2003 Matt Wilson <msw@redhat.com> 0.5-1
- add nvsetenv

* Wed Jul 30 2003 Matt Wilson <msw@redhat.com> 0.4-1
- add mkzimage for pSeries zImage creation with initrd

* Tue May 20 2003 Matt Wilson <msw@redhat.com> 0.3-1
- add addSystemMap from arch/ppc64/boot/addSystemMap.c

* Fri Apr 18 2003 Jeremy Katz <katzj@redhat.com> 0.2-1
- make addRamDisk quieter by default, set $VERBOSE to get old output

* Tue Apr 15 2003 Jeremy Katz <katzj@redhat.com> 0.1-1
- include addRamDisk from kernel source tree (arch/ppc64/boot/addRamDisk.c) 
  to create kernel/initrd blobs on iSeries


