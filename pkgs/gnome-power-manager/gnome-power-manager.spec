%global momorel 1
Summary: GNOME power management service
Name: gnome-power-manager
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: GPLv2+ and GFDL
Group: Applications/System
Source: http://download.gnome.org/sources/gnome-power-manager/3.6/gnome-power-manager-%{version}.tar.xz
NoSource: 0
URL: http://projects.gnome.org/gnome-power-manager/

BuildRequires: gnome-doc-utils >= 0.3.2
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: cairo-devel
BuildRequires: upower-devel >= 0.9.0
BuildRequires: intltool
BuildRequires: glib2-devel >= 2.25.9
BuildRequires: gtk3-devel >= 2.91.3
BuildRequires: docbook-utils

Requires: gnome-icon-theme

# obsolete sub-package
Obsoletes: gnome-power-manager-extra <= 2.30.1
Provides: gnome-power-manager-extra

%description
GNOME Power Manager uses the information and facilities provided by UPower
displaying icons and handling user callbacks in an interactive GNOME session.

%prep
%setup -q

%build
%configure
%make

%install
make install DESTDIR=%{buildroot}

%find_lang %name --with-gnome

%postun
update-desktop-database %{_datadir}/applications &> /dev/null || :
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor &> /dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
touch --no-create %{_datadir}/icons/hicolor
gtk-update-icon-cache -q %{_datadir}/icons/hicolor &> /dev/null || :
update-desktop-database %{_datadir}/applications &> /dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING README
%{_bindir}/*
%{_datadir}/applications/*.desktop
%{_datadir}/glib-2.0/schemas/*.gschema.xml
%{_datadir}/gnome-power-manager/icons/hicolor/*/*/*.*
%{_datadir}/icons/hicolor/*/apps/gnome-power-statistics.*
%dir %{_datadir}/gnome-power-manager
%{_datadir}/glib-2.0/schemas/*.gschema.xml
%{_mandir}/man1/*.1.bz2

%changelog
* Fri Aug 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-3m)
- rebuild for glib 2.33.2

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-2m)
- disable HAL support

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-3m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-5m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-4m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.1-3m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.1-2m)
- add Requires: GConf2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.2-1m)
- update to 2.29.2

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
- delete patch0

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.3-2m)
- remove Plural-Forms (Patch0)

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.2-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-2m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- fix autostart

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92
- enable policykit

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-4m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-3m)
- rebuild with libXrandr-1.2.99.4
-- delete patch0

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-2m)
- add patch0 (for libXrandr) "REMOVE PLEASE"

* Thu Feb 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-1m)
- update to 2.25.3

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-3m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Tue Nov 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Wed Oct 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Sat Aug 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-4m)
- disable policykit

* Sun Apr 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-3m)
- enable policykit

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against gcc43

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sat Dec 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Thu Nov 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Sat Mar 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Sep 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0)
- update to 2.16.0

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-5m)
- rebuild against 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.3-4m)
- rebuild against expat-2.0.0-1m

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-3m)
- delete duplicated dir

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- delete duplicated dir

* Thu Apr 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.3-1m)
- version 2.14.3
- add Patch0 (gnome-power-manager-DBUSDIR.patch)
