%global momorel 7

Summary:	Toolchain for mastering recordable DVD media
Name:		dvd+rw-tools
Version:	7.1
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/Multimedia
URL:		http://fy.chalmers.se/~appro/linux/DVD+RW/
Source0:	http://fy.chalmers.se/~appro/linux/DVD+RW/tools/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0:		%{name}-7.0-wexit.patch
Patch1:		%{name}-7.0-glibc2.6.90.patch
Patch2:		%{name}-7.0-reload.patch
Patch3:		%{name}-7.0-wctomb.patch
Requires:	genisoimage
BuildRequires:	gcc-c++ >= 3.4.1-1m
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Collection of tools to master DVD+RW/+R/-R/-RW media. For further
information see http://fy.chalmers.se/~appro/linux/DVD+RW/.

%prep
%setup -q

%patch0 -p1 -b .wexit
%patch1 -p1 -b .glibc27
%patch2 -p1 -b .reload
%patch3 -p0 -b .wctomb

%build
make CFLAGS="%{optflags}" CPPFLAGS="%{optflags}"
make rpl8 btcflash CFLAGS="%{optflags}" CPPFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make prefix=%{buildroot}%{_prefix} manprefix=%{buildroot}%{_mandir} install

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE index.html
%{_bindir}/btcflash
%{_bindir}/dvd+rw-booktype
%{_bindir}/dvd+rw-format
%{_bindir}/dvd+rw-mediainfo
%{_bindir}/dvd-ram-control
%{_bindir}/growisofs
%{_bindir}/rpl8
%{_mandir}/man1/growisofs.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1-2m)
- rebuild against rpm-4.6

* Sun Nov 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.1-1m)
- version 7.1
- import dvd+rw-tools-7.0-reload.patch from Fedora
 +* Wed Mar 26 2008 Harald Hoyer <harald@redhat.com> 7.0-11
 +- fixed widechar overflow (bug #426068) (patch from Jonathan Kamens)
- import dvd+rw-tools-7.0-reload.patch from Fedora
 +* Tue Nov 20 2007 Harald Hoyer <harald@redhat.com> - 7.0-8
 +- added a patch to fix a reload problem on some drives,
 +  after a successful burn
- update wexit.patch
- remove merged phys.patch and pthread.patch
- remove bacula.patch
- License: GPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-4m)
- %%NoSource -> NoSource

* Sun Oct 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-3m)
- import dvd+rw-tools-7.0-glibc2.6.90.patch from Fedora
 +* Wed Aug 15 2007 Harald Hoyer <harald@redhat.com> - 7.0-5
 +- added limits.h to transport.hxx
- import dvd+rw-tools-7.0-pthread.patch from Fedora
 +* Thu Dec 14 2006 Harald Hoyer <harald@redhat.com> - 7.0-0.4
 +- set pthread stack size according to limit (#215818)
- import dvd+rw-tools-7.0-wexit.patch from Fedora
 +* Thu Jun 21 2007 Harald Hoyer <harald@redhat.com> - 7.0-4
 +- fixed exit status (#243036)
- import dvd+rw-tools-7.0-phys.patch from Fedora
 +* Wed Dec 13 2006 Harald Hoyer <harald@redhat.com> - 7.0-0.3
 +- use _SC_PHYS_PAGES instead of _SC_AVPHYS_PAGES to determine available memory
 +- Resolves: rhbz#216794
- import dvd+rw-tools-7.0-bacula.patch from Fedora
 +* Thu Jun 21 2007 Harald Hoyer <harald@redhat.com> - 7.0-4
 +- Allow session to cross 4GB boundary regardless of medium type.
 +  Add a -F option (used instead of -M or -Z), which displays 
 +  next_session offset and capacity. (#237967)

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-2m)
- good-bye cdrtools and welcome cdrkit

* Wed Nov 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-1m)
- version 7.0

* Fri Aug 11 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.1-3m)
- remove Patch0: dvd+rw-tools-6.1-64bit.patch (not necessary now)

* Sun Apr  9 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.1-2m)
- add Patch0: dvd+rw-tools-6.1-64bit.patch

* Sat Feb 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.1-1m)
- version 6.1

* Sun Oct  3 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.21-1m)
- Linux: fix for kernel version 2.6>=8, 2.6.8 itself is deficient,
  but the problem can be worked around by installing this version
  set-root-uid

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.20-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Thu Aug  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.20-1m)
- support DVD+R DoubleLayer

* Sun Apr 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.19-1m)
- address speed verification issues with NEC ND-2500 and Plextor PX-708A
- make DVD-RAM work in "poor-man" mode
- average write speed report at the end of recording
- LG GSA-4081B fails to "SET STREAMING" with "LBA OUT OF RANGE" for
  DVD+RW media, but not e.g. DVD-R

* Mon Apr  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.18.4.8.6-1m)
- minimize amount of compiler warnings on 64-bit platforms
- skip count-down if no_tty_check is set
- -use-the-force-luke=tracksize:size option by suggestion from K3b
- Linux: fix for "Bad file descriptor" with DVD+RW kernel patch

* Tue Feb 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (5.17.4.8.6-1m)
- initial import
