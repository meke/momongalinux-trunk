%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:		anki
Version:	1.2.8
Release:	%{momorel}m%{?dist}
Summary:	Flashcard program for using space repetition learning

Group:		Amusements/Games
# the file anki-%{version}/libanki/anki/features/chinese/unihan.db 
# was created out of  Unihan.txt from www.unicode.org (MIT license)
License:	GPLv3+ and MIT
URL:		http://www.ichi2.net/anki
Source0:	http://anki.googlecode.com/files/%{name}-%{version}.tgz
NoSource:       0

# Config change: don't check for new updates.
Patch0:		%{name}-%{version}-noupdate.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	python-devel >= 2.7 , python-setuptools, python-sqlalchemy
BuildRequires:  python-simplejson 
BuildRequires:	PyQt4-devel >= 4.7.6
BuildRequires:	desktop-file-utils
BuildRequires:  qt-devel >= 4.7.0

Requires:	qt, PyQt4
Requires:	python-sqlalchemy, python-simplejson, python-sqlite2
Requires:	python-matplotlib
Requires:	pygame
BuildArch:	noarch

%description
Anki is a program designed to help you remember facts (such as words
and phrases in a foreign language) as easily, quickly and efficiently
as possible. Anki is based on a theory called spaced repetition.

%prep
%setup -q
%patch0 -p1 -b .noupdate 

%build
pushd libanki
%{__python} setup.py build
popd

%{__python} setup.py build


%install
rm -rf %{buildroot}
pushd libanki
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
popd

%{__python} setup.py install -O1 --skip-build --root %{buildroot}

install -d %{buildroot}%{_datadir}/applications
desktop-file-install \
  --vendor= \
  --remove-category=KDE \
  --dir %{buildroot}%{_datadir}/applications \
  %{name}.desktop

install -d %{buildroot}%{_datadir}/pixmaps
install -m 644 icons/anki.png %{buildroot}%{_datadir}/pixmaps/

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog CREDITS README*
# libankiqt
%dir %{python_sitelib}/ankiqt
%{python_sitelib}/ankiqt/*.py*
%{python_sitelib}/ankiqt/ui
%{python_sitelib}/ankiqt/forms

# libanki
%dir %{python_sitelib}/anki
%{python_sitelib}/anki/*.py*
%{python_sitelib}/anki/importing
#%%{python_sitelib}/anki/features

%{python_sitelib}/anki/template/*.py*

# locale
%dir %{python_sitelib}/anki/locale/
%dir %{python_sitelib}/ankiqt/locale/
%lang(ar) %{python_sitelib}/*/locale/ar/
%lang(az) %{python_sitelib}/*/locale/az/
%lang(bg) %{python_sitelib}/*/locale/bg/
%lang(ca) %{python_sitelib}/*/locale/ca/
%lang(cs) %{python_sitelib}/*/locale/cs/
%lang(da) %{python_sitelib}/*/locale/da/
%lang(de) %{python_sitelib}/*/locale/de/
%lang(el) %{python_sitelib}/*/locale/el/
%lang(en) %{python_sitelib}/*/locale/en*/
%lang(eo) %{python_sitelib}/*/locale/eo/
%lang(es) %{python_sitelib}/*/locale/es/
%lang(et) %{python_sitelib}/*/locale/et/
%lang(fa) %{python_sitelib}/*/locale/fa/
%lang(fi) %{python_sitelib}/*/locale/fi/
%lang(fr) %{python_sitelib}/*/locale/fr/
%lang(gu) %{python_sitelib}/*/locale/gu/
%lang(he) %{python_sitelib}/*/locale/he/
%lang(hu) %{python_sitelib}/*/locale/hu/
%lang(it) %{python_sitelib}/*/locale/it/
%lang(ja) %{python_sitelib}/*/locale/ja/
%lang(ko) %{python_sitelib}/*/locale/ko/
%lang(lt) %{python_sitelib}/*/locale/lt/
%lang(lt) %{python_sitelib}/*/locale/lv/
%lang(mn) %{python_sitelib}/*/locale/mn/
%lang(ms) %{python_sitelib}/*/locale/ms/
%lang(nb) %{python_sitelib}/*/locale/nb/
%lang(nl) %{python_sitelib}/*/locale/nl/
%lang(oc) %{python_sitelib}/*/locale/oc/
%lang(pl) %{python_sitelib}/*/locale/pl/
%lang(pt) %{python_sitelib}/*/locale/pt*/
%lang(ro) %{python_sitelib}/*/locale/ro/
%lang(ru) %{python_sitelib}/*/locale/ru/
%lang(sl) %{python_sitelib}/*/locale/sl/
%lang(sv) %{python_sitelib}/*/locale/sv/
%lang(tlh) %{python_sitelib}/*/locale/tlh/
%lang(tr) %{python_sitelib}/*/locale/tr/
%lang(uk) %{python_sitelib}/*/locale/uk/
%lang(vi) %{python_sitelib}/*/locale/vi/
%lang(zh) %{python_sitelib}/*/locale/zh*/

%{python_sitelib}/*egg-info
%{_bindir}/anki
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.8-1m)
- update to 1.2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- add BuildRequires

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9.8.6-1m)
- update to 0.9.9.8.6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9.8.4-3m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.8.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.8.4-1m)
- import from Fedora 11
- update to 0.9.9.8.4

* Mon May 25 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.7.9b-1
- Update to new upstream version 0.9.9.7.9b to fix a syncing bug

* Tue May 12 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.7.9-1
- Update to new upstream version 0.9.9.7.9 to fix an update problem of the 
statusbar and of the titlebar

* Thu May 07 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.7.8-2
- Updated noupdate patch

* Wed May 06 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.7.8-1
- Update to new upstream version 0.9.9.7.8

* Sat Apr 11 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.7.4-1
- Update to new upstream version 0.9.9.7.4 (BZ 494598)
- Require python-matplotlib instead of numpy (BZ 495232)

* Wed Apr 01 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.7.1-1
- Update to new upstream version 0.9.9.7.1
- Drop unihaninstall patch (applied upstream)
- Updated noupdate patch
- Use original upstream tgz since upstream doesn't ship the example files
anymore

* Sun Mar 01 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.6-4
- Bump release

* Fri Feb 27 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.6-3
- Proper packaging of locale files

* Fri Feb 13 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.6-2
- Fixed license field
- Install unihan.db

* Wed Feb 11 2009 Christian Krause <chkr@fedoraproject.org> - 0.9.9.6-1
- First spec file for anki
