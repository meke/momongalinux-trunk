%global momorel 1
%global unstable 0
%global kdever 4.8.1
%global kdelibsrel 1m
%global kdebaserel 1m
%global kdesdkrel 1m
%global kdevplatformver 1.2.90
%global kdevplatformrel 1m
%global qtver 4.8.0
%global qtrel 1m
%global cmakever 2.6.4
%global cmakerel 1m
%global ftpdirver 1.0.0
%if 0%{?unstable}
%global sourcedir unstable/%{name}/%{ftpdirver}/src
%else
%global sourcedir stable/%{name}/%{ftpdirver}/src
%endif

Name: kdevelop-pg-qt
Summary: KDevelop-PG-Qt
Version: 1.0.0
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: GPLv2+
URL: http://techbase.kde.org/Development/KDevelop-PG-Qt_Introduction
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: bison
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: flex

%description 
KDevelop-PG-Qt is a parser generator written in readable source-code and
generating readable source-code. Its syntax was inspirated by AntLR. It
implements the visitor-pattern and uses the Qt library. That is why it
is ideal to be used in Qt-/KDE-based applications like KDevelop.

%package devel
Summary: KDevelop-PG-Qt development files
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
This package contains development files of %{name}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_kde4_bindir}/kdev-pg-qt

%files devel
%defattr(-,root,root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/cmake/KDevelop-PG-Qt

%changelog
* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.82-1m)
- update to 0.9.82

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- initial build for Momonga Linux

