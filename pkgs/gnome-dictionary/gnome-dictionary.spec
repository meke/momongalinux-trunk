%global momorel 1
Summary: A dictionary application for GNOME
Name:    gnome-dictionary
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: GPLv3+ and LGPLv2+ and GFDL
Group:   Applications/Text
#VCS: git:git://git.gnome.org/gnome-dictionary
Source:  	http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
URL:     http://www.gnome.org/

BuildRequires: gtk3-devel
BuildRequires: intltool
BuildRequires: gnome-doc-utils
BuildRequires: scrollkeeper
BuildRequires: desktop-file-utils
BuildRequires: docbook-dtds

Obsoletes: gnome-utils <= 3.3
Obsoletes: gnome-utils-libs <= 3.3
Obsoletes: gnome-utils-devel <= 3.3

%description
gnome-dictionary lets you look up words in dictionary sources.

%package devel
Summary: Development files for using libgdict
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
gnome-dictionary-devel contains header files and others that
are needed to build applications using the libgdict library.

%prep
%setup -q

%build
%configure
make %{_smp_mflags}

%install
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_libdir}/*.la
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop
%find_lang %{name} --with-gnome

%post -p /sbin/ldconfig

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f %{name}.lang
%doc NEWS AUTHORS README COPYING COPYING.libs COPYING.docs
%{_bindir}/gnome-dictionary
%{_datadir}/applications/gnome-dictionary.desktop
%{_datadir}/gdict-1.0
%{_datadir}/glib-2.0/schemas/org.gnome.dictionary.gschema.xml
%{_datadir}/gnome-dictionary
%{_mandir}/man1/gnome-dictionary.1.bz2
%{_libdir}/libgdict-1.0.so.*
%doc %{_datadir}/help/*/*

%files devel
%{_includedir}/gdict-1.0/
%{_libdir}/libgdict-1.0.so
%{_libdir}/pkgconfig/gdict-1.0.pc
%{_datadir}/gtk-doc/html/gdict

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Aug 02 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- import from fedora

