%global         momorel 1
%global         major 12.10
%global         minor 2
%global         libver 0.4

%define         with_gtk3 1

Summary:        Library for applications to pass a menu scructure accross DBus
Name:           libdbusmenu
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
License:        LGPLv3
Group:          System Environment/Libraries
URL:            https://launchpad.net/dbusmenu
Source0:        http://launchpad.net/dbusmenu/%{major}/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:       0
Patch1:		libdbusmenu-0.6.2-glib2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  intltool
BuildRequires:  libxml2-devel
BuildRequires:  glib2-devel >= 2.33.12
BuildRequires:  dbus-glib-devel
BuildRequires:  gtk2-devel
%if %{?with_gtk3}
BuildRequires:  gtk3-devel
%endif
BuildRequires:  json-glib-devel
BuildRequires:  vala-devel >= 0.17.5 vala-tools 
BuildRequires:  gobject-introspection-devel >= 1.33.10

%description
A small little library that was created by pulling out some common code
out of indicator-applet. It passes a menu structure across DBus so that
a program can create a menu simply without worrying about how it is
displayed on the other side of the bus.

%package -n dbusmenu-glib
Summary:        Library for applications to pass a menu scructure accross DBus
Group:          System Environment/Libraries

%description -n dbusmenu-glib
A small library for applications to raise "flags" on DBus for other
components of the desktop to pick up and visualize. Currently used by
the messaging indicator.

%package -n dbusmenu-gtk
Summary:        Library for applications to pass a menu scructure accross DBus
Group:          System Environment/Libraries

%description -n dbusmenu-gtk
A small little library that was created by pulling out some common code
out of indicator-applet. It passes a menu structure across DBus so that
a program can create a menu simply without worrying about how it is
displayed on the other side of the bus.

%package -n dbusmenu-glib-devel
Summary:        Library headers for %{name}
Group:          System Environment/Libraries
Requires:       dbusmenu-glib = %{version}
Provides:       %{name}-devel = %{version}-%{release}

%description -n dbusmenu-glib-devel
This is the libraries, include files and other resources you can use
to incorporate dbusmenu into applications.

%package -n dbusmenu-gtk-devel
Summary:        Library headers for dbusmenu
Group:          System Environment/Libraries
Requires:       dbusmenu-gtk = %{version}
Requires:       dbusmenu-glib-devel = %{version}
Provides:       dbusmenu-gtk-devel = %{version}-%{release}

%description -n dbusmenu-gtk-devel
This is the libraries, include files and other resources you can use
to incorporate dbusmenu into applications.

%if %{?with_gtk3}
%package -n dbusmenu-gtk3
Summary:        Library for applications to pass a menu scructure accross DBus
Group:          System Environment/Libraries

%description -n dbusmenu-gtk3
A small little library that was created by pulling out some common code
out of indicator-applet. It passes a menu structure across DBus so that
a program can create a menu simply without worrying about how it is
displayed on the other side of the bus.

%package -n dbusmenu-gtk3-devel
Summary:        Library headers for dbusmenu
Group:          System Environment/Libraries
Requires:       gtk3-devel
Requires:       dbusmenu-gtk3 = %{version}
Requires:       dbusmenu-glib-devel = %{version}
Provides:       dbusmenu-gtk3-devel = %{version}-%{release}

%description -n dbusmenu-gtk3-devel
This is the libraries, include files and other resources you can use
to incorporate dbusmenu into applications.
%endif

%package -n dbusmenu-jsonloader
Summary:        Library for applications to pass a menu scructure accross DBus
Group:          System Environment/Libraries

%description -n dbusmenu-jsonloader
A loader to turn JSON into dbusmenu menuitems

%package -n dbusmenu-jsonloader-devel
Summary:        Library headers for dbusmenu
Group:          System Environment/Libraries
Requires:       dbusmenu-gtk3 = %{version}
Requires:       dbusmenu-glib = %{version}
Requires:       dbusmenu-jsonloader = %{version}
Provides:       dbusmenu-jsonloader-devel = %{version}-%{release}

%description -n dbusmenu-jsonloader-devel
This is the libraries, include files and other resources you can use
to incorporate dbusmenu into applications.

%package tools
Summary:        Tools useful when building applications
Group:          System Environment/Libraries

%description tools
This package contains tools that are useful when building applications. 

%prep
%setup -q 
%patch1 -p1 -b .glib2~

%if %{with_gtk3}
pushd ..
cp -r %{name}-%{version} %{name}-gtk3-%{version}
popd
%endif


%build
%configure --disable-static --with-gtk=2 --enable-introspection=yes

%make %{?_smp_mflags} 

%if %{with_gtk3}
pushd ../%{name}-gtk3-%{version}
%configure \
      --disable-static \
      --disable-dumper \
      --disable-gtk-doc \
      --with-gtk=3 \
      --enable-introspection=yes 

%make %{?_smp_mflags}
popd
%endif

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
%if %{with_gtk3}
pushd ../%{name}-gtk3-%{version}
%makeinstall
%endif

#rm $RPM_BUILD_ROOT%%{_datadir}/doc/%{name}/README.dbusmenu-bench
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%clean
rm -rf --preserve-root %{buildroot}

%post -n dbusmenu-glib -p /sbin/ldconfig

%post -n dbusmenu-gtk -p /sbin/ldconfig

%if %{?with_gtk3}
%post -n dbusmenu-gtk3 -p /sbin/ldconfig
%postun -n dbusmenu-gtk3 -p /sbin/ldconfig
%endif

%postun -n dbusmenu-glib -p /sbin/ldconfig

%postun -n dbusmenu-gtk -p /sbin/ldconfig

%files -n dbusmenu-glib
%defattr(-,root,root)
%{_libdir}/%{name}-glib.so.*
%{_libdir}/girepository-1.0/Dbusmenu-%{libver}.typelib
%{_datadir}/gtk-doc/html/%{name}-glib

%files -n dbusmenu-glib-devel
%defattr(-,root,root)
%dir %{_includedir}/%{name}-glib-0.4/%{name}-glib/
%{_includedir}/%{name}-glib-0.4/%{name}-glib/*
%{_libdir}/%{name}-glib.so
#%exclude %{_libdir}/%{name}-glib.la
%{_libdir}/pkgconfig/dbusmenu-glib-%{libver}.pc
%{_datadir}/gir-1.0/Dbusmenu-%{libver}.gir
%{_datadir}/vala/vapi/Dbusmenu-%{libver}.vapi

%files -n dbusmenu-gtk
%defattr(-,root,root)
%{_libdir}/%{name}-gtk.so.*
#%exclude %{_libdir}/%{name}-gtk.la
%{_libdir}/girepository-1.0/DbusmenuGtk-%{libver}.typelib
%{_datadir}/gtk-doc/html/%{name}-gtk

%files -n dbusmenu-gtk-devel
%defattr(-,root,root)
%{_includedir}/%{name}-gtk-%{libver}
%{_libdir}/%{name}-gtk.so
%{_libdir}/pkgconfig/dbusmenu-gtk-%{libver}.pc
%{_datadir}/gir-1.0/DbusmenuGtk-%{libver}.gir
%{_datadir}/vala/vapi/DbusmenuGtk-%{libver}.vapi

%if %{?with_gtk3}

%files -n dbusmenu-gtk3
%defattr(-,root,root)
%{_libdir}/%{name}-gtk3.so.*
%{_libdir}/girepository-1.0/DbusmenuGtk3-%{libver}.typelib

%files -n dbusmenu-gtk3-devel
%defattr(-,root,root)
%{_includedir}/%{name}-gtk3-%{libver}
%{_libdir}/%{name}-gtk3.so
%{_libdir}/pkgconfig/dbusmenu-gtk3-%{libver}.pc
%{_datadir}/gir-1.0/DbusmenuGtk3-%{libver}.gir
%{_datadir}/vala/vapi/DbusmenuGtk3-%{libver}.vapi
%endif

%files -n dbusmenu-jsonloader
%defattr(-,root,root)
%{_libdir}/%{name}-jsonloader.so.*
#%exclude %{_libdir}/%{name}-jsonloader.la


%files -n dbusmenu-jsonloader-devel
%defattr(-,root,root)
%{_libdir}/%{name}-jsonloader.so
%dir %{_includedir}/%{name}-glib-0.4/
%{_includedir}/%{name}-glib-0.4/%{name}-jsonloader/*
%{_libdir}/pkgconfig/dbusmenu-jsonloader-%{libver}.pc

%files tools
%defattr(-,root,root)
%{_libexecdir}/dbusmenu-bench
%{_libexecdir}/dbusmenu-dumper
%{_libexecdir}/dbusmenu-testapp
%{_datadir}/%{name}/json/test-gtk-label.json
%{_defaultdocdir}/%{name}/

%changelog
* Sun Feb 23 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.10.2-1m)
- update to 12.10.2

* Thu Dec 26 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-3m)
- add patch for glib 2.36+

* Mon Sep 24 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.6.2-2m)
- modified spec to build for both gtk2 and gtk3 

* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.16-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.16-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.16-1m)
- update to 0.3.16
- add jsonloader and jsonloader-devel sub packages

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.9-7m)
- disable-introspection

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.9-6m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.9-5m)
- build with vala-0.9.2

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.9-4m)
- vapi and gir to devel

* Wed Jun 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.9-3m)
- add --enable-introspection=yes
- add patch0 (remove.please)

* Mon May 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.9-2m)
- add --enable-introspection=no (cannot build)

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-1m)
- initial build for Momonga Linux
