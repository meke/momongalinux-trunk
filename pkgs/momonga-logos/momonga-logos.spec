%global momorel 3

Name: momonga-logos
Summary: Momonga Linux icons and pictures.
Version: 8.0
Release: %{momorel}m%{?dist}
Group: System Environment/Base
Source0: momonga-logos-%{version}.tar.bz2
Source1: dummy-system-logo-white.png
License: see "Credits"
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Obsoletes: redhat-logos
Provides: redhat-logos
Provides: system-logos
Conflicts: kdebase <= 3.1.5
Conflicts: anaconda-images <= 10
# for /usr/share/icons/Bluecurve
Requires: bluecurve-icon-theme
# for /usr/share/icons/hicolor
Requires: hicolor-icon-theme
Obsoletes: momonga-images

BuildRequires: homura-backgrounds-gnome

%description
The momonga-logos package contains image files.

See the included COPYING file for information on copying and
redistribution.



%prep
%setup -q

%build

%install
rm -rf %{buildroot}

# should be ifarch i386
mkdir -p %{buildroot}/boot/grub
for i in bootloader/* ; do
  install -p -m 644 -D $i %{buildroot}/boot/grub
done

# firstboot - Momonga Linux theme
mkdir -p %{buildroot}%{_datadir}/firstboot/themes/Momonga8
for i in firstboot/themes/Momonga8/* ; do
  install -p -m 644 $i %{buildroot}%{_datadir}/firstboot/themes/Momonga8
done

# need plymouth
mkdir -p %{buildroot}%{_datadir}/pixmaps/
cp %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/system-logo-white.png

for size in 16x16 32x32 36x36 48x48 96x96 ; do
  mkdir -p %{buildroot}%{_datadir}/icons/hicolor/$size/apps
  #mkdir -p %{buildroot}%{_datadir}/icons/Bluecurve/$size/apps
  for i in icons/hicolor/$size/apps/* ; do
    install -m 644 $i %{buildroot}%{_datadir}/icons/hicolor/$size/apps
    #pushd ${buildroot}%{_datadir}/icons/Bluecurve/$size/apps
    #ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png icon-panel-menu.png
    #ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png gnome-main-menu.png
    #ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png kmenu.png
    #ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png start-here.png
    #popd
  done
done

## Anaconda images
(cd anaconda; make DESTDIR=%{buildroot} install)

## GNOME background image
mkdir -p %{buildroot}%{_datadir}/backgrounds/images
mkdir -p %{buildroot}%{_datadir}/gnome-background-properties
for i in backgrounds/images/*.jpg ; do
  install -p -m 644 -D $i %{buildroot}%{_datadir}/backgrounds/images/
done
install -p -m 644 backgrounds/momonga-backgrounds.xml %{buildroot}/%{_datadir}/gnome-background-properties/

## Hayabusa image
mkdir -p %{buildroot}%{_datadir}/backgrounds/hayabusa
install -p -m 644 -D backgrounds/hayabusa/hayabusa-2048x1536.jpg %{buildroot}%{_datadir}/backgrounds/hayabusa/
install -p -m 644 -D backgrounds/hayabusa/hayabusa-1920x1200.jpg %{buildroot}%{_datadir}/backgrounds/hayabusa/
install -p -m 644 backgrounds/hayabusa/hayabusa.xml %{buildroot}%{_datadir}/backgrounds/hayabusa/
install -p -m 644 backgrounds/momonga-hayabusa.xml %{buildroot}/%{_datadir}/gnome-background-properties/


## kdm background image
for i in kdm/* ; do
  install -p -m 644 -D $i %{buildroot}%{_datadir}/backgrounds/images/
done


## momonga face image
mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps/faces
for i in faces/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/pixmaps/faces/
done

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  if [ -f %{_datadir}/icons/hicolor/index.theme ]; then
    gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
  fi
fi

%files
%defattr(-, root, root)
%doc COPYING Credits
#%config(noreplace) %{_sysconfdir}/favicon.png
%dir %{_datadir}/firstboot/themes/Momonga8/
%{_datadir}/firstboot/themes/Momonga8/*
#%{_datadir}/plymouth/themes/charge/
#%{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/*/places/*
#%{_kde4_appsdir}/ksplash/Themes/Leonidas/2048x1536/logo.png
%{_datadir}/pixmaps/*
%{_datadir}/anaconda/pixmaps/*
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/anaconda/boot/*png
%{_datadir}/anaconda/boot/*.sh
#%{_datadir}/anaconda/boot/*.jpg

# we multi-own these directories, so as not to require the packages that
# provide them, thereby dragging in excess dependencies.
#%dir %{_datadir}/icons/Bluecurve
#%dir %{_datadir}/icons/hicolor
#%dir %{_datadir}/anaconda
#%dir %{_datadir}/anaconda/pixmaps/
#%dir %{_datadir}/kde-settings
#%dir %{_datadir}/kde-settings/kde-profile
#%dir %{_datadir}/kde-settings/kde-profile/default
#%dir %{_datadir}/kde-settings/kde-profile/default/share
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/16x16
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/16x16/places
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/24x24
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/24x24/places
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/32x32
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/32x32/places
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/36x36
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/36x36/places
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/48x48
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/48x48/places
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/96x96
#%dir %{_datadir}/kde-settings/kde-profile/default/share/icons/Fedora-KDE/96x96/places
#%dir %{_kde4_appsdir}
#%dir %{_kde4_appsdir}/ksplash
#%dir %{_kde4_appsdir}/ksplash/Themes
# should be ifarch i386
/boot/grub/splash*.xpm.gz
# end i386 bits
%dir %{_datadir}/backgrounds/images
%dir %{_datadir}/backgrounds/hayabusa
%{_datadir}/backgrounds/images/*
%{_datadir}/backgrounds/hayabusa/*
%dir %{_datadir}/gnome-background-properties
%{_datadir}/gnome-background-properties/*


%changelog
* Fri Apr  6 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (8.0-3m)
- add momonga face images

* Sat Mar 17 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (8.0-2m)
- disable gnome default.jpg

* Sat Mar 10 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (8.0-1m)
- Momonga Linux 8

* Sun Feb 12 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.994-6m)
- change gnome default image

* Fri Feb 10 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.994-5m)
- fukkatsu Hayabusa images
- change gnome default image file (default.png -> default.jpg)

* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org> 
- (7.994-4m)
- change symlink about Mo8 wallpaper 

* Wed Feb  8 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.994-3m)
- change syslinux splash images

* Sun Jan 29 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.994-2m)
- change syslinux splash images

* Fri Jan  6 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.994-1m)
- change anaconda images

* Fri Nov 11 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.993-2m)
- change firstboot theme directory (Momonga7->Momonga8)

* Mon Oct 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.993-1m)
- change splash-vesa.png 

* Sat Oct 15 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (7.992-1m)
- change anaconda images for Mo8
- add firstboot theme for Mo8
- change dummy-system-logo-white.png for Mo8

* Mon May 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.991-1m)
- update 7.991

* Fri May 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.99-1m)
- update 7.99
-- change anaconda's directory

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-6m)
- rebuild for new GCC 4.5

* Thu Sep  9 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-5m)
- change dummy-system-logo-white.png

* Sun Sep  5 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-4m)
- modify background image files (hayabusa)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-3m)
- full rebuild for mo7 release

* Fri Aug 27 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-2m)
- change background image license

* Thu Aug 26 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-1m)
- Momonga Linux 7
- add Hayabusa image

* Wed Aug 18 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (6.93-1m)
- add bootloader/splash-mo7.xpm.gz

* Wed Aug 18 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.92-1m)
- add backgrounds/images/momoandkao/jpg
- add kdm/momonga-chibi-fly.png

* Tue Aug 17 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.91-1m)
- update firstboot images
- change firstboot theme name (Momonga6 -> Momonga7)

* Tue Aug 17 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.90-1m)
- for Momonga Linux 7 beta
- change anaconda images

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 24 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.0-1m)
- modify background image list

* Wed Jul 22 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.91-2m)
- replace progress_first image

* Sun Jul 19 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.91-1m)
- rewite anaconda header image
- add anaconda progress_first image
- rewrite dummy-system-logo-white

* Sun Jul 19 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (5.90-6m)
- regenerate syslinux-vesa-splash.jpg

* Sat Jul 18 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.90-5m)
- grub splash image package miss
- orette ... orz

* Wed Jul 15 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.90-4m)
- add backgrounds
- add firstboot (Momonga Linux 6 theme)

* Sun Jul 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (5.90-3m)
- add Obsolete momonga-images

* Sun Jul 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (5.90-2m)
- modify duplicate file

* Sun Jul 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (5.90-1m)
- add Momonga Linux 6 splash image
- sync fedora-logos

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0-3m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0-2m)
- add /usr/share/pixmaps/system-logo-white.png
-- need plymouth

* Thu Oct  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0-1m)
- version 5.0-1m

* Sun Sep 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0-0.1.5m)
- rewind splash image

* Mon Aug 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0-0.1.4m)
- update grub image

* Sun Aug 24 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.0-0.1.3m)
- delete firstboot images

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0-0.1.2m)
- change Requires from redhat-artwork to bluecurve-icon-theme

* Fri May  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0-0.1.1m)
- set main-logo.png for Alpha1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0-3m)
- rebuild against gcc43

* Wed Oct  3 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0-2m)
- add Momonga Linux Logo icon

* Sun Aug  5 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0-1m)
- modify firstboot images

* Tue Jul 24 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-0.5m)
- add bootloader image for Momonga Linux 4

* Wed Jul  4 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0-0.4m)
- change firstboot images for Momonga Linux 4

* Tue Jun 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-0.3m)
- revise main-logo.png to rhgb format, 24bpp -> 32bpp

* Fri Jun 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-0.2m)
- set main-logo.png for Beta1

* Wed Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-0.1m)
- add Dummy rhgb images
- Fix ME!
-- /usr/share/rhgb/progress.png
-- /usr/share/rhgb/main-logo.png

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-2m)
- install grub images

* Mon Mar 12 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.1-1m)
- include grub images
- Provides: system-logos

* Sat Jul 29 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.0-3m)
- change firstboot-left.png

* Thu Jul 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.0-2m)
- change splash-small.png
- add firstboot-left.png

* Wed Jul 26 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.0-1m)
- change Momonga Linux 3 image

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-2m)
- delete duplicate dir

* Wed Jul 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- first release
