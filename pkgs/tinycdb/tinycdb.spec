%global momorel 1

Summary: TinyCDB - a Constant DataBase
Name: tinycdb
Version: 0.78
Release: %{momorel}m%{?dist}
#Source0: http://www.corpit.ru/mjt/tinycdb/tinycdb_%{version}.tar.gz
Source0: http://ftp.debian.org/debian/pool/main/t/tinycdb/tinycdb_%{version}.tar.gz
NoSource: 0
License: Public Domain
Group: Applications/Databases
URL: http://www.corpit.ru/mjt/tinycdb.html

%description
tinycdb is a small, fast and reliable utility set and subroutine
library for creating and reading constant databases. The database
structure is tuned for fast reading:
+ Successful lookups take normally just two disk accesses.
+ Unsuccessful lookups take only one disk access.
+ Small disk space and memory size requirements; a database
  uses 2048 bytes for the header and 24 bytes plus size of
  (key,value) per record.
+ Maximum database size is 4GB; individual record size is not
  otherwise limited.
+ Portable file format.
+ Fast creation of new databases.
+ No locking, updates are atomical.

This package contains both the utility and the development
files, together with nss_cdb module.

%package devel
Summary: Development files for tinycdb
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
tinycdb is a small, fast and reliable utility set and subroutine
library for creating and reading constant databases.

This package contains tinycdb development libraries and header files.

%prep
%setup -q

%build
make CFLAGS="%{optflags}" \
 staticlib sharedlib cdb-shared nss \
 sysconfdir=%{_sysconfdir}

%install
rm -rf %{buildroot}
%makeinstall DESTDIR=%{buildroot} \
 libdir=%{_libdir} bindir=%{_bindir} mandir=%{_mandir} \
 syslibdir=%{_libdir} sysconfdir=%{_sysconfdir} \
 includedir=%{_includedir} \
 install-all install-nss install-piclib install-sharedlib \
 INSTALLPROG=cdb-shared CP="cp -p"

chmod 755 %{buildroot}%{_libdir}/libnss_cdb.so.*
chmod 755 %{buildroot}%{_libdir}/libcdb.so.*

%files
%defattr(-,root,root)
%doc ChangeLog NEWS debian/changelog
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_libdir}/libcdb.so.*
%{_libdir}/libnss_cdb*
%{_sysconfdir}/cdb-Makefile

%files devel
%defattr(-,root,root)
%{_libdir}/libcdb.a
%{_libdir}/libcdb_pic.a
%{_libdir}/libcdb.so
%{_mandir}/man3/*
%{_includedir}/*

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.78-1m)
- update 0.78

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.77-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.77-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.77-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.77-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.77-1m)
- import to Momonga for dbskkd-cdb-2.00
