#!/bin/sh
# Strips proto_fsp.c from etherboot tarball
#
# Author: Eduardo Habkost <ehabkost@redhat.com>
#
# proto_fsp.c has a custom license that doesn't explicitly permit
# redistribution and has an additional indemnification requirement.

set -e

src="$1"
base="$(basename "$src" .tar.gz)"

tmpdir="$(mktemp -d)"
tar -C "$tmpdir" -zxf "$1"

if [ ! -d "${tmpdir}/${base}" ];then
	echo "Uh? Where is the $base directory?" >&2
	exit 1
fi


pushd "${tmpdir}/${base}"

# Sanity check:

# be conservative: look for all files with the problematic license
# and make sure it is the expected list of files.
lfiles="$(grep -r -l 'and that you hold the author' . | sort)"
expected="./src/core/proto_fsp.c"

if [ "$lfiles" != "$expected" ];then
	echo "Unexpected list of files." >&2
	echo "License list: $lfiles" >&2
	echo "Expected list: $expected" >&2
	exit 1
fi


# Clean the source files
rm -f src/core/proto_fsp.c

# proto_fsp.c is already inside a disabled-by-default #ifdef, so we
# can just make it empty and have the same build result.
echo '/* proto_fsp.c removed */' > src/core/proto_fsp.c

popd
tar -C "$tmpdir" -zcf "${base}-nofsp.tar.gz" "$base"
