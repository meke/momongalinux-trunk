%global momorel 8

%define formats  pxe rom zrom

# debugging firmwares does not goes the same way as a normal program.
# moreover, all architectures providing debuginfo for a single noarch
# package is currently clashing in koji, so don't bother.
%global debug_package %{nil}

Name: etherboot
Version: 5.4.4
Release: %{momorel}m%{?dist}
Summary: Etherboot collection of boot roms

Group: Development/Tools
License: GPLv2
URL: http://etherboot.org

# The original source:
#Source0: http://kernel.org/pub/software/utils/boot/etherboot/%{name}-%{version}.tar.gz
# The source without proto_fsp.c:
Source0: %{name}-%{version}-nofsp.tar.gz
# The script used to remove the proto_fsp.c files:
Source2: strip-etherboot-tarball.sh

Patch0: etherboot-5.4.4-e1000-tx-volatile.patch


BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%ifarch %{ix86}
BuildRequires: perl
BuildRequires: syslinux
BuildRequires: mtools

%package pxes
Summary: Etherboot - boot roms in .pxe format
Group: Development/Tools
BuildArch: noarch


%package roms
Summary: Etherboot - boot roms in .rom format
Group: Development/Tools
Requires: %{name}-roms-kvm
BuildArch: noarch

%package roms-kvm
Summary: Etherboot - boot roms supported by KVM, .rom format
Group: Development/Tools
BuildArch: noarch


%package zroms
Summary: Etherboot - boot roms in .zrom format
Group: Development/Tools
Requires: %{name}-zroms-kvm
BuildArch: noarch

%package zroms-kvm
Summary: Etherboot - boot roms supported by KVM, .zrom format
Group: Development/Tools
BuildArch: noarch




%description pxes
Etherboot is a software package for creating ROM images that can
download code over an Ethernet network to be executed on an x86
computer. Many network adapters have a socket where a ROM chip can be
installed. Etherboot is code that can be put in such a ROM

This package contains the Etherboot roms in .pxe format.


%description roms
Etherboot is an open source (GPL) network bootloader. It provides a direct
replacement for proprietary PXE ROMs, with many extra features such as
DNS, HTTP, iSCSI, etc

This package contains the Etherboot roms in .rom format.


%description roms-kvm
This package contains the etherboot ROMs for devices emulated by KVM,
in .rom format.


%description zroms
Etherboot is an open source (GPL) network bootloader. It provides a direct
replacement for proprietary PXE ROMs, with many extra features such as
DNS, HTTP, iSCSI, etc

This package contains the Etherboot roms in .zrom format.


%description zroms-kvm
This package contains the etherboot ROMs for devices emulated by KVM,
in .zrom format.
%endif

%description
Etherboot is a software package for creating ROM images that can
download code over an Ethernet network to be executed on an x86
computer. Many network adapters have a socket where a ROM chip can be
installed. Etherboot is code that can be put in such a ROM


%prep
%setup -q

%ifarch %{ix86}

%patch0 -p1 -b .e1000-tx-volatile

# Enable PXE_DHCP_STRICT (see bug #494541)
sed -i -e 's/# \(CFLAGS.*PXE_DHCP_STRICT\)/\1/' src/Config
# Disable ASK_BOOT (see bug #506616)
sed -i -e 's/^\(CFLAGS.*-DASK_BOOT\)=3/\1=-1/' src/Config

# undi is broken by now
sed -i -e '/undi/d' src/Families
%endif

%build
%ifarch %{ix86}
# %{optflags} wouldn't work with our hack to build the 32-bit binaries
# on x86_64, so use %{__global_cflags} instead, that doesn't include
# -m64 & related flags
# from /usr/lib/rpm/redhat/macros at momonga
%define extra_cflags `echo "-O1 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions --param=ssp-buffer-size=4 -Wno-unused-but-set-variable -Wno-unused-but-set-parameter"`
%define makeflags EXTRA_CFLAGS="%{extra_cflags}"

# make -j12 fail, 5.4.4-7m
# make -C src %%{?_smp_mflags} %%{makeflags} allpxes allroms allzroms
make -C src %{makeflags} allpxes allroms allzroms
%endif

%install
rm -rf %{buildroot}
%ifarch %{ix86}
mkdir -p %{buildroot}/%{_datadir}/%{name}/
pushd src/bin/
for fmt in %{formats};do
	for img in *.${fmt};do
		cp $img %{buildroot}/%{_datadir}/%{name}/
		echo %{_datadir}/%{name}/$img >> ../../${fmt}.list
	done
done
popd


# the roms supported by kvm will be packaged separatedly
# remove from the main rom list and add them to kvm.list
# ne is included only for compatibility with earlier versions of qemu
for fmt in rom zrom;do
	for rom in rtl8029 ne e1000-82542 pcnet32 rtl8139 virtio-net;do
		sed -i -e "/\/${rom}.${fmt}/d" ${fmt}.list
		echo %{_datadir}/%{name}/${rom}.${fmt} >> kvm.${fmt}.list
	done
done
%endif

%clean
rm -rf %{buildroot}

%ifarch %{ix86}
%files pxes -f pxe.list
%defattr(-,root,root,-)

%files roms -f rom.list
%defattr(-,root,root,-)

%files roms-kvm -f kvm.rom.list
%defattr(-,root,root,-)

%files zroms -f zrom.list
%defattr(-,root,root,-)

%files zroms-kvm -f kvm.zrom.list
%defattr(-,root,root,-)
%endif


%changelog
* Thu Apr 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4.4-8m)
- change makeflags from -O2 to -O1 to enable build, probably gcc46's bug?
- disable parallel build to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.4.4-5m)
- full rebuild for mo7 release

* Sun Dec  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.4-4m)
- add rtl8139 to etherboot-zroms-qemu

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.4-2m)
- remove duplicate directories

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.4-1m)
- import from Fedora 11 for qemu (kvm)

* Tue Jun 23 2009 Mark McLoughlin <markmc@redhat.com> - 5.4.4-16
- Fix e1000 PXE boot - caused by compiler optimization (bug #507391)

* Fri Jun 19 2009 Mark McLoughlin <markmc@redhat.com> - 5.4.4-15
- Disable ASK_BOOT (bug #506616)

* Mon May 25 2009 Mark McLoughlin <markmc@redhat.com> - 5.4.4-14
- Enable PXE_DHCP_STRICT to fix PXE boot failures (bug #494541)

* Tue Mar 03 2009 Glauber Costa <glommer@redhat.com> - 5.4.4.13
- Use conditionals on ix86 instead of BuildArch. This scheme is uglier, but
  works much better while rpm bug #442105 is not fixed yet.

* Mon Mar 02 2009 Glauber Costa <glommer@redhat.com> - 5.4.4-12
- bump version

* Mon Mar 02 2009 Glauber Costa <glommer@redhat.com> - 5.4.4-11
- Due to a not yet fixed bug in rpm, have to keep x86_64 in BuildArch

* Mon Mar 02 2009 Glauber Costa <glommer@redhat.com> - 5.4.4-10
- No more prebuilt binaries. Koji now supports noarch subpackages

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.4.4-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 27 2009 Eduardo Habkost <ehabkost@redhat.com> - 5.4.5-8
- Strips proto_fsp.c from etherboot tarball due to licensing reasons

* Mon Dec 01 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.5-7
- Updated prebuilt-binaries tarball to binaries from 5.4.5-6

* Mon Dec 01 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.5-6
- New subpackages: -zroms and -zroms-kvm

* Mon Dec 01 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.4-5
- Make packages own /usr/share/etherboot directory

* Thu Oct 02 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.4-4
- Use __global_cflags instead of optflags to allow building on x86_64
- Move the x86_64 build hack to %%build on the spec file, instead of
  a patch

* Tue Sep 30 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.4-3
- Use Fedora optflags when building

* Mon Sep 29 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.4-2
- Build on all arches, but with a hack to use prebuilt binaries
  on non-x86 arches
- Readded patch to build on x86_64
- BuildRequires glibc32 on x86_64

* Mon Sep 29 2008 Glauber Costa <glommer@redhat.com> - 5.4.4-1
- Changed arch to i386 only
- Upgraded to 5.4.4
- Removed virtio patch (in 5.4.4)
- Changed source URL.

* Fri Sep 26 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.3-3
- Package the ROMs supported by KVM into a separated subpackage

* Fri Sep 26 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.3-2
- cherry-picked virtio-net driver from git

* Fri Sep 26 2008 Eduardo Habkost <ehabkost@redhat.com> - 5.4.3-1
- First etherboot package, based on gPXE package from Glauber Costa
