From: Mark McLoughlin <markmc@redhat.com>
Subject: [PATCH] Use volatile for eth_hdr in e1000_transmit()

With gcc-4.4.0 and etherboot-5.4.4 we're seeing e1000 send packets
where the type field in the ethernet header is (incorrectly) zero.

Enabling debugging in drivers/net/e1000.c made the problem go away,
which was the first clue.

The code is as follows:

    struct eth_hdr {
        unsigned char dst_addr[ETH_ALEN];
 unsigned char src_addr[ETH_ALEN];
        unsigned short type;
    } hdr;
    ...
    hdr.type = htons (type);
    txhd = tx_base + tx_tail;
    tx_tail = (tx_tail + 1) % 8;
    ...
    txhd->buffer_addr = virt_to_bus (&hdr);
    ...
    E1000_WRITE_REG (&hw, TDT, tx_tail);

i.e. we're setting the type in the header on the stack, setting up a tx
descriptor to point to header on the stack and then writing the descriptor
number to the device queue.

Looking at the assembly, we see:

     36d:       8b 4c 24 38             mov    0x38(%esp),%ecx
     371:       86 cd                   xchg   %cl,%ch
     ...
     3fb:       89 90 18 38 00 00       mov    %edx,0x3818(%eax)
     ...
     407:       66 89 4c 24 1e          mov    %cx,0x1e(%esp)

i.e. we're only actually moving the results of the htons() into the header on
the stack until after we've set the TDT register. At that point the packet has
already been sent.

The problem is that the compiler has no way of knowing this memory is used as a
result of us writing to the register. So, if we do:

-       struct eth_hdr {
+       volatile struct eth_hdr {

we see:

     36c:       8b 44 24 38             mov    0x38(%esp),%eax
     370:       86 c4                   xchg   %al,%ah
     372:       66 89 44 24 1e          mov    %ax,0x1e(%esp)
     ...
     400:       89 90 18 38 00 00       mov    %edx,0x3818(%eax)

This fixes the problem.

Signed-off-by: Mark McLoughlin <markmc@redhat.com>

diff -up etherboot-5.4.4/src/drivers/net/e1000.c.volatile etherboot-5.4.4/src/drivers/net/e1000.c
--- etherboot-5.4.4/src/drivers/net/e1000.c.volatile	2009-06-23 12:19:35.296917613 +0100
+++ etherboot-5.4.4/src/drivers/net/e1000.c	2009-06-23 12:19:48.914663692 +0100
@@ -3733,7 +3733,7 @@ e1000_transmit (struct nic *nic, const c
 		    const char *p)	/* Packet */
 {
 	/* send the packet to destination */
-	struct eth_hdr {
+	volatile struct eth_hdr {
 		unsigned char dst_addr[ETH_ALEN];
 		unsigned char src_addr[ETH_ALEN];
 		unsigned short type;
