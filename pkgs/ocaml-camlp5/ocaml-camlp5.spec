%global momorel 1
%global ocamlver 3.12.1
%global origname camlp5

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Summary: A preprocessor and pretty-printer for OCaml programs
Name: ocaml-%{origname}
Version: 6.02.3
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Development/Languages
URL: http://pauillac.inria.fr/~ddr/camlp5/
Source0: http://pauillac.inria.fr/~ddr/camlp5/distrib/src/%{origname}-%{version}.tgz
NoSource: 0
Source1: camlp5-META
Patch0: patch-6.02.3-1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-ocamldoc
Requires: ocaml-runtime >= %{ocamlver}

%global __ocaml_requires_opts -i Asttypes -i Parsetree -i Pa_extend
%global __ocaml_provides_opts -i Dynlink -i Dynlinkaux -i Pa_extend

%description
Camlp5 is a preprocessor-pretty-printer of ocaml.  It is the
continuation of the classical Camlp4 with new features.

%package devel
Summary: Development files for %{name}
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%setup -q -n %{origname}-%{version}
%patch0 -p0

%build
./configure -prefix %{_prefix} -mandir %{_mandir} -libdir `ocamlc -where`
make all
%if %opt
make opt
strip meta/camlp5r.opt etc/camlp5o.opt
%endif

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
cp %{SOURCE1} %{buildroot}%{_libdir}/ocaml/camlp5/META

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc README LICENSE
%{_libdir}/ocaml/camlp5
%if %opt
%exclude %{_libdir}/ocaml/camlp5/*.a
%exclude %{_libdir}/ocaml/camlp5/*.cmxa
%exclude %{_libdir}/ocaml/camlp5/*.cmx
%endif
%exclude %{_libdir}/ocaml/camlp5/*.mli

%files devel
%defattr(-,root,root)
%doc README LICENSE CHANGES ICHANGES DEVEL UPGRADING
%if %opt
%{_libdir}/ocaml/camlp5/*.a
%{_libdir}/ocaml/camlp5/*.cmxa
%{_libdir}/ocaml/camlp5/*.cmx
%endif
%{_libdir}/ocaml/camlp5/*.mli
%{_bindir}/camlp5*
%{_bindir}/mkcamlp5*
%{_bindir}/ocpp5
%{_mandir}/man1/*.1*

%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.02.3-1m)
- update to 6.02.3
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.13-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.13-1m)
- update to 5.13
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.12-1m)
- update to 5.12

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.11-1m)
- update to 5.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.10-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.10-1m)
- rewrite %%opt and %%files in the Fedora devel style (5.10-1)
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.08-2m)
- rebuild against gcc43
 
* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.08-1m)
- update to 5.08
- rebuild against ocaml-3.10.2

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.07-2m)
- we must specify -libdir option at configure time on x86_64 architecture

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.07-1m)
- update to 5.07
- rebuild against ocaml-3.10.1-1m

* Mon Dec 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.05-1m)
- update to 5.05

* Tue Nov 27 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.04-1m)
- update to 5.04

* Tue Nov 20 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.03-1m)
- update to 5.03

* Wed Oct 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.01-1m)
- initial packaging
