%global momorel 7

%define rubyabi 1.9.1

Summary: EmacsRuby engine
Name: el4r
Version: 1.0.4
Release: %{momorel}m%{?dist}
Source0: http://www.rubyist.net/~rubikitch/archive/el4r-%{version}.tar.gz
NoSource: 0
URL: http://www.rubyist.net/~rubikitch/computer/el4r/
License: GPLv2+ and LGPLv2+
Group: Applications/Editors
BuildArch: noarch
Requires: ruby(abi) = %{rubyabi}
Requires: emacs

%description
El4r enables you to write Emacs programs in Ruby as well as in
EmacsLisp. I call the Ruby language to manipulate Emacs `EmacsRuby'.

El4r and Test::Unit enables you to unit-test EmacsLisp/EmacsRuby
programs automatically.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
ruby setup.rb config \
    --prefix=%{buildroot}%{_prefix} \
    --mandir=%{buildroot}%{_mandir}/man1 \
    --rbdir=%{buildroot}%{ruby_sitelibdir}
ruby setup.rb install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc BUGS Changes el4r.en.html el4r.ja.html
%{_bindir}/el4r
%{_bindir}/el4r-instance
%{_bindir}/el4r-rctool
%{_bindir}/el4r-runtest
%dir %{ruby_sitelibdir}/el4r
%{ruby_sitelibdir}/el4r/el4r-sub.rb
%{ruby_sitelibdir}/el4r/exec-el4r.rb
%dir %{ruby_sitelibdir}/el4r/emacsruby
%{ruby_sitelibdir}/el4r/emacsruby/el4r-mode.rb
%{ruby_sitelibdir}/el4r/emacsruby/stdlib.rb
%dir %{ruby_sitelibdir}/el4r/emacsruby/autoload
%{ruby_sitelibdir}/el4r/emacsruby/autoload/70el4r-mode.rb
%{_datadir}/emacs/site-lisp/el4r.el
%{_mandir}/man1/el4r.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-5m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-4m)
- Requires: ruby(abi)-1.9.1

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-1m)
- initial packaging
