%global momorel 1
%global boost_version 1.55.0
%global srcname libtorrent-rasterbar
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary:	C++ library that aims to be a good alternative to all the other bittorrent implementations around
Name:		rb_libtorrent
Version:	0.16.13
Release:	%{momorel}m%{?dist}
License:	see "COPYING"
Group:		System Environment/Libraries
URL:            http://www.rasterbar.com/products/libtorrent/
Source0:        http://libtorrent.googlecode.com/files/%{srcname}-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#BuildRequires:	asio-devel
BuildRequires:	GeoIP-devel
BuildRequires:	boost-devel >= %{boost_version}
BuildRequires:	openssl-devel >= 1.0.0
BuildRequires:	zlib-devel
BuildRequires:	python-devel >= 2.7
BuildRequires:	python-setuptools

%description
libtorrent is a C++ library that aims to be a good alternative to all the other bittorrent implementations around. 
It is a library and not a full featured client, although it comes with a working example client.
The main goals of libtorrent are:
    * to be cpu efficient
    * to be memory efficient
    * to be very easy to use

%package devel
Summary: Development library and include files for libtorrent
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
#Requires: asio-devel
Requires: boost-devel
Requires: openssl-devel

%description devel
The devel package contains the libtorrent library and the include files

%package devel-static
Summary: Static link library for libtorrent
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel-static
The devel-static package contains the libtorrent static link library files

%package	examples
Summary:	Example clients using %{name}
Group:		Applications/Internet
License:	BSD
Requires:	%{name} = %{version}-%{release}

%description	examples
The %{name}-examples package contains example clients which intend to
show how to make use of its various features. (Due to potential
namespace conflicts, a couple of the examples had to be renamed. See the
included documentation for more details.)


%package	python
Summary:	Python bindings for %{name}
Group:		Development/Languages
License:	see "COPYING"
Requires:	%{name} = %{version}-%{release}

%description	python
The %{name}-python package contains Python language bindings (the 'libtorrent'
module) that allow it to be used from within Python applications.

%prep
%setup -q -n %{srcname}-%{version}
## The RST files are the sources used to create the final HTML files; and are
## not needed.
rm -f docs/*.rst
## Finally, ensure that everything is UTF-8, as it should be.
iconv -t UTF-8 -f ISO_8859-15 AUTHORS -o AUTHORS.iconv
mv AUTHORS.iconv AUTHORS

sed -i -e 's|#!/bin/python|#!/usr/bin/python|' bindings/python/simple_client.py

# Use boost filesystem 2 explicitly (bug 654807)
sed -i -e '/Cflags:/s|^\(.*\)$|\1 -DBOOST_FILESYSTEM_VERSION=2|' \
	libtorrent-rasterbar.pc.in

%build
## XXX: Even with the --with-asio=system configure option, the stuff in
## the local include directory overrides that of the system. We don't like
## local copies of system code. :)
rm -rf include/libtorrent/asio*

# There are lots of warning about breaking aliasing rules, so
## for now compiling with -fno-strict-aliasing. Please check if
## newer version fixes this.
export CFLAGS="%{optflags} -fno-strict-aliasing"
export CXXFLAGS="%{optflags} -fno-strict-aliasing"

# Use boost filesystem 2 explicitly (bug 654807)
export CFLAGS="$CFLAGS -DBOOST_FILESYSTEM_VERSION=2"
export CXXFLAGS="$CXXFLAGS -DBOOST_FILESYSTEM_VERSION=2"

%configure --enable-static				\
	--enable-examples				\
	--enable-python-binding				\
	--with-asio=system				\
	--with-boost-filesystem				\
	--with-boost-program_options			\
	--with-boost-python				\
	--with-boost-regex				\
	--with-boost-system				\
	--with-boost-thread				\
	--with-libgeoip=system				\
	--with-encryption=on 				\
	--with-ssl 					\
	--with-zlib=system                              \
	--with-boost-libdir=%{_libdir}			\
	LIBS="-lrt"
make %{?_smp_mflags}

%check
make check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
## Install the python binding module.
pushd bindings/python
	%{__python} setup.py install -O1 --skip-build --root %{buildroot}
popd 

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}/%{_bindir}/client_test
rm -f %{buildroot}/%{_bindir}/simple_client

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/connection_tester
%{_bindir}/fragmentation_test
%{_bindir}/upnp_test
%{_libdir}/libtorrent-rasterbar.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/libtorrent/
%{_libdir}/pkgconfig/libtorrent-rasterbar.pc
%{_libdir}/libtorrent-rasterbar.so

%files devel-static
%defattr(-,root,root,-)
%{_libdir}/libtorrent-rasterbar.a

%files examples
%doc COPYING
%{_bindir}/*torrent*
%{_bindir}/enum_if
%{_bindir}/parse_hash_fails
%{_bindir}/parse_request_log
%{_bindir}/rss_reader
%{_bindir}/utp_test

%files	python
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog bindings/python/{simple_,}client.py
%{python_sitearch}/python_libtorrent-%{version}-py?.?.egg-info
%{python_sitearch}/libtorrent.so

%changelog
* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.13-1m)
- update to 0.16.13

* Wed Jan 15 2014 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.16.4-4m)
- add "--with-boost-libdir=" to configure argument for x86_64

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.4-3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.4-2m)
- rebuild against boost-1.52.0

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.4-1m)
- update to 0.16.4

* Sun Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.3-1m)
- update to 0.16.3

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.2-1m)
- update to 0.16.2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.1-2m)
- rebuild for boost

* Thu Jul 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.1-1m)
- update to 0.16.1

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.0-2m)
- rebuild for boost 1.50.0

* Sat May  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Sun Mar 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.10-1m)
- update to 0.15.10

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.9-1m)
- update to 0.15.9

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.8-2m)
- rebuild for boost-1.48.0

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.8-1m)
- update to 0.15.8

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.7-2m)
- rebuild for boost

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.7-1m)
- update to 0.15.7

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.6-2m)
- rebuild against python-2.7

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.6-1m)
- update to 0.15.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.5-5m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.5-4m)
- rebuild against boost-1.46.1

* Wed Mar  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.5-3m)
- import Fedora's hack to enable build with new boost

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.5-2m)
- rebuild against boost-1.46.0

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.5-1m)
- updat to 0.15.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.4-3m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.4-2m)
- rebuild against boost-1.44.0

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.4-1m)
- update to 0.15.4

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.3-1m)
- update to 0.15.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.10-5m)
- full rebuild for mo7 release

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.10-4m)
- rebuild against boost-1.43.0

* Wed Jun 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.10-3m)
- swtich to use boost/asio 

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.10-2m)
- fix build with gcc-4.4.4

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.10-1m)
- update to 0.14.10

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.9-2m)
- rebuild against openssl-1.0.0

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.9-1m)
- update to 0.14.9

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.8-1m)
- update to 0.14.8

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.7-3m)
- delete __libtoolize hack

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.7-2m)
- rebuild agaisnt boost-1.41.0

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.7-1m)
- update to 0.14.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.6-5m)
- rebuild against boost-1.40.0

* Mon Oct 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.6-4m)
- s|#!/bin/python|#!/usr/bin/python|

* Mon Oct 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.6-3m)
- remove duplicate %%configure to fix build

* Thu Oct  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6-2m)
- add python binding
- import some hack from fedora

* Sun Sep 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.6-1m)
- update to 0.14.6

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.5-1m)
- update to 0.14.5

* Sun Jul  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.4-2m)
- renamed to rb_libtorrent
- do not provide and obsolete libtorrent packages

* Wed Jun 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.4-1m)
- update to 0.14.4
- [SECURITY] CVE-2009-1760

* Mon May 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.3-3m)
- fix build with new libtool, %%global __libtoolize :

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-2m)
- rebuilt for boost-1.39.0

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.2-2m)
- rebuild against openssl-0.9.8k

* Tue Feb 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.1-5m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.1-4m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.1-3m)
- fix build on x86_64 with new boost

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.1-2m)
- rebuild against boost-1.37.0

* Mon Dec 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Mon Sep 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.1-2m)
- change URL to sourceforge.net

* Sun Aug 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.1-1m)
- initial build for Momonga Linux 5
