%global momorel 13
Name: donkey
Version: 0.5
Release: %{momorel}m%{?dist}
Summary: Donkey is an alternative for S/KEY's "key" command.
#'
License: GPL
Group: Applications/System
#Source: ftp://ftp.aist-nara.ac.jp/pub/Security/tool/donkey/donkey-%{version}.tar.gz
Source0: ftp://ftp.jp.freebsd.org/pub/FreeBSD/distfiles/donkey-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Donkey is an alternative for S/KEY's "key" command. The new feature that
the original key doesn't have is print an entry for skeykeys as
follows;

	kazu 0099 al02004          115d83956f1089b6  Apr 26,1995 22:13:27

This means that donkey is also an alternative for "keyinit". Since the
entry is printed to stdout (not to /etc/skeykeys), you can easily sent
it to remote operator by e-mail (with PGP signature or something). So,
it possible to initiate S/KEY without login from the console of the
host.

The name "Donkey" is an acronym of "Don't Key".

%prep
%setup -q

%build
# cd ./src
# rm -f ./configure
#
# make configure from configure.in by new autoconf
#
# autoconf
#
# cd -
#
./setup
cd ./obj/`uname`.`uname -r`

./configure
make

%install
rm -rf %{buildroot}
cd obj/`uname`.`uname -r`
mkdir -p %{buildroot}%{_bindir}
make BINDIR=%{buildroot}%{_bindir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc 00readme 00changes
%{_bindir}/donkey
%{_bindir}/key

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-8m)
- rebuild against gcc43

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-7m)
- good-bye autoconf

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-6m)
- s/Copyright:/License:/
- use momorel
- use rpm macros

* Thu Mar 11 2002 TABUCHI Takaaki <tab@kondara.org>
- (0.5-5k)
- use new autoconf

* Thu Mar 4 2002 TABUCHI Takaaki <tab@kondara.org>
- (0.5-3k)
- Kondarize
- NoSource: 0, setup -q

* Mon May 7 2001 Junichi Fujita <jeisi@mcn.ne.jp>
- 0.5-0vl1
