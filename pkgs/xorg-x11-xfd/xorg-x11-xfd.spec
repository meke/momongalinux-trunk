%global momorel 1
%global realname xfd

Summary: X.Org X11 X client utilities (%{realname})
Name: xorg-x11-%{realname}
Version: 1.1.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{realname}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: freetype-devel
BuildRequires: fontconfig-devel
BuildRequires: libXft-devel
BuildRequires: libXt-devel
BuildRequires: Xaw3d-devel

# FIXME: check if still needed for X11R7
Requires(pre): filesystem >= 2.4.6

%description
%{realname}

%prep
%setup -q -n %{realname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/%{realname}
%{_mandir}/man1/%{realname}.1*
%{_datadir}/X11/app-defaults/Xfd

%changelog
* Sat Feb 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update to 1.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against rpm-4.6

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- add BuildRequires: Xaw3d-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- %%NoSource -> NoSource

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- separated from xorg-x11-utils
