%global momorel 4

Summary:        A maildir indexer and searcher
Name:           mairix
Version:        0.22
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Internet
URL:            http://www.rc0.org.uk/mairix/
Source:         http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
mairix is a tool for indexing email messages stored in maildir format folders
and performing fast searches on the resulting index.  The output is a new
maildir folder containing symbolic links to the matched messages.

%prep
%setup -q

%build
CC=gcc CFLAGS="%{optflags}" ./configure \
--prefix=%{_prefix} --mandir=%{_mandir}
%make

cat <<EOF > dot.mairixrc
# ~/.mairixrc
base=/home/user
maildir=Maildir...
#mh=Mail/mhdir...
#mbox=Mail/inbox:Mail/outbox
mfolder=Mail/mairix
mformat=maildir
database=/home/user/.mairix_database
EOF

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -d -m 755 %{buildroot}%{_datadir}/config-sample/mairix
install -m 644 dot.mairixrc \
%{buildroot}%{_datadir}/config-sample/mairix/dot.mairixrc

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ACKNOWLEDGEMENTS
%doc COPYING
%doc NEWS
%doc README
%doc dotmairixrc.eg
%{_bindir}/mairix
%{_mandir}/man1/mairix.1*
%{_mandir}/man5/mairixrc.5*
%{_datadir}/config-sample/mairix

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-1m)
- update to 0.21
-- remove old info and html

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15-2m)
- rebuild against gcc43

* Wed Dec 15 2004 TAKAHASHI Tamotsu <tamo>
- (0.15-1m)

* Sat Feb 28 2004 TAKAHASHI Tamotsu <tamo>
- (0.14.1-2m)
- global momorel

* Mon Feb 23 2004 TAKAHASHI Tamotsu <tamo>
- (0.14.1-1m)
- major feature enhancement
 Note: database format has been changed
 Note: mairixrc syntax has been changed
- update URL
- Requires: info
- configure before make
- chmod docs
- add dot.mairixrc
- make install with DESTDIR

* Thu Jul  3 2003 TAKAHASHI Tamotsu <tamo>
- (0.11-1m)
- first import to momonga
- mairix.info.* -> mairix.info
- add COPYING and NEWS

* Sat Jan 18 2003 TAKAHASHI Tamotsu
- (0.10-0m)

* Sat Jan 11 2003 TAKAHASHI Tamotsu
- (0.9-0m)

* Mon Nov 25 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.7-0m)
- momonganized from mairix.spec.sample written by Richard P. Curnow
