%global momorel 5
%global abi_ver 1.9.1

# Generated from simple-rss-1.1.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname simple-rss
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: A simple, flexible, extensible, and liberal RSS and Atom reader for Ruby. It is designed to be backwards compatible with the standard RSS parser, but will never do RSS generation
Name: rubygem-%{gemname}
Version: 1.2.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://simple-rss.rubyforge.org/
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
A simple, flexible, extensible, and liberal RSS and Atom reader for Ruby. It
is designed to be backwards compatible with the standard RSS parser, but will
never do RSS generation.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Nov 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Thu Dec 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2-1m)
- update 1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- Initial package for Momonga Linux
