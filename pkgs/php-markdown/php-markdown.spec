%global momorel 1

Name:        php-markdown
Version:     1.0.1
Release:     %{momorel}m%{?dist}
Summary:     Markdown implementation in PHP
Group:       Development/Languages
License:     BSD
URL:         http://michelf.com/projects/php-markdown/
Source0:     http://michelf.com/docs/projets/%{name}-%{version}p.zip
NoSource:    0
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:   noarch
Requires:    php-pcre

%description
This is a PHP implementation of John Gruber's Markdown. This is a
Python implementation of John Gruber's Markdown. It is almost
completely compliant with the reference implementation.

%prep
%setup -q -n 'PHP Markdown %{version}p'

%build
# Empty build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_datadir}/php/
install -p -m 644 markdown.php %{buildroot}%{_datadir}/php/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc *.text
%{_datadir}/php/markdown.php

%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora for moodle-2.4.1

* Tue Jan 29 2013 Remi Collet <remi@fedoraproject.org> - 1.0.1p-1
- Updated to 1.0.1p
- don't requires php

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1n-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1n-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1n-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon May 03 2010 Rakesh Pandit <rakesh@fedoraproject.irg> 1.0.1n-1
- Updated to 1.0.1n

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1m-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun May 24 2009 Rakesh Pandit <rakesh@fedoraproject.irg> 1.0.1m-2
- Fixed mixed use of space and tabs, using install in place of cp

* Sun May 24 2009 Rakesh Pandit <rakesh@fedoraproject.org> 1.0.1m-1
- Initial package
