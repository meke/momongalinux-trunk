%global momorel 4

%define fontname  wqy-bitmap
%define fontconf  61-wqy-bitmapsong.conf
%define wqyroot   wqy-bitmapsong

Name:           %{fontname}-fonts
Version:        1.0.0
Release:        0.1.%{momorel}m%{?dist}
Summary:        WenQuanYi Bitmap Chinese Fonts

Group:          User Interface/X
License:        "GPLv2 with exceptions"
URL:            http://wenq.org/enindex.cgi
Source0:        http://downloads.sourceforge.net/wqy/wqy-bitmapsong-bdf-1.0.0-RC1.tar.gz
Source1:        61-wqy-bitmapsong.conf
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel, bdftopcf
Requires:       fontpackages-filesystem

%description
WenQuanYi bitmap fonts include all 20,932 Unicode 5.2
CJK Unified Ideographs (U4E00 - U9FA5) and 6,582
CJK Extension A characters (U3400 - U4DB5) at
5 different pixel sizes (9pt-12X12, 10pt-13X13,
10.5pt-14x14, 11pt-15X15 and 12pt-16x16 pixel).
Use of this bitmap font for on-screen display of Chinese
(traditional and simplified) in web pages and elsewhere
eliminates the annoying "blurring" problems caused by
insufficient "hinting" of anti-aliased vector CJK fonts.
In addition, Latin characters, Japanese Kanas and
Korean Hangul glyphs (U+AC00~U+D7A3) are also included.

%prep
%setup -q -n %{wqyroot}


%build
make wqyv1


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.pcf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.pcf

%doc AUTHORS ChangeLog COPYING README LOGO.png
%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-0.1.2m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.1.1m)
- sync with Fedora 13 (1.0.0-0.1.rc1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-6m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.9-5m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9-4m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-3m)
- rebuild against rpm-4.6

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.9-2m)
- remove configuration file for antialiasing

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.9-1m)
- import from Fedora

*Tue Dec 11 2007 Qianqian Fang <fangqq@gmail.com> 0.9.9-4
- used tag 0.9.9-3 due to previous mistake, have to bump release to tag

*Tue Dec 11 2007 Qianqian Fang <fangqq@gmail.com> 0.9.9-3
- replace %SOURCE1 by actual config file name

*Tue Dec 11 2007 Qianqian Fang <fangqq@gmail.com> 0.9.9-2
- fontconfig file was rewriten to minimize impact to non-CJK users (#381311)

*Sat Nov 17 2007 Qianqian Fang <fangqq@gmail.com> 0.9.9-1
- avoid using Latin glyphs from this font in monospace environment such as in consoles

*Sat Nov 10 2007 Qianqian Fang <fangqq@gmail.com> 0.9.9-0
- include the complete CJK Extension A glyphs (6,582 x 4 point sizes), provide full coverage to Standard GB18030
- first round standard-compliance validation for all CJK basic characters between U4E00-U9FA5
- numerous bitmap glyph fine-tuning and corrections

* Tue Aug 28 2007 Jens Petersen <petersen@redhat.com> - 0.8.1-8
- fix font catalogue symlink (#252279)
- drop fontconfig and xorg-x11-font-utils requires for scripts
- drop freetype and xorg-x11-font-utils requires
- run mkfontdir at buildtime
- use the standard font scriptlets

*Thu Aug 16 2007 Qianqian Fang <fangqq@gmail.com> 0.8.1-7
- drop chkfontpath from the spec file, use fontpath.d instead (#252279)

*Fri May 18 2007 Qianqian Fang <fangqq@gmail.com> 0.8.1-6
- final polishing of spec file and initial upload to cvs

*Thu May 10 2007 Qianqian Fang <fangqq@gmail.com> 0.8.1-5
- further polishing the spec file (by Jens Petersen)

*Sun May 06 2007 Qianqian Fang <fangqq@gmail.com> 0.8.1-4
- use nightly-build cvs20070506-bdf as sources

*Fri May 04 2007 Qianqian Fang <fangqq@gmail.com> 0.8.1-3
- remove superfluous conflicts and provides

*Thu Apr 12 2007 Qianqian Fang <fangqq@gmail.com> 0.8.1-2
- update to upstream new release 0.8.1-7

*Sun Feb 18 2007 Qianqian Fang <fangqq@gmail.com> 0.8.0-1
- initial packaging for Fedora (#230560)
