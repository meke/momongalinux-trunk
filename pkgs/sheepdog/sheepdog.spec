%global momorel 1
%global gitrev geda5993
%global dirname sheepdog-sheepdog-eda5993
%global srcname sheepdog-sheepdog-v%{version}-0-%{gitrev}.tar.gz

Name: sheepdog
Summary: The Sheepdog Distributed Storage System for KVM/QEMU
Version: 0.8.1
Release: %{momorel}m%{?dist}
License: GPLv2 and GPLv2+
Group: System Environment/Base
URL: http://www.osrg.net/sheepdog
# See  https://github.com/collie/sheepdog/
Source0: https://github.com/sheepdog/sheepdog/archive/v%{version}.tar.gz
NoSource: 0
Source1: sheepdog.service

# Runtime bits
Requires: corosync
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv

# Build bits
BuildRequires: autoconf automake
BuildRequires: corosync >= 2.3.1 corosynclib >= 2.3.1 corosynclib-devel >= 2.3.1
BuildRequires: userspace-rcu-devel >= 0.8.4

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
This package contains the Sheepdog server, and command line tool which offer
a distributed object storage system for KVM.

%prep
%setup -q

%build
./autogen.sh
%{configure} --with-initddir=%{_initscriptdir}

make %{_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/%{_unitdir}
cp -a %{SOURCE1} %{buildroot}/%{_unitdir}/

## tree fixup
# drop static libs
rm -f %{buildroot}%{_libdir}/*.a

rm -rf %{buildroot}%{_initscriptdir}

%clean
rm -rf %{buildroot}

%post 
if [ $1 -eq 1 ] ; then 
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable sheepdog.service > /dev/null 2>&1 || :
    /bin/systemctl stop sheepdog.service > /dev/null 2>&1 || :
fi

%postun
%triggerun -- sheepdog < 0.2.4-2
/usr/bin/systemd-sysv-convert --save httpd >/dev/null 2>&1 ||:
/sbin/chkconfig --del httpd >/dev/null 2>&1 || :
/bin/systemctl try-restart sheepdog.service >/dev/null 2>&1 || :
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart sheepdog.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%doc COPYING README INSTALL
%{_sysconfdir}/bash_completion.d/*
%{_sbindir}/dog
%{_sbindir}/sheep
%{_sbindir}/sheepfs
%{_sbindir}/shepherd
%{_unitdir}/sheepdog.service
%dir %{_localstatedir}/lib/sheepdog
%{_mandir}/man8/*.8*

%changelog
* Wed Apr 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-1m)
- update 0.8.1

* Sun Aug 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-3m)
- rebuild against corosync-2.3.1

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-2m)
- support systemd

* Sun Jan  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-1m)
- update 0.3.0

* Wed Aug 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-1m)
- Initial Commit Momonga Linux
