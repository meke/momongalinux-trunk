%global         momorel 1
%global         srcname QtCurve-KDE4

Name:		qtcurve-kde4
Version:	1.8.14
Release:	%{momorel}m%{?dist}
Summary:	This is a set of widget styles for Qt4/KDE4 based apps
Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://www.kde-look.org/content/show.php?content=40492
Source0:	http://craigd.wikispaces.com/file/view/%{srcname}-%{version}.tar.bz2
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kdebase-workspace-devel
BuildRequires:  gettext
Requires:       kdelibs
Requires:       kdebase-workspace

%global kde4_version %((kde4-config --version 2>/dev/null || echo "KDE 4.3.0") | grep ^KDE | cut -d' ' -f2)

%description
QtCurve is a desktop theme for the GTK+ and Qt widget toolkits,
allowing users to achieve a uniform look between these widget toolkits.

%prep
%setup -q -n QtCurve-KDE4-%{version}
## Due to the name change this sed is needed for find-lang.
sed -i 's,set(catalogname qtcurve),set(catalogname qtcurve-kde4),g' po/CMakeLists.txt

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
## Build qtcurve-kde4 with xbar support
%{cmake_kde4} .. -DQTC_XBAR_SUPPORT=true
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast -C %{_target_platform} DESTDIR=%{buildroot}
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS README TODO COPYING ChangeLog
%{_kde4_appsdir}/QtCurve
%{_kde4_appsdir}/color-schemes/*.colors
%{_kde4_appsdir}/kstyle/themes/qtcurve.themerc
## No GUI .desktop file
%{_kde4_appsdir}/kwin/*.desktop
%{_kde4_libdir}/kde4/kstyle_qtcurve_config.so
%{_kde4_libdir}/kde4/kwin3_qtcurve.so
%{_kde4_libdir}/kde4/kwin_qtcurve_config.so
%{_kde4_libdir}/kde4/plugins/styles/qtcurve.so

%changelog
* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.14-1m)
- update to 1.8.14

* Wed Jul 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.13-1m)
- update to 1.8.13

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.12-1m)
- update to 1.8.12

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.11-1m)
- update to 1.8.11

* Tue Oct 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.9-1m)
- update to 1.8.9

* Mon Jul  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.8-1m)
- update to 1.8.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.7-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.7-1m)
- update to 1.8.7

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6-1m)
- update to 1.8.6

* Wed Feb 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Sat Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- import from Fedora devel and update to 1.5.2

* Thu Jul 08 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.5.1-1
- qtcurve-kde4 1.5.1 bugfix release

* Thu Jul 01 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.5.0-1
- qtcurve-kde 1.5.0

* Sun Jun 06 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.4.3-1
- qtcurve-kde 1.4.3

* Mon May 17 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.4.0-1
- qtcurve 1.4.0 

* Mon Apr 12 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.3.0-1
- qtcurve 1.3.0

* Tue Mar 02 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.1.1-1
- New upstream source 1.1.1

* Tue Feb 23 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.1.0-1
- New upstream source 1.1.0

* Mon Feb 01 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.0.2-1
- Update to upstream source 1.0.2

* Mon Jan 11 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.0.1-1
- Update to source 1.0.1

* Thu Nov 19 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.69.2-1.1
- rebuild (for qt-4.6.0-rc1, f13+)

* Sun Nov 01 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.69.2-1
- Updated to source 0.69.2

* Mon Oct 12 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.69.0-1
- Updated to source version 0.69.0
- Made the QtCurve dir owned
- Removed unneeded scriptlets (no shlibs)
- Dropped unneeded BR
- Notified upstream about the borked windeco
- sed cleanup

* Sun Oct 04 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.68.1-3
- Added build comment
- Better use of name macro

* Mon Sep 28 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.68.1-2
- Changed to lowercase name for sanity

* Fri Sep 25 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.68.1-1
- Initial Release 0.68.1-1
