%global momorel 1

Summary:	A C++ port of Lucene
Name:		clucene
Version:	2.3.3.4
Release:	%{momorel}m%{?dist}
License:	"LGPLv2+ or ASL 2.0"
Group:		Development/System
URL:		http://www.sourceforge.net/projects/clucene
Source0:	http://dl.sourceforge.net/sourceforge/clucene/clucene-core-%{version}.tar.gz
NoSource:	0
## upstreamable patches
# include LUCENE_SYS_INCLUDES in pkgconfig --cflags output
# https://bugzilla.redhat.com/748196
# and
# https://sourceforge.net/tracker/?func=detail&aid=3461512&group_id=80013&atid=558446
# pkgconfig file is missing clucene-shared
Patch50:        clucene-core-2.3.3.4-pkgconfig.patch
# https://bugzilla.redhat.com/794795
# https://sourceforge.net/tracker/index.php?func=detail&aid=3392466&group_id=80013&atid=558446
# contribs-lib is not built and installed even with config
Patch51:        clucene-core-2.3.3.4-install_contribs_lib.patch  
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	automake gawk
Obsoletes:      %{name}-contrib
Obsoletes:      %{name}-contrib-devel

%description
CLucene is a C++ port of Lucene.
It is a high-performance, full-featured text search 
engine written in C++. CLucene is faster than lucene
as it is written in C++

%package        core
Summary:	Core clucene module
Group:		Development/System
Provides:	clucene
#Requires:       %{name} = %{version}-%{release}

%description core
The core clucene module

%package        core-devel
Summary:	Headers for developing programs that will use %{name}
Group:		Development/Libraries
Requires:	%{name}-core = %{version}-%{release}

%description core-devel
This package contains the static libraries and header files needed for
developing with clucene

%package	contribs-lib
Summary:	Core clucene module
Group:		Development/System
Requires:	%{name}-core >= %{version}-%{release}

%description contribs-lib
Collection of contributions for C++ port of Lucene

%prep
%setup -q -n %{name}-core-%{version}
%patch50 -p1 -b .pkgconfig
%patch51 -p1 -b .install_contribs_lib

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
  -DBUILD_CONTRIBS_LIB=BOOL:ON \
  -DLIB_DESTINATION:PATH=%{_libdir} \
  -DLUCENE_SYS_INCLUDES:PATH=%{_libdir} \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf --preserve-root %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

rm -rf %{buildroot}%{_libdir}/CLuceneConfig.cmake

%clean
rm -rf --preserve-root %{buildroot}

%post core -p /sbin/ldconfig

%postun core -p /sbin/ldconfig

%post contribs-lib -p /sbin/ldconfig

%postun contribs-lib -p /sbin/ldconfig

%files core
%defattr(-, root, root, -)
%doc APACHE.license AUTHORS ChangeLog COPYING LGPL.license README
%{_libdir}/libclucene-core.so.1*
%{_libdir}/libclucene-core.so.%{version}
%{_libdir}/libclucene-shared.so.1*
%{_libdir}/libclucene-shared.so.%{version}

%files contribs-lib
%defattr(-, root, root, -)
%{_libdir}/libclucene-contribs-lib.so.1*
%{_libdir}/libclucene-contribs-lib.so.%{version}

%files core-devel
%defattr(-, root, root, -)
%dir %{_libdir}/CLucene
%{_includedir}/CLucene
%{_includedir}/CLucene.h
%{_libdir}/libclucene*.so
%{_libdir}/CLucene/clucene-config.h
%{_libdir}/CLucene/CLuceneConfig.cmake
%{_libdir}/pkgconfig/libclucene-core.pc

%changelog
* Sun Sep 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3.4-1m)
- update to 2.3.3.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.21b-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.21b-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.21b-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.21b-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.21b-1m)
- update to 0.9.21b

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.20-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.20-2m)
- rebuild against gcc43

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.20-1m)
- import from Fedora devel

* Wed Oct 25 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.20-3
- Fix a typo in the License field

* Wed Oct 25 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.20-2
- Fix multiarch conflicts (BZ #340891)
- Bypass 'make check' for ppc64, its failing two tests there

* Tue Aug 21 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.20-1
- Update to version 0.9.20

* Sat Aug 11 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.19-1
- Latest release update

* Fri Aug 03 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.16a-2
- License tag update

* Thu Feb 22 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.16a-2
- Add -contrib subpackage 

* Thu Dec 07 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.16a-1
- Update to latest stable release 
- Run make check during build

* Mon Nov 20 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.15-3
- Don't package APACHE.license since we've LGPL instead 
- Package documentation in devel subpackage

* Mon Nov 13 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.15-2
- Fix a bunch of issues with the spec (#215258)
- Moved the header file away from lib dir

* Sat Nov 04 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.15-1
- Initial packaging for Fedora Extras
