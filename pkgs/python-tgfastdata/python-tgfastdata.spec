%global momorel 10

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define module tgfastdata

Name:           python-tgfastdata
Version:        0.9a6
Release:        %{momorel}m%{?dist}
Summary:        Automatic user interface generation for TurboGears

Group:          Development/Languages
License:        MIT/X
URL:            http://www.turbogears.org/docs/plugins/template.html
Source0:        http://files.turbogears.org/eggs/TGFastData-%{version}-py2.4.egg
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel >= 2.7


%description
FastData is an extension to TurboGears which can provide automatic user
interface generation based upon an application's model objects.


%prep
%setup -q -c


%build


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{python_sitelib}/%{module}{,/templates}
install -p -m 0644 %{module}/*.py %{buildroot}%{python_sitelib}/%{module}
install -p -m 0644 %{module}/templates/* %{buildroot}%{python_sitelib}/%{module}/templates

# Install the egg-info directory
mkdir -p %{buildroot}%{python_sitelib}/TGFastData-%{version}-py%{pyver}.egg-info
install -p -m 0644 EGG-INFO/* %{buildroot}%{python_sitelib}/TGFastData-%{version}-py%{pyver}.egg-info


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{python_sitelib}/%{module}/
%{python_sitelib}/TGFastData-%{version}-py%{pyver}.egg-info

%changelog
* Wed Jan 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9a6-10m)
- add source
- some download tools can not get source

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9a6-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9a6-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9a6-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9a6-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9a6-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9a6-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9a6-3m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9a6-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9a6-1m)
- import from Fedora

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> - 0.9a6-6
- Rebuild for new python

* Mon Oct 30 2006 Luke Macken <lmacken@redhat.com> - 0.9a6-5
- Fix egg-info directory

* Tue Oct 14 2006 Luke Macken <lmacken@redhat.com> - 0.9a6-4
- Fix Source0
- Own %%{python_sitelib}/%{module}
- Don't install unusable tests

* Tue Oct 10 2006 Luke Macken <lmacken@redhat.com> - 0.9a6-3
- Remove TurboGears requirement

* Sat Sep 30 2006 Luke Macken <lmacken@redhat.com> - 0.9a6-2
- Rename to python-tgfastdata
- Install egg-info

* Sun Sep 17 2006 Luke Macken <lmacken@redhat.com> - 0.9a6-1
- Initial creation
