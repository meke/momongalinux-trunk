%global momorel 8

Summary: German man pages from the Linux Documentation Project.
Name: man-pages-de
Version: 0.5
Release: %{momorel}m%{?dist}
License: see "COPYRIGHT"
Group: Documentation
URL: http://www.infodrom.org/projects/manpages-de/
Source: http://www.infodrom.org/projects/manpages-de/download/manpages-de-%{version}.tar.gz
Patch0: man-pages-de-0.3-nolocalfile.patch
Patch1: man-pages-de-0.4-ps_space.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
NoSource: 0

%description
Manual pages from the Linux Documentation Project, translated into
German.

%prep

%setup -q -n manpages-de-%{version}
%patch0 -p1
%patch1 -p1

%build
rm -f man1/hostname.*

%install
rm -fr %{buildroot}
mkdir -p %{buildroot}%{_mandir}/de
for n in 1 2 3 4 5 6 7 8 9 n; do
	mkdir %{buildroot}%{_mandir}/de/man$n
done
for i in man*/*; do
    iconv -f ISO-8859-1 -t UTF-8 < $i > $i.new
    mv -f $i.new $i
done
LC_ALL=de_DE make prefix=%{buildroot}%{_prefix} \
    MANDIR=%{buildroot}%{_mandir}/de install

# conflicts with man
rm -f %{buildroot}%{_mandir}/de/man1/apropos.1
rm -f %{buildroot}%{_mandir}/de/man1/man.1
rm -f %{buildroot}%{_mandir}/de/man1/whatis.1
rm -f %{buildroot}%{_mandir}/de/man5/man.config.5

# conflicts with shadow-utils
rm -f %{buildroot}%{_mandir}/de/man1/newgrp.1

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES COPYRIGHT README
%{_mandir}/de/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-3m)
- rebuild against gcc43

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- remove duplicated line from %%install
- remove %%{_mandir}/de from %%files, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5-1m)
- sync FC7
- update to 0.5

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-4m)
- modify %%install to avoid conflicting with man

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-3m)
- modify %%install to avoid conflicting with shadow-utils

* Wed Nov 12 2003 zunda <zunda at freeshell.org>
- (0.4-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4-1m)

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- mada nigirisugi

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- nigirisugi

* Tue Aug 14 2001 Tim Powers <timp@redhat.com>
- rebuilt to hopefully fix rpm verify problem

* Mon Aug 13 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Rebuild (should fix #51677)

* Thu Aug  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Own /usr/share/man/de

* Thu May 31 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.3
- Added URL, changed location
- Remove --local-file option from the German man(1) (#39211)

* Sun Apr  8 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Remove hostname.1, it's now part of net-tools

* Tue Apr  3 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Some fixes to the roff sources (#34183)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Mon Jun 19 2000 Matt Wilson <msw@redhat.com>
- defattr root

* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_mandir} and %%{_tmppath} 

* Mon May 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
