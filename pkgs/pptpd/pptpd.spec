%global momorel 1

%define buildlibwrap 1
%define buildbsdppp 0
%define buildslirp 0
%define buildipalloc 0
%define buildbcrelay 1
%define buildpnsmode 0

Summary: PoPToP Point to Point Tunneling Server
Name: pptpd
Requires: ppp >= 2.4.3
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Daemons
URL: http://poptop.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/poptop/%{name}-%{version}.tar.gz
NoSource: 0
Source1: pptpd.service
Source2: pptpd.sysconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
This implements a Virtual Private Networking Server (VPN) that is
compatible with Microsoft VPN clients. It allows windows users to
connect to an internal firewalled network using their dialup.

# allow --with[out] <feature> at rpm command line build
# e.g. --with ipalloc --without libwrap
# --with overrides --without
%{?_without_libwrap: %{expand: %%define buildlibwrap 0}}
%{?_without_bsdppp: %{expand: %%define buildbsdppp 0}}
%{?_without_slirp: %{expand: %%define buildslirp 0}}
%{?_without_ipalloc: %{expand: %%define buildipalloc 0}}
%{?_without_bcrelay: %{expand: %%define buildbcrelay 0}}
%{?_without_pnsmode: %{expand: %%define buildpnsmode 0}}
%{?_with_libwrap: %{expand: %%define buildlibwrap 1}}
%{?_with_bsdppp: %{expand: %%define buildbsdppp 1}}
%{?_with_slirp: %{expand: %%define buildslirp 1}}
%{?_with_ipalloc: %{expand: %%define buildipalloc 1}}
%{?_with_bcrelay: %{expand: %%define buildbcrelay 1}}
%{?_with_pnsmode: %{expand: %%define buildpnsmode 1}}

%prep
%setup -q

%build
buildopts=""
%if %{buildlibwrap}
buildopts="$buildopts --with-libwrap"
%endif
%if %{buildbsdppp}
buildopts="$buildopts --with-bsdppp"
%endif
%if %{buildslirp}
buildopts="$buildopts --with-slirp"
%endif
%if %{buildipalloc}
buildopts="$buildopts --with-pppd-ip-alloc"
%endif
%if %{buildbcrelay}
buildopts="$buildopts --with-bcrelay"
%endif
%if %{buildpnsmode}
buildopts="$buildopts --with-pns-mode"
%endif

autoreconf -fi
%configure $buildopts
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/ppp
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_bindir}

make %{?_smp_mflags} \
	DESTDIR=%{buildroot} \
	INSTALL="install -p" \
	LIBDIR=%{buildroot}%{_libdir}/pptpd \
	install
install -pm 0644 samples/pptpd.conf %{buildroot}%{_sysconfdir}/pptpd.conf
install -pm 0644 samples/options.pptpd %{buildroot}%{_sysconfdir}/ppp/options.pptpd
install -pm 0755 tools/vpnuser %{buildroot}%{_bindir}/vpnuser
install -pm 0755 tools/vpnstats.pl %{buildroot}%{_bindir}/vpnstats.pl
install -pm 0755 tools/pptp-portslave %{buildroot}%{_sbindir}/pptp-portslave
install -pm 0644 pptpd.conf.5 %{buildroot}%{_mandir}/man5/pptpd.conf.5
install -pm 0644 pptpd.8 %{buildroot}%{_mandir}/man8/pptpd.8
install -pm 0644 pptpctrl.8 %{buildroot}%{_mandir}/man8/pptpctrl.8
install -pm 0644 %{SOURCE1} %{buildroot}%{_unitdir}
install -pm 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/pptpd

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%doc AUTHORS COPYING README* TODO ChangeLog* samples
%{_sbindir}/pptpd
%{_sbindir}/pptpctrl
%{_sbindir}/pptp-portslave
%{!?_without_bcrelay:%{_sbindir}/bcrelay}
%dir %{_libdir}/pptpd
%{_libdir}/pptpd/pptpd-logwtmp.so
%{_bindir}/vpnuser
%{_bindir}/vpnstats.pl
%{_mandir}/man5/pptpd.conf.5*
%{_mandir}/man8/*.8*
%{_unitdir}/pptpd.service
%config(noreplace) %{_sysconfdir}/sysconfig/pptpd
%config(noreplace) %{_sysconfdir}/pptpd.conf
%config(noreplace) %{_sysconfdir}/ppp/options.pptpd

%changelog
* Fri Mar 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-8m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-4m)
- rebuild against gcc43

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-3m)
- fix %%changelog section

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-2m)
- PreReq: chkconfig

* Mon Jun  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.4-1m)
- [SECURITY] CVE-2007-0244
- update up 1.3.4

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.2-2m)
- move man files from /usr/man to %%{_mandir}

* Sun Aug 13 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.2-1m)
- version up 1.3.2

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-6m)
- add gcc4 patch
- Patch1: pptpd-1.1.4-gcc4.patch

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.4-5m)
- stop daemon

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-4m)
- s/Copyright:/License:/

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-3m)
- kill %%define name

* Mon Sep 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.4-2m)
- revise options.pptpd for ppp-2.4.2

* Wed May 14 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4-b4 for security fix.

* Fri Oct 25 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3 CVS 20021009
- merge specs from Richard de Vroede <richard@linvision.com>

* Wed Apr 03 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.0.1-6k)
- chap-secrets nigirisugi

* Mon Mar 25 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.0.1-4k)
- rewrite initscript

* Mon Jan 09 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.0.1-2k)
- Kondarized.

* Mon Oct 9 2000 Andy Worthington <christopherandrew@ou.edu>
- changed url and source to new website
- added man pages to files
- removed startup via inittab
- added startup via init (%{_sysconfdir}/rc.d/init.d/)
- updated to 1.0.1

* Tue Nov 23 1999 Tim Powers <timp@redhat.com>
- added "%config /etc/pptpd.conf" back to spec
- updated to 1.0.0

* Mon Aug 30 1999 Tim Powers <timp@redhat.com>
- changed group

* Wed Aug 4 1999 Tim Powers <timp@redhat.com>
- started changelog
- quiet setup
- changed prefix to be /usr instead of /usr/local
- added buildroot
- fixed %%install section so that it uses buildroot
- updated source to 0.9.10
- built to be included into Powertools
- spec based heavily on spec by "Allan's Package-O-Matic Blenderfier"
