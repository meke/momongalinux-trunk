%global momorel 12
%{expand: %%define py_ver %(python -c 'import sys;print(sys.version[0:3])')}
##%global py_ver 2.5

Summary: Numerical Extension to Python
Name: python-numeric
Version: 24.2
Release: %{momorel}m%{?dist}
URL: http://numpy.sourceforge.net/
Source: http://dl.sourceforge.net/sourceforge/numpy/Numeric-%{version}.tar.gz
NoSource: 0
License: "Python"
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7
BuildRequires: python-setuptools-devel >= 0.6c9-2m
Patch0: Numeric-24.2-fix-dlamc3.patch

%description
Numeric is a python module that provides support for numerical
operations.
 
%prep
%setup -q -n Numeric-%{version}
%patch0 -p1 -b .dlamc3

%build
env CFLAGS="$RPM_OPT_FLAGS" /usr/bin/python setup.py build

%install
rm -rf --preserve-root %{buildroot}
/usr/bin/python setup.py install --root=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc Demo/
%{_libdir}/python*/site-packages/Numeric.pth
%{_libdir}/python*/site-packages/Numeric/
%{_includedir}/python*/Numeric/

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (24.2-12m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.2-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (24.2-9m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.2-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (24.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (24.2-6m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (24.2-5m)
- rebuild against python-2.6.1

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (24.2-4m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (24.2-3m)
- rebuild against python-setuptools-0.6c9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (24.2-2m)
- rebuild against gcc43

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (23.7-2m)
- rebuild against python-2.5

* Mon Apr 10 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (23.7-1m)
- first import from Fedora Core 5

* Mon Apr 10 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (23.7-1m)
- first import from Fedora Core 5

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 23.7-2.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 23.7-2.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Feb 23 2005 Jonathan Blandford <jrb@redhat.com> 23.7-2
- try a new version

* Wed Feb 23 2005 Jonathan Blandford <jrb@redhat.com> 23.7-1
- rename Numeric to python-numeric

* Thu Feb 17 2005  <jrb@redhat.com> - 
- Initial build.

