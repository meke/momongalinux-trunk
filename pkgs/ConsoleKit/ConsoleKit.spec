%global momorel 3

%global glib2_version           2.6.0
%global dbus_version            0.90
%global dbus_glib_version       0.70
%global polkit_version          0.94

Summary: System daemon for tracking users, sessions and seats
Name: ConsoleKit
Version: 0.4.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.freedesktop.org/wiki/Software/ConsoleKit
Source0: http://www.freedesktop.org/software/%{name}/dist/%{name}-%{version}.tar.bz2
NoSource: 0
# convert to new upstart syntax
Patch0: %{name}-0.4.1-upstart06.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: dbus >= %{dbus_version}
Requires: dbus-glib >= %{dbus_glib_version}
Conflicts: upstart < 0.6.0
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: dbus-devel  >= %{dbus_version}
BuildRequires: dbus-glib-devel >= %{dbus_glib_version}
BuildRequires: polkit-devel >= %{polkit_version}
BuildRequires: pam-devel
BuildRequires: libX11-devel
BuildRequires: xmlto
BuildRequires: pkgconfig
BuildRequires: zlib-devel
BuildRequires: systemd

%description 
ConsoleKit is a system daemon for tracking what users are logged
into the system and how they interact with the computer (e.g.
which keyboard and mouse they use).

It provides asynchronous notification via the system message bus.

%package x11
Summary: X11-requiring add-ons for ConsoleKit
License: GPLv2+
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libX11

%description x11 
ConsoleKit contains some tools that require Xlib to be installed,
those are in this separate package so server systems need not install
X. Applications (such as xorg-x11-xinit) and login managers (such as
gdm) that need to register their X sessions with ConsoleKit needs to
have a Requires: for this package.

%package libs
Summary: ConsoleKit libraries
License: MIT
Group: Development/Libraries
Requires: pam
Requires: dbus >= %{dbus_version}

%description libs
This package contains libraries and a PAM module for interacting 
with ConsoleKit.

%package devel
Summary: Development libraries and headers for ConsoleKit
License: MIT
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: dbus-devel >= %{dbus_version}
Requires: pkgconfig

%description devel
This package contains headers and libraries needed for
developing software that is interacting with ConsoleKit.

%package docs
Summary: Developer documentation for ConsoleKit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description docs
This package contains developer documentation for ConsoleKit.

%prep
%setup -q

%patch0 -p1 -b .upstart06

%build
%configure \
	--with-pid-file=%{_localstatedir}/run/console-kit-daemon.pid \
	--enable-pam-module \
	--with-pam-module-dir=/%{_lib}/security \
	--enable-docbook-docs \
	--docdir=%{_docdir}/%{name}-%{version}

make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}/%{_lib}/security/*.a
rm -f %{buildroot}/%{_lib}/security/*.la

# make sure we don't package a history log
rm -f %{buildroot}/%{_var}/log/ConsoleKit/history

# the sample upstart files are good enough for us.
mkdir -p %{buildroot}%{_sysconfdir}/init
install -m 644 data/ck-log-system-{start,stop,restart}.conf %{buildroot}%{_sysconfdir}/init/

# for doc
install -m 644 AUTHORS COPYING ChangeLog HACKING NEWS README TODO %{buildroot}%{_docdir}/%{name}-%{version}

%clean
rm -rf --preserve-root %{buildroot}

%post
if [ -f /var/log/ConsoleKit/history ]; then
   chmod a+r /var/log/ConsoleKit/history
fi
if [ $1 -eq 1 ]; then
        /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]; then
        /bin/systemctl stop console-kit-daemon.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc %dir %{_datadir}/doc/%{name}-%{version}
%doc %{_datadir}/doc/%{name}-%{version}/README
%doc %{_datadir}/doc/%{name}-%{version}/ChangeLog
%doc %{_datadir}/doc/%{name}-%{version}/AUTHORS
%doc %{_datadir}/doc/%{name}-%{version}/HACKING
%doc %{_datadir}/doc/%{name}-%{version}/NEWS
%doc %{_datadir}/doc/%{name}-%{version}/COPYING
%doc %{_datadir}/doc/%{name}-%{version}/TODO
%{_sysconfdir}/dbus-1/system.d/*
%config(noreplace) %{_sysconfdir}/init/*
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/polkit-1/actions/*.policy
%dir %{_sysconfdir}/ConsoleKit
%dir %{_sysconfdir}/ConsoleKit/seats.d
%dir %{_sysconfdir}/ConsoleKit/run-seat.d
%dir %{_sysconfdir}/ConsoleKit/run-session.d
%dir %{_prefix}/lib/ConsoleKit
%dir %{_prefix}/lib/ConsoleKit/scripts
%dir %{_prefix}/lib/ConsoleKit/run-seat.d
%dir %{_prefix}/lib/ConsoleKit/run-session.d
%ghost %dir %{_var}/run/ConsoleKit
%attr(755,root,root) %dir %{_var}/log/ConsoleKit
%config %{_sysconfdir}/ConsoleKit/seats.d/00-primary.seat
%{_sbindir}/console-kit-daemon
%{_sbindir}/ck-log-system-restart
%{_sbindir}/ck-log-system-start
%{_sbindir}/ck-log-system-stop
%{_bindir}/ck-history
%{_bindir}/ck-launch-session
%{_bindir}/ck-list-sessions
%{_prefix}/lib/ConsoleKit/scripts/*
/lib/systemd/system/console-kit-daemon.service
/lib/systemd/system/console-kit-log-system-start.service
/lib/systemd/system/console-kit-log-system-stop.service
/lib/systemd/system/console-kit-log-system-restart.service
/lib/systemd/system/basic.target.wants/console-kit-log-system-start.service
/lib/systemd/system/halt.target.wants/console-kit-log-system-stop.service
/lib/systemd/system/poweroff.target.wants/console-kit-log-system-stop.service
/lib/systemd/system/reboot.target.wants/console-kit-log-system-restart.service
/lib/systemd/system/kexec.target.wants/console-kit-log-system-restart.service

%files x11
%defattr(-,root,root,-)
%{_libexecdir}/ck-collect-session-info
%{_libexecdir}/ck-get-x11-display-device
%{_libexecdir}/ck-get-x11-server-pid

%files libs
%defattr(-,root,root,-)
/%{_lib}/security/pam_ck_connector.so
%{_libdir}/libck-connector.so.*
%{_mandir}/man8/pam_ck_connector.8*

%files devel
%defattr(-,root,root,-)
%{_includedir}/ConsoleKit
%{_libdir}/pkgconfig/ck-connector.pc
%{_libdir}/libck-connector.so
%{_datadir}/dbus-1/interfaces/org.freedesktop.ConsoleKit.*.xml

%files docs
%defattr(-,root,root,-)
%doc %{_datadir}/doc/%{name}-%{version}/spec

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-3m)
- rebuild for glib 2.33.2

* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-2m)
- delete conflict dir

* Fri Jul  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.5-1m)
- update 0.4.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-6m)
- full rebuild for mo7 release

* Wed Mar 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-5m)
- import reorder-initialization.patch from ConsoleKit-0.4.1-3.fc12
 +* Fri Dec 18 2009 Ray Strode <rstrode@redhat.com> 0.4.1-3
 +- Register object methods with dbus-glib before taking bus name
    (may fix 545267)
- https://bugzilla.redhat.com/show_bug.cgi?id=545267
- ConsoleKit is working fully with hal-0.5.14 now
- and sound is working fine
- please remove users from audio group, let's reboot!

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-4m)
- merge Fedora's changes
 +* Tue Dec 15 2009 Matthias Clasen <mclasen@redhat.com> 0.4.1-3
 +- Don't daemonize when activated
 +* Wed Dec  9 2009 Bill Nottingham <notting@redhat.com> 0.4.1-2
 +- Adjust for upstart 0.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-2m)
- own few directories
- remove la files

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
-- comment out all patch (0-3)

* Fri May  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-2m)
- import null-warning.patch from Fedora
 +* Tue Apr 21 2009 Matthias Clasen  <mclasen@redhat.com> - 0.3.0-8
 +- Fix a warning on login (#496636)
- update dbus-permissions.patch from Fedora
 +* Wed Apr  8 2009 Matthias Clasen  <mclasen@redhat.com> - 0.3.0-7
 +- Allow GetSessions calls in the dbus policy
- set BuildArch: noarch on package docs

* Sat Mar  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0
- import skipvalidation.patch from Fedora
 +* Fri Feb 27 2009 Matthias Clasen  <mclasen@redhat.com> - 0.3.0-6
 +- Fix the build
- import dbus-permissions.patch from Fedora
 +* Wed Jan 14 2009 Colin Walters <walters@verbum.org> - 0.3.0-4
 +- Add patch to fix up dbus permissions
- import get-vt-from-display-instead-of-controlling-tty.patch from Fedora
 +* Tue Sep 16 2008 Ray Strode  <rstrode@redhat.com> - 0.3.0-2
 +- Grab X server display device from XFree86_VT root window property,
 +  if X server doesn't have a controlling terminal.
- remove old patches

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.10-4m)
- rebuild against rpm-4.6

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.10-3m)
- import ck-exit-with-bus.patch and ck-fix-thread-shutdown.patch from Fedora
 +* Mon May  5 2008 Jon McCann  <jmccann@redhat.com> - 0.2.10-4
 +- Exit with system bus (#441571)
 +- Correctly shutdown event logger threads (#440349)

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.10-2m)
- use make instead of %%make

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.10-1m)
- version 0.2.10

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-2m)
- %%NoSource -> NoSource

* Fri Apr 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- initial package for Momonga Linux
- import and modify spec file from Fedora

* Mon Apr 02 2007 David Zeuthen <davidz@redhat.com> - 0.2.1-0.git20070402
- Update to git snapshot to get a lot of bug fixes
- Use libX11 rather than gtk2 to verify X11 sessions; update BR and R
- Split X11-using bits into a new subpackage ConsoleKit-x11 (#233982)
- Use correct location for PAM module on 64-bit (#234545)
- Build developer documentation and put them in ConsoleKit-docs

* Mon Mar 19 2007 David Zeuthen <davidz@redhat.com> - 0.2.0-2
- BR gtk2-devel and make ConsoleKit Require gtk2 (could just be
  libX11 with a simple patch)

* Mon Mar 19 2007 David Zeuthen <davidz@redhat.com> - 0.2.0-1
- Update to upstream release 0.2.0
- Daemonize properly (#229206)

* Sat Mar  3 2007 David Zeuthen <davidz@redhat.com> - 0.1.3-0.git20070301.1
- Allow caller to pass uid=0 in libck-connector

* Thu Mar  1 2007 David Zeuthen <davidz@redhat.com> - 0.1.3-0.git20070301
- Update to git snapshot
- Drop all patches as they are committed upstream
- New tool ck-list-sessions
- New -libs subpackage with run-time libraries and a PAM module
- New -devel subpackage with headers

* Tue Feb  6 2007 David Zeuthen <davidz@redhat.com> - 0.1.0-5%{?dist}
- Start ConsoleKit a bit earlier so it starts before HAL (98 -> 90)
- Minimize stack usage so VIRT size is more reasonable (mclasen)
- Make session inactive when switching to non-session (davidz)

* Fri Jan 12 2007 Matthias Clasen <mclasen@redhat.com> - 0.1.0-4
- Don't mark initscripts %%config
- Use proper lock and pid ile names

* Fri Jan 12 2007 Matthias Clasen <mclasen@redhat.com> - 0.1.0-3
- More package review feedback

* Fri Jan 12 2007 Matthias Clasen <mclasen@redhat.com> - 0.1.0-2
- Incorporate package review feedback

* Thu Jan 11 2007 Matthias Clasen <mclasen@redhat.com> - 0.1.0-1
- Update to the first public release 0.1.0
- Some spec cleanups

* Mon Oct 23 2006 David Zeuthen <davidz@redhat.com> - 0.0.3-1
- Initial build.

