%global momorel 6

Name: zaf
Summary: South Africa hyphenation rules
%define upstreamid 20071123
Version: 0
Release: 0.1.%{upstreamid}svn.%{momorel}m%{?dist}
Source: zaf-0-0.1.%{upstreamid}svn.tar.bz2
Group: Applications/Text
URL: http://zaf.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+
BuildArch: noarch

%description
South Africa hyphenation rules.

%package -n hyphen-af
Summary: Afrikaans hyphenation rules
Group: Applications/Text
Requires: hyphen

%description -n hyphen-af
Afrikaans hyphenation rules.

%package -n hyphen-zu
Summary: Zulu hyphenation rules
Group: Applications/Text
Requires: hyphen

%description -n hyphen-zu
Zulu hyphenation rules.

%prep
%setup -q -n zaf

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p ./af/hyph/hyph_af_ZA.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p ./zu/hyph/hyph_zu_ZA.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen

pushd $RPM_BUILD_ROOT/%{_datadir}/hyphen/
af_ZA_aliases="af_NA"
for lang in $af_ZA_aliases; do
        ln -s hyph_af_ZA.dic hyph_$lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files -n hyphen-af
%defattr(-,root,root,-)
%doc af/CREDITS af/COPYING af/README
%{_datadir}/hyphen/hyph_af*

%files -n hyphen-zu
%defattr(-,root,root,-)
%doc zu/CREDITS zu/COPYING zu/README
%{_datadir}/hyphen/hyph_zu*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.1.20071123svn.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.1.20071123svn.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.1.20071123svn.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.1.20071123svn.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.1.20071123svn.2m)
- rebuild against rpm-4.6

* Fri May 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0-0.1.20071123svn.1m)

* Fri Nov 23 2007 Caolan McNamara <caolanm@redhat.com> - 0-0.1.20071123svn
- initial version
