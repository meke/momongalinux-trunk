%global momorel 1
%global prever 5

%define rescue %{nil}

Name: elinks
Summary: A text-mode Web browser
Version: 0.12
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPLv2
Source0: http://elinks.or.cz/download/elinks-%{version}pre%{prever}.tar.bz2
NoSource: 0
Source2: elinks.conf
Group: Applications/Internet
URL: http://elinks.or.cz/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: automake
BuildRequires: bzip2-devel
BuildRequires: expat-devel
BuildRequires: js-devel >= 1.8.5
BuildRequires: libidn-devel
BuildRequires: krb5-devel
BuildRequires: nss_compat_ossl-devel >= 0.9.3
BuildRequires: nss-devel
BuildRequires: pkgconfig
BuildRequires: tre-devel >= 0.8.0
Requires: zlib >= 1.2.0.2
Requires(preun): chkconfig
Requires(post): chkconfig
Requires(postun): chkconfig
Provides: webclient
Obsoletes: links < 2.4
Provides: links = 2.4

Patch0: elinks-0.11.0-ssl-noegd.patch
Patch1: elinks-0.10.1-utf_8_io-default.patch
Patch3: elinks-0.11.0-getaddrinfo.patch
Patch4: elinks-0.11.0-sysname.patch
Patch5: elinks-0.10.1-xterm.patch
Patch6: elinks-0.11.0-union.patch
Patch7: elinks-0.11.3-macropen.patch
Patch8: elinks-scroll.patch
Patch9: elinks-nss.patch
Patch10: elinks-nss-inc.patch
Patch11: elinks-0.12pre5-js185.patch

%description
Elinks is a text-based Web browser. Elinks does not display any images,
but it does support frames, tables and most other HTML tags. Elinks'
advantage over graphical browsers is its speed--Elinks starts and exits
quickly and swiftly displays Web pages.

%prep
%setup -q -n %{name}-%{version}pre%{prever}

# Prevent crash when HOME is unset (bug #90663).
%patch0 -p1
# UTF-8 by default
%patch1 -p1
# Make getaddrinfo call use AI_ADDRCONFIG.
%patch3 -p1
# Don't put so much information in the user-agent header string (bug #97273).
%patch4 -p1
# Fix xterm terminal: "Linux" driver seems better than "VT100" (#128105)
%patch5 -p1
# Fix #157300 - Strange behavior on ppc64
%patch6 -p1
# fix for open macro in new glibc
%patch7 -p1
#upstream fix for out of screen dialogs
%patch8 -p1
# Port elinks to use NSS library for cryptography (#346861) - accepted upstream
%patch9 -p1
# Port elinks to use NSS library for cryptography (#346861) - incremental patch
%patch10 -p1
# backported upstream commits f31cf6f, 2844f8b, and 218a225
%patch11 -p1

# remove bogus serial numbers
sed -i 's/^# *serial [AM0-9]*$//' acinclude.m4 config/m4/*.m4

%build
# we need to recreate autotools files because of the NSS patch
aclocal -I config/m4
autoconf
autoheader

export CFLAGS="$RPM_OPT_FLAGS $(getconf LFS_CFLAGS) -D_GNU_SOURCE"
%configure %{?rescue:--without-gpm} --without-x --with-gssapi \
  --enable-bittorrent --with-nss_compat_ossl

MOPTS="V=1"
if tty >/dev/null 2>&1; then
    # turn on fancy colorized output only when we have a TTY device
    MOPTS=
fi
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
ln -s elinks $RPM_BUILD_ROOT%{_bindir}/links
ln -s elinks.1 $RPM_BUILD_ROOT%{_mandir}/man1/links.1
rm -f $RPM_BUILD_ROOT%{_datadir}/locale/locale.alias
%find_lang elinks

%clean
rm -rf $RPM_BUILD_ROOT

%postun
if [ "$1" -ge "1" ]; then
	links=`readlink %{_sysconfdir}/alternatives/links`
	if [ "$links" == "%{_bindir}/elinks" ]; then
		%{_sbindir}/alternatives --set links %{_bindir}/elinks
	fi
fi
exit 0

%post
#Set up alternatives files for links
%{_sbindir}/alternatives --install %{_bindir}/links links %{_bindir}/elinks 90 \
  --slave %{_mandir}/man1/links.1.gz links-man %{_mandir}/man1/elinks.1.gz
links=`readlink %{_sysconfdir}/alternatives/links`
if [ "$links" == "%{_bindir}/elinks" ]; then
	%{_sbindir}/alternatives --set links %{_bindir}/elinks
fi


%preun
if [ $1 = 0 ]; then
	%{_sbindir}/alternatives --remove links %{_bindir}/elinks
fi
exit 0

%files -f elinks.lang
%defattr(-,root,root)
%doc README SITES TODO COPYING
%{_bindir}/links
%{_bindir}/elinks
%{_mandir}/man1/links.1*
%{_mandir}/man1/elinks.1*
%{_mandir}/man5/*

%changelog
* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-0.5.1m)
- update to 0.12pre5
- use alternatives
- rebuild against js-1.8.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-0.3.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-0.3.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-0.3.4m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-0.3.3m)
- rebuild against tre-0.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-0.3.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-0.3.1m)
- update to 0.12pre3 based on Fedora 11 (0.12-0.13.pre3)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.4-4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.4-3m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.4-2m)
- update Patch2 for fuzz=0

* Wed Jul  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.4-1m)
- [SECURITY] CVE-2008-7224
- update to 0.11.4

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.3-2m)
- rebuild against gcc43

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.3-1m)
- [SECURITY] CVE-2007-2027 CVE-2007-5034
- update to 0.11.3 (sync with FC-devel)
- remove unused patch

* Mon Apr  9 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.11.1-1m)
- import from fc

* Tue Nov 21 2006 Karel Zak <kzak@redhat.com> 0.11.1-5.1
- fix #215734: CVE-2006-5925 elinks smb protocol arbitrary file access

* Wed Oct 11 2006 Karel Zak <kzak@redhat.com> 0.11.1-5
- fix #210103 - elinks crashes when given bad HTTP_PROXY

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.11.1-4.1
- rebuild

* Mon Jun 12 2006 Karel Zak <kzak@redhat.com> 0.11.1-4
- improved negotiate-auth patch (faster now)

* Fri Jun  9 2006 Karel Zak <kzak@redhat.com> 0.11.1-3
- added negotiate-auth (GSSAPI) support -- EXPERIMENTAL!

* Mon May 29 2006 Karel Zak <kzak@redhat.com> 0.11.1-2
- update to new upstream version

* Wed May 17 2006 Karsten Hopp <karsten@redhat.de> 0.11.0-3
- add buildrequires bzip2-devel, expat-devel,libidn-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.11.0-2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.11.0-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 10 2006 Karel Zak <kzak@redhat.com> 0.11.0-2
- use upstream version of srcdir.patch

* Tue Jan 10 2006 Karel Zak <kzak@redhat.com> 0.11.0-1
- update to new upstream version
- fix 0.11.0 build system (srcdir.patch)
- regenerate patches:
     elinks-0.11.0-getaddrinfo.patch, 
     elinks-0.11.0-ssl-noegd.patch,
     elinks-0.11.0-sysname.patch,
     elinks-0.11.0-union.patch

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> 0.10.6-2.1
- rebuilt

* Wed Nov  9 2005 Karel Zak <kzak@redhat.com> 0.10.6-2
- rebuild (against new openssl)

* Thu Sep 29 2005 Karel Zak <kzak@redhat.com> 0.10.6-1
- update to new upstream version

* Tue May 17 2005 Karel Zak <kzak@redhat.com> 0.10.3-3
- fix #157300 - Strange behavior on ppc64 (patch by Miloslav Trmac)

* Tue May 10 2005 Miloslav Trmac <mitr@redhat.com> - 0.10.3-2
- Fix checking for numeric command prefix (#152953, patch by Jonas Fonseca)
- Fix invalid C causing assertion errors on ppc and ia64 (#156647)

* Mon Mar 21 2005 Karel Zak <kzak@redhat.com> 0.10.3-1
- sync with upstream; stable 0.10.3

* Sat Mar  5 2005 Karel Zak <kzak@redhat.com> 0.10.2-2
- rebuilt

* Tue Feb  8 2005 Karel Zak <kzak@redhat.com> 0.10.2-1
- sync with upstream; stable 0.10.2

* Fri Jan 28 2005 Karel Zak <kzak@redhat.com> 0.10.1-1
- sync with upstream; stable 0.10.1

* Thu Oct 14 2004 Karel Zak <kzak@redhat.com> 0.9.2-2
- the "Linux" driver seems better than "VT100" for xterm (#128105)

* Wed Oct  6 2004 Karel Zak <kzak@redhat.com> 0.9.2-1
- upload new upstream tarball with stable 0.9.2 release

* Mon Sep 20 2004 Jindrich Novy <jnovy@redhat.com> 0.9.2-0.rc7.4
- 0.9.2rc7.
- changed summary in spec to get rid of #41732, #61499

* Mon Sep 13 2004 Tim Waugh <twaugh@redhat.com> 0.9.2-0.rc4.3
- Avoid symbol clash (bug #131170).

* Fri Aug  6 2004 Tim Waugh <twaugh@redhat.com> 0.9.2-0.rc4.2
- 0.9.2rc4.

* Mon Jul 12 2004 Tim Waugh <twaugh@redhat.com> 0.9.2-0.rc2.2
- Fix elinks -dump -stdin (bug #127624).

* Thu Jul  1 2004 Tim Waugh <twaugh@redhat.com> 0.9.2-0.rc2.1
- 0.9.2rc2.

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun  2 2004 Tim Waugh <twaugh@redhat.com> 0.9.1-3
- Build with LFS support (bug #125064).

* Fri May 28 2004 Tim Waugh <twaugh@redhat.com> 0.9.1-2
- Use UTF-8 by default (bug #76445).

* Thu Mar 11 2004 Tim Waugh <twaugh@redhat.com> 0.9.1-1
- 0.9.1.
- Use %%find_lang.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Dec  8 2003 Tim Waugh <twaugh@redhat.com> 0.4.3-1
- 0.4.3.
- Updated pkgconfig patch.

* Mon Aug 11 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-7.1
- Rebuilt.

* Mon Aug 11 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-7
- Don't require XFree86-libs (bug #102072).

* Tue Jul 22 2003 Nalin Dahyabhai <nalin@redhat.com> 0.4.2-6.2
- rebuild

* Thu Jun 12 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-6.1
- Rebuilt.

* Thu Jun 12 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-6
- Make getaddrinfo call use AI_ADDRCONFIG.
- Don't put so much information in the user-agent header string (bug #97273).

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun  2 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-4.1
- Rebuild again.

* Mon Jun  2 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-4
- Rebuild.

* Mon May 12 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-3
- Prevent crash when HOME is unset (bug #90663).

* Sun May 04 2003 Florian La Roche <Florian.LaRoche@redhat.de> 0.4.2-2
- use relative symlinks to elinks

* Wed Feb  5 2003 Tim Waugh <twaugh@redhat.com> 0.4.2-1
- 0.4.2 (bug #83273).

* Wed Jan 22 2003 Tim Powers <timp@redhat.com> 0.3.2-5
- rebuilt

* Thu Jan 16 2003 Tim Waugh <twaugh@redhat.com>
- Fix URL (bug #81987).

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 0.3.2-4
- rebuild

* Mon Dec 23 2002 Tim Waugh <twaugh@redhat.com> 0.3.2-3
- Fix bug #62368.

* Thu Dec 12 2002 Nalin Dahyabhai <nalin@redhat.com>
- use openssl's pkg-config data, if available

* Wed Nov 20 2002 Tim Powers <timp@redhat.com> 0.3.2-2
- rebuild on all arches

* Tue Aug 20 2002 Jakub Jelinek <jakub@redhat.com> 0.3.2-1
- update to 0.3.2 to fix the DNS Ctrl-C segfaults
- update URLs, the project moved
- include man page

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Jan 24 2002 Tim Powers <timp@redhat.com>
- rebuilt against new openssl

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jan  2 2002 Preston Brown <pbrown@redhat.com> 0.96-4
- cookie fix

* Thu Sep 13 2001 Bernhard Rosenkraenzer <bero@redhat.com> 0.96-3
- Save some more space in rescue mode

* Wed Jul 18 2001 Bernhard Rosenkraenzer <bero@redhat.com> 0.96-2
- Add the links manual from links.sourceforge.net (RFE #49228)

* Tue Jul  3 2001 Bernhard Rosenkraenzer <bero@redhat.com> 0.96-1
- update to 0.96

* Fri Jun 22 2001 Nalin Dahyabhai <nalin@redhat.com>
- actually run make in build phase

* Tue Jun 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Fri Mar  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Tue Jan  9 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.95

* Mon Jan  1 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.94 final

* Sun Dec 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- pre9

* Mon Dec 10 2000 Preston Brown <pbrown@redhat.com>
- Upgraded to pre8.

* Tue Dec  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.94pre7
- Minor fixes to the specfile (s/Copyright:/License:/)
- merge rescue stuff

* Fri Nov 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.94pre5

* Wed Nov 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.94pre4

* Tue Oct 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.94pre1

* Wed Aug  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.92 (needed - prior versions won't display XHTML properly)

* Thu Jul 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment to work around bugs

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jun 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.84

* Sun Jun 11 2000 Preston Brown <pbrown@redhat.com>
- provides virtual package webclient.

* Thu Jan  6 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- initial RPM
