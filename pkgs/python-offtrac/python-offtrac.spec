%global momorel 5

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-offtrac
Version:        0.0.3
Release:        %{momorel}m%{?dist}
Summary:        Trac xmlrpc library

Group:          Development/Languages
License:        GPLv2+
URL:            http://fedorahosted.org/offtrac
# No tarballs are made, generate them from scm
# git clone git://git.fedorahosted.org/git/offtrac
# cd offtrac
# git checkout -b tarball %{version}
# python setup.py sdist --formats=bztar
# 
Source0:        offtrac-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7

%description
There is the offtrac python library which offers the TracServer class. This
object is how one interacts with a Trac instance via xmlrpc.


%prep
%setup -q -n offtrac-%{version}


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING LICENSE README
# For noarch packages: sitelib
%dir %{python_sitelib}/offtrac
%{python_sitelib}/offtrac/__init__.py*
%{python_sitelib}/offtrac-*.egg-info


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.3-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.3-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-1m)
- import from Fedora 13

* Wed Aug 12 2009 Jesse Keating <jkeating@redhat.com> - 0.0.3-1
- Declare classes in the correct way, to work on python-2.5

* Wed Aug 05 2009 Jesse Keating <jkeating@redhat.com> - 0.0.2-1
- Fix licensing issues

* Tue Aug 04 2009 Jesse Keating <jkeating@redhat.com> - 0.0.1-1
- Initial package

