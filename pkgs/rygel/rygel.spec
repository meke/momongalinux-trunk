%global		momorel 1
Name:          rygel
Version:       0.15.0.1
Release: %{momorel}m%{?dist}
Summary:       A collection of UPnP/DLNA services

Group:         Applications/Multimedia
License:       LGPLv2+
URL:           http://live.gnome.org/Rygel
Source0:       ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.15/%{name}-%{version}.tar.xz
NoSource:      0
BuildRequires: dbus-glib-devel
BuildRequires: desktop-file-utils
BuildRequires: gstreamer-devel
BuildRequires: gtk3-devel
BuildRequires: gupnp-devel
BuildRequires: gupnp-av-devel
BuildRequires: gupnp-dlna-devel
BuildRequires: libgee-devel
BuildRequires: libsoup-devel
BuildRequires: libuuid-devel
BuildRequires: pkgconfig(gee-1.0)
BuildRequires: sqlite-devel
BuildRequires: intltool

%description
Rygel is a home media solution that allows you to easily share audio, video and
pictures, and control of media player on your home network. In technical terms
it is both a UPnP AV MediaServer and MediaRenderer implemented through a plug-in
mechanism. Interoperability with other devices in the market is achieved by
conformance to very strict requirements of DLNA and on the fly conversion of
media to format that client devices are capable of handling.

%package devel
Summary: Development package for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Files for development with %{name}.

%package tracker
Summary: Tracker plugin for %{name}
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: tracker

%description tracker
A plugin for rygel to use tracker to locate media on the local machine.

%prep
%setup -q

%build
%configure --with-gtk=3.0 --enable-tracker-plugin --enable-media-export-plugin --enable-external-plugin \
  --enable-mediathek-plugin --enable-gst-launch-plugin --disable-silent-rules

make %{?_smp_mflags} V=1

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang %{name}

# Verify the desktop files
desktop-file-validate %{buildroot}/%{_datadir}/applications/rygel.desktop
desktop-file-validate %{buildroot}/%{_datadir}/applications/rygel-preferences.desktop

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README TODO NEWS doc/README.Mediathek 
%config(noreplace) %{_sysconfdir}/rygel.conf
%{_bindir}/rygel
%{_bindir}/rygel-preferences
%{_libdir}/rygel-1.0/librygel-external.so
%{_libdir}/rygel-1.0/librygel-media-export.so
%{_libdir}/rygel-1.0/librygel-gst-launch.so
%{_libdir}/rygel-1.0/librygel-mpris.so
%{_libdir}/rygel-1.0/librygel-playbin.so
%{_libdir}/rygel-1.0/librygel-mediathek.so
%{_datadir}/rygel/
%{_datadir}/applications/rygel*
%{_datadir}/dbus-1/services/org.gnome.Rygel1.service
%{_datadir}/icons/hicolor/*/apps/rygel*
%{_datadir}/man/man?/rygel*

%files tracker
%defattr(-,root,root,-)
%{_libdir}/rygel-1.0/librygel-tracker.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/rygel-1.0
%{_libdir}/pkgconfig/rygel-1.0.pc
%{_datadir}/vala/vapi/rygel-1.0.deps
%{_datadir}/vala/vapi/rygel-1.0.vapi

%changelog
* Fri Jun 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.0.1-2m)
- update to 0.15.0.1
- reimport from fedora

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.7-1m)
- update to 0.12.7

* Mon Oct 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.5-1m)
- update to 0.12.5

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.4-1m)
- update to 0.12.4

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.3-1m)
- update to 0.12.3

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-1m)
- update to 0.12.2

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Mon Jul 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.3-2m)
- add BuildRequires

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.1-1m)
- update to 0.10.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Wed Feb 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-2m)
- update %%files

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3

* Sun Oct 17 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.1-3m)
- add BR libgee-devel

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- add BuildRequires

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- initial build
