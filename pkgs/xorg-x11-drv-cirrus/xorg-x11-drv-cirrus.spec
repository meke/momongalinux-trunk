%global momorel 1
%define tarball xf86-video-cirrus
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 cirrus video driver
Name:      xorg-x11-drv-cirrus
Version:   1.5.2
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
Patch0:	   cirrus-1.2.0-qemu.patch
Patch1:	   cirrus-1.3.2-virt-16bpp.patch
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExcludeArch: s390 s390x

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13

Requires:  xorg-x11-server-Xorg >= 1.13

%description 
X.Org X11 cirrus video driver.

%prep
%setup -q -n %{tarball}-%{version}
%patch0 -p1 -b .qemu
%patch1 -p1 -b .16bit

%build
autoreconf -ivf
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

mkdir -p $RPM_BUILD_ROOT%{_datadir}/hwdata/videoaliases

# FIXME: Remove all libtool archives (*.la) from modules directory.  This
# should be fixed in upstream Makefile.am or whatever.
find %{buildroot} -regex ".*\.la$" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/cirrus_drv.so
%{driverdir}/cirrus_alpine.so
%{driverdir}/cirrus_laguna.so
%{_mandir}/man4/cirrus.4*

%changelog
* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1

* Tue May  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-2m)
- add cirrus-1.3.2-virt-16bpp.patch

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0
- delete patch1
- comment out patch2

* Tue Jan 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-10m)
- drop cirrus.xinf

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-9m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-8m)
- rebuild for xorg-x11-server-1.10.99

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-8m)
- add qemu patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-7m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-6m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-2m)
- rebuild against xorg-x11-server-1.7.1

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update to 1.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.1-2m)
- works on x86_64

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-4m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- rebuild against xorg-x11-server-1.4

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0.5-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.5-1.1m)
- import to Momonga

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0.5-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.0.5-1
- Updated xorg-x11-drv-cirrus to version 1.0.0.5 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.4-1
- Updated xorg-x11-drv-cirrus to version 1.0.0.4 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.
- Added cirrus_alpine.so, cirrus_laguna.so to the file manifest.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.2-1
- Updated xorg-x11-drv-cirrus to version 1.0.0.2 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-cirrus to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Set "ExclusiveArch: %{ix86}"

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for cirrus video driver generated automatically
  by my xorg-driverspecgen script.
