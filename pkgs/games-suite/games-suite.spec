%global momorel 1

Summary:     Meta package for Games and Entertainment
Name:        games-suite
Version:     8.0
Release:     %{momorel}m%{?dist}
License:     GPL and LGPL and MPL and Modified BSD
Group:       Applications/Multimedia
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:    BZFlag
Requires:    Maelstrom
Requires:    armagetronad
Requires:    csmash
Requires:    freeciv
Requires:    netmaj
Requires:    supertux
Requires:    supertuxkart
Requires:    tuxracer
Requires:    tuxkart
Requires:    tuxtype
Requires:    xkobo

%description
This is meta package for Games and Entertainment.
Listed packages are independent of all desktop environments.
(NOT GNOME, NOT KDE, NOT XFCE4)

%files

%changelog
* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0-1m)
- version 8.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-2m)
- full rebuild for mo7 release

* Sun Feb  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-1m)
- version 7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0-1m)
- version 6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc43

* Mon Oct 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-3m)
- add Requires: supertuxkart

* Tue Jul  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- add Requires: freeciv

* Tue Apr 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- initial package for Momonga Linux
