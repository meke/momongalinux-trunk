%global momorel 10
Name:           paps
Version:        0.6.8
Release:	%{momorel}m%{?dist}

License:        LGPLv2+
URL:            http://paps.sourceforge.net/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
Source1:        paps.convs
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  pango-devel automake autoconf libtool doxygen cups-devel
BuildRequires:  freetype-devel >= 2.5.3
## https://sourceforge.net/tracker/index.php?func=detail&aid=1832897&group_id=153049&atid=786241
Patch0:         paps-0.6.8-shared.patch
## https://sourceforge.net/tracker/index.php?func=detail&aid=1832924&group_id=153049&atid=786241
Patch1:         paps-0.6.8-wordwrap.patch
## https://sourceforge.net/tracker/index.php?func=detail&aid=1832926&group_id=153049&atid=786241
Patch2:         paps-langinfo.patch
## https://sourceforge.net/tracker/index.php?func=detail&aid=1832929&group_id=153049&atid=786241
Patch3:         paps-0.6.6-lcnumeric.patch
## https://sourceforge.net/tracker/index.php?func=detail&aid=1832935&group_id=153049&atid=786241
Patch4:         paps-exitcode.patch
Patch50:        paps-cups.patch
Patch51:        paps-cpilpi.patch
Patch52:        paps-dsc-compliant.patch
Patch53:        paps-autoconf262.patch
Patch54:        paps-fix-cpi.patch
Patch55:	paps-fix-loop-in-split.patch
## rhbz#857592
Patch56:        paps-fix-tab-width.patch
Patch57:        paps-fix-non-weak-symbol.patch
Patch58:        paps-correct-fsf-address.patch
## rhbz#1078519
Patch59:	%{name}-ft-header.patch

Summary:        Plain Text to PostScript converter
Group:          Applications/Publishing
%description
paps is a PostScript converter from plain text file using Pango.

%package libs
Summary:        Libraries for paps
Group:          Development/Libraries
%description libs
paps is a PostScript converter from plain text file using Pango.

This package contains the library for paps.

%package devel
Summary:        Development files for paps
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}
%description devel
paps is a PostScript converter from plain text file using Pango.

This package contains the development files that is necessary to develop
applications using paps API.

%prep
%setup -q
%patch0 -p1 -b .shared
%patch1 -p1 -b .wordwrap
%patch2 -p1 -b .langinfo
%patch3 -p1 -b .lcnumeric
%patch4 -p1 -b .exitcode
%patch50 -p1 -b .cups
%patch51 -p1 -b .cpilpi
%patch52 -p1 -b .dsc
%patch53 -p1 -b .autoconf262
%patch54 -p1 -b .fixcpi
%patch55 -p1 -b .loop
%patch56 -p1 -b .tab
%patch57 -p1 -b .weak-symbol
%patch58 -p1 -b .fsf
%patch59 -p1 -b .ft-header
libtoolize -f -c
autoreconf


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# remove unnecessary files
rm $RPM_BUILD_ROOT%{_libdir}/libpaps.la

# make a symlink for CUPS filter
%{__mkdir_p} $RPM_BUILD_ROOT/usr/lib/cups/filter # Not libdir
ln -s %{_bindir}/paps $RPM_BUILD_ROOT/usr/lib/cups/filter/texttopaps

install -d %{buildroot}%{_sysconfdir}/cups
install -p -m0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/cups
%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc AUTHORS COPYING.LIB README TODO
%{_bindir}/paps
%{_mandir}/man1/paps.1*
/usr/lib/cups/filter/texttopaps
%dir %attr (0755, root, lp) %{_sysconfdir}/cups
%{_sysconfdir}/cups/paps.convs

%files libs
%defattr(-, root, root, -)
%doc COPYING.LIB
%{_libdir}/libpaps.so.*

%files devel
%defattr(-, root, root, -)
%doc COPYING.LIB
%{_includedir}/libpaps.h
%{_libdir}/libpaps.so

%changelog
* Mon Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.8-10m)
- enable to build with freetype-2.5.3

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-9m)
- fix build failure
-- merge fixes from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.8-6m)
- full rebuild for mo7 release

* Sat Dec 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-5m)
- autoreconfig should be called with "-fi" when libtool stuff is used

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-2m)
- update Patch2,4 for fuzz=0

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.8-1m)
- update to 0.6.8 (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-4m)
- rebuild against gcc43

* Sat Jun 16 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.6-3m)
- revised cups filter directory

* Sat Jun 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-2m)
- change Source URL

* Thu Jun 14 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.6-1m)
- import from Fedora
- for cups



