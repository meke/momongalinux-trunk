%global momorel 1

Summary: A system documentation reader from the Gnome project
Name: yelp
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
BuildRequires: pkgconfig
BuildRequires: yelp-xsl >= 3.1.2
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: gnome-doc-utils-devel >= 0.19.1
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: libxslt-devel >= 1.1.24
BuildRequires: startup-notification-devel >= 0.9
BuildRequires: dbus-glib-devel >= 0.80
BuildRequires: pango-devel >= 1.24.0
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: libgcrypt-devel
BuildRequires: libXfixes-devel
BuildRequires: rarian-devel
Requires(pre): hicolor-icon-theme gtk2
Requires: gnome-doc-utils
Requires: yelp-xsl

%description
Yelp is the Gnome 2 help/documentation browser. It is designed
to help you browse all the documentation on your system in
one central tool.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --disable-static \
    --disable-schemas-install \
    --with-search=basic
    
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
update-desktop-database &> /dev/null ||:

%postun
update-desktop-database &> /dev/null ||:
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc ChangeLog AUTHORS COPYING MAINTAINERS NEWS README
%{_bindir}/*
%{_libdir}/libyelp.so.*
%exclude %{_libdir}/*.la
%{_datadir}/applications/*
%{_datadir}/%{name}
%{_datadir}/glib-2.0/schemas/org.gnome.yelp.gschema.xml
%{_datadir}/yelp-xsl/xslt/common/domains/yelp.xml

%files devel
%defattr(-,root,root)
%{_includedir}/libyelp
%{_libdir}/*.so
%{_datadir}/gtk-doc/html/libyelp

%changelog
* Tue Oct 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Mon Sep 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- fix build failure with glib 2.33+

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.3-2m)
- add BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.3-1m)
- update to 3.1.3

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue Aug 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-4m)
- add Requires: gnome-doc-utils

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Wed May 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sun Feb 21 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.29.4-2m)
- fix BR version gnome-doc-utils-devel

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.4-1m)
- update to 2.29.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-2m)
- delete __libtoolize hack

* Mon Nov 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.28.0-3m)
- revised BuildRequires gnome-doc-utils-devel version

* Thu Oct 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
- good-bye beagle

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Aug  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-4m)
- Change BPR from rarian to rarian-devel

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-3m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Tue May 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.0-1m)
- add autoreconf. support libtool-2.2.x

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.1-1m)
- update to 2.25.1

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24.0-3m)
- rebuild without lzma

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Aug 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.1-6m)
- [SECURITY] CVE-2008-3533
- add security patch from http://bugzilla.gnome.org/attachment.cgi?id=115890

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.1-5m)
- add Requires: gecko-libs

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-4m)
- delete patch2 (broken patch)
- fix desktop

* Thu May  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.22.1-3m)
- release a directory provided by gnome-desktop

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against firefox-3

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.22.0-4m)
- rebuild against firefox-2.0.0.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.22.0-2m)
- rebuild against firefox-2.0.0.13

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.0-8m)
- rebuild against firefox-2.0.0.12

* Wed Dec 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-7m)
- delete patch0 (good-bye beagle)

* Sun Dec  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-6m)
- rebuild against beagle-0.3.0 (add patch0)

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-5m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-4m)
- rebuild against firefox-2.0.0.10

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-3m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-2m)
- rebuild against firefox-2.0.0.8

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Sep 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.1-5m)
- rebuild against firefox-2.0.0.7

* Mon Aug  6 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.18.1-4m)
- rebuild against firefox-2.0.0.6

* Sat Jul 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.1-3m)
- rebuild against firefox-2.0.0.5

* Sat Jun  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-2m)
- rebuild against firefox

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Nov 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.3-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sun Apr 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-2m)
- rebuild against mozilla-1.7.13

* Sat Apr 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.12.2-2m)
- rebuild against openssl-0.9.8a

* Mon Dec  5 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.5-1m)
- version 2.6.5
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Fri Apr 16 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0
- GNOME 2.6 Desktop
- remove 3 pathches, but yelp-1.0.1-stylesheetpath.patch might be needed

* Fri Nov 14 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.2-1m)
- version 2.4.2

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Fri Sep 19 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.4.0-2m)
- revise %%files section (/usr/libexec/{yelp-man2html,yelp-info2html} were missing)

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Tue Apr 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-3m)
- rebuild against for XFree86-4.3.0

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0-2m)
  rebuild against openssl 0.9.7a

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Tue Oct 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Wed Sep  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.6-1m)
- version 1.0.6

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.5-1m)
- version 1.0.5

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.4-3m)
- mujitu!

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-2m)
- rebuild against libgnome-2.0.2-2m

* Fri Aug 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.4-1m)
- version 1.0.4

* Wed Aug 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-1m)
- version 1.0.3

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-11m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-10m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-9m)
- add man uniq patch

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-8m)
- add section_key patch

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-7m)
- rebuild against for gnome-games-2.0.1.1
- rebuild against for gail-0.17
- rebuild against for pygtk-1.99.12
- rebuild against for librsvg-2.0.1
- rebuild against for eel-2.0.1
- rebuild against for nautilus-2.0.1

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-6m)
- stylesheet path

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-5m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-4k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-2k)
- version 1.0.1

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-12k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-10k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-8k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-6k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-2k)
- version 1.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-12k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-10k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-8k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-6k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-4k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-2k)
- version 0.10

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-2k)
- version 0.9.1

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.9-2k)
- version 0.9

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.8-2k)
- version 0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-8k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Tue May 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-2k)
- version 0.7

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.6.1-2k)
- version 0.6.1

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.6-6k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.6-4k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.6-2k)
- version 0.6

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.5-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.5-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.5-2k)
- version 0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-2k)
- version 0.4

* Wed Mar 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-2k)
- version 0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-18k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-16k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-14k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-12k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-10k)
- rebuild against for gail-0.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-8k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-4k)
- change depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-2k)
- version 0.3

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-18k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-16k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-14k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-12k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-8k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-6k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-4k)
- rebuild against for bonobo-activation-0.9.4

* Thu Feb 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.1-2k)
- version 0.2.1
* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-20k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-18k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-16k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-14k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-12k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-10k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-8k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-6k)
- rebuild against for gnome-vfs-1.9.6

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-4k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-2k)
- version 0.2
- rebuild against for libgnomeui-1.110.0
- rebuild against for libbonobo-1.110.0

* Mon Jan 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.1-2k)
- create
