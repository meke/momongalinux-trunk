%global momorel 29

Summary: A GNU utility for monitoring a program's use of system resources.
Name: time
Version: 1.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://www.gnu.org/directory/time.html
Source0: ftp://ftp.gnu.org/gnu/time/%{name}-%{version}.tar.gz
NoSource: 0
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The GNU time utility runs another program, collects information about
the resources used by that program while it is running and
displays the results.

Time can help developers optimize their programs.

%prep
%setup -q

%build
echo "ac_vc_func_wait3=\${ac_vc_func_wait3='yes'}" >> config.cache
%configure
make

%install
rm -rf %{buildroot}
%makeinstall

# gzip -9nf %{buildroot}%{_infodir}/time.info
strip %{buildroot}/%{_bindir}/time

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/time.info %{_infodir}/dir \
	--entry="* time: (time).		GNU time Utility" 

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/time.info %{_infodir}/dir \
	--entry="* time: (time).		GNU time Utility" 
fi

%files
%defattr(-,root,root)
%doc NEWS README
%{_bindir}/time
%{_infodir}/time.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7-27m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7-26m)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-25m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-24m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7-23m)
- rebuild against gcc43

* Tue Jan 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7-22m)
- PreReq: info

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.7-21m)
- sync with rawhide(time-1.7-21)
   remove time-1.7-alpha-uuum.patch and always asume that we have wait3.
- revise %%post and %%preun

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.7-20k)
- updated Source0 URL.

* Mon Oct 15 2001 Masaru Sato <masachan@kondara.org>
- add url tag

* Thu Jul 19 2001 Toru Hoshina <toru@df-usa.com>
- alpha. uuum. wait3 issue.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Jun 29 2000 Preston Brown <pbrown@redhat.com>
- using / as the file manifesto has weird results.

* Sun Jun  4 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 9)

* Mon Aug 10 1998 Erik Troan <ewt@redhat.com>
- buildrooted and defattr'd

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Oct 27 1997 Cristian Gafton <gafton@redhat.com>
- fixed info handling

* Thu Oct 23 1997 Cristian Gafton <gafton@redhat.com>
- updated the spec file; added info file handling

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
