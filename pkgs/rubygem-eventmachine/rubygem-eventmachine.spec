# Generated from eventmachine-1.0.0.beta.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname eventmachine
%global real_version 1.0.0.beta.4

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{real_version}
%global rubyabi 1.9.1

Summary: Ruby/EventMachine library
Name: rubygem-%{gemname}
Version: 1.0.0
Release: 0.4.%{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyeventmachine.com
Source0: %{gemname}-%{real_version}.gem
#NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) > 1.3.1
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) > 1.3.1
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{real_version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
EventMachine implements a fast, single-threaded engine for arbitrary network
communications. It's extremely easy to use in Ruby. EventMachine wraps all
interactions with IP sockets, allowing programs to concentrate on the
implementation of network protocols. It can be used to create both network
servers and clients. To create a server or client, a Ruby program only needs
to specify the IP address and port, and provide a Module that implements the
communications protocol. Implementations of several standard network protocols
are provided with the package, primarily to serve as examples. The real goal
of EventMachine is to enable programs to easily interface with other programs
using TCP/IP, especially if custom protocols are required.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{real_version}
%doc %{geminstdir}/README.md
%doc %{geminstdir}/docs/DocumentationGuidesIndex.md
%doc %{geminstdir}/docs/GettingStarted.md
%doc %{geminstdir}/docs/old/ChangeLog
%doc %{geminstdir}/docs/old/DEFERRABLES
%doc %{geminstdir}/docs/old/EPOLL
%doc %{geminstdir}/docs/old/INSTALL
%doc %{geminstdir}/docs/old/KEYBOARD
%doc %{geminstdir}/docs/old/LEGAL
%doc %{geminstdir}/docs/old/LIGHTWEIGHT_CONCURRENCY
%doc %{geminstdir}/docs/old/PURE_RUBY
%doc %{geminstdir}/docs/old/RELEASE_NOTES
%doc %{geminstdir}/docs/old/SMTP
%doc %{geminstdir}/docs/old/SPAWNED_PROCESSES
%doc %{geminstdir}/docs/old/TODO
%{gemdir}/gems/%{gemname}-%{real_version}/
%{gemdir}/cache/%{gemname}-%{real_version}.gem
%{gemdir}/specifications/%{gemname}-%{real_version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-0.4.1m)
- update 1.0.0.beta.4

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.12.10-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.10-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.10-1m)
- Initial package for Momonga Linux

