%global momorel 7

Summary: alternative views on images
Name: colorblind
Version: 0.0.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://colorblind.alioth.debian.org/
Source0: http://alioth.debian.org/frs/download.php/1952/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This library provides an unified way to recalculate colors in order to
present alternative views on images for colorblind people.

%package devel
Summary: colorblind-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
colorblind-devel

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libcolorblind.so.*

%files devel
%{_includedir}/colorblind.h
%{_libdir}/libcolorblind.a
%{_libdir}/libcolorblind.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-2m)
- rebuild against gcc43

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.1-1m)
- initial build
