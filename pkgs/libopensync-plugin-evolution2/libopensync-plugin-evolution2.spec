%global momorel 9
%global libopensyncrel 2m

Summary: Evolution 2 Synchronization Plug-In for OpenSync
Name: libopensync-plugin-evolution2
Version: 0.39
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.opensync.org/
Group: System Environment/Libraries
Source0: http://opensync.org/download/releases/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: evolution-data-server
BuildRequires: cmake
BuildRequires: evolution-devel
BuildRequires: evolution-data-server-devel >= 3.1
BuildRequires: libX11-devel
BuildRequires: libopensync-devel >= %{version}-%{libopensyncrel}
BuildRequires: pkgconfig
Obsoletes: %{name}-devel

%description
This plug-in allows applications using OpenSync to synchronize to and
from Evolution 2.

Additionally install the libopensync package.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
	-DCMAKE_SKIP_RPATH=YES ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README
%{_libdir}/libopensync1/formats/evo2-format.so
%{_libdir}/libopensync1/plugins/evo2-sync.so
%{_datadir}/libopensync1/defaults/evo2-sync

%changelog
* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.39-9m)
- rebuild against evolution-data-server-3.1

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.39-8m)
- rebuild against evolution-data-server-3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-6m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.39-5m)
- rebuild against evolution-2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-4m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.39-3m)
- rebuild against evolution-2.30.2

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.39-2m)
- rebuild against evolution-data-server-2.29.90

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-1m)
- version 0.39

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-1m)
- version 0.38

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-0.20081004.3m)
- rebuild against rpm-4.6

* Sat Oct 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-0.20081004.2m)
- rebuild against evolution-data-server-2.24.1

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-0.20081004.1m)
- update to 0.38 svn snapshot

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.36-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.36-1m)
- version 0.36

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-1m)
- initial package for libopensync
- Summary and %%description are imported from opensuse
