%global momorel 1

Summary: A graphical interface for modifying the system language
Name: system-config-language
Version: 1.4.0
Release: %{momorel}m%{?dist}
URL: http://fedoraproject.org/wiki/SystemConfig/language
License: GPLv2+
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source0:  https://fedorahosted.org/releases/s/y/system-config-language/%{name}-%{version}.tar.bz2
NoSource: 0
#https://bugzilla.redhat.com/show_bug.cgi?id=1040298
Patch0:   %{name}-%{version}-Disable-OK-for-default-language.patch
#https://bugzilla.redhat.com/show_bug.cgi?id=1040309
Patch1:   %{name}-%{version}-Fix-No-Groups-exists-traceback.patch
#https://bugzilla.redhat.com/show_bug.cgi?id=1045993
Patch2:   %{name}-%{version}-Change-Oriya-to-Odia.patch
#https://bugzilla.redhat.com/show_bug.cgi?id=1050967
Patch3:   %{name}-%{version}-Fix-text-mode-crash-for-no-group-exists.patch
#https://bugzilla.redhat.com/show_bug.cgi?id=1046846
Patch4:   %{name}-%{version}-translation-updates.patch
Patch5:   %{name}-%{version}-update-po-headers.patch
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
Requires: pygtk2
Requires: python
Requires: usermode >= 1.36
Requires: usermode-gtk >= 1.36
Requires: yum >= 2.9.5
Requires: gtk2 >= 2.6
Obsoletes: redhat-config-language
Obsoletes: locale_config

%description
system-config-language is a graphical user interface that 
allows the user to change the default language of the system.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p2
%patch5 -p1

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

desktop-file-install --vendor system --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
  --add-category System \
  --add-category Settings \
  --add-category X-Red-Hat-Base                             \
  %{buildroot}%{_datadir}/applications/system-config-language.desktop

%find_lang %name

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
#%doc doc/*
%{_bindir}/system-config-language
%dir %{_datadir}/system-config-language
%{_datadir}/system-config-language/*
#%dir %{_datadir}/firstboot/
#%dir %{_datadir}/firstboot/modules
#%{_datadir}/firstboot/modules/language.py*
%{_datadir}/applications/system-config-language.desktop
%{_datadir}/icons/hicolor/48x48/apps/system-config-language.png
%{_mandir}/man1/system-config-language.1.*
%config(noreplace) %{_sysconfdir}/pam.d/system-config-language
%config(noreplace) %{_sysconfdir}/security/console.apps/system-config-language


%changelog
* Mon Jun 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0

* Thu Mar 22 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.3.5-1m)
- update 1.3.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-5m)
- full rebuild for mo7 release

* Tue Aug 24 2010 Masaru SANUKI <sanuki@momonga-linux.org> 
- (1.3.4-4m)
- change desktop category

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.4-3m)
- import fedora 13 patches 

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-2m)
- build fix with desktop-file-utils-0.16

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-1m)
- update 1.3.4
- remove Requires: rhpl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-1m)
- sync with Fedora 11 (1.3.2-5)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.15-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.15-1m)
- update 1.2.15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.16-3m)
- rebuild against gcc43

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16-2m)
- modify %%files

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.16-1m)
- update to 1.1.16

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
-  (1.1.11-4m)
- delete pyc pyo

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
-  (1.1.11-3m)
- remove category X-Red-Hat-Base SystemSetup Application

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
-  (1.1.11-2m)
- delete duplicated dir

* Mon May  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
-  (1.1.11-1m)
- first commit Momonga Linux

* Tue Feb 28 2006 Paul Nasrat <pnasrat@redhat.com> - 1.1.11-1
- Update translations
- Serbian locales (#172600)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Oct 24 2005 Paul Nasrat <pnasrat@redhat.com> - 1.1.10-1
- pam_stack deprecated (#170631)

* Wed Apr 27 2005 Jeremy Katz <katzj@redhat.com> - 1.1.9-2
- silence %%post

* Fri Apr 01 2005 Paul Nasrat <pnasrat@redhat.com> 1.1.9-1
- Translation updates
- pygtk deprecations

* Mon Mar 28 2005 Christopher Aillon <caillon@redhat.com>
- rebuilt

* Fri Mar 25 2005 Christopher Aillon <caillon@redhat.com> 1.1.8-2
- Update the GTK+ theme icon cache on (un)install

* Fri Oct 01 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.8-1
- Indic UTF-8 locales
- Translations

* Wed Sep 29 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.7-1
- update locale-list (bug# 134034)

* Tue Sep 07 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.6-2
- Buildrequires intltool

* Tue Sep 07 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.6-1
- Translatable desktop

* Mon Sep 06 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.5-3
- fix gtk.mainloop/mainquit 

* Thu Apr  8 2004 Brent Fox <bfox@redhat.com> 1.1.5-2
- fix icon path (bug #120177)

* Mon Jan 12 2004 Brent Fox <bfox@redhat.com> 1.1.5-1
- update locale-list (bug #107450)

* Fri Jan  9 2004 Brent Fox <bfox@redhat.com> 1.1.4-1
- enable TUI mode

* Wed Jan 07 2004 Than Ngo <than@redhat.com> 1.1.3-1
- make changes for Python2.3

* Thu Nov 20 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- fix typo in the Obsoletes

* Wed Nov 19 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- rebuild

* Wed Nov 12 2003 Brent Fox <bfox@redhat.com> 1.1.0-1
- add Obsoletes for redhat-config-language
- make changes for Python2.3

* Mon Nov 10 2003 Brent Fox <bfox@redhat.com> 1.1.0-1
- convert redhat-config-language into system-config-language

* Mon Oct 13 2003 Brent Fox <bfox@redhat.com> 1.0.16-1
- rebuild for latest translations (bug #106618)

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.0.15-2
- bump release num and rebuild

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.0.15-1
- add Requires for rhpl (bug #104210)

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.0.14-1
- tag on every release

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.0.12-1
- remove python-tools dependency

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.0.11-2
- bump relnum and rebuild

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.0.11-1
- fix build problem

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.0.10-2
- bump relnum and rebuild

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.0.10-1
- change runPriority

* Thu Jul  3 2003 Brent Fox <bfox@redhat.com> 1.0.9-2
- bump relnum and rebuild

* Thu Jul  3 2003 Brent Fox <bfox@redhat.com> 1.0.9-1
- use UTF-8 in CJK locales (bug #98522)

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.0.8-2
- bump relnum and rebuild

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.0.8-1
- use rhpl translation module

* Thu Jun 26 2003 Brent Fox <bfox@redhat.com> 1.0.7-1
- make sure the config file is written before calling changeLocale()

* Thu Jun 26 2003 Brent Fox <bfox@redhat.com> 1.0.6-1
- add some hooks for firstboot so locale can change on the fly (#91984)

* Wed May 21 2003 Brent Fox <bfox@redhat.com> 1.0.5-1
- add some hacks to make simplified chinese work (bug #84772)

* Tue Feb 18 2003 Brent Fox <bfox@redhat.com> 1.0.4-1
- update locale-list (bug #84183)

* Wed Feb 12 2003 Jeremy Katz <katzj@redhat.com> 1.0.3-3
- fixes for cjk tui (#83518)

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.3-2
- fix a po file encoding problem.  please use utf-8 in the future

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.3-1
- bump and build

* Thu Jan 23 2003 Brent Fox <bfox@redhat.com> 1.0.2-2
- add Bulgarian to locale-list

* Thu Jan 23 2003 Brent Fox <bfox@redhat.com> 1.0.2-1
- update translations in desktop file

* Tue Dec 17 2002 Bill Nottingham <notting@redhat.com> 1.0.1-13
- fix dangling symlink that broke firstboot

* Mon Dec 16 2002 Brent Fox <bfox@redhat.com> 1.0.1-12
- fix a typo

* Mon Dec 16 2002 Brent Fox <bfox@redhat.com> 1.0.1-11
- show a warning if run in console mode (bug #78739)

* Sun Dec 15 2002 Brent Fox <bfox@redhat.com> 1.0.1-10
- strip off @euro from the supported langs (bug #77637)

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.0.1-9
- pam path changes

* Tue Oct 15 2002 Brent Fox <bfox@redhat.com> 1.0.1-8
- Handle upgrading with different encodings in /etc/sysconfig/clock

* Thu Sep 19 2002 Brent Fox <bfox@redhat.com> 1.0.1-7
- Patch to desktop file from kmraas@online.no applied for [no] translation

* Tue Sep 10 2002 Bill Nottingham <notting@redhat.com> 1.0.1-6
- don't write SYSFONTACM="utf8"; switch default font to match anaconda

* Tue Sep  3 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.0.1-5
- Obsolete locale_config

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-4
- Convert to noarch

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-3
- Remove dupe for Romanian

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-2
- Only apply the changes if the user actually changed something

* Wed Aug 21 2002 Preston Brown <pbrown@localhost.localdomain> 1.0.1-1
- we were writing to the wrong gdm file...

* Fri Aug 16 2002 Brent Fox <bfox@redhat.com> 1.0-2
- pull translations into locale-list
- convert locale-list to UTF-8

* Fri Aug 16 2002 Preston Brown <pbrown@redhat.com> 1.0-1
- reset GDM config if lang changes

* Wed Aug 14 2002 Brent Fox <bfox@redhat.com> 0.9.9-8
- call destroy on window close

* Tue Aug 13 2002 Tammy Fox <tfox@redhat.com> 0.9.9-7
- better icon

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-6
- Fix desktop file icon path

* Mon Aug 12 2002 Brent Fox <bfox@redhat.com> 0.9.9-5
- update locale list

* Mon Aug 12 2002 Tammy Fox <tfox@redhat.com> 0.9.9-4
- Replace System with SystemSetup in desktop file categories

* Sun Aug 11 2002 Brent Fox <bfox@redhat.com> 0.9.9-3
- fix desktop file

* Mon Aug 05 2002 Brent Fox <bfox@redhta.com> 0.9.9-1
- pull in desktop file translations

* Fri Aug 02 2002 Tammy Fox <tfox@redhat.com> 0.9.8-2
- Fix desktop file categories

* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Make changes for new pam timestamp policy

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-3
- fix Makefiles and spec files so that translations get installed

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- update spec file for public beta 2

* Tue Jul 23 2002 Tammy Fox <tfox@redhat.com> 0.9.5-2
- Fix desktop file (bug #69475)

* Thu Jul 18 2002 Brent Fox <bfox@redhat.com> 0.9.5-1
- Update for pygtk2 API change

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- bump rev num and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-2
- Update changelogs and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-1
- Update changelogs and rebuild

* Mon Jul 01 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Bump rev number

* Mon Jul 01 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Bump rev number

* Thu Jun 27 2002 Brent Fox <bfox@redhat.com> 0.9.1-2
- Added a message dialog when applying changes

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Fixed description

* Tue Jun 25 2002 Brent Fox <bfox@redhat.com> 0.9.4-5
- Create pot file

* Mon Jun 24 2002 Brent Fox <bfox@redhat.com> 0.9.4-4
- Fix spec file

* Fri Jun 21 2002 Brent Fox <bfox@redhat.com> 0.9.0-3
- Remove cancel button
- init doDebug to None

* Thu Jun 20 2002 Brent Fox <bfox@redhat.com> 0.9.0-2
- Don't pass doDebug into init
- Add snapsrc to Makefile

* Wed May 29 2002 Brent Fox <bfox@redhat.com> 0.2.0-6
- handle an existing but empty i18n file 

* Sun May 26 2002 Brent Fox <bfox@redhat.com> 0.2.0-5
- raise a RuntimeError if /etc/sysconfig/i18n file doesn't exist

* Tue May 14 2002 Brent Fox <bfox@redhat.com>
- improved check for existing i18n file
- added debug mode capability

* Tue Nov 28 2001 Brent Fox <bfox@redhat.com>
- initial coding and packaging

