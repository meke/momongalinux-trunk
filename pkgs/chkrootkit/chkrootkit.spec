%global         momorel 5

Name:           chkrootkit
Version:        0.49
Release:        %{momorel}m%{?dist}
Summary:        A tool to locally check for signs of a rootkit
Group:          Applications/System
License:        BSD
URL:            http://www.chkrootkit.org/
Source0:        ftp://ftp.pangeia.com.br/pub/seg/pac/%{name}-%{version}.tar.gz 
Source1:        %{name}X
Source2:        %{name}.png
Source3:        %{name}.desktop
Source4:        %{name}.console
Source5:        %{name}.pam
Patch1:         %{name}-0.44-getCMD.patch
Patch2:         %{name}-0.44-inetd.patch
Patch6:         %{name}-0.47-chklastlog.patch
Patch8:         %{name}-%{version}-nophpcheck.patch
Patch9:         %{name}-%{version}-chkproc-psver.patch
Patch10:        %{name}-%{version}-chkutmp-outofbounds.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       usermode
BuildRequires:  desktop-file-utils

%description
chkrootkit is a tool to locally check for signs of a rootkit. It contains:

 * chkrootkit: shell script that checks system binaries for rootkit
  modification. The following tests are made:

  o aliens asp bindshell lkm rexedcs sniffer wted scalper slapper z2 amd
    basename biff chfn chsh cron date du dirname echo egrep env find fingerd
    gpm grep hdparm su ifconfig inetd inetdconf init identd killall ldsopreload
    login ls lsof mail mingetty netstat named passwd pidof pop2 pop3 ps pstree
    rpcinfo rlogind rshd slogin sendmail sshd syslogd tar tcpd tcpdump top
    telnetd timed traceroute vdir w write 

 * ifpromisc.c: checks if the interface is in promiscuous mode.
 * chklastlog.c: checks for lastlog deletions.
 * chkwtmp.c: checks for wtmp deletions.
 * check_wtmpx.c: checks for wtmpx deletions. (Solaris only)
 * chkproc.c: checks for signs of LKM trojans.
 * chkdirs.c: checks for signs of LKM trojans.
 * strings.c: quick and dirty strings replacement. 


%prep
%setup -q -n %{name}-%{version}
%patch1 -p1 -b .getCMD
%patch2 -p1 -b .inetd
%patch6 -p1 -b .chklastlog
%patch8 -p0 -b .nophpcheck
%patch9 -p0 -b .chkproc-paver
%patch10 -p0 -b .chkutmp-outofbounds
sed -i -e 's!\s\+@strip.*!!g' Makefile

%build
make sense CC="%{__cc} %{optflags} -D_FILE_OFFSET_BITS=64"

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_sbindir}
cat << EOF > .tmp.chkrootkit.sbin
#! /bin/sh
cd %{_libdir}/%{name}-%{version}
./chkrootkit
EOF
install -p -D -m0755 .tmp.chkrootkit.sbin %{buildroot}%{_sbindir}/chkrootkit

mkdir -p %{buildroot}%{_bindir}
ln -s %{_bindir}/consolehelper %{buildroot}%{_bindir}/chkrootkit

install -p -D -m0755 %{SOURCE1} %{buildroot}%{_bindir}/chkrootkitX
perl -pi -e 's!/usr/bin!%{_bindir}!' %{buildroot}%{_bindir}/chkrootkitX
install -p -D -m0644 %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/chkrootkit.png
install -p -D -m0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/security/console.apps/chkrootkit
perl -pi -e 's!/usr/sbin!%{_sbindir}!' %{buildroot}%{_sysconfdir}/security/console.apps/chkrootkit
install -p -D -m0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/pam.d/chkrootkit
for f in \
    check_wtmpx  \
    chkdirs  \
    chklastlog  \
    chkproc  \
    chkrootkit  \
    chkutmp  \
    chkwtmp  \
    ifpromisc  \
    strings-static \
; do
    install -p -D -m0755 $f %{buildroot}%{_libdir}/%{name}-%{version}/${f}
done
ln -s strings-static %{buildroot}%{_libdir}/%{name}-%{version}/strings

desktop-file-install --vendor=""                    \
  --dir %{buildroot}%{_datadir}/applications      \
  --remove-category=Application \
  %{SOURCE3}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ACKNOWLEDGMENTS COPYRIGHT README README.chklastlog README.chkwtmp chkrootkit.lsm
%{_sbindir}/chkrootkit
%{_bindir}/chkrootkit
%{_bindir}/chkrootkitX
%{_sysconfdir}/pam.d/chkrootkit
%{_sysconfdir}/security/console.apps/chkrootkit
%{_libdir}/%{name}-%{version}
%{_datadir}/applications/chkrootkit.desktop
%{_datadir}/pixmaps/chkrootkit.png

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-5m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.49-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.49-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.49-2m)
- full rebuild for mo7 release

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49
- sync with Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-2m)
- rebuild against rpm-4.6

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48
- sync with Fedora devel and import some patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.47-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.47-3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-2m)
- update chkrootkit.pam for new pam
- enable %%{optflags}

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47 (sync with FC-devel)
- import patch3 - patch6 from FC

* Fri Sep 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.46a-2m)
- remove category Application

* Tue Nov  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.46a-1m)
- version up

* Fri Jul  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.45-2m)
- add chkutmp

* Sun Mar 21 2005 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (0.45-1m)
- some bug fixes

* Thu Dec 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.44-2m)
- requires 'usermode' package instead of '%{_bindir}/consolehelper'

* Wed Dec 15 2004 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (0.44-1m)
- import from fedora.us

* Mon Oct  4 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.44-0.fdr.2
- Fix inetd/sshd checks.

* Sat Sep 11 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:0.44-0.fdr.1
- Update to 0.44.
