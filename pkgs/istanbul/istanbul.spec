%global momorel 9

%define gstreamer_version		0.10.9
%define gstreamer_plugins_version	0.10.3
%define pygtk2_version 			2.8.6
%define pyver %(python -c 'import sys ; print sys.version[:3]')
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_pytho\n_lib()")}

Summary: Desktop Session Recorder 
Name: istanbul 
Version: 0.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://live.gnome.org/Istanbul/
Source: http://zaheer.merali.org/istanbul-%{version}.tar.bz2
NoSource: 0
Patch0: istanbul-configure.patch
Patch1: istanbul-screencast.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: pygtk2 >= %{pygtk2_version}
Requires: gnome-desktop >= 2.6.0
Requires: gstreamer >= %{gstreamer_version}
Requires: gstreamer-plugins-good >= %{gstreamer_plugins_version}
Requires: gstreamer-python
#Requires: gnome-python2-libegg
Requires: gnome-python2-gconf
Requires: gnome-python2-extras
Requires: python-xlib

BuildRequires: python, python-devel
BuildRequires: python-xlib
BuildRequires: pygtk2-devel >= %{pygtk2_version}
BuildRequires: gnome-python2-extras
BuildRequires: gnome-python2-gconf
#BuildRequires: gnome-python2-libegg
BuildRequires: desktop-file-utils
BuildRequires: gstreamer-devel >= %{gstreamer_version}
BuildRequires: gstreamer-plugins-good >= %{gstreamer_plugins_version}
BuildRequires: gstreamer-python, gtk2-devel 
BuildRequires: perl-XML-Parser
BuildRequires: gettext

%description
Istanbul is a desktop session recorder.  You can use it to record your 
desktop session and then play it back for demos, tutorials and 
presentations.  Sessions are recorded to ogg theora files for later 
playback.

#%package devel
#Summary: Libraries/include files for Istanbul.
#Group: Development/Libraries
#Requires: %{name} = %{version}-%{release}

%prep
%setup -q
%patch0
%patch1
%build
#aclocal
#automake
#autoconf

%configure
# --disable-schemas-install

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make DESTDIR=%{buildroot} install
rm -f $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/*.la

desktop-file-install --vendor=		\
    --remove-category Application \
    --add-category Utility \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications			\
  $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop

%find_lang %{name}
  
%pre
if [ $1 -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    if [ -f %{_sysconfdir}/gconf/schemas/istanbul.schemas ]; then
        gconftool-2 --makefile-uninstall-rule \
          %{_sysconfdir}/gconf/schemas/istanbul.schemas >/dev/null || :
        killall -HUP gconfd-2 || :
    fi
fi

%preun
if [ $1 -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/istanbul.schemas > /dev/null || :
    killall -HUP gconfd-2 || :
fi

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/istanbul.schemas > /dev/null || :
killall -HUP gconfd-2 || :
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%clean
rm -rf $RPM_BUILD_ROOT

#%post

#%postun

%files -f %{name}.lang
#%files
%defattr(-,root,root,-)
#%doc AUTHORS COPYING ChangeLog NEWS README 
%doc COPYING ChangeLog
%{_bindir}/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/%{name}.png
%{_sysconfdir}/gconf/schemas/istanbul.schemas
#%dir %{_datadir}/%{name}
#%dir %{_datadir}/%{name}/glade
#%{_datadir}/%{name}/glade/prefs.glade

%dir %{python_sitelib}/%{name}
%{python_sitelib}/%{name}/*.py*

#%dir %{python_sitelib}/%{name}/extern
#%{python_sitelib}/%{name}/extern/*.py*
#%dir %{python_sitelib}/%{name}/extern/pytrayicon
#%{python_sitelib}/%{name}/extern/pytrayicon/*.py*

%{_libdir}/gstreamer-0.10/libistximagesrc.so
%{_libdir}/gstreamer-0.10/libistximagesrc.so.*
%dir %{python_sitelib}/%{name}/main
%{python_sitelib}/%{name}/main/*.py*
%dir %{python_sitelib}/%{name}/configure
%{python_sitelib}/%{name}/configure/*.py*
%{_mandir}/man1/*

#%{python_sitelib}/%{name}/extern/pytrayicon/*.so*

%changelog
* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.2-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-6m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.2-3m)
- rebuild against python-2.6.1-1m

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-2m)
- change BuildRequires: gst-plugins-good to gstreamer-plugins-good
- change BuildRequires: gst-python to gstreamer-python
- change Requires: gst-plugins-good to gstreamer-plugins-good
- change Requires: gst-python to gstreamer-python

* Tue May 13 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.2-1m)
- import to Momonga (from Fedora)

* Fri May 2 2008 Jef Spaleta <jspaleta@fedoraproject.org> - 0.2.2-7
- patch from upstream svn for gst audio stream closure.

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.2.2-6
- Autorebuild for GCC 4.3

* Fri Oct 19 2007 Jef Spaleta <jspaleta@gmail.com> - 0.2.2-5
- Source Url fix

* Tue Aug 21 2007 Jef Spaleta <jspaleta@gmail.com> - 0.2.2-4
- bump for rebuild

* Fri Aug 03 2007 Jef Spaleta <jspaleta@gmail.com> - 0.2.2-3
- Update spec for new licensing guidance.

* Sat Apr 14 2007 Jef Spaleta <jspaleta@gmail.com> - 0.2.2-2
- Add python-xlib dependancy

* Sat Mar 24 2007 Jef Spaleta <jspaleta@gmail.com> - 0.2.2-1
- Update to latest upstream release

* Mon Dec 11 2006 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-10
- rebuild against python 2.5 in devel tree

* Thu Mar 23 2006 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-9
- attempt to fix 64bit build failures do to unpackaged la

* Wed Feb 01 2006 Jef Spaleta  <jspaleta@gmail.com> - 0.1.1-8
- added pygtk2-devel buildrequires

* Fri Jan 27 2006 Jonathan Blandford <jrb@redhat.com> - 0.1.1-6
- change gstreamer-python to gstreamer08-python

* Sat Jan 07 2006 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-6
- Change requirements to gstreamer08* to match rawhide change

* Thu Aug 18 2005 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-5
- rebuild

* Thu Jul 21 2005 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-4
- minor specfile cleanup

* Wed Jul 20 2005 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-3
- minor specfile cleanup

* Tue Jul 19 2005 Jef Spaleta <jspaleta@gmail.com> - 0.1.1-2
- added gtk2-devel and python-devel BuildRequires 
- removed the python-abi requires because rpm in fc4+ catches this now

* Fri Jul 01 2005 John (J5) Palmieri <johnp@redhat.com> - 0.1.1-1
- upgrade to upstream 0.1.1
- remove pythondir patch

* Wed Jun 29 2005 John (J5) Palmieri <johnp@redhat.com> - 0.1.0-2
- Cleanup the spec file

* Wed Jun 29 2005 John (J5) Palmieri <johnp@redhat.com> - 0.1.0-1
- Initial packaging

