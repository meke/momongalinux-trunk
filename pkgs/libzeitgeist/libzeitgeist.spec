%global		momorel 2
Name:           libzeitgeist
Version:        0.3.18
Release:        %{momorel}m%{?dist}
Summary:        Client library for applications that want to interact with the Zeitgeist daemon

Group:          System Environment/Libraries
License:        LGPLv3 and GPLv3
URL:            https://launchpad.net/libzeitgeist
Source0:        http://launchpad.net/%{name}/0.3/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:         %{name}-disable-log-test.patch

BuildRequires:  glib2-devel%{?_isa} >= 2.26
BuildRequires:  gtk-doc

Requires:  zeitgeist

%description
This project provides a client library for applications that want to interact
with the Zeitgeist daemon. The library is written in C using glib and provides
an asynchronous GObject oriented API.

%package        devel
Summary:        Development files for %{name}%{?_isa}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
%patch0 -p1


%build
%configure --disable-static
%make V=1


%check
make check


%install
make install DESTDIR=%{buildroot} INSTALL="install -p"
install -d -p -m 755 %{buildroot}%{_datadir}/vala/vapi
install -D -p -m 644 bindings/zeitgeist-1.0.{vapi,deps} %{buildroot}%{_datadir}/vala/vapi
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# remove duplicate documentation
rm -fr %{buildroot}%{_defaultdocdir}/%{name}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)

# documentation
%doc COPYING COPYING.GPL README

# essential
%{_libdir}/*.so.*


%files devel
%defattr(-,root,root,-)

# Documentation
%doc AUTHORS ChangeLog COPYING COPYING.GPL MAINTAINERS NEWS 
%doc examples/*.vala examples/*.c
%{_datadir}/gtk-doc/html/zeitgeist-1.0/

# essential
%{_includedir}/zeitgeist-1.0/
%{_libdir}/pkgconfig/zeitgeist-1.0.pc
%{_libdir}/*.so

# extra
%{_datadir}/vala/vapi/


%changelog
* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.18-2m)
- fix momorel

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.18-1m)
- import from fedora