%global momorel 1

Name:		intltool
Summary:	This module contains some utility scripts and assorted auto* magic for internationalizing various kinds of data files.
Version: 0.50.2
Release: %{momorel}m%{?dist}
License: 	GPL
Group:		Development/Tools
Source0: http://edge.launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.gz
NoSource: 0
URL: 		https://edge.launchpad.net/intltool
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
Obsoletes:	xml-i18n-tools
Provides: 	xml-i18n-tools = 0.11
BuildRequires: 	perl-XML-Parser
BuildRequires: 	gettext
Requires:	patch
Requires:	automake
Requires:	gettext-devel
Requires: 	perl(XML::Parser)

%description
** Automatically extracts translatable strings from oaf, glade, bonobo
  ui, nautilus theme and other XML files into the po files.

** Automatically merges translations from po files back into .oaf files
  (encoding to be 7-bit clean). I can also extend this merging
  mechanism to support other types of XML files.

%prep
%setup -q

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README TODO
%{_bindir}/intltool-extract
%{_bindir}/intltool-update
%{_bindir}/intltoolize
%{_bindir}/intltool-prepare
%{_bindir}/intltool-merge
%{_datadir}/intltool
%{_mandir}/man8/*.8*
%{_datadir}/aclocal/*.m4

%changelog
* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.50.2-1m)
- update to 0.50.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.41.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.41.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.41.1-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.41.1-1m)
- update to 0.41.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.40.6-1m)
- update to 0.40.6

* Fri Mar  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40.5-3m)
- revise spec
-- remove unused entries
-- add Requires: and BuildRequires:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40.5-2m)
- rebuild against rpm-4.6

* Thu Oct  9 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.40.5-1m)
- update to 0.40.5

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.40.4-1m)
- update to 0.40.4

* Thu Jun  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.40.0-1m)
- update to 0.40.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.37.1-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.37.1-1m)
- update to 0.37.1

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.37.0-1m)
- update to 0.37.0

* Mon Dec 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.36.3-1m)
- update to 0.36.3

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.36.2-1m)
- update to 0.36.2

* Wed Aug 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.36.1-1m)
- update to 0.36.1

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.36.0-1m)
- update to 0.36.0

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.35.5-1m)
- update to 0.35.5

* Tue Feb  6 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPreReq: perl-XML-Parser

* Sun Jan 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.35.4-1m)
- update to 0.35.4

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.35.0-1m)
- update to 0.35.0

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.34.2-1m)
- update to 0.34.2

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.34.1-2m)
- comment out unnessesaly autoreconf and make check

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.34.1-1m)
- version up.
- GNOME 2.12.1 Desktop

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.32.1-1m)
- version 0.32.1
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.31.2-1m)
- version 0.31.2

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.31-1m)
- version 0.31

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.30-1m)
- version 0.30
- GNOME 2.6 Desktop

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.27.2-2m)
- change docdir %%defattr

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.27.2-1m)
- version 0.27.2

* Mon May 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.26-1m)
- version  0.26

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.25-1m)
- version 0.25

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.24-1m)
- version 0.24

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.23-1m)
- version 0.23

* Tue Jun  4 2002 Shingo Akagaki <dora@kondara.org>
- (0.22-2k)
- version 0.22

* Tue Apr  2 2002 Shingo Akagaki <dora@kondara.org>
- (0.18-2k)
- version 0.18

* Sun Mar 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.17-2k)
- version 0.17

* Wed Feb 27 2002 Shingo Akagaki <dora@kondara.org>
- (0.16-2k)
- version 0.16

* Wed Feb  6 2002 Shingo Akagaki <dora@kondara.org>
- (0.14-2k)
- version 0.14

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.13-2k)
- version 0.13

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (0.12-2k)
- version 0.12

* Tue Oct  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.11-2k)
- package name changed

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (0.8.1-6k)
- no more ifarch alpha.

* Fri Apr 20 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- fix doc dir

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- first release for K2K

* Thu Jan 04 2000 Robin * Slomkowski <rslomkow@eazel.com>
- created this thing
