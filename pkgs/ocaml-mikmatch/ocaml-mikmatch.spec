%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-mikmatch
Version:        1.0.4
Release:        %{momorel}m%{?dist}
Summary:        OCaml extension for pattern matching with regexps

Group:          Development/Libraries
License:        Modified BSD
URL:            http://martin.jambon.free.fr/micmatch.html
Source0:        http://martin.jambon.free.fr/mikmatch-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m
BuildRequires:  pcre-devel

%global __ocaml_provides_opts -i Charset -i Constants -i Global_def -i Match -i Messages -i Mm_util -i Pa_mikmatch_pcre -i Pa_mikmatch_str -i Pcre_lib -i Regexp_ast -i Select_lib -i Str_lib -i Syntax_common -i Syntax_pcre -i Syntax_str

%description
Mikmatch (with a 'k') is the OCaml >= 3.10 version of Micmatch, an
extension for adding pattern matching with regular expressions to the
language.

The goal of Micmatch/Mikmatch is to make text-oriented programs even
easier to write, read and run without losing the unique and powerful
features of Objective Caml (OCaml).

Micmatch/Mikmatch provides a concise and highly readable syntax for
regular expressions, and integrates it into the syntax of OCaml thanks
to Camlp4.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n mikmatch-%{version}


%build
make all str pcre
%if %opt
make opt
%endif


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install-str install-pcre


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/mikmatch_str
%{_libdir}/ocaml/mikmatch_pcre
%if %opt
%exclude %{_libdir}/ocaml/mikmatch_str/*.a
%exclude %{_libdir}/ocaml/mikmatch_str/*.cmxa
%exclude %{_libdir}/ocaml/mikmatch_str/*.cmx
%exclude %{_libdir}/ocaml/mikmatch_pcre/*.a
%exclude %{_libdir}/ocaml/mikmatch_pcre/*.cmxa
%exclude %{_libdir}/ocaml/mikmatch_pcre/*.cmx
%endif
%exclude %{_libdir}/ocaml/mikmatch_str/*.mli
%exclude %{_libdir}/ocaml/mikmatch_pcre/*.mli


%files devel
%defattr(-,root,root,-)
%doc LICENSE README
%if %opt
%{_libdir}/ocaml/mikmatch_str/*.a
%{_libdir}/ocaml/mikmatch_str/*.cmxa
%{_libdir}/ocaml/mikmatch_str/*.cmx
%{_libdir}/ocaml/mikmatch_pcre/*.a
%{_libdir}/ocaml/mikmatch_pcre/*.cmxa
%{_libdir}/ocaml/mikmatch_pcre/*.cmx
%endif
%{_libdir}/ocaml/mikmatch_str/*.mli
%{_libdir}/ocaml/mikmatch_pcre/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- rebuild against ocaml-3.11.2
- rebuild against ocaml-pcre-6.1.0-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora 11
- update to 1.0.1

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.0-4
- Patch for dynlink.cma dependency for camlp4.
- Rebuild for OCaml 3.11.0.

* Wed Nov 26 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.0-3
- Rebuild for OCaml 3.11.0+rc1.

* Sun Aug 24 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.0-2
- +BR pcre-devel

* Mon Jul 28 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.0-1
- Initial RPM release.
