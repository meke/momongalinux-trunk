%global momorel 1

Summary: Utility for the creation of squashfs filesystems
Name: squashfs-tools
Version: 4.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://squashfs.sourceforge.net
Source0: http://dl.sourceforge.net/sourceforge/squashfs/squashfs%{version}.tar.gz
NoSource: 0
Patch0: squashfs-cflags.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel
BuildRequires: xz-devel
Obsoletes: squashfs
Provides: squashfs

%description
Squashfs is a highly compressed read-only filesystem for Linux.  This package
contains the utilities for manipulating squashfs filesystems.

%prep
%setup -q -n squashfs%{version}
#%patch0 -p1 -b .cflags

%build
pushd squashfs-tools
make  EXTRA_CFLAGS="%{optflags}" XZ_SUPPORT=1

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/sbin $RPM_BUILD_ROOT/usr/sbin
install -m 755 squashfs-tools/mksquashfs $RPM_BUILD_ROOT/sbin/mksquashfs
install -m 755 squashfs-tools/unsquashfs $RPM_BUILD_ROOT%{_sbindir}/unsquashfs

chmod -x README PERFORMANCE.README COPYING ACKNOWLEDGEMENTS CHANGES

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README PERFORMANCE.README COPYING ACKNOWLEDGEMENTS CHANGES
/sbin/mksquashfs
%{_sbindir}/unsquashfs

%changelog
* Thu May 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3-1m)
- update 4.3-release

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-1m)
- update 4.2-release
- support xz compression

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0-1m)
- update 4.0-release

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-2m)
- update Patch0 for fuzz=0

* Thu Dec 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4-1m)
- update

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3-3m)
- rename squashfs to squashfs-tools

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3-2m)
- rebuild against gcc43

* Sun Mar  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3-1m)
- update 3.3 + cvs-fix.patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2r2-2m)
- %%NoSource -> NoSource

* Sun Mar  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-1m)
- update to 3.2-r2

* Wed Oct 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-1m)
- update to 3.1-r2

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Sat Aug 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Thu Apr 28 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1r2-1m)
- create
- from Berry Linux unionfs.spec
- use /sbin
- chmod 0755

* Fri Mar 25 2005 Yuichiro Nakada <berry@po.yui.mine.nu>
- Create for Berry Linux
