%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-eyed3
Version:        0.6.17
Release:        %{momorel}m%{?dist}
Summary:        Python module for processing ID3 tags

Group:          Development/Languages
License:        GPLv2+
URL:            http://eyed3.nicfit.net/
Source0:        http://eyed3.nicfit.net/releases/eyeD3-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7


%description
A Python module and program for processing ID3 tags. Information about
mp3 files(i.e bit rate, sample frequency, play time, etc.) is also
provided. The formats supported are ID3 v1.0/v1.1 and v2.3/v2.4.


%prep
%setup -q -n eyeD3-%{version}


%build
%configure
# Make isn't necessary.

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# Remove dup & unnecessary docs
rm -f $RPM_BUILD_ROOT%{_datadir}/doc/eyeD3-%{version}/AUTHORS
rm -f $RPM_BUILD_ROOT%{_datadir}/doc/eyeD3-%{version}/COPYING.gz
rm -f $RPM_BUILD_ROOT%{_datadir}/doc/eyeD3-%{version}/ChangeLog.gz
rm -f $RPM_BUILD_ROOT%{_datadir}/doc/eyeD3-%{version}/README
rm -f $RPM_BUILD_ROOT%{_datadir}/doc/eyeD3-%{version}/THANKS

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{_bindir}/eyeD3
%{_mandir}/man1/*.1*
%{python_sitelib}/*


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.17-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.17-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.17-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.17-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.17-1m)
- update 0.6.17

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.16-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.16-2m)
- rebuild against python-2.6.1

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.16-1m)
- update to 0.6.16

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.12-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.12-2m)
- %%NoSource -> NoSource

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.12-1m)
- import to Momonga from Fedora Extras devel

* Sat Feb 24 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.6.12-1
- Update to 0.6.12.

* Fri Dec  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.6.11-3
- Change BR to python-devel.

* Fri Dec  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.6.11-2
- Rebuild against new python.

* Wed Nov 22 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.6.11-1
- Update to 0.6.11.

* Sat Oct  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.6.10-2
- Change BR to python.
- Remove unnecessary make in build section.

* Sat Oct  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.6.10-1
- Initial FE spec.

