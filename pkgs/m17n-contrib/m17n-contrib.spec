%global momorel 1
%global m17n_db_ver 1.6.4

Summary: Contributed input methods for m17n library
Name: m17n-contrib
Version: 1.1.14
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.nongnu.org/m17n/
Source0: http://ftp.twaren.net/Unix/NonGNU//m17n/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: bug433416-bn-probhat.patch
Patch1: as-inscript-keysummary-440201.patch
Patch2: ml-inscript-keysummary-435259.patch
Patch3: kn-inscript-ZWNJ-440007.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: m17n-db >= %{m17n_db_ver}
BuildRequires: m17n-db-devel >= %{m17n_db_ver}

%description
Contributed input methods for m17n library.

%prep
%setup -q

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/m17n/icons/*.png
%{_datadir}/m17n/scripts
%{_datadir}/m17n/*.mim

%changelog
* Tue Jul 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.14-1m)
- version 1.1.14

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.13-1m)
- version 1.1.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.11-2m)
- full rebuild for mo7 release

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-1m)
- version 1.1.11

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10-1m)
- version 1.1.10

* Wed Jun 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-2m)
- No NoSource

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-1m)
- version 1.1.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.8-1m)
- version 1.1.8

* Sat Jul  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-1m)
- version 1.1.7

* Wed May  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-1m)
- initial package for Momonga Linux
- import 4 patches from Fedora
