%global momorel 4

%global fontname stix
%global fontconf 61-%{fontname}

%global archivename STIXv%{version}

%global common_desc \
The mission of the Scientific and Technical Information Exchange (STIX) font \
creation project is the preparation of a comprehensive set of fonts that serve \
the scientific and engineering community in the process from manuscript \
creation through final publication, both in electronic and print formats.

Name:    %{fontname}-fonts
Version: 1.0.0
Release: %{momorel}m%{?dist}
Summary: STIX scientific and engineering fonts

Group:     User Interface/X
License:   "STIX"
URL:       http://www.stixfonts.org/
Source0:   %{archivename}.zip
Source1:   STIXFontLicense2010.txt
Source2:   stix-fonts-fontconfig.conf
Source3:   stix-fonts-pua-fontconfig.conf
Source4:   stix-fonts-integrals-fontconfig.conf
Source5:   stix-fonts-sizes-fontconfig.conf
Source6:   stix-fonts-variants-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem

%description
%common_desc

This package includes base Unicode fonts containing most glyphs for standard
use.


%package -n %{fontname}-pua-fonts
Summary:  STIX scientific and engineering fonts, PUA glyphs
Group:     User Interface/X
Requires: %{name} = %{version}-%{release}

Obsoletes: %{name}-pua < 0.9-10

%description -n %{fontname}-pua-fonts
%common_desc

This package includes fonts containing glyphs called out from the Unicode
Private Use Area (PUA) range. Glyphs in this range do not have an official
Unicode codepoint. They're generally accessible only through specialised
software. Text using them will break if they're ever accepted by the Unicode
Consortium and moved to an official codepoint.

%_font_pkg -n pua -f %{fontconf}-pua.conf STIXNonUni*otf


%package -n %{fontname}-integrals-fonts
Summary:  STIX scientific and engineering fonts, additional integral glyphs
Group:     User Interface/X
Requires: %{name} = %{version}-%{release}

Obsoletes: %{name}-integrals < 0.9-10

%description -n %{fontname}-integrals-fonts
%common_desc

This package includes fonts containing additional integrals of various size
and slant.

%_font_pkg -n integrals -f %{fontconf}-integrals.conf STIXInt*.otf


%package -n %{fontname}-sizes-fonts
Summary:  STIX scientific and engineering fonts, additional glyph sizes
Group:     User Interface/X
Requires: %{name} = %{version}-%{release}

Obsoletes: %{name}-sizes < 0.9-10

%description -n %{fontname}-sizes-fonts
%common_desc

This package includes fonts containing glyphs in additional sizes (Mostly
"fence" and "piece" glyphs).

%_font_pkg -n sizes -f %{fontconf}-sizes.conf STIXSiz*.otf


%package -n %{fontname}-variants-fonts
Summary:  STIX scientific and engineering fonts, additional glyph variants
Group:     User Interface/X
Requires: %{name} = %{version}-%{release}

Obsoletes: %{name}-variants < 0.9-10

%description -n %{fontname}-variants-fonts
%common_desc

This package includes fonts containing alternative variants of some glyphs.

%_font_pkg -n variants -f %{fontconf}-variants.conf STIXVar*otf


%prep
%setup -q -n %{archivename}
cp %{SOURCE1} .


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p Fonts/*.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf
install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pua.conf
install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-integrals.conf
install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-sizes.conf
install -m 0644 -p %{SOURCE6} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-variants.conf

for fconf in %{fontconf}.conf \
             %{fontconf}-pua.conf \
             %{fontconf}-integrals.conf \
             %{fontconf}-sizes.conf \
             %{fontconf}-variants.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf}.conf STIXGeneral*otf

%doc *.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 0.9-11
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Fri Jan 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 0.9-10
- Convert to new naming guidelines

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 0.9-9
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 0.9-8
- Rebuild using new rpm-fonts

* Fri Jul 11 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 0.9-7
- Fedora 10 alpha general package cleanup

* Thu Nov 1 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 0.9-6
- Add some fontconfig aliasing rules
- 0.9-4
- Initial experimental packaging
