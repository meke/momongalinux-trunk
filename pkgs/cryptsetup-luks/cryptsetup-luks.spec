%global momorel 1
%global srcname cryptsetup-%{version}

Summary: A utility for setting up encrypted filesystems
Name: cryptsetup-luks
Version: 1.6.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://cryptsetup.googlecode.com/
#Source0: http://cryptsetup.googlecode.com/files/%{srcname}.tar.bz2
Source0: https://www.kernel.org/pub/linux/utils/cryptsetup/v1.6/%{srcname}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgcrypt-devel >= 1.4.1-2m, popt-devel, device-mapper-devel
BuildRequires: libgpg-error-devel, libuuid-devel, libsepol-devel
BuildRequires: libselinux-devel
Provides: cryptsetup = %{version}-%{release}
Obsoletes: cryptsetup <= 0.1
Requires: cryptsetup-luks-libs = %{version}-%{release}

%description
This package contains cryptsetup, a utility for setting up
encrypted filesystems using Device Mapper and the dm-crypt target.

%package devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libgcrypt-devel > 1.1.42, device-mapper-devel, libuuid-devel
Requires: pkgconfig
Summary: Headers and libraries for using encrypted filesystems

%description devel
The cryptsetup-luks-devel package contain libraries and header files
used for writing code that makes use of encrypted filesystems.

%package libs
Summary: Cryptsetup shared library

%description libs
This package contains the cryptsetup shared library, libcryptsetup.

%prep
%setup -q -n %{srcname}

iconv -f latin1 -t utf8 ChangeLog > ChangeLog.new
mv -f ChangeLog.new ChangeLog 

%build
%configure
# remove rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf  %{buildroot}/%{_libdir}/*.la %{buildroot}/%{_libdir}/cryptsetup

%find_lang cryptsetup

%post -n cryptsetup-luks-libs -p /sbin/ldconfig

%postun -n cryptsetup-luks-libs -p /sbin/ldconfig

%files -f cryptsetup.lang
%defattr(-,root,root,-)
%doc COPYING ChangeLog AUTHORS TODO
%{_mandir}/man8/cryptsetup.8*
%{_mandir}/man8/veritysetup.8*
%{_sbindir}/cryptsetup
%{_sbindir}/veritysetup

%files devel
%defattr(-,root,root,-)
%{_includedir}/libcryptsetup.h
%{_libdir}/libcryptsetup.so
%{_libdir}/pkgconfig/libcryptsetup.pc

%files libs
%defattr(-,root,root,-)
%{_libdir}/libcryptsetup.so.*

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Fri Feb 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3-1m)
- update to 1.4.3

* Wed Aug 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-2m)
- add patch

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update to 1.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-0.1.1m)
- update to 1.1.0-rc2 based on Rawhide (1.1.0-0.3)
-- 
-- * Thu Oct 01 2009 Milan Broz <mbroz@redhat.com> - 1.1.0-0.3
-- - Update to cryptsetup 1.1.0-rc2
-- - Fix libcryptsetup to properly export only versioned symbols.
-- 
-- * Tue Sep 29 2009 Milan Broz <mbroz@redhat.com> - 1.1.0-0.2
-- - Update to cryptsetup 1.1.0-rc1
-- - Add luksHeaderBackup and luksHeaderRestore commands.
-- 
-- * Thu Sep 11 2009 Milan Broz <mbroz@redhat.com> - 1.1.0-0.1
-- - Update to new upstream testing version with new API interface.
-- - Add luksSuspend and luksResume commands.
-- - Introduce pkgconfig.
-- 
-- * Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-2
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Wed Jul 22 2009 Milan Broz <mbroz@redhat.com> - 1.0.7-1
-- - Update to upstream final release.
-- - Split libs subpackage.
-- - Remove rpath setting from cryptsetup binary.
-- 
-- * Wed Jul 15 2009 Till Maas <opensource@till.name> - 1.0.7-0.2
-- - update BR because of libuuid splitout from e2fsprogs
-- 
-- * Mon Jun 22 2009 Milan Broz <mbroz@redhat.com> - 1.0.7-0.1
-- - Update to new upstream 1.0.7-rc1.
-- 
-- - Wipe old fs headers to not confuse blkid (#468062)
-- * Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.6-7
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Thu Oct 30 2008 Milan Broz <mbroz@redhat.com> - 1.0.6-6
-- - Wipe old fs headers to not confuse blkid (#468062)
-- 
-- * Tue Sep 23 2008 Milan Broz <mbroz@redhat.com> - 1.0.6-5
-- - Change new project home page.
-- - Print more descriptive messages for initialization errors.
-- - Refresh patches to versions commited upstream.
-- 
-- * Sat Sep 06 2008 Milan Broz <mbroz@redhat.com> - 1.0.6-4
-- - Fix close of zero decriptor.
-- - Fix udevsettle delays - use temporary crypt device remapping.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against rpm-4.6

* Thu Jul 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6
- import two patches from Fedora devel (1.0.6-3)

* Sat Jun 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-5m)
- rebuild against libgcrypt

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-4m)
- rebuild against gcc43

* Tue Jul 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-3m)
- add autotools (libtool-1.5.24)

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-2m)
- delete libtool library

* Sun Sep 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.3-1m)
- import from fc-devel

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.0.3-2.1
- rebuild

* Wed Jun  7 2006 Jeremy Katz <katzj@redhat.com> - 1.0.3-2
- put shared libs in the right subpackages

* Fri Apr  7 2006 Bill Nottingham <notting@redhat.com> 1.0.3-1
- update to final 1.0.3

* Wed Feb 27 2006 Bill Nottingham <notting@redhat.com> 1.0.3-0.rc2
- update to 1.0.3rc2, fixes bug with HAL & encrypted devices (#182658)

* Wed Feb 22 2006 Bill Nottingham <notting@redhat.com> 1.0.3-0.rc1
- update to 1.0.3rc1, reverts changes to default encryption type

* Tue Feb 21 2006 Bill Nottingham <notting@redhat.com> 1.0.2-1
- update to 1.0.2, fix incompatiblity with old cryptsetup (#176726)

* Mon Feb 20 2006 Karsten Hopp <karsten@redhat.de> 1.0.1-5
- BuildRequires: libselinux-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-4.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-4.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Dec  5 2005 Bill Nottingham <notting@redhat.com> 1.0.1-4
- rebuild against new libdevmapper

* Thu Oct 13 2005 Florian La Roche <laroche@redhat.com>
- add -lsepol to rebuild on current fc5

* Mon Aug 22 2005 Karel Zak <kzak@redhat.com> 1.0.1-2
- fix cryptsetup help for isLuks action

* Fri Jul  1 2005 Bill Nottingham <notting@redhat.com> 1.0.1-1
- update to 1.0.1 - fixes incompatiblity with previous cryptsetup for
  piped passwords

* Thu Jun 16 2005 Bill Nottingham <notting@redhat.com> 1.0-2
- add patch for 32/64 bit compatibility (#160445, <redhat@paukstadt.de>)

* Tue Mar 29 2005 Bill Nottingham <notting@redhat.com> 1.0-1
- update to 1.0

* Thu Mar 10 2005 Bill Nottingham <notting@redhat.com> 0.993-1
- switch to cryptsetup-luks, for LUKS support

* Tue Oct 12 2004 Bill Nottingham <notting@redhat.com> 0.1-4
- oops, make that *everything* static (#129926)

* Tue Aug 31 2004 Bill Nottingham <notting@redhat.com> 0.1-3
- link some things static, move to /sbin (#129926)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Apr 16 2004 Bill Nottingham <notting@redhat.com> 0.1-1
- initial packaging
