%global momorel 2

Summary: An Open Source implementation of the GDI+ API
Name: libgdiplus
Version: 2.10.9
Release: %{momorel}m%{?dist}
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-freetype.patch
License: LGPL MPL
Group: System Environment/Libraries
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel >= 2.5.3
BuildRequires: libXrender-devel
BuildRequires: libpng-devel
BuildRequires: libexif-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: cairo libtiff libjpeg libungif libpng
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
An Open Source implementation of the GDI+ API

%package devel
Summary: An Open Source implementation of the GDI+ API for devel
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
An Open Source implementation of the GDI+ API for devel

%prep
%setup -q
%patch0 -p1 -b .freetype

%build
%configure \
    --enable-pdf \
    --enable-svg \
    --enable-png \
    --enable-glitz \
    --enable-xcb \
    --with-cairo=system \
    --with-pango \
    --disable-static \
    LIBS="-lglib-2.0 -lX11"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog LICENSE MPL-1.1.html NEWS README TODO
%{_libdir}/libgdiplus.so.*
%exclude %{_libdir}/libgdiplus.la

%files devel
%{_libdir}/libgdiplus.so
%{_libdir}/pkgconfig/libgdiplus.pc

%changelog
* Mon Apr 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.9-2m)
- enable to build with freetype-2.5.3

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.9-1m)
- update to 2.10.9

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-4m)
- rebuild for glib 2.33.2

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-3m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.7-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.7-1m)
- update to 2.6.7
- delete patch0

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-2m)
- add patch0 (build with libpng-1.2.44, but libpng-1.4.3 has released)

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-3m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-2m)
- delete __libtoolize hack

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-2m)
- rebuild against libjpeg-7

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.4-2m)
- define __libtoolize (build fix)

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- change source url (NoSource)

* Thu Aug 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update 2.0

* Sun Aug  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0106765-2m)
- no NoSource

* Sat Jul  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0106765-1m)
- update to 1.9.1.0106765 (svn version)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-2m)
- rebuild against gcc43

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sun Sep  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5
- delete patch (fixed)

* Mon Jul 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m) 
- apply Patch0: libgdiplus-1.2.4-cairo-ctype.patch

* Wed May 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Fri Feb  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Thu Dec 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Fri Nov  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Tue Oct 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.18-1m)
- update to 1.1.18

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.1.16.1-2m)
- rebuild against expat-2.0.0-1m

* Mon Aug 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.16.1-1m)
- update to 1.1.16.1

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.15-2m)
- rebuild against monodoc

* Mon Apr 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-1m)
- update to 1.1.15

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.14-1m)
- update to 1.1.14 (1.2 beta)

* Fri Jan 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13.2-1m)
- Start
