%global momorel 1

%global xfce4ver 4.11.0
%global major 0.7

Summary:	XKB layout switcher for the Xfce panel
Name: 		xfce4-xkb-plugin
Version: 	0.7.0
Release:	%{momorel}m%{?dist}

Group: 		User Interface/Desktops
License:	BSD
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, perl-XML-Parser
BuildRequires:  gtk2-devel
BuildRequires:  libxml2-devel
#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxklavier-devel >= 5.0
BuildRequires:  pkgconfig
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  libxfce4ui-devel
BuildRequires:	xorg-x11-proto-devel
Requires:	xfce4-panel >= %{xfce4ver}

%description 
Xfce XKB layout switch plugin for the Xfce panel. It displays the current 
keyboard layout, and refreshes when layout changes. The layout can be 
switched by simply clicking on the plugin. For now the keyboard layouts 
cannot be configured from the plugin itself, they should be set in the 
XF86Config file or some other way (e.g. setxkbmap).

%prep
%setup -q

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'
find %{buildroot} -name "*.la" -delete
%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%dir %{_datadir}/xfce4/xkb/
%dir %{_datadir}/xfce4/xkb/flags
%{_datadir}/xfce4/xkb/flags/*.svg


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0
- build against xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.4.3-1m)
- update
- build against xfce4-4.10.0

* Tue Aug 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4.1-2m)
- add BuildRequires

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4.1-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3.3-5m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3.3-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3.3-1m)
- update
- rebuild against xfce4-4.6.2

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3.2-6m)
- rebuild against libxklavier-5.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3.2-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3.2-3m)
- rebuild against libxklavier-4.0
-- add patch0

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3.2-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3.2-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-6m)
- rebuild against rpm-4.6

* Thu Sep 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3-5m)
- correct filenames

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-4m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-1m)
- upda to 0.4.3
- rebuild against xfce4 4.4.0
- delete patch0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-2m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.5-1m)
- update to 0.3.5

* Sat Jul  2 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.2-7m)
- enable x86_64.
- add patch0(xfce4-xkb-plugin-0.3.2-x86_64.patch)
- revised spec

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-6m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-5m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-4m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-3m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-2m)
- rebuild against xfce4 4.1.90

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-1m)

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-2m)
- rebuild against xfce4-4.0.6

* Sun Jun 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.1-1m)
- import to Momonga
