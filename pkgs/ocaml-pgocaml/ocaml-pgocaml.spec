%global momorel 6
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-pgocaml
Version:        1.4
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for type-safe access to PostgreSQL databases

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://developer.berlios.de/projects/pgocaml/
Source0:        http://download.berlios.de/pgocaml/pgocaml-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel, ocaml-ocamldoc
BuildRequires:  ocaml-extlib-devel >= 1.5.2-1m
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m, pcre-devel >= 8.31
BuildRequires:  ocaml-calendar-devel >= 2.03.1-1m
BuildRequires:  ocaml-csv-devel >= 1.1.7-8m
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-deriving-devel >= 0.1.1a-10m

%global __ocaml_requires_opts -i Asttypes -i Calendar_builder -i Calendar_sig -i Date -i Date_sig -i Fcalendar -i Ftime -i Parsetree -i Period -i Printer -i Time -i Time_sig -i Time_Zone -i Utils -i Version-i GtkSourceView_types -i GtkSourceView2_types

%description
PG'OCaml is a type-safe, simple interface to PostgreSQL from OCaml. It
lets you embed SQL statements directly into OCaml code.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n pgocaml-%{version}


%build
make depend
make all
make doc
strip pgocaml_prof


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR
ocamlfind install pgocaml META *.mli *.cmi *.cmx *.cma *.cmxa *.a pa_*.cmo

mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m 0755 pgocaml_prof $RPM_BUILD_ROOT%{_bindir}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/ocaml/pgocaml
%if %opt
%exclude %{_libdir}/ocaml/pgocaml/*.a
%exclude %{_libdir}/ocaml/pgocaml/*.cmxa
%exclude %{_libdir}/ocaml/pgocaml/*.cmx
%endif
%exclude %{_libdir}/ocaml/pgocaml/*.mli
%{_bindir}/pgocaml_prof


%files devel
%defattr(-,root,root,-)
%doc README.txt README.profiling BUGS.txt CONTRIBUTORS.txt COPYING.LIB HOW_IT_WORKS.txt html/*
%if %opt
%{_libdir}/ocaml/pgocaml/*.a
%{_libdir}/ocaml/pgocaml/*.cmxa
%{_libdir}/ocaml/pgocaml/*.cmx
%endif
%{_libdir}/ocaml/pgocaml/*.mli


%changelog
* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-6m)
- rebuild against pcre-8.31

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-5m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- update to 1.4
- rebuild against the following packages
-- ocaml-3.11.2
-- ocaml-calendar-2.02
-- ocaml-csv-1.1.7-4m
-- ocaml-extlib-1.5.1-5m
-- ocaml-pcre-6.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against ocaml-pcre-6.0.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- import Fedora devel changes (1.1-4, 1.1-3)
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1-1
- New upstream release 1.1.
- Clarify license is LGPLv2+ with exceptions.
- New home page and download URL.

* Mon Mar  3 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9-5
- Ignore modules which are really submodules of CalendarLib.

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9-4
- Add missing BR for ocaml-camlp4-devel.
- Add missing BR for pcre-devel.
- Check it builds in mock.

* Sat Feb 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9-3
- Check it builds with OCaml 3.10.1
- Only keep license file in main package.
- Clarify license is LGPLv2 with exceptions.

* Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9-2
- Added the syntax extension.

* Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9-1
- Initial RPM release.
