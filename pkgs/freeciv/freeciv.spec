%global momorel 1

Summary: A Free and Open Source empire-building strategy game
Name: freeciv
Version: 2.3.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Amusements/Games
URL: http://freeciv.wikia.com/wiki/Main_Page
Source0: http://dl.sourceforge.net/project/freeciv/Freeciv%202.3/%{version}/freeciv-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel
BuildRequires: SDL_mixer-devel
BuildRequires: alsa-lib-devel
BuildRequires: esound-devel >= 0.2.29-2m
BuildRequires: freetype-devel
BuildRequires: gtk2-devel
BuildRequires: imlib2-devel
BuildRequires: readline-devel
BuildRequires: libbind-devel >= 6.0
BuildRequires: ggz-client-libs
BuildRequires: ggz-gtk-client-devel
Obsoletes: freeciv-client freeciv-server

%description
FreeCiv is an implementation of Civilization II for UNIX/X

%prep
%setup -q

%build
%{configure} --disable-static
%{make}

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# remove ggz.modules
rm -f %{buildroot}%{_sysconfdir}/ggz.modules
cp data/civclient.dsc %{buildroot}%{_datadir}/freeciv

rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la

%find_lang %{name}

%clean 
rm -rf --preserve-root %{buildroot}

%post
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
/usr/bin/ggz-config --install --force --modfile=%{_datadir}/freeciv/civclient.dsc

%preun
/usr/bin/ggz-config --remove --modfile=%{_datadir}/freeciv/civclient.dsc

%files -f %{name}.lang
%defattr(-,root,root,)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README doc
%{_bindir}/freeciv-gtk2
%{_bindir}/freeciv-manual
%{_bindir}/freeciv-modpack
%{_bindir}/freeciv-server
%{_datadir}/applications/freeciv.desktop
%{_datadir}/applications/freeciv-server.desktop
%dir %{_datadir}/freeciv
%{_datadir}/freeciv/*
%{_datadir}/applications/freeciv-modpack.desktop
%{_datadir}/icons/hicolor/*/apps/freeciv-client.png
%{_datadir}/icons/hicolor/*/apps/freeciv-modpack.png
%{_datadir}/icons/hicolor/*/apps/freeciv-server.png
%{_datadir}/pixmaps/freeciv-client.png
%{_mandir}/man6/freeciv-client.6*
%{_mandir}/man6/freeciv-ftwl.6*
%{_mandir}/man6/freeciv-gtk2.6*
%{_mandir}/man6/freeciv-modpack.6*
%{_mandir}/man6/freeciv-sdl.6*
%{_mandir}/man6/freeciv-server.6*
%{_mandir}/man6/freeciv-win32.6*
%{_mandir}/man6/freeciv-xaw.6*

%changelog
* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.5-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.5-1m)
- update to 2.2.5

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.4-1m)
- update to 2.2.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-2m)
- full rebuild for mo7 release

* Fri Jun 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.1-1m)
- [SECURITY] CVE-2010-2445
- update to 2.2.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.9-4m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.9-2m)
- rebuild against libbind-6.0
- add --force to ggz-config at %%post

* Tue Apr  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.9-1m)
- update to 2.1.9
--please change Source0 URL

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.5-2m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.5-1m)
- update to 2.1.5

* Fri Apr 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.4-1m)
- update to 2.1.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-3m)
- rebuild against gcc43

* Sat Mar 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-2m)
- delete -D and --force options from %%post script

* Tue Jan 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Sun Jan 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-2m)
- ggz.modules updates %%post and %%preun
 
* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Sun Dec  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.1-3m)
- rebuild against bind for libbind, maybe work now

* Sat Dec  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.1-2m)
- clean up spec
-- but does not work (civserver)

* Mon Dec  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Sat Nov  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Fri Jun 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.9-1m)
- version 2.0.9
- [SECURITY] CVE-2006-3913
- import icons from cooker
- clean up spec file

* Fri Dec 12 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.14.1-1m)
- update to 1.14.1

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.14.0-4m)
  rebuild against DirectFB 0.9.18
  
* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.14.0-3m)
- rebuild against for XFree86-4.3.0

* Wed Mar  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.14.0-2m)
- remove exec prefix

* Tue Mar  4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.0-1m)
- update to 1.14.0

* Wed Aug 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.13.0-2m)
- rebuild against esound-	0.2.29-2m

* Tue Jul 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.13.0-1m)
- version 1.13.0

* Wed Jan  2 2002 Shingo Akagaki <dora@kondara.org>
- (1.12.0-2k)
- version 1.12.0

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.11.4-12k)
- nigittenu

* Sun Nov 11 2001 Motonobu Ichimura <famao@kondara.org>
- (1.11.4-10k)
- modify ja.po (euc-japan -> EUC-JP)

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.11.4-8k)
- rebuild against gettext 0.10.40.

* Sun May 13 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.11.4-6k)
- modified directory typo name correctly afaterstep to afterstep

* Thu Mar 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix source URL

* Wed Mar 28 2001 Toru Hoshina <toru@df-usa.com>
- [1.11.4-3k]

* Wed Mar 21 2001 Motonobu Ichimura <famao@kondara.org>
- changed GroupName from X11/Game -> Amusements/Games ;-<

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.11.0-11k)
- added freeciv.as for afterstep

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.11.0-9k)
- imported freeciv.desktop

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<
- fixed defattr problem ;-<

* Thu Jul 06 2000 KUSUNOKI Masanori <nori@kondara.org>
- Updated package to 1.11.0

* Thu Oct 21 1999  Hugo van der Kooij <hvdkooij@caiw.nl>
- Updated package to 1.9.0

* Tue Aug 03 1999 Hugo van der Kooij <hvdkooij@caiw.nl>
- Updated package to 1.8.1

* Wed Apr 21 1999 Hugo van der Kooij <hvdkooij@caiw.nl>
- Updated package to 1.8.0

* Sat Dec 26 1998 Hugo van der Kooij <hvdkooij@caiw.nl>
- Updated package to 1.7.2
- Split with server and client part

* Mon Dec 14 1998 Hugo van der Kooij <hvdkooij@caiw.nl>
- Fixed the place where the files are stored to be FHS 2.0 compliant.
- Made the lot RHCN complient.

* Tue Oct 6 1998 Michael Maher <mike@redhat.com>
- updated package

* Tue Aug 4 1998 Michael Maher <mike@redhat.com>
- built package                                                  
