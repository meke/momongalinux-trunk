%global momorel 5
%global dirname 52078836
%global qtver 4.7.0

Summary: Dock any application in the system tray
Name: kdocker
Version: 4.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: https://launchpad.net/kdocker
Source0: http://launchpadlibrarian.net/%{dirname}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-4.2.2.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: desktop-file-utils
BuildRequires: libXmu-devel
BuildRequires: libXpm-devel

%description
KDocker will help you dock any application in the system tray. This means you
can dock openoffice, xmms, firefox, thunderbolt, eclipse, anything! Just point
and click. Works for both KDE and GNOME (In fact it should work for most modern
window managers that support NET WM Specification. I believe it works for XFCE,
for instance)

All you need to do is start KDocker and select an application using the mouse
and lo! the application gets docked into the system tray. The application can 
also be made to dissappear from the task bar.

KDocker supports the KDE System Tray Protocol and the System Tray Protocol from
freedesktop.org

Very few apps have docking capabilities (e.g. Yahoo! and XMMS don't have any). 
Even if they do, sometimes they are specific to desktops (working on KDE but 
not on GNOME, and vice versa). KDocker will help you dock any application in 
the system tray. This means you can dock OpenOffice.org, XMMS, Firefox, 
Thunderbird, etc. Just point and click. It works for KDE, GNOME, XFCE, and 
probably many more.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
qmake-qt4

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

# convert and install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
convert %{buildroot}%{_datadir}/%{name}/icons/%{name}.png \
  -sample 16x16 %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert %{buildroot}%{_datadir}/%{name}/icons/%{name}.png \
  -sample 22x22 %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
convert %{buildroot}%{_datadir}/%{name}/icons/%{name}.png \
  -sample 32x32 %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert %{buildroot}%{_datadir}/%{name}/icons/%{name}.png \
  -sample 48x48 %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert %{buildroot}%{_datadir}/%{name}/icons/%{name}.png \
  -sample 64x64 %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
install -m 644 %{buildroot}%{_datadir}/%{name}/icons/%{name}.png \
  %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category X-KDE-Utilities-Desktop \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS BUGS COPYING CREDITS ChangeLog INSTALL README TODO
%config(noreplace) %{_sysconfdir}/bash_completion.d/%{name}
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4-4m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4-1m)
- version 4.4
- remove merged momonga.patch

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3-3m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3-2m)
- touch up spec file

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3-1m)
- version 4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2.1-1m)
- version 4.2.2.1
- update desktop.patch and momonga.patch

* Sat Sep 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1-1m)
- version 4.1
- update License and URL
- update desktop.patch and momonga.patch
- good-bye Qt3

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3-6m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-5m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-1m)
- initial package for Momonga Linux
