%global         momorel 1

Summary:        Handling page faults in user mode
Name:           libsigsegv
Version:        2.10
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          System Environment/Libraries
URL:            http://libsigsegv.sourceforge.net/
Source0:        http://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{version}.tar.gz 
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a library for handling page faults in user mode. A page fault
occurs when a program tries to access to a region of memory that is
currently not available. Catching and handling a page fault is a useful
technique for implementing:
  - pageable virtual memory,
  - memory-mapped access to persistent databases,
  - generational garbage collectors,
  - stack overflow handlers,
  - distributed shared memory,

%package devel
Summary: Development libraries and header files for %{name}
Group:   Development/Libraries
Requires: %{name} = %{version}-%{release}
%description devel
Libraries and header files for %{name} development.

%prep
%setup -q

%build
CFLAGS=`echo %{optflags} | sed -e 's/-Wp,-D_FORTIFY_SOURCE=2//'`
%configure --enable-shared --enable-static

%make
make check

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# delete .la file
rm -rf %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog* INSTALL NEWS
%doc PORTING README*
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*.h
%{_libdir}/lib*.so
%{_libdir}/lib*.a

%changelog
* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-1m)
- update to 2.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8-2m)
- full rebuild for mo7 release

* Sun Dec 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-3m)
- drop -Wp,-D_FORTIFY_SOURCE=2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-2m)
- rebuild against rpm-4.6

* Wed Oct 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Sat May 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5-1m)
- update to 2.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- %%NoSource -> NoSource

* Sun Dec 31 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2-1m)
- Initial build for Momonga Linux 3 (for clisp-2.38)
