%global momorel 3
%define tarball xf86-video-vmware
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 vmware video driver
Name:      xorg-x11-drv-vmware
Version:   13.0.0
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
Patch0: xatracker-v2-fixes.patch
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64

BuildRequires: pkgconfig
BuildRequires: xatracker-devel >= 10.0.2
BuildRequires: xorg-x11-server-devel >= 1.13.0
Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 vmware video driver.

%prep
%setup -q -n %{tarball}-%{version}

%patch0 -p1 -b .xatracker

%build
# autoreconf -ivf
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/vmware_drv.so
#%{driverdir}/vmwlegacy_drv.so
%{_mandir}/man4/vmware.4*

%changelog
* Sun Jan 26 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (13.0.0-3m)
- [BUILD FIX] import a patch from Arch Linux

* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (13.0.0-2m)
- rebuild against xatracker-10.0.2

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (13.0.0-1m)
- update 13.0.0

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.2-1m)
- rebuild against xorg-x11-server-1.13

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.2-1m)
- update 12.0.2

* Fri Mar 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.0-1m)
- update 12.0.0

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.3-6m)
- version down 11.0.3
- kvm-vmvga driver was broken?

* Sat Jan 21 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.1.0-2m)
- add vgahw patch
-- maybe run X server...

* Thu Jan 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.1.0-1m)
- update 11.1.0
-- better support xorg-server-1.12

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.3-5m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.3-4m)
- rebuild against xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.0.3-3m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (11.0.3-2m)
- rebuild against xorg-x11-server-1.9.3

* Mon Dec 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.3-1m)
- update 11.0.3
- fixed crash bug

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.0.2-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.2-1m)
- update 11.0.2
-- fix "Xorg -configure" crash

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.1-2m)
- full rebuild for mo7 release

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.1-1m)
- update 11.0.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.16.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.16.8-1m)
- update 10.16.8

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.16.6-1m)
- update 10.16.6

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.16.5-3m)
- rebuild against xorg-x11-server 1.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.16.5-2m)
- rebuild against rpm-4.6

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.16.5-1m)
- update 10.16.5

* Fri Jul 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.16.4-1m)
- update 10.16.4

* Sun Jul 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.16.3-1m)
- update 10.16.3

* Fri Jun 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (10.16.2-1m)
- update 10.16.2

* Sun May  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (10.16.1-1m)
- update 10.16.1
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.15.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (10.15.2-2m)
- %%NoSource -> NoSource

* Wed Oct  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.15.2-1m)
- update to 10.15.2

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.15.1-1m)
- update to 10.15.1

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (10.15.0-2m)
- rebuild against xorg-x11-server-1.4

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.15.0-1m)
- update to 10.15.0

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.14.1-1m)
- update to 10.14.1

* Sun Nov 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.14.0-1m)
- update to 10.14.0

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.13.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (10.13.0-1m)
- update 10.13.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.11.1.3-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.11.1.3-1.1m)
- import to Momonga

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 10.11.1.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 10.11.1.3-1
- Updated xorg-x11-drv-vmware to version 10.11.1.3 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 10.11.1.2-1
- Updated xorg-x11-drv-vmware to version 10.11.1.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 10.11.1-1
- Updated xorg-x11-drv-vmware to version 10.11.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 10.11.0.1-1
- Updated xorg-x11-drv-vmware to version 10.11.0.1 from X11R7 RC1
- Fix *.la file removal.

* Tue Oct 4 2005 Mike A. Harris <mharris@redhat.com> 10.10.2-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86, x86_64, ia64

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 10.10.2-0
- Initial spec file for vmware video driver generated automatically
  by my xorg-driverspecgen script.
