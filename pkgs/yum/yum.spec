%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%global momorel 20

%global move_yum_conf_back 1
%global auto_sitelib 1
%global disable_check 0

# We always used /usr/lib here, even on 64bit ... so it's a bit meh.
%global yum_pluginslib   /usr/lib/yum-plugins
%global yum_pluginsshare /usr/share/yum-plugins

# disable broken /usr/lib/rpm/brp-python-bytecompile
%define __os_install_post %{nil}
%define compdir %(pkg-config --variable=completionsdir bash-completion)
%if "%{compdir}" == ""
%define compdir "/etc/bash_completion.d"
%endif

Summary: RPM installer/updater
Name: yum
Version: 3.4.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
Source0: http://linux.duke.edu/projects/yum/download/3.4/%{name}-%{version}.tar.gz
NoSource: 0
Source1: yum.conf.momonga
Source2: yum-updatesd.conf.momonga
Source5: yum-updatesd-0.9-20090115.tar.bz2
Patch1: yum-distro-configs.patch
Patch5: geode-arch.patch
Patch6: yum-HEAD.patch
Patch7: yum-ppc64-preferred.patch
Patch20: yum-manpage-files.patch
Patch21: yum-completion-helper.patch

Patch100: yum-3.2.27-remove-updatesd.patch
Patch101: yum-3.2.27-fix-pungi.patch

Source99: plugin.conf
# default plugins here
Source100: installonlyn.py

Patch10: yum-3.4.3-momonga.patch
Patch11: yum-3.1.3-lib64.patch

URL: http://linux.duke.edu/yum/
BuildArchitectures: noarch
BuildRequires: python
BuildRequires: gettext
BuildRequires: sed
Conflicts: pirut < 1.1.4
Requires: python >= 2.7, rpm-python, rpm >= 0:4.4.2
Requires: urlgrabber >= 3.9
Requires: yum-metadata-parser 
Requires: pygpgme
Requires: python-iniparse
Requires: dbus-python
Requires: pygobject
Requires: python-pycurl
Obsoletes: yum-skip-broken, yum-basearchonly
Obsoletes: yum-updatesd

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Yum is a utility that can check for and automatically download and
install updated RPM packages. Dependencies are obtained and downloaded 
automatically prompting the user as necessary.

%package cron
Summary: Files needed to run yum updates as a cron job
Group: System Environment/Base
Requires: yum >= 3.4.3-18m cronie crontabs findutils
BuildRequires: systemd-units
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description cron
These are the files needed to run yum updates as a cron job.

%package cron-daily
Summary: Files needed to run yum updates as a daily cron job
Group: System Environment/Base
Provides: yum-cron-BE = %{version}-%{release}
Requires: yum-cron > 3.4.3-18m

%description cron-daily
This is the configuration file for the daily yum-cron update service, which
lives %{_sysconfdir}/yum/yum-cron.conf.
Install this package if you want auto yum updates nightly via cron (or something
else, via. changing the configuration).
By default this just downloads updates and does not apply them.

%package cron-hourly
Summary: Files needed to run yum updates as an hourly cron job
Group: System Environment/Base
Provides: yum-cron-BE = %{version}-%{release}
Requires: yum-cron > 3.4.3-18m

%description cron-hourly
This is the configuration file for the daily yum-cron update service, which
lives %{_sysconfdir}/yum/yum-cron-hourly.conf.
Install this package if you want automatic yum metadata updates hourly via
cron (or something else, via. changing the configuration).

%package cron-security
Summary: Files needed to run security yum updates as once a day
Group: System Environment/Base
Provides: yum-cron-BE = %{version}-%{release}
Requires: yum-cron > 3.4.3-18m

%description cron-security
This is the configuration file for the security yum-cron update service, which
lives here: %{_sysconfdir}/yum/yum-cron-security.conf
Install this package if you want automatic yum security updates once a day
via. cron (or something else, via. changing the configuration -- this will be
confusing if it's not security updates anymore though).
By default this will download and _apply_ the security updates, unlike
yum-cron-daily which will just download all updates by default.
This runs after yum-cron-daily, if that is installed.

%prep
%setup -q -a 5

%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch20 -p1
%patch21 -p1
%patch1 -p1

# Momonga Patch
%patch10 -p1 -b .momonga
%patch11 -p1

%build
make

%install
rm -rf $RPM_BUILD_ROOT

INIT=systemd

make DESTDIR=$RPM_BUILD_ROOT UNITDIR=%{_unitdir} INIT=$INIT install

install -m 644 %{SOURCE1} $RPM_BUILD_ROOT/%{_sysconfdir}/yum.conf
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/yum/pluginconf.d $RPM_BUILD_ROOT/%{yum_pluginslib}
mkdir -p $RPM_BUILD_ROOT/%{yum_pluginsshare}

%if %{move_yum_conf_back}
# for now, move repodir/yum.conf back
mv $RPM_BUILD_ROOT/%{_sysconfdir}/yum/repos.d $RPM_BUILD_ROOT/%{_sysconfdir}/yum.repos.d
rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/yum/yum.conf
%endif

# yum-updatesd has moved to the separate source version
rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/yum/yum-updatesd.conf 
rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/init.d/yum-updatesd
rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/dbus-1/system.d/yum-updatesd.conf
rm -f $RPM_BUILD_ROOT/%{_sbindir}/yum-updatesd
rm -f $RPM_BUILD_ROOT/%{_mandir}/man*/yum-updatesd*
rm -f $RPM_BUILD_ROOT/%{_datadir}/yum-cli/yumupd.py*

# Ghost files:
mkdir -p $RPM_BUILD_ROOT/var/lib/yum/history
mkdir -p $RPM_BUILD_ROOT/var/lib/yum/plugins
mkdir -p $RPM_BUILD_ROOT/var/lib/yum/yumdb
touch $RPM_BUILD_ROOT/var/lib/yum/uuid

# rpmlint bogus stuff...
chmod +x $RPM_BUILD_ROOT/%{_datadir}/yum-cli/*.py
chmod +x $RPM_BUILD_ROOT/%{python_sitelib}/yum/*.py
chmod +x $RPM_BUILD_ROOT/%{python_sitelib}/rpmUtils/*.py

%find_lang %name

# Remove the yum-cron sysV stuff to make rpmbuild happy..
rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/init.d/yum-cron

rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/init.d/yum-updatesd

cp -a etc/yum-makecache.service $RPM_BUILD_ROOT/%{_unitdir}
cp -a etc/yum-makecache.timer   $RPM_BUILD_ROOT/%{_unitdir}

%clean
rm -rf $RPM_BUILD_ROOT

%post cron
#systemd_post yum-cron.service
#  Do this manually because it's a fake service for a cronjob, and cronjobs
# are default on atm. This may change in the future.
if [ $1 = 1 ]; then
 systemctl enable yum-cron >/dev/null 2>&1
else
#  Note that systemctl preset is being run here ... but _only_ on initial
# install. So try this...

if [ -f /var/lock/subsys/yum-cron -a -f /etc/rc.d/init.d/yum-cron ]; then
 systemctl enable yum-cron >/dev/null 2>&1
fi
fi
# Also note:
#  systemctl list-unit-files | fgrep yum-cron

%preun cron
%systemd_preun yum-cron.service

%postun cron
%systemd_postun_with_restart yum-cron.service

%post
%systemd_post yum-makecache.timer

%preun
%systemd_preun yum-makecache.timer

%postun
%systemd_postun_with_restart yum-makecache.timer

%files -f %{name}.lang
%defattr(-, root, root, -)
%doc README AUTHORS COPYING TODO ChangeLog PLUGINS
%if %{move_yum_conf_back}
%config(noreplace) %{_sysconfdir}/yum.conf
%dir %{_sysconfdir}/yum.repos.d
%else
%config(noreplace) %{_sysconfdir}/yum/yum.conf
%dir %{_sysconfdir}/yum/repos.d
%endif
%config(noreplace) %{_sysconfdir}/yum/version-groups.conf
%dir %{_sysconfdir}/yum
%dir %{_sysconfdir}/yum/protected.d
%dir %{_sysconfdir}/yum/fssnap.d
%dir %{_sysconfdir}/yum/vars
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%(dirname %{compdir})
%dir %{_datadir}/yum-cli
%{_datadir}/yum-cli/*
%exclude %{_datadir}/yum-cli/completion-helper.py?
%{_bindir}/yum
%{python_sitelib}/yum
%{python_sitelib}/rpmUtils
%dir /var/cache/yum
%dir /var/lib/yum
%ghost /var/lib/yum/uuid
%ghost /var/lib/yum/history
%ghost /var/lib/yum/plugins
%ghost /var/lib/yum/yumdb
%{_mandir}/man*/yum.*
%{_mandir}/man*/yum-shell*
# plugin stuff
%dir %{_sysconfdir}/yum/pluginconf.d
%dir %{yum_pluginslib}
%dir %{yum_pluginsshare}
%{_unitdir}/yum-makecache.service
%{_unitdir}/yum-makecache.timer

%files cron
%defattr(-,root,root)
%doc COPYING
%{_sysconfdir}/cron.daily/0yum-daily.cron
%{_sysconfdir}/cron.hourly/0yum-hourly.cron
%config(noreplace) %{_sysconfdir}/yum/yum-cron.conf
%config(noreplace) %{_sysconfdir}/yum/yum-cron-hourly.conf
%{_unitdir}/yum-cron.service
%{_sbindir}/yum-cron
%{_mandir}/man*/yum-cron.*

%files cron-daily
%defattr(-,root,root)
%{_sysconfdir}/cron.daily/0yum-daily.cron
%config(noreplace) %{_sysconfdir}/yum/yum-cron.conf

%files cron-hourly
%defattr(-,root,root)
%{_sysconfdir}/cron.hourly/0yum-hourly.cron
%config(noreplace) %{_sysconfdir}/yum/yum-cron-hourly.conf

%files cron-security
%defattr(-,root,root)
%{_sysconfdir}/cron.daily/0yum-security.cron
%config(noreplace) %{_sysconfdir}/yum/yum-cron-security.conf

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-20m)
- update yum-HEAD.patch

* Wed May 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-19m)
- update yum-HEAD.patch

* Tue Nov 12 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-18m)
- update yum-HEAD.patch

* Thu Nov  7 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.4.3-17m)
- remove non-existent yum-downloadonly requirement for yum-cron
- add Patch9: BZ-922992-move-bash-completion-scripts.patch

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-16m)
- update yum-HEAD.patch

* Thu Sep 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-15m)
- update yum-HEAD.patch

* Mon Jul 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-14m)
- update yum-HEAD.patch
-- many bugfix 

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-13m)
- update yum-HEAD.patch
-- fixed downgrade bug

* Fri Mar  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-12m)
- fix build error
-- remake any patches

* Thu Mar  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-11m)
- update yum-HEAD.patch
- fix BTS URL

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-10m)
- update yum-HEAD.patch

* Mon Dec 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-9m)
- update yum-HEAD.patch

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-8m)
- update yum-HEAD.patch
-- remove RHBZ 737826 patch. fix anaconda crash.

* Sun Oct  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-7m)
- add patch

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-6m)
- update yum-HEAD.patch

* Mon Sep 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-5m)
- update yum-HEAD.patch

* Mon Aug 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-4m)
- update yum-HEAD.patch

* Fri Aug  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-3m)
- update yum-HEAD.patch

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-2m)
- update yum-HEAD.patch

* Fri Jul  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-1m)
- update 3.4.3

* Fri Jun 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.2-1m)
- update 3.4.2

* Fri May 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.1-1m)
- update 3.4.1

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.0-4m)
- rebuild against python-2.7

* Wed Apr 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- move yum-cron to _initscriptdir

* Wed Apr 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-2m)
- modify Requires

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.0-1m)
- update 3.4.0

* Wed Apr 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.29-1m)
- update 3.2.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.28-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.28-7m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.28-6m)
- update yum-HEAD.patch.

* Sun Nov  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.28-5m)
- update yum-HEAD.patch.

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.28-4m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.28-3m)
- add yum-HEAD.patch. import from fedora13

* Sun Aug 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.28-3m)
- add Requires:

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.28-2m)
- exclude xvid usoxvid in yum.conf

* Sun Aug  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.28-1m)
- update 3.2.28

* Thu Jul 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.27-6m)
- update yum-HEAD patch

* Sun Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.27-5m)
- update yum-HEAD patch

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.27-4m)
- update yum-HEAD patch

* Mon Jun 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.27-3m)
- update yum-HEAD patch

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.27-2m)
- update yum-HEAD patch

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.27-1m)
- update 3.2.27

* Thu Mar 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.26-1m)
- update 3.2.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.25-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.25-1m)
- update 3.2.25

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.24-1m)
- update 3.2.24

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.23-2m)
- update yum-updatesd

* Thu Jun  4 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.23-1m)
- update 3.2.23

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.22-1m)
- update 3.2.22

* Sun Mar 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.21-2m)
- update yum-HEAD.patch(import from yum-3_2_21-16_fc11)

* Sun Feb  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.21-1m)
- update 3.2.21

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.20-6m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.20-5m)
- update yum-3_2_X HEAD patch

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.20-4m)
- update Patch10 for fuzz=0
- License: GPLv2+

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.20-3m)
- rebuild against python-2.6.1-1m

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.20-2m)
- import yum-HEAD patch

* Tue Nov 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.20-1m)
- update 3.2.20

* Thu Oct 16 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.19-1m)
- update 3.2.19

* Sun Sep 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.17-3m)
- modify Requires: dbus-python pygobject

* Wed Aug  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.17-2m)
- add Requires: python-iniparse

* Sat Jul 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.17-1m)
- update 3.2.17

* Sat Jun  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.16-4m)
- modify yum-3.2-momonga.patch

* Sat Jun  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.16-3m)
- remake yum-3.2-momonga.patch

* Tue May 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.16-2m)
- add 0001-gotta-check-both-not-either-otherwise-filedeps-do.patch

* Sat May 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.16-1m)
- update 3.2.16

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.14-2m)
- update yum-big-head.patch

* Sun Apr 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.14-1m)
- update 3.2.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.10-3m)
- rebuild against gcc43

* Fri Jan 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.10-2m)
- add Req: pygpgme

* Fri Jan 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.10-1m)
- update 3.2.10

* Tue Dec  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.8-1m)
- update 3.2.8

* Tue Nov 20 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.7-2m)
- drop Patch0: yum-showasdep.patch, applied upstream

* Mon Nov 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.7-1m)
- update 3.2.7

* Sun Oct 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.6-1m)
- update 3.2.6

* Wed Sep 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.5-1m)
- update 3.2.5

* Wed Aug 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.4-1m)
- update 3.2.4

* Fri Aug 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.3-1m)
- update 3.2.3

* Mon Aug 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.2-1m)
- update 3.2.2

* Tue Jun 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-2m)
- rewind to version 3.2.0 for pungi and anaconda
  installer doesn't work with yum-3.2.1
  I lost 10 DVDs...

* Fri Jun 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.1-1m)
- update 3.2.1

* Sat May 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-1m)
- update 3.2.0

* Sat Apr 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.7-1m)
- update 3.1.7

* Sun Apr  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.6-1m)
- update 3.1.6

* Fri Mar 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.5-1m)
- update 3.1.5

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.4-1m)
- update 3.1.4

* Mon Mar  4 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.3-1m)
- update 3.1.3
- sync rawhide
- modify install python site-package dir

* Sun Feb 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-1m)
- update 3.1.2

* Wed Feb 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.1-1m)
- update 3.1.1

* Thu Jan 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-1m)
- update 3.0.3

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-1m)
- update 3.0.1
- add python-2.5 patch

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-3m)
- rebuild against python-2.5

* Mon Oct 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-2m)
- move yum-updatesd.conf from yum to yum-updatesd

* Sun Oct 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-1m)
- update-3.0

* Thu Aug  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.1-4m)
- remove exclude = kernel* for yum.conf

* Thu Jul  6 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.1-3m)
- revise 'yum-2.6.1-momonga.patch' to force upgrading to different version
- add 'Obsoletes: yum-metadata-parser'

* Thu Jul  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-2m)
- maki modosi

* Sun Jul  2 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.9.2-2m)
- exclude lame-devel and usolame-devel

* Thu Jun 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.2-1m)
- update to version 2.9.2

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.1-1m)
- update to version 2.9.1
- Require yum-metadata-parser

* Thu Apr 27 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.1-1m)
- update to version 2.6.1
- delete included patches
- add into yum.conf.momonga
  +keepcache=0
  +plugins=1
  +metadata_expire=1800
- add Source99: plugin.conf
- add Source100: installonlyn.py
- add Patch0: yum-2.6.0-proxy.patch
- use %%make
- fix %%files section
- update Patch10: yum-2.6.1-momonga.patch

* Sun Oct 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-1m)
- version 2.4.0
- sync with Fedora Core devel
 +* Sun Oct 23 2005 Paul Nasrat <pnasrat@redhat.com> - 2.4.0-7
 +- Drop anaconda flag patch
 +- Fix ppc64pseries/iseries basearch substitution
 +
 +* Thu Oct 06 2005 Paul Nasrat <pnasrat@redhat.com> - 2.4.0-6
 +- Backport transaction constants
 +- Allow setting anaconda flag
 +
 +* Tue Oct  4 2005 Jeremy Katz <katzj@redhat.com>
 +- add dirs for plugins
 +
 +* Tue Sep 27 2005 Jeremy Katz <katzj@redhat.com> - 2.4.0-5
 +- add yum-cli dir (#169334)
 +
 +* Wed Sep 21 2005 Jeremy Katz <katzj@redhat.com> - 2.4.0-4
 +- make returnByName* be consistent in what it returns (#168712)
 +
 +* Fri Sep 16 2005 Jeremy Katz <katzj@redhat.com> - 2.4.0-3
 +- add two patches for anaconda that have been committed upstream
 +  * allow removal of packages from transaction
 +  * support search by name with sqlite
 +
 +* Thu Sep 01 2005 Paul Nasrat <pnasrat@redhat.com> - 2.4.0-2
 +- Initial version of macro support patch

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-3m)
- rebuild against python-2.4.2

* Thu Apr 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.3.2-2m)
- add Require

* Sun Apr 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2
- update Patch0: yum-2.3.2-momonga.patch
- delete included Patch1: yum-2.3.1-sqlite-fix.patch 

* Wed Mar 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.3.1-1m)
- up to 2.3.1

* Sat Mar 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.0-2m)
- modify rpmUtils.arch.getBaseArch()
  Same arch as system arch is selected.

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.0-1m)
- upgrade 2.3.0

* Sat Jan 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.7-5m)
- default architecture of ix86 is now i686
- use specopt for i586

* Mon Aug  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.7-4m)
- update yum.conf for MomongaLinux1 release

* Sun Jul 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-3m)
- change yum.conf for momonga linux 1 beta2

* Sun Jul 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-2m)
- add patch1 for fix yum.conf

* Thu May 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.7-1m)
- 1st release.

* Fri May  7 2004 Seth Vidal <skvidal@phy.duke.edu>
- 2.0.7

* Tue Mar 16 2004 Seth Vidal <skvidal@phy.duke.edu>
- 2.0.6

* Sat Jan 31 2004 Seth Vidal <skvidal@phy.duke.edu>
- 2.0.5

* Sun Oct 19 2003 Seth Vidal <skvidal@phy.duke.edu>
- 2.0.4

* Mon Sep  8 2003 Seth Vidal <skvidal@phy.duke.edu>
- brown paper-bag 2.0.3

* Sun Sep  7 2003 Seth Vidal <skvidal@phy.duke.edu>
- bump to 2.0.2

* Fri Aug 15 2003 Seth Vidal <skvidal@phy.duke.edu>
- bump to 2.0.1

* Sun Jul 13 2003 Seth Vidal <skvidal@phy.duke.edu>
- bump to 2.0

* Sat Jul 12 2003 Seth Vidal <skvidal@phy.duke.edu>
- made yum.cron config(noreplace)

* Sat Jun  7 2003 Seth Vidal <skvidal@phy.duke.edu>
- add stubs to spec file for rebuilding easily with custom yum.conf and
- yum.cron files

* Sat May 31 2003 Seth Vidal <skvidal@phy.duke.edu>
- bump to 1.98

* Mon Apr 21 2003 Seth Vidal <skvidal@phy.duke.edu>
- bump to 1.97

* Wed Apr 16 2003 Seth Vidal <skvidal@phy.duke.edu>
- moved to fhs compliance
- ver to 1.96

* Mon Apr  7 2003 Seth Vidal <skvidal@phy.duke.edu>
- updated for 1.95 betaish release
- remove /sbin legacy
- no longer starts up by default
- do the find_lang thing

* Sun Dec 22 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped ver to 0.9.4
- new spec file for rhl 8.0

* Sun Oct 20 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped ver to 0.9.3

* Mon Aug 26 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped ver to 0.9.2

* Thu Jul 11 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped ver to 0.9.1

* Thu Jul 11 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped ver  to 0.9.0

* Thu Jul 11 2002 Seth Vidal <skvidal@phy.duke.edu>
- added rpm require

* Sun Jun 30 2002 Seth Vidal <skvidal@phy.duke.edu>
- 0.8.9

* Fri Jun 14 2002 Seth Vidal <skvidal@phy.duke.edu>
- 0.8.7

* Thu Jun 13 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped to 0.8.5

* Thu Jun 13 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped to 0.8.4

* Sun Jun  9 2002 Seth Vidal <skvidal@phy.duke.edu>
- bumped to 0.8.2
* Thu Jun  6 2002 Seth Vidal <skvidal@phy.duke.edu>
- First packaging
