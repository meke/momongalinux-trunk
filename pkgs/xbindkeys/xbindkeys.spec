%global momorel 1

Summary: grab keys in X and start the corresponding shell command.
Name:    xbindkeys
Version: 1.8.5
Release: %{momorel}m%{?dist}
URL:     http://www.nongnu.org/xbindkeys/xbindkeys.html
Source0: http://www.nongnu.org/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
License: GPL
Group:   User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel, tk
BuildRequires: guile-devel >= 2.0.9

%description 
This program grab keys in X and start the corresponding shell command.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/xbindkeys
%{_bindir}/xbindkeys_show
%{_mandir}/man1/xbindkeys.1*
%{_mandir}/man1/xbindkeys_show.1*

%changelog
* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5
- rebuild against guile-2.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.3-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3-3m)
- %%NoSource -> NoSource

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.3-2m)
- rebuild against guile-1.8.1

* Sun Nov 12 2006  Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.3-1m)
- initial build
