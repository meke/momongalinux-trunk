%global momorel 11

%global fontname efont-unicode
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: unicode BDF font by /efont/
Name: efont-unicode-bdf
Version: 0.4.2
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: User Interface/X
URL: http://openlab.ring.gr.jp/efont/unicode/
Source0: http://openlab.ring.gr.jp/efont/dist/unicode-bdf/%{name}-%{version}.tar.bz2
Source1: h10_with_naga10.tar.bz2
Source2: b10_with_naga10.tar.bz2
Source10: fonts.alias.efont-unicode
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: fontpackages-devel
BuildRequires: gzip, xorg-x11-font-utils
Requires: xorg-x11-font-utils

%description
unicode BDF fonts developed by /efont/ openlab.
This font package includes 10, 12, 14 and 16 pixel ISO-10646 fonts.

%prep
%setup -q -a 1 -a 2

%build
for i in *.bdf ; do
    /usr/bin/bdftopcf $i > `basename $i .bdf`.pcf
    /usr/bin/gzip -9 `basename $i .bdf`.pcf
done

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_fontdir}
install -m 444 *.pcf.gz  %{buildroot}/%{_fontdir}
install -m 444 %{SOURCE10} %{buildroot}/%{_fontdir}/fonts.alias
/usr/bin/mkfontdir %{buildroot}/%{_fontdir}

mkdir -p %{buildroot}%{catalogue}
ln -sf %{_fontdir} %{buildroot}%{catalogue}/%{fontname}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README* COPYRIGHT ChangeLog
%{catalogue}/%{fontname}
%dir %{_fontdir}
%{_fontdir}/*.pcf.gz
%verify(not md5 size mtime) %{_fontdir}/fonts.alias
%verify(not md5 size mtime) %{_fontdir}/fonts.dir

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-11m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-5m)
- correct font catalogue
- remove chkfontpath-momonga

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-3m)
- rebuild against gcc43

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-2m)
- revise for xorg-7.0
- change install dir

* Tue Feb 15 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.2-1m)
- version 0.4.2

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (0.4-6k)
- mkfontdir tukattendaro korua.

* Tue Apr 30 2002 Kenta MURATA <muraken2@nifty.com>
- (0.4-4k)
- /usr/X11/bin/{mkfontdir,bdftopcf} -> XFree86 and /usr/bin/gzip -> gzip in BuildPreReq.

* Sat Nov  3 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.4.0-2k)
- merge 10px fonts and naga10 by permission of NAGAO, Sadakazu (the
  author of naga10)

* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
- (0.3.1-6k)
- add alias (alias-variable)

* Fri Oct 26 2001 Motonobu Ichimura <famao@kondara.org>
- (0.3.1-4k)
- add unicode-fontspecific aliases

* Fri Jun 29 2001 Akira Higuchi <a@kondara.org>
- (0.3.1-0k)
- updated to 0.3.1
- fixed the problem that some aliases are broken

* Sun Jun  3 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.3-2k)
- modify BuildRoot

* Sat Jun 02 2001 Kezni Cano <kc@ring.gr.jp>
- (0.3-3k)
- version up

* Thu Mar 10 2001 Kezni Cano <kc@ring.gr.jp>
- (0.2.2-3k)
- version up

* Wed Jan 17 2001 Kezni Cano <kc@ring.gr.jp>
- (0.2-3k)
- version up

* Sat Dec 16 2000 Kezni Cano <kc@ring.gr.jp>
- (0.14-1k)
- first release
