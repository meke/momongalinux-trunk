%global momorel 1

%global _includedir /usr/include/wine

Summary: WINE Is Not an Emulators - runs MS Windows programs
Name: wine
Version: 1.7.20
Release: %{momorel}m%{?dist}
# Release: %{rel}m%{?dist}
License: LGPLv2+
Group: Applications/Emulators
URL: http://www.winehq.com/
Source0: http://dl.sourceforge.net/sourceforge/wine/wine-%{version}.tar.bz2
Source1: http://mirrors.ibiblio.org/wine/source/1.6/wine-docs-1.6.tar.bz2
NoSource: 0
NoSource: 1
Source2: wine.init
Source3: wine.systemd
Source4: wine-32.conf
Source5: wine-64.conf
Source6: wine-chooser.sh
# desktop stuff
Source100: wine-notepad.desktop
Source101: wine-regedit.desktop
Source102: wine-uninstaller.desktop
Source103: wine-winecfg.desktop
Source104: wine-winefile.desktop
Source105: wine-winemine.desktop
Source106: wine-winhelp.desktop
Source107: wine-wineboot.desktop
Source108: wine-wordpad.desktop
Source109: wine-oleview.desktop
# desktop dir
Source200: wine.menu
Source201: wine.directory
# mime types
Source300: wine-mime-msi.desktop

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: freetype
Requires: nkf
BuildRequires: alsa-lib-devel
BuildRequires: audiofile-devel
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: bison
BuildRequires: cups-devel
BuildRequires: dbus-devel
BuildRequires: desktop-file-utils
BuildRequires: docbook-dtds
BuildRequires: docbook-utils
BuildRequires: docbook-utils-pdf
BuildRequires: esound-devel
BuildRequires: flex >= 2.5
BuildRequires: fontconfig-devel
BuildRequires: fontforge
BuildRequires: freeglut-devel
BuildRequires: freetype-devel
BuildRequires: giflib-devel
BuildRequires: gnutls-devel
BuildRequires: gsm-devel
BuildRequires: isdn4k-utils-devel
BuildRequires: jack-devel
BuildRequires: jadetex
BuildRequires: mesa-libGL-devel >= 7.0.1-2m
BuildRequires: mesa-libGLU-devel
BuildRequires: lcms-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXext-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXxf86dga-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libXmu-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libgphoto2-devel >= 2.5.0
BuildRequires: libieee1284-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: libstdc++-devel
BuildRequires: libtool
BuildRequires: libusb-devel
BuildRequires: libxml2-devel
BuildRequires: libxslt-devel
BuildRequires: linuxdoc-tools
BuildRequires: ncurses-devel
BuildRequires: openal-soft-devel
BuildRequires: openjade
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: openssl-devel
BuildRequires: perl
BuildRequires: pulseaudio-libs-devel
BuildRequires: sane-backends-devel
BuildRequires: unixODBC-devel
BuildRequires: zlib-devel
ExclusiveArch: %{ix86} x86_64

%description
Wine is a program that allows running MS-Windows programs under X11.
It consists of a program loader, that loads and executes an
MS-Windows binary, and of an emulation library that translates Windows
API calls to their Unix/X11 equivalent.

%package systemd
Summary:        Systemd config for the wine binfmt handler
Group:          Applications/Emulators
Requires:       systemd >= 23
BuildArch:      noarch

%description systemd
Register the wine binary handler for windows executables via systemd binfmt
handling. See man binfmt.d for further information.

%package sysvinit
Summary:        SysV initscript for the wine binfmt handler
Group:          Applications/Emulators
BuildArch:      noarch

%description sysvinit
Register the wine binary handler for windows executables via SysV init files.

%package devel
Summary: Development portion of Wine
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
Includes and headers to develop applications with the WINE libraries.

%package utils
Summary: Utilities of Wine
Group: Applications/Emulators
Requires: %{name} = %{version}

%description utils

Wine is a program which allows running Microsoft Windows programs
(including DOS, Windows 3.x and Win32 executables) on Unix.

This package consists many of the utilities provided by wine, both 
for compiling source using winelib and for running wine.  This
package is not strictly necessary.

%prep
%setup -q -n %{name}-%{version} -a 1

%build
autoreconf -if
CFLAGS="%(echo '%{optflags}'|sed -e 's/-fstack-protector//g'|sed -e 's/-fomit-frame-pointer//g') -fno-tree-fre -fno-tree-pre"
CXXFLAGS="%(echo '%{optflags}'|sed -e 's/-fstack-protector//g'|sed -e 's/-fomit-frame-pointer//g') -fno-tree-fre -fno-tree-pre"
FLAGS="%(echo '%{optflags}'|sed -e 's/-fstack-protector//g'|sed -e 's/-fomit-frame-pointer//g') -fno-tree-fre -fno-tree-pre"
export CFLAGS
export CXXFLAGS
export FLAGS
./configure \
 --prefix=%{_prefix} \
 --exec-prefix=%{_prefix} \
 --bindir=%{_bindir} \
 --sbindir=%{_sbindir} \
 --sysconfdir=%{_sysconfdir}/%{name} \
 --datadir=%{_datadir} \
 --includedir=%{_includedir}/%{name} \
 --libdir=%{_libdir} \
 --libexecdir=%{_libexecdir} \
 --localstatedir=%{_var} \
 --sharedstatedir=%{_prefix}/com \
 --mandir=%{_mandir} \
 --infodir=%{_infodir} \
 --disable-debug \
 --disable-trace \
%ifarch x86_64
 --enable-win64 \
%endif
 --without-hal \
 --with-dbus \
 --with-pulse \
 --with-x

%make depend all

# mv documentation/samples/config documentation/samples/config.orig

%install
rm -rf %{buildroot}
%makeinstall dlldir=%{buildroot}%{_libdir}/wine
#mkdir -p %{buildroot}%{_sysconfdir}/wine
#install -c tools/fnt2bdf %{buildroot}%{_bindir}/fnt2bdf

# FIXME wine-doc-1.6
sed -i 's/&#956;/micro/g' wine-docs-*/en/winedev-multimedia.sgml

pushd wine-docs-*
%configure 
pushd en
make dist
tar zxf wine-doc-en-html.tar.gz -C ../../documentation/
popd
popd

# make a filelist 
find %{buildroot}%{_libdir}/%{name}/*.*.so | grep -v ".exe.so" | grep -v ".exe16.so" | sed "s|%{buildroot}||g" > wine-files
find %{buildroot}%{_libdir}/%{name}/fakedlls/* | grep -v ".exe" | sed "s|%{buildroot}||g" >> wine-files
%ifarch %{ix86}
echo "%{_libdir}/%{name}/winevdm.exe.so" >> wine-files
%endif

#(from mandrake) winevdm.exe.so should go into wine package and not wine, and in stead of listing all files in %%files, just
# make a filelist without it
find %{buildroot}%{_libdir}/%{name}/*.exe*|grep -v winevdm.exe| grep -v libntoskrnl.exe.def |sed "s|%{buildroot}||g" > wine-utils-exe-files
find %{buildroot}%{_libdir}/%{name}/fakedlls/*.exe*|sed "s|%{buildroot}||g" >> wine-utils-exe-files

rm -rf %{buildroot}/%{_mandir}/de.UTF-8
rm -rf %{buildroot}/%{_mandir}/fr.UTF-8
rm -rf %{buildroot}/%{_mandir}/pl.UTF-8

%ifarch %{ix86}
# rename wine to wine32
mv %{buildroot}%{_bindir}/wine{,32}
%endif

# if x86_64 rename to wine64
%ifarch x86_64
mv %{buildroot}%{_bindir}/wineserver{,64}
%endif

mkdir -p %{buildroot}%{_sysconfdir}/wine

# Allow users to launch Windows programs by just clicking on the .exe file...
mkdir -p %{buildroot}%{_initscriptdir}
install -m 755 %{SOURCE2} %{buildroot}%{_initscriptdir}/wine
mkdir -p %{buildroot}%{_sysconfdir}/binfmt.d/
install -p -c -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/binfmt.d/wine.conf

mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d/

%ifarch %{ix86}
install -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/ld.so.conf.d/
%endif

%ifarch x86_64
install -m 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/ld.so.conf.d/
%endif

install -m 755 %{SOURCE6} %{buildroot}%{_bindir}/wine

%pre
# Add the "wine" group 
#/usr/sbin/groupadd -g 66 -r wine &>/dev/null || :

%post sysvinit
if [ $1 = 1 ]; then
/sbin/chkconfig --add wine
fi

/sbin/ldconfig

%preun sysvinit
if [ $1 = 0 ]; then
	/sbin/chkconfig --del wine
fi

%postun
/sbin/ldconfig

%clean
rm -rf %{buildroot}

%files -f wine-files
%defattr (-, root, root)
%doc README ANNOUNCE AUTHORS LICENSE COPYING.LIB
%doc documentation/winelib-guide.html
%doc documentation/winedev-guide.html
%doc documentation/wineusr-guide.html
%dir %{_sysconfdir}/%{name}
%ifarch %{ix86}
%{_sysconfdir}/ld.so.conf.d/wine-32.conf
%endif
%ifarch x86_64
%{_sysconfdir}/ld.so.conf.d/wine-64.conf
%endif
%{_bindir}/wine
%ifarch %{ix86}
%{_bindir}/wine32
%{_bindir}/wine-preloader
%{_bindir}/wineserver
%endif
%ifarch x86_64
%{_bindir}/wine64
%{_bindir}/wine64-preloader
%{_bindir}/wineserver64
%endif
%{_bindir}/wineconsole*
%{_bindir}/wineboot
%{_bindir}/function_grep.pl
#%%{_bindir}/wineclipsrv
#%%{_bindir}/wine-kthread
#%%{_bindir}/wine-pthread
#%%{_bindir}/wineprefixcreate
#%%{_bindir}/winebrowser
%{_bindir}/msiexec
%{_libdir}/libwine*.so.*
%ifnarch x86_64
%{_libdir}/wine/comm.drv16.so
#%%{_libdir}/wine/commdlg.dll16
%{_libdir}/wine/compobj.dll16.so
%{_libdir}/wine/dispdib.dll16.so
%{_libdir}/wine/display.drv16.so
%{_libdir}/wine/keyboard.drv16.so
%{_libdir}/wine/lzexpand.dll16.so
#%%{_libdir}/wine/mmsystem.dll16
%{_libdir}/wine/mouse.drv16.so
%{_libdir}/wine/msacm.dll16.so
%{_libdir}/wine/ole2.dll16.so
%{_libdir}/wine/ole2conv.dll16.so
%{_libdir}/wine/ole2disp.dll16.so
%{_libdir}/wine/ole2nls.dll16.so
%{_libdir}/wine/ole2prox.dll16.so
%{_libdir}/wine/ole2thk.dll16.so
%{_libdir}/wine/olecli.dll16.so
%{_libdir}/wine/olesvr.dll16.so
#%%{_libdir}/wine/setupx.dll16
%{_libdir}/wine/storage.dll16.so
%{_libdir}/wine/stress.dll16.so
#%%{_libdir}/wine/system.drv16
#%%{_libdir}/wine/toolhelp.dll16
%{_libdir}/wine/typelib.dll16.so
#%%{_libdir}/wine/ver.dll16
%{_libdir}/wine/win87em.dll16.so
%{_libdir}/wine/windebug.dll16.so
%endif
#%%{_libdir}/wine/wineps16.drv16
#%%{_libdir}/wine/wing.dll16
#%%{_libdir}/wine/winsock.dll16
#%%{_libdir}/wine/wprocs.dll16
# %{_libdir}/%{name}/*.*.so
%{_mandir}/man1/msiexec.1*
%{_mandir}/man1/notepad.1*
%{_mandir}/man1/regedit.1*
%{_mandir}/man1/regsvr32.1*
%{_mandir}/man1/wine.1*
%{_mandir}/man1/wineboot.1*
%{_mandir}/man1/winecfg.1*
%{_mandir}/man1/wineconsole.1*
%{_mandir}/man1/winecpp.1*
%{_mandir}/man1/winefile.1*
%{_mandir}/man1/winemine.1*
%{_mandir}/man1/winepath.1*
%{_mandir}/man1/wineserver.1*
# %{_mandir}/man5/wine.conf.5*
%{_datadir}/wine
#%%{_datadir}/wine/fonts/*
%{_datadir}/applications/wine.desktop
#%%attr(0775, root, wine) /var/lib/wine

%files systemd
%defattr(0644,root,root)
%config %{_sysconfdir}/binfmt.d/wine.conf

%files sysvinit
%defattr(0755,root,root)
%config %{_initscriptdir}/%{name}

%files devel
%defattr (-, root, root)
# %%doc DEVELOPERS-HINTS
%%doc documentation/winedev-guide.html
%%doc documentation/winelib-guide.html
%{_libdir}/wine/*.a
%{_libdir}/libwine*.so
%{_libdir}/%{name}/*.def
%dir %{_includedir}
%{_includedir}/*
#%%{_datadir}/aclocal/*
%{_bindir}/wmc
%{_mandir}/man1/wmc.1*
%{_bindir}/wrc
%{_mandir}/man1/wrc.1*
%{_bindir}/winebuild
%{_mandir}/man1/winebuild.1*
%{_mandir}/man1/widl.1*
%{_mandir}/man1/winedump.1*
#%%{_mandir}/man1/wineprefixcreate.1*

%files utils -f wine-utils-exe-files
%defattr(-,root,root)
%doc LICENSE
#%%{_bindir}/fnt2bdf
%{_bindir}/notepad
#%%{_bindir}/progman
%{_bindir}/regedit
#%%{_bindir}/uninstaller
%{_bindir}/winedbg
%{_bindir}/winedump
%{_bindir}/winemaker
%{_bindir}/winemine
%{_bindir}/winepath
#%{_bindir}/winhelp
%{_bindir}/regsvr32
%{_bindir}/widl
%{_bindir}/winefile
%{_bindir}/winegcc
%{_bindir}/winecpp
%{_bindir}/wineg++
#%%{_bindir}/winelauncher
%{_bindir}/winecfg
%{_mandir}/man1/winemaker.1*
%{_mandir}/man1/winedbg.1*
%{_mandir}/man1/winegcc.1*
%{_mandir}/man1/wineg++.1*

%changelog
* Mon Jun 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.20-1m)
- update to 1.7.20

* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.16-1m)
- update to 1.7.16

* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.15-1m)
- update to 1.7.15

* Wed Mar 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.14-1m)
- update to 1.7.14

* Wed Feb 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.13-1m)
- update to 1.7.13

* Sat Jan 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.10-1m)
- update to 1.7.10

* Wed Dec 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.8-1m)
- update to 1.7.8

* Tue Dec 03 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.7-1m)
- update 1.7.7

* Mon Sep 16 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-1m)
- update 1.7.2

* Mon May 27 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.31-1m)
- update 1.5.31

* Mon May 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.30-1m)
- update 1.5.30

* Sun Apr 28 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.29-1m)
- update 1.5.29

* Sun Apr 14 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.28-1m)
- update 1.5.28

* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.27-1m)
- update 1.5.27

* Wed Jan 23 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.22-1m)
- update 1.5.22

* Fri Jan 11 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.21-1m)
- update 1.5.21

* Tue Dec 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.20-1m)
- update 1.5.20

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.19-1m)
- update 1.5.19

* Sat Oct 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.16-1m)
- update 1.5.16

* Mon Oct 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.15-1m)
- update 1.5.15

* Sun Sep 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.14-1m)
- update 1.5.14

* Sun Sep  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.12-1m)
- update 1.5.12

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-4m)
- change Source1 URI

* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-3m)
- rebuild against libgphoto2-2.5.0

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-2m)
- fix source file
-- 1.4 rc3 used...

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-2m)
- update 1.4

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-0.996.1m)
- update 1.4-RC6

* Sat Feb 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-0.994.1m)
- update 1.4-RC4

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-0.993.1m)
- update 1.4-RC3

* Sat Feb  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-0.992.1m)
- update 1.4-RC2

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.36-1m)
- update 1.3.36

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.35-1m)
- update 1.3.35

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.34-1m)
- update 1.3.34

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.33-1m)
- update 1.3.33

* Mon Nov  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.32-1m)
- update 1.3.32

* Mon Oct 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.31-1m)
- update 1.3.31

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.29-1m)
- update 1.3.29

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.24-1m)
- update 1.3.24

* Mon Jun 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.22-1m)
- update 1.3.22

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.21-2m)
- fix %%files for x86_64

* Mon May 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.21-1m)
- update 1.3.21

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Mon Dec 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-2m)
- rebuild for new GCC 4.5

* Wed Oct 27 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-3m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (1.2-2m)
- fix build on x86_64

* Sun Jul 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2-1m)
- update 1.2
- update winepulse-0.38

* Sat May 22 2010 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (1.1.44-2m)
- stop daemon at initial system startup

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.44-1m)
- update 1.1.44

* Thu Mar 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.40-1m)
- update 1.1.40

* Thu Feb 19 2010 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (1.1.38-2m)
- enable build on x86_64
- support pulseaudio
- import many patches and desktop files from Fedora
- update wine.init from Fedora
- sort and fix up BRs

* Thu Feb 19 2010 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (1.1.38-1m)
- version 1.1.38

* Thu Jan 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.37-1m)
- update to 1.1.37

* Tue Jan 12 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.36-1m)
- update to 1.1.36

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.35-1m)
- update to 1.1.35

* Tue Dec  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.34-1m)
- update to 1.1.34

* Tue Nov 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.33-1m)
- update to 1.1.33

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.31-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.31-1m)
- update to 1.1.31

* Mon Sep  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.29-1m)
- update to 1.1.29

* Wed Aug 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.28-1m)
- update to 1.1.28

* Sat Aug 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.27-1m)
- update to 1.1.27

* Tue Jul 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.26-1m)
- update to 1.1.26

* Tue Jun 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.24-1m)
- update to 1.1.24
- set FontSmoothing to 2 (Patch1)

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.22-1m)
- update to 1.1.22

* Fri May 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.21-1m)
- update to 1.1.21

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.19-1m)
- update to 1.1.19

* Wed Mar 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17

* Wed Mar 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16-2m)
- wine-utils own winhelp.exe16.so

* Tue Mar 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16-1m)
- update to 1.1.16
- drop Patch1
-- please set \HKEY_CURRENT_USER\Control Panel\Desktop\FontSmoothing to 2

* Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-1m)
- update to 1.1.15
-- follow winegcc changes, not specify --host

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.14-1m)
- update to 1.1.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.13-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.13-1m)
- update to 1.1.13
-- drop Patch1 for the time being

* Thu Dec 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-1m)
- update to 1.1.11

* Sun Dec  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.10-1m)
- update to 1.1.10

* Sun Nov 23 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-1m)
- update to 1.1.9
-- removed wineshelllink

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-1m)
- update to 1.1.8
-- apply bison24 patch from http://bugs.winehq.org/show_bug.cgi?id=15950

* Sun Nov  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.6-1m)
- update 1.1.6

* Mon Sep  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4

* Sun Aug 24 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-1m)
- update 1.1.3

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Sat Jul 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Wed Jul  2 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (1.1.0-1m)
- update 1.1.0

* Wed Jun 18 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (1.0-1m)
- update 1.0 Release!

* Tue Jun 17 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (1.0-0.995.2m)
- add wine-1.0-rc5-conflict.patch
-- need wime

* Sun Jun 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.995.1m)
- update 1.0-rc5

* Sat Jun  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.994.1m)
- update 1.0-rc4

* Sat May 31 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (1.0-0.993.1m)
- update 1.0-rc3

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.992.2m)
- add Patch10: wine-signal_i386-vm86.patch

* Sat May 24 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (1.0-0.992.1m)
- update 1.0-rc2

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (1.0-0.991.1m)
- update 1.0-rc1

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (0.9.61-1m)
- update 0.9.61

* Sun Apr 20 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (0.9.60-1m)
- update 0.9.60

* Sun Apr  6 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (0.9.59-1m)
- update 0.9.59

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.58-2m)
- rebuild against gcc43

* Fri Mar 28 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (0.9.58-1m)
- update 0.9.58

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (0.9.57-1m)
- update 0.9.57

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-liunux.org>
- (0.9.56-2m)
- rebuild against openldap-2.4.8

* Tue Feb 26 2008 Masayuki SANO <nosanosa@momonga-liunux.org>
- (0.9.56-1m)
- update to 0.9.56
- add patch1 (for antialias fonts)

* Mon Feb 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.55-1m)
- update 0.9.55

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.54-1m)
- update 0.9.54

* Sat Jan 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.53-1m)
- update 0.9.53

* Sat Dec 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.52-1m)
- update 0.9.52

* Sun Dec 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.51-2m)
- add patch0 (do not use valgrind-3.3.0)

* Sun Dec 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.51-1m)
- update 0.9.51

* Mon Dec  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.50-1m)
- update 0.9.50

* Sat Nov 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.49-1m)
- update 0.9.49

* Sat Oct 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.48-1m)
- 0.9.48
- add BuildReq alsa-lib-devel

* Tue Oct 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.47-2m)
- add BuildReq alsa-lib-devel

* Sun Oct 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.47-1m)
- update 0.9.47

* Mon Sep 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.45-1m)
- update 0.9.45

* Sat Aug 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.44-1m)
- update 0.9.44

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.43-1m)
- update 0.9.43

* Mon Jul 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.42-1m)
- update 0.9.42

* Tue Jul 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.41-1m)
- update 0.9.41

* Mon Jul  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.40-1m)
- update 0.9.40

* Sun Jun 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.39-1m)
- update 0.9.39

* Wed Jun  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.38-2m)
- fix duplicate files

* Tue Jun  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.38-1m)
- update 0.9.38

* Tue May 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.37-1m)
- update 0.9.37

* Tue May  1 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.36-1m)
- update 0.9.36

* Thu Apr 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.35-1m)
- update 0.9.35

* Mon Apr  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.34-1m)
- update 0.9.34

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.33-2m)
- Requires: freetype2 -> freetype

* Mon Mar 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.33-1m)
- update to 0.9.33

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.32-1m)
- update to 0.9.32

* Sat Feb 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.31-1m)
- update to 0.9.31

* Sun Jan 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.30-1m)
- update to 0.9.30

* Tue Dec 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.27-1m)
- update to 0.9.27

* Fri Dec 01 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.26-1m)
- update to 0.9.26
- add wine-0.9.26-backout.patch
- (http://pc8.2ch.net/test/read.cgi/linux/1160189203/240)

* Fri Nov 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.22-2m)
- canceld 0.9.24 and back to 0.9.22 again
- Sorry, version 0.9.23/0.9.24 are really unstable and unusable.

* Fri Nov 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.24-1m)
- update to 0.9.24

* Sat Sep 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.22-1m)
- update to 0.9.22

* Fri Sep  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.20-1m)
- update to 0.9.20

* Sun Jul 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.18-1m)
- update to 0.9.18

* Fri Jul 14 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.17-1m)
- update to 0.9.17

* Thu Jun 22 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.16-1m)
- update to 0.9.16

* Sat Jun 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.15-1m)
- update to 0.9.15

* Sat May 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.14-1m)
- update to 0.9.14

* Fri May 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.13-1m)
- update to 0.9.13

* Wed Apr 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.12-1m)
- update to 0.9.12

* Wed Apr 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.11-2m)
- remove outdated setting stuffs (wine-setup, wine.config.template etc.)

* Mon Apr 10 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.11-1m)
- update 0.9.11

* Sun Feb 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-1m)
- update 0.9.7

* Mon Jan 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-1m)
- update 0.9.6

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.4-2m)
- rebuild against openldap-2.3.11

* Fri Dec 23 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4

* Fri Dec 23 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4

* Sat Dec 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-1m)
- update 0.9.3

* Fri Nov 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-1m)
- update 0.9.2

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-1m)
- update 0.9.1

* Wed Oct 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-1m)
- update 0.9

* Sat Oct  1 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050930-1m)
- update 20050930

* Wed Aug 31 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050830-1m)
- update 20050830

* Wed Jul 27 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050725-1m)
- update 20050725

* Wed Jun 29 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050628-1m)
- update 20050628
- rename source wine-doc-en-html.tar.gz

* Thu May 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050524-1m)
- update 20050524
- add /bin/msiexec
- add source wine-doc-html.tar.gz (any %doc files moved out of the source tree)

* Mon Apr 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050419-1m)
- update 20050419
- add rechedit patch

* Thu Apr 14 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050310-2m)
- fix conflict files

* Wed Apr 13 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20050310-1m)
- update to 20050310
- add oliver's DirectX9 patch d3d9patch.2005-03-10.diff.bz2
- add "BuildRequire fontforge"
- update wine.config.template (wine-config-sidenet-1.8.1.tgz)

* Tue Mar 01 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (20050211-1m)
- update to 20050211
- use gcc 3.4.3 (not gcc_3_2)
- not execute "make -C documentation" because of an error, and commented out some documentation files

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (20040914-2m)
- use linuxdoc-tools
- and modify font dir

* Fri Nov 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040914-1m)
- update to 20040914 (20040914 is more usable than latest 20041019)
- renew wine-setup.sh completely
- import config files (wine.inf, config) and wineshelllink.ja from "Sidenet wine configuration utility" (http://sidenet.ddo.jp/)
- use gcc_3_2 (bugs.winehq.org #2477)
- remove "-fstack-protector" from flags because build fails when it exists
- remove "-fomit-frame-pointer" because binary built with it does not work (bugs.winehq.org #2357).
- remove system-wide stuff (It has not been tested and maintained)

* Thu Jul  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (20040505-5m)
- use sazanami instead of kochi

* Thu Jul 01 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040505-4m)
- rewind (20040615 causes some annoying errors in running windows binaries)

* Sun Jun 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040615-1m)
- update to release 20040615

* Wed Jun 09 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040505-3m)
- revise wine-setup.sh and wine.conf

* Tue May 25 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040505-2m)
- update to release 20040505

* Sat May 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040505-1m)
- update to release 20040505

* Wed May  5 2004 Toru Hoshina <t@momonga-linux.org>
- (20040121-2m)
- revise spec.

* Wed Jan 28 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (20040121-1m)
- update to release 20040121
- Full IME support for Asian locales seems to bigin, 
  but we still use XIM patch at http://sidenet.ddo.jp/winetips/ because 
  original IME support doesn't work well.
- [Input Method] section in ~/.wine/config has been obsoleted.

* Wed Dec 10 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (20031118-1m)
- update to release 20031118

* Mon Oct 20 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (20031016-1m)
- update to release 20031016
- use http://sidenet.ddo.jp/winetips/files/wine-20031016-xim-jp106.diff.zip (Source6) as a xim patch
- make html docs locally
- revise wine-setup.sh (no win.ini file now)

* Thu Aug 21 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (20030813-1m)
- revise a spec file ([Momonga-devel.ja:02026])
- update to release 20030813
- according to project homepage, what's new are
- - Partial implementation of the Microsoft Installer (msi.dll).
- - GUI for regedit (from ReactOS).
- - DirectMusic dlls reorganization.
- - Many Wininet improvements.
- - Lots of bug fixes.

* Sat Aug  9 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (20030709-1m)
- version 20030709
- The license has become LGPL
- import XIM patch (http://www.2chlinux.org/)
- make wine-utils subpackage and revise %%files (as Mandrake Cooker 20030709-2mdk)
- add simple setup script (wine-setup) to create config files and fake windows system directory
- revised and customized wine.conf in a new format
   (The location of "fake windows system" can be changed by modifying [Drive C] section in ~/.wine/config)
- install default registry files (from RedHat8 20020605-2)
- make systemwide fake windows system directory as /var/lib/wine (as Mandrake Cooker 20030709-2mdk)
   (It is recommended that users using a systemwide configuration belong to "wine" group since Wine requires a writable access to a fake windows system directory (/var/lib/wine)) 
- revise word "%%post" and "%%postun" in old changelogs
- use momorel, %%NoSource, %%make macro

* Wed Mar 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (20030219-1m)
- version 20030219

* Sat Mar 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (20020310-2k)

* Thu Mar  7 2002 Kazuhiko <kazuhiko@kondara.org>
- (20020228-4k)
- revise %%post and %%postun script

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (20011004-4k)
- nigittenu

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (20011004-2k)
- merge from Jirai.

* Sun Oct 06 2001 Motonobu Ichimura <famao@kondara.org>
- (20011004-3k)
- up to 20011004

* Sat Sep 22 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (20010824-7k)
- fix %%post,%%postun section

* Tue Sep  4 2001 Shingo Akagaki <dora@kondara.org>
- (20010824-5k)
- add ba-tari font patch

* Sat Sep  1 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- Up to 20010501
- delete Wine-990923-gcc296.patch
- contain wine-20010510-font.patch(from VinePlus),but rejected ;p

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Oct 27 1999 KUSUNOKI Masanori <masanori@linux.or.jp>
- build against Kondara MNU/Linux 1.0
- prefix /usr/local to /usr
- wine.conf fixed

* Sun Aug  1 1999 Robert Pouliot <krynos@clic.net>
- Fixed a few things for the RPM to build
- The devel part is probably broken, I don't have time to fix now.

* Tue Dec 15 1998 Robert Pouliot <krynos@clic.net>
- Reverted back to /usr/local directories

* Sun Dec 13 1998 Robert Pouliot <krynos@clic.net>
- Integrated changes from Bruce Guenter <bruce.guenter@qcc.sk.ca>

* Mon Nov 9 1998 Robert Pouliot <krynos@clic.net>
- upgraded to 981108 snapshot
- credit to Kjetil Wiekhorst Jrgensen <jorgens@pvv.org>


