%global momorel 1

Name:           fftw
Version:        3.3.3
Release:        %{momorel}m%{?dist}
Summary:        Fast Fourier Transform library
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.fftw.org/
Source0:        ftp://ftp.fftw.org/pub/fftw/fftw-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gcc-gfortran

Provides:       fftw3 = %{version}-%{release}
Obsoletes:      fftw3 < 3.1.2-4m

%description
FFTW is a C subroutine library for computing the Discrete Fourier
Transform (DFT) in one or more dimensions, of both real and complex
data, and of arbitrary input size.

The API of FFTW 3.x is incompatible with that of FFTW 2.x, for reasons 
of performance and generality (see the FAQ or the manual). We encourage 
you to upgrade, but we still maintain version 2.1.5 for legacy users. 
MPI parallel transforms are still only available in 2.1.5.

%package        devel
Summary:        Headers, libraries and docs for the FFTW library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires(post): info
Requires(preun): info
Provides:       fftw3-devel = %{version}-%{release}
Obsoletes:      fftw3-devel < 3.1.2-4m

%description    devel
FFTW is a C subroutine library for computing the Discrete Fourier
Transform (DFT) in one or more dimensions, of both real and complex
data, and of arbitrary input size.

This package contains header files and development libraries needed to
develop programs using the FFTW fast Fourier transform library.

%package        static
Summary:        Static version of the FFTW library
Group:          Development/Libraries
Requires:       %{name}-devel = %{version}-%{release}
Provides:       fftw3-static = %{version}-%{release}

%description    static
The fftw-static package contains the statically linkable version of
the FFTW fast Fourier transform library.

%prep
%setup -q -c %{name}-%{version}
mv %{name}-%{version} single
cp -a single double
cp -a single long

%build
CONFIG_FLAGS="--enable-shared --disable-dependency-tracking --enable-threads"
pushd double
	%configure $CONFIG_FLAGS
	make %{?_smp_mflags}
popd
pushd single
	%configure $CONFIG_FLAGS --enable-single
	make %{?_smp_mflags}
popd
pushd long
	%configure $CONFIG_FLAGS --enable-long-double
	make %{?_smp_mflags}
popd

%install
rm -rf %{buildroot}
pushd double
	make install DESTDIR=%{buildroot}
	cp -a AUTHORS COPYING COPYRIGHT ChangeLog NEWS README* TODO ../
	cp -a doc/ ../
popd
pushd single
	make install DESTDIR=%{buildroot}
popd
pushd long
	make install DESTDIR=%{buildroot}
popd
rm -f %{buildroot}%{_infodir}/dir


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%post devel
/sbin/install-info %{_infodir}/%{name}3.info --section="Math" --entry="* FFTW: (fftw). C subroutines for computing the Discrete Fourier Transform." %{_infodir}/dir || :

%preun devel
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/%{name}3.info %{_infodir}/dir || :
fi


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYRIGHT ChangeLog NEWS README* TODO
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%doc doc/*.pdf doc/html/* doc/FAQ/fftw-faq.html/
%{_infodir}/*.info*
%exclude %{_libdir}/*.la
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_libdir}/*.so

%files static
%defattr(-,root,root,-)
%exclude %{_libdir}/*.la
%{_libdir}/*.a

%changelog
* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.2-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.1-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-2m)
- fix info file name

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1 based on Fedora 11 (3.2.1-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-2m)
- rebuild against rpm-4.6

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.3-5m)
- fix install-info

* Wed Apr 23 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.3-4m)
- rename fftw -> fftw2, fftw3 -> fftw

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-2m)
- %%NoSource -> NoSource

* Fri May 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.1-2m)
- revised %%files to resolve a issue of dupplcated files

* Wed Apr 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Fri Jun 30 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.1-1m)
- import to Momonga based on fftw3-3.0.1-3.src.rpm at FC

* Fri Apr 22 2005 Quentin Spencer <qspencer@users.sf.net> 3.0.1-3
- Changed gcc-g77 to gcc-gfortran
- replaced file removal at install with %exclude

* Mon Mar 21 2005 Quentin Spencer <qspencer@users.sf.net> 3.0.1-2
- Updated package description.

* Thu Mar  3 2005 Quentin Spencer <qspencer@users.sf.net> 3.0.1-1
- First version. Spec file based on Fedora Extras fftw 2.1.5.
