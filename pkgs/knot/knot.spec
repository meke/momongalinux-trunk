%global momorel 1

Summary: Authoritative-only DNS server from nic.cz
Name: knot
Version: 1.4.4
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: System Environment/Daemons
URL: http://www.knot-dns.cz/
Source0: http://public.nic.cz/files/knot-dns/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.service
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bison
BuildRequires: flex
BuildRequires: libtool
BuildRequires: openssl-devel
BuildRequires: systemd-units
BuildRequires: userspace-rcu-devel >= 0.8.4
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units


%description
High-performance authoritative-only DNS server which supports all key
features of the domain name system including zone transfers, dynamic
updates and DNSSEC.


%prep
%setup -q

%build
./configure --prefix=%{_prefix} \
	--sysconfdir=%{_sysconfdir}/%{name} \
	--localstatedir=%{_localstatedir} \
	--libdir=%{_libdir} --libexecdir=%{_libexecdir}/%{name}
make %{?_smp_mflags}


%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

install -d %{buildroot}/%{_localstatedir}/%{name}
install -d %{buildroot}/%{_unitdir}
install -p -m644 %{SOURCE1} %{buildroot}/%{_unitdir}/%{name}.service

#%%{__mkdir_p} %{buildroot}/var/knot

rm -rf %{buildroot}/usr/share/info/dir

%post
if [ $1 -eq 1 ]; then
	/bin/systemctl daemon-reload >/dev/null 2>&1 || :
	/bin/systemctl %{name}.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
	/bin/systemctl --no-reload disable %{name}.service > /dev/null 2>&1 || :
	/bin/systemctl stop %{name}.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
	/bin/systemctl try-restart %{name}.service >/dev/null 2>&1 || :
fi

%clean
rm -rf --preserve-root %{buildroot}


%files
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS
%config(noreplace) %attr(644,root,root) %{_sysconfdir}/%{name}/*
%dir %{_localstatedir}/%{name}
%{_bindir}/*
%{_sbindir}/*
#%{_libexecdir}/%{name}
#%exclude %{_libexecdir}/%{name}/unittests*
%{_infodir}/knot*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_unitdir}/%{name}.service


%changelog
* Wed Apr 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-1m)
- update 1.4.4

* Wed Mar 21 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update

* Sun Nov  6 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-1m)
- initial spec  file
