%global momorel 9
Summary: Japanese Code Conversion Library
Name: libjconv
Version: 2.9
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source: http://ghost.math.sci.hokudai.ac.jp/misc/%{name}/%{name}-%{version}.tar.gz
# NoSource: 0
URL: http://ghost.math.sci.hokudai.ac.jp/misc/libjconv/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: glibc >= 2.1.0

%description
This package provide Japanese Code Conversion capability based on iconv.

%prep
%setup -q

%build
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
mkdir -p %{buildroot}{%{_libdir},%{_includedir},%{_bindir}}
mkdir -p %{buildroot}%{_sysconfdir}/libjconv
install -m 755 libjconv.so %{buildroot}%{_libdir}
install -m 644 libjconv.a %{buildroot}%{_libdir}
install -m 755 jconv %{buildroot}%{_bindir}
install -m 644 jconv.h %{buildroot}%{_includedir}
install -m 644 default.conf %{buildroot}%{_sysconfdir}/libjconv

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc libjconv.html README.old COPYING
%{_libdir}/libjconv.so
%{_libdir}/libjconv.a
%{_bindir}/jconv
%{_includedir}/jconv.h
%config %{_sysconfdir}/libjconv

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9-4m)
- rebuild against gcc43

* Sun Feb  6 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.9-3m)
- enable x86_64.

* Sat Jan 4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9-2m)
- change to No NoSource

* Tue Jul 23 2002 kourin <kourin@momonga-linux.org>
- (2.9-1m)
- ver up to 2.9
- change to nosource

* Wed Dec 19 2001 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- version 2.9
- minor bug fixes

* Wed Nov 21 2001 Shingo Akagaki <dora@kondara.org>
- version 2.8.1
- fixup README from vine-users:030436

* Sat Aug  5 2000 Akira Higuchi <a@kondara.org>
- version 2.8
- use nl_langinfo(CODESET)

* Fri Jun 30 2000 Akira Higuchi <a@kondara.org>
- version 2.7
- added data for UTF-8 locales

* Fri Apr 14 2000 Akira Higuchi <a@kondara.org>
- version 2.5

* Thu Apr  6 2000 Akira Higuchi <a@kondara.org>
- version 2.3

* Sun Feb 27 2000 Akira Higuchi <a@kondara.org>
- added libjconv.a
- fixed a bug in jconv_strdup_conv_autodetect()

* Mon Feb  7 2000 Akira Higuchi <a@kondara.org>
- major version up.

* Sun Feb  6 2000 Shingo Akagaki <dora@kondara.org>
- version up.

* Wed Jan 15 2000 Toru Hoshina <t@kondara.org>
- version up.

* Sat Dec 11 1999 Akira Higuchi <a@kondara.org>
- convert_kanji(): write reset sequences for state-dependent encodings

* Mon Nov  8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Sep 29 1999 Toru Hoshina <hoshina@best.com>
- fixed Nie-a bug :-P

* Thu Sep 23 1999 Toru Hoshina <hoshina@best.com>
- localedata-ja is no longer required because we decide to provide our own
  glibc including localedata-ja !!

* Sun Aug 29 1999 Toru Hoshina <hoshina@best.com>
- 1st release.
