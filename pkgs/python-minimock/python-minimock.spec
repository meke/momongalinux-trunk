%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global with_python3 1

Name:           python-minimock
Version:        1.2.8
Release:        %{momorel}m%{?dist}
Summary:        The simplest possible mock library

Group:          Development/Tools
License:        MIT
URL:            http://pypi.python.org/pypi/MiniMock
Source0:        http://pypi.python.org/packages/source/M/MiniMock/MiniMock-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools

%if 0%{?with_python3}
BuildRequires:  python3-devel >= 3.4
BuildRequires:  python3-setuptools >= 0.6.21-2m
BuildRequires:  python-tools
%endif

%description
minimock is a simple library for doing Mock objects with doctest.

%if 0%{?with_python3}
%package -n python3-minimock
Summary:        The simplest possible mock library
%description -n python3-minimock
minimock is a simple library for doing Mock objects with doctest.
%endif

%prep
%setup -q -n MiniMock-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -ar . %{py3dir}
2to3 --write --nobackups %{py3dir}
%endif

%build
%{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
    %{__python3} setup.py build
popd
%endif


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%if 0%{?with_python3}
pushd %{py3dir}
    %{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
popd
%endif

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc docs/*
%{python_sitelib}/MiniMock-%{version}-py?.?.egg-info
%{python_sitelib}/minimock*

%if 0%{?with_python3}
%files -n python3-minimock
%defattr(-,root,root,-)
%doc docs/*
%{python3_sitelib}/MiniMock-%{version}-py?.?.egg-info
%{python3_sitelib}/minimock*
%{python3_sitelib}/__pycache__/minimock*
%endif

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.8-1m)
- update 1.2.8

* Mon Sep 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6-2m)
- force build with python3-3.2.2

* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.6-1m)
- import from Fedora

* Sun Feb 20 2011 Thomas Spura <tomspur@fedoraproject.org> - 1.2.6-1
- update to new version (#678851)

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Aug 22 2010 Thomas Spura <tomspur@fedoraproject.org> - 1.2.5-5
- rebuild with python3.2
  http://lists.fedoraproject.org/pipermail/devel/2010-August/141368.html

* Thu Jul 22 2010 David Malcolm <dmalcolm@redhat.com> - 1.2.5-4
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Fri Feb  5 2010 Thomas Spura <tomspur@fedoraproject.org> - 1.2.5-3
- update python3 subpackage to reflect actuall guidelines

* Thu Jan 14 2010 Thomas Spura <tomspur@fedoraproject.org> - 1.2.5-2
- also ship python3-minimock
- be more explicit in %%files

* Thu Dec 22 2009 Thomas Spura <tomspur@fedoraproject.org> - 1.2.5-1
- correct URL
- update to new version

* Sat Dec 5 2009 Thomas Spura <tomspur@fedoraproject.org> - 1.0-1
- Initial creation for review
