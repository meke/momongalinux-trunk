%global momorel 5

Summary: GPLed Arc de-archiver
Name:    nomarch
Version: 1.4
Release: %{momorel}m%{?dist}

Group:     Applications/Archiving
License:   GPLv2+
URL:       http://www.svgalib.org/rus/nomarch.html
Source0:   ftp://ftp.ibiblio.org/pub/Linux/utils/compress/%{name}-%{version}.tar.gz
NoSource:  0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
nomarch lists/extracts/tests `.arc' archives. (It also handles `.ark'
files, they're exactly the same.) This is a *very* outdated file
format which should never be used for anything new, but unfortunately,
you can still run into it every so often.


%prep
%setup -q


%build
make %{?_smp_mflags} CFLAGS="%{optflags}"


%install
rm -fr %{buildroot}
%makeinstall \
    BINDIR="%{buildroot}%{_bindir}" \
    MANDIR="%{buildroot}%{_mandir}/man1"


%clean
rm -fr %{buildroot}


%files
%defattr(0644, root, root, 0755)
%doc ChangeLog NEWS README TODO
%doc %{_mandir}/man?/*
%attr(0755,root,root) %{_bindir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-1m)
- import from Fedora to Momonga for amavisd-new

* Wed Sep 3 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.4-4
- Update URL

* Fri Feb 08 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.4-3
- gcc 4.3 rebuild
- License fix

* Sat Sep 02 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.4-2
- FE6 Rebuild

* Sun Jun 30 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.4-1
- upstream made a new release! Packager nirvana ends...

* Mon Feb 13 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.3-5
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 16 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.3-4
- me is gcc4.1-proof

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com>
- 1.3-3
- rebuild on all arches

* Fri Apr 7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sun Apr 18 2004 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- 0:1.3-0.fdr.1
- Fedorization

* Sun Jan 26 2003 Dag Wieers <dag@wieers.com>
- 1.3-0
- Initial package. (using DAR)
