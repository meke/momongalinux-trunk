%global momorel 1
%global wikidir %{_var}/www/mediawiki
%global ocamlver 3.11.2

Summary: The PHP-based wiki software behind Wikipedia
Name: mediawiki
Version: 1.21.3
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2+
URL: http://www.mediawiki.org/
Source0: http://download.wikimedia.org/mediawiki/1.21/mediawiki-%{version}.tar.gz
NoSource: 0
Source1: mediawiki.conf
Source2: README.RPM
Source3: mw-createinstance.in
Source4: mw-updateallinstances.in
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

Requires: httpd
Requires: php >= 4.1.2
Requires: php-mysql
Requires: php-pgsql
# to make sure the "apache" group is created before mediawiki is installed
Requires(pre): httpd

Obsoletes: mediawiki-math

%description
MediaWiki is the software used for Wikipedia (http://www.wikipedia.org) and
the other Wikimedia Foundation websites. Compared to other wikis, it has a
wide range of features and support for high-traffic websites using
multiple servers.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}

# move away the documentation to the final folder.
mkdir -p %{buildroot}%{_pkgdocdir}
cp -p %{SOURCE2} %{buildroot}%{_pkgdocdir}

# now copy the rest to the buildroot.
mkdir -p %{buildroot}%{_datadir}/mediawiki
cp -a * %{buildroot}%{_datadir}/mediawiki/

# remove unneeded parts
rm -fr %{buildroot}%{_datadir}/mediawiki/{t,test,tests}
rm -fr %{buildroot}%{_datadir}/mediawiki/includes/zhtable
find %{buildroot}%{_datadir}/mediawiki/ \
  \( -name .htaccess -or -name \*.cmi \) \
  | xargs -r rm

# fix permissions
find %{buildroot}%{_datadir}/mediawiki -name \*.pl | xargs -r chmod +x
chmod +x %{buildroot}%{_datadir}/mediawiki/maintenance/cssjanus/cssjanus.py
chmod +x %{buildroot}%{_datadir}/mediawiki/maintenance/cssjanus/csslex.py
chmod +x %{buildroot}%{_datadir}/mediawiki/maintenance/hiphop/make
chmod +x %{buildroot}%{_datadir}/mediawiki/maintenance/hiphop/run-server
chmod +x %{buildroot}%{_datadir}/mediawiki/maintenance/storage/make-blobs
chmod +x %{buildroot}%{_datadir}/mediawiki/includes/limit.sh
chmod +x %{buildroot}%{_datadir}/mediawiki/includes/normal/UtfNormalTest2.php
chmod +x %{buildroot}%{_datadir}/mediawiki/extensions/ConfirmEdit/captcha.py

# remove version control/patch files
find %{buildroot} -name .svnignore | xargs -r rm
find %{buildroot} -name \*.commoncode | xargs -r rm
find %{buildroot} -name .gitreview | xargs -r rm
find %{buildroot} -name .jshintignore | xargs -r rm
find %{buildroot} -name .jshintrc | xargs -r rm

# https://bugzilla.wikimedia.org/show_bug.cgi?id=49436
rm -f %{buildroot}%{_datadir}/mediawiki/maintenance/language/zhtable/trad2simp_supp_unset.manual

# placeholder for a default instance
mkdir -p %{buildroot}/var/www/wiki

mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d/
install -p -m 0644 %{SOURCE1} \
  %{buildroot}%{_sysconfdir}/httpd/conf.d/mediawiki.conf

# tools for keeping mediawiki instances current
mkdir -p %{buildroot}%{_sbindir}
sed -e's,@datadir@,%{_datadir},g' -e's,@sysconfdir@,%{_sysconfdir},g' \
  < %{SOURCE3} > %{buildroot}%{_sbindir}/mw-createinstance
sed -e's,@datadir@,%{_datadir},g' -e's,@sysconfdir@,%{_sysconfdir},g' \
  < %{SOURCE4} > %{buildroot}%{_sbindir}/mw-updateallinstances
chmod 0755 %{buildroot}%{_sbindir}/mw-*
mkdir %{buildroot}%{_sysconfdir}/mediawiki
echo /var/www/wiki > %{buildroot}%{_sysconfdir}/mediawiki/instances

%post
%{_sbindir}/mw-updateallinstances >> /var/log/mediawiki-updates.log 2>&1 || :

%files
%defattr(-,root,root,-)
%doc COPYING FAQ HISTORY README RELEASE-NOTES-1.21 UPGRADE CREDITS docs
%{_datadir}/mediawiki
/var/www/wiki
%config(noreplace) %{_sysconfdir}/httpd/conf.d/mediawiki.conf
%dir %{_sysconfdir}/mediawiki
%config(noreplace) %{_sysconfdir}/mediawiki/instances
%{_sbindir}/mw-createinstance
%{_sbindir}/mw-updateallinstances

%changelog
* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21.3-1m)
- [SECURITY] CVE-2013-4567 CVE-2013-4568 CVE-2013-4569 CVE-2013-4572
- [SECURITY] CVE-2013-4573 CVE-2012-5394
- update to 1.21.3

* Mon Jul  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17.5-1m)
- [SECURITY] CVE-2012-2698
- update to 1.17.5

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17.4-1m)
- update 1.17.4

* Wed Jan 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17.1-1m)
- [SECURITY] CVE-2011-4360 CVE-2011-4361
- update to 1.17.1

* Thu May 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.5-1m)
- [SECURITY] CVE-2011-1765 CVE-2011-1766
- update to 1.16.5

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.4-1m)
- [SECURITY] CVE-2011-1578 CVE-2011-1579 CVE-2011-1580 CVE-2011-1587
- update to 1.16.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16.2-2m)
- rebuild for new GCC 4.6

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.2-1m)
- [SECURITY] CVE-2011-0047 CVE-2011-0537
- update to 1.16.2

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.1-1m)
- [SECURITY] CVE-2011-0003
- update to 1.16.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.15.5-2m)
- full rebuild for mo7 release

* Sat Aug 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.5-1m)
- [SECURITY] CVE-2010-2787 CVE-2010-2788 CVE-2010-2789
- update to 1.15.5

* Fri Jun  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.4-1m)
- [SECURITY] CVE-2010-1647 CVE-2010-1648
- update to 1.15.4

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.3-2m)
- rebuild against ocaml-3.11.2

* Thu Apr  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.3-1m)
- [SECURITY] CVE-2010-1150
- update to 1.15.3

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.2-1m)
- [SECURITY] CVE-2010-1189 CVE-2010-1190
- update to 1.15.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.1-2m)
- do not use _smp_mflags

* Wed Jul 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.15.1-1m)
- [SECURITY] CVE-2009-4589
- update to 1.15.1
-- fix XSS vulnerability

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.0-2m)
- fix owner of config directory

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.0-1m)
- update to 1.15.0

* Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.0-1m)
- [SECURITY] CVE-2009-0737
- update to 1.14.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13.3-2m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13.3-1m)
- [SECURITY] CVE-2008-5249 CVE-2008-5250 CVE-2008-5252
- [SECURITY] CVE-2008-5687 CVE-2008-5688
- update to 1.13.3

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13.2-2m)
- rebuild against ocaml-3.11.0

* Mon Oct 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13.2-1m)
- [SECURITY] CVE-2008-4408
- update to 1.13.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12.0-2m)
- rebuild against gcc43

* Wed Mar 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.0-1m)
- [SECURITY] CVE-2008-1318
- update to 1.12.0

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.1-2m)
- rebuild against ocaml-3.10.2

* Wed Feb  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11.1-1m)
- [SECURITY] CVE-2008-0460
- update 1.11.1

* Tue Dec 4 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.11.0-2m)
- add missing file

* Tue Dec 4 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.11.0-1m)
- update to 1.11.0

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.4-1m)
- [SECURITY] CVE-2007-4828, CVE-2007-4883
- update to 1.9.4

* Sat Jun 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.2-1m)
- update to 1.9.2

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7.1-2m)
- disable math for ppc64

* Sun Sep 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.7.1-1m)
- version up 1.7.1

* Fri Jul 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.8-1m)
- import from fedora extra

* Mon Apr 03 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.8-1
- Update to upstream 1.5.8 (contains security fixes)

* Thu Mar 02 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.7-1
- Update to upstream 1.5.7

* Mon Feb 13 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.6-6
- Rebuild for Fedora Extras 5

* Mon Feb 06 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.6-5
- Put internal doc files in the right directory, renaming it
  back to docs (#180177)

* Sat Feb 04 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.6-4
- Use Requires(pre) instead of PreReq (Mike McGrath)

* Thu Feb 02 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.6-3
- Refactor the %%files section (Mike McGrath)
- Replace "/etc" with macro
- Package docs under %%{_defaultdocdir}/internals
- Minor change in description

* Tue Jan 31 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.6-2
- Add %%defattr for -math subpackage
- Fixed typo in description

* Tue Jan 31 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.6-1
- Update to upstream 1.5.6

* Sun Jan 15 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.5-3
- Add PreReq for httpd, since we use the apache user
- Make mediawiki-math dependencies more specific
- Package documentation for mediawiki-math

* Sat Jan 14 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.5-2
- Separate math support into a subpackage

* Thu Jan 12 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 1.5.5-1
- Initial packaging
