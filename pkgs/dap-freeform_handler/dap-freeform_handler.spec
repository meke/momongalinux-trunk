%global momorel 4
%global src_name freeform_handler

Summary:         FreeForm data handler for the OPeNDAP Data server
Name:            dap-freeform_handler
Version:         3.8.1
Release:         %{momorel}m%{?dist}
License:         LGPLv2+
Group:           System Environment/Daemons 
Source0:         http://www.opendap.org/pub/source/%{src_name}-%{version}.tar.gz
NoSource:        0
Patch0:          freeform_handler-3.8.1-libm.patch
URL:             http://www.opendap.org/
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	 bes-devel >= 3.8.4
BuildRequires:   libdap-devel >= 3.10.2
#Requires:       dap-server

%description 
This is the foreeform data handler for our data server. It reads ASCII,
binary and DB4 files which have been described using FreeForm and returns DAP
responses that are compatible with DAP2 and the dap-server 3.5 software.

%prep 
%setup -q -n %{src_name}-%{version}
%patch0 -p1 -b .libm

%build
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name '*.la' -exec rm -f {} \;

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING COPYRIGHT NEWS README
%config(noreplace) %{_sysconfdir}/bes/modules/ff.conf
%{_libdir}/bes/libff_module.so
%{_datadir}/hyrax/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.1-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-1m)
- rebuild against libdap-3.10.2 and bes-3.8.4
- update to 3.8.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.9-3m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.9-2m)
- update Patch0 for fuzz=0
- License: LGPLv2+

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.9-1m)
- update to 3.7.9
- rebuild against libdap-3.8.2
- rebuild against bes-3.6.2

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.8-1m)
- update to 3.7.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.2-4m)
- rebuild against gcc43

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.2-3m)
- rebuild for new libdap

* Sat Jan  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.2-2m)
- add patch for gcc43

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.7.2-1m)
- import from Fedora

* Tue Oct 31 2006 Patrice Dumas <pertusus@free.fr> 3.7.2-3
- rebuild for new libcurl soname (indirect dependency through libdap)

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 3.7.2-2
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Wed Sep 20 2006 Patrice Dumas <pertusus@free.fr> 3.7.2-1
- update to 3.7.2

* Wed Sep  6 2006 Patrice Dumas <pertusus@free.fr> 3.7.1-3
- rebuild for FC-6

* Sat Jul 22 2006 Patrice Dumas <pertusus@free.fr> 3.7.1-2
- update  to 3.7.1

* Mon Feb 27 2006 James Gallagher <jgallagher@opendap.org> 3.6.0-1
- Update to 3.6.0

* Thu Sep 21 2005 James Gallagher <jgallagher@opendap.org> 3.5.0-1
- initial release
