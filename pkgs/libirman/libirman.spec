%global momorel 5

Name:           libirman
Version:        0.4.5
Release:        %{momorel}m%{?dist}
Summary:        Library for IRMAN hardware

Group:          System Environment/Libraries
#The files which make up the library are covered under the GNU Library
#General Public License, which is in the file COPYING.lib.
#The files which make up the test programs and the documentation are covered
#under the GNU General Public License, which is in the file COPYING.
License:        GPLv2+ and LGPLv2+
URL:            http://lirc.sourceforge.net/software/snapshots/
Source0:        http://dl.sourceforge.net/sourceforge/lirc/libirman-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf, automake, libtool

%description
A library for accessing the IRMAN hardware from Linux and other Unix systems.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
libtoolize --force --copy
autoreconf
%configure --disable-static --disable-rpath \
  --prefix=%{_prefix} \
  --mandir=%{_mandir} \
  --sysconfdir=%{_sysconfdir}
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING* README TODO NEWS
%config(noreplace) %{_sysconfdir}/irman.conf
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc TECHNICAL
%{_includedir}/*
%{_libdir}/*.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-1m)
- import from Fedora 11

* Sun May 17 2009 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.4.5-3
- added libtoolize to fix build for f11

* Sat Apr 18 2009 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.4.5-2
- added autoreconf and --disable-rpath

* Fri Apr 10 2009 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.4.5-1
- new upstream
- updated Source0 to sourceforge
- removed autoconf things

* Thu Apr 02 2009 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.4.4-5.20090314cvs
- removed cvs patch, added instructions to create cvs snapshot tar package,
  which is now defined as Source0

* Sat Mar 14 2009 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.4.4-4.20090314cvs
- applied cvs patch, which fixed dynamic library build and IRMAN restart
- added BuildRequires: autoconf, automake, libtool

* Sat Dec  6 2008 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.4.4-3
- initial release
