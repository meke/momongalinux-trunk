%global momorel 11

Summary: Thread-safe hash library
Name: libmhash
Version: 0.9.9
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/mhash/mhash-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://mhash.sourceforge.net/

%description
%{name} is a thread-safe hash library, implemented in C, and provides a
uniform interface to a large number of hash algorithms. These
algorithms can be used to compute checksums, message digests, and
other signatures. The HMAC support implements the basics for message
authentication, following RFC 2104.

Algorithms currently supplied are:

CRC-32, CRC-32B, ALDER-32, MD-2, MD-4, MD-5, RIPEMD-128, RIPEMD-160,
RIPEMD-256, RIPEMD-320, SHA-1, SHA-224, SHA-256, SHA-384, SHA-512, HAVAL-128,
HAVAL-160, HAVAL-192, HAVAL-256, TIGER, TIGER-128, TIGER-160, GOST, WHIRLPOOL,
SNEFRU-128, SNEFRU-256

Algorithms will be added to this list over time.

%package devel
Summary: Header files and libraries for developing apps which will use %{name}
Group: Development/Libraries
Requires: %name = %{version}-%{release}

%description devel
The %{name}-devel package contains the header files and libraries needed
to develop programs that use the %{name} library.

Install the %{name}-devel package if you want to develop applications that
will use the %{name} library.

%prep
%setup -q -n mhash-%{version}

%build
%configure --enable-static --program-prefix=""
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%{_libdir}/libmhash.so.*

%files devel
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL ChangeLog NEWS README TODO THANKS
%{_libdir}/libmhash.a
%{_libdir}/libmhash.so
%{_includedir}/mhash.h
%{_includedir}/mutils
%{_mandir}/man?/mhash.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9-9m)
- full rebuild for mo7 release

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-8m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.9-5m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9-2m)
- %%NoSource -> NoSource

* Thu Apr  5 2007 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (0.9.9-1m)
- updated to 0.9.9

* Tue Mar 13 2007 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (0.9.8.1-1m)
- updated to 0.9.8.1

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.7.1-2m)
- delete libtool library

* Mon Jul 24 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (0.9.7.1-1m)
- updated to 0.9.7.1

* Wed May 24 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (0.9.6-1m)
- updated to 0.9.6
- remove Patch: mincludes.patch
  included 0.9.5

* Wed Jan 25 2006 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.9.4a-2m)
- add Patch: mincludes.patch
  fix missing mutils/mincludes.h

* Sat Jan 21 2006 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.9.4a-1m)
- updated to 0.9.4a
  code cleanup
- change source URL

* Fri Jan 21 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.9.2-1m)
- updated to 0.9.2
  Added Snefru. Re-introduced MD2.
  Fixed memory leaks, warnings and other bugs.
- imported spec file from original tar archive.

* Tue Apr 20 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.9.1-1m)
- updated to 0.9.1
  Added RIPEMD128/256/320, SHA224/384/512, Whirlpool

* Mon Feb 2 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.8.18-1m)
- updated to 0.8.18
- use %%{momorel}, %%NoSource
- add --enable-static --program-prefix="" to %%configure
- move libmhash.so to devel
- remove libtoolize, aclocal, autoconf from %%build
- use %%make
- revise %%post, %%postun
- add man file to devel

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.16-1m)
- add libmhash


