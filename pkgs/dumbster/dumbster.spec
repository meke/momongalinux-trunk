%global momorel 5

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Summary:        Fake SMTP Server
Name:           dumbster
Version:        1.6
Release:        %{momorel}m%{?dist}
Epoch:          0
License:        "ASL 2.0"
URL:            http://quintanasoft.com/dumbster/
Group:          Development/Tools
# cvs -z3 -d:pserver:anonymous@dumbster.cvs.sourceforge.net:/cvsroot/dumbster export -r RELEASE_1_6 dumbster
# tar czf dumbster-1.6-src.tgz dumbster
Source0:        %{name}-%{version}-src.tgz
Source1:        %{name}-1.6.pom
Patch0:         %{name}-SimpleSmtpServer.patch
BuildRequires:  ant >= 0:1.6
BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  jaf
BuildRequires:  javamail
BuildRequires:  junit
Requires:       jaf
Requires:       java-sasl
Requires:       javamail

BuildArch:      noarch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Dumbster is a very simple fake SMTP server designed for
unit and system testing applications that send email messages.
It responds to all standard SMTP commands but does not deliver
messages to the user. The messages are stored within the
Dumbster for later extraction and verification.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
%{summary}.

%prep
%setup -q -n %{name}
# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;

%patch0
rm -f src/com/dumbster/smtp/SimpleSmtpServer.java.orig

%build
pushd lib
ln -sf $(build-classpath javamail)
ln -sf $(build-classpath jaf)
ln -sf $(build-classpath junit)
ln -sf $(build-classpath sasl)
popd

ant jar javadoc

%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}

install -m 0644 build/%{name}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr doc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

# pom
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
cp -pr %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}.pom
%add_to_maven_depmap dumbster %{name} 1.6 JPP %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(0644,root,root,0755)
%doc license.txt
%{_javadir}/*.jar
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-3m)
- full rebuild for mo7 release

* Wed Mar 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-2m)
- remove duplicate directories

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-1m)
- import from Fedora 13

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.6-9
- Fix groups.
- Remove javadoc Requires(post/postun).

* Wed Aug 19 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.6-8
- Drop gcj support.
- Fix javadoc build error.
- Remove javadoc ghost dirs.

* Sun May 17 2009 Fernando Nasser <fnasser@redhat.com> 0:1.6-7
- Fix license
- Add upstream fix to race condition

* Tue Mar 17 2009 Yong Yang <yyang@redhat.com> 0:1.6-6
- bump release number

* Tue Mar 17 2009 Yong Yang <yyang@redhat.com> 0:1.6-5
- rebuild with new maven2 2.0.8 built in bootstrap mode

* Thu Feb 05 2009 Yong Yang <yyang@redhat.com> - 0:1.6-4
- Fix release tag

* Wed Jan 14 2009 Yong Yang <yyang@redhat.com> - 0:1.6-3jpp.2
- re-build with gcj

* Thu Jan 08 2009 Yong Yang <yyang@redhat.com> - 0:1.6-3jpp.1
- Import from dbhole's maven 2.0.8 packages, Initial building on JPP6

* Mon Jan 07 2008 Deepak Bhole <dbhole@redhat.com> 1.6-2jpp.1
- Merge with JPackage
- Update as per Fedora spec

* Wed May 09 2007 Ralph Apel <r.apel@r-apel.de> - 0:1.6-2jpp
- Fix date in copyright notice
- Fix aot build
- Make Vendor, Distribution based on macro
- Fix BR for java-sasl to gnu-crypto-sasl-jdk1.4

* Wed Sep 13 2006 Ralph Apel <r.apel@r-apel.de> - 0:1.6-1jpp
- First JPackage build
- Add post/postun Requires for javadoc
- Add gcj_support option
