%global momorel 10

%global pyver 2.7

Summary:  Collection of video disc authoring tools
Name:     tovid
Version:  0.30
Release: %{momorel}m%{?dist}
Group:    Applications/Multimedia
License:  GPL
URL:      http://tovid.wikia.com/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Requires: python wxPython python-imaging pycairo
Requires: mplayer mjpegtools ffmpeg
Requires: ImageMagick dvdauthor dvd+rw-tools vcdimager
Requires: sox normalize 
BuildRequires: txt2tags python >= 2.7 python >= 2.7
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description

%prep
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# force LANG=en_US.utf8
mv -f %{buildroot}%{_bindir}/tovidgui %{buildroot}%{_bindir}/tovidgui.py
cat > %{buildroot}%{_bindir}/tovidgui <<EOF
#!/bin/bash
LANG=en_US.utf8 tovidgui.py $@
EOF
chmod 755 %{buildroot}%{_bindir}/tovidgui

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README INSTALL ChangeLog AUTHORS COPYING NEWS
%{_bindir}/*
%{_libdir}/python%{pyver}/site-packages/libtovid
%{_datadir}/applications/*.desktop
%{_datadir}/icons/*/*/*/*
%{_mandir}/man1/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.30-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.30-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.30-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.30-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.30-4m)
- rebuild agaisst python-2.6.1-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.30-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.30-2m)
- %%NoSource -> NoSource

* Tue Apr 10 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.30-1m)
- import to Momonga
