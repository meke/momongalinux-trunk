%global         momorel 13

Name:           perl-Convert-Binary-C
Version:        0.76
Release:        %{momorel}m%{?dist}
Summary:        Binary Data Conversion using C Types
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Convert-Binary-C/
Source0:        http://www.cpan.org/authors/id/M/MH/MHX/Convert-Binary-C-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-pod.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Convert::Binary::C is a preprocessor and parser for C type definitions. It
is highly configurable and supports arbitrarily complex data structures.
Its object-oriented interface has pack and unpack methods that act as
replacements for Perl's pack and unpack and allow to use C types instead of
a string representation of the data structure for conversion of binary data
from and to Perl's complex data structures.

%prep
%setup -q -n Convert-Binary-C-%{version}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%{_bindir}/*
%{perl_vendorarch}/auto/Convert/Binary/C*
%{perl_vendorarch}/Convert/Binary/C*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-13m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-12m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-11m)
- reuild against perl-5.18.1

* Mon May 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-10m)
- fix pod test

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-9m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-8m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-7m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-6m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-5m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76
- parallel build does not work...

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.74-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.74-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.74-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.74-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.74-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-1m)
- update to 0.74

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.71-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.71-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.71-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb  8 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.71-2
- Add patch to fix #elif directives for new GCC 4.4

* Wed Jun  4 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.71-1
- Update to latest upstream (0.71)

* Mon Mar  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.70-5
- rebuild for new perl (again)

* Sat Feb 23 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.70-4
- Bump release to fix koji problem that prevented tagging the previous
  (correct) build.

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.70-3
- Autorebuild for GCC 4.3

* Fri Feb  8 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.70-2
- rebuild for new perl

* Sun Jan  6 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.70-1
- Update to latest upstream (0.70)

* Thu Aug 23 2007 Alex Lancaster <alexlan[AT]fedoraproject org> 0.68-2
- License tag to GPL+ or Artistic as per new guidelines.

* Sat Aug 18 2007 Alex Lancaster <alexlan[AT]fedoraproject org> 0.68-1
- Update to latest upstream

* Mon Apr 02 2007 Alex Lancaster <alexlan[AT]fedoraproject org> 0.67-4
- Remove '%{?_smp_mflags}', package does not support parallel make.

* Thu Mar 29 2007 Alex Lancaster <alexlan[AT]fedoraproject org> 0.67-3
- Add BuildRequires:  perl(Test::Pod), perl(Test::Pod::Coverage)

* Tue Mar 27 2007 Alex Lancaster <alexlan[AT]fedoraproject org> 0.67-2
- Add perl(ExtUtils::MakeMaker) BR.

* Fri Mar 23 2007 Alex Lancaster <alexlan[AT]fedoraproject org> 0.67-1
- Specfile autogenerated by cpanspec 1.69.1.
