%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global momorel 1

Summary:  A python library for handling exceptions
Name: python-meh
Url: http://git.fedoraproject.org/git/?p=python-meh.git
Version: 0.25
Release: %{momorel}m%{?dist}
# This is a Red Hat maintained package which is specific to
# our distribution.  Thus the source is only available from
# within this srpm.
# This tarball was created from upstream git:
#   git clone git://git.fedoraproject.org/git/python-meh.git
#   cd python-meh && make archive
Source0: %{name}-%{version}.tar.gz

License: GPLv2+
Group: System Environment/Libraries
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7, gettext, python-setuptools-devel, intltool
BuildRequires: libreport-python
Requires: python, dbus-python, pygobject3, gtk3
Requires: openssh-clients, rpm-python, yum, newt-python, libreport-gtk , libreport-newt

%description
The python-meh package is a python library for handling, saving, and reporting
exceptions.

%prep
%setup -q

%build
make

%check
make test

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc ChangeLog COPYING
%{python_sitelib}/*
%{_datadir}/python-meh

%changelog
* Wed May 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25-1m)
- update 0.25
 
* Sun Sep  1 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-2m)
- fix build failure

* Wed Dec 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20-1m)
- update 0.20

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15-1m)
- update 0.15

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11-2m)
- rebuild against python-2.7

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11-1m)
- update 0.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-2m)
- full rebuild for mo7 release

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-1m)
- version down python-meh

* Wed Mar 17 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-1m)
- Initial commit Momonga Linux. import from Fedora

* Thu Mar 04 2010 Chris Lumens <clumens@redhat.com> - 0.8-1
- And add a requirement for report as well. (clumens)
- filer.py is now completely taken over by meh. (clumens)
- Everything from savers.py has moved into report. (clumens)
- Remove unused UI code now that report handles all this for me. (clumens)
- Switch ExceptionHandler to use report (#562656). (clumens)
- Don't allow an exception when writing out an attribute stop the whole dump. (clumens)
- Credit where credit is due. (clumens)

* Tue Nov 03 2009 Chris Lumens <clumens@redhat.com> - 0.7-1
- Add a test case framework.
- Move src -> meh for ease of test case writing.
- Another attempt at making the attrSkipList work (#532612, #532737).

* Thu Oct 08 2009 Chris Lumens <clumens@redhat.com> - 0.6-1
- Make idSkipList work again.
- Support dumping objects derived from Python's object.
- Use the right method to set text on a snack.Entry (#526884).

* Tue Sep 29 2009 Chris Lumens <clumens@redhat.com> - 0.5-1
- Always compare version numbers as strings (#526188).

* Fri Sep 25 2009 Chris Lumens <clumens@redhat.com> - 0.4-1
- Add a default description to bug reports.
- Handle the user pressing Escape by continuing to show the dialog.
- Lots more translation updates.

* Thu Sep 10 2009 Chris Lumens <clumens@redhat.com> - 0.3-1
- Pull in lots of new translations (#522410).

* Wed Aug 19 2009 Chris Lumens <clumens@redhat.com> - 0.2-1
- Add a title to the main exception dialog so it looks right in anaconda.
- Don't include an extra '/' in the displayed bug URL (#517515).
- Now that there's .po files, package them.
- Use the new exception icon (#517164).

* Tue Jul 28 2009 Chris Lumens <clumens@redhat.com> - 0.1-1
- Initial package.
