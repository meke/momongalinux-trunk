%global momorel 1
%global glib_ver 2.16

Name:           json-glib
Version:        0.16.2
Release:        %{momorel}m%{?dist}
Summary:        Library for JavaScript Object Notation format
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://live.gnome.org/JsonGlib
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.16/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  glib2-devel >= %{glib_ver}
BuildRequires:  gobject-introspection-devel

%description
%{name} is a library providing serialization and deserialization support
for the JavaScript Object Notation (JSON) format.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       glib2-devel >= %{glib_ver}
Requires:       gtk-doc
Requires:       pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static \
  --enable-gtk-doc \
  --enable-introspection=yes \
  --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}-1.0

%check
make check

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}-1.0.lang
%defattr(-,root,root,-)
%doc COPYING NEWS
%{_libdir}/lib%{name}*.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/Json-1.0.typelib

%files devel
%defattr(-,root,root,-)
%{_libdir}/lib%{name}*.so
%{_libdir}/pkgconfig/%{name}-1.0.pc
%{_includedir}/%{name}-1.0/
%{_datadir}/gtk-doc/html/%{name}
%{_datadir}/gir-1.0/Json-1.0.gir

%changelog
* Fri Dec 27 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.2-1m)
- update to 0.16.2

* Mon Sep 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.2-1m)
- update to 0.15.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.2-2m)
- rebuild for glib 2.33.2

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.90-1m)
- update to 0.13.90

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.6-1m)
- update to 0.12.6

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.4-1m)
- update to 0.12.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.2-2m)
- rebuild for new GCC 4.6

* Wed Feb 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.2-1m)
- update to 0.12.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.4-3m)
- full rebuild for mo7 release

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-2m)
- move gir file from devel to main package

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.4-1m)
- import from Fedora devel

* Fri Mar 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.10.4-1
- Update to 0.10.4.

* Wed Jan 27 2010 Peter Robinson <pbrobinson@gmail.com> - 0.10.0-3
- Require the gobject-introspection-devel package, not the library

* Wed Jan 27 2010 Peter Robinson <pbrobinson@gmail.com> - 0.10.0-2
- Enable gobject-introspection support

* Tue Dec 29 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.10.0-1
- Update to 0.10.0.

* Mon Nov 16 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.2-1
- Update to 0.8.2.

* Tue Sep 29 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.0-1
- Update to 0.8.0.
- Update source url.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.6.2-3
- Disable tests for now.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat May 31 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.2-1
- Update to 0.6.2.
- Enable tests.

* Mon May 19 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.0-1
- Update 0.6.0.
- Disable tests for now.
- Add requires on gtk-doc.

* Sun Apr 20 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.4.0-1
- Initial Fedora spec.

