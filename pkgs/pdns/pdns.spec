%global         momorel 2
%global boost_version 1.55.0
#%global rcver 2
#%global srcname %{name}-%{version}-rc%{rcver}
%global backends %{nil}

Summary:        A modern, advanced and high performance authoritative-only nameserver
Name:           pdns
Version:        3.1
Release:        %{momorel}m%{?dist}

Group:          System Environment/Daemons
License:        GPLv2
URL:            http://powerdns.com
Source0:        http://downloads.powerdns.com/releases/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        pdns.service
Patch0:         pdns-fix-mongo-backend.patch

Patch10:        pdns-mongodb2.patch
# from https://github.com/azlev/powerdns/commit/a402d8493e5610e139ea19a9ef700e26b2e6e35c

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(pre):  shadow-utils
Requires(post): systemd-units, systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units

BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  chrpath, lua-devel, cryptopp-devel, systemd-units
BuildRequires:  openssl-devel >= 1.0.0
Provides:       powerdns = %{version}-%{release}

%description
The PowerDNS Nameserver is a modern, advanced and high performance
authoritative-only nameserver. It is written from scratch and conforms
to all relevant DNS standards documents.
Furthermore, PowerDNS interfaces with almost any database.

%package	backend-mysql
Summary:        MySQL backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
BuildRequires:  mysql-devel >= 5.5.10
%global backends %{backends} gmysql

%package	backend-postgresql
Summary:        PostgreSQL backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
BuildRequires:  postgresql-devel
%global backends %{backends} gpgsql

%package	backend-pipe
Summary:        Pipe backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
%global backends %{backends} pipe

%package	backend-geo
Summary:        Geo backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
%global backends %{backends} geo

%package	backend-ldap
Summary:        LDAP backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
BuildRequires:  openldap-devel >= 2.4.8
%global backends %{backends} ldap

%package	backend-sqlite
Summary:        SQLite backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
BuildRequires:  sqlite-devel
%global backends %{backends} gsqlite3

%ifarch %{ix86} x86_64
%package	backend-mongodb
Summary:        MongoDB backend for %{name}
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
BuildRequires:  mongodb-devel >= 2.0.4-5m
%global backends %{backends} mongodb
%endif

%description    backend-mysql
This package contains the gmysql backend for %{name}

%description    backend-postgresql
This package contains the gpgsql backend for %{name}

%description    backend-pipe
This package contains the pipe backend for %{name}

%description    backend-geo
This package contains the geo backend for %{name}
It allows different answers to DNS queries coming from different
IP address ranges or based on the geographic location

%description    backend-ldap
This package contains the ldap backend for %{name}

%description    backend-sqlite
This package contains the SQLite backend for %{name}

%ifarch %{ix86} x86_64
%description    backend-mongodb
This package contains the MongoDB backend for %{name}
%endif

%prep
%setup -q
%patch0 -p1 -b .fixmongo

%patch10 -p1

%build
export CPPFLAGS="-DLDAP_DEPRECATED %{optflags}"

%configure \
        --sysconfdir=%{_sysconfdir}/%{name} \
        --libdir=%{_libdir}/%{name} \
        --disable-static \
        --with-modules='' \
        --with-lua \
        --with-dynmodules='%{backends}' \
        --enable-cryptopp

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}

%{__rm} -f %{buildroot}%{_libdir}/%{name}/*.la
%{__mv} %{buildroot}%{_sysconfdir}/%{name}/pdns.conf{-dist,}

# add the pdns user to the config file
sed -i '1i\setuid=pdns' %{buildroot}%{_sysconfdir}/%{name}/pdns.conf
sed -i '2i\setgid=pdns' %{buildroot}%{_sysconfdir}/%{name}/pdns.conf

# strip the static rpath from the binaries
chrpath --delete %{buildroot}%{_bindir}/pdns_control
chrpath --delete %{buildroot}%{_bindir}/zone2ldap
chrpath --delete %{buildroot}%{_bindir}/zone2sql
chrpath --delete %{buildroot}%{_sbindir}/pdns_server
chrpath --delete %{buildroot}%{_libdir}/%{name}/*.so

# Copy systemd service file
install -p -D -m 644 %{SOURCE1} %{buildroot}%{_unitdir}/pdns.service


%pre
getent group pdns >/dev/null || groupadd -r pdns
getent passwd pdns >/dev/null || \
        useradd -r -g pdns -d / -s /sbin/nologin \
        -c "PowerDNS user" pdns
exit 0 


%post
if [ $1 -eq 1 ]; then
        # Initial installation
        /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi


%preun
if [ $1 -eq 0 ]; then
        # Package removal; not upgrade
        /bin/systemctl --no-reload disable pdns.service &>/dev/null || :
        /bin/systemctl stop pdns.service &>/dev/null || :
fi


%postun 
/bin/systemctl daemon-reload &>/dev/null || :
if [ $1 -ge 1 ]; then
        # Package upgrade; not install
        /bin/systemctl try-restart pdns.service &>/dev/null || :
fi


%triggerun -- pdns < 3.0-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply pdns
# to migrate them to systemd targets
%{_bindir}/systemd-sysv-convert --save pdns &>/dev/null ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del pdns &>/dev/null || :
/bin/systemctl try-restart pdns.service &>/dev/null || :


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/dnsreplay
%{_bindir}/pdns_control
%{_bindir}/pdnssec
%{_bindir}/zone2ldap
%{_bindir}/zone2sql
%{_sbindir}/pdns_server
%{_mandir}/man8/pdns_control.8.*
%{_mandir}/man8/pdns_server.8.*
%{_mandir}/man8/zone2sql.8.*
%{_unitdir}/pdns.service
%dir %{_libdir}/%{name}/
%dir %{_sysconfdir}/%{name}/
%config(noreplace) %{_sysconfdir}/%{name}/pdns.conf

%files backend-mysql
%defattr(-,root,root,-)
%{_libdir}/%{name}/libgmysqlbackend.so

%files backend-postgresql
%defattr(-,root,root,-)
%{_libdir}/%{name}/libgpgsqlbackend.so

%files backend-pipe
%defattr(-,root,root,-)
%{_libdir}/%{name}/libpipebackend.so

%files backend-geo
%defattr(-,root,root,-)
%doc modules/geobackend/README
%{_libdir}/%{name}/libgeobackend.so

%files backend-ldap
%defattr(-,root,root,-)
%{_libdir}/%{name}/libldapbackend.so

%files backend-sqlite
%defattr(-,root,root,-)
%{_libdir}/%{name}/libgsqlite3backend.so

%ifarch %{ix86} x86_64
%files backend-mongodb
%defattr(-,root,root,-)
%{_libdir}/%{name}/libmongodbbackend.so
%endif


%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-2m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-1m)
- update to 3.1
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-4m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- rebuild for mongodb 2.0.4 and boost 1.50.0

* Mon Mar 19 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.1-2m)
- add backend-mongodb subpackage

* Fri Jan 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- [SECURITY] CVE-2012-0206
- update to 3.0.1

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-2m)
- rebuild for boost-1.48.0

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-1m)
- update 3.0
- support systemd

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-0.2.2m)
- rebuild for boost

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.2.1m)
- update
- delete patch1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-14m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.22-13m)
- rebuild against mysql-5.5.10

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-12m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-11m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-10m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-9m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.22-8m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.22-7m)
- rebuild against openssl-1.0.0

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.22-6m)
- rebuild against postgresql-8.4.2-1m

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.22-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.22-3m)
- define __libtoolize

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.22-2m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.22-1m)
- update

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.21.2-5m)
- rebuild against mysql-5.1.32

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.21.2-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.21.2-3m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.21.2-2m)
- fix build (from NetBSD fixes)

* Thu Dec 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.21.2-1m)
- [SECURITY] CVE-2008-5277
- update to 2.9.21.2
-- update Patch1

* Thu Sep 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.21.1-1m)
- [SECURITY] CVE-2008-3337 
- http://doc.powerdns.com/powerdns-advisory-2008-02.html

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.21-7m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.21-6m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.21-5m)
- rebuild against openldap-2.4.8

* Wed Feb 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.21-4m)
- import gcc43 patch from Fedora devel

* Wed Jan 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.21-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.21-2m)
- Add README for geo backend to docs (fedora 2.9.21-2 changes)

* Fri Oct  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.21-1m)
- import to Momonga from fedora

* Tue Apr 24 2007 Ruben Kerkhof <ruben@rubenkerkhof.com> 2.9.21-1
- Upstream released 2.9.21
- Enabled new SQLite backend
* Thu Apr 10 2007 <ruben@rubenkerkhof.com> 2.9.20-9
- Add Requires for chkconfig, service and useradd (#235582)
* Mon Jan 1 2007 <ruben@rubenkerkhof.com> 2.9.20-8
- Add the pdns user and group to the config file
- Don't restart pdns on an upgrade
- Minor cleanups in scriptlets
* Mon Jan 1 2007 <ruben@rubenkerkhof.com> 2.9.20-7
- Fixed typo in scriptlet
* Mon Jan 1 2007 <ruben@rubenkerkhof.com> 2.9.20-6
- Check if user pdns exists before adding it
* Sat Dec 30 2006 <ruben@rubenkerkhof.com> 2.9.20-5
- Strip rpath from the backends as well
* Fri Dec 29 2006 <ruben@rubenkerkhof.com> 2.9.20-4
- Disable rpath
* Thu Dec 28 2006 <ruben@rubenkerkhof.com> 2.9.20-3
- More fixes as per review #219973
* Wed Dec 27 2006 <ruben@rubenkerkhof.com> 2.9.20-2
- A few changes for FE review (bz #219973):
- Renamed package to pdns, since that's how upstream calls it
- Removed calls to ldconfig
- Subpackages now require %%{version}-%%{release}
* Sat Dec 16 2006 <ruben@rubenkerkhof.com> 2.9.20-1
- Initial import

