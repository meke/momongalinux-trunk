%global momorel 1

%global srcname gst-ffmpeg

Name: gstreamer-ffmpeg
Version: 0.10.13
Release: %{momorel}m%{?dist}
Summary: GStreamer plugin for FFmpeg

Group: Applications/Multimedia
License: GPL
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer >= 0.10.25

# plugins
BuildRequires: gstreamer-devel >= 0.10.36
BuildRequires: ffmpeg-devel 
BuildRequires: x264-devel >= 0.0.721-0.20080503
# documentation
BuildRequires:  gtk-doc
Obsoletes: gst-ffmpeg

%description
GStreamer is a streaming-media framework, based on graphs of filters which
operate on media data. Applications using this library can do anything
from real-time sound processing to playing videos, and just about anything
else media-related. Its plugin-based architecture means that new data
types or processing capabilities can be added simply by installing new
plug-ins.

This plugin contains the FFmpeg codecs, containing codecs for most popular
multimedia formats.

%prep
%setup -q -n %{srcname}-%{version}

%build
./configure \
    --prefix=%{_prefix} \
    --libdir=%{_libdir} \
    --enable-static=no \
    --enable-gtk-doc \
    --enable-docbook
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install


%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
# plugins
%{_libdir}/gstreamer-0.10/libgstffmpeg.so
%{_libdir}/gstreamer-0.10/libgstpostproc.so
%{_libdir}/gstreamer-0.10/libgstffmpegscale.so
%{_libdir}/gstreamer-0.10/*.la
# document
# %{_datadir}/gtk-doc/html/gst-ffmpeg-plugins-0.10

%changelog
* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.11-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.9-1m)
- update to 0.10.9

* Wed Aug  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Thu Apr 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.6-2m)
- rebuild against rpm-4.6

* Sat Nov 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-2m)
- specify --libdir at configure for x86_64

* Mon Oct 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.4-2m)
- rename gst-ffmpeg to gstreamer-ffmpeg

* Wed May 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4
- does not use %%configure
- delete --with-system-ffmpeg

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-10m)
- rebuild against x264

* Tue Apr  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.3-5m)
- add --with-system-ffmpeg for gcc43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.3-4m)
- rebuild against gcc43

* Tue Mar 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-3m)
- delete --with-system-ffmpeg flag for configure

* Sat Mar 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.3-2m)
- switch to use system ffmpeg
-- add ffmpeg-devel
-- import patch1 from debian

* Sun Jan  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Sat Dec 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sat Nov 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- initial build
