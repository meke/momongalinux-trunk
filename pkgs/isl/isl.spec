%global         momorel 1

Name:           isl
Version:        0.10
Release:        %{momorel}m%{?dist}
Summary:        manipulating sets and relations of integer points bounded by linear constraints.
Group:          System Environment/Libraries
License:        LGPL
URL:		http://www.kotnet.org/~skimo/isl/
Source0:	ftp://ftp.linux.student.kuleuven.be/pub/people/skimo/isl/isl-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gmp-devel >= 5.0.2

Conflicts:	cloog < 0.17.0-1m

#Requires(post): info
#Requires(preun): info

%description
isl is a library for manipulating sets and relations of integer points
bounded by linear constraints. Supported operations on sets include
intersection, union, set difference, emptiness check, convex hull,
(integer) affine hull, integer projection, and computing the
lexicographic minimum using parametric integer programming. It also
includes an ILP solver based on generalized basis reduction.

%package devel
Summary:        Development tools for the isl
Group:          Development/Libraries
Requires: 	gmp-devel >= 5.0.2
Conflicts:	cloog-devel < 0.17.0-1m

%description devel
The header files and dynamic shared libraries of isl

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/libisl.so.*gdb.*

%check
make check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/isl
%{_includedir}/isl/*
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/pkgconfig/*.pc

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%changelog
* Sun Sep  2 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-1m)
- initial version
