%global momorel 9

%global xfce4ver 4.8.0
%global thunarver 1.2.0

Summary:        Thunar file manager extension to share files using Samba
Name:           thunar-shares-plugin
Version:        0.2.0
Release:        %{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/thunar-plugins/%{name}
Source0:	http://archive.xfce.org/src/thunar-plugins/%{name}/0.2/%{name}-%{version}.tar.gz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  Thunar-devel >= %{thunarver}
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  intltool, gettext
Requires:       Thunar >= %{thunarver}
Requires:       samba >= 3.0.22

# The provides is supposed to be removed in F12
Provides:       thunar-shares = 0.16-2
Obsoletes:      thunar-shares < 0.16-2

%description
The Thunar Shares plugin allows you to quickly share a folder using Samba from
Thunar (the Xfce file manager) without requiring root access. It's  backend is
based on nautilus-share.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{_libdir}/thunarx-1/%{name}.la
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_libdir}/thunarx-1/%{name}.so


%changelog
* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-9m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-8m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-4m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-1m)
- import from Fedora to Momonga

* Mon Feb 16 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Initial Fedora RPM based on the former thunar-shares package
