%global momorel 4

Name: hunspell-nl
Summary: Dutch hunspell dictionaries
Version: 1.10
Release: %{momorel}m%{?dist}
Source: http://www.opentaal.org/bestanden/1_10/nl-dict.oxt
Group: Applications/Text
URL: http://www.opentaal.org/english.php
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: BSD or "CC-BY"
BuildArch: noarch

Requires: hunspell

%description
Dutch hunspell dictionaries.

%prep
%setup -q -c

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
nl_NL_aliases="nl_AW nl_BE"
for lang in $nl_NL_aliases; do
        ln -s nl_NL.aff $lang.aff
        ln -s nl_NL.dic $lang.dic
done

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc description/desc_en_US.txt description/desc_nl_NL.txt README_EN.txt README_NL.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-1m)
- sync with Fedora 13 (1.10-2)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.00g-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.00g-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.00g-1m)
- import from Fedora to Momonga

* Fri Jun 08 2007 Caolan McNamara <caolanm@redhat.com> - 1.00g-1
- OpenTaal project publishes Dutch Language Union approved dictionary

* Wed Feb 14 2007 Caolan McNamara <caolanm@redhat.com> - 0.20050720-1
- update to match upstream id

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20050617-1
- initial version
