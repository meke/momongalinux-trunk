%global momorel 1
%global ocamlver 3.12.1

%global __os_install_post /usr/lib/rpm/momonga/brp-compress %{nil}
%global __ocaml_requires_opts -i Decl_expr -i Fol -i Miniml -i Sos
%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)

Summary: The Coq Proof Assistant
Name: coq
Version: 8.3pl2
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: Applications/Engineering
URL: http://coq.inria.fr/
Source0: http://coq.inria.fr/distrib/V%{version}/files/coq-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-camlp5-devel >= 6.02.3-1m
BuildRequires: ocaml-lablgtk-devel >= 2.14.0
BuildRequires: ncurses-devel
Obsoletes: coq-xemacs

%description
Coq is a Proof Assistant for a Logical Framework known as the Calculus
of Inductive Constructions. It allows the interactive construction of
formal proofs, and also the manipulation of functional programs
consistently with their specifications.

%package -n emacs-coq-mode
Group: Applications/Engineering
Summary: Emacs mode for Coq
License: GPLv2+
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
Obsoletes: coq-emacs
BuildArch: noarch
Obsoletes: elisp-coq-mode

%description -n emacs-coq-mode
Emacs mode for Coq.

%prep
%setup -q

chmod -x tools/README.coq-tex

%build
# Define opt flag based upon prior opt detection and restrictions
%if %{opt}
%global opt_option --opt
%else
%global opt_option --byte-only
%endif

./configure -prefix %{_prefix}     \
            -libdir %{_libdir}/coq \
            -bindir %{_bindir}     \
            -mandir %{_mandir}     \
            -docdir /dev/null      \
            -emacslib %{_emacs_sitelispdir}/coq \
            -coqdocdir %{_datadir}/texmf/tex/latex/misc \
            %{opt_option} \
            -browser "xdg-open %s" \
            -camlp5dir %{_libdir}/ocaml/camlp5

make world

%install
rm -rf %{buildroot}

make COQINSTALLPREFIX=%{buildroot} install-coq
make COQINSTALLPREFIX=%{buildroot} install-coqide

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES COPYRIGHT CREDITS INSTALL INSTALL.ide
%doc LICENSE README tools/README.coq-tex
%{_bindir}/*
%{_libdir}/coq
%{_mandir}/man1/*
%{_datadir}/texmf/tex/latex/misc/*

%files -n emacs-coq-mode
%defattr(-,root,root,-)
%doc tools/README.emacs
%dir %{_emacs_sitelispdir}/coq
%{_emacs_sitelispdir}/coq

%changelog
* Mon Dec 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.3pl2-1m)
- update to 8.3pl2

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2pl2-3m)
- rename elisp-coq-mode to emacs-coq-mode

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2pl2-2m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2pl2-1m)
- update to 8.2pl2
- enable to build with GNU Make 3.82

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2pl1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.2pl1-3m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2pl1-2m)
- rebuild against emacs-23.2

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2pl1-1m)
- update to 8.2pl1
- rebuild against ocaml-3.11.2
- rebuild against ocaml-camlp5-5.13-1m

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-11m)
- modify __os_install_post for new momonga-rpmmacros

* Fri Mar 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-10m)
- rename coq-emacs to elisp-coq-mode
- kill coq-xemacs

* Wed Mar 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-9m)
- ignore some modules

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2-7m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2-6m)
- rebuild against emacs 23.0.96

* Sat Jun 27 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (8.2-5m)
- version up 8.2-1

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-4m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-3m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-2m)
- rebuild against xemacs-21.5.29

* Tue Feb 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-1m)
- update to 8.2
- rebuild against ocaml-lablgtk-2.12.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-0.0.20081202.2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2-0.0.20081202.1m)
- update to svn snapshot (r11645)
- rebuild against ocaml-3.11.0

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1pl3-3m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1pl3-2m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1pl3-1m)
- update to 8.1pl3
- rebuild against ocaml 3.10.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.1pl2-2m)
- %%NoSource -> NoSource
 
* Wed Oct 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1pl2-1m)
- update to 8.1pl2

* Thu Oct 19 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0pl3-1m)
- first packaging
