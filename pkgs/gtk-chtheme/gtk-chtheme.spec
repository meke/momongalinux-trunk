%global momorel 8

Summary: A program to change gtk+ 2.0 theme
Name:    gtk-chtheme
Version: 0.3.1
Release: %{momorel}m%{?dist}
Group:   User Interface/Desktops
License: GPL
URL:     http://plasmasturm.org/code/gtk-chtheme/  
Source0: http://plasmasturm.org/code/gtk-chtheme/gtk-chtheme-%{version}.tar.bz2
NoSource: 0
Patch0: gtk-chtheme-0.3.1-gtkobject.patch
Patch1: %{name}-%{version}-fix-build.patch
Requires: gtk2
BuildRequires: gtk2-devel 
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
As the name suggests, this little program lets you change your gtk+ 2.0 theme. The aim is to make theme preview and selection as slick as possible. Themes installed on the system are presented for selection and previewed on the fly.

This program makes and rewrites ~/.gtkrc-2.0.

%prep
%setup -q
%patch0 -p1 -b .gtkobject
%patch1 -p1 -b .fix-build

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 gtk-chtheme %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog COPYING
%{_bindir}/*

%changelog
* Thu Apr 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-8m)
- add a patch to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-6m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-5m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-2m)
- rebuild against rpm-4.6

* Wed May 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.1-1m)
- import to Momonga
