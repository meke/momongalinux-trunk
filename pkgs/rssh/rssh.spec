%global momorel 1

Summary: a restricted shell for scp or sftp
Name: rssh
Version: 2.3.4
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Shells
Source0: http://dl.sourceforge.net/sourceforge/rssh/rssh-%{version}.tar.gz 
NoSource: 0
Patch0: rssh-2.3.4-git.patch
URL: http://www.pizzashack.org/rssh/
Requires: openssh
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
rssh is a restricted shell for use with ssh, which allows the system
administrator to restrict a user's access to a system via scp or sftp, or
both.

%prep
%setup -q
%patch0 -p1 -b .git

%build
%configure --with-sftp-server=/usr/libexec/ssh/sftp-server
%make 

%install
%{__rm} -rf %{buildroot}
%makeinstall

%files
%defattr(644, root, root, 0755)
%doc AUTHORS ChangeLog CHROOT COPYING README SECURITY TODO conf_convert.sh mkchroot.sh
%doc %{_mandir}/man?/*
%config(noreplace) %{_sysconfdir}/rssh.conf.default
%attr(755, root, root) %{_bindir}/rssh
%attr(4755, root, root) %{_libexecdir}/rssh_chroot_helper

%clean
%{__rm} -rf %{buildroot}

%changelog
* Sun Jan 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.4-1m)
- [SECURITY] CVE-2012-2251 CVE-2012-2252
- update to 2.3.4

* Fri Aug 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-9m)
- [SECURITY] CVE-2012-3478

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-2m)
- %%NoSource -> NoSource

* Thu Aug 17 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.3.2-1m)
- welcome to momonga
 

