%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global akonadi_version 1.12.0
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name:    baloo-widgets
Summary: Widgets for Baloo
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2
Group:   User Interface/Desktops
URL:     https://projects.kde.org/projects/kde/kdelibs/baloo-widgets
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
## upstream patches

BuildRequires: baloo-devel >= %{version}
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kfilemetadata-devel >= %{version}
Requires: baloo-libs >= %{version}

%description
%{summary}.

%package devel
Summary:  Developer files for %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: baloo-devel

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_kde4_libdir}/libbaloowidgets.so.4*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/baloo/*.h
%{_kde4_libdir}/cmake/BalooWidgets
%{_kde4_libdir}/libbaloowidgets.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- initial build (KDE 4.13 RC)
