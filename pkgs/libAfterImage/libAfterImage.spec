%global		momorel 2
Name:           libAfterImage
Version:        1.20
Release:        %{momorel}m%{?dist}
Summary:        A generic image manipulation library

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.afterstep.org/afterimage/index.php
Source0:        ftp://ftp.afterstep.org/stable/libAfterImage/libAfterImage-%{version}.tar.bz2
NoSource:	0
Source1:        libAfterImage-COPYING
Patch0:         libAfterImage-Makefile-ldconfig.patch
Patch1:         libAfterImage-afterimage-config.patch
Patch2:         libAfterImage-multiarch.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  freetype-devel
BuildRequires:  zlib-devel
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  libpng-devel
BuildRequires:  libungif-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  gawk

%description
libAfterImage   is a generic image manipulation library. It was initially
implemented to address AfterStep Window Manager's needs for image handling,
but it evolved into extremely powerful and flexible software, suitable for
virtually any project that has needs for loading, manipulating, displaying
images, as well as writing images in files. Most of the popular image formats
are supported using standard libraries, with XCF, XPM, PPM/PNM, BMP, ICO,
TGA and GIF being supported internally.

PNG, JPEG and TIFF formats are supported via standard libraries.

Powerful text rendering capabilities included, providing support for
TrueType fonts using FreeType library, and antialiasing of standard fonts
from X window system. 

%package devel
Summary:  Files needed for software development with %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: freetype-devel
Requires: zlib-devel
Requires: libtiff-devel
Requires: libjpeg-devel
Requires: libpng-devel
Requires: libX11-devel
Requires: libXext-devel
Requires: libICE-devel
Requires: libSM-devel
Requires: mesa-libGL-devel

%description devel
The %{name}-devel package contains the files needed for development with
%{name}

%prep
%setup -q
%patch0 
%patch1
%patch2 -b multiarch

%build
%configure --enable-i18n --enable-sharedlibs \
--with-xpm --without-builtin-ungif --disable-staticlibs --enable-glx \
--without-afterbase --disable-mmx-optimization \
--x-includes=%{_includedir} --x-libraries=%{_libdir}

make CCFLAGS="-DNO_DEBUG_OUTPUT -fPIC $RPM_OPT_FLAGS" %{?_smp_mflags} \
LIBAFTERIMAGE_PATH=../


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_includedir}
make install DESTDIR=%{buildroot} LIBAFTERIMAGE_PATH=../

cp %{SOURCE1} %{_builddir}/%{name}-%{version}/COPYING

touch -r ChangeLog %{buildroot}%{_bindir}/afterimage-{config,libs}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README ChangeLog COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/*
%{_includedir}/libAfterImage/
%{_libdir}/*.so

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-2m)
- rebuild against libtiff-4.0.1

* Mon Aug 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20-1m)
- import from fedora
