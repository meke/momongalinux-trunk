%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global sopranover 2.9.4
%global strigiver 0.7.8
%global shared_desktop_ontologies_version 0.11.0

Summary:       Nepomuk Core utilities and libraries
Name:          nepomuk-core
Version:       %{kdever}
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         User Interface/Desktops
URL:           http://www.kde.org/
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:      0
Source1:       nepomuk-inotify.conf
## upstream patches
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:      kdelibs >= %{version}-%{kdelibsrel}
Requires:      %{name}-libs = %{version}-%{release}
Requires:      shared-desktop-ontologies >= %{shared_desktop_ontologies_version}
Requires:      soprano >= %{soprano_version}
Requires:      virtuoso-opensource
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: dbus-x11
BuildRequires: exiv2-devel >= 0.23
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: kdelibs-devel >= %{version}-%{kdelibsrel}
BuildRequires: pkgconfig
BuildRequires: qmobipocket-devel >= 4.12.97-4m
BuildRequires: poppler-qt4-devel >= 0.22
BuildRequires: shared-desktop-ontologies-devel >= %{shared_desktop_ontologies_version}
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: taglib-devel
BuildRequires: strigi-devel >= %{strigiver}
BuildRequires: virtuoso-opensource
Conflicts:     kde-runtime < 4.8.97

%description
%{summary}.

%package devel
Group:    Development/Libraries
Summary:  Developer files for %{name}
Requires: kdelibs-devel
Requires: %{name}-libs = %{version}-%{release}

%description devel
%{summary}.

%package libs
Summary: Runtime libraries for %{name}
Group:   System Environment/Libraries
Requires: kdelibs >= %{version}
Requires: %{name} = %{version}-%{release}

%description libs
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

install -p -m644 -D %{SOURCE1} %{buildroot}%{_sysconfdir}/sysctl.d/nepomuk-inotify.conf

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ontologies/README
%config(noreplace) %{_sysconfdir}/sysctl.d/nepomuk-inotify.conf
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/org.kde.nepomuk.filewatch.conf
%{_kde4_appsdir}/fileindexerservice
%{_kde4_appsdir}/nepomukfilewatch
%{_kde4_appsdir}/nepomukstorage
%{_kde4_bindir}/nepomuk2-rcgen
%{_kde4_bindir}/nepomuk-simpleresource-rcgen
%{_kde4_bindir}/nepomukbackup
%{_kde4_bindir}/nepomukbaloomigrator
%{_kde4_bindir}/nepomukcleaner
%{_kde4_bindir}/nepomukcmd
%{_kde4_bindir}/nepomukctl
%{_kde4_bindir}/nepomukfileindexer
%{_kde4_bindir}/nepomukfilewatch
%{_kde4_bindir}/nepomukindexer
%{_kde4_bindir}/nepomukmigrator
%{_kde4_bindir}/nepomuksearch
%{_kde4_bindir}/nepomukserver
%{_kde4_bindir}/nepomukservicestub
%{_kde4_bindir}/nepomukshow
%{_kde4_bindir}/nepomukstorage
%{_kde4_libdir}/libkdeinit4_nepomukserver.so
%{_kde4_libexecdir}/kde_nepomuk_filewatch_raiselimit
%{_kde4_datadir}/applications/kde4/nepomukbackup.desktop
%{_kde4_datadir}/applications/kde4/nepomukcleaner.desktop
%{_kde4_datadir}/autostart/nepomukbaloomigrator.desktop
%{_kde4_datadir}/autostart/nepomukserver.desktop
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/servicetypes/nepomukcleaningjob.desktop
%{_kde4_datadir}/kde4/servicetypes/nepomukextractor.desktop
%{_kde4_datadir}/kde4/servicetypes/nepomukservice.desktop
%{_kde4_datadir}/kde4/servicetypes/nepomukservice2.desktop
%{_kde4_datadir}/ontology/kde
%{_datadir}/dbus-1/interfaces/*.xml
%{_datadir}/dbus-1/system-services/org.kde.nepomuk.filewatch.service
%{_datadir}/polkit-1/actions/org.kde.nepomuk.filewatch.policy

%files devel
%defattr(-,root,root,-)
%{_kde4_libdir}/libnepomukcleaner.so
%{_kde4_libdir}/libnepomukcore.so
%{_kde4_libdir}/cmake/NepomukCore
%{_kde4_includedir}/nepomuk2
%{_kde4_includedir}/Nepomuk2

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/libnepomukcleaner.so.*
%{_kde4_libdir}/libnepomukcommon.so
%{_kde4_libdir}/libnepomukcore.so.*
%{_kde4_libdir}/libnepomukextractor.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.12.1-2m)
- rebuild against ffmpeg

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Sat Aug  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9.0-2m)
- fix build failrue

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- initial build (KDE 4.9 RC2)
