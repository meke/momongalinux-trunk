%global momorel 4

Summary: Utility to copy DVD .vob files to disk
Name: vobcopy
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://vobcopy.org/
Source0: http://vobcopy.org/download/vobcopy-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libdvdread-devel >= 4.1.2
Requires: libdvdread

%description
Vobcopy copies DVD .vob files to disk and merges them into file(s)
with the name extracted from the DVD. There is one drawback though: at
the moment vobcopy doesn't deal with multi-angle-dvd's. But since
these are rather sparse this shouldn't matter much.

%prep
%setup -q

%build
./configure.sh \
    --prefix=%{_prefix} \
    --mandir=%{_mandir} \
    --with-lfs

make CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

rm -rf %{buildroot}%{_defaultdocdir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING Changelog README Release-Notes TODO
%doc alternative_programs.txt
%{_bindir}/vobcopy
%{_mandir}/man1/vobcopy.1*
%lang(de) %{_mandir}/de/man1/vobcopy.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- full rebuild for mo7 release

* Sun May 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-4m)
- rebuild against libdvdread-4.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- %%NoSource -> NoSource

* Thu Apr 19 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.1-1m)
- import from freshrpms.net

* Mon Nov 27 2006 Matthias Saou <http://freshrpms.net/> 1.0.1-1
- Update to 1.0.1.
- Remove no longer needed gcc change in the Makefile patch.

* Tue Apr 18 2006 Matthias Saou <http://freshrpms.net/> 1.0.0-1
- Update to 1.0.0.
- Add s/gcc-3.4/gcc/ to the Makefile patch.

* Mon Mar 27 2006 Matthias Saou <http://freshrpms.net/> 0.5.16-1
- Major spec file cleanup.

* Fri Jan 6 2006 Robos  <robos@muon.de>
- 0.5.16: -see changelog

* Fri Jul 29 2005 Robos  <robos@muon.de>
- 0.5.15: -option to skip already present files with -m.
  	  copying of dvd's with files ending in ";?" should work now.

* Sun Oct 24 2004 Robos  <robos@muon.de>
- 0.5.14-rc1: - misc *bsd fixes and first straight OSX support

* Mon Mar 7 2004 Robos  <robos@muon.de>
- 0.5.12-1: -m off-by-one error fixed

* Mon Jan 19 2004 Robos <robos@muon.de>
- 0.5.10-1: -O now works
  	    cleanup



* Wed Nov 13 2003 Robos <robos@muon.de>
- 0.5.9-1: -F now accepts factor number
  	   cleanups and small bugfix
  	   new vobcopy.spec

* Sun Nov 09 2003 Florin Andrei <florin@andrei.myip.org>
- 0.5.8-2: libdvdread is now a pre-requisite

* Sun Nov 09 2003 Florin Andrei <florin@andrei.myip.org>
- first package, 0.5.8-1
