%global         momorel 1

Name:           seabios
Version:        1.7.5
Release:        %{momorel}m%{?dist}
Summary:        Open-source legacy BIOS implementation

Group:          Applications/Emulators
License:        LGPLv3
URL:            http://www.seabios.org/SeaBIOS
Source0:        http://code.coreboot.org/p/seabios/downloads/get/%{name}-%{version}.tar.gz
NoSource:	0

Source10:       config.vga.cirrus
Source11:       config.vga.isavga
Source12:       config.vga.qxl
Source13:       config.vga.stdvga
Source14:       config.vga.vmware
Source15:       config.csm
Source16:       config.coreboot
Source17:       config.seabios-128k
Source18:       config.seabios-256k

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: binutils >= 2.21.51.0.9
BuildRequires: python
ExclusiveArch: %{ix86} x86_64

Requires: %{name}-bin = %{version}-%{release}
Requires: seavgabios-bin = %{version}-%{release}

# Seabios is noarch, but required on architectures which cannot build it.
# Disable debuginfo because it is of no use to us.
%global debug_package %{nil}

# Similarly, tell RPM to not complain about x86 roms being shipped noarch
%global _binaries_in_noarch_packages_terminate_build   0

%description
SeaBIOS is an open-source legacy BIOS implementation which can be used as
a coreboot payload. It implements the standard BIOS calling interfaces
that a typical x86 proprietary BIOS implements.

%package bin
Summary: Seabios for x86
Buildarch: noarch

%description bin
SeaBIOS is an open-source legacy BIOS implementation which can be used as
a coreboot payload. It implements the standard BIOS calling interfaces
that a typical x86 proprietary BIOS implements.

%package -n seavgabios-bin
Summary: Seavgabios for x86
Buildarch: noarch
Obsoletes: vgabios
Provides: vgabios

%description -n seavgabios-bin
SeaVGABIOS is an open-source VGABIOS implementation.


%prep
%setup -q

# Makefile changes version to include date and buildhost
sed -i 's,VERSION=%{version}.*,VERSION=%{version},g' Makefile


%build
export CFLAGS="$RPM_OPT_FLAGS"
mkdir binaries

build_bios() {
    make clean distclean
    cp $1 .config
    echo "CONFIG_DEBUG_LEVEL=%{debug_level}" >> .config
    make oldnoconfig V=1

    make V=1 $4

    cp out/$2 binaries/$3
}

# seabios
build_bios %{SOURCE15} Csm16.bin bios-csm.bin
build_bios %{SOURCE16} bios.bin.elf bios-coreboot.bin
build_bios %{SOURCE17} bios.bin bios.bin
build_bios %{SOURCE18} bios.bin bios-256k.bin

cp out/src/fw/*dsdt*.aml binaries
# seavgabios
for config in %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14}; do
	name=${config#*config.vga.}
	make clean distclean
	cp ${config} .config
	echo "CONFIG_DEBUG_LEVEL=%{debug_level}" >> .config
	make oldnoconfig
	make V=1
	cp out/vgabios.bin binaries/vgabios-${name}.bin
done


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_datadir}/seabios
mkdir -p $RPM_BUILD_ROOT%{_datadir}/seavgabios
install -m 0644 binaries/bios.bin $RPM_BUILD_ROOT%{_datadir}/seabios/bios.bin
install -m 0644 binaries/bios-256k.bin $RPM_BUILD_ROOT%{_datadir}/seabios/bios-256k.bin
install -m 0644 binaries/bios-csm.bin $RPM_BUILD_ROOT%{_datadir}/seabios/bios-csm.bin
install -m 0644 binaries/bios-coreboot.bin $RPM_BUILD_ROOT%{_datadir}/seabios/bios-coreboot.bin
install -m 0644 binaries/*.aml $RPM_BUILD_ROOT%{_datadir}/seabios
install -m 0644 binaries/vgabios*.bin $RPM_BUILD_ROOT%{_datadir}/seavgabios

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc COPYING COPYING.LESSER README TODO

%files bin
%dir %{_datadir}/seabios/
%{_datadir}/seabios/bios*.bin
%{_datadir}/seabios/*.aml

%files -n seavgabios-bin
%dir %{_datadir}/seavgabios/
%{_datadir}/seavgabios/vgabios*.bin

%changelog
* Fri Jun 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.5-1m)
- update 1.7.5

* Sat Mar 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.4-3m)
- add bios config

* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.4-2m)
- split seavgabios,
- Obso vgabios

* Sun Dec 29 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.4-1m)
- update 1.7.4

* Fri Oct 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3.2-1m)
- update 1.7.3.2

* Wed Aug 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3.1-1m)
- update 1.7.3.1

* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3-1m)
- update 1.7.3

* Mon Jun 10 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2.2-1m)
- update 1.7.2.2

* Fri Sep 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-2m)
- add upstream patch.
-- support any devices

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update 1.7.0

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3.1-2m)
- add virtio-scsi patch

* Sun Jan  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3.1-1m)
- update 1.6.3.1

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- update 1.6.3

* Tue Aug  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-3m)
- add qxl patch

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-2m)
- add HEAD.patch. support Xen-HVM

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- seabios cannot be built with binutils-2.21.51.0.8...
- update to 0.6.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- sync with Fedora 13 (0.5.1-3)

* Tue Jan 26 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.5.1-0.1.20100108git669c991.1m)
- import from fedora devel

* Thu Jan 07 2010 Justin M. Forbes <jforbes@redhat.com> 0.5.1-0.1.20100108git669c991
- Created initial package

