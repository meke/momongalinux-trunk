%global momorel 3

# PECL(PHP Extension Community Library) related definitions
%{!?__pecl:     %{expand: %%global __pecl     %{_bindir}/pecl}}
%define pecl_name rpmreader

Summary: RPM file meta information reader
Name: php-pecl-rpmreader
Version: 0.4
Release: %{momorel}m%{?dist}
License: PHP
Group: Development/Languages
URL: http://pecl.php.net/package/%{pecl_name}
Source0: http://pecl.php.net/get/%{pecl_name}-%{version}.tgz
NoSource: 0
Patch0: %{name}-%{version}-php54.patch
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires: php-devel >= 5.4.1, php-pear >= 1.9.4
Requires: php >= 5.5.2
Requires(post): %{__pecl}
Requires(postun): %{__pecl}
%if 0%{?php_zend_api}
Requires: php(zend-abi) = %{php_zend_api}
Requires: php(api) = %{php_core_api}
%else
Requires: php-api = %{php_apiver}
%endif
Provides: php-pecl(%{pecl_name}) = %{version}-%{release}

%description
rpmreader is an extension that provides the ability to
read RedHat Package Manager (RPM) files' header information. This
extension currently does not provide the functionality to read the
signature or archive sections of the RPM file.

%prep
%setup -c -q
%patch0 -p0 -b .php54

%build
pushd %{pecl_name}-%{version}
%{_bindir}/phpize
%configure  --enable-rpmreader
make %{?_smp_mflags}
(echo "; Enable rpmreader extension module"
 echo "extension=rpmreader.so") > rpmreader.ini.dist
popd

%install
pushd %{pecl_name}-%{version}
rm -rf %{buildroot}
install -D -p -m 0755 modules/rpmreader.so %{buildroot}%{php_extdir}/rpmreader.so
install -D -p -m 0644 rpmreader.ini.dist %{buildroot}%{_sysconfdir}/php.d/rpmreader.ini.dist

# Install XML package description
%{__mkdir_p} %{buildroot}%{pecl_xmldir}
%{__install} -m 644 ../package.xml %{buildroot}%{pecl_xmldir}/%{name}.xml
popd

%clean
rm -rf %{buildroot}

%if 0%{?pecl_install:1}
%post
%{pecl_install} %{pecl_xmldir}/%{name}.xml >/dev/null || :
%endif

%if 0%{?pecl_uninstall:1}
%postun
if [ $1 -eq 0 ] ; then
    %{pecl_uninstall} %{pecl_name} >/dev/null || :
fi
%endif

%files
%defattr(-,root,root,-)
%doc %{pecl_name}-%{version}/LICENSE %{pecl_name}-%{version}/CREDITS
%doc %{pecl_name}-%{version}/examples/*
%config(noreplace) %{_sysconfdir}/php.d/rpmreader.ini.dist
%{php_extdir}/rpmreader.so
%{pecl_xmldir}/%{name}.xml

%changelog
* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-3m)
- rebuild against php-5.5.2

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-2m)
- rebuild against php-5.4.1

* Sun May 01 2011 Yasuo Ohgaki <yohgkai@momonga-linux.org>
- (0.4-1m)
- Initial release
