%global momorel 30

Summary: A classical treasure hunting game for SDL
Name: sdlscav
Version: 137
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Amusements/Games
URL: http://www.xdr.com/dash/scavenger.html
Source: http://www.xdr.com/dash/%{name}-%{version}.tgz
Nosource: 0
Source1: %{name}.desktop
Source2: %{name}.as
# convert from http://www.xdr.com/dash/scav.gif
Source3: %{name}-128.png
Patch0: newer%{name}.diff
Patch1: %{name}-137.diff
Patch2: %{name}-137-va_list.patch
Patch3: unresolved.resolve.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: SDL >= 1.2.0
BuildRequires: ImageMagick
BuildRequires: SDL-devel >= 1.2.0, audiofile-devel >= 0.2.1

%description
Scavenger is like Lode Runner. You've got to run around gathering
treasures while avoiding enemies. You can dig down through some of the
blocks to get at buried objects. After you've collected everything,
you've got to escape through the top of the screen. If an enemy falls
into a dug brick, he is stunned for a while. You can only kill guards
by making them fall in the blocks that are going to fill themselves up
again... but beware! If a guard gets killed, another guard is going to
reappear just around the corner to chase you! And beware! The pit monster 
is lurking somewhere waiting for its dinner: treasure hunter with chili!

This package uses the SDL library.

%prep
%setup -q
%patch0 -p 1
%patch1 -p 1
%patch2 -p 1
%patch3 -p1

%build
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# install sdlscav
mkdir -p %{buildroot}%{_datadir}/games/scavenger
install -c -D %{name} %{buildroot}%{_bindir}/%{name}
install -c -m 644 data/* %{buildroot}%{_datadir}/games/scavenger

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

mkdir -p %{buildroot}%{_datadir}/afterstep/start/Games
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/afterstep/start/Games

# install and convert icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64,128x128}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
convert -scale 16x16 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-,root,root)
%doc CREDITS DOC INSTALL README TODO
%{_bindir}/%{name}
%{_datadir}/afterstep/start/Games/%{name}.as
%{_datadir}/applications/%{name}.desktop
%{_datadir}/games/scavenger
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (137-30m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (137-29m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (137-28m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (137-27m)
- add icons for menu and fix up desktop file
- add %%post and %%postun

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (137-26m)
- fix Source1 (sdlscav.desktop)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (137-25m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (137-24m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (137-23m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2

* Tue May 20 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (137-22m)
- remove workaround patch for gcc43's bug (PR tree-optimization/36245)

* Tue Apr  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (137-21m)
- add workaround patch to avoid ICE with gcc43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (137-20m)
- rebuild against gcc43

* Wed Mar 23 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (137-19m)
- revised desktop file

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (137-18m)
- kill %%define name

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (137-17m)
  rebuild against DirectFB 0.9.18

* Wed Apr 18 2001 Kenichi Matsubara <m@digitalfactory.co.jp>
- (137-16k)
- rebuild against SDL-1.2.0.

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (137-15k)
- rebuild against audiofile-0.2.1.

* Thu Feb  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (137-13k)
- added /usr/share/afterstep/start/Games/sdlscav.as to %files section (#842)

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (137-11k)
- added sdlscav.as for afterstep

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (137-9k)
- imported sdlscav.desktop

* Thu Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (137-7k)
- build against audiofile-0.2.0

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (137-5k)
- added unresolved.resolve.patch

* Fri Nov 24 2000 Toru Hoshina <toru@df-usa.com>
- use va_list appropriate syntax.

* Fri Nov 10 2000 Kenichi Matsubara <m@kondara.org>
- bugfix specfile Source.

* Thu Oct 19 2000 Davide Inglima <cat@kondara.org>
- Used the patch for SDL 1.1 found on sdlscav's website
- Built against Jirai and FHS.

* Thu Aug 24 2000 Davide Inglima <cat@kondara.org>
- Created this specfile.
