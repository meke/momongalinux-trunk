%global         momorel 2

Name:           perl-Getopt-Long-Descriptive
Version:        0.097
Release:        %{momorel}m%{?dist}
Summary:        Getopt::Long, but simpler and more powerful
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Getopt-Long-Descriptive/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Getopt-Long-Descriptive-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Getopt-Long >= 2.33
BuildRequires:  perl-Params-Validate >= 0.97
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Sub-Exporter >= 0.972
BuildRequires:  perl-Test-Simple
Requires:       perl-Getopt-Long >= 2.33
Requires:       perl-Params-Validate >= 0.97
Requires:       perl-Scalar-Util
Requires:       perl-Sub-Exporter >= 0.972
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Getopt::Long::Descriptive is yet another Getopt library. It's built atop
Getopt::Long, and gets a lot of its features, but tries to avoid making you
think about its huge array of options.

%prep
%setup -q -n Getopt-Long-Descriptive-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.097-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.097-1m)
- update to 0.097

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.096-2m)
- rebuild against perl-5.18.2

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.096-1m)
- update to 0.096

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.095-1m)
- update to 0.095

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.094-1m)
- update to 0.094

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093-2m)
- rebuild against perl-5.16.2

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093-1m)
- update to 0.093

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.092-2m)
- rebuild against perl-5.16.1

* Wed Aug  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.092-1m)
- update to 0.092

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.091-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.091-1m)
- update to 0.091

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.090-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.090-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.090-3m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.090-2m)
- this package needs perl-Params-Varidate >= 0.97

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.090-1m)
- update to 0.090

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.089-2m)
- rebuild for new GCC 4.6

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.089-1m)
- update to 0.089

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.088-1m)
- update to 0.088

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.087-1m)
- update to 0.087

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.086-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.086-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.086-1m)
- update to 0.086

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.085-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.085-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.085-2m)
- rebuild against perl-5.12.0

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.085-1m)
- update to 0.085

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.084-1m)
- update to 0.084

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.083-1m)
- update to 0.083

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.082-1m)
- update to 0.082

* Sun Nov 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.081-1m)
- update to 0.081

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.077-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.077-1m)
- update to 0.077

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.074-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.074-3m)
- rebuild against perl-5.10.1

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.074-2m)
- modify BuildRequires

* Fri May 29 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.074-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
