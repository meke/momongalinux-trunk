%global momorel 5

Summary: A tiling, keyboard driven X11 Window Manager written entirely in Common Lisp
Name: stumpwm
Version: 0.9.7
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: GPLv2+
URL: http://www.nongnu.org/stumpwm/
Source0: http://ftp.twaren.net/Unix/NonGNU/stumpwm/stumpwm-%{version}.tgz
NoSource: 0
BuildRequires: clisp >= 2.48-2m
BuildRequires: texinfo
BuildRequires: atk-devel
BuildRequires: cairo-devel
BuildRequires: dbus-devel
BuildRequires: ffcall-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gdbm-devel
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: gtk2-devel
BuildRequires: libXau-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXpm-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libfcgi-devel
BuildRequires: libglade2-devel
BuildRequires: libpng-devel
BuildRequires: libsigsegv-devel >= 2.7
BuildRequires: libxml2-devel
BuildRequires: ncurses-devel
BuildRequires: pango-devel
BuildRequires: pcre-devel
BuildRequires: postgresql-devel
BuildRequires: readline-devel
BuildRequires: zlib-devel

%description
Stumpwm is a tiling, keyboard driven X11 Window Manager written
entirely in Common Lisp.

If you're tired of flipping through themes like channel-surfing, and
going from one perfect-except-for-just-one-thing window manager to
another even-more-broken-in-some-other-way then perhaps Stumpwm can
help.

Stumpwm attempts to be customizable yet visually minimal. There are no
window decorations, no icons, and no buttons. It does have various
hooks to attach your personal customizations, and variables to tweak.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/stumpwm.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/stumpwm.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README
%{_bindir}/stumpwm
%{_infodir}/stumpwm.info*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-4m)
- rebuild against readline6

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-3m)
- rebuild against libsigsegv-2.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-1m)
- initial packaging
