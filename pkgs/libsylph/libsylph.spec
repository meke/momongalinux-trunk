%global momorel 13

%global srcname %{name}-%{version}

# 1 to enable gpgme support
%global gpgme 1

# 1 to enable openldap support
%global openldap 1

# 1 to enable openssl support
%global openssl 1

# 1 to enable compface support
%global compface 0

# 1 to enable jpilot support
%global jpilot 0

# 1 to enable gtkspell support
%global gtkspell 0

Summary: LibSylph -- E-Mail client library
Name: libsylph

### default configuration
# If you'd like to change this configuration, please copy it to
# ${HOME}/rpm/specopt/sylpheed.specopt and edit it.

## Configuration
%{?!replace_icons:      %global replace_icons              1}

Version: 1.1.0
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://sylpheed.sraoss.jp/
Group: System Environment/Libraries
Source0: http://sylpheed.sraoss.jp/sylpheed/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gdk-pixbuf-devel >= 0.13.0
BuildRequires: glib1-devel
BuildRequires: gtk2-devel
BuildRequires: libX11-devel
BuildRequires: libXcursor-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libjconv
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRequires: readline-devel

%if %{gpgme}
Requires: gpgme
BuildRequires: gpgme-devel
BuildRequires: libgpg-error-devel
%endif

%if %{openldap}
Requires: cyrus-sasl
Requires: gdbm
Requires: openldap
Requires: pam
BuildRequires: cyrus-sasl-devel
BuildRequires: gdbm-devel
BuildRequires: openldap-devel >= 2.3.11
BuildRequires: pam-devel

%endif

%if %{openssl}
Requires: openssl
BuildRequires: openssl-devel >= 1.0.0
%endif

%if %{compface}
BuildRequires: faces-devel
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
LibSylph is an e-mail client library which is derived from Sylpheed.
LibSylph is a lightweight but featureful library. It has many common e-mail
related features and other useful functions, and you can utilize them from
your application. Moreover you can create a new e-mail client by wrapping
LibSylph with any UI.

%package devel
Summary: Header files and libraries for developing apps which will use libsylph.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The libsylph-devel package contains the header files and libraries needed
to develop programs that use the libsylph library.

Install the libsylph-devel package if you want to develop applications that
will use the libsylph library.


%prep
%setup -q -n %{srcname}

%build
%if ! %{_ipv6}
IPV6="--disable-ipv6"
%endif

%if %{openldap}
LDAP="--enable-ldap"
%endif

%if ! %{gpgme}
GPG="--disable-gpgme"
%endif

%if ! %{openssl}
OPENSSL="--disable-ssl"
%endif

%if ! %{compface}
FASE="--disable-compface"
%endif

%if %{jpilot}
JPILOT="--enable-jpilot"
%endif

%if ! %{gtkspell}
GTKSPELL="--disable-gtkspell"
%endif

%configure $OPENSSL $IPV6 $LDAP $GPG $FASE $JPILOT $GTKSPELL CPPFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/libsylph*.so.*
%{_datadir}/locale/*/*/*

%files devel
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL* NEWS README* TODO
%{_includedir}/sylph
%{_libdir}/libsylph*.so
%{_libdir}/libsylph*.a
%{_libdir}/libsylph*.la

%changelog
* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-13m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-10m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-9m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-6m)
- define __libtoolize :

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-4m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc43

* Tue Feb 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- %%NoSource -> NoSource

* Sat Sep  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- revise License (from GPL and LGPL to LGPL)

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- initial build for Momonga Linux
