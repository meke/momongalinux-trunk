%global momorel 1
Name: switchdesk
Summary: A desktop environment switcher
Version: 4.0.10
Release: %{momorel}m%{?dist}
Url: http://than.fedorapeople.org/
Source: http://than.fedorapeople.org/%{name}-%{version}.tar.bz2
NoSource: 0
License: GPLv2+
Group: User Interface/Desktops
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: python-devel
BuildRequires: desktop-file-utils

%description
The Desktop Switcher is a tool which enables users to easily switch
between various desktop environments that they have installed.

Support for different environments on different computers is available, as
well as support for setting a global default environment.

Install switchdesk if you need a tool for switching between desktop
environments.

%package gui
Group: User Interface/Desktops
Summary: A graphical interface for the Desktop Switcher
Requires: %{name} = %{version}-%{release}
Requires: python
Requires: pygtk2

%description gui
The switchdesk-gui package provides the graphical user interface for
the Desktop Switcher.

%prep

%setup -q

%build
%make 

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop
install -p -m 644 %{name}.desktop %{buildroot}%{_datadir}/applications/

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING
%dir %{_datadir}/%{name}
%{_bindir}/%{name}*
%{_datadir}/%{name}/Xclients*
%{_mandir}/man1/%{name}*
%lang(fr)%{_mandir}/fr/man1/%{name}*

%files gui -f %{name}.lang
%defattr(-,root,root)
%{_datadir}/%{name}/pixmaps
%{_datadir}/%{name}/*.glade
%{_datadir}/%{name}/*.py*
%{_datadir}/applications/*

%changelog
* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.10-1m)
- import from fedora

