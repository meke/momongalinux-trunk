%global	momorel 1
%define run_tests 1

Name:           python-rdflib
Version:        3.2.0
Release:        %{momorel}m%{?dist}
Summary:        Python library for working with RDF

Group:          Development/Languages
License:        BSD
URL:            http://code.google.com/p/rdflib/
Source0:        http://rdflib.googlecode.com/files/rdflib-%{version}.tar.gz
NoSource:	0
# Upstreamed: http://code.google.com/p/rdflib/issues/detail?id=206
Patch0:         0001-Skip-test-if-it-can-not-join-the-network.patch
BuildArch:      noarch

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Requires:       python-isodate

BuildRequires:  python-isodate
BuildRequires:  python-devel
%if 0%{?fedora} >= 8
BuildRequires: python-setuptools-devel
%else
BuildRequires: python-setuptools
%endif

%if %{run_tests}
BuildRequires:  python-nose >= 0.9.2
%endif

%description
RDFLib is a Python library for working with RDF, a simple yet powerful
language for representing information.

The library contains parsers and serializers for RDF/XML, N3, NTriples,
Turtle, TriX and RDFa. The library presents a Graph interface which can
be backed by any one of a number of store implementations, including
memory, MySQL, Redland, SQLite, Sleepycat, ZODB and SQLObject.

%prep
%setup -q -n rdflib-%{version}

%patch0 -p0 -b .test

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
cp LICENSE $RPM_BUILD_ROOT/%{python_sitelib}/rdflib/LICENSE

# Various .py files within site-packages have a shebang line but aren't
# flagged as executable.
# I've gone through them and either removed the shebang or made them
# executable as appropriate:

# __main__ parses URI as N-Triples:
chmod +x $RPM_BUILD_ROOT/%{python_sitelib}/rdflib/plugins/parsers/ntriples.py

# __main__ parses the file given on the command line:
chmod +x $RPM_BUILD_ROOT/%{python_sitelib}/rdflib/plugins/parsers/notation3.py


%check
%if %{run_tests}
sed -i -e "s|'--with-doctest'|#'--with-doctest'|" run_tests.py
%{__python} run_tests.py
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE
%{python_sitelib}/*

%changelog
* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-1m)
- import from fedora