%global momorel 10

Summary:        Library for SVG Files
Name:           libsvg
Version:        0.1.4
Release: %{momorel}m%{?dist}
License:        LGPL
Group:          System Environment/Libraries
URL:            http://www.cairographics.org/
Source0: http://cairographics.org/snapshots/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libjpeg-devel, libpng-devel, libxml2-devel, pkgconfig

%description
libsvg provides a parser for SVG content in files or buffers.


%package devel
Summary:        Library for SVG Files
Group:          Development/Libraries
Requires:       %{name} = %{version}
Requires:	libxml2-devel

%description devel
libsvg provides a parser for SVG content in files or buffers.


%prep
%setup -q

%build
%configure
%make

%install
rm -rf  %{buildroot}
%makeinstall

rm -f  %{buildroot}%{_libdir}/*.la

%clean
rm -rf  %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/svg.h


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-8m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-5m)
- define libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-2m)
- %%NoSource -> NoSource

* Fri Mar 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-1m)
- initial commit

