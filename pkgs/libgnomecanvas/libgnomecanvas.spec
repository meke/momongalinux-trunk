%global momorel 1

# Find a free display (resources generation requires X) and sets XDISPLAY
%define init_xdisplay XDISPLAY=2; while [ $XDISPLAY -lt 13 ]; do if [ ! -f /tmp/.X$XDISPLAY-lock ]; then sleep 2s; ( /usr/bin/Xvfb -ac :$XDISPLAY >& /dev/null & ); sleep 15s; if [ -f /tmp/.X$XDISPLAY-lock ]; then export DISPLAY=:$XDISPLAY; break ; fi; fi; XDISPLAY=$(($XDISPLAY+1)); done; if [ $XDISPLAY -ge 13 ]; then echo No free display found; exit 1; fi
# The virtual X server PID
%define kill_xdisplay kill $(cat /tmp/.X$XDISPLAY-lock)

Summary: An engine for structured graphics API.
Name: libgnomecanvas

Version: 2.30.3
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPLv2+
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.30/%{name}-%{version}.tar.bz2
NoSource: 0

Patch0: libgnomecanvas-1.113.0-alpha-valist.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: libart_lgpl-devel >= 2.3.20
BuildRequires: pango-devel >= 1.24.0
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: xorg-x11-server-Xvfb

%description
The GNOME canvas is an engine for structured graphics that offers a
rich imaging mode, high performance rendering, and a powerful,
high-level API.

%package devel
Summary: An engine for structured graphics API.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libglade-devel
Requires: libart_lgpl-devel

%description devel
The GNOME canvas is an engine for structured graphics that offers a
rich imaging mode, high performance rendering, and a powerful,
high-level API.

%prep
%setup -q
%ifarch alpha
%patch0 -p1 -b .valist
%endif

%build
%{init_xdisplay}
export DISPLAY=":$XDISPLAY"
%configure --enable-gtk-doc --disable-static
%make
%{kill_xdisplay}

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
  
%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING.LIB ChangeLog NEWS README
%{_libdir}/libgnomecanvas-2.so.*
%exclude %{_libdir}/*.la
%{_datadir}/locale/*/*/*

%files devel
%defattr(-, root, root)
%{_libdir}/libgnomecanvas-2.so
%{_libdir}/pkgconfig/libgnomecanvas-2.0.pc
%{_includedir}/libgnomecanvas-2.0
%doc %{_datadir}/gtk-doc/html/libgnomecanvas

%changelog
* Mon Aug  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.30.0-2m)
- use BuildRequires

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26.0-2m)
- fix build with new libtool

* Thu Mar 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.1.1-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.1.1-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.1.1-2m)
- rebuild against gcc43

* Sun Oct 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1.1-1m)
- update to 2.20.1.1

* Sun Oct 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-2m)
- fix %%post script

* Sun Oct 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- delete libtool library

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-5m)
- revise for xorg-7.0
- revise init_xdisplay macro

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-4m)
- comment out unnessesaly autoreconf and make check

* Sat Nov 19 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.0-3m)
- use Xvfb
- BuildRequires: xorg-x11-Xvfb

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-2m)
- enable gtk-doc

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up.
- GNOME 2.12.1 Desktop

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1.1-1m)
- version 2.6.1.1

* Tue May  4 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-4m)
- remove --disable-gtk-doc

* Tue Apr 27 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-3m)
- add --disable-gtk-doc

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-2m)
- adjustment BuildPreReq

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Fri Mar 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.2-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.2-1m)
- version 2.2.0.2

* Fri Jan 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90-1m)
- version 2.1.90

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-8m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-7m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.0-6k)
- rebuild against for libglade-2.0.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.0-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.0-2k)
- version 1.117.0

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-2k)
- version 1.116.0

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-2k)
- version 1.115.0

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-6k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-2k)
- version 1.114.0

* Fri Mar 22 2002 Toru Hoshina <t@kondara.org>
- (1.113.0-4k)
- add alpha support... dassa......

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-2k)
- version 1.113.0

* Sun Mar 17 2002 Toru Hoshina <t@kondara.org>
- (1.112.1-16k)
- * Thu Mar  7 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- - (1.112.1-6k)
- - add BuildPrereq: libart_lgpl >= 2.3.8
- why...?

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-14k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-12k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-10k)
- rebuild against for pango-1.0.0.rc2

* Fri Mar  8 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-8k)
- glib2 gtk+2 to glib gtk+

* Thu Mar  7 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.112.1-6k)
- add BuildRequires: libart_lgpl >= 2.3.8

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-4k)
- modify depend list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-2k)
- version 1.112.1

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-10k)
- rebuild against for libglade-1.99.8

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-8k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-6k)
- change require python2 to python

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-2k)
- version 1.112.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-10k)
- uso ppati 1.112

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-6k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-4k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-2k)
- version 1.111.0
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-12k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-10k)
- rebuild against for glib-1.3.13

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-6k)
- rebuild against for libglade-1.99.6

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-4k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-2k)
- version 1.110.0
- rebuild against for atk-0.10

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.108.0-4k)
- add python2 reqs

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.108.0-2k)
- port from Jirai
- version 1.108.0

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.103.0-3k)
- created spec file
