# Generated from facets-2.9.3.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname facets

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Premium Ruby Extensions
Name: rubygem-%{gemname}
Version: 2.9.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: Ruby
URL: http://rubyworks.github.com/facets
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Facets is the premier collection of extension methods for the Ruby programming
language. Facets extensions are unique by virtue of thier atomicity. They are
stored in individual files allowing for highly granular control of
requirements. In addition, Facets includes a few additional classes and mixins
suitable to wide variety of applications.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/RUBY.txt
%doc %{geminstdir}/HISTORY.rdoc
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/NOTICE.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.3-1m)
- update 2.9.3

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.9.2-2m)
- use RbConfig

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.2-1m)
- update 2.9.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.0-1m)
- update 2.9.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.4-1m)
- update 2.8.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.1-1m)
- import from Fedora to Momonga

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Mar 16 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 2.5.1-1
- New upstream version

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec 02 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 2.5.0-1
- New upstream version

* Sun Oct 25 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 2.4.5-2
- Fix %%doc files

* Sat Oct 25 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 2.4.5-1
- Initial package
