%global momorel 1
%global real_name rubygems

%define gem_dir %(ruby18 -rrbconfig -e 'puts File::expand_path(File::join(Config::CONFIG["sitedir"],"..","gems"))')
%define rb_ver %(ruby18 -rrbconfig -e 'puts Config::CONFIG["ruby_version"]')
%define gem_home %{gem_dir}/%{rb_ver}
%define ruby_sitelib %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: The Ruby standard for packaging ruby libraries
Name: rubygems18
Version: 1.8.10
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Ruby or GPL
URL: http://www.rubygems.org/
Source0: http://production.cf.rubygems.org/rubygems/%{real_name}-%{version}.tgz
NoSource: 0
Patch0: rubygems-1.3.4-noarch-gemdir.patch
Patch1: rubygems-1.3.7-ruby18.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = 1.8
Requires: ruby18-rdoc
BuildRequires: ruby18 ruby18-rdoc
BuildArch: noarch
Provides: ruby18(rubygems) = %{version}

%description
RubyGems is the Ruby standard for publishing and managing third party 
libraries.

%prep
%setup -q -n %{real_name}-%{version}
#%%patch0 -p1
#%%patch1 -p1
# Some of the library files start with #! which rpmlint doesn't like
# and doesn't make much sense
for f in `find lib -name \*.rb` ; do
  head -1 $f | grep -q '^#!/usr/bin/env ruby18' && sed -i -e '1d' $f
done

%build

%install
rm -rf %{buildroot}
GEM_HOME=%{buildroot}/%{gem_home} \
ruby18 setup.rb --prefix=%{_prefix} \
              --no-rdoc --no-ri \
	      --destdir=%{buildroot}/%{ruby_sitelib}/

mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}/%{ruby_sitelib}/usr/bin/gem18 %{buildroot}/%{_bindir}/gem18
mv %{buildroot}/%{ruby_sitelib}/usr/lib/* %{buildroot}/%{ruby_sitelib}/.
rm -rf %{buildroot}/%{ruby_sitelib}/usr
mkdir -p %{buildroot}/%{gem_home}/{cache,doc,gems,specifications}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc History.txt README.rdoc
%doc LICENSE.txt Manifest.txt
#%dir %{gem_dir}
%dir %{gem_home}
%dir %{gem_home}/cache
%dir %{gem_home}/gems
%dir %{gem_home}/specifications
%doc %{gem_home}/doc
%{_bindir}/gem18
%{ruby_sitelib}/*

%changelog
* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.10-1m)
- update 1.8.10

* Mon Aug 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.9-1m)
- update 1.8.9

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7-1m)
- update 1.8.7

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update 1.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.7-4m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.3.7-3m)
- deleted provided directory

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-2m)
- fix ruby(abi)
- set correct default_dir (Patch1)

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.7-1m)
- update 1.3.7
- rename to rubygems18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.5-1m)
- update 1.3.5

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.4-1m)
- update 1.3.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-2m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-2m)
- rebuild against gcc43

* Fri Jan 11 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.4-1m)
- version up 0.9.4

* Thu Jun 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- import from fc7

* Tue Feb 27 2007 David Lutterkort <dlutter@redhat.com> - 0.9.2-1
- New version
- Add patch0 to fix multilib sensitivity of Gem::dir (bz 227400)

* Thu Jan 18 2007 David Lutterkort <dlutter@redhat.com> - 0.9.1-1
- New version; include LICENSE.txt and GPL.txt
- avoid '..' in gem_dir to work around a bug in gem installer
- add ruby-rdoc to requirements

* Tue Jan  2 2007 David Lutterkort <dlutter@redhat.com> - 0.9.0-2
- Fix gem_dir to be arch independent
- Mention dual licensing in License field

* Fri Dec 22 2006 David Lutterkort <dlutter@redhat.com> - 0.9.0-1
- Updated to 0.9.0
- Changed to agree with Fedora Extras guidelines

* Mon Jan  9 2006 David Lutterkort <dlutter@redhat.com> - 0.8.11-1
- Updated for 0.8.11

* Sun Oct 10 2004 Omar Kilani <omar@tinysofa.org> 0.8.1-1ts
- First version of the package 
