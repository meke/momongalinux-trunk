%global         momorel 3

Name:           perl-Perl-Critic
Version:        1.121
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Critique Perl source code for best-practices
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Perl-Critic/
Source0:        http://www.cpan.org/authors/id/T/TH/THALJEF/Perl-Critic-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-base
BuildRequires:  perl-B-Keywords >= 1.05
BuildRequires:  perl-Config-Tiny >= 2
BuildRequires:  perl-Devel-Cycle >= 1.11-13m
BuildRequires:  perl-Email-Address >= 1.889
BuildRequires:  perl-Exception-Class >= 1.23
BuildRequires:  perl-File-HomeDir
BuildRequires:  perl-File-Path
BuildRequires:  perl-File-Temp
BuildRequires:  perl-File-Which
BuildRequires:  perl-Getopt-Long
BuildRequires:  perl-IO-String
BuildRequires:  perl-lib
BuildRequires:  perl-List-MoreUtils >= 0.19
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Pluggable >= 3.1
BuildRequires:  perl-PathTools
BuildRequires:  perl-Perl-Tidy >= 20120619
BuildRequires:  perl-Pod-Parser
BuildRequires:  perl-Pod-Spell >= 1
BuildRequires:  perl-PPI >= 1.208
BuildRequires:  perl-PPIx-Regexp
BuildRequires:  perl-PPIx-Utilities >= 1.000
BuildRequires:  perl-Readonly >= 1.03
BuildRequires:  perl-Readonly-XS
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-String-Format >= 1.13
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Term-ANSIColor >= 2.02
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Text-ParseWords >= 3
BuildRequires:  perl-version
Requires:       perl-base
Requires:       perl-B-Keywords >= 1.05
Requires:       perl-Config-Tiny >= 2
Requires:       perl-Email-Address >= 1.889
Requires:       perl-Exception-Class >= 1.23
Requires:       perl-File-HomeDir
Requires:       perl-File-Path
Requires:       perl-File-Temp
Requires:       perl-File-Which
Requires:       perl-Getopt-Long
Requires:       perl-IO-String
Requires:       perl-List-MoreUtils >= 0.19
Requires:       perl-Module-Build >= 3.1
Requires:       perl-PathTools
Requires:       perl-Perl-Tidy >= 20120619
Requires:       perl-Pod-Parser
Requires:       perl-Pod-Spell >= 1
Requires:       perl-PPI >= 1.208
Requires:       perl-PPIx-Regexp
Requires:       perl-PPIx-Utilities >= 1.000
Requires:       perl-Readonly >= 1.03
Requires:       perl-Readonly-XS
Requires:       perl-Scalar-Util
Requires:       perl-String-Format >= 1.13
Requires:       perl-Task-Weaken
Requires:       perl-Term-ANSIColor >= 2.02
Requires:       perl-Test-Simple
Requires:       perl-Text-ParseWords >= 3
Requires:       perl-version
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Perl::Critic is an extensible framework for creating and applying coding
standards to Perl source code. Essentially, it is a static source code
analysis engine. Perl::Critic is distributed with a number of
Perl::Critic::Policy modules that attempt to enforce various coding
guidelines. Most Policy modules are based on Damian Conway's book Perl Best
Practices. However, Perl::Critic is not limited to PBP and will even
support Policies that contradict Conway. You can enable, disable, and
customize those Polices through the Perl::Critic interface. You can also
create new Policy modules that suit your own tastes.

%prep
%setup -q -n Perl-Critic-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{_bindir}/perlcritic
%{perl_vendorlib}/Perl/*
%{perl_vendorlib}/Test/Perl/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.121-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.121-2m)
- rebuild against perl-5.18.2

* Sun Nov  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.121-1m)
- update to 1.121

* Sun Oct 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.120-1m)
- update to 1.120

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.119-1m)
- update to 1.119

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.118-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.118-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.118-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.118-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.118-2m)
- rebuild against perl-5.16.1

* Wed Jul 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.118-1m)
- update to 1.118

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.117-2m)
- rebuild against perl-5.16.0

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.117-1m)
- update to 1.117

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.116-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.116-2m)
- rebuild against perl-5.14.1

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.116-1m)
- update to 1.116

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.115-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.115-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.115-1m)
- update to 1.115

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.114-1m)
- update to 1.114

* Wed Dec 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.111-1m)
- update to 1.111

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.109-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.109-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.109-1m)
- update to 1.109

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.108-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.108-2m)
- add epoch to %%changelog

* Wed Jun 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.108-1m)
- update to 1.108

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.106-2m)
- rebuild against perl-5.12.1

* Tue May 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.106-1m)
- update to 1.106

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.105-4m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.105-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.105-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.105-1m)
- update to 1.105

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.104-1m)
- update to 1.104

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.103-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.103-1m)
- update to 1.103

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.100-1m)
- update to 1.100

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.098-1m)
- update to 1.098

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.096-1m)
- update to 1.096

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.094.001-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.094.001-1m)
- update to 1.094001

* Wed Sep  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.092-1m)
- update to 1.092

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.090-1m)
- update to 1.090

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.089-1m)
- update to 1.089

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.088-1m)
- update to 1.088

* Mon Jun 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.087-1m)
- update to 1.087

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.086-1m)
- update to 1.086

* Sun Jun  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.085-1m)
- update to 1.085

* Sun Jun  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:1.082-4m)
- add Epoch: 1 to enable upgrading from STABLE_4

* Sat May 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.082-3m)
- add BuildRequires: perl-B-Keywords (thanks ryu san)
- add BuildRequires: perl-File-Which and perl-Pod-Spell

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.082-2m)
- rebuild against gcc43

* Sun Mar  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.082-1m)
- update to 1.082

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.080-1m)
- update to 1.080

* Thu Sep 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.078-1m)
- update to 1.078

* Sun Sep 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.077-1m)
- update to 1.077

* Sat Sep  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.076-1m)
- update to 1.076

* Wed Sep  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.074-1m)
- update to 1.074
- use %%{buildroot} instead of $RPM_BUILD_ROOT

* Tue Sep  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.073-1m)
- update to 1.073
- do not use %%NoSource macro

* Sat Aug 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.071-1m)
- update to 1.071

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.061-1m)
- update to 1.061

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.06-2m)
- add Epoch: 1 for yum upgrade

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Tue Jun  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.053-1m)
- update to 1.053

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.052-1m)
- update to 1.052

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.051-2m)
- use vendor

* Thu Apr 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.051-1m)
- update to 1.051

* Wed Mar 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Wed Feb 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Mon Feb 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Fri Jan 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01
- add Requires: perl-B-Keywords

* Sat Jan 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
