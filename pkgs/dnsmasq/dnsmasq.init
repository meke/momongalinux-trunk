#!/bin/sh
#
# Startup script for the DNS caching server
#
# chkconfig: - 49 50
# description: This script starts your DNS caching server
# processname: dnsmasq
# pidfile: /var/run/dnsmasq.pid

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ ${NETWORKING} = "no" ] && exit 0

dnsmasq=/usr/sbin/dnsmasq
[ -f $dnsmasq ] || exit 0

DOMAIN_SUFFIX=`dnsdomainname`
if [ ! -z "${DOMAIN_SUFFIX}" ]; then
  OPTIONS="-s $DOMAIN_SUFFIX"
fi

pidfile=${PIDFILE-/var/run/dnsmasq.pid}
lockfile=${LOCKFILE-/var/lock/subsys/dnsmasq}


RETVAL=0

# See how we were called.
case "$1" in
  start)
        echo -n "Starting dnsmasq: "
        daemon $dnsmasq $OPTIONS
	RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && touch ${lockfile}
        ;;
  stop)
        echo -n "Shutting down dnsmasq: "
        killproc -p ${pidfile} ${dnsmasq}
	RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && rm -f ${lockfile} ${pidfile}
        ;;
  status)
	status dnsmasq
	RETVAL=$?
	;;
  reload)
	echo -n "Reloading dnsmasq: "
	killproc -p ${pidfile} ${dnsmasq} -HUP
	RETVAL=$?
	echo
	;;
  restart)
	$0 stop
	$0 start
	RETVAL=$?
	;;
  condrestart)
	    if test "x`pidfileofproc dnsmasq`" != x; then
		$0 stop
		$0 start
		RETVAL=$?
	    fi
	    ;;
  *)
        echo "Usage: $0 {start|stop|restart|reload|condrestart|status}"
        exit 1
esac

exit $RETVAL

