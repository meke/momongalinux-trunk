%global momorel 5

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           fuse-python
Version:        0.2.1
Release:        %{momorel}m%{?dist}
Summary:        Python bindings for FUSE - filesystem in userspace

Group:          System Environment/Base
License:        LGPLv2+
URL:            http://fuse.sourceforge.net/wiki/index.php/FusePython
Source0:        http://dl.sourceforge.net/sourceforge/fuse/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       python-fuse = %{version}-%{release}

BuildRequires:  pkgconfig
BuildRequires:  fuse-devel
BuildRequires:  python-setuptools-devel

%description
This package provides python bindings for FUSE. FUSE makes it possible
to implement a filesystem in a userspace program.

%prep
%setup -q

%build
python setup.py build
mv -f Changelog Changelog.old
iconv -f iso8859-1 -t utf-8 < Changelog.old > Changelog

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --skip-build --root $RPM_BUILD_ROOT 

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING Changelog FAQ example
%doc README.new_fusepy_api README.new_fusepy_api.html README.package_maintainers
%{python_sitearch}/*

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-5m)
- rebuild for python=2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-1m)
- import from Fedora 11

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.2-9
- Rebuild for Python 2.6

* Sun Apr 27 2008 Peter Lemenkov <lemenkov@gmail.com> 0.2-8
- Fix issue with libewf

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.2-7
- Autorebuild for GCC 4.3

* Thu Oct 18 2007 Michel Salim <michel.sylvan@gmail.com> 0.2-6
- Fix source URL
- Include examples

* Fri Oct  5 2007 Peter Lemenkov <lemenkov@gmail.com> 0.2-5
- Removed BR: python-devel (excessive since we BR: python-setuptool)
- Cleaned up mixed usage of macros and explicit commands ( __python and python)

* Fri Oct  5 2007 Peter Lemenkov <lemenkov@gmail.com> 0.2-4
- Changes according to http://fedoraproject.org/wiki/Packaging/Python/Eggs

* Sun Sep  9 2007 Jan ONDREJ (SAL) <ondrejj@salstar.sk> 0.2-3
- removed non used macros
- Changelog file converted to UTF-8

* Thu Sep  6 2007 Jan ONDREJ (SAL) <ondrejj@salstar.sk> 0.2-2
- changed permissions for sitearch files to 644
- added fuseparts dir to package
- added egg-info directory with it's content
- license changed to LGPLv2, according to documentation and sources
- added provides for python-fuse (remove it on rename)

* Sun Aug  5 2007 Peter Lemenkov <lemenkov@gmail.com> 0.2-1
- Ver. 0.2
- Cleanups

* Tue Jun 26 2007 Robin Norwood <rnorwood@redhat.com> - 0.2-pre3-2
- Put everything in python_sitearch, which should allow x86_64 builds

* Fri Jun 22 2007 Robin Norwood <rnorwood@redhat.com> - 0.2-pre3-1
- Initial build
