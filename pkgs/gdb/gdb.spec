%global momorel 5
# rpmbuild parameters:
# --with testsuite: Run the testsuite (biarch if possible).  Default is without.
# --with asan: gcc -fsanitize=address
# --without python: No python support.
# --with profile: gcc -fprofile-generate / -fprofile-use: Before better
#                 workload gets run it decreases the general performance now.
# --define 'scl somepkgname': Independent packages by scl-utils-build.

%global rhel 7
%global el6 1

%{?scl:%scl_package gdb}
%{!?scl:
 %global pkg_name %{name}
 %global _root_prefix %{_prefix}
 %global _root_datadir %{_datadir}
 %global _root_bindir %{_bindir}
}

Summary: A GNU source-level debugger for C, C++, Fortran, Go and other languages
Name: %{?scl_prefix}gdb

# 6e5c95e6cf1e3c37bd3a822ca9e6721caab97a85
#global snap       20140127
# Freeze it when GDB gets branched
%global snapsrc    20140108
# See timestamp of source gnulib installed into gdb/gnulib/ .
%global snapgnulib 20121213
Version: 7.7

# The release always contains a leading reserved number, start it at 1.
# `upstream' is not a part of `name' to stay fully rpm dependencies compatible for the testing.
Release: %{momorel}m%{?dist}

License: "GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and BSD and Public Domain"
Group: Development/Debuggers
# Do not provide URL for snapshots as the file lasts there only for 2 days.
# ftp://sourceware.org/pub/gdb/releases/gdb-%{version}.tar.bz2
#Source: gdb-%{version}.tar.bz2
Source: ftp://sourceware.org/pub/gdb/releases/gdb-%{version}.tar.bz2
NoSource: 0
Buildroot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
URL: http://gnu.org/software/gdb/

%if "%{scl}" == "devtoolset-1.1"
Obsoletes: devtoolset-1.0-%{pkg_name}
%endif

# For our convenience
%global gdb_src %{pkg_name}-%{version}
%global gdb_build build-%{_target_platform}

# Make sure we get rid of the old package gdb64, now that we have unified
# support for 32-64 bits in one single 64-bit gdb.
%ifarch ppc64
Obsoletes: gdb64 < 5.3.91
%endif

%global have_inproctrace 0
%ifarch %{ix86} x86_64
# RHEL-5.i386: [int foo, bar; bar = __sync_val_compare_and_swap(&foo, 0, 1);]
#              undefined reference to `__sync_val_compare_and_swap_4'
%if 0%{!?el5:1}
%global have_inproctrace 1
%endif # 0%{!?el5:1}
%endif # %{ix86} x86_64

# gdb-add-index cannot be run even for SCL package on RHEL<=6.
%if 0%{!?rhel:1} || 0%{?rhel} > 6
# eu-strip: -g recognizes .gdb_index as a debugging section. (#631997)
Conflicts: elfutils < 0.149
%endif

# https://fedorahosted.org/fpc/ticket/43 https://fedorahosted.org/fpc/ticket/109
Provides: bundled(libiberty) = %{snapsrc}
Provides: bundled(gnulib) = %{snapgnulib}
Provides: bundled(binutils) = %{snapsrc}
# https://fedorahosted.org/fpc/ticket/130
Provides: bundled(md5-gcc) = %{snapsrc}

# GDB patches have the format `gdb-<version>-bz<red-hat-bz-#>-<desc>.patch'.
# They should be created using patch level 1: diff -up ./gdb (or gdb-6.3/gdb).

#=
#push=Should be pushed upstream.
#maybepush=Should be pushed upstream unless it got obsoleted there.
#fedora=Should stay as a Fedora patch.
#fedoratest=Keep it in Fedora only as a regression test safety.
#+ppc=Specific for ppc32/ppc64/ppc*
#+work=Requires some nontrivial work.

# Cleanup any leftover testsuite processes as it may stuck mock(1) builds.
#=push
Source2: gdb-orphanripper.c

# Man page for gstack(1).
#=push
Source3: gdb-gstack.man

# /etc/gdbinit (from Debian but with Fedora compliant location).
#=fedora
Source4: gdbinit

# libstdc++ pretty printers from GCC SVN HEAD (4.5 experimental).
%global libstdcxxpython gdb-libstdc++-v3-python-r155978
Source5: %{libstdcxxpython}.tar.bz2

# Provide gdbtui for RHEL-5 and RHEL-6 as it is removed upstream (BZ 797664).
Source6: gdbtui

# Work around out-of-date dejagnu that does not have KFAIL
#=drop: That dejagnu is too old to be supported.
Patch1: gdb-6.3-rh-dummykfail-20041202.patch

# Match the Fedora's version info.
#=fedora
Patch2: gdb-6.3-rh-testversion-20041202.patch

# Better parse 64-bit PPC system call prologues.
#=maybepush+ppc: Write new testcase.
Patch105: gdb-6.3-ppc64syscall-20040622.patch

# Include the pc's section when doing a symbol lookup so that the
# correct symbol is found.
#=maybepush: Write new testcase.
Patch111: gdb-6.3-ppc64displaysymbol-20041124.patch

# Fix upstream `set scheduler-locking step' vs. upstream PPC atomic seqs.
#=push+work: It is a bit difficult patch, a part is ppc specific.
Patch112: gdb-6.6-scheduler_locking-step-sw-watchpoints2.patch
# Make upstream `set scheduler-locking step' as default.
#=push+work: How much is scheduler-locking relevant after non-stop?
Patch260: gdb-6.6-scheduler_locking-step-is-default.patch

# Add a wrapper script to GDB that implements pstack using the
# --readnever option.
#=push+work: with gdbindex maybe --readnever should no longer be used.
Patch118: gdb-6.3-gstack-20050411.patch

# VSYSCALL and PIE
#=fedoratest
Patch122: gdb-6.3-test-pie-20050107.patch
#=push: May get obsoleted by Tom's unrelocated objfiles patch.
Patch389: gdb-archer-pie-addons.patch
#=push+work: Breakpoints disabling matching should not be based on address.
Patch394: gdb-archer-pie-addons-keep-disabled.patch

# Get selftest working with sep-debug-info
#=fedoratest
Patch125: gdb-6.3-test-self-20050110.patch

# Test support of multiple destructors just like multiple constructors
#=fedoratest
Patch133: gdb-6.3-test-dtorfix-20050121.patch

# Fix to support executable moving
#=fedoratest
Patch136: gdb-6.3-test-movedir-20050125.patch

# Test sibling threads to set threaded watchpoints for x86 and x86-64
#=fedoratest
Patch145: gdb-6.3-threaded-watchpoints2-20050225.patch

# Notify observers that the inferior has been created
#=fedoratest
Patch161: gdb-6.3-inferior-notification-20050721.patch

# Verify printing of inherited members test
#=fedoratest
Patch163: gdb-6.3-inheritancetest-20050726.patch

# Add readnever option
#=push
Patch164: gdb-6.3-readnever-20050907.patch

# Fix debuginfo addresses resolving for --emit-relocs Linux kernels (BZ 203661).
#=push+work: There was some mail thread about it, this patch may be a hack.
Patch188: gdb-6.5-bz203661-emit-relocs.patch

# Support TLS symbols (+`errno' suggestion if no pthread is found) (BZ 185337).
#=push+work: It should be replaced by existing uncommitted Roland's glibc patch for TLS without libpthreads.
Patch194: gdb-6.5-bz185337-resolve-tls-without-debuginfo-v2.patch

# Fix TLS symbols resolving for shared libraries with a relative pathname.
# The testsuite needs `gdb-6.5-tls-of-separate-debuginfo.patch'.
#=fedoratest+work: One should recheck if it is really fixed upstream.
Patch196: gdb-6.5-sharedlibrary-path.patch

# Suggest fixing your target architecture for gdbserver(1) (BZ 190810).
# FIXME: It could be autodetected.
#=push+work: There are more such error cases that can happen.
Patch199: gdb-6.5-bz190810-gdbserver-arch-advice.patch

# Testcase for deadlocking on last address space byte; for corrupted backtraces.
#=fedoratest
Patch211: gdb-6.5-last-address-space-byte-test.patch

# Improved testsuite results by the testsuite provided by the courtesy of BEA.
#=fedoratest+work: For upstream it should be rewritten as a dejagnu test, the test of no "??" was useful.
Patch208: gdb-6.5-BEA-testsuite.patch

# Fix readline segfault on excessively long hand-typed lines.
#=fedoratest
Patch213: gdb-6.5-readline-long-line-crash-test.patch

# Fix bogus 0x0 unwind of the thread's topmost function clone(3) (BZ 216711).
#=fedora
Patch214: gdb-6.5-bz216711-clone-is-outermost.patch

# Test sideeffects of skipping ppc .so libs trampolines (BZ 218379).
#=fedoratest
Patch216: gdb-6.5-bz218379-ppc-solib-trampoline-test.patch

# Fix lockup on trampoline vs. its function lookup; unreproducible (BZ 218379).
#=fedora
Patch217: gdb-6.5-bz218379-solib-trampoline-lookup-lock-fix.patch

# Find symbols properly at their original (included) file (BZ 109921).
#=fedoratest
Patch225: gdb-6.5-bz109921-DW_AT_decl_file-test.patch

# Update PPC unwinding patches to their upstream variants (BZ 140532).
#=fedoratest+ppc
Patch229: gdb-6.3-bz140532-ppc-unwinding-test.patch

# Testcase for exec() from threaded program (BZ 202689).
#=fedoratest
Patch231: gdb-6.3-bz202689-exec-from-pthread-test.patch

# Backported fixups post the source tarball.
#Xdrop: Just backports.
Patch232: gdb-upstream.patch

# Testcase for PPC Power6/DFP instructions disassembly (BZ 230000).
#=fedoratest+ppc
Patch234: gdb-6.6-bz230000-power6-disassembly-test.patch

# Temporary support for shared libraries >2GB on 64bit hosts. (BZ 231832)
#=push+work: Upstream should have backward compat. API: libc-alpha: <20070127104539.GA9444@.*>
Patch235: gdb-6.3-bz231832-obstack-2gb.patch

# Allow running `/usr/bin/gcore' with provided but inaccessible tty (BZ 229517).
#=fedoratest
Patch245: gdb-6.6-bz229517-gcore-without-terminal.patch

# Notify user of a child forked process being detached (BZ 235197).
#=push: This is more about discussion if/what should be printed.
Patch247: gdb-6.6-bz235197-fork-detach-info.patch

# Avoid too long timeouts on failing cases of "annota1.exp annota3.exp".
#=fedoratest
Patch254: gdb-6.6-testsuite-timeouts.patch

# Support for stepping over PPC atomic instruction sequences (BZ 237572).
#=fedoratest
Patch258: gdb-6.6-bz237572-ppc-atomic-sequence-test.patch

# Test kernel VDSO decoding while attaching to an i386 process.
#=fedoratest
Patch263: gdb-6.3-attach-see-vdso-test.patch

# Test leftover zombie process (BZ 243845).
#=fedoratest
Patch271: gdb-6.5-bz243845-stale-testing-zombie-test.patch

# New locating of the matching binaries from the pure core file (build-id).
#=push
Patch274: gdb-6.6-buildid-locate.patch
# Fix loading of core files without build-ids but with build-ids in executables.
#=push
Patch659: gdb-6.6-buildid-locate-solib-missing-ids.patch
#=push
Patch353: gdb-6.6-buildid-locate-rpm.patch
#=push
Patch415: gdb-6.6-buildid-locate-core-as-arg.patch
# Workaround librpm BZ 643031 due to its unexpected exit() calls (BZ 642879).
#=push
Patch519: gdb-6.6-buildid-locate-rpm-librpm-workaround.patch
# [SCL] Skip deprecated .gdb_index warning for Red Hat built files (BZ 953585).
Patch833: gdb-6.6-buildid-locate-rpm-scl.patch
# Fix 'gdb gives highly misleading error when debuginfo pkg is present,
# but not corresponding binary pkg' (RH BZ 981154).
Patch863: gdb-6.6-buildid-locate-misleading-warning-missing-debuginfo-rhbz981154.patch

# Add kernel vDSO workaround (`no loadable ...') on RHEL-5 (kernel BZ 765875).
#=push
Patch276: gdb-6.6-bfd-vdso8k.patch

# Fix displaying of numeric char arrays as strings (BZ 224128).
#=fedoratest: But it is failing anyway, one should check the behavior more.
Patch282: gdb-6.7-charsign-test.patch

# Test PPC hiding of call-volatile parameter register.
#=fedoratest+ppc
Patch284: gdb-6.7-ppc-clobbered-registers-O2-test.patch

# Test ia64 memory leaks of the code using libunwind.
#=fedoratest
Patch289: gdb-6.5-ia64-libunwind-leak-test.patch

# Test hiding unexpected breakpoints on intentional step commands.
#=fedoratest
Patch290: gdb-6.5-missed-trap-on-step-test.patch

# Support DW_TAG_interface_type the same way as DW_TAG_class_type (BZ 426600).
#=fedoratest
Patch294: gdb-6.7-bz426600-DW_TAG_interface_type-test.patch

# Test gcore memory and time requirements for large inferiors.
#=fedoratest
Patch296: gdb-6.5-gcore-buffer-limit-test.patch

# Test debugging statically linked threaded inferiors (BZ 239652).
#  - It requires recent glibc to work in this case properly.
#=fedoratest
Patch298: gdb-6.6-threads-static-test.patch

# Test GCORE for shmid 0 shared memory mappings.
#=fedoratest: But it is broken anyway, sometimes the case being tested is not reproducible.
Patch309: gdb-6.3-mapping-zero-inode-test.patch

# Test a crash on `focus cmd', `focus prev' commands.
#=fedoratest
Patch311: gdb-6.3-focus-cmd-prev-test.patch

# Test various forms of threads tracking across exec() (BZ 442765).
#=fedoratest
Patch315: gdb-6.8-bz442765-threaded-exec-test.patch

# Silence memcpy check which returns false positive (sparc64)
#=push: But it is just a GCC workaround, look up the existing GCC PR for it.
Patch317: gdb-6.8-sparc64-silence-memcpy-check.patch

# Test a crash on libraries missing the .text section.
#=fedoratest
Patch320: gdb-6.5-section-num-fixup-test.patch

# Fix register assignments with no GDB stack frames (BZ 436037).
#=push+work: This fix is incorrect.
Patch330: gdb-6.8-bz436037-reg-no-longer-active.patch

# Make the GDB quit processing non-abortable to cleanup everything properly.
#=fedora: It was useful only after gdb-6.8-attach-signalled-detach-stopped.patch .
Patch331: gdb-6.8-quit-never-aborts.patch

# [RHEL5] Workaround kernel for detaching SIGSTOPped processes (BZ 809382).
#=fedora
Patch335: gdb-rhel5-compat.patch

# [RHEL5,RHEL6] Fix attaching to stopped processes.
#=fedora
Patch337: gdb-6.8-attach-signalled-detach-stopped.patch

# Test the watchpoints conditionals works.
#=fedoratest
Patch343: gdb-6.8-watchpoint-conditionals-test.patch

# Fix resolving of variables at locations lists in prelinked libs (BZ 466901).
#=fedoratest
Patch348: gdb-6.8-bz466901-backtrace-full-prelinked.patch

# The merged branch `archer-jankratochvil-fedora15' of:
# http://sourceware.org/gdb/wiki/ProjectArcher
#=push+work
Patch349: gdb-archer.patch

# Fix parsing elf64-i386 files for kdump PAE vmcore dumps (BZ 457187).
# - Turn on 64-bit BFD support, globally enable AC_SYS_LARGEFILE.
#=fedoratest
Patch360: gdb-6.8-bz457187-largefile-test.patch

# New test for step-resume breakpoint placed in multiple threads at once.
#=fedoratest
Patch381: gdb-simultaneous-step-resume-breakpoint-test.patch

# Fix GNU/Linux core open: Can't read pathname for load map: Input/output error.
# Fix regression of undisplayed missing shared libraries caused by a fix for.
#=push+work: It should be in glibc: libc-alpha: <20091004161706.GA27450@.*>
Patch382: gdb-core-open-vdso-warning.patch

# Fix syscall restarts for amd64->i386 biarch.
#=push
Patch391: gdb-x86_64-i386-syscall-restart.patch

# Fix stepping with OMP parallel Fortran sections (BZ 533176).
#=push+work: It requires some better DWARF annotations.
Patch392: gdb-bz533176-fortran-omp-step.patch

# Use gfortran44 when running the testsuite on RHEL-5.
#=fedoratest
Patch393: gdb-rhel5-gcc44.patch

# Fix regression by python on ia64 due to stale current frame.
#=push
Patch397: gdb-follow-child-stale-parent.patch

# Workaround ccache making lineno non-zero for command-line definitions.
#=fedoratest: ccache is rarely used and it is even fixed now.
Patch403: gdb-ccache-workaround.patch

# Testcase for "Do not make up line information" fix by Daniel Jacobowitz.
#=fedoratest
Patch407: gdb-lineno-makeup-test.patch

# Test power7 ppc disassembly.
#=fedoratest+ppc
Patch408: gdb-ppc-power7-test.patch

# Fix i386+x86_64 rwatch+awatch before run, regression against 6.8 (BZ 541866).
# Fix i386 rwatch+awatch before run (BZ 688788, on top of BZ 541866).
#=push+work: It should be fixed properly instead.
Patch417: gdb-bz541866-rwatch-before-run.patch

# Workaround non-stop moribund locations exploited by kernel utrace (BZ 590623).
#=push+work: Currently it is still not fully safe.
Patch459: gdb-moribund-utrace-workaround.patch

# Fix follow-exec for C++ programs (bugreported by Martin Stransky).
#=fedoratest
Patch470: gdb-archer-next-over-throw-cxx-exec.patch

# Backport DWARF-4 support (BZ 601887, Tom Tromey).
#=fedoratest
Patch475: gdb-bz601887-dwarf4-rh-test.patch

# [delayed-symfile] Test a backtrace regression on CFIs without DIE (BZ 614604).
#=fedoratest
Patch490: gdb-test-bt-cfi-without-die.patch

# Provide /usr/bin/gdb-add-index for rpm-build (Tom Tromey).
#=fedora: Re-check against the upstream version.
Patch491: gdb-gdb-add-index-script.patch

# Out of memory is just an error, not fatal (uninitialized VLS vars, BZ 568248).
#=drop+work: Inferior objects should be read in parts, then this patch gets obsoleted.
Patch496: gdb-bz568248-oom-is-error.patch

# Verify GDB Python built-in function gdb.solib_address exists (BZ # 634108).
#=fedoratest
Patch526: gdb-bz634108-solib_address.patch

# New test gdb.arch/x86_64-pid0-core.exp for kernel PID 0 cores (BZ 611435).
#=fedoratest
Patch542: gdb-test-pid0-core.patch

# [archer-tromey-delayed-symfile] New test gdb.dwarf2/dw2-aranges.exp.
#=fedoratest
Patch547: gdb-test-dw2-aranges.patch

# [archer-keiths-expr-cumulative+upstream] Import C++ testcases.
#=fedoratest
Patch548: gdb-test-expr-cumulative-archer.patch

# Toolchain on sparc is slightly broken and debuginfo files are generated
# with non 64bit aligned tables/offsets.
# See for example readelf -S ../Xvnc.debug.
#
# As a consenquence calculation of sectp->filepos as used in
# dwarf2_read_section (gdb/dwarf2read.c:1525) will return a non aligned buffer
# that cannot be used directly as done with MMAP.
# Usage will result in a BusError.
#
# While we figure out what's wrong in the toolchain and do a full archive
# rebuild to fix it, we need to be able to use gdb :)
#=push+work
Patch579: gdb-7.2.50-sparc-add-workaround-to-broken-debug-files.patch

# Work around PR libc/13097 "linux-vdso.so.1" warning message.
#=push
Patch627: gdb-glibc-vdso-workaround.patch

# Hack for proper PIE run of the testsuite.
#=fedoratest
Patch634: gdb-runtest-pie-override.patch

# Work around readline-6.2 incompatibility not asking for --more-- (BZ 701131).
#=fedora
#Patch642: gdb-readline62-ask-more-rh.patch

# Print reasons for failed attach/spawn incl. SELinux deny_ptrace (BZ 786878).
#=push
Patch653: gdb-attach-fail-reasons-5of5.patch

# Workaround crashes from stale frame_info pointer (BZ 804256).
#=fedora
Patch661: gdb-stale-frame_info.patch

# Workaround PR libc/14166 for inferior calls of strstr.
#=fedora: Compatibility with RHELs (unchecked which ones).
Patch690: gdb-glibc-strstr-workaround.patch

# Include testcase for `Unable to see a variable inside a module (XLF)' (BZ 823789).
#=fedoratest
#+ppc
Patch698: gdb-rhel5.9-testcase-xlf-var-inside-mod.patch

# Testcase for `Setting solib-absolute-prefix breaks vDSO' (BZ 818343).
#=fedoratest
Patch703: gdb-rhbz-818343-set-solib-absolute-prefix-testcase.patch

# Fix `GDB cannot access struct member whose offset is larger than 256MB'
# (RH BZ 795424).
#=push+work
Patch811: gdb-rhbz795424-bitpos-20of25.patch
Patch812: gdb-rhbz795424-bitpos-21of25.patch
Patch813: gdb-rhbz795424-bitpos-22of25.patch
Patch814: gdb-rhbz795424-bitpos-23of25.patch
Patch816: gdb-rhbz795424-bitpos-25of25.patch
Patch817: gdb-rhbz795424-bitpos-25of25-test.patch
Patch818: gdb-rhbz795424-bitpos-lazyvalue.patch

# Import regression test for `gdb/findvar.c:417: internal-error:
# read_var_value: Assertion `frame' failed.' (RH BZ 947564) from RHEL 6.5.
#=fedoratest
Patch832: gdb-rhbz947564-findvar-assertion-frame-failed-testcase.patch

# Fix crash on 'enable count' (Simon Marchi, BZ 993118).
Patch843: gdb-enable-count-crash.patch

# Fix testsuite "ERROR: no fileid for".
Patch846: gdb-testsuite-nohostid.patch

# Fix Python stack corruption.
Patch847: gdb-python-stacksmash.patch

# [rhel6] DTS backward Python compatibility API (BZ 1020004, Phil Muldoon).
Patch848: gdb-dts-rhel6-python-compat.patch

# Fix gdb-7.7 auto-load from /usr/share/gdb/auto-load/ regression.
Patch849: gdb-auto-load-lost-path-7.7.patch

# Fix crash of -readnow /usr/lib/debug/usr/bin/gnatbind.debug (BZ 1069211).
Patch850: gdb-gnat-dwarf-crash-1of3.patch
Patch851: gdb-gnat-dwarf-crash-2of3.patch
Patch852: gdb-gnat-dwarf-crash-3of3.patch

# Fix build failures for GCC 4.9 (Nick Clifton).
Patch864: gcc-4.9-compat.patch

%if 0%{!?rhel:1} || 0%{?rhel} > 6
# RL_STATE_FEDORA_GDB would not be found for:
# Patch642: gdb-readline62-ask-more-rh.patch
# --with-system-readline
#BuildRequires: readline-devel%{?_isa} >= 6.2-4
%endif # 0%{!?rhel:1} || 0%{?rhel} > 6

BuildRequires: ncurses-devel%{?_isa} texinfo gettext flex bison
BuildRequires: expat-devel%{?_isa}
%if 0%{!?rhel:1} || 0%{?rhel} > 6
BuildRequires: xz-devel%{?_isa}
%endif
%if 0%{!?el5:1}
# dlopen() no longer makes rpm-libs%{?_isa} (it's .so) a mandatory dependency.
BuildRequires: rpm-devel%{?_isa}
%endif # 0%{!?el5:1}
BuildRequires: zlib-devel%{?_isa} libselinux-devel%{?_isa}
%if 0%{!?_without_python:1}
%if 0%{?el5:1}
# This RHEL-5.6 python version got split out python-libs for ppc64.
# RHEL-5 rpm does not support .%{_arch} dependencies.
Requires: python-libs-%{_arch} >= 2.4.3-32.el5
%endif
BuildRequires: python-devel%{?_isa}
%if 0%{?rhel:1} && 0%{?rhel} <= 6
# Temporarily before python files get moved to libstdc++.rpm
# libstdc++%{bits_other} is not present in Koji, the .spec script generating
# gdb/python/libstdcxx/ also does not depend on the %{bits_other} files.
BuildRequires: libstdc++%{?_isa}
%endif # 0%{?rhel:1} && 0%{?rhel} <= 6
%endif # 0%{!?_without_python:1}
# gdb-doc in PDF, see: https://bugzilla.redhat.com/show_bug.cgi?id=919891#c10
BuildRequires: texinfo-tex
%if 0%{!?rhel:1} || 0%{?rhel} > 6
#BuildRequires: texlive-collection-latexrecommended
%endif
# Permit rebuilding *.[0-9] files even if they are distributed in gdb-*.tar:
BuildRequires: /usr/bin/pod2man

# BuildArch would break RHEL-5 by overriding arch and not building noarch.
%if 0%{?el5:1}
ExclusiveArch: noarch i386 x86_64 ppc ppc64 s390 s390x
%endif # 0%{?el5:1}

%if 0%{?_with_testsuite:1}

# Ensure the devel libraries are installed for both multilib arches.
%global bits_local %{?_isa}
%global bits_other %{?_isa}
%if 0%{!?el5:1}
%ifarch s390x
%global bits_other (%{__isa_name}-32)
%else #!s390x
%ifarch ppc
%global bits_other (%{__isa_name}-64)
%else #!ppc
%ifarch sparc64 ppc64 s390x x86_64
%global bits_other (%{__isa_name}-32)
%endif #sparc64 ppc64 s390x x86_64
%endif #!ppc
%endif #!s390x
%endif #!el5

BuildRequires: sharutils dejagnu
# gcc-objc++ is not covered by the GDB testsuite.
BuildRequires: gcc gcc-c++ gcc-gfortran gcc-objc
%if 0%{!?rhel:1} || 0%{?rhel} < 7
BuildRequires: gcc-java libgcj%{bits_local} libgcj%{bits_other}
%endif
%if 0%{!?rhel:1} || 0%{?rhel} > 6
#BuildRequires: gcc-go
%endif
# archer-sergiodj-stap-patch-split
BuildRequires: systemtap-sdt-devel
# Copied from prelink-0.4.2-3.fc13.
%ifarch %{ix86} alpha sparc sparcv9 sparc64 s390 s390x x86_64 ppc ppc64
# Prelink is broken on sparcv9/sparc64.
%ifnarch sparc sparcv9 sparc64
BuildRequires: prelink
%endif
%endif
%if 0%{!?rhel:1}
# Fedora arm does not yet have fpc built.
%ifnarch %{arm}
BuildRequires: fpc
%endif
%endif
%if 0%{?el5:1}
BuildRequires: gcc44 gcc44-gfortran
%endif
# Copied from gcc-4.1.2-32.
%ifarch %{ix86} x86_64 ppc alpha
#BuildRequires: gcc-gnat
#BuildRequires: libgnat%{bits_local} libgnat%{bits_other}
%endif
BuildRequires: glibc-devel%{bits_local} glibc-devel%{bits_other}
BuildRequires: libgcc%{bits_local} libgcc%{bits_other}
# libstdc++-devel of matching bits is required only for g++ -static.
BuildRequires: libstdc++%{bits_local} libstdc++%{bits_other}
%if 0%{!?rhel:1} || 0%{?rhel} > 6
#BuildRequires: libgo-devel%{bits_local} libgo-devel%{bits_other}
%endif
%if 0%{!?el5:1}
BuildRequires: glibc-static%{bits_local}
%endif
# multilib glibc-static is open Bug 488472:
#BuildRequires: glibc-static%{bits_other}
# for gcc-java linkage:
BuildRequires: zlib-devel%{bits_local} zlib-devel%{bits_other}
# Copied from valgrind-3.5.0-1.
%ifarch %{ix86} x86_64 ppc ppc64
BuildRequires: valgrind%{bits_local} valgrind%{bits_other}
%endif
%if 0%{!?rhel:1} || 0%{?rhel} > 6
BuildRequires: xz
%endif

%endif # 0%{?_with_testsuite:1}

%{?scl:Requires:%scl_runtime}

%description
GDB, the GNU debugger, allows you to debug programs written in C, C++,
Java, and other languages, by executing them in a controlled fashion
and printing their data.

# It would break RHEL-5 by leaving excessive files for the doc subpackage.
%ifnarch noarch

%package gdbserver
Summary: A standalone server for GDB (the GNU source-level debugger)
Group: Development/Debuggers

%if "%{scl}" == "devtoolset-1.1"
Obsoletes: devtoolset-1.0-%{pkg_name}-gdbserver
%endif

%description gdbserver
GDB, the GNU debugger, allows you to debug programs written in C, C++,
Java, and other languages, by executing them in a controlled fashion
and printing their data.

This package provides a program that allows you to run GDB on a different
machine than the one which is running the program being debugged.

# It would break RHEL-5 by leaving excessive files for the doc subpackage.
%endif # !noarch
%if 0%{!?el5:1} || "%{_target_cpu}" == "noarch"

%package doc
Summary: Documentation for GDB (the GNU source-level debugger)
License: GFDL
Group: Documentation
# It would break RHEL-5 by overriding arch and not building noarch separately.
%if 0%{!?el5:1}
BuildArch: noarch
%endif # 0%{!?el5:1}

%if "%{scl}" == "devtoolset-1.1"
Obsoletes: devtoolset-1.0-%{pkg_name}-doc
%endif

%description doc
GDB, the GNU debugger, allows you to debug programs written in C, C++,
Java, and other languages, by executing them in a controlled fashion
and printing their data.

This package provides INFO, HTML and PDF user manual for GDB.

Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%endif # 0%{!?el5:1} || "%{_target_cpu}" == "noarch"

%prep
%setup -q -n %{gdb_src}

%if 0%{?rhel:1} && 0%{?rhel} <= 6
# libstdc++ pretty printers.
tar xjf %{SOURCE5}
%endif # 0%{?rhel:1} && 0%{?rhel} <= 6

# Files have `# <number> <file>' statements breaking VPATH / find-debuginfo.sh .
rm -f gdb/ada-exp.c gdb/ada-lex.c gdb/c-exp.c gdb/cp-name-parser.c gdb/f-exp.c
rm -f gdb/jv-exp.c gdb/m2-exp.c gdb/objc-exp.c gdb/p-exp.c gdb/go-exp.c

# *.info* is needlessly split in the distro tar; also it would not get used as
# we build in %{gdb_build}, just to be sure.
find -name "*.info*"|xargs rm -f

# Apply patches defined above.

# Match the Fedora's version info.
%patch2 -p1

%patch349 -p1
%patch232 -p1
%patch1 -p1

%patch105 -p1
%patch111 -p1
%patch112 -p1
%patch118 -p1
%patch122 -p1
%patch125 -p1
%patch133 -p1
%patch136 -p1
%patch145 -p1
%patch161 -p1
%patch163 -p1
%patch164 -p1
%patch188 -p1
%patch194 -p1
%patch196 -p1
%patch199 -p1
%patch208 -p1
%patch211 -p1
%patch213 -p1
%patch214 -p1
%patch216 -p1
%patch217 -p1
%patch225 -p1
%patch229 -p1
%patch231 -p1
%patch234 -p1
%patch235 -p1
%patch245 -p1
%patch247 -p1
%patch254 -p1
%patch258 -p1
%patch260 -p1
%patch263 -p1
%patch271 -p1
%patch274 -p1
%patch659 -p1
%patch353 -p1
%patch276 -p1
%patch282 -p1
%patch284 -p1
%patch289 -p1
%patch290 -p1
%patch294 -p1
%patch296 -p1
%patch298 -p1
%patch309 -p1
%patch311 -p1
%patch315 -p1
%patch317 -p1
%patch320 -p1
%patch330 -p1
%patch343 -p1
%patch348 -p1
%patch360 -p1
%patch381 -p1
%patch382 -p1
%patch391 -p1
%patch392 -p1
%patch397 -p1
%patch403 -p1
%patch389 -p1
%patch394 -p1
%patch407 -p1
%patch408 -p1
%patch417 -p1
%patch459 -p1
%patch470 -p1
%patch475 -p1
%patch415 -p1
%patch519 -p1
%patch490 -p1
%patch491 -p1
%patch496 -p1
%patch526 -p1
%patch542 -p1
%patch547 -p1
%patch548 -p1
%patch579 -p1
%patch627 -p1
%patch634 -p1
%patch653 -p1
%patch661 -p1
%patch690 -p1
%patch698 -p1
%patch703 -p1
%patch811 -p1
%patch812 -p1
%patch813 -p1
%patch814 -p1
%patch816 -p1
%patch817 -p1
%patch818 -p1
%patch832 -p1
%patch843 -p1
%patch846 -p1
%patch847 -p1
%patch849 -p1
%patch850 -p1
%patch851 -p1
%patch852 -p1
%patch863 -p1
%patch864 -p1

%patch848 -p1
%if 0%{!?el6:1}
%patch848 -p1 -R
%endif
%patch393 -p1
%if 0%{!?el5:1} || 0%{?scl:1}
%patch393 -p1 -R
%endif
%patch833 -p1
%if 0%{!?el6:1} || 0%{!?scl:1}
%patch833 -p1 -R
%endif
#%patch642 -p1
#%if 0%{?rhel:1} && 0%{?rhel} <= 6
#%patch642 -p1 -R
#%endif
%patch337 -p1
%patch331 -p1
%patch335 -p1
%if 0%{!?rhel:1} || 0%{?rhel} > 6
%patch335 -p1 -R
%patch331 -p1 -R
%patch337 -p1 -R
%endif

find -name "*.orig" | xargs rm -f
! find -name "*.rej" # Should not happen.

# Change the version that gets printed at GDB startup, so it is RH specific.
#cat > gdb/version.in << _FOO
#%if 0%{!?rhel:1}
#Fedora %{version}-%{release}
#%else # !0%{!?rhel:1} 
#Red Hat Enterprise Linux %{version}-%{release}
#%endif # !0%{!?rhel:1} 
#_FOO

# Remove the info and other generated files added by the FSF release
# process.
rm -f libdecnumber/gstdint.h
rm -f bfd/doc/*.info
rm -f bfd/doc/*.info-*
rm -f gdb/doc/*.info
rm -f gdb/doc/*.info-*

%if 0%{!?rhel:1} || 0%{?rhel} > 6
# RL_STATE_FEDORA_GDB would not be found for:
# Patch642: gdb-readline62-ask-more-rh.patch
# --with-system-readline
mv -f readline/doc readline-doc
rm -rf readline/*
mv -f readline-doc readline/doc
%endif # 0%{!?rhel:1} || 0%{?rhel} > 6

%build
rm -rf %{buildroot}

# Identify the build directory with the version of gdb as well as the
# architecture, to allow for mutliple versions to be installed and
# built.
# Initially we're in the %{gdb_src} directory.

for fprofile in %{?_with_profile:-fprofile} ""
do

mkdir %{gdb_build}$fprofile
cd %{gdb_build}$fprofile

export CFLAGS="$RPM_OPT_FLAGS %{?_with_asan:-fsanitize=address}"
export LDFLAGS="%{?__global_ldflags} %{?_with_asan:-fsanitize=address}"

# --htmldir and --pdfdir are not used as they are used from %{gdb_build}.
../configure							\
	--prefix=%{_prefix}					\
	--libdir=%{_libdir}					\
	--sysconfdir=%{_sysconfdir}				\
	--mandir=%{_mandir}					\
	--infodir=%{_infodir}					\
	--with-system-gdbinit=%{_sysconfdir}/gdbinit		\
	--with-gdb-datadir=%{_datadir}/gdb			\
	--enable-gdb-build-warnings=,-Wno-unused		\
%ifnarch %{ix86} alpha ppc s390 s390x x86_64 ppc64 sparc sparcv9 sparc64
	--disable-werror					\
%else
	--enable-werror						\
%endif
	--with-separate-debug-dir=/usr/lib/debug		\
	--disable-sim						\
	--disable-rpath						\
%if 0%{!?rhel:1} || 0%{?rhel} > 6
	--with-system-readline					\
%else
	--without-system-readline				\
%endif
	--with-expat						\
$(: ppc64 host build crashes on ppc variant of libexpat.so )	\
	--without-libexpat-prefix				\
	--enable-tui						\
%if 0%{!?_without_python:1}
	--with-python						\
%else
	--without-python					\
%endif
$(: Workaround rpm.org#76, BZ 508193 on recent OSes. )		\
$(: RHEL-5 librpm has incompatible API. )			\
%if 0%{?el5:1}
	--without-rpm						\
%else
%if 0%{?el6:1}
	--with-rpm=librpm.so.1					\
%else
	--with-rpm=librpm.so.3					\
%endif
%endif
%if 0%{!?rhel:1} || 0%{?rhel} > 6
	--with-lzma						\
%else
	--without-lzma						\
%endif
	--without-libunwind					\
%ifarch sparc sparcv9 sparc64
	--without-mmap						\
%endif
	--enable-64-bit-bfd					\
%if %{have_inproctrace}
	--enable-inprocess-agent				\
%else
	--disable-inprocess-agent				\
%endif
$(: %{_bindir}/mono-gdb.py is workaround for mono BZ 815501. )										\
	      --with-auto-load-dir='$debugdir:$datadir/auto-load%{?scl::%{_root_datadir}/gdb/auto-load}'				\
	--with-auto-load-safe-path='$debugdir:$datadir/auto-load%{?scl::%{_root_datadir}/gdb/auto-load}:%{_root_bindir}/mono-gdb.py'	\
%ifarch sparc sparcv9
	sparc-%{_vendor}-%{_target_os}%{?_gnu}
%else
$(: It breaks RHEL-5 by %{_target_platform} being noarch-redhat-linux-gnu ) \
%ifarch noarch
	$(:)
%else
	--enable-targets=s390-linux-gnu,powerpc-linux-gnu,arm-linux-gnu,aarch64-linux-gnu	\
	%{_target_platform}
%endif
%endif

if [ -z "%{!?_with_profile:no}" ]
then
  # Run all the configure tests being incompatible with $FPROFILE_CFLAGS.
  %make configure-host configure-target
  %make clean

  # Workaround -fprofile-use:
  # linux-x86-low.c:2225: Error: symbol `start_i386_goto' is already defined
  %{make} -C gdb/gdbserver linux-x86-low.o
fi

# Global CFLAGS would fail on:
# conftest.c:1:1: error: coverage mismatch for function 'main' while reading counter 'arcs'
if [ "$fprofile" = "-fprofile" ]
then
  FPROFILE_CFLAGS='-fprofile-generate'
elif [ -z "%{!?_with_profile:no}" ]
then
  FPROFILE_CFLAGS='-fprofile-use'
  # We cannot use -fprofile-dir as the bare filenames clash.
  (cd ../%{gdb_build}-fprofile;
   # It was 333 on x86_64.
   test $(find -name "*.gcda"|wc -l) -gt 300
   find -name "*.gcda" | while read -r i
   do
     ln $i ../%{gdb_build}/$i
   done
  )
else
  FPROFILE_CFLAGS=""
fi

# Prepare gdb/config.h first.
%make  CFLAGS="$CFLAGS $FPROFILE_CFLAGS" LDFLAGS="$LDFLAGS $FPROFILE_CFLAGS" maybe-configure-gdb
perl -i.relocatable -pe 's/^(D\[".*_RELOCATABLE"\]=" )1(")$/${1}0$2/' gdb/config.status

%make  CFLAGS="$CFLAGS $FPROFILE_CFLAGS" LDFLAGS="$LDFLAGS $FPROFILE_CFLAGS"

! grep '_RELOCATABLE.*1' gdb/config.h
grep '^#define HAVE_LIBSELINUX 1$' gdb/config.h
grep '^#define HAVE_SELINUX_SELINUX_H 1$' gdb/config.h

if [ "$fprofile" = "-fprofile" ]
then
  cd gdb
  cp -p gdb gdb-withindex
  PATH="$PWD:$PATH" sh ../../gdb/gdb-add-index $PWD/gdb-withindex
  ./gdb -nx -ex q ./gdb-withindex
  ./gdb -nx -readnow -ex q ./gdb-withindex
  cd ..
fi

cd ..

done	# fprofile

cd %{gdb_build}

make \
$(: There was a race on RHEL-5: ) \
$(: fmtutil: format directory '/builddir/.texmf-var/web2c' does not exist. ) \
%if 0%{?el5:1}
     -j1 \
%else
     %{?_smp_mflags} \
%endif
     -C gdb/doc {gdb,annotate}{.info,/index.html,.pdf} MAKEHTMLFLAGS=--no-split MAKEINFOFLAGS=--no-split

grep '#define HAVE_ZLIB_H 1' gdb/config.h

# Copy the <sourcetree>/gdb/NEWS file to the directory above it.
cp $RPM_BUILD_DIR/%{gdb_src}/gdb/NEWS $RPM_BUILD_DIR/%{gdb_src}

%check
# Initially we're in the %{gdb_src} directory.
cd %{gdb_build}

%if 0%{!?_with_testsuite:1}
echo ====================TESTSUITE DISABLED=========================
%else
echo ====================TESTING=========================
cd gdb
gcc -o ./orphanripper %{SOURCE2} -Wall -lutil -ggdb2
# Need to use a single --ignore option, second use overrides first.
# No `%{?_smp_mflags}' here as it may race.
# WARNING: can't generate a core file - core tests suppressed - check ulimit
# "readline-overflow.exp" - Testcase is broken, functionality is OK.
(
  # ULIMIT required for `gdb.base/auxv.exp'.
  ulimit -H -c
  ulimit -c unlimited || :

  # Setup $CHECK as `check//unix/' or `check//unix/-m64' for explicit bitsize.
  # Never use two different bitsizes as it fails on ppc64.
  echo 'int main (void) { return 0; }' >biarch.c
  CHECK=""
  for BI in -m64 -m32 -m31 ""
  do
    # Do not use size-less options if any of the sizes works.
    # On ia64 there is no -m64 flag while we must not leave a bare `check' here
    # as it would switch over some testing scripts to the backward compatibility
    # mode: when `make check' was executed from inside the testsuite/ directory.
    if [ -z "$BI" -a -n "$CHECK" ];then
      continue
    fi
    # Do not use $RPM_OPT_FLAGS as the other non-size options will not be used
    # in the real run of the testsuite.
    if ! gcc $BI -o biarch biarch.c
    then
      continue
    fi
    CHECK="$CHECK check//unix/$BI"
  done
  # Do not try -m64 inferiors for -m32 GDB as it cannot handle inferiors larger
  # than itself.
  # s390 -m31 still uses the standard ELF32 binary format.
  gcc $RPM_OPT_FLAGS -o biarch biarch.c
  RPM_SIZE="$(file ./biarch|sed -n 's/^.*: ELF \(32\|64\)-bit .*$/\1/p')"
  if [ "$RPM_SIZE" != "64" ]
  then
    CHECK="$(echo " $CHECK "|sed 's# check//unix/-m64 # #')"
  fi

  # Disable some problematic testcases.
  # RUNTESTFLAGS='--ignore ...' is not used below as it gets separated by the
  # `check//...' target spawn and too much escaping there would be dense.
  for test in				\
    gdb.base/readline-overflow.exp	\
    gdb.base/bigcore.exp		\
  ; do
    mv -f ../../gdb/testsuite/$test ../gdb/testsuite/$test-DISABLED || :
  done

  # Run all the scheduled testsuite runs also in the PIE mode.
  # See also: gdb-runtest-pie-override.exp
  CHECK="$(echo $CHECK|sed 's#check//unix/[^ ]*#& &/-fPIC/-pie#g')"

  ./orphanripper make %{?_smp_mflags} -k $CHECK || :
)
for t in sum log
do
  for file in testsuite*/gdb.$t
  do
    suffix="${file#testsuite.unix.}"
    suffix="${suffix%/gdb.$t}"
    ln $file gdb-%{_target_platform}$suffix.$t || :
  done
done
# `tar | bzip2 | uuencode' may have some piping problems in Brew.
tar cjf gdb-%{_target_platform}.tar.bz2 gdb-%{_target_platform}*.{sum,log}
uuencode gdb-%{_target_platform}.tar.bz2 gdb-%{_target_platform}.tar.bz2
cd ../..
echo ====================TESTING END=====================
%endif

%install
# Initially we're in the %{gdb_src} directory.
cd %{gdb_build}
rm -rf %{buildroot}

# It would break RHEL-5 by leaving excessive files for the doc subpackage.
%ifnarch noarch

%make install DESTDIR=%{buildroot}

# Provide gdbtui for RHEL-5 and RHEL-6 as it is removed upstream (BZ 797664).
%if 0%{?rhel:1} && 0%{?rhel} <= 6
test ! -e %{buildroot}%{_prefix}/bin/gdbtui
install -m 755 %{SOURCE6} %{buildroot}%{_prefix}/bin/gdbtui
ln -sf gdb.1 %{buildroot}%{_mandir}/man1/gdbtui.1
%endif # 0%{?rhel:1} && 0%{?rhel} <= 6

mkdir -p %{buildroot}%{_sysconfdir}/gdbinit.d
touch -r %{SOURCE4} %{buildroot}%{_sysconfdir}/gdbinit.d
sed 's#%%{_sysconfdir}#%{_sysconfdir}#g' <%{SOURCE4} >%{buildroot}%{_sysconfdir}/gdbinit
touch -r %{SOURCE4} %{buildroot}%{_sysconfdir}/gdbinit

for i in `find %{buildroot}%{_datadir}/gdb/python/gdb -name "*.py"`
do
  # Files could be also patched getting the current time.
  touch -r $RPM_BUILD_DIR/%{gdb_src}/gdb/ChangeLog $i
done

%if 0%{?_enable_debug_packages:1} && 0%{!?_without_python:1}
mkdir -p %{buildroot}/usr/lib/debug%{_bindir}
cp -p $RPM_BUILD_DIR/%{gdb_src}/gdb/gdb-gdb.py %{buildroot}/usr/lib/debug%{_bindir}/
for pyo in "" "-O";do
  # RHEL-5: AttributeError: 'module' object has no attribute 'compile_file'
  python $pyo -c 'import compileall, re, sys; sys.exit (not compileall.compile_dir("'"%{buildroot}/usr/lib/debug%{_bindir}"'", 1, "'"/usr/lib/debug%{_bindir}"'"))'
done
%endif # 0%{?_enable_debug_packages:1} && 0%{!?_without_python:1}

mkdir %{buildroot}%{_datadir}/gdb/auto-load
%if 0%{!?_without_python:1}
%if 0%{?rhel:1} && 0%{?rhel} <= 6
# Temporarily now:
for LIB in lib lib64;do
  LIBPATH="%{buildroot}%{_datadir}/gdb/auto-load%{_root_prefix}/$LIB"
  mkdir -p $LIBPATH
  # basename is being run only for the native (non-biarch) file.
  sed -e 's,@pythondir@,%{_datadir}/gdb/python,'		\
      -e 's,@toolexeclibdir@,%{_root_prefix}/'"$LIB,"		\
      < $RPM_BUILD_DIR/%{gdb_src}/%{libstdcxxpython}/hook.in	\
      > $LIBPATH/$(basename %{_root_prefix}/%{_lib}/libstdc++.so.6.*)-gdb.py
  # Test the filename 'libstdc++.so.6.*' has matched.
  test -f $LIBPATH/libstdc++.so.6.[0-9]*-gdb.py
done
test ! -e %{buildroot}%{_datadir}/gdb/python/libstdcxx
cp -a $RPM_BUILD_DIR/%{gdb_src}/%{libstdcxxpython}/libstdcxx	\
      %{buildroot}%{_datadir}/gdb/python/libstdcxx
for i in `find %{buildroot}%{_datadir}/gdb/python -name "*.py"` \
         `find %{buildroot}%{_datadir}/gdb/auto-load%{_prefix} -name "*.py"` \
; do
  # Files come from gdb-archer.patch and can be also further patched.
  touch -r $RPM_BUILD_DIR/%{gdb_src}/gdb/ChangeLog $i
done
%else # 0%{!?rhel:1} || 0%{?rhel} > 6
# BZ 999645: /usr/share/gdb/auto-load/ needs filesystem symlinks
mkdir -p %{buildroot}%{_datadir}/gdb/auto-load
for i in $(echo bin lib $(basename %{_libdir}) sbin|tr ' ' '\n'|sort -u);do
  ln -s $(echo %{_root_prefix}|sed 's#^/*##')/$i \
        %{buildroot}%{_datadir}/gdb/auto-load/$i
done
%endif # 0%{!?rhel:1} || 0%{?rhel} > 6
%endif # 0%{!?_without_python:1}

# gdb-add-index cannot be run even for SCL package on RHEL<=6.
%if 0%{?rhel:1} && 0%{?rhel} <= 6
rm -f %{buildroot}%{_bindir}/gdb-add-index
%endif

# Remove the files that are part of a gdb build but that are owned and
# provided by other packages.
# These are part of binutils

rm -rf %{buildroot}%{_datadir}/locale/
rm -f %{buildroot}%{_infodir}/bfd*
rm -f %{buildroot}%{_infodir}/standard*
rm -f %{buildroot}%{_infodir}/configure*
rm -rf %{buildroot}%{_includedir}
rm -rf %{buildroot}/%{_libdir}/lib{bfd*,opcodes*,iberty*}

# pstack obsoletion

cp -p %{SOURCE3} %{buildroot}%{_mandir}/man1/gstack.1
ln -s gstack.1 %{buildroot}%{_mandir}/man1/pstack.1
ln -s gstack %{buildroot}%{_bindir}/pstack

# Packaged GDB is not a cross-target one.
(cd %{buildroot}%{_datadir}/gdb/syscalls
 rm -f mips*.xml
%ifnarch sparc sparcv9 sparc64
 rm -f sparc*.xml
%endif
%ifnarch x86_64
 rm -f amd64-linux.xml
%endif
%ifnarch %{ix86} x86_64
 rm -f i386-linux.xml
%endif
%ifnarch ppc ppc64
 rm -f ppc{,64}-linux.xml
%endif
)

# It would break RHEL-5 by leaving excessive files for the doc subpackage.
%if 0%{?el5:1}
rm -f %{buildroot}%{_infodir}/annotate.info*
rm -f %{buildroot}%{_infodir}/gdb.info*
%endif # 0%{?el5:1} 
%else # noarch
# -j1: There is some race resulting in:
# /usr/bin/texi2dvi: texinfo.tex appears to be broken, quitting.
make -j1 -C gdb/doc install DESTDIR=%{buildroot}
rm -rf %{buildroot}%{_mandir}
%endif # noarch

# Documentation only for development; keep 'rm's here after "noarch" above.
rm -f %{buildroot}%{_infodir}/gdbint*
rm -f %{buildroot}%{_infodir}/stabs*

# Delete this too because the dir file will be updated at rpm install time.
# We don't want a gdb specific one overwriting the system wide one.

rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

# It would break RHEL-5 by leaving excessive files for the doc subpackage.
%ifnarch noarch

%files
%defattr(-,root,root)
%doc COPYING3 COPYING COPYING.LIB README NEWS
%{_bindir}/gcore
%{_bindir}/gdb
%config(noreplace) %{_sysconfdir}/gdbinit
%{_sysconfdir}/gdbinit.d
%{_mandir}/*/gdbinit.5*
%{_mandir}/*/gdb.1*
%{_mandir}/*/gcore.1*
# gdb-add-index cannot be run even for SCL package on RHEL<=6.
%if 0%{!?rhel:1} || 0%{?rhel} > 6
%{_mandir}/*/gdb-add-index.1*
%endif
%{_bindir}/gstack
%{_mandir}/*/gstack.1*
# Provide gdbtui for RHEL-5 and RHEL-6 as it is removed upstream (BZ 797664).
%if 0%{?rhel:1} && 0%{?rhel} <= 6
%{_bindir}/gdbtui
%{_mandir}/*/gdbtui.1*
%endif # 0%{?rhel:1} && 0%{?rhel} <= 6
# gdb-add-index cannot be run even for SCL package on RHEL<=6.
%if 0%{!?rhel:1} || 0%{?rhel} > 6
%{_bindir}/gdb-add-index
%endif
%{_bindir}/pstack
%{_mandir}/*/pstack.1*
%{_datadir}/gdb

# don't include the files in include, they are part of binutils

%ifnarch sparc sparcv9
%files gdbserver
%defattr(-,root,root)
%{_bindir}/gdbserver
%{_mandir}/*/gdbserver.1*
%if %{have_inproctrace}
%{_libdir}/libinproctrace.so
%endif # %{have_inproctrace}
%endif

%if 0%{!?_without_python:1}
# [rhel] Do not migrate /usr/share/gdb/auto-load/ with symlinks on RHELs.
%if 0%{!?rhel:1}
%pre
for i in $(echo bin lib $(basename %{_libdir}) sbin|tr ' ' '\n'|sort -u);do
  src="%{_datadir}/gdb/auto-load/$i"
  dst="%{_datadir}/gdb/auto-load/%{_root_prefix}/$i"
  if test -d $src -a ! -L $src;then
    if ! rmdir 2>/dev/null $src;then
      mv -n $src/* $dst/
      rmdir $src
    fi
  fi
done
%endif # 0%{!?rhel:1}
%endif # 0%{!?_without_python:1}

# It would break RHEL-5 by leaving excessive files for the doc subpackage.
%endif # !noarch
%if 0%{!?el5:1} || "%{_target_cpu}" == "noarch"

%files doc
%doc %{gdb_build}/gdb/doc/{gdb,annotate}.{html,pdf}
%defattr(-,root,root)
%{_infodir}/annotate.info*
%{_infodir}/gdb.info*

%post doc
# This step is part of the installation of the RPM. Not to be confused
# with the 'make install ' of the build (rpmbuild) process.

# For --excludedocs:
if [ -e %{_infodir}/gdb.info.bz2 ]
then
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/annotate.info || :
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/gdb.info || :
fi

%preun doc
if [ $1 = 0 ]
then
  # For --excludedocs:
  if [ -e %{_infodir}/gdb.info.bz2 ]
  then
    /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/annotate.info || :
    /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/gdb.info || :
  fi
fi

%endif # 0%{!?el5:1} || "%{_target_cpu}" == "noarch"

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-5m)
- import two bug fix patches from fedora

* Thu Mar 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-4m)
- add workaround for upcomming GCC 4.9.x

* Sun Mar 16 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-3m)
- merged from T4R/gdb

* Wed Feb 26 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-2m)
- import bug fix patches from fedora

* Sun Feb  9 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-1m)
- update to 7.7
- import patches from fedora

* Fri Jan 10 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6.50.20130731-1m)
- update to snapshot version of 7.6.50
- import patches from fedora's gdb-7.6.50.20130731-18.fc21

* Tue Jun 18 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-1m)
- update to 7.6
- import a lot of patches from fedora's gdb-7.6-31.fc19

* Tue Oct 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5.0.20120926-1m)
- update 7.5.0.20120926

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.4.50.20120703-1m)
- update 7.4.50.20120703

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.4.50.20120603-3m)
- remove some files that conflicts with libstdc++

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.4.50.20120603-2m)
- update 7.4.50.20120603
-- merge from T4R/gdb

* Wed Mar 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.4.50.20120120-1m)
- update to 7.4.50
- merge from fedora's gdb-7.4.50.20120120-31.fc17

* Sun Nov  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:7.3.1-1m)
- update to 7.3.1 release
-- add Epoch
- import patches from fedora

* Wed Aug 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.50.20110722-2m)
- merge from TSUPPA4RI/gdb

* Mon Aug  1 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.50.20110722-1m)
- merge from fedora

* Sun May 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.90.20110525-1m)
- merge from fedora

* Sat May  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.50.20110429-1m)
- merge from fedora

* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.50.20110412-1m)
- merge from fedora

* Wed Mar 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.50.20110328-1m)
- update to the latest snapshot

* Wed Mar 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.50.20110315-1m)
- merge from fedora

* Wed Feb 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.50.20110222-1m)
- merge bug fixes from fedora gdb-7.2.50.20110222-26.fc15

* Fri Feb 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.50.20110218-1m)
- update to 7.3 pre-release
- merge a lot of changes from gdb-7.2.50.20110218-24.fc15

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-4m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-3m)
- rebuild against readline6

* Sun May  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-2m)
- drop useless patch

* Sat May  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-1m)
- update 7.1
- merge a lot of changes from fedora (gdb-7_1-18_fc13)

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.91.20090930-3m)
- rebuild against rpm-4.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.91.20090930-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.91.20090930-1m)
- update to 6.8.91.20090930
- apply patches from Rawhide (6.8.91.20090930-1)

* Wed Jun 24 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8.50.20090302-1m)
- merge many changes from fedora (6.8.50.20090302-33)
-- switch to 6.8.50 snapshot and archer branch

* Sat Apr 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8-10m)
- rebuild against rpm-4.7

* Sat Dec 27 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8-9m)
- sync with trunk (r28804)
- rebuild against rpm-4.6
-- update Patch274 from Rawhide (6.8-33)
-- temporarily drop momonga patch (Patch9999)
- License: GPLv3+

* Thu Nov  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-8m)
- merge many bug fixes from fc-devel (gdb-6_8-28_fc10)
- add gdb-6.8-momonga.patch

* Sun Aug 31 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-7m)
- fix version

* Thu Jul 17 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-6m)
- merge many bug fixes from fc-devel (gdb-6_8-17_fc10)

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-5m)
- merge changes from fc-devel (gdb-6_8-13_fc10)
--* Thu Jul  3 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.8-13
--- Support transparent debugging of inlined functions for an optimized code.
--* Fri Jun 20 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.8-12
--- Remove the gdb/gdbtui binaries duplicity.

* Thu Jun 19 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-4m)
- merge changes from fc-devel (gdb-6_8-11_fc10)

* Tue Jun  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-3m)
- merge changes from fc-devel (gdb-6_8-10_fc10)

* Sat Apr 26 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-2m)
- merge changes from fc-devel (gdb-6_8-4_fc10)
- delete unused patches

* Thu Apr 17 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-1m)
- update to 6.8 (copy from T4R/gdb r23541)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.7.1-10m)
- rebuild against gcc43

* Sun Feb 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.7.1-9m)
- Obsoletes: pstack

* Fri Feb 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-8m)
- sync with fc-devel (gdb-6_7_1-14_fc9)
--* Thu Feb 21 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-14
--- Rename `set debug build-id' as `set build-id-verbose', former level 1 moved
--  to level 2, default value is now 1, use `set build-id-verbose 0' now to
--  disable the missing separate debug filenames messages (BZ 432164).
--* Wed Feb 20 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-13
--- ia64 build fixes from Doug Chapman (BZ 428882).
--- gdbserver separated into an extra package (BZ 405791).
--- pstack obsoleted by included gstack (BZ 197020).
--- Fix #include <asm/ptrace.h> on kernel-headers-2.6.25-0.40.rc1.git2.fc9.x86_64.
--- Drop the PowerPC simulator as no longer being compatible with Fedora binaries.
--* Thu Feb  7 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-12
--- build-id debug messages print now the library names unconditionally.

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.1-7m)
- %%NoSource -> NoSource

* Fri Jan 25 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-6m)
- sync with fc-devel (gdb-6_7_1-11_fc9)
--* Thu Jan 24 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-11
--- Improve the text UI messages for the build-id debug files locating.
--  - Require now the rpm libraries.
--- Fix false `(no debugging symbols found)' on `-readnever' runs.
--- Extend the testcase `gdb.arch/powerpc-prologue.exp' for ppc64.
--* Sat Jan 12 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-10
--- Compilation fixup (-9 was never released).
--* Sat Jan 12 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-9
--- Fix also threaded inferiors for hardware watchpoints after the fork call.
--- Test debugging statically linked threaded inferiors (BZ 239652).
--  - It requires recent glibc to work in this case properly.
--- Testcase cleanup fixup of the gcore memory and time requirements of 6.7.1-8.
--* Thu Jan 10 2008 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-8
--- Fix detaching from a threaded formerly stopped process with non-primary
--  thread currently active (general BZ 233852).
--  - Enable back again the testcases named `attachstop.exp' (no such exist now).
--  - Rename the testcase `gdb.threads/attachstop' to `gdb.threads/attachstop-mt'.
--- Test ia64 memory leaks of the code using libunwind.
--- Testcase delay increase (for BZ 247354).
--- Test gcore memory and time requirements for large inferiors.

* Thu Jan 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-5m)
- sync with fc-devel (gdb-6_7_1-7_fc9)

* Sat Dec 15 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-4m)
- merged from TSUPPA4RI-branch
- import a patch from fc-devel
--* Mon Dec 10 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-6
--- Testsuite fixes for more stable/comparable results.

* Wed Nov 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-3m)
- added patch to fix compilation issue with gcc43
- imported some changes from fc-devel (gdb-6_7_1-4_fc9)
--* Fri Nov 16 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-4
--- Fix `errno' resolving across separate debuginfo files.
--- Fix segfault on no file loaded, `set debug solib 1', `info sharedlibrary'.
--- Support gdb.fortran/ tests by substituting the g77 compiler by gfortran.

* Sun Nov 11 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-2m)
- sync with fc-devel (gdb-6_7_1-3_fc9)
--* Sun Nov  4 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-3
--- Fix `errno' resolving on recent glibc with broken DW_AT_MIPS_linkage_name.
--- Imported new test for 6.7 PPC hiding of call-volatile parameter register.
--* Sat Nov  3 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-2
--- Backport `Breakpoints at multiple locations' patch primarily for C++.

* Fri Nov  2 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-1m)
- copy from trunk/pkgs/gdb r19471
- update to gdb-6.7.1
- sync with fc-devel (gdb-6_7_1-1_fc9)
--* Thu Nov  1 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7.1-1
--- Upgrade to GDB 6.7.1.  Drop redundant patches, forward-port remaining ones.
--* Thu Nov  1 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.7-1
--- Upgrade to GDB 6.7.  Drop redundant patches, forward-port remaining ones.
--- Fix rereading of the main executable on its change.

* Sat Oct 20 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-20m)
- sync with fc-devel (gdb-6_6-37_fc9)
--* Fri Oct 19 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-37
--- Fix hiding unexpected breakpoints on intentional step/next commands.
--- Fix s390 compilation warning/failure due to a wrongly sized type-cast.
--* Sun Oct 14 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-36
--- Fix hardware watchpoints after inferior forks-off some process.
--* Fri Oct 13 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-35
--- Fix non-threaded watchpoints CTRL-C regression on `set follow child'.

* Sat Oct 13 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-19m)
- sync with fc-devel (gdb-6_6-34_fc9)
--* Fri Oct 12 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-34
--- Fix gdbserver for threaded applications and recent glibc (BZ 328021).
--* Tue Oct  9 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-33
--- Fix debug load for sparse assembler files (such as vDSO32 for i386-on-x86_64).
--* Mon Oct  8 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-32
--- Set the breakpoints always to all the ctors/dtors variants (BZ 301701).
--- Fix a TUI visual corruption due to the build-id warnings (BZ 320061).
--- Fixed the kernel i386-on-x86_64 VDSO loading (producing `Lowest section in').
--* Fri Oct  5 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-31
--- Fix address changes of the ctors/dtors breakpoints w/multiple PCs (BZ 301701).
--- Delete an info doc file on `rpmbuild -bp' later rebuilt during `rpmbuild -bc'.

* Fri Sep 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-18m)
- sync with fc-devel (gdb-6_6-30_fc8)
--* Tue Sep 25 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-30
--- Fix re-setting of the ctors/dtors breakpoints with multiple PCs (BZ 301701).
--- Avoid one useless user question in the core files locator (build-id).
--* Sun Sep 23 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-29
--- Fixed the kernel VDSO loading (`warning: no loadable sections found in ...').
--- Fix the testcase for pending signals (from BZ 233852).
--* Sat Sep 22 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-28
--- Support also the `$allocate' and `$delete' ctor/dtor variants (BZ 301701).
--- Fix the build compatibility with texinfo >= 4.10.
--- Fix the testcase for pending signals (from BZ 233852).

* Thu Sep 20 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-17m)
- sync with fc-devel (gdb-6_6-27_fc8)
--* Sun Sep 16 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-27
--- Fix attaching to stopped processes and/or pending signals.

* Mon Sep  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-16m)
- sync with fc-devel (gdb-6_6-26_fc8)
--* Tue Aug 28 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-26
--- New fast verification whether the .debug file matches its peer (build-id).
--- New locating of the matching binaries from the pure core file (build-id).
--* Fri Aug 17 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-25
--- Fixed excessive RPATH (related to BZ 228891).

* Sun Aug 12 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-15m)
- sync with fc-devel (gdb-6_6-24_fc8)
--* Wed Aug  8 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-24
--- Fixed compatibility with the Rawhide glibc open(2) syscall sanity checking.
--- Update the core_dump_elf_headers=1 compatibility code to the upstream variant.
--* Mon Aug  6 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-23
--- Update PPC unwinding patches to their upstream variants (BZ 140532).
--* Wed Jul 25 2007 Jesse Keating <jkeating@redhat.com> - 6.6-22
--- Rebuild for RH #249435
--* Mon Jul 23 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-21
--- Fixed compatibility with Rawhide kernel fs.binfmt_elf.core_dump_elf_headers=1.
--- .spec file updates to mostly pass RPMLINT - Fedora merge review (BZ 225783).
--- Fixed testcase of the exit of a thread group leader (of BZ 247354).
--- Cleanup any leftover testsuite processes as it may stuck mock(1) builds.

* Mon Jul  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-14m)
- sync with fc-devel (gdb-6_6-20_fc8)
--* Sun Jul  8 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-20
--- Do not hang on exit of a thread group leader (BZ 247354).
--- New test for upstream fix of VDSO decoding while attaching to an i386 process.
--- Fixed BZ # 232014 -> 232015.
--* Thu Jul  5 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-19
--- Link with libreadline provided by the operating system.

* Sat Jun 30 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-13m)
- sync with fc-devel (gdb-6_6-18_fc8)
-- 2 bugs are fixed

* Thu Jun 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-12m)
- sync with fc-devel (gdb-6_6-16_fc8)

* Wed Jun  6 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-11m)
- sync with f-7 (gdb-6_6-15_fc7)
--* Tue Jun  5 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-15
--- Fix crash on missing filenames debug info (BZ 242155).

* Mon May 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-10m)
- sync with fc-devel (gdb-6_6-14_fc7)

* Wed Apr 25 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-9m)
- sync with fc-devel (6.6-11_fc7)
--* Tue Apr 24 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-11
--- Package review, analysed by Ralf Corsepius (BZ 225783).
---- Fix prelink(8) testcase for non-root $PATH missing `/usr/sbin' (BZ 225783).
---- Fix debugging GDB itself - the compiled in source files paths (BZ 225783).
---- Fix harmless GCORE stack buffer overflow, by _FORTIFY_SOURCE=2 (BZ 235753).
---- Fix XML support - the build was missing `expat-devel'.
---- Updated the `info' files handling by the spec file.
---- Building now with the standard Fedora code protections - _FORTIFY_SOURCE=2.
---- Use multiple CPUs for the build (%{?_smp_mflags}).
---- Separate testsuite run to its %check section.
---- Fix (remove) non-ASCII spec file characters.
---- Remove system tools versions dumping - already present in mock build logs.


* Mon Apr 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-8m)
- added "ulimit -c unlimited" for testing
- sync with fc-devel (6.6-10_fc7)
--* Sun Apr 22 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-10
--- Notify user of a child forked process being detached (BZ 235197).
--* Sun Apr 22 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-9
--- Allow running `/usr/bin/gcore' with provided but inaccessible tty (BZ 229517).
--- Fix testcase for watchpoints in threads (for BZ 237096).
--- BuildRequires now `libunwind-devel' instead of the former `libunwind'.
--- Use the runtime libunwind .so.7, it requires now >= 0.99-0.1.frysk20070405cvs.

--* Sat Mar 24 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-8
--- Use definition of an empty structure as it is not an opaque type (BZ 233716).
--- Fixed the gdb.base/attachstop.exp testcase false 2 FAILs.

* Thu Mar 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-7m)
- sync with fc-devel (6.6-7_fc7)
--* Thu Mar 15 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-7
--- Suggest SELinux permissions problem; no assertion failure anymore (BZ 232371).

* Thu Mar 15 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-6m)
- sync with fc-devel (6.6-6_fc7)
--* Wed Mar 14 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-6
--- Fix occasional dwarf2_read_address: Corrupted DWARF expression (BZ 232353).
--
--* Mon Mar 12 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-5
--- Temporary support for shared libraries >2GB on 64bit hosts. (BZ 231832)
--
--* Sun Feb 25 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-4
--- Backport + testcase for PPC Power6/DFP instructions disassembly (BZ 230000).

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6-5m)
- Momonga Linux 3 -> Momonga Linux 4
- fix gcc-4.2
-- gdb-6.6-ada-invalild-cast.patch

* Sun Feb 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-4m)
- update gdb-6.6
- merged from TSUPPA4RI-branch
-* Tue Feb  6 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
-- (6.6-3m)
-- sync with fc-devel (gdb-6_6-3_fc7)
---* Mon Feb  5 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-3
---- Fix a race during attaching to dying threads; backport (BZ 209445).
---- Testcase of unwinding has now marked its unsolvable cases (for BZ 140532).
-* Sat Jan 27 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
-- (6.6-2m)
-- sync with fc-devel (gdb-6_6-2_fc7)
---* Fri Jan 26 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-2
---- Backported post gdb-6.6 release PPC `show endian' fixup.
---- Fix displaying of numeric char arrays as strings (BZ 224128).
---- Simplified patches by merging upstream accepted ones into a single file.
-* Thu Jan 25 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
-- (6.6-1m)
-- copy from trunk (r13742)
-- add specopt "do_test" to skip testing
-- sync with fc-devel (gdb-6_6-1_fc7)
---* Sat Jan 20 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.6-1
---- Upgrade to GDB 6.6.  Drop redundant patches, forward-port remaining ones.
---- Backported post gdb-6.6 release ia64 unwinding fixups.
---- Testcase for exec() from threaded program (BZ 202689).
---* Mon Jan 15 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-27
---- Fix the testsuite results broken in 6.5-26, stop invalid testsuite runs.

* Sat Jan 13 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5-12m)
- sync with fc-devel (6.5-26_fc7)
--* Fri Jan 13 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-26
--- Fix unwinding of non-debug (.eh_frame) PPC code, Andreas Schwab (BZ 140532).
--- Fix unwinding of debug (.debug_frame) PPC code, workaround GCC (BZ 140532).
--- Fix missing testsuite .log output of testcases using get_compiler_info().
--* Fri Jan 12 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-25
--- Fix unwinding of non-CFI (w/o debuginfo) PPC code by recent GCC (BZ 140532).
--* Thu Jan 11 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-24
--- Backport readline history for input mode commands like `command' (BZ 215816).

* Wed Jan 10 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5-11m)
- sync with fc-devel (6.5-23_fc7)
--* Tue Jan  9 2007 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-23
--- Find symbols properly at their original (included) file (BZ 109921).
--- Remove the stuck mock(1) builds disfunctional workaround (-> mock BZ 221351).
--* Sat Dec 30 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-22
--- Fix unwinding crash on older gcj(1) code (extended CFI support) (BZ 165025).
--- Include testcase for the readline history of input mode commands (BZ 215816).
--* Sat Dec 23 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-21
--- Try to reduce sideeffects of skipping ppc .so libs trampolines (BZ 218379).
--- Fix lockup on trampoline vs. its function lookup; unreproducible (BZ 218379).

* Wed Dec 20 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5-10m)
- sync with fc-devel (6.5-20_fc7)
-* Tue Dec 19 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-20
-- Fix bogus 0x0 unwind of the thread's topmost function clone(3) (BZ 216711).
-- Testcase for readline segfault on excessively long hand-typed lines.

* Wed Dec 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5-9m)
- sync with fc-devel (6.5-19_fc7)
-* Tue Dec 12 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-19
-- Fix attachment also to a threaded stopped process (BZ 219118).
-- Cleanup any leftover testsuite processes as it may stuck mock(1) builds.

* Sat Nov 25 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5-8m)
- sync with fc-devel (6.5-18_fc7)
-* Sat Nov 25 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-18
-- Fix readline history for input mode commands like `command' (BZ 215816).
-* Wed Nov 16 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-17
-- Bugfix testcase typo of gdb-6.5-16.
-* Wed Nov 16 2006 Jan Kratochvil <jan.kratochvil@redhat.com> - 6.5-16
-- Provide testcase for accessing the last address space byte.

* Sun Nov 12 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5-7m)
- updated to 6.5
- merged from branches/TSUPPA4RI (r12642)

* Sun Jun 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3-4m)
- Momonga Linux 2 -> Momonga Linux 3

* Sun Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3-3m)
- change version name 
-- RedHat Linux -> Momonga Linux 2

* Sun Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3-2m)
- sync from FC(gdb-6.3.0.0-1.94)

* Tue Feb 15 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (6.3-1m)
- update 6.3 and import from FC

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.0-2m)
- enable x86_64.

* Mon Mar 29 2004 Toru Hoshina <t@momonga-linux.org>
- (6.0-2m)
- /usr/share/info/annotate.info is missed.

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (6.0-1m)
- version up. this is expected for kernel 2.6 era, probably.
- dejagnu is also imported, evaluation is needed from now on.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (5.3-2m)
- revised spec for enabling rpm 4.2.

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.3-1m)
  update to 5.3

* Mon Jul 29 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.2.1-1m)
  update to 5.2.1

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (5.1.1-4k)
- /sbin/install-info -> info in PreReq.

* Thu Feb 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (5.1.1-2k)
- fix URL
  update to 5.1.1

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Mon Aug 27 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.0-22k)
- removed alpha.patch for building

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- release version tar ball

* Sun Mar 18 2001 Toru Hoshina <toru@df-usa.com>
- new snapshot

* Sat Feb 04 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.0-15k)
- update source to gdb-20010119.tar.bz2
- Delete gdb-5.0-alpha.patch and gdb-5.0-sparc.patch.
  They have been already included in source.
- don't Exclude sparc

* Thu Nov 09 2000 Kenichi Matsubara <m@kondara.org>
- [5.0-10k]
- bugfix %post.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Jul 25 2000 Trond Eivind Glomsrod <teg@redhat.com>
- upgrade to CVS snapshot
- excludearch SPARC, build on IA61

* Wed Jul 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jul 02 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Fri Jun 08 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%configure, %%makeinstall, %%{_infodir}, %%{_mandir},
  and %%{_tmppath}
- the install scripts  for info are broken(they don't care about
  you specify in the installstep), work around that.
- don't build for IA64

* Mon May 22 2000 Trond Eivind Glomsrod <teg@redhat.com>
- upgraded to 5.0 - dump all patches. Reapply later if needed.
- added the NEWS file to the %%doc files
- don't delete files which doesn't get installed (readline, texinfo)
- let build system handle stripping and gzipping
- don't delete libmmalloc
- apply patch from jakub@redhat.com to make it build on SPARC

* Sun May 20 2000 Takaaki Tabuchi <tab@kondara.org>
- update 5.0.

* Fri Apr 28 2000 Matt Wilson <msw@redhat.com>
- rebuilt against new ncurses

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (4.18-11).

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Tue Feb  8 2000 Jakub Jelinek <jakub@redhat.com>
- fix core file handling on i386 with glibc 2.1.3 headers

* Fri Jan 14 2000 Jakub Jelinek <jakub@redhat.com>
- fix reading registers from core on sparc.
- hack around build problems on i386 with glibc 2.1.3 headers

* Thu Oct 7 1999 Jim Kingdon
- List files to install in %{_infodir} specifically (so we don't pick up
things like info.info from GDB snapshots).

* Thu Oct 7 1999 Jim Kingdon
- Update GDB to 19991004 snapshot.  This eliminates the need for the
sigtramp, sparc, xref, and threads patches.  Update sparcmin patch.

* Mon Aug 23 1999 Jim Kingdon
- Omit readline manpage.

* Tue Aug 7 1999 Jim Kingdon
- Remove H.J. Lu's patches (they had been commented out).
- Add sigtramp patch (from gdb.cygnus.com) and threads patch (adapted
from code fusion CD-ROM).

* Wed Apr 14 1999 Jeff Johnson <jbj@redhat.com>
- merge H.J. Lu's patches into 4.18.

* Mon Apr 05 1999 Cristian Gafton <gafton@redhat.com>
- updated the kern22 patch with stuff from davem

* Thu Apr  1 1999 Jeff Johnson <jbj@redhat.com>
- sparc with 2.2 kernels no longer uses sunos ptrace (davem)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 3)

* Mon Mar  8 1999 Jeff Johnson <jbj@redhat.com>
- Sparc fiddles for Red Hat 6.0.
