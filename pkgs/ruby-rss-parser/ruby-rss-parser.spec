%global momorel 4

Summary: Ruby RSS Parser
Name: ruby-rss-parser
Version: 0.2.6
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: Ruby
URL: http://raa.ruby-lang.org/project/rss/
Source0: http://www.cozmixng.org/~kou/download/rss-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-xmlparser
BuildArch: noarch

%description
This library can parse RSS (RDF Site Summary) 1.0, RSS 0.9x/2.0
excluding 0.90 and 1.0 with validation (or non validation).

%prep
%setup -q -n rss-%{version}

%build
ruby setup.rb config \
    --prefix=%{_prefix} \
    --bindir=%{_bindir} \
    --libdir=%{_libdir} \
    --siterubyver=%{ruby_sitelibdir}
ruby setup.rb setup

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby setup.rb config \
    --prefix=%{buildroot}%{_prefix} \
    --bindir=%{buildroot}%{_bindir} \
    --libdir=%{buildroot}%{_libdir} \
    --siterubyver=%{buildroot}%{ruby_sitelibdir}
ruby setup.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr (-,root,root)
%{ruby_sitelibdir}/rss*
%doc README.* Reference.* Tutorial.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.6-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.6-1m)
- update 0.2.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.8-2m)
- rebuild against gcc43

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-1m)
- initial packaging
