%global         momorel 1

Name:           perl-Catalyst-Plugin-Authorization-ACL
Version:        0.16
Release:        %{momorel}m%{?dist}
Summary:        ACL support for Catalyst applications
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Plugin-Authorization-ACL/
Source0:        http://www.cpan.org/authors/id/R/RK/RKITOVER/Catalyst-Plugin-Authorization-ACL-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-base
BuildRequires:  perl-Carp
BuildRequires:  perl-Catalyst-Runtime
BuildRequires:  perl-Catalyst-Plugin-Authentication
BuildRequires:  perl-Catalyst-Plugin-Authorization-Roles
BuildRequires:  perl-Catalyst-Plugin-Session
BuildRequires:  perl-Catalyst-Plugin-Session-State-Cookie
BuildRequires:  perl-Class-Throwable
BuildRequires:  perl-Cwd
BuildRequires:  perl-Exporter
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-IPC-Open3
BuildRequires:  perl-lib
BuildRequires:  perl-Moose
BuildRequires:  perl-mro
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Scalar-List-Util
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-WWW-Mechanize-Catalyst
BuildRequires:  perl-Tree-Simple
BuildRequires:  perl-Tree-Simple-VisitorFactory
Requires:       perl-Carp
Requires:       perl-Catalyst-Runtime
Requires:       perl-Class-Throwable
Requires:       perl-Exporter
Requires:       perl-Moose
Requires:       perl-mro
Requires:       perl-namespace-autoclean
Requires:       perl-Scalar-List-Util
Requires:       perl-Tree-Simple
Requires:       perl-Tree-Simple-VisitorFactory
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides Access Control List style path protection, with
arbitrary rules for Catalyst applications. It operates only on the Catalyst
private namespace, at least at the moment.

%prep
%setup -q -n Catalyst-Plugin-Authorization-ACL-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/Catalyst/Plugin/Authorization/ACL.pm
%{perl_vendorlib}/Catalyst/Plugin/Authorization/ACL
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- rebuild against perl-5.20.0
- update to 0.16

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-7m)
- rebuild against perl-5.12.1

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15-6m)
- add BuildRequires:  perl-Catalyst-Plugin-Authorization-Roles

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15-2m)
- update BuildRequires:  perl-Catalyst-Runtime >= 5.80013

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sun Sep 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.10.1

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09
- Specfile re-generated by cpanspec 1.77 for Momonga Linux.

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08-4m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.08-3m)
- use vendor

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08-2m)
- delete dupclicate directory

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
