%global momorel 1
%define pixman_version 0.18.4
%define freetype_version 2.1.9
%define fontconfig_version 2.2.95

Summary:	A 2D graphics library
Name:		cairo
Version:	1.12.16
Release: %{momorel}m%{?dist}
URL:		http://cairographics.org
#VCS:		git:git://git.freedesktop.org/git/cairo
#Source0:	http://cairographics.org/snapshots/%{name}-%{version}.tar.xz
Source0:	http://cairographics.org/releases/%{name}-%{version}.tar.xz
NoSource: 0
License:	LGPLv2 or MPLv1.1
Group:		System Environment/Libraries

BuildRequires: pkgconfig
BuildRequires: libXrender-devel
BuildRequires: libX11-devel
BuildRequires: libpng-devel
BuildRequires: libxml2-devel
BuildRequires: pixman-devel >= %{pixman_version}
BuildRequires: freetype-devel >= %{freetype_version}
BuildRequires: fontconfig-devel >= %{fontconfig_version}
BuildRequires: glib2-devel
BuildRequires: librsvg2-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libEGL-devel

%description
Cairo is a 2D graphics library designed to provide high-quality display
and print output. Currently supported output targets include the X Window
System, OpenGL (via glitz), in-memory image buffers, and image files (PDF,
PostScript, and SVG).

Cairo is designed to produce consistent output on all output media while
taking advantage of display hardware acceleration when available (e.g.
through the X Render Extension or OpenGL).

%package devel
Summary: Development files for cairo
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libXrender-devel
Requires: libpng-devel
Requires: pixman-devel >= %{pixman_version}
Requires: freetype-devel >= %{freetype_version}
Requires: fontconfig-devel >= %{fontconfig_version}
Requires: pkgconfig

%description devel
Cairo is a 2D graphics library designed to provide high-quality display
and print output.

This package contains libraries, header files and developer documentation
needed for developing software which uses the cairo graphics library.

%package gobject
Summary: GObject bindings for cairo
Group: System Environment/Libraries

%description gobject
Cairo is a 2D graphics library designed to provide high-quality display
and print output.

This package contains functionality to make cairo graphics library
integrate well with the GObject object system used by GNOME.

%package gobject-devel
Summary: Development files for cairo-gobject
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: pkgconfig

%description gobject-devel
Cairo is a 2D graphics library designed to provide high-quality display
and print output.

This package contains libraries, header files and developer documentation
needed for developing software which uses the cairo Gobject library.

%package tools
Summary: Development tools for cairo
Group: Development/Tools

%description tools
Cairo is a 2D graphics library designed to provide high-quality display
and print output.

This package contains tools for working with the cairo graphics library.
 * cairo-trace: Record cairo library calls for later playback

%prep
%setup -q

%build
%configure --disable-static	\
	--enable-warnings	\
	--enable-xlib		\
	--enable-freetype	\
	--enable-ps		\
	--enable-pdf		\
	--enable-svg		\
	--enable-tee		\
	--enable-gl		\
	--enable-gobject	\
	--disable-gtk-doc
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make V=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install V=1 DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post gobject -p /sbin/ldconfig
%postun gobject -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS BIBLIOGRAPHY BUGS COPYING COPYING-LGPL-2.1 COPYING-MPL-1.1 NEWS README
%{_libdir}/libcairo.so.*
%{_libdir}/libcairo-script-interpreter.so.*
%{_bindir}/cairo-sphinx

%files devel
%defattr(-,root,root,-)
%doc ChangeLog PORTING_GUIDE
%dir %{_includedir}/cairo/
%{_includedir}/cairo/cairo-deprecated.h
%{_includedir}/cairo/cairo-features.h
%{_includedir}/cairo/cairo-ft.h
%{_includedir}/cairo/cairo.h
%{_includedir}/cairo/cairo-pdf.h
%{_includedir}/cairo/cairo-ps.h
%{_includedir}/cairo/cairo-script-interpreter.h
%{_includedir}/cairo/cairo-svg.h
%{_includedir}/cairo/cairo-tee.h
%{_includedir}/cairo/cairo-version.h
%{_includedir}/cairo/cairo-xlib-xrender.h
%{_includedir}/cairo/cairo-xlib.h
%{_includedir}/cairo/cairo-gl.h
%{_includedir}/cairo/cairo-script.h
%{_includedir}/cairo/cairo-xcb.h
%{_libdir}/libcairo.so
%{_libdir}/libcairo-script-interpreter.so
%{_libdir}/pkgconfig/cairo-fc.pc
%{_libdir}/pkgconfig/cairo-ft.pc
%{_libdir}/pkgconfig/cairo.pc
%{_libdir}/pkgconfig/cairo-pdf.pc
%{_libdir}/pkgconfig/cairo-png.pc
%{_libdir}/pkgconfig/cairo-ps.pc
%{_libdir}/pkgconfig/cairo-svg.pc
%{_libdir}/pkgconfig/cairo-tee.pc
%{_libdir}/pkgconfig/cairo-xlib.pc
%{_libdir}/pkgconfig/cairo-xlib-xrender.pc
%{_libdir}/pkgconfig/cairo-egl.pc
%{_libdir}/pkgconfig/cairo-gl.pc
%{_libdir}/pkgconfig/cairo-glx.pc
%{_libdir}/pkgconfig/cairo-script.pc
%{_libdir}/pkgconfig/cairo-xcb-shm.pc
%{_libdir}/pkgconfig/cairo-xcb.pc
%{_datadir}/gtk-doc/html/cairo

%files gobject
%defattr(-,root,root,-)
%{_libdir}/libcairo-gobject.so.*

%files gobject-devel
%defattr(-,root,root,-)
%{_includedir}/cairo/cairo-gobject.h
%{_libdir}/libcairo-gobject.so
%{_libdir}/pkgconfig/cairo-gobject.pc

%files tools
%defattr(-,root,root,-)
%{_bindir}/cairo-trace
%{_libdir}/cairo/

%changelog
* Tue Aug 27 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12.16-1m)
- update 1.12.16

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.14-1m)
- update 1.12.14

* Sat Feb  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.12-1m)
- update 1.12.12

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.10-1m)
- update 1.12.10

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.8-1m)
- update 1.12.8

* Fri Nov  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.6-1m)
- update 1.12.6

* Fri Oct 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.4-1m)
- update 1.12.4

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.2-3m)
- add %%post and %%postun

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.2-2m)
- rebuild for librsvg2 2.36.1

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.2-1m)
- update to 1.12.2
- reimport from fedora

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-5m)
- set --enable-tee option for new firefox4 (xulrunner-2.0)

* Tue Apr 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10.2-4m)
- set %%global with_gl 0 for the moment

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.2-3m)
- rebuild for new GCC 4.6

* Tue Feb 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.2-2m)
- remove unrecognized options from %%configure;  --enable-glitz, --enable-freetype
- add specopt named "with_gl" to disable opengl backend

* Sat Jan  8 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.2-1m)
- update to 1.10.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.0-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.10-2m)
- full rebuild for mo7 release

* Tue Mar 16 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.10-1m)
- update to 1.8.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.8-1m)
- update to 1.8.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-2m)
- rebuild against rpm-4.6

* Mon Dec 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.6-1m)
- update to 1.8.6

* Sun Nov 16 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.4-1m)
- update to 1.8.4

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Tue Oct  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-5m)
- add Requires at %%package devel

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.6-1m)
- update 1.7.6 (unstable)

* Sun Sep 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-4m)
- add Requires: glitz

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-3m)
- add Requires: xcb-util-devel

* Thu Apr 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-2m)
- add BuildRequires: pixman >= 0.10.0

* Sun Apr 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.14-3m)
- rebuild against gcc43

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.14-2m)
- add many configure flags

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.14-1m)
- update to 1.4.14

* Sat Dec  1 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.12-1m)
- update to 1.4.12
- [SECURITY] CVE-2007-5503

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.10-1m)
- update to 1.4.10

* Tue Jun 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-2m)
- BuildRequires: freetype2-devel -> freetype-devel

* Fri Mar 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Tue Mar  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.14-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.14-1m)
- update to 1.3.14 (development snapshot)

* Fri Nov 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4
- delete libtool library

* Fri Mar 17 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.4-1m)
- version 1.0.4(bug-fix release)
- remove Patch0: cairo-1.0.0-pkgconfig-0_15.patch
-        Patch1: cairo-1.0.2-dup-define.patch

* Wed Jan 18 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-2m)
- add Patch1: cairo-1.0.2-dup-define.patch
- Bug #5136 (https://bugs.freedesktop.org/show_bug.cgi?id=5136)
- Remove duplicate definitions of pixman_color_to_pixel and pixman_composite.

* Fri Oct  7 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-1m)
- version 1.0.2

* Thu Sep 22 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-3m)
- revised Patch0(add xft in cairo.pc)

* Thu Sep 22 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-2m)
- add Patch0: cairo-1.0.0-pkgconfig-0_15.patch

* Tue Aug 30 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- initial import

