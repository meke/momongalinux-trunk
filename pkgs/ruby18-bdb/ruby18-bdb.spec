%global momorel 11

%global ruby_sitearchdir %(ruby18 -rrbconfig -e "puts Config::CONFIG['sitearchdir']")

%define        tarname bdb
Summary:       Oracle Berkeley DB and DB XML
Name:          ruby18-bdb
Version:       0.6.5
Release:       %{momorel}m%{?dist}
License:       Ruby
Group:         Development/Libraries
Source0:       ftp://moulon.inra.fr/pub/ruby/%{tarname}-%{version}.tar.gz
Patch0:        ruby18-bdb-libdb48.patch
URL:           http://moulon.inra.fr/ruby/bdb.html
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18, ruby18-devel, db4-devel >= 4.8.26-1m
Requires:      ruby(abi) = 1.8
Provides:      ruby(bdb) = %{version}
Obsoletes:     ruby-bdb < 0.6.5-6m

%description
Berkeley DB is an embedded database system that supports keyed access
to data.

%prep
%setup -q -n %{tarname}-%{version}
%patch0 -p1

%build
ruby18 extconf.rb
make %{?_smp_mflags}

%check
make test

%clean 
rm -rf $RPM_BUILD_ROOT


%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT install


%files 
%defattr(-,root,root)
%doc Changes README.en bdb.html bdb.rd docs/ examples/
%{ruby_sitearchdir}/bdb.so

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-11m)
- add source

* Mon Sep  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.5-10m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-7m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-6m)
- renamed from ruby-bdb which became obsolete

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-5m)
- build with ruby18
-- ruby-bdb on ruby19 does not work well

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-4m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-2m)
- rebuild against rpm-4.6

* Wed Oct  8 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5
- build against db4-4.7.25-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-2m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamzaki <muradaikan@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2
- rebuild against db4-4.6.21

* Fri Jul 13 2007 zunda <zunda at freeshell.org>
- (0.6.0-4m)
- Reverted install section to 0.6.0-2m. Rebuilding the package
  somehow fixed the location of the library.

* Fri Jul 13 2007 zunda <zunda at freeshell.org>
- (0.6.0-3m)
- Moved bdb.so from /usr/local/lib/ruby ... to /usr/lib/ruby ...

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.0-2m)
- rebuild against ruby-1.8.6-4m

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0
- build against db-4.5

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.7-2m)
- rebuild against db4.3

* Sun Nov 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.7-1m)
- version up

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.6-3m)
- /usr/lib/ruby

* Sun Jun 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.6-2m)
- fixed missing directory name by tarball

* Mon May 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.6-1m)
- initial import to Momonga
