%global momorel 1
#global hg_version 16759

%global srcname %{name}-%{hg_version}
%global __spec_install_post /usr/lib/rpm/brp-compress

Summary: The Go Programming Language
Name: golang

Version: 1.2
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Development/Languages

Source0: http://go.googlecode.com/files/go1.2.src.tar.gz
NoSource: 0

# made by
# hg clone -r release https://go.googlecode.com/hg/ $GOROOT
# and archvie
#Source0: #{srcname}.tar.xz

BuildRequires: gcc emacs >= %{_emacs_version}
ExclusiveArch: noarch %{ix86} x86_64 arm
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://golang.org/

%description
Go is simple, fast, safe, concurrent, fun, open source language

go lang is not installed in $HOME, so you need set some environment
variables: GOROOT, GOBIN, GOARCH, and GOOS.
Please write in .bashrc below.

 export GOROOT=/usr/{lib|lib64}/go
 export GOBIN=/usr/bin
 export GOARCH={386|amd64}
 export GOOS=linux

%package -n emacs-go-mode
Summary: An Emacs major mode for editing Go source text
Group: Applications/Editors
Requires: emacs >= %{_emacs_version}

%description -n emacs-go-mode
Go-mode is a major mode for editing Go source text.

%prep
%setup -q -n go

%build
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}

export GOROOT=$(pwd)
export GOOS=linux
export GOBIN=%{buildroot}%{_bindir}
export PATH="$PATH:%{buildroot}%{_bindir}"
export DISABLE_NET_TESTS=1
%ifarch %{ix86}
export GOARCH=386
%endif
%ifarch x86_64
export GOARCH=amd64
%endif
%ifarch arm
export GOARCH=arm
%endif

# for main package
pushd src
bash ./all.bash
popd

# for emacs
%{_emacs_bytecompile} misc/emacs/go-mode.el

%install
# for emacs-lisp
mkdir -p %{buildroot}%{_emacs_sitelispdir}/go-mode
install -m 644 misc/emacs/go-mode-load.el %{buildroot}%{_emacs_sitelispdir}/go-mode
install -m 644 misc/emacs/go-mode.el* %{buildroot}%{_emacs_sitelispdir}/go-mode

# for runtime
mkdir -p %{buildroot}%{_libdir}/go
cp -ar pkg %{buildroot}%{_libdir}/go

# for document
cp -ar lib %{buildroot}%{_libdir}/go
cp -ar src %{buildroot}%{_libdir}/go
find %{buildroot}%{_libdir}/go/src -type f | grep -v "\.go$" | xargs rm
cp -ar doc %{buildroot}%{_libdir}/go

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc README LICENSE CONTRIBUTORS AUTHORS
%{_bindir}/go
#%{_bindir}/godoc
%{_bindir}/gofmt
%{_libdir}/go

%files -n emacs-go-mode
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/go-mode
%{_emacs_sitelispdir}/go-mode/go-mode-load.el
%{_emacs_sitelispdir}/go-mode/go-mode.el*

%changelog
* Thu Jan 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Tue May  7 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0.16759-1m)
- update go-1.1-rc2-rev14501

* Thu Oct  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0.14501-1m)
- update go-rev14501

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.12882-2m)
- rebuild for emacs-24.1

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0.12882-1m)
- update go-rev12882

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.11094-1m)
- update go-rev11094

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.10030-1m)
- update go-rev10030

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9323-2m)
- revise emacs-go-mode

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.9323-1m)
- update go-rev9323

* Thu Jun 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.8282-2m)
- remove -Werror to enable build

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.8282-1m)
- update go-rev8282

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7002-2m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.7002-1m)
- update go-rev7002
- delete specopt part

* Tue Dec 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.6945-1m)
- update go-rev6945
- update Patch0
- comment out Patch1
- add Patch2: go-tick_test.patch
- fix %%files
- tmp comment out specopt "do_test"

* Tue Dec 14 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.0.6790-3m)
- use DISABLE_NET_TESTS for disabling test insted of patch

* Tue Dec 14 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.0.6790-2m)
- revised notest2.patch

* Sun Nov 28 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.6790-1m)
- update go-rev6790

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5960-4m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.0.5960-3m)
- fix request_test URL

* Tue Sep 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.6270-1m)
- update go-rev6270
- update Patch0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.5960-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.5960-1m)
- update go-rev5960

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4592-2m)
- rebuild against emacs-23.2

* Wed Jan 13 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4592-1m)
- update
- set specopt "do_test"

* Wed Jan 13 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4531-5m)
- add Patch0

* Sun Jan 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4531-4m)
- enable godoc -http=:port

* Sun Jan 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4531-3m)
- delete unused binary files for godoc

* Sun Jan 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4531-2m)
- enable godoc

* Sun Jan 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4531-1m)
- update to 4531
- please read rpm -qi golang ;-)

* Sat Jan  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4564-1m)
- version up

* Thu Jan  7 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4542-1m)
- version up

* Tue Dec 29 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4506-1m)
- version up

* Sat Dec 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4493-1m)
- version up

* Thu Dec 24 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4491-1m)
- version up

* Thu Dec 24 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4489-1m)
- version up

* Thu Dec 24 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4481-1m)
- roll back 4481
- use export instead of env

* Thu Dec 24 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4489-1m)
- version up

* Wed Dec 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4481-1m)
- version up
- do not strip, strip makes gofmt SEGV

* Tue Dec  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4250-1m)
- version up

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4008-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4008-2m)
- BuildRequires: gcc emacs
- create emacs-go-mode package

* Thu Nov 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4008-1m)
- version up

* Wed Nov 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3975-1m)
- initial spec file for Momonga Linux from mgzip.spec
