%global momorel 1

%define apidocs 0

# apidocs disabled until we agree on a standard path.

Name:           grantlee
Version:        0.3.0
Release:        %{momorel}m%{?dist}
Summary:        Qt string template engine based on the Django template system
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.gitorious.org/grantlee/pages/Home
Source0:        http://downloads.grantlee.org/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= 4.8.4
BuildRequires:  cmake

%if 0%{?apidocs}   
BuildRequires:  doxygen
%endif

%description
Grantlee is a plug-in based String Template system written 
using the Qt framework. The goals of the project are to make it easier for
application developers to separate the structure of documents from the 
data they contain, opening the door for theming.

The syntax is intended to follow the syntax of the Django template system, 
and the design of Django is reused in Grantlee. 
Django is covered by a BSD style license.

Part of the design of both is that application developers can extend 
the syntax by implementing their own tags and filters. For details of 
how to do that, see the API documentation.

For template authors, different applications using Grantlee will present 
the same interface and core syntax for creating new themes. For details of 
how to write templates, see the documentation.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package apidocs
Group: Development/Documentation
Summary: Grantlee API documentation
BuildArch: noarch

%description apidocs
This package includes the Grantlee API documentation in HTML
format for easy browsing.

%prep
%setup -q -n %{name}-%{version}

sed -i 's,${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX},%{_libdir},' CMakeLists.txt

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} \
    -DCMAKE_BUILD_TYPE=release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%if 0%{?apidocs}
make docs -C %{_target_platform}
%endif

%install
rm -rf %{buildroot}
make install -C %{_target_platform} DESTDIR=%{buildroot}

%if 0%{?apidocs}
mkdir -p %{buildroot}%{_docdir}/HTML/en/grantlee-apidocs
cp -prf %{_target_platform}/apidocs/html/* %{buildroot}%{_docdir}/HTML/en/grantlee-apidocs
%endif

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGELOG COPYING.LIB README GOALS
%{_libdir}/lib%{name}*.so.*
%{_libdir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_includedir}/%{name}_core.h
%{_includedir}/%{name}_templates.h
%{_libdir}/lib%{name}*.so
%{_libdir}/cmake/%{name}

%if 0%{?apidocs}
%files apidocs
%{_docdir}/HTML/en/grantlee-apidocs
%endif

%changelog
* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-2m)
- specify PATH for Qt4

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.2-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.2-1m)
- import from Fedora devel

* Sun Jul 04 2010 Thomas Janssen <thomasj@fedoraproject.org> 0.1.2-1
- grantlee 0.1.2

* Tue May 18 2010 Thomas Janssen <thomasj@fedoraproject.org> 0.1.1-3
- disabled apidocs until we find a standard path

* Tue May 11 2010 Jaroslav Reznik <jreznik@redhat.com> 0.1.1-2
- added -apidocs subpackage

* Sun May 09 2010 Thomas Janssen <thomasj@fedoraproject.org> 0.1.1-1
- grantlee 0.1.1
- fixed Group

* Thu Apr 15 2010 Thomas Janssen <thomasj@fedoraproject.org> 0.1.0-1
- initial fedora release
