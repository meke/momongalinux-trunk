%global momorel 7
%global src1name scons-local
%global src1ver 1.3.0

Summary: Professional multitrack audio recording application
Name: ardour
Version: 2.8.12
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://ardour.org/
Source0: http://ardour.org/files/releases/%{name}-%{version}.tar.bz2
Source1: http://dl.sourceforge.net/project/scons/%{src1name}/%{src1ver}/%{src1name}-%{src1ver}.tar.gz
NoSource: 1
Source2: %{name}2.desktop
# patches from Fedora
Patch10: %{name}-SConscript.patch
Patch11: %{name}-session.cc-no_stomp.patch
Patch12: %{name}-2.5-HOST_NOT_FOUND.patch
Patch14: ardour-2.8.12-glib2332.patch
Patch15: ardour-2.8.12-boost150.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(post): shared-mime-info
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): shared-mime-info
Requires(posttrans): gtk2
BuildRequires: alsa-lib-devel
BuildRequires: atk-devel
BuildRequires: aubio-devel >= 0.3.2-9m
BuildRequires: boost-devel >= 1.55.0
BuildRequires: cairo-devel
BuildRequires: cairomm-devel
BuildRequires: curl-devel
BuildRequires: cwiid-devel
BuildRequires: cyrus-sasl-devel
BuildRequires: doxygen
BuildRequires: e2fsprogs-devel
BuildRequires: expat-devel
BuildRequires: fftw3-devel
BuildRequires: flac-devel
BuildRequires: freetype-devel
BuildRequires: gail-devel
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: glitz-devel
BuildRequires: gtk2-devel
BuildRequires: gtkmm-devel
BuildRequires: jack-devel
BuildRequires: krb5-devel
BuildRequires: keyutils-libs-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libcurl-devel
BuildRequires: libgnomecanvas-devel
BuildRequires: libgnomecanvasmm-devel
BuildRequires: libidn-devel
BuildRequires: liblo-devel
BuildRequires: liblrdf-devel
BuildRequires: libogg-devel
BuildRequires: libpng-devel
BuildRequires: libsamplerate-devel
BuildRequires: libselinux-devel
BuildRequires: libsigc++-devel
BuildRequires: libssh2-devel
BuildRequires: libtool
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: libxslt-devel
BuildRequires: openldap-devel
BuildRequires: openssl-devel
BuildRequires: pango-devel
BuildRequires: pixman-devel
BuildRequires: pkgconfig
BuildRequires: python
BuildRequires: raptor-devel
BuildRequires: scons >= 1.2.0
BuildRequires: zlib-devel

%description
Ardour is a multichannel hard disk recorder. It is capable of recording 24
or more channels of 32 bit audio at 48kHz. Ardour is intended to function
as a "professional" HDR system, replacing dedicated hardware solutions such
as the Mackie HDR, the Tascan 2424 and more traditional tape systems like
the Alesis ADAT series. It supports MIDI Machine Control, and so can be
controlled from any MMC controller, such as the Mackie Digital 8 Bus mixer
and many other modern digital mixers.

Ardour-KSI is a curses-based interface to Ardour.

You MUST have jackd running and an ALSA sound driver to use ardour.

%prep
%setup -q -a 1

# patches from Fedora
%patch10 -p1 -b .build
#%patch11 -p0 -b .no_stomp
%patch12 -p0 -b .HOST_NOT_FOUND
%patch14 -p1 -b .glib2332~
%patch15 -p1 -b .boost150~

%build
# <sigh> ardours SConstruct script is a mess when it comes to determining
# opt_flags, so we override the lot using the ARCH= and DIST_TARGET= options

TARGETCPU="none"

%ifarch %{ix86}
ARCH_FLAGS="-DARCH_X86 -DBUILD_SSE_OPTIMIZATIONS -msse -mfpmath=sse -DUSE_XMMINTRIN"
TARGETCPU="i386"
%endif
%ifarch x86_64
ARCH_FLAGS="-DARCH_X86 -DBUILD_SSE_OPTIMIZATIONS -DUSE_X86_64_ASM -msse -mfpmath=sse -DUSE_XMMINTRIN"
TARGETCPU="x86_64"
%endif
%ifarch ppc
TARGETCPU=powerpc
%endif
%ifarch ppc64
TARGETCPU=powerpc64
%endif

./scons.py %{?_smp_mflags} \
	PREFIX=%{_prefix} \
	LIBDIR=%{_lib} \
	WIIMOTE=0 \
	FREESOUND=1 \
	DIST_TARGET="$TARGETCPU" \
	ARCH="%{optflags} $ARCH_FLAGS"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}
./scons.py install DESTDIR=%{buildroot}

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/applications/

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48}/apps
install -m 644 gtk2_%{name}/icons/%{name}_icon_16px.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}2.png
install -m 644 gtk2_%{name}/icons/%{name}_icon_22px.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}2.png
install -m 644 gtk2_%{name}/icons/%{name}_icon_32px.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}2.png
install -m 644 gtk2_%{name}/icons/%{name}_icon_48px.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}2.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}2.png %{buildroot}%{_datadir}/pixmaps/%{name}2.png

# install mime file
mkdir -p %{buildroot}%{_datadir}/mime/packages
install -m 644 gtk2_%{name}/%{name}2.xml %{buildroot}%{_datadir}/mime/packages/

# install the man pages
install -dm 755 %{buildroot}%{_mandir}/man1
install -p -m 0644 %{name}.1 %{buildroot}%{_mandir}/man1/
for lang in es fr ru ; do
  install -dm 755 %{buildroot}%{_mandir}/${lang}/man1
  install -p -m 0644 %{name}.1.${lang} %{buildroot}%{_mandir}/${lang}/man1/
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
%{_bindir}/update-desktop-database &> /dev/null || :
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
    %{_bindir}/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
%{_bindir}/update-desktop-database &> /dev/null || :
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%posttrans
%{_bindir}/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root)
%doc COPYING README
%dir %{_sysconfdir}/%{name}2
%config(noreplace) %{_sysconfdir}/%{name}2/SAE-de-keypad.bindings
%config(noreplace) %{_sysconfdir}/%{name}2/SAE-de-nokeypad.bindings
%config(noreplace) %{_sysconfdir}/%{name}2/SAE-us-keypad.bindings
%config(noreplace) %{_sysconfdir}/%{name}2/SAE-us-nokeypad.bindings
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}-sae.menus
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}.menus
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}2_ui_dark.rc
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}2_ui_dark_sae.rc
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}2_ui_default.conf
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}2_ui_light.rc
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}2_ui_light_sae.rc
%config(noreplace) %{_sysconfdir}/%{name}2/%{name}_system.rc
%config(noreplace) %{_sysconfdir}/%{name}2/ergonomic-us.bindings
%config(noreplace) %{_sysconfdir}/%{name}2/mnemonic-us.bindings
%{_bindir}/%{name}2
%{_libdir}/%{name}2
%{_datadir}/applications/%{name}2.desktop
%{_datadir}/%{name}2
%{_datadir}/icons/hicolor/*/apps/%{name}2.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_mandir}/man1/%{name}.1*
%{_mandir}/*/man1/%{name}.1*
%{_datadir}/mime/packages/%{name}2.xml
%{_datadir}/pixmaps/%{name}2.png

%changelog
* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.12-7m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.12-6m)
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.12-5m)
- rebuild for boost

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.12-4m)
- update glib 2.33 patch

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.12-3m)
- fix build failure with boost 1.50.0

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.12-2m)
- fix build failure with glib 2.33

* Mon Jan 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.12-1m)
- update 2.8.12
-- disable Wiimote support

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-10m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (2.8.11-9m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-8m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-7m)
- rebuild against boost-1.46.1
- add workaround for gcc46.patch

* Wed Mar  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.11-6m)
- remove gcc46.patch for the moment
- can not build ardour with gcc46.patch by gcc45, wait for new gcc

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-5m)
- rebuild against boost-1.46.0
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-4m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.11-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.11-1m)
- version 2.8.11

* Tue Jun 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.10-1m)
- version 2.8.10
- use scons-local-1.3.0 for the moment (can not build with scons-2.0.0)

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.9-1m)
- version 2.8.9
- modify ARCH_FLAGS
- update desktop file
- install mime file
- import SConscript.patch from Fedora
- remove SConstruct.patch
- import and modify %%post, %%postun and %%posttrans from Fedora

* Fri Feb  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.7-1m)
- version 2.8.7

* Wed Jan 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.6-1m)
- version 2.8.6

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.4-1m)
- version 2.8.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.3-1m)
- version 2.8.3
- update SConstruct.patch

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.2-1m)
- version 2.8.2
- update SConstruct.patch

* Tue Jul 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.1-1m)
- version 2.8.1

* Sun Apr 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-2m)
- No NoSource

* Sun Mar 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-1m)
- version 2.8
- update SConstruct.patch
- remove gcc44.patch

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-5m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-4m)
- set WIIMOTE=1
- BuildRequires: cwiid-devel

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-3m)
- BuildRequires: scons >= 1.2.0
- drop Patch0 for fuzz=0, already merged upstream
- License: GPLv2+

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.7.1-2m)
- rebuild against python-2.6.1-1m and scons-1.0.1-2m

* Sun Dec  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-1m)
- version 2.7.1

* Wed Nov 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7-1m)
- version 2.7
- re-import SConstruct.patch from Fedora

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-1m)
- version 2.6.1
- re-import SConstruct.patch and import HOST_NOT_FOUND.patch from Fedora
- remove merged flac-1.2.1.patch

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5-1m)
- version 2.5
- use internal libraries again (hold BR for future version)
- use external scons again
- update gcc43.patch
- remove session.cc-_total_free_4k_blocks.patch

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-3m)
- rebuild against openssl-0.9.8h-1m

* Sat Apr 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.1-2m)
- use scons-local-0.97 for the moment

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.1-1m)
- replace gcc43.patch to Fedora's patch
- use external libraries

* Mon Apr  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4-3m)
- import, modify and mixup gcc43.patch from opensuse, debian and fedora
- suse's ardour
 +-* Mon Nov 19 2007 tiwai@suse.de
 +- fix build with gcc 4.3
- debian's audacity
 +- Bug#455635: Log for failed build of audacity_1.3.4-1 (dist=unstable4)
- fedora's rubberband
 +* Sun Mar 30 2008 Michel Salim <michel.sylvan@gmail.com> - 1.0.1-1
 +- Initial package
- my flavor
 +- gtk2_ardour/fft_result.cc
 +- libs/glibmm2/glibmm/propertyproxy_base.cc
 +- libs/gtkmm2/gtk/gtkmm/scale.cc
 +- libs/gtkmm2/gtk/gtkmm/targetentry.cc

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-2m)
- rebuild against gcc43

* Tue Apr  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4-1m)
- version 2.4
- import building style and 3 patches from Fedora

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- version 2.3.1

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-1m)
- version 2.2
- License: GPLv2

* Tue Oct  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-1m)
- version 2.1
- remove merged gtk212.patch

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.5-2m)
- fix build on new gtk

* Wed Aug 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.5-1m)
- version 2.0.5

* Wed Aug  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-1m)
- version 2.0.4
- enable optflags
- enable parallel build
- import and modify ardour-no-sse.patch from Fedora
 +* Sat Jan 06 2007 Anthony Green <green@redhat.com> 0.99.3-8
 +- Add ardour-no-sse.patch to disable use of build-system dependent 
 +  hardware features (SSE, MMX, etc).
- remove merged powermate_installdir.patch

* Thu Jul  5 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.3-2m)
- add powermate installdir patch for lib64

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-1m)
- version 2.0.3

* Tue May 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- initial package for Momonga Linux
- Summary and %%description are imported from cooker
