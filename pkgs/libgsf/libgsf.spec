%global momorel 1

Summary: GNOME Structured File library
Name: libgsf

Version: 1.14.30
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPLv2
URL: http://www.gnome.org/gnumeric/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.14/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bzip2-devel
BuildRequires: gobject-introspection-devel
BuildRequires: intltool
BuildRequires: glib2-devel
BuildRequires: libxml2-devel

%description
A library for reading and writing structured files (e.g. MS OLE and Zip)

%package devel
Summary: Support files necessary to compile applications with libgsf.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: glib2-devel
Requires: libxml2-devel

%description devel
Libraries, headers, and support files necessary to compile applications using libgsf.

%prep
%setup -q

%build
%configure --enable-gtk-doc \
    --enable-introspection=yes \
    --with-bz2 \
    --with-gnome \
    --disable-static

%make

%install
rm -rf --preserve-root %{buildroot}
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%make DESTDIR=%{buildroot} install

# Remove lib rpaths
chrpath --delete %{buildroot}%{_bindir}/gsf*

%find_lang libgsf

# Remove .la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{python_sitearch}/gsf/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(644,root,root,755)
%doc AUTHORS COPYING README
%{_libdir}/lib*.so.*
%{_libdir}/girepository-1.0/Gsf-1.typelib
%{_bindir}/gsf-office-thumbnailer
%{_mandir}/man1/gsf-office-thumbnailer.1.*
%dir %{_datadir}/thumbnailers
%{_datadir}/thumbnailers/gsf-office.thumbnailer

##%dir %{_libdir}/python*/site-packages/gsf
##%{_libdir}/python*/site-packages/gsf/*.so
##%%{_libdir}/python*/site-packages/gsf/*.la
##%{_prefix}/lib/python*/site-packages/gsf/__init*
%{_datadir}/locale/*/*/*

%files devel
%defattr(755,root,root,-)
%{_bindir}/gsf
%{_bindir}/gsf-vba-dump
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/libgsf-1
%{_includedir}/libgsf-1/*
%{_datadir}/gtk-doc/html/gsf
%{_datadir}/gir-1.0/Gsf-1.gir
%{_mandir}/man1/gsf.1.*
%{_mandir}/man1/gsf-vba-dump.1.*


%changelog
* Sat May 31 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14.30-1m)
- update to 1.4.30

* Tue Dec 11 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14.25-1m)
- update to 1.14.25

* Mon Oct  8 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14.24-1m)
- update to 1.14.24

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.21-2m)
- rebuild for glib 2.33.2

* Mon Aug  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.19-1m)
- update to 1.14.21

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14.19-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.19-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.19-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.19-1m)
- update to 1.14.19

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.18-3m)
- full rebuild for mo7 release

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.18-2m)
- add execute bit

* Tue Apr 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.18-1m)
- update to 1.14.18

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.17-1m)
- update to 1.14.17

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.16-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.16-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.16-1m)
- update to 1.14.16

* Sun Jun 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.15-1m)
- update to 1.14.15

* Mon May 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.14-1m)
- update to 1.14.14

* Thu May  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.13-1m)
- update to 1.14.13

* Sun Apr 26  2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.12-1m)
- update to 1.14.12
- enable gtk-doc

* Sun Mar  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.11-5m)
- fix %%file on i686

* Sun Mar  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.11-4m)
- build fix in x86_64

* Sun Mar  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.11-3m)
- disable-gtk-doc (cannot build)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.11-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.11-1m)
- update to 1.14.11

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14.10-2m)
- rebuild agaisst python-2.6.1-1

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.10-1m)
- update to 1.14.10

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.9-1m)
- update to 1.14.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-2m)
- rebuild against gcc43

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.8-1m)
- update 1.14.8

* Thu Oct  4 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPrereq pygtk2-devel for pygtk-codegen-2.0

* Sun Sep  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.7-1m)
- update to 1.14.7

* Wed Sep  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.6-1m)
- update to 1.14.6

* Tue Aug 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.5-1m)
- update to 1.14.5

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.3-1m)
- [SECUIRTY] CVE-2006-4514
- update to 1.14.3

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.1-2m)
- delete libtool library

* Wed May 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.1-1m)
- update to 1.14.1

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.14.0-1m)
- update to 1.14.0

* Sun Dec 4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.13.3-3m)
- delete link (nautilus was need version up!)

* Tue Nov 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.13.3-2m)
- add link libgsf-1.so.113 to libgsf-1.so.1
- For nautilus

* Tue Nov 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.13.3-1m)
- update to 1.13.3
- For gnumeric

* Sun Nov 20 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.12.3-1m)
- update to 1.12.3
- GNOME 2.12 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.11.1-1m)
- update to 1.11.1
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.11.0-1m)
- update to 1.11.0

* Tue Aug 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.9.0-2m)
- use autoconf instead of autoconf-2.58

* Tue May 11 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.9.0-1m)
- update to 1.9.0

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.8.2-5m)
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.8.2-4m)
- rebuild against for libxml2-2.6.8

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.8.2-3m)
- revised spec for enabling rpm 4.2.

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.2-2m)
- pretty spec file.

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.8.2-2m)
- change URL

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.8.2-1m)
- version 1.8.2

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.8.1-1m)
- version 1.8.1

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.8.0-1m)
- version 1.8.0

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.7.2-3m)
  rebuild against openssl 0.9.7a

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.2-2m)
- fix URL

* Wed Jan 29 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.7.2-1m)
- version 1.7.2

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.6.0-1m)
- version 1.6.0

* Mon Oct 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5.0-1m)
- version 1.5.0

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.4.0-1m)
- version 1.4.0

* Mon Aug 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.0-1m)
- version 1.3.0

* Tue Aug 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-1m)
- version 1.2.0

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.0-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.0-2m)
- fix file list

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.0-1m)
- create
