%global momorel 2

Summary: image display, conversion, and manipulation under X
Name: ImageMagick

# specopt
%{?include_specopt}
%{?!with_gif:                   %global with_gif                0}
%{?!with_dps:                   %global with_dps                0}

# GIF Support switch
%if %{with_gif}
%append _configure_args -- --enable-lzw
%else
%append _configure_args -- --disable-lzw
%endif

# DPS Support switch
%if %{with_dps}
%append _configure_args -- --with-dps
%else
%append _configure_args -- --without-dps
%endif

# version macros
%global source_version 6.8.8
%global source_release 10

# global information
Version: %{source_version}.%{source_release}
Release: %{momorel}m%{?dist}
License: see "LICENSE"
Group: Applications/Multimedia
URL: http://www.imagemagick.org/

# sources
# Source0: ftp://ftp.imagemagick.org/pub/ImageMagick/%{name}-%{source_version}-%{source_release}.tar.xz
Source0: ftp://ftp.imagemagick.org/pub/ImageMagick/legacy/%{name}-%{source_version}-%{source_release}.tar.xz
NoSource: 0
Source1: magick_small.png
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.16.0
BuildRequires: libtool >= 2.2 , libjpeg-devel >= 8a, libtiff-devel >= 4.0.1, bzip2-devel
BuildRequires: freetype-devel >= 2.3.1-2m, expat-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel
BuildRequires: libXext-devel, libXt-devel, 
BuildRequires: ghostscript >= 7.07-14m
BuildRequires: graphviz-devel >= 2.28.0
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: autoconf >= 2.61
BuildRequires: automake >= 1.5-20m
BuildRequires: libpng-devel >= 1.2.12-2m
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: libwmf-devel >= 0.2.8.3
BuildRequires: libxml2-devel >= 2.4.9
BuildRequires: graphviz-devel >= 1.18
BuildRequires: jasper-devel
BuildRequires: ilmbase >= 1.0.3
BuildRequires: OpenEXR >= 1.7.1
%if %{with_dps}
BuildRequires: dgs-devel
%endif
BuildRequires: djvulibre-devel >= 3.5.22

# package information
Requires: ghostscript

%description
ImageMagick is an image display, conversion, and manipulation tool.
It runs under X windows.  It is very powerful in terms of it's
ability to allow the user to edit images.  It can handle many
different formats as well.
#'--------------------------------------------------------------------------
%package devel
Summary: static libraries and header files for ImageMagick development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This is the ImageMagick development package.  It includes the static
libraries and header files for use in developing your own applications
that make use of the ImageMagick code and/or APIs.

%package djvu
Summary: DjVu plugin for ImageMagick
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description djvu
This package contains a plugin for ImageMagick which makes it possible to
save and load DjVu files from ImageMagick and libMagickCore using applications.

%package doc
Summary: ImageMagick html documentation
Group: Documentation

%description doc
ImageMagick documentation, this package contains usage (for the
commandline tools) and API (for the libraries) documentation in html format.
Note this documentation can also be found on the ImageMagick website:
http://www.imagemagick.org/

%package c++
Summary: object-oriented C++ API to the ImageMagick library
License: see "COPYING"
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: ImageMagick-Magick++
Provides:  ImageMagick-Magick++

%description c++
This is the Magick++ package, the object-oriented C++ API to the ImageMagick
image-processing library.

%package c++-devel
Summary: object-oriented C++ API to the ImageMagick library
License: see "COPYING"
Group: Development/Libraries
Requires: %{name}-Magick++ = %{version}-%{release}
Obsoletes: ImageMagick-Magick++-devel
Provides:  ImageMagick-Magick++-devel

%description c++-devel
This is the Magick++ package, the object-oriented C++ API to the ImageMagick
image-processing library.

%package perl
Summary: libraries and modules for access to ImageMagick from perl
License: Public Domain
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description perl
This is the ImageMagick perl support package. It includes perl modules and
support files for access to ImageMagick library from perl.

# scripts
%prep
%setup -q -n %{name}-%{source_version}-%{source_release}
iconv -f ISO-8859-1 -t UTF-8 README.txt > README.txt.tmp
touch -r README.txt README.txt.tmp
mv README.txt.tmp README.txt

%build
%configure \
	--enable-shared \
	--with-modules \
	--with-perl \
	--with-x \
	--with-threads \
	--with-magick_plus_plus \
	--with-gslib \
	--with-wmf \
	--with-lcms \
	--with-rsvg \
	--with-xml \
	--with-perl-options="INSTALLDIRS=vendor %{?perl_prefix} CC='%__cc -L$PWD/magick/.libs' LDDLFLAGS='-shared -L$PWD/magick/.libs'" \
	--without-windows-font-dir \
	--without-dps \
	--without-included-ltdl \
	--with-ltdl-include=%{_includedir} \
	--with-ltdl-lib=%{_libdir} \
	--with-gs-font-dir=%{_datadir}/fonts/default/Type1 \
	%{_configure_args}

# Disable rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make


%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make transform='s,x,x,' install DESTDIR=%{buildroot} INSTALL="install -p"

# fix weird perl Magick.so permissions
chmod 755 %{buildroot}%{perl_vendorarch}/auto/Image/Magick/Magick.so

# perlmagick: fix perl path of demo files
%{__perl} -MExtUtils::MakeMaker -e 'MY->fixin(@ARGV)' PerlMagick/demo/*.pl

find %{buildroot} -name "*.bs" |xargs rm -f
find %{buildroot} -name ".packlist" |xargs rm -f
find %{buildroot} -name "perllocal.pod" |xargs rm -f

# perlmagick: build files list
echo "%defattr(-,root,root)" > perl-pkg-files
find %{buildroot}%{_libdir}/perl* -type f -print \
        | sed "s@^%{buildroot}@@g" > perl-pkg-files 
find %{buildroot}%{perl_vendorarch} -type d -print \
        | sed "s@^%{buildroot}@%dir @g" \
        | grep -v '^%dir %{perl_vendorarch}$' \
        | grep -v '/auto$' >> perl-pkg-files 
if [ -z perl-pkg-files ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

rm -rf %{buildroot}%{_libdir}/ImageMagick
# Keep config
rm -rf %{buildroot}%{_datadir}/%{name}-%{VER}/[a-b,d-z,A-Z]*
rm -rf %{buildroot}%{_libdir}/libltdl.*
rm -f  %{buildroot}%{_libdir}/ImageMagick-*/modules*/*/*.a
rm -f  %{buildroot}%{_libdir}/*.{a,la}

# fix multilib issues
%ifarch x86_64 s390x ia64 ppc64 alpha sparc64
%define wordsize 64
%else
%define wordsize 32
%endif

mv %{buildroot}%{_includedir}/ImageMagick-6/magick/magick-config.h \
   %{buildroot}%{_includedir}/ImageMagick-6/magick/magick-config-%{wordsize}.h

cat >%{buildroot}%{_includedir}/ImageMagick-6/magick/magick-config.h <<EOF
#ifndef IMAGEMAGICK_MULTILIB
#define IMAGEMAGICK_MULTILIB

#include <bits/wordsize.h>

#if __WORDSIZE == 32
# include "magick-config-32.h"
#elif __WORDSIZE == 64
# include "magick-config-64.h"
#else
# error "unexpected value for __WORDSIZE macro"
#endif

#endif
EOF


%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post   c++ -p /sbin/ldconfig
%postun c++ -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc QuickStart.txt ChangeLog Platforms.txt
%doc README.txt LICENSE NOTICE AUTHORS.txt NEWS.txt
%attr(755,root,root) %{_libdir}/libMagickCore-6.Q16.so.*
%attr(755,root,root) %{_libdir}/libMagickWand-6.Q16.so.*
%{_sysconfdir}/%{name}-6
%{_bindir}/[a-z]*
%{_libdir}/%{name}-%{source_version}
%{_datadir}/%{name}-6
%{_mandir}/man[145]/[a-z]*
%{_mandir}/man1/ImageMagick.*
%exclude %{_libdir}/%{name}-%{source_version}/modules-Q16/coders/djvu.*

%files devel
%defattr(-,root,root)
%{_bindir}/MagickCore-config
%{_bindir}/Magick-config
%{_bindir}/MagickWand-config
%{_bindir}/Wand-config
%{_libdir}/libMagickCore-6.Q16.so
%{_libdir}/libMagickWand-6.Q16.so
%{_libdir}/pkgconfig/MagickCore.pc
%{_libdir}/pkgconfig/MagickCore-6.Q16.pc
%{_libdir}/pkgconfig/ImageMagick.pc
%{_libdir}/pkgconfig/ImageMagick-6.Q16.pc
%{_libdir}/pkgconfig/MagickWand.pc
%{_libdir}/pkgconfig/MagickWand-6.Q16.pc
%{_libdir}/pkgconfig/Wand.pc
%{_libdir}/pkgconfig/Wand-6.Q16.pc
%dir %{_includedir}/ImageMagick-6
%{_includedir}/ImageMagick-6/magick
%{_includedir}/ImageMagick-6/wand
%{_mandir}/man1/Magick-config.*
%{_mandir}/man1/MagickCore-config.*
%{_mandir}/man1/Wand-config.*
%{_mandir}/man1/MagickWand-config.*

%files djvu
%defattr(-,root,root,-)
%{_libdir}/%{name}-%{source_version}/modules-Q16/coders/djvu.*

%files doc
%defattr(-,root,root,-)
%doc %{_datadir}/doc/%{name}-6

%files c++
%defattr(-,root,root)
%{_libdir}/libMagick++-6.Q16.so.*

%files c++-devel
%defattr(-,root,root)
%{_bindir}/Magick++-config
%{_includedir}/ImageMagick-6/Magick++
%{_includedir}/ImageMagick-6/Magick++.h
%{_libdir}/libMagick++-6.Q16.so
%{_libdir}/pkgconfig/Magick++.pc
%{_libdir}/pkgconfig/Magick++-6.Q16.pc
%{_libdir}/pkgconfig/ImageMagick++.pc
%{_libdir}/pkgconfig/ImageMagick++-6.Q16.pc
%{_mandir}/man1/Magick++-config.*

%files perl -f perl-pkg-files
%defattr(-,root,root)
%{_mandir}/man3/*
%doc PerlMagick/demo/ PerlMagick/Changelog PerlMagick/README.txt

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.8.10-2m)
- rebuild against perl-5.20.0

* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.8.10-1m)
- update to 6.8.8-10

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.8.7.10-3m)
- rebuild against graphviz-2.36.0-1m

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.7.10-2m)
- rebuild against perl-5.18.2

* Tue Dec 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.7.10-1m)
- update to 6.8.7-10

* Thu Sep 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.6.10-1m)
- update to 6.8.6-10

* Tue Aug 27 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.8.6.9-1m)
- update to 6.8.6-9

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.6.8-2m)
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.6.8-1m)
- fix convert segfault
- update to 6.8.6-8

* Sun Jun  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.5.10-1m)
- update to 6.8.5-10

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.4.10-2m)
- rebuild against perl-5.18.0

* Tue Apr 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.4.10-1m)
- update to 6.8.4-10

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.3.10-3m)
- remove BuildRequires: and Requires: inkscape
- add svg patch to fix BTS#481

* Tue Mar 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.3.10-2m)
- add BuildRequires: and Requires: inkscape for glib2 < 2.36

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.3.10-1m)
- update to 6.8.3-10

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.2.10-2m)
- rebuild against perl-5.16.3

* Fri Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.2.10-1m)
- update to 6.8.2-10

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.8.1.10-1m)
- update to 6.8.1-10

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.0.10-2m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.8.0.10-1m)
- update to 6.8.0-10

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.9.10-2m)
- rebuild against perl-5.16.2

* Sat Oct 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.9.10-1m)
- update to 6.7.9-10

* Sun Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.8.10-1m)
- update to 6.7.8-10

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.7.10-1m)
- [SECURITY] CVE-2012-3437
- update to 6.7.7-10

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.6.10-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.6.10-2m)
- rebuild against perl-5.16.0

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.6.10-1m)
- [SECURITY] CVE-2012-1610
- update to 6.7.6-10

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.5.10-3m)
- rebuild against libtiff-4.0.1

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.5.10-2m)
- [SECURITY] CVE-2012-0259 CVE-2012-0260 CVE-2012-1610 CVE-2012-1798
- see http://www.imagemagick.org/discourse-server/viewtopic.php?f=4&t=20629

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.5.10-1m)
- update to 6.7.5-10

* Thu Feb 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.4.10-1m)
- [SECURITY] CVE-2012-0247 CVE-2012-0248
- update to 6.7.4-10

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.3.10-1m)
- update to 6.7.3-10
- do not update except x.x.x-10, source tar ball will be removed

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.7.3.1-1m)
- update to 6.7.3-1

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.2.10-1m)
- update to 6.7.2-10

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.1.10-2m)
- rebuild against perl-5.14.2

* Sat Aug 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.1.10-1m)
- update to 6.7.1-10

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.7.0.10-1m)
- update to 6.7.0-10

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.9.10-2m)
- rebuild against perl-5.14.1

* Sun May 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.9.10-1m)
- update to 6.6.9-10

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.8.10-4m)
- rebuild against graphviz-2.28.0

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.8.10-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6.8.10-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.8.10-1m)
- update to 6.6.8-10
- separate djvu and doc subpackage

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.7.10-1m)
- update to 6.6.7-10

* Sun Jan  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.6.10-1m)
- update to 6.6.6-10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6.5.10-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.5.10-1m)
- update to 6.6.5-10

* Sun Oct 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.4.10-1m)
- update to 6.6.4-10

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.3.10-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.3.10-1m)
- update to 6.6.3-10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.2.10-2m)
- full rebuild for mo7 release

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.2.10-1m)
- update to 6.6.2-10

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.1.10-1m)
- update to 6.6.1-10

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.0.10-5m)
- rebuild against perl-5.12.1

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (6.6.0.10-4m)
- add %%dir

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.0.10-3m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.6.0.10-2m)
- rebuild against libjpeg-8a

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.6.0.10-1m)
- update to 6.6.0-10

* Tue Mar  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.9.10-1m)
- update to 6.5.9-10

* Sun Jan 31 2010 YAMAZAKI Makoto <zaki@momonga-linux.org>
- (6.5.8.10-2m)
- fixed gs font path

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.8.10-1m)
- update to 6.5.8-10

* Tue Dec 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5.7.10-2m)
- revise for new %%configure macro

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.7.10-1m)
- update to 6.5.7-10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5.6.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.6.10-1m)
- update to 6.5.6-10

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.6.1-1m)
- update to 6.5.6-1

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.5.5.7-2m)
- rebuild against libjpeg-7

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.5.7-1m)
- update to 6.5.5-7

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.5.4-1m)
- update to 6.5.5-4

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.4.10-3m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.4.10-2m)
- tar.lzma has been removed from ftp server

* Thu Aug 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.4.10-1m)
- update to 6.5.4-10

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.4.6-1m)
- update to 6.5.4-6

* Wed Jul  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.4.1-1m)
- update to 6.5.4-1

* Wed Jun 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.3.10-1m)
- update to 6.5.3-10

* Fri Jun 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.3.9-1m)
- update to 6.5.3-9

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.3.4-1m)
- update to 6.5.3-4

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.3.1-1m)
- [SECURITY] CVE-2009-1882
- update to 6.5.3-1

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5.2.8-2m)
- rebuild against djvulibre-devel >= 3.5.22

* Mon May 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.2.8-1m)
- update to 6.5.2-8

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.2.4-1m)
- update to 6.5.2-4

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.5.2.1-3m)
- rebuild against graphviz-2.22.2

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.5.2.1-2m)
- rebuild against libtool-2.2.6

* Thu May  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.2.1-1m)
- update to 6.5.2-1

* Sun May  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.1.10-1m)
- update to 6.5.1-10

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.1.6-1m)
- update to 6.5.1-6

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.1.2-1m)
- update to 6.5.1-2

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.0.9-1m)
- update to 6.5.0-9

* Sun Mar 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.5.0.4-1m)
- update to 6.5.0-4

* Fri Mar 13 2009 NARITA Koichi <pulsar@momong-linuxx.org>
- (6.5.0.0-1m)
- update to 6.5.0-0

* Sat Feb 28 2009 NARITA Koichi <pulsar@momong-linuxx.org>
- (6.4.9.7-1m)
- update to 6.4.9-7

* Sat Feb 21 2009 NARITA Koichi <pulsar@momong-linuxx.org>
- (6.4.9.6-1m)
- update to 6.4.9-6

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.9.2-1m)
- update to 6.4.9-2

* Thu Feb  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.9.1-1m)
- update to 6.4.9-1

* Sat Jan 24 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.4.8.9-1m)
- 6.4.8-6 missing.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.4.8.6-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.8.6-1m)
- update to 6.4.8-6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.8.5-1m)
- update to 6.4.8-5

* Wed Dec 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.8.3-1m)
- update to 6.4.8-3

* Sun Dec 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.8.2-1m)
- update to 6.4.8-2

* Sat Dec 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.4.7.10-1m)
- update to 6.4.7-10

* Fri Dec 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.7.6-1m)
- update to 6.4.7-6

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.6.6-1m)
- update to 6.4.6-6

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.6.0-1m)
- update to 6.4.6-0

* Thu Nov 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.5.8-1m)
- update to 6.4.5-8

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.5.5-1m)
- update to 6.4.5-5

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.4.5.4-1m)
- update to 6.4.5-4

* Sat Oct 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.5.0-1m)
- update to 6.4.5-0

* Wed Oct 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.4.6-1m)
- update to 6.4.4-6

* Sun Oct  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.4.1-1m)
- update to 6.4.4-1

* Sun Sep 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.4.0-1m)
- update to 6.4.4-0
- use NoSoure

* Sat Sep 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.3.6-1m)
- update to 6.4.3-6

* Sun Aug 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.2.9-1m)
- update to 6.4.2-9

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.2.4-1m)
- update to 6.4.2-4

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.4.2.1-1m)
- update to 6.4.2-1
- sync with Fedora devel

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.8-6m)
- rebuild against graphviz-2.18

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.8-5m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.3.8-4m)
- rebuild against OpenEXR-1.6.1

* Sun Mar  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.8-3m)
- version down to 6.3.8-4

* Sat Mar  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.9-1m)
- update to 6.3.9-1

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.3.8-2m)
- rebuild against perl-5.10.0-1m

* Sat Jan 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.8-1m)
- update to 6.3.8-1

* Sun Jan 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-6m)
- update to 6.3.7-10

* Sun Jan  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-5m)
- update to 6.3.7-9

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-4m)
- update to 6.3.7-8

* Sat Dec  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-3m)
- update to 6.3.7-3

* Wed Dec  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-2m)
- update to 6.3.7-2

* Sun Nov 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-1m)
- update to 6.3.7-0

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.6-5m)
- update to 6.3.6-10

* Sat Nov  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.6-4m)
- update to 6.3.6-6

* Sun Oct 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.6-3m)
- update to 6.3.6-3

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.6-2m)
- update to 6.3.6-1
- enable OpenMP again

* Wed Oct  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.6-1m)
- update to 6.3.6-0

* Thu Sep 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-7m)
- [SECURITY] CVE-2007-4985, CVE-2007-4986, CVE-2007-4987, CVE-2007-4988
- update to 6.3.5-10

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-6m)
- update to 6.3.5-8

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.5-5m)
- fix %%changelog section

* Sat Aug 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-4m)
- update to 6.3.5-6

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-3m)
- update to 6.3.5-3

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-2m)
- do not use OpenMP for the moment...

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-1m)
- update to 6.3.5-0

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.4-2m)
- update to 6.3.4-10

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.4-1m)
- update to 6.3.4-6

* Sat Apr  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.3-5m)
- update to 6.3.3-6

* Sun Apr  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.3-4m)
- update to 6.3.3-5

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.3.3-3m)
- BuildRequires: freetype2-devel -> freetype-devel

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.3-2m)
- update to 6.3.3-3
- modify patch0

* Sun Mar 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.3-1m)
- update to 6.3.3-1

* Sun Feb 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-9m)
- update to 6.3.2-9

* Sat Feb 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-8m)
- update to 6.3.2-5

* Thu Feb 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.3.2-7m)
- revival modules libtool library

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.3.2-6m)
- fix duplicate files

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.3.2-5m)
- delete libtool library

* Mon Feb 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-4m)
- update to 6.3.2-4

* Thu Feb  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-3m)
- update to 6.3.2-3

* Fri Jan 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-2m)
- update to 6.3.2-1

* Sat Jan 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-1m)
- update to 6.3.2-0

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-8m)
- update to 6.3.1-7

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-7m)
- rebuild against jasper-1.900.0-1m

* Sat Jan  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-6m)
- update to 6.3.1-6

* Sun Dec 31 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-5m)
- update to 6.3.1-5

* Fri Dec 22 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-4m)
- update to 6.3.1-3

* Sat Dec 16 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-3m)
- update to 6.3.1-2

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-2m)
- update to 6.3.1-1

* Sun Dec  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-1m)
- update to 6.3.1-0

* Wed Nov 29 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-9m)
- update to 6.3.0-7

* Sat Nov 25 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-8m)
- update to 6.3.0-6
- BuildRequires: autoconf >= 2.61

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-7m)
- update to 6.3.0-5

* Mon Nov 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-6m)
- update to 6.3.0-4

* Thu Nov  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-5m)
- update to 6.3.0-3
- delete patch for CVE-2006-5456, it has already applied this version

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-4m)
- [SECURITY] CVE-2006-5456
- import patch from GraphicsMagick to fix PALM and DCM buffer overflows

* Thu Nov  2 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-3m)
- update to 6.3.0-2

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-2m)
- update to 6.3.0-1

* Tue Oct 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.0-1m)
- update to 6.3.0-0

* Mon Oct  2 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2.9-7m)
- update to 6.2.9-8

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.9-6m)
- update to 6.2.9-7

* Sun Sep 17 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.9-5m)
- update to 6.2.9-6

* Fri Sep 15 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.9-4m)
- update to 6.2.9-5
- use transform='s,x,x,' at make install

* Mon Sep  4 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.9-3m)
- update to 6.2.9-3

* Fri Sep  1 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.9-2m)
- update to 6.2.9-2

* Sat Aug 19 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.9-1m)
- update to 6.2.9-0

* Wed Aug 16 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (6.2.8-6m)
- rebuild against libpng-1.2.12-2m for non x86

* Sun Aug  6 2006 NARITA Koichi <pulsar@sea.plala.or.jp
- (6.2.8-5m)
- update to 6.2.8-8

* Fri Jul 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.8-4m)
- update to 6.2.8-7

* Sun Jul 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.8-3m)
- update to 6.2.8-5

* Wed Jul  5 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.8-2m)
- update to 6.2.8-3

* Wed Jun  7 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.8-1m)
- update to 6.2.8-0

* Thu May 04 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.2.7-1m)
- update to 6.2.7-2
- revise lib64 patch

* Mon Feb 13 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.2.6-1m)
- update to 6.2.6-1

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.2.5-2m)
- rebuild against perl-5.8.8

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (6.2.5-1m)
- update to 6.2.5
- modify lib64 patch and delete other patches

* Sun Jun 12 2005 Nishio Futoshi <futshi@momonga-linux.org>
- (6.2.3-2m)
- add dependency jasper-devel
- add one line BuildRequire:

* Thu May 26 2005 mutecat <mutecat@momonga-linux.org>
- (6.2.3-1m)
- up to 6.2.3
- remove ImageMagick-6.1.4-gsinc.patch

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.1.8-3m)
- enable x86_64.
- remove ExclusiveArch.

* Sun Jan 23 2005 Kazuhiko <kazuhiko@fdiary.net>
- (6.1.8-2m)
- install magick/xwindow.h to build ruby-rmagick

* Wed Jan 19 2005 TAKAHASHI Tamotsu <tamo>
- (6.1.8-1m)
- update to 6.1.8-9 which fixes CAN-2005-0005

* Sat Nov 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (6.1.5-2m)
- add some config files

* Tue Nov 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (6.1.5-1m)
- version 6.1.5-3
- spec file cleanup

* Wed Nov 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (6.1.4-2m)
- rebuild against libwmf-0.2.8.3
- build against graphviz-devel-1.12

* Wed Nov 17 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (6.1.4-1m)
- security update.
- [SA12995: ImageMagick EXIF Parser Buffer Overflow Vulnerability]
  ref. http://secunia.com/advisories/12995/

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.5.7-18m)
- rebuild against gcc-c++-3.4.1

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.5.7-17m)
- build against perl-5.8.5

* Tue Aug 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.5.7-16m)
- revise %%install section for i686

* Tue Aug 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.5.7-15m)
- use autoconf instead of autoconf-2.58

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.5.7-14m)
- remove Epoch from BuildPrereq

* Sat Jun 19 2004 mutecat <mutecat@momonga-linux.org>
- (5.5.7-13m)
- add %ifarch ppc and %ifnarch ppc

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.5.7-12m)
- rebuild against for libxml2-2.6.8

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (5.5.7-11m)
- revised spec for enabling rpm 4.2.

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.5.7-10m)
- specfile cleanup

* Thu Dec 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.5.7-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Dec 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.5.7-8m)
- use autoconf-2.58 instead of autoconf-2.57

* Tue Dec  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.5.7-7m)
- configure with '--with-gs-font-dir=/usr/share/fonts/type1/urw'

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.5.7-6m)
- rebuild against perl-5.8.2

* Sat Nov  8 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (5.5.7-5m)
- rebuild against perl-5.8.1
- revise a specfile for perl-5.8.1

* Sun Oct 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.5.7-4m)
- disable ghostscript

* Sat Oct 25 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.5.7-3m)
- move modules-Q16/{coders|filters}/*.la from devel package to main package.

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.5.7-2m)
- revise for ghostscript header location

* Sun Oct 19 2003 Kenta MURATA <muraken2@nifty.com>
- (5.5.7-1m)
- version up.
- adapt the License: preamble for the Momonga Linux licence expression
  unification policy (draft).

* Thu Apr 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.4.9-2m)
- rebuild against zlib 1.1.4-5m
- move name tag to top of spec file for fix about specopt

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.4.9-1m)
- update to 5.4.9-1

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.4.7-4m)
- rebuild against perl-5.8.0

* Fri Oct 18 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.4.7-3m)
- Do not be NoSource. And do not remove warning!

* Sun Oct 13 2002 Kenta MURATA <muraken2@nifty.com>
- (5.4.7-2m)
- rebuild against XFree86 and dgs.
- if build with dps, you set with_dps to 1.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (5.4.7-1k)
- update to 5.4.7-4.
- no more patch0.

* Mon Apr 22 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (5.4.4-4k)
- revise SOURCE URL

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (5.4.4-2k)
- 5.4.4-5
- rebuild against libpng-1.2.2

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (5.4.0.3-8k)
- remove Option: tag

* Wed Nov  7 2001 Shingo Akagaki <dora@kondara.org>
- up to 5.4.0.3

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (5.4.0-4k)
- add Requires,BuildRequires
- up to 5.4.0
- added patch for libwmf-0.2.1

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (5.3.9-4k)
- rebuild against libpng 1.2.0.

* Sun Oct 14 2001 Masaru Sato <masachan@kondara.org>
- Modify BuildRoot

* Sun Sep 30 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.3.9-2k)
- update to 5.3.9 for fixing many bugs (jitterbug #951)

* Mon Mar 26 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.0-7k)
- delete BuildPreReq: autoconf

* Sat Mar 24 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.0-5k12KOM)
- do not be NoSource, since source file may be modified day by day
  without name changing. :-<
- delete gs patch since it has already been in source.

* Sat Mar 24 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.0-5k10KOM)
- add BuildRequires, Requires: ghostscript
- change BuildPreReq to BuildRequires
- add %%post and %%postun to Magick++
- move Copyright.txt and QuickStart.txt to %%pkgdocdir

* Sat Mar 24 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.0-5k8KOM)
- re-enable ttf
- add gs delegate patch (ImageMagick-5.3.0-gs.patch)

* Sat Mar 24 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.0-5k6KOM)
- disable ttf

* Sat Mar 24 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.0-5k4KOM)
- fix to install delegates.mgk
- modify to install docs to %%{_docdir}
- delete www/perl.html from PerlMagick
- move Magick-config to devel, and Magick++-config to Magick++
- add BuildPreReq: lib{tiff,jpeg,png}-devel, zlib-devel, bzip2-devel,
  XFree86-devel

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.3.0-5k)
- modified spec file about doc files (#866)

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.3.0-3k)
- revive Patch3
- change to use 'install' instead of 'install-strip', because rpm automatically
- strip binaries by macro

* Sat Feb 10 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.2.9-3k)
- version 5.2.9
- add BuildPreReq: libtool
- update URL: and ftp site
- use 'install-strip' instead of 'install'

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- (5.2.4-7k)
- change GIF Support switch tag.
- (Vendor to Option).

* Fri Nov 03 2000 Kenichi Matsubara <m@kondara.org>
- (5.2.4-2k)
- add GIF Support switch.

* Mon Sep 18 2000 Kazuhiko <kazuhiko@kondara.org>
- (5.2.4-1k)

* Sun Sep 17 2000 Kazuhiko <kazuhiko@kondara.org>
- (5.2.3-3k)
- modify permissions

* Wed Sep 13 2000 Kazuhiko <kazuhiko@kondara.org>
- (5.2.3-2k)
- make ImageMagick-{perl,Magick++} packages
- remove 'demo' from SUBDIRS in Magick++
- revise set-perl-filelist so as to support gziped man pages.

* Mon Sep  4 2000 Kazuhiko <kazuhiko@kondara.org>
- version 5.2.3

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed Jun 28 2000 Toru Hoshina <t@kondara.org>
- Be a NoSRC :-P
- fixed gcc 2.96 issue.

* Fri Jun 09 2000 Kenichi Matsubara <m@kondara.org>
- fix Magick-config ( add -I/usr/X11R6/include/X11 )

* Thu Jun 08 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- fix for Magick-config (-cppflags option)
- Magick-config was moved to devel package

* Mon Jun  5 2000 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 5.2.0
- adapted filelist due to dynamic modules

* Sun Jun 04 2000 Takaaki Tabuchi <tab@kondara.org>
- merge from rawhide-2000-0526 (5.1.1-3).
- delete patch for configure{,.in}.
- delete {Build,}Requires: bzip2 >= 1.0.0

* Fri May 19 2000 Takaaki Tabuchi <tab@kondara.org>
- build ageinst bzip2-1.0.0.
- add patch for configure{,.in}.
- add patch2 from rawhide.
- add {Build,}Requires: bzip2 >= 1.0.0

* Tue Feb 29 2000 Alexander Zimmermann <A_Zimmermann@gmx.de>
- supplemented Magick++ file list

* Fri Feb 18 2000 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 5.1.1

* Sat Jan  1 2000 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 5.1.0

* Wed Sep  8 1999 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 4.2.9

* Tue Aug 10 1999 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 4.2.8

* Tue Jul  6 1999 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 4.2.7
- added Magick++ package

* Wed Jun  2 1999 Alexander Zimmermann <A_Zimmermann@gmx.de>
- updated to 4.2.6
- added perl script set-perl-filelist

* Sat May  1 1999 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.2.3
- added package perl

* Mon Mar  1 1999 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.2.0
- removed ImageMagick.patch

* Sat Feb 13 1999 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.1.8

* Wed Dec  3 1998 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.1.7
- added delegates.mgk
- removed lsm-entry-file

* Sun Nov  8 1998 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.1.3

* Thu Oct 15 1998 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.1.2

* Wed Sep 16 1998 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.1.0

* Tue Aug  4 1998 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.0.8

* Thu Jul  9 1998 Alexander Zimmermann <Alexander.Zimmermann@fmi.uni-passau.de>
- updated to 4.0.7
- added ImageMagick.patch

* Wed May  6 1998 Tomasz Kloczko <kloczek@rudy.mif.pg.gda.pl>
- removed compiling mpeg_lib with IM (now this is separated package),
- added -q %setup parameter and removed all other parameters,
- "rm -rf %{buildroot}" moved from %prep to %%install,
- added %clean section,
- added URL,
- removed declaration %%{version} macro (it is predefined),
- moved html documentation to devel,
- added using %%{buildarch} macro in %files in subpackage perl (now
  ImageMagick is more portable to diffrent OSes and archs,
- added fiew missing %dir in subpackage perl,
- added using %%{name} macro in Buildroot and Source fields also in Requires
  in subpackages,
- fiew modifications and simplification in %files,
- added %defattr and %attr macros in %files (allows building package from
  non-root account).

* Fri Apr  3 1998 Khimenko Victor <khim@sch57.msk.ru>
- updated to 4.0.4
- added PerlMagick

* Thu Mar 12 1998 Jani Hakala <jahakala@cc.jyu.fi>
- updated to 4.0.3

* Fri Feb 27 1998 Jani Hakala <jahakala@cc.jyu.fi>
- updated to 4.0.2

* Sat Feb 21 1998 Jani Hakala <jahakala@cc.jyu.fi>
- updated to 4.0.1
- buildrooted

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- updated from 3.8.3 to 3.9.1
- removed PNG patch (appears to be fixed)

* Wed Oct 15 1997 Erik Troan <ewt@redhat.com>
- build against new libpng

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Thu Mar 20 1997 Michael Fulbright <msf@redhat.com>
- updated to version 3.8.3.
- updated source and url tags.
