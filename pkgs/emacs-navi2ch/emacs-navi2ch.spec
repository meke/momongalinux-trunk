%global momorel 9

%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global cvsdate 20100722

Summary:     2ch client on Emacs
Name: emacs-navi2ch
Epoch:       1
Version:     2.0.0
Release:     0.0.%{cvsdate}.%{momorel}m%{?dist}
#Release:     0.1.%{momorel}m%{?dist}
#Release:     %{momorel}m%{?dist}
License:     GPLv2+
Group:       Applications/Editors
Source0:     http://navi2ch.sourceforge.net/snapshot/navi2ch-cvs_0.0.%{cvsdate}-1.tar.gz
#Source0:     http://navi2ch.sourceforge.net/beta/navi2ch-%{version}-BETA.tar.gz
#Source0:     http://dl.sourceforge.net/sourceforge/navi2ch/navi2ch-%{version}.tar.gz
#NoSource:    0
Source1:     my-navi2ch.tgz

Patch0:      navi2ch-set-icondir.patch
URL:         http://navi2ch.sourceforge.net/
BuildArch:   noarch
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: navi2ch-emacs
Obsoletes: navi2ch-xemacs

Obsoletes: elisp-navi2ch
Provides: elisp-navi2ch

%description
2ch client on Emacs.

%prep
%setup -q -n navi2ch-cvs-%{cvsdate}
#%%setup -q -n navi2ch-%{version}-BETA
#%%setup -q -n navi2ch-%{version}
%patch0 -p1 -b .icondir

tar xzf %{SOURCE1}
patch -p0 -b --suffix .my-navi2ch~ < my-navi2ch/my-navi2ch.patch

%build
EMACS=emacs ./configure --with-lispdir=%{e_sitedir}/navi2ch --with-icondir=%{_datadir}/emacs/site-lisp/etc/navi2ch
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{e_sitedir}/navi2ch
%{makeinstall} lispdir=%{buildroot}%{e_sitedir}/navi2ch icondir=%{buildroot}%{_datadir}/emacs/site-lisp/etc/navi2ch
%{__install} -m 644 my-navi2ch/*.el* %{buildroot}%{e_sitedir}/navi2ch

# remove
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{sitepdir}/info/dir

%post
/sbin/install-info %{_infodir}/navi2ch.info %{_infodir}/dir \
        --section="Emacs"

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/navi2ch.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%doc doc
%{e_sitedir}/navi2ch
%{e_sitedir}/etc/navi2ch
%{_datadir}/info/navi2ch.info*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.0.0-0.0.20100722.9m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.0.0-0.0.20100722.8m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1:2.0.0-0.0.20100722.7m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.0.0-0.0.20100722.6m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:2.0.0-0.0.20100722.5m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.0.0-0.0.20100722.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:2.0.0-0.0.20100722.3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20100722.2m)
- add epoch to %%changelog

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20100722.1m)
- update to cvs snapshot (2010-07-22)

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20091107.3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20091107.2m)
- merge navi2ch-emacs to elisp-navi2ch
- kill navi2ch-xemacs

* Thu Nov 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20091107.1m)
- update to cvs snapshot (2009-11-07)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20091010.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20091010.2m)
- update my-navi2ch
-- http://pc12.2ch.net/test/read.cgi/unix/1221368890/858

* Wed Oct 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20091010.1m)
- update to cvs snapshot (2009-10-10)

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:2.0.0-0.0.20090723.3m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:2.0.0-0.0.20090723.2m)
- rebuild against emacs 23.0.96

* Sat Jul 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:2.0.0-0.0.20090723.1m)
- update to cvs_0.0.20090723-1

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090628.1m)
- update to cvs_0.0.20090628-1

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090617.2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090617.1m)
- update to cvs_0.0.20090617-1

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090516.2m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090516.1m)
- update to cvs_0.0.20090516-1

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090412.4m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090412.3m)
- rebuild against emacs-23.0.93

* Mon Apr 27 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:2.0.0-0.0.20090302.1m)
- update to cvs_0.0.20090412-1

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090302.2m)
- rebuild against emacs-23.0.92

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.0.0-0.0.20090302.1m)
- update to cvs_0.0.20090302-1

* Sun Mar  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.8.2-2m)
- update my-navi2ch
-- http://pc11.2ch.net/test/read.cgi/unix/1221368890/483
- License: GPLv2+

* Mon Mar  2 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.8.2-1m)
- update to 1.8.2

* Thu Feb 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.8.2-0.1.3m)
- replaced Source0
  old Source sha256sum: 89001e884472e93cf935e1c333c3ad5998ab9f48763137a6fdf67dca43a523d6
  new Source sha256sum: 41b8ae77b22798d6ce2c49791c2ccf7a0baa2ce19879b1618d3ea9032d544fb3
- tmp No NoSource for build correctly

* Wed Feb 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.8.2-0.1.2m)
- revive Epoch for yum

* Wed Feb 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.2-0.1.1m)
- update to 1.8.2-BETA
- delete Epoch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-2m)
- rebuild against rpm-4.6

* Thu Sep 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Tue Sep 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.1-0.1.1m)
- update to 1.8.1-BETA
- delete included Patch1: navi2ch-useragent.patch

* Fri Sep 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-3m)
- apply navi2ch-useragent.patch (avoid fusianasan)
- http://sourceforge.net/tracker/index.php?func=detail&aid=2105538&group_id=39552&atid=435774

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-2m)
- rebuild against emacs-22.3

* Thu Jul 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Sat Jul 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-0.3.1m)
- update to 1.8.0 BETA 20080726

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-0.2.1m)
- rebuild against xemacs-21.5.28
- update to 1.8.0 BETA 20080723

* Mon Jul 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-0.1.1m)
- update to 1.8.0 BETA

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.5.1-0.20070423.3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070423.2m)
- rebuild against gcc43

* Mon Jul  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.5.1-0.20070423.1m)
- update to cvs snapshot

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.5.1-0.20070206.7m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070206.6m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070206.5m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070206.4m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070206.3m)
- rebuild against emacs-22.0.94

* Sun Feb 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.5.1-0.20070206.2m)
- add Epoch: 1

* Tue Feb 06 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070206.1m)
- cvs update (filter bug fix version)

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070126.3m)
- add my-navi2ch

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070126.2m)
- rebuild against emacs-22.0.93

* Fri Jan 26 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20070126.1m)
- update and changed to unstable branches source
  cvs -d:pserver:anonymous@navi2ch.cvs.sourceforge.net:/cvsroot/navi2ch co -runstable navi2ch

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20061114.3m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20061114.2m)
- rebuild against emacs-22.0.91

* Wed Nov 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20061114.1m)
- update to cvs snapshot

* Mon Nov 06 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20061106.1m)
- update to cvs snapshot

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.20061024.3m)
- rebuild against emacs-22.0.90

* Sat Oct 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.5.1-0.20061024.2m)
- change version notation

* Wed Oct 25 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.5.1-0.0020061024001m)
- update to cvs snapshot

* Fri Aug 18 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.5.1-0.0020060706002m)
- add Patch0: navi2ch-set-icondir.patch

* Sat Jul 22 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.5.1-0.0020060706001m)
- update cvs version 20060706
-- hana mogera cookie support was added to development version snapshot
-- delete Patch0: navi2ch-hanamogera2.patch

* Mon May 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020060529001m)
- update cvs version 20060529
- update Patch0: to navi2ch-hanamogera2.patch

* Sun May 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020051124002m)
- add navi2ch-hanamogera.patch

* Sun May 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020051124001m)
- update cvs version 20051124

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020050920001m)
- update cvs version 20050920

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020050423001m)
- update cvs version 20050423

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020050316001m)
- update cvs version 20050316

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020050211004m)
- add rm -rf %%{buildroot} after %%setup

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7.5.1-0.0020050211003m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7.5.1-0.0020050211002m)
- use %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020050211001m)
- rebuild against emacs 22.0.50
- update cvs version 20050211

* Tue Feb  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.7.5.1-0.0020050207001m)
- update cvs version 20050207
- change etcdir, don't use version specific directory

* Thu Dec 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020041213001m)
- update cvs version 20041213

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7.5.1-0.0020041027002m)
- rebuild against emacs-21.3.50

* Tue Nov 03 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-0.0020041027001m)
- update cvs version 20041027

* Wed Oct 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5-1m)
- update to 1.7.5

* Sun Oct 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5-0.1.1m)
- update to 1.7.5-BETA

* Sun Oct 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.4.1-0.0020041009001m)
- update cvs version 20041009

* Thu Aug 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.4-1m)
- update to 1.7.4

* Thu Jun 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.3-2m)
- roll back to 1.7.3

* Sat Jun 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.3.1-0.0020040531001m)
- update cvs version 20040531

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.7.3-1m)
- verup

* Mon Mar 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.2.1-0.0020040321001m)
- update cvs version 20040321

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.7.1.1-0.0020031209002m)
- revised spec for enabling rpm 4.2.

* Sun Dec 21 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.1.1-0.0020031209001m)
- update cvs version 20031209

* Tue Dec 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.1.1-0.0020031206001m)
- update cvs version 20031206
- use http://navi2ch.sourceforge.net/tmp/cvs/navi2ch-article.el.1.231

* Thu Nov 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.1.1-0.0020031110001m)
- update cvs version 20031110
- use %%global

* Sat Oct 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.1.1-0.0020031020001m)
- update cvs version 20031020

* Sat Oct 11 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.7.1-2m)
- add info file for emacs
- add PreReq: info for navi2ch-emacs
- execute install-info in %%post and %%preun section of navi2ch-emacs

* Wed Oct 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1
- change Source0 URI

* Tue Jul 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220030702001m)
- update cvs version 20030702

* Thu May 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220030513001m)
- update cvs version 20030513

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220030318001m)
- rebuild against emacs-21.3
- update cvs version 20030318

* Tue Mar 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220030310001m)
- update cvs version 20030310

* Wed Jan 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220030122001m)
- update cvs version 20030122

* Sat Jan 4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220021231001m)
- update cvs version 20021231

* Sat Dec 28 2002 HOSONO Hidetomo <h12o@h12o.org>
- (1.6.0.1-0.00220021226001m)
- update cvs version 20021226
- fix tab's changelog entry, Mon Dec 2 2002

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.1-0.00220021129002m)
- add BuildPreReq: xemacs-sumo
- update cvs version 20021129

* Wed Sep 18 2002 smbd <smbd@momonga-linux.org>
- (1.6.0.1-0.0020020906001m)
- update official snapshot

* Tue Aug 15 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.6.0.1-0.0020020815001m)
- update official snapshot
- use xemacs package for xemacs

* Mon Jul 29 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.6.0.1-0.0020020727001m)
- update official snapshot
- update version to 1.6.0.1

* Wed Jul 23 2002 smbd <smbd@momonga-linux.org>
- (1.5.2.1-0.0020020723001m)
- update official snapshot

* Mon Jul 12 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.5.2.1-0.0020020712002k)
- update official snapshot
- update version to 1.5.2.1

* Wed Jul  3 2002 smbd <smbd@momonga-linux.org>
- (1.5.1.1-0.0020020702002k)
- update official snapshot

* Tue May 21 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.5.1.1-0.0020020520002k)
- update official snapshot
- update xemacs for 21.4.8

* Tue Apr 23 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.5.1.1-0.0020020419002k)
- update official snapshot

* Mon Apr 08 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.5.1.1-0.0020020408002k)
- using official snapshot
- including icons

* Mon Apr 08 2002 Toshiuro HIKITA <toshi@sodan.org>
- (1.5.1-4k)
- fix %files

* Mon Mar 11 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.5.1-2k)
- welcome to Kondara
