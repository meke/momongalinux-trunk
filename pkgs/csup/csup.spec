%global momorel 12
%global snapdate 20060318

Summary: csup is a rewrite of the CVSup software in C
Name: csup
Version: 0.0.%{snapdate}
Release: %{momorel}m%{?dist}
Source0: ftp://ftp.jp.freebsd.org/pub/FreeBSD/distfiles/%{name}-snap-%{snapdate}.tgz
NoSource: 0
Source1: postgres.cvsup
Source2: ruby.cvsup
License: Modified BSD
Group: Applications/Internet
URL: http://mu.org/~mux/csup.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0

%description
csup is a rewrite of the CVSup software in C

%prep
%setup -q -n %{name}

%build
make -f GNUmakefile
perl -i -pe 's,/usr/local,,' csup.1

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_bindir}
%{__mkdir_p} %{buildroot}%{_mandir}/man1
%{__mkdir_p} %{buildroot}%{_sysconfdir}/cvsup

install -m 755 csup %{buildroot}%{_bindir}
install -m 644 csup.1 %{buildroot}%{_mandir}/man1

# sample files
cp %{S:1} %{buildroot}%{_sysconfdir}/cvsup
cp %{S:2} %{buildroot}%{_sysconfdir}/cvsup

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/*
%dir %{_sysconfdir}/cvsup
%{_sysconfdir}/cvsup/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.20060318-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.20060318-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.20060318-10m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20060318-9m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20060318-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20060318-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20060318-6m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.20060318-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.20060318-4m)
- rebuild against gcc43

* Thu Nov 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.20060318-3m)
- add Source2: ruby.cvsup

* Fri Apr 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.20060318-2m)
- add %%{_sysconfdir}/cvsup in files
- add sample file %%{S:1}

* Fri Apr 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.20060318-1m)
- initial package
