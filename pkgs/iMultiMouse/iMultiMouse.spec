%global momorel 33

Summary: iMultiMouse that can be used two mouses at the same time
Name: iMultiMouse
Version: 2.4.0
Release: %{momorel}m%{?dist}
Group: Applications/System
License: GPL
Source: http://www.netfort.gr.jp/~take/linux/iMultiMouse-2.4.0.tar.gz
Patch1: iMultiMouse.patch
URL: http://www.netfort.gr.jp/~take/linux/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(preun): chkconfig

%description
iMultiMouse is derived from MultiMouse, which is written by Mr.Manabe in
order to use more than one mouse.

%prep
rm -rf %{buildroot}

%setup -q -n %{name}-%{version}
%patch1 -p1 -b .iMultiMouse

%build
CFLAGS="%{optflags}" make PREFIX=%{_prefix}

%install
mkdir -p %{buildroot}%{_prefix}/bin
mkdir -p %{buildroot}%{_prefix}/sbin
mkdir -p %{buildroot}%{_mandir}/{man1,man8}
mkdir -p %{buildroot}%{_mandir}/ja/{man1,man8}
mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}/dev
make PREFIX=%{buildroot}%{_prefix} MANDIR=%{buildroot}%{_mandir} JMANDIR=%{buildroot}%{_mandir}/ja FIFO=%{buildroot}/dev/mumse install
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done
install -m 755 etc/rc.multimouse %{buildroot}%{_initscriptdir}/imultimouse
install -m 644 etc/sysconfig.multimouse %{buildroot}%{_sysconfdir}/imultimouse.conf

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add imultimouse

%preun
if [ $1 = 0 ]; then
   %{_initscriptdir}/multimouse stop > /dev/null 2>&1
   /sbin/chkconfig --del imultimouse
fi

%files
%defattr(-,root,root)
%doc README README.jis BUGS CHANGES CHANGES.jis COPYING
%doc 00ORIGINAL-DOCS
%{_prefix}/sbin/imultimoused
%{_prefix}/sbin/immconfig
%{_prefix}/bin/imultimouse
%{_mandir}/man1/imultimouse.1*
%{_mandir}/man8/imultimoused.8*
%{_mandir}/ja/man1/imultimouse.1*
%{_mandir}/ja/man8/imultimoused.8*
%config %{_initscriptdir}/imultimouse
%config(noreplace) %{_sysconfdir}/imultimouse.conf
/dev/mumse

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-33m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-32m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-31m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-30m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-28m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-27m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-26m)
- rebuild against gcc43

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.0-25m)
- convert ja.man(UTF-8)

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.0-24m)
- stop daemon

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.0-23m)
- revised spec for enabling rpm 4.2.

* Sun Aug  4 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.4.0-22m)
- stop nosrc(where is original source...)

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (2.4.0-21m)
- modify release number only.

* Thu Feb 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.4.0-20k)
- treat the init script and the config file as %config

* Sun Jan 20 2002 Tadataka YOSHIKAWA <yosshy@kondara.org>
- (2.4.0-18k)
- fixed configuration-file (/etc/imultimouse.conf)

* Sun Jan 20 2002 Tadataka YOSHIKAWA <yosshy@kondara.org>
- (2.4.0-16k)
- fixed daemon script (/etc/init.d/imultimouse)

* Mon Dec 10 2001 Tadataka YOSHIKAWA <yosshy@kondara.org>
- (2.4.0-14k)
- fixed configuration-file (/etc/imultimouse.conf)

* Mon Dec 10 2001 Tadataka YOSHIKAWA <yosshy@kondara.org>
- (2.4.0-12k)
- fixed configuration-file directory
- fixed configuration-file name

* Tue Oct 11 2001 Masahiro TAKAHATA <takahata@mtaka.com>
- (2.4.0-11k)
- bug fixes in iMultiMouse.spec

* Wed Oct 10 2001 Masahiro TAKAHATA <takahata@mtaka.com>
- (2.4.0-9k)
- fixed install script(build by normal user)
- fixed install-directories of 00ORIGINAL-DOCS

* Tue Oct 9 2001  Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (2.4.0-7k)
- fixed install-directories of some documents

* Mon Oct 8 2001  Tadataka YOSHIKAWA <yosikawa@mc.neweb.ne.jp>
- (2.4.0-5k)
- bug fixes in imultimouse.patch

* Mon Oct 8 2001  Tadataka YOSHIKAWA <yosikawa@mc.neweb.ne.jp>
- (2.4.0-3k)
- make 'iMultiMouse' package
