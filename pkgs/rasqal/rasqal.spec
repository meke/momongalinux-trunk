%global         momorel 1

Name:           rasqal
Version:        0.9.30
Release:        %{momorel}m%{?dist}
Summary:        Rasqal RDF Query Library

Group:          System Environment/Libraries
License:        "LGPLv2+ or ASL 2.0"
URL:            http://librdf.org/rasqal/
Source:         http://download.librdf.org/source/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       raptor2 >= 2.0.9
BuildRequires:  raptor2-devel >= 2.0.9
BuildRequires:  gtk-doc
BuildRequires:  pcre-devel
BuildRequires:  libxml2-devel
BuildRequires:  mpfr-devel >= 3.0.0
# make check requires
BuildRequires:  perl-libxml-enno
# to avoid /usr/lib64 rpath on x86_64
BuildRequires:  libtool

%description
Rasqal is a library providing full support for querying Resource
Description Framework (RDF) including parsing query syntaxes, constructing
the queries, executing them and returning result formats.  It currently
handles the RDF Data Query Language (RDQL) and SPARQL Query language.

%package        devel
Summary:        Development files for the Rasqal RDF libraries
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       raptor-devel >= 1.4.16
Requires:       pkgconfig

%description    devel
Libraries, includes etc to develop with the Rasqal RDF query language library.

%prep
%setup -q

# hack to nuke rpaths
%if "%{_libdir}" != "/usr/lib"
sed -i -e 's|"/lib /usr/lib|"/%{_lib} %{_libdir}|' configure
%endif

%build
%configure --enable-release --with-raptor=system --disable-static

make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}

make DESTDIR=%{buildroot} install
find %{buildroot} -name \*.la -exec rm {} \;

# remove .a files that we now get because of overriding libtool
find %{buildroot} -name \*.a -exec rm {} \;

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)

%doc AUTHORS COPYING COPYING.LIB ChangeLog LICENSE.txt NEWS README
%doc LICENSE-2.0.txt NOTICE
%doc *.html
%{_bindir}/roqet
%{_libdir}/librasqal*.so.*
%{_mandir}/man1/roqet.1*
%{_mandir}/man3/librasqal.3*

%files devel
%defattr(-,root,root,-)
%doc docs/README.html
%{_libdir}/librasqal*.so
%{_includedir}/*
%{_bindir}/rasqal-config
%{_mandir}/man1/rasqal-config.1*
%{_libdir}/pkgconfig/rasqal.pc
%{_datadir}/gtk-doc/html/*

%changelog
* Tue Mar 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.30-1m)
- update to 0.9.30

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.29-1m)
- update to 0.9.29

* Mon Dec  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.28-2m)
- BuildRequires: raptor2 >= 2.0.6

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.28-1m)
- update to 0.9.28

* Sat Aug  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.26-1m)
- update to 0.9.26

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.25-1m)
- update to 0.9.25

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.20-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.20-2m)
- rebuild against mpfr-3.0

* Fri Oct 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.20-1m)
- update to 0.9.20

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.19-2m)
- full rebuild for mo7 release

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.19-1m)
- update to 0.9.19

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.17-1m)
- update to 0.9.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.16-1m)
- update to 0.9.16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.15-6m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.15-5m)
- rebuild against openssl-0.9.8h-1m

* Fri Apr  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-4m)
- add BuildRequires: perl-libxml-enno

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.15-3m)
- rebuild against gcc43

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.15-2m)
- adjust duplicate files/directories

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.15-1m)
- import from Fedora devel and update to 0.9.15

* Mon Oct 15 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.9.14-2
- Update minimum raptor version

* Mon Oct 15 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.9.14-1
- Update to 0.9.14 (for redland 1.0.6, also lots of bugfixes)
- Specify LGPL version in License tag

* Mon Dec 18 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.12-5
- added pcre-devel and libxml2-devel buildrequires

* Wed Dec 13 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.12-4
- Requires: pkgconfig in -devel package (Kevin Fenzi)

* Fri Nov 17 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.12-3
- rpmlint cleanup

* Thu Oct 26 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.12-2
- Surrender and use DESTDIR install

* Sat Jun 17 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.12-2
- fixed x86_64 rpath issue with an ugly hack
- removed OPTIMIZE from make invocation
- added smp flags
- added make check
- updated license

* Sun May 14 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.12-1
- new upstream release

* Fri Apr 07 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.11-1: packaged for Fedora Extras

* Wed Aug 11 2005  Dave Beckett <dave.beckett@bristol.ac.uk>
- Update Source:
- Use makeinstall macro

* Wed Aug 10 2005  Dave Beckett <dave.beckett@bristol.ac.uk>
- Use configure macro.

* Fri Jul 28 2005  Dave Beckett <dave.beckett@bristol.ac.uk>
- Updated for gtk-doc locations

* Fri Oct 22 2004 <Dave.Beckett@bristol.ac.uk>
- License now LGPL/Apache 2
- Added LICENSE-2.0.txt and NOTICE

* Wed May 5 2004 <Dave.Beckett@bristol.ac.uk>
- Ship roqet and roqet.1

* Sat May 1 2004 <Dave.Beckett@bristol.ac.uk>
- Requires raptor 1.3.0

* Mon Feb 24 2004 <Dave.Beckett@bristol.ac.uk>
- Requires raptor

* Mon Aug 11 2003 <Dave.Beckett@bristol.ac.uk>
- Initial packaging
