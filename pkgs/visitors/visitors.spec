%global momorel 10

Summary: A fast web log analyzer
Name: visitors
Version: 0.7
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.hping.org/visitors/index_jp.php
Group: Applications/Internet
Source0: http://www.hping.org/visitors/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: httpd, graphviz
#BuildRequires: 

%description
Visitors is a fast web log analyzer

%prep
%setup -q -n %{name}_%{version}

%build
export CFLAGS="%{optflags}"
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_bindir}
%{__mkdir_p} %{buildroot}%{_mandir}/man1

cp visitors %{buildroot}%{_bindir}
cp visitors.1 %{buildroot}%{_mandir}/man1

mkdir -p %{buildroot}%{_localstatedir}/www/html/visitors

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc TODO SECURITY README Changelog COPYING AUTHORS
%{_bindir}/*
%{_mandir}/man1/*
%dir %{_localstatedir}/www/html/visitors

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7-10m)
- rebuild against graphviz-2.36.0-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-3m)
- %%NoSource -> NoSource

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-2m)
- add %%dir %{_localstatedir}/www/html/visitors in %%files section

* Wed Mar  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-1m)
- initial package
- spec was copied from yauap
