%global momorel 23

Summary: Dictionaries for SKK
Name: skk-jisyo
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
URL: http://openlab.ring.gr.jp/skk/dic-ja.html
Source0: http://openlab.jp/skk/skk/dic/SKK-JISYO.L.gz
Source1: http://openlab.jp/skk/skk/dic/SKK-JISYO.L.unannotated.gz
Source2: http://openlab.jp/skk/skk/dic/SKK-JISYO.M.gz
Source3: http://openlab.jp/skk/skk/dic/SKK-JISYO.S.gz
Source4: http://openlab.jp/skk/skk/dic/SKK-JISYO.JIS2.gz
Source5: http://openlab.jp/skk/skk/dic/SKK-JISYO.JIS3_4.gz
Source6: http://openlab.jp/skk/skk/dic/SKK-JISYO.jinmei.gz
Source7: http://openlab.jp/skk/skk/dic/SKK-JISYO.law.gz
Source8: http://openlab.jp/skk/skk/dic/SKK-JISYO.okinawa.gz
Source9: http://openlab.jp/skk/skk/dic/SKK-JISYO.mazegaki.gz
Source10: http://openlab.jp/skk/skk/dic/SKK-JISYO.geo.gz
Source11: http://openlab.ring.gr.jp/skk/dic/SKK-JISYO.edict.tar.gz
Source12: http://openlab.jp/skk/skk/dic/SKK-JISYO.pubdic+.gz
Source13: http://openlab.jp/skk/skk/dic/SKK-JISYO.wrong.gz
Source14: http://openlab.jp/skk/skk/dic/SKK-JISYO.wrong.annotated.gz
Source15: http://openlab.ring.gr.jp/skk/dic/zipcode.tar.gz
Source16: http://openlab.ring.gr.jp/skk/skk/tools/unannotation.awk

Source17: http://openlab.jp/skk/skk/dic/SKK-JISYO.JIS2004.gz
Source18: http://openlab.jp/skk/skk/dic/SKK-JISYO.assoc.gz
Source19: http://openlab.jp/skk/skk/dic/SKK-JISYO.china_taiwan.gz
Source20: http://openlab.jp/skk/skk/dic/SKK-JISYO.itaiji.gz
Source21: http://openlab.jp/skk/skk/dic/SKK-JISYO.itaiji.JIS3_4.gz
Source22: http://openlab.jp/skk/skk/dic/SKK-JISYO.propernoun.gz
Source23: http://openlab.jp/skk/skk/dic/SKK-JISYO.station.gz

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: skktools
Requires: words

%description
Kanji-Kana translation dictionaries for SKK.

%prep
%setup -q -cT
%{__gzip} -dc %{SOURCE0} > SKK-JISYO.L
%{__gzip} -dc %{SOURCE1} > SKK-JISYO.L.unannotated
%{__gzip} -dc %{SOURCE2} > SKK-JISYO.M
%{__gzip} -dc %{SOURCE3} > SKK-JISYO.S
%{__gzip} -dc %{SOURCE4} > SKK-JISYO.JIS2
%{__gzip} -dc %{SOURCE5} > SKK-JISYO.JIS3_4
%{__gzip} -dc %{SOURCE6} > SKK-JISYO.jinmei
%{__gzip} -dc %{SOURCE7} > SKK-JISYO.law
%{__gzip} -dc %{SOURCE8} > SKK-JISYO.okinawa
%{__gzip} -dc %{SOURCE9} > SKK-JISYO.mazegaki
%{__gzip} -dc %{SOURCE10} > SKK-JISYO.geo
tar zxf %{SOURCE11}
%{__gzip} -dc %{SOURCE12} > SKK-JISYO.pubdic+
%{__gzip} -dc %{SOURCE13} > SKK-JISYO.wrong
%{__gzip} -dc %{SOURCE14} > SKK-JISYO.wrong.annotated

%{__gzip} -dc %{SOURCE17} > SKK-JISYO.JIS2004
%{__gzip} -dc %{SOURCE18} > SKK-JISYO.assoc
%{__gzip} -dc %{SOURCE19} > SKK-JISYO.china_taiwan
%{__gzip} -dc %{SOURCE20} > SKK-JISYO.itaiji
%{__gzip} -dc %{SOURCE21} > SKK-JISYO.itaiji.JIS3_4
%{__gzip} -dc %{SOURCE22} > SKK-JISYO.propernoun
%{__gzip} -dc %{SOURCE23} > SKK-JISYO.station

tar zxf %{SOURCE15}
mv zipcode/SKK-JISYO.* .
mv zipcode/README.ja README.zipcode.ja

%build
awk -f %{SOURCE16} SKK-JISYO.jinmei > SKK-JISYO.jinmei.unannotated
awk -f %{SOURCE16} SKK-JISYO.edict > SKK-JISYO.edict.unannotated
awk -f %{SOURCE16} SKK-JISYO.station > SKK-JISYO.station.unannotated
skkdic-expr SKK-JISYO.L.unannotated \
    + SKK-JISYO.JIS2 \
    + SKK-JISYO.jinmei.unannotated \
    + SKK-JISYO.law \
    + SKK-JISYO.geo \
    + SKK-JISYO.station.unannotated \
    + SKK-JISYO.edict.unannotated \
    + SKK-JISYO.office.zipcode \
    + SKK-JISYO.zipcode \
    | skkdic-sort > SKK-JISYO.Momonga

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_datadir}/skk
for i in `ls SKK-JISYO.*`; do
    install -c -m 0644 $i %{buildroot}%{_datadir}/skk
done
install -c -m 0644 %{SOURCE16} %{buildroot}%{_datadir}/skk

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc edict_doc.txt README.zipcode.ja
%dir %{_datadir}/skk
%{_datadir}/skk/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-23m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-22m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-21m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-19m)
- update 2009-06-30

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-18m)
- update 2009-05-23
- add JIS2004, assoc, china_taiwan, itaiji, itaiji.JIS3_4, propernoun, station

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-17m)
- rebuild against rpm-4.6

* Sun Sep  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-16m)
- update 2008-09-07
- remove dic-ja.html

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-15m)
- rebuild against gcc43

* Sun Mar 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-14m)
- No NoSource

* Wed Aug  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-13m)
- momonganize

* Sun Dec 01 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-12m)
- s/.bz2/.gz/g at Source, and s/bzip2 -dc/%{__gzip} -dc/g at %setup

* Fri Nov 29 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-11m)
- Source15: bz2 -> gz

* Sat Feb 16 2002 Kenta MURATA <muraken@kondara.org>
- (1.0-10k)
- words.zipcode no ohikkoshi

* Thu Feb  8 2002 Kenta MURATA <muraken2@nifty.com>
- (1.0-8k)
- merge words.zipcode.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0-6k)
- nigittenu

* Mon Nov 12 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0-4k)
- add %clean section

* Sun Nov  4 2001 Kenta MURATA <muraken2@nifty.com>
- (1.0-2k)
- first release for Kondara.
