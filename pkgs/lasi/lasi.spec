%global         momorel 9

Name:           lasi
Version:        1.1.0
Release:        %{momorel}m%{?dist}
Summary:        C++ library for creating Postscript documents

Group:          Development/Libraries
License:        LGPL
URL:            http://www.unifont.org/lasi/
Source0:        http://downloads.sourceforge.net/lasi/libLASi-%{version}.tar.gz
Patch0:         %{name}-%{version}-freetype.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  pango-devel, cmake
BuildRequires:  freetype-devel >= 2.5.3
# For testing
BuildRequires:  dejavu-sans-fonts
BuildRequires:  dejavu-sans-mono-fonts
BuildRequires:  dejavu-serif-fonts


%description
LASi is a library written by Larry Siden  that provides a C++ stream output
interface ( with operator << ) for creating Postscript documents that can
contain characters from any of the scripts and symbol blocks supported in
Unicode  and by Owen Taylor's Pango layout engine. The library accomodates
right-to-left scripts such as Arabic and Hebrew as easily as left-to-right
scripts. Indic and Indic-derived Complex Text Layout (CTL) scripts, such as
Devanagari, Thai, Lao, and Tibetan are supported to the extent provided by
Pango and by the OpenType fonts installed on your system. All of this is
provided without need for any special configuration or layout calculation on
the programmer's part.

Although the capability to produce Unicode-based multilingual Postscript
documents exists in large Open Source application framework libraries such as
GTK+, QT, and KDE, LASi was designed for projects which require the ability
to produce Postscript independent of any one application framework.


%package        devel
Summary:        Development headers and libraries for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pango-devel

%description    devel
%{summary}.


%prep
%setup -q -n libLASi-%{version}
%patch0 -p1 -b .freetype

%build
mkdir fedora
cd fedora
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
export FFLAGS="$RPM_OPT_FLAGS"
%cmake ..
make VERBOSE=1 %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
cd fedora
make install DESTDIR=$RPM_BUILD_ROOT VERBOSE=1


%check
cd fedora
ctest --verbose


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/libLASi.so.*


%files devel
%defattr(-,root,root,-)
%{_includedir}/LASi.h
%{_libdir}/libLASi.so
%{_libdir}/pkgconfig/lasi.pc
%doc %{_datadir}/lasi%{version}/
%doc %{_docdir}/libLASi-%{version}/


%changelog
* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-9m)
- enable to build with freetype-2.5.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-6m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-5m)
- change BR for new dejavu-fonts

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-3m)
- change BR from dejavu-fonts to dejavu-fonts-compat

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- import from Fedora for plplot->gdl

* Sat Feb  9 2008 - Orion Poplawski <orion@cora.nwra.com> - 1.1.0-1
- Update to 1.1.0

* Tue Aug 29 2006 - Orion Poplawski <orion@cora.nwra.com> - 1.0.6-1
- Update to 1.0.6
- Remove pkg-config patch applied upstream

* Mon May  8 2006 - Orion Poplawski <orion@cora.nwra.com> - 1.0.5-2
- Disable static libs
- Patch pc file to return -lLASi

* Thu May  4 2006 - Orion Poplawski <orion@cora.nwra.com> - 1.0.5-1
- Update to 1.0.5
- Remove unneeded patches and autotools
- Move doc dir to -devel package
- Make -devel package require pango-devel, included in LASi.h

* Mon Apr 24 2006 - Orion Poplawski <orion@cora.nwra.com> - 1.0.4-1
- Initial Fedora Extras version
