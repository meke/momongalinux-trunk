%global momorel 6

# Not noarch, but nothing to strip:
%define debug_package %{nil}

#for the gid creation
#%%bcond_without  fedora
%global username ctapiusers

Name:           ctapi-common
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Common files and packaging infrastructure for CT-API modules

Group:          System Environment/Libraries
License:        MIT/X
URL:            http://fedoraproject.org/
Requires(pre):	shadow-utils

Provides:       group(%username)
Source0:        %{name}.LICENSE
Source1:        %{name}.README
#BuildRequires:  fedora-usermgmt-devel
#%%{?FE_USERADD_REQ}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
%{summary}.


%prep
%setup -c -T
install -pm 644 %{SOURCE0} LICENSE
install -pm 644 %{SOURCE1} README


%build
echo %{_libdir}/ctapi > ctapi.conf


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 ctapi.conf \
    $RPM_BUILD_ROOT/etc/ld.so.conf.d/ctapi-%{_target_cpu}.conf
install -dm 755 $RPM_BUILD_ROOT%{_libdir}/ctapi


%clean
rm -rf $RPM_BUILD_ROOT

%pre
getent group %username >/dev/null || groupadd -r %username || :

%files
%defattr(-,root,root,-)
%doc LICENSE README
# Hardcoded /etc in README -> hardcoded here.
/etc/ld.so.conf.d/ctapi-%{_target_cpu}.conf
%{_libdir}/ctapi/


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Sun Jun 10 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Fri Sep 15 2006 Ville Skytta <ville.skytta at iki.fi> - 1.0-4
- Rebuild.

* Mon Jul 31 2006 Ville Skytta <ville.skytta at iki.fi> - 1.0-3
- Ensure proper doc file permissions.

* Sat May  6 2006 Ville Skytta <ville.skytta at iki.fi> - 1.0-2
- Encourage dir based dependency on %%{_libdir}/ctapi in packages (#190911).
- Split contents of README into a separate file.
- Change license to MIT, include license text.
- Add URL.

* Sat May  6 2006 Ville Skytta <ville.skytta at iki.fi> - 1.0-1
- First build.
