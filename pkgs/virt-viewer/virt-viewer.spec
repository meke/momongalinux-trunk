%global momorel 1

# -*- rpm-spec -*-

# Plugin isn't ready for real world use yet - it needs
# a security audit at very least
%define _with_plugin %{?with_plugin:1}%{!?with_plugin:0}

Name: virt-viewer
Version: 0.6.0
Release: %{momorel}m%{?dist}
Summary: Virtual Machine Viewer
Group: Applications/System
License: GPLv2+
URL: http://virt-manager.org/
Source0: http://virt-manager.org/download/sources/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: openssh-clients

BuildRequires: gtk2-devel
BuildRequires: libvirt-devel >= 0.9.7
BuildRequires: libxml2-devel
BuildRequires: libglade2-devel
BuildRequires: gtk-vnc-devel >= 0.3.8
BuildRequires: spice-gtk-devel >= 0.13
BuildRequires: perl
BuildRequires: intltool
%if %{_with_plugin}
BuildRequires: xulrunner-devel
%endif

%description
Virtual Machine Viewer provides a graphical console client for connecting
to virtual machines. It uses the GTK-VNC widget to provide the display,
and libvirt for looking up VNC server details.

%if %{_with_plugin}
%package plugin
Summary: Mozilla plugin for the gtk-vnc library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description plugin
gtk-vnc is a VNC viewer widget for GTK. It is built using coroutines
allowing it to be completely asynchronous while remaining single threaded.

This package provides a web browser plugin for Mozilla compatible
browsers.
%endif

%prep
%setup -q

%build
%configure --enable-plugin=yes --with-gtk=3.0 --with-spice-gtk --disable-update-mimedb
%make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%__make install  DESTDIR=$RPM_BUILD_ROOT
%if %{_with_plugin}
rm -f %{buildroot}%{_libdir}/mozilla/plugins/%{name}-plugin.a
rm -f %{buildroot}%{_libdir}/mozilla/plugins/%{name}-plugin.la
%endif
%find_lang %{name}


%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
%{_sbindir}/update-alternatives --install %{_libexecdir}/spice-xpi-client \
  spice-xpi-client %{_libexecdir}/spice-xpi-client-remote-viewer 25
update-desktop-database -q %{_datadir}/applications
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null

%postun
if [ $1 -eq 0 ] ; then
  /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
  /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  %{_sbindir}/update-alternatives --remove spice-xpi-client %{_libexecdir}/spice-xpi-client-remote-viewer
fi
update-desktop-database -q %{_datadir}/applications
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README COPYING AUTHORS ChangeLog NEWS
%{_bindir}/%{name}
%{_bindir}/remote-viewer
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/ui/
%{_datadir}/applications/remote-viewer.desktop
%{_datadir}/%{name}/ui/virt-viewer-auth.xml
%{_datadir}/%{name}/ui/virt-viewer-about.xml
%{_datadir}/%{name}/ui/virt-viewer.xml
%{_datadir}/icons/hicolor/*/apps/virt-viewer.png
%{_datadir}/mime/packages/virt-viewer-mime.xml
%{_mandir}/man1/%{name}*
%{_mandir}/man1/remote-viewer*

%if %{_with_plugin}
%files plugin
%defattr(-, root, root)
%{_libdir}/mozilla/plugins/%{name}-plugin.so
%endif

%changelog
* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.6-1m)
- update to 0.5.6

* Thu Mar 14 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.5-1m)
- update to 0.5.5

* Fri Sep 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.4-1m)
- update 0.5.4

* Fri Sep  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-1m)
- update 0.5.3

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2-1m)
- update 0.5.2

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-1m)
- update 0.4.2

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-1m)
- update 0.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-2m)
- rebuild against rpm-4.6

* Mon Jul 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.3-1m)
- version up 0.0.3

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.2-3m)
- rebuild against gnutls-2.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-2m)
- rebuild against gcc43

* Tue Sep 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.2-1m)
- initial commit Momonga Linux
- import from Fedora development

* Tue Aug 28 2007 Daniel P. Berrange <berrange@redhat.com> - 0.0.2-1.fc8
- Added support for remote console access

* Fri Aug 17 2007 Daniel P. Berrange <berrange@redhat.com> - 0.0.1-2.fc8
- Restrict built to x86 & ia64 because libvirt is only on those arches

* Wed Aug 15 2007 Daniel P. Berrange <berrange@redhat.com> - 0.0.1-1.fc8
- First release

