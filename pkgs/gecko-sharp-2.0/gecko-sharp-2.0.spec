%global momorel 13

Summary: gtkmozembed-sharp
Name: gecko-sharp-2.0
Version: 0.13
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL MPL
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mono-devel >= 2.8
BuildRequires: gtk2-devel >= 2.12.9
BuildRequires: monodoc-devel >= 1.9
BuildRequires: pkgconfig
BuildRequires: gtk-sharp2-devel >= 2.12.10-4m
Requires: gtk2 mono-core gtk-sharp2
Patch2: gecko-sharp-2.0-0.11-lib64.patch

Obsoletes: gecko-sharp
Provides: gecko-sharp = 2.0.%{version}

%description
This is gtkmozembed-sharp.  To use it, you need two things:
1) A working mozilla build.
   Note, the build *must* use gtk2.  You can do this by adding:
   ac_add_options --enable-default-toolkit=gtk2
   to your .mozconfig file.

   Also note, this needs gtk2.  The Win32 version of mozilla doesn't
   even touch gtk.  Therefore, this will *not* work on Win32.  It might,
   however.  Someone enterprising needs to try it.
#'
2) The CVS release of gtk-sharp.  There was a bug in the generator
   which is now fixed.  gtk-sharp also needs to be installed.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q
%patch2 -p1 -b .lib64

%build
autoreconf -fi
%configure --program-prefix=""
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING LICENSE.LGPL LICENSE.MPL NEWS README
%{_prefix}/lib/mono/gac/gecko-sharp
%{_prefix}/lib/mono/gecko-sharp-2.0
%{_prefix}/lib/monodoc/sources/*
# %{_datadir}/doc/gecko-sharp-2.0

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/gecko-sharp-2.0.pc


%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-13m)
- change Source0 URI

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13-12m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13-10m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13-9m)
- rebuild against mono-2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-5m)
- fix files

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-4m)
- add devel package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc43

* Tue Mar 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-5m)
- rebuild against gtk-sharp2-2.12.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-4m)
- %%NoSource -> NoSource

* Mon Aug 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-3m)
- remove BuildPrereq: mono-tools, mono-tools requires this package

* Tue May 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-2m)
- fix BuildPrereq

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- rename package gecko-sharp to gecko-sharp-2.0

* Sun Oct 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-0.11-9m)
- modify %%files for autoconf-2.60

* Wed Oct 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.11-8m)
- rewind %%files for autoconf-2.59

* Tue Oct 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-0.11-7m)
- rebuild against mono-1.1.18 monodoc-1.1.18

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-0.11-6m)
- rebuild against mono-1.1.17.1 gtk-sharp 2.10.0 monodoc-1.1.17.1

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0-0.11-5m)
- revised for multilibarch

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-0.11-4m)
- rebuild mono -> mono-core

* Thu Feb 02 2006 Masanobu Sato <satoshiga@momonga-linux.org
- (2.0-0.11-3m)
- revised spec file for monodoc files
 
* Thu Feb 02 2006 Masanobu Sato <satoshiga@momonga-linux.org
- (2.0-0.11-2m)
- add patches for x86_64

* Mon Jan 30 2006 Nishio Futoshi <futoshi@momonga-linux.org.
- (2.0-0.11-1m)
- start
