%global momorel 1
%global qtver 4.8.0
%global qtrel 1m
%global qtdir %{_libdir}/qt4

Summary: A documentation system for C and C++.
Name: doxygen
Version: 1.8.6
Release: %{momorel}m%{?dist}
Source0: ftp://ftp.stack.nl/pub/users/dimitri/doxygen-%{version}.src.tar.gz
NoSource: 0

Patch1: doxygen-1.8.6-config.patch
Patch2: doxygen-1.8.5-html_timestamp_default_false.patch 
Patch3: doxygen-1.8.3-multilib.patch

Group: Development/Tools
License: GPLv2
URL: http://www.stack.nl/~dimitri/doxygen/
Prefix: %{_prefix}
BuildRequires: libstdc++-devel >= 3.4.1-1m, perl, texlive-common
BuildRequires: ghostscript >= 7.07-14m
BuildRequires: libpng-devel >= 1.2.2
# for doxywizard
BuildRequires: qt-devel >= %{qtver}, libpng-devel >= 1.2.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Doxygen is a documentation system for C and C++.  Doxygen can generate
an on-line class browser (in HTML) and/or a reference manual (in
LaTeX) from a set of documented source files. The documentation is
extracted directly from the sources.  Doxygen can be configured to
extract the code structure from undocumented source files.

%package doxywizard
Summary: a GUI front-end for creating and editing configuration files
Group: User Interface/X
Requires: %{name} = %{version}

%description doxywizard
Doxywizard is a GUI front-end for creating and editing
configuration files that are used by doxygen.

%prep
%setup -q

%patch1 -p1 -b .config
%patch2 -p1 -b .html_timestamp_default_false
%patch3 -p1 -b .multilib

%build
export QTDIR=%{qtdir}
export QMAKE=%{qtdir}/bin/qmake

./configure --prefix %{_prefix} --docdir %{_mandir} --shared --release --with-doxywizard 

# workaround for "Error: operand out of range", language.cpp needs to be splitted
%ifarch ppc64
make -C src Makefile.libdoxygen
sed -i -e "s|-o ../objects/language.o|-fno-merge-constants -fsection-anchors -o ../objects/language.o|" src/Makefile.libdoxygen
%endif

%make all
make docs

%install
rm -rf --preserve-root %{buildroot}

%makeinstall INSTALL=%{buildroot}%{_prefix}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc LANGUAGE.HOWTO README.md examples
%{_bindir}/doxygen
%{_mandir}/man1/doxygen.1.*

%files doxywizard
%defattr(-,root,root)
%{_bindir}/doxywizard
%{_mandir}/man1/doxywizard.1.*

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.6-1m)
- update 1.8.6

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.6.1-2m)
- version down to 1.7.6.1

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Sat Dec 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.6.1-1m)
- update to 1.7.6.1
- patch10 was applied upstream

* Fri Dec  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6-2m)
- fix Hung
-- https://bugzilla.gnome.org/show_bug.cgi?id=665720 

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6-1m)
- update to 1.7.6

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5.1-1m)
- update to 1.7.5.1

* Thu Aug 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5-1m)
- update to 1.7.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.4-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-1m)
- update to 1.7.4

* Sun Jan 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.3-1m)
- update to 1.7.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-3m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.7.2-2m)
- BuildRequires: tetex -> texlive-common, for OmoiKondara build

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.2-1m)
- update to 1.7.2

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.1-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.1-1m)
- update to 1.7.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update to 1.7.0

* Sun Mar 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3. a lot of bugs are fixed

* Tue Jan 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-1m)
- update 1.6.2
- drop patch2, which is merged in upstream.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-1m)
- update 1.6.1
- import two patches from fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.8-3m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.8-2m)
- update Patch4,5 for fuzz=0

* Sun Dec 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.8-1m)
- update to 1.5.8
-- doxywizard now uses qt4 

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.7.1-1m)
- update to 1.5.7.1

* Wed Jul  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.6-1m)
- update 1.5.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.5-3m)
- rebuild against gcc43

* Fri Feb 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-2m)
- import patch to make doxygen using system libpng/zlib from fedora

* Fri Feb 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-1m)
- update to 1.5.5
- License: GPLv2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4-2m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4

* Mon Aug 13 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Fri Apr 13 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Tue Nov  7 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1
- remove unused patches
- import doxygen-1.2.18-libdir64.patch from fc-devel
- remove "Requires: php"

* Sun Jun 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.7-1m)
- update to 1.4.7
- do not apply patch3 and patch5 because no longer needed

* Tue Jun  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-1m)
- ver up.
- add Patch4: doxygen-1.4.5-man.patch
- enable gcc-4.1
- - Patch3: doxygen-1.4.5-qtools.patch

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.9.1-1m)
- ver up.

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.4-3m)
- enable x86_64.
  add doxygen-1.3.4-lib64.patch patch.

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.4-2m)
- rebuild against libstdc++-3.4.1

* Tue Nov 11 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4
- add a patch to avoid segfault on indexing japanese source files

* Sat Jul 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.3-1m)
- update to 1.3.3

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.2-1m)
- update to 1.3.2

* Tue Jun  5 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3-3m)
- add BuildPrereq: tetex

* Wed May 28 2003 zunda <zunda at freeshell.org>
- (1.3-2m)
- add --install=/usr/bin/install to configure: /usr/bin/install from
  coreutils is not recognized as a GNU install

* Sun Apr 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3-1m)
  update to 1.3

* Tue Mar 25 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3-0.0.3.2m)
- change %%{qtdir} to /usr/lib/qt3 and BuildPreReq: qt >= %%{qtver}

* Sat Mar  8 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3-0.0.3.1m)
- 1.3-rc3

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (1.2.15-2k)
- ver up.

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.11.1-8k)
- rebuild against libpng-1.2.2

* Mon Mar 25 2002 Toru Hoshina <t@kondara.org>
- (1.2.11.1-6k)
- rebuild against qt2 2.3.1 :-P

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (1.2.11.1-4k)
- rebuild against qt 2.3.2.

* Mon Nov  5 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.11.1-2k)
- version 1.2.11.1

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (1.2.9.1-6k)
- rebuild against libpng 1.2.0.

* Fri Aug 31 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.9.1-5k)
- rebuild against qt-2.3.1
- add BuildPrereq qt = %{qtver}

* Tue Aug 21 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.2.9.1

* Sat Jul  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.8.1-3K)

* Wed Jun 13 2001 Than Ngo <than@redhat.com>
- update tp 1.2.8.1
- make doxywizard as separat package
- fix to use install as default

* Tue Jun 05 2001 Than Ngo <than@redhat.com>
- update to 1.2.8

* Tue May 01 2001 Than Ngo <than@redhat.com>
- update to 1.2.7
- clean up specfile
- patch to use RPM_OPT_FLAG

* Wed Mar 14 2001 Jeff Johnson <jbj@redhat.com>
- update to 1.2.6

* Wed Feb 28 2001 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Tue Dec 26 2000 Than Ngo <than@redhat.com>
- update to 1.2.4
- remove excludearch ia64
- bzip2 sources

* Mon Dec 11 2000 Than Ngo <than@redhat.com>
- rebuild with the fixed fileutils

* Mon Oct 30 2000 Jeff Johnson <jbj@redhat.com>
- update to 1.2.3.

* Sun Oct  8 2000 Jeff Johnson <jbj@redhat.com>
- update to 1.2.2.
- enable doxywizard.

* Sat Aug 19 2000 Preston Brown <pbrown@redhat.com>
- 1.2.1 is latest stable, so we upgrade before Winston is released.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul  4 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Fri Jun 30 2000 Florian La Roche <laroche@redhat.de>
- fix QTDIR detection

* Fri Jun 09 2000 Preston Brown <pbrown@redhat.com>
- compile on x86 w/o optimization, revert when compiler fixed!!

* Wed Jun 07 2000 Preston Brown <pbrown@redhat.com>
- use newer RPM macros

* Tue Jun  6 2000 Jeff Johnson <jbj@redhat.com>
- add to distro.

* Tue May  9 2000 Tim Powers <timp@redhat.com>
- rebuilt for 7.0

* Wed Feb  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- recompile with current Qt (2.1.0/1.45)

* Wed Jan  5 2000 Jeff Johnson <jbj@redhat.com>
- update to 1.0.0.
- recompile with qt-2.0.1 if available.
- relocatable package.

* Mon Nov  8 1999 Tim Powers <timp@redhat.com>
-updated to 0.49-991106

* Tue Jul 13 1999 Tim Powers <timp@redhat.com>
- updated source
- cleaned up some stuff in the spec file

* Thu Apr 22 1999 Jeff Johnson <jbj@redhat.com>
- Create Power Tools 6.0 package.
