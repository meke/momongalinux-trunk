%global momorel 1
%define ldap_support 1
%define static_ldap 1
%define krb5_support 1
%define nntp_support 1
%define largefile_support 1

%define glib2_version 2.30.0
%define gtk3_version 3.2.0
%define gcr_version 3.4
%define gtk_doc_version 1.9
%define intltool_version 0.35.5
%define libgdata_version 0.10.0
%define libgweather_version 3.5.92
%define libical_version 0.46
%define liboauth_version 0.9.4
%define soup_version 2.31.2
%define sqlite_version 3.5

%define eds_base_version 3.6

%define camel_provider_dir %{_libdir}/evolution-data-server/camel-providers
%define ebook_backends_dir %{_libdir}/evolution-data-server/addressbook-backends
%define ecal_backends_dir %{_libdir}/evolution-data-server/calendar-backends
%define modules_dir %{_libdir}/evolution-data-server/registry-modules

### Abstract ###

Name: evolution-data-server
Version: 3.6.1
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
Summary: Backend data server for Evolution
License: LGPLv2+
URL: http://projects.gnome.org/evolution/
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Source: http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Provides: evolution-webcal = %{version}
Obsoletes: evolution-webcal < 2.24.0

### Patches ###

# RH bug #243296
Patch01: evolution-data-server-1.11.5-fix-64bit-acinclude.patch

### Build Dependencies ###

BuildRequires: bison
BuildRequires: db4-devel
BuildRequires: dbus-glib-devel
BuildRequires: gcr-devel >= %{gcr_version}
BuildRequires: gettext
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gnome-common
BuildRequires: gnome-online-accounts-devel
BuildRequires: gnutls-devel
BuildRequires: gperf
BuildRequires: gtk-doc >= %{gtk_doc_version}
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: intltool >= %{intltool_version}
BuildRequires: libgdata-devel >= %{libgdata_version}
BuildRequires: gcr-devel
BuildRequires: libgweather-devel >= %{libgweather_version}
BuildRequires: libical-devel >= %{libical_version}
BuildRequires: liboauth-devel >= %{liboauth_version}
BuildRequires: libsoup-devel >= %{soup_version}
BuildRequires: libtool
BuildRequires: nspr-devel
BuildRequires: nss-devel
BuildRequires: sqlite-devel >= %{sqlite_version}
BuildRequires: vala-devel >= 0.17.4
BuildRequires: vala-tools

%if %{ldap_support}
%if %{static_ldap}
BuildRequires: openldap-evolution-devel
BuildRequires: openssl-devel
%else
BuildRequires: openldap-devel >= 2.0.11 
%endif
%endif

%if %{krb5_support} 
BuildRequires: krb5-devel 
# tweak for krb5 1.2 vs 1.3
%define krb5dir /usr/kerberos
#define krb5dir `pwd`/krb5-fakeprefix
%endif

%description
The %{name} package provides a unified backend for programs that work
with contacts, tasks, and calendar information.

It was originally developed for Evolution (hence the name), but is now used
by other packages.

%package devel
Summary: Development files for building against %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-glib-devel
Requires: gnome-online-accounts-devel
Requires: libgdata-devel
Requires: gcr-devel
Requires: libgweather-devel
Requires: libical-devel
Requires: liboauth-devel
Requires: libsoup-devel
Requires: sqlite-devel

%description devel
Development files needed for building things which link against %{name}.

%package doc
Summary: Documentation files for %{name}
Group: Development/Libraries
BuildArch: noarch

%description doc
This package contains developer documentation for %{name}.

%prep
%setup -q

%patch01 -p1 -b .fix-64bit-acinclude

mkdir -p krb5-fakeprefix/include
mkdir -p krb5-fakeprefix/lib
mkdir -p krb5-fakeprefix/%{_lib}

%build
%if %{ldap_support}

%if %{static_ldap}
%define ldap_flags --with-openldap=%{_libdir}/evolution-openldap --with-static-ldap
# Set LIBS so that configure will be able to link with static LDAP libraries,
# which depend on Cyrus SASL and OpenSSL.  XXX Is the "else" clause necessary?
if pkg-config openssl ; then
	export LIBS="-lsasl2 `pkg-config --libs openssl`"
else
	export LIBS="-lsasl2 -lssl -lcrypto"
fi
# newer versions of openldap are built with Mozilla NSS crypto, so also need
# those libs to link with the static ldap libs
if pkg-config nss ; then
    export LIBS="$LIBS `pkg-config --libs nss`"
else
    export LIBS="$LIBS -lssl3 -lsmime3 -lnss3 -lnssutil3 -lplds4 -lplc4 -lnspr4"
fi
%else
%define ldap_flags --with-openldap=yes
%endif

%else
%define ldap_flags --without-openldap
%endif

%if %{krb5_support}
%define krb5_flags --with-krb5=%{krb5dir}
%else
%define krb5_flags --without-krb5
%endif

%if %{nntp_support}
%define nntp_flags --enable-nntp=yes
%else
%define nntp_flags --enable-nntp=no
%endif

%if %{largefile_support}
%define largefile_flags --enable-largefile
%else
%define largefile_flags --disable-largefile
%endif

%define ssl_flags --enable-nss=yes --enable-smime=yes

if ! pkg-config --exists nss; then 
  echo "Unable to find suitable version of nss to use!"
  exit 1
fi

export CPPFLAGS="-I%{_includedir}/et"
export CFLAGS="$RPM_OPT_FLAGS -DLDAP_DEPRECATED -fPIC -I%{_includedir}/et"

# Regenerate configure to pick up configure.in and acinclude.m4 changes..
aclocal -I m4
autoheader
automake
libtoolize
intltoolize --force
autoconf

# See Ross Burton's blog entry for why we want --with-libdb.
# http://www.burtonini.com/blog//computers/eds-libdb-2006-07-18-10-40

%configure \
	--disable-maintainer-mode \
	--with-libdb=/usr \
	--enable-file-locking=fcntl \
	--enable-dot-locking=no \
	--enable-gtk-doc \
	--enable-introspection=yes \
	--enable-vala-bindings \
	%ldap_flags %krb5_flags %nntp_flags %ssl_flags \
	%largetfile_flags
export tagname=CC

# Do not build in parallel. The libedata-book and libedata-cal directories
# each produce a shared library and an executable binary that links to the
# shared library. If the executable binary finishes first the build fails.
# There may be other similar cases in the source tree.
#make %{?_smp_mflags} LIBTOOL=/usr/bin/libtool
make LIBTOOL=/usr/bin/libtool

%install
rm -rf $RPM_BUILD_ROOT
export tagname=CC
make DESTDIR=$RPM_BUILD_ROOT LIBTOOL=/usr/bin/libtool install

# remove libtool archives for importers and the like
find $RPM_BUILD_ROOT/%{_libdir} -name '*.la' -exec rm {} \;
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.a
rm -f $RPM_BUILD_ROOT/%{_libdir}/evolution-data-server/camel-providers/*.a
rm -f $RPM_BUILD_ROOT/%{_libdir}/evolution-data-server/addressbook-backends/*.a
rm -f $RPM_BUILD_ROOT/%{_libdir}/evolution-data-server/calendar-backends/*.a
rm -f $RPM_BUILD_ROOT/%{_libdir}/evolution-data-server/registry-modules/*.a

# give the libraries some executable bits 
find $RPM_BUILD_ROOT -name '*.so.*' -exec chmod +x {} \;

%find_lang %{name}-%{eds_base_version}

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f %{name}-%{eds_base_version}.lang
%defattr(-,root,root,-)
%doc README COPYING ChangeLog NEWS AUTHORS
%{_libdir}/libcamel-1.2.so.*
%{_libdir}/libebackend-1.2.so.*
%{_libdir}/libebook-1.2.so.*
%{_libdir}/libecal-1.2.so.*
%{_libdir}/libedata-book-1.2.so.*
%{_libdir}/libedata-cal-1.2.so.*
%{_libdir}/libedataserver-1.2.so.*
%{_libdir}/libedataserverui-3.0.so.*

%{_libdir}/girepository-1.0/EBook-1.2.typelib
%{_libdir}/girepository-1.0/ECalendar-1.2.typelib
%{_libdir}/girepository-1.0/EDataServer-1.2.typelib

%{_libexecdir}/camel-index-control-1.2
%{_libexecdir}/camel-lock-helper-1.2
%{_libexecdir}/evolution-addressbook-factory
%{_libexecdir}/evolution-calendar-factory
%{_libexecdir}/evolution-source-registry

# GSettings schemas:
%{_datadir}/GConf/gsettings/libedataserver.convert
%{_datadir}/GConf/gsettings/evolution-data-server.convert
%{_datadir}/glib-2.0/schemas/org.gnome.Evolution.DefaultSources.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution-data-server.addressbook.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution-data-server.calendar.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.eds-shell.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.shell.network-config.gschema.xml

%{_datadir}/evolution-data-server-%{eds_base_version}
%{_datadir}/dbus-1/services/org.gnome.evolution.dataserver.AddressBook.service
%{_datadir}/dbus-1/services/org.gnome.evolution.dataserver.Calendar.service
%{_datadir}/dbus-1/services/org.gnome.evolution.dataserver.Sources.service
%{_datadir}/pixmaps/evolution-data-server

%dir %{_libdir}/evolution-data-server
%dir %{camel_provider_dir}
%dir %{ebook_backends_dir}
%dir %{ecal_backends_dir}
%dir %{modules_dir}

# Camel providers:
%{camel_provider_dir}/libcamelimap.so
%{camel_provider_dir}/libcamelimap.urls

%{camel_provider_dir}/libcamelimapx.so
%{camel_provider_dir}/libcamelimapx.urls

%{camel_provider_dir}/libcamellocal.so
%{camel_provider_dir}/libcamellocal.urls

%{camel_provider_dir}/libcamelnntp.so
%{camel_provider_dir}/libcamelnntp.urls

%{camel_provider_dir}/libcamelpop3.so
%{camel_provider_dir}/libcamelpop3.urls

%{camel_provider_dir}/libcamelsendmail.so
%{camel_provider_dir}/libcamelsendmail.urls

%{camel_provider_dir}/libcamelsmtp.so
%{camel_provider_dir}/libcamelsmtp.urls

# e-d-s extensions:
%{ebook_backends_dir}/libebookbackendfile.so
%{ebook_backends_dir}/libebookbackendgoogle.so
%{ebook_backends_dir}/libebookbackendldap.so
%{ebook_backends_dir}/libebookbackendvcf.so
%{ebook_backends_dir}/libebookbackendwebdav.so
%{ecal_backends_dir}/libecalbackendcaldav.so
%{ecal_backends_dir}/libecalbackendcontacts.so
%{ecal_backends_dir}/libecalbackendfile.so
%{ecal_backends_dir}/libecalbackendhttp.so
%{ecal_backends_dir}/libecalbackendweather.so
%{modules_dir}/module-cache-reaper.so
%{modules_dir}/module-google-backend.so
%{modules_dir}/module-online-accounts.so
%{modules_dir}/module-yahoo-backend.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/evolution-data-server-%{eds_base_version}
%{_libdir}/libcamel-1.2.so
%{_libdir}/libebackend-1.2.so
%{_libdir}/libebook-1.2.so
%{_libdir}/libecal-1.2.so
%{_libdir}/libedata-book-1.2.so
%{_libdir}/libedata-cal-1.2.so
%{_libdir}/libedataserver-1.2.so
%{_libdir}/libedataserverui-3.0.so
%{_libdir}/pkgconfig/camel-1.2.pc
%{_libdir}/pkgconfig/evolution-data-server-1.2.pc
%{_libdir}/pkgconfig/libebackend-1.2.pc
%{_libdir}/pkgconfig/libebook-1.2.pc
%{_libdir}/pkgconfig/libecal-1.2.pc
%{_libdir}/pkgconfig/libedata-book-1.2.pc
%{_libdir}/pkgconfig/libedata-cal-1.2.pc
%{_libdir}/pkgconfig/libedataserver-1.2.pc
%{_libdir}/pkgconfig/libedataserverui-3.0.pc
%{_datadir}/gir-1.0/EBook-1.2.gir
%{_datadir}/gir-1.0/ECalendar-1.2.gir
%{_datadir}/gir-1.0/EDataServer-1.2.gir
%{_datadir}/vala/vapi/libebook-1.2.deps
%{_datadir}/vala/vapi/libebook-1.2.vapi
%{_datadir}/vala/vapi/libecalendar-1.2.deps
%{_datadir}/vala/vapi/libecalendar-1.2.vapi
%{_datadir}/vala/vapi/libedataserver-1.2.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/camel
%{_datadir}/gtk-doc/html/libebackend
%{_datadir}/gtk-doc/html/libebook
%{_datadir}/gtk-doc/html/libecal
%{_datadir}/gtk-doc/html/libedata-book
%{_datadir}/gtk-doc/html/libedata-cal
%{_datadir}/gtk-doc/html/libedataserver
%{_datadir}/gtk-doc/html/libedataserverui

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3.1-2m)
- add gweather patch

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3.1-1m)
- update to 3.5.3.1

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- update to 3.5.2
- reimport from fedora
- switch to use gcr-devel instaed of gnome-keyring-devel

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-2m)
- rebuild against libdb-5.3.15

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-1m)
- update to 3.2.3

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.91-3m)
- specify libgdata version

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91-2m)
- add BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.2.1-2m)
- rebuild against libdb

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2.1-1m)
- update to 3.0.2.1

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Thu Apr 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.2-3m)
- add a patch to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.2-2m)
- rebuild for new GCC 4.6

* Wed Feb  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-1m)
- update to 2.32.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2.1-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2.1-1m)
- update to 2.30.2.1

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- --disable-gtk-doc

* Mon Dec 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Thu Apr 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1.1-1m)
- update to 2.26.1.1

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- [SECURITY] CVE-2009-0547 CVE-2009-0582
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Tue Feb  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4.1-1m)
- update to 2.24.4.1
- delete patch0 (fix gtk-doc.make)

* Sat Jan 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4-1m)
- update to 2.24.4
- add patch0 (for build document fix only)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Fri Nov  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1.1-1m)
- update to 2.24.1.1

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.22.3-3m)
- rebuild against db4-4.7.25-1m

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.3-2m)
- rebuild against gnutls-2.4.1

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.2-2m)
- rebuild against openssl-0.9.8h-1m

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Mon May  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-1m)
- update to 2.22.1.1

* Tue Apr 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- sync Fedora

* Sat Mar  8 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.12.3-2m)
- add BuildRequires: gtk-doc

* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.3-1m)
- update to 1.12.3

* Mon Nov 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.2-1m)
- update to 1.12.2

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.12.1-2m)
- rebuild against db4-4.6.21

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.1-1m)
- update to 1.12.1
- sync Fedora

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.3.1-1m)
- update to 1.10.3.1

* Thu Jun 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.2-2m)
- [SECURITY] CVE-2007-3257
- add patch303 http://svn.gnome.org/viewcvs/evolution-data-server/trunk/camel/providers/imap/camel-imap-folder.c?r1=7720&r2=7817&view=patch

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.2-1m)
- update to 1.10.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Sat Mar 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-2m)
- delete patch302 (not used 0 byte)

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0
- modify patch 102

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.92-1m)
- update to 1.9.92

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- update 1.8.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update 1.8.1

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.8.0-2m)
- rebuild against gnutls-1.4.4-1m

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 2.8.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.0-2m)
- rebuild against gnome-vfs

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0
- update patch102 from FC

* Wed Feb 22 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.2.1-2m)
- build with new nspr/nss

* Fri Jan 20 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.4.2.1-1m)
- update to 1.4.2.1

* Thu Jan 19 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.4.1.1-2m)
- -DLDAP_DEPRECATED should be removed ASAP

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.1.1-1m)
- import from Fedora

* Mon Oct 17 2005 David Malcolm <dmalcolm@redhat.com> - 1.4.1.1-1
- 1.4.1.1

* Mon Oct 17 2005 David Malcolm <dmalcolm@redhat.com> - 1.4.1-2
- Updated patch 102 (fix-implicit-function-declarations) to include fix for 
  http calendar backend (thanks to Peter Robinson)

* Tue Oct  4 2005 David Malcolm <dmalcolm@redhat.com> - 1.4.1-1
- 1.4.1

* Wed Sep 14 2005 Jeremy Katz <katzj@redhat.com> - 1.4.0-2
- rebuild now that mozilla builds on ppc64

* Tue Sep  6 2005 David Malcolm <dmalcolm@redhat.com> - 1.4.0-1
- 1.4.0
- Removed evolution-data-server-1.3.8-fix-libical-vsnprintf.c.patch; a version
  of this is now upstream (was patch 103, added in 1.3.8-2)

* Wed Aug 31 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.8-6
- Use regular LDAP library for now, rather than evolution-openldap (#167238)

* Tue Aug 30 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.8-5
- Add -Werror-implicit-function-declaration back to CFLAGS at the make stage, 
  after the configure, to spot 64-bit problems whilst avoiding breaking 
  configuration tests; expand patch 102 to avoid this breaking libdb's CFLAGS
#'

* Wed Aug 24 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.8-4
- Remove -Werror-implicit-function-declaration from CFLAGS; this broke the
  configuration test for fast mutexes in the internal copy of libdb, and hence
  broke access to local addressbooks (#166742)
- Introduce static_ldap macro; use it to link to static evolution-openldap 
  library, containing NTLM support for LDAP binds (needed by Exchange support)

* Tue Aug 23 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.8-3
- Updated patch 102 to fix further implicit function declarations

* Tue Aug 23 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.8-2
- added patch (103) to fix problem with configuration macros in libical's
  vsnprintf.c
#'

* Tue Aug 23 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.8-1
- 1.3.8
- Add -Werror-implicit-function-declaration to CFLAGS, to avoid 64-bit issues
  and add patch to fix these where they occur (patch 102)

* Mon Aug 15 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.7-2
- rebuild

* Tue Aug  9 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.7-1
- 1.3.7

* Mon Aug  8 2005 Tomas Mraz <tmraz@redhat.com> - 1.3.6.1-2
- rebuild with new gnutls

* Fri Jul 29 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.6.1-1
- 1.3.6.1

* Thu Jul 28 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.6-1
- 1.3.6

* Mon Jul 25 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.5-2
- Added patch to use nspr rather than mozilla-nspr when doing pkg-config tests
  (Patch5: evolution-data-server-1.3.5-nspr_fix.patch)

* Mon Jul 25 2005 David Malcolm <dmalcolm@redhat.com> - 1.3.5-1
- 1.3.5
- Split eds_major (was 1.2) into eds_base_version (1.4) and eds_api_version
  (1.2) to correspond to BASE_VERSION and API_VERSION in configure.in; updated
  rest of specfile accordingly.
- Removed upstreamed patch: 
  evolution-data-server-1.2.0-cope-with-a-macro-called-read.patch

* Wed Jun 27 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.2-4.fc5
- Added leak fixes for GNOME bug 309079 provided by Mark G. Adams

* Wed May 18 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.2-3
- bumped libsoup requirement to 2.2.3; removed mozilla_build_version, using
  pkg-config instead for locating NSPRS and NSS headers/libraries (#158085)

* Mon Apr 11 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.2-2
- added patch to calendar/libecal/e-cal.c to fix missing declaration of open_calendar

* Mon Apr 11 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.2-1
- 1.2.2

* Thu Mar 17 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.1-1
- 1.2.1

* Thu Mar 10 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.0-3
- Removed explicit run-time spec-file requirement on mozilla.
  The Mozilla NSS API/ABI stabilised by version 1.7.3
  The libraries are always located in the libdir
  However, the headers are in /usr/include/mozilla-%{mozilla_build_version}
  and so they move each time the mozilla version changes.
  So we no longer have an explicit mozilla run-time requirement in the specfile; 
  a requirement on the appropriate NSS and NSPR .so files is automagically generated on build.
  We have an explicit, exact build-time version, so that we can find the headers (without
  invoking an RPM query from the spec file; to do so is considered bad practice)
- Introduced mozilla_build_version, to replace mozilla_version
- Set mozilla_build_version to 1.7.6 to reflect current state of tree

* Tue Mar  8 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.0-2
- Added a patch to deal with glibc defining a macro called "read"

* Tue Mar  8 2005 David Malcolm <dmalcolm@redhat.com> - 1.2.0-1
- 1.2.0
- Removed patch for GCC 4 as this is now in upstream tarball

* Wed Mar  2 2005 Jeremy Katz <katzj@redhat.com> - 1.1.6-6
- rebuild to fix library linking silliness

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.6-5
- disabling gtk-doc on ia64 and s390x

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.6-4
- added macro use_gtk_doc; added missing BuildRequires on gtk-doc; enabled gtk-doc generation on all platforms (had been disabled on ia64)

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.6-3
- extended patch to deal with camel-groupwise-store-summary.c

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.6-2
- added patch to fix badly-scoped declaration of "namespace_clear" in camel-imap-store-summary.c

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.6-1
- 1.1.6

* Tue Feb  8 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.5-3
- rebuild

* Tue Feb  8 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.5-2
- forgot to fix sources

* Tue Feb  8 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.5-1
- 1.1.5

* Thu Jan 27 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.4.2-1
- Update from unstable 1.1.4.1 to unstable 1.1.1.4.2

* Wed Jan 26 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.4.1-3
- disable gtk-doc generation on ia64 for now

* Wed Jan 26 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.4.1-2
- Exclude ppc64 due to missing mozilla dependency

* Wed Jan 26 2005 David Malcolm <dmalcolm@redhat.com> - 1.1.4.1-1
- Update from 1.0.3 to 1.1.4.1
- Updated eds_major from 1.0 to 1.2; fixed translation search path.
- Removed 64-bit patch for calendar backend hash table; upstream now stores pointers to ECalBackendFactory, rather than GType
- Removed calendar optimisation patch for part of part of bug #141283 as this is now in the upstream tarball
- Added /usr/lib/evolution-data-server-%{eds_major} to cover the extensions, plus the camel code now in e-d-s, rather than evolution
- Added /usr/share/pixmaps/evolution-data-server-%{eds_major} to cover the category pixmaps
- Camel code from evolution is now in evolution-data-server:
  - Added camel-index-control and camel-lock-helper to packaged files
  - Added mozilla dependency code from the evolution package
  - Ditto for LDAP
  - Ditto for krb5
  - Ditto for NNTP support handling
  - Ditto for --enable-file-locking and --enable-dot-locking
- Added requirements on libbonobo, libgnomeui, gnome-vfs2, GConf2, libglade2
- Updated libsoup requirement from 2.2.1 to 2.2.2
- Enabled gtk-doc

* Wed Dec 15 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.3-2
- fixed packaging of translation files to reflect upstream change to GETTEXT_PACKAGE being evolution-data-server-1.0 rather than -1.5

* Wed Dec 15 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.3-1
- update from upstream 1.0.2 to 1.0.3:
  * Address Book
    - prevent e_book_commit_contact from crashing on multiple calls (Diego Gonzalez)
    - prevent file backend from crashing if uid of vcard is NULL (Diego Gonzalez)

  * Calendar
    #XB59904 - Speed up calendar queries (Rodrigo)
    #XB69624 - make changes in evo corresponding to soap schema changes  (Siva)
    - fix libical build for automake 1.9 (Rodney)
    - fix putenv usage for portability (Julio M. Merino Vidal)

  * Updated Translations:
    - sv (Christian Rose)

- Removed patches to fix build on x86_64 and calendar optimisation for XB59004 as these are in the upstream tarball

* Tue Dec  7 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.2-6
- Amortize writes to a local cache of a webcal calendar, fixing further aspect of #141283 (upstream bugzilla #70267), as posted to mailing list here:
http://lists.ximian.com/archives/public/evolution-patches/2004-December/008338.html
(The groupwise part of that patch did not cleanly apply, so I removed it).

* Thu Dec  2 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.2-5
- Added fix for #141283 (upstream bugzilla XB 59904), a backported calendar 
optimisation patch posted to upstream development mailing list here:
http://lists.ximian.com/archives/public/evolution-patches/2004-November/008139.html

* Wed Nov  3 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.2-4
- Added patch to fix usage of GINT_TO_POINTER/GPOINTER_TO_INT for calendar backend GType hash table, breaking on ia64  (#136914)

* Wed Oct 20 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.2-3
- added workaround for a backend leak that causes the "contacts" calendar 
backend to hold open an EBook for the local contacts (filed upstream at:
http://bugzilla.ximian.com/show_bug.cgi?id=68533 ); this was causing e-d-s to
never lose its last addressbook, and hence never quit.  We workaround this by
detecting this condition and exiting when it occurs, fixing bug #134851 and #134849.

* Tue Oct 12 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.2-2
- added patch to fix build on x86_64 (had multiple definitions of mutex code in libdb/dbinc.mutex.h)

* Tue Oct 12 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.2-1
- update from 1.0.1 to 1.0.2
- increased libsoup requirement to 2.2.1 to match configuration script

* Tue Sep 28 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.1-1
- update from 1.0.0 to 1.0.1
- removed patch that fixed warnings in calendar code (now in upstream tarball)

* Mon Sep 20 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.0-2
- fixed various warnings in the calendar code 
  (filed upstream here: http://bugzilla.ximian.com/show_bug.cgi?id=66383)

* Tue Sep 14 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.0-1
- update from 0.0.99 to 1.0.0
- changed path in FTP source location from 0.0 to 1.0

* Tue Aug 31 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.99-1
- update from 0.0.98 to 0.0.99
- increased libsoup requirement to 2.2.0 to match configuration script

* Mon Aug 16 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.98-1
- updated tarball from 0.0.97 to 0.0.98; updated required libsoup version to 2.1.13

* Thu Aug  5 2004 Warren Togami <wtogami@redhat.com> - 0.0.97-2
- pkgconfig -devel Requires libbonobo-devel, libgnome-devel

* Wed Aug  4 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.97-1
- upgraded to 0.0.97; rewrote the package's description
#'

* Mon Jul 26 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Tue Jul 20 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.96-2
- added version numbers to the BuildRequires test for libsoup-devel and ORBit2-devel

* Tue Jul 20 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.96-1
- 0.0.96; libsoup required is now 2.1.12

* Thu Jul  8 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Wed Jul  7 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Tue Jul  6 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.95-1
- 0.0.95

* Thu Jun 17 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.94.1-1
- 0.0.94.1

* Mon Jun  7 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.94-2
- rebuilt

* Mon Jun  7 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.94-1
- 0.0.94

* Wed May 26 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.93-4
- added ORBit2 requirement

* Fri May 21 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.93-3
- rebuild again

* Fri May 21 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.93-2
- rebuilt

* Thu May 20 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.93-1
- 0.0.93; libsoup required is now 2.1.10

* Wed Apr 21 2004 David Malcolm <dmalcolm@redhat.com> - 0.0.92-1
- Update to 0.0.92; added a define and a requirement on the libsoup version

* Wed Mar 10 2004 Jeremy Katz <katzj@redhat.com> - 0.0.90-1
- 0.0.90

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jan 26 2004 Jeremy Katz <katzj@redhat.com> - 0.0.6-1
- 0.0.6

* Wed Jan 21 2004 Jeremy Katz <katzj@redhat.com> - 0.0.5-2
- better fix by using system libtool

* Mon Jan 19 2004 Jeremy Katz <katzj@redhat.com> 0.0.5-1
- add some libdb linkage to make the build on x86_64 happy

* Wed Jan 14 2004 Jeremy Katz <katzj@redhat.com> 0.0.5-0
- update to 0.0.5

* Sat Jan  3 2004 Jeremy Katz <katzj@redhat.com> 0.0.4-0
- Initial build.

