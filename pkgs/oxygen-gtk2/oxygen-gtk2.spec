%global         momorel 1
%global         src_ver 1.4.5

Name:           oxygen-gtk2
Version:        %{src_ver}
Release:        %{momorel}m%{?dist}
Summary:        A port of the default KDE widget theme (Oxygen), to gtk2
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://www.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/stable/%{name}/%{src_ver}/src/%{name}-%{src_ver}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk2-devel
BuildRequires:  cairo-devel
BuildRequires:  kde-workspace-devel
Obsoletes:      oxygen-gtk

%description
Oxygen-Gtk2 is a port of the default KDE widget theme (Oxygen), to gtk2.

It's primary goal is to ensure visual consistency between gtk-based and qt-based applications running under KDE. 
A secondary objective is to also have a stand-alone nice looking gtk theme that would behave well on other Desktop 
Environments.

Unlike other attempts made to port the KDE oxygen theme to gtk, this attempt does not depend on Qt (via some Qt to 
Gtk conversion engine), nor does render the widget appearance via hard coded pixmaps, which otherwise breaks 
everytime some setting is changed in KDE.

%prep
%setup -q -n %{name}-%{src_ver}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast -C %{_target_platform} DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL README
%{_kde4_bindir}/oxygen-gtk-demo
%{_kde4_libdir}/gtk-2.0/*/engines/liboxygen-gtk.so
%{_kde4_datadir}/themes/oxygen-gtk

%changelog
* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Sun Feb  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3

* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Thu Dec 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Fri Aug 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Tue Apr 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2.1-1m)
- update to 1.3.2.1

* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sun Mar 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2.1-1m)
- update to 1.2.2-1

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1
- rename from oxygen-gtk to oxygen-gtk2

* Thu Dec 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Fri Nov 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Tue Oct 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Sun Aug 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Sat Jul 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for new GCC 4.6

* Sun Mar 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sat Feb 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Fri Jan 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Mon Dec 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- initial build for Momonga Linux
