%global momorel 9
Summary: Ng (Micro Nemacs)
Name: ng
Version: 1.4.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://tt.sakura.ne.jp/~amura/archives/ng/ng-%{version}.tar.gz
Source1: dot.ng
URL: http://tt.sakura.ne.jp/~amura/ng/
Patch0: http://tt.sakura.ne.jp/~amura/archives/ng/ng-1.4.4-replace_bug.patch
Patch10: ng-1.4.3-mkstemp.patch
Patch20: ng-1.4.4-typeahead.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0
BuildRequires: Canna-devel
Requires: Canna-library

%description
Ng (Micro Nemacs)

%prep
%setup -q
%patch0 -p0
%patch10 -p1
%patch20 -p1

%build
make -f sys/sysv/Makefile \
	CANNADEF="-DCANNA" \
	CANNALIB="-L/usr/lib -lcanna" \
	CANNAINC="-I/usr/include"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 ng %{buildroot}%{_bindir}/
mkdir -p %{buildroot}%{_datadir}/config-sample/ng
install -c -m644 %{SOURCE1} \
        %{buildroot}%{_datadir}/config-sample/ng

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING LICENSE docs/*
%{_bindir}/ng
%{_datadir}/config-sample/ng

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-4m)
- rebuild against gcc43

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.4-3m)
- rebuild against ncurses 5.3.

* Fri Mar 14 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.4-2m)
- apply an official patch to fix 'query-replace'

* Fri Mar  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.4-1m)

* Fri Oct 26 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.4.3-4k)
- add config-sample

* Mon Oct 22 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.4.3-2k)
