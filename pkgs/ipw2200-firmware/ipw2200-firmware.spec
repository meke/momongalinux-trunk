%global momorel 5

Summary: Firmware for Intel(R) PRO/Wireless 2200 network adaptors
Name: ipw2200-firmware
Version: 3.1
Release: %{momorel}m%{?dist}
License: "Redistributable, no modification permitted"
Group: System Environment/Kernel
URL: http://ipw2200.sourceforge.net/firmware.php
# License agreement must be displayed before download (referer protection)
Source0: ipw2200-fw-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
# This is so that the noarch packages only appears for these archs
ExclusiveArch: noarch %{ix86} x86_64

%description
This package contains the firmware files required by the ipw2200 driver for
Linux. Usage of the firmware is subject to the terms and conditions contained
in /lib/firmware/LICENSE.ipw2200. Please read it carefully.

%prep
%setup -q -n ipw2200-fw-%{version}

%build

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}/lib/firmware
# Terms state that the LICENSE *must* be in the same directory as the firmware
%{__install} -p -m 0644 *.fw %{buildroot}/lib/firmware/
%{__install} -p -m 0644 LICENSE.ipw2200-fw %{buildroot}/lib/firmware/

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc /lib/firmware/LICENSE.ipw2200-fw
/lib/firmware/*.fw

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-1m)
- sync with Fedora 11 (3.1-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-1m)
- import from fc to Momonga

* Tue Mar 20 2007 Matthias Saou <http://freshrpms.net> 3.0-9
- Add "noarch" to the ExclusiveArchs since plague chokes otherwise.

* Mon Mar  5 2007 Matthias Saou <http://freshrpms.net> 3.0-8
- Change group and license fields to reflect latest firmware guidelines.

* Mon Feb 26 2007 Matthias Saou <http://freshrpms.net> 3.0-7
- Move from a symlink to using a copy for the LICENSE (cleaner and easier).

* Sat Feb 24 2007 Matthias Saou <http://freshrpms.net> 3.0-6
- Fix group and license tags.
- Add (partially useful) exclusivearch.
- Quiet %%setup.

* Wed Feb 14 2007 Matthias Saou <http://freshrpms.net> 3.0-5
- Don't mark the LICENSE in /lib/firmware as %%doc since it could be excluded
  when using --excludedocs, symlink a file in %%doc to it instead.
- Remove 2.2 and 2.3 firmwares since only 2.4 and 3.0 are required for FC5+.

* Wed Feb 14 2007 Matthias Saou <http://freshrpms.net> 3.0-4
- Minor spec file cleanup for Fedora inclusion.

* Tue Oct 17 2006 Matthias Saou <http://freshrpms.net> 3.0-3
- Move the LICENSE as LICENSE.ipw2200 in the firmware directory to fully
  comply to the Intel redistribution terms and conditions.

* Sun Jun 25 2006 Matthias Saou <http://freshrpms.net> 3.0-2
- Fix inclusion of the 3.0 firmware files.

* Sat Jun 24 2006 Dag Wieers <dag@wieers.com> - 3.0-1
- Updated to release 3.0.

* Mon Jan  2 2006 Matthias Saou <http://freshrpms.net> 2.4-2
- Convert spec file to UTF-8.

* Thu Oct 27 2005 Matthias Saou <http://freshrpms.net> 2.4-1
- Update to 2.4, but keep 2.2 and 2.3 included too.

* Tue May 31 2005 Matthias Saou <http://freshrpms.net> 2.3-2
- Also bundle 2.2 firmware : The recent driver downgrade required this :-/

* Wed May 25 2005 Matthias Saou <http://freshrpms.net> 2.3-1
- Update to 2.3, required by latest FC4 dev and 2.6.12rc.

* Thu Apr 21 2005 Matthias Saou <http://freshrpms.net> 2.2-3
- Remove all symlinks, the only useful location is /lib/firmware now.
- No longer rename all firmware files, recent ipw2200 modules expect the
  default names now (tested w/ FC3 2.6.11 updates and FC4test).

* Thu Feb 17 2005 Matthias Saou <http://freshrpms.net> 2.2-2
- Rename all files from ipw-2.2-* to ipw2200_* as required.

* Wed Feb  9 2005 Matthias Saou <http://freshrpms.net> 2.2-1
- Update to 2.2, required by latest FC kernels.

* Wed Nov  3 2004 Matthias Saou <http://freshrpms.net> 2.0-1
- Update to 2.0.
- Now put the files in /lib/firmware and symlinks in other dirs.

* Wed Jun 16 2004 Matthias Saou <http://freshrpms.net> 1.2-1
- Initial RPM release, based on the ipw2100-firmware spec file.

