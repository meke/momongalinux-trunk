%global momorel 1

Name: phpMyAdmin
Version: 4.0.5
Release: %{momorel}m%{?dist}
Summary: Web based MySQL browser written in php

Group:	Applications/Internet
License: GPLv2+
URL: http://www.phpmyadmin.net/	
Source0: http://dl.sourceforge.net/sourceforge/phpmyadmin/%{name}-%{version}-all-languages.tar.bz2
NoSource: 0
Source1: phpMyAdmin-config.inc.php
Source2: phpMyAdmin.htaccess
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

Requires: webserver 
Requires: php >= 5.4.1
Requires: php-mysql  >= 5.4.1
Requires: php-mbstring >= 5.4.1
Requires(postun): initscripts
Requires(post): initscripts

%description
phpMyAdmin is a tool written in PHP intended to handle the administration of
MySQL over the Web. Currently it can create and drop databases,
create/drop/alter tables, delete/edit/add fields, execute any SQL statement,
manage keys on fields, manage privileges,export data into various formats and
is available in 50 languages

%prep
%setup -qn phpMyAdmin-%{version}-all-languages

%install
rm -rf %{buildroot}
%{__mkdir} -p %{buildroot}/%{_datadir}/%{name}
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/httpd/conf.d/
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/%{name}
%{__cp} -ad ./* %{buildroot}/%{_datadir}/%{name}
%{__cp} %{SOURCE2} %{buildroot}/%{_sysconfdir}/httpd/conf.d/phpMyAdmin.conf.dist
%{__cp} %{SOURCE1} %{buildroot}/%{_sysconfdir}/%{name}/config.inc.php
ln -s %{_sysconfdir}/%{name}/config.inc.php %{buildroot}/%{_datadir}/%{name}/config.inc.php

%{__rm} -f %{buildroot}/%{_datadir}/%{name}/*txt
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/[IRLT]*
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/CREDITS
%{__rm} -f %{buildroot}/%{_datadir}/%{name}/libraries/.htaccess

%clean
rm -rf %{buildroot}

%post
/sbin/service httpd condrestart > /dev/null 2>&1 || :

%postun
/sbin/service httpd condrestart > /dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc ChangeLog README* LICENSE RELEASE-DATE-%{version}
%{_datadir}/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/phpMyAdmin.conf.dist
%config(noreplace) %{_sysconfdir}/%{name}

%changelog
* Tue Aug  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.5-1m)
- [SECURITY] CVE-2013-5029
- update to 4.0.5

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4.2-1m)
- [SECURITY] CVE-2013-4995 PMASA-2013-15
- update to 4.0.4.2

* Wed Jul 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4.1-1m)
- [SECURITY] CVE-2013-3742 CVE-2013-4729
- update to 4.0.4.1

* Thu May  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8.1-1m)
- [SECURITY] CVE-2013-1937
- update to 3.5.8.1

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to 3.5.8

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.3-1m)
- [SECURITY] CVE-2012-5339 CVE-2012-5368
- update to 3.5.3

* Wed Aug 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.2.2-1m)
- [SECURITY] CVE-2012-4219 CVE-2012-4345 CVE-2012-4579
- update to 3.5.2.2

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.2.1-1m)
- [SECURITY] CVE-2012-4219
- update to 3.5.2.1

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-2m)
-rebuild against php-5.4.1

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- [SECURITY] CVE-2012-1902
- update to 3.5.0

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.9-1m)
- [SECURITY] CVE-2011-4780 CVE-2011-4782
- update to 3.4.9

* Tue Dec  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.8-1m)
- [SECURITY] CVE-2011-3646 CVE-2011-4064 (fixed in 3.4.6)
- [SECURITY] CVE-2011-4107 (fixed in 3.4.7.1)
- [SECURITY] CVE-2011-4634 (fixed in 3.4.8)
- update to 3.4.8

* Sun Sep 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.5-1m)
- [SECURITY] PMASA-2011-14
- update to 3.4.5

* Fri Aug 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.4-1m)
- [SECURITY] CVE-2011-3181
- update to 3.4.4

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.3.2-1m)
- [SECURITY] CVE-2011-2642 CVE-2011-2643 CVE-2011-2718 CVE-2011-2719
- update to 3.4.3.2

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-1m)
- [SECURITY] CVE-2011-1940 CVE-2011-1941
- CVE-2011-1941 affects only version 3.4.0
- update to 3.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.10-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.10-1m)
- update to 3.3.10

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.9.2-1m)
- [SECURITY] CVE-2011-0986 CVE-2011-0987
- update to 3.3.9.2

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.9-1m)
- [SECURITY] CVE-2010-4480 CVE-2010-4481
- update to 3.3.9

* Tue Nov 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.8.1-1m)
- [SECURITY] CVE-2010-4329
- update to 3.3.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.8-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.8-1m)
- update to 3.3.8 (bug fix version)

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-1m)
- [SECURITY] CVE-2010-3263
- update to 3.3.7

* Wed Sep  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.6-1m)
- [SECURITY] CVE-2010-2958
- update to 3.3.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.5.1-2m)
- full rebuild for mo7 release

* Sat Aug 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.5.1-1m)
- [SECURITY] CVE-2010-3056
- update to 3.3.5.1

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.5-1m)
- update to 3.3.5

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.4-1m)
- update to 3.3.4

* Sun Jun 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2.1-1m)
- [SECURITY] CVE-2009-3696 CVE-2009-3697
- update to 3.2.2.1

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0.1-1m)
- [SECURITY] CVE-2009-2284
- update to 3.2.0.1

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Apr 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.3.2-1m)
- [SECURITY] CVE-2009-1285
- update to 3.1.3.2

* Fri Mar 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.3.1-1m)
- [SECURITY] CVE-2009-1148 CVE-2009-1149 CVE-2009-1150 CVE-2009-1151
- update to 3.1.3.1

* Sun Feb 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.2-1m)
- [SECURITY] CVE-2008-7251 CVE-2008-7252 CVE-2009-4605
- update to 3.1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.9.4-2m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.9.4-1m)
- [SECURITY] CVE-2008-5621 CVE-2008-5622
- update to 2.11.9.4

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.9.3-1m)
- [SECURITY] CVE-2008-4775
- update to 2.11.9.3

* Wed Sep 24 2008 NARITA Koichi <pulsar@momonga-linua.org>
- (2.11.9.2-1m)
- [SECURITY] CVE-2008-4326
- [SECURITY] PMASA-2008-8, see http://www.phpmyadmin.net/home_page/security.php?issue=PMASA-2008-8
- update to 2.11.9.2

* Wed Sep 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.9.1-1m)
- [SECURITY] CVE-2008-4096
- [SECURITY] PMASA-2008-7, see http://www.phpmyadmin.net/home_page/security.php?issue=PMASA-2008-7
- update to 2.11.9.1

* Wed Aug  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.8-1m)
- [SECURITY] CVE-2008-3456 CVE-2008-3457
- update to 2.11.8

* Tue May  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.6-1m)
- [SECURITY] CVE-2008-1924 was fixed in 2.11.5.2
- update to 2.11.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.5.1-2m)
- rebuild against gcc43

* Sun Mar 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.5.1-1m)
- update to 2.11.5.1

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.5-1m)
- update to 2.11.5

* Sun Nov 25 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.11.2.2-1m)
- import from Fedora

* Wed Nov 21 2007 Robert Scheck <robert@fedoraproject.org> 2.11.2.2-1
- Upstream released 2.11.2.2 (#393771)

* Tue Nov 20 2007 Mike McGrath <mmcgrath@redhat.com> 2.11.2.1-1
- Upstream released new version

* Fri Oct 29 2007 Mike McGrath <mmcgrath@redhat.com> 2.11.2-1
* upstream released new version

* Mon Oct 22 2007 Mike McGrath <mmcgrath@redhat.com> 2.11.1.2-1
* upstream released new version

* Thu Sep 06 2007 Mike McGrath <mmcgrath@redhat.com> 2.11.0-1
- Upstream released new version
- Altered sources file as required
- Added proper license

* Mon Jul 23 2007 Mike McGrath <mmcgrath@redhat.com> 2.10.3-1
- Upstream released new version

* Sat Mar 10 2007 Mike McGrath <mmcgrath@redhat.com> 2.10.0.2-3
- Switched to the actual all-languages, not just utf-8

* Sun Mar 04 2007 Mike McGrath <mmcgrath@redhat.com> 2.10.0.2-1
- Upstream released new version

* Sat Jan 20 2007 Mike McGrath <imlinux@gmail.com> 2.9.2-1
- Upstream released new version

* Fri Dec 08 2006 Mike McGrath <imlinux@gmail.com> 2.9.1.1-2
- Fixed bug in spec file

* Fri Dec 08 2006 Mike McGrath <imlinux@gmail.com> 2.9.1.1-1
- Upstream released new version

* Wed Nov 15 2006 Mike McGrath <imlinux@gmail.com> 2.9.1-3alpha
- Added dist tag

* Wed Nov 15 2006 Mike McGrath <imlinux@gmail.com> 2.9.1-2alpha
- Fixed 215159

* Fri Nov 10 2006 Mike McGrath <imlinux@gmail.com> 2.9.1-1alpha
- Added alpha tag since this is a release candidate

* Tue Nov 07 2006 Mike McGrath <imlinux@gmail.com> 2.9.1-1
- Upstream released new version

* Wed Oct 04 2006 Mike McGrath <imlinux@gmail.com> 2.9.0.2-1
- Upstream released new version

* Thu Jul 06 2006 Mike McGrath <imlinux@gmail.com> 2.8.2-2
- Fixed a typo in the Apache config

* Mon Jul 03 2006 Mike McGrath <imlinux@gmail.com> 2.8.2-1
- Upstream released 2.8.2
- Added more restrictive directives to httpd/conf.d/phpMyAdmin.conf
- removed htaccess file from the libraries dir
- Specific versions for various requires

* Sat May 13 2006 Mike McGrath <imlinux@gmail.com> 2.8.0.4-1
- Upstream released 2.8.0.4
- Added requires php, instead of requires httpd, now using webserver

* Sun May 07 2006 Mike McGrath <imlinux@gmail.com> 2.8.0.3-2
- Added mysql-php and php-mbstring as a requires

* Thu Apr 07 2006 Mike McGrath <imlinux@gmail.com> 2.8.0.3-1
- Fixed XSS vulnerability: PMASA-2006-1
- It was possible to conduct an XSS attack with a direct call to some scripts
- under the themes directory.

* Tue Apr 04 2006 Mike McGrath <imlinux@gmail.com> 2.8.0.2-3
- Made config files actually configs
- Moved doc files to the doc dir

* Tue Apr 04 2006 Mike McGrath <imlinux@gmail.com> 2.8.0.2-2
- Moved everything to %{_datadir}
- Moved config file to /etc/
- Used description from phpMyAdmin project info

* Mon Apr 03 2006 Mike McGrath <imlinux@gmail.com> 2.8.0.2-1
- Initial Spec file creation for Fedora
