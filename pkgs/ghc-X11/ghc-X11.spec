%global momorel 6

%global pkg_name X11

%bcond_without doc
%bcond_without prof

# ghc does not emit debug information
%global debug_package %{nil}

Name:           ghc-%{pkg_name}
Version:        1.4.5
Release:        %{momorel}m%{?dist}
Summary:        Haskell %{pkg_name} library

Group:          Development/Libraries
License:        BSD
URL:            http://hackage.haskell.org/cgi-bin/hackage-scripts/package/%{pkg_name}
Source0:        http://hackage.haskell.org/packages/archive/%{pkg_name}/%{version}/%{pkg_name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# fedora ghc archs:
ExclusiveArch:  %{ix86} x86_64 ppc alpha
BuildRequires:  ghc, ghc-rpm-macros
%if %{with doc}
BuildRequires:  ghc-doc
%endif
%if %{with prof}
BuildRequires:  ghc-prof
%endif
BuildRequires:  libXinerama-devel, libX11-devel, libXext-devel

%description
This package provides the Haskell %{pkg_name} library for ghc.

The library consists of bindings to direct translations of the C bindings; for
documentation of these calls, refer to "The Xlib Programming
Manual", available online at <http://tronche.com/gui/x/xlib/>.

%package devel
Summary:        Haskell %{pkg_name} library
Group:          Development/Libraries
Requires:       ghc = %{ghc_version}
Requires(post): ghc = %{ghc_version}
Requires(preun): ghc = %{ghc_version}
Requires:       libXinerama-devel, libX11-devel, libXext-devel

%description devel
This package contains the development files for %{name}
built for ghc-%{ghc_version}.


%if %{with doc}
%package doc
Summary:        Documentation for %{name}
Group:          Development/Libraries
Requires:       ghc-doc = %{ghc_version}
Requires(post): ghc-doc = %{ghc_version}
Requires(postun): ghc-doc = %{ghc_version}

%description doc
This package contains development documentation files for the %{name} library.
%endif


%if %{with prof}
%package prof
Summary:        Profiling libraries for %{name}
Group:          Development/Libraries
Requires:       %{name}-devel = %{version}-%{release}
Requires:       ghc-prof = %{ghc_version}

%description prof
This package contains profiling libraries for %{name}
built for ghc-%{ghc_version}.
%endif


%prep
%setup -q -n %{pkg_name}-%{version}


%build
%cabal_configure --ghc %{?with_prof:-p}
%cabal build
%if %{with doc}
%cabal haddock
%endif
%ghc_gen_scripts


%install
rm -rf $RPM_BUILD_ROOT
%cabal_install
%ghc_install_scripts
%ghc_gen_filelists %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%post devel
%ghc_register_pkg


%if %{with doc}
%post doc
%ghc_reindex_haddock
%endif


%preun devel
if [ "$1" -eq 0 ] ; then
  %ghc_unregister_pkg
fi


%if %{with doc}
%postun doc
if [ "$1" -eq 0 ] ; then
  %ghc_reindex_haddock
fi
%endif


%files devel -f %{name}-devel.files
%defattr(-,root,root,-)
%{_docdir}/%{name}-%{version}


%if %{with doc}
%files doc -f %{name}-doc.files
%defattr(-,root,root,-)
%endif


%if %{with prof}
%files prof -f %{name}-prof.files
%defattr(-,root,root,-)
%endif


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.5-2m)
- rebuild against ghc-6.0.4
- memo: ghc_version is dedined /etc/rpm/macros.ghc provided by ghc-rpm-macros

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-1m)
- import from Fedora 11

* Sat May 16 2009 Jens Petersen <petersen@redhat.com> - 1.4.5-11
- buildrequires ghc-rpm-macros (cabal2spec-0.16)

* Fri Apr 24 2009 Jens Petersen <petersen@redhat.com> - 1.4.5-10
- ghc_version is now in macros.ghc

* Fri Apr 24 2009 Jens Petersen <petersen@redhat.com> - 1.4.5-9
- try defining ghc_version with global

* Fri Apr 24 2009 Jens Petersen <petersen@redhat.com> - 1.4.5-8
- define ghc_version correctly

* Mon Apr 20 2009 Jens Petersen <petersen@redhat.com> - 1.4.5-7
- rebuild with ghc-6.10.2
- update to latest cabal2spec template and macros.ghc
  - pkg_libdir and pkg_docdir moved to macros.ghc
  - drop ghc_version from buildrequires
  - fix prof configure
  - add doc filelist
  - get ghc_version from ghc

* Sat Apr  4 2009 Yaakov M. Nemoy <yankee@localhost.localdomain> - 1.4.5-6
- rebuild bump to raise EVR manually, to match with F-10 branch

* Sun Mar  8 2009 Yaakov M. Nemoy <yankee@localhost.localdomain> - 1.4.5-5
- corrected a faulty tag

* Sun Mar  8 2009 Yaakov M. Nemoy <yankee@localhost.localdomain> - 1.4.5-4
- forgot to include the right arch tags

* Sat Feb 28 2009 Jens Petersen <petersen@redhat.com> - 1.4.5-3
- sync with cabal2spec-0.12:
- improve requires

* Mon Feb 23 2009 Yaakov M. Nemoy <loupgaroublond@gmail.com> - 1.4.5-2
- updated template to new guidelines

* Mon Jan  5 2009 Yaakov M. Nemoy <loupgaroublond@gmail.com> - 1.4.5-1
- initial packaging for Fedora created by cabal2spec
- added description and build requires
- altered license from template
