%global momorel 6

Summary: Utility for visualizing block layer IO patterns and performance
Name: seekwatcher
Version: 0.12
Release: %{momorel}m%{?dist}
License: GPLv2
BuildArch: noarch
Group: Development/System
Source: http://oss.oracle.com/~mason/seekwatcher/seekwatcher-%{version}.tar.bz2
NoSource: 0
Url: http://oss.oracle.com/~mason/seekwatcher/
Requires: blktrace, python, python-matplotlib, theora-tools
BuildRequires: python

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Seekwatcher generates graphs from blktrace runs to help visualize IO patterns
and performance. It can plot multiple blktrace runs together, making it easy
to compare the differences between different benchmark runs.

You should install the seekwatcher package if you need to visualize detailed
information about IO patterns.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
cp -p seekwatcher %{buildroot}%{_bindir}
chmod +x %{buildroot}%{_bindir}/seekwatcher

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.html COPYING
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-2m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-1m)
- import from Fedora-devel to Momonga

* Wed Jun 04 2008 Eric Sandeen <sandeen@redhat.com> - 0.12-1
- New upstream version, fixes IO plots for high block ranges.

* Mon Mar 14 2008 Eric Sandeen <sandeen@redhat.com> - 0.11-1
- New upstream version, includes support for multiple devices.

* Fri Mar 07 2008 Eric Sandeen <sandeen@redhat.com> - 0.10-1
- New upstream version, includes filtering for fewer dropped events.

* Fri Nov 30 2007 Eric Sandeen <sandeen@redhat.com> - 0.9-2
- More specfile fiddling

* Thu Nov 29 2007 Eric Sandeen <sandeen@redhat.com> - 0.8-2
- Add python to reqs/buildreqs

* Thu Nov 29 2007 Eric Sandeen <sandeen@redhat.com> - 0.8-1
- Update to 0.8, includes support for theora movies

* Mon Sep 10 2007 Eric Sandeen <sandeen@redhat.com> - 0.7-1
- New package, initial build.
