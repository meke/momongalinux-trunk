%global momorel 4
#%%global _numjobs 1
#%%global srcname %{name}_%{version}-1
#%%global srcname %{name}-%{version}.src

Summary: Data Management API runtime environment.
Name: dmapi
Version: 2.2.12
Release: %{momorel}m%{?dist}
# Copyright: Copyright (C) 2000 Silicon Graphics, Inc.
License: GPL
Group: System Environment/Base
URL: http://oss.sgi.com/projects/xfs/
Source0: ftp://oss.sgi.com/projects/xfs/cmd_tars/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: xfsprogs-devel >= 3.0.0-1m

%description
Files required by system software using the Data Management API
(DMAPI).  This is used to implement the interface defined in the
X/Open document:  Systems Management: Data Storage Managment
(XDSM) API dated February 1997.  This interface is implemented
by the libdm library.

%package devel
Summary: Data Management API static libraries and headers.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
dmapi-devel contains the libraries and header files needed to
develop programs which make use of the Data Management API
(DMAPI).  If you install dmapi-devel, you'll also want to install
the dmapi (runtime) package and the xfsprogs-devel package.
#'

# If .census exists, then no setup is necessary, just go and do the build,
# otherwise run setup
%prep
%setup -q

%build
#export LOCAL_CONFIGURE_OPTIONS="--libdir=/%{_libdir} --libexecdir=%{_libdir}"
autoreconf -ivf
%configure \
    --disable-static \
    --libdir=%{_libdir}
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
DIST_ROOT="%{buildroot}"
DIST_INSTALL=`pwd`/install.manifest
DIST_INSTALL_DEV=`pwd`/install-dev.manifest
export DIST_ROOT DIST_INSTALL DIST_INSTALL_DEV
/usr/bin/make install DIST_MANIFEST="$DIST_INSTALL"
/usr/bin/make -C build/rpm rpmfiles DIST_MANIFEST="$DIST_INSTALL"
/usr/bin/make install-dev DIST_MANIFEST="$DIST_INSTALL_DEV"
/usr/bin/make -C build/rpm rpmfiles-dev DIST_MANIFEST="$DIST_INSTALL_DEV"

# nuke .la files, etc
#rm -f $RPM_BUILD_ROOT/{%{_lib}/*.{la,a,so},%{_libdir}/*.la}
rm -f $RPM_BUILD_ROOT/{%{_lib}/*.la,%{_libdir}/*.{la,a}} $RPM_BUILD_ROOT/{lib/*.la,%{_prefix}/lib/*.la} $RPM_BUILD_ROOT/%{_libexecdir}/*.{la,so}

# (sb) installed but unpackaged files
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/dmapi

rm -f $RPM_BUILD_ROOT/%{_libdir}/*.a
#mv $RPM_BUILD_ROOT/%{_lib}/*.{a,so} $RPM_BUILD_ROOT/%{_libdir}/
rm -f $RPM_BUILD_ROOT/%{_lib}/*.{a,so}

#mv  $RPM_BUILD_ROOT/%{_lib}/* $RPM_BUILD_ROOT/%{_libdir}/

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc doc/* README
%attr(0755,root,root) %{_libdir}/*.so.*

%files devel
%defattr(-, root, root, 0755)
%{_libdir}/*.so
%{_mandir}/man3/*
%{_includedir}/xfs/*

%changelog
* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.12-4m)
- disable static library

* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.12-3m)
- remove "make configure". use autoreconf

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.12-2m)
- support UserMove env

* Wed Jan 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.12-1m)
- update 2.2.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.10-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.10-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.10-2m)
- use BuildRequires

* Wed Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10
- dmapi could be build without removing xfsdump

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May  8 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (2.2.9-2m)
- modifiy %%files for x86_84
- new source location
- update sha256 checksum

* Sat Feb  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.9-1m)
- update to 2.2.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.8-5m)
- rebuild against rpm-4.6

* Sun Apr 06 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.8-4m)
- update spec for x86_64 removing Patch0: dmapi-2.2.8-lib64.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.8-3m)
- rebuild against gcc43

* Fri Mar 30 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.8-2m)
- add Patch0 dmapi-2.2.8-lib64.patch

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.8-1m)
- update to 2.2.8

* Fri Dec 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.5-4m)
- rebuild against xfsprogs 2.8.18-1m
- chmod 0755

* Mon Oct 30 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.2.5-3m)
- add LOCAL_CONFIGURE_OPTIONS for lib64

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.5-2m)
- fix defattr-ken
- fix duplicate directory with xfsprogs problem

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.5-1m)
- update 2.2.5

* Sun May 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.3-1m)
- update 2.2.3

* Wed Dec 28 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.1-4m)
- no NoSource

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.1-3m)
- enable x86_64.

* Thu Oct 28 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.1-2m)
  fix patch reject

* Wed Oct 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.1-1m)
  update to 2.2.1

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-1m)
- verup

* Wed Mar 17 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.0-2m)
- revised spec for enabling rpm 4.2.

* Wed Dec  3 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.8-1m)
- update to 2.0.8

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (2.0.5-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Sep  3 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.5-1m)
  update to 2.0.5

* Wed Jul  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.2-4m)
- devel packages requires '%{name} = %{version}-%{release}'

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.0.2-4k)
- omit /sbin/ldconfig in PreReq.

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.0.2-2k)
- update to 2.0.2

* Sun Mar 17 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.0-2k)
- update to 2.0.0

* Thu Nov  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.2-4k)
- modify source URL

* Fri Sep 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.2-2k)
- First Kondarization 
