%global momorel 7

Summary: A set of audio plugins for LADSPA by Fons Adriaensen.
Name: ladspa-fil-plugins
Version: 0.3.0
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kokkinizita.linuxaudio.org/linuxaudio/
Group: Development/Libraries
Source0:  http://kokkinizita.linuxaudio.org/linuxaudio/downloads/FIL-plugins-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel

%description
There's one plugin in this first release, a four-band parametric equaliser. Each section has an active/bypass switch, frequency, bandwidth and gain controls. There is also a global bypass switch and gain control.

#'
%prep
%setup -q -n FIL-plugins-%{version}

%build
%make

%install
install -d %{buildroot}%{_libdir}/ladspa
install -m 755 *.so   %{buildroot}%{_libdir}/ladspa

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Sun May 29 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.0-7m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.0-1m)
- Initial Build for Momonga Linux
