%global momorel 1
%global srcname Argyll
%global coname argyll
%global udevrulesdir /lib/udev/rules.d

Summary: ICC compatible color management system
Name: argyllcms
Version: 1.5.1
Release: %{momorel}m%{?dist}
License: AGPLv3+ and GPLv2+ and MIT
URL: http://www.argyllcms.com/
Group: User Interface/X
Source0: http://www.argyllcms.com/%{srcname}_V%{version}_src.zip
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: color-filesystem
Requires: systemd
BuildRequires: jam
BuildRequires: libX11-devel
BuildRequires: libgudev1-devel
BuildRequires: libjpeg-devel
BuildRequires: libtiff-devel
BuildRequires: libtool
BuildRequires: systemd-devel
BuildRequires: unzip
BuildRequires: xorg-x11-proto-devel

%description
The Argyll color management system supports accurate ICC profile creation for
scanners, CMYK printers, film recorders and calibration and profiling of
displays.

Spectral sample data is supported, allowing a selection of illuminants observer
types, and paper fluorescent whitener additive compensation. Profiles can also
incorporate source specific gamut mappings for perceptual and saturation
intents. Gamut mapping and profile linking uses the CIECAM02 appearance model,
a unique gamut mapping algorithm, and a wide selection of rendering intents. It
also includes code for the fastest portable 8 bit raster color conversion
engine available anywhere, as well as support for fast, fully accurate 16 bit
conversion. Device color gamuts can also be viewed and compared using a VRML

%prep
%setup -q -n %{srcname}_V%{version}

%build
jam  -fJambase %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
jam -q -fJambase install

# preparing
mkdir -p %{buildroot}%{udevrulesdir}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/color/%{coname}

rm -f bin/License.txt

# install
install -m 755 bin/* %{buildroot}%{_bindir}/
install -m 644 ref/*  %{buildroot}%{_datadir}/color/%{coname}/
install -m 644 usb/55-%{srcname}.rules %{buildroot}%{udevrulesdir}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc Readme.txt doc/*
%{udevrulesdir}/55-%{srcname}.rules
%{_bindir}/*
%{_datadir}/color/%{coname}

%changelog
* Fri Jul  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- initial package for Momonga Linux
- import some parts of spec from opensuse