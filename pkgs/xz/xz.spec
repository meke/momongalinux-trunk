%global _binary_payload w9.gzdio
%global src_name xz-5.1.3alpha
%global momorel 1

Summary: 	XZ utils
Name: 		xz
Version: 	5.1.3
Release: 	%{momorel}m%{?dist}
License: 	GPLv3+
Group:		Applications/File
#Source0:	http://tukaani.org/%{name}/%{name}-%{version}alpha.tar.bz2
Source0:	http://tukaani.org/%{name}/%{src_name}.tar.xz
NoSource:	0

URL:		http://tukaani.org/%{name}/
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	%{name}-libs	= %{version}-%{release}
Provides:	lzma
Obsoletes:	lzma

%description
LZMA provides very high compression ratio and fast decompression. The
core of the LZMA utils is Igor Pavlov's LZMA SDK containing the actual
LZMA encoder/decoder. XZ utils add a few scripts which provide
gzip-like command line interface and a couple of other LZMA related
tools. 

%package 	libs
Summary:	Libraries for decoding XZ compression
Group:		System Environment/Libraries
License:	LGPLv2+
Provides:	lzma-libs
Obsoletes:	lzma-libs

%description 	libs
Libraries for decoding LZMA compression.

%package 	devel
Summary:	Devel libraries & headers for liblzmadec
Group:		Development/Libraries
License:	LGPLv2+
Requires:	%{name}-libs	= %{version}-%{release}
Provides:	lzma-devel
Obsoletes:	lzma-devel

%description  devel
Devel libraries & headers for liblzmadec.

%package static
Summary: Static libraries for liblzmadec
Group: Development/Libraries
License: LGPLv2+
Requires: %{name}-devel = %{version}-%{release}
Provides:	lzma-static
Obsoletes:	lzma-static

%description static
The zlib-static package includes static libraries needed
to develop programs that use the zlib compression and
decompression library.

%prep
#%%setup -q
%setup -q -n %{src_name}

%build

CFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64" \
CXXFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64" \
# autoreconf -ivf
%configure
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"
rm -f %{buildroot}/%{_libdir}/*.la
rm -rf %{buildroot}/%{_datadir}/doc/xz

%{find_lang} %{name}

%check
LD_PRELOAD=%{buildroot}%{_libdir}/liblzma.so.5
export LD_PRELOAD
make check

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README THANKS ChangeLog AUTHORS NEWS COPYING COPYING.GPLv2 COPYING.GPLv3
%doc doc/faq.txt doc/lzma-file-format.txt doc/xz-file-format.txt
%{_bindir}/*
%{_mandir}/man1/*

%files libs
%defattr(-,root,root,-)
%doc COPYING.LGPLv2.1
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/lzma.h
%{_includedir}/lzma/*.h
%{_libdir}/pkgconfig/liblzma.pc
%{_libdir}/*.so

%files static
%defattr(-,root,root,-)
%{_libdir}/liblzma.a

%changelog
* Thu Nov 28 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1.3-1m)
- update to 5.1.3

* Thu Sep 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.1.2-1m)
- update to 5.1.2

* Sat Jun 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.4-1m)
- update to 5.0.4

* Sun May 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.3-1m)
- update to 5.0.3

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.2-1m)
- update to 5.0.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-2m)
- rebuild for new GCC 4.6

* Tue Feb  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.1-1m)
- update to 5.0.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.0-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.0-1m)
- update to 5.0.0
- set NoSource: 0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.999.9beta-6m)
- full rebuild for mo7 release

* Sun Aug  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.9beta-5m)
- update 4.999.9beta-20100727

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.9beta-4m)
- update 4.999.9beta-20100615

* Wed Jun  2 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.999.9beta-3m)
- update 4.999.9beta-20100601

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.999.9beta-2m)
- update 4.999.9beta-20100331

* Tue Apr  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.999.9beta-1m)
- rename lzma to xz

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.999.9beta-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.9beta-1m)
- update 4.999.9beta

* Fri Feb 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.8beta-1m)
- update 4.999.8beta

* Sat Jan 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.7beta-1m)
- %%global _binary_payload w9.gzdio
-- always gzip-payload

* Thu Jan 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.7beta-1m)
- update 4.999.7beta
- source rename to xz

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.32.7-4m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.32.7-2m)
- create static library package
- License: GPLv3+

* Wed Dec 31 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.999.6alpha-1m)
- update 4.999.6alpha

* Thu Dec 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.999.5alpha-1m)
- update 4.999.5alpha

* Thu Aug  7 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.32.7-1m)
- update 4.32.7

* Fri May 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.32.6-1m)
- update 4.32.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.32.5-2m)
- rebuild against gcc43

* Mon Jan 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.32.5-1m)
- update 4.32.5
- delete included patch for gcc-4.3

* Thu Jan 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.32.4-1m)
- update 4.32.4

* Wed Jan  9 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.32.2-2m)
- add patch for gcc43

* Wed Jan  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.32.2-1m)
- Initial commit Momonga Linux

* Sat Oct 27 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.2-2
- Forgot to upload new sources, bumping...

* Sat Oct 27 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.2-1
- 'make tag' problems, bumping...

* Sat Oct 27 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.2-0
- Switch to version 4.32.2

* Tue Aug 7 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.0-0.6.beta5
- More clean-up in spec and use beta5 release

* Tue Aug 7 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.0-0.5.beta3
- More clean-up in spec

* Thu Jul 25 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.0-0.4.beta3
- Add COPYING, remove *.a and *.la file, make description shorter

* Thu Jul 19 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.0-0.3.beta3
- Add dist and _smp_mflags

* Tue Jul 17 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.0-0.2.beta3
- Clean-up in spec.

* Sun Jul 15 2007 Per Patrice Bouchand <patrice.bouchand.fedora<at>gmail.com> 4.32.0-0.1.beta3
- Initial Fedora release (inspired by mandriva spec)

