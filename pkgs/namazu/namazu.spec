%global momorel 1
%global _contentdir	/var/www

Summary: Namazu is a full-text search engine
Name: namazu
Version: 2.0.21
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
Source0: http://www.namazu.org/stable/namazu-%{version}.tar.gz
NoSource: 0
Source1: http://www.cwo.zaq.ne.jp/vine-linux/text/TCL-scroll.txt
Source2: namazu-momonga.tar.bz2
Source3: tknamazurc.momonga
Source4: dw3m
Source5: tknamazu.desktop
Patch1: tknamazu.tcl.selall.patch
Patch2: tknamazu.tcl.wheel.patch
Patch3: tknamazu.tcl.w3m.patch
Patch4: dyky.w3m_4_configure.patch
Patch10: mcHT.tknamazu-enable-bz2.patch
#Patch12: namazu-2.0.10-tdiary.pl.patch
#Patch13: namazu-2.0.10-recursive_filter.patch
Patch20: namazu-install-data-local.patch
URL: http://www.namazu.org/
BuildRequires: perl >= 5.005, perl-NKF >= 1.92-12k, perl-Text-Kakasi >= 1.05
BuildRequires: perl-File-MMagic >= 1.13, perl-Text-ChaSen >= 1.03
BuildRequires: tcl, tk
Requires: perl >= 5.005, perl-File-MMagic >= 1.13, perl-NKF >= 1.92-12k
Requires: kakasi >= 2.3.3, perl-Text-Kakasi >= 1.05
BuildRequires: w3m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Namazu is a full-text search engine software intended for easy use.
Not only it works as CGI program for small or medium scale WWW
search engine, but also works as personal use such as search system
for local HDD.

%package devel
Summary: Libraries and include files of Namazu
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and include files of Namazu.

%package cgi
Summary: CGI interface of Namazu
Group: Applications/Text
Requires: webserver, %{name} = %{version}-%{release}

%description cgi
namazu-cgi package provides a CGI interface of Namazu.
Try http://localhost/cgi-bin/namazu.cgi

%package tk
Summary: Tk interface of Namazu (tknamazu)
Group: Applications/Text
Requires: tcl, tk, %{name} = %{version}-%{release}
Requires: w3m

%description tk
namazu-tk package provides a Tcl/Tk interface of Namazu (tknamazu).

%prep 
%setup -q -a 2 -n namazu-%{version}
cd tknamazu
%patch1 -p0
%patch2 -p0
%patch3 -p0
cd -
%patch4 -p1
%patch10 -p1
#%%patch12 -p1
#%%patch13 -p1
%patch20 -p1

%build
autoconf
pushd tknamazu
aclocal
autoconf
automake
popd

%configure \
	--with-namazu=%{_bindir}/namazu \
	--with-mknmz=%{_bindir}/mknmz \
	--libexecdir=%{_contentdir}/cgi-bin \
	--enable-tknamazu \
	--with-included-gettext
%make

%install
rm -rf %{buildroot}
%makeinstall libexecdir=%{buildroot}%{_contentdir}/cgi-bin

mv %{buildroot}%{_sysconfdir}/namazu/namazurc-sample \
	%{buildroot}%{_sysconfdir}/namazu/namazurc
mv %{buildroot}%{_sysconfdir}/namazu/mknmzrc-sample \
	%{buildroot}%{_sysconfdir}/namazu/mknmzrc
chmod a+rw -R %{buildroot}%{_localstatedir}/cache/namazu
chmod a+rw -R %{buildroot}%{_localstatedir}/cache/namazu/index

install -m 755 %{SOURCE4} %{buildroot}%{_bindir}/
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE5} %{buildroot}%{_datadir}/applications/
rm lisp/Makefile*

# prepare documents 
(mv doc namazu-docs
rm namazu-docs/Makefile*
cd %{buildroot}%{_datadir}/namazu
rm -rf doc
)
mkdir tknamazu-docs
cp  tknamazu/{AUTHORS,COPYING,ChangeLog,NEWS,README} tknamazu-docs

# install tknamazu additional files
install -m 644 %{SOURCE3} %{buildroot}/usr/share/tknamazu/tknamazurc
cp %{SOURCE1} %{buildroot}/usr/share/tknamazu/wheel.tcl

# install items for Momonga Linux
(cd namazu-momonga
make install \
	DESTDIR=%{buildroot} \
	WWWDIR=%{_contentdir} \
	INDEXDIR=%{_datadir}/namazu/index
)

# remove
rm -rf %{buildroot}%{_datadir}/namazu/etc
rm -f %{buildroot}%{_datadir}/locale/locale.alias

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc ABOUT-NLS AUTHORS COPYING CREDITS ChangeLog ChangeLog.1
%doc INSTALL INSTALL-ja NEWS README README-es README-ja
%doc THANKS TODO etc/namazu.png
%doc namazu-docs lisp
%config(noreplace) %{_sysconfdir}/namazu/*
%{_bindir}/bnamazu
%{_bindir}/mailutime
%{_bindir}/namazu
%{_bindir}/nmzcat
%{_bindir}/nmzegrep
%{_bindir}/nmzgrep
%{_bindir}/nmzmerge
%{_bindir}/*nmz
%{_libdir}/libnmz.so.*
%{_contentdir}/icons/*
%{_mandir}/man1/*
%{_datadir}/locale/*/*/*
%{_datadir}/namazu/filter
%{_datadir}/namazu/index
%{_datadir}/namazu/pl
%{_datadir}/namazu/template
%dir %{_sysconfdir}/namazu
%dir %{_datadir}/namazu
%dir %{_localstatedir}/cache/namazu
%dir %{_localstatedir}/cache/namazu/index

%files devel
%defattr(-, root, root)
%{_bindir}/nmz-config
%{_includedir}/namazu
%{_libdir}/libnmz.so
%{_libdir}/libnmz.a

%files cgi
%defattr(-, root, root)
%{_contentdir}/cgi-bin/namazu.cgi
%config(noreplace) %{_contentdir}/cgi-bin/.namazurc

%files tk
%defattr(-, root, root)
%doc tknamazu-docs/*
%{_bindir}/tknamazu
%{_bindir}/dw3m
%{_datadir}/applications/tknamazu.desktop
%{_datadir}/tknamazu

%changelog
* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.21-1m)
- [SECURITY] CVE-2011-4345
- update to 2.0.21

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.20-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.20-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.20-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.20-5m)
- use BuildRequires

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.20-4m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.20-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.20-1m)
- [SECURITY] Fix buffer overrun
- http://www.namazu.org/security.html
- update to 2.0.20

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.19-1m)
- update to 2.0.19
- License: GPLv2+

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.18-4m)
- define __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.18-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-2m)
- rebuild against gcc43

* Wed Mar 12 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-1m)
- update

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.16-5m)
- rebuild against perl-5.10.0-1m

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-4m)
- fix %%changelog section

* Tue Feb 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.16-3m)
- add patch20 for use %%{_localstatedir}/cache/namazu instead of %%{_localstatedir}/namazu

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.16-2m)
- delete libtool library

* Mon Mar 13 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.16-1m)
- [SECIRITY] fix a directory traversal problem
- temporarily disable 'namazu-2.0.10-recursive_filter.patch'

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.14-2m)
- move tknamazu.desktop to %%{_datadir}/applications/
- update Source5: tknamazu.desktop

* Wed Dec 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.14-1m)
- [SECURITY] Fix a cross-site scripting vulnerability.
- (When query which begins from a tab (%09) is specified.)
-  http://jvn.jp/jp/JVN%23904429FE.html
- update to 2.0.14

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.13-3m)
- build against perl-5.8.5

* Tue Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.13-2m)
- modified BuildPrereq: tcl, tk
- change URI

* Wed Apr 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.13-1m)
- feature enhancements and bugfixes

* Wed Mar 24 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.12-7m)
- revised spec for rpm 4.2.

* Mon Dec  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.12-6m)
- rebuild against perl-5.8.2-2m

* Mon Feb 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.12-5m)
- rebuild against httpd-2.0.43

* Wed Jan 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.12-4m)
- fix changelog of smbd ( smbd smbd -> smbd)

* Wed Jan 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.12-3m)
- use w3m.sh instead of w3m.en

* Wed Jan 29 2003 smbd <smbd@momonga-linux.org>
- (2.0.12-2m)
- use w3m.en for build tknamazu, instead of w3m

* Wed Sep  4 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.12-1m)
- update to 2.0.12
- update mcHT.tknamazu-enable-bz2.patch
- remove patches (merged to original source)
  Patch11: namazu-2.0.10-sigtrap.patch
  Patch14: namazu-2.0.10-pdf.pl.patch
  Patch15: namazu-2.0.10-output.c.patch

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.10-17m)
- enable to build under non exist namazu environment in more smart way
  (thanks to sugazen@pop02.odn.ne.jp)
- Momonga version

* Mon Jul 29 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.10-16m)
- add configure option --with-namazu=%{_bindir}/namazu --with-mknmz=%{_bindir}/mknmz

* Sun Jul 28 2002 smbd <smbd@momonga-linux.org>
- (2.0.10-15m)
- enable to build under non exist namazu environment

* Mon Jun  3 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.10-14k)
- use autoconf

* Mon Apr  8 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.10-12k)
- BuildPrereq: w3m

* Fri Mar 15 2002 zunda <zunda@kondara.org>
- (2.0.10-10k)
- namazu-2.0.10-man-sigpipe.patch is replaced by the `official' one
  [naamzu-devel-jp:?????]

* Wed Mar 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.0.10-8k)
- revise pdf.pl filter for xpdf-1.00
- apply a patch to output.c [namazu-devel-ja:02409]

* Tue Mar  4 2002 zunda <zunda@kondara.org>
- (2.0.10-6k)
- namazu-2.0.10-recursive_filter.patch to let mknmz decompress the
  documetns first even if we get media type specified by the command line

* Tue Feb 26 2002 zunda <zunda@kondara.org>
- (2.0.10-4k)
- namazu-2.0.10-man-sigpipe.patch to withstand broken man pages
- changed tknamazurc.kondara and namazu-kondarahtml.tar.bz2
  to be compatible with namazu-MANdb
- added tdiary.pl written by Uechi-san via kazuhiko-san

* Thu Dec 27 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.10-2k)

* Sat Dec 15 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.9-4k)
- add --with-included-gettext
- %config(noreplace)

* Fri Nov 30 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.9-2k)

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (2.0.8-4k)
- modified requirement chain.
- moved dw3m to namazu-tk.

* Wed Nov 28 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.8-2k)
- remove obsolete patches

* Sat Oct 27 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.7-14k)
- modify %files tk section for defattr

* Sun Oct 21 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.7-12k)
- add tknamazu.desktop

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.0.7-10k)
- rebuild against gettext 0.10.40.

* Tue Sep 18 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.7-8k)
- separate cgi and tk packages

* Sat Sep 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.7-6k)
- add namazu-2.0.7-mhonarc.pl.patch

* Fri Sep 14 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.7-4k)
- revise tknamazurc.kondara to handle man pages with table
- revise mcHT.tknamazu-enable-bz2.patch to handle links in search result
- revise dw3m to handle links in search result
- remove mandoc patch
- treat %{_contentdir}/cgi-bin/.namazurc as %config

* Thu Sep 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.7-2k)
- add namazu-2.0.7-form.c.patch and namazu-2.0.7-output.c.patch

* Mon Sep 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.0.7-0.0002002k)
- version 2.0.7pre2
- add mandoc patch
- revise tknamazurc.kondara
- revise namazu-kondarahtml.tar.bz2
- remove search.html (please try http://localhost/cgi-bin/namazu.cgi )
- add BuildRequires: perl-Text-ChaSen

* Sun Jun  3 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.0.5-12k)
- added mcHT.tknamazu-enable-bz2.patch
- (this patch allow tknamazu to handle bz2 file)
- more macro
- fix %files and any more in spec X)
- modify namazu-kondarahtml.tar.bz2 (See ChangeLog in it)
- exclude unnessesary "ja_JP.SJIS" catalog
- ignore %{_datadir}/namzu/etc and lisp files in ${_defaultsdocdir}

* Mon May 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.5-10k)
- modified contentdir to /home/httpd

* Fri Dec 22 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.5-9k)
- added dyky.w3m_4_configure.patch for not need lynx on building

* Thu Nov 02 2000 Takaaki Tabuchi <tab@kondara.org>
- (2.0.5-5k)
- remove Distribution Tag and it depends on rpm

* Thu Oct 26 2000 Kazuhiko <kazuhiko@kondara.org>
- (2.0.5-3k)

* Mon Oct 23 2000 Kazuhiko <kazuhiko@kondara.org>
- (2.0.4-9k)
- change localstatedir from /var to /var/lib

* Thu Oct 19 2000 Kazuhiko <kazuhiko@kondara.org>
- (2.0.4-7k)
- change http directory from /home/httpd to /var/www

* Fri Aug 25 2000 Kazuhiko <kazuhiko@kondara.org>
- base on FHS
- remove some files from %doc
- add lisp files to /usr/share/namazu/etc/
- fix index location in namazu-kondarahtml/namazrc and tknamazurc.kondara

* Thu Aug 10 2000 Kazuhiko <kazuhiko@kondara.org>
- revise search.html

* Tue Jul 15 2000 Kazuhiko <kazuhiko@kondara.org>
- fix namazu.spec
-   (namazu-vinehtml -> namazu-kondarahtml)
-   (remove /usr/share/namazu/doc symbolic link to prevent unlink
-    failure in case of rpm -U)
- fix namazu-kondarahtml-1.2/Makefile
-   (${DESTDIR}/${IDDIR} -> ${DESTDIR}${IDDIR})
- fix namazu-kondarahtml-1.2/.namazurc
-   (comment out WAKATI entry)
- fix namazu-kondarahtml-1.2/search.html
-   (dbname: KondaraDocs -> Kondara)
-   (change path of Kondara-Docs to /usr/doc/Kondara/)
-   (change vinelogo-s.png to kondaralogo11.png)
- modify dw3m
-   (support local gziped html files)

* Tue Jul 11 2000 Takaaki Tabuchi <tab@kondara.org>
- import vine patches for tknamazu

* Thu Jul 06 2000 Takaaki Tabuchi <tab@kondara.org>
- changed Buildroot 
- added -q at %setup
- added --enable-tknamazu at configure

* Thu Jun 15 2000 Jun Nishii <jun@vinelinux.org>
- 2.0.4-0vl1
- ported for Vine Linux
- now, use w3m in tknamazu!

* Tue Apr 11 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 2.0.4.

* Thu Apr 06 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 2.0.3.
- Requires perl-File-MMagic-1.06.

* Tue Mar 28 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 2.0.2.
- Requires perl-File-MMagic >= 1.02 at build time.

* Wed Mar 01 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 2.0.1.

* Sun Feb 20 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Install namazu.cgi at /home/httpd/cgi-bin.
- Fixed typo.

* Sat Feb 19 2000 Satoru Takabayashi <satoru-t@is.aist-nara.ac.jp>
- Change URL.

* Tue Feb 15 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Delete package entries elisp and cgi.

* Wed Feb 02 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Apapted for namazu-current.
- Changed group Utilities/Text -> Applications/Text.

* Thu Dec 30 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- rpm-3.0.x adaptations.
- Added package entries elisp and cgi (currently comment out). 
  [Merged SAKA Toshihide's changes for Kondara MNU/Linux.]

* Mon Nov 08 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Changed includedir %{prefix}/include/namazu.
- Bug fix at configure section.

* Thu Nov 04 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Added nmz-config in devel package.

* Wed Nov 03 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Use our definite macros, ver, rel, prefix, sysconfdir, and localstatedir.
- If configure not found, use autogen.sh.
- Optimized for SMP environment.
- Build devel package.

* Tue Oct 12 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Fixed correctly executables entry at %files.
- Added missing /usr/share/locale entry at %files.
 
* Thu Aug 26 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Requires perl >= 5.004.
- Delete Packager tag.
- Clean up at %prep.
- Use CFLAGS="%{optflags}" at %build.
- Use %{buildroot} variables at %%install.
- Change configure option at %build and %files for new namazu directory structure.

* Sun May 23 1999 Taku Kudoh <taku@TAHOO.ORG>
- 
