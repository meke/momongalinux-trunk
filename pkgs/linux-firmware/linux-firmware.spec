%global momorel 1
%global checkout a4f3bc03
%global _firmwarepath /lib/firmware

Summary: Firmware files used by the Linux kernel
Name: linux-firmware
Version: 20140605
Release: %{momorel}m%{?dist}
License: GPL+ and GPLv2+ and MIT and "Redistributable, no modification permitted"
Group: System Environment/Kernel
URL: http://www.kernel.org/
#Source0: ftp://ftp.kernel.org/pub/linux/kernel/people/dwmw2/firmware/%{name}-%{version}.tar.gz
Source0: %{name}-%{version}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Provides: kernel-firmware = %{version} xorg-x11-drv-ati-firmware = 7.0
Obsoletes: kernel-firmware < %{version} xorg-x11-drv-ati-firmware < 6.13.0-0.22
Requires: udev
Provides: iwl100-firmware = 39.31.5.1-2m
Provides: iwl105-firmware = 18.168.6.1
Provides: iwl135-firmware = 18.168.6.1
Provides: iwl1000-firmware = 1:39.31.5.1-2m
Provides: iwl2000-firmware = 18.168.6.1
Provides: iwl2030-firmware = 18.168.6.1
Provides: iwl3945-firmware = 15.32.2.9-6m
Provides: iwl4965-firmware = 228.61.2.24-6m
Provides: iwl5000-firmware = 8.83.5.1_1
Provides: iwl5150-firmware = 8.24.2.2-6m
Provides: iwl6000-firmware = 9.221.4.1-5m
Provides: iwl6000g2a-firmware = 17.168.5.3-2m
Provides: iwl6000g2b-firmware = 17.168.5.2-2m
Provides: iwl6050-firmware = 41.28.5.1
Provides: libertas-usb8388-firmware = %{version}-%{release}
Provides: libertas-usb8388-olpc-firmware = %{version}-%{release}
Provides: libertas-sd8686-firmware = %{version}-%{release}
Provides: libertas-sd8787-firmware = %{version}-%{release}
Provides: ql2100-firmware = 1.19.38-7m
Provides: ql2200-firmware = 2.02.08-7m
Provides: ql23xx-firmware = 3.03.28-5m
Provides: ql23xx-firmware = 3.03.28-5m
Provides: ql2400-firmware = 5.08.00-1m
Provides: ql2500-firmware = 5.08.00-1m
Provides: rt61pci-firmware = 1.2-8m
Provides: rt73usb-firmware = 1.8-8m
Provides: ueagle-atm4-firmware = 1.0-6m
Obsoletes: iwl100-firmware < 39.31.5.1-2m
Obsoletes: iwl1000-firmware < 1:39.31.5.1-2m
Obsoletes: iwl3945-firmware < 15.32.2.9-6m
Obsoletes: iwl4965-firmware < 228.61.2.24-6m
Obsoletes: iwl5000-firmware < 8.83.5.1_1
Obsoletes: iwl5150-firmware < 8.24.2.2-6m
Obsoletes: iwl6000-firmware < 9.221.4.1-5m
Obsoletes: iwl6000g2a-firmware < 17.168.5.3-2m
Obsoletes: iwl6000g2b-firmware < 17.168.5.2-2m
Obsoletes: iwl6050-firmware < 41.28.5.1
Obsoletes: libertas-usb8388-firmware < %{version}
Obsoletes: netxen-firmware < 4.0.534-9
Obsoletes: ql2100-firmware < 1.19.38-7m
Obsoletes: ql2200-firmware < 2.02.08-7m
Obsoletes: ql23xx-firmware < 3.03.28-5m
Obsoletes: ql2400-firmware < 5.08.00-1m
Obsoletes: ql2500-firmware < 5.08.00-1m
Obsoletes: rt61pci-firmware < 1.2-8m
Obsoletes: rt73usb-firmware < 1.8-8m
Obsoletes: ueagle-atm4-firmware < 1.0-6m

%description
Kernel-firmware includes firmware files required for some devices to
operate.

%prep
%setup -q -n linux-firmware-%{checkout}

%build
# Remove firmware shipped in separate packages already
# Perhaps these should be built as subpackages of linux-firmware?
rm -rf ess korg sb16 yamaha
# And _some_ conexant firmware.
rm v4l-cx23418-apu.fw v4l-cx23418-cpu.fw v4l-cx23418-dig.fw v4l-cx25840.fw

# Remove source files we don't need to install
rm -f usbdux/*dux */*.asm
rm -rf carl9170fw

# No need to install old firmware versions where we also provide newer versions
# which are preferred and support the same (or more) hardware
rm -f libertas/sd8686_v8*
rm -f libertas/usb8388_v5.bin

# Remove firmware for Creative CA0132 HD as it's in alsa-firmware
rm -f ctefx.bin ctspeq.bin

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_firmwarepath}/updates
cp -r * %{buildroot}%{_firmwarepath}/
rm %{buildroot}%{_firmwarepath}/{WHENCE,LICENCE.*,LICENSE.*}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}


%files
%defattr(-, root, root)
%doc WHENCE LICENCE.* LICENSE.*
%{_firmwarepath}/*

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (20140605-1m)
- version 20140605

* Mon Oct 21 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20131001-1m)
- version 20131001

* Wed Sep 25 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20130724-1m)
- version 20130724

* Sun Jun  2 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20130418-1m)
- version 20130418

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20110731-2m)
- add source

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110731-1m)
- update 20110731

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110709-1m)
- update 20110709

* Tue Jun  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110604-1m)
- update 20110604

* Thu May 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110511-1m)
- update 20110511

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110311-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110311-1m)
- update 20110311

* Tue Jan 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110110-1m)
- update 20110110

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20101113-2m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20101113-1m)
- update 20101113
-- add firmware:  Realtek RTL8712U and RTL8192CE

* Wed Oct  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20100930-1m)
- update 20100930

* Thu Sep 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20100912-1m)
- update 20100912

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100808-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20100808-1m)
- update 20100808

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20100617-2m)
- Provides: ueagle-atm4-firmware = 1.0

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (20100617-1m)
- import from Fedora
- update 20100617

* Fri Apr 09 2010 Dave Airlie <airlied@redhat.com> 20100106-4
- Add further radeon firmwares

* Wed Feb 10 2010 Dave Airlie <airlied@redhat.com> 20100106-3
- add radeon RLC firmware - submitted upstream to dwmw2 already.

* Tue Feb 09 2010 Ben Skeggs <bskeggs@redhat.com> 20090106-2
- Add firmware needed for nouveau to operate correctly (this is Fedora
  only - do not upstream yet - we just moved it here from Fedora kernel)

* Wed Jan 06 2010 David Woodhouse <David.Woodhouse@intel.com> 20090106-1
- Update

* Fri Aug 21 2009 David Woodhouse <David.Woodhouse@intel.com> 20090821-1
- Update, fix typos, remove some files which conflict with other packages.

* Thu Mar 19 2009 David Woodhouse <David.Woodhouse@intel.com> 20090319-1
- First standalone kernel-firmware package.
