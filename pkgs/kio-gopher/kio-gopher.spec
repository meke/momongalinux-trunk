%global         momorel 1
%global         qtver 4.8.0
%global         kdever 4.7.97
%global         kdelibsrel 1m
%global         qtdir %{_libdir}/qt4
%global         kdedir /usr
%global         sourcedir stable/extragear

Name:           kio-gopher
Version:        0.1.4
Release:        %{momorel}m%{?dist}
Summary:        Gopher KIO slave for Konqueror
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://kgopher.berlios.de/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  cmake
BuildRequires:  gettext
Obsoletes:      kio_gopher < 0.1.4
Provides:       kio_gopher = %{version}-%{release}

%description
This KIO slave adds support for the Gopher protocol to Konqueror.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
mkdir %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README COPYING BUGS
# FAQ is only about installation problems which shouldn't affect the RPM.
%{_kde4_libdir}/kde4/kio_gopher.so
%{_kde4_datadir}/kde4/services/gopher.protocol
%{_kde4_datadir}/locale/*/LC_MESSAGES/kio_gopher.mo
%{_kde4_docdir}/HTML/*/kioslave/gopher

%changelog
* Sun Jan  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.4-1m)
- update to 0.1.4
- rename to kio-gopher

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-liux.org>
- (0.1.3-1m)
- import from Fedora devel

* Sat Feb 13 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.1.3-3
- new tarball from 4.4.0 extragear, contains updated translations

* Wed Sep 16 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.1.3-2
- drop postfix tag from Release

* Tue Sep 15 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.1.3-1.kde4.3.1
- Initial Fedora package
