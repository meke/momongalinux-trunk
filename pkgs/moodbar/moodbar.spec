%global momorel 13

Summary: moodbar - audio signal refinement architecture
Name: moodbar
Version: 0.1.2
Release: %{momorel}m%{?dist}
License: GPL
URL: http://amarok.kde.org/wiki/Moodbar
Group: Applications/Multimedia
Source0: http://pwsp.net/~qbob/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: 10_gthread_init.dpatch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fftw3-devel
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: libmad >= 0.15.1b-0.1.2m

%description
The Moodbar is an algorithm for creating a colorful visual
representation of the contents of an audio file, giving an idea of its
"mood" (this is a rather fanciful term for the simple analysis it
actually does).  The Moodbar was invented by Gav Wood for inclusion in
the Amarok music player.

This package contains a plugin with elements that are used in the
moodbar analysis, and an application that actually does the analysis.

The plugin contains elements for performing forward and reverse
Fourier transforms to an audio signal, and a couple of example
analysis elements.  The included elements are:

  fftwspectrum:   Convert a raw audio stream into a frequency spectrum
  fftwunspectrum: Convert a frequency spectrum stream into a raw audio stream
  spectrumeq:     Scale amplitudes of bands of a spectrum
  moodbar:        Toy analysis plugin based on Gav Wood's Exscalibar

The application is called moodbar.  Its syntax is:

    moodbar -o [outfile] [infile]

where [outfile] will contain the moodbar data, and [infile] is an audio
file that gstreamer can decode.

%prep
%setup -q

%patch0 -p1 -b .gthread_init

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/moodbar
%{_libdir}/gstreamer-0.10/libmoodbar.a
%{_libdir}/gstreamer-0.10/libmoodbar.la
%{_libdir}/gstreamer-0.10/libmoodbar.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-9m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-8m)
- change BuildRequires: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-7m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-6m)
- %%NoSource -> NoSource

* Sat Nov 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.2-5m)
- apply latest svn fix (use debian's patch)
 +- moodbar (0.1.2-1) unstable; urgency=low
 +* 10_gthread_init.dpatch: Initialize threading before making glib calls. 
 +-- Bryan Donlan <bdonlan@gmail.com>  Sun, 01 Apr 2007 14:24:46 -0400

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.2-4m)
- use md5sum_src0

* Thu Feb 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.2-3m)
- revive libmoodbar.la

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.2-2m)
- delete libtool library

* Tue Oct 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.2-1m)
- version 0.1.2

* Tue Sep  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-1m)
- initial package for amarok-1.4.3
- Summary is imported from moodbar-0.1.1-1.pclo2005.mde.src.rpm
