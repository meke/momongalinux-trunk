%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}


# Oh the horrors of arch dependent noarch packages!
# (bzrlib is arch dependent.  Thus bzrlib plugins are also arch dependent.)
%define debug_package %{nil}

Name:           bzr-gtk
Version:        0.103.0
Release:        %{momorel}m%{?dist}
Summary:        Bazaar plugin for GTK+ interfaces to most Bazaar operations

Group:          Development/Tools
License:        GPLv2+
URL:            http://bazaar-vcs.org/bzr-gtk
# and see https://launchpad.net/bzr-gtk
#Source0:        http://samba.org/~jelmer/bzr/bzr-gtk-%{version}.tar.gz
Source0:        http://launchpad.net/bzr-gtk/gtk3/%{version}/+download/%{name}-%{version}.tar.gz
NoSource: 0

Source10: credits.pickle

# This requires some nonexistent functionality.  Bug filed upstream.  Disabled
# for now.
Patch1:         bzr-gtk-disable-nautilus-pull.patch
Patch2:         bzr-gtk-desktop-version.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  bzr >= 2.2
BuildRequires:  gettext
BuildRequires:  desktop-file-utils >= 0.16
BuildRequires:  nautilus-python-devel >= 1.0
Requires:       bzr >= 2.2
Requires:       pygtk2
Requires:       hicolor-icon-theme
# This enables the commit-notify functionality but it's not packaged for
# Fedora yet.
#Requires:       bzr-dbus
Obsoletes:      olive < %{version}-%{release}
Provides:       olive = %{version}-%{release}


%description
bzr-gtk is a plugin for Bazaar that aims to provide GTK+ interfaces to most
Bazaar operations.

%package -n nautilus-bzr
Summary: Nautilus plugin for the bazaar revision control system
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Requires:       nautilus-python >= 0.4.3

%description -n nautilus-bzr
nautilus-bzr is an extension for Nautilus, the GNOME file manager.  It
allows you to perform some revision control commands on Bazaar Working Trees
from within Nautilus.

%prep
%setup -q
#%%patch1 -p1 -b .nautilusdisable
#%%patch2 -p1 -b .desktop-version

cp %{SOURCE10} .

%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT

desktop-file-validate bazaar-properties.desktop
desktop-file-validate bzr-handle-patch.desktop
# desktop-file-validate olive-gtk.desktop
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT
if test "%{python_sitelib}" != "%{python_sitearch}" ; then
    install -d -m 0755 $RPM_BUILD_ROOT%{python_sitearch}
    mv $RPM_BUILD_ROOT%{python_sitelib}/* $RPM_BUILD_ROOT%{python_sitearch}/
fi

# No translations yet
#%find_lang olive-gtk

# This won't do anything until after we add bzr-dbus.
rm -rf $RPM_BUILD_ROOT%{_datadir}/applications/bzr-notify.desktop

%clean
rm -rf $RPM_BUILD_ROOT


#%%files -f olive-gtk.lang
%files 
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/*
%{_datadir}/applications/*
%{_datadir}/application-registry/bzr-gtk.applications
#%{_datadir}/olive/
%{_datadir}/bzr-gtk/
%{_datadir}/pixmaps/*.png
%{_datadir}/icons/hicolor/scalable/*/*
%{python_sitearch}/bzrlib/plugins/gtk/
%{python_sitearch}/*.egg-info

%files -n nautilus-bzr
#{_libdir}/nautilus/extensions-3.0/python/*
%{_datadir}/nautilus-python/

%changelog
* Mon Dec 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103.0-1m)
- update to 0.103.0

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.100.0-4m)
- rebuild against nautilus-python-1.0

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.100.0-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.100.0-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.100.0-1m)
- update 0.100.0

* Thu Feb  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.0-2m)
- add credits.pickle

* Thu Feb  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.0-1m)
- update 0.99.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.0-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.98.0-4m)
- rebuild against bzr-2.2

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.98.0-3m)
- build fix with desktop-file-utils-0.16

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.98.0-2m)
- rebuild against desktop-file-utils-0.16

* Mon May 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.98.0-1m)
- update to 0.98.0
- comment out patch1 patch2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.0-1m)
- update to 0.97.0

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96.2-1m)
- update to 0.96.2
- use desktop-file-validate-0.15 instead of desktop-file-validate
- fix import of nautilus-bzr.py

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95.0-3m)
- correct nutilus extensins directory

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95.0-2m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95.0-1m)
- update to 0.95.0
-- drop Patch0,2, not needed
-- update Patch1 for fuzz=0

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.94-2m)
- rebuild against python-2.6.1

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-1m)
- import from Fedora to Momonga for bzr

* Mon May 5 2008 Toshio Kuratomi <toshio@fedoraproject.org> 0.94-1
- Update to 0.94.
- Merge olive package into bzr-gtk to fix BZ#441139.
- Remove patches that were merged into 0.94.

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.93.0-3
- Autorebuild for GCC 4.3

* Tue Dec 11 2007 Toshio Kuratomi <a.badger@gmail.com> 0.93-2
- Move the egg-info into sitearch along with the module.

* Tue Dec 11 2007 Toshio Kuratomi <a.badger@gmail.com> 0.93-1
- Update to bzr-1.0 compatible package.

* Wed Sep 26 2007 Toshio Kuratomi <a.badger@gmail.com> 0.91.0-2
- Olive must be moved to an arch specific directory as well for now.
  If rpm could have noarch subpackages, this would be fine to leave in
  python_sitelib.

* Wed Sep 26 2007 Toshio Kuratomi <a.badger@gmail.com> 0.91.0-1
- Update to 0.91.0.

* Thu Aug 30 2007 Toshio Kuratomi <a.badger@gmail.com> 0.90.0-2
- Move the plugins manually as distutils doesn't know that bzr is arch
  specific.

* Tue Aug 28 2007 Toshio Kuratomi <a.badger@gmail.com> 0.90.0-1
- Update to 0.90.0.
- Update license tag to the new Licensing Guidelines.
- Bzr is now arch specific so all its plugins have to be as well.

* Wed Jul 25 2007 Warren Togami <wtogami@redhat.com> 0.18.0-1
- Update to 0.18.0.

* Thu Jun 28 2007 Warren Togami <wtogami@redhat.com> 0.17.0-1
- Update to 0.17.0.

* Fri May 11 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 0.16.0-1
- Update to 0.16.0.
- Apply patch to fix traceback in nautilus module.

* Mon Apr 30 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 0.15.2-3
- Add a nautilus subpackage as there's now a nautilus-python package in Fedora.

* Wed Apr 18 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 0.15.2-2
- Fix olive description.

* Fri Apr 3 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 0.15.2-1
- Upgrade to 0.15.2:
  + Traceback fixed upstream.
  + UI fix to allow resizing the gstatus dialog window.
- Disable the commit notifier as it won't work until we get bzr-dbus into
  Fedora.

* Thu Apr 2 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 0.15.1-2
- Fix a traceback in two bzr-gtk subcommands.

* Thu Mar 23 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 0.15.1-1
- Update to 0.15.1
- Split olive directory in site-packages into the olive package.
- Don't own the bzrlib or plugins directory as bzr already owns them.
- Notes on splitting the nautilus functionality out.  This isn't done yet as
  we need to add python-nautilus into Fedora.

* Wed Dec 06 2006 Toshio Kuratomi <toshio@tiki-lounge.com> 0.13.0-1
- Update to 0.13.0
- Desktop file patch merged upstream.

* Wed Dec 06 2006 Toshio Kuratomi <toshio@tiki-lounge.com> 0.12.0-1
- Update to 0.12.0
- Add a subpackage for the olive code

* Sun Sep 17 2006 Warren Togami <wtogami@redhat.com> 0.10.0-2
- Fix review issues #206877

* Sun Sep 17 2006 Warren Togami <wtogami@redhat.com> 0.10.0-1
- 0.10.0

* Sat Sep 16 2006 Warren Togami <wtogami@redhat.com> 0.9.0-1
- initial Fedora
