%global momorel 10

Summary: Image viewer with tiny filters
Name: mgw
Version: 0.3.04
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://sourceforge.net/projects/mgw/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib-devel >= 2.4.4
BuildRequires: gtk2-devel >= 2.4.4
BuildRequires: gnome-libs-devel >= 1.4.2
Requires: glib >= 2.4.4
Requires: gtk2 >= 2.4.4
Requires: gnome-libs >= 1.4.2

%description
Image viewer with tiny filters.

%prep

%setup -q

%build
%configure LIBS="-lm"
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-, root,root)
%doc AUTHORS COPYING README INSTALL NEWS TODO ChangeLog
%{_bindir}/mgw
%{_datadir}/locale/*/LC_MESSAGES/mgw.mo

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.04-10m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.04-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.04-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.04-7m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.04-6m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.04-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.04-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.04-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.04-2m)
- %%NoSource -> NoSource

* Sat Dec 17 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.3.04-1m)
- Initial import.