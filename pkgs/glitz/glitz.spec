%global momorel 10

Summary:	OpenGL image compositing library.
Name:		glitz
Version:	0.5.6
Release: %{momorel}m%{?dist}
Group:		System Environment/Libraries
License:	LGPL
#Source0:        http://cairographics.org/snapshots/%{name}-%{version}.tar.gz
#NoSource:       0
Source0:        %{name}-%{version}.tar.gz 
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  mesa-libGL-devel

%description
Glitz is an OpenGL image compositing library. Glitz provides Porter/Duff
compositing of images and implicit mask generation for geometric primitives
including trapezoids, triangles, and rectangles.

%package 	devel
Summary: 	Development files for glitz
Group: 		Development/Libraries
Requires:	mesa-libGL

%description 	devel
Development files for glitz.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/*.la

%clean 
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.6-8m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-5m)
- fix libtool issue

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.6-3m)
- rebuild against gcc43

* Mon Jul 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.6-2m)
- add autoreconf (libtool-1.5.24)

* Tue Jun 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
-  (0.5.6-1m)
- update 0.5.6

* Wed Jun 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-2m)
- fix files list (%%{_includedir}) 

* Fri Mar 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
-  (0.5.4-1m)
- initial commit

