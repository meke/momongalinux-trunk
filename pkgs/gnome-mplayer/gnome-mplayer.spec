%global         momorel 2

Name:           gnome-mplayer
Summary:        GNOME Frontend for MPlayer
Version:        1.0.7
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Multimedia 
URL:            http://code.google.com/p/gnome-mplayer/
Source0:        http://gnome-mplayer.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  GConf2-devel
BuildRequires:  alsa-lib-devel >= 0.7.0
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  glib2-devel
BuildRequires:  gmtk-devel >= %{version}
BuildRequires:  gtk2-devel
BuildRequires:  gtk3-devel
BuildRequires:  libgpod-devel >= 0.7.0
BuildRequires:  libimobiledevice-devel >= 1.1.5
BuildRequires:  libnotify-devel >= 0.7.2
BuildRequires:  nautilus-devel >= 3.0.1.1
BuildRequires:  pkgconfig
BuildRequires:  usbmuxd-devel >= 1.0.8
Requires:       gmtk
Requires:       mplayer
Requires(pre):          GConf2
Requires(post):         GConf2
Requires(preun):        GConf2

%description
GNOME MPlayer is a simple GUI for MPlayer. It is based heavily on the mplayerplug-in source code 
and can basically be seen as a standalone version of that. GNOME MPlayer is currently changing alot. 
However, it is good enough that I can use it as my default viewer for media on my personal machine.

%package nautilus
Summary: %{name}-nautilus
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description nautilus
%{name} nautilus extention

%prep
%__rm -rf --preserve-root %{buildroot}
%setup -q

%build
%configure --enable-nautilus \
    --enable-gtk3 \
    --with-gio \
    --with-alsa \
    --with-libnotify \
    --with-libgpod \
    --with-dbus \
    --with-pulseaudio \
    --with-libmusicbrainz3 \
    "LIBS=-lm"

%make

%install
%__rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

## delete duplicated documents
%__rm -rf %{buildroot}%{_docdir}/%{name}

%clean
%__rm -rf --preserve-root %{buildroot}

%post
update-desktop-database %{_datadir}/applications 2> /dev/null

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files
%defattr(-, root, root)
%doc COPYING ChangeLog DOCS/ INSTALL README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/gnome-control-center/default-apps/*.xml
%{_datadir}/glib-2.0/schemas/apps.gecko-mediaplayer.preferences.gschema.xml
%{_datadir}/glib-2.0/schemas/apps.gnome-mplayer.preferences.enums.xml
%{_datadir}/glib-2.0/schemas/apps.gnome-mplayer.preferences.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/*
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/scalable/apps/gnome-mplayer.svg
%{_mandir}/man1/*

%files nautilus
%defattr(-, root, root)
%{_libdir}/nautilus/extensions-3.0/libgnome-mplayer-properties-page.*


%changelog
* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-2m)
- rebuild against libimobiledeveice-1.1.5

* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Wed Aug 29 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-3m)
- rebuild against libimobiledevice-1.1.4 and usbmuxd-1.0.8

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-2m)
- rebuild for glib 2.33.2

* Sat Jul 30 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-5m)
- rebuild against nautilus-3.0.1.1

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- add patch for gcc46

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.2-5m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.9.2-4m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.9.2-3m)
- add Requires: GConf2

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.9.2-2m)
- build fix

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9.2-1m)
- update to 0.9.9.2

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Thu May 28 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.9.5-2m)
- define __libtoolize (build fix)

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-1m)
- version 0.9.5
- build with libgpod-0.7.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Sun May 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- initial build for Momonga Linux 5
