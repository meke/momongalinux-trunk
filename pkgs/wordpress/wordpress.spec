%global         momorel 1

Summary:	WordPress blogging software
URL:		http://wordpress.org/
Name:		wordpress
Version:	3.4.2
Group:		Applications/Publishing
Release:	%{momorel}m%{?dist}
License:	GPLv2
Source0:	http://wordpress.org/%{name}-%{version}.tar.gz
NoSource:	0
Source1:	wordpress-httpd-conf
Source2:	README.fedora.wordpress
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	php >= 4.1.0, httpd, php-mysql
BuildArch:	noarch

%description
Wordpress is an online publishing / weblog package that makes it very easy,
almost trivial, to get information out to people on the web.

%prep
%setup -q -n wordpress
# disable wp_version_check, updates are always installed via rpm
sed -i -e "s,\(.*\)'wp_version_check'\(.*\),#\1'wp_version_check'\2,g" \
        wp-includes/update.php
# disable update_nag() function
sed -i -e "s,\(.*\)'update_nag'\(.*\),#\1'update_nag'\2,g; \
        s,\(.*\)\$msg .=\(.*\),\1\$msg .= '';,g;" \
        wp-admin/includes/update.php
# fix file encoding
sed -i -e 's/\r//' license.txt

%install
mkdir -p ${RPM_BUILD_ROOT}%{_datadir}/wordpress
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/wordpress
install -m 0644 -D -p %{SOURCE1} ${RPM_BUILD_ROOT}%{_sysconfdir}/httpd/conf.d/wordpress.conf.dist
cp -pr * ${RPM_BUILD_ROOT}%{_datadir}/wordpress
cat wp-config-sample.php | sed -e "s|dirname(__FILE__).'/'|'/usr/share/wordpress/'|g" > \
    ${RPM_BUILD_ROOT}%{_sysconfdir}/wordpress/wp-config.php
/bin/ln -sf ../../../etc/wordpress/wp-config.php ${RPM_BUILD_ROOT}%{_datadir}/wordpress/wp-config.php
/bin/cp %{SOURCE2} ./README.fedora
# Remove empty files to make rpmlint happy
find ${RPM_BUILD_ROOT} -empty -exec rm -f {} \;
# These are docs, remove them from here, docify them later
rm -f ${RPM_BUILD_ROOT}%{_datadir}/wordpress/{license.txt,readme.html}

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/httpd/conf.d/wordpress.conf.dist
%dir %{_datadir}/wordpress
%{_datadir}/wordpress/wp-admin
%{_datadir}/wordpress/wp-content
%{_datadir}/wordpress/wp-includes
%{_datadir}/wordpress/index.php
%doc license.txt
%doc readme.html
%doc README.fedora
%{_datadir}/wordpress/wp-activate.php
%{_datadir}/wordpress/wp-app.php
%{_datadir}/wordpress/wp-blog-header.php
%{_datadir}/wordpress/wp-comments-post.php
%{_datadir}/wordpress/wp-config-sample.php
%{_datadir}/wordpress/wp-config.php
%config(noreplace) %{_sysconfdir}/wordpress/wp-config.php
%{_datadir}/wordpress/wp-cron.php
%{_datadir}/wordpress/wp-links-opml.php
%{_datadir}/wordpress/wp-load.php
%{_datadir}/wordpress/wp-login.php
%{_datadir}/wordpress/wp-mail.php
%{_datadir}/wordpress/wp-settings.php
%{_datadir}/wordpress/wp-signup.php
%{_datadir}/wordpress/wp-trackback.php
%{_datadir}/wordpress/xmlrpc.php
%dir %{_sysconfdir}/wordpress

%changelog
* Mon Sep 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.2-1m)
- [SECURITY] http://wordpress.org/news/2012/09/wordpress-3-4-2/
- update to 3.4.2

* Sun Apr 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-1m)
- [SECURITY] CVE-2012-2399 CVE-2012-2400 CVE-2012-2401 CVE-2012-2402
- [SECURITY] CVE-2012-2403 CVE-2012-2404
- [SECURITY] http://codex.wordpress.org/Version_3.3.2
- update to 3.3.2

* Fri Jul  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.4-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.1.4
- update to 3.1.4

* Thu May 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.3-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.1.3
- update to 3.1.3

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.2-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.1.2
- update to 3.1.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.1.1
- update to 3.1.1

* Wed Feb 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.5-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.0.5
- update to 3.0.5

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.0.4
- update to 3.0.4

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.3-1m)
- update 3.0.3
- [SECURITY] http://codex.wordpress.org/Version_3.0.3

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-1m)
- [SECURITY] http://codex.wordpress.org/Version_3.0.2
- update to 3.0.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-2m)
- rebuild for new GCC 4.5

* Tue Sep  7 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.1-1m)
- update 3.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0-1m)
- update 3.0

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.2-1m)
- [SECURITY] CVE-2010-0682
- update to 2.9.2

* Sun Jan 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.6-1m)
- [SECURITY] CVE-2009-3890 CVE-2009-3891
- update to 2.8.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linuxx.org>
- (2.8.5-1m)
- [SECURITY] CVE-2009-3622
- update to 2.8.5

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.4-1m)
- [SECURITY] CVE-2009-2762
- update to 2.8.4

* Wed Aug  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.3-1m)
- [SECURITY] CVE-2009-2853 CVE-2009-2854
- update to 2.8.3

* Thu Jul 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.2-1m)
- [SECURITY] CVE-2009-2851
- update to 2.8.2

* Sun Jul 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-1m)
- [SECURITY] CVE-2009-2334 CVE-2009-2335 CVE-2009-2336
- update to 2.8.1

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7-2m)
- rebuild against rpm-4.6

* Tue Dec 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Thu Nov 27 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-1m)
- [SECURITY] CVE-2008-5278
- update to 2.6.5

* Thu Sep 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-1m)
- [SECURITY] fix MySQL and SQL Column Truncation Vulnerabilities
- [SECURITY] fix mt_srand and not so random numbers
- see http://wordpress.org/development/2008/09/wordpress-262/
- update to 2.6.2

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6, contains some security fixes

* Fri May 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.1-1m)
- import from Fedora

* Sat Apr 26 2008 Adrian Reber <adrian@lisas.de> - 2.5.1-1
- updated to 2.5.1 for security fixes - BZ 444396

* Fri Feb  8 2008 John Berninger <john at ncphotography dot com> - 2.3.3-0
- update to 2.3.3 for security fixes - BZ 431547

* Sun Dec 30 2007 Adrian Reber <adrian@lisas.de> - 2.3.2-1
- updated to 2.3.2 (bz 426431, Draft Information Disclosure)

* Tue Oct 30 2007 Adrian Reber <adrian@lisas.de> - 2.3.1-1
- updated to 2.3.1 (bz 357731, wordpress XSS issue)

* Mon Oct 15 2007 Adrian Reber <adrian@lisas.de> - 2.3-1
- updated to 2.3
- disabled wordpress-core-update

* Tue Sep 11 2007 Adrian Reber <adrian@lisas.de> - 2.2.3-0
- updated to 2.2.3 (security release)

* Wed Aug 29 2007 John Berninger <john at ncphotography dot com> - 2.2.2-0
- update to upstream 2.2.2
- license tag update

* Mon Apr 16 2007 john Berninger <jwb at redhat dot com> - 2.1.3-1
- update to 2.1.3 final - bz235912

* Mon Mar 26 2007 John Berninger <jwb at redhat dot com> - 2.1.3-rc2
- update to 2.1.3rc2 for bz 233703

* Sat Mar  3 2007 John Berninger <jwb at redhat dot com> - 2.1.2-0
- update to 2.1.2 - backdoor exploit introduced upstream in 2.1.1 - bz 230825

* Tue Feb 27 2007 John Berninger <jwb at redhat dot com> - 2.1.1-0
- update to 2.1.1 to fix vuln in bz 229991

* Wed Jan 31 2007 John Berninger <jwb at redhat dot com> - 2.1-0
- update to v2.1 to fix multiple bz/vuln's

* Sun Dec  3 2006 John Berninger <jwb at redhat dot com> - 2.0.5-2
- Remove mysql-server dependency

* Sun Dec  3 2006 John Berninger <jwb at redhat dot com> - 2.0.5-1
- Update to upstream 2.0.5 to fix vuln in bz 213985

* Thu Oct 26 2006 John Berninger <jwb at redhat dot com> - 2.0.4-1
- Doc fix for BZ 207822

* Sat Aug 12 2006 John Berninger <jwb at redhat dot com> - 2.0.4-0
- Upstream security vulns - bz 201989

* Sun Jul 23 2006 John Berninger <jwb at redhat dot com> - 2.0.3-4
- Fix broken upgrade path from FE4

* Tue Jul  4 2006 John Berninger <jwb at redhat dot com> - 2.0.3-3
- Add a README.fedora file
- Add php-mysql requires

* Tue Jun 20 2006 John Berninger <jwb at redhat dot com> - 2.0.3-2
- Remove use of installprefix macro
- %%{_datadir}/wordpress/wp-config.php is not a config file
- Symlink is relative

* Mon Jun 19 2006 John Berninger <jwb at redhat dot com> - 2.0.3-1
- Changes from Jarod Wilson as below
- Update to 2.0.3
- Rearrange to use /usr/share/wordpress and /etc/wordpress
- Remove patch (included upstream)
- Remove empty files to make rpmlint happy

* Tue May 30 2006 John Berninger <jwb at redhat dot com> - 2.0.2-1
- Added patch for \n cache injection - upstream changeset #3797

* Sat May 27 2006 John Berninger <jwb at redhat dot com> - 2.0.2-0
- Initial build
