%global momorel 6

Summary: library to import Microsoft Word documents
Name: wv2
Version: 0.4.2
Release: %{momorel}m%{?dist}
License: GPL
URL: http://sourceforge.net/projects/wvware/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/wvware/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# see bug #192291
Patch2: wv2-0.4.0-extra_libs.patch
BuildRequires: cmake
BuildRequires: libgsf-devel >= 1.14.0
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: zlib-devel

%description
The wv2 library is used to import Microsoft Word documents in koffice
for example.

%package devel
Summary: Headers for developing programs that will use wv2
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libgsf-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use libraries from wv2.

%prep
%setup -q
%patch2 -p1 -b .extra_libs

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
export CXXFLAGS=-DGLIB_COMPILATION
export LDFLAGS=-lgmodule-2.0
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

find %{buildroot} -name "*.la" -delete

%check
pushd %{_target_platform}
# checks currently fail on ppc64, appears to be a toolchain prob
%ifarch ppc64
make -k test -C ||:
%else
make -k test
%endif
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING.LIB ChangeLog INSTALL README RELEASE THANKS TODO
%{_libdir}/libwv2.so.*
%dir %{_libdir}/wvWare

%files devel
%defattr(-, root, root)
%doc doc/*
%{_bindir}/wv2-config
%{_includedir}/wv2
%{_libdir}/libwv2.so
%{_libdir}/wvWare/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-6m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-5m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-3m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-2m)
- update to 0.4.2 again

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-8m)
- full rebuild for mo7 release

* Sun Aug  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.3-7m)
- revert to version 0.2.3 for koffice

* Mon Dec 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.3-5m)
- rebuild against gcc43

* Thu Feb 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-4m)
- add patch for gcc43, generated by gen43patch(v1)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-3m)
- %%NoSource -> NoSource

* Fri Aug 18 2006 Nishio Futoshi <futoshi@momonga-linix.org>
- (0.2.3-2m)
- delete libtool library

* Sat Jun 17 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.3-1m)
- update to 0.2.3

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.2-3m)
- rebuild against libgsf-1.14.0

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-2m)
- rebuild against libgsf-1.13.3

* Fri Jun 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-1m)
- initial package for koffice
- import Summary and %%description from SUSE supplementary
