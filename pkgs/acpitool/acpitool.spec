%global momorel 4

Summary: Command line ACPI client
Name: acpitool
Version: 0.5.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://freeunix.dyndns.org:8000/site2/acpitool.shtml

Source0: http://freeunix.dyndns.org:8088/ftp_site/pub/unix/acpitool/acpitool-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
AcpiTool is a Linux ACPI client. It's a small command line application, 
intended to be a replacement for the apm tool. Besides "basic" ACPI 
information like battery status, AC presence, putting the laptop to
sleep, Acpitool also supports various extensions for Toshiba, Asus and 
IBM Thinkpad laptops, allowing you to change the LCD brightness level, 
toggle fan on/off, and more. 


%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -fr $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -fr $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING INSTALL README TODO
%{_bindir}/acpitool
%{_mandir}/man1/acpitool*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1m)
- update 0.5.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Sep 18 2008 Patrice Dumas <pertusus@free.fr> 0.5-1
- update to 0.5

* Mon Jul 14 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.4.7-5
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.4.7-4
- Autorebuild for GCC 4.3

* Thu Jan  3 2008 Patrice Dumas <pertusus@free.fr> 0.4.7-3
- fixes for gcc 4.3

* Thu May 24 2007 Patrice Dumas <pertusus@free.fr> 0.4.7-2
- update to 0.4.7

* Fri Oct  6 2006 Patrice Dumas <pertusus@free.fr> 0.4.6-2
- set Group to Applications/System (fix #209230)

* Mon Aug 28 2006 Patrice Dumas <pertusus@free.fr> 0.4.6-1
- update to 0.4.6

* Sun May 21 2006 Patrice Dumas <pertusus@free.fr> 0.4.5-1
- update to 0.4.5

* Thu Feb 16 2006 Patrice Dumas <pertusus@free.fr> 0.4.4-1.1
- new version
- remove now unneeded patch

* Thu Nov 10 2005 Patrice Dumas <pertusus@free.fr> 0.3.0-3
- add patch to avoid ignoring CXXFLAGS

* Fri Nov  4 2005 Patrice Dumas <pertusus@free.fr> 0.3.0-2
- update using fedora core conventions, some cleanings

* Tue Aug 24 2004 Robert Ambrose <rna@muttsoft.com>
- Created .spec file.

