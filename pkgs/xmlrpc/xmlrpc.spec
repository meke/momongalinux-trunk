%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

Name:       xmlrpc
Version:    2.0.1
Release:    %{momorel}m%{?dist}
Epoch:      0
Summary:    Java XML-RPC implementation
License:    "ASL 2.0"
Group:      Development/Libraries
Url:        http://ws.apache.org/xmlrpc/xmlrpc2/
Source0:    http://www.apache.org/dist/ws/xmlrpc/sources/xmlrpc-2.0.1-src.tar.gz
Patch0:     %{name}-%{version}-jessie.patch

BuildRequires:  ant
BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  servletapi5
BuildRequires:  junit
BuildRequires:  jakarta-commons-httpclient
BuildRequires:  jakarta-commons-codec >= 1.3
BuildRequires:  jsse
Requires:       jpackage-utils >= 0:1.6
Requires:       servletapi5
Requires:       junit
Requires:       jakarta-commons-httpclient
Requires:       jakarta-commons-codec >= 1.3
Requires:       jsse

%if ! %{gcj_support}
Buildarch:    noarch
%endif
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:    java-gcj-compat-devel
Requires(post):   java-gcj-compat
Requires(postun): java-gcj-compat
%endif

%description
Apache XML-RPC is a Java implementation of XML-RPC, a popular protocol
that uses XML over HTTP to implement remote procedure calls.
Apache XML-RPC was previously known as Helma XML-RPC. If you have code
using the Helma library, all you should have to do is change the import
statements in your code from helma.xmlrpc.* to org.apache.xmlrpc.*.

%package javadoc
Summary:    Javadoc for %{name}
Group:      Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{name}-%{version}
# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;

%patch0 -b .sav

%build
export CLASSPATH=%(build-classpath jsse commons-httpclient commons-codec servletapi5 junit 2>/dev/null)
ant -Dbuild.dir=./bin -Dbuild.dest=./bin -Dsrc.dir=./src -Dfinal.name=%{name}-%{version} -Djavadoc.destdir=./docs/apidocs -Dhave.deps=true jar
ant -Dbuild.dir=./bin -Dbuild.dest=./bin -Dsrc.dir=./src -Dfinal.name=%{name}-%{version} -Djavadoc.destdir=./docs/apidocs -Dhave.deps=true javadocs

%install
rm -rf $RPM_BUILD_ROOT

# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 bin/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
install -m 644 bin/%{name}-%{version}-applet.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-applet-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do \
ln -sf ${jar} ${jar/-%{version}/}; done)

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr docs/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc LICENSE.txt README.txt
%{_javadir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/xmlrpc-applet-2.0.1.jar.*
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-1m)
- import from Fedora 11
- use gcj

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:2.0.1-5.5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jul 10 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0:2.0.1-4.5
- drop repotag

* Thu May 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0:2.0.1-4jpp.4
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0:2.0.1-4jpp.3
- Autorebuild for GCC 4.3

* Tue Mar 27 2007 Matt Wringe <mwringe@redhat.com> 0:2.0.1-3jpp.3
- Spec file clean up for Fedora Extras Review

* Thu Mar 08 2007 Deepak Bhole <dbhole@redhat.com> 2.0.1-3jpp.2
- Add javax.net.ssl support to build org.apache.xmlrpc.secure.*
- Minor spec file cleanup

* Fri Aug 04 2006 Vivek Lakshmanan <vivekl@redhat.com> - 0:2.0.1-3jpp.1
- Merge with latest from JPP.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:2.0.1-1jpp_8.2fc
- Rebuilt

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:2.0.1-1jpp_8.1fc
- rebuild

* Wed Mar  8 2006 Rafael Schloming <rafaels@redhat.com> - 0:2.0.1-1jpp_7fc
- excluded s390 due to eclipse

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:2.0.1-1jpp_6fc
- stop scriptlet spew

* Fri Feb 24 2006 Igor Foox <ifoox@redhat.com> - 0:2.0.1-1jpp_5fc
- Added post/postun dependency on coreutils.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0:2.0.1-1jpp_4fc
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0:2.0.1-1jpp_3fc
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 25 2006 Igor Foox <ifoox@redhat.com>  0:2.0.1-1jpp_2fc
- ExcludeArch s390x and ppc64

* Wed Jan 18 2006 Andrew Overholt <overholt@redhat.com> 0:2.0.1-1jpp_2fc
- Comment out JPackage Distribution and Vendor tags

* Wed Jan 18 2006 Jesse Keating <jkeating@redhat.com> 0:2.0.1-1jpp_2fc
- bump for test

* Wed Jan 18 2006 Igor Foox <ifoox@redhat.com> 0:2.0.1-1jpp_1fc
- Update to version 2.0.1
- Natively compile

* Thu Aug 26 2004 Ralph Apel <r.apel at r-apel.de> 0:1.2-0.b1.3jpp
- Build with ant-1.6.2

* Thu Apr 29 2004 David Walluck <david@jpackage.org> 0:1.2-0.b1.2jpp
- add jar symlinks
- remove %%buildroot in %%install

* Tue May 06 2003 David Walluck <david@anti-microsoft.org> 0:1.2-0.b1.1jpp
- 1.2-b1
- update for JPackage 1.5

* Mon Mar 18 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1-1jpp 
- 1.1
- generic servlet support
- used source release
- dropped patch
- added applet jar

* Mon Jan 21 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-3jpp 
- versioned dir for javadoc
- no dependencies for javadoc package
- dropped jsse package
- adaptation to new servlet3 package
- adaptation to new jsse package
- section macro

* Fri Dec 7 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-2jpp
- javadoc into javadoc package

* Sat Nov 3 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-1jpp
- first JPackage release
