%global momorel 5

Name: taglib-sharp
Version: 2.0.3.7
Release: %{momorel}m%{?dist}
License: LGPLv2+
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://download.banshee-project.org/taglib-sharp/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: taglib-sharp-2.0.3.2-libdir.patch

Summary: taglib for .NET
Group: Development/Libraries

BuildRequires: pkgconfig
BuildRequires: mono-core
BuildRequires: mono-devel
BuildRequires: gnome-sharp-devel

%description
TagLib# is a FREE and Open Source library for the .NET 2.0 and Mono
frameworks which will let you tag your software with as much or as
little detail as you like without slowing you down. It supports a
large variety of movie and music formats which abstract away the work,
handling all the different cases, so all you have to do is access
file.Tag.Title, file.Tag.Lyrics, or my personal favorite
file.Tag.Pictures.

But don't think all this abstraction is gonna keep you from tagging's
greatest gems. You can still get to a specific tag type's features
with just a few lines of code.
#'

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
%{name}-devel

%prep
%setup -q
%patch0 -p1

%build
%configure --disable-docs
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_prefix}/lib/mono/%{name}
%{_prefix}/lib/mono/gac/%{name}
%{_prefix}/lib/mono/gac/policy.2.0.%{name}

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/taglib-sharp.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3.7-5m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3.7-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3.7-1m)
- update to 2.0.3.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3.2-1m)
- update to 2.0.3.2
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3.0-4m)
- rebuild against rpm-4.6

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3.0-3m)
- good-bye debug symbol

* Mon Jul 21 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.3.0-2m)
- add libdir patch for lib64

* Mon Jul 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3.0-1m)
- initial build
