%global         momorel 1
#global         betaver 3
#global         prever 6
%global         qtver 4.8.4
%global         qtdir %{_libdir}/qt4

Name:           nmapsi4
Version:        0.4.1
Release:        %{momorel}m%{?dist}
Summary:        NmapSI qt4 porting
License:        GPLv2
Group:          Applications/System
URL:            http://www.nmapsi4.org/
Source0:        http://nmapsi4.googlecode.com/files/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires:       qt >= %{qtver}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  ImageMagick
BuildRequires:  cmake
BuildRequires:  desktop-file-utils

%description
NmapSi4 is a complete Qt-based Gui with the design goals to provide a complete nmap interface 
for Users, in order to menagement all options of this power security net scanner!

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}
chmod +x %{buildroot}%{_bindir}/*

# convert and link icon
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/48x48/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
convert -scale 48x48 icons/64x64/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/%{name}
%{_datadir}/dbus-1/interfaces/org.nmapsi4.Nmapsi4.xml
%{_datadir}/pixmaps/%{name}.png

%changelog
* Thu Jan 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Tue Dec 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- update to 0.4

* Tue Nov  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.80-1m)
- update to 0.3.80 (0.4.0 beta1)

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Mon Dec 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- update to 0.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-2m)
- specify PATH for Qt4

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-1m)
- update to 0.2

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.6.2m)
- fix build failure

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.6.1m)
- update to 0.2-beta3
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-0.5.3m)
- full rebuild for mo7 release

* Sun Aug 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-0.5.2m)
- add icon to nmapsi4-logr.desktop
- install extra icon for DE/WMs
- correct install directory of desktop files
- add %%post and %%postun
- sort %%files
- stop using macros.kde4, this package does not depend on KDE

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.5.1m)
- update to 0.2-beta2

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.4.4m)
- rebuild against qt-4.6.3-1m

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.4.3m)
- fix build with qt-4.7.0

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-0.4.2m)
- explicitly link libQtNetwork

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.4.1m)
- update to 0.2-beta1

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.3.2m)
- replace source (correct version number...)

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.3.1m)
- update to 0.2-alpha3

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.2.1m)
- update to 0.2-alpha2

* Fri Dec 25 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2-0.1.2m)
- change sha256sum 

* Fri Nov 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-0.1.1m)
- update to 0.2-alpha1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.97-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.97-1m)
- update to 0.1.97

* Sun Jun 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.96-1m)
- update to 0.1.96rev2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-1m)
- update to 0.1 official release
- update desktop patch

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-0.4.4m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-0.4.3m)
- rebuild against gcc43

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-0.4.2m)
- enable parallel build
- update desktop.patch

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-0.4.1m)
- update to 0.1-rc1

* Sun Dec  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-0.3.1m)
- initial build for Momonga Linux 4
