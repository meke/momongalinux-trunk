%global momorel 14

Summary: Tuxedo T. Penguin stars in Tuxkart
Name: tuxkart
Version: 0.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://tuxkart.sourceforge.net/
Group: Amusements/Games
Source0: http://tuxkart.sourceforge.net/dist/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: %{name}.desktop
Source2: %{name}-16x16.png
Source3: %{name}-32x32.png
Source4: %{name}-48x48.png
Patch0: tuxkart-0.4.0-alpha.patch
Patch1: %{name}-%{version}-ppc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: freeglut
BuildRequires: plib freeglut-devel

%description
This is another game that stars your Favorite Hero: Tux, the Linux Penguin.

%prep
%setup -q

%patch0 -p1 -b .alpha~
%ifarch ppc ppc64
%patch1 -p1 -b .ppc~
%endif

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE4} %{buildroot}%{_datadir}/pixmaps/tuxkart.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS CHANGES COPYING LICENSE README
%{_bindir}/tuxkart
%{_datadir}/applications/tuxkart.desktop
%{_datadir}/games/tuxkart
%{_datadir}/pixmaps/tuxkart.png
%{_datadir}/tuxkart

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-12m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-10m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-9m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-7m)
- %%NoSource -> NoSource

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.0-6m)
- enable ppc64

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-5m)
- rebuild against freeglut

* Fri Jun 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-4m)
- import icons: tuxkart-*.png from cooker contrib
- add tuxkart.desktop

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.0-3m)
- remove duplicate dir

* Mon Feb 28 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.0-2m)
- build fix for ppc.

* Sat Apr  3 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0
- rebuild against plib-1.8.2

* Tue Nov 12 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.2.0-1m)
- add more docs as 644
 ("%%attr" makes /usr/share/doc/tuxkart-0.2.0 644, too!)

* Wed May  8 2002 Toru Hoshina <t@kondara.org>
- (0.1.0-2k)
- version up.
- rebuild against XFree86 4.2.0 without external Mesa.

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.0.6-6k)
- s/Copyright/License/

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.6-4k)
- remove automake autoconf

* Sun Sep  9 2001 Toru Hoshina <toru@df-usa.com>
- (0.0.6-2k)
- 1st release.
