%global momorel 9
%global srcver 0.6
%global srcrel 113365
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m
%global kdeworkspacerel 1m

Summary: A hardware device information KCM
Name: kcm_devinfo
Version: 0.6
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://kde-apps.org/content/show.php/Device+Information+KCM?content=113365
Source0: http://kde-apps.org/CONTENT/content-files/%{srcrel}-%{name}%{srcver}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdebase-workspace >= %{kdever}-%{kdeworkspacerel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase-workspace-devel >= %{kdever}-%{kdeworkspacerel}
BuildRequires: cmake
Provides: devinfo = %{version}-%{release}
Obsoletes: devinfo < %{version}-%{release}

%description
DevInfo is a hardware device information KCM. 
This module allows you to view current information
about the hardware in your PC.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changelog TODO
%{_kde4_libdir}/kde4/%{name}.so
%{_kde4_datadir}/kde4/services/%{name}.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-8m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-6m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-5m)
- adapt kcm_devinfo.desktop to KDE 4.5 specification

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-4m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-3m)
- touch up spec file

* Wed Dec  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-2m)
- change Name: from devinfo to kcm_devinfo

* Wed Dec  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- version 0.6
- update dekstop.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0

* Sun Nov  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- version 0.4.0

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-1m)
- initial package for Momonga Linux
