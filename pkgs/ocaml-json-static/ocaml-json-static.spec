%global momorel 5
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-json-static
Version:        0.9.8
Release:        %{momorel}m%{?dist}
Summary:        OCaml JSON validator and converter (syntax extension)

Group:          Development/Libraries
License:        Modified BSD
URL:            http://martin.jambon.free.fr/json-static.html
Source0:        http://martin.jambon.free.fr/json-static-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel

# Make this dependency explicit because users won't be able
# to do much useful without it, and the automatic dependency
# checking script cannot pick it up.
Requires:       ocaml-json-wheel

%global __ocaml_requires_opts -i Asttypes -i Parsetree

%description
json-static is a tool for converting parsed JSON data with an
unchecked structure into specialized OCaml types and vice-versa.
It is a complement to the json-wheel library which provides a
parser and a (pretty-) printer.


%prep
%setup -q -n json-static-%{version}


%build
make


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE README Changes yahoo.ml
%{_libdir}/ocaml/json-static


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8-5m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-1m)
- import from Fedora

* Mon May 10 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.6-3
- Fixed the description.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.6-2
- Remove ExcludeArch ppc64.

* Thu Feb 28 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.6-1
- Initial RPM release.
