%global momorel 8

%global	ruby_sitelib		%(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")

%global	gemname	allison
%global	gemdir		%(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global	geminstdir	%{gemdir}/gems/%{gemname}-%{version}

%global	rubyabi	1.9.1

Summary:	A modern, pretty RDoc template
Name:		rubygem-%{gemname}
Version:	2.0.3
Release:	%{momorel}m%{?dist}
Group:		Development/Languages
License:	"Academic Free License (AFL) v. 3.0"
URL:		http://github.com/fauna/allison/tree/master
Source0:	http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	ruby(abi) = %{rubyabi}
BuildRequires:	ruby(rubygems)
Requires:	ruby(abi) = %{rubyabi}
Requires:	ruby(rubygems)
BuildArch:	noarch
Provides:	rubygem(%{gemname}) = %{version}-%{release}

%description
%{summary}.

%package	doc
Summary:	Documentation for %{name}
Group:		Documentation
Requires:	%{name} = %{version}-%{release}

%description	doc
This package contains documentation for %{name}.

%prep
%setup -q -c -T

mkdir -p .%{gemdir}
gem install \
	--local \
	--install-dir $(pwd)%{gemdir} \
	--force \
	--rdoc \
	-V \
	%{SOURCE0}

pushd .%{geminstdir}
popd

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* %{buildroot}%{gemdir}/

# move bin file
mkdir -p %{buildroot}%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}%{_bindir}/
rm -rf %{buildroot}%{gemdir}/bin

# And cleanups
rm -rf %{buildroot}%{gemdir}/bin
rm -f %{buildroot}%{geminstdir}/%{gemname}.gemspec

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/%{gemname}

%dir %{geminstdir}
%doc %{geminstdir}/[A-Z]*
%exclude %{geminstdir}/Manifest
%{geminstdir}/bin/
%{geminstdir}/lib/
%{geminstdir}/cache/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%defattr(-,root,root,-)
%{geminstdir}/Manifest
%{geminstdir}/contrib/
%{gemdir}/doc/%{gemname}-%{version}/

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-8m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-5m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-4m)
- remove .yardoc

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.3-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-1m)
- import from Fedora to Momonga

* Sat Jul 25 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.3-4
- F-12: Mass rebuild

* Thu Jun 25 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.3-3
- It turned out that the patch mentioned below was not needed...

* Tue Jun 16 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.3-2
- Patch to make allison work when called as %%_bindir/allison

* Tue Jun 16 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.3-1
- Initial package
