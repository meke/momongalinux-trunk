%global momorel 1
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; 
print get_python_lib(1)")}

%global with_python3 1

Summary: Create deltas between rpms
Name: deltarpm
Version: 3.6
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Base
URL: http://gitorious.org/deltarpm/deltarpm
Source: ftp://ftp.suse.com/pub/projects/deltarpm/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: bzip2-devel, xz-devel >= 5.0.0, rpm-devel >= 4.8.0, popt-devel
BuildRequires: zlib-devel
BuildRequires: python-devel

%if 0%{?with_python3}
BuildRequires: python3-devel >= 3.4
# FIXME: remove BR: python3-libs again, once #564527 is gone
BuildRequires: python3-libs
%endif

Requires: xz-libs >= 5.0.0

%description
A deltarpm contains the difference between an old
and a new version of a rpm, which makes it possible
to recreate the new rpm from the deltarpm and the old
one. You don't have to have a copy of the old rpm,
deltarpms can also work with installed rpms.

%package -n drpmsync
Summary: Sync a file tree with deltarpms
Requires: deltarpm = %{version}-%{release}

%description -n drpmsync
This package contains a tool to sync a file tree with
deltarpms.

%package -n deltaiso
Summary: Create deltas between isos containing rpms
Requires: deltarpm = %{version}-%{release}
Requires: xz-libs >= 5.0.0

%description -n deltaiso
This package contains tools for creating and using deltasisos,
a difference between an old and a new iso containing rpms.

%package -n python-deltarpm
Summary: Python bindings for deltarpm
Requires: deltarpm = %{version}-%{release}
Requires: xz-libs >= 5.0.0

%description -n python-deltarpm
This package contains python bindings for deltarpm.

%if 0%{?with_python3}
%package -n python3-deltarpm
Summary: Python bindings for deltarpm
Requires: deltarpm = %{version}-%{release}
Requires: xz-libs >= 5.0.0

%description -n python3-deltarpm
This package contains python bindings for deltarpm.
%endif

%prep
%setup -q 

%build
%{__make} %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS" \
    bindir=%{_bindir} libdir=%{_libdir} mandir=%{_mandir} prefix=%{_prefix} \
    zlibbundled='' zlibldflags='-lz' zlibcppflags=''
%{__make} %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS" \
    bindir=%{_bindir} libdir=%{_libdir} mandir=%{_mandir} prefix=%{_prefix} \
    zlibbundled='' zlibldflags='-lz' zlibcppflags='' \
    python

%install
%{__rm} -rf %{buildroot}
%makeinstall pylibprefix=%{buildroot}

%if 0%{?with_python3}
# nothing to do
%else
rm -rf %{buildroot}%{_libdir}/python3.1
%endif


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc LICENSE.BSD README
%doc %{_mandir}/man8/applydeltarpm*
%doc %{_mandir}/man8/makedeltarpm*
%doc %{_mandir}/man8/combinedeltarpm*
%{_bindir}/applydeltarpm
%{_bindir}/combinedeltarpm
%{_bindir}/makedeltarpm
%{_bindir}/rpmdumpheader

%files -n deltaiso
%defattr(-, root, root, 0755)
%doc LICENSE.BSD README
%doc %{_mandir}/man8/applydeltaiso*
%doc %{_mandir}/man8/makedeltaiso*
%doc %{_mandir}/man8/fragiso*
%{_bindir}/applydeltaiso
%{_bindir}/fragiso
%{_bindir}/makedeltaiso

%files -n drpmsync
%defattr(-, root, root, 0755)
%doc LICENSE.BSD README
%doc %{_mandir}/man8/drpmsync*
%{_bindir}/drpmsync

%files -n python-deltarpm
%defattr(-, root, root, 0755)
%doc LICENSE.BSD
%{python_sitearch}/*


%if 0%{?with_python3}

%files -n python3-deltarpm
%defattr(-, root, root, 0755)
%doc LICENSE.BSD
## this directory provided by python3-libs
%exclude %dir %{python3_sitearch}/__pycache__
%{python3_sitearch}/*

%endif

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-1m)
- update 3.6

* Mon Sep 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6-0.20100223git.3m)
- modify %%files

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6-0.20100223git.2m)
- rebuild against python-3.2

* Tue Apr 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6-0.20100223git.1m)
- update 3.6-0.20100223

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-0.20100121git.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-0.20100121git.4m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5-0.20100121git.3m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5-0.20100121git.2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5-0.20100121git.1m)
- sync Fedora
- update 20100121git

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-5m)
- explicitly link librpmio

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-4m)
- rebuild against rpm-4.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-2m)
- [SECURITY] CVE-2005-1849
- use system zlib because deltarpm has vulnerable zlib copy

* Mon May 11 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.4-1m)
- import from Fedora

* Thu Mar 26 2009 Jonathan Dieter <jdieter@gmail.com> - 3.4-15
- Fix bug when checking sequence with new sha256 file digests

* Tue Mar 24 2009 Jonathan Dieter <jdieter@gmail.com> - 3.4-14
- Add support for rpms with sha256 file digests

* Fri Mar 06 2009 Jesse Keating <jkeating@redhat.com> - 3.4-13
- Rebuild for new rpm libs

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jul 13 2008 Jonathan Dieter <jdieter@gmail.com> - 3.4-11
- Rebuild for rpm 4.6

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 3.4-10
- Autorebuild for GCC 4.3

* Mon Jan  7 2008 Jonathan Dieter <jdieter@gmail.com> - 3.4-9
- Add patch that allows deltarpm to rebuild rpms from deltarpms that have
  had the rpm signature added after their creation.  The code came from
  upstream.
- Drop nodoc patch added in 3.4-4 as most packages in repository have been
  updated since April-May 2007 and this patch was supposed to be temporary.

* Wed Aug 29 2007 Jonathan Dieter <jdieter@gmail.com> - 3.4-6
- Bring in popt-devel in BuildRequires to fix build in x86_64

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 3.4-5
- Rebuild for selinux ppc32 issue.

* Wed Jul 11 2007 Jonathan Dieter <jdieter@gmail.com> - 3.4-4
- Fix prelink bug
- Ignore verify bits on %doc files as they were set incorrectly in older
  versions of rpm.  Without this patch, deltarpm will not delta doc files
  in rpm created before April-May 2007

* Tue Jun  5 2007 Jeremy Katz <katzj@redhat.com> - 3.4-3
- include colored binaries from non-multilib-dirs so that deltas can work 
  on multilib platforms

* Wed May 09 2007 Adam Jackson <ajax@redhat.com> 3.4-2
- Add -a flag to work around multilib ignorance. (#238964)

* Tue Mar 06 2007 Adam Jackson <ajax@redhat.com> 3.4-1
- Update to 3.4 (#231154)

* Mon Feb 12 2007 Adam Jackson <ajax@redhat.com> 3.3-7
- Add RPM_OPT_FLAGS to make line. (#227380)

* Mon Feb 05 2007 Adam Jackson <ajax@redhat.com> 3.3-6
- Fix rpm db corruption in rpmdumpheader.  (#227326)

* Mon Sep 11 2006 Mihai Ibanescu <misa@redhat.com> - 3.3-5
- Rebuilding for new toolset

* Thu Aug 17 2006 Mihai Ibanescu <misa@redhat.com> - 3.3-4
- Removing BuildRequires: gcc

* Tue Aug 15 2006 Mihai Ibanescu <misa@redhat.com> - 3.3-3
- Fedora packaging guidelines build

* Tue Aug  8 2006 Mihai Ibanescu <misa@redhat.com> - 3.3-2
- Added BuildRequires: rpm-devel, gcc

* Sat Dec 03 2005 Dries Verachtert <dries@ulyssis.org> - 3.3-1 - 3768/dries
- Initial package.
