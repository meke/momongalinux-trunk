%global         momorel 2

Name:           perl-Path-Tiny
Version:        0.054
Release:        %{momorel}m%{?dist}
Summary:        File path utility
License:        Apache
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Path-Tiny/
Source0:        http://www.cpan.org/authors/id/D/DA/DAGOLDEN/Path-Tiny-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= v5.10.0
BuildRequires:  perl-autodie >= 2.00
BuildRequires:  perl-constant
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Fcntl
BuildRequires:  perl-File-Path >= 2.07
BuildRequires:  perl-File-Temp >= 0.18
BuildRequires:  perl-Path-Class
BuildRequires:  perl-Test-Deep
BuildRequires:  perl-Test-FailWarnings
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple >= 0.96
BuildRequires:  perl-Unicode-UTF8
Requires:       perl-autodie >= 2.00
Requires:       perl-constant
Requires:       perl-Cwd
Requires:       perl-Fcntl
Requires:       perl-File-Path >= 2.07
Requires:       perl-File-Temp >= 0.18
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module attempts to provide a small, fast utility for working with file
paths. It is friendlier to use than File::Spec and provides easy access to
functions from several other core file handling modules.

%prep
%setup -q -n Path-Tiny-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CONTRIBUTING dist.ini LICENSE META.json perlcritic.rc tidyall.ini
%{perl_vendorlib}/Path/Tiny.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.054-2m)
- rebuild against perl-5.20.0

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.054-1m)
- update to 0.054

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.052-1m)
- update to 0.052
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.051-1m)
- update to 0.051

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.046-1m)
- update to 0.046

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.044-1m)
- update to 0.044

* Mon Oct 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.043-1m)
- update to 0.043

* Mon Oct 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.042-1m)
- update to 0.042

* Sat Oct 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.041-1m)
- update to 0.041

* Wed Oct  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.040-1m)
- update to 0.040

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.038-1m)
- update to 0.038

* Fri Sep 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.037-1m)
- update to 0.037

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.033-1m)
- update to 0.033

* Sun Sep  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.032-1m)
- update to 0.032

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.031-1m)
- update to 0.031

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.030-1m)
- update to 0.030

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.029-1m)
- update to 0.029

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.028-1m)
- update to 0.028
- rebuild against perl-5.18.1

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.027-1m)
- update to 0.027

* Tue Jul 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.026-1m)
- update to 0.026

* Fri Jul 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.025-1m)
- update to 0.025

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.024-1m)
- update to 0.024

* Thu Jun 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.023-1m)
- update to 0.023

* Thu May 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.022-1m)
- update to 0.022

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.021-2m)
- rebuild against perl-5.18.0

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.021-1m)
- update to 0.021

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.020-1m)
- update to 0.020

* Fri Apr 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.019-1m)
- update to 0.019

* Tue Apr  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.018-1m)
- update to 0.018

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.017-1m)
- update to 0.017

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.016-1m)
- update to 0.016

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.015-1m)
- update to 0.015
- rebuild against perl-5.16.3

* Sun Mar 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.014-1m)
- update to 0.014

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.012-1m)
- update to 0.012

* Sat Feb 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009-1m)
- update to 0.009

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.007-1m)
- update to 0.007

* Tue Feb 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.006-1m)
- update to 0.006

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.004-1m)
- update to 0.004

* Sun Feb 03 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.003-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
