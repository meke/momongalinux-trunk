%global momorel 12
%global honokaver 0.9.1
%global honokarel 13m

Summary: An Anthy input plugin for honoka
Name: honoka-plugin-anthy
Version: 0.9.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://sourceforge.jp/projects/scim-imengine/
Source0: http://nop.net-p.org/files/honoka/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: anthy
Requires: honoka-plugin-ascii     >= 0.9.1-3m
Requires: honoka-plugin-kanainput >= 0.9.1-3m
Requires: honoka-plugin-romkan    >= 0.9.0-3m
Requires: honoka-plugin-simpleprediction >= 0.9.0-3m
BuildRequires: honoka-devel >= %{honokaver}-%{honokarel}
BuildRequires: anthy-devel
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: automake19
BuildRequires: coreutils
BuildRequires: libtool-ltdl-devel

%description
An Anthy input plugin for honoka.

%prep
%setup -q

cp -f /usr/share/automake-1.9/mkinstalldirs .

%build
[[ -f configure ]] || ./bootstrap

%configure
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# remove devel files
rm -f %{buildroot}/%{_libdir}/scim-1.0/1.4.0/honoka/*.{a,la}

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README.jp
%{_libdir}/scim-1.0/1.4.0/honoka/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-10m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-9m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-5m)
- %%NoSource -> NoSource

* Mon Oct 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-4m)
- update honoka_version

* Wed Oct 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-3m)
- modify %%files (for scim-1.4.5)

* Mon Jul  3 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-2m)
- global "honoka_version   0.9.0"

* Sun Jul  2 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-1m)
- initial commit Momonga Linux

