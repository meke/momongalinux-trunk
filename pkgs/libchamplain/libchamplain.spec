%global momorel 1
Summary:	Map view for Clutter
Name:		libchamplain
Version:	0.12.3
Release: %{momorel}m%{?dist}
License:	LGPLv2+
Group:		System Environment/Libraries
URL:		http://projects.gnome.org/libchamplain/
Source0:	http://download.gnome.org/sources/libchamplain/0.12/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: champlain-grr.patch

Requires:	gobject-introspection

BuildRequires:	chrpath
BuildRequires:	clutter-devel
BuildRequires:	clutter-gtk-devel
BuildRequires:	libsoup-devel
BuildRequires:	sqlite-devel
BuildRequires:	gtk3-devel
BuildRequires:	gtk-doc
BuildRequires:	vala-devel
BuildRequires:	vala-tools

BuildRequires: autoconf automake libtool

%description
Libchamplain is a C library aimed to provide a ClutterActor to display
rasterized maps.

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	gobject-introspection-devel
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains development files for %{name}.

%package gtk
Summary:	Gtk+ widget wrapper for %{name}
Group:		System Environment/Libraries
Requires:	%{name} = %{version}-%{release}

%description gtk
Libchamplain-gtk is a library providing a GtkWidget to embed %{name}
into Gtk+ applications.

%package gtk-devel
Summary:	Development files for %{name}-gtk
Group:		Development/Libraries
Requires:	%{name}-devel = %{version}-%{release}
Requires:	%{name}-gtk = %{version}-%{release}

%description gtk-devel
This package contains development files for %{name}-gtk.

%package vala
Summary:	Vala bindings for %{name}
Group:		Development/Libraries
Requires:	%{name}-devel = %{version}-%{release}
Requires:	vala

%description vala
This package contains vala bindings for development %{name}.

%package demos
Summary:	Demo apps for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	%{name}-devel = %{version}-%{release}
Requires:	%{name}-gtk-devel = %{version}-%{release}
BuildArch:      noarch

%description demos
This package contains demos for development using %{name}.

%prep
%setup -q
%patch0 -p1

autoreconf -i -f

%build
%configure --disable-debug --disable-silent-rules --disable-static \
  --enable-gtk --enable-gtk-doc --enable-introspection=yes --enable-vala \
  --enable-vala-demos

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

%make 

%install
make install INSTALL="%{__install} -p" DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -delete

# Remove rpaths.
chrpath --delete %{buildroot}%{_libdir}/%{name}-gtk-*.so.*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post gtk -p /sbin/ldconfig

%postun gtk -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README NEWS
%{_libdir}/girepository-1.0/Champlain-0.12.typelib
%{_libdir}/%{name}-0.12.so.*

%files devel
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc/html/libchamplain
%{_datadir}/gir-1.0/Champlain-0.12.gir
%{_libdir}/%{name}-0.12.so
%{_libdir}/pkgconfig/champlain-0.12.pc
%{_includedir}/%{name}-0.12

%files gtk
%defattr(-,root,root,-)
%{_libdir}/girepository-1.0/GtkChamplain-0.12.typelib
%{_libdir}/%{name}-gtk-0.12.so.*

%files gtk-devel
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc/html/libchamplain-gtk
%{_datadir}/gir-1.0/GtkChamplain-0.12.gir
%{_libdir}/%{name}-gtk-0.12.so
%{_libdir}/pkgconfig/champlain-gtk-0.12.pc
%{_includedir}/%{name}-gtk-0.12

%files vala
%defattr(-,root,root,-)
%{_datadir}/vala/vapi/champlain-0.12.vapi
%{_datadir}/vala/vapi/champlain-gtk-0.12.vapi

%files demos
%defattr(-,root,root,-)
%doc demos/*.c
%doc demos/*.h
%doc demos/*.vala
%doc demos/*.js
%doc demos/Makefile*
%doc demos/*.py
%doc demos/*.osm

%changelog
* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.3-1m)
- update to 0.12.3

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.2-1m)
- reimport from fedora

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-4m)
- rebuild for cogl-1.10.2

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-3m)
- fix build failure with glib 2.33+

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- rebuild against cogl-1.8.0
- (0.12.0-2m)

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.0-2m)
- delete duplicated file

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.0-1m)
- update to 0.11.0

* Sat Aug 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.0-2m)
- fix BR

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Wed Apr 13 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-5m)
- remove BR: pyclutter-gtk-devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-2m)
- rebuild against gobject-introspection-0.9.10

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-3m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.1-2m)
- separate gtk and gtk-devel

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-2m)
- vapi and gir to devel

* Tue Jun  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-1m)
- update to 0.4.6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-2m)
- use BuildRequires

* Mon Jan 11 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- BuildPrereq: clutter-devel >= 1.0.4-2m
- BuildPrereq: gobject-introspection-devel

* Tue Sep 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.92-2m)
- use python_sitearch

* Sun Sep  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.92-1m)
- update to 0.3.92
- enable pyclutter
-- add patch0 (but not work so good)

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.91-1m)
- initial build
-- disable-python (need clutter-0.8 pyclutter-0.8 to old)
-- disable-managed (need clutter-sharp)
