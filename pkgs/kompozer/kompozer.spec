%global momorel 7
%global nspr_version 4.8.2
%global nss_version 3.12.4
%global srcname %{name}-%{version}b1-src-dfsg

Summary: A WYSIWYG HTML editor
Name: kompozer
Version: 0.8
Release: 0.1.%{momorel}m%{?dist}
URL: http://www.kompozer.net/
License: MPLv1.1 or GPLv2+ or LGPLv2+
Group: Applications/Editors
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{srcname}.tar.bz2
NoSource: 0
Source4: kompozer.desktop
Patch21: kompozer-0.7.10-CVE-2009-XXXX.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: nspr-devel >= %{nspr_version}
BuildRequires: nss-devel >= %{nss_version}
BuildRequires: cairo-devel
BuildRequires: desktop-file-utils
BuildRequires: freetype-devel >= 2.1.9
BuildRequires: gnome-vfs-devel
BuildRequires: gtk2-devel >= 2.8.3-2m
BuildRequires: krb5-devel
BuildRequires: libIDL-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: libgnome-devel
BuildRequires: libgnomeui-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: pango-devel
BuildRequires: zlib-devel
BuildRequires: ImageMagick
BuildRequires: desktop-file-utils
BuildRequires: hunspell
BuildRequires: sed
BuildRequires: zip
Requires: nspr >= %{nspr_version}
Requires: nss >= %{nss_version}
Obsoletes: kompozer-devel < 0.8

%description
KompoZer is a complete web authoring system that combines web file
management and easy-to-use WYSIWYG web page editing.

KompoZer is designed to be extremely easy to use, making it ideal for
non-technical computer users who want to create an attractive,
professional-looking web site without needing to know HTML or web
coding.

%prep
%setup -q -c %{name}-%{version}
%patch21 -p0 -b .CVE-2009-XXXX

%build
MOZ_OPT_FLAGS=$(echo "%{optflags} -fpermissive" | %{__sed} -e 's/-Wall//')
export CFLAGS=$MOZ_OPT_FLAGS
export CXXFLAGS=$MOZ_OPT_FLAGS

cd mozilla
%{__cp} -f composer/config/mozconfig.fedora .mozconfig
echo "ac_add_options --libdir %{_libdir}" >> .mozconfig
echo "ac_add_options --with-default-mozilla-five-home=%{_libdir}/kompozer" >> .mozconfig
echo "ac_add_options --with-distribution-id=org.momonga-linux" >> .mozconfig
sed -i -e 's|MOZILLA_OFFICIAL=1|MOZILLA_OFFICIAL=0|' .mozconfig
sed -i -e 's|BUILD_OFFICIAL=1|BUILD_OFFICIAL=0|' .mozconfig

%{__make} -f client.mk build_all

%install
%{__rm} -rf %{buildroot}

pushd obj-kompozer/xpfe/components && %{__make}; popd
pushd obj-kompozer && %{__make} install DESTDIR=%{buildroot}; popd

%{__mkdir_p} %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
    --dir %{buildroot}%{_datadir}/applications \
    %{SOURCE4}

%{__mkdir_p} %{buildroot}%{_datadir}/pixmaps
convert -scale 48x48 %{buildroot}%{_libdir}/kompozer/icons/mozicon50.xpm \
    %{buildroot}%{_datadir}/pixmaps/%{name}.png

rm -rf %{buildroot}%{_libdir}/kompozer/components/myspell/
rm -rf %{buildroot}%{_libdir}/kompozer/dictionaries/
pushd %{buildroot}%{_libdir}/kompozer
ln -s %{_datadir}/myspell %{buildroot}%{_libdir}/kompozer/dictionaries
popd

chmod -x %{buildroot}%{_libdir}/kompozer/components/*.js

rm -rf %{buildroot}/usr/include/
rm -rf %{buildroot}%{_datadir}/idl/
rm -f %{buildroot}%{_bindir}/kompozer-config
rm -rf %{buildroot}%{_libdir}/pkgconfig/
rm -rf %{buildroot}%{_libdir}/debug/

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc mozilla/LEGAL mozilla/LICENSE mozilla/README.txt
%{_bindir}/kompozer
%{_libdir}/kompozer
%{_datadir}/applications/kompozer.desktop
%{_datadir}/pixmaps/kompozer.png

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-0.1.7m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-0.1.6m)
- modify FLAGS to enable build by gcc46

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-0.1.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-0.1.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-0.1.3m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-0.1.2m)
- rebuild against libjpeg-8a

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-0.1.1m)
- update to 0.8b1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.10-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.10-7m)
- [SECURITY] CVE-2009-2625
- [SECURITY] Expat UTF-8 Parser DoS (Patch21)
- import Patch17-21 from Mandriva Cooker (0.7.10-6mdv2010.0)
- License: MPLv1.1 or GPLv2+ or LGPLv2+
-- my mistake

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.10-6m)
- rebuild against libjpeg-7

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.10-5m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.10-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.10-3m)
- update Patch13 for fuzz=0
- License: MPLv1.1 and GPLv2+ and LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.10-2m)
- rebuild against gcc43

* Thu Oct  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.10-1m)
- initial import (based on cooker 0.7.10-1mdv2008.0)

