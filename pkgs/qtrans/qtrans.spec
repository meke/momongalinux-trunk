%global momorel 12
%global srcrel 74876
%global qtver 4.8.0
%global kdever 4.8.1
%global kdelibsrel 1m

Summary: KDE instant translator
Name: qtrans
Version: 0.2.2.6
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://kde-apps.org/content/show.php/QTrans?content=74876
Group: Applications/Text
Source0: http://kde-apps.org/CONTENT/content-files/%{srcrel}-%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.2.2.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils

%description
QTrans is a Linux KDE instant translator that uses the Babylon dictionaries.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/qtrans.png %{buildroot}%{_datadir}/pixmaps/qtrans.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --remove-category X-Red-Hat-Base \
  --remove-category Application \
  --add-category Utility \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/QTrans.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog Copying README
%{_kde4_bindir}/qtrans
%{_kde4_datadir}/applications/kde4/QTrans.desktop
%{_kde4_iconsdir}/hicolor/*/apps/qtrans*.png
%{_kde4_datadir}/kde4/apps/qtrans
%{_datadir}/pixmaps/qtrans.png

%changelog
* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-12m)
- source tarball was replaced 
- use --rmsrc option

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-11m)
- unfortunately, source was replaced, but version was the same...
- we must use --rmsrc option to build

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-10m)
- source was replaced again and again...
- use --rmsrc option to build

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.6-9m)
- source was replaced
- use --rmsrc option

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-8m)
- source was replaced
- use --rmsrc option

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-7m)
- source was changed without version up again and again
- USE --rmsrc option

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-6m)
- source was changed without version up
- USE --rmsrc option

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-5m)
- update source, use --rmsrc option

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2.6-4m)
- rebuild for new GCC 4.6

* Wed Mar 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-3m)
- update sources, use --rmsrc option

* Tue Feb 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-2m)
- update sources

* Mon Dec 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.6-1m)
- version 0.2.2.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.5-8m)
- fix build error

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2.5-7m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.5-6m)
- source tar archive was changed, please remove downloaded source and build

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.5-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2.5-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2.5-3m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2.5-2m)
- explicitly link libz

* Mon Mar  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.5-1m)
- version 0.2.2.5

* Sat Feb 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.4-1m)
- version 0.2.2.4

* Fri Jan 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.3-1m)
- version 0.2.2.3

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.2-1m)
- version 0.2.2.2

* Mon Dec 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.1-1m)
- version 0.2.2.1
- update desktop.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Nov  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2.0-1m)
- version 0.2.2.0

* Sun Oct 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.11-2m)
- replace tar-ball to new source

* Thu Oct 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.11-1m)
- version 0.2.1.11

* Sat Sep 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.10-4m)
- version 0.2.1.10-4

* Tue Aug 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.10-3m)
- version 0.2.1.10-2

* Wed Jul  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.10-2m)
- version 0.2.1.10-1

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.10-1m)
- version 0.2.1.10

* Mon May 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.9-2m)
- version 0.2.1.9-1

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.9-1m)
- version 0.2.1.9

* Wed May 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.8-1m)
- version 0.2.1.8

* Sun May  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.7-1m)
- version 0.2.1.7

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.6-1m)
- version 0.2.1.6

* Mon Mar  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.5-1m)
- version 0.2.1.5

* Tue Feb 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1.4-3m)
- source was updated to 0.2.1.4-3

* Tue Feb 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1.4-2m)
- source was updated to 0.2.1.4-2

* Sat Feb 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1.4-1m)
- initial package for Momonga Linux
