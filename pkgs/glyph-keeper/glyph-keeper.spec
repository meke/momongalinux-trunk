%global momorel 12

Summary: a C language library for text rendering
Name: glyph-keeper
Version: 0.32
Release: %{momorel}m%{?dist}
License: "See lisence.txt"
Group: Development/Libraries
Source0: http://kirill-kryukov.com/%{name}/files/%{name}-%{version}.zip
NoSource: 0
URL: http://kirill-kryukov.com/glyph-keeper/
Patch0: glyph-keeper-0.32-math.patch
Patch1: glyph-keeper-0.32-pthread.patch
Patch2: glyph-keeper-0.32-link.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libXext-devel
BuildRequires: libXpm-devel
BuildRequires: libXcursor-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libXxf86dga-devel
BuildRequires: libX11-devel
BuildRequires: allegro-devel

%description
Glyph Keeper is a C language library for text rendering. Glyph Keeper
helps your program to load a font, render character glyphs and write
them to the target surface. Right now only Allegro and SDL targets are
supported, but there will be more in future. Glyph Keeper is free,
portable, effecient and easy to use. If you are new here, you are
welcome to see the Introduction page.

%prep
%setup -q
%patch0 -p1 -b .math
%patch1 -p1 -b .pthread
%patch2 -p1 -b .link

%build
make -f Makefile.GNU.all TARGET=TEXT all
make -f Makefile.GNU.all TARGET=ALLEGRO all
#make -f Makefile.GNU.all TARGET=SDL all

%install
rm -rf --preserve-root %{buildroot}
install -d %{buildroot}%{_includedir} \
    %{buildroot}%{_libdir} \
    %{buildroot}%{_datadir}/%{name}
make -f Makefile.GNU.all TARGET=TEXT \
    includedir_u=%{buildroot}%{_includedir} \
    libdir_u=%{buildroot}%{_libdir} \
    install
make -f Makefile.GNU.all TARGET=ALLEGRO \
    includedir_u=%{buildroot}%{_includedir} \
    libdir_u=%{buildroot}%{_libdir} \
    install
#make -f Makefile.GNU.all TARGET=SDL \
#    includedir_u=%{buildroot}%{_includedir} \
#    libdir_u=%{buildroot}%{_libdir} \
#    install
cp -ar benchmark docs examples %{buildroot}%{_datadir}/%{name}/

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc authors.txt changes.txt license.txt quotes.txt readme.txt releng.txt
%{_includedir}/glyph.h
%{_libdir}/libglyph-alleg.a
%{_libdir}/libglyph-text.a
%{_datadir}/%{name}

%changelog
* Wed Aug 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-12m)
- change primary website and Source0 URLs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.32-9m)
- full rebuild for mo7 release

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-8m)
- fix build with gcc-4.4.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jan 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.32-6m)
- add patch1 (link pthread)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32-4m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.32-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.32-2m)
- %%NoSource -> NoSource

* Mon Aug 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.32-1m)
- initial build
