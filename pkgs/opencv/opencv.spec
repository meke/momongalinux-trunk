%global momorel 2
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           opencv
Version:        2.4.5
Release: 	%{momorel}m%{?dist}
Summary:        Collection of algorithms for computer vision

Group:          Development/Libraries
# This is normal three clause BSD.
License:        BSD
URL:            http://opencv.willowgarage.com/wiki/
Source0:        http://dl.sourceforge.net/project/opencvlibrary/opencv-unix/%{version}/%{name}-%{version}.tar.gz
NoSource:	0
Source1:        opencv-samples-Makefile
Patch0:         opencv-pkgcmake.patch
Patch1:         opencv-pkgcmake2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%{?include_specopt}
## Configuration
%{!?do_test:            %global do_test                    0}
%{!?with_ffmpeg:        %global with_ffmpeg                1}
%{!?with_python:        %global with_python                1}
%{!?with_libdc1394:     %global with_libdc1394             1}
%{!?with_xine:          %global with_xine                  1}
%{!?with_gstreamer:	%global with_gstreamer		   1}
%{!?with_cuda:		%global	with_cuda		   0}
%{!?cuda_topdir:	%global cuda_topdir		   /usr}
%{!?with_openni:	%global with_openni		   1}
BuildRequires:  libtool
BuildRequires:  cmake >= 2.6.3
BuildRequires:  chrpath
BuildRequires:  f2c

BuildRequires:  eigen2-devel
BuildRequires:  gtk2-devel
BuildRequires:  ilmbase >= 1.0.3
BuildRequires:  imlib2-devel
BuildRequires:  libucil-devel
BuildRequires:  libtheora-devel
BuildRequires:  libvorbis-devel
%ifnarch s390 s390x
BuildRequires:  libraw1394-devel
BuildRequires:  libdc1394-devel
%endif
BuildRequires:  jasper-devel
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  libv4l-devel
BuildRequires:  OpenEXR-devel >= 1.7.1
%if %{with_openni}
BuildRequires:  openni-devel >= 1.3.1.2
BuildRequires:  openni-primesense >= 5.0.3.3
%endif
%ifarch %{ix86} x86_64 ia64
BuildRequires:  tbb-devel >= 4.0
%endif
BuildRequires:  zlib-devel, pkgconfig
%if %{with_python}
BuildRequires:  python-devel
BuildRequires:  python-imaging, numpy, swig >= 1.3.24
BuildRequires:  numpy-f2py
BuildRequires:  python-sphinx
%endif
%{?with_ffmpeg:BuildRequires:  ffmpeg-devel >= 0.4.9}
%if %{with_gstreamer}
BuildRequires:  gstreamer-devel gstreamer-plugins-base-devel
%endif
%{?with_xine:BuildRequires:  xine-lib-devel}

%description
OpenCV means Intel® Open Source Computer Vision Library. It is a collection of
C functions and a few C++ classes that implement some popular Image Processing
and Computer Vision algorithms.


%package devel
Summary:        Development files for using the OpenCV library
Group:          Development/Libraries
Requires:       opencv = %{version}-%{release}
Requires:       pkgconfig

%description devel
This package contains the OpenCV C/C++ library and header files, as well as
documentation. It should be installed if you want to develop programs that
will use the OpenCV library. You should consider installing opencv-devel-docs
package.

%package devel-docs
Summary:        Development files for using the OpenCV library
Group:          Development/Libraries
Requires:       opencv-devel = %{version}-%{release}
Requires:       pkgconfig
BuildArch:      noarch

%description devel-docs
This package contains the OpenCV documentation and examples programs.

%if %{with_python}
%package python
Summary:        Python bindings for apps which use OpenCV
Group:          Development/Libraries
Requires:       opencv = %{version}-%{release}
Requires:       python-imaging
Requires:       numpy

%description python
This package contains Python bindings for the OpenCV library.
%endif

%prep
%setup -q
%patch0 -p1 -b .pkgcmake
%patch1 -p1 -b .pkgcmake2

# fix dos end of lines
sed -i 's|\r||g'  samples/c/adaptiveskindetector.cpp


%build
mkdir -p build
pushd build
# enabled by default if libraries are presents at build time:
# GTK, GSTREAMER, UNICAP, 1394, V4L
# non available on Fedora: FFMPEG, XINE
%cmake CMAKE_VERBOSE=1 \
 -DPYTHON_PACKAGES_PATH=%{python_sitearch} \
 -DCMAKE_SKIP_RPATH=ON \
%ifnarch x86_64 ia64
 -DENABLE_SSE=0 \
 -DENABLE_SSE2=0 \
%endif
 -DCMAKE_BUILD_TYPE=Release \
 -DBUILD_TEST=1 \
%ifarch %{ix86} x86_64 ia64
 -DWITH_TBB=1 -DTBB_LIB_DIR=%{_libdir} \
%endif
 %{?_without_gstreamer:-DWITH_GSTREAMER=0} \
 %{!?_with_ffmpeg:-DWITH_FFMPEG=0} \
%if %{with_cuda}
 -DCUDA_TOOLKIT_ROOT_DIR=%{?_cuda_topdir} \
 -DCUDA_VERBOSE_BUILD=1 \
 -DCUDA_PROPAGATE_HOST_FLAGS=0 \
%endif
%ifarch %{ix86} x86_64
 %{with_openni:-DWITH_OPENNI=ON} \
%endif
 -DWITH_XINE=0 \
 -DINSTALL_C_EXAMPLES=1 \
 -DINSTALL_PYTHON_EXAMPLES=1 \
 ..

%make VERBOSE=1

popd


%install
rm -rf %{buildroot}  __devel-doc
pushd build
make install DESTDIR=%{buildroot} INSTALL="install -p" CPPROG="cp -p"
find %{buildroot} -name '*.la' -exec rm -f {} ';'


rm -f %{buildroot}%{_datadir}/%{name}/samples/c/build_all.sh \
      %{buildroot}%{_datadir}/%{name}/samples/c/cvsample.dsp \
      %{buildroot}%{_datadir}/%{name}/samples/c/cvsample.vcproj \
      %{buildroot}%{_datadir}/%{name}/samples/c/facedetect.cmd
install -pm644 %{SOURCE1} %{buildroot}%{_datadir}/OpenCV/samples/c/GNUmakefile

# remove unnecessary documentation
rm -rf %{buildroot}%{_datadir}/OpenCV/doc

popd

#Cmake mess
mkdir -p  %{buildroot}%{_libdir}/cmake/OpenCV
mv %{buildroot}%{_datadir}/OpenCV/*.cmake \
  %{buildroot}%{_libdir}/cmake/OpenCV

%check
# Check fails since we don't support most video
# read/write capability and we don't provide a display
# ARGS=-V increases output verbosity
# Make test is unavailble as of 2.3.1
%if 0
#ifnarch ppc64
pushd build
    LD_LIBRARY_PATH=%{_builddir}/%{tar_name}-%{version}/lib:$LD_LIBARY_PATH make test ARGS=-V || :
popd
%endif

%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig



%files
%defattr(-,root,root,-)
%doc doc/license.txt
%{_bindir}/opencv_*
%{_libdir}/lib*.so.*
%dir %{_datadir}/OpenCV
%{_datadir}/OpenCV/haarcascades
%{_datadir}/OpenCV/lbpcascades
%{_datadir}/OpenCV/java


%files devel
%defattr(-,root,root,-)
%{_includedir}/opencv
%{_includedir}/opencv2
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/opencv.pc
# own cmake dir avoiding dep on cmake
%{_libdir}/cmake/


%files devel-docs
%defattr(-,root,root,-)
%doc doc/*.{htm,png,jpg}
%doc %{_datadir}/OpenCV/samples
%doc %{_datadir}/opencv/samples

%files python
%defattr(-,root,root,-)
%{python_sitearch}/cv.py*
%{python_sitearch}/cv2.so


%changelog
* Tue Jun 18 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.5-2m)
- fix specopt support

* Sat Jun  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-5m)
- rebuild against libtiff-4.0.1

* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-4m)
- add BuildRequires

* Thu Oct 27 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-3m)
- rebuild against tbb-devel >= 4.0

* Tue Oct 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-2m)
- update to 2.3.1a

* Mon Aug 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1
- merge from fedora

* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-6m)
- fix v4l issue
- revise spec

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-4m)
- rebuild for new GCC 4.6

* Mon Feb  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-3m)
- add patch for GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-1m)
- update 2.1.0
-- import 5 patches from fedora

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-4m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-3m)
- rebuild against libjpeg-8a

* Fri Mar 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-2m)
- add with_libdc1394 and with_xine switch

* Mon Mar  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0
- remove merged gcc44.patch
- import cmake-libdir.patch from Fedora
- good-bye autotools and hello cmake

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-13m)
- delete __libtoolize hack

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-12m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-10m)
- rebuild against libjpeg-7

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-9m)
- fix build with new libtool

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-8m)
- apply gcc44 patch
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-7m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-6m)
- go to main for kipi-plugins4
- build without ffmpeg for the moment
- modify specopt, URL, BR, configure %%install and %%files section
- add %%post and %%postun

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-4m)
- %%NoSource -> NoSource

* Sat Jun 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- Added %dir tags

* Thu Apr 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- revise spec
-- fix source url
-- add %%check section
 
* Fri Jan  5 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-1m)
- Initial import
