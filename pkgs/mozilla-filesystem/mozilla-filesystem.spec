%global momorel 6

Name:           mozilla-filesystem
Version:        1.9
Release:        %{momorel}m%{?dist}
Summary:        Mozilla filesytem layout
Group:          Applications/Internet
License:        MPL
#BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package provides some directories required by packages which use
Mozilla technologies such as NPAPI plugins or toolkit extensions.

%prep

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/usr/{lib,%{_lib}}/mozilla/{plugins,extensions}
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mozilla/extensions
#mkdir -p $RPM_BUILD_ROOT/etc/skel/.mozilla/{plugins,extensions}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
/usr/lib*/mozilla
%{_datadir}/mozilla
#/etc/skel/.mozilla

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-2m)
- rebuild against rpm-4.6

* Sun May  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-1m)
- import from Fedora to Momonga

* Wed Apr 30 2008 Christopher Aillon <caillon@redhat.com> 1.9-2
- Also own the */mozilla parent dirs

* Wed Apr 30 2008 Christopher Aillon <caillon@redhat.com> 1.9-1
- Initial RPM
