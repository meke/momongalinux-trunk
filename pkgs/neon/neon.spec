%global momorel 5

Summary: neon is an HTTP and WebDAV client library, with a C interface
Name: neon
%{?include_specopt}
%{?!with_libproxy: %global with_libproxy 1}
Version: 0.29.6
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.webdav.org/neon/
Source0: http://www.webdav.org/neon/%{name}-%{version}.tar.gz 
NoSource: 0
Patch1: neon-0.27.0-multilib.patch
Patch2: neon-0.29.3-ignore-tests.patch
Patch3: neon-0.29.6-gnutls3.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: expat-devel, gnutls-devel >= 2.6.6-2m, zlib-devel, krb5-devel
BuildRequires: pkgconfig, pakchois-devel
BuildRequires: gnutls >= 3.2.0
%if %{with_libproxy}
BuildRequires: libproxy-devel >= 0.4.4
%endif
BuildRequires: pkgconfig, pakchois-devel

%description
neon is an HTTP and WebDAV client library, with a C interface. Featuring:

    * High-level interface to HTTP and WebDAV methods (PUT, GET, HEAD etc)
    * Low-level interface to HTTP request handling, to allow implementing new methods easily.
    * persistent connections
    * RFC2617 basic and digest authentication (including auth-int, md5-sess)
    * Proxy support (including basic/digest authentication)
    * SSL/TLS support using OpenSSL (including client certificate support)
    * Generic WebDAV 207 XML response handling mechanism
    * XML parsing using the expat or libxml parsers
    * Easy generation of error messages from 207 error responses
    * WebDAV resource manipulation: MOVE, COPY, DELETE, MKCOL.
    * WebDAV metadata support: set and remove properties, query any set of properties (PROPPATCH/PROPFIND).
    * autoconf macros supplied for easily embedding neon directly inside an application source tree. 

%package devel
Summary: Header files and libraries for developing apps which will use neon.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The neon-devel package contains the header files and libraries needed
to develop programs that use the neon library.

%prep
%setup -q
%patch1 -p1 -b .multilib
%patch2 -p1 -b .ignore-tests
%patch3 -p1 -b .gnutls3

%build
%global cabundle %{_sysconfdir}/pki/tls/certs/ca-bundle.crt
%configure --with-expat --enable-shared --enable-warnings \
           --enable-warnings --with-ca-bundle=%{cabundle} \
 	   --with-ssl=gnutls --enable-threadsafe-ssl=posix \
%if %{with_libproxy}
           --with-libproxy
%else
           --without-libproxy
%endif

%make

%install
rm -rf %{buildroot}
%makeinstall

%{find_lang} %{name}

%check
make -C test %{?_smp_mflags} check || true

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS BUGS TODO src/COPYING.LIB NEWS README THANKS
%{_libdir}/libneon.so.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/neon-config
%{_includedir}/neon
%{_libdir}/libneon.so
%{_libdir}/libneon.a
%{_libdir}/libneon.la
%{_libdir}/pkgconfig/neon.pc
%{_mandir}/man?/*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29.6-5m)
- rebuild against gnutls-3.2.0

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.29.6-4m)
- use ssl=gnutls
-- fixed gnome-keyring WARNING

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.29.6-3m)
- use ssl=openssl
-- fix gnome-keyring WARNING

* Fri Dec 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29.6-2m)
- add specopt flag to disable libproxy support

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.29.6-1m)
- update 0.29.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29.5-2m)
- rebuild for new GCC 4.6

* Mon Jan 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.29.5-1m)
- update 0.29.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29.3-3m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29.3-2m)
- rebuild against libproxy-0.4.4

* Thu May 20 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29.3-1m)
- update 0.29.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.28.6-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.28.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.28.6-2m)
- ignore new tests (fail_nul_cn and fail_nul_san of ssl tests).
  these tests try to connect https://www.bank.com:7777, but we can not do that.
- rebuild against gnutls-2.6.6-2m

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.28.6-1m)
- [SECURITY] CVE-2009-2473 CVE-2009-2474
- update 0.28.6

* Mon Mar 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.28.4-1m)
- update 0.28.4

* Thu Jan 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28.3-3m)
- import multilib patch from fedora
- revise BuildPreReq: and Requires:
-- neon doesn't use openssl and libxml2 any longer.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.28.3-2m)
- rebuild against rpm-4.6

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.28.3-1m)
- update 0.28.3
-- CVE-2008-3746

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28.2-3m)
- rebuild against gnutls-2.4.1

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.28.2-1m)
- update 0.28.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.28.1-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.28.1-1m)
- update 0.28.1

* Sat Mar  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.28.0-1m)
- update 0.28.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25.5-7m)
- %%NoSource -> NoSource

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.25.5-6m)
- retrived libtool library

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.25.5-5m)
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.25.5-4m)
- rebuild against expat-2.0.0-1m

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.25.5-3m)
- rebuild against openssl-0.9.8a

* Sat Feb 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.25.5-2m)
- sync with fc-devel

* Sun Jan 22 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25.5-1m)
- updated to 0.25.5

* Wed Jan  4 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (0.25.4-1m)
- updated to 0.25.4
  (subversion 1.3.0 requires 0.25.1 or newer to enable 'Interruptible http(s):// connections')

  'Interruptible http(s):// connections'
    If built with Neon 0.25.1 or higher, Subversion can be interrupted during network operations
    over http:// and https://. This is a long-standing bug that has finally been fixed.

* Tue Jul 20 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24.7-1m)
- update to 0.24.7

* Wed May 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.24.6-1m)
- update to 0.24.6
- SECURITY
    CVE CAN-2004-0398 http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0398
    Fix sscanf overflow in ne_rfc1036_parse, thanks to Stefan Esser.
- Link libneon against libexpat during Subversion build using bundled neon.
- Win32 build script update (Jon Foster). 

* Fri Apr 16 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24.5-1m)
- update to 0.24.5
- security fix
- http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0179
- http://rhn.redhat.com/errata/RHSA-2004-159.html
- delete patch1

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (0.24.3-3m)
- revised spec for rpm 4.2.
- applied wrongver patch to fix neon.m4 so that html is installed as 0.24.3.

* Mon Mar  8 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.24.3-2m)
- add %%defattr(-,root,root,-)

* Thu Jan  8 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.24.3-2m)
- separate package into neon and neon-devel
- disable krb5

* Tue Oct 14 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.24.3-1m)
- update to 0.24.3 (required by subversion-0.31)
- revise BuildRoot
- add %%{_libdir}/pkgconfig/neon.pc to %%files

* Sat Apr 26 2003  <tom@tom.homelinux.org>
- Initial build.
