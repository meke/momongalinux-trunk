%global momorel 1
%global relname Vesta
%global kde_release %{version}-%{release}.%{relname}
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global distname Momonga Linux
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global atticaver 0.4.2
%global docbookver 4.5
%global docbookrel 10m%{?dist}
%global phononver 4.7.1
%global polkitqtver 0.103.0
%global sopranover 2.9.4
%global sopranorel 1m
%global strigiver 0.7.8
%global shared_desktop_ontologies_version 0.11.0

%global apidocs 1
# to build/include QCH apidocs or not (currently broken)
#global apidocs_qch 1
%global libplasma_pk 1

Summary: K Desktop Environment 4 - Libraries
Name: kdelibs
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: System Environment/Libraries
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0

# Momonga Menus
Source10: applications.menu.momonga
Source11: mo-server-settings.menu
Source12: mo-system-settings.menu

Source20: kde.sh
Source21: kde.csh

# make -devel packages parallel-installable
Patch0: kdelibs-4.9.95-parallel_devel.patch
# fix http://bugs.kde.org/149705
Patch2: kdelibs-4.10.0-kde149705.patch
# install all .css files and Doxyfile.global in kdelibs-common to build
# kdepimlibs-apidocs against
Patch8: kdelibs-4.3.90-install_all_css.patch
# add Fedora/V-R to KHTML UA string
#Patch9: kdelibs-4.0.2-branding.patch
#Patch12: kdelibs-4.0.72-xdg-menu.patch
# Momonga build type - adds -DNDEBUG, removes -O2 -g (already in RPM_OPT_FLAGS)
Patch13: kdelibs-4.9.1-momonga-buildtype.patch
# patch KStandardDirs to use %{_libexecdir}/kde4 instead of %{_libdir}/kde4/libexec
Patch14: kdelibs-4.11.3-libexecdir.patch
# kstandarddirs changes: search /etc/kde, find %{_kde4_libexecdir}
Patch18: kdelibs-4.11.97-kstandarddirs.patch
# set build type
Patch20: kdelibs-4.10.0-cmake.patch
# die rpath die, since we're using standard paths, we can avoid
# this extra hassle (even though cmake is *supposed* to not add standard
# paths (like /usr/lib64) already! With this, we can drop
# -DCMAKE_SKIP_RPATH:BOOL=ON (finally)
Patch27: kdelibs-4.10.0-no_rpath.patch

# Trigger installation of missing components when installing a package.
# https://git.reviewboard.kde.org/r/102291/
Patch41: 0002-Trigger-installation-of-missing-components-when-inst.patch
# Implement automatic scanning of source code for required data engines.
# https://git.reviewboard.kde.org/r/102350/
Patch42: 0003-Implement-automatic-scanning-of-source-code-for-requ.patch

# limit solid qDebug spam
# http://bugzilla.redhat.com/882731
# TODO: could make uptreamable and conditional only on Release-type builds
Patch49: kdelibs-solid_qt_no_debug_output.patch

## upstreamable
# knewstuff2 variant of:
# https://git.reviewboard.kde.org/r/102439/
Patch50: kdelibs-4.7.0-knewstuff2_gpg2.patch
# Toggle solid upnp support at runtime via env var SOLID_UPNP=1 (disabled by default)
Patch52: kdelibs-4.10.0-SOLID_UPNP.patch
# add s390/s390x support in kjs
Patch53: kdelibs-4.7.2-kjs-s390.patch
# return valid locale (RFC 1766)
Patch54: kdelibs-4.8.4-kjs-locale.patch
# make filter working, TODO: upstream?  -- rex
Patch59: kdelibs-4.9.3-kcm_ssl.patch
# disable dot to reduce apidoc size
Patch61: kdelibs-4.12.90-dot.patch
# set QT_NO_GLIB in klauncher_main.cpp as a possible fix/workaround for #983110
Patch63: kdelibs-4.11.3-klauncher-no-glib.patch

## upstream
# revert these commits for
#https://bugs.kde.org/315578
# for now, causes regression,
#https://bugs.kde.org/317138
Patch90: return-not-break.-copy-paste-error.patch
Patch91: coding-style-fixes.patch
Patch92: return-application-icons-properly.patch
# revert disabling of packagekit
Patch93: turn-the-packagekit-support-feature-off-by-default.patch

Patch100: kdelibs-4.12.1-header.patch

# set useragent to Momonga
Patch400: kdelibs-4.6.95-useragent.patch

BuildRequires: qt-devel >= %{qtver}-%{qtrel}
BuildRequires: qt-webkit-devel >= %{qtver}-%{qtrel}
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: qt >= %{qtver}-%{qtrel}
Requires: kdelibs-common
Requires: kactivities >= %{version}
Requires: xdg-utils
Requires: hicolor-icon-theme
Requires: udisks2
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: alsa-lib-devel
BuildRequires: attica-devel >= %{atticaver}
BuildRequires: automoc
BuildRequires: avahi-devel
BuildRequires: bzip2-devel
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: cups-devel cups
BuildRequires: dbusmenu-qt-devel
BuildRequires: docbook-dtds = %{docbookver}-%{docbookrel}
BuildRequires: enchant-devel
BuildRequires: gamin-devel
BuildRequires: gettext-devel
BuildRequires: giflib-devel
BuildRequires: herqq-devel
# busted at least on x86_64
# BuildRequires: hspell-devel
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: jasper-devel
BuildRequires: krb5-devel
BuildRequires: libacl-devel
BuildRequires: libattr-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel
BuildRequires: libutempter-devel
BuildRequires: libxslt-devel
BuildRequires: libxml2-devel
BuildRequires: openssh-clients
BuildRequires: openssl-devel
BuildRequires: pcre-devel >= 8.31
BuildRequires: phonon-devel >= %{phononver}
BuildRequires: polkit-qt-devel >= %{polkitqtver}
BuildRequires: sed
BuildRequires: shared-desktop-ontologies-devel >= %{shared_desktop_ontologies_version}
BuildRequires: shared-mime-info
BuildRequires: soprano-devel >= %{sopranover}-%{sopranorel}
BuildRequires: strigi-devel >= %{strigiver}
BuildRequires: subversion
BuildRequires: systemd-devel >= 187
BuildRequires: libudisks2-devel
BuildRequires: xz-devel >= 5.0.0
BuildRequires: zlib-devel

# extra X deps (seemingly needed and/or checked-for by most kde4 buildscripts)
%define x_deps libXcomposite-devel libXdamage-devel libxkbfile-devel libXpm-devel libXScrnSaver-devel libXtst-devel libXv-devel libXxf86misc-devel
%{?x_deps:BuildRequires: %{x_deps}}

%if 0%{?apidocs}
BuildRequires: doxygen
BuildRequires: graphviz
BuildRequires: qt-doc
%else
Obsoletes: kdelibs-apidocs
%endif
Provides: kross(javascript) = %{version}-%{release}
Provides: kross(qtscript) = %{version}-%{release}
Obsoletes: kdelibs-experimental < %{version}
Provides: kdelibs-experimental = %{version}-%{release}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Conflicts: kdelibs3 < 3.5.10-13m

%description
Libraries for the K Desktop Environment 4.

%package common
Summary: Common files for KDE 3 and KDE 4 libraries
Group: System Environment/Libraries

%description common
This package includes the common files for the KDE 3 and KDE 4 libraries.

%package devel
Summary: Header files for compiling KDE 4 applications
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: qt-devel
Requires: automoc
Requires: docbook-dtds = %{docbookver}-%{docbookrel}
Requires: openssl-devel
Requires: phonon-devel >= 4.2.1
Requires: bzip2-devel
Requires: gamin-devel
Requires: libacl-devel
Requires: zlib-devel
Requires: strigi-devel >= 0.7.0
%{?x_deps:Requires: %{x_deps}}
Obsoletes: kdelibs-experimental-devel < %{version}
Provides: kdelibs-experimental-devel = %{version}-%{release}

%description devel
This package includes the header files you will need to compile
applications for KDE 4.

%package apidocs
Summary: KDE 4 API documentation
Group: Documentation
BuildArch: noarch
Requires: %{name} = %{version}

%description apidocs
This package includes the KDE 4 API documentation in HTML
format for easy browsing.

%package apidocs-qch
Group: Development/Documentation
Summary: KDE 4 API documentation for Qt Assistant
Requires: qt
BuildArch: noarch

%description apidocs-qch
This package includes the KDE 4 API documentation in Qt Assistant QCH
format for use with the Qt 4 Assistant or KDevelop 4.

%prep
%setup -q -n %{name}-%{version}

%patch0 -p1 -b .parallel_devel
%patch2 -p1 -b .kde149705
%patch8 -p1 -b .all-css
sed -i -e "s|@@VERSION_RELEASE@@|%{version}-%{release}|" kio/kio/kprotocolmanager.cpp
%patch13 -p1 -b .momonga-buildtype
%patch14 -p1 -b .libexecdir
%patch18 -p1 -b .kstandarddirs
%patch20 -p1 -b .cmake
%patch27 -p1 -b .no_rpath

# libplasma PackageKit integration
%patch41 -p1 -b .libplasma-pk-0002
%patch42 -p1 -b .libplasma-pk-0003
%patch49 -p1 -b .solid_qt_no_debug_output

# upstreamable patches
%patch50 -p1 -b .knewstuff2_gpg2
%patch52 -p1 -b .SOLID_UPNP
%patch53 -p1 -b .kjs-s390
%patch54 -p1 -b .kjs-locale
%patch59 -p1 -b .filter
%patch61 -p1 -b .dot
%patch63 -p1 -b .klauncher-no-glib

# upstream patches
%patch90 -p1 -R -b .return-not-break.-copy-paste-error
%patch91 -p1 -R -b .coding-style-fixes.patch
%patch92 -p1 -R -b .return-application-icons-properly
%patch93 -p1 -R -b .turn-the-packagekit-support-feature-off-by-default

%patch100 -p1 -b .qfile

# set useragent
%patch400 -p1 -b .momonga

# set KDE_VERSION_STRING
sed -i -e "s|@KDE_VERSION_STRING@|%{kde_release} %{distname} (KDE %{version})|" kdecore/util/kdeversion.h.cmake

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DHUPNP_ENABLED:BOOL=ON \
	-DKAUTH_BACKEND:STRING="PolkitQt-1" \
	-DKDE_DISTRIBUTION_TEXT="%{kde_release} %{distname}" \
	-DWITH_SOLID_UDISKS2:BOOL=ON \
	..
popd

# kludge hack by Momonga
%if "%{_smp_mflags}" == "-j1"
%define _smp_mflags -j2
print_specopt() {
cat <<EOF
Configurations:
_smp_mflags .......... %{_smp_mflags}
EOF
}
#print_specopt
%endif

make %{?_smp_mflags} -C %{_target_platform}

# build apidocs
%if 0%{?apidocs}
export QTDOCDIR="%{?_qt4_docdir}%{?!_qt4_docdir:%(pkg-config --variable=docdir Qt)}"
%if 0%{?apidocs_qch}
export PROJECT_NAME="%{name}"
export PROJECT_VERSION="%{version}"
doc/api/doxygen.sh --qhppages .
%else
doc/api/doxygen.sh .
%endif
%endif

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

# see also use-of/patching of XDG_MENU_PREFIX in kdebase
# mv %{buildroot}%{_kde4_sysconfdir}/xdg/menus/applications.menu \
#   %{buildroot}%{_kde4_sysconfdir}/xdg/menus/kde4-applications.menu

# create/own, see http://bugzilla.redhat.com/483318
mkdir -p %{buildroot}%{_kde4_libdir}/kconf_update_bin

## use ca-certificates' ca-bundle.crt, symlink as what most other
## distros do these days (http://bugzilla.redhat.com/521902)
if [  -f %{buildroot}%{_kde4_appsdir}/kssl/ca-bundle.crt -a \
      -f /etc/pki/tls/certs/ca-bundle.crt ]; then
  ln -sf /etc/pki/tls/certs/ca-bundle.crt \
         %{buildroot}%{_kde4_appsdir}/kssl/ca-bundle.crt
fi

# move devel symlinks
mkdir -p %{buildroot}%{_kde4_libdir}/kde4/devel
pushd %{buildroot}%{_kde4_libdir}
for i in lib*.so
do
  case "$i" in
    libkdeinit4_*.so)
      ;;
    *)
      linktarget=`readlink "$i"`
      rm -f "$i"
      ln -sf "../../$linktarget" "kde4/devel/$i"
      ;;
  esac
done
popd

install -p -m 644 -D %{SOURCE20} %{buildroot}%{_sysconfdir}/profile.d/kde.sh
install -p -m 644 -D %{SOURCE21} %{buildroot}%{_sysconfdir}/profile.d/kde.csh

# fix Sonnet documentation multilib conflict
bunzip2 %{buildroot}%{_kde4_docdir}/HTML/en/sonnet/index.cache.bz2
sed -i -e 's!<a name="id[0-9]*"></a>!!g' %{buildroot}%{_kde4_docdir}/HTML/en/sonnet/index.cache
bzip2 -9 %{buildroot}%{_kde4_docdir}/HTML/en/sonnet/index.cache

# install apidocs and generator script
install -p -D doc/api/doxygen.sh %{buildroot}%{_kde4_bindir}/kde4-doxygen.sh

%if 0%{?apidocs}
mkdir -p %{buildroot}%{_kde4_docdir}/HTML/en
cp -a kdelibs-%{version}-apidocs %{buildroot}%{_kde4_docdir}/HTML/en/kdelibs4-apidocs
find   %{buildroot}%{_kde4_docdir}/HTML/en/ -name 'installdox' -exec rm -fv {} ';'
rm -vf %{buildroot}%{_kde4_docdir}/HTML/en/kdelibs4-apidocs/*.tmp \
       %{buildroot}%{_kde4_docdir}/HTML/en/kdelibs4-apidocs/index.qhp \
       %{buildroot}%{_kde4_docdir}/HTML/en/kdelibs4-apidocs/*/html/index.qhp

%if 0%{?apidocs_qch}
mkdir -p %{buildroot}%{_qt4_docdir}/qch
for i in %{buildroot}%{_kde4_docdir}/HTML/en/kdelibs4-apidocs/*/qch
do
  mv -f "$i"/* %{buildroot}%{_qt4_docdir}/qch/
  rmdir "$i"
done
%endif
%endif

# own %%{_kde4_datadir}/kde4/services/ServiceMenus for kdegraphics and kdemultimedia
mkdir -p %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus

# install Momonga Menus
mv %{buildroot}%{_sysconfdir}/xdg/menus/applications.menu %{buildroot}%{_sysconfdir}/xdg/menus/applications.menu.kde
install -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/xdg/menus/applications.menu
install -m 644 %{SOURCE11} %{SOURCE12} %{buildroot}%{_sysconfdir}/xdg/menus/

%post
/sbin/ldconfig
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
/sbin/ldconfig ||:
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
    update-mime-database %{_kde4_datadir}/mime &> /dev/null
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q &> /dev/null
update-mime-database %{_kde4_datadir}/mime >& /dev/null
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS README TODO
%doc COPYING.LIB
%{_kde4_bindir}/*
%config(noreplace) %{_sysconfdir}/profile.d/*
%exclude %{_kde4_bindir}/kconfig_compiler4
%{_kde4_appsdir}
# kdewidgets
%exclude %{_kde4_bindir}/makekdewidgets4
%exclude %{_kde4_bindir}/kde4-doxygen.sh
%exclude %{_kde4_libdir}/kde4/plugins/designer/kdewidgets.so
# ksgmltools2
%exclude %{_kde4_appsdir}/ksgmltools2
# conflicts with kdepim
%exclude %{_kde4_appsdir}/kcharselect
%{_kde4_appsdir}/kcharselect/kcharselect-data
%{_kde4_configdir}
%{_datadir}/dbus-1/interfaces/*
%{_sysconfdir}/dbus-1/system.d/*
%{_kde4_datadir}/mime/packages/*
%{_kde4_sharedir}/kde4
%{_kde4_iconsdir}/hicolor/*/*/*.png
%dir %{_kde4_docdir}/HTML
%dir %{_kde4_docdir}/HTML/en
%{_kde4_docdir}/HTML/en/kioslave
%{_kde4_docdir}/HTML/en/sonnet
%{_kde4_libdir}/lib*.so.*
%{_kde4_libdir}/libkdeinit4_*.so
%{_kde4_libdir}/kde4/
%exclude %{_kde4_libdir}/kde4/devel
%dir %{_kde4_libexecdir}
%{_kde4_libexecdir}/filesharelist
%{_kde4_libexecdir}/fileshareset
%{_kde4_libexecdir}/kauth-policy-gen
%{_kde4_libexecdir}/kconf_update
%{_kde4_libexecdir}/kdesu_stub
%{_kde4_libexecdir}/kio_http_cache_cleaner
%{_kde4_libexecdir}/kioslave
%{_kde4_libexecdir}/klauncher
# see kio/misc/kpac/README.wpad
%attr(4755,root,root) %{_kde4_libexecdir}/kpac_dhcp_helper
%{_kde4_libexecdir}/ksendbugmail
%{_kde4_libexecdir}/lnusertemp
%{_kde4_libexecdir}/start_kdeinit
%{_kde4_libexecdir}/start_kdeinit_wrapper
%{_kde4_sysconfdir}/xdg/menus/*.menu*
%{_kde4_datadir}/applications/kde4/kmailservice.desktop
%{_kde4_datadir}/applications/kde4/ktelnetservice.desktop
%{_mandir}/man*/*
%exclude %{_kde4_configdir}/colors/40.colors
%exclude %{_kde4_configdir}/colors/Rainbow.colors
%exclude %{_kde4_configdir}/colors/Royal.colors
%exclude %{_kde4_configdir}/colors/Web.colors
%exclude %{_kde4_configdir}/ksslcalist
%exclude %{_kde4_configdir}/kdebug.areas
%exclude %{_kde4_configdir}/kdebugrc
%exclude %{_kde4_configdir}/ui
%exclude %{_kde4_configdir}/ui/ui_standards.rc
%exclude %{_kde4_appsdir}/kdeui
%exclude %{_kde4_bindir}/preparetips
%exclude %{_kde4_bindir}/plasma-dataengine-depextractor

%files common
%defattr(-,root,root,-)
%{_kde4_bindir}/preparetips
%{_kde4_configdir}/colors/40.colors
%{_kde4_configdir}/colors/Rainbow.colors
%{_kde4_configdir}/colors/Royal.colors
%{_kde4_configdir}/colors/Web.colors
%{_kde4_configdir}/ksslcalist
%{_kde4_configdir}/kdebug.areas
%{_kde4_configdir}/kdebugrc
%dir %{_kde4_configdir}/ui
%{_kde4_configdir}/ui/ui_standards.rc
%{_kde4_appsdir}/kdeui
%{_kde4_docdir}/HTML/en/common
%{_kde4_datadir}/locale/all_languages
%{_kde4_datadir}/locale/en_US/entry.desktop

%files devel
%defattr(-,root,root,-)
%doc KDE4PORTING.html
%{_kde4_bindir}/kconfig_compiler4
%{_kde4_bindir}/makekdewidgets4
%{_kde4_bindir}/kde4-doxygen.sh
%{_kde4_bindir}/plasma-dataengine-depextractor
%{_kde4_libdir}/kde4/plugins/designer/kdewidgets.so
%{_kde4_appsdir}/ksgmltools2
%{_kde4_includedir}/KDE
%{_kde4_includedir}/dnssd
%{_kde4_includedir}/dom
%{_kde4_includedir}/kdesu
%{_kde4_includedir}/khexedit
%{_kde4_includedir}/kio
%{_kde4_includedir}/kjs
%{_kde4_includedir}/kmediaplayer
%{_kde4_includedir}/knewstuff2
%{_kde4_includedir}/knewstuff3
%{_kde4_includedir}/kparts
%{_kde4_includedir}/kross
%{_kde4_includedir}/ksettings
%{_kde4_includedir}/ktexteditor
%{_kde4_includedir}/kunitconversion
%{_kde4_includedir}/kunittest
%{_kde4_includedir}/nepomuk
%{_kde4_includedir}/plasma
%{_kde4_includedir}/solid
%{_kde4_includedir}/sonnet
%{_kde4_includedir}/threadweaver
%{_kde4_includedir}/*.h
%{_kde4_includedir}/*.tcc
%{_kde4_libdir}/cmake/KDeclarative
%{_kde4_libdir}/kde4/devel

%if 0%{?apidocs}
%files apidocs
%defattr(-,root,root,-)
%{_kde4_docdir}/HTML/en/kdelibs4-apidocs
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.12.97-2m)
- rebuild against graphviz-2.36.0-1m

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Sun Jan 26 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.12.1-2m)
- [BUILD FIX] add a patch for kradio

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-2m)
- import patches from Fedora

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-2m)
- rebuild against ilbase-1.0.3, OpenEXR-1.7.1

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Fri Dec 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-2m)
- import patches from Fedora

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-3m)
- update kdelibs-udisks2-backend.patch, again
- import some patches from Fedora

* Tue Sep 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-2m)
- update kdelibs-udisks2-backend.patch

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-3m)
- rebuild against pcre-8.31

* Sat Aug  4 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.0-2m)
- rebuild against systemd-187

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Thu Jul  5 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8.4-2m)
- import and apply three patches for udisks2
- we have udisks2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Sun May  6 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8.3-2m)
- [CRITICAL BUG FIXES] remove two patches for udisks2
- we have no udisks2
- if these patches are applied, KDE can detect no media

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Fri Mar 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-3m)
- apply upstream patch to fix kde#295615

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- apply upstream patches

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Sun Feb  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- fix KDE version string (kde#293204)

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-3m)
- rebuild against attica-0.3.0

* Sat Dec 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.95-2m)
- rebuild against docbook-dtds-4.5-10m.mo8

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Fri Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- [SECURITY] CVE-2010-0046
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- [SECURITY] CVE-2011-3365
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-2m)
- import three patches from Fedora

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.5-3m)
- fix build of kdepim* with shared-desktop-ontologies-0.7.1
- http://bugs.kde.org/268595

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-3m)
- import upstream patch
-- fix kio regression causing requests submitted twice (kde#272466)

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-2m)
- fix plasma crash in KIconLauncher (kde#258706)

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.2-3m)
- rebuild against docbook-dtds-4.5-9m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-4m)
- import patch to fix kde#264487

* Sat Mar 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-3m)
- replace kde#267770 patch to upstream patch

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- import 2 patches from Fedora devel

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Wed Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-2m)
- import the halectomy patch from Fedora devel

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Mon Dec 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-2m)
- rebuild against polkit-qt-0.99.0
- apply new upstream patch

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-4m)
- own %%{_includedir}/KDE

* Sun Nov 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.80-3m)
- rebuild against docbook-dtds-4.5-8m

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-2m)
- fix kde#246652 (apply upstream patch)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-4m)
- rebuild against xz-5.0.0

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-3m)
- import two patches from Fedora (upstream)
-- kio/krun patch so kde services can open urls directly too
-- backport configChanged() for wallpaper

* Mon Oct 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- apply upstream patches
-- kde253294, KMail and Kopete download and open https url instead of only opening
-- kde253387, switching comic in comic applet crashes plasma

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Mon Sep  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-5m)
- [BUG FIX] fix up kde4.csh and kde4.sh for /.kde issue

* Wed Sep  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-4m)
- [BUILD FIX] rebuild against docbook-dtds-4.5-7m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- update applications.menu.momonga and mo-system-settings.menu

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Thu Aug  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-3m)
- update applications.menu.momonga and mo-system-settings.menu for momonga-nonfree-package-builder

* Tue Aug  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- update applications.menu.momonga and mo-system-settings.menu for momonga-nonfrees-packager and sdr

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-3m)
- change BuildRequires: from libattica-devel to attica-devel

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-2m)
- update applications.menu.momonga for Momonga Linux 7

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Wed Jun 02 2010 Daniel McLellan <daniel.mclellan@gmail.com>
- (4.4.3-3m)
- build with qt-webkit

* Sat May 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-2m)
- build with dbusmenu-qt

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-2m)
- rebuild against libjpeg-8a

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4 4 2

* Mon Mar 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-4m)
- patch50 was replaced with http://mail.kde.gr.jp/pipermail/kdeveloper/2010-March/000181.html

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-3m)
- import patch28 and patch100 from fedora devel
-- - KDE default in noisy debug mode to stdout/stderr (kde#227089)
-- - fix crash in KPixmapCache (bug#568389)

* Fri Mar 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-2m)
- fix xim input methods (#222620)

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Sat Feb 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-3m)
- import upstream patches
-- krunner crash patch (kde#227118)
-- plasma crash patch (kde#226823)

* Mon Feb 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-2m)
- import patch100 and patch200 from Fedora devel
- remove patch26

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-2m)
- import patch50 from Fedora devel

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-2m)
- import patch27 from Fedora devel
-- fix kauth polkit policies installation

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Wed Dec 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.85-2m)
- BuildRequires: libattica-devel >= 0.1.1

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- [SECURITY] CVE-2009-0689
- update to KDE 4.3.4

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-4m)
- import Patch25, Patch26, Patch50 and Patch100 from Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- add missing includes (Patch25)

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-2m)
- revise applications.menu.momonga and mo-system-settings.menu
  for gnome-packagekit-2.28.1-1m and guake-0.4.0-1m

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-3m)
- [SECURITY] CVE-2009-2702
- import a security patch (Patch300) from Rawhide (6:4.3.1-3)

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.1-2m)
- rebuild against libjpeg-7

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Tue Aug 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-3m)
- [BUG FIX] fix broken KDE menu
- exclude vmware-user.desktop from applications.menu.momonga
  to avoid displaying "Lost & Found" and no-icon vmware-user.desktop in menu of KDE

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- set %%global qtver 4.5.2 and BR: soprano-devel >= 2.3.0 for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Thu Jul 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-2m)
- [SECURITY] CVE-2009-1687 CVE-2009-1698 CVE-2009-1725 CVE-2009-2537

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)
- update parallel_devel patch
- ommit kdebug#198338 patch

* Mon Jul  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-2m)
- import upstream patch from Fedora (resolve kdebug#198338)
- update openssl patch

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.91-2m)
- update applications.menu.momonga mo-system-settings.menu for system-switch-displaymanager

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-2m)
- - add new plasma features

- * Sun May 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Sun May 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.3-2m)
- remove plasma-default-wallpaper.patch
- new plasma allows to set default wallpaper using kdebase-workspace >= 4.2.3-3m

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuild with new release source
- - import some patches from Fedora devel

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Fri Apr 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-3m)
- rebuild against openssl-0.9.8k

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- import some patches from Fedora devel

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- add patch101 from Fedora devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC
- update kde4.sh and kde4.csh

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sat Dec  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.81-3m)
- set the default wallpaper again

* Tue Dec  2 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.81-2m)
- fix make -j1 cause error by redefine _smp_mflags

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-2m)
- revise %%files to avoid conflicting

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80
- almost sync with Fedora devel

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Tue Oct 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-2m)
- remove conflicting files with koffice

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Thu Sep  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-4m)
- remove authconfig.desktop and polkit-gnome-authorization.desktop from Settingsmenu

* Wed Sep  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-3m)
- apply upstream fixes
 - kdelibs-4.1.1-kde#169447-khtml-regression.patch
   https://bugs.kde.org/show_bug.cgi?id=119292
 - kdelibs-4.1.1-kde#856379-cookiejar.patch
   https://bugs.kde.org/show_bug.cgi?id=169851
   https://bugs.kde.org/show_bug.cgi?id=170147
 - kdelibs-4.1.1-kde#856403-urlnav.patch
   https://bugs.kde.org/show_bug.cgi?id=169497
   https://bugs.kde.org/show_bug.cgi?id=170211
- Patch18 and Patch19 should be updated?

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62

- * Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-2m)
- import upstream patch, http://bugs.kde.org/show_bug.cgi?id=167826

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-2m)
- release %%{_kde4_includedir}/KDE, it's provided by phonon-devel

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Sun Jun 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-6m)
- update menus for new system-config-*

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-5m)
- enable -DCMAKE_BUILD_TYPE:STRING=MOMONGA

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-4m)
- update openssl.patch for openssl-0.9.8h

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-3m)
- rebuild against openssl-0.9.8h-1m

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- merge changes of kded/applications.menu partially,
  keep 4.0.x style on utility section, it's useful style
- modify game section of menu for lbreakout2
- own %%{_kde4_datadir}/kde4/services/ServiceMenus for kdegraphics and kdemultimedia

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Fri May 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-6m)
- update mo-server-settings.menu, mo-system-settings.menu and applications.menu.momonga
- for system-config-*, system-contorol-*, openjdk and PackageKit

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-5m)
- import new patches from Fedora 9 (Patch15 - Patch19)
- delete Patch3

* Fri May 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-4m)
- change Requires from bluecurve-icon-theme to fedora-icon-theme

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-3m)
- Requires: bluecurve-icon-theme

* Sat May 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-2m)
- update applications.menu.momonga for guake.desktop

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4
- rebuild against qt-4.4.0-1m

* Tue Apr 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-3m)
- update applications.menu.momonga and mo-system-settings.menu

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-2m)
- [SECURITY] CVE-2008-1670

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- import some patches from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-6m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-5m)
- rebuild against OpenEXR-1.6.1

* Sat Mar 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-4m)
- now, we set relname to "Kilroy"

* Fri Mar 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-3m)
- set useragent of konqueror to "%%{version}-%%{release} Momonga"
- set -DKDE_DISTRIBUTION_TEXT="%%{kde_release} %%{distname}"
- kde_release is including relname, please set the relname, pulsar

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-2m)
- remove apidocs from main-package
- provide %%{_libdir}/kde4/plugins/designer for kdeedu by main-package

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.1
- delete unused patches
- import some patches from Fedora devel
- build apidoc

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- modify menus

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1
- import some patches from Fedora devel

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-3m)
- own _kde4_appsdir, _kde4_configdir, _kde4_sharedir/kde4 and _kde4_docdir/HTML

* Sun Jan 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-2m)
- import kdelibs-3.97.0-alsa-default-device.patch from Fedora devel

* Sun Jan 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-5m)
- apply latest svn fix
- http://bugs.kde.org/show_bug.cgi?id=155001

* Thu Dec 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-4m)
- apply latest svn fixes to avoid crashing nspluginviewer with new flash

* Thu Nov 29 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-3m)
- add a patch for gcc-4.3

* Fri Oct 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-2m)
- add fixarcjpencode patch for non-ASCII file name in zip archives

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8
- delete unused patches and update some patches

* Fri Oct  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-12m)
- update applications.menu.momonga for new gnome-games

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.7-11m)
- revised spec for debuginfo

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-10m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- update security patch (post-3.5.7-kdelibs-kdecore-2.diff)

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-9m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- import upstream security patch (post-3.5.7-kdelibs-kdecore.diff)

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-8m)
- enable to build with cups-1.3
- import patch from FC-devel

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-7m)
- fix mo-server-settings.menu

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-6m)
- update menus for system-config-{audit,cluster,lvm}

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-5m)
- remove ICEauthority.patch

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-4m)
- update menus for revisor

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- exclude screensavers from applications.menu.momonga

* Sun Jun  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- update menus for system-config-{httpd,nfs,samba,sevices}
- import kdelibs-3.5.7-ICEauthority.patch from Fedora
- import kdelibs-3.5.7-kde#146105.patch from Fedora
 +* Thu May 24 2007 Than Ngo <than@redhat.com> 3.5.6-10.fc7
 +- don't change permission .ICEauthority by sudo KDE programs
 +- apply upstream patch to fix kde#146105

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete unused upstream patches
- kde-khtml-overflow-CVE-2006-4811.patch is still needed

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-16m)
- set relname rainbowshoes, LittleWing is a registered trademark of LittleWing CO.LTD.

* Sun May  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-15m)
- set relname littlewing
- sort BuildPreReq

* Sun May  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-14m)
- set relname LittleWing for STABLE release

* Thu Mar 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-13m)
- [SECURITY] CVE-2007-1564
- replace CVE-2007-1308.patch to CVE-2007-1564-kdelibs-3.5.6.diff
  http://www.kde.org/info/security/advisory-20070326-1.txt

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-12m)
- import upstream patch to fix following problem
  #138976, Some links are difficult to click in the documentation (docs.kde.org)

* Wed Mar 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-11m)
- update applications.menu.momonga for gnome-sudoku, gnomeattacks, monkey-bubble and tuxtype

* Sun Mar 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-10m)
- [SECURITY] CVE-2007-1308
  import http://bindshell.net/advisories/konq355/konq355-patch.diff as CVE-2007-1308.patch
  http://bindshell.net/advisories/konq355
- modify applications.menu.momonga
  include all desktop files of new gdm
- import kdelibs-3.5.6-utempter.patch from Fedora
- BuildPreReq: libutempter-devel
- add --with-utempter option to configure

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-9m)
- BuildPreReq: attr-devel -> libattr-devel

* Sun Mar  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-8m)
- update openssl.patch
- import kdelibs-3.5.1-smooth-scrolling.patch from cooker
 +* Fri Jul 14 2006 Helio Chissini de Castro <helio@mandriva.com> 30000000:3.5.3-8mdv2007.0
 ++ Revision: 41174
 +- Added smooth scrolling patch

* Sat Feb 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-7m)
- import upstream patch to fix following problem
  #141670, Paste image from clipboard to another app (crash)

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-6m)
- rebuild against arts pcre gdbm libjpeg aspell jasper

* Tue Feb  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-5m)
- revise avahi-support patch (patch30)

* Sun Feb  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-4m)
- import upstream patch to fix following problem
  #140768, loading new page resets scroll position of old page to top (Reverting r617941)

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-3m)
- revive avahi-support patch (patch30)

* Sat Feb  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-2m)
- update applications.menu.momonga
 - include epiphany.desktop, bmp-*.desktop and totem.desktop
  - Epiphany, BMPx and Totem are working on KDE now
 - modify for SELinux tools
- modify mo-system-settings.menu for SELinux tools

* Sat Jan 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches
- some of previously imported patches were modified

* Thu Jan 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-19m)
- import upstream patch to fix XSS vulnerability
- see http://www.securityfocus.com/archive/1/457763/30/30/threaded

* Tue Jan 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-18m)
- import upstream patch to fix following problem
  #136630, 'Save As' behaviour change (Revert 589847)

* Fri Jan 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-17m)
- import upstream patches to fix following problems
  #107455, [patch] File upload input does not fire "onchange" event (javascript)
  #105351, get new wallpapers doesn't save extension

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-16m)
- rebuild against jasper-1.900.0-1m
- add Requires: jasper and BuildPreReq: jasper-devel

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-15m)
- import upstream patch to fix following problem
  #121867, [site-issue] Largest Hungarian press media webpage looks ugly - www.nol.hu

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-14m)
- import upstream patches to fix following problems
  #139488, crash when finding backwards
  #24820, visited links don't change colour until after they are hovered over with the mouse.
  #109038, folder icon not changed back when write/read-permission is re-added to a folder

* Sun Dec 24 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-13m)
- import upstream patch to fix following problem
  #138818, "Select a year" in KDatePicker does not properly update KDateTable child widget

* Sun Dec 24 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-12m)
- import upstream patch to fix following problem
  r615933, the dialog is not created every time, but is cached, so values are not reset

* Fri Dec 22 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-11m)
- import upstream patch to fix following problem
  #139077, kdeinit prints "created link" to stdout which spoils kdialog output

* Tue Dec 12 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-10m)
- import upstream patch to fix following problem
  #103775, kontact --module XYZ crashes after minimizing to tray icon and maximizing back
- revise kdelibs-3.5.5-kde#125559.patch

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-9m)
- import upstream patch to fix following problem
  #125559, Keycode 111 starts ksnapshot (hardcoded?)

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-8m)
- rebuild against OpenEXR-1.4.0

* Sun Nov 12 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-7m)
- rebuild against hspell-1.0-1m
- add Requires: hspell >= 0.9
- add BuildPreReq: hspell-devel >= 0.9
- import upstream patches to fix following problems
  #15876, status bar has 2 lines
  #136952, [regression] crash on www.cosmote.gr
  #122047, &notin; is displayed as &not;in;

* Wed Nov  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-6m)
- modify applications.menu.momonga and mo-*-settings.menu for system-config-printer

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-5m)
- import upstream patch to fix following problem
- #136649, 64bit specific bug in KDEPrintd

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- [SECURITY] import patch from mdk (kde-khtml-overflow-CVE-2006-4811.patch)
-- * Wed Oct 18 2006 Vincent Danen <vdanen@mandriva.com>
--   security fix for CVE-2006-4811
- import upstream patches from FC6
- fix CUPS manager problem (kdelibs-3.5.5-kmcupsmanager.patch)
- fix khtml problem (kdelibs-3.5.5-kde#135988.patch)

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.5-3m)
- rebuild against expat-2.0.0-2m

* Sat Oct 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-2m)
- modify applications.menu.momonga and mo-*-settings.menu for system-config-netboot
 - SystemSetup and X-Red-Hat-ServerConfig are obsoleted by desktop-file-utils
- exclude bmp-*2.0.desktop from applications.menu.momonga
 - bmpx doesn't work on KDE

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-5m)
- import 10 upstream patches from Fedora Core devel
 +* Tue Sep 05 2006 Than Ngo <than@redhat.com> 6:3.5.4-5
 +- apply upstream patches
 +   fix #123413, kded crash when KDED modules make DCOP calls in their destructors
 +   fix #133529, konqueror's performance issue
 +   fix kdebug crash
 +   more icon contexts (Tango icontheme)
 +   fix #133677, file sharing doesn't work with 2-character long home directories
 +* Mon Sep 04 2006 Than Ngo <than@redhat.com> 6:3.5.4-4
 +- apply upstream patches
 +   fix kde#121528, konqueror crash
 +* Wed Aug 23 2006 Than Ngo <than@redhat.com> 6:3.5.4-3
 +- apply upstream patches
 +   fix kde#131366, Padding-bottom and padding-top not applied to inline elements
 +   fix kde#131933, crash when pressing enter inside a doxygen comment block
 +   fix kde#106812, text-align of tables should only be reset in quirk mode
 +   fix kde#90462, konqueror crash while rendering in khtml

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- rebuild against arts-1.5.4-2m and avahi-0.6.13-3m

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-3m)
- rebuild against esound-devel-0.2.36-4m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against libart_lgpl-2.3.17-4m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- remove merged upstream patches

* Mon Jul 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-9m)
- fix kde#116092
- remove transkicker-highcolor.patch

* Sat Jul 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.5.3-8m)
- [SECURITY] CVE-2006-3672
- http://browserfun.blogspot.com/2006/07/mobb-14-konqueror-replacechild.html
- add Patch301: kdelibs-3.5.3-CVE-2006-3672.patch

* Wed Jul 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-7m)
- import 2 upstream patches from Fedora Core devel
 +* Tue Jul 11 2006 Than Ngo <than@redhat.com> 6:3.5.3-8
 +- upstream patches,
 +    kde#130605 - konqueror crash
 +    kde#129187 - konqueror crash when modifying address bar address

* Tue Jul 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-6m)
- import 2 upstream patches from Fedora Core devel
 +* Mon Jul 10 2006 Than Ngo <than@redhat.com> 6:3.5.3-7
 +- apply upstream patches,
 +    kde#123307 - Find previous does nothing sometimes
 +    kde#106795 - konqueror crash

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-5m)
- fix setuid

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- import 2 upstream patches from Fedora Core devel
 +* Tue Jul 04 2006 Than Ngo <than@redhat.com> 6:3.5.3-6
 +- apply upstream patches, fix #128940/#81806/#128760

* Sat Jul  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- import 9 upstream patches from Fedora Core devel
 +* Sat Jun 24 2006 Than Ngo <than@redhat.com> 6:3.5.3-5
 +- apply upstream patches
- set distname

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- rebuild with avahi (enable Zeroconf support)
- import kdelibs-3.5.0-add-dnssd-avahi-support.patch from cooker
 +* Wed Jan 25 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-25 18:45:30 (1469)
 +- Fix linkage of dnssd with libavahi-client (thanks gb)
 +* Wed Jan 25 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-25 08:55:44 (1460)
 +- Add support for zeroconf+avahi lib
 +* Tue Jan 24 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-24 17:05:13 (1457)
 +- Fix search avahi daemon
 +* Mon Jan 23 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-23 17:58:36 (1448)
 +- Add support for avahi

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- remove modular-x.patch

* Fri May 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-10m)
- revise %%files for rpm-4.4.2

* Wed May 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-9m)
- update applications.menu.momonga
 - exclude nact.desktop
  - nautilus-actions is an extension of nautilus for GNOME
 - include gnomebaker.desktop
  - gnomebaker works fine on KDE

* Tue May  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-8m)
- update openssl.patch

* Sat Apr 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-7m)
- update mo-system-settings.menu and applications.menu.momonga for pirut

* Wed Apr 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.5.2-6m)
- add kdelibs-3.5.2-menu-fixcomment.patch and revised applications.menu.momonga
- - wrong position of a comment in applications.menu seemed to prevent "science" submenu to appear in a menu.

* Fri Apr 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-5m)
- exclude gnome-screenshot.desktop from applications.menu.momonga
 - ksnapshot has same function as gnome-screenshot
- exclude sabayon.desktop from mo-system-settings.menu
 - sabayon.desktop doesn't work on KDE

* Tue Apr 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-4m)
- exclude epiphany.desktop and mergeant.desktop from applications.menu.momonga
 - epiphany doesn't work on KDE
 - mergeant leaves an unnecessary process at the next session of KDE

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- update openssl.patch

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.2-2m)
- rebuild against openssl-0.9.8a

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-3m)
- import kdelibs-3.5.0-modular-x.patch from Fedora Core devel
 +* Thu Dec 01 2005 Than Ngo <than@redhat.com> 6:3.5.0-1
 +- add fix for modular X, thanks to Ville Skytt #174131

* Mon Feb 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- modify applications.menu.momonga

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- remove post-3.4.3-kdelibs-kjs.diff

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-4m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Fri Jan 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- [SECURITY] CVE-2006-0019
  post-3.4.3-kdelibs-kjs.diff for "kjs encodeuri/decodeuri heap overflow vulnerability"
  http://www.kde.org/info/security/advisory-20060119-1.txt

* Fri Dec 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- import kdelibs-3.5.0-kicker-crash.patch from gentoo-x86-portage
 +- 14 Dec 2005; Diego Petteno <flameeyes@gentoo.org>
 +- +files/kdelibs-3.5.0-kicker-crash.patch, +kdelibs-3.5.0-r1.ebuild:
 +- Add patch to fix kicker's crashes, hopefully fixing bug #114141. Patch from
 +- upstream committed by aseigo.

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.3m)
- remove lnusertemp.patch
  (move %%{_bindir}/lnusertemp from arts to kdelibs)
- disable hidden visibility

* Sun Nov 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- fix applications.menu.momonga

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- update applications.menu.momonga
- update buildroot.patch from Fedora Core devel
- import 2 patches from Fedora Core devel
 - kdelibs-3.4.92-inttype.patch
 - kdelibs-3.4.92-lnusertemp.patch
  +* Fri Nov 04 2005 Than Ngo <than@redhat.com> 6:3.4.92-2
  +- move lnusertemp in arts, workaround for #169631
- import Source4: devices.protocol from Fedora Core devel
- BuildPreReq: attr-devel >= 2.4.10-2m, libacl-devel >= 2.2.28-2m
- BuildPreReq: aspell-devel

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- add %%doc

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- update openssl.patch
- update visibility.patch
- remove post-3.4.0-kdelibs-kimgio-fixed.diff
- remove kdelibs-3.4.0-FC.diff

* Mon May  9 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.4.0-7m)
- ppc build fix.

* Fri May  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-6m)
- [SECURITY] update Patch30: post-3.4.0-kdelibs-kimgio(-fixed).diff
  http://kde.org/info/security/advisory-20050504-1.txt

* Sat Apr 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-5m)
- [SECURITY] post-3.4.0-kdelibs-kimgio.diff for "kimgio input validation errors"
  http://kde.org/info/security/advisory-20050421-1.txt

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-4m)
- modify %%files section
- remove -fno-rtti from CXXFLAGS

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-3m)
- sync with FC devel due to enable x86_64.

* Tue Mar 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-2m)
- modify applications.menu.momonga for HelixPlayer

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- update applications.menu.momonga
- update rpath.patch
- update openssl.patch from Fedora Core
- update buildroot.patch from Fedora Core
- remove old patches
- BuildPrereq: doxygen, libart_lgpl-devel, pkgconfig, perl
- BuildPrereq: libjpeg-devel, libpng-devel, libtiff-devel
- BuildPrereq: cups-devel >= 1.1.9

* Thu Mar 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-8m)
- [SECURITY] post-3.3.2-kdelibs-dcopidlng.patch for "Insecure temporary file creation by dcopidlng"
  http://kde.org/info/security/advisory-20050316-3.txt
- [SECURITY] post-3.3.2-kdelibs-idn-2.patch for "Konqueror International Domain Name Spoofing"
  http://kde.org/info/security/advisory-20050316-2.txt
- [SECURITY] post-3.3.2-kdelibs-dcop.patch for "Local DCOP denial of service vulnerability"
  http://kde.org/info/security/advisory-20050316-1.txt

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-7m)
- modify mo-server-settings.menu and mo-system-settings.menu

* Sun Feb 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-6m)
- update Source1: applications.menu.momonga
- change Source2 to mo-server-settings.menu
- add Source3: mo-system-settings.menu
  Source2 and Source3 are modified FC3 files

* Sun Feb 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.2-5m)
- add BuildPrereq: esound-devel for use /usr/lib/libesd.la

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-4m)
- edit applications.menu
  add Source1: applications.menu.momonga
  add Source2: rh-system-settings.menu.momonga
- Requires: redhat-artwork

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- [SECURITY] post-3.3.2-kdelibs-htmlframes2.patch for "Konqueror Window Injection Vulnerability"
  http://kde.org/info/security/advisory-20041213-1.txt
- [SECURITY] post-3.3.2-kdelibs-kioslave.patch for "ftp kioslave command injection"
  http://kde.org/info/security/advisory-20050101-1.txt
- import patches from Fedora Core
 - kdelibs-3.3.2-kate-immodule.patch
  +* Sat Feb 12 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.7
  +- backport CVS patch, cleanup InputMethod
 - kdelibs-3.3.2-arts.patch
  +* Thu Feb 10 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.5
  +- fix knotify crash after applying sound system change
 - kdelibs-3.3.2-cleanup.patch
  +* Thu Feb 10 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.5
  +- add steve cleanup patch
 - kdelibs-3.3.2-ppc.patch
  +* Wed Dec 15 2004 Than Ngo <than@redhat.com> 6:3.3.2-0.4
  +- get rid of broken AltiVec instructions on ppc

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.
- remove kdelibs-3.1.93-lib64.patch.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2
- [SECURITY] post-3.3.2-kdelibs-kio.diff for "plain text password exposure" (http://www.kde.org/info/security/advisory-20041209-1.txt)

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-3m)
- rebuild against qt-3.3.3-2m, arts-1.3.0-2m (libstdc++-3.4.1)

* Sun Sep 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-2m)
- revise to include x-ms-{asf,wmv}.desktop again...(I made a mistake...sorry!)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- cleanup patches
- import Patch1,5,31,32,33 from Fedora Core

* Tue Aug 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-4m)
- apply following patch
- http://www.kde.org/info/security/advisory-20040823-1.txt

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-3m)
- apply following patches
- http://www.kde.org/info/security/advisory-20040811-1.txt
- http://www.kde.org/info/security/advisory-20040811-2.txt
- http://www.kde.org/info/security/advisory-20040811-3.txt

* Wed Aug 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-2m)
- revise %%install section with update of kaffeine

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Tue May 25 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.2-4m)
- fixed vulnerability in the mailto handler, CAN-2004-0411
- fixed KDE Telnet URI Handler File Vulnerability , CAN-2004-0411

* Fri May 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.2-3m)
- kde-i18n-Azerbaijani and kde-i18n-Icelandic is exist.

* Sat May 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.2-2m)
- remove /usr/share/icons/hicolor/index.theme which is provided by hicolor-icon-theme
- add Requires: hicolor-icon-theme

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.2-1m)
- KDE 3.2.2

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.2.1-3m)
- rebuild against for libxml2-2.6.8
- rebuild against for libxslt-1.1.5

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.1-2m)
- revised spec to avoid conflicting with kaffeine.

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Tue Feb 17 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.0-2m)
- use %%patch macro for Patch101

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- add --sysconfdir argument to ./configure (for %%{_sysconfdir}/xdg/menus)
- following patches can't be applied (so are omitted now)
- - Patch5: kdelibs-2.1.1-path.patch
- - Patch7: kdelibs-3.0.2-dock.patch
- - Patch12: kioslavetest.patch
- - Patch13: metatest.patch
- - Patch15: kdelibs-3.1-buildroot.patch
- - Patch18: kdelibs-3.1-icon_scale.patch
- - Patch21: http://www6.plala.or.jp/d-frog/kdelibs-3.1.3-khtml-spacefix-r1.patch.gz
- - Patch30: kdelibs-3.1.3a-xandr.patch
- replace following patches by patches from Fedora
- - Patch11: kdelibs-3.1-lib64.patch -> kdelibs-3.1.93-lib64.patch
- change the way doc build (make apidox)
- modified %files (many new files)

* Thu Jan 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.5-2m)
- include again %%{_datadir}/mimelnk/application/x-kword.desktop, etc.
- Without these files, applications in koffice couldn't have saved files
  with their native formats for a long time... now fixed!

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Tue Jan  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-6m)
- put kdelibs-3.1.3-khtml-spacefix-r1.patch.gz into repository

* Tue Jan  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-5m)
- import IE compatible space handling patch for khtml

* Thu Dec 25 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-4m)
- rebuild against qt-3.2.3 (merge from QT322 branch)

* Fri Dec 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-3m)
- remove -fno-rtti, -fno-check-new from CFLAGS for configure to detect availability of -fPIC option
- add patch fot alsa-1.0.0

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.4-2m)
- rebuild against gdbm-1.8.0

* Wed Sep 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4
  - changes  are
    * Allows compilation against Qt 3.2.x
    * kio: only cache successful passwords, otherwise its impossible to re-enter a password when it failed authorisation the first time.
    * other changes are listed in http://www.kde.org/announcements/changelogs/changelog3_1_3to3_1_4.php

* Wed Jul 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3
  - changes are
    * keramik: Major fixes to toolbar gradient alignment, as well as some miscellaneous fixes.
    * SECURITY: kio/khtml: Improve referer handling, always strip out username and password.

* Mon Jul 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-4m)
- add security patches from
     [Kuser:04021] KDE SECURITY:  Konqueror Referer Leaking Website Authentication Credentials
- remove --disable-warnings from configure

* Tue Jul  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-3m)
- add -fno-stack-protector if gcc is 3.3

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-2m)
- remove --disable-warnings from configure

* Thu May 22 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     https authentication through proxy fixed.
     KZip failed for some .zip archives.
     Fixed a bug in socket code that made KDEPrint crash.
     kspell: Support for Hebrew spell checking using hspell (requires hspell 0.5).
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m

# make -f Makefile.cvs

* Sun May 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-4m)
- export _POSIX2_VERSION=199209

* Thu May  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-3m)
- Kondara MNU/Linux -> Momonga Linux

* Sun Apr 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-2m)
  fix keramik style

* Fri Apr 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-1m)
  update to 3.1.1a

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- specopt
- enable cups by default
- update to 3.1.1
    kdialog: Fix screen numbering problem for centerOnScreen() static method
    kprogress: Fix math problem in displaying correct percentage for large numbers
    kio_http: Fix data processing for webdav(s) listing of directories and files
    Fixed code completion drop-down box position
    kate: Many small bugfixes, including:
      Fixed code completion drop-down box position
      Fixed "wrap cursor disabled" bugs
      Fixed vertical scrollbar middle mouse behaviour
      Fixed remove whitespace feature
      Now clears the redo history when it is irrelevant
      Fixed crash after starting up with a non-existant directory in the file selector history
    kparts: Fix extension of transparently downloaded files, this fixes ark (used to display temp file instead of archive content)
    klauncher: Fixed support for "Path=" entry in .desktop files. This entry can be used to specify a working directory.
    kio: Don't let ChModJob's -X emulation interfere with mandatory file locking.
    kdeui: Fix for alternate background coloring in Konqueror list views.
    kdeui: Fix to prevent an event loop in conjunction with Qt 3.1.2.
    kio/bookmarks: Properly handle multiple shown bookmarks with the same URL; fixes crash on exit in Konqueror when bookmarkbar is on and some bookmarks points to the same place
    kstyles: Handle focus indicators on label-less checkboxes better
    kdeprint: Don't freeze when there is no route to the selected CUPS server
    SSL: add support for OpenSSL 0.9.7
    SSL: ADH ciphers are now explicitly disabled in all cases
    SSL: new CA root certificate added
    Several Xinerama related fixes
    QXEmbed fixes for various situations that don't handle XEMBED well
    Java on IRIX with SGI 1.2 VM is fixed
    khtml: Several major bugfixes, partially incorporated fixes from Safari as well.

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-6m)
- add URL

* Thu Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1-5m)
  rebuild against openssl 0.9.7a

* Thu Feb 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-4m)
- add Obsoletes: kde-i18n-Azerbaijani
- add Obsoletes: kde-i18n-Korean
- add Obsoletes: kde-i18n-Icelandic
- add Obsoletes: kde-i18n-Latvian

* Sat Feb 08 2003 TAKAHASHI Tamotsu <tamo>
- (3.1-3m)
- do not apply lib64 patch if _lib != lib64
 (if you want to build kdelibs on lib64 machine,
  you need to do more fixes)

* Wed Feb 05 2003 TAKAHASHI Tamotsu <tamo>
- (3.1-2m)
- Patch11: kdelibs-3.1-lib64.patch (from SUSE kdelibs3-3.1-0)

* Sat Feb  1 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-1m)
- ver up.

* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-2m)
- rebuild against qt-3.0.5-8m.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-3m)
- add BuildPrereq: kernel >= 2.4.18-119m
  for deviceman.cc require sound/asound.h and sound/asequencer.h

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Thu Sep 12 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-3m)
- update to kdelibs-3.0.3a by security fixes.

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.3-2m)
- rebuild against arts-1.0.3-2m

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- remove patch1.

* Thu Jun 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.1-6k)
- momonga-release

* Tue Jun 18 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-4k)
- add transrate patch for highcolor & keramik style.
- ( See http://kde-look.org/content/show.php?content=2177 )
- change build options. ( use %{optflags} ).

* Sun Jun 09 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Mon May 20 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-10k)
- no more docbook-utils.

* Sat Apr 20 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-8k)
- applied kdelibs-3.0-gui_effect-20020415.patch

* Tue Apr  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-6k)
- add keramik kstyle from kdelibs-3.0rc3
- (keramik kstyle is a alpha-blending style.)
- applied kdelibs-3.0-kate-charset-20020407.diff

* Wed Apr  5 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-4k)
- remove kde-i18n-Norwegian kde-i18n-Norwegian-Nynorsk from Obsolutes

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003004k)
- rebuild.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.
- arts has been separated.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-12k)
- nigirisugi.
- revised spec file.

* Tue Nov  6 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.1-10k)
- update libxslt patch for new libxslt

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised spec file.

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- rebuild against libpng 1.2.0.

* Sat Oct 13 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- added debug flag.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Tue Sep 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-6k)
- add ppc support.

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Wed Aug  9 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.
- Obsolete kdesupport, kdesupport-devel

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002004k)
- based on 2.2alpha2.

* Wed May 30 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001004k)
- Ummmm. KDE 2.2alpha1 is disappeared :-P

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Sat Apr 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.2alpha1-2k)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-6k)
- rebuild against openssl 0.9.6.

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-5k)
- rebuild against audiofile-0.2.1.

* Fri Mar 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- [2.1.1-3k]

* Tue Mar 20 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-6k]
- obsolete sound, sound-devel.

* Fri Mar 16 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-2k]

* Mon Jan 21 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-14k]
- backport 2.0.1-15k(Jirai).

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-12k]
- backport 2.0.1-13k(Jirai).

* Mon Jan 01 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k.

* Mon Jan 01 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-11k]
- rebuild against.

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- rebuild against audiofile-0.2.0.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Thu Dec 07 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Fri Nov 24 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-7k]
- klauncher.la change package kdelibs-devel to kdelibs.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-6k]
- rebuild against egcs++.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-5k]
- rebuild against new environment XFree86-4.0.1-39k.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild against new environment gcc-2.95.2, glibc-2.2.
- obsolete kdelibs-2.0-khtml-m17n-20001024.diff

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Wed Nov 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-2k]
- update kdelibs-2.0-gcc296.patch.

* Wed Nov 01 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- update kdelibs-2.0-gcc296.patch
- update kdelibs-2.0-khtml-m17n-20001024.diff.
- add kdelibs-2.0-artswrapper.patch.
- dcopserver.la change package kdelibs-devel to kdelibs.
- bugfix %files sections.
- add GIF Support switch.
- release.

* Tue Oct 24 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- [2.0-0.1k]
- update to 2.0

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- [2.0rc2-0.5k]
- add kdelibs-2.0rc2-khtml-egcs112.diff for egcs-1.1.2 :-P

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.4k]
- modified Requires,BuildPrereq.

* Wed Oct 18 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.3k]
- add kdelibs-2.0rc2-khtml-m17n-20001013.diff.
- modified Requires,BuildPrereq

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- enable debug mode.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- little bit fixed specfile.

* Wed Oct 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.

* Thu Sep 23 2000 Kenichi Matsubara <m@kondara.org>
- bugfix specfile.[%files sections.]

* Wed Sep 20 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Fri Jun 23 2000 Kenichi Matsubara <m@kondara.org>
- add kdelibs-1.91-without-openssl-kondara.patch.

* Mon Jun 19 2000 Kenichi Matsubara <m@kondara.org>
- update 1.91.

* Tue May 16 2000 Kenichi Matsubara <m@kondara.org>
- initial release Kondara MNU/Linux.
