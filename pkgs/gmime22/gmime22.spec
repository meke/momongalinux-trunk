%global momorel 4
%define tarname gmime

Summary: Library for creating and parsing MIME messages
Name: gmime22
Version: 2.2.27
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPLv2+
URL: http://spruce.sourceforge.net/gmime/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{tarname}/2.2/%{tarname}-%{version}.tar.bz2
Nosource: 0
Patch0: gmime-2.2.1-lib64.patch

# for new mono-2.0
Patch1: gmime-2.2.22-gacutils.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  glib2-devel >= 2.18.1
BuildRequires:  zlib-devel >= 1.2.3
%ifnarch ppc64 alpha sparc64
BuildRequires:  mono-devel >= 2.8
BuildRequires:  gtk-sharp2-devel >= 2.12.10-4m
%endif

%description
The GMime suite provides a core library and set of utilities which may be
used for the creation and parsing of messages using the Multipurpose
Internet Mail Extension (MIME).

%package        devel
Summary:        Header files to develop libgmime applications
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       glib2-devel

%description    devel
The GMime suite provides a core library and set of utilities which may be
used for the creation and parsing of messages using the Multipurpose
Internet Mail Extension (MIME). The devel-package contains header files
to develop applications that use libgmime.

%ifnarch ppc64 alpha sparc64
%package        sharp
Summary:        mono bindings for gmime
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk-sharp2

%description    sharp
The GMime suite provides a core library and set of utilities which may be
used for the creation and parsing of messages using the Multipurpose
Internet Mail Extension (MIME). The devel-package contains support
for developing mono applications that use libgmime.

%package sharp-devel
Summary: %{name}-sharp-devel
Group: Development/Libraries
Requires: %{name}-sharp = %{version}-%{release}

%description sharp-devel
%{name}-sharp-devel

%endif

%prep
%setup -q -n %{tarname}-%{version}
%if %{_lib} == "lib64"
%patch0 -p1 -b .lib64
%endif

%patch1 -p1 -b .gacutils

%build
gtkdocize --copy
autoreconf -fvi
%ifnarch ppc64 alpha sparc64
%configure --enable-mono --enable-gtk-doc --enable-largefile
%else
%configure --enable-gtk-doc --enable-largefile
%endif
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# Don't conflict with sharutils:
mv %{buildroot}%{_bindir}/uuencode %{buildroot}%{_bindir}/gmime-uuencode
mv %{buildroot}%{_bindir}/uudecode %{buildroot}%{_bindir}/gmime-uudecode


%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%exclude %{_bindir}/gmime-uu??code
%{_bindir}/gmime-config
%{_libdir}/libgmime-2.0.so.*
%exclude %{_libdir}/libgmime-2.0.la
%{_libdir}/gmimeConf.sh

%files devel
%defattr(-,root,root,-)
%{_libdir}/libgmime-2.0.a
%{_libdir}/libgmime-2.0.so
%{_libdir}/pkgconfig/gmime-2.0.pc
%{_includedir}/gmime-2.0
%{_datadir}/gtk-doc/html/gmime

%ifnarch ppc64 alpha sparc64
%files sharp
%defattr(-,root,root,-)
%{_prefix}/lib/mono/gac/gmime-sharp/*
%{_prefix}/lib/mono/gmime-sharp
%{_datadir}/gapi-2.0/gmime-api.xml

%files sharp-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/gmime-sharp.pc
%endif

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.27-4m)
- rebuild for mono-2.10.9

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.27-3m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.27-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.27-1m)
- update to 2.2.27
- delete patch2 (merged)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.25-5m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.25-4m)
- rebuild against mono-2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.25-3m)
- full rebuild for mo7 release

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.25-2m)
- [SECURITY] CVE-2010-0409
- import a security patch from Rawhide (2.2.23-8)

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.25-1m)
- update to 2.2.25

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.24-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.24-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.24-1m)
- rename gmime22
- add sharp-devel package

* Sat Aug 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.24-1m)
- update to 2.2.24

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.23-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.23-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.23-2m)
- back to 2.2.23

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Mon Sep 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.23-1m)
- update to 2.2.23

* Mon Jul 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.22-1m)
- update to 2.2.22

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.21-1m)
- update to 2.2.21

* Sun May 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.20-1m)
- update to 2.2.20

* Mon May  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.19-1m)
- update to 2.2.19

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.18-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.18-1m)
- update to 2.2.18

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.17-2m)
- rebuild against gtk-sharp2-2.12.0

* Mon Feb 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.17-1m)
- update to 2.2.17

* Sun Feb  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.16-1m)
- update to 2.2.16

* Thu Jan  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.15-1m)
- update to 2.2.15

* Sun Dec 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.12-1m)
- update to 2.2.12

* Fri Dec 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Fri Jul 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Tue May 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.9-1m)
- update to 2.2.9

* Tue May 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-5m)
- fix BuildPrereq

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.1-4m)
- disable mono for ppc64, alpha, sparc64

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-4m)
- rebuild against mono-1.1.17.1 gtk-sharp 2.10.0

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.1-3m)
- revised for multilibarch

* Tue May  9 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.2.1-2m)
- add lib64 patch
- revised %files section in spec file

* Sat May  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.1-1m)
- start.
