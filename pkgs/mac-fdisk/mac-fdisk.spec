%global momorel 9

Summary: Apple disk partition manipulation tool
Name: mac-fdisk
Version: 0.1
Release: %{momorel}m%{?dist}
License: see "copyright"
Group: System Environment/Base
Source0: ftp://ftp.debian.org/debian/pool/main/m/mac-fdisk/%{name}_%{version}.orig.tar.gz 
NoSource: 0
Patch0: mac-fdisk_0.1-12.diff.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description 
The fdisk utilities from the MkLinux project, adopted for Linux/m68k. Mac-fdisk allows you to create and edit the partition table of a disk. It supports only the Apple partition format used on Macintosh and PowerMac, use pmac-fdisk for PC partition format disks as used on PowerPC machines. Mac-fdisk is an interactive tool with a menu similar to PC fdisk, supported options are somewhat different from PC fdisk due to the differences in partition format.

%prep
%setup -q -n mac-fdisk-0.1.orig
%patch0 -p1 

%build
make 

mv fdisk mac-fdisk
mv pdisk mac-pdisk
mv debian/changelog .
mv debian/copyright .
%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}/%{_mandir}/man8
install -s -m 755 mac-fdisk %{buildroot}/sbin
install -s -m 755 mac-pdisk %{buildroot}/sbin
install -m 644 mac-fdisk.8.in %{buildroot}/%{_mandir}/man8/mac-fdisk.8
install -m 644 pmac-fdisk.8.in %{buildroot}/%{_mandir}/man8/pmac-fdisk.8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README HISTORY changelog copyright
/sbin/mac-fdisk
/sbin/mac-pdisk
%{_mandir}/man8/mac-fdisk.8*
%{_mandir}/man8/pmac-fdisk.8*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-4m)
- revise for rpm46 (s/Patch/Patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-2m)
- %%NoSource -> NoSource

* Sun Jun 25 2005 mutecat <mutecat@momonga-linux.org>
- (0.1-1m)
- first import 
