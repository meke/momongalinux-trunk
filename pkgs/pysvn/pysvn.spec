%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           pysvn
Version:        1.7.6
Release:        %{momorel}m%{?dist}
Summary:        Pythonic style bindings for Subversion
Group:          Development/Languages
License:        Apache
URL:            http://pysvn.tigris.org/
Source0:        http://pysvn.barrys-emacs.org/source_kits/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel >= 2.7
BuildRequires:  PyXML
BuildRequires:  subversion-devel
BuildRequires:  krb5-devel
BuildRequires:  neon-devel
BuildRequires:  apr-devel
BuildRequires:  openssl-devel >= 1.0.0

%description
Pythonic style bindings for Subversion

%prep
%setup -q -n %{name}-%{version}

%build

pushd Source
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py configure --enable-debug --verbose --fixed-module-name
# Set correct build flags
%{__sed} -i -e 's/-Wall -fPIC -fexceptions -frtti/%{optflags} -fPIC -frtti/' Makefile
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 755 %{buildroot}%{python_sitearch}/%{name}
%{__install} -p -m 644 Source/pysvn/__init__.py %{buildroot}%{python_sitearch}/%{name}
%{__install} -p -m 755 Source/pysvn/_pysvn.so %{buildroot}%{python_sitearch}/%{name}

%check
%if 0
pushd Tests
%{__make} %{?_smp_mflags}
%endif

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc Docs/pysvn.html Docs/pysvn_prog_guide.html Docs/pysvn_prog_ref.html
%doc Docs/pysvn_prog_ref.js
%doc Examples
%doc LICENSE.txt
%{python_sitearch}/%{name}

%changelog
* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.6-1m)
- update to 1.7.6
- disable tests

* Tue Jan 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5-3m)
- support subversion-1.7

* Mon Jun 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.5-2m)
- import patch0 and patch1 from Fedora

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5-1m)
- update 1.7.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-2m)
- full rebuild for mo7 release

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.2-1m)
- update to 1.7.2
- NoSource again

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-5m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-3m)
- update to trunk r1172
-- support subversion-1.6.0

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-2m)
- rebuild against openssl-0.9.8k

* Sat Mar  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-2m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.2-2m)
- import from Fedora to Momonga
 
* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> - 1.6.2-3
- rebuild with new openssl

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.6.2-2
- Rebuild for Python 2.6

* Tue Oct 28 2008 Caitlyn O'Hanna <ravenoak@virtualxistenz.com> - 1.6.2-1
- Upstream to 1.6.2, upstream provided some build fixes to remove patches
-   (Thanks Barry!).  Re-enabled testing with provided patch to fix whitespace

* Sat Oct 11 2008 Caitlyn O'Hanna <ravenoak@virtualxistenz.com> - 1.6.1-2
- Fixed lingering module versioning in __init__

* Mon Oct 06 2008 Caitlyn O'Hanna <ravenoak@virtualxistenz.com> - 1.6.1-1
- Update to 1.6.1, fix F10 FBFS
- Disabled tests, might be because of subversion 1.5

* Wed Feb 27 2008 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.3-1
- Update to 1.5.3

* Thu Feb 14 2008 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.2-6
- Clean up. Name change (back to upstream)

* Tue Feb 12 2008 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.2-5
- Temporary fix for tests.  Need to work with upstream for permanent fix.

* Fri Feb 08 2008 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.2-4
- Fixed build requires, libgssapi-devel was still in there (for EL5 support)

* Fri Jan 11 2008 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.2-3
- Merged patches and spec changes by Terje Rosten <terje.rosten@ntnu.no>
- Fixed the test failures

* Fri Jan 04 2008 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.2-2 
- Attempting to make the spec work with different versions of Python

* Mon Sep 03 2007 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.2-1
- Update to 1.5.2
- Some spec clean up

* Fri Jan 12 2007 Timothy Selivanow <timothy.selivanow@virtualxistenz.com> - 1.5.0-1
- Initial spec creation

