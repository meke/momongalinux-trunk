%global momorel 6
%global pythonver 2.7

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A Python datetime library
Name: python-dateutil
Version: 1.4.1
Release: %{momorel}m%{?dist}
License: see "LICENSE"
URL: http://labix.org/python-dateutil
Group: Development/Libraries
Source0: http://labix.org/download/python-dateutil/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
BuildRequires: python-devel >= %{pythonver}

%description
The python dateutil module provides powerful extensions to the standard
datetime module.

* Computing of relative deltas (next month, next year, next monday,
   last week of month, etc)

* Computing of relative deltas between two given date and/or
   datetime objects

* Computing of dates based on very flexible recurrence rules, using
   a superset of the iCalendar specification. Parsing of RFC strings
   is supported as well.

* Generic parsing of dates in almost any string format

* Timezone (tzinfo) implementations for tzfile(5) format files
   (/etc/localtime, /usr/share/zoneinfo, etc), TZ environment string
   (in all known formats), iCalendar format files, given ranges
   (with help from relative deltas), local machine timezone, fixed
   offset timezone, UTC timezone, and Windows registry-based time
   zones.

* Internal up-to-date world timezone information based on Olson's
   database.
#'

* Computing of Easter Sunday dates for any given year, using Western,
Orthodox or Julian algorithms

%prep
%setup -q

%build
CFLAGS="%{optflags}" python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install --prefix=%{_prefix} --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE NEWS PKG-INFO README
%{python_sitelib}/dateutil
%{python_sitelib}/python_dateutil-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc43

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- initial package for libopensync-plugin-google-moto
- Summary and %%description are imported from opensuse
