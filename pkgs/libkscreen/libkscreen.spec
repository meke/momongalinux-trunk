%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.13.1
%global         kdelibsrel 1m
%global         qtver 4.8.6
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 1.0.4
%global         sourcedir %{release_dir}/%{name}/%{ftpdirver}/src

Name:           libkscreen
Version:        %{ftpdirver}
Release:        %{momorel}m%{?dist}
Summary:        Display configuration library
License:        GPLv2+
Group:          System Environment/Libraries
URL:            https://projects.kde.org/projects/playground/libs/libkscreen
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  libXrandr-devel
BuildRequires:  qjson-devel >= 0.8.1

%description
LibKScreen is a library that provides access to current configuration
of connected displays and ways to change the configuration.

%package        devel
Group:          Development/Libraries
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} .. 
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc COPYING
%{_kde4_libdir}/libkscreen.so.*
%{_kde4_libdir}/kde4/plugins/kscreen

%files devel
%{_includedir}/kscreen
%{_kde4_libdir}/libkscreen.so
%{_kde4_libdir}/cmake/LibKScreen
%{_kde4_libdir}/pkgconfig/kscreen.pc

%changelog
* Thu May 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Mon Apr  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Wed Jun 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.92-1m)
- update to 0.0.92

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.81-1m)
- update to 0.0.81

* Tue Jan 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.71-1m)
- initial build for Momonga Linux
