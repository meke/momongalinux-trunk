%global momorel 7
%global unstable 1
%if 0%{unstable}
#global svnrel 1082677
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.3.95
%global kdelibsrel 1m
%global qtver 4.6.0
%global qtrel 1m
%global cmakever 2.6.4
%global cmakerel 1m
%global ftpdirver 4.3.95
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global svnrel 1082677

Name:             webkitkde
Version:          0.0.5
Release:          0.%{svnrel}.%{momorel}m%{?dist}
Summary:          QtWebKit bindings to KDE
Group:            System Environment/Libraries
License:          LGPLv2+
URL:              http://techbase.kde.org/Projects/WebKit
# To create a tarball from svn use create_tarball.rb script
#
# http://websvn.kde.org/trunk/KDE/kdesdk/scripts/createtarball/
#
# To use it you will need the script itself and a config.ini in the same directory
# config.ini contents:
#
# [webkitkde]
# mainmodule   = playground
# submodule    = libs
# kde_release  = no
# version      = 0.0.5svn[svndate]
# docs         = no
#
# To create a new checkout use it with anonymous svn access
# ./create_tarball.rb -n -a webkitkde
Source0:          %{name}-%{version}-svn.tar.bz2
Patch0:           webkitkde-0.0.5-svn-linking.patch
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:    cmake
BuildRequires:    gettext
BuildRequires:    kdelibs-devel  >= %{kdever}
BuildRequires:    soprano-devel

%description
This project aims to integrate WebKit (QtWebKit) into KDE.
The project is made up of the following components:
- WebKitKDE: The binding between KDE and QtWebKit
- WebKitPart: A KPart based on it
The WebKitKDE library sits on top of QtWebKit, providing
KDE integration (for example, WebActions using KDE icons
and shortcuts, downloading using KIO, etc).

%package -n webkitpart
Summary:          A KPart based on WebKitKDE library
Group:            System Environment/Libraries
Requires:         %{name} = %{version}-%{release}

%description -n webkitpart
WebKitPart is a KPart that allows KPart-based applications,
for example Konqueror, to make use of WebKit for viewing
web pages.

%package -n webkitpart-devel
Summary:          Development files for webkitpart
Group:            Development/Libraries
Requires:         kdelibs-devel
Requires:         webkitpart = %{version}-%{release}

%description -n webkitpart-devel
Development files for the webkitpart.

%prep
%setup -q -n %{name}-%{version}-svn
%patch0 -p1 -b .linking

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}
#mkdir #{buildroot}#{_kde4_appsdir}/webkitpart/kpartplugins

#find_lang #{name}

%clean
rm -rf %{buildroot}

%if 0%{?kde43}
%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig
%endif

%post -n webkitpart
/sbin/ldconfig
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%posttrans -n webkitpart
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%postun -n webkitpart
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
fi

%files
%defattr(-,root,root,-)
%doc README COPYING.LIB

%files -n webkitpart
%defattr(-,root,root,-)
%doc README COPYING.LIB
%{_kde4_libdir}/kde4/kwebkitpart.so.*
%{_kde4_libdir}/libkwebkit.so.*
%{_kde4_iconsdir}/hicolor/*/apps/webkit.png
%{_kde4_appsdir}/kwebkitpart
%{_kde4_datadir}/kde4/services/kwebkitpart.desktop

%files -n webkitpart-devel
%defattr(-,root,root,-)
%doc README
%{_kde4_includedir}/KDE/KWebKitPart
%{_kde4_includedir}/kwebkitpart.h
%{_kde4_includedir}/kwebkit_export.h
%{_kde4_libdir}/kde4/kwebkitpart.so
%{_kde4_libdir}/libkwebkit.so
%{_kde4_appsdir}/cmake/modules/FindWebKitPart.cmake

%changelog
* Sat Apr 16 2011 McLellan Daniel <daniel.mclellan@gmail.com>
- (0.0.5-0.1082677.7m)
- added needed BuildRequires of soprano-devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-0.1082677.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-0.1082677.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.5-0.1082677.4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.5-0.1082677.3m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.5-0.1082677-2m)
- explicitly link libkio

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.5-0.1082677-1m)
- update to 0.0.5svn1082677

* Sun Jan 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.4-0.1079294-1m)
- update to 0.0.4svn1079294

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.3-0.1057318-1m)
- import from Fedora devel

* Thu Dec  3 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.3-0.2.svn1057318
- svn 1057318

* Tue Nov 24 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.3-0.1.svn1049337
- version changed to 0.0.3 (kdewebkit moved to kdelibs 4.4)
- drop webkitkde-devel subpackage for KDE 4.4

* Wed Nov 18 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.0.2-0.2.20091109svn
- rebuild (qt-4.6.0-rc1, fc13+)

* Mon Nov  9 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.2-0.1.20091109svn
- version changed to 0.0.2 for new API

* Mon Nov  9 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.1-0.6.20091109svn
- removed kdelauncher from CMakeLists because it not installs

* Mon Nov  9 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.1-0.5.20091109svn
- snapshot 1046552 with new API

* Sun Sep 27 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.1-0.2.20090924svn
- webkitpart should owns kpartplugins in webkitpart apps dir

* Thu Sep 24 2009 Alexey Kurov <nucleo@fedoraproject.org> - 0.0.1-0.1.20090924svn
- Initial RPM release
