# Generated from activeldap-3.1.1.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname activeldap

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: ActiveLdap is a object-oriented API to LDAP
Name: rubygem-%{gemname}
Version: 3.1.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: Ruby or GPLv2+ 
URL: http://ruby-activeldap.rubyforge.org/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(activemodel) => 3.1.0
Requires: rubygem(activemodel) < 3.4
Requires: rubygem(locale) 
Requires: rubygem(fast_gettext) 
Requires: rubygem(gettext_i18n_rails) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
'ActiveLdap' is a ruby library which provides a clean
objected oriented interface to the Ruby/LDAP library.  It was inspired
by ActiveRecord. This is not nearly as clean or as flexible as
ActiveRecord, but it is still trivial to define new objects and manipulate
them with minimal difficulty.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README.textile
%doc %{geminstdir}/TODO
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-2m)
- change Request activemodel version

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-1m)
- update 3.1.1

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-1m)
- update 3.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-2m)
- Requires: ruby(abi)-1.9.1

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Wed Nov 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- rebuild against locale-2.0.5, gettext-2.1.0 and gettext_activerecord-2.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Mon Aug 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- import from Fedora to Momonga

* Thu Jul 30 2009 Darryl Pierce <dpierce@redhat.com> - 1.1.0-3
- Changed dependency on rubygem-activerecord to be >= 2.3.2.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 20 2009 Darryl Pierce <dpierce@redhat.com> - 1.1.0-1
- Release 1.1.0 of ActiveLdap.
- Dependency on rubygem-hoe changed to 2.3.2.
- Dependency on rubygem-activerecord changed to 2.3.2.
- Dependency on rubygem-locale added. 
- Dependency on rubygem-gettext added.
- Dependency on rubygem-gettext_activerecord added.

* Fri Jun  5 2009 Darryl Pierce <dpierce@redhat.com> - 1.0.9-1
- Release 1.0.9 of ActiveLdap.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 23 2009 Darryl Pierce <dpierce@redhat.com> - 1.0.2-1
- Release 1.0.2 of ActiveLdap.

* Tue Jun 17 2008 Darryl Pierce <dpierce@redhat.com> - 1.0.1-1
- Release 1.0.1 of the gem.

* Mon Jun 09 2008 Darryl Pierce <dpierce@redhat.com> - 1.0.0-1
- Release 1.0.0 of the gem.

* Thu May 15 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-10
- First official build for rawhide.

* Mon May 12 2008 Darryl Pierce <dpierce@redaht.com> - 0.10.0-9
- First build updated for Fedora.

* Tue Apr 29 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-8
- Missed a script.

* Tue Apr 29 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-7
- Fixing three scripts to be executable.

* Tue Apr 29 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-6
- Fixed the shebang in all scripts to remove an implied dependency on /usr/bin/ruby1.8

* Mon Apr 28 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-5
- Added requirement for ruby-ldap

* Mon Apr 28 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-4
- Moved all macro definitions to the top of the spec file.

* Mon Apr 28 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-3
- Modified the spec to fix rpmlint errors

* Mon Apr 28 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-2
- Fixed the spec file to comply with packaging guidelines

* Fri Apr 18 2008 Darryl Pierce <dpierce@redhat.com> - 0.10.0-1
- Initial package
