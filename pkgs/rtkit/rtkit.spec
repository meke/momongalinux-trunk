%global momorel 2
Name:             rtkit
Version:          0.11
Release: %{momorel}m%{?dist}
Summary:          Realtime Policy and Watchdog Daemon
Group:            System Environment/Base
# The daemon itself is GPLv3+, the reference implementation for the client BSD
License:          GPLv3+ and BSD
URL:              http://git.0pointer.de/?p=rtkit.git
Requires:         dbus
Requires:         polkit
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
BuildRequires:    dbus-devel >= 1.2
BuildRequires:    libcap-devel
BuildRequires:    polkit-devel
Source0:          http://0pointer.de/public/%{name}-%{version}.tar.xz
NoSource: 0
Patch1:		rtkit-0.11-temp.patch
Patch2:           rtkit-mq_getattr.patch
Patch3:           0001-SECURITY-Pass-uid-of-caller-to-polkit.patch
Patch4:           rtkit-controlgroup.patch

%description
RealtimeKit is a D-Bus system service that changes the
scheduling policy of user processes/threads to SCHED_RR (i.e. realtime
scheduling mode) on request. It is intended to be used as a secure
mechanism to allow real-time scheduling to be used by normal user
processes.

%prep
%setup -q

%patch1 -p1 -b .temp~
%patch2 -p1
%patch3 -p1
%patch4 -p1

%build
%configure --with-systemdsystemunitdir=%{_unitdir}
%make

./rtkit-daemon --introspect > org.freedesktop.RealtimeKit1.xml

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -D org.freedesktop.RealtimeKit1.xml %{buildroot}/%{_datadir}/dbus-1/interfaces/org.freedesktop.RealtimeKit1.xml

%pre
getent group rtkit >/dev/null 2>&1 || groupadd \
        -r \
        -g 172 \
        rtkit
getent passwd rtkit >/dev/null 2>&1 || useradd \
        -r -l \
        -u 172 \
        -g rtkit \
        -d /proc \
        -s /sbin/nologin \
        -c "RealtimeKit" \
        rtkit
:;

%post
if [ $1 -eq 1 ]; then
        /bin/systemctl enable rtkit.service >/dev/null 2>&1 || :
fi
dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig >/dev/null 2>&1 || :

%preun
if [ "$1" -eq 0 ]; then
        /bin/systemctl --no-reload disable rtkit-daemon.service >/dev/null 2>&1 || :
        /bin/systemctl stop rtkit-daemon.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%files
%defattr(0644,root,root,0755)
%doc README GPL LICENSE rtkit.c rtkit.h
%attr(0755,root,root) %{_sbindir}/rtkitctl
%attr(0755,root,root) %{_libexecdir}/rtkit-daemon
%{_datadir}/dbus-1/system-services/org.freedesktop.RealtimeKit1.service
%{_datadir}/dbus-1/interfaces/org.freedesktop.RealtimeKit1.xml
%{_datadir}/polkit-1/actions/org.freedesktop.RealtimeKit1.policy
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/org.freedesktop.RealtimeKit1.conf
%{_unitdir}/rtkit-daemon.service
%{_mandir}/man8/*

%changelog
* Sun Feb  2 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-2m)
- fix build failure
- add patches for glibc-2.18.90+

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-1m)
- reimport from fedora

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10-1m)
- update 0.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-3m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-2m)
- use Requires

* Sun Dec 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-1m)
- initial package for Momonga Linux
