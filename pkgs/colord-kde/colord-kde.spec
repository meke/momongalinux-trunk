%global         momorel 1
%global         qtver 4.8.4
%global         kdever 4.10.2
%global         ftpdir pub/kde/stable/%{name}/%{version}/src

Name:           colord-kde
Version:        0.3.0
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
Summary:        Colord support for KDE

License:        GPLv2+
URL:            http://www.kde.org/
Source0:        ftp://ftp.kde.org/%{ftpdir}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRequires:  cmake
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  colord-devel
# need kcmshell4 from kde-runtime at least (from kcm_touchpad SPEC)
Requires:       kde-runtime >= %{kdever}
# colord is a dbus daemon
Requires:       colord

%description
KDE support for colord including KDE Daemon module and System Settings module.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%files
%doc COPYING MAINTAINERS TODO
%{_kde4_bindir}/colord-kde-icc-importer
%{_kde4_libdir}/kde4/*.so
%{_kde4_datadir}/kde4/services/*
%{_kde4_datadir}/applications/kde4/colordkdeiccimporter.desktop

%changelog
* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- import from Fedora

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Apr 05 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.2.0-1
- update to version 0.2.0

* Thu Mar 22 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.1.0-2
- fix kcmshell4 visibility by setting X-KDE-ParentApp

* Wed Mar 21 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.1.0-1
- initial try
