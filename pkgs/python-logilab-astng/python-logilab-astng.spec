%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-logilab-astng
Version:        0.20.1
Release:        %{momorel}m%{?dist}
Summary:        Python Abstract Syntax Tree New Generation

Group:          Development/Languages
License:        GPLv2+
URL:            http://www.logilab.org/projects/astng/
Source0:        ftp://ftp.logilab.org/pub/astng/logilab-astng-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Provides:       python-astng = %{version}-%{release}
Obsoletes:      python-astng <= 0.16.0

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7 python-setuptools
Requires:       python-logilab-common

%description
The aim of this module is to provide a common base representation of
python source code for projects such as pychecker, pyreverse,
pylint, and others. It extends the class defined in the compiler.ast 
python module with some additional methods and attributes.


%prep
%setup -q -n logilab-astng-%{version}


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT%{python_sitelib}/logilab/astng/test
# Provided by the python-logilab-common package
rm -f $RPM_BUILD_ROOT%{python_sitelib}/logilab/__init__.*
# Fix encoding in readme
for FILE in README; do
    iconv -f iso-8859-15 -t utf-8 $FILE > $FILE.utf8
    mv -f $FILE.utf8 $FILE
done

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
%{python_sitelib}/logilab/astng
%{python_sitelib}/logilab_astng*.egg-info
%{python_sitelib}/logilab_astng*.pth

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20.1-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20.1-1m)
- import from Fedora 13 and update to 0.20.1

* Tue Apr 06 2010 Brian C. Lane <bcl@redhat.com> - 0.20.0-1
- Added python-setuptools to BuildRequires
- Upstream 0.20.0

* Sun Aug 30 2009 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.19.1-1
- Upstream 0.19.1 (bugfixes)

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.19.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jun 17 2009 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.19.0-1
- Upstream 0.19.0
- Fixes for better support of python 2.5 and 2.6

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Dec 27 2008 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.17.4-1
- Upstream 0.17.4

* Thu Jan 17 2008 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.17.2-1
- Upstream 0.17.2
- Package .egg-info file

* Mon Dec 24 2007 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.17.1-1
- Upstream 0.17.1
- Adjust license to a more specific GPLv2+
- Fix docs to be valid utf-8

* Sun Apr 01 2007 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.17.0-1
- Upstream 0.17.0

* Sun Dec 17 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.16.3-1
- Upstream 0.16.3

* Tue Sep 26 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.16.1-2
- Setting Provides/Obsoletes as per guidelines.

* Tue Sep 26 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.16.1-1
- Renaming package python-logilab-astng from python-astng. Should have done
  a while ago.
- Upstream version 0.16.1

* Mon May 01 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.16.0-0
- Version 0.16.0

* Sun Mar 12 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.15.1-1
- Version 0.15.1

* Thu Jan 12 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.14.0-1
- Version 0.14.0
- Drop the modname patch

* Tue Nov 15 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.13.1-2
- Patch for the modname traceback

* Sat Nov 12 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.13.1-1
- Fedora Extras import
- Disttagging

* Mon Nov 07 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.13.1-0.1
- Version 0.13.1
- Remove our own GPL license text, since it's now included.

* Sun Nov 06 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.13.0-0.1
- Initial packaging.
