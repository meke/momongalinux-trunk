%global momorel 12

%global fontname iso8859-2

%global __mkfontdir umask 133;mkfontdir
%global catalogue        %{_sysconfdir}/X11/fontpath.d

Name: %{fontname}-fonts
Version: 1.0
Release: %{momorel}m%{?dist}
License: MIT
# Upstream url http://www.biz.net.pl/images/ISO8859-2-bdf.tar.gz is dead now.
Source: ISO8859-2-bdf.tar.gz

Patch0: XFree86-ISO8859-2-1.0-redhat.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Group: User Interface/X
Summary: Central European language fonts for the X Window System
Buildrequires: xorg-x11-font-utils
BuildRequires: fontpackages-devel 
Requires: mkfontdir
 
%description
If you use the X Window System and you want to display Central
European fonts, you should install this package.

%package common
Summary:        Common files of %{name}
Requires:       fontpackages-filesystem

%description common
Common files of %{name}.

%package -n %{fontname}-misc-fonts
Group: User Interface/X
Summary: A set of misc Central European language fonts for X
Requires: mkfontdir
Obsoletes: fonts-ISO8859-2 < 1.0-23
Provides: fonts-ISO8859-2 = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-misc-fonts
This package contains a set of Central European fonts, in
compliance with the ISO8859-2 standard.

%package -n %{fontname}-75dpi-fonts
Group: User Interface/X
Summary: A set of 75dpi Central European language fonts for X
Requires: mkfontdir
Obsoletes: fonts-ISO8859-2-75dpi < 1.0-23
Provides: fonts-ISO8859-2-75dpi = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-75dpi-fonts
This package contains a set of Central European language fonts in 75 dpi
resolution for the X Window System. 


%package -n %{fontname}-100dpi-fonts
Group: User Interface/X
Summary: A set of 100dpi Central European language fonts for X
Requires: mkfontdir
Obsoletes: fonts-ISO8859-2-100dpi < 1.0-23
Provides: fonts-ISO8859-2-100dpi = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-100dpi-fonts
This package includes Central European (ISO8859-2) fonts, in 100 dpi
resolution, for the X Window System.


%prep
%setup -c -q
chmod 644 RELEASE_NOTES.TXT

%patch0 -p1 -b .redhat

%build
make all

%install
rm -rf $RPM_BUILD_ROOT
make install PREFIX=$RPM_BUILD_ROOT \
           FONTDIR=$RPM_BUILD_ROOT%{_fontdir}

# Install catalogue symlink
mkdir -p $RPM_BUILD_ROOT%{catalogue}
ln -sf %{_fontdir}/misc $RPM_BUILD_ROOT%{catalogue}/%{fontname}-misc-fonts
ln -sf %{_fontdir}/75dpi $RPM_BUILD_ROOT%{catalogue}/%{fontname}-75dpi-fonts
ln -sf %{_fontdir}/100dpi $RPM_BUILD_ROOT%{catalogue}/%{fontname}-100dpi-fonts

%clean
rm -rf $RPM_BUILD_ROOT

%post -n %{fontname}-misc-fonts
{
    %__mkfontdir %{_fontdir}/misc
} &> /dev/null || :

%post -n %{fontname}-75dpi-fonts
{
    %__mkfontdir %{_fontdir}/75dpi
} &> /dev/null || :

%post -n %{fontname}-100dpi-fonts
{
    %__mkfontdir %{_fontdir}/100dpi
} &> /dev/null || :


%files -n %{fontname}-misc-fonts
%defattr(-,root,root,-)
%doc
%dir %{_fontdir}/misc
%{_fontdir}/misc/*.gz
%verify(not md5 size mtime) %{_fontdir}/misc/fonts.alias
%verify(not md5 size mtime) %{_fontdir}/misc/fonts.dir
%{catalogue}/%{fontname}-misc-fonts

%files -n %{fontname}-75dpi-fonts
%defattr(-,root,root,-)
%doc
%dir %{_fontdir}/75dpi
%{_fontdir}/75dpi/*.gz
%verify(not md5 size mtime) %{_fontdir}/75dpi/fonts.alias
%verify(not md5 size mtime) %{_fontdir}/75dpi/fonts.dir
%{catalogue}/%{fontname}-75dpi-fonts

%files -n %{fontname}-100dpi-fonts
%defattr(-,root,root,-)
%doc
%dir %{_fontdir}/100dpi
%{_fontdir}/100dpi/*.gz
%verify(not md5 size mtime) %{_fontdir}/100dpi/fonts.alias
%verify(not md5 size mtime) %{_fontdir}/100dpi/fonts.dir
%{catalogue}/%{fontname}-100dpi-fonts

%files common
%defattr(-,root,root,-)
%doc *.TXT
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-10m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9m)
- renamed to iso8859-2-fonts from fonts-ISO8859-2
- sync with Fedora 13
-- * Mon May 17 2010 Parag <pnemade AT redhat.com> 1.0-24
-- - Repackaged according to current Fonts packaging guidelines.
-- - Added -common subpackage

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- remove duplicate directories

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- sync with Rawhide (1.0-22)

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc43

* Wed Jun 13 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0-2m)
- change BuildRequires:  mkfontdir   --> xorg-x11-mkfontdir
-                        chkfontpath --> chkfontpath-momonga

* Sun Jun 10 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.0-17.1
- rebuild

* Tue Feb 21 2006 Karsten Hopp <karsten@redhat.de> 1.0-17
- BuildRequire xorg-x11-font-utils

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov  7 2005 Caolan McNamara <caolanm@redhat.com> - 1.0-16
- modular X

* Fri Oct 28 2005 Caolan McNamara <caolanm@redhat.com> - 1.0-15
- package description mentions old package name

* Thu Mar  3 2005 Mike A. Harris <mharris@redhat.com> - 1.0-14
- *cough* Rebuild for FC4 with gcc 4 *cough*

* Wed Sep 22 2004 Owen Taylor <otaylor@redhat.com> - 1.0-13
- Require xorg-x11-font-utils (#125031, Maxim Dzumanenko)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Oct  7 2002 Mike A. Harris <mharris@redhat.com> 1.0-10
- All-arch rebuild
-
* Fri Jun 21 2002 Mike A. Harris <mharris@redhat.com> 1.0-9
- Totally non-automated rebuild done manually without assistance of
  any fancy pants scripts or other automation utilities  ;o)
- Added :unscaled FPE attributes to all bitmap font rpm scripts

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Jun  4 2002 Mike A. Harris <mharris@redhat.com> 1.0-7
- Removed ulT1mo fonts

* Thu May 23 2002 Tim Powers <timp@redhat.com> 1.0-4
- automated rebuild

* Mon Apr 15 2002 Mike A. Harris <mharris@redhat.com> 1.0-3
- Made Type1 subpackage to own the afm/pfm directories
- removed fonts.* glob from all subpackages, and manually listed each of the
  various fonts.{alias,dir,scale} files individually with the proper
  ghost/config/verify flags (MF #61694)
- Cleaned up post/postun scripts to match the XFree86 packaging style, and
  made the fonts.dir files generated at install time like they should be
  instead of included with the packaging which is broken and evil.

* Mon Apr  8 2002 Mike A. Harris <mharris@redhat.com> 1.0-2
- Added proper prereqs for chkfontpath, mkfontdir for bug (#62741)

* Thu Feb 28 2002 Mike A. Harris <mharris@redhat.com> 1.0-1
- Moved ulT1mo fonts from XFree86 packaging back out into their own happy
  rpm package.  Renamed it to fonts-foo to remove XFree86 reference since
  it is not an XFree86 package.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 17 2000 Than Ngo <than@redhat.de>
- rebuilt in the new build environment

* Sat May 27 2000 Ngo Than <than@redhat.de>
- rebuild for 7.0

* Thu Aug 19 1999 Preston Brown <pbrown@redhat.com>
- fonts.* are now config files

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Tue Mar 09 1999 Preston Brown <pbrown@redhat.com>
- fixed typos

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Tue Feb 16 1999 Preston Brown <pbrown@redhat.com>
- chkfontpath compliant.

* Sat Feb 06 1999 Preston Brown <pbrown@redhat.com>
- fonts moved to /usr/share/fonts/ISO8859-2 under new font scheme.

* Fri Dec 18 1998 Preston Brown <pbrown@redhat.com>
- bumped spec number for initial rh 6.0 build

* Mon Oct 05 1998 Cristian Gafton <gafton@redhat.com>
- packaged for RH 5.2
- made a noarch package

