%global         momorel 6

Name:           perl-WWW-Wikipedia
Version:        2.01
Release:        %{momorel}m%{?dist}
Summary:        Automated interface to the Wikipedia
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/WWW-Wikipedia/
Source0:        http://www.cpan.org/authors/id/B/BR/BRICAS/WWW-Wikipedia-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Text-Autoformat
BuildRequires:  perl-URI
Requires:       perl-libwww-perl
Requires:       perl-Text-Autoformat
Requires:       perl-URI
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
WWW::Wikipedia provides an automated interface to the Wikipedia
http://www.wikipedia.org, which is a free, collaborative, online
encyclopedia. This module allows you to search for a topic and return the
resulting entry. It also gives you access to related topics which are also
available via the Wikipedia for that entry.

%prep
%setup -q -n WWW-Wikipedia-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/wikipedia
%{perl_vendorlib}/WWW/Wikipedia
%{perl_vendorlib}/WWW/Wikipedia.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-4m)
- reuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-2m)
- rebuild against perl-5.16.3

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.00-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update to 2.00

* Thu Feb 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.99-1m)
- update to 1.99

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.98-1m)
- update to 1.98

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.97-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.97-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.97-2m)
- full rebuild for mo7 release

* Thu Jun 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.97-1m)
- update to 1.97

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-4m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-1m)
- update to 1.96

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.95-2m)
- rebuild against perl-5.10.1

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.95-1m)
- update to 1.95

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.94-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.94-2m)
- rebuild against gcc43

* Tue Feb 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.94-1m)
- update to 1.94

* Wed Dec 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.93-1m)
- update to 1.93

* Mon Oct  1 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.92-2m)
- disable make test

* Mon Sep 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
