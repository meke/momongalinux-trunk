%global momorel 1

%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%global pear_name Crypt_CHAP

Name:           php-pear-Crypt-CHAP
Version:        1.5.0
Release:        %{momorel}m%{?dist}
Summary:        Class to generate CHAP packets
Group:          Development/Languages
License:        BSD
URL:            http://pear.php.net/package/Crypt_CHAP
Source0:        http://pear.php.net/get/%{pear_name}-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear >= 1.4.9
Requires:       php-pear(PEAR) 
Requires:       php-mcrypt >= 5.1.2
Requires(post): %{__pear}
Requires(postun): %{__pear}
Provides:       php-pear(%{pear_name}) = %{version}

%description
This package provides Classes for generating CHAP packets.  
Currently these types of CHAP are supported: 
- CHAP-MD5, 
- MS-CHAPv1, 
- MS-CHAPv2. 

%prep
%setup -qc
# Create a "localized" php.ini to avoid build warning
cp /etc/php.ini .
echo "date.timezone=UTC" >>php.ini

cd %{pear_name}-%{version}
# package.xml is V2
mv ../package.xml %{name}.xml

%build
cd %{pear_name}-%{version}
# Empty build section, most likely nothing required.

%install
cd %{pear_name}-%{version}
rm -rf $RPM_BUILD_ROOT docdir
PHPRC=../php.ini %{__pear} install --nodeps --packagingroot $RPM_BUILD_ROOT %{name}.xml

# Clean up unnecessary files
rm -rf $RPM_BUILD_ROOT%{pear_phpdir}/.??*

# Install XML package description
mkdir -p $RPM_BUILD_ROOT%{pear_xmldir}
install -pm 644 %{name}.xml $RPM_BUILD_ROOT%{pear_xmldir}

%clean
rm -rf $RPM_BUILD_ROOT

%check
cd %{pear_name}-%{version}
PHPRC=../php.ini %{__pear} run-tests \
   -i "-d include_path=%{buildroot}%{pear_phpdir}:%{pear_phpdir}" \
   tests

%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        %{pear_name} >/dev/null || :
fi

%files
%defattr(-,root,root,-)
%{pear_xmldir}/%{name}.xml
%{pear_testdir}/%{pear_name}
%{pear_phpdir}/Crypt

%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- import from Fedora for moodle-2.4.1

* Tue Aug 14 2012 Remi Collet <remi@fedoraproject.org> - 1.5.0-5
- rebuilt for new pear_testdir

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Sep 19 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.5.0-1
- upstream 1.5.0 with patches merged
- run tests during %%check

* Fri Aug 27 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.0.2-3
- add patch for deprecated mhash extension 
  Upstream bug is http://pear.php.net/bugs/17828

* Thu Aug 26 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.0.2-2
- clean define
- set date.timezone during build
- review desc. (remove ref to mhash)

* Sun Aug 30 2009 Christopher Stone <chris.stone@gmail.com> 1.0.2-1
- Upstream sync

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 13 2009 Remi Collet <Fedora@FamilleCollet.com> - 1.0.1-3
- remove mhash dependency (optional, and not provided by php 5.3.0)
- rename Crypt_CHAP.xml to php-pear-Crypt-CHAP.xml

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Mar 14 2007 Christopher Stone <chris.stone@gmail.com> 1.0.1-1
- Upstream sync
- Remove no longer needed patch

* Tue Mar 13 2007 Christopher Stone <chris.stone@gmail.com> 1.0.0-2
- Apply patch to fix warnings/errors on test scripts (bz #222597)

* Sun Jan 14 2007 Christopher Stone <chris.stone@gmail.com> 1.0.0-1
- Initial Fedora release
