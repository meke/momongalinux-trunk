%global momorel 1
%define tarball xf86-video-geode
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 AMD Geode video driver
Name:      xorg-x11-drv-geode
Version:   2.11.14
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/wiki/AMDGeodeDriver
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0

License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:  xorg-x11-drv-amd = %{version}-%{release}
Obsoletes: xorg-x11-drv-amd <= 2.7.7.7

ExclusiveArch: %{ix86}

BuildRequires: pkgconfig
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: xorg-x11-server-sdk >= 1.13.0
BuildRequires: xorg-x11-proto-devel

Requires:  xorg-x11-server-Xorg >= 1.13.0

BuildRequires: autoconf automake

%description 
X.Org X11 AMD Geode video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static --libdir=%{_libdir} --mandir=%{_mandir} \
	     --enable-visibility
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# Compat symlink for legacy driver name so existing xorg.conf's do not break
ln -s geode_drv.so $RPM_BUILD_ROOT%{_libdir}/xorg/modules/drivers/amd_drv.so

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog NEWS README TODO
%{driverdir}/amd_drv.so
%{driverdir}/geode_drv.so
%{driverdir}/ztv_drv.so
%exclude %{driverdir}/*.la

%changelog
* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.14-1m)
- update to 2.11.14

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.12-3m)
- rebuild against xorg-x11-server-1.13.0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.13-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jan  1 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.12-1m)
- update to 2.11.13

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.12-3m)
- rebuild for xorg-x11-server-1.10.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.12-2m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.12-1m)
- update to 2.11.12

* Tue Dec 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.11-1m)
- update to 2.11.11

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.10-1m)
- update to 2.11.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.8-2m)
- full rebuild for mo7 release

* Fri May 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.8-1m)
- update 2.11.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.6-2m)
- rebuild against xorg-x11-server-1.7.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.6-1m)
- update 2.11.6

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.3-1m)
- update 2.11.3

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.1-2m)
- rebuild against xorg-x11-server 1.6

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.1-1m)
- update 2.11.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.1-2m)
- rebuild against rpm-4.6

* Sun Aug 24 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.1-1m)
- update to 2.10.1

* Wed Jun 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0
- rename from xorg-x11-drv-amd
- sync Fedora

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.7.7-1m)
- update 2.7.7.7
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.7.6-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.7.6-3m)
- %%NoSource -> NoSource

* Thu Jan 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.7.6-2m)
- ExclusiveArch: %%{ix86} only

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.7.6-1m)
- Initial Commit Momonga Linux
