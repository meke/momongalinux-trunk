%global momorel 8

%global fontname    un-extra
%global fontconf    66-%{fontname}

%global archivename un-fonts-extra
%define alphatag    080608

%define common_desc_en \
The UN set of Korean TrueType fonts is derived from the HLaTeX Type1 fonts \
made by Koaunghi Un in 1998. They were converted to TrueType with \
FontForge(PfaEdit) by Won-kyu Park in 2003. \
The Un Extra set is composed of: \
\
- UnPen, UnPenheulim: script \
- UnTaza: typewriter style \
- UnBom: decorative \
- UnShinmun \
- UnYetgul: old Korean printing style \
- UnJamoSora, UnJamoNovel, UnJamoDotum, UnJamoBatang \
- UnVada \
- UnPilgia: script \

%define common_desc_ko \
은글꼴 시리즈는 HLaTex개발자이신 은광희님이 1998년에 개발한 폰트입니다. \
2003년에 박원규님이 FontForge를 이용하여 트루타입폰트로 변환했습니다. \
은글꼴은 가장 일반적인 글꼴들입니다. \
\
Extra 모음 \
- 은펜, 은펜흘림: script \
- 은타자: typewriter style \
- 은봄: decorative \
- 은신문 \
- 은옛글: old Korean printing style \
- 은자모소라, 은자모노벨, 은자모돋음, 은자모바탕 \
- 은바다 \
- 은필기a: script \ 

Name:        %{fontname}-fonts
Version:     1.0.2
Release:     0.%{alphatag}.%{momorel}m%{?dist}
Summary:     Un Extra family of Korean TrueType fonts
Summary(ko): 한글 은글꼴 Extra 모음

Group:     User Interface/X
License:   GPLv2
URL:       http://kldp.net/projects/unfonts/
Source0:   http://kldp.net/frs/download.php/4696/%{archivename}-%{version}-%{alphatag}.tar.gz
NoSource:  0
Source1:   %{name}-bom-fontconfig.conf
Source2:   %{name}-jamobatang-fontconfig.conf
Source3:   %{name}-jamodotum-fontconfig.conf
Source4:   %{name}-jamonovel-fontconfig.conf
Source5:   %{name}-jamosora-fontconfig.conf
Source6:   %{name}-pen-fontconfig.conf
Source7:   %{name}-penheulim-fontconfig.conf
Source8:   %{name}-pilgia-fontconfig.conf
Source9:   %{name}-shinmun-fontconfig.conf
Source10:  %{name}-taza-fontconfig.conf
Source11:  %{name}-vada-fontconfig.conf
Source12:  %{name}-yetgul-fontconfig.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
BuildRequires: fontpackages-devel


%package common
Summary:     Common files for the Un Extra font set
Requires:    fontpackages-filesystem

%files common
%defattr(0644,root,root,0755)
%doc COPYING README


%define un_subpkg() \
%package -n %{fontname}-%{1}-fonts \
Summary:     Un Extra fonts - %(echo %2) \
Summary(ko): 한글 은글꼴 Extra 모음 - %(echo %3) \
Group:       User Interface/X \
Requires:    %{name}-common = %{version}-%{release} \
Obsoletes:   %{fontname}-fonts-%{1} < 1.0.2-0.080608.4m \
\
\

%un_subpkg bom UnBom 은봄
%un_subpkg jamobatang UnJamoBatang 은자모바탕
%un_subpkg jamodotum UnJamoDotum 은자모돋음
%un_subpkg jamonovel UnJamoNovel 은자모노벨
%un_subpkg jamosora UnJamoSora 은자모소라
%un_subpkg pen UnPen 은펜
%un_subpkg penheulim UnPenheulim 은펜흘림
%un_subpkg pilgia UnPilgia 은필기a
%un_subpkg shinmun UnShinmun 은신문
%un_subpkg taza UnTaza 은타자
%un_subpkg vada UnVada 은바다
%un_subpkg yetgul UnYetgul 은옛글

%description
%common_desc_en

%description -l ko
%common_desc_ko

%description common
%common_desc_en

This package consists of files used by other %{name} packages.

%description -n %{fontname}-bom-fonts
%common_desc_en

This package includes UnBom, a decorative font.

%description -l ko -n %{fontname}-bom-fonts
%common_desc_ko

이 패키지에는 은봄글꼴이 포함되어 있습니다.

%description -n %{fontname}-jamobatang-fonts
%common_desc_en

This package includes the UnJamoBatang font.

%description -l ko -n %{fontname}-jamobatang-fonts
%common_desc_ko

이 패키지에는 은자모바탕글꼴이 포함되어 있습니다.

%description -n %{fontname}-jamodotum-fonts
%common_desc_en

This package includes the UNJamoDotum font.

%description -l ko -n %{fontname}-jamodotum-fonts
%common_desc_ko

이 패키지에는 은자모돋음글꼴이 포함되어 있습니다.

%description -n %{fontname}-jamonovel-fonts
%common_desc_en

This package includes the UNJamoNovel font.

%description -l ko -n %{fontname}-jamonovel-fonts
%common_desc_ko
 
이 패키지에는 은자모노벨글꼴이 포함되어 있습니다.

%description -n %{fontname}-jamosora-fonts
%common_desc_en

This package includes the UNJamoSora font.

%description -l ko -n %{fontname}-jamosora-fonts
%common_desc_ko

이 패키지에는 은자모소라글꼴이 포함되어 있습니다.

%description -n %{fontname}-pen-fonts
%common_desc_en

This package includes UnPen, a script font.

%description -l ko -n %{fontname}-pen-fonts
%common_desc_ko

이 패키지에는 은펜글꼴이 포함되어 있습니다.

%description -n %{fontname}-penheulim-fonts
%common_desc_en

This package includes UnPenheulim, a script font.

%description -l ko -n %{fontname}-penheulim-fonts
%common_desc_ko

이 패키지에는 은펜흘림글꼴이 포함되어 있습니다.

%description -n %{fontname}-pilgia-fonts
%common_desc_en

This package includes UnPilgia, a script font.

%description -l ko -n %{fontname}-pilgia-fonts
%common_desc_ko

이 패키지에는 은필기a글꼴이 포함되어 있습니다.

%description -n %{fontname}-shinmun-fonts
%common_desc_en

This package includes the UnShinmun font.

%description -l ko -n %{fontname}-shinmun-fonts
%common_desc_ko

이 패키지에는 은신문글꼴이 포함되어 있습니다.

%description -n %{fontname}-taza-fonts
%common_desc_en

This package includes UnTaza, a typewriter font.

%description -l ko -n %{fontname}-taza-fonts
%common_desc_ko

이 패키지에는 은타자글꼴이 포함되어 있습니다.

%description -n %{fontname}-vada-fonts
%common_desc_en

This package includes the UnVada font.

%description -l ko -n %{fontname}-vada-fonts
%common_desc_ko

이 패키지에는 은바다글꼴이 포함되어 있습니다.

%description -n %{fontname}-yetgul-fonts
%common_desc_en

This package includes UnYetgul, an old Korean printing font.

%description -l ko -n %{fontname}-yetgul-fonts
%common_desc_ko

이 패키지에는 은옛글글꼴이 포함되어 있습니다.


%_font_pkg -n bom -f %{fontconf}-bom.conf UnBom.ttf
%_font_pkg -n jamobatang -f %{fontconf}-jamobatang.conf UnJamoBatang.ttf
%_font_pkg -n jamodotum -f %{fontconf}-jamodotum.conf UnJamoDotum.ttf
%_font_pkg -n jamonovel -f %{fontconf}-jamonovel.conf UnJamoNovel.ttf
%_font_pkg -n jamosora -f %{fontconf}-jamosora.conf UnJamoSora.ttf
%_font_pkg -n pen -f %{fontconf}-pen.conf UnPen.ttf
%_font_pkg -n penheulim -f %{fontconf}-penheulim.conf UnPenheulim.ttf
%_font_pkg -n pilgia -f %{fontconf}-pilgia.conf UnPilgia.ttf
%_font_pkg -n shinmun -f %{fontconf}-shinmun.conf UnShinmun.ttf
%_font_pkg -n taza -f %{fontconf}-taza.conf UnTaza.ttf
%_font_pkg -n vada -f %{fontconf}-vada.conf UnVada.ttf
%_font_pkg -n yetgul -f %{fontconf}-yetgul.conf UnYetgul.ttf


%prep
%setup -q -n un-fonts


%build


%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-bom.conf
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-jamobatang.conf
install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-jamodotum.conf
install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-jamonovel.conf
install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-jamosora.conf
install -m 0644 -p %{SOURCE6} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pen.conf
install -m 0644 -p %{SOURCE7} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-penheulim.conf
install -m 0644 -p %{SOURCE8} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pilgia.conf
install -m 0644 -p %{SOURCE9} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-shinmun.conf
install -m 0644 -p %{SOURCE10} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-taza.conf
install -m 0644 -p %{SOURCE11} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-vada.conf
install -m 0644 -p %{SOURCE12} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-yetgul.conf

for fconf in %{fontconf}-bom.conf \
    %{fontconf}-jamobatang.conf \
    %{fontconf}-jamodotum.conf \
    %{fontconf}-jamonovel.conf \
    %{fontconf}-jamosora.conf \
    %{fontconf}-pen.conf \
    %{fontconf}-penheulim.conf \
    %{fontconf}-pilgia.conf \
    %{fontconf}-shinmun.conf \
    %{fontconf}-taza.conf \
    %{fontconf}-vada.conf \
    %{fontconf}-yetgul.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -rf %{buildroot}


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.2-0.080608.8m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-0.080608.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-0.080608.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-0.080608.5m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-0.080608.4m)
- sync with Fedora 13 (1.0.2-0.12.080608)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-0.080608.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-0.080608.2m)
- revise un_subpkg() to fix file conflictions

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-0.080608.1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-0.8.080608
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Oct 13 2008 Dennis Jang <smallvil@get9.net> - 1.0.2-0.7.080608
- fixed subpackage description and fontconfig

* Sun Oct 12 2008 Nicolas Mailhot <nicolas dot mailhot at laposte dot net> - 1.0.2-0.6.080608
- complete the subpackages
- revert subpackage description macroization, it's not worth it

* Wed Oct 08 2008 Dennis Jang <smallvil@get9.net> - 1.0.2-0.5.080608
- add subpackages with a macro
- add description

* Mon Jul 07 2008 Dennis Jang <smallvil@get9.net> - 1.0.2-0.4.080608
- Refined .spec literal

* Sun Jul 06 2008 Dennis Jang <smallvil@get9.net> - 1.0.2-0.3.080608
- Added or Changed a Summary and Description.
- Removed nil item.
- Refined versioning contents.
- Renamed from un-fonts-extra.spec

* Thu Jul 03 2008 Dennis Jang <smallvil@get9.net> - 1.0.2-0.2.080608
- Refined .spec literal, license, versioning contents.

* Sat Jun 28 2008 Dennis Jang <smallvil@get9.net> - 1.0.2-0.1.080608
- Initial release.
