%global momorel 1

Summary: Fast and lean authoritative DNS Name Server
Name: nsd
Version: 3.2.13
Release: %{momorel}m%{?dist}
License: Modified BSD
Url: http://open.nlnetlabs.nl/nsd/
Source0: http://open.nlnetlabs.nl/downloads/nsd/%{name}-%{version}.tar.gz
NoSource: 0
Source1: nsd.service
Source2: nsd.cron
Source3: nsd.sysconfig
Source4: tmpfiles-nsd.conf
Patch0: nsd-install.patch
Patch1: nsd-fixlogfile.patch
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: flex, openssl-devel >= 1.0.0
BuildRequires: systemd-units
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(pre): shadow-utils

%description
NSD is a complete implementation of an authoritative DNS name server.
For further information about what NSD is and what NSD is not please
consult the REQUIREMENTS document which is a part of this distribution
(thanks to Olaf).

%prep
%setup -q 
%patch0 -p1
%patch1 -p1

%build
%configure --enable-bind8-stats --enable-checking --enable-nsec3 \
           --with-pidfile=%{_localstatedir}/run/%{name}/%{name}.pid --with-ssl \
           --with-user=nsd --with-difffile=%{_localstatedir}/lib/%{name}/ixfr.db \
           --with-xfrdfile=%{_localstatedir}/lib/%{name}/ixfr.state \
           --with-dbfile=%{_localstatedir}/lib/%{name}/nsd.db

%{__make} %{?_smp_mflags}
#convert to utf8
iconv -f iso8859-1 -t utf-8 doc/RELNOTES > doc/RELNOTES.utf8
iconv -f iso8859-1 -t utf-8 doc/CREDITS > doc/CREDITS.utf8
mv -f doc/RELNOTES.utf8 doc/RELNOTES
mv -f doc/CREDITS.utf8 doc/CREDITS


%install
rm -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_unitdir}
install -d -m 0755 %{buildroot}%{_sysconfdir}/cron.hourly
install -c -m 0755 %{SOURCE2} %{buildroot}%{_sysconfdir}/cron.hourly/nsd
install -m 0755 %{SOURCE1} %{buildroot}/%{_unitdir}/nsd.service
install -d -m 0755 %{buildroot}%{_localstatedir}/run/%{name}
install -d -m 0700 %{buildroot}%{_localstatedir}/lib/%{name}
install -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
install -m 0755 %{SOURCE3} %{buildroot}/%{_sysconfdir}/sysconfig/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d/
install -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/tmpfiles.d/nsd.conf

# change .sample to normal config files
head -76 %{buildroot}%{_sysconfdir}/nsd/nsd.conf.sample > %{buildroot}%{_sysconfdir}/nsd/nsd.conf
rm %{buildroot}%{_sysconfdir}/nsd/nsd.conf.sample 
echo "database: /var/lib/nsd/nsd.db" >> %{buildroot}%{_sysconfdir}/nsd/nsd.conf
echo "# include: \"/some/path/file\"" >> %{buildroot}%{_sysconfdir}/nsd/nsd.conf

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc doc/*
%doc contrib/nsd.zones2nsd.conf
%dir %{_sysconfdir}/nsd/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/nsd/nsd.conf
#%%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/nsd/nsd.zones
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/nsd
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/tmpfiles.d/nsd.conf
%{_unitdir}/%{name}.service
%{_sysconfdir}/cron.hourly/nsd
%ghost %attr(0755,root,root) %dir %{_localstatedir}/run/%{name}
%attr(0755,%{name},%{name}) %dir %{_localstatedir}/lib/%{name}
%{_sbindir}/*
%{_mandir}/*/*

%pre
getent group nsd >/dev/null || groupadd -r nsd
getent passwd nsd >/dev/null || \
useradd -r -g nsd -d /etc/nsd -s /sbin/nologin \
-c "nsd daemon account" nsd
exit 0

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
   # Package removal, not upgrade
    /bin/systemctl --no-reload disable nsd.service > /dev/null 2>&1 || :
    /bin/systemctl stop nsd.service > /dev/null 2>&1 || :
fi

%postun 
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart nsd.service >/dev/null 2>&1 || :
fi

%triggerun -- nsd < 3.2.8-2m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply nsd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save nsd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del nsd >/dev/null 2>&1 || :
/bin/systemctl try-restart nsd.service >/dev/null 2>&1 || :

%changelog
* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.13-1m)
- [SECURITY] CVE-2012-2979
- update to 3.2.13

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.12-1m)
- [SECURITY] CVE-2012-2978
- update to 3.2.12

* Sun Mar 18 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.10-1m)
- update

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.8-3m)
- apply patch1

* Thu Sep 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.8-2m)
- support systemd

* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.8-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.6-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.6-1m)
- update

* Sun Jul 18 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.5-1m)
- update

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.4-2m)
- rebuild against openssl-1.0.0

* Fri Jan  8 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.4-1m)
- update

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-1m)
- update

* Tue Jun 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-2m)
- change nsd.sysconfig NSDC_PROG value

* Wed May 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-1m)
- [SECURITY] CVE-2009-1755
- - http://www.nlnetlabs.nl/publications/NSD_vulnerability_announcement.html
- update to 3.2.2
- add Source3: nsd.sysconfig and nsd-install.patch from Fedora devel
- Removed obsoleted options --enable-plugins --enable-mmap by Fedora devel

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-3m)
- rebuild against openssl-0.9.8k

* Fri Feb  6 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.1-2m)
- fix nsd.cron: Do not make /%1 and /dev/nul...
- sync nsd.init with fedora

* Sun Feb  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-2m)
- rebuild against rpm-4.6

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Tue Jul  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.8-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.8-1m)
- update to 3.0.8

* Tue May 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.7-3m)
- sync Fedora

* Thu May  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.7-2m)
- add Source2: nsd.cron

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.5-2m)
- rebuild against gcc43

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 3.0.7-3
- Autorebuild for GCC 4.3

* Wed Dec  5 2007 Paul Wouters <paul@xelerance.com> - 3.0.7-2
- Rebuild for new libcrypto

* Tue Nov 13 2007 Paul Wouters <paul@xelerance.com> - 3.0.7-1
- Updated to new version
- fix RELNOTES/README to be utf8
- Fix path to nsd.db in cron job.

* Thu Nov  8 2007 Paul Wouters <paul@xelerance.com> - 3.0.6-7
- Modified cron to only rebuild/reload when zone updates
  have been received

* Wed Nov  7 2007 Paul Wouters <paul@xelerance.com> - 3.0.6-6
- Added hourly cron job to do various maintenance tasks
- Added nsd rebuild to create the proper nsd.db file on startup
- Added nsd patch on shutdown to ensure zonefiles are up to date

* Tue Oct  2 2007 Paul Wouters <paul@xelerance.com> - 3.0.6-5
- nsdc update and nsdc notify are no longer needed in initscript.

* Mon Sep 24 2007 Jesse Keating <jkeating@redhat.com> - 3.0.6-4
- Bump release for upgrade path.

* Fri Sep 14 2007 Paul Wouters <paul@xelerance.com> 3.0.6-3
- Do not include examples from nsd.conf.sample that causes
  bogus network traffic.

* Fri Sep 14 2007 Paul Wouters <paul@xelerance.com> 3.0.6-2
- Change locations of ixfr.db and xfrd.state to /var/lib/nsd
- Enable NSEC3
- Delay running nsdc update until after nsd has started
- Delete xfrd.state on nsd stop
- Run nsdc notify in the background, since it can take
  a very long time when remote servers are unavailable.

* Tue Sep 11 2007 Paul Wouters <paul@xelerance.com> 3.0.6-1
- Upgraded to 3.0.6
- Do not include bind2nsd, since it didn't compile for me

* Tue Jul 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.4-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Fri Jul 13 2007 Paul Wouters <paul@xelerance.com> 3.0.5-2
- Fix init script, bug #245546

* Fri Mar 23 2007 Paul Wouters <paul@xelerance.com> 3.0.5-1
- Upgraded to 3.0.5

* Tue Feb 06 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-1m)
- import to Momonga from fc-extras
- update to 3.0.4

* Thu Dec  7 2006 Paul Wouters <paul@xelerance.com> 3.0.3-1
- Upgraded to 3.0.3

* Mon Nov 27 2006 Paul Wouters <paul@xelerance.com> 3.0.2-1
- Upgraded to 3.0.2.
- Use new configuration file nsd.conf. Still needs migration script.
  patch from Farkas Levente <lfarkas@bppiac.hu>

* Mon Oct 16 2006  Paul Wouters <paul@xelerance.com> 2.3.6-2
- Bump version for upgrade path

* Thu Oct 12 2006  Paul Wouters <paul@xelerance.com> 2.3.6-1
- Upgraded to 2.3.6
- Removed obsolete workaround in nsd.init
- Fixed spec file so daemon gets properly restarted on upgrade

* Mon Sep 11 2006 Paul Wouters <paul@xelerance.com> 2.3.5-4
- Rebuild requested for PT_GNU_HASH support from gcc
- Removed dbaccess.c from doc section

* Mon Jun 26 2006 Paul Wouters <paul@xelerance.com> - 2.3.5-3
- Bump version for FC-x upgrade path

* Mon Jun 26 2006 Paul Wouters <paul@xelerance.com> - 2.3.5-1
- Upgraded to nsd-2.3.5

* Sun May  7 2006 Paul Wouters <paul@xelerance.com> - 2.3.4-3
- Upgraded to nsd-2.3.4. 
- Removed manual install targets because DESTDIR is now supported
- Re-enabled --checking, checking patch no longer needed and removed.
- Work around in nsd.init for nsd failing to start when there is no ipv6

* Thu Dec 15 2005 Paul Wouters <paul@xelerance.com> - 2.3.3-7
- chkconfig and attribute  changes as proposed by Dmitry Butskoy

* Thu Dec 15 2005 Paul Wouters <paul@xelerance.com> - 2.3.3-6
- Moved pid file to /var/run/nsd/nsd.pid.
- Use _localstatedir instead of "/var"

* Tue Dec 13 2005 Paul Wouters <paul@xelerance.com> - 2.3.3-5
- Added BuildRequires for openssl-devel, removed Requires for openssl.

* Mon Dec 12 2005 Paul Wouters <paul@xelerance.com> - 2.3.3-4
- upgraded to nsd-2.3.3

* Wed Dec  7 2005 Tom "spot" Callaway <tcallawa@redhat.com> - 2.3.2-2
- minor cleanups

* Mon Dec  5 2005 Paul Wouters <paul@xelerance.com> - 2.3.2-1
- Upgraded to 2.3.2. Changed post scripts to comply to Fedora
  Extras policies (eg do not start daemon on fresh install)

* Tue Oct  4 2005 Paul Wouters <paul@xelerance.com> - 2.3.1-1
- Initial version
