%global momorel 3

Name:           libmodman
Version:        2.0.1
Release:        %{momorel}m%{?dist}
Summary:        A simple library for managing C++ modules (plug-ins)
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://code.google.com/p/libmodman/
Source0:        http://libmodman.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake >= 2.8.0
BuildRequires:  zlib-devel

%description
libmodman is a simple library for managing C++ modules (plug-ins).

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       cmake >= 2.8.0

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
sed -i 's|-Werror||' libmodman/CMakeLists.txt

%build
%{cmake}
make VERBOSE=1 %{?_smp_mflags}

%check
make test

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/libmodman-2.0.pc
%{_datadir}/cmake/Modules/Findlibmodman.cmake

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora devel

* Sat Jul 03 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 1.0.1-5
- Remove conflict with libproxy, its not really necessary since
  we already have the latest libproxy in rawhide.

* Sun Jun 20 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 1.0.1-4
- Remove docs from -devel package
- Upgrade cmake requirement to 2.8.0
- Add cmake to -devel requires

* Fri Jun 18 2010 Nathaniel McCallum - 1.0.1-3
- Add docs to -devel package
- Fix include directory ownership

* Fri Jun 18 2010 Nathaniel McCallum - 1.0.1-2
- Added BuildRequires on zlib-devel (used for tests)

* Sun Jun 13 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 1.0.1-1
- First release
