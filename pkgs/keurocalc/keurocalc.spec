%global momorel 1
%global qtver 4.8.0
%global kdever 4.8.1
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: KEuroCalc is a universal currency converter and calculator
Name: keurocalc
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Productivity
URL: http://opensource.bureau-cornavin.com/keurocalc/
Source0: http://opensource.bureau-cornavin.com/%{name}/sources/%{name}-%{version}.tgz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-desktop-0.16.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils

%description
KEuroCalc is a universal currency converter and calculator. It
can convert from and to many currencies, either with a fixed
conversion rate or a variable conversion rate. It directly
downloads the latest variable rates through the Internet

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .keywords

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor="" --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-key Keywords \
  --remove-category Application \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog INSTALL TODO
%{_kde4_bindir}/curconvd
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/curconvd
%{_kde4_appsdir}/%{name}
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_datadir}/icons/*/*/*/%{name}.png
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/locale/*/LC_MESSAGES/curconvd.mo
%{_kde4_datadir}//pixmaps/%{name}.png

%changelog
* Fri Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- rebuild for new GCC 4.6

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-7m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-5m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-4m)
- build fix with desktop-file-utils-0.16
-- add patch1 (delete keywords)

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-3m)
- rebuild against qt-4.6.3-1m

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-2m)
- source tar ball was replaced, update sources

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NARITA Koichi <pulsar@omonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-4m)
- revise %%{_docdir}/HTML/*/keurocalc/common

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc43

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- version 1.0.0 KDE4

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-3m)
- %%NoSource -> NoSource

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-1m)
- initial package for Momonga Linux
