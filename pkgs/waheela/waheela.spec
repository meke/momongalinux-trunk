%global momorel 8
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m
%global kdebaseruntimerel 1m

Summary: Amarok full-screen display
Name: waheela
Version: 0.3
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://linux.wuertz.org/
Group: Applications/Multimedia
Source0: http://linux.wuertz.org/dists/sid/main/source/%{name}_%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: kdebase-runtime >= %{kdever}-%{kdebaseruntimerel}
Requires: amarok
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: bzip2-devel
BuildRequires: cmake
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: eggdbus-devel
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel
BuildRequires: keyutils-libs-devel
BuildRequires: krb5-devel
BuildRequires: libacl-devel
BuildRequires: libattr-devel
BuildRequires: libcom_err-devel
BuildRequires: libpng-devel
BuildRequires: libselinux-devel
BuildRequires: libuuid-devel
BuildRequires: libxml2-devel
BuildRequires: lzma-devel
BuildRequires: openssl-devel
BuildRequires: polkit-devel
BuildRequires: polkit-qt-devel
BuildRequires: zlib-devel

%description
Waheela is a program that offers you a party mode for Amarok2.
It's a similar to kirocker, but it's a kde4 program.
Waheela supports themes and gives you the possibility
to look the player with a password.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .desktop-ja~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHOR COPYING
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%dir %{_kde4_configdir}/%{name}
%dir %{_kde4_configdir}/%{name}/themes
%config %{_kde4_configdir}/%{name}/themes/*.config
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-8m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-6m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-3m)
- rebuild against qt-4.6.3-1m

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-2m)
- use BuildRequires

* Sat Feb 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-1m)
- initial package for music freaks using Momonga Linux
