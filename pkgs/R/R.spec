%global momorel 1

%ifarch ppc64
%global optflags %(echo %{optflags} -mminimal-toc)
%endif

Summary: A language for data analysis and graphics
Name: R
Version: 3.0.3
Release: %{momorel}m%{?dist}
Source0: ftp://cran.r-project.org/pub/%{name}/src/base/R-3/R-%{version}.tar.gz
NoSource: 0
License: GPLv2+
Group: Applications/Engineering
URL: http://www.r-project.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libpng-devel, libjpeg-devel >= 8a, readline-devel, ncurses-devel
buildRequires: libtiff-devel >= 4.0.1
BuildRequires: tetex-latex, texinfo, tcl-devel, tk-devel
BuildRequires: texinfo-tex
BuildRequires: lapack-devel
BuildRequires: libSM-devel, libX11-devel, libICE-devel, libXt-devel
BuildRequires: libicu-devel >= 52
BuildRequires: xz-devel >= 5.0.0
BuildRequires: pcre-devel >= 8.31
# We use lpr as R_PRINTCMD
Requires: cups
# We use xdg-open as R_PDFVIEWER and R_BROWSER
Requires: xdg-utils >= 1.1.0
Requires: xz-libs >= 5.0.0

%description
`GNU S' - A language and environment for statistical computing and
graphics. R is similar to the award-winning S system, which was
developed at Bell Laboratories by John Chambers et al. It provides a
wide variety of statistical and graphical techniques (linear and
nonlinear modelling, statistical tests, time series analysis,
classification, clustering, ...).
#`

R is designed as a true computer language with control-flow
constructions for iteration and alternation, and it allows users to
add additional functionality by defining new functions. For
computationally intensive tasks, C, C++ and Fortran code can be linked
and called at run time.

%package devel
Summary: files for development of R packages.
Group: Applications/Engineering
Requires: R = %{version}-%{release}
# You need all the BuildRequires for the development version
Requires: gcc-c++, gcc-gfortran, tetex-latex, texinfo 
Requires: libpng-devel, libjpeg-devel, readline-devel, ncurses-devel
Requires: libSM-devel, libX11-devel, libICE-devel, libXt-devel
Requires: tcl-devel, tk-devel

%description devel
Install R-devel if you are going to develop or compile R packages.

%package -n libRmath
Summary: standalone math library from the R project
Group: Development/Libraries

%description -n libRmath
A standalone library of mathematical and statistical functions derived
from the R project.  This packages provides the shared libRmath library.

%package -n libRmath-devel
Summary: standalone math library from the R project
Group: Development/Libraries
Requires: libRmath = %{version}-%{release}

%description -n libRmath-devel
A standalone library of mathematical and statistical functions derived
from the R project.  This packages provides the static libRmath library
and header files.

%prep
%setup -q 

%build
export R_PDFVIEWER="%{_bindir}/xdg-open"
export R_PRINTCMD="lpr"
export R_BROWSER="%{_bindir}/xdg-open"
export F77="gfortran"
%configure     --with-system-zlib \
    --with-system-bzlib \
    --with-system-pcre \
    --with-lapack \
    --with-tcl-config=%{_libdir}/tclConfig.sh \
    --with-tk-config=%{_libdir}/tkConfig.sh \
    --enable-R-shlib \
    CFLAGS="%{optflags} -O0"
	
%make
(cd src/nmath/standalone; make)
make pdf
# What a hack.
# Current texinfo doesn't like @eqn. Use @math instead where stuff breaks.
cp doc/manual/R-exts.texi doc/manual/R-exts.texi.spot
cp doc/manual/R-intro.texi doc/manual/R-intro.texi.spot
sed -i 's|@eqn|@math|g' doc/manual/R-exts.texi
sed -i 's|@eqn|@math|g'	doc/manual/R-intro.texi
make info

%install
rm -rf --preserve-root %{buildroot}
%makeinstall rhome=%{buildroot}%{_libdir}/R install-info
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_infodir}/dir.old

#Install libRmath files
(cd src/nmath/standalone; make install \
    includedir=%{buildroot}%{_includedir} \
    libdir=%{buildroot}%{_libdir})

#Fix location of R_HOME_DIR in shell wrapper.
sed -e "s@R_HOME_DIR=.*@R_HOME_DIR=%{_libdir}/R@" bin/R \
  > %{buildroot}%{_libdir}/R/bin/R
chmod 755 %{buildroot}%{_libdir}/R/bin/R 
sed -e "s@R_HOME_DIR=.*@R_HOME_DIR=%{_libdir}/R@" bin/R \
  > %{buildroot}%{_bindir}/R
chmod 755 %{buildroot}%{_bindir}/R

# Get rid of buildroot in script
for i in %{buildroot}%{_libdir}/R/bin/Rscript %{buildroot}%{_bindir}/Rscript %{buildroot}%{_libdir}/pkgconfig/libR*.pc;
do
  sed -i "s|$RPM_BUILD_ROOT||g" $i;
done

# Remove package indices. They are rebuilt by the postinstall script.
#
rm -f %{buildroot}%{_libdir}/R/doc/html/function.html
rm -f %{buildroot}%{_libdir}/R/doc/html/packages.html
rm -f %{buildroot}%{_libdir}/R/doc/html/search/index.txt

# Some doc files are also installed. We don't need them
(cd %{buildroot}%{_libdir}/R;
 rm -f AUTHORS COPYING COPYING.LIB COPYRIGHTS FAQ NEWS NEWS.0 NEWS.1 NEWS.2 RESOURCES THANKS)

mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/R/lib" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%clean
rm -rf --preserve-root %{buildroot}

%post 
/sbin/ldconfig
for doc in admin exts FAQ intro lang; do
   file=%{_infodir}/R-${doc}.info.bz2
   if [ -e ${file} ]; then
      /sbin/install-info %{_infodir}/R-${doc}.info %{_infodir}/dir 2>/dev/null
   fi
done

%preun
if [ $1 = 0 ]; then
   # Delete directory entries for info files (if they were installed)
   for doc in admin exts FAQ intro lang; do
      file=%{_infodir}/R-${doc}.info.bz2
      if [ -e ${file} ]; then
         /sbin/install-info --delete %{_infodir}/R-${doc}.info %{_infodir}/dir 2>/dev/null
      fi
   done
fi

%postun 
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING ChangeLog NEWS README
%{_sysconfdir}/ld.so.conf.d/R-%{_arch}.conf
%{_bindir}/R
%{_bindir}/Rscript
%{_libdir}/R
%{_mandir}/man1/*
%{_infodir}/*
%doc doc/AUTHORS doc/COPYING doc/COPYRIGHTS doc/FAQ NEWS
%doc NEWS.0 NEWS.1 NEWS.2 README doc/RESOURCES doc/THANKS VERSION
%doc doc/manual/R-admin.pdf
%doc doc/manual/R-FAQ.pdf
%doc doc/manual/R-lang.pdf
%doc doc/manual/R-data.pdf
%doc doc/manual/R-intro.pdf

%files devel
%defattr(-, root, root)
%doc doc/manual/R-exts.pdf
%{_libdir}/pkgconfig/libR.pc
%{_libdir}/pkgconfig/libRmath.pc

%files -n libRmath
%defattr(-, root, root)
%{_libdir}/libRmath.so

%files -n libRmath-devel
%defattr(-, root, root)
%{_libdir}/libRmath.a
%{_includedir}/Rmath.h

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-1m)
- update 3.0.3

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-3m)
- revive info

* Sat Jul 13 2013 NARITA Koichi <pulsar@momonfa-linux.org>
- (3.0.1-2m)
- fix %%files

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.2-1m)
- update to 2.15.2

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.1-1m)
- update to 2.15.1

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.0-2m)
- rebuild against pcre-8.31

* Fri Aug 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.0-2m)
- switch to use xdg-open

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.0-1m)
- update to 2.15.0
- rebuild against libtiff-4.0.1

* Wed Dec 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.1-3m)
- rebuild for lapack-3.4.0

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.1-2m)
- rebuild against icu-4.6

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.13.1-1m)
- update 2.13.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1-10m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.1-9m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.1-8m)
- full rebuild for mo7 release

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.1-7m)
- fix %%post and %%preun
-- correct path to info files
-- remove obsoletes build-help.pl

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.1-1m)
- update to 2.11.1

* Sun May 16 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.9.2-6m)
- add BuildRequires: libicu-devel >= 4.2

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.9.2-5m)
- rebuild against icu-4.2.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.2-4m)
- rebuild against readline6

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.2-3m)
- rebuild against libjpeg-8a

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-2m)
- R does not conflict with lilypond

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-1m)
- update to 2.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.0-2m)
- rebuild against libjpeg-7

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-3m)
- rebuild against rpm-4.6

* Tue May  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.7.0-2m)
- add CFLAGS=-O0

* Sat Apr 26 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.1-4m)
- rebuild against Tcl/Tk 8.5

* Sun Apr  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.1-3m)
- add BuildRequires

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.1-2m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-1m)
- upgrade 2.6.1
- no use libtermcap

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.1-2m)
- rebuild against perl-5.10.0-1m

* Sun Jul  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.4.0-4m)
- enable ppc64

* Wed Nov 15 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.0-3m)
- R-2.4.0-patched-2006-11-03.patch from Fedora Extra
- - some security flaws are fixed in CVS
- separated sub packages
- to Main.

* Wed Oct  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-2m)
- enable R shared library

* Wed Oct  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Sat Sep 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-1m)
- import from ftp://ftp.ecc.u-tokyo.ac.jp/CRAN/bin/linux/redhat/SRPMS/
-- more simple ;-)

* Fri Jun 2 2006 Martyn Plummer <plummer@iarc.fr> 1:2.3.1-1
- Set FC environment variable (for systems that have intel compiler installed)

* Fri May 5 2006 Martyn Plummer <plummer@iarc.fr> 1:2.3.0-2
- Corrected dependency on XFree86-devel for R-devel on FC5

* Wed May 3 2006 Martyn Plummer <plummer@iarc.fr> 1:2.3.0-1
- Restored packager field missing in previous builds

* Tue Mar 21 2006 Martyn Plummer <plummer@iarc.fr> 1:2.2.1-2
- Modified for FC5

* Tue Jan 3 2006 Martyn Plumer <plummber@iarc.fr> 1:2.2.1-1
- Restored CAPABILITIES file lost in 2.2.1beta

* Mon Dec 12 2005 Martyn Plummer <plummer@iarc.fr> 1:2.2.1beta-1
- Configure uses --without-blas by default.
- Install pdf manuals.
- All toplevel documentation files are installed into _libdir/R
  as they may be required by R (qv note on AUTHORS and THANKS below).
- Legacy scripts for rebuilding package indices removed.

* Mon Sep 19 2005 Martyn Plummer <plummer@iarc.fr> 1:2.2.0-0
- R now compiles with gcc 4.0.1 and default rpm optimization flags.

* Wed Jul 13 2005 Martyn Plummer <plummer@iarc.fr> 1:2.1.1-2
- The files AUTHORS and THANKS need to be installed into _libdir/R
  as they are required by the function contributors().  Previously
  they were installed only as documentation files.

* Tue Jun 21 2005 Martyn Plummer <plummer@iarc.fr>
- Corrected date error in SPEC file.

* Sun Jun 19 2005 Martyn Plummer <plummer@iarc.fr>
- Dropped support for Red Hat boxed set.
- Synchronized with Fedora Extras 4 RPM by Tom Calloway
  including support for shared R library.
- Added gfortran support. However, gcc 4.0.0 will not compile R correctly
  with the default optimization flags, so there is a temporary work-around.

* Wed Jun 15 2005 Gernot Stocker <gernot.stocker@tugraz.at>
- Adaptations for CentOS and Rocks-Linux, tested under Release 4.0

* Sat Apr 30 2005 Joseph P. Skudlarek <Jskud@Jskud.com> 0:2.1.0-0.fdr.3
- Install R-data.info file as well.
- Make info processing conditional on texinfo version, not platform release,
  so that if a new enough version is installed, we will build and install info.

* Mon Apr 18 2005 Martyn Plummer <plummer@iarc.fr> 0:2.1.0-0.fdr.1
- Built R 2.1.0.  R now supports internationalization, so the patch
  to set the locale to "C" is now dropped.
- install.packages() exits gracefully with a helpful message if
  the file INSTALL is not present, so this is now included in the
  R-devel package.

* Mon Mar 14 2005 Martyn Plummer <plummer@iarc.fr> 0:2.0.1-0.fdr.5
- Added support for Scientific Linux (http://www.scientificlinux.org)
  A distribution based on RHEL. Thanks to Jon Peatfield.

* Mon Feb 28 2005 Martyn Plummer <plummer@iarc.fr> 0:2.0.1-0.fdr.4
- Fixed file ownership in R-devel and libRmath packages

* Wed Feb 16 2005 Martyn Plummer <plummer@iarc.fr> 0:2.0.1-0.fdr.3
- R-devel package is now a stub package with no files, except a documentation
  file (RPM won't accept sub-packages with no files). R now conflicts
  with earlier (i.e 0:2.0.1-0.fdr.2) versions of R-devel.
- Created libRmath subpackage with shared library.

* Mon Jan 31 2005 Martyn Plummer <plummer@iarc.fr> 0:2.0.1-0.fdr.2
- Created R-devel and libRmath-devel subpackages

* Mon Nov 15 2004 Martyn Plummer <plummer@iarc.fr> 0:2.0.1-0.fdr.1
- Built R 2.0.1

* Wed Nov 10 2004 Martyn Plummer <plummer@iarc.fr> 0:2.0.0-0.fdr.3
- Set R_PRINTCMD at configure times so that by default getOption(printcmd)
  gives "lpr".
- Define macro fcx for all Fedora distributions. This replaces Rinfo

* Tue Oct 12 2004 Martyn Plummer <plummer@iarc.fr> 0:2.0.0-0.fdr.2
- Info support is now conditional on the macro Rinfo, which is only
  defined for Fedora 1 and 2. 

* Thu Oct 7 2004 Martyn Plummer <plummer@iarc.fr> 0:2.0.0-0.fdr.1
- Built R 2.0.0
- There is no longer a BUGS file, so this is not installed as a 
  documentation file.
