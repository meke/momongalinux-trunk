%global momorel 15

%global _binary_payload w9.gzdio

# momonga support xz payload
%bcond_without xz
# sqlite backend is pretty useless
%bcond_with sqlite
# just for giggles, option to build with internal Berkeley DB
%bcond_with int_bdb
# run internal testsuite?
%bcond_without check

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define rpmhome /usr/lib/rpm

%define bdbver 4.8.26
%define dbprefix db

Summary: The RPM package management system
Name: rpm
Version: 4.8.1
Release: %{momorel}m%{?dist}
Group: System Environment/Base
Url: http://www.rpm.org/
Source0: http://rpm.org/releases/rpm-4.8.x/%{name}-%{version}.tar.bz2
NoSource: 0
%if %{with int_bdb}
Source1: db-%{bdbver}.tar.gz
%endif

Patch0: rpm-4.7.90-devel-autodep.patch
Patch1: rpm-4.5.90-pkgconfig-path.patch
Patch2: rpm-4.5.90-gstreamer-provides.patch
# Fedora specspo is setup differently than what rpm expects, considering
# this as Fedora-specific patch for now
Patch3: rpm-4.7.90-fedora-specspo.patch
# Postscript driver provides extraction is Momonga specific for now
Patch4: rpm-4.8.0-psdriver-deps.patch

# Patches already in upstream
Patch202: rpm-4.8.0-pythondeps-parallel.patch
Patch203: rpm-4.8.0-python-bytecompile.patch
Patch210: rpm-4.8.0-debugedit-lazy-buildid.patch
Patch216: rpm-4.8.0-multiple-pubkeys.patch
Patch217: rpm-4.8.0-freshen-arch.patch
Patch218: rpm-4.8.0-sigsanity.patch
Patch219: rpm-4.8.0-getoutputfrom.patch
Patch220: rpm-4.8.0-debuginfo-hardlinks.patch
Patch221: rpm-4.8.0-debuginfo-index.patch
Patch222: rpm-4.8.0-verifyscript-status.patch
Patch223: rpm-4.8.0-sigcompare.patch
Patch224: rpm-4.8.0-pwcheck-errors.patch
Patch225: rpm-4.8.0-umask.patch
Patch226: rpm-4.8.0-prefcolor-erase.patch

Patch227: rpm-4.8.0-selfconflict.patch
Patch228: rpm-4.8.0-rpm2cpio.sh-xz.patch
Patch229: rpm-4.8.0-import-GPG.patch
Patch230: rpm-4.8.0-fileconflicts-1.patch
Patch231: rpm-4.8.0-fileconflicts-2.patch
Patch233: rpm-4.8.0-sigcompare-fix.patch
Patch234: rpm-4.8.0-sigcheck.patch

Patch235: rpm-4.8.0-header-sanity.patch
Patch236: rpm-4.8.x-pgpsubtype.patch

Patch237: rpm-4.8.x-cli-define.patch
Patch238: rpm-4.8.0-usrmove.patch
Patch239: rpm-4.8.x-perl-multiline.patch
Patch240: rpm-4.8.x-pretrans-fail.patch
Patch241: rpm-4.8.x-inode-remap.patch
Patch242: rpm-4.8.x-python-srcheader.patch
Patch243: rpm-4.8.x-luadir.patch
Patch244: rpm-4.8.x-cron-secontext.patch
Patch245: rpm-4.8.x-last-nvra.patch
Patch246: rpm-4.8.x-obsolete-color.patch

Patch260: rpm-4.8.x-headerload-region.patch
Patch261: rpm-4.8.x-pkgread-region.patch
Patch262: rpm-4.8.x-region-size.patch
Patch263: rpm-4.8.x-region-trailer.patch
Patch264: rpm-4.8.x-multiple-debuginfo.patch
Patch265: rpm-4.4.2.3-Japanese-typo.patch
Patch266: rpm-4.4.2.3-man-D-E.patch
Patch267: rpm-4.8.x-man-setperms.patch
Patch268: rpm-4.8.x-man-md5.patch
Patch270: rpm-4.8.x-non-silent-patch.patch
Patch271: rpm-4.8.x-Python-reloadConfig.patch
Patch272: rpm-4.8.x-tilde-version.patch
Patch273: rpm-4.8.x-defattr-attr.patch
Patch276: rpm-4.8.x-multikey-pgp.patch
Patch277: rpm-4.8.x-dwarf-4.patch
Patch278: rpm-4.8.x-import-error.patch

Patch279: rpm-4.8.x-caps-double-free.patch
Patch280: rpm-4.8.x-cond-include.patch
Patch281: rpm-4.8.x-strict-script-errors.patch
Patch282: rpm-4.8.x-rpmdb-hdrunload.patch

# These are not yet upstream
Patch301: rpm-4.6.0-niagara.patch
Patch302: rpm-4.7.1-geode-i686.patch
Patch303: rpm-4.8.0-em64t.patch
Patch304: rpm-4.8.x-read-retry.patch
Patch305: rpm-4.8.x-man-fileid.patch

# Momonga specific patches
Patch500: rpm-4.6.0-rc3-momonga.patch
Patch502: rpm-4.6.0-rc3-shortcircuit.patch
Patch505: rpm-4.6.0-rc4-macros-_build_arch.patch
Patch506: rpm-4.7.0-disable-_smp_mflags.patch
Patch508: rpm-4.4.2.2-fix-group.patch
Patch509: rpm-4.6.0-rc3-platform__id_u.patch
Patch510: rpm-4.6.0-rc3-skip-nonexistence-patch.patch

# Avoid librpm level lua extension bug
Patch700: rpm-4.7.0-not-librpm-level-lua.patch

# Fix lua directory handling (from openSUSE)
Patch800: rpm-4.8.1-luaroot.patch

# Partially GPL/LGPL dual-licensed and some bits with BSD
# SourceLicense: (GPLv2+ and LGPLv2+ with exceptions) and BSD 
License: GPLv2+

Requires: coreutils
%if %{without int_bdb}
# db recovery tools, rpmdb_util symlinks
Requires: db4-utils
%endif
Requires: popt >= 1.10.2.1
Requires: curl

%if %{without int_bdb}
BuildRequires: db4-devel%{_isa}
%endif

%if %{with check}
BuildRequires: fakechroot
%endif

# XXX generally assumed to be installed but make it explicit as rpm
# is a bit special...
BuildRequires: momonga-rpmmacros >= 20100322-1m
BuildRequires: gawk
BuildRequires: elfutils-devel%{_isa} >= 0.112
BuildRequires: elfutils-libelf-devel%{_isa}
BuildRequires: readline-devel%{_isa} zlib-devel%{_isa}
BuildRequires: nss-devel%{_isa}
# The popt version here just documents an older known-good version
BuildRequires: popt-devel%{_isa} >= 1.10.2
BuildRequires: file-devel%{_isa}
BuildRequires: gettext-devel%{_isa}
BuildRequires: libselinux-devel%{_isa}
BuildRequires: ncurses-devel%{_isa}
BuildRequires: bzip2-devel%{_isa} >= 0.9.0c-2
BuildRequires: python-devel%{_isa} >= 2.7
BuildRequires: lua-devel%{_isa} >= 5.1
BuildRequires: libcap-devel%{_isa}
BuildRequires: libacl-devel%{_isa}
%if %{with xz}
BuildRequires: xz-devel%{_isa} >= 5.0.0
%endif
%if %{with sqlite}
BuildRequires: sqlite-devel%{_isa}
%endif
BuildRequires: pkgconfig >= 0.23-4m

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The RPM Package Manager (RPM) is a powerful command line driven
package management system capable of installing, uninstalling,
verifying, querying, and updating software packages. Each software
package consists of an archive of files along with information about
the package like its version, a description, etc.

%package libs
Summary:  Libraries for manipulating RPM packages
Group: Development/Libraries
License: GPLv2+ and "LGPLv2+ with exceptions"
Requires: rpm = %{version}-%{release}
# librpm uses cap_compare, introduced sometimes between libcap 2.10 and 2.16.
# A manual require is needed, see #505596
Requires: libcap%{_isa} >= 2.16

%description libs
This package contains the RPM shared libraries.

%package devel
Summary:  Development files for manipulating RPM packages
Group: Development/Libraries
License: GPLv2+ and "LGPLv2+ with exceptions"
Requires: rpm = %{version}-%{release}
Requires: popt-devel%{_isa}
Requires: file-devel%{_isa}

%description devel
This package contains the RPM C library and header files. These
development files will simplify the process of writing programs that
manipulate RPM packages and databases. These files are intended to
simplify the process of creating graphical package managers or any
other tools that need an intimate knowledge of RPM packages in order
to function.

This package should be installed if you want to develop programs that
will manipulate RPM packages and databases.

%package build
Summary: Scripts and executable programs used to build packages
Group: Development/Tools
Requires: rpm = %{version}-%{release}
Requires: elfutils >= 0.128 binutils
Requires: findutils sed grep gawk diffutils file patch >= 2.5
Requires: unzip gzip bzip2 cpio xz >= 5.0.0
Requires: pkgconfig
Conflicts: ocaml-runtime < 3.11.0-12m

%description build
The rpm-build package contains the scripts and executable programs
that are used to build packages using the RPM Package Manager.

%package python
Summary: Python bindings for apps which will manipulate RPM packages
Group: Development/Libraries
Requires: rpm = %{version}-%{release}

%description python
The rpm-python package contains a module that permits applications
written in the Python programming language to use the interface
supplied by RPM Package Manager libraries.

This package should be installed if you want to develop Python
programs that will manipulate RPM packages and databases.

%package apidocs
Summary: API documentation for RPM libraries
Group: Documentation
BuildArch: noarch

%description apidocs
This package contains API documentation for developing applications
that will manipulate RPM packages and databases.

%package cron
Summary: Create daily logs of installed packages
Group: System Environment/Base
BuildArch: noarch
Requires: crontabs logrotate rpm = %{version}-%{release}

%description cron
This package contains a cron job which creates daily logs of installed
packages on a system.

%prep
%setup -q -n %{name}-%{version} %{?with_int_bdb:-a 1}
%patch0 -p1 -b .devel-autodep
%patch1 -p1 -b .pkgconfig-path
%patch2 -p1 -b .gstreamer-prov
%patch3 -p1 -b .fedora-specspo
%patch4 -p1 -b .psdriver-deps

%patch202 -p1 -b .pythondeps-parallel
%patch203 -p1 -b .python-bytecompile
%patch210 -p1 -b .debugedit-lazy-buildid
%patch216 -p1 -b .multiple-pubkeys
%patch217 -p1 -b .freshen-arch
%patch218 -p1 -b .sigsanity
%patch219 -p1 -b .getoutputfrom
%patch220 -p1 -b .debuginfo-hardlinks
%patch221 -p1 -b .debuginfo-index
%patch222 -p1 -b .verifyscript-status
%patch223 -p1 -b .sigcompare
%patch224 -p1 -b .pwcheck-errors
%patch225 -p1 -b .umask
%patch226 -p1 -b .prefcolor-erase

%patch227 -p1 -b .selfconflict
%patch228 -p1 -b .rpm2cpio.sh-xz
%patch229 -p1 -b .import-GPG
%patch230 -p1 -b .fileconflicts-1
%patch231 -p1 -b .fileconflicts-2
%patch233 -p1 -b .sigcompare-fix
%patch234 -p1 -b .sigcheck
%patch235 -p1 -b .header-sanity
%patch236 -p1 -b .pgpsubtype

%patch237 -p1 -b .cli-define
%patch238 -p1 -b .usrmove
%patch239 -p1 -b .perl-multiline
%patch240 -p1 -b .pretrans-fail
%patch241 -p1 -b .inode-remap
%patch242 -p1 -b .python-srcheader
%patch243 -p1 -b .luadir
%patch244 -p1 -b .cron-secontext
%patch245 -p1 -b .last-nvra
%patch246 -p1 -b .obsolete-color

%patch260 -p1 -b .headerload-region
%patch261 -p1 -b .pkgread-region
%patch262 -p1 -b .region-size
%patch263 -p1 -b .region-trailer
%patch264 -p1 -b .multiple-debuginfo
%patch265 -p1 -b .Japanese-typo
%patch266 -p1 -b .man-D-E
%patch267 -p1 -b .man-setperms
%patch268 -p1 -b .man-md5
%patch270 -p1 -b .non-silent-patch
%patch271 -p1 -b .reloadConfig
%patch272 -p1 -b .tilde
%patch273 -p1 -b .defattr
%patch276 -p1 -b .multikey-pgp
%patch277 -p1 -b .dwarf-4
%patch278 -p1 -b .import-error

%patch279 -p1 -b .caps-double-free
%patch280 -p1 -b .cond-include
%patch281 -p1 -b .strict-script-errors
%patch282 -p1 -b .rpmdb-hdrunload

%patch301 -p1 -b .niagara
%patch302 -p1 -b .geode
%patch303 -p1 -b .em64t
%patch304 -p1 -b .read-retry
%patch305 -p1 -b .man-fileid

%patch500 -p1 -b .momonga
%patch502 -p1 -b .shortcircuit
#%%patch503 -p1 -b .bzip2
#%%patch504 -p1 -b .brp
%patch505 -p1 -b ._build_arch
%patch506 -p1 -b .smp_mflag
#%%patch507 -p1 -b .eu-strip
%patch508 -p1 -b .fixgroup
%patch509 -p1 -b .__id_u
%patch510 -p1 -b .nonexistence

%patch700 -p1 -b .not-librpm-level-lua

#%%patch800 -p1 -b .luaroot

%if %{with int_bdb}
ln -s db-%{bdbver} db
%endif

# Momonga specific changes
find doc -type f | xargs perl -pi -e 's|/usr/lib/rpm/redhat|/usr/lib/rpm/momonga|g'

%build
%if %{without int_bdb}
#CPPFLAGS=-I%{_includedir}/db%{bdbver} 
#LDFLAGS=-L%{_libdir}/db%{bdbver}
%endif
CPPFLAGS="$CPPFLAGS `pkg-config --cflags nss` -I%{_includedir}/db4"
CFLAGS="$RPM_OPT_FLAGS -L%{_libdir}/db4"
export CPPFLAGS CFLAGS LDFLAGS

# Using configure macro has some unwanted side-effects on rpm platform
# setup, use the old-fashioned way for now only defining minimal paths.
./configure \
    --prefix=%{_usr} \
    --sysconfdir=%{_sysconfdir} \
    --localstatedir=%{_var} \
    --sharedstatedir=%{_var}/lib \
    --libdir=%{_libdir} \
    %{!?with_int_bdb: --with-external-db} \
    %{?with_sqlite: --enable-sqlite3} \
    --with-lua \
    --with-selinux \
    --with-cap \
    --with-acl \
    --enable-python

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR="%{buildroot}" install

# Save list of packages through cron
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.daily
install -m 755 scripts/rpm.daily ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.daily/rpm

mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d
install -m 644 scripts/rpm.log ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/rpm

mkdir -p %{buildroot}%{_sysconfdir}/rpm

mkdir -p %{buildroot}/var/lib/rpm
for dbi in \
    Basenames Conflictname Dirnames Group Installtid Name Packages \
    Providename Provideversion Requirename Requireversion Triggername \
    Filedigests Pubkeys Sha1header Sigmd5 Obsoletename \
    __db.001 __db.002 __db.003 __db.004 __db.005 __db.006 __db.007 \
    __db.008 __db.009
do
    touch %{buildroot}/var/lib/rpm/$dbi
done

# plant links to db utils as rpmdb_foo so existing documantion is usable
%if %{without int_bdb}
for dbutil in \
    archive deadlock dump load printlog \
    recover stat upgrade verify
do
    ln -s ../../bin/%{dbprefix}_${dbutil} %{buildroot}/%{rpmhome}/rpmdb_${dbutil}
done
%endif

%find_lang %{name}

find %{buildroot} -name "*.la"|xargs rm -f

# avoid dragging in tonne of perl libs for an unused script
chmod 0644 %{buildroot}/%{rpmhome}/perldeps.pl

# compress our ChangeLog, it's fairly big...
bzip2 -9 ChangeLog

# Provide python-cups
rm -rf %{buildroot}%{rpmhome}/postscriptdriver.prov

%clean
rm -rf %{buildroot}

%if %{with check}
%check
make check
[ "$(ls -A tests/rpmtests.dir)" ] && cat tests/rpmtests.log
%endif

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%posttrans
# XXX this is klunky and ugly, rpm itself should handle this
dbstat=/usr/lib/rpm/rpmdb_stat
if [ -x "$dbstat" ]; then
    if "$dbstat" -e -h /var/lib/rpm 2>&1 | grep -q "doesn't match environment version \| Invalid argument"; then
        rm -f /var/lib/rpm/__db.* 
    fi
fi
exit 0

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc GROUPS COPYING CREDITS ChangeLog.bz2 doc/manual/[a-z]*

%dir                            %{_sysconfdir}/rpm

%attr(0755, root, root)   %dir /var/lib/rpm
%attr(0644, root, root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/lib/rpm/*
%attr(0755, root, root) %dir %{rpmhome}

/bin/rpm
%{_bindir}/rpm2cpio
%{_bindir}/rpmdb
%{_bindir}/rpmsign
%{_bindir}/rpmquery
%{_bindir}/rpmverify

%{_mandir}/man8/rpm.8*
%{_mandir}/man8/rpm2cpio.8*

# XXX this places translated manuals to wrong package wrt eg rpmbuild
%lang(fr) %{_mandir}/fr/man[18]/*.[18]*
%lang(ko) %{_mandir}/ko/man[18]/*.[18]*
%lang(ja) %{_mandir}/ja/man[18]/*.[18]*
%lang(pl) %{_mandir}/pl/man[18]/*.[18]*
%lang(ru) %{_mandir}/ru/man[18]/*.[18]*
%lang(sk) %{_mandir}/sk/man[18]/*.[18]*

%{rpmhome}/macros
%{rpmhome}/rpmpopt*
%{rpmhome}/rpmrc

%{rpmhome}/rpmdb_*
%{rpmhome}/rpm.daily
%{rpmhome}/rpm.log
%{rpmhome}/rpm.xinetd
%{rpmhome}/rpm2cpio.sh
%{rpmhome}/tgpg

%{rpmhome}/platform

%files libs
%defattr(-,root,root)
%{_libdir}/librpm*.so.*

%files build
%defattr(-,root,root)
%{_bindir}/rpmbuild
%{_bindir}/gendiff

%{_mandir}/man1/gendiff.1*

%{rpmhome}/brp-*
%{rpmhome}/check-buildroot
%{rpmhome}/check-files
%{rpmhome}/check-prereqs
%{rpmhome}/check-rpaths*
%{rpmhome}/debugedit
%{rpmhome}/find-debuginfo.sh
%{rpmhome}/find-lang.sh
%{rpmhome}/find-provides
%{rpmhome}/find-requires
%{rpmhome}/javadeps
%{rpmhome}/mono-find-provides
%{rpmhome}/mono-find-requires
%{rpmhome}/ocaml-find-provides.sh
%{rpmhome}/ocaml-find-requires.sh
%{rpmhome}/osgideps.pl
%{rpmhome}/perldeps.pl
%{rpmhome}/libtooldeps.sh
%{rpmhome}/pkgconfigdeps.sh
%{rpmhome}/perl.prov
%{rpmhome}/perl.req
%{rpmhome}/tcl.req
%{rpmhome}/pythondeps.sh
%{rpmhome}/rpmdeps
%{rpmhome}/config.guess
%{rpmhome}/config.sub
%{rpmhome}/mkinstalldirs
%{rpmhome}/rpmdiff*
%{rpmhome}/desktop-file.prov
%{rpmhome}/fontconfig.prov

%{rpmhome}/macros.perl
%{rpmhome}/macros.python
%{rpmhome}/macros.php

%{_mandir}/man8/rpmbuild.8*
%{_mandir}/man8/rpmdeps.8*

%files python
%defattr(-,root,root)
%{python_sitearch}/rpm

%files devel
%defattr(-,root,root)
%{_includedir}/rpm
%{_libdir}/librp*[a-z].so
%{_mandir}/man8/rpmgraph.8*
%{_bindir}/rpmgraph
%{_libdir}/pkgconfig/rpm.pc

%files cron
%defattr(-,root,root)
%{_sysconfdir}/cron.daily/rpm
%config(noreplace) %{_sysconfdir}/logrotate.d/rpm

%files apidocs
%defattr(-,root,root)
%doc doc/librpm/html/*

%changelog
* Wed Feb 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.1-15m)
- import RHEL patches

* Tue Nov 19 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.1-14m)
- add dwarf-4 support to debugedit

* Thu Apr  5 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.1-13m)
- import RHEL patches
-- Proper region tag validation on package/header read (CVE-2012-0060)
-- Double-check region size against header size (CVE-2012-0061)
-- Validate negated offsets too in headerVerifyInfo() (CVE-2012-0815)

* Thu Dec 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.1-12m)
- import RHEL patches

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.1-11m)
- [SECURITY] CVE-2011-3378

* Mon Sep  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.8.1-10m)
- rebuild against db-4.8

* Thu May 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.1-9m)
- remove postscriptdriver.prov

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.1-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.1-7m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.1-6m)
- add patch from fedora

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.1-5m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-4m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.1-3m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.1-2m)
- avoid installation failures when installing rpms which have <lua>
  scripts (e.g. 389-* (Patch800 from openSUSE)
-- https://bugzilla.novell.com/show_bug.cgi?id=201518

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.1-1m)
- [SECURITY] CVE-2010-2059 CVE-2010-2198
- update to 4.8.1

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.0-4m)
- import Patch{4,206,207,208} from Fedora 13 (4.8.0-14)

* Fri Apr 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8.0-3m)
- lzma -> xz

* Mon Mar 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.0-2m)
- drop Patch{503,504,507} which modify some brp scripts.
  now momonganized brp scripts are included in
  momonga-rpmmacros-20100322-1m or later version.

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.0-1m)
- sync with Fedora 13 (4.8.0-10)
- do not run brp-strip-shared (Patch504)

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.2-3m)
- rebuild against db-4.8.26

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.2-2m)
- do not specify db4 versioned dependencies which make it harder to
  upgrade db4

* Thu Nov 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.2-1m)
- update to 4.7.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.1-3m)
- 's|/usr/lib/rpm/redhat|/usr/lib/rpm/momonga|g' in man files

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.1-2m)
- merge the following upstream changes
-- * Tue Sep 15 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.1-5
-- - fix duplicate dependency filtering on build (#490378)
-- - permit absolute paths in file lists again (#521760)
-- - use permissions 444 for all .debug files (#522194)
-- - add support for optional bugurl tag (#512774)

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.1-1m)
- update to 4.7.1 based on Rawhide
- remove specopt configurations, not needed
- remove xz man patch, we use bzip2 since bzip2 is better than xz/lzma
  when compress man and info files.
- remove xz default patch, we move the settings of payload compression
  algorithm to /usr/lib/rpm/momonga/macros (in momonga-rpmmacros package)
- Rawhide changes
-- * Fri Aug 14 2009 Jesse Keating <jkeating@redhat.com> - 4.7.1-4
-- - Patch to make geode appear as i686 (#517475)
-- 
-- * Thu Aug 06 2009 Jindrich Novy <jnovy@redhat.com> - 4.7.1-3
-- - rebuild because of the new xz
-- 
-- * Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.7.1-2
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Tue Jul 21 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.1-1
-- - update to 4.7.1 ((http://rpm.org/wiki/Releases/4.7.1)
-- - fix source url
-- 
-- * Mon Jul 20 2009 Bill Nottingham <notting@redhat.com> - 4.7.0-9
-- - enable XZ support
-- 
-- * Thu Jun 18 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-8
-- - updated OSGi dependency extractor (#506471)
-- - fix segfault in symlink fingerprinting (#505777)
-- - fix invalid memory access causing bogus file dependency errors (#506323)
-- 
-- * Tue Jun 16 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-7
-- - add dwarf-3 support to debugedit (#505774)
-- 
-- * Fri Jun 12 2009 Stepan Kasal <skasal@redhat.com> - 4.7.0-6
-- - require libcap >= 2.16 (#505596)
-- 
-- * Tue Jun 03 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-5
-- - don't mess up problem altNEVR in python ts.check() (#501068)
-- - fix hardlink size calculation on build (#503020)
-- 
-- * Thu May 14 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-4
-- - split cron-job into a sub-package to avoid silly deps on core rpm (#500722)
-- - rpm requires coreutils but not in %%post
-- - build with libcap and libacl
-- - fix pgp pubkey signature tag parsing
-- 
-- * Tue Apr 21 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-3
-- - couple of merge-review fixes (#226377)
--   - eliminate bogus leftover rpm:rpm rpmdb ownership
--   - unescaped macro in changelog
-- - fix find-lang --with-kde with KDE3 (#466009)
-- - switch back to default file digest algorithm
-- 
-- * Fri Apr 17 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-2
-- - file classification tweaks for text files (#494817)
--   - disable libmagic text token checks, it's way too error-prone
--   - consistently classify all text as such and include description

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.0-8m)
- revise comments

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.0-7m)
- run autoreconf for libtool22
- enable extra-prov patch

* Mon May  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.0-6m)
- update BuildRequires lzma >= 4.999.8 for use xz command

* Fri May  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.0-5m)
- merge from T4R
- 
- * Sun Apr 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- - (4.7.0-4m)
- - temporarily disable extra-prov patch
- -- now fontconfig is old, so fontconfig.prov does not work well
- 
- * Sat Apr 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- - (4.7.0-3m)
- - enable lua support but still disable librpm level Lua-extensions (Patch700)
- 
- * Tue Apr 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- - (4.7.0-2m)
- - temporarily disable lua support
- -- librpm level Lua-extensions cause memory corruptions
- 
- * Fri Apr 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- - (4.7.0-1m)
- - merge the following Fedora 11 changes 
- -
- -- * Thu Apr 16 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-1
- -- - update to 4.7.0 final (http://rpm.org/wiki/Releases/4.7.0)
- -- - fixes #494049, #495429
- -- - dont permit test-suite failure anymore
- -- 
- -- * Thu Apr 09 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.rc1.1
- -- - update to 4.7.0-rc1
- -- - fixes #493157, #493777, #493696, #491388, #487597, #493162
- -- 
- -- * Fri Apr 03 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.9
- -- - fix recorded file state of otherwise skipped files (#492947)
- -- - compress ChangeLog, drop old CHANGES file (#492440)
- -- 
- -- * Thu Apr  2 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 4.7.0-0.beta1.8
- -- - Fix sparcv9v and sparc64v targets
- -- 
- -- * Tue Mar 24 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.7
- -- - prefer more specific types over generic "text" in classification (#491349)
- -- 
- -- * Mon Mar 23 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.6
- -- - with the fd leak gone, let libmagic look into compressed files again (#491596)
- -- 
- -- * Mon Mar 23 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.5
- -- - fix font provide generation on filenames with whitespace (#491597)
- -- 
- -- * Thu Mar 12 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.4
- -- - handle RSA V4 signatures (#436812)
- -- - add alpha arch ISA-bits
- -- - enable internal testsuite on build
- -- 
- -- * Mon Mar 09 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.3
- -- - fix _install_langs behavior (#489235)
- -- - fix recording of file states into rpmdb on install
- -- 
- -- * Sun Mar 08 2009 Panu Matilainen <pmatilai@redhat.com> - 4.7.0-0.beta1.2
- -- - load macros before creating directories on src.rpm install (#489104)
- -- 
- -- * Fri Feb 06 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-0.rc4.5
- -- - enable fontconfig provides generation
- -- 
- -- * Wed Feb 04 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-0.rc4.3
- -- - extract mimehandler provides from .desktop files
- -- - preliminaries for extracting font provides (not enabled yet)
- -- - dont classify font metrics data as fonts
- -- - only run script dep extraction once per file, duh
- -
- - rebuild against db4-4.7.25
- - drop Patch501, not needed
- - update Patch504,506
- - refine xz (LZMA) patches
- - remove %%global _default_patch_fuzz 2

* Sat Apr 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-9m)
- rename man_xz to xz_man

* Sun Apr  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-8m)
- revive %%global _binary_payload w9.gzdio for conservative upgrading from mo5

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-7m)
- %%define dbprefix db45

* Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-6m)
- merge the following Fedora 10 changes
-
-- * Thu Mar 12 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-2
-- - handle RSA V4 signatures (#436812)
-- - add alpha arch ISA-bits
-
- merge the following Rawhide changes
-
-- * Thu Feb 26 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-10
-- - handle NULL passed as EVR in rpmdsSingle() again (#485616)
--
-- * Wed Feb 25 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-9
-- - pull out python byte-compile syntax check for now
--
-- * Sat Feb 21 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-7
-- - loosen up restrictions on dependency names (#455119)
-- - handle inter-dependent pkg-config files for requires too (#473814)
-- - error/warn on elf binaries in noarch package in build
--
-- * Fri Feb 20 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-6
-- - error out on uncompilable python code (Tim Waugh)
--
-- * Tue Feb 17 2009 Jindrich Novy <jnovy@redhat.com> - 4.6.0-5
-- - remove two offending hunks from anyarch patch causing that
--   RPMTAG_BUILDARCHS isn't written to SRPMs
-- 
-- * Mon Feb 16 2009 Jindrich Novy <jnovy@redhat.com> - 4.6.0-4
-- - inherit group tag from the main package (#470714)
-- - ignore BuildArch tags for anyarch actions (#442105)
-- - don't check package BuildRequires when doing --rmsource (#452477)
-- - don't fail because of missing sources when only spec removal
--   is requested (#472427)
--
-- * Thu Feb 05 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-0.rc4.4
-- - fixup rpm translation lookup to match Fedora specspo (#436941)
--
-- * Sat Jan 31 2009 Panu Matilainen <pmatilai@redhat.com> - 4.6.0-0.rc4.2
-- - change platform sharedstatedir to something more sensible (#185862)
-- - add rpmdb_foo links to db utils for documentation compatibility

* Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-5m)
- add specopt to select the man compression algorithm
-- bzip2 is default

* Mon Mar 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-4m)
- update man xz patch (Patch503)

* Sun Mar 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-3m)
- add specopt to select the payload compression algorithm
-- gzip is default
- apply PayloadIsLzma downgrade patch
- remove %%global _binary_payload w9.gzdio

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-2m)
- xz man and info (Patch503)

* Fri Feb  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-1m)
- update to 4.6.0

* Sat Jan 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.0-0.4.2m)
- %%global _binary_payload w9.gzdio
-- always gzip-payload

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.4.1m)
- update to 4.6.0-rc4

* Fri Jan 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.8m)
- fix Patch505 for archs except i686

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.7m)
- rebuild against pkgconfig-0.23-4m

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.6m)
- rebuild against python-2.6.1

* Tue Dec 30 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.0-0.3.5m)
- support LZMA payload(lzma-utils-5.x) patch

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.4m)
- update Patch509

* Mon Dec 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.3m)
- update Patch505

* Thu Dec 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.2m)
- sync with Rawhide (4.6.0-0.rc3.2)
- momonganize
-- update momonga patch (Patch500)
-- update momonga-macros patch (Patch501)
-- update shortcircuit patch (Patch502)
-- drop macros-fhs2 patch, merged
-- update brp patch (Patch504)
-- update macros-_build_arch.patch patch (Patch505)
-- update platform__id_u patch (Patch509)
-- drop lzma-support patch, merged
-- apply Patch510 which avoids the updatespecdb bug
- set _default_patch_fuzz to 2
- BuildRequires: compat-db45
- BuildRequires: file-devel

* Sat Dec 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-0.3.1m)
- sync with Fedora 10 (4.6.0-0.rc3.1)

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.3-2m)
- build fix x86_64

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.3-1m)
- update 4.4.2.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2.2-13m)
- rebuild against gcc43

* Sun Mar  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.2-12m)
- rebuild against neon-0.28.0

* Sun Feb 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2.2-11m)
- add __id_u macro 

* Sun Feb 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.2-10m)
- add lzma default patch (but disabled)
- add no_ts_check patch

* Sat Feb  9 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2.2-9m)
- update gcc43.patch
- import some patches from fc-devel
- add %%doc

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.4.2.2-8m)
- rebuild against perl-5.10.0-1m

* Wed Jan  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.2-7m)
- support LZMA compress

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2.2-6m)
- added a patch to fix compilation issue with gcc43

* Tue Nov  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.2-5m)
- no build popt package

* Thu Oct 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2.2-4m)
- fixed momorel number
- merged SOME fixes between fc-devel rpm-4_4_2_2-0_5_rc2 and rpm-4_4_2_2-4_fc8
-- add osgi automagic dependency extractor
-- add debugedit fixes
-- minor spec cleanups

* Fri Oct 12 2007 Masanobu Sato <satoshiga@momonga-linux.rog>
- (4.4.2.2-2m)
- revised %files section for x86_64. don't use %{rpmhome}

* Fri Oct 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.2-1m)
- update 4.4.2.2 release

* Sun Sep 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2.2-0.2.rc2-1m)
- remove rpm-4.4.2-exclude-from-strip.patch, which is no longer required
- sync with fc-devel (rpm-4_4_2_2-0_5_rc2)

* Sun Sep 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.2-0.2.rc2-1m)
- update 4.4.2.2-rc2

* Sun Sep  2 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2.2-0.1.rc1.4m)
- remove unused files
- sync with fc-devel rpm-4_4_2_2-0_1_rc1
--* Tue Aug 28 2007 Panu Matilainen <pmatilai@redhat.com> 4.4.2.2-0.1.rc1
--- update to 4.4.2.2-rc1
--- remove no longer needed hacks
--- drop patches merged upstream

* Fri Aug 24 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2.1-4m)
- copy from trunk:r18204
- just for test
-- add rpm-4.4.2.1-momonga-eu-strip.patch
-- import new find-debuginfo.sh from fc-devel

* Tue Aug 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.1-3m)
- import fedora patches
- remove rpm-4.4.2.1-ts_no_check.patch

* Thu Aug  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.1-2m)
- add rpm-4.4.2.1-ts_no_check.patch
-- check on transaction processing is loosened

* Tue Jul 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2.1-1m)
- update 4.4.2.1

* Mon May 28 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-20m)
- change japanese manual encode as utf-8

* Wed May 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-19m)
- sync fc-devel (4.4.2-46)

* Fri Mar 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-18m)
- sync fc-devel (4.4.2-40)

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-17m)
- makimodoshi 4.4.2-15m

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-16m)
- add LDFLAGS="-L /%%{_lib}"
-- Because /lib/libncurses.so cannot be linked

* Tue Feb 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-15m)
- add Require elfutils-libelf

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.4.2-14m)
- delete libtool library

* Sat Jan  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.4.2-13m)
- add patch200 (for rpm-python by h_nakamura)

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-12m)
- rebuild against python-2.5

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-11m)
- add rpm-4.4.2-gnuhash.patch

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.4.2-10m)
- rebuild against expat-2.0.0-1m

* Sat Jul  8 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2-9m)
- add rpm-4.4.2-exclude-from-strip.patch

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-8m)
- depend sqlite3 -> sqlite

* Mon Jun  5 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-7m)
- disable rpm-4.4.2-rpmlib-require.patch 

* Wed May 31 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-6m)
- import from fc-devel
-   rpm-4.4.2-netsharedpath.patch
-   rpm-4.4.2-userlock.patch
-   rpm-4.4.2-vercmp.patch
-   rpm-4.4.2-doxy.patch
-   rpm-4.4.2-trust.patch
-   rpm-4.4.2-devel-autodep.patch
-   rpm-4.4.2-rpmfc-skip.patch

* Thu May 25 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.4.2-5m)
- enable rpm-4.4.2-rpmlib-require.patch 

* Thu May 25 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-4m)
- enable rpm-4.4.2-brp.patch
- enable rpm-4.4.2-macros-fhs2.patch
- enable rpm-4.4.2-macros-_build_arch.patch
- enable rpm-4.4.2-disable-_smp_mflags.patch
- convert japanese manpage to euc-jp

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-3m)
- enable rpm-4.4.2-man-bzip2.patch

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-2m)
- enable rpm-4.4.2-shortcircuit.patch
- modify spec

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.2-1m)
- version up

* Sat Feb 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.2-20m)
- glibc-2.3.90 ready
-- add rpm-4.2-nptl-1.patch

* Wed Nov 2 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.3.2-19m)
- make rpm package python-2.4 ready

* Mon Oct 31 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3.2-18m)
- add gcc4 patch (Patch35)

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.3.2-17m)
- rebuild against python-2.4.2

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.3.2-16m)
- enable ppc64, mipsel

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.3.2-15m)
- rebuild against zlib-1.2.3 (CAN-2005-1849)

* Wed Jul 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.3.2-14m)
- rebuild against nptl-devel-2.3.4-2m

* Tue Jul 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.3.2-13m)
- rebuild against zlib-devel 1.2.2-2m, which fixes CAN-2005-2096.

* Wed Mar 02 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (4.3.2-12m)
- fixed typo in ja/rpm.8

* Fri Feb 18 2005 Toru Hoshina <t@momonga-linux.org>
- (4.3.2-11m)
- revised spec so the static library avoid stack protection.
- libpopt-diet.a is no longer provided, because libpopt.a is meaning it.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.3.2-10m)
- enable x86_64 libpopt-diet.

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (4.3.2-9m)
- enable x86_64.

* Mon Nov 29 2004 TAKAHASHI Tamotsu <tamo>
- (4.3.2-8m)
- with SELinux support
- BuildPreReq: dietlibc
- BuildPreReq: libselinux-devel

* Mon Oct  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.3.2-7m)
- remove '-fomit-frame-pointer' from optflags

* Sun Oct  3 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (4.3.2-6m)
- add a patch to fix typo (patch34).

* Sat Sep 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.2-5m)
- rebuild against python2.3

* Fri Aug 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.3.2-4m)
  added libpopt-diet.a for anaconda

* Wed Aug 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3.2-3m)
- revise char code of japanese man pages (UTF-8 -> EUC-JP)

* Mon Aug  9 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3.2-2m)
- add pthread patch (thanks to famao-san)

* Sat Aug  7 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3.2-1m)
- update to 4.3.2
- without selinux support

* Thu Jul 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2
- disable secure_getenv temporarily for anaconda

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.2.1-9m)
- remove Epoch from BuildPrereq

* Mon Jul  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.1-8m)
- require momonga-rpmmacros

* Fri May 22 2004 Toru Hoshina <t@momonga-linux.org>
- (4.2.1-7m)
- add elfutils-devel as a buildrequires.

* Wed May 12 2004 Toru Hoshina <t@momonga-linux.org>
- (4.2.1-6m)
- revised omit-brp-strip-static-archive.patch to strip shared object too.

* Tue Apr 13 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.1-5m)
- moved local configuraton directory from /etc/rpm/specopt to ~/rpm/specopt/

* Sun Mar 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.1-3m)
- omit brp-strip-static-archive from platform.in

* Wed Mar 11 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.1-2m)
- revise rpm-4.2.1-brp.patch

* Wed Mar 10 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.1-1m)
- import to momonga

* Wed Jul 16 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.30
- repair find-debuginfo.sh to avoid recursing in /usr/lib/debug.
- fix: ia64: don't attempt autorelocate on .src.rpm's.
- fix: debuginfo: -not -path /usr/lib/debug needed -prune too.

* Thu Jul 10 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.26
- apply debugedit patch necessary to produce kernel -debuginfo files.
- zap zlib files so that apidocs gets included.

* Wed Jul  9 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.21
- resolve elf32/elf64 file conflicts to prefer elf64.

* Tue Jul  8 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.20
- resurrect manifests, RPMRC_NOTFOUND returned by readLead().
- python: missed tuple refcount in IDTXload/IDTXglob.
- fix: IDTXglob should return REMOVETID sorted headers (#89857).

* Wed Jul  2 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.19
- autorelocate ix86 package file paths on ia64.

* Tue Jul  1 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.18
- don't attempt to remove dbenv on --rebuilddb.
- rebuild.

* Tue Jun 24 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.17
- update for fr.po (#97829).

* Fri Jun 20 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.16
- brp-python-bytecompile to automagically bytecode compile python.

* Thu Jun 19 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.15
- 2nd test release.

* Thu Jun 12 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.13
- fdCLose typo (#97257).
- test release.

* Mon Jun  9 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.12
- gratuitous bump/rebuild to exclude ppc64 for the moment.

* Thu Jun  5 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.11
- update ja man pages (#92261).
- backport rpmsw stopwatch, insturment rpmts operations.
- toy method to enable --stats through bindings.

* Wed Jun  4 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.8
- pass structure pointer, not args, through headerSprintf call chain.
- add ":xml" header format modifier.
- --queryformat '[%%{*:xml}\n]' to dump header content in XML.
- add ".arch" suffix to erase colored packages with identical NEVR.

* Tue Jun  3 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.6
- rebuild against fixes in beecrypt-3.0.0-0.20030603.
- treat missing epoch's exactly the same as Epoch: 0.

* Mon Jun  2 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.4
- rebuild against fixes in beecrypt-3.0.0-0.20030602.

* Thu May 29 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.3
- build with external beecrypt-3.0.0.
- blueprint beecrypt-3.0.0 changes against rpm-4.3.
- x86_64 -> athlon, ppc64[ip]series -> ppc64 arch compatibility.

* Thu Mar 27 2003 Jeff Johnson <jbj@redhat.com> 4.2.1-0.1
- start rpm-4.2.1.
- hack out O_DIRECT support in db4 for now.
