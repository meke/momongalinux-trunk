%global         momorel 12

Name:           perl-POE-Component-SSLify
Version:        1.008
Release:        %{momorel}m%{?dist}
Summary:        Makes using SSL in the world of POE easy!
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/POE-Component-SSLify/
Source0:        http://www.cpan.org/authors/id/A/AP/APOCAL/POE-Component-SSLify-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-File-Temp
BuildRequires:  perl-IO >= 1.25
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Net-SSLeay >= 1.36
BuildRequires:  perl-POE >= 1.267
BuildRequires:  perl-Socket
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-IO >= 1.25
Requires:       perl-Net-SSLeay >= 1.36
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This component represents the standard way to do SSL in POE.

%prep
%setup -q -n POE-Component-SSLify-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/POE/Component/SSLify*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-12m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-11m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-10m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-9m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-8m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-7m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-6m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-5m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008-1m)
- update to 1.008

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006-1m)
- update to 1.006

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.005-2m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005-1m)
- update to 1.005

* Wed Mar  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004-1m)
- update to 1.004

* Tue Mar  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003-1m)
- update to 1.003

* Sun Feb 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002-1m)
- update to 1.002

* Mon Feb 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.001-1m)
- update to 1.001

* Sun Feb 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000-1m)
- update to 1.000

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.12.1

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-2m)
- rebuild against perl-5.12.0

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.10.1

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Wed Jun 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Tue Jun 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-2m)
- rebuild against gcc43

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
