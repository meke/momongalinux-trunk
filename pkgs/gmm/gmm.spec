%global momorel 4

Name:		gmm
Summary:	A generic C++ template library for sparse, dense and skyline matrices
Version:	4.1
Release:	%{momorel}m%{?dist} 
Group:		Development/Libraries
License:	LGPLv2+ 
URL:		http://home.gna.org/getfem/gmm_intro
Source0:	http://download.gna.org/getfem/stable/gmm-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
%description
%{summary}

%package devel
Summary:	A generic C++ template library for sparse, dense and skyline matrices
Group:		Development/Libraries
Provides:	%{name} = %{version}-%{release}
Provides:	gmm++-devel = %{version}-%{release}
%description devel
%{summary}

%prep
%setup -q

%build
## %%configure does not work...
./configure \
	--prefix=/usr \
	--exec-prefix=/usr \
	--bindir=/usr/bin \
	--sbindir=/usr/sbin \
	--sysconfdir=/etc \
	--datadir=/usr/share \
	--includedir=/usr/include \
	--libdir=/usr/lib64 \
	--libexecdir=/usr/libexec \
	--localstatedir=/var \
	--sharedstatedir=/usr/com \
	--mandir=/usr/share/man \
	--infodir=/usr/share/info

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} 

%clean 
rm -rf %{buildroot}

%files devel
%defattr(-,root,root,-)
%{_includedir}/gmm/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-1m)
- update to 4.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-2m)
- rebuild against rpm-4.6

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-1m)
- import from Fedora devel and update to 3.0.4

* Wed May 28 2008 Steven Parrish <smparrish[at]shallowcreek.net> 3.0-3
- corrected license

* Wed May 28 2008 Rex Dieter <rdieter@fedoraproject.org> 3.0-2
- name gmm
- -devel: Provides: gmm++-devel = ...

* Tue May 27 2008 Steven Parrish <smparrish[at]shallowcreek.net> 3.0-1
-  Initial SPEC file
