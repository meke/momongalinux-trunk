%global         momorel 1

Summary:        Netscape Portable Runtime
Name:           nspr
Version:        4.10.6
Release:        %{momorel}m%{?dist}
License:        MPLv1.1 or GPLv2+ or LGPLv2+
URL:            http://www.mozilla.org/projects/nspr/
Group:          System Environment/Libraries
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Sources available at ftp://ftp.mozilla.org/pub/mozilla.org/nspr/releases/
# When CVS tag based snapshots are being used, refer to CVS documentation on
# mozilla.org and check out subdirectory mozilla/nsprpub.
Source0:        ftp://ftp.mozilla.org/pub/mozilla.org/nspr/releases/v%{version}/src/%{name}-%{version}.tar.gz
NoSource:	0

Patch1:         nspr-config-pc.patch

%description
NSPR provides platform independence for non-GUI operating system 
facilities. These facilities include threads, thread synchronization, 
normal file and network I/O, interval timing and calendar time, basic 
memory management (malloc and free) and shared library linking.

%package devel
Summary:        Development libraries for the Netscape Portable Runtime
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
Header files for doing development with the Netscape Portable Runtime.

%prep

%setup -q

# Original nspr-config is not suitable for our distribution,
# because on different platforms it contains different dynamic content.
# Therefore we produce an adjusted copy of nspr-config that will be 
# identical on all platforms.
# However, we need to use original nspr-config to produce some variables
# that go into nspr.pc for pkg-config.

cp ./nspr/config/nspr-config.in ./nspr/config/nspr-config-pc.in
%patch1 -p0

%build

./nspr/configure \
                 --prefix=%{_prefix} \
                 --libdir=%{_libdir} \
                 --includedir=%{_includedir}/nspr4 \
%ifarch x86_64 ppc64 ia64 s390x sparc64
                 --enable-64bit \
%endif
%ifarch armv7l armv7hl armv7nhl
                 --enable-thumb2 \
%endif
                 --enable-optimize="$RPM_OPT_FLAGS" \
                 --disable-debug

make

%check

# Run test suite.
perl ./nspr/pr/tests/runtests.pl 2>&1 | tee output.log

TEST_FAILURES=`grep -c FAILED ./output.log` || :
if [ $TEST_FAILURES -ne 0 ]; then
  echo "error: test suite returned failure(s)"
  exit 1
fi
echo "test suite completed"

%install

%{__rm} -Rf $RPM_BUILD_ROOT

DESTDIR=$RPM_BUILD_ROOT \
  make install

NSPR_LIBS=`./config/nspr-config --libs`
NSPR_CFLAGS=`./config/nspr-config --cflags`
NSPR_VERSION=`./config/nspr-config --version`
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}/pkgconfig

# Get rid of the things we don't want installed (per upstream)
%{__rm} -rf \
   $RPM_BUILD_ROOT/%{_bindir}/compile-et.pl \
   $RPM_BUILD_ROOT/%{_bindir}/prerr.properties \
   $RPM_BUILD_ROOT/%{_libdir}/libnspr4.a \
   $RPM_BUILD_ROOT/%{_libdir}/libplc4.a \
   $RPM_BUILD_ROOT/%{_libdir}/libplds4.a \
   $RPM_BUILD_ROOT/%{_datadir}/aclocal/nspr.m4 \
   $RPM_BUILD_ROOT/%{_includedir}/nspr4/md

%clean
%{__rm} -Rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig >/dev/null 2>/dev/null

%postun
/sbin/ldconfig >/dev/null 2>/dev/null

%files
%defattr(-,root,root)
%{_libdir}/libnspr4.so
%{_libdir}/libplc4.so
%{_libdir}/libplds4.so

%files devel
%defattr(-, root, root)
%{_includedir}/nspr4
%{_libdir}/pkgconfig/nspr.pc
%{_bindir}/nspr-config

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.6-1m)
- update to 4.10.6

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to 4.10.4

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to 4.10.2

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to 4.10.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10-1m)
- update to 4.10

* Sat Apr 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.6-1m)
- update to 4.9.6

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.5-1m)
- update to 4.9.5

* Mon Dec 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.4-1m)
- update to 4.9.4

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to 4.9.3

* Sat Oct 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.2-2m)
- fix %%files to avoid conflicting

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to 4.9.2

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9-1m)
- update to 4.9

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.9-1m)
- update to 4.8.9

* Tue Aug  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.8-1m)
- update
- merge with fedora's nspr-4.8.8-4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.7-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.7-1m)
- update

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.6-2m)
- rebuild for new GCC 4.5

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.6-1m)
- update to 4.8.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.4-2m)
- full rebuild for mo7 release

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.4-1m)
- update to 4.8.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.2-1m)
- [SECURITY] CVE-2009-1563 CVE-2009-2463
- update to 4.8.2

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8-1m)
- update to 4.8 for xulrunner-1.9.1rc3

* Sat Mar  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.3-3m)
- import gcc44 patch from Rawhide (4.7.3-5)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.3-2m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.3-1m)
- update to 4.7.3

* Thu Jun  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.1-1m)
- update 4.7.1

* Sun Apr 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.0.99.2-1m)
- update for nss

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.6-4m)
- rebuild against gcc43

+* Thu Mar 20 2008 Jesse Keating <jkeating@redhat.com> - 4.7.0.99.2-2
+- Drop the old obsoletes/provides that aren't needed anymore.
#'
 
* Mon Mar 17 2008 Kai Engert <kengert@redhat.com> - 4.7.0.99.2-1
- Update to NSPR_4_7_1_BETA2

* Tue Feb 26 2008 Kai Engert <kengert@redhat.com> - 4.7.0.99.1-2
- Addressed cosmetic review comments from bug 226202

* Mon Feb 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.0.99.1-1m)
- update for nss

* Fri Feb 22 2008 Kai Engert <kengert@redhat.com> - 4.7.0.99.1-1
- Update to NSPR 4.7.1 Beta 1
- Use /usr/lib{64} as devel libdir, create symbolic links.

* Fri Feb 22 2008 Kai Engert <kengert@redhat.com> - 4.7.0.99.1-1
- Update to NSPR 4.7.1 Beta 1
- Use /usr/lib{64} as devel libdir, create symbolic links.

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.6-3m)
- %%NoSource -> NoSource

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7-1m)
- update for xlurunner

* Sat Feb 09 2008 Kai Engert <kengert@redhat.com> - 4.7-1
- Update to NSPR 4.7

* Thu Jan 24 2008 Kai Engert <kengert@redhat.com> - 4.6.99.3-1
- NSPR 4.7 beta snapshot 20080120

* Mon Jan 07 2008 Kai Engert <kengert@redhat.com> - 4.6.99-2
- move .so files to /lib

* Wed Nov 07 2007 Kai Engert <kengert@redhat.com> - 4.6.99-1
- NSPR 4.7 alpha

* Tue Aug 28 2007 Kai Engert <kengert@redhat.com> - 4.6.7-3
- Updated license tag

* Sat Jul 14 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.6-2m)
- merged thread cleanup patch from upstream

* Mon May 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.6-1m)
- Update to 4.6.6
- sync with fc-devel (nspr-4_6_6-1)

* Sat Feb 24 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.5-2m)
- sync with fc-devel (nspr-4_6_5-2)
--* Sat Feb 24 2007 Kai Engert <kengert@redhat.com> - 4.6.5-2
--- Update to latest ipv6 upstream patch
--- Add upstream patch to fix a thread cleanup issue
--- Now requires pkgconfig

* Thu Jan 25 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.5-1m)
- Update to 4.6.5

* Wed Nov 22 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.4-1m)
- Update to 4.6.4

* Sat Nov 11 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.3-1m)
- sync with fc-devel (nspr-4_6_3)

* Sun Feb 19 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.6.1-1m)
- update to 4.6.1

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.6-1m)
- import from fedora

* Fri Jul 15 2005 Christopher Aillon <caillon@redhat.com> 4.6-4
- Use the NSPR version numbering scheme reported by NSPR,
  which unfortunately is not exactly the same as the real
  version (4.6 != 4.6.0 according to RPM and pkgconfig).

* Fri Jul 15 2005 Christopher Aillon <caillon@redhat.com> 4.6-3
- Correct the CFLAGS reported by pkgconfig

* Tue Jul 12 2005 Christopher Aillon <caillon@redhat.com> 4.6-2
- Temporarily include the static libraries allowing nss and 
  its dependencies to build. 

* Tue Jul 12 2005 Christopher Aillon <caillon@redhat.com> 4.6-1
- Update to NSPR 4.6

* Wed Apr 20 2005 Christopher Aillon <caillon@redhat.com> 4.4.1-2
- NSPR doesn't have make install, but it has make real_install.  Use it.

* Thu Apr 14 2005 Christopher Aillon <caillon@redhat.com> 4.4.1-1
- Let's make an RPM.
