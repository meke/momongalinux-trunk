%global momorel 1
%define pkgname xinput
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.6.0
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: libXt-devel
BuildRequires: xorg-x11-proto-devel >= 7.7-0.5m

%description
%{pkgname}

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS README
%{_bindir}/xinput
%{_mandir}/man1/xinput.1.bz2

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.99.1-1m)
- update to 1.5.99.1

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- update to 1.5.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-2m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- Initial Commit Momonga Linux

