%global momorel 10

Summary: General dimension convex hull programs
Name: qhull
Version: 2003.1
Release: %{momorel}m%{?dist}
License: see "COPYING.txt"
Group: System Environment/Libraries
Source0: http://www.qhull.org/download/qhull-%{version}.tar.gz 
NoSource: 0
URL: http://www.qhull.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Qhull is a general dimension convex hull program that reads a set
of points from stdin, and outputs the smallest convex set that contains
the points to stdout.  It also generates Delaunay triangulations, Voronoi
diagrams, furthest-site Voronoi diagrams, and halfspace intersections
about a point.

%package devel
Group: Development/Libraries
Summary: Development files for qhull
Requires: %{name} = %{version}-%{release}

%description devel
Qhull is a general dimension convex hull program that reads a set
of points from stdin, and outputs the smallest convex set that contains
the points to stdout.  It also generates Delaunay triangulations, Voronoi
diagrams, furthest-site Voronoi diagrams, and halfspace intersections
about a point.

%prep
%setup -q -n %{name}-%{version}


%build
CFLAGS="%{optflags} -fno-strict-aliasing" \
%configure
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} \
  docdir=%{_docdir}/%{name}-%{version} install

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc %{_docdir}/%{name}-%{version}
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%{_libdir}/*.*a
%{_libdir}/*.so
%{_includedir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2003.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2003.1-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2003.1-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2003.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2003.1-6m)
- fix segmentation fault

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2003.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2003.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2003.1-3m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2003.1-2m)
- delete libtool library

* Tue May 16 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2003.1-1m)
- import to Momonga for octave-forge
 
* Tue Jun 28 2005 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (2003.1-0.0.1m)
- build for Momonga
- based on qhull-2003.1-4.src.rpm at Fedora extra

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 2003.1-4
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sun Aug 08 2004 Ralf Corsepius <ralf[AT]links2linux.de>	- 2003.1-0.fdr.2
- Use default documentation installation scheme.

* Fri Jul 16 2004 Ralf Corsepius <ralf[AT]links2linux.de>	- 2003.1-0.fdr.1
- Initial Fedora RPM.
