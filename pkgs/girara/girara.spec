%global momorel 1
Name:           girara
Version:        0.1.9
Release: %{momorel}m%{?dist}
Summary:        Simple user interface library
Group:          Development/Libraries
License:        "zlib"
URL:            http://pwmt.org/projects/%{name}/
Source0:        http://pwmt.org/projects/%{name}/download/%{name}-%{version}.tar.gz
NoSource: 0
BuildRequires:  gtk2-devel gettext

%description
Girara is a library that implements a user interface that focuses on simplicity
and minimalism.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
# enforce gtk2
sed -i -e 's/^GIRARA_GTK_VERSION.*/GIRARA_GTK_VERSION ?= 2/g' config.mk
# build
CFLAGS='%{optflags}' %make

%install
make install DESTDIR=%{buildroot} LIBDIR=%{_libdir}
chmod +x %{buildroot}/%{_libdir}/libgirara-gtk?.so.1.1
# fcami - I wish upstream used a consistent naming scheme
%find_lang lib%{name}-gtk2-1
mv lib%{name}-gtk2-1.lang %{name}.lang
rm -f %{buildroot}/%{_libdir}/libgirara-gtk?.a

%files -f  %{name}.lang
%doc AUTHORS LICENSE README 
%{_libdir}/libgirara-gtk?.so.*

%files devel
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%{_libdir}/pkgconfig/girara-gtk2.pc
%{_libdir}/libgirara-gtk?.so

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Wed Feb 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.9-1m)
- import from fedora

