%global momorel 13
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: The international fonts
Name: intlfonts
Version: 1.2.1
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: GPLv2+
URL: http://directory.fsf.org/project/intlfonts/
Source0: http://ftp.gnu.org/gnu/intlfonts/intlfonts-%{version}.tar.gz
NoSource: 0
Patch0: jksp24-240.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fontpackages-devel
BuildRequires: xorg-x11-font-utils
BuildArch: noarch
Obsoletes: XFree86-intlfonts
Obsoletes: XFree86-intl-fonts

%description
Intlfonts is one of the font packages for internationalized X Window System.

%prep
%setup -qn intlfonts-%{version}
%patch0 -p1

./configure --with-fontdir=%{buildroot}%{_fontdir} --without-bdf --enable-compress=gzip --without-type1 --without-truetype 
#make
for i in */README; do cp -p $i `echo $i|sed 's,\(.*\)/README,README-\1,'`; done

%install
rm -rf %{buildroot}
make SUBDIRS_X="" prefix=%{buildroot}%{_prefix} install
/usr/bin/mkfontdir %{buildroot}%{_fontdir}

mkdir -p %{buildroot}%{catalogue}
ln -sf %{_fontdir} %{buildroot}%{catalogue}/%{name}

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc ChangeLog Emacs.ap NEWS README*
%{catalogue}/%{name}
%dir %{_fontdir}
%{_fontdir}/*.pcf.gz
%verify(not md5 size mtime) %{_fontdir}/fonts.alias
%verify(not md5 size mtime) %{_fontdir}/fonts.dir

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-9m)
- correct font catalogue
- remove Prereq: chkfontpath-momonga

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-7m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+
- URL: http://directory.fsf.org/project/intlfonts/

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-6m)
- rebuild against gcc43

* Fri Apr 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-5m)
- change download URL: ftp.gnu.org/gnu/intlfonts/

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-4m)
- change xorg-x11 -> xorg-x11-fonts-base
- delete Provides: XFree86-intl-fonts

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-3m)
- revise for xorg-7.0
- change install dir

* Sat Dec  6 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Aug  3 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.1-1m)
- add more documents

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (1.2.4-8k)
- mkfontdir tukattendaro korua.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.4-6k)

* Fri Dec 07 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.4-4k)
- added jksp24-240.patch
- jksp24.bdf is made by bderesize.
- its size is 234, causes problem on some applications.

* Sun Mar 06 2001 Kenzi Cano<kc@ring.gr.jp>
- (1.2-3k)
- package-name was changed( XFree86-intl-fonts > intlfonts )

* Thu Jul 06 2000 Kenzi Cano<kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2-1k)
- package-name was changed( XFree86-intlfonts > XFree86-intl-fonts )

* Wed Mar 22 2000 Hidetomo Machi <mcHT@kondara.org>
- add %dir

* Thu Jan 13 2000 Toru Hoshina <t@kondara.org>
- add chkfontpath.

* Sat Jan  8 2000 SAKA Toshihide <saka@yugen.org>
- First release.
- Installation information originally comes from
  http://www.running-dog.net/bsd/330/font+.html
