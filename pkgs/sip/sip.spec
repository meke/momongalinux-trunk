%global pythonver 2.7
%global pythonsitedir %{_libdir}/python%{pythonver}/site-packages
%global pythonpkg python
%global minor .3
%global src_ver 4.15
#global snapshot_date 20100108
%global with_python3 1

%if 0%{?with_python3}
%{!?python3_inc:%global python3_inc %(%{__python3} -c "from distutils.sysconfig import get_python_inc; print(get_python_inc(1))")}
%endif

%{!?python_sitearch:%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?python_inc:%global python_inc %(%{__python} -c "from distutils.sysconfig import get_python_inc; print get_python_inc(1)")}

%global qtver 4.8.5
%global qtpkg qt
%global momorel 2


%if "%{pythonver}" == "2.7"
%global pythonprog /usr/bin/python2.7
%else
%global pythonprog /usr/bin/python
%endif

Summary: SIP - Python/C++ Bindings Generator
Name: sip
Version: %{src_ver}%{?minor:%{minor}}
Release: %{?snapshot_date:0.%{snapshot_date}.}%{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Url: http://www.riverbankcomputing.co.uk/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://dl.sourceforge.net/project/pyqt/%{name}/%{name}-%{version}/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires: %{pythonpkg}-devel >= %{pythonver}
BuildRequires: %{qtpkg}-devel >= %{qtver}
BuildRequires: sed
%if 0%{?with_python3}
BuildRequires:  python3-devel >= 3.4
%endif

Requires: %{pythonpkg} >= %{pythonver}

%description
SIP is a tool for generating bindings for C++ classes so that they can be
accessed as normal Python classes.  SIP takes many of its ideas from SWIG but,
because it is specifically designed for C++ and Python, is able to generate
tighter bindings.  SIP is so called because it is a small SWIG.

SIP was originally designed to generate Python bindings for KDE and so has
explicit support for the signal slot mechanism used by the Qt/KDE class
libraries.  However, SIP can be used to generate Python bindings for any C++
class library.

%package devel
Summary: Files needed to generate Python bindings for any C++ class library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python-devel 

%description devel
This package contains files needed to generate Python bindings for any C++
classes library.

%if 0%{?with_python3}
%package -n python3-sip
Summary: SIP - Python 3/C++ Bindings Generator
Group: Development/Tools

%description -n python3-sip
This is the Python 3 build of SIP.

SIP is a tool for generating bindings for C++ classes so that they can be
accessed as normal Python 3 classes. SIP takes many of its ideas from SWIG but,
because it is specifically designed for C++ and Python, is able to generate
tighter bindings. SIP is so called because it is a small SWIG.

SIP was originally designed to generate Python bindings for KDE and so has
explicit support for the signal slot mechanism used by the Qt/KDE class
libraries. However, SIP can be used to generate Python 3 bindings for any C++
class library.

%package -n python3-sip-devel
Summary: Files needed to generate Python 3 bindings for any C++ class library
Group: Development/Libraries
Requires: python3-sip = %{version}-%{release}
Requires: python3-devel

%description -n python3-sip-devel
This package contains files needed to generate Python 3 bindings for any C++
classes library.
%endif

%prep

%setup -q -n %{name}-%{version}%{?snapshot_date:-snapshot-%{snapshot_date}}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
%{__python} configure.py -d %{pythonsitedir} CXXFLAGS="%{optflags}" CFLAGS="%{optflags}"

make %{?_smp_mflags}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} configure.py -d %{python3_sitearch} CXXFLAGS="%{optflags}" CFLAGS="%{optflags}" --sipdir=%{_datadir}/python3-sip

make %{?_smp_mflags} 
popd
%endif

%install
rm -rf %{buildroot}

# Perform the Python 3 installation first, to avoid stomping over the Python 2
# /usr/bin/sip:
%if 0%{?with_python3}
pushd %{py3dir}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_datadir}/python3-sip
mv %{buildroot}%{_bindir}/sip %{buildroot}%{_bindir}/python3-sip
popd
%endif

# Python 2 installation:
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_datadir}/sip

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE* NEWS README
%{pythonsitedir}/*

%files devel
%defattr(-,root,root)
%{_bindir}/sip
%{_datadir}/sip
%{_includedir}/python%{pythonver}/*

%if 0%{?with_python3}
%files -n python3-sip
%defattr(-,root,root,-)
## this directory provided by python3-libs
%exclude %dir %{python3_sitearch}/__pycache__
%{python3_sitearch}/*

%files -n python3-sip-devel
%defattr(-,root,root,-)
# Note that the "sip" binary is invoked by name in a few places higher up
# in the KDE-Python stack; these will need changing to "python3-sip":
%{_bindir}/python3-sip
%{_datadir}/python3-sip/
%{python3_inc}/*
%endif

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.15.3-2m)
- rebuild against python-3.4

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.15.3-1m)
- update to 4.15.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.15.2-1m)
- update to 4.15.2

* Thu Jun 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.7-1m)
- update to 4.14.7

* Mon Apr 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.6-1m)
- update to 4.14.6

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.5-1m)
- update to 4.14.5

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.4-1m)
- update to 4.14.4

* Sun Feb  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.3-1m)
- update to 4.14.3

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.2-1m)
- update to 4.14.2

* Mon Nov  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14.1-1m)
- update to 4.14.1

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14-1m)
- update to 4.14

* Fri Jun 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.3-1m)
- update to 4.13.3

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.2-1m)
- update to 4.13.2

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to 4.13.1

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13-1m)
- update to 4.13

* Mon Sep 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.12.4-3m)
- modify %%files

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.12.4-2m)
- rebuild against python-3.2

* Thu Aug 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.4-1m)
- update to 4.12.4

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to 4.12.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.12.2-2m)
- rebuild for python-2.7

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to 4.12.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.12.1-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to 4.12.1

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to 4.12

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-3m)
- enable python3 bindings

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.11.2-2m)
- rebuild for new GCC 4.5

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to 4.11.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to 4.11.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.10.5-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.5-1m)
- update to 4.10.5

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to 4.10.3

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-2m)
- rebuild against qt-4.6.3-1m

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to 4.10.2

* Sat Mar 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to 4.10.1

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to 4.10 official release

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-0.20100108-5m)
- update snapshot source

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-0.20100108.4m)
- no NoSource again and again...

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-0.20100108.3m)
- NoSource again, remove SOURCES/sip* and build

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-0.20100108.2m)
- no NoSource

* Sun Jan 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.10.0-0.20100108.1m)
- update to 4.10 snapshot 20100108

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-0.20100102.1m)
- update to 4.10 snapshot 20100102

* Wed Nov 25 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.9.3-1m)
- update to 4.9.3

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to 4.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.9.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to 4.9.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8.2-2m)
- set %%global qtver 4.5.2 for automatic building of STABLE_6

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to 4.8.2

* Wed Jun 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- no NoSource
- fix momorel

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to 4.8

* Fri Jun  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-0.20090603.1m)
- update to 4.8 snapshot 20090603

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-0.20090601.1m)
- update to 4.8 snapshot 20090601

* Fri May 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-0.20090527.1m)
- update to 4.8 snapshot 20090527

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.9-4m)
- version down to 4.7.9

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-0.20090525.1m)
- update to 4.8 snapshot 20090525
- sip does not depend on qt3 any more

* Mon May 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-0.20090430.1m)
- update to 4.8 snapshot 20090430

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.9-3m)
- rebuild against rpm-4.6

* Sat Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.7.9-2m)
- rebuild against python-2.6.1-1m

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.9-1m)
- update to 4.7.9

* Sat Aug 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.7-1m)
- update to 4.7.7

* Thu May 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.6-1m)
- update to 4.7.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.4-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.4-2m)
- rebuild against gcc43

* Sun Feb 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.4-1m)
- version 4.7.4
- remove lib64 patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.3-2m)
- %%NoSource -> NoSource

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- version 4.7.3

* Fri May 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6-1m)
- version 4.6

* Fri Feb 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.2-1m)
- version 4.5.2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5-2m)
- rebuild against python-2.5

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.5-1m)
- separated from kdebindings
- based on the package in Fedora Core (sip-4.5-1.src.rpm)

* Mon Nov 06 2006 Than Ngo <than@redhat.com> 4.5-1
- 4.5

* Thu Sep 28 2006 Than Ngo <than@redhat.com> 4.4.5-3
- fix #207297, use qt qmake files

* Wed Sep 20 2006 Than Ngo <than@redhat.com> 4.4.5-2
- fix #206633, own %%_datadir/sip

* Wed Jul 19 2006 Than Ngo <than@redhat.com> 4.4.5-1
- update to 4.4.5

* Mon Jul 17 2006 Than Ngo <than@redhat.com> 4.4.3-2
- rebuild

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 4.4.3-1.1
- rebuild

* Thu Apr 27 2006 Than Ngo <than@redhat.com> 4.4.3-1
- update to 4.4.3
- built with %%{optflags}


* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 4.3.1-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 4.3.1-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Sep 12 2005 Than Ngo <than@redhat.com> 4.3.1-1
- update to 4.3.1

* Wed Mar 23 2005 Than Ngo <than@redhat.com> 4.2.1-1
- 4.2.1

* Fri Mar 04 2005 Than Ngo <than@redhat.com> 4.2-1
- 4.2

* Thu Nov 11 2004 Than Ngo <than@redhat.com> 4.1-2
- rebuild against python 2.4

* Fri Sep 24 2004 Than Ngo <than@redhat.com> 4.1-1
- update to 4.1

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu May 27 2004 Than Ngo <than@redhat.com> 3.10.2-1
- update to 3.10.2

* Fri Mar 12 2004 Than Ngo <than@redhat.com> 3.10.1-1
- update to 3.10.1

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Feb 19 2004 Than Ngo <than@redhat.com> 3.10-6 
- fix Requires issue, bug #74004

* Thu Feb 19 2004 Than Ngo <than@redhat.com> 3.10-5
- fix lib64 issue

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Feb 12 2004 Than Ngo <than@redhat.com> 3.10-3
- use new method of building SIP

* Wed Feb 11 2004 Than Ngo <than@redhat.com> 3.10-2
- rebuilt against qt 3.3.0

* Wed Feb 04 2004 Than Ngo <than@redhat.com> 3.10-1
- 3.10

* Thu Nov 27 2003 Than Ngo <than@redhat.com> 3.8-2
- rebuild against python 2.3 and Qt 3.2.3

* Fri Sep 26 2003 Harald Hoyer <harald@redhat.de> 3.8-1
- 3.8

* Mon Jul 21 2003 Than Ngo <than@redhat.com> 3.7-1
- 3.7

* Tue Jun 24 2003 Than Ngo <than@redhat.com> 3.6-3
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May  6 2003 Than Ngo <than@redhat.com> 3.6-1.1
- 3.6

* Tue Mar  4 2003 Than Ngo <than@redhat.com> 3.5.1-0.20030301.0
- snapshot 20030301, support qt 3.1.2

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Dec 11 2002 Than Ngo <than@redhat.com> 3.5-1
- 3.5 release

* Mon Nov 18 2002 Than Ngo <than@redhat.com> 3.5-0.20021114.1
- update RC, which supports qt 3.1.0
- fix dependency problem with python

* Thu Nov  7 2002 Than Ngo <than@redhat.com> 3.4-4
- update to 3.4

* Wed Aug 28 2002 Than Ngo <than@redhat.com> 3.3.2-4
- rpath issue

* Mon Aug 26 2002 Than Ngo <than@redhat.com> 3.3.2-3
- rebuild against new qt

* Sat Aug 10 2002 Elliot Lee <sopwith@redhat.com>
- rebuilt with gcc-3.2 (we hope)

* Tue Jul 23 2002 Than Ngo <than@redhat.com> 3.3.2-1
- 3.3.2 release for qt 3.0.5

* Mon Jul  1 2002 Than Ngo <than@redhat.com> 3.2.4-4
- move python modul libsip.so into sip (bug #67640)

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed May 22 2002 Harald Hoyer <harald@redhat.de>
- updated to release 3.2.4

* Thu May 02 2002 Than Ngo <than@redhat.com> 3.2-0.rc4
- 3.2rc4
- build against python 2

* Tue Apr 16 2002 Than Ngo <than@redhat.com> 3.1-2
- rebuild

* Fri Mar 29 2002 Than  Ngo <than@redhat.com> 3.1-1
- update 3.1 for qt 3.0.3

* Tue Mar  07 2002 Than Ngo <than@redhat.com> 3.0-6
- rebuild against qt3

* Fri Feb 22 2002 Than Ngo <than@redhat.com> 3.0-5
- build against python 1.5 and qt 2.3.2

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Jan 08 2002 Than Ngo <than@redhat.com> 3.0-3
- rebuild to get rid of libGL

* Mon Nov 19 2001 Than Ngo <than@redhat.com> 3.0-2
- build against qt3

* Sun Nov 18 2001 Than Ngo <than@redhat.com> 3.0-1
- update to 3.0

* Sun Nov 11 2001 Than Ngo <than@redhat.com> 3.0-0.20011110.1
- snapshot

* Tue Aug 14 2001 Than Ngo <than@redhat.com> 2.5-1
- update to 2.5
- requires python 2
- Updated URL

* Mon Jul 23 2001 Than Ngo <than@redhat.com>
- fix build dependency (bug #49698)

* Mon Jul 16 2001 Trond Eivind Glomsrod <teg@redhat.com>
- s/Copyright/License/
- Make devel subpackage depend on main

* Mon Apr 23 2001 Than Ngo <than@redhat.com>
- update to 2.4

* Wed Feb 28 2001 Tim Powers <timp@redhat.com>
- rebuilt against new libmng

* Fri Feb 23 2001 Than Ngo <than@redhat.com>
- fix to use python1.5

* Thu Feb 22 2001 Than Ngo <than@redhat.com>
- update to 2.3 release

* Fri Feb 02 2001 Than Ngo <than@redhat.com>
- rebuild in new envoroment

* Tue Dec 26 2000 Than Ngo <than@redhat.com>
- rebuilt against qt-2.2.3
- update Url

* Mon Nov 20 2000 Tim Powers <timp@redhat.com>
- rebuilt to fix bad dir perms

* Wed Nov 8 2000 Than Ngo <than@redhat.com>
- update to 2.2
- don't apply the patch, since the gcc-2.96-62 works correct

* Mon Oct 23 2000 Than Ngo <than@redhat.com>
- update to 2.1

* Thu Aug 3 2000 Than Ngo <than@redhat.de>
- add ldconfig in %post, %postun and Prereq (Bug #15136)

* Thu Jul 27 2000 Than Ngo <than@redhat.de>
- don't hardcode Qt version

* Mon Jul 25 2000 Prospector <prospector@redhat.com>
- rebuilt

* Mon Jul 17 2000 Tim Powers <timp@redhat.com>
- added defattr to both packages

* Wed Jul 12 2000 Than Ngo <than@redhat.de>
- fix to built withe gcc-2.96

* Mon Jul 03 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat May 27 2000 Ngo Than <than@redhat.de>
- update 0.12 for 7.0

* Mon May  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.11.1
- Qt 2.1.0

* Wed Feb  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.10.1
- Qt 1.45
- handle RPM_OPT_FLAGS

* Tue Dec 21 1999 Ngo Than <than@redhat.de>
- updated 0.10

* Tue Dec 14 1999 Ngo Than <than@redhat.de>
- 0.10pre5

* Sun Nov 28 1999 Ngo Than <than@redhat.de>
- Initial packaging as RPM for powertools-6.2
