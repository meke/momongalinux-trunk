%global momorel 1

Summary: A tool for memory profiling and leak detection.
Name: memprof
Version: 0.6.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.6/%{name}-%{version}.tar.bz2
NoSource: 0
Patch2: memprof-0.5.1-desktop.patch

BuildRequires: libgnomeui-devel >= 2.0
BuildRequires: binutils-devel >= 2.17.50.0.8-2
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: gtk2-devel
BuildRequires: libglade2-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/projects/memprof/

%description
Memprof is a tool for profiling memory usage and detecting memory
leaks. Memprof can be used with existing binaries without
recompilation.

%prep
%setup -q
%patch2 -p1 -b .UTF-8~

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%changelog
* Mon Jul 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-1m)
- update 0.6.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-40m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-39m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-38m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-37m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-36m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-35m)
- rebuild against rpm-4.6

* Wed Oct 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-34m)
- add "LDFLAGS=-lz" for binutils-2.18.50.0.9

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-33m)
- add patch2 fix desktop file

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-32m)
- rebuild against gcc43

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-31m)
- delete libtool library

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-30m)
- rebuild against binutils-2.17.50.0.8

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-29m)
- rebuild against binutils-2.17.50.0.3-3m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.5.1-28m)
- rebuild against expat-2.0.0-1m

* Wed Jun 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-27m)
- rebuild against binutils-2.17

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-26m)
- rebuild against binutils-2.16.94

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.1-25m)
- rebuild against openssl-0.9.8a

* Wed Feb 15 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.1-24m)
- rebuild against binutils-2.6.91.0.6

* Tue Jan 17 2006 Yohsuke ooi <meke@momonga-linux.org>
- (0.5.1-23m)
- rebuild against binutils 2.16.91.0.5

* Sun Dec 25 2005 Yohsuke ooi <meke@momonga-linux.org>
- (0.5.1-22m)
- rebuild against binutils 2.16.91.0.3

* Tue Oct 20 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.5.1-21m)
- rebuild against binutils 2.16.91.0.2

* Thu Oct 20 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5.1-20m)
- enable ia64, ppc64, alpha, sparc, mipsel

* Sun Jul 17 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-19m)
- rebuild against binutils 2.15.92.0.2.

* Mon May 23 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-18m)
- rebuild against binutils 2.16.90.0.3.

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5.1-17m)
- enable ppc.
- rename patch..

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5.1-16m)
- enable x86_64.

* Sat Feb  5 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.1-15m)
- rebuild against binutils 2.15.92.0.2.
- memprof-0.5.1-bfd_section-rename.patch is applied again.

* Thu Sep  9 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.5.1-14m)
- rebuild against binutils 2.15.90.0.3

* Sat Dec 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-13m)
- rebuild against binutils 2.14.90.0.7
- use sed lines

* Sat Oct 25 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.1-12m)
- rebuild against binutils 2.14.90.0.6.

* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.5.1-11m)
  rebuild against binutils 2.14.90.0.4

* Wed May 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.5.1-10m)
- rewind

* Thu Apr 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.1-9m)
- rebuild against for binutils 2.14.90.0.1

* Thu Apr 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-8m)
- rebuild against for binutils 2.13.90.0.20

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.1-7m)
- rebuild against for XFree86-4.3.0

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.5.1-6m)
  rebuild against openssl 0.9.7a

* Tue Mar 5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-5m)
- add URL

* Sun Feb 9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-4m)
- rebuild against for binutils 2.13.90.0.18

* Wed Jan 1 2003 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (0.5.1-3m)
- require exact version of binutils

* Wed Jan 1 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against binutils 2.13.90.0.16

* Sun Sep  1 2002 Shingo Akagaki <dora@kitty.dnsalias.ort>
- (0.5.1-1m)
- version 0.5.1

* Mon Aug 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-1m)
- version 0.5.0

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.4.1-8k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (0.4.1-6k)
- rebuild against gettext 0.10.40.

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (0.4.1-4k)
- no more ifarch alpha.

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 0.4.1

* Fri Feb 16 2001 Motonobu Ichimura <famao@kondara.org>
- rebuild against libglade

* Sun Jul 09 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.4.0

* Tue Feb 29 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Changed group Applications/System -> Development/Tools.
- Added ja.po and memprof.desktop.
- fixed requires.
  [Merged these RedHat updates.] 
- Link against libbfd and libiberty statically. (by Owen Taylor <otaylor@redhat.com>)
- fix description and summary. (by Cristian Gafton <gafton@redhat.com>)

* Sat Dec 11 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Sun Nov 21 1999 Akira Higuchi <a@kondara.org>
- don't trace stack on an alpha, because I don't know how to do it
- fixed minor bugs

* Wed Oct 27 1999 Owen Taylor <otaylor@redhat.com>
- Initial package

%files
%defattr(-,root,root)
%{_bindir}/memprof
%{_bindir}/speedprof
%{_libdir}/memprof/lib*
%{_datadir}/memprof
%{_datadir}/locale/*/*/*
%{_datadir}/applications/memprof.desktop
%{_datadir}/pixmaps/memprof.png
