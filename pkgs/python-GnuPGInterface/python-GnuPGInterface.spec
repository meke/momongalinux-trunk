%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%define pkgname GnuPGInterface

Summary:	A Python module to interface with GnuPG
Name:		python-GnuPGInterface
Version:	0.3.2
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Languages
URL:		http://py-gnupg.sourceforge.net/
Source0:	http://dl.sourceforge.net/project/py-gnupg/GnuPGInterface/%{version}/GnuPGInterface-%{version}.tar.gz
NoSource:	0
BuildRequires:	python-devel >= 2.7
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GnuPGInterface is a Python module to interface with GnuPG. It
concentrates on interacting with GnuPG via filehandles, providing
access to control GnuPG via versatile and extensible means.

%prep
%setup -q -n %{pkgname}-%{version}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

# Correct some permissions
chmod 644 ChangeLog COPYING NEWS THANKS

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc ChangeLog COPYING NEWS THANKS
%{python_sitelib}/%{pkgname}*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-1m)
- import from Fedora 13 for duplicity

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Feb 23 2009 Robert Scheck <robert@fedoraproject.org> 0.3.2-5
- Rebuild against rpm 4.6

* Fri Dec  5 2008 Jeremy Katz <katzj@redhat.com> - 0.3.2-4
- Rebuild for python 2.6

* Tue Jan 08 2008 Robert Scheck <robert@fedoraproject.org> 0.3.2-3
- Rebuild (and some minor spec file tweaks)

* Mon Sep 03 2007 Robert Scheck <robert@fedoraproject.org> 0.3.2-2
- Updated source URL to match with the guidelines (#265381)
- Use get_python_lib() macro according to the policy (#265381)

* Wed Aug 29 2007 Robert Scheck <robert@fedoraproject.org> 0.3.2-1
- Upgrade to 0.3.2
- Initial spec file for Fedora and Red Hat Enterprise Linux
