%global momorel 28

%define rstatd_ver	3.03

Summary: Displays the users logged into machines on the local network.
Name: rusers
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Daemons
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-rusers-%{version}.tar.gz
NoSource: 0
Source1: rusersd.service
# Source2: rstatd.tar.gz
Source2: ftp://sunsite.unc.edu/pub/Linux/system/network/daemons/rpc.rstatd-%{rstatd_ver}.tar.gz 
NoSource: 2
Source3: rstatd.service
Patch1: netkit-rusers-0.15-numusers.patch
Patch4: netkit-rusers-0.17-truncate.patch
Patch8: netkit-rusers-0.17-rup.patch
Patch9: netkit-rusers-0.17-rup-timeout.patch
Patch20: ftp://ftp.debian.org/debian/dists/Debian2.2r2/main/source/net/rstatd_%{rstatd_ver}-2.diff.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: rup

%description
The rusers program allows users to find out who is logged into various
machines on the local network.  The rusers command produces output
similar to who, but for the specified list of hosts or for all
machines on the local network.

Install rusers if you need to keep track of who is logged into your
local network.


%package server
Summary: Server for the rusers protocol.
Group: System Environment/Daemons
Requires(post): systemd-units
Requires(preun): systemd-units 
Requires(postun): systemd-units
Requires(post): systemd-sysv
Requires: portmap
BuildRequires: systemd-units
Provides: rpc.rstatd

%description server
The rusers program allows users to find out who is logged into various
machines on the local network.  The rusers command produces output
similar to who, but for the specified list of hosts or for all
machines on the local network. The rusers-server package contains the
server for responding to rusers requests.

Install rusers-server if you want remote users to be able to see
who is logged into your machine.


%prep
%setup -q -n netkit-rusers-%{version} -a 2
%patch1 -p1 -b .numusers
%patch4 -p1 -b .truncate
%patch8 -p1 -b .rup
%patch9 -p1 -b .rup

pushd rpc.rstatd-%{rstatd_ver}
%patch20 -p1 -b .debian
popd

%build
sh configure
perl -pi -e '
    s,^CC=.*$,CC=cc,;
    s,-O2,\$(RPM_OPT_FLAGS),;
    s,^BINDIR=.*$,BINDIR=%{_bindir},;
    s,^MANDIR=.*$,MANDIR=%{_mandir},;
    s,^SBINDIR=.*$,SBINDIR=%{_sbindir},;
    ' MCONFIG

make
make rpc.rstatd CFLAGS="%{optflags} -DNORPCLIB" \
	-C rpc.rstatd-%{rstatd_ver}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man{1,8}
mkdir -p %{buildroot}%{_unitdir}

make INSTALLROOT=%{buildroot} install
pushd rpc.rstatd-%{rstatd_ver}
  install -m 750 rpc.rstatd   %{buildroot}%{_sbindir}
  install -m 644 rpc.rstatd.8 %{buildroot}%{_mandir}/man8
popd

install %SOURCE1 %{buildroot}%{_unitdir}/rusersd.service
install %SOURCE3 %{buildroot}%{_unitdir}/rstatd.service

%clean
rm -rf %{buildroot}

%post server
if [ $1 -eq 1 ] ; then
  # Initial installation
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi
 
%preun server
if [ $1 = 0 ]; then
# Package removal, not upgrade
  /bin/systemctl --no-reload disable rstatd.service rusersd.service > /dev/null 2>&1 || :
  /bin/systemctl stop rstatd.service rusersd.service > /dev/null 2>&1 || :
fi
  
%postun server
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart rstatd.service rusersd.service >/dev/null 2>&1 || :
fi

%triggerun -- rusers-server < 0.17-27m
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply httpd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save rstatd rusersd >/dev/null 2>&1 ||:
 
# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del rstatd rusersd >/dev/null 2>&1 || :
/bin/systemctl try-restart rstatd.service rusersd.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%{_bindir}/rup
%{_bindir}/rusers
%{_mandir}/man1/*

%files server
%defattr(-,root,root)
%{_mandir}/man8/*
%attr(0750,root,bin) %{_sbindir}/rpc.rstatd
%attr(0750,root,bin) %{_sbindir}/rpc.rusersd
%{_unitdir}/rusersd.service
%{_unitdir}/rstatd.service

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-28m)
- change primary site and download URIs

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17-27m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-26m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-24m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-23m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-21m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-20m)
- update Patch4 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-19m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-18m)
- %%NoSource -> NoSource

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-17m)
- rebuild against new environment.
- applied numusers.patch, truncate.patch, rup.patch and rup-timeout.patch.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.17-16k)

* Tue Mar 20 2001 AYUHANA Tomonori <l@kondara.org>
- (0.17-15k)
- change Source0 0.17-snapshot to 0.17
- change Source2 rstatd.tar.gz to rpc.rstatd-3.03.tar.gz
- comment outed Patch0 rstatd-jbj.patch
- change make rpc.rstatd process
- add Patch2 rstatd_3.03-2.diff.gz from debian
- add %attr(0750,root,bin) for daemon
- add Provides: rup
- add Provides: rpc.rstatd

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.17-13k)
- rebuild againt rpm-3.0.5-39k

* Wed Nov 29 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to {_initscriptdir}.

* Thu Jul 20 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Sun Jul 16 2000 Matt Wilson <msw@redhat.com>
- rebuilt against new procps

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Preston Brown <pbrown@redhat.com>
- move initscripts

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.
- update to 0.17.

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages (again).

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description and summary
- man pages are compressed

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Wed Nov 10 1999 Bill Nottingham <notting@redhat.com>
- rebuild against new procps

* Wed Sep 22 1999 Jeff Johnson <jbj@redhat.com>
- rusers init script started rstatd.

* Mon Sep 20 1999 Jeff Johnson <jbj@redhat.com>
- (re-)initialize number of users (#5244).

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- initscripts check for portmapper running before starting (#2615)

* Fri Aug 27 1999 Jeff Johnson <jbj@redhat.com>
- return monitoring statistics like solaris does (#4237).

* Thu Aug 26 1999 Jeff Johnson <jbj@redhat.com>
- update to netkit-0.15.
- on startup, rpc.rstatd needs to read information twice (#3994).

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Tue Apr  6 1999 Jeff Johnson <jbj@redhat.com>
- add rpc.rstatd (#2000)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Tue May 05 1998 Cristian Gafton <gafton@redhat.com>
- added /etc/rc.d/init.d/functions to the init script

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- enhanced initscript

* Tue Oct 21 1997 Erik Troan <ewt@redhat.com>
- added init script
- users %attr
- supports chkconfig

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
