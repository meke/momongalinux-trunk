%global __os_install_post /usr/lib/rpm/momonga/brp-compress %{nil}
%global mozilla_home %{_libdir}/mozilla

Name: flashplayer-plugin
Summary: Macromedia Flash player plugin for browsers
%global momorel 1
Version: 11.2.202.378
Release: %{momorel}m%{?dist}
License: see "http://www.adobe.com/products/eulas/players/flash/"
Group: Applications/Internet
URL: http://www.adobe.com/products/flashplayer/

%ifarch %{ix86}
Source0: http://fpdownload.macromedia.com/get/flashplayer/pdc/%{version}/install_flash_player_11_linux.i386.tar.gz
%endif
%ifarch x86_64
Source0: http://fpdownload.macromedia.com/get/flashplayer/pdc/%{version}/install_flash_player_11_linux.x86_64.tar.gz
%endif
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{ix86} x86_64
Requires: glibc >= 2.2
Requires: glib, atk, gtk2, pango, cairo
Requires: freetype, fontconfig
Requires: zlib, libpng
Requires: libX11, libXext, libXmu, libXt, libXfixes, libXcursor, 
Requires: libXrender, libXau, libXdmcp, libSM, libICE
Requires: libstdc++, esound, pulseaudio
Requires: mozilla-filesystem
Requires: libflashsupport

Provides: firefox-plugin-flashplayer = %{version}-%{release}
Obsoletes: flash flashplayer
Obsoletes: firefox-plugin-flashplayer < %{version}-%{release}
Obsoletes: gnash-klash
Obsoletes: gnash-plugin
Obsoletes: swfdec-mozilla
Obsoletes: totem-mozplugin

%description
Over 414 million web users can now use Macromedia Flash Player to
seamlessly view content created with Macromedia Flash,the professional
standard for producing high-impact,vector-based Web experiences.

%prep
%ifarch %{ix86}
%setup -q -c 0
%endif

%ifarch x86_64
%setup -q -c 1
%endif

%build

%install
test "/" != "%{buildroot}"  && %{__rm} -rf %{buildroot}

# preparing...
%{__mkdir_p} %{buildroot}%{mozilla_home}/plugins

# install plugins
%{__install} -m 755 libflashplayer.so %{buildroot}%{mozilla_home}/plugins/

%clean
test "/" != "%{buildroot}"  && %{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{mozilla_home}/plugins/libflashplayer.so

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.378-1m)
- [SECURITY] CVE-2014-0531 CVE-2014-0532 CVE-2014-0533 CVE-2014-0534
- [SECURITY] CVE-2014-0535 CVE-2014-0536
- update to 11.2.202.378

* Wed May 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.359-1m)
- [SECURITY] CVE-2014-0510 CVE-2014-0516 CVE-2014-0517 CVE-2014-0518
- [SECURITY] CVE-2014-0519 CVE-2014-0520
- update to 11.2.202.359

* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.356-1m)
- [SECURITY] CVE-2014-0515
- update to 11.2.202.356

* Wed Apr  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.350-1m)
- [SECURITY] CVE-2014-0506 CVE-2014-0507 CVE-2014-0508 CVE-2014-0509
- update to 11.2.202.350

* Wed Mar 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.346-1m)
- [SECURITY] CVE-2014-0503 CVE-2014-0504
- update to 11.2.202.346

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.336-1m)
- [SECURITY] CVE-2014-0497
- update to 11.2.202.336

* Sat Jan 18 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.335-1m)
- [SECURITY] CVE-2014-0491 CVE-2014-0492
- update to 11.2.202.335

* Wed Dec 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.332-1m)
- [SECURITY] CVE-2013-5331 CVE-2013-5332
- update to 11.2.202.332

* Wed Nov 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.327-1m)
- [SECURITY] CVE-2013-5329 CVE-2013-5330
- update to 11.2.202.327

* Fri Sep 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.310-1m)
- [SECURITY] CVE-2013-3361 CVE-2013-3362 CVE-2013-3363 CVE-2013-5324
- update to 11.2.202.310

* Fri Jul 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.297-1m)
- [SECURITY] CVE-2013-3344 CVE-2013-3345 CVE-2013-3347
- update to 11.2.202.297

* Thu Jun 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.291-1m)
- [SECURITY] CVE-2013-3343
- update to 11.2.202.291

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.285-1m)
- [SECURITY] CVE-2013-2728 CVE-2013-3324 CVE-2013-3325 CVE-2013-3326
- [SECURITY] CVE-2013-3327 CVE-2013-3328 CVE-2013-3329 CVE-2013-3330
- [SECURITY] CVE-2013-3331 CVE-2013-3332 CVE-2013-3333 CVE-2013-3334
- [SECURITY] CVE-2013-3335
- update to 11.2.202.285

* Wed Apr 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.280-1m)
- [SECURITY] CVE-2013-1378 CVE-2013-1379 CVE-2013-1380 CVE-2013-2555
- update to 11.2.202.280

* Wed Mar 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.275-1m)
- [SECURITY] CVE-2013-0646 CVE-2013-0650 CVE-2013-1371 CVE-2013-1375
- update to 11.2.202.275

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.273-1m)
- [SECURITY] CVE-2013-0504 CVE-2013-0643 CVE-2013-0648
- update to 11.2.202.273

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.270-1m)
- [SECURITY] CVE-2013-0637 CVE-2013-0638 CVE-2013-0639 CVE-2013-0642
- [SECURITY] CVE-2013-0644 CVE-2013-0645 CVE-2013-0647 CVE-2013-0649
- [SECURITY] CVE-2013-1365 CVE-2013-1366 CVE-2013-1367 CVE-2013-1368
- [SECURITY] CVE-2013-1369 CVE-2013-1370 CVE-2013-1372 CVE-2013-1373
- [SECURITY] CVE-2013-1374
- update to 11.2.202.270

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.262-1m)
- [SECURITY] CVE-2013-0633 CVE-2013-0634
- update to 11.2.202.262

* Wed Jan  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.261-1m)
- [SECURITY] CVE-2013-0630
- update to 11.2.202.261

* Wed Dec 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.258-1m)
- [SECURITY] CVE-2012-5676 CVE-2012-5677 CVE-2012-5678
- update to 11.2.202.258

* Thu Nov  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.251-1m)
- [SECURITY] CVE-2012-5274 CVE-2012-5275 CVE-2012-5276 CVE-2012-5277
- [SECURITY] CVE-2012-5278 CVE-2012-5279 CVE-2012-5280
- update to 11.2.202.251

* Wed Oct 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.243-1m)
- [SECURITY] CVE-2012-5248 CVE-2012-5249 CVE-2012-5250 CVE-2012-5251
- [SECURITY] CVE-2012-5252 CVE-2012-5253 CVE-2012-5254 CVE-2012-5255
- [SECURITY] CVE-2012-5256 CVE-2012-5257 CVE-2012-5258 CVE-2012-5259
- [SECURITY] CVE-2012-5260 CVE-2012-5261 CVE-2012-5262 CVE-2012-5263
- [SECURITY] CVE-2012-5264 CVE-2012-5265 CVE-2012-5266 CVE-2012-5267
- [SECURITY] CVE-2012-5268 CVE-2012-5269 CVE-2012-5270 CVE-2012-5271
- [SECURITY] CVE-2012-5272
- update to 11.2.202.243

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.238-1m)
- [SECURITY] CVE-2012-1535
- update to 11.2.202.238

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.2.202.236-2m)
- revise Requires

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.236-1m)
- [SECURITY] CVE-2012-2034 CVE-2012-2035 CVE-2012-2036 CVE-2012-2037
- [SECURITY] CVE-2012-2038 CVE-2012-2039 CVE-2012-2040
- update to 11.2.202.236

* Sat May  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.235-1m)
- [SECURITY] CVE-2012-0779
- update to 11.2.202.235

* Thu Mar 29 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (11.2.202.228-2m)
- obsoletes totem-mozplugin

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.2.202.228-1m)
- [SECURITY] CVE-2012-0772 CVE-2012-0773
- update to 11.2.202.228

* Tue Mar  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.1.102.63-1m)
- [SECURITY] CVE-2012-0768 CVE-2012-0769
- update to 11.1.102.63

* Thu Feb 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.1.102.62-1m)
- [SECURITY] CVE-2012-0751 CVE-2012-0752 CVE-2012-0753 CVE-2012-0754
- [SECURITY] CVE-2012-0755 CVE-2012-0756 CVE-2012-0767
- update to 11.1.102.62

* Sat Nov 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (11.1.102.55-1m)
- [SECURITY] CVE-2011-2445 CVE-2011-2450 CVE-2011-2451 CVE-2011-2452 
- [SECURITY] CVE-2011-2453 CVE-2011-2454 CVE-2011-2455 CVE-2011-2456 
- [SECURITY] CVE-2011-2457 CVE-2011-2458 CVE-2011-2459 CVE-2011-2460
- update to 11.1.102.55

* Tue Oct 25 2011 Satoshi Hiruta <minakami@momonga-linux.org>
- (11.0.1.152-m1)
- update

* Sat Oct 22 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.3.183.10-1m)
- update

* Fri Aug 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.3.183.7-1m)
- bug fix release
- update to 10.3.183.7

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.3.183.5-1m)
- [SECURITY] CVE-2011-2130 CVE-2011-2134 CVE-2011-2135 CVE-2011-2136
- [SECURITY] CVE-2011-2137 CVE-2011-2138 CVE-2011-2139 CVE-2011-2140
- [SECURITY] CVE-2011-2414 CVE-2011-2415 CVE-2011-2416 CVE-2011-2417
- [SECURITY] CVE-2011-2425 CVE-2011-2424
- update to 10.3.183.5 for i686
- update to 11.0.d1.98 for x86_64

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.3.181.26-1m)
- [SECURITY] CVE-2011-2110
- update to 10.3.181.26

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.3.181.14-1m)
- [SECURITY] CVE-2011-0589 CVE-2011-0618 CVE-2011-0619 CVE-2011-0620
- [SECURITY] CVE-2011-0621 CVE-2011-0622 CVE-2011-0623 CVE-2011-0624
- [SECURITY] CVE-2011-0625 CVE-2011-0626 CVE-2011-0627
- update to 10.3.181.14

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.2.159.1-2m)
- separate momorel of each architecture
- if you update one of momorel, you must also udate the other momorel properly

* Tue May  3 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.2.159.1-1m)
- [SECURITY] CVE-2011-0611
- update to 10.2.159.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.2.153.1-2m)
- rebuild for new GCC 4.6

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.2.153.1-1m)
- [SECURITY] CVE-2011-0609
- update to 10.2.153.1

* Tue Feb 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.2.152.27-1m)
- [SECURITY] CVE-2011-0558 CVE-2011-0559 CVE-2011-0560 CVE-2011-0561 
- [SECURITY] CVE-2011-0571 CVE-2011-0572 CVE-2011-0573 CVE-2011-0574 
- [SECURITY] CVE-2011-0575 CVE-2011-0577 CVE-2011-0578 CVE-2011-0607 
- [SECURITY] CVE-2011-0608
- update to 10.2.152.27

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.1.102.64-2m)
- rebuild for new GCC 4.5

* Sat Nov 20 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.1.102.64-1m)
- [SECURITY] CVE-2010-3636 CVE-2010-3639 CVE-2010-3640 CVE-2010-3641
- [SECURITY] CVE-2010-3642 CVE-2010-3643 CVE-2010-3644 CVE-2010-3645
- [SECURITY] CVE-2010-3646 CVE-2010-3647 CVE-2010-3648 CVE-2010-3649
- [SECURITY] CVE-2010-3650 CVE-2010-3652 CVE-2010-3654
- Adobe - Security Bulletins: APSB10-26 Security Updates available for Adobe Flash Player
- http://www.adobe.com/support/security/bulletins/apsb10-26.html
- update to 10.1.102.64

* Thu Sep 30 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.1.85.3-5m)
- [SECURITY] CVE-2010-2884
- on x86_64, update to square preview 2 (version 10.2.161.23)

* Fri Sep 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.1.85.3-4m)
- [SECURITY] CVE-2010-2884
- on i686, update to 10.1.85.3

* Thu Sep 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.45.2-3m)
- on x86_64, update to square preview 1 (version 10.2.161.22)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10.0.45.2-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.1.82.76-1m)
- [SECURITY] CVE-2010-0209 CVE-2010-2188 CVE-2010-2213 CVE-2010-2214
- [SECURITY] CVE-2010-2215 CVE-2010-2216
- update to 10.1.82.76

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.1.53.64-4m)
- set Obsoletes: gnash-klash
- set Obsoletes: gnash-plugin
- set Obsoletes: swfdec-mozilla

* Mon Jul 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.1.53.64-3m)
- [SECURITY] CVE-2009-3793 CVE-2010-1297 CVE-2010-2160 CVE-2010-2161
- [SECURITY] CVE-2010-2162 CVE-2010-2163 CVE-2010-2164 CVE-2010-2165
- [SECURITY] CVE-2010-2166 CVE-2010-2167 CVE-2010-2169 CVE-2010-2170
- [SECURITY] CVE-2010-2171 CVE-2010-2173 CVE-2010-2174 CVE-2010-2175
- [SECURITY] CVE-2010-2176 CVE-2010-2177 CVE-2010-2178 CVE-2010-2179
- [SECURITY] CVE-2010-2180 CVE-2010-2181 CVE-2010-2182 CVE-2010-2183
- [SECURITY] CVE-2010-2184 CVE-2010-2185 CVE-2010-2186 CVE-2010-2187
- [SECURITY] CVE-2010-2188 CVE-2010-2189
- update to 10.1.53.64 on i686 only

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.45.2-2m)
- modify __os_install_post for new momonga-rpmmacros

* Tue Feb 16 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (10.0.45.2-1m)
- update to 10.0.45.2
- [SECURITY] CVE-2010-0186, CVE-2010-0187
  - Adobe - Security Bulletins: APSB10-06 Security update available for Adobe Flash Player
 - http://www.adobe.com/support/security/bulletins/apsb10-06.html

* Wed Dec  9 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (10.0.42.34-1m)
- update to 10.0.42.34
- [SECURITY] CVE-2009-3794, CVE-2009-3796, CVE-2009-3797, CVE-2009-3798, CVE-2009-3799, CVE-2009-3800, CVE-2009-3951
  - Adobe - Security Bulletin APSB09-19 Security Advisory for Adobe Flash Player
  - http://www.adobe.com/support/security/bulletins/apsb09-19.html

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.32.18-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (10.0.32.18-1m)
- [SECURITY] CVE-2009-1862, CVE-2009-0901, CVE-2009-2395, CVE-2009-2493, CVE-2009-1863, CVE-2009-1864, CVE-2009-1865, CVE-2009-1866, CVE-2009-1867, CVE-2009-1868, CVE-2009-1869, CVE-2009-1870
 - Adobe - Security Bulletins: APSB09-10 Security Updates available for Adobe Flash Player
 - http://www.adobe.com/support/security/bulletins/apsb09-10.html
- update to 10.0.32.18

* Wed Feb 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.0.22.87-1m)
- [SECURITY] CVE-2009-0519, CVE-2009-0520, CVE-2009-0522, CVE-2009-0114, CVE-2009-0521
  - Adobe - Security Advisories : APSB09-01 - Flash Player update available to address security vulnerabilities
  - http://www.adobe.com/support/security/bulletins/apsb09-01.html
- update to 10.0.22.87

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.15.3-2m)
- rebuild against rpm-4.6

* Thu Dec 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.15.3-1m)
- [SECURITY] CVE-2008-5499
- update to 10.0.15.3

* Wed Nov 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.12.36-4m)
- do not provide libflashsupport.so
  from now on, libflashsupport.so is provided by libflashsupport

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.0.12.36-3m)
- add BuildRequires: openssl-devel

* Mon Nov 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.0.12.36-2m)
- add x86_64 support

* Thu Oct 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0.12.36-1m)
- reassign version 10.0.12.36

* Fri Oct 17 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.0.12-1m)
- [SECURITY] CVE-2007-6243 CVE-2008-3873 CVE-2007-4324 CVE-2008-4401 CVE-2008-4503
- [SECURITY] http://www.adobe.com/support/security/bulletins/apsb08-18.html
- update to flash player 10 r12

* Sat Aug  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.0.0_d525-1m)
- update to flash player 10 beta 2

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.0.124.0-2m)
- rebuild against openssl-0.9.8h

* Thu Apr 10 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.0.124.0-1m)
- up to 9.0.124.0
- [SECURITY] CVE-2007-0071 CVE-2007-5275 CVE-2007-6019 CVE-2007-6243
             CVE-2007-6637 CVE-2008-1654 CVE-2008-1655

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.0.115.0-3m)
- rebuild against gcc43

* Sun Mar  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.0.115.0-2m)
- change BR from pulseaudio-devel to pulseaudio-libs-devel

* Wed Dec  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (9.0.115.0-1m)
- update to 9.0.115.0

* Thu Jul 12 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (9.0.48.0-1m)
- [SECURITY] CVE-2007-3456 CVE-2007-3457 CVE-2007-2022
- update to 9.0.48.0
- install_flash_player_9_linux/Readme.txt missing?

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.0.31.0-2m)
- Requires: freetype2 -> freetype

* Wed Mar 21 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.0.31.0-1m)
- up to 9.0.31.0
- good-bye standalone player, gflashplayer
- add flashsupport for audio output to esound
- clean up spec file

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.0.21.78-3m)
- remove Requires: firefox

* Sun Dec 10 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.0.21.78-2m)
- good bye mozilla

* Tue Nov 21 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (9.0.21.78-1m)
- update: FlashPlayer 9 beta2

* Thu Oct 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (9.0.21.55-1m)
- FlashPlayer 9 beta released.

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.68.0-1m)
- flashplayer-plugin version up to 7,0,68,0
  [SECURITY] CVE-2006-3014, CVE-2006-3311, CVE-2006-3587, CVE-2006-3588, CVE-2006-4640
  http://www.adobe.com/support/security/bulletins/apsb06-11.html
- remove Application from Categories of desktop file

* Wed Mar 15 2006 Yonekawa Susumu <yonekawas@mmg.roka.jp>
- (7.0.63.0-1m)
- flashplayer-plugin version up to 7,0,63,0
  [SECURITY] CAN-2006-0024
  http://www.macromedia.com/devnet/security/security_zone/apsb06-03.html
- remove unused definitions: mozilla_version, firefox_version

* Wed Nov 16 2005 Yonekawa Susumu <yonekawa@mmg.roka.jp>
- (7.0.61.0-1m)
- flashplayer-plugin version up to 7,0,61,0
  [SECURITY] CAN-2005-2628
  http://www.macromedia.com/devnet/security/security_zone/mpsb05-07.html

* Thu Mar 24 2005 Toru Hoshina <t@momonga-linux.org>
- (7.0.25.0-10m)
- rebuild against firefox 1.0.1.
- rebuild against mozilla-1.7.6
- install to {_libdir}/mozilla/plugins and {_libdir}/firefox/plugins.

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (7.0.25.0-9m)
- rebuild against firefox 1.0.1.

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.25.0-8m)
- move gflashplayer.desktop to %%{_datadir}/applications/
- BuildRequires: desktop-file-utils
- Requires: XFree86-libs -> xorg-x11-libs

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.0.25.0-7m)
- rebuild against mozilla-1.7.5

* Mon Sep 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (7.0.25.0-6m)
- add firefox-plugin-flashplayer

* Thu Sep 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.0.25.0-5m)
- rebuild agaist mozilla-1.7.3

* Sat Aug  7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.0.25.0-4m)
- rebuild agaist mozilla-1.7.2

* Tue Jul  6 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.0.25.0-3m)
- rebuild agaist mozilla-1.7
- 2m's changelog is not found... why?

* Thu May 27 2004 Yonekawa Susumu <yonekawa@mmg.roka.jp>
- (7.0.25.0-1m)
- flashplayer-plugin version up to 7,0,25,0

* Sat Apr  3 2004 Toru Hoshina <t@momonga-linux.org>
- (6.0.81.0-2m)
- revised spec for rpm 4.2.1.

* Tue Mar 30 2004 Yonekawa Susumu <yonekawa@mmg.roka.jp>
- (6.0.81.0-1m)
- flashplayer-plugin version up to 6,0,81,0
- standalone flashplayer version up to 6,0,79,0
  this is release version (non-beta)
- revise URL

* Sat Jan 17 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (6.0.79.0-0.920031017.3m)
- rebuild against mozilla-1.6

* Fri Dec  5 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.0.79.0-0.920031017.2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Oct 17 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0.79.0-0.920031017.1m)
- rebuild against mozilla-1.5
- changed version and release control.

* Wed Jul  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.920030702m)
- rebuild against for mozilla-1.4

* Fri May 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.920030516m)
- rebuild against for mozilla-1.3.1

* Fri Mar 14 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.920030314)
- fix plugin install directory for mozilla-1.3 (T_T)

* Thu Mar 13 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.920030323)
- fix plugin install directory for mozilla-1.3b

* Thu Mar  6 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.9m)
- flashplayer-plugin version up to 6,0,79,0
  (thanks Yonekawa Susumu san)

* Sun Dec 15 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.8m)
- flashplayer-plugin version up to 6,0,69,0
- modified tar ball URI.

* Sun Dec  8 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.7m)
- fix file list. (flash.{keys,mime} -> swf.{keys,mime})

* Sat Dec  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (6.0-0.6m)
- cleanup specfile

* Tue Dec 03 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (6.0-0.5m)
- cancel permission modification
- define __os_install_post to skip stripping (and find requires)
- Argh! The archive seems remade again! mime-info/flash.* -> mime-info/swf.*

* Tue Nov 12 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.4m)
- modified working directory path. (Macromdia remade archive)

* Sun Nov  3 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.3m)
- modified post section.

* Sun Nov  3 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.2m)
- modified permission for gflashplayer. (thanks tamo san)

* Thu Oct 31 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (6.0-0.1m)
- import bata release.
- added standalone player (but, mada matomo jya nai arune...)
- I renamed package from "flash". ("FLASHPLAYER" IS NOT "FLASH".)

* Mon Oct 28 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (5.0r51-1m)

* Sun Aug 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.0r50-2m)
- revise %clean

* Sat Aug 10 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (5.0r50-1m)
- update to 5.0r50

* Mon Apr  1 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (5.0r48-2k)
- update to 5.0r48

* Thu Dec 20 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (5.0r47-2k)
- first release
