%global momorel 5
%global libopensync_version 0.39

Summary:     Meta package for OpenSync
Name:        opensync-suite
Version:     %{libopensync_version}
Release:     %{momorel}m%{?dist}
License:     GPLv2+ and LGPLv2+
# same as libopensync-*
Group:       Applications/Productivity
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:    libopensync
Requires:    osynctool
# Requires:    multisync-gui

# plugins
# Requires:    libopensync-plugin-evolution2
Requires:    libopensync-plugin-file
# Requires:    libopensync-plugin-gnokii
# Requires:    libopensync-plugin-google-calendar
# Requires:    libopensync-plugin-gpe
# Requires:    libopensync-plugin-irmc
# Requires:    libopensync-plugin-kdepim
# Requires:    libopensync-plugin-moto
# Requires:    libopensync-plugin-opie
# Requires:    libopensync-plugin-palm
# Requires:    libopensync-plugin-python
Requires:    libopensync-plugin-syncml
Requires:    libopensync-plugin-vformat
Requires:    libopensync-plugin-xmlformat

%description
This is meta package for OpenSync.

%files

%changelog
* Sun Jul 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-5m)
- remove Requires: libopensync-plugin-evolution2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-2m)
- full rebuild for mo7 release

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-1m)
- version 0.39
- add Requires: libopensync-plugin-xmlformat
- change Requires: from msynctool to osynctool
- remove Requires: SKIPed plugins

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-1m)
- version 0.38

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-0.20081004.3m)
- rebuild against rpm-4.6

* Sat Nov  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-0.20081004.2m)
- revive Requires: multisync-gui

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-0.20081004.1m)
- version 0.38 (svn snapshot)

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.36-3m)
- remove Requires: libopensync-plugin-kdepim for the moment

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.36-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.36-1m)
- version 0.36
- remove Requires: multisync-gui
- remove Requires: libopensync-plugin-sunbird
- add Requires: libopensync-plugin-vformat

* Tue Jun 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-1m)
- initial package for OpenSync
