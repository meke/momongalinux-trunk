%global momorel 5
%global abi_ver 1.9.1

# Generated from gruff-0.3.4.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname gruff
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}
%define installroot %{buildroot}%{geminstdir}

Summary:	Beautiful graphs for one or multiple datasets
Name:		rubygem-%{gemname}
Version:	0.3.6
Release:	%{momorel}m%{?dist}
Group:		Development/Languages
License:	MIT/X
URL:		http://nubyonrails.com/pages/gruff
Source0:	http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	ruby(abi) = %{abi_ver}
Requires:	rubygems
Requires:	rubygem(hoe) >= 1.7.0
Requires:	rubygem(rmagick)
BuildRequires:	rubygems
BuildArch:	noarch
Provides:	rubygem(%{gemname}) = %{version}

%description
Beautiful graphs for one or multiple datasets. Can be used on websites or in
documents.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

chmod +x %{installroot}/test/test_spider.rb 
chmod +x %{installroot}/test/test_pie.rb 
chmod +x %{installroot}/test/test_photo.rb 
chmod +x %{installroot}/test/test_base.rb 
chmod +x %{installroot}/test/test_stacked_area.rb 
chmod +x %{installroot}/test/test_stacked_bar.rb 
chmod +x %{installroot}/test/test_net.rb 
chmod +x %{installroot}/test/test_sidestacked_bar.rb 
chmod +x %{installroot}/test/test_area.rb 
chmod +x %{installroot}/test/test_bar.rb 
chmod +x %{installroot}/test/test_line.rb 
chmod +x %{installroot}/test/test_scene.rb

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.6-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.6-1m)
- update 0.3.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-2m)
- Requires: rubygem(rmagick)

* Thu Aug 13 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-1m)
- import from Fedora to Momonga

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec 17 2008 Darryl Pierce <dpierce@redhat.com> - 0.3.4-3
- Added a requirement for ruby-RMagick.

* Mon Nov 10 2008 Darryl Pierce <dpierce@redhat.com> - 0.3.4-2
- Fixed the license for package review.

* Mon Oct 20 2008 Darryl Pierce <dpierce@redhat.com> - 0.3.4-1
- Initial packaging for review.
