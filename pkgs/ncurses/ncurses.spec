%global momorel 21
%global date 20140524

%{?!with_gpm:		%global with_gpm 0}
%{?!without_xterm_new:	%global without_xterm_new 0}

Summary: A CRT screen handling and optimization package
Name: ncurses
Version: 5.9
%define includedirw %{_includedir}/ncursesw
Release: %{momorel}m%{?dist}
License: MIT
Group: System Environment/Libraries
URL: http://www.gnu.org/ncurses/
#Source0: ftp://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{version}.tar.gz
Source0: ftp://invisible-island.net/ncurses/current/ncurses-%{version}-%{date}.tgz
NoSource: 0
Source1: ncurses-linux
Source2: ncurses-linux-m
Source3: ncurses-resetall.sh
Source100: terminfo.jfbterm
Source101: terminfo.kon
Source102: terminfo.mlterm

# Patches:
# ftp://invisible-island.net/ncurses/5.9/
# Patch30: ncurses-5.9-20130504-patch.sh.bz2

Patch90: ncurses-5.7-20090124-config.patch
Patch91: ncurses-5.6-20111009-libs.patch
Patch92: ncurses-5.9-20110909-urxvt.patch
Patch93: ncurses-5.9-20110716-kbs.patch

Patch102: ncurses-5.4-mlterm.patch
Patch103: ncurses-5.8-lib64.patch

# strlen(0) get SIGSEGV
Patch200: ncurses-5.9-strlen.patch

BuildRequires: coreutils, sharutils, pkgconfig
BuildRequires: gpm-devel
Requires: %{name}-libs = %{version}-%{release}
# BuildRequires: libtermcap >= 2.0.8-44
Obsoletes: libtermcap-devel < 2.0.8-48
Requires(pre,preun): coreutils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The curses library routines are a terminal-independent method of
updating character screens with reasonable optimization.  The ncurses
(new curses) library is a freely distributable replacement for the
discontinued 4.4 BSD classic curses library.

This package contains support utilities, including a terminfo compiler
tic, a decompiler infocmp, clear, tput, tset, and a termcap conversion
tool captoinfo.

%package libs
Summary: Ncurses libraries
Group: System Environment/Libraries
Requires: %{name}-base = %{version}-%{release}
# libs introduced in 5.6-13
Obsoletes: ncurses < 5.6-9
Conflicts: ncurses < 5.6-9
Obsoletes: libtermcap 

%description libs
The curses library routines are a terminal-independent method of
updating character screens with reasonable optimization.  The ncurses
(new curses) library is a freely distributable replacement for the
discontinued 4.4 BSD classic curses library.

This package contains the ncurses libraries.

%package base
Summary: Descriptions of common terminals
Group: System Environment/Base
Obsoletes: termcap 
# base introduced in 5.6-13
Conflicts: ncurses < 5.6-9

%description base
This package contains descriptions of common terminals. Other terminal
descriptions are included in the ncurses-term package.

%package term
Summary: Terminal descriptions
Group: System Environment/Base
Requires: %{name}-base = %{version}-%{release}

%description term
This package contains additional terminal descriptions not found in
the ncurses-base package.

%package devel
Summary: The development files for applications that use ncurses.
Group: Development/Libraries
Requires: ncurses = %{version}-%{release}
Requires: pkgconfig
Provides: %{name}-c++-devel
Obsoletes: %{name}-c++-devel

%description devel
The header files and libraries for developing applications that use
the ncurses CRT screen handling and optimization package.

Install the ncurses-devel package if you want to develop applications
which will use ncurses.

Use the following compiler flags to build against the ncurses library:

%package static
Summary: Static libraries for the ncurses library
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The ncurses-static package includes static libraries of the ncurses library.

%prep
# workaround for a possible "Permission denied" error.
find -type d -exec chmod u+x {} \;
#%%setup -q
%setup -q -n %{name}-%{version}-%{date}

#%%patch30 -p1 

#%%patch90 -p1 -b .config
%patch91 -p1 -b .libs
%patch92 -p1 -b .urxvt
%patch93 -p1 -b .kbs

#%%patch102 -p1 -b .mlterm
%if %{_lib}=="lib64"
%patch103 -p1 -b .lib64
%endif

%patch200 -p1 -b .strlen

# this will be in documentation, drop executable bits
find test -type f | xargs chmod 644

# cat %{SOURCE100} %{SOURCE101} %{SOURCE102} >> misc/terminfo.src

%build
CFLAGS="%{optflags} -DPURE_TERMINFO -DSVR4_CURSES"
%define optflags $CFLAGS

%if %{with_gpm}
WITH_GPM="--with-gpm"
%endif

%if %{without_xterm_new}
WITHOUT_XTERM_NEW="--without-xterm-new"
%endif

%define rootdatadir /lib
%define ncurses_options \\\
    --with-shared --without-ada --with-ospeed=unsigned \\\
    --enable-hard-tabs --enable-xmc-glitch --enable-colorfgbg \\\
    --with-terminfo-dirs=%{_sysconfdir}/terminfo:%{_datadir}/terminfo:%{rootdatadir}/terminfo \\\
    --includedir=%{_includedir}/ncursesw \\\
    --enable-overwrite \\\
    --with-pkg-config-libdir=%{_libdir}/pkgconfig \\\
    --enable-pc-files \\\
    --with-termlib=tinfo \\\
    --with-chtype=long \\\
    --program-prefix=""

export PKG_CONFIG_LIBDIR=%{_libdir}/pkgconfig

mkdir narrowc widec
cd narrowc
ln -s ../configure .

%configure %{ncurses_options} --with-ticlib
## make -j8 and make -j16 fail (5.7-6m)
# make %%{?_smp_mflags} libs
# make %%{?_smp_mflags} -C progs
make libs
make -C progs


cd ../widec
ln -s ../configure .
%configure %{ncurses_options} --enable-widec --without-progs
## make -j8 and make -j16 fail (5.7-6m)
# make %%{?_smp_mflags} libs
make libs

cd ..

%install
rm -rf --preserve-root %{buildroot}

make -C narrowc DESTDIR=%{buildroot} install.{libs,progs,data}  transform='s,x,x,'
make -C widec DESTDIR=%{buildroot} install.{libs,includes,man}  transform='s,x,x,'

chmod 755 %{buildroot}%{_libdir}/lib*.so.*.*
chmod 644 %{buildroot}%{_libdir}/lib*.a

mkdir -p %{buildroot}{%{rootdatadir},%{_sysconfdir}}/terminfo

# move few basic terminfo entries to /lib
baseterms=
for termname in \
        ansi dumb linux vt100 vt100-nav vt102 vt220 vt52
do
    for t in $(find %{buildroot}%{_datadir}/terminfo \
        -samefile %{buildroot}%{_datadir}/terminfo/${termname::1}/$termname)
    do
        baseterms="$baseterms $(basename $t)"
    done
done
for termname in $baseterms; do
    termpath=terminfo/${termname::1}/$termname
    mkdir %{buildroot}%{rootdatadir}/terminfo/${termname::1} &> /dev/null || :
    mv %{buildroot}%{_datadir}/$termpath %{buildroot}%{rootdatadir}/$termpath
    ln -s $(dirname %{_datadir}/$termpath | \
        sed 's,\(^/\|\)[^/][^/]*,..,g')%{rootdatadir}/$termpath \
        %{buildroot}%{_datadir}/$termpath
done

# prepare -base and -term file lists
for termname in \
    Eterm cons25 cygwin gnome* hurd jfbterm mach* mrxvt nsterm \
    putty* pcansi rxvt rxvt-\* screen screen-\* screen.linux screen.xterm* \
    sun teraterm wsvt25* xfce xterm xterm-\* xfce
do
    for i in %{buildroot}%{_datadir}/terminfo/?/$termname; do
        for t in $(find %{buildroot}%{_datadir}/terminfo -samefile $i); do
            baseterms="$baseterms $(basename $t)"
        done
    done
done 2> /dev/null
for t in $baseterms; do
    echo "%dir %{_datadir}/terminfo/${t::1}"
    echo %{_datadir}/terminfo/${t::1}/$t
done 2> /dev/null | sort -u > terms.base
find %{buildroot}%{_datadir}/terminfo \! -type d | \
    sed "s|^%{buildroot}||" | while read t
do
    echo "%dir $(dirname $t)"
    echo $t
done 2> /dev/null | sort -u | comm -2 -3 - terms.base > terms.term

ln -sf ncurses/{curses,unctrl,eti,form,menu,ncurses,panel,term}.h \
    %{buildroot}%{_includedir}

# can't replace directory with symlink (rpm bug), symlink all headers
mkdir %{buildroot}%{_includedir}/ncurses
for l in %{buildroot}%{_includedir}/ncursesw/*.h; do
    ln -s ../ncursesw/$(basename $l) %{buildroot}%{_includedir}/ncurses
done

# don't require -ltinfo when linking with --no-add-needed
for l in %{buildroot}%{_libdir}/libncurses{,w}.so; do
    soname=$(basename $(readlink $l))
    rm -f $l
    echo "INPUT($soname -ltinfo)" > $l
done

rm -f %{buildroot}%{_libdir}/libcurses.so
echo "INPUT(-lncurses)" > %{buildroot}%{_libdir}/libcurses.so
echo "INPUT(-lncursesw)" > %{buildroot}%{_libdir}/libcursesw.so

echo "INPUT(-ltinfo)" > %{buildroot}%{_libdir}/libtermcap.so

rm -f %{buildroot}%{_libdir}/terminfo
rm -f %{buildroot}%{_libdir}/pkgconfig/{*_g,ncurses++*}.pc

# fix configuration script
sed --in-place s,"tinfow","tinfo",g  %{buildroot}%{_bindir}/ncursesw5-config

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc ANNOUNCE AUTHORS README TO-DO
%{_bindir}/[cirt]*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man7/*

%files libs
%defattr(-,root,root)
%{_libdir}/lib*.so.*
%{_libdir}/lib*.so.*

%files base -f terms.base
%defattr(-,root,root)
%dir %{_sysconfdir}/terminfo
%{rootdatadir}/terminfo
%{_datadir}/tabset
%dir %{_datadir}/terminfo

%files term -f terms.term
%defattr(-,root,root)

%files devel
%defattr(-,root,root)
# FIXME: Can't find any file error...
#%doc test
%doc doc/html/hackguide.html
%doc doc/html/ncurses-intro.html
%{_bindir}/ncurses*-config
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/ncurses
%{_includedir}/ncurses/*.h
%dir %{_includedir}/ncursesw
%{_includedir}/ncursesw/*.h
%{_includedir}/curses.h
%{_includedir}/ncurses.h
%{_includedir}/eti.h
%{_includedir}/form.h
%{_includedir}/menu.h
%{_includedir}/panel.h
%{_includedir}/term.h
%{_includedir}/unctrl.h
%{_mandir}/man3/*

%doc c++/NEWS c++/PROBLEMS c++/README*

%files static
%defattr(-,root,root)
%{_libdir}/lib*.a

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9-21m)
- update 20140524

* Fri Apr 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9-20m)
- update 20140329

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9-19m)
- update 20140222

* Thu Jan 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9-18m)
- update 20131221

* Wed May  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-17m)
- update 20130504

* Sun Jan 27 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-16m)
- update 20130126

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-15m)
- update 20120630

* Fri Mar 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-14m)
- update 20120317

* Wed Jan  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-13m)
- update 20111231

* Sat Dec 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-12m)
- update 20111211

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-11m)
- update 20111022

* Sun Oct 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9-10m)
- add patch200 (work around strlen(0))
-- [BTS-401]

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9-9m)
- correct pkgconfig directory

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-8m)
- update 20111008

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-7m)
- update 20110903

* Fri Sep  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.9-6m)
- update urxvt.patch for new rxvt-unicode

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-5m)
- update 20110716

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9-4m)
- revise workaround

* Wed Jun 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9-3m)
- fix a possible build failure, again

* Wed Jun  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9-2m)
- fix a possible build failure

* Thu Apr 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9-1m)
- update ncurses-5.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8-3m)
- rebuild for new GCC 4.6

* Mon Mar  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8-2m)
- update lib64 patch

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8-1m)
- update ncurses-5.8

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-23m)
- update 20110115

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-22m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-21m)
- update 20101113

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.7-20m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-19m)
- fix build error
-- update ncurses-5.7-lib64.patch

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-18m)
- update 20100808

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-17m)
- enable pkgconfig

* Mon Jul  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-16m)
- update 20100703

* Wed May 26 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.7-15m)
- update 20100522

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-14m)
- do not require -ltinfo when linking with --no-add-needed

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.7-13m)
- remake lib64 patch

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.7-12m)
- update 5.7-20100403

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-11m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-9m)
- fix segfault issue, 
 see http://developer.momonga-linux.org/kagemai/guest.cgi?action=view_report&id=245&project=momongaja

* Fri Aug 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-8m)
- downgrade to 5.7-20090425
-- alsamixier, alsaconf, dialog and others do not work with new ncurses

* Mon Jun  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-7m)
- update 5.7-20090607

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.7-6m)
- disable parallel build to enable build

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-5m)
- update 5.7-20090425

* Sun Mar 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-4m)
- update 5.7-20090314

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-3m)
- rebuild against rpm-4.6

* Sat Dec 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-2m)
- update 5.7-20081220

* Thu Nov  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-1m)
- update 5.7

* Sat Oct 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.6-20m)
- update 20081012

* Thu Sep 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.6-19m)
- correct filenames

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.6-18m)
- update 20080726

* Tue Jul 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.6-17m)
- update 20080713

* Tue Jul  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-16m)
- update 20080705

* Wed Jun 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-15m)
- update 20080607

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-14m)
- update 20080503

* Wed Apr 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-13m)
- update 20080419

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.6-12m)
- rebuild against gcc43

* Sun Mar  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-11m)
- Obsoletes libtermcap, termcap

* Wed Feb  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-10m)
- update 20080112

* Wed Feb  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-10m)
- update 20080112

* Mon Oct 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-9m)
- update 20070812

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-8m)
- modify spec

* Mon Mar 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-7m)
- modify spec

* Mon Mar 12 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.6-6m)
- modify spec

* Fri Mar  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-5m)
- update 20070303

* Sun Mar  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-4m)
- update 20070217

* Wed Feb 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-3m)
- add transform='s,x,x,'

* Tue Feb 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.6-2m)
- remove static libraries from devel package

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6-1m)
- update 20070210
- add ncurses-static

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (5.5-4m)
- update 20061209

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.5-3m)
- delete ncurses-c++-devel package

* Mon May 29 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (5.5-2m)
- revise lib64 patch and Source0 url

* Sun May 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.5-1m)
- version up ncurses-5.5

* Sun May 28 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (5.4-8m)
- add lib64 patch

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4-7m)
- revise %%files for rpm-4.4.2

* Sat Feb 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.4-6m)
- change Source0 to domestic mirror site

* Sat Apr 23 2005 TAKAHASHI Tamotsu <tamo>
- (5.4-5m)
- Patch102: delete mlterm terminfo
- Source102: add mlterm's terminfo

* Mon Mar 28 2005 TAKAHASHI Tamotsu <tamo>
- (5.4-4m)
- update to 5.4-20050327 to fix a minor bug
 introduced in 20050320.

* Sun Mar 27 2005 TAKAHASHI Tamotsu <tamo>
- (5.4-3m)
- update to 5.4-20050326 to fix another widec problem

* Sat Mar 26 2005 TAKAHASHI Tamotsu <tamo>
- (5.4-2m)
- update to 5.4-20050320 to fix widec problems
- re-add terminfo data for jfbterm and kon
 (Shall I remove terminfo.kon?)
- update terminfo.jfbterm to 0.4.7
- remove man2 with wildcard

* Tue Mar  8 2005 Toru Hoshina <t@momonga-linux.org>
- (5.4-1m)
- sync with FC3(5.4-13).

* Wed Sep 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (5.3-10m)
- use %%{_lib} instead of lib

* Sat May  1 2004 Toru Hoshina <t@momonga-linux.org>
- (5.3-9m)
- import from FC1 for anaconda.

* Tue Mar 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (5.2-23m)
- move libcurses.so to /lib/

* Sun Mar 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (5.2-22m)
- move libncurses.so* to /lib/ for zsh

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (5.2-21m)
- revised spec for rpm 4.2.

* Wed Mar 10 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (5.2-20m)
- Change BuildRequest coreutils

* Sun Nov  9 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (5.2-19m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- use %%{momorel}

* Fri Nov 22 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- change ftp site ([Momonga-devel.ja:00825] thanks, TNF) 
  do not bump Release 

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (5.2-18k)
- No docs could be excutable :-p

* Tue May 22 2001 Toru Hoshina <toru@df-usa.com>
- (5.2-14k)

* Tue May 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.2-12k)
- added terminfo.kon.alpha for alhpa architecture, because not installation

* Sun May  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.2-10k)
- modified terminfo.kon same as kterm

* Sun May  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.2-8k)
- modify terminfo.kon

* Sun May  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.2-6k)
- add two terminfo files, i.e. jfbterm and kon

* Fri Dec 15 2000 AYUHANA Tomonori <l@kondara.org>
- (5.2-2k)
- backport 5.2-3k for 2.0

* Thu Dec  7 2000 AYUHANA Tomonori <l@kondara.org>
- (5.2-3k)
- version 5.2 (ported from RawHide 5.2-2)
- comment out strip

* Fri Nov 24 2000 Toru Hoshina <toru@df-usa.com>
- use egcs instead of gcc, especially on Alpha :-P

* Tue Nov 14 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- added suppress warning patch

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge from redhat-6.2 (5.0-11).

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Fri Feb 18 2000 Preston Brown <pbrown@redhat.com>
- xterm terminfo entries from XFree86 3.3.6
- final round of xterm fixes, follow debian policy.

* Sat Feb  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- strip libraries

* Thu Feb  3 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- More xterm fixes (Bug #9087)

* Thu Jan 27 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- More xterm fixes from Hans de Goede (Bug #8633)

* Sat Jan 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- remove some broken symlinks (leftovers from libncurses.so.5)
- Use %configure (Bug #8484)

* Tue Jan 11 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Add xterm patch from Hans de Goede <hans@highrise.nl>
- Patch 20000108, this fixes a problem with a header file.

* Wed Jan  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Add 20000101 patch, hopefully finally fixing the xterm description

* Wed Dec 22 1999 Cristian Gafton <gafton@redhat.com>
- revert to the old major number - because the ABI is not changed (and we
  should be handling the changes via symbol versioning anyway)

* Fri Nov 12 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix a typo in spec
- Add the 19991006 patch, fixing some C++ STL compatibility problems.
- get rid of profiling and debugging versions - we need to save space...

* Thu Nov  4 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 5.0
- some spec cleanups to make updating easier
- add links *.so.5 to *.so.4 - they are fully binary compatible.
  (Why did they change the invocation number???)

* Wed Sep 22 1999 Cristian Gafton <gafton@redhat.com>
- make clean in the test dir - don't ship any binaries at all.

* Mon Sep 13 1999 Preston Brown <pbrown@redhat.com>
- fixed stripping of test programs.

* Sun Aug 29 1999 Preston Brown <pbrown@redhat.com>
- removed 'flash' capability for xterm; see bug #2820 for details.

* Fri Aug 27 1999 Cristian Gafton <gafton@redhat.com>
- add the resetall script from Marc Merlin <marc@merlins.org>

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- added iris-ansi-net as alias for iris-ansi (bug #2561)

* Fri Jul 30 1999 Michael K. Johnson <johnsonm@redhat.com>
- added ncurses-intro.hmtl and hackguide.html to -devel package [bug #3929]

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- make sure ALL binaries are stripped (incl. test binaries)

* Thu Mar 25 1999 Preston Brown <pbrown@redhat.com>
- made xterm terminfo stuff MUCH better.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 16)

* Sat Mar 13 1999 Cristian Gafton <gafton@redhat.com>
- fixed header for C++ compiles

* Fri Mar 12 1999 Jeff Johnson <jbj@redhat.com>
- add terminfo entries for linux/linux-m on sparc (obsolete termfile_sparc).

* Thu Feb 18 1999 Cristian Gafton <gafton@redhat.com>
- updated patchset from original site

* Thu Dec 03 1998 Cristian Gafton <gafton@redhat.com>
- don't build the C++ demo code
- update patch set to the current as of today (redid all the individual
  patches in a single one)

* Wed Oct 14 1998 Cristian Gafton <gafton@redhat.com>
- make sure to strip the binaries

* Wed Sep 23 1998 Cristian Gafton <gafton@redhat.com>
- added another zillion of patches. The spec file *is* ugly
- defattr

* Mon Jul 20 1998 Cristian Gafton <gafton@redhat.com>
- added lots of patches. This spec file is starting to look ugly

* Wed Jul 01 1998 Alan Cox <alan@redhat.com>
- Fix setuid trusting. Open termcap/info files as the real user.

* Wed May 06 1998 Cristian Gafton <gafton@redhat.com>
- added terminfo entry for the poor guys using lat1 and/or lat-2 on their
  consoles... Enjoy linux-lat ! Thanks, Erik !

* Tue Apr 21 1998 Cristian Gafton <gafton@redhat.com>
- new patch to get xterm-color and nxterm terminfo entries
- aliased them to rxvt, as that seems to satisfy everybody

* Sun Apr 12 1998 Cristian Gafton <gafton@redhat.com>
- added %clean section

* Tue Apr 07 1998 Cristian Gafton <gafton@redhat.com>
- removed /usr/lib/terminfo symlink - we shouldn't need that

* Mon Apr 06 1998 Cristian Gafton <gafton@redhat.com>
- updated to 4.2 + patches
- added BuildRoot

* Sat Apr 04 1998 Cristian Gafton <gafton@redhat.com>
- rebuilt with egcs on alpha

* Wed Dec 31 1997 Erik Troan <ewt@redhat.com>
- version 7 didn't rebuild properly on the Alpha somehow -- no real changes
  are in this version

* Tue Dec 09 1997 Erik Troan <ewt@redhat.com>
- TIOCGWINSZ wasn't used properly

* Tue Jul 08 1997 Erik Troan <ewt@redhat.com>
- built against glibc, linked shared libs against -lc

