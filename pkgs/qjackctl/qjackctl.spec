%global momorel 1
%global qtver 4.8.5

Summary: JACK Audio Connection Kit Qt GUI Interface
Name: qjackctl
Version: 0.3.11
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://qjackctl.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.3.8-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: jack
BuildRequires: qt-devel >= %{qtver}
BuildRequires: alsa-lib-devel
BuildRequires: coreutils
BuildRequires: dbus-devel
BuildRequires: desktop-file-utils
BuildRequires: jack-devel
BuildRequires: pkgconfig
BuildRequires: portaudio-devel

%description
JACK Audio Connection Kit - Qt GUI Interface: A simple Qt application
to control the JACK server. Written in C++ around the Qt4 toolkit
for X11, most exclusively using Qt Designer. Provides a simple GUI
dialog for setting several JACK server parameters, which are properly
saved between sessions, and a way control of the status of the audio
server. With time, this primordial interface has become richer by 
including a enhanced patchbay and connection control features.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja~

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category ALSA \
  --remove-category JACK \
  --remove-category MIDI \
  --remove-key X-SuSE-translate \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO TRANSLATORS
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/locale/%{name}_*.qm
%{_mandir}/man1/%{name}.1*

%changelog
* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.11-1m)
- update to 0.3.11

* Wed Apr  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.10-1m)
- update to 0.3.10

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.9-1m)
- update to 0.3.9

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-6m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-3m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-2m)
- explicitly link libX11

* Fri Mar 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-1m)
- initial package for music freaks using Momonga Linux
