%global momorel 1

%global gcc_release 4.6
%global gcc_version %{gcc_release}.4

#%%global DATE 20130329

%global pkg_suffix %{nil}
%global bin_suffix %{nil}

%global gcj_sub_dir gcj-%{gcc_version}-12
%global gcj_so_ver 12

%global objc_so_ver 3
%global gfortran_so_ver 3

%global debug_package %{nil}

%global build_plugin 1

%ifarch %{ix86} x86_64
###%%%global build_go 1
%global build_go 0
%else
%global build_go 0
%endif
%ifarch %{ix86} x86_64 ia64
%global build_libquadmath 1
%else
%global build_libquadmath 0
%endif

Summary: Various compilers (C, C++, Objective-C, Java, ...)
Name: gcc%{pkg_suffix}

%global _unpackaged_files_terminate_build 0
%global multilib_64_archs sparc64 ppc64 s390x x86_64

%ifarch %{ix86} x86_64 ia64 ppc s390 alpha
###%global build_ada 1
%global build_ada 0
%else
%global build_ada 0
%endif
%global build_java 1
%ifarch s390x
%global multilib_32_arch s390
%endif
%ifarch sparc64
%global multilib_32_arch sparcv9
%endif
%ifarch ppc64
%global multilib_32_arch ppc
%endif
%ifarch x86_64
%global multilib_32_arch i686
%endif

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpm/specopt/gcc%%{pkg_suffix}.specopt and edit it.

%{?!do_test: %global do_test 0}
%{?!build_test: %global build_test 0}
%{?!use_graphite: %global use_graphite 1}
%{?!build_libstdcxx_docs: %global build_libstdcxx_docs 1}

# bootstrap sequence is
# 1) install gcc-java and libgcj
# 2) build gcc with bootstrap_java=0 and build_java_tar=1
# 3) install gcc-bootstrap-java
# 4) remove gcc-java and libgcj using "rpm -e --nodeps gcc-java libgcj"
# 5) build gcc again with bootstrap_java=1 and build_java_tar=0
# 6) install gcc-java and libgcj
# 7) build gcc with bootstrap_java=0 and build_java_tar=0
%{?!bootstrap_java: %global bootstrap_java 0}
%{?!build_java_tar: %global build_java_tar 0}

Version: %{gcc_version}
Release: %{momorel}m%{?dist}
# !!FIXME!!
# libgcc, libgfortran, libmudflap and crtstuff have an exception which allows
# linking it into any kind of programs or shared libraries without
# restrictions.
#License: GPLv3+ and GPLv2+ with exceptions
License: GPL
Group: Development/Languages
#Source0: ftp://gcc.gnu.org/pub/gcc/snapshots/%{gcc_release}-%{DATE}/gcc-%{gcc_release}-%{DATE}.tar.bz2
#Source0: ftp://gcc.gnu.org/pub/gcc/snapshots/%{gcc_version}-RC-%{DATE}/gcc-%{gcc_version}-RC-%{DATE}.tar.bz2
Source0: ftp://gcc.gnu.org/pub/gcc/releases/gcc-%{gcc_version}/gcc-%{gcc_version}.tar.bz2
NoSource: 0
Source1: libgcc_post_upgrade.c
#Source3: protoize.1

# momonga's source
%if "%{bin_suffix}" == ""
Source8000: c89.in
Source8001: c99.in
%endif

# fedora patches
Patch0: gcc46-hack.patch
Patch4: gcc46-java-nomulti.patch
Patch8: gcc46-i386-libgomp.patch
Patch10: gcc46-libgomp-omp_h-multilib.patch
Patch11: gcc46-libtool-no-rpath.patch
Patch14: gcc46-pr38757.patch
Patch15: gcc46-libstdc++-docs.patch
Patch17: gcc46-no-add-needed.patch
Patch18: gcc46-unwind-debughook-sdt.patch

Patch100: gcc-support-glibc215.patch

# upstream patches
# svn diff -r XXXXXX:YYYYYY svn://gcc.gnu.org/svn/gcc/branches/redhat/gcc-4_4-branch/

# momonga's patches
%if "%{bin_suffix}" != ""
Patch8000: gcc46-textdomain.patch
Patch8001: gcc46-rename-info.patch
#Patch8002: gcc46-fix-gcj.patch
%endif

URL: http://gcc.gnu.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Need binutils with -pie support >= 2.14.90.0.4-4
# Need binutils which can omit dot symbols and overlap .opd on ppc64 >= 2.15.91.0.2-4
# Need binutils which handle -msecure-plt on ppc >= 2.16.91.0.2-2
# Need binutils which support .weakref >= 2.16.91.0.3-1
# Need binutils which support DT_GNU_HASH >= 2.17.50.0.31
# Need binutils which support --build-id >= 2.17.50.0.17-3
BuildRequires: binutils >= 2.17.50.0.18
# While gcc doesn't include statically linked binaries, during testing
# -static is used several times.
BuildRequires: glibc-static
BuildRequires: zlib-devel, gettext, dejagnu, bison, flex, texinfo, sharutils
%if %{build_java}
BuildRequires: fastjar
%if %{bootstrap_java}
BuildRequires: gcc-bootstrap-java
%else
BuildRequires: gcc-java, libgcj
%endif
%endif
# Make sure pthread.h doesn't contain __thread tokens
# Make sure glibc supports stack protector
# Make sure glibc supports DT_GNU_HASH
BuildRequires: glibc-devel >= 2.3.90-3m
BuildRequires: glibc-static
BuildRequires: elfutils-devel >= 0.72
%ifarch ppc ppc64 s390 s390x sparc sparcv9 alpha
# Make sure glibc supports TFmode long double
BuildRequires: glibc >= 2.3.90-3m
%endif
%ifarch %{multilib_64_archs} sparcv9 ppc
# Ensure glibc{,-devel} is installed for both multilib arches
BuildRequires: /lib/libc.so.6 /usr/lib/libc.so /lib64/libc.so.6 /usr/lib64/libc.so
%endif
%if %{build_ada}
# Ada requires Ada to build
BuildRequires: gcc-gnat >= 3.1, libgnat >= 3.1
%endif
%ifarch ia64
BuildRequires: libunwind >= 0.98
%endif
%if %{build_libstdcxx_docs}
BuildRequires: doxygen
BuildRequires: graphviz
%endif
%if %{do_test}
# testsuite requires autogen
BuildRequires: autogen
%endif
%if %{use_graphite}
BuildRequires: ppl-devel >= 1.0
BuildRequires: cloog-devel >= 0.17.0
%endif
BuildRequires: libmpc-devel
%ifarch x86_64
BuildConflicts: libgcc(x86-32)
%endif
Requires: cpp%{pkg_suffix} >= %{version}-%{release}
# Need .eh_frame ld optimizations
# Need proper visibility support
# Need -pie support
# Need --as-needed/--no-as-needed support
# On ppc64, need omit dot symbols support and --non-overlapping-opd
# Need binutils that owns /usr/bin/c++filt
# Need binutils that support .weakref
# Need binutils that supports --hash-style=gnu
# Need binutils that support mffgpr/mftgpr
# Need binutils that support --build-id
Requires: binutils >= 2.17.50.0
# Make sure gdb will understand DW_FORM_strp
Conflicts: gdb < 5.1-2
Requires: glibc-devel >= 2.2.90-12
Requires: libgcc%{pkg_suffix} >= %{version}-%{release}
Requires: libgomp%{pkg_suffix} >= %{version}-%{release}
BuildRequires: momonga-rpmmacros >= 20090121-1m
%ifarch ppc ppc64 s390 s390x sparc sparcv9 alpha
# Make sure glibc supports TFmode long double
Requires: glibc >= 2.3.90-35
%endif
#Obsoletes: gcc3
#Obsoletes: egcs
%ifarch sparc
#Obsoletes: gcc-sparc32
#Obsoletes: gcc-c++-sparc32
%endif
%ifarch ppc
#Obsoletes: gcc-ppc32
#Obsoletes: gcc-c++-ppc32
%endif
#Obsoletes: gcc-chill
%if !%{build_ada}
Obsoletes: gcc-gnat < %{version}-%{release}
Obsoletes: libgnat < %{version}-%{release}
%endif
%ifarch sparc sparc64
#Obsoletes: egcs64
%endif
#Obsoletes: gcc%{pkg_suffix}
%if "%{bin_suffix}" != ""
Provides: gcc = %{version}-%{release}
%endif
Requires: info
AutoReq: true

# On ARM EABI systems, we do want -gnueabi to be part of the
# target triple.
%ifnarch %{arm}
%global _gnu %{nil}
%endif
%ifarch sparcv9
%global gcc_target_platform sparc64-%{_vendor}-%{_target_os}
%endif
%ifarch ppc
%global gcc_target_platform ppc64-%{_vendor}-%{_target_os}
%endif
%ifnarch sparcv9 ppc
%global gcc_target_platform %{_target_platform}
%endif

%description
The gcc package contains the GNU Compiler Collection version %{gcc_release}.
You'll need this package in order to compile C code.

%package -n libgcc%{pkg_suffix}
Summary: GCC version %{gcc_release} shared support library
Group: System Environment/Libraries
Autoreq: false
Provides: /usr/sbin/libgcc_post_upgrade
#Provides: libgcc = %{version}-%{release}
Obsoletes: libgcc41
Obsoletes: libgcc42
Obsoletes: libgcc43
Obsoletes: libgcc44
Obsoletes: libgcc45
Provides: libgcc41
Provides: libgcc42
Provides: libgcc43
Provides: libgcc44
Provides: libgcc45
Provides: libgcc46

%description -n libgcc%{pkg_suffix}
This package contains GCC shared support library which is needed
e.g. for exception handling support.

%package c++
Summary: C++ support for GCC
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libstdc++%{pkg_suffix} >= %{version}-%{release}
Requires: libstdc++%{pkg_suffix}-devel >= %{version}-%{release}
#Obsoletes: gcc%{pkg_suffix}-c++
#Provides: gcc%{pkg_suffix}-c++ = %{version}-%{release}
Autoreq: true

%description c++
This package adds C++ support to the GNU Compiler Collection.
It includes support for most of the current C++ specification,
including templates and exception handling.

%package -n libstdc++%{pkg_suffix}
Summary: GNU Standard C++ Library
Group: System Environment/Libraries
Autoreq: true
#Provides: libstdc++ = %{version}-%{release}
Obsoletes: libstdc++ <= 4.1.2

%description -n libstdc++%{pkg_suffix}
The libstdc++ package contains a rewritten standard compliant GCC Standard
C++ Library.

%package -n libstdc++%{pkg_suffix}-devel
Summary: Header files and libraries for C++ development
Group: Development/Libraries
Requires: libstdc++%{pkg_suffix} >= %{version}-%{release}
Autoreq: true

%description -n libstdc++%{pkg_suffix}-devel
This is the GNU implementation of the standard C++ libraries.  This
package includes the header files and libraries needed for C++
development. This includes rewritten implementation of STL.

%package -n libstdc++%{pkg_suffix}-static
Summary: Static libraries for the GNU standard C++ library
Group: Development/Libraries
Requires: libstdc++%{pkg_suffix}-devel = %{version}-%{release}
Autoreq: true

%description -n libstdc++%{pkg_suffix}-static
Static libraries for the GNU standard C++ library. 

%if %{build_libstdcxx_docs}
%package -n libstdc++%{pkg_suffix}-docs
Summary: Documentation for the GNU standard C++ library
Group: Development/Libraries
Autoreq: true

%description -n libstdc++%{pkg_suffix}-docs
Manual, doxygen generated API information and Frequently Asked Questions
for the GNU standard C++ library.
%endif

%package objc
Summary: Objective-C support for GCC
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libobjc%{pkg_suffix} >= %{version}-%{release}
Autoreq: true

%description objc
gcc-objc provides Objective-C support for the GCC.
Mainly used on systems running NeXTSTEP, Objective-C is an
object-oriented derivative of the C language.

%package objc++
Summary: Objective-C++ support for GCC
Group: Development/Languages
Requires: %{name}-c++ = %{version}-%{release}
Requires: %{name}-objc = %{version}-%{release}
Autoreq: true

%description objc++
gcc-objc++ package provides Objective-C++ support for the GCC.

%package -n libobjc%{pkg_suffix}
Summary: Objective-C runtime
Group: System Environment/Libraries
Autoreq: true
Obsoletes: libobjc <= 4.1.2
# for libobjc.so.3
Provides: libobjc46

%description -n libobjc%{pkg_suffix}
This package contains Objective-C shared library which is needed to run
Objective-C dynamically linked programs.

%package gfortran
Summary: Fortran support
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgfortran%{pkg_suffix} >= %{version}-%{release}
BuildRequires: gmp-devel >= 4.2.4
BuildRequires: mpfr-devel
Requires: info
%if %{build_libquadmath}
Requires: libquadmath%{pkg_suffix} = %{version}-%{release}
Requires: libquadmath%{pkg_suffix}-devel = %{version}-%{release}
%endif
Obsoletes: gcc%{pkg_suffix}-g77
Provides: gcc%{pkg_suffix}-g77
#Obsoletes: gcc%{pkg_suffix}-gfortran
#Provides: gcc%{pkg_suffix}-gfortran
Autoreq: true

%description gfortran
The gcc-gfortran package provides support for compiling Fortran
programs with the GNU Compiler Collection.

%package -n libgfortran%{pkg_suffix}
Summary: Fortran runtime
Group: System Environment/Libraries
Autoreq: true
%if %{build_libquadmath}
Requires: libquadmath%{pkg_suffix} = %{version}-%{release}
%endif
# for libgfortran.so.3
Obsoletes: libgfortran43
Obsoletes: libgfortran44
Obsoletes: libgfortran45
Provides: libgfortran43
Provides: libgfortran44
Provides: libgfortran45
Provides: libgfortran46

%description -n libgfortran%{pkg_suffix}
This package contains Fortran shared library which is needed to run
Fortran 95 dynamically linked programs.

%package -n libgfortran%{pkg_suffix}-static
Summary: Static Fortran libraries
Group: Development/Libraries
Requires: libgfortran%{pkg_suffix} = %{version}-%{release}
Requires: %{name} = %{version}-%{release}
%if %{build_libquadmath}
Requires: libquadmath%{pkg_suffix}-static = %{version}-%{release}
%endif

%description -n libgfortran%{pkg_suffix}-static
This package contains static Fortran libraries.

%package -n libgomp%{pkg_suffix}
Summary: GCC OpenMP v3.0 shared support library
Group: System Environment/Libraries
#Provides: libgomp = %{version}-%{release}
Obsoletes: libgomp <= 4.1.2
Obsoletes: libgomp41
Obsoletes: libgomp42
Obsoletes: libgomp43
Obsoletes: libgomp44
Obsoletes: libgomp45
Provides: libgomp41
Provides: libgomp42
Provides: libgomp43
Provides: libgomp44
Provides: libgomp45
Provides: libgomp46

%description -n libgomp%{pkg_suffix}
This package contains GCC shared support library which is needed
for OpenMP v3.0 support.

%package -n libmudflap%{pkg_suffix}
Summary: GCC mudflap shared support library
Group: System Environment/Libraries
#Provides: libmudflap = %{version}-%{release}
Obsoletes: libmudflap <= 4.1.2
Obsoletes: libmudflap41
Obsoletes: libmudflap42
Obsoletes: libmudflap43
Obsoletes: libmudflap44
Obsoletes: libmudflap45
Provides: libmudflap41
Provides: libmudflap42
Provides: libmudflap43
Provides: libmudflap44
Provides: libmudflap45
Provides: libmudflap46

%description -n libmudflap%{pkg_suffix}
This package contains GCC shared support library which is needed
for mudflap support.

%package -n libmudflap%{pkg_suffix}-devel
Summary: GCC mudflap support
Group: Development/Libraries
Requires: libmudflap%{pkg_suffix} = %{version}-%{release}
Requires: %{name} = %{version}-%{release}
Obsoletes: libmudflap-devel <= 4.1.2

%description -n libmudflap%{pkg_suffix}-devel
This package contains headers and static libraries for building
mudflap-instrumented programs.

To instrument a non-threaded program, add -fmudflap
option to GCC and when linking add -lmudflap, for threaded programs
also add -fmudflapth and -lmudflapth.

%package -n libmudflap%{pkg_suffix}-static
Summary: Static libraries for mudflap support
Group: Development/Libraries
Requires: libmudflap%{pkg_suffix}-devel = %{version}-%{release}

%description -n libmudflap%{pkg_suffix}-static
This package contains static libraries for building mudflap-instrumented
programs.

%if %{build_libquadmath}
%package -n libquadmath%{pkg_suffix}
Summary: GCC __float128 shared support library
Group: System Environment/Libraries
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description -n libquadmath%{pkg_suffix}
This package contains GCC shared support library which is needed
for __float128 math support and for Fortran REAL*16 support.

%package -n libquadmath%{pkg_suffix}-devel
Summary: GCC __float128 support
Group: Development/Libraries
Requires: libquadmath%{pkg_suffix} = %{version}-%{release}
Requires: %{name} = %{version}-%{release}

%description -n libquadmath%{pkg_suffix}-devel
This package contains headers for building Fortran programs using
REAL*16 and programs using __float128 math.

%package -n libquadmath%{pkg_suffix}-static
Summary: Static libraries for __float128 support
Group: Development/Libraries
Requires: libquadmath%{pkg_suffix}-devel = %{version}-%{release}

%description -n libquadmath%{pkg_suffix}-static
This package contains static libraries for building Fortran programs
using REAL*16 and programs using __float128 math.
%endif

%if %{build_java}
%package java
Summary: Java support for GCC
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgcj%{pkg_suffix} >= %{version}
Requires: libgcj%{pkg_suffix}-devel >= %{version}
Requires: zlib-devel
# for /usr/share/java/eclipse-ecj.jar
Requires: ecj
#Obsoletes: gcc%{pkg_suffix}-java
#Provides: gcc%{pkg_suffix}-java
Requires: info
Autoreq: true

%description java
This package adds support for compiling Java(tm) programs and
bytecode into native code.

%package -n libgcj%{pkg_suffix}
Summary: Java runtime library for gcc
Group: System Environment/Libraries
Requires: info
Requires: zip >= 2.1
Requires: gtk2 >= 2.4.0
Requires: glib2 >= 2.4.0
Requires: libart_lgpl >= 2.1.0
%if "%{bin_suffix}" != ""
# requires meta package
Requires: libgcj >= 4.4-4m
%endif
BuildRequires: gtk2-devel >= 2.4.0
BuildRequires: glib2-devel >= 2.4.0
#BuildRequires: xulrunner-devel
BuildRequires: libart_lgpl-devel >= 2.1.0
BuildRequires: alsa-lib-devel
BuildRequires: libXtst-devel
BuildRequires: libXt-devel
Obsoletes: gcc-libgcj
#Obsoletes: libgcj%{pkg_suffix}
#Provides: libgcj%{pkg_suffix}
Autoreq: true
Requires: fastjar
Obsoletes: libgcj <= 4.1.2
#Provides: libgcj = %{version}-%{release}

%description -n libgcj%{pkg_suffix}
The Java(tm) runtime library. You will need this package to run your Java
programs compiled using the Java compiler from GNU Compiler Collection (gcj).

%package -n libgcj%{pkg_suffix}-devel
Summary: Libraries for Java development using GCC
Group: Development/Languages
Requires: zip >= 2.1
Requires: libgcj%{pkg_suffix} = %{version}-%{release}
Requires: zlib-devel
Requires: /bin/awk
%if "%{bin_suffix}" != ""
# requires meta package
Requires: libgcj-devel >= 4.4-4m
%endif
# note: gcc is required because of /usr/include/c++
Requires: gcc 
Autoreq: false
Autoprov: false

%description -n libgcj%{pkg_suffix}-devel
The Java(tm) static libraries and C header files. You will need this
package to compile your Java programs using the GCC Java compiler (gcj).

%package -n libgcj%{pkg_suffix}-src
Summary: Java library sources from GCC4 preview
Group: System Environment/Libraries
Requires: libgcj%{pkg_suffix} = %{version}-%{release}
#Obsoletes: libgcj%{pkg_suffix}-src
#Provides: libgcj%{pkg_suffix}-src
Obsoletes: libgcj-src <= 4.1.2
Autoreq: true

%description -n libgcj%{pkg_suffix}-src
The Java(tm) runtime library sources for use in Eclipse.
%endif

%package -n cpp%{pkg_suffix}
Summary: The C Preprocessor.
Group: Development/Languages
Requires: info
%ifarch ia64
#Obsoletes: gnupro
%endif
Autoreq: true

%description -n cpp%{pkg_suffix}
Cpp is the GNU C-Compatible Compiler Preprocessor.
Cpp is a macro processor which is used automatically
by the C compiler to transform your program before actual
compilation. It is called a macro processor because it allows
you to define macros, abbreviations for longer
constructs.

The C preprocessor provides four separate functionalities: the
inclusion of header files (files of declarations that can be
substituted into your program); macro expansion (you can define macros,
and the C preprocessor will replace the macros with their definitions
throughout the program); conditional compilation (using special
preprocessing directives, you can include or exclude parts of the
program according to various conditions); and line control (if you use
a program to combine or rearrange source files into an intermediate
file which is then compiled, you can use line control to inform the
compiler about where each source line originated).

You should install this package if you are a C programmer and you use
macros.

%package gnat
Summary: Ada 95 support for GCC
Group: Development/Languages
Requires: %{name} = %{version}-%{release}, libgnat%{pkg_suffix} = %{version}-%{release}
Obsoletes: gnat-devel, gcc3-gnat
Requires: info
Autoreq: true

%description gnat
GNAT is a GNU Ada 95 front-end to GCC. This package includes development tools,
the documents and Ada 95 compiler.

%package -n libgnat%{pkg_suffix}
Summary: GNU Ada 95 runtime shared libraries
Group: System Environment/Libraries
Obsoletes: libgnat3
Obsoletes: libgnat <= 4.1.2
Autoreq: true

%description -n libgnat%{pkg_suffix}
GNAT is a GNU Ada 95 front-end to GCC. This package includes shared libraries,
which are required to run programs compiled with the GNAT.

%package -n libgnat%{pkg_suffix}-devel
Summary: GNU Ada 95 libraries
Group: Development/Languages
Autoreq: true
%description -n libgnat%{pkg_suffix}-devel
GNAT is a GNU Ada 95 front-end to GCC. This package includes libraries,
which are required to compile with the GNAT.

%package -n libgnat%{pkg_suffix}-static
Summary: GNU Ada 95 static libraries
Group: Development/Languages
Requires: libgnat%{pkg_suffix}-devel = %{version}-%{release}
Autoreq: true

%description -n libgnat%{pkg_suffix}-static
GNAT is a GNU Ada 95 front-end to GCC. This package includes static libraries.

%if %{build_java_tar}
%package -n gcc-bootstrap-java
Summary: bootstrap version of Java compiler
Group: Development/Languages
%description -n gcc-bootstrap-java
This package provides a bootstrap version of Java compiler, which
is used when you bootstrap gcc-java.
%endif

%if %{build_go}
%package go%{pkg_suffix}
Summary: Go support
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgo%{pkg_suffix} = %{version}-%{release}
Requires: libgo%{pkg_suffix}-devel = %{version}-%{release}
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info
Autoreq: true

%description go%{pkg_suffix}
The gcc-go package provides support for compiling Go programs
with the GNU Compiler Collection.

%package -n libgo%{pkg_suffix}
Summary: Go runtime
Group: System Environment/Libraries
Autoreq: true

%description -n libgo%{pkg_suffix}
This package contains Go shared library which is needed to run
Go dynamically linked programs.

%package -n libgo%{pkg_suffix}-devel
Summary: Go development libraries
Group: Development/Languages
Requires: libgo%{pkg_suffix} = %{version}-%{release}
Autoreq: true

%description -n libgo%{pkg_suffix}-devel
This package includes libraries and support files for compiling
Go programs.

%package -n libgo%{pkg_suffix}-static
Summary: Static Go libraries
Group: Development/Libraries
Requires: libgo%{pkg_suffix} = %{version}-%{release}
Requires: %{name} = %{version}-%{release}
 
%description -n libgo%{pkg_suffix}-static
This package contains static Go libraries.
%endif

%if %{build_plugin} 
%package plugin-devel
Summary: Support for compiling GCC plugins
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description plugin-devel
This package contains header files and other support files
for compiling GCC plugins.  The GCC plugin ABI is currently
not stable, so plugins must be rebuilt any time GCC is updated.
%endif

%prep
#%%setup -q -n gcc-%{gcc_release}-%{DATE}
#%%setup -q -n gcc-%{gcc_version}-RC-%{DATE}
%setup -q -n gcc-%{gcc_version}


# upstream patch

# fedora's patches
%patch0 -p0 -b .hack~
%patch4 -p0 -b .java-nomulti~
%patch8 -p0 -b .i386-libgomp~
%patch10 -p0 -b .libgomp-omp_h-multilib~
%patch11 -p0 -b .libtool-no-rpath~
%patch14 -p0 -b .pr38757~
%if %{build_libstdcxx_docs}
%patch15 -p1 -b .libstdc++-docs~
%endif
%patch17 -p0 -b .no-add-needed~
%patch18 -p0 -b .unwind-debughook-sdt~

#%patch100 -p0 -b .glibc215

# momonga's patches
%if "%{bin_suffix}" != ""
%patch8000 -p1 -b .textdomain~
%patch8001 -p1 -b .info~
#%patch8002 -p1 -b .fixgcj~
%endif

%if %{bootstrap_java}
# libjava-classes.tar.bz2 is provided by gcc-bootstrap-java 
tar xjf %{_libexecdir}/gcc-bootstrap-java/libjava-classes.tar.bz2
%endif

#sed -i -e 's/4\.6\.2/4.6.1/' gcc/BASE-VER

# Default to -gdwarf-3 rather than -gdwarf-2
sed -i '/UInteger Var(dwarf_version)/s/Init(2)/Init(3)/' gcc/common.opt
sed -i 's/\(may be either 2, 3 or 4; the default version is \)2\./\13./' gcc/doc/invoke.texi

cp -a libstdc++-v3/config/cpu/i{4,3}86/atomicity.h

# Hack to avoid building multilib libjava
perl -pi -e 's/^all: all-redirect/ifeq (\$(MULTISUBDIR),)\nall: all-redirect\nelse\nall:\n\techo Multilib libjava build disabled\nendif/' libjava/Makefile.in
perl -pi -e 's/^install: install-redirect/ifeq (\$(MULTISUBDIR),)\ninstall: install-redirect\nelse\ninstall:\n\techo Multilib libjava install disabled\nendif/' libjava/Makefile.in
perl -pi -e 's/^check: check-redirect/ifeq (\$(MULTISUBDIR),)\ncheck: check-redirect\nelse\ncheck:\n\techo Multilib libjava check disabled\nendif/' libjava/Makefile.in
perl -pi -e 's/^all: all-recursive/ifeq (\$(MULTISUBDIR),)\nall: all-recursive\nelse\nall:\n\techo Multilib libjava build disabled\nendif/' libjava/Makefile.in
perl -pi -e 's/^install: install-recursive/ifeq (\$(MULTISUBDIR),)\ninstall: install-recursive\nelse\ninstall:\n\techo Multilib libjava install disabled\nendif/' libjava/Makefile.in
perl -pi -e 's/^check: check-recursive/ifeq (\$(MULTISUBDIR),)\ncheck: check-recursive\nelse\ncheck:\n\techo Multilib libjava check disabled\nendif/' libjava/Makefile.in

./contrib/gcc_update --touch
# To make rpmlint happy (argh), fix up names in ChangeLog entries to valid UTF-8
LC_ALL=C sed -i \
  -e 's/D\(o\|\xf6\)nmez/D\xc3\xb6nmez/' \
  -e 's/\(Av\|\x81\xc1v\|\xc1v\|\xef\xbf\xbdv\?\|\x81\xc3\x81v\|\xc3v\)ila/\xc3\x81vila/' \
  -e 's/Esp\(in\|\x81\xedn\|\xedn\|\xef\xbf\xbdn\?\|\xef\xbf\xbd\xadn\|\x81\xc3\xadn\|\xc3\xef\xbf\xbd\xadn\)dola/Esp\xc3\xadndola/' \
  -e 's/Schl\(u\|\xef\xbf\xbd\|\xfcu\?\|\x81\xfc\|\x81\xc3\xbc\|\xc3\xaf\xc2\xbf\xc2\xbd\|\xef\xbf\xbd\xef\xbf\xbd\xef\xbf\xbd\xc2\xbc\)ter/Schl\xc3\xbcter/' \
  -e 's/Humi\(e\|\xe8\)res/Humi\xc3\xa8res/' \
  -e 's/L\(ow\|\xc3\xaf\xc2\xbf\xc2\xbd\|oew\|\xf6w\)is/L\xc3\xb6wis/' \
  -e 's/G\xfctlein/G\xc3\xbctlein/' \
  -e 's/G\xe1[b]or/G\xc3\xa1bor/' \
  -e 's/L\xf3ki/L\xc3\xb3ki/' \
  -e 's/Fautr\xc3 /Fautr\xc3\xa9 /' \
  -e 's/S\xe9[b]astian/S\xc3\xa9bastian/' \
  -e 's/Th\xef\xbf\xbd[d]ore/Th\xc3\xa9odore/' \
  -e 's/Cors\xc3\xc2\xa9pius/Cors\xc3\xa9pius/' \
  -e 's/K\xfchl/K\xc3\xbchl/' \
  -e 's/R\xf6nnerup/R\xc3\xb6nnerup/' \
  -e 's/L\xf8vset/L\xc3\xb8vset/' \
  -e 's/Ph\x81\xfb\x81\xf4ng-Th\x81\xe5o/Ph\xc3\xbb\xc3\xb4ng-Th\xc3\xa5o/' \
  -e 's/V\x81\xf5/V\xc3\xb5/' \
  -e 's/J\xf6nsson/J\xc3\xb6nsson/' \
  -e 's/V\xef\xbf\xbdis\xef\xbf\xbdnen/V\xc3\xa4is\xc3\xa4nen/' \
  -e 's/J\xef\xbf\xbdrg/J\xc3\xb6rg/' \
  -e 's/M\xef\xbf\xbdsli/M\xc3\xb6sli/' \
  -e 's/R\xe4ty/R\xc3\xa4ty/' \
  -e 's/2003\xc2\xad-/2003-/' \
  -e 's/\xc2\xa0/ /g' \
  -e 's/ \xa0/  /g' \
  -e 's/\xa0 //' \
  `find . -name \*ChangeLog\*`
LC_ALL=C sed -i -e 's/\xa0/ /' gcc/doc/options.texi

%ifarch ppc
if [ -d libstdc++-v3/config/abi/post/powerpc64-linux-gnu ]; then
  mkdir -p libstdc++-v3/config/abi/post/powerpc64-linux-gnu/64
  mv libstdc++-v3/config/abi/post/powerpc64-linux-gnu/{,64/}baseline_symbols.txt
  mv libstdc++-v3/config/abi/post/powerpc64-linux-gnu/{32/,}baseline_symbols.txt
  rm -rf libstdc++-v3/config/abi/post/powerpc64-linux-gnu/32
fi
%endif
%ifarch sparc
if [ -d libstdc++-v3/config/abi/post/sparc64-linux-gnu ]; then
  mkdir -p libstdc++-v3/config/abi/post/sparc64-linux-gnu/64
  mv libstdc++-v3/config/abi/post/sparc64-linux-gnu/{,64/}baseline_symbols.txt
  mv libstdc++-v3/config/abi/post/sparc64-linux-gnu/{32/,}baseline_symbols.txt
  rm -rf libstdc++-v3/config/abi/post/sparc64-linux-gnu/32
fi
%endif

# This test causes fork failures, because it spawns way too many threads
rm -f gcc/testsuite/go.test/test/chan/goroutines.go

%build

%if %{build_java}
export GCJ_PROPERTIES=jdt.compiler.useSingleThread=true
%endif

rm -fr obj-%{gcc_target_platform}
mkdir obj-%{gcc_target_platform}
cd obj-%{gcc_target_platform}

%if %{build_java}
%if !%{bootstrap_java}
# If we don't have gjavah in $PATH, try to build it with the old gij
mkdir java_hacks
cd java_hacks
cp -a ../../libjava/classpath/tools/external external
mkdir -p gnu/classpath/tools
cp -a ../../libjava/classpath/tools/gnu/classpath/tools/{common,javah,getopt} gnu/classpath/tools/
cp -a ../../libjava/classpath/tools/resource/gnu/classpath/tools/common/Messages.properties gnu/classpath/tools/common
cp -a ../../libjava/classpath/tools/resource/gnu/classpath/tools/getopt/Messages.properties gnu/classpath/tools/getopt
cd external/asm; for i in `find . -name \*.java`; do gcj --encoding ISO-8859-1 -C $i -I.; done; cd ../..
for i in `find gnu -name \*.java`; do gcj -C $i -I. -Iexternal/asm/; done
gcj -findirect-dispatch -O2 -fmain=gnu.classpath.tools.javah.Main -I. -Iexternal/asm/ `find . -name \*.class` -o gjavah.real
cat > gjavah <<EOF
#!/bin/sh
export CLASSPATH=`pwd`${CLASSPATH:+:$CLASSPATH}
exec `pwd`/gjavah.real "\$@"
EOF
chmod +x `pwd`/gjavah
cat > ecj1 <<EOF
#!/bin/sh
exec gij -cp /usr/share/java/eclipse-ecj.jar org.eclipse.jdt.internal.compiler.batch.GCCMain "\$@"
EOF
chmod +x `pwd`/ecj1
export PATH=`pwd`${PATH:+:$PATH}
cd ..
%endif
%endif

CC=gcc
OPT_FLAGS=`echo %{optflags}|sed -e 's/\(-Wp,\)\?-D_FORTIFY_SOURCE=[12]//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-m64//g;s/-m32//g;s/-m31//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-fstack-protector//g' -e 's/-fno-exceptions//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-Wno-unused-but-set-variable//g' -e 's/-Wno-unused-but-set-parameter//g'`

%ifarch sparc
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mcpu=ultrasparc/-mtune=ultrasparc/g;s/-mcpu=v[78]//g'`
%endif
%ifarch %{ix86}
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-march=i.86//g'`
%endif
%ifarch sparc64
cat > gcc64 <<"EOF"
#!/bin/sh
exec /usr/bin/gcc -m64 "$@"
EOF
chmod +x gcc64
CC=`pwd`/gcc64
%endif
%ifarch ppc64
if gcc -m64 -xc -S /dev/null -o - > /dev/null 2>&1; then
  cat > gcc64 <<"EOF"
#!/bin/sh
exec /usr/bin/gcc -m64 "$@"
EOF
  chmod +x gcc64
  CC=`pwd`/gcc64
fi
%endif
OPT_FLAGS=`echo "$OPT_FLAGS" | sed -e 's/[[:blank:]]\+/ /g'`
case "$OPT_FLAGS" in
  *-fasynchronous-unwind-tables*)
    sed -i -e 's/-fno-exceptions /-fno-exceptions -fno-asynchronous-unwind-tables/' \
      ../gcc/Makefile.in
    ;;
esac

%if %{build_test}
OPT_FLAGS="-O0"
%endif

CC="$CC" CFLAGS="$OPT_FLAGS" CXXFLAGS="$OPT_FLAGS" XCFLAGS="$OPT_FLAGS" TCFLAGS="$OPT_FLAGS" \
	GCJFLAGS="$OPT_FLAGS" \
	../configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
	--with-pkgversion="Momonga Linux %{version}-%{release}" \
	--program-suffix=%{bin_suffix} \
	--enable-shared --enable-threads=posix --enable-checking=release \
	--with-system-zlib --enable-__cxa_atexit \
	--enable-lto --enable-gold \
	--disable-libunwind-exceptions \
  	--enable-gnu-unique-object --enable-linker-build-id \
  	--host=%{gcc_target_platform} --build=%{gcc_target_platform} --target=%{gcc_target_platform} \
%ifarch ia64
        --with-system-libunwind \
%else
        --without-system-libunwind \
%endif
	--enable-languages=c,c++,objc,obj-c++,fortran\
%if %{build_ada}
,ada\
%endif
%if %{build_java}
,java\
%endif
%if %{build_go}
,go\
%endif
%if %{use_graphite}
	--with-ppl \
	--enable-cloog-backend=isl \
%endif
%if !%{build_java}
	--disable-libgcj \
%else
	--enable-java-awt=gtk --disable-dssi \
	--with-java-home=%{_prefix}/lib/jvm/java-1.5.0-gcj-1.5.0.0/jre \
	--enable-libgcj-multifile \
%if !%{bootstrap_java}
	--enable-java-maintainer-mode \
%endif
	--with-ecj-jar=/usr/share/java/eclipse-ecj.jar \
	--disable-libjava-multilib \
%endif
%ifarch %{arm}
	--disable-sjlj-exceptions \
%endif
%ifarch ppc ppc64
	--enable-secureplt \
%endif
%ifarch sparc sparcv9 sparc64 ppc ppc64 s390 s390x alpha
	--with-long-double-128 \
%endif
%ifarch sparc
	--disable-linux-futex \
%endif
%ifarch sparc64
	--with-cpu=ultrasparc \
%endif
%ifarch sparc sparcv9
	--host=%{gcc_target_platform} --build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=v7
%endif
%ifarch ppc
	--build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=default32
%endif
%ifarch %{ix86} x86_64
	--with-tune=generic \
%endif
%if %{build_test}
        --disable-bootstrap \
%endif
%ifarch %{ix86}
	--with-arch=i686 \
%endif
%ifarch x86_64
	--with-arch_32=i686 \
%endif
%ifarch s390 s390x
	--with-arch=z9-109 --with-tune=z10 --enable-decimal-float \
%endif
%ifnarch sparc sparcv9 ppc
	--build=%{gcc_target_platform}
%endif

%if !%{build_test}
%make BOOT_CFLAGS="$OPT_FLAGS" GCJFLAGS="$OPT_FLAGS" profiledbootstrap
%else
# build without bootstrapping
%make BOOT_CFLAGS="$OPT_FLAGS" GCJFLAGS="$OPT_FLAGS"
%endif

%if %{do_test}
# run the tests.
make %{?_smp_mflags} -k check RUNTESTFLAGS="ALT_CC_UNDER_TEST=gcc ALT_CXX_UNDER_TEST=g++" || :
cd gcc
mv testsuite{,.normal}
make %{?_smp_mflags} -k \
  `sed -n 's/check-ada//;s/^CHECK_TARGETS[[:blank:]]*=[[:blank:]]*//p' Makefile` \
  RUNTESTFLAGS="--target_board=unix/-fstack-protector ALT_CC_UNDER_TEST=gcc ALT_CXX_UNDER_TEST=g++" || :
mv testsuite{,.ssp}
mv testsuite{.normal,}
cd ..
echo ====================TESTING=========================
( ../contrib/test_summary || : ) 2>&1 | sed -n '/^cat.*EOF/,/^EOF/{/^cat.*EOF/d;/^EOF/d;/^LAST_UPDATED:/d;p;}'
echo ====================TESTING END=====================
mkdir testlogs-%{_target_platform}-%{version}-%{release}
for i in `find . -name \*.log | grep -F testsuite/ | grep -v 'config.log\|acats\|ada'`; do
  ln $i testlogs-%{_target_platform}-%{version}-%{release}/ || :
done
for i in `find . -name \*.log | grep -F testsuite.ssp/ | grep -v 'config.log\|acats\|ada'`; do
  ln $i testlogs-%{_target_platform}-%{version}-%{release}/ssp-`basename $i` || :
done
tar cf - testlogs-%{_target_platform}-%{version}-%{release} | bzip2 -9c \
  | uuencode testlogs-%{_target_platform}.tar.bz2 || :
rm -rf testlogs-%{_target_platform}-%{version}-%{release}
%endif

# Make protoize
#make -C gcc CC="./xgcc -B ./ -O2" proto

# Make generated man pages even if Pod::Man is not new enough
perl -pi -e 's/head3/head2/' ../contrib/texi2pod.pl
for i in ../gcc/doc/*.texi; do
  cp -a $i $i.orig; sed 's/ftable/table/' $i.orig > $i
done
make -C gcc generated-manpages
for i in ../gcc/doc/*.texi; do mv -f $i.orig $i; done

# Make generated doxygen pages.
%if %{build_libstdcxx_docs}
cd %{gcc_target_platform}/libstdc++-v3
make doc-html-doxygen
make doc-man-doxygen
cd ../..
%endif

# Copy various doc files here and there
cd ..
mkdir -p rpm.doc/gfortran rpm.doc/objc
mkdir -p rpm.doc/boehm-gc rpm.doc/fastjar rpm.doc/libffi rpm.doc/libjava
mkdir -p rpm.doc/go rpm.doc/libgo rpm.doc/libquadmath
mkdir -p rpm.doc/changelogs/{gcc/cp,gcc/java,gcc/ada,libstdc++-v3,libobjc,libmudflap,libgomp}

for i in {gcc,gcc/cp,gcc/java,gcc/ada,libstdc++-v3,libobjc,libmudflap,libgomp}/ChangeLog*; do
	cp -p $i rpm.doc/changelogs/$i
done

(cd gcc/fortran; for i in ChangeLog*; do
	cp -p $i ../../rpm.doc/gfortran/$i
done)
(cd libgfortran; for i in ChangeLog*; do
	cp -p $i ../rpm.doc/gfortran/$i.libgfortran
done)
(cd libobjc; for i in README*; do
	cp -p $i ../rpm.doc/objc/$i.libobjc
done)
(cd boehm-gc; for i in ChangeLog*; do
	cp -p $i ../rpm.doc/boehm-gc/$i.gc
done)
(cd libffi; for i in ChangeLog* README* LICENSE; do
	cp -p $i ../rpm.doc/libffi/$i.libffi
done)
(cd libjava; for i in ChangeLog* README*; do
	cp -p $i ../rpm.doc/libjava/$i.libjava
done)
cp -p libjava/LIBGCJ_LICENSE rpm.doc/libjava/
%if %{build_libquadmath}
(cd libquadmath; for i in ChangeLog* COPYING.LIB; do
	cp -p $i ../rpm.doc/libquadmath/$i.libquadmath
done)
%endif
%if %{build_go}
(cd gcc/go; for i in README* ChangeLog*; do
	cp -p $i ../../rpm.doc/go/$i
done)
(cd libgo; for i in LICENSE* PATENTS* README; do
	cp -p $i ../rpm.doc/libgo/$i.libgo
done)
%endif

rm -f rpm.doc/changelogs/gcc/ChangeLog.[1-9]
find rpm.doc -name \*ChangeLog\* | xargs bzip2 -9

%if %{build_java_tar}
find libjava -name \*.h -type f | xargs grep -l '// DO NOT EDIT THIS FILE - it is machine generated' > libjava-classes.list
find libjava -name \*.class -type f >> libjava-classes.list
find libjava/testsuite -name \*.jar -type f >> libjava-classes.list
tar cf - -T libjava-classes.list | bzip2 -9 > libjava-classes-%{version}-%{release}.tar.bz2
%endif

%install
rm -fr $RPM_BUILD_ROOT

perl -pi -e \
  's~href="l(ibstdc|atest)~href="http://gcc.gnu.org/onlinedocs/libstdc++/l\1~' \
  libstdc++-v3/doc/html/api.html

cd obj-%{gcc_target_platform}

%if %{build_java}
export GCJ_PROPERTIES=jdt.compiler.useSingleThread=true
%if !%{bootstrap_java}
export PATH=`pwd`/java_hacks${PATH:+:$PATH}
%endif
%endif

TARGET_PLATFORM=%{gcc_target_platform}

# There are some MP bugs in libstdc++ Makefiles
make -C %{gcc_target_platform}/libstdc++-v3

make prefix=%{buildroot}%{_prefix} mandir=%{buildroot}%{_mandir} \
  infodir=%{buildroot}%{_infodir} install
%if %{build_java}
mkdir -p %{buildroot}/%{_datadir}/java
make DESTDIR=%{buildroot} -C %{gcc_target_platform}/libjava install-src.zip
%endif
%if %{build_ada}
chmod 644 %{buildroot}%{_infodir}/gnat*
%endif

FULLPATH=%{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
FULLEPATH=%{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}

# fix some things
ln -sf cpp%{bin_suffix} %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-cpp%{bin_suffix}
ln -sf gcc%{bin_suffix} %{buildroot}%{_prefix}/bin/cc%{bin_suffix}
mkdir -p %{buildroot}/lib
ln -sf ..%{_prefix}/bin/cpp%{bin_suffix} %{buildroot}/lib/cpp%{bin_suffix}
ln -sf gfortran%{bin_suffix} %{buildroot}%{_prefix}/bin/f95%{bin_suffix}

rm -f %{buildroot}%{_infodir}/dir
%if "%{bin_suffix}" != ""
mv %{buildroot}%{_infodir}/libgomp.info %{buildroot}%{_infodir}/libgomp%{bin_suffix}.info
mv %{buildroot}%{_infodir}/libquadmath.info %{buildroot}%{_infodir}/libquadmath%{bin_suffix}.info
%endif
#gzip -9 %{buildroot}%{_infodir}/*.info*

%if %{build_ada}
ln -sf gcc%{bin_suffix} %{buildroot}%{_prefix}/bin/gnatgcc%{bin_suffix}
%endif

cxxconfig="`find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h`"
for i in `find %{gcc_target_platform}/[36]*/libstdc++-v3/include -name c++config.h 2>/dev/null`; do
  if ! diff -up $cxxconfig $i; then
    cat > %{buildroot}%{_prefix}/include/c++/%{gcc_version}/%{gcc_target_platform}/bits/c++config.h <<EOF
#ifndef _CPP_CPPCONFIG_WRAPPER
#define _CPP_CPPCONFIG_WRAPPER 1
#include <bits/wordsize.h>
#if __WORDSIZE == 32
%ifarch %{multilib_64_archs}
`cat $(find %{gcc_target_platform}/32/libstdc++-v3/include -name c++config.h)`
%else
`cat $(find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h)`
%endif
#else
%ifarch %{multilib_64_archs}
`cat $(find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h)`
%else
`cat $(find %{gcc_target_platform}/64/libstdc++-v3/include -name c++config.h)`
%endif
#endif
#endif
EOF
    break
  fi
done

for f in `find %{buildroot}%{_prefix}/include/c++/%{gcc_version}/%{gcc_target_platform}/ -name c++config.h`; do
  for i in 1 2 4 8; do
    sed -i -e 's/#define _GLIBCXX_ATOMIC_BUILTINS_'$i' 1/#ifdef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_'$i'\
&\
#endif/' $f
  done
done

# Nuke bits/stdc++.h.gch dirs
# 1) there is no bits/stdc++.h header installed, so when gch file can't be
#    used, compilation fails
# 2) sometimes it is hard to match the exact options used for building
#    libstdc++-v3 or they aren't desirable
# 3) there are multilib issues, conflicts etc. with this
# 4) it is huge
# People can always precompile on their own whatever they want, but
# shipping this for everybody is unnecessary.
rm -rf %{buildroot}%{_prefix}/include/c++/%{gcc_version}/%{gcc_target_platform}/bits/stdc++.h.gch

%if %{build_libstdcxx_docs}
libstdcxx_doc_builddir=%{gcc_target_platform}/libstdc++-v3/doc/doxygen
mkdir -p ../rpm.doc/libstdc++-v3
cp -r -p ../libstdc++-v3/doc/html ../rpm.doc/libstdc++-v3/html
[ -d $libstdcxx_doc_builddir/html ] && mv $libstdcxx_doc_builddir/html ../rpm.doc/libstdc++-v3/html/api
mkdir -p %{buildroot}%{_mandir}
[ -f $libstdcxx_doc_builddir/man/man3 ] && mv $libstdcxx_doc_builddir/man/man3 %{buildroot}%{_mandir}/man3/
find ../rpm.doc/libstdc++-v3 -name \*~ | xargs rm
%endif

%ifarch sparcv9 sparc64
ln -f %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-gcc%{bin_suffix} \
  %{buildroot}%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc%{bin_suffix}
%endif
%ifarch ppc ppc64
ln -f %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-gcc%{bin_suffix} \
  %{buildroot}%{_prefix}/bin/ppc-%{_vendor}-%{_target_os}-gcc%{bin_suffix}
%endif

%ifarch sparcv9 ppc
FULLLPATH=$FULLPATH/lib32
%endif
%ifarch sparc64 ppc64
FULLLPATH=$FULLPATH/lib64
%endif
if [ -n "$FULLLPATH" ]; then
  mkdir -p $FULLLPATH
else
  FULLLPATH=$FULLPATH
fi

find %{buildroot} -name \*.la | xargs rm -f
%if %{build_java}
# gcj -static doesn't work properly anyway, unless using --whole-archive
# and saving 35MB is not bad.
find %{buildroot} -name libgcj.a -o -name libgtkpeer.a \
		     -o -name libgjsmalsa.a -o -name libgcj-tools.a -o -name libjvm.a \
		     -o -name libgij.a -o -name libgcj_bc.a -o -name libjavamath.a \
  | xargs rm -f

mv %{buildroot}%{_prefix}/lib/libgcj.spec $FULLPATH/
sed -i -e 's/lib: /&%%{static:%%eJava programs cannot be linked statically}/' \
  $FULLPATH/libgcj.spec
%endif

mv %{buildroot}%{_prefix}/lib/libgfortran.spec $FULLPATH/

mkdir -p %{buildroot}/%{_lib}
mv -f %{buildroot}%{_prefix}/%{_lib}/libgcc_s.so.1 %{buildroot}/%{_lib}/libgcc_s-%{gcc_version}-%{DATE}.so.1
chmod 755 %{buildroot}/%{_lib}/libgcc_s-%{gcc_version}-%{DATE}.so.1
ln -sf libgcc_s-%{gcc_version}-%{DATE}.so.1 %{buildroot}/%{_lib}/libgcc_s.so.1
ln -sf /%{_lib}/libgcc_s.so.1 $FULLPATH/libgcc_s.so
%ifarch sparcv9 ppc
ln -sf /lib64/libgcc_s.so.1 $FULLPATH/64/libgcc_s.so
%endif
%ifarch %{multilib_64_archs}
ln -sf /lib/libgcc_s.so.1 $FULLPATH/32/libgcc_s.so
%endif

%ifarch ppc
rm -f $FULLPATH/libgcc_s.so
echo '/* GNU ld script
   Use the shared library, but some functions are only in
   the static library, so try that secondarily.  */
OUTPUT_FORMAT(elf32-powerpc)
GROUP ( /lib/libgcc_s.so.1 libgcc.a )' > $FULLPATH/libgcc_s.so
%endif
%ifarch ppc64
rm -f $FULLPATH/32/libgcc_s.so
echo '/* GNU ld script
   Use the shared library, but some functions are only in
   the static library, so try that secondarily.  */
OUTPUT_FORMAT(elf32-powerpc)
GROUP ( /lib/libgcc_s.so.1 libgcc.a )' > $FULLPATH/32/libgcc_s.so
%endif

mv -f %{buildroot}%{_prefix}/%{_lib}/libgomp.spec $FULLPATH/

%if %{build_ada}
mv -f $FULLPATH/adalib/libgnarl-*.so %{buildroot}%{_prefix}/%{_lib}/
mv -f $FULLPATH/adalib/libgnat-*.so %{buildroot}%{_prefix}/%{_lib}/
rm -f $FULLPATH/adalib/libgnarl.so* $FULLPATH/adalib/libgnat.so*
%endif

mkdir -p %{buildroot}%{_prefix}/libexec/getconf
if gcc/xgcc -B gcc/ -E -dD -xc /dev/null | grep __LONG_MAX__.*2147483647; then
  ln -sf POSIX_V6_ILP32_OFF32 %{buildroot}%{_prefix}/libexec/getconf/default
else
  ln -sf POSIX_V6_LP64_OFF64 %{buildroot}%{_prefix}/libexec/getconf/default
fi

%if %{build_java}
if [ "%{_lib}" != "lib" ]; then
  mkdir -p %{buildroot}%{_prefix}/%{_lib}/pkgconfig
  sed '/^libdir/s/lib$/%{_lib}/' %{buildroot}%{_prefix}/lib/pkgconfig/libgcj-%{gcc_release}.pc \
      > %{buildroot}%{_prefix}/%{_lib}/pkgconfig/libgcj-%{gcc_release}.pc
fi
%endif

mkdir -p %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}
mv -f %{buildroot}%{_prefix}/%{_lib}/libstdc++*gdb.py* \
      %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/
pushd ../libstdc++-v3/python
for i in `find . -name \*.py`; do
  touch -r $i %{buildroot}%{_prefix}/share/gcc-%{gcc_version}/python/$i
done
touch -r hook.in %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/libstdc++*gdb.py
popd

pushd $FULLPATH
if [ "%{_lib}" = "lib" ]; then
ln -sf ../../../libobjc.so.%{objc_so_ver} libobjc.so
ln -sf ../../../libstdc++.so.6.*[0-9] libstdc++.so
ln -sf ../../../libgfortran.so.%{gfortran_so_ver}.* libgfortran.so
ln -sf ../../../libgomp.so.1.* libgomp.so
ln -sf ../../../libmudflap.so.0.* libmudflap.so
ln -sf ../../../libmudflapth.so.0.* libmudflapth.so
%if %{build_go}
ln -sf ../../../libgo.so.0.* libgo.so
%endif
%if %{build_libquadmath}
ln -sf ../../../libquadmath.so.0.* libquadmath.so
%endif
%if %{build_java}
ln -sf ../../../libgcj.so.%{gcj_so_ver}.* libgcj.so
ln -sf ../../../libgcj-tools.so.%{gcj_so_ver}.* libgcj-tools.so
ln -sf ../../../libgij.so.%{gcj_so_ver}.* libgij.so
%endif
else
ln -sf ../../../../%{_lib}/libobjc.so.%{objc_so_ver} libobjc.so
ln -sf ../../../../%{_lib}/libstdc++.so.6.*[0-9] libstdc++.so
ln -sf ../../../../%{_lib}/libgfortran.so.%{gfortran_so_ver}.* libgfortran.so
ln -sf ../../../../%{_lib}/libgomp.so.1.* libgomp.so
ln -sf ../../../../%{_lib}/libmudflap.so.0.* libmudflap.so
ln -sf ../../../../%{_lib}/libmudflapth.so.0.* libmudflapth.so
%if %{build_go}
ln -sf ../../../../%{_lib}/libgo.so.0.* libgo.so
%endif
%if %{build_libquadmath}
ln -sf ../../../../%{_lib}/libquadmath.so.0.* libquadmath.so
%endif
%if %{build_java}
ln -sf ../../../../%{_lib}/libgcj.so.%{gcj_so_ver}.* libgcj.so
ln -sf ../../../../%{_lib}/libgcj-tools.so.%{gcj_so_ver}.* libgcj-tools.so
ln -sf ../../../../%{_lib}/libgij.so.%{gcj_so_ver}.* libgij.so
%endif
fi
%if %{build_java}
mv -f %{buildroot}%{_prefix}/%{_lib}/libgcj_bc.so $FULLLPATH/
%endif
mv -f %{buildroot}%{_prefix}/%{_lib}/libstdc++.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libsupc++.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libgfortran.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libobjc.*a .
mv -f %{buildroot}%{_prefix}/%{_lib}/libgomp.*a .
mv -f %{buildroot}%{_prefix}/%{_lib}/libmudflap{,th}.*a $FULLLPATH/
%if %{build_libquadmath}
mv -f %{buildroot}%{_prefix}/%{_lib}/libquadmath.*a $FULLLPATH/
%endif
%if %{build_go}
mv -f %{buildroot}%{_prefix}/%{_lib}/libgo.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libgobegin.*a $FULLLPATH/
%endif

%if %{build_ada}
%ifarch sparcv9 ppc
rm -rf $FULLPATH/64/ada{include,lib}
%endif
%ifarch %{multilib_64_archs}
rm -rf $FULLPATH/32/ada{include,lib}
%endif
if [ "$FULLPATH" != "$FULLLPATH" ]; then
mv -f $FULLPATH/ada{include,lib} $FULLLPATH/
pushd $FULLLPATH/adalib
if [ "%{_lib}" = "lib" ]; then
ln -sf ../../../../../libgnarl-*.so libgnarl.so
ln -sf ../../../../../libgnarl-*.so libgnarl-4.6.so
ln -sf ../../../../../libgnat-*.so libgnat.so
ln -sf ../../../../../libgnat-*.so libgnat-4.6.so
else
ln -sf ../../../../../../%{_lib}/libgnarl-*.so libgnarl.so
ln -sf ../../../../../../%{_lib}/libgnarl-*.so libgnarl-4.6.so
ln -sf ../../../../../../%{_lib}/libgnat-*.so libgnat.so
ln -sf ../../../../../../%{_lib}/libgnat-*.so libgnat-4.6.so
fi
popd
else
pushd $FULLPATH/adalib
if [ "%{_lib}" = "lib" ]; then
ln -sf ../../../../libgnarl-*.so libgnarl.so
ln -sf ../../../../libgnarl-*.so libgnarl-4.6.so
ln -sf ../../../../libgnat-*.so libgnat.so
ln -sf ../../../../libgnat-*.so libgnat-4.6.so
else
ln -sf ../../../../../%{_lib}/libgnarl-*.so libgnarl.so
ln -sf ../../../../../%{_lib}/libgnarl-*.so libgnarl-4.6.so
ln -sf ../../../../../%{_lib}/libgnat-*.so libgnat.so
ln -sf ../../../../../%{_lib}/libgnat-*.so libgnat-4.6.so
fi
popd
fi
%endif

%ifarch sparcv9 ppc
ln -sf ../../../../../lib64/libobjc.so.%{objc_so_ver} 64/libobjc.so
ln -sf ../`echo ../../../../lib64/libstdc++.so.6.*[0-9] | sed s~/../lib64/~/~` 32/libstdc++.so
ln -sf ../`echo ../../../../lib/libgfortran.so.%{gfortran_so_ver}.* | sed s~/lib/~/lib64/~` 64/libgfortran.so
ln -sf ../`echo ../../../../lib/libgomp.so.1.* | sed s~/lib/~/lib64/~` 64/libgomp.so
rm -f libmudflap.so libmudflapth.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libmudflap.so.0.* | sed 's,^.*libm,libm,'`' )' > libmudflap.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libmudflapth.so.0.* | sed 's,^.*libm,libm,'`' )' > libmudflapth.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libmudflap.so.0.* | sed 's,^.*libm,libm,'`' )' > 64/libmudflap.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libmudflapth.so.0.* | sed 's,^.*libm,libm,'`' )' > 64/libmudflapth.so
%if %{build_go}
rm -f libgo.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libgo.so.0.* | sed 's,^.*libg,libg,'`' )' > libgo.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libgo.so.0.* | sed 's,^.*libg,libg,'`' )' > 64/libgo.so
%endif
%if %{build_libquadmath}
rm -f libquadmath.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > libquadmath.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > 64/libquadmath.so
%endif
%if %{build_java}
ln -sf ../`echo ../../../../lib/libgcj.so.%{gcj_so_ver}.* | sed s~/lib/~/lib64/~` 64/libgcj.so
ln -sf ../`echo ../../../../lib/libgcj-tools.so.%{gcj_so_ver}.* | sed s~/lib/~/lib64/~` 64/libgcj-tools.so
ln -sf ../`echo ../../../../lib/libgij.so.%{gcj_so_ver}.* | sed s~/lib/~/lib64/~` 64/libgij.so
ln -sf lib32/libgcj_bc.so libgcj_bc.so
ln -sf ../lib64/libgcj_bc.so 64/libgcj_bc.so
%endif
ln -sf lib32/libgfortran.a libgfortran.a
ln -sf ../lib64/libgfortran.a 64/libgfortran.a
mv -f %{buildroot}%{_prefix}/lib64/libobjc.*a 64/
mv -f %{buildroot}%{_prefix}/lib64/libgomp.*a 64/
ln -sf lib32/libstdc++.a libstdc++.a
ln -sf ../lib64/libstdc++.a 64/libstdc++.a
ln -sf lib32/libsupc++.a libsupc++.a
ln -sf ../lib64/libsupc++.a 64/libsupc++.a
ln -sf lib32/libmudflap.a libmudflap.a
ln -sf ../lib64/libmudflap.a 64/libmudflap.a
ln -sf lib32/libmudflapth.a libmudflapth.a
ln -sf ../lib64/libmudflapth.a 64/libmudflapth.a
%if %{build_libquadmath}
ln -sf lib32/libquadmath.a libquadmath.a
ln -sf ../lib64/libquadmath.a 64/libquadmath.a
%endif
%if %{build_go}
ln -sf lib32/libgo.a libgo.a
ln -sf ../lib64/libgo.a 64/libgo.a
ln -sf lib32/libgobegin.a libgobegin.a
ln -sf ../lib64/libgobegin.a 64/libgobegin.a
%endif
%if %{build_ada}
ln -sf lib32/adainclude adainclude
ln -sf ../lib64/adainclude 64/adainclude
ln -sf lib32/adalib adalib
ln -sf ../lib64/adalib 64/adalib
%endif
%endif
%ifarch %{multilib_64_archs}
mkdir -p 32
ln -sf ../../../../libobjc.so.%{objc_so_ver} 32/libobjc.so
ln -sf ../`echo ../../../../lib64/libstdc++.so.6.*[0-9] | sed s~/../lib64/~/~` 32/libstdc++.so
ln -sf ../`echo ../../../../lib64/libgfortran.so.%{gfortran_so_ver}.* | sed s~/../lib64/~/~` 32/libgfortran.so
ln -sf ../`echo ../../../../lib64/libgomp.so.1.* | sed s~/../lib64/~/~` 32/libgomp.so
ln -sf ../`echo ../../../../lib64/libmudflap.so.0.* | sed s~/../lib64/~/~` 32/libmudflap.so
ln -sf ../`echo ../../../../lib64/libmudflapth.so.0.* | sed s~/../lib64/~/~` 32/libmudflapth.so
rm -f libmudflap.so libmudflapth.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libmudflap.so.0.* | sed 's,^.*libm,libm,'`' )' > libmudflap.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libmudflapth.so.0.* | sed 's,^.*libm,libm,'`' )' > libmudflapth.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libmudflap.so.0.* | sed 's,^.*libm,libm,'`' )' > 32/libmudflap.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libmudflapth.so.0.* | sed 's,^.*libm,libm,'`' )' > 32/libmudflapth.so
%if %{build_go}
rm -f libgo.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libgo.so.0.* | sed 's,^.*libg,libg,'`' )' > libgo.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libgo.so.0.* | sed 's,^.*libg,libg,'`' )' > 32/libgo.so
%endif
%if %{build_libquadmath}
rm -f libquadmath.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > libquadmath.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > 32/libquadmath.so
%endif
%if %{build_java}
ln -sf ../`echo ../../../../lib64/libgcj.so.%{gcj_so_ver}.* | sed s~/../lib64/~/~` 32/libgcj.so
ln -sf ../`echo ../../../../lib64/libgcj-tools.so.%{gcj_so_ver}.* | sed s~/../lib64/~/~` 32/libgcj-tools.so
ln -sf ../`echo ../../../../lib64/libgij.so.%{gcj_so_ver}.* | sed s~/../lib64/~/~` 32/libgij.so
%endif
mv -f %{buildroot}%{_prefix}/lib/libobjc.*a 32/
mv -f %{buildroot}%{_prefix}/lib/libgomp.*a 32/
%endif
%ifarch sparc64 ppc64
ln -sf ../lib32/libgfortran.a 32/libgfortran.a
ln -sf lib64/libgfortran.a libgfortran.a
ln -sf ../lib32/libstdc++.a 32/libstdc++.a
ln -sf lib64/libstdc++.a libstdc++.a
ln -sf ../lib32/libsupc++.a 32/libsupc++.a
ln -sf lib64/libsupc++.a libsupc++.a
ln -sf ../lib32/libmudflap.a 32/libmudflap.a
ln -sf lib64/libmudflap.a libmudflap.a
ln -sf ../lib32/libmudflapth.a 32/libmudflapth.a
ln -sf lib64/libmudflapth.a libmudflapth.a
%if %{build_libquadmath}
ln -sf ../lib32/libquadmath.a 32/libquadmath.a
ln -sf lib64/libquadmath.a libquadmath.a
%endif
%if %{build_go}
ln -sf ../lib32/libgo.a 32/libgo.a
ln -sf lib64/libgo.a libgo.a
ln -sf ../lib32/libgobegin.a 32/libgobegin.a
ln -sf lib64/libgobegin.a libgobegin.a
%endif
%if %{build_java}
ln -sf ../lib32/libgcj_bc.so 32/libgcj_bc.so
ln -sf lib64/libgcj_bc.so libgcj_bc.so
%endif
%else
%if %{build_ada}
ln -sf ../lib32/adainclude 32/adainclude
ln -sf lib64/adainclude adainclude
ln -sf ../lib32/adalib 32/adalib
ln -sf lib64/adalib adalib
%endif
%ifarch %{multilib_64_archs}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libgfortran.a 32/libgfortran.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libstdc++.a 32/libstdc++.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libsupc++.a 32/libsupc++.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libmudflap.a 32/libmudflap.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libmudflapth.a 32/libmudflapth.a
%if %{build_libquadmath}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libquadmath.a 32/libquadmath.a
%endif
%if %{build_go}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libgo.a 32/libgo.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libgobegin.a 32/libgobegin.a
%endif
%if %{build_java}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libgcj_bc.so 32/libgcj_bc.so
%endif
%if %{build_ada}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/adainclude 32/adainclude
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/adalib 32/adalib
%endif
%endif
%endif

# Strip debug info from Fortran/ObjC/Java static libraries
strip -g `find . \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		    -o -name libmudflap.a -o -name libmudflapth.a \
		    -o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		    -o -name libgo.a \) -a -type f`
popd
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgfortran.so.%{gfortran_so_ver}.*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgomp.so.1.*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libmudflap{,th}.so.0.*
%if %{build_libquadmath}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libquadmath.so.0.*
%endif
%if %{build_go}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgo.so.0.*
%endif
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libobjc.so.%{objc_so_ver}.*

%if %{build_ada}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgnarl*so*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgnat*so*
%endif

mv $FULLPATH/include-fixed/syslimits.h $FULLPATH/include/syslimits.h
mv $FULLPATH/include-fixed/limits.h $FULLPATH/include/limits.h
for h in `find $FULLPATH/include -name \*.h`; do
  if grep -q 'It has been auto-edited by fixincludes from' $h; then
    rh=`grep -A2 'It has been auto-edited by fixincludes from' $h | tail -1 | sed 's|^.*"\(.*\)".*$|\1|'`
    diff -up $rh $h || :
    rm -f $h
  fi
done

%if "%{bin_suffix}" == ""
install -m755 %{SOURCE8000} %{buildroot}/%{_prefix}/bin/c89
install -m755 %{SOURCE8001} %{buildroot}/%{_prefix}/bin/c99
%endif

mkdir -p %{buildroot}%{_prefix}/sbin
gcc -static -Os %{SOURCE1} -o %{buildroot}%{_prefix}/sbin/libgcc_post_upgrade
strip %{buildroot}%{_prefix}/sbin/libgcc_post_upgrade

cd ..
%find_lang gcc%{bin_suffix}
%find_lang cpplib%{bin_suffix}

# Remove binaries we will not be including, so that they don't end up in
# gcc-debuginfo
rm -f %{buildroot}%{_prefix}/%{_lib}/libiberty.a
rm -f $FULLEPATH/install-tools/{mkheaders,fixincl}
rm -f %{buildroot}%{_prefix}/lib/{32,64}/libiberty.a
rm -f %{buildroot}%{_prefix}/%{_lib}/libssp*

%ifarch %{multilib_64_archs}
# Remove libraries for the other arch on multilib arches
rm -f %{buildroot}%{_prefix}/lib/lib*.so*
rm -f %{buildroot}%{_prefix}/lib/lib*.a
%if %{build_go}
rm -rf %{buildroot}%{_prefix}/lib/go/%{gcc_version}/%{gcc_target_platform}
%ifnarch sparc64 ppc64
ln -sf %{multilib_32_arch}-%{_vendor}-%{_target_os} %{buildroot}%{_prefix}/lib/go/%{gcc_version}/%{gcc_target_platform}
%endif
%endif
%else
%ifarch sparcv9 ppc
rm -f %{buildroot}%{_prefix}/lib64/lib*.so*
rm -f %{buildroot}%{_prefix}/lib64/lib*.a
%if %{build_go}
rm -rf %{buildroot}%{_prefix}/lib64/go/%{gcc_version}/%{gcc_target_platform}
%endif
%endif
%endif

%if %{build_java}
mkdir -p %{buildroot}%{_prefix}/share/java/gcj-endorsed \
	 %{buildroot}%{_prefix}/%{_lib}/%{gcj_sub_dir}/classmap.db.d
chmod 755 %{buildroot}%{_prefix}/share/java/gcj-endorsed \
	  %{buildroot}%{_prefix}/%{_lib}/%{gcj_sub_dir} \
	  %{buildroot}%{_prefix}/%{_lib}/%{gcj_sub_dir}/classmap.db.d
touch %{buildroot}%{_prefix}/%{_lib}/%{gcj_sub_dir}/classmap.db

%if "%{gcj_sub_dir}" != "gcj-%{gcc_version}"
# for java-1.5.0-gcj
ln -sf %{gcj_sub_dir} %{buildroot}%{_prefix}/%{_lib}/gcj-%{gcc_version}
%endif
%endif

#install -m644 %{SOURCE3} %{buildroot}%{_mandir}/man1/protoize%{bin_suffix}.1
#echo '.so man1/protoize.1' > %{buildroot}%{_mandir}/man1/unprotoize%{bin_suffix}.1
#chmod 644 %{buildroot}%{_mandir}/man1/unprotoize%{bin_suffix}.1


%if %{build_java_tar}
mkdir -p %{buildroot}%{_libexecdir}/gcc-bootstrap-java
install -m644 libjava-classes-%{version}-%{release}.tar.bz2 %{buildroot}%{_libexecdir}/gcc-bootstrap-java/
ln -sf libjava-classes-%{version}-%{release}.tar.bz2 %{buildroot}%{_libexecdir}/gcc-bootstrap-java/libjava-classes.tar.bz2
%endif

%clean
rm -rf %{buildroot}

%post
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gcc%{bin_suffix}.info || :
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gccint%{bin_suffix}.info || :
[ -x %{_sbindir}/update-ccache.sh ] && %{_sbindir}/update-ccache.sh || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gcc%{bin_suffix}.info || :
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gccint%{bin_suffix}.info || :
fi

%postun
[ -x %{_sbindir}/update-ccache.sh ] && %{_sbindir}/update-ccache.sh || :

%post c++
[ -x %{_sbindir}/update-ccache.sh ] && %{_sbindir}/update-ccache.sh || :

%postun c++
[ -x %{_sbindir}/update-ccache.sh ] && %{_sbindir}/update-ccache.sh || :

%post -n cpp%{pkg_suffix}
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/cpp%{bin_suffix}.info || :
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/cppinternals%{bin_suffix}.info || :

%preun -n cpp%{pkg_suffix}
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/cpp%{bin_suffix}.info || :
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/cppinternals%{bin_suffix}.info || :
fi

%post gfortran
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gfortran%{bin_suffix}.info || :

%preun gfortran
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gfortran%{bin_suffix}.info || :
fi

%if %{build_java}
%post java
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gcj%{bin_suffix}.info || :

%preun java
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gcj%{bin_suffix}.info || :
fi
%endif

%post gnat
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gnat_rm%{bin_suffix}.info || :
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gnat_ugn_unw%{bin_suffix}.info || :
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/gnat-style%{bin_suffix}.info || :

%preun gnat
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gnat_rm%{bin_suffix}.info || :
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gnat_ugn_unw%{bin_suffix}.info || :
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gnat-style%{bin_suffix}.info || :
fi

%post -n libgomp%{pkg_suffix}
/sbin/ldconfig
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/libgomp%{bin_suffix}.info || :

%postun -n libgomp%{pkg_suffix}
/sbin/ldconfig

%preun -n libgomp%{pkg_suffix}
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/libgomp%{bin_suffix}.info || :
fi

# Because glibc Prereq's libgcc and /sbin/ldconfig
# comes from glibc, it might not exist yet when
# libgcc is installed
%post -n libgcc%{pkg_suffix} -p %{_prefix}/sbin/libgcc_post_upgrade

%postun -n libgcc%{pkg_suffix}
/sbin/ldconfig

%post -n libstdc++%{pkg_suffix}
/sbin/ldconfig

%postun -n libstdc++%{pkg_suffix}
/sbin/ldconfig

%post -n libobjc%{pkg_suffix}
/sbin/ldconfig

%postun -n libobjc%{pkg_suffix}
/sbin/ldconfig

%if %{build_java}
%post -n libgcj%{pkg_suffix}
/sbin/ldconfig

%postun -n libgcj%{pkg_suffix}
/sbin/ldconfig

%post -n libgcj%{pkg_suffix}-devel
/sbin/ldconfig

%postun -n libgcj%{pkg_suffix}-devel
/sbin/ldconfig
%endif

%post -n libgfortran%{pkg_suffix}
/sbin/ldconfig

%postun -n libgfortran%{pkg_suffix}
/sbin/ldconfig

%post -n libgnat%{pkg_suffix}
/sbin/ldconfig

%postun -n libgnat%{pkg_suffix}
/sbin/ldconfig

%post -n libmudflap%{pkg_suffix}
/sbin/ldconfig

%postun -n libmudflap%{pkg_suffix}
/sbin/ldconfig

%if %{build_libquadmath}
%post -n libquadmath%{pkg_suffix}
/sbin/ldconfig
/sbin/install-info \
    --info-dir=%{_infodir} %{_infodir}/libquadmath%{bin_suffix}.info || :

%preun -n libquadmath%{pkg_suffix}
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/libquadmath%{bin_suffix}.info || :
fi

%postun -n libquadmath%{pkg_suffix} -p /sbin/ldconfig
%endif

%if %{build_go}
%post -n libgo%{pkg_suffix} -p /sbin/ldconfig

%postun -n libgo%{pkg_suffix} -p /sbin/ldconfig
%endif

%files -f gcc%{bin_suffix}.lang
%defattr(-,root,root)
%{_prefix}/bin/cc%{bin_suffix}
%if "%{bin_suffix}" == ""
%{_prefix}/bin/c89
%{_prefix}/bin/c99
%endif
%{_prefix}/bin/gcc%{bin_suffix}
#%{_prefix}/bin/gccbug%{bin_suffix}
%{_prefix}/bin/gcov%{bin_suffix}
#%{_prefix}/bin/protoize%{bin_suffix}
#%{_prefix}/bin/unprotoize%{bin_suffix}
%ifarch ppc
%{_prefix}/bin/%{_target_platform}-gcc%{bin_suffix}
%endif
%ifarch sparc64 sparcv9
%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc%{bin_suffix}
%endif
%ifarch ppc64
%{_prefix}/bin/ppc-%{_vendor}-%{_target_os}-gcc%{bin_suffix}
%endif
%{_prefix}/bin/%{gcc_target_platform}-gcc%{bin_suffix}
%{_prefix}/bin/%{gcc_target_platform}-gcc-%{version}
%{_mandir}/man1/gcc%{bin_suffix}.1*
%{_mandir}/man1/gcov%{bin_suffix}.1*
#%{_mandir}/man1/protoize%{bin_suffix}.1*
#%{_mandir}/man1/unprotoize%{bin_suffix}.1*
%{_infodir}/gcc*
%if "%{bin_suffix}" == ""
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%endif
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/*.h
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/quadmath*.h
%if %{build_java}
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/j*.h
%endif
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/mf-runtime.h
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/ssp
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/ssp/*.h
%if !%{build_plugin}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/plugin
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/plugin
%endif
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/collect2
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/lto-wrapper
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/lto1
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/liblto_plugin.*
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcc_s.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgomp.spec
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgomp.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgomp.so
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcc_s.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgomp.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgomp.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libmudflap.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libmudflapth.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libmudflap.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libmudflapth.so
%if %{build_libquadmath}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libquadmath.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libquadmath.so
%endif
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcc_s.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgomp.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgomp.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libmudflap.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libmudflapth.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libmudflap.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libmudflapth.so
%if %{build_libquadmath}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libquadmath.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libquadmath.so
%endif
%endif
%ifarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflap.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflapth.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflap.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflapth.so
%if %{build_libquadmath}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libquadmath.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libquadmath.so
%endif
%endif
%dir %{_prefix}/libexec/getconf
%{_prefix}/libexec/getconf/default
%doc gcc/README* rpm.doc/changelogs/gcc/ChangeLog* gcc/COPYING*

%files -n cpp%{pkg_suffix} -f cpplib%{bin_suffix}.lang
%defattr(-,root,root)
/lib/cpp%{bin_suffix}
%{_prefix}/bin/cpp%{bin_suffix}
%{_prefix}/bin/%{gcc_target_platform}-cpp%{bin_suffix}
%{_mandir}/man1/cpp%{bin_suffix}.1*
%{_infodir}/cpp*
%if "%{bin_suffix}" == ""
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%endif
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/cc1

%files -n libgcc%{pkg_suffix}
%defattr(-,root,root)
/%{_lib}/libgcc_s-%{gcc_version}-%{DATE}.so.1
/%{_lib}/libgcc_s.so.1
%{_prefix}/sbin/libgcc_post_upgrade
%doc gcc/COPYING.LIB

%files c++
%defattr(-,root,root)
%{_prefix}/bin/%{gcc_target_platform}-*++%{bin_suffix}
%{_prefix}/bin/g++%{bin_suffix}
%{_prefix}/bin/c++%{bin_suffix}
%{_mandir}/man1/g++%{bin_suffix}.1*
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/cc1plus
%doc rpm.doc/changelogs/gcc/cp/ChangeLog*

%files -n libstdc++%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/%{_lib}/libstdc++.so.6*[^so]
%dir %{_datadir}/gdb
%dir %{_datadir}/gdb/auto-load
%dir %{_datadir}/gdb/auto-load/%{_prefix}
%dir %{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/
%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/libstdc*gdb.py*
%dir %{_prefix}/share/gcc-%{gcc_version}
%{_prefix}/share/gcc-%{gcc_version}/python

%files -n libstdc++%{pkg_suffix}-devel
%defattr(-,root,root)
%if "%{bin_suffix}" == ""
%dir %{_prefix}/include/c++
%endif
%dir %{_prefix}/include/c++/%{gcc_version}
%{_prefix}/include/c++/%{gcc_version}/[^gj]*
%if %{build_java}
%exclude %{_prefix}/include/c++/%{gcc_version}/sun
%exclude %{_prefix}/include/c++/%{gcc_version}/org
%endif
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libstdc++.so
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libstdc++.so
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libstdc++.so

%files -n libstdc++%{pkg_suffix}-static
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libsupc++.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libsupc++.a
%endif
%ifarch sparcv9 ppc
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libsupc++.a
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libsupc++.a
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libsupc++.a

%if %{build_libstdcxx_docs}
%files -n libstdc++%{pkg_suffix}-docs
%defattr(-,root,root)
%if "%{build_java}" != "0"
%{_mandir}/man3/*
%exclude %{_mandir}/man3/ffi*
%endif
%doc rpm.doc/libstdc++-v3/html
%doc rpm.doc/changelogs/libstdc++-v3/ChangeLog* libstdc++-v3/README* libstdc++-v3/doc/html/
%endif

%files objc
%defattr(-,root,root)
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/objc
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/cc1obj
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libobjc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libobjc.so
%ifarch sparcv9 ppc
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libobjc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libobjc.so
%endif
%ifarch %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libobjc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libobjc.so
%endif
%doc rpm.doc/objc/*
%doc libobjc/THREADS* rpm.doc/changelogs/libobjc/ChangeLog*

%files objc++
%defattr(-,root,root)
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/cc1objplus

%files -n libobjc%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/%{_lib}/libobjc.so.%{objc_so_ver}*

%files gfortran
%defattr(-,root,root)
%{_prefix}/bin/gfortran%{bin_suffix}
%{_prefix}/bin/%{gcc_target_platform}-gfortran%{bin_suffix}
%{_prefix}/bin/f95%{bin_suffix}
%{_mandir}/man1/gfortran%{bin_suffix}.1*
%{_infodir}/gfortran*
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/finclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/finclude/omp_lib.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/finclude/omp_lib.f90
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/finclude/omp_lib.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/finclude/omp_lib_kinds.mod
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/f951
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgfortran.spec
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgfortranbegin.a
%ifarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgfortran.a
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgfortran.so
%ifarch sparcv9 ppc
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgfortranbegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgfortran.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgfortran.so
%endif
%ifarch %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgfortranbegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgfortran.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgfortran.so
%endif
%doc rpm.doc/gfortran/*

%files -n libgfortran%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/%{_lib}/libgfortran.so.%{gfortran_so_ver}*

%files -n libgfortran%{pkg_suffix}-static
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libgfortran.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libgfortran.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgfortran.a
%endif

%if %{build_java}
%files java
%defattr(-,root,root)
%{_prefix}/bin/gcj%{bin_suffix}
%{_prefix}/bin/%{gcc_target_platform}-gcj%{bin_suffix}
%{_prefix}/bin/gjavah%{bin_suffix}
%{_prefix}/bin/gcjh%{bin_suffix}
%{_prefix}/bin/jcf-dump%{bin_suffix}
%{_mandir}/man1/gcj%{bin_suffix}.1*
%{_mandir}/man1/gcjh%{bin_suffix}.1*
%{_mandir}/man1/gjavah%{bin_suffix}.1*
%{_mandir}/man1/jcf-dump%{bin_suffix}.1*
%{_infodir}/gcj*
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/jc1
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/ecj1
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/jvgenmain
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcj.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcj-tools.so
%ifarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcj_bc.so
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgij.so
%ifarch sparc ppc
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcj.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcj-tools.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgcj_bc.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgij.so
%endif
%ifarch %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcj.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcj-tools.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgcj_bc.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgij.so
%endif
%doc rpm.doc/changelogs/gcc/java/ChangeLog*

%files -n libgcj%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/bin/jv-convert%{bin_suffix}
%{_prefix}/bin/gij%{bin_suffix}
%{_prefix}/bin/gjar%{bin_suffix}
#%{_prefix}/bin/fastjar
%{_prefix}/bin/gnative2ascii%{bin_suffix}
#%{_prefix}/bin/grepjar
%{_prefix}/bin/grmic%{bin_suffix}
%{_prefix}/bin/grmid%{bin_suffix}
%{_prefix}/bin/grmiregistry%{bin_suffix}
%{_prefix}/bin/gtnameserv%{bin_suffix}
%{_prefix}/bin/gkeytool%{bin_suffix}
%{_prefix}/bin/gorbd%{bin_suffix}
%{_prefix}/bin/gserialver%{bin_suffix}
%{_prefix}/bin/gcj-dbtool%{bin_suffix}
%{_prefix}/bin/gc-analyze%{bin_suffix}
#%{_prefix}/bin/gnative2ascii%{bin_suffix}
%{_prefix}/bin/gjarsigner%{bin_suffix}
#%{_mandir}/man1/fastjar.1*
#%{_mandir}/man1/grepjar.1*
%{_mandir}/man1/gjar%{bin_suffix}.1*
%{_mandir}/man1/gjarsigner%{bin_suffix}.1*
%{_mandir}/man1/jv-convert%{bin_suffix}.1*
%{_mandir}/man1/gij%{bin_suffix}.1*
%{_mandir}/man1/gnative2ascii%{bin_suffix}.1*
%{_mandir}/man1/grmic%{bin_suffix}.1*
%{_mandir}/man1/grmiregistry%{bin_suffix}.1*
%{_mandir}/man1/gcj-dbtool%{bin_suffix}.1*
%{_mandir}/man1/gkeytool%{bin_suffix}.1*
%{_mandir}/man1/gorbd%{bin_suffix}.1*
%{_mandir}/man1/grmid%{bin_suffix}.1*
%{_mandir}/man1/gserialver%{bin_suffix}.1*
%{_mandir}/man1/gtnameserv%{bin_suffix}.1*
%{_mandir}/man1/gc-analyze%{bin_suffix}.1*
%{_mandir}/man1/gnative2ascii%{bin_suffix}.1*
#%{_infodir}/fastjar*
#%{_infodir}/cp-tools*
%{_prefix}/%{_lib}/libgcj.so.*
%{_prefix}/%{_lib}/libgcj-tools.so.*
%{_prefix}/%{_lib}/libgcj_bc.so.*
%{_prefix}/%{_lib}/libgij.so.*
%dir %{_prefix}/%{_lib}/%{gcj_sub_dir}
%{_prefix}/%{_lib}/%{gcj_sub_dir}/libgtkpeer.so
%{_prefix}/%{_lib}/%{gcj_sub_dir}/libgjsmalsa.so
%{_prefix}/%{_lib}/%{gcj_sub_dir}/libjawt.so
%{_prefix}/%{_lib}/%{gcj_sub_dir}/libjvm.so
%{_prefix}/%{_lib}/%{gcj_sub_dir}/libjavamath.so
%if "%{gcj_sub_dir}" != "gcj-%{gcc_version}"
%{_prefix}/%{_lib}/gcj-%{gcc_version}
%endif
%{_prefix}/share/java/libgcj-%{version}.jar
%if "%{bin_suffix}" == ""
%dir %{_prefix}/share/java/gcj-endorsed
%dir %{_prefix}/%{_lib}/security
%config(noreplace) %{_prefix}/%{_lib}/security/classpath.security
%{_prefix}/%{_lib}/logging.properties
%endif
%dir %{_prefix}/%{_lib}/%{gcj_sub_dir}/classmap.db.d
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) %{_prefix}/%{_lib}/%{gcj_sub_dir}/classmap.db

%files -n libgcj%{pkg_suffix}-devel
%defattr(-,root,root)
#%{_prefix}/bin/addr2name.awk%{bin_suffix}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/gcj
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/jawt.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/jawt_md.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/jni.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/jni_md.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/jvmpi.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcj.spec
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libgcj_bc.so
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libgcj_bc.so
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgcj_bc.so
%endif
%dir %{_prefix}/include/c++
%dir %{_prefix}/include/c++/%{gcc_version}
%{_prefix}/include/c++/%{gcc_version}/[gj]*
%{_prefix}/include/c++/%{gcc_version}/org
%{_prefix}/include/c++/%{gcc_version}/sun
%{_prefix}/%{_lib}/pkgconfig/libgcj-*.pc
%doc rpm.doc/boehm-gc/*
%doc rpm.doc/libjava/*
%endif

%if %{build_java}
%files -n libgcj%{pkg_suffix}-src
%defattr(-,root,root)
%{_prefix}/share/java/src*.zip
%{_prefix}/share/java/libgcj-tools-%{version}.jar
%endif

%if %{build_ada}
%files gnat
%defattr(-,root,root,-)
%{_prefix}/bin/gnat
%{_prefix}/bin/gnat[^i]*
%{_infodir}/gnat*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/adalib
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/adalib
%endif
%ifarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib
%endif
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/gnat1
%doc rpm.doc/changelogs/gcc/ada/ChangeLog*

%files -n libgnat%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/%{_lib}/libgnat-*.so
%{_prefix}/%{_lib}/libgnarl-*.so

%files -n libgnat%{pkg_suffix}-devel
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adalib
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adalib/libgnat.a
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adalib/libgnarl.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adalib
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adalib/libgnat.a
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adalib/libgnarl.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib/libgnat.a
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib/libgnarl.a
%endif

%files -n libgnat%{pkg_suffix}-static
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adalib/libgnat.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/adalib/libgnarl.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adalib/libgnat.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/adalib/libgnarl.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib/libgnat.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/adalib/libgnarl.a
%endif
%endif

%files -n libgomp%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/%{_lib}/libgomp.so.1*
%doc rpm.doc/changelogs/libgomp/ChangeLog*
%{_infodir}/libgomp*

%files -n libmudflap%{pkg_suffix}
%defattr(-,root,root)
%{_prefix}/%{_lib}/libmudflap.so.0*
%{_prefix}/%{_lib}/libmudflapth.so.0*

%files -n libmudflap%{pkg_suffix}-devel
%defattr(-,root,root)
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/mf-runtime.h
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflap.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflapth.so
%endif
%doc rpm.doc/changelogs/libmudflap/ChangeLog*

%files -n libmudflap%{pkg_suffix}-static
%defattr(-,root,root)
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libmudflap.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libmudflapth.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libmudflap.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libmudflapth.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflap.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libmudflapth.a
%endif
%doc rpm.doc/changelogs/libmudflap/ChangeLog*

%if %{build_java_tar}
%files -n gcc-bootstrap-java
%defattr(-,root,root)
%dir %{_libexecdir}/gcc-bootstrap-java
%{_libexecdir}/gcc-bootstrap-java/libjava-classes*.tar.bz2
%endif

%if %{build_libquadmath}
%files -n libquadmath%{pkg_suffix}
%defattr(-,root,root,-)
%{_prefix}/%{_lib}/libquadmath.so.0*
%{_infodir}/libquadmath%{bin_suffix}.info*
%doc rpm.doc/libquadmath/COPYING*

%files -n libquadmath%{pkg_suffix}-devel
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/quadmath.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/include/quadmath_weak.h
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libquadmath.so
%endif
#%doc rpm.doc/libquadmath/ChangeLog*

%files -n libquadmath%{pkg_suffix}-static
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libquadmath.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libquadmath.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libquadmath.a
%endif
%endif

%if %{build_go}
%files go%{pkg_suffix}
%defattr(-,root,root,-)
%{_prefix}/bin/gccgo%{bin_suffix}
%{_mandir}/man1/gccgo.1*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_version}/go1
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgo.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgo.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/64/libgobegin.a
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgo.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgo.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/libgobegin.a
%endif
%ifarch sparcv9 ppc %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgo.so
%endif
%ifarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgo.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgobegin.a
%endif
%doc rpm.doc/go/*

%files -n libgo%{pkg_suffix}
%defattr(-,root,root,-)
%{_prefix}/%{_lib}/libgo.so.0*
%doc rpm.doc/libgo/*

%files -n libgo%{pkg_suffix}-devel
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%dir %{_prefix}/%{_lib}/go
%dir %{_prefix}/%{_lib}/go/%{gcc_version}
%{_prefix}/%{_lib}/go/%{gcc_version}/%{gcc_target_platform}
%ifarch %{multilib_64_archs}
%ifnarch sparc64 ppc64
%dir %{_prefix}/lib/go
%dir %{_prefix}/lib/go/%{gcc_version}
%{_prefix}/lib/go/%{gcc_version}/%{gcc_target_platform}
%endif
%endif
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libgobegin.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libgobegin.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgo.so
%endif

%files -n libgo%{pkg_suffix}-static
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib32/libgo.a
%endif
%ifarch sparc64 ppc64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/lib64/libgo.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/libgo.a
%endif
%endif

%if %{build_plugin}
%files plugin-devel
%defattr(-,root,root,-)
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/plugin
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/plugin
%endif

%changelog
* Tue May  7 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-1m)
- update 4.6.4 release

* Sun May  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-0.20130329.2m)
- rebuild against ppl-1.0

* Tue Apr  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20130329.1m)
- update 20130329

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20130222.1m)
- update 20130222

* Sat Jan 26 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20130125.1m)
- update 20130125

* Mon Jan  7 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20130104.1m)
- update 20130104

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20121207.1m)
- update 20121207

* Sat Oct 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20121026.1m)
- update 20121026

* Sun Sep 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120921.1m)
- update 20120921

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-0.20120803.2m)
- rebuild against cloog-0.17.0

* Mon Aug  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120803.1m)
- update 20120803

* Mon Jul 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120713.1m)
- update 20120713

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120608.1m)
- update 20120608

* Sat May 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120518.1m)
- update 20120518

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120330.1m)
- update 20120330

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.4-0.20120316.1m)
- update 20120316

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-0.20120106.1m)
- update 20120106

* Sat Dec 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-0.20111223.1m)
- update 20111223

* Sat Dec 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-0.20111209.1m)
- update 20111209

* Sat Nov 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-0.20111125.1m)
- update 20111125

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-0.20111104.1m)
- update 20111104

* Sat Oct 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-0.20111028.1m)
- update 20111028

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20111021.1m)
- update 20111021

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20111014.1m)
- update 20111014

* Thu Oct 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20111007.1m)
- update 20111007

* Sun Oct  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-0.20110930.2m)
- merge from TSUPPA4RI/gcc-test/gcc46 rev. 55324
-- fix --with-pkgversion arg; it should contain gcc version number only
-- revise spec

* Sat Oct  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110930.1m)
- update 20110930

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110916.1m)
- update 20110916

* Sun Sep 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110909.1m)
- update 20110909

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110902.2m)
- add gcc-r178680.patch

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110902.1m)
- update 20110902

* Mon Aug 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110826.1m)
- update 20110826

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110819.1m)
- update 20110819

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110812.1m)
- update 20110812

* Tue Aug  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.2-0.20110805.2m)
- rebuild without libgcc.i686 on x86_64

* Sat Aug  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-0.20110805.1m)
- update to 20110805 (snapshot version)
- rebuild against cloog-0.16.3-1m

* Sun Jul 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110729.1m)
- update to 20110729 (snapshot version)

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110722.1m)
- update to 20110722 (snapshot version)

* Mon Jul 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.2-0.20110715.1m)
- update to 20110715 (snapshot version)

* Sun Jul 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-0.20110708.1m)
- update to 20110708 (snapshot version)

* Sat Jul  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-0.20110701.1m)
- merge from T4R/gcc-test/gcc46
-- update to 20110701 (snapshot version)
-- now in 4.6.2 prerelease
- fix libstdc++-doc 

* Mon Jun 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.1-0.20110624.1m)
- update to 20110624 (snapshot version)

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.1-0.20110610.1m)
- update to 20110610 (snapshot version)

* Thu Jun  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.1-0.20110603.1m)
- update to 20110603 (snapshot version)

* Sat May 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.1-0.20110520.1m)
- update to 20110520 (snapshot version)

* Fri Apr 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-0.20110422.1m)
- update to 20110422 (snapshot version)
- fix two issues
-- enable profiledbootstrap again
-- fix #352; enable build_libstdcxx_docs again

* Sat Apr 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-0.20110415.1m)
- update to 20110415 (snapshot version)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-0.20110408.2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-0.20110408.1m)
- update to 20110408 (snapshot version)

* Sat Apr  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-0.20110401.1m)
- update to 20110401 (snapshot version)
- fix file conflicts between gcc-c++ and libstdc++
- merge following changes from fedora
-- add gnative2ascii binary and man page to libgcj

* Sat Mar 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-0.20110325.1m)
- update to 20110325 (snapshot version)
- this is almost same as GCC 4.6.0 Release

* Tue Mar 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110321.1m)
- update to 4.6.0 second RC 

* Sun Mar 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110318.1m)
- update to 4.6.0 RC

* Sat Mar 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110312.1m)
- update to 20110312 (snapshot version)
- switch to use cloog-isl-devel
- revise Provides/Obsoletes tags

* Sun Mar  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110305.1m)
- update to 20110305 (snapshot version)

* Sat Feb 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110226.1m)
- update to 20110226 (snapshot version)

* Sun Feb 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110219.1m)
- update to 20110219 (snapshot version)
- drop pr47620 patch

* Sat Feb 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110212.1m)
- update to 20110212 (snapshot version)

* Sat Feb  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110205.1m)
- update to 20110205 (snapshot version)

* Sun Jan 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.20110129.1m)
- initial package for upcomming GCC-4.6 series
-- GCC 4.6.0 is now in "regression fixes and documentation fixes only" phase
- temporary disable "go" lang
