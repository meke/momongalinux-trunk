%global momorel 39
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global e_startdir %{e_sitedir}/site-start.d
%global lispname mell
%global confsamp %{_datadir}/config-sample

Summary: M Emacs Lisp Library
Name: emacs-mell
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
URL: http://taiyaki.org/elisp/mell/
Source0: http://taiyaki.org/elisp/mell/src/%{lispname}-%{version}.tar.gz 
Source1: http://taiyaki.org/elisp/kakasi/src/kakasi.el 
Requires: emacs
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Obsoletes: mell-emacs
Obsoletes: mell-xemacs

Obsoletes: elisp-mell
Provides: elisp-mell

%description
Mell is an Emacs common library.

%prep
%setup -q -n %{lispname}-%{version}
%{__cp} %{S:1} src/

%build
%configure --with-emacs-sitelispdir=%{e_sitedir} --with-emacs-initdir=%{e_startdir}
make
%{__mv} etc/init-mell.el etc/init-mell.el.emacs

cd src
emacs -batch -q -no-site-file -eval "(add-to-list 'load-path \".\")" \
    -f batch-byte-compile *.el
for f in *.elc; do
  %{__mv} $f $f.emacs
done
for f in *.el; do
  %{__cp} $f $f.emacs
done
cd -

cat <<EOF > mell.dot.emacs
(require 'init-mell)
EOF

%install
%{__rm} -rf %{buildroot}

cd src
%{__mkdir_p} %{buildroot}%{e_sitedir}/mell
for f in *.emacs ; do
  %{__install} -m 0644 $f %{buildroot}%{e_sitedir}/mell/${f%%.emacs}
done
cd -

%{__mkdir_p} %{buildroot}%{e_startdir}

%{__install} -m 644  etc/init-mell.el.emacs \
  %{buildroot}%{e_startdir}/init-mell.el

%{__mkdir_p} %{buildroot}%{confsamp}/%{name}
%{__install} -m 0644 mell.dot.emacs %{buildroot}%{confsamp}/%{name}/mell.dot.emacs

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc/index.html
%{confsamp}/%{name}/mell.dot.emacs
%dir %{e_sitedir}/mell
%{e_sitedir}/mell/*.el*
%{e_startdir}/init-mell.el

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-39m)
- add source(s)

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-38m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-37m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-36m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-35m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-34m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-33m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-32m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-31m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-30m)
- merge mell-emacs to elisp-mell
- kill mell-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-28m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-27m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-26m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-25m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-24m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-23m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-22m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-21m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-20m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-19m)
- rebuild against xemacs-21.5.28
- use %%{xe_sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-18m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-17m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-16m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-15m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-14m)
- rebuild against emacs-22.1

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-13m)
- Change Source URL

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-12m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-11m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-10m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-9m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-8m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-7m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-6m)
- rebuild against emacs-22.0.90

* Mon Jan  9 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-5m)
- rebuild against emacs-22.0.50-0.20051222.1m

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.0-4m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.0-2m)
- rebuild against emacs-21.3.50

* Wed Nov 10 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.0-1m)
- initial import for Momonga.
