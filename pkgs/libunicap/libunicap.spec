%global momorel 6

Summary: Library to access different kinds of (video) capture devices
Name: libunicap
Version: 0.9.8
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: http://www.unicap-imaging.org/
Source0: http://www.unicap-imaging.org/downloads/%{name}-%{version}.tar.gz
NoSource: 0
Patch1:	  libunicap-0.9.8-use-libv4l1-videodev.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: gettext
BuildRequires: glib2-devel
# keep for new version
BuildRequires: gtk-doc
BuildRequires: intltool
BuildRequires: libraw1394-devel
BuildRequires: libv4l-devel
BuildRequires: perl
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig

%description
Unicap provides a uniform interface to video capture devices. It allows
applications to use any supported video capture device via a single API.
The unicap library offers a high level of hardware abstraction while
maintaining maximum performance. Zero copy capture of video buffers is
possible for devices supporting it allowing fast video capture with low
CPU usage even on low-speed architectures.

%package devel
Summary: Header files and libraries from libunicap
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libunicap.

%prep
%setup -q
%patch1 -p1 -b .use-libv4l1-videodev~

%build
%configure \
	   --enable-libv4l
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_libdir}/unicap2
%{_libdir}/%{name}.so.*
%{_datadir}/locale/*/LC_MESSAGES/unicap.mo

%files devel
%defattr(-,root,root)
%{_includedir}/unicap
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-6m)
- rebuild for glib 2.33.2

* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-5m)
- fix v4l issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-2m)
- full rebuild for mo7 release

* Mon Mar  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8-1m)
- initial package for libucil-0.9.8
- disable gtk-doc for the moment
