%global         momorel 1
%global         qtver 4.8.5
%global         qtdir %{_libdir}/qt4
%global         rb_libtorrentver 0.16.13
%global         unstable 0
%global         boost_version 1.55.0
%if %{unstable}
%global         prever 5
%global         rcver %{prever}
%endif

Summary:        qBittorrent is a bittorrent client programmed in C++ / Qt4 that uses libtorrent
Name:           qbittorrent
Version:        3.1.4
Release:        %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Internet
URL:            http://www.qbittorrent.org/
%if ! %{unstable}
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}/%{name}-%{version}/%{name}-%{version}.tar.gz
%else
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}-unstable/%{name}-%{version}rc%{rcver}.tar.gz
%endif
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       qt
Requires:       rb_libtorrent >= %{rb_libtorrentver}
Requires:       curl
Requires:       polyester >= 2.0.0
Requires:       python
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  rb_libtorrent-devel >= %{rb_libtorrentver}
BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  ImageMagick-c++-devel
BuildRequires:  zziplib-devel
BuildRequires:  openssl-devel >= 1.0.0

%description
qBittorrent is a bittorrent client programmed in C++ / Qt4 that uses
libtorrent (sometimes called rblibtorrent) by Arvid Norberg.

It aims to be a good alternative to all other bittorrent clients
out there. qBittorrent is fast, stable and provides unicode
support as well as many features.

%prep
%setup -q -n %{name}-%{version}%{?prever:rc%{rcver}}

%build
## %%configure does not work ...
./configure \
        --prefix=%{_prefix} \
        --bindir=%{_bindir} \
        --datadir=%{_datadir} \
        --qtdir=%{qtdir}
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make INSTALL_ROOT=%{buildroot} install

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING Changelog INSTALL NEWS README TODO
%{_bindir}/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}//pixmaps/qbittorrent.png
%{_mandir}/man1/*

%changelog
* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.6-3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.6-2m)
- rebuild against boost-1.52.0

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.6-1m)
- update to 3.0.6

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Sat Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.5.1m)
- update to 3.0.0rc5

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-0.4.2m)
- rebuild for boost

* Thu Jul 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.4.1m)
- update to 3.0.0rc4

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (2.9.7-3m)
- rebuild for boost 1.50.0

* Sat May  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.7-2m)
- rebuild against rb_libtorrent-0.16.0

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.7-1m)
- update to 2.9.7

* Sun Mar 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.6-1m)
- update to 2.9.6

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.5-1m)
- update to 2.9.5

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.3-1m)
- update to 2.9.3

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.2-2m)
- rebuild for boost-1.48.0

* Wed Nov  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-1m)
- update to 2.9.2

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0 official release

* Mon Oct  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-0.3.1m)
- update to 2.9.0rc3

* Mon Sep 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-0.1.1m)
- update to 2.9.0rc1

* Sun Sep 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.5-1m)
- update to 2.8.5

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.4-2m)
- rebuild for boost

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.4-1m)
- update to 2.8.4

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2

* Mon Jun  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Tue Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- update to 2.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.2-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.9-1m)
- update to 2.6.9

* Sun Mar 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.8-1m)
- update to 2.6.8

* Wed Mar  9 2011 ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.7-3m)
- strict set rb_libtorrentver 0.15.5-3m

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-2m)
- rebuild against boost-1.46.0

* Sun Feb 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.7-1m)
- update to 2.6.7

* Wed Feb 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.6-1m)
- update to 2.6.6

* Mon Feb  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5

* Sun Jan 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Sat Jan 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3

* Sun Dec 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.11-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.10-1m)
- update to 2.4.10

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-2m)
- rebuild against boost-1.44.0

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-1m)
- update to 2.4.9

* Mon Oct 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-1m)
- update to 2.4.8

* Fri Oct 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-2m)
- rebuild with new rb_libtorrent (0.15.4)

* Wed Oct 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Mon Oct  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1)
- update to 2.4.3

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-2m)
- full rebuild for mo7 release

* Sun Aug  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.10-2m)
- rebuild against qt-4.6.3-1m

* Fri Jun 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.8-2m)
- rebuild against boost-1.43.0

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.8-1m)
- update to 2.2.8

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.7-1m)
- update to 2.2.7

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.6-1m)
- update to 2.2.6

* Thu Apr  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.5-1m)
- update to 2.2.5

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.2-2m)
- rebuild against openssl-1.0.0

* Fri Mar 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.6-2m)
- rebuild againsr rb_libtorrent-0.14.9

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.6-1m)
- update to 2.1.6

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.5-1m)
- update to 2.1.5

* Mon Feb  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7

* Wed Jan  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6

* Fri Jan  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Thu Dec 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Wed Dec 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sun Dec 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Mon Dec 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sat Dec 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Dec  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-0.5.1m)
- update to 2.0.0rc5

* Sun Dec  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.4.1m)
- update to 2.0.0rc4

* Tue Dec  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.1m)
- update to 2.0.0rc3

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.6-2m)
- rebuild against boost-1.41.0

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.6-1m)
- update to 1.5.6

* Tue Nov 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-1m)
- update to 1.5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4

* Mon Oct 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3 and rebuild against boost-1.40.0

* Sun Sep 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-0.2.1m)
- update to 1.5.0rc2

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-0.3.1m)
- update to 1.4.0rc1

* Sun Jul  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.0-0.2.5m)
- libtorrent -> rb_libtorrent

* Wed Jun 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-0.2.4m)
- rebuild against libtorrent-0.14.4-1m

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-0.2.3m)
- rebuilt for boost-1.39.0

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-0.2.2m)
- rebuild against libtorrent-0.14.3-1m

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-0.2.1m)
- update to 1.4.0beta2

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against openssl-0.9.8k

* Fri Mar 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Tue Feb 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-2m)
- rebuild against libtorrent-0.14.2-1m

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against boost-1.37.0 and libtorrent-0.14.1-2m
- License: GPLv2+

* Sun Jan 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-0.3.2m)
- add Requires: polyester >= 2.0-0.2.1m

* Mon Dec 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-0.3.1m)
- update to 1.3.0rc3

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Thu Oct 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Mon Sep 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Tue Aug 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-2m)
- add support for ImageMagick and libzzip

* Sun Aug 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- initial build for Momonga Linux 5
