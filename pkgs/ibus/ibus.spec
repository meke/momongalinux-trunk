%global momorel 1
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global fedora 18

%define with_xkbfile 1
%define with_dconf 1
%define with_pygobject2 1
%define with_pygobject3 1

%ifarch ppc ppc64 s390 s390x
%define with_gjs 0
%else
%define with_gjs 1
%endif

%if (0%{?fedora} > 16 || 0%{?rhel} > 6)
%define ibus_gjs_version 3.4.1.20120815
%define ibus_gjs_build_failure 1
%else
%define ibus_gjs_version 3.2.1.20111230
%define ibus_gjs_build_failure 0
%endif

%define ibus_api_version 1.0

%define glib_ver %([ -a %{_libdir}/pkgconfig/glib-2.0.pc ] && pkg-config --modversion glib-2.0 | cut -d. -f 1,2 || echo -n "999")
%define dbus_python_version 0.83.0
# FIXME: It's better to use the new icon names
%define gnome_icon_theme_legacy_version 2.91.6

Name:       ibus
Version:    1.4.99.20121006
Release: %{momorel}m%{?dist}
Summary:    Intelligent Input Bus for Linux OS
License:    LGPLv2+
Group:      System Environment/Libraries
URL:        http://code.google.com/p/ibus/
Source0:    http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:   0
Source1:    xinput-ibus
Source2:    http://fujiwara.fedorapeople.org/ibus/gnome-shell/ibus-gjs-%{ibus_gjs_version}.tar.gz
NoSource:   2
Patch0:     ibus-HEAD.patch
Patch1:     ibus-810211-no-switch-by-no-trigger.patch
Patch2:     ibus-541492-xkb.patch
Patch3:     ibus-530711-preload-sys.patch
Patch4:     ibus-xx-setup-frequent-lang.patch

%if 0%{?fedora} <= 17
# Workaround to disable preedit on gnome-shell until bug 658420 is fixed.
# https://bugzilla.gnome.org/show_bug.cgi?id=658420
Patch92:    ibus-xx-g-s-disable-preedit.patch
%endif
Patch93:    ibus-771115-property-compatible.patch
# Hide no nused properties in f17.
Patch94:    ibus-xx-no-use.diff

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


BuildRequires:  gettext-devel
BuildRequires:  libtool
BuildRequires:  python
BuildRequires:  gtk2-devel
BuildRequires:  gtk3-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  dbus-python-devel >= %{dbus_python_version}
BuildRequires:  desktop-file-utils
BuildRequires:  gtk-doc
%if %with_dconf
BuildRequires:  dconf-devel
BuildRequires:  dbus-x11
BuildRequires:  vala
BuildRequires:  vala-tools
%endif
# for AM_GCONF_SOURCE_2 in configure.ac
BuildRequires:  GConf2-devel
%if %with_pygobject3
BuildRequires:  gobject-introspection-devel
%endif
BuildRequires:  intltool
BuildRequires:  iso-codes-devel
%if %with_xkbfile
BuildRequires:  libxkbfile-devel
BuildRequires:  libgnomekbd-devel
%endif
# for ibus-gjs-xx.tar.gz
BuildRequires:  gjs
%if ! %ibus_gjs_build_failure
BuildRequires:  gnome-shell
%endif

Requires:   %{name}-libs = %{version}-%{release}
Requires:   %{name}-gtk2 = %{version}-%{release}
%if (0%{?fedora} > 14 || 0%{?rhel} > 6)
Requires:   %{name}-gtk3 = %{version}-%{release}
%endif

%if %with_pygobject2
Requires:   pygtk2
%endif
%if %with_pygobject3
Requires:   pygobject3
%endif
Requires:   pyxdg
Requires:   iso-codes
Requires:   dbus-python >= %{dbus_python_version}
Requires:   dbus-x11
Requires:   im-chooser
%if %with_dconf
Requires:   dconf
%else
Requires:   GConf2
%endif
Requires:   notify-python
Requires:   libgnomekbd
Requires:   librsvg2
Requires:   gnome-icon-theme-legacy >= %{gnome_icon_theme_legacy_version}
Requires:   gnome-icon-theme-symbolic

Requires(post):  desktop-file-utils
Requires(postun):  desktop-file-utils
%if %with_dconf
Requires(postun):  dconf
Requires(posttrans): dconf
%endif

Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2

Requires(post):  %{_sbindir}/alternatives
Requires(postun):  %{_sbindir}/alternatives

%define _xinputconf %{_sysconfdir}/X11/xinit/xinput.d/ibus.conf

%description
IBus means Intelligent Input Bus. It is an input framework for Linux OS.

%package libs
Summary:    IBus libraries
Group:      System Environment/Libraries

Requires:   glib2 >= %{glib_ver}
Requires:   dbus >= 1.2.4

%description libs
This package contains the libraries for IBus

%package gtk2
Summary:    IBus im module for gtk2
Group:      System Environment/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   %{name}-libs = %{version}-%{release}
Requires(post): glib2 >= %{glib_ver}
# Added for F14: need to keep bumping for backports
Obsoletes:  ibus-gtk < %{version}-%{release}
Provides:   ibus-gtk = %{version}-%{release}

%description gtk2
This package contains ibus im module for gtk2

%package gtk3
Summary:    IBus im module for gtk3
Group:      System Environment/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   %{name}-libs = %{version}-%{release}
Requires:   imsettings-gnome
Requires(post): glib2 >= %{glib_ver}

%description gtk3
This package contains ibus im module for gtk3

%if %with_gjs
%package gnome3
Summary:    IBus gnome-shell-extension for GNOME3
Group:      System Environment/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   %{name}-libs = %{version}-%{release}
Requires:   gnome-shell

%description gnome3
This is a transitional package which allows users to try out new IBus
GUI for GNOME3 in development.  Note that this package will be marked
as obsolete once the integration has completed in the GNOME3 upstream.
%endif

%package devel
Summary:    Development tools for ibus
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   %{name}-libs = %{version}-%{release}
Requires:   glib2-devel
Requires:   dbus-devel

%description devel
The ibus-devel package contains the header files and developer
docs for ibus.

%package devel-docs
Summary:    Developer documents for ibus
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel-docs
The ibus-devel-docs package contains developer documentation for ibus


%prep
%setup -q
%if %with_gjs
zcat %SOURCE2 | tar xf -
%endif

# patch0 -p1
%patch0 -p1
%if 0%{?fedora} <= 17
%patch92 -p1 -b .g-s-preedit
cp client/gtk2/ibusimcontext.c client/gtk3/ibusimcontext.c ||
%endif
%patch1 -p1 -b .noswitch
%if %with_xkbfile
%patch2 -p1 -b .xkb
rm -f bindings/vala/ibus-1.0.vapi
rm -f data/dconf/00-upstream-settings
%endif
%patch3 -p1 -b .preload-sys
%patch4 -p1 -b .setup-frequent-lang

%if 0%{?fedora} <= 16
%patch93 -p1 -b .compat
%endif

%patch94 -p1 -b .no-used

%build
%if %with_xkbfile
XKB_PRELOAD_LAYOUTS=\
"us,us(chr),us(dvorak),ad,al,am,ara,az,ba,bd,be,bg,br,bt,by,"\
"de,dk,ca,ch,cn(tib),cz,ee,epo,es,et,fi,fo,fr,"\
"gb,ge,ge(dsb),ge(ru),ge(os),gh,gh(akan),gh(ewe),gh(fula),gh(ga),gh(hausa),"\
"gn,gr,hu,hr,ie,ie(CloGaelach),il,"\
"in,"\
"in(tel),in(bolnagri),iq,iq(ku),ir,ir(ku),is,it,jp,"\
"kg,kh,kz,la,latam,lk,lk(tam_unicode),lt,lv,ma,ma(tifinagh),mal,mao,"\
"me,mk,mm,mt,mv,ng,ng(hausa),ng,ng(igbo),ng(yoruba),nl,no,no(smi),np,"\
"pk,pl,pl(csb),pt,ro,rs,ru,ru(cv),ru(kom),ru(sah),ru(tt),ru(xal),"\
"se,si,sk,sy,sy(ku),th,tj,tr,ua,uz,vn"
autoreconf -f -i
%endif
%configure \
    --disable-static \
    --enable-gtk2 \
    --enable-gtk3 \
    --enable-xim \
    --enable-gtk-doc \
    --with-no-snooper-apps='gnome-do,Do.*,firefox.*,.*chrome.*,.*chromium.*' \
    --enable-surrounding-text \
%if %with_xkbfile
    --with-xkb-preload-layouts=$XKB_PRELOAD_LAYOUTS \
    --enable-xkb \
    --enable-libgnomekbd \
%endif
%if %with_dconf
    --enable-dconf \
    --disable-gconf \
%endif
%if %with_pygobject2
    --enable-python-library \
%endif
    --enable-introspection

%if %with_xkbfile
make -C ui/gtk3 maintainer-clean-generic
%endif
# make -C po update-gmo
make USE_SYMBOL_ICON=TRUE %{?_smp_mflags}

%if %with_gjs
d=`basename %SOURCE2 .tar.gz`
cd $d
export PKG_CONFIG_PATH=..:/usr/lib64/pkgconfig:/usr/lib/pkgconfig
%configure \
  --with-gnome-shell-version="3.5.3,3.4,3.3.92,3.3.90,3.3.5,3.3.4,3.3.3,3.2" \
  --with-gjs-version="1.33.3,1.32,1.31.22,1.31.20,1.31.10,1.31.6,1.31.11,1.30"
%make
cd ..
%endif

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/libibus-%{ibus_api_version}.la
rm -f %{buildroot}%{_libdir}/gtk-2.0/*/immodules/im-ibus.la
rm -f %{buildroot}%{_libdir}/gtk-3.0/*/immodules/im-ibus.la

# install xinput config file
install -pm 644 -D %{SOURCE1} %{buildroot}%{_xinputconf}

# install .desktop files
# correct location in upstream.
if test ! -f %{buildroot}%{_sysconfdir}/xdg/autostart/ibus.desktop -a \
          -f %{buildroot}%{_datadir}/applications/ibus.desktop ; then
  mkdir -p %{buildroot}%{_sysconfdir}/xdg/autostart
  mv %{buildroot}%{_datadir}/applications/ibus.desktop \
     %{buildroot}%{_sysconfdir}/xdg/autostart/ibus.desktop
fi
echo "NoDisplay=true" >> %{buildroot}%{_datadir}/applications/ibus-setup.desktop
echo "X-GNOME-Autostart-enabled=false" >> %{buildroot}%{_sysconfdir}/xdg/autostart/ibus.desktop
rm -rf %{buildroot}%{_sysconfdir}/xdg/autostart/ibus.desktop
rm -rf %{buildroot}%{_datadir}/applications/ibus.desktop
# workaround for desktop-file-install
sed -i -e 's|Comment\[ja\]=IBus |& |' \
  %{buildroot}%{_datadir}/applications/ibus-setup.desktop
desktop-file-install --delete-original          \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/*

%if %with_gjs
# https://bugzilla.redhat.com/show_bug.cgi?id=657165
d=`basename %SOURCE2 .tar.gz`
cd $d
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ibus-gjs.mo
cd ..
%endif

# FIXME: no version number
%find_lang %{name}10

%clean
rm -rf %{buildroot}

%post
# recreate icon cache
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%{_sbindir}/alternatives --install %{_sysconfdir}/X11/xinit/xinputrc xinputrc %{_xinputconf} 83 || :

%if !%with_dconf
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/ibus.schemas > /dev/null 2>&1 || :
%endif

%pre
%if !%with_dconf
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/ibus.schemas > /dev/null 2>&1 || :
fi
%endif

%preun
%if !%with_dconf
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/ibus.schemas > /dev/null 2>&1 || :
fi
%endif

%postun
if [ "$1" -eq 0 ]; then
  # recreate icon cache
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

  %{_sbindir}/alternatives --remove xinputrc %{_xinputconf} || :
  # if alternative was set to manual, reset to auto
  [ -L %{_sysconfdir}/alternatives/xinputrc -a "`readlink %{_sysconfdir}/alternatives/xinputrc`" = "%{_xinputconf}" ] && %{_sbindir}/alternatives --auto xinputrc || :

%if %with_dconf
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
  # 'dconf update' sometimes does not update the db...
  dconf update
  if [ -f %{_sysconfdir}/dconf/db/ibus ] ; then
      rm -f %{_sysconfdir}/dconf/db/ibus
  fi
%endif
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
%if %with_dconf
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
dconf update
%endif

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post gtk2
%{_bindir}/update-gtk-immodules %{_host} || :

%postun gtk2
%{_bindir}/update-gtk-immodules %{_host} || :

%post gtk3
%{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} --update-cache || :

%postun gtk3
%{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} --update-cache || :

# FIXME: no version number
%files -f %{name}10.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%if %with_pygobject2
%dir %{python_sitelib}/ibus
%{python_sitelib}/ibus/*
%endif
%dir %{_datadir}/ibus/
%{_bindir}/ibus
%{_bindir}/ibus-daemon
%{_bindir}/ibus-setup
%if %with_pygobject3
%{_datadir}/ibus/*
%endif
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/apps/*
%if %with_dconf
%{_datadir}/GConf/gsettings/*
%{_datadir}/glib-2.0/schemas/*.xml
%{_libexecdir}/ibus-engine-simple
%{_libexecdir}/ibus-dconf
%else
%{_libexecdir}/ibus-gconf
%endif
%{_libexecdir}/ibus-ui-gtk3
%{_libexecdir}/ibus-x11
# {_sysconfdir}/xdg/autostart/ibus.desktop
%{_sysconfdir}/bash_completion.d/ibus.bash
%if %with_dconf
%{_sysconfdir}/dconf/db/ibus.d
%{_sysconfdir}/dconf/profile/ibus
%else
%{_sysconfdir}/gconf/schemas/ibus.schemas
%endif
%config %{_xinputconf}
%if %with_xkbfile
%{_libexecdir}/ibus-xkb
%endif

%files libs
%defattr(-,root,root,-)
%{_libdir}/libibus-%{ibus_api_version}.so.*
%if %with_pygobject3
%{_libdir}/girepository-1.0/IBus-1.0.typelib
%endif

%files gtk2
%defattr(-,root,root,-)
%{_libdir}/gtk-2.0/*/immodules/im-ibus.so

%files gtk3
%defattr(-,root,root,-)
%{_libdir}/gtk-3.0/*/immodules/im-ibus.so

%if %with_gjs
%files gnome3
%defattr(-,root,root,-)
%{_datadir}/gnome-shell/js/ui/status/ibus
%{_datadir}/gnome-shell/extensions/ibus-indicator@example.com
%endif

%files devel
%defattr(-,root,root,-)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/gir-1.0/IBus-1.0.gir
%{_datadir}/vala/vapi/ibus-1.0.vapi
%{_datadir}/vala/vapi/ibus-1.0.deps

%files devel-docs
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/*

%changelog
* Sat Oct 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.99.20121006-1m)
- update to 1.4.99.20121006

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.99.20120917-1m)
- update to 1.4.99.20120917
- temporarily use --disable-gtk-doc

* Sat Sep 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.99.20120914-1m)
- update to 1.4.99.20120914

* Fri Aug  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-2m)
- add workaroud for BTS #462

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-1m)
- update 1.4.1

* Thu Sep 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Thu Aug 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9.99.20110817-1m)
- update 1.3.9.99.20110817

* Sun Jul 31 2011 Masaru SANUKI <sanuki@momonga-linux.org> 
- (1.3.9.99.20110419-3m)
- Added ibus-530711-preload-sys.patch

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9.99.20110419-1m)
- update 1.3.9.99.20110419
-- suppport gtk3 immodule

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.9-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9-1m)
- update 1.3.9

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.8-1m)
- update 1.3.8
-- many bugfix release
- enable-introspection

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.7-5m)
- disable-introspection

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.7-4m)
- full rebuild for mo7 release

* Thu Aug 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-3m)
- the language panel appears in only active as default (Patch2)

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-2m)
- use Shift+Space as trigger

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.7-1m)
- update to 1.3.7

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-2m)
- modify %%post and %%postun gtk for new gtk2

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.6-1m)
- update to 1.3.6

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.5-3m)
- vapi and gir to devel

* Tue Jun 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-2m)
- fix build with gobject-introspection and vala
- sort %%files

* Sun Jun 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.5-1m)
- update 1.3.5

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-1m)
- update 1.3.3

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-1m)
- update 1.3.2

* Tue May  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0.20100111-4m)
- revise xinput-ibus

* Tue May  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0.20100111-3m)
- install Qt3 support configuration file for sdr and gdm
- update xinput-ibus

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0.20100111-2m)
- --enable-gtk-doc

* Thu Jan 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20100111-1m)
- update 1.2.0.20100111

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0.20091225-1m)
- fix %%postun error

* Sun Dec 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20091225-1m)
- update 1.2.0.20091225

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0.20091215-2m)
- --disable-gtk-doc

* Thu Dec 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20091215-1m)
- update 1.2.0.20091215

* Tue Nov 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20091124-1m)
- update 1.2.0.20091124

* Tue Nov 24 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.2.0.20090927-3m)
- modified xinput-ibus for Momonga's xinit system

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090927-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090927-1m)
- update to 1.2.0.20090927

* Thu Jul 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-0.20090612.2m)
- rebuild against qt-4.5.2-1m

* Thu Jun 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090612.1m)
- update 1.1.0-0.20090612

* Wed Jun  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090531.1m)
- update 1.1.0-0.20090531

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.20090508.3m)
- remove desktop.patch

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.20090508.2m)
- stop using autogen.sh to enable build
- re-enable qt4-immodule
- update and re-apply desktop.patch

* Sat May  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090508.1m)
- update 1.1.0-0.20090508

* Fri May  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-0.20090407.2m)
- rebuild against qt-4.5.1-1m

* Tue Apr  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090407.1m)
- update 1.1.0-0.20090407

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-0.20090311.2m)
- remove duplicate files

* Mon Mar 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.20090311.1m)
- update 1.1.0-0.20090311

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-0.20081023.2m)
- rebuild against rpm-4.6

* Sat Jan  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20081023.1m)
- update 0.1.1-0.20081023

* Sat Oct 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20081016.1m)
- update 0.1.1-0.20081016

* Fri Oct 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20081006.1m)
- update 0.1.1-0.20081006

* Sun Oct  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20081004.1m)
- update 0.1.1-0.20081004

* Wed Oct  1 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20081001.1m)
- update 0.1.1-0.20081001

* Tue Sep 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20080923.1m)
- update 0.1.1-0.20080923

* Wed Sep 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20080917.1m)
- update 0.1.1-0.20080917

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20080914.1m)
- update 0.1.1-0.20080914

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-0.20080908.1m)
- update 0.1.1

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080819.1m)
- update to 20080819 git snapshot

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080810.2m)
- add Name[ja] to ibus-setup.desktop

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080810.1m)
- initial package for Momonga Linux
