%global momorel 1

Summary: easier to access web services
Name: rest
Version: 0.7.90
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Libraries
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.7/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: libsoup-devel
BuildRequires: libxml2-devel


%description
This library has been designed to make it easier to access web services that
claim to be "RESTful". A reasonable definition of what this means can be found
on Wikipedia [1]. However a reasonable description is that a RESTful service
should have urls that represent remote objects which methods can then be
called on.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
	--enable-silent-rules \
	--enable-gtk-doc \
	--enable-introspection=yes
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/librest-0.7.so.*
%{_libdir}/librest-extras-0.7.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/Rest-0.7.typelib
%{_libdir}/girepository-1.0/RestExtras-0.7.typelib

%files devel
%defattr(-,root,root)
%{_includedir}/rest-0.7
%{_libdir}/*.so
%{_libdir}/pkgconfig/rest-0.7.pc
%{_libdir}/pkgconfig/rest-extras-0.7.pc
%{_datadir}/gir-1.0/Rest-0.7.gir
%{_datadir}/gir-1.0/RestExtras-0.7.gir
%{_datadir}/gtk-doc/html/rest-0.7

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.90-1m)
- update 0.7.90

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.12-2m)
- rebuild for glib 2.33.2

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.12-1m)
- update 0.7.12

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.11-1m)
- update 0.7.11

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.10-1m)
- initial build
