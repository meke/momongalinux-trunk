%global         momorel 2

Name:           perl-Catalyst-Authentication-Store-DBIx-Class
Version:        0.1506
Release:        %{momorel}m%{?dist}
Summary:        Storage class for Catalyst Authentication using DBIx::Class
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Authentication-Store-DBIx-Class/
Source0:        http://www.cpan.org/authors/id/I/IL/ILMARI/Catalyst-Authentication-Store-DBIx-Class-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Catalyst-Model-DBIC-Schema >= 0.18
BuildRequires:  perl-Catalyst-Plugin-Authentication >= 0.10008
BuildRequires:  perl-Catalyst-Runtime >= 5.8
BuildRequires:  perl-DBIx-Class >= 0.08
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Moose
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Try-Tiny
Requires:       perl-Catalyst-Model-DBIC-Schema >= 0.18
Requires:       perl-Catalyst-Plugin-Authentication >= 0.10008
Requires:       perl-Catalyst-Runtime >= 5.8
Requires:       perl-DBIx-Class >= 0.08
Requires:       perl-List-MoreUtils
Requires:       perl-Moose
Requires:       perl-namespace-autoclean
Requires:       perl-Test-Simple
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Catalyst::Authentication::Store::DBIx::Class class provides access to
authentication information stored in a database via DBIx::Class.

%prep
%setup -q -n Catalyst-Authentication-Store-DBIx-Class-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Authentication/Realm/*
%{perl_vendorlib}/Catalyst/Authentication/Store/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1506-2m)
- rebuild against perl-5.20.0

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1506-1m)
- update to 0.1506

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1505-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1505-2m)
- rebuild against perl-5.18.1

* Thu Jun 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1505-1m)
- update to 0.1505

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-5m)
- rebuild against perl-5.16.2

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-4m)
- source tarball was changed, usr --rmsrc option to build

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-2m)
- rebuild against perl-5.16.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1503-1m)
- update to 0.1503

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1502-2m)
- rebuild against perl-5.14.2

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1502-1m)
- update to 0.1502

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1501-2m)
- rebuild against perl-5.14.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1501-1m)
- update to 0.1501

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1500-1m)
- update to 0.1500

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1401-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1401-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1401-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1401-1m)
- update to 0.1401

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1400-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1400-1m)
- updat to 0.1400

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1300-2m)
- full rebuild for mo7 release

* Thu Jun 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1300-1m)
- update to 0.1300

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1200-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1200-2m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1200-1m)
- update to 0.1200

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1082-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1082-2m)
- rebuild against perl-5.10.1

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1082-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
