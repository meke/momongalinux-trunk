# Generated from nokogiri-1.5.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname nokogiri

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Nokogiri (é¸) is an HTML, XML, SAX, and Reader parser
Name: rubygem-%{gemname}
Version: 1.5.5
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://nokogiri.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby >= 1.8.7
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby >= 1.8.7
BuildRequires: libxml2-devel >= 2.9.0
BuildRequires: libxslt-devel >= 1.1.28
Requires:      libxml2
Requires:      libxslt
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Nokogiri (é¸) is an HTML, XML, SAX, and Reader parser.  Among Nokogiri's
many features is the ability to search documents via XPath or CSS3 selectors.
XML is like violence - if it doesnât solve your problems, you are not using
enough of it.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x
# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/nokogiri
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/CHANGELOG.ja.rdoc
%doc %{geminstdir}/README.ja.rdoc
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/C_CODING_STYLE.rdoc
%doc %{geminstdir}/CHANGELOG.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sat Apr  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-1m)
- update 1.5.5
- add some BuildRequires and Requires

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3.1-1m)
- update 1.4.3.1

* Sun Jan  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-3m)
- provides rubygem(nokogiri)

* Sun Jan  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-2m)
- really update to version 1.4.1, 1.4.1-1m was version 1.3.3

* Fri Jan  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-1m)
- update 1.4.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 29 2009  <meke@localhost.localdomain>
- (1.3.3-1m)
- Initial package for Momonga Linux

