%global momorel 7
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-p3l
Version:        2.03
Release:        %{momorel}m%{?dist}
Summary:        OCaml compiler for parallel programs

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

URL:            http://ocamlp3l.inria.fr/
Source0:        http://ocamlp3l.inria.fr/ocamlp3l-%{version}.tgz
NoSource:       0

# These patches come from Debian:
Patch0:         debian-01-correct-href-to-gz-doc.patch
Patch1:         debian-02-install-mli.patch

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-ocamldoc


%description
OCamlP3l is a compiler for Caml parallel programs.

The OCamlP3l programming paradigm is skeleton programming. The
skeletons encapsulate basic parallel programming patterns in a well
understood and structured approach. Based on P3L, the Pisa Parallel
Programming Language, OCamlP3l skeleton algebra is embedded in a
full-fledged functional programming language, namely Objective Caml.

The skeleton programming approach used in OCamlP3l allows three
different operational interpretations of the same source program:

* the sequential interpretation which is deterministic, hence easy
  to understand and debug,
* the parallel interpretation using a network of computing nodes
  run in parallel to speed up the computation,
* the graphical interpretation, which is run to obtain a drawing
  of the parallel computing network deployed at run-time by the
  parallel interpretation. 


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%package        doc
Summary:        User manual and other documentation for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    doc
The %{name}-doc package contains the user manual and other
documentation for %{name}.


%prep
%setup -q -n ocamlp3l-%{version}

%patch0 -p1
%patch1 -p1


%build
make configure
make


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $RPM_BUILD_ROOT%{_bindir}

make install \
  CAMLLIBDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml \
  PREFIX=$RPM_BUILD_ROOT%{_prefix}

strip $RPM_BUILD_ROOT%{_bindir}/ocamlp3l*

# Zero-length file - remove it.
rm doc/favicon.ico


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/LICENSE
%{_libdir}/ocaml/ocamlp3l
%if %opt
%exclude %{_libdir}/ocaml/ocamlp3l/vprocess/*.cmx
%exclude %{_libdir}/ocaml/ocamlp3l/vthread/*.cmx
%endif
%exclude %{_libdir}/ocaml/ocamlp3l/vprocess/*.mli
%exclude %{_libdir}/ocaml/ocamlp3l/vthread/*.mli
%{_bindir}/ocamlp3lc
%{_bindir}/ocamlp3lopt
%{_bindir}/ocamlp3ltop


%files devel
%defattr(-,root,root,-)
%if %opt
%{_libdir}/ocaml/ocamlp3l/vprocess/*.cmx
%{_libdir}/ocaml/ocamlp3l/vthread/*.cmx
%endif
%{_libdir}/ocaml/ocamlp3l/vprocess/*.mli
%{_libdir}/ocaml/ocamlp3l/vthread/*.mli


%files doc
%defattr(-,root,root,-)
%doc doc/*


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.03-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.03-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.03-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.03-3m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.03-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.03-1m)
- import from Fedora 11

* Mon Mar 16 2009 Richard W.M. Jones <rjones@redhat.com> - 2.03-2
- Don't duplicate the LICENSE and README.Fedora files.

* Sat Dec 20 2008 Richard W.M. Jones <rjones@redhat.com> - 2.03-1
- Initial RPM release.
