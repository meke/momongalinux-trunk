%global         momorel 8

Name:           perl-CatalystX-SimpleLogin
Version:        0.18
Release:        %{momorel}m%{?dist}
Summary:        Provide a simple Login controller which can be reused
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CatalystX-SimpleLogin/
Source0:        http://www.cpan.org/authors/id/B/BO/BOBTFISH/CatalystX-SimpleLogin-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Action-RenderView
BuildRequires:  perl-Catalyst-Action-REST >= 0.74
BuildRequires:  perl-Catalyst-ActionRole-ACL
BuildRequires:  perl-Catalyst-Controller-ActionRole >= 0.15
BuildRequires:  perl-Catalyst-Plugin-Authentication >= 0.10017
BuildRequires:  perl-Catalyst-Plugin-Session >= 0.27
BuildRequires:  perl-Catalyst-Plugin-Session-State-Cookie
BuildRequires:  perl-Catalyst-Plugin-Session-Store-File
BuildRequires:  perl-Catalyst-Runtime >= 5.80013
BuildRequires:  perl-Catalyst-View-TT
BuildRequires:  perl-CatalystX-Component-Traits >= 0.13
BuildRequires:  perl-CatalystX-InjectComponent
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTML-FormHandler >= 0.28001
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Moose
BuildRequires:  perl-Moose-Autobox
BuildRequires:  perl-MooseX-MethodAttributes >= 0.18
BuildRequires:  perl-MooseX-RelatedClassRoles >= 0.004
BuildRequires:  perl-MooseX-Types
BuildRequires:  perl-MooseX-Types-Common
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-SQL-Translator
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.94
Requires:       perl-Catalyst-Action-RenderView
Requires:       perl-Catalyst-Action-REST >= 0.74
Requires:       perl-Catalyst-ActionRole-ACL
Requires:       perl-Catalyst-Controller-ActionRole >= 0.15
Requires:       perl-Catalyst-Plugin-Authentication >= 0.10017
Requires:       perl-Catalyst-Plugin-Session >= 0.27
Requires:       perl-Catalyst-Plugin-Session-State-Cookie
Requires:       perl-Catalyst-Plugin-Session-Store-File
Requires:       perl-Catalyst-Runtime >= 5.80013
Requires:       perl-Catalyst-View-TT
Requires:       perl-CatalystX-Component-Traits >= 0.13
Requires:       perl-CatalystX-InjectComponent
Requires:       perl-HTML-FormHandler >= 0.28001
Requires:       perl-libwww-perl
Requires:       perl-Moose
Requires:       perl-Moose-Autobox
Requires:       perl-MooseX-MethodAttributes >= 0.18
Requires:       perl-MooseX-RelatedClassRoles >= 0.004
Requires:       perl-MooseX-Types
Requires:       perl-MooseX-Types-Common
Requires:       perl-namespace-autoclean
Requires:       perl-SQL-Translator
Requires:       perl-Test-Exception
Requires:       perl-Test-Simple >= 0.94
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
CatalystX::SimpleLogin is an application class Moose::Role which will
inject a Catalyst::Controller which is an instance of
CatalystX::SimpleLogin::Controller::Login into your application. This
provides a simple login and logout page with the adition of only one line
of code and one template to your application.

%prep
%setup -q -n CatalystX-SimpleLogin-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/ActionRole/NeedsLogin.pm
%{perl_vendorlib}/CatalystX/SimpleLogin.pm
%{perl_vendorlib}/CatalystX/SimpleLogin
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-2m)
- rebuild against perl-5.16.1

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-5m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-4m)
- specify version of some perl modules

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14-2m)
- rebuild for new GCC 4.5

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
