%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-lwt
Version:        2.3.2
Release:        %{momorel}m%{?dist}
Summary:        OCaml lightweight thread library

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://ocsigen.org/lwt/
Source0:        http://ocsigen.org/download/lwt-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-ssl >= 0.4.6-1m
BuildRequires:  ocaml-react >= 0.9.2-1m
BuildRequires:  ocaml-lablgtk-devel >= 2.14.2
BuildRequires:  chrpath
BuildRequires:  libev-devel


%description
Lwt is a lightweight thread library for Objective Caml.  This library
is part of the Ocsigen project.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n lwt-%{version}

mv README README.old
iconv -f iso-8859-1 -t utf-8 < README.old > README


%build
export C_INCLUDE_PATH=%{_includedir}/libev
export LIBRARY_PATH=%{_libdir}
make


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install

strip $OCAMLFIND_DESTDIR/stublibs/dll*.so
chrpath --delete $OCAMLFIND_DESTDIR/stublibs/dll*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE COPYING
%{_libdir}/ocaml/lwt
%if %opt
%exclude %{_libdir}/ocaml/lwt/*.a
%exclude %{_libdir}/ocaml/lwt/*.cmxa
%endif
%exclude %{_libdir}/ocaml/lwt/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc LICENSE COPYING CHANGES CHANGES.darcs README
%if %opt
%{_libdir}/ocaml/lwt/*.a
%{_libdir}/ocaml/lwt/*.cmxa
%endif
%{_libdir}/ocaml/lwt/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-3m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-2m)
- rebuild against ocaml-lablgtk-2.14.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0
- rebuild against ocaml-3.11.2
- rebuild against ocaml-ssl-0.4.3-4m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Dec  4 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1.0-3
- Rebuild.

* Wed Sep  3 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1.0-2
- Rebuild with higher EVR than F-9 branch.

* Mon Sep  1 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1.0-1
- Initial RPM release.
