%global momorel 2
%global pkg ruby-mode
%global pkgname ruby-mode

%define	rubyver		1.9.3
%global _rc 1
%define dotrc 		%{?_rc:-rc%{_rc}}
%define dotpreview      %{?_preview:-preview%{_preview}}
%define dotdate         %{?_date:-%{_date}}
%define patchlevel      %{?_patchlevel:-p%{_patchlevel}}

Summary: Ruby mode for Emacs
Name: emacs-%{pkg}
Version: %{rubyver}%{?dotpatchlevel}
Release: %{momorel}m%{?dist}
License: BSD or GPLv2
Group: Applications/Editors
URL: http://www.ruby-lang.org/
Source0: http://ftp.ruby-lang.org/pub/ruby/1.9/ruby-%{rubyver}%{?dotrc}%{?dotpreview}%{?patchlevel}%{?dotdate}.tar.bz2
NoSource: 0
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
Requires: emacs(bin) >= %{_emacs_version}
Provides: elisp-ruby-mode

%description
%{pkgname} is Emacs Lisp for editing and debugging ruby codes

%package -n %{name}-el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}

%description -n %{name}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q -n ruby-%{rubyver}%{?dotrc}%{?dotpreview}%{?patchlevel}%{?dotdate}

%build
cd misc
for x in *.el; do 
    %{_emacs_bytecompile} $x
done

cat > %{pkg}-init.el <<"EOF"
(autoload 'ruby-mode "ruby-mode" nil t)
EOF

%install
rm -rf %{buildroot}

cd misc
%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/%{pkg}
for x in *.el *.elc; do
    install -m 644 $x %{buildroot}%{_emacs_sitelispdir}/%{pkg}
done

%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m 644 %{pkg}-init.el %{buildroot}%{_emacs_sitestartdir}/%{pkg}-init.el

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n %{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-2m)
- rebuild for emacs-24.1

* Sun Oct  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-1m)
- Initilal package
