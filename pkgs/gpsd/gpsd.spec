%global         momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           gpsd
Version:        3.7
Release:        %{momorel}m%{?dist}
Summary:        Service daemon for mediating access to a GPS
Group:          System Environment/Daemons
License:        BSD
URL:            http://catb.org/gpsd/
Source0:        http://download-mirror.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
Source10:       gpsd.service
Source11:       gpsd.sysconfig
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       %{name}-libs = %{version}-%{release}
Requires:       udev >= 151
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  ncurses-devel
BuildRequires:  xmlto
BuildRequires:  python-devel >= 2.7
BuildRequires:  lesstif-devel
BuildRequires:  libXaw-devel
BuildRequires:  desktop-file-utils

%description 
gpsd is a service daemon that mediates access to a GPS sensor
connected to the host computer by serial or USB interface, making its
data on the location/course/velocity of the sensor available to be
queried on TCP port 2947 of the host computer.  With gpsd, multiple
GPS client applications (such as navigational and wardriving software)
can share access to a GPS without contention or loss of data.  Also,
gpsd responds to queries with a format that is substantially easier to
parse than NMEA 0183.  

%package libs
Summary: Client libraries in C and Python for talking to a running gpsd or GPS
Group: System Environment/Libraries

%description libs
This package contains the gpsd libraries and python modules that manage access
to a GPS for applications.

%package devel
Summary:        Client libraries in C and Python for talking to a running gpsd or GPS
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package provides C header files for the gpsd shared libraries
that manage access to a GPS for applications; also Python modules.

%package        clients
Summary:        Clients for gpsd
Group:          Applications/System

%description clients
xgps is a simple test client for gpsd with an X interface. It displays
current GPS position/time/velocity information and (for GPSes that
support the feature) the locations of accessible satellites.

xgpsspeed is a speedometer that uses position information from the GPS.
It accepts an -h option and optional argument as for gps, or a -v option
to dump the package version and exit. Additionally, it accepts -rv
(reverse video) and -nc (needle color) options.

cgps resembles xgps, but without the pictorial satellite display.  It
can run on a serial terminal or terminal emulator.

%prep
%setup -q

echo '#define REVISION "release-%{version}-%{release}"' > revision.h

%build
export CFLAGS="%{optflags}"

scons \
        dbus=yes \
        systemd=yes \
        libQgpsmm=no \
        debug=yes \
        prefix="" \
        sysconfdif=%{_sysconfdir} \
        bindir=%{_bindir} \
        includedir=%{_includedir} \
        libdir=%{_libdir} \
        sbindir=%{_sbindir} \
        mandir=%{_mandir} \
        docdir=%{_docdir} \
        build

%install
rm -rf %{buildroot}
DESTDIR=%{buildroot} scons install

# service files
%{__install} -d -m 0755 %{buildroot}/lib/systemd/system
%{__install} -p -m 0644 %{SOURCE10} \
        %{buildroot}/lib/systemd/system/gpsd.service

%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
%{__install} -p -m 0644 %{SOURCE11} \
        %{buildroot}%{_sysconfdir}/sysconfig/gpsd

# udev rules
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/udev/rules.d
%{__install} -p -m 0644 gpsd.rules \
        %{buildroot}%{_sysconfdir}/udev/rules.d/99-gpsd.rules

# hotplug script
%{__install} -d -m 0755 %{buildroot}/lib/udev
%{__install} -p -m 0755 gpsd.hotplug %{buildroot}/lib/udev

# Install the .desktop files
%{__mkdir} -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
        --dir %{buildroot}%{_datadir}/applications \
        packaging/X11/xgps.desktop
desktop-file-install --vendor= \
        --dir %{buildroot}%{_datadir}/applications \
        packaging/X11/xgpsspeed.desktop

# Install logo icon for .desktop files
%{__install} -d -m 0755 %{buildroot}%{_datadir}/gpsd
%{__install} -p -m 0644 packaging/X11/gpsd-logo.png %{buildroot}%{_datadir}/gpsd/gpsd-logo.png

# Not needed since gpsd.h is not installed
rm %{buildroot}%{_libdir}/{libgpsd.so,pkgconfig/libgpsd.pc}

%clean
rm -rf %{buildroot}

%post
/bin/systemctl daemon-reload &> /dev/null
if [ -f %{_initscriptdir}/%{name} ] && /sbin/chkconfig --level 3 %{name}; then
        /bin/systemctl enable %{name}.service &> /dev/null
fi
:

%preun
if [ $1 = 0 ]; then
        /bin/systemctl --no-reload disable %{name}.service &> /dev/null
        /bin/systemctl stop %{name}.service &> /dev/null
fi
:

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README INSTALL COPYING
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %{_sysconfdir}/udev/rules.d/*
%{_sbindir}/gpsd
%{_sbindir}/gpsdctl
%{_bindir}/gpsprof
%{_bindir}/gpsmon
%{_bindir}/gpsctl
/lib/systemd/system/gpsd.service
/lib/udev/gpsd*
%{_mandir}/man8/gpsd.8*
%{_mandir}/man8/gpsdctl.8*
%{_mandir}/man1/gpsprof.1*
%{_mandir}/man1/gpsmon.1*
%{_mandir}/man1/gpsctl.1*

%files libs
%defattr(-,root,root,-)
%{_libdir}/libgps*.so.*
%{python_sitearch}/gps*
%exclude %{python_sitearch}/gps/fake*

%files devel
%defattr(-,root,root,-)
%doc TODO
%{_bindir}/gpsfake
%{_libdir}/libgps*.so
%{_libdir}/pkgconfig/*.pc
%{python_sitearch}/gps/fake*
%{_includedir}/gps.h
%{_includedir}/libgpsmm.h
%{_mandir}/man1/gpsfake.1*
%{_mandir}/man3/libgps.3*
%{_mandir}/man3/libgpsmm.3*
%{_mandir}/man3/libgpsd.3*
%{_mandir}/man3/libQgpsmm.3*
%{_mandir}/man5/gpsd_json.5*
%{_mandir}/man5/srec.5*

%files clients
%defattr(-,root,root,-)
%{_bindir}/cgps
%{_bindir}/gegps
%{_bindir}/gpscat
%{_bindir}/gpsdecode
%{_bindir}/gpspipe
%{_bindir}/gpxlogger
%{_bindir}/lcdgps
%{_bindir}/xgps
%{_bindir}/xgpsspeed
%{_mandir}/man1/gegps.1*
%{_mandir}/man1/gps.1*
%{_mandir}/man1/gpsdecode.1*
%{_mandir}/man1/gpspipe.1*
%{_mandir}/man1/lcdgps.1*
%{_mandir}/man1/xgps.1*
%{_mandir}/man1/xgpsspeed.1*
%{_mandir}/man1/cgps.1*
%{_mandir}/man1/gpscat.1*
%{_datadir}/applications/*.desktop
%dir %{_datadir}/gpsd
%{_datadir}/gpsd/gpsd-logo.png

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7-1m)
- update to 3.7

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.95-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.95-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.95-2m)
- rebuild for new GCC 4.5

* Wed Oct  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.95-1m)
- update to 2.95 (sync with Fedora devel)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.39-6m)
- full rebuild for mo7 release

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.39-5m)
- explicitly link libX11 and libXt

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.39-4m)
- update rules.gpsd for udev-151

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.39-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.39-2m)
- import a patch to enable parallel build on i686 from Rawhide (2.39-5)

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.39-1m)
- update to 2.39

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.37-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.37-3m)
- rebuild against python-2.6.1-2m

* Sat Jul 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.37-2m)
- import udev setting files from opensuse
- remove hotplug scripts
- DO NOT USE HOTPLUG, it's obsoleted
- please remove /etc/hotplug.d manually after installing this release

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.37-1m)
- update to 2.37, sync with Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.34-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.34-2m)
- %%NoSource -> NoSource

* Wed Aug 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-1m)
- import from Fedora

* Sun Aug 19 2007 Matthew Truch <matt at truch.net> - 2.34-8
- Patch Makefile to also install gpsd_config.h as needed by
  libgpsmm.h.  Redhat BZ 253433.

* Sat Jun 30 2007 Matthew Truch <matt at truch.net> - 2.34-7
- Make sure the logo is actually included (via the spec file).
  I need to wake up before I try even trivial updates.  

* Sat Jun 30 2007 Matthew Truch <matt at truch.net> - 2.34-6
- Learn how to use search and replace (aka fix all instances of
  gpsd-logo.png spelled incorrectly as gspd-logo.png).

* Sat Jun 30 2007 Matthew Truch <matt at truch.net> - 2.34-5
- Fix desktop file and logo file name.

* Sat Jun 30 2007 Matthew Truch <matt at truch.net> - 2.34-4
- Include icon for .desktop files per BZ 241428

* Tue Mar 20 2007 Michael Schwendt <mschwendt[AT]users.sf.net> - 2.34-3
- Bump release for FE5 -> Fedora 7 upgrade path.

* Tue Feb 27 2007 Matthew Truch <matt at truch.net> - 2.34-2
- BR python-devel instead of python to make it build.  

* Tue Feb 27 2007 Matthew Truch <matt at truch.net> - 2.34-1
- Upgrade to 2.34.
- Get rid of %%makeinstall (which was never needed).
- Possibly fix hotplug issuses (BZ 219750).
- Use %%python_sitelib for python site-files stuff.

* Sat Dec 9 2006 Matthew Truch <matt at truch.net> - 2.33-6
- Rebuild to pull in new version of python.

* Tue Sep 26 2006 Matthew Truch <matt at truch.net> - 2.33-5
- Remove openmotif requirment, and switch to lesstif.

* Mon Aug 28 2006 Matthew Truch <matt at truch.net> - 2.33-4
- Bump release for rebuild in prep. for FC6.

* Thu Jul 20 2006 Matthew Truch <matt at truch.net> - 2.33-3
- Actually, was a missing BR glib-dbus-devel.   Ooops.

* Thu Jul 20 2006 Matthew Truch <matt at truch.net> - 2.33-2
- Missing BR glib-devel

* Thu Jul 20 2006 Matthew Truch <matt at truch.net> - 2.33-1
- Update to version 2.33

* Wed Apr 19 2006 Matthew Truch <matt at truch.net> - 2.32-5
- Don't --enable-tnt in build as it causes some gpses to not work
  properly with sattelite view mode.  See bugzilla bug 189220.

* Thu Apr 13 2006 Matthew Truch <matt at truch.net> - 2.32-4
- Add dbus-glib to BuildRequires as needed for build.

* Sun Apr 9 2006 Matthew Truch <matt at truch.net> - 2.32-3
- Include xmlto and python in buildrequires so things build right.
- Don't package static library file.  

* Wed Apr 5 2006 Matthew Truch <matt at truch.net> - 2.32-2
- Use ye olde %%{?dist} tag.

* Wed Apr 5 2006 Matthew Truch <matt at truch.net> - 2.32-1
- Initial Fedora Extras specfile
