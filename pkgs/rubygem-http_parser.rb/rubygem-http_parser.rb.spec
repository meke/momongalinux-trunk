# Generated from http_parser.rb-0.5.3.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname http_parser.rb

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Simple callback-based HTTP request/response parser
Name: rubygem-%{gemname}
Version: 0.5.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/tmm1/http_parser.rb
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}

%description
Ruby bindings to http://github.com/ry/http-parser and
http://github.com/a2800276/http-parser.java


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            -V \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%files
%dir %{geminstdir}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}


%changelog
* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-1m)
- update 0.5.3

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2-1m) 
- update 0.5.2

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1m) 
- Initial package for Momonga Linux
