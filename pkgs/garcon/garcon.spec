%global momorel 1
%global xfcever 4.11

Name:           garcon
Version:        0.3.0
Release:        %{momorel}m%{?dist}
Summary:        Implementation of the freedesktop.org menu specification

Group:          System Environment/Libraries
# garcon's source code is licensed under the LGPLv2+,
# while its documentation is licensed under the GFDL 1.1
License:        LGPLv2+ and GFDL
URL:            http://xfce.org/
Source0:        http://archive.xfce.org/src/xfce/%{name}/0.3/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= 2.14
BuildRequires:  gtk2-devel >= 2.12.0
BuildRequires:  gtk-doc
BuildRequires:  gettext
BuildRequires:  intltool
Requires:	libxfce4util

# not yet in place
#Provides:       libxfce4menu-devel = 4.6.2 
#Obsoletes:      libxfce4menu-devel < 4.6.2

%description
Garcon is an implementation of the freedesktop.org menu specification replacing
the former Xfce menu library libxfce4menu. It is based on GLib/GIO only and 
aims at covering the entire specification except for legacy menus.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk2-devel
Requires:       pkgconfig
Requires:       gnome-python2-gnomevfs

# not yet in place
#Provides:       libxfce4menu = 4.6.2 
#Obsoletes:      libxfce4menu < 4.6.2

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static --enable-gtk-doc
make %{?_smp_mflags} V=1


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_sysconfdir}/xdg/menus/*
%{_libdir}/*.so.*
%{_datadir}/desktop-directories/*

%files devel
%defattr(-,root,root,-)
%doc HACKING STATUS TODO
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%doc %{_datadir}/gtk-doc/html/garcon
%{_datadir}/gtk-doc/html/garcon/*

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.0-1m)
- update to version 0.2.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-2m)
- rebuild for glib 2.33.2

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.7-1m)
- update

* Wed Dec 22 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-1m)
- import to Momonga
- update to 0.1.4 required by xfce4-panel
- add Requires:       gnome-python2-gnomevfs for /usr/share/gtk-doc/html

* Thu Oct 07 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-2
- Drop dependency on gtk-doc (#604352)

* Fri Feb 26 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-1
- Update to 0.1.1

* Tue Jan 12 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-2
- Build gtk-doc

* Tue Jan 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Initial spec file
