%global momorel 1
%global gcc_ver $(gcc -dumpversion)
%global build_gcc 1
%global date 20120924

# Build options:
#
# --with doxygen
#   The doxygen docs are HUGE, so they are not built by default.

%ifarch s390 s390x sparc64
  # No ocaml on these arches
  %bcond_with ocaml
%else
  %bcond_without ocaml
%endif

#%%global prerel rc1
#%global downloadurl http://llvm.org/%{?prerel:pre-}releases/%{version}

Name:           llvm
Version:        3.4.2
Release:        %{momorel}m%{?dist}
Summary:        The Low Level Virtual Machine

Group:          Development/Languages
License:        "NCSA"
URL:            http://llvm.org/

# Release Source
Source0:        http://llvm.org/releases/%{version}/llvm-%{version}.src.tar.gz
NoSource:       0
#Source1:        http://llvm.org/releases/%{version}/clang-%{version}.src.tar.gz
Source1:        http://llvm.org/releases/%{version}/cfe-%{version}.src.tar.gz
NoSource:       1

# PreRelease Source
#Source0:        http://llvm.org/pre-releases/%{version}/%{prerel}/llvm-%{version}%{?prerel}.tar.gz
#NoSource:       0
#Source1:        http://llvm.org/pre-releases/%{version}/%{prerel}/cfe-%{version}%{?prerel}.tar.gz
#NoSource:       1

# snapshot Source
#Source0:        http://llvm.org/pre-releases/%{version}/%{prerel}/llvm-%{version}-%{?date}.tar.xz
#Source1:        http://llvm.org/pre-releases/%{version}/%{prerel}/clang-%{version}-%{?date}.tar.xz`

# multilib fixes5B
Source2:        llvm-Config-config.h
Source3:        llvm-Config-llvm-config.h


# Data files should be installed with timestamps preserved
#Patch0:         llvm-2.6-timestamp.patch
Patch10:        clang-3.4-momonga.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  bison
BuildRequires:  chrpath
BuildRequires:  flex
BuildRequires:  gcc-c++ >= 3.4
BuildRequires:  groff
BuildRequires:  libffi-devel
BuildRequires:  libtool-ltdl-devel
%if %{with ocaml}
BuildRequires:  ocaml-ocamldoc
%endif
# for DejaGNU test suite
BuildRequires:  dejagnu tcl-devel python
%if 0%{?_with_doxygen}
BuildRequires:  doxygen graphviz
%endif
BuildRequires:  perl-Pod-Html
Requires:       llvm-libs = %{version}-%{release}

%description
LLVM is a compiler infrastructure designed for compile-time,
link-time, runtime, and idle-time optimization of programs from
arbitrary programming languages.  The compiler infrastructure includes
mirror sets of programming tools as well as libraries with equivalent
functionality.


%package devel
Summary:        Libraries and header files for LLVM
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       libffi-devel
Requires:       libstdc++-devel >= 3.4
Provides:       llvm-static = %{version}-%{release}

Requires(posttrans): /usr/sbin/alternatives
Requires(postun):    /usr/sbin/alternatives

%description devel
This package contains library and header files needed to develop new
native programs that use the LLVM infrastructure.


%package doc
Summary:        Documentation for LLVM
Group:          Documentation
BuildArch:	noarch
Requires:       %{name} = %{version}-%{release}
# might seem redundant, but needed to kill off the old arch-ed -doc
# subpackage
Obsoletes:      %{name}-doc < %{version}-%{release}

%description doc
Documentation for the LLVM compiler infrastructure.


%package libs
Summary:        LLVM shared libraries
Group:          System Environment/Libraries

%description libs
Shared libraries for the LLVM compiler infrastructure.


%package -n clang
Summary:        A C language family front-end for LLVM
Group:          Development/Languages
Requires:       llvm = %{version}-%{release}
# clang requires gcc; clang++ gcc-c++
Requires:       gcc-c++

%description -n clang
clang: noun
    1. A loud, resonant, metallic sound.
    2. The strident call of a crane or goose.
    3. C-language family front-end toolkit.

The goal of the Clang project is to create a new C, C++, Objective C
and Objective C++ front-end for the LLVM compiler. Its tools are built
as libraries and designed to be loosely-coupled and extensible.


%package -n clang-devel
Summary:        Header files for clang
Group:          Development/Languages
Requires:       clang = %{version}-%{release}

%description -n clang-devel
This package contains header files for the Clang compiler.


%package -n clang-analyzer
Summary:        A source code analysis framework
Group:          Development/Languages
Requires:       clang = %{version}-%{release}
# not picked up automatically since files are currently not instaled
# in standard Python hierarchies yet
Requires:       python

%description -n clang-analyzer
The Clang Static Analyzer consists of both a source code analysis
framework and a standalone tool that finds bugs in C and Objective-C
programs. The standalone tool is invoked from the command-line, and is
intended to run in tandem with a build of a project or code base.


%package -n clang-doc
Summary:        Documentation for Clang
Group:          Documentation
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}

%description -n clang-doc
Documentation for the Clang compiler front-end.


%if 0%{?_with_doxygen}
%package apidoc
Summary:        API documentation for LLVM
Group:          Development/Languages
BuildArch:      noarch
Requires:       %{name}-doc = %{version}-%{release}


%description apidoc
API documentation for the LLVM compiler infrastructure.


%package -n clang-apidoc
Summary:        API documentation for Clang
Group:          Development/Languages
BuildArch:      noarch
Requires:       clang-doc = %{version}-%{release}


%description -n clang-apidoc
API documentation for the Clang compiler.
%endif


%if %{with ocaml}
%package        ocaml
Summary:        OCaml binding for LLVM
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       ocaml-runtime

%description    ocaml
OCaml binding for LLVM.


%package        ocaml-devel
Summary:        Development files for %{name}-ocaml
Group:          Development/Libraries
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-ocaml = %{version}-%{release}
Requires:       ocaml

%description    ocaml-devel
The %{name}-ocaml-devel package contains libraries and signature files
for developing applications that use %{name}-ocaml.


%package ocaml-doc
Summary:        Documentation for LLVM's OCaml binding
Group:          Documentation
BuildArch:	noarch
Requires:       %{name}-ocaml = %{version}-%{release}

%description ocaml-doc
HTML documentation for LLVM's OCaml binding.
%endif


%prep
#%%setup -q -n llvm-%{version}%{?prerel} -a1 %{?_with_gcc:-a2}
%setup -q -n llvm-%{version}%{?prerel}.src -a1 %{?_with_gcc:-a2}
#%%setup -q -n llvm.src -a1 %{?_with_gcc:-a2}
rm -rf tools/clang
mv cfe-%{version}%{?prerel}.src tools/clang
#mv cfe.src tools/clang
#mv clang-%{version} tools/clang

# llvm patches
#%patch0 -p1 -b .timestamp

# clang patches
#pushd tools/clang
#%patch1 -p1 -b .add_gcc_ver
#popd

# momonga patch includes patch1
%patch10 -p1 -b .momonga~

%build

# Disabling assertions now, rec. by pure and needed for OpenGTL
# TESTFIX no PIC on ix86: http://llvm.org/bugs/show_bug.cgi?id=3801
%if %{build_gcc}
CC=gcc \
CXX=g++ \
%configure \
  --build=%{_arch}-momonga-linux \
  --host=%{_arch}-momonga-linux \
  --target=%{_arch}-momonga-linux \
%else
%configure \
%endif
  --prefix=%{_prefix} \
  --libdir=%{_libdir}/%{name} \
  --datadir=%{_libdir}/%{name} \
%if 0%{?_with_doxygen}
  --enable-doxygen \
%endif
  --disable-assertions \
  --enable-debug-runtime \
%if %{with ocaml}
  --enable-bindings=ocaml \
%else
  --enable-bindings=none \
%endif
  --enable-jit \
  --enable-libffi \
  --enable-shared \
  --with-c-include-dirs=%{_includedir}:/usr/lib/gcc/%{_arch}-%{_vendor}-%{_os}/%{gcc_ver}/include \
  --with-cxx-include-root=%{_includedir}/c++/%{gcc_ver} \
  --with-cxx-include-arch=%{_arch}-%{_vendor}-%{_os} \
%if %{__isa_bits} == 64
  --with-cxx-include-32bit-dir=32 \
%endif

# FIXME file this
# configure does not properly specify libdir
sed -i 's|(PROJ_prefix)/lib|(PROJ_prefix)/%{_lib}/%{name}|g' Makefile.config

# FIXME upstream need to fix this
# llvm-config.cpp hardcodes lib in it
sed -i 's|ActiveLibDir = ActivePrefix + "/lib"|ActiveLibDir = ActivePrefix + "/%{_lib}/%{name}"|g' tools/llvm-config/llvm-config.cpp

make %{_smp_mflags} REQUIRES_RTTI=1 \
%ifarch ppc
  OPTIMIZE_OPTION="%{optflags} -fno-var-tracking-assignments"
%else
  OPTIMIZE_OPTION="%{optflags}"
%endif


%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} \
     PROJ_docsdir=/moredocs

# multilib fixes
mv %{buildroot}%{_bindir}/llvm-config{,-%{__isa_bits}}

pushd %{buildroot}%{_includedir}/llvm/Config
mv config.h config-%{__isa_bits}.h
cp -p %{SOURCE2} config.h
mv llvm-config.h llvm-config-%{__isa_bits}.h
cp -p %{SOURCE3} llvm-config.h
popd

# Create ld.so.conf.d entry
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
cat >> %{buildroot}%{_sysconfdir}/ld.so.conf.d/llvm-%{_arch}.conf << EOF
%{_libdir}/llvm
EOF

# Static analyzer not installed by default:
# http://clang-analyzer.llvm.org/installation#OtherPlatforms
mkdir -p %{buildroot}%{_libdir}/clang-analyzer
# create launchers
for f in scan-{build,view}; do
  ln -s %{_libdir}/clang-analyzer/$f/$f %{buildroot}%{_bindir}/$f
done

(cd tools/clang/tools && cp -pr scan-{build,view} \
 %{buildroot}%{_libdir}/clang-analyzer/)


# Move documentation back to build directory
# 
mv %{buildroot}/moredocs .
rm -f moredocs/*.tar.gz
rm -f moredocs/ocamldoc/html/*.tar.gz

# and separate the apidoc
%if 0%{?_with_doxygen}
mv moredocs/html/doxygen apidoc
mv tools/clang/docs/doxygen/html clang-apidoc
%endif

# And prepare Clang documentation
#
mkdir clang-docs
for f in LICENSE.TXT NOTES.txt README.txt ; do
  ln tools/clang/$f clang-docs/
done
rm -rf tools/clang/docs/{doxygen*,Makefile*,*.graffle,tools}


#find %%{buildroot} -name .dir -print0 | xargs -0r rm -f
file %{buildroot}/%{_bindir}/* | awk -F: '$2~/ELF/{print $1}' | xargs -r chrpath -d
file %{buildroot}/%{_libdir}/llvm/*.so | awk -F: '$2~/ELF/{print $1}' | xargs -r chrpath -d
#chrpath -d %%{buildroot}/%%{_libexecdir}/clang-cc

# Get rid of erroneously installed example files.
rm %{buildroot}%{_libdir}/%{name}/*LLVMHello.*

chmod -x %{buildroot}%{_libdir}/%{name}/*.a

# remove documentation makefiles:
# they require the build directory to work
find examples -name 'Makefile' | xargs -0r rm -f


#%check
## LLVM test suite failing on PPC64
#%ifnarch ppc64
#make check LIT_ARGS="-v -j4"
#%endif
#
## clang test suite failing on PPC
#%ifnarch ppc ppc64
#
## Disable parallel test to avoid possible deadlock with python-2.7.2
#make -C tools/clang/test TESTARGS="-v -j1"
#%endif

%post libs -p /sbin/ldconfig
%post -n clang -p /sbin/ldconfig


%postun libs -p /sbin/ldconfig
%postun -n clang -p /sbin/ldconfig


%posttrans devel
# link llvm-config to the platform-specific file;
# use ISA bits as priority so that 64-bit is preferred
# over 32-bit if both are installed
alternatives \
  --install \
  %{_bindir}/llvm-config \
  llvm-config \
  %{_bindir}/llvm-config-%{__isa_bits} \
  %{__isa_bits}

%postun devel
if [ $1 -eq 0 ]; then
  alternatives --remove llvm-config \
    %{_bindir}/llvm-config-%{__isa_bits}
fi
exit 0


%files
%defattr(-,root,root,-)
%doc CREDITS.TXT LICENSE.TXT README.txt
%{_bindir}/bugpoint
%{_bindir}/llc
%{_bindir}/lli
%{_bindir}/lli-child-target
%exclude %{_bindir}/llvm-config-%{__isa_bits}
%{_bindir}/llvm*
%{_bindir}/macho-dump
%{_bindir}/opt
%exclude %{_mandir}/man1/clang.1.*
%doc %{_mandir}/man1/*.1.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/llvm-config-%{__isa_bits}
%{_includedir}/%{name}
%{_includedir}/%{name}-c
%{_libdir}/%{name}/*.a

%files libs
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/llvm-%{_arch}.conf
%dir %{_libdir}/%{name}
%exclude %{_libdir}/%{name}/libclang.so
%{_libdir}/%{name}/*.so

%files -n clang
%defattr(-,root,root,-)
%doc clang-docs/*
%{_bindir}/clang*
%{_bindir}/c-index-test
%{_libdir}/%{name}/libclang.so
%{_prefix}/lib/clang
%doc %{_mandir}/man1/clang.1.*

%files -n clang-devel
%defattr(-,root,root,-)
%{_includedir}/clang
%{_includedir}/clang-c

%files -n clang-analyzer
%defattr(-,root,root,-)
%{_bindir}/scan-build
%{_bindir}/scan-view
%{_libdir}/clang-analyzer

%files -n clang-doc
%defattr(-,root,root,-)
%doc tools/clang/docs/*

%files doc
%defattr(-,root,root,-)
%doc examples moredocs/html

%if %{with ocaml}
%files ocaml
%defattr(-,root,root,-)
%{_libdir}/ocaml/*.cma
%{_libdir}/ocaml/*.cmi
%{_libdir}/ocaml/META.llvm*
%{_libdir}/ocaml//dllllvm*

%files ocaml-devel
%defattr(-,root,root,-)
%{_libdir}/ocaml/*.a
%{_libdir}/ocaml/*.cmx*
%{_libdir}/ocaml/*.mli

%files ocaml-doc
%defattr(-,root,root,-)
%doc moredocs/ocamldoc/html/*
%endif

%if 0%{?_with_doxygen}
%files apidoc
%defattr(-,root,root,-)
%doc apidoc/*

%files -n clang-apidoc
%defattr(-,root,root,-)
%doc clang-apidoc/*
%endif


%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.2-1m)
- llvm-3.4.2

* Mon May 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.1-1m)
- llvm-3.4.1

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.4-2m)
- rebuild against graphviz-2.36.0-1m

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4-1m)
- llvm-3.4

* Mon Nov 25 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4-0.1m)
- llvm-3.4-rc1

* Mon Sep 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3-1m)
- llvm-3.3

* Sat Dec 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2-1m)
- llvm-3.2

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2-0.1m)
- llvm-3.2-rc3

* Tue May 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-2m)
- merge from fedora

* Sat May 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-1m)
- copy from trunk@r56680
- update to 3.1

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-1m)
- update 3.0 release

* Sun Nov 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.3m)
- update 3.0-rc4

* Sun Nov 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-0.2m)
- remove %%{_bindir}/llvm-tblgen conflicting with llvm from clang

* Sun Nov 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.1m)
- update 3.0-rc2

* Sun Oct  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-4m)
- fix deadlock issue again

* Mon Sep 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-3m)
- fix deadlock issue in %%check section

* Wed Sep 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-2m)
- fix build failure
-- update clang-2.9-momonga.patch
-- update gcc_ver macro
- merge fedora's changes
-- fix multilib issue
-- add BuildRequires

* Thu Apr 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-1m)
- update to 2.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-0.20110222.2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-0.20110222.1m)
- update to 2.9pre 20110222

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-3m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.7-2m)
- add obso llvm-apidoc

* Thu Apr 29 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-3m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6-1m)
- update to 2.6

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-2m)
- apply glibc210 patch

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-1m)
- update to 2.5
- delete included Patch2: llvm-2.4-gcc44.patch

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-3m)
- rebuild against rpm-4.6

* Tue Dec 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-2m)
- import Patch1: llvm-2.4-fix-ocaml.patch from fedora

* Mon Nov 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Tue Nov  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-0.1m)
- update to 2.4 prerelases
- enable with_gcc test

* Wed Aug 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-1m)
- import from Fedora to Momonga
- add --enable-bindings=none at %%configure, for avoid ocaml bindings make error
- fix force man .gz setting
- use specopt

* Wed Jun 18 2008 Bryan O'Sullivan <bos@serpentine.com> - 2.3-2
- Add dependency on groff

* Wed Jun 18 2008 Bryan O'Sullivan <bos@serpentine.com> - 2.3-1
- LLVM 2.3

* Thu May 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.2-4
- fix license tags

* Wed Mar  5 2008 Bryan O'Sullivan <bos@serpentine.com> - 2.2-3
- Fix compilation problems with gcc 4.3

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.2-2
- Autorebuild for GCC 4.3

* Sun Jan 20 2008 Bryan O'Sullivan <bos@serpentine.com> - 2.1-2
- Fix review comments

* Sun Jan 20 2008 Bryan O'Sullivan <bos@serpentine.com> - 2.1-1
- Initial version
