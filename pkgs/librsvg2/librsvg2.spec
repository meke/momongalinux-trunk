%global momorel 1
Name:           librsvg2
Summary:        An SVG library based on cairo
Version:        2.36.4
Release: %{momorel}m%{?dist}

License:        LGPLv2+
Group:          System Environment/Libraries
#VCS:           git:git://git.gnome.org/librsvg
Source:         http://download.gnome.org/sources/librsvg/2.36/librsvg-%{version}.tar.xz
NoSource: 0

Requires(post):   gdk-pixbuf2
Requires(postun): gdk-pixbuf2
BuildRequires:  libpng-devel
BuildRequires:  glib2-devel
BuildRequires:  gdk-pixbuf2-devel
BuildRequires:  gtk3-devel
BuildRequires:  pango-devel
BuildRequires:  libxml2-devel
BuildRequires:  freetype-devel
BuildRequires:  cairo-devel
BuildRequires:  cairo-gobject-devel
BuildRequires:  libgsf-devel
BuildRequires:  libcroco-devel
BuildRequires:  libgsf-devel
BuildRequires:  gobject-introspection-devel
# grr, librsvg does not install api docs if --disable-gtk-doc
BuildRequires:  gtk-doc
BuildRequires:  automake
BuildRequires:  autoconf

Provides:       librsvg3 = %{name}.%{version}-%{release}
Obsoletes:      librsvg3 <= 2.26.3-3.fc14

%description
An SVG library based on cairo.


%package devel
Summary:        Libraries and include files for developing with librsvg
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

Provides:       librsvg3-devel = %{name}.%{version}-%{release}
Obsoletes:      librsvg3-devel <= 2.26.3-3.fc14

%description devel
This package provides the necessary development libraries and include
files to allow you to develop with librsvg.

%prep
%setup -q -n librsvg-%{version}


%build
GDK_PIXBUF_QUERYLOADERS=/usr/bin/gdk-pixbuf-query-loaders-%{__isa_bits}
export GDK_PIXBUF_QUERYLOADERS
# work around an ordering problem in configure
enable_pixbuf_loader=yes
export enable_pixbuf_loader
%configure --disable-static  \
        --disable-gtk-doc \
        --disable-gtk-theme \
        --enable-introspection
%make 

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"
find %{buildroot} -name '*.la' -exec rm -f {} ';'

rm -f %{buildroot}%{_libdir}/mozilla/
rm -f %{buildroot}%{_sysconfdir}/gtk-2.0/gdk-pixbuf.loaders
rm -f %{buildroot}%{_datadir}/pixmaps/svg-viewer.svg

%post
/sbin/ldconfig
gdk-pixbuf-query-loaders-%{__isa_bits} --update-cache || :

%postun
/sbin/ldconfig
gdk-pixbuf-query-loaders-%{__isa_bits} --update-cache || :


%files
%doc AUTHORS COPYING COPYING.LIB NEWS README
%{_libdir}/librsvg-2.so.*
%{_libdir}/gdk-pixbuf-2.0/*/loaders/libpixbufloader-svg.so
%{_libdir}/girepository-1.0/*
%{_bindir}/rsvg-view-3
%{_bindir}/rsvg-convert
%{_mandir}/man1/rsvg-convert.1*

%files devel
%{_libdir}/librsvg-2.so
%{_includedir}/librsvg-2.0
%{_libdir}/pkgconfig/librsvg-2.0.pc
%{_datadir}/gir-1.0/*
%{_datadir}/vala/vapi/librsvg-2.0.vapi
%doc %{_datadir}/gtk-doc/html/rsvg-2.0

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.36.4-1m)
- update to 2.36.4

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.36.3-1m)
- update to 2.36.3

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.36.1-1m)
- reimport from fedora

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.2-1m)
- update to 2.34.2

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.1-1m)
- update to 2.34.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.34.0-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.0-1m)
- update to 2.34.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.3-3m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26.3-2m)
- modify %%post and %%postun for new gtk2

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.3-6m)
- rebuild against rpm-4.6

* Sun Nov  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-5m)
- fix pkg-config file (patch2)

* Mon Oct 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.2-4m)
- fix force bz2 man

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.2-3m)
- rebuild against firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.2-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2
- delete patch0 (merged)

* Sun Jan 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update 2.20.0

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Tue Aug 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Wed Jul 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16.1-5m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16.1-4m)
- BuildPrereq: freetype2-devel -> freetype-devel

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-3m)
- rename librsvg -> librsvg2

* Mon Nov 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.16.1-2m)
- copy from momonga's trunk (r12379)
- add librsvg-2.16.1-firefox-plugin.patch to build firefox plugin

* Fri Nov  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sun Sep 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sun Sep 10 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.14.4-7m)
- add Patch0: librsvg-2.14.4-cairo_pc.patch

* Mon Aug 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.4-6m)
- rebuild against expat-2.0.0-1m

* Mon Aug 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-5m)
- rebuild ageinst libcroco-0.6.1-2m libgsf-1.14.1-2m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-4m)
- delete libtool library

* Sat Jul  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-3m)
- add %%post script (gdk-pixbuf.loaders)

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-2m)
- delete duplicated dir

* Thu May 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-1m)
- update to 2.14.4

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.3-3m)
- rebuild against libgsf-1.14.0

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- rebuild against openssl-0.9.8a

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Sat Feb 25 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12.7-3m)
- rebuild with new nspr

* Sun Dec 4 2005 Nishio futoshi <futoshi@momonga-linux.org>
- (2.12.7-1m)
- update to 2.12.7
- Gnome 2.12.2 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-2m)
- delete autoreconf and make check

* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-1m)
- version up
- GNOME 2.12.1 Desktop

* Fri Mar 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.1-3m)
- rebuild against mozilla-1.7.6
- install moz_plugins to %%{_libdir}/mozilla/plugins

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (2.8.1-2m)
- moz_plugins should point mozilla_version directory.

* Fri Feb  4 2005 Toru Hoshina <t@momonga-linux.org>
- BuildPreReq: gimp-devel coused BuildPreReq loop.

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1
- GNOME 2.8 Desktop

* Fri Jun 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.5-1m)
- cancel %%requires_eq gtk+

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.4-1m)
- version 2.6.4
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.0-4m)
- rebuild against gtk+-2.4.0

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.0-3m)
- revised spec for enabling rpm 4.2.

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.0-2m)
- change docdir %%defattr

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0-1m)
- pretty spec file.

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Wed Sep 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.1-2m)

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Fri Jul 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-5m)
- rebuild against for libcroco-0.3.0

* Sat Jun 28 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.3.0-4m)
- add libgsf-devel to BuildPrereq,Requires.

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-3m)
- rebuild against for gtk+-2.2.2

* Fri Apr 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-2m)
- support libcroco.

* Wed Apr 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.5-1m)
- version 2.2.5

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.4-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.4-1m)
- version 2.2.4

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.3-1m)
- version 2.2.3

* Wed Feb  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2.1-2m)
- just rebuild for gtk+

* Mon Feb 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2.1-1m)
- version 2.2.2.1

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-4m)
- just rebuild for gtk+

* Sun Dec 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-3m)
- just rebuild for gtk+

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-2m)
- add requires_eq gtk+
- add Prereq gtk+

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-2m)
- rebuild against for gtk+-2.1.1

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Sun Oct  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-3m)
- 2.1.0-1m

* Sun Oct  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.0-2m)
- revise to prevent building failure

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-3m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-18k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-16k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-14k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-12k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-10k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-8k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-6k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-2k)
- version 1.1.6

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-6k)
- rebuild against for pango-1.0.0.rc2

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-4k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-2k)
- version 1.1.5

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.4-2k)
- version 1.1.4

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-12k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-6k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-4k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-2k)
- version 1.1.3
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-6k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-4k)
- rebuild against for glib-1.3.13

* Tue Feb  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-2k)
- version 1.1.2

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-8k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-6k)
- rebuild against for atk-0.10

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-2k)
- release version

* Mon Jan  7 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-0.02002010702k)
- version 1.1.1 cvs
- gnome2 env

* Tue Oct  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.0.2-2k)
- version 1.0.2

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.1-2k)
- arrange spec file

* Thu Aug 22 2001 Shingo Akagaki <dora@kondara.org>
- port from Jirai

* Mon Jul  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.1

* Tue Oct 10 2000 Robin Slomkowski <rslomkow@eazel.com>
- removed obsoletes from sub packages and added mozilla and trilobite
subpackages

* Wed Apr 26 2000 Ramiro Estrugo <ramiro@eazel.com>
- created this thing
