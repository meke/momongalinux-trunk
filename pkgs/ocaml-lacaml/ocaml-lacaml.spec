%global momorel 2
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-lacaml
Version:        5.5.2
Release:        %{momorel}m%{?dist}
Summary:        BLAS/LAPACK-interface for OCaml

Group:          Development/Libraries
License:        "LGPLv2 with exceptions"
URL:            http://www.ocaml.info/home/ocaml_sources.html#lacaml
Source0:        http://hg.ocaml.info/release/lacaml/archive/release-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-findlib-devel
BuildRequires:  lapack-devel
BuildRequires:  blas-devel

%global __ocaml_requires_opts -i Common -i Asttypes -i Parsetree
%global __ocaml_provides_opts -i Common -i Install_printers -i Io -i Utils

%description
This OCaml-library interfaces the BLAS-library (Basic Linear Algebra
Subroutines) and LAPACK-library (Linear Algebra routines), which are
written in FORTRAN.

This allows people to write high-performance numerical code for
applications that need linear algebra.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n lacaml-release-%{version}


%build
make
make examples

strip lib/dlllacaml_stubs.so


%install
# These rules work if the library uses 'ocamlfind install' to install itself.
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install

# By installing the *.cmx files, the compiler can do cross-module inlining.
install -m 0644 lib/*.cmx $RPM_BUILD_ROOT%{_libdir}/ocaml/lacaml


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYRIGHT LICENSE
%{_libdir}/ocaml/lacaml
%if %opt
%exclude %{_libdir}/ocaml/lacaml/*.a
%exclude %{_libdir}/ocaml/lacaml/*.cmxa
%exclude %{_libdir}/ocaml/lacaml/*.cmx
%endif
%exclude %{_libdir}/ocaml/lacaml/*.mli
%exclude %{_libdir}/ocaml/lacaml/*.ml
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc COPYRIGHT Changelog LICENSE README.txt TODO
%if %opt
%{_libdir}/ocaml/lacaml/*.a
%{_libdir}/ocaml/lacaml/*.cmxa
%{_libdir}/ocaml/lacaml/*.cmx
%endif
%{_libdir}/ocaml/lacaml/*.mli
%{_libdir}/ocaml/lacaml/*.ml


%changelog
* Wed Dec 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5.2-2m)
- rebuild for lapack-3.4.0

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.2-1m)
- update to 5.5.2
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.4.7-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.7-1m)
- update to 5.4.7
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1-1m)
- update to 5.4.1

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.3-1m)
- update to 5.0.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.9-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.9-1m)
- update to 4.6.9
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.2-1m)
- import from Fedora

* Fri May  2 2008 Richard W.M. Jones <rjones@redhat.com> - 4.3.2-1
- New upstream version 4.3.2.

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 4.3.1-2
- Rebuild for OCaml 3.10.2

* Mon Apr 21 2008 Richard W.M. Jones <rjones@redhat.com> - 4.3.1-1
- New upstream release 4.3.1.
- Fix upstream URL.

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 4.3.0-2
- Rebuild for ppc64.

* Wed Feb 20 2008 Richard W.M. Jones <rjones@redhat.com> - 4.3.0-1
- Initial RPM release.
