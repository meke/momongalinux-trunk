%global         momorel 3

Name:           perl-Plack
Version:        1.0030
Release:        %{momorel}m%{?dist}
Summary:        Perl Superglue for Web frameworks and Web Servers (PSGI toolkit)
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Plack/
Source0:        http://www.cpan.org/authors/id/M/MI/MIYAGAWA/Plack-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  mod_perl >= 2.0.8
BuildRequires:  perl-Apache-LogFormat-Compiler
BuildRequires:  perl-Devel-StackTrace >= 1.23
BuildRequires:  perl-Devel-StackTrace-AsHTML >= 0.11
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-ShareDir >= 1.00
BuildRequires:  perl-Filesys-Notify-Simple
BuildRequires:  perl-Hash-MultiValue >= 0.05
BuildRequires:  perl-HTTP-Body >= 1.06
BuildRequires:  perl-libwww-perl >= 5.814
BuildRequires:  perl-parent
BuildRequires:  perl-Pod-Parser
BuildRequires:  perl-Stream-Buffered
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Test-TCP >= 0.11
BuildRequires:  perl-Try-Tiny
BuildRequires:  perl-URI >= 1.36
Requires:       mod_perl >= 2.0.8
Requires:       perl-Apache-LogFormat-Compiler
Requires:       perl-Devel-StackTrace >= 1.23
Requires:       perl-Devel-StackTrace-AsHTML >= 0.11
Requires:       perl-File-ShareDir >= 1.00
Requires:       perl-Filesys-Notify-Simple
Requires:       perl-Hash-MultiValue >= 0.05
Requires:       perl-HTTP-Body >= 1.06
Requires:       perl-libwww-perl >= 5.814
Requires:       perl-parent
Requires:       perl-Pod-Parser
Requires:       perl-Stream-Buffered
Requires:       perl-Test-TCP >= 0.11
Requires:       perl-Try-Tiny
Requires:       perl-URI >= 1.36
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Plack is a set of tools for using the PSGI stack. It contains middleware
components, a reference server and utilities for Web application
frameworks. Plack is like Ruby's Rack or Python's Paste for WSGI.

%prep
%setup -q -n Plack-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Apache::Request)/d'

EOF
%define __perl_requires %{_builddir}/Plack-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/plackup
%{perl_vendorlib}/HTTP/Message
%{perl_vendorlib}/HTTP/Server/PSGI.pm
%{perl_vendorlib}/Plack.pm
%{perl_vendorlib}/Plack
%{perl_vendorlib}/auto/share/dist/Plack
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0030-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0030-2m)
- rebuild against perl-5.18.2

* Mon Nov 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0030-1m)
- update to 1.0030

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0029-1m)
- update to 1.0029

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0028-2m)
- rebuild against perl-5.18.1

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0028-1m)
- update to 1.0028

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0027-1m)
- update to 1.0027

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0024-2m)
- rebuild against perl-5.18.0

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0024-1m)
- update to 1.0024

* Tue Apr  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0023-1m)
- update to 1.0023

* Wed Apr  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0022-1m)
- update to 1.0022

* Tue Apr  2 2013 NARITA Koichi ,pulsar@momonga-linux.org>
- (1.0020-1m)
- update to 1.0020

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0018-2m)
- rebuild against perl-5.16.3

* Sat Mar  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0018-1m)
- update to 1.0018

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0016-1m)
- update to 1.0016

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0015-1m)
- update to 1.0015

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0014-1m)
- update to 1.0014

* Thu Nov 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0013-1m)
- update to 1.0013

* Mon Nov 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0011-1m)
- update to 1.0011

* Sat Nov  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0010-1m)
- update to 1.0010

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0009-2m)
- rebuild against perl-5.16.2

* Tue Oct 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0009-1m)
- update to 1.0009

* Sun Oct 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0007-1m)
- update to 1.0007

* Sat Oct 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0006-1m)
- update to 1.0006

* Wed Oct 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0005-1m)
- update to 1.0005

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0004-1m)
- update to 1.0004

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0003-1m)
- update to 1.0003

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0002-1m)
- update to 1.0002

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0001-2m)
- rebuild against perl-5.16.1

* Fri Jul 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0001-1m)
- update to 1.0001

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0000-1m)
- update to 1.0000

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9989-1m)
- update to 0.9989
- rebuild against perl-5.16.0

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9986-1m)
- update to 0.9986

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0085-1m)
- update to 0.9985

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9984-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9984-1m)
- update to 0.9984

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9983-1m)
- update to 0.9983

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9982-1m)
- update to 0.9982

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9981-1m)
- update to 0.9981

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9980-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9980-1m)
- update to 0.9980

* Wed May 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9979-1m)
- update to 0.9979

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9978-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9978-1m)
- update to 0.9978

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9977-1m)
- update to 0.9977

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9976-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9976-1m)
- update to 0.9976

* Sun Feb 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9973-1m)
- update to 0.9973

* Sat Feb 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9969-1m)
- update to 0,9969

* Thu Feb 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9968-1m)
- update to 0.9968

* Thu Jan 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9967-1m)
- update to 0.9967

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9965-1m)
- update to 0.9965

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9964-1m)
- update to 0.9964

* Tue Jan 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9963-1m)
- update to 0.9963

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9962-1m)
- update to 0.9962

* Sat Jan  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9961-1m)
- update to 0.9961

* Sun Dec 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9960-1m)
- update to 0.9960

* Wed Dec 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9959-1m)
- update to 0.9959

* Tue Dec 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9958-1m)
- update to 0.9958

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9957-1m)
- update to 0.9957

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9956-1m)
- update to 0.9956

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9954-1m)
- update to 0.9954

* Sat Dec  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9953-1m)
- update to 0.9953

* Fri Dec  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9952-1m)
- update to 0.9952

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9951-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9951-1m)
- update to 0.9951

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9950-1m)
- update to 0.9950

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9949-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9949-1m)
- update to 0.9949

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9944-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9944-1m)
- update to 0.9944

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9943-1m)
- update to 0.9943

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9942-1m)
- update to 0.9942

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9941-1m)
- update to 0.9941

* Sat Jul  3 2010 NARITA Koichi <puldar@momonga-linux.org>
- (0.9940-1m)
- update to 0.9940

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9939-1m)
- update to 0.9939

* Mon May 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9938-1m)
- update to 0.9938

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9937-2m)
- rebuild against perl-5.12.1

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9937-1m)
- update to 0.9937

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9936-1m)
- update to 0.9936

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9935-1m)
- update to 0.9935

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9934-1m)
- update to 0.9934

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9933-1m)
- update to 0.9933

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9932-1m)
- update to 0.9932

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9931-1m)
- update to 0.9931

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9929-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9929-1m)
- update to 0.9929

* Tue Mar 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9927-1m)
- update to 0.9927

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9920-1m)
- update to 0.9920

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9916-1m)
- update to 0.9916

* Tue Mar  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9915-1m)
- update to 0.9915

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9914-1m)
- update to 0.9914

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9913-1m)
- update to 0.9913

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9031-1m)
- update to 0.9031

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-liuxx.org>
- (0.9030-1m)
- update to 0.9030

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9029-1m)
- update to 0.9029

* Wed Jan  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9028-1m)
- update to 0.9028

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9027-1m)
- update to 0.9027

* Fri Jan  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9026-1m)
- update to 0.9026

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9025-1m)
- update to 0.9025

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9024-1m)
- update to 0.9024

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9017-1m)
- update to 0.9017

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9015-1m)
- update to 0.9015

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9014-1m)
- update to 0.9014

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9013-1m)
- update to 0.9013

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9012-1m)
- update to 0.9012

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9008-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9008-1m)
- update to 0.9008

* Mon Oct 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9007-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
