%global momorel 1
Summary: Rotates, compresses, removes and mails system log files
Name: logrotate
Version: 3.8.6
Release: %{momorel}m%{?dist}
License: GPL+
Group: System Environment/Base
Url: https://fedorahosted.org/logrotate/
Source: https://fedorahosted.org/releases/l/o/logrotate/logrotate-%{version}.tar.gz
NoSource: 0

Requires: coreutils >= 5.92 libsepol libselinux popt libacl
BuildRequires: libselinux-devel popt-devel libacl-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The logrotate utility is designed to simplify the administration of
log files on a system which generates a lot of log files.  Logrotate
allows for the automatic rotation compression, removal and mailing of
log files.  Logrotate can be set to handle a log file daily, weekly,
monthly or when the log file gets to a certain size.  Normally,
logrotate runs as a daily cron job.

Install the logrotate package if you need a utility to deal with the
log files on your system.

%prep
%setup -q

%build
## stil asprintf problem exists...
RPM_OPT_FLAGS="`echo $RPM_OPT_FLAGS | sed 's|-Wp,-D_FORTIFY_SOURCE=2|-Wp,-D_FORTIFY_SOURCE=1|g'`"

make %{?_smp_mflags} RPM_OPT_FLAGS="$RPM_OPT_FLAGS" WITH_SELINUX=yes WITH_ACL=yes

%install
rm -rf %{buildroot}
make PREFIX=%{buildroot} MANDIR=%{_mandir} install
mkdir -p %{buildroot}/%{_sysconfdir}/logrotate.d
mkdir -p %{buildroot}/%{_sysconfdir}/cron.daily
mkdir -p %{buildroot}/%{_localstatedir}/lib

install -p -m 644 examples/logrotate-default %{buildroot}/%{_sysconfdir}/logrotate.conf
install -p -m 755 examples/logrotate.cron %{buildroot}/%{_sysconfdir}/cron.daily/logrotate
touch %{buildroot}/%{_localstatedir}/lib/logrotate.status

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES COPYING
%attr(0755, root, root) %{_sbindir}/logrotate
%attr(0644, root, root) %{_mandir}/man8/logrotate.8*
%attr(0644, root, root) %{_mandir}/man5/logrotate.conf.5*
%attr(0755, root, root) %{_sysconfdir}/cron.daily/logrotate
%attr(0644, root, root) %config(noreplace) %{_sysconfdir}/logrotate.conf
%attr(0755, root, root) %dir %{_sysconfdir}/logrotate.d
%attr(0644, root, root) %verify(not size md5 mtime) %config(noreplace) %{_localstatedir}/lib/logrotate.status

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.6-1m)
- update 3.8.6

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-2m)
- fix build asprintf problem

* Tue Oct 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1
- [SECURITY] CVE-2011-1154, CVE-2011-1155 (fixed in 3.8.0)

* Thu Apr 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.9-3m)
- fix build failure; temporary relax _FORTIFY_SOURCE checking

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.9-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.9-1m)
- [SECURITY] CVE-2011-1098 CVE-2011-1154 CVE-2011-1155
- update to 3.7.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.8-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.8-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.8-1m)
- update to 3.7.8
-- import Patch1,2 from Fedora 11 (3.7.8-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.6-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.6-1m)
- update to 3.7.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.5-2m)
- rebuild against gcc43

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7.5-1m)
- update to 3.7.5

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.7.3-1m)
- update to 3.7.3

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (3.7.1-1m)
- sync with FC3(3.7.1-2).

* Fri Jan 30 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.7-1m)
- update to 3.7 - imported from Fedora (3.7-4)
- add (noreplace) to /etc/logrotated.conf
- %%global momorel

* Wed Oct 15 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.6.10-1m)
- update to 3.6.10 - fixed delaycompress directive
- add COPYING to %%doc

* Sat Jul 26 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.6.9-1m)
- update to 3.6.9
- use %%{momorel} macro
- sync with RawHide(logrotate-3.6.9-1)
-  changes are:
- * Wed Jul 09 2003 Elliot Lee <sopwith@redhat.com> 3.6.9-1
- - Fix #90229, #90274, #89458, #91408
- 
- * Mon Jan 20 2003 Elliot Lee <sopwith@redhat.com> 3.6.8-1
- - Old patch from pm@debian.org
- 
- * Tue Jan 14 2003 Elliot Lee <sopwith@redhat.com> 3.6.7-1
- - Fixes from bugzilla
- 
- * Fri Nov 15 2002 Elliot Lee <sopwith@redhat.com> 3.6.6-1
- - Commit patch from Fidelis Assis <fidelis@embratel.net.br>
- 
- * Thu Jun 20 2002 Elliot Lee <sopwith@redhat.com> 3.6.5-1
- - Commit fix for #65299

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (3.6.4-2k)
- ver up.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (3.5.9-2k)
- merge fro Jirai.

* Mon Oct 08 2001 Motonobu Ichimura <famao@kondara.org>
- (3.5.9-3k)
- up to 3.5.9

* Sat Dec  2 2000 Daiki Matsuda <dyky@df-usa.com>
- (3.3.2-5k)
- modified spec file with %{_mandir} macro

* Tue Aug 15 2000 Erik Troan <ewt@redhat.com>
- see CHANGES

* Sun Jul 23 2000 Erik Troan <ewt@redhat.com>
- see CHANGES

* Tue Jul 11 2000 Erik Troan <ewt@redhat.com>
- support spaces in filenames
- added sharedscripts

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- use %%{_mandir} for man pages

* Thu Feb 24 2000 Erik Troan <ewt@redhat.com>
- don't rotate lastlog

* Thu Feb 03 2000 Erik Troan <ewt@redhat.com>
- gzipped manpages
