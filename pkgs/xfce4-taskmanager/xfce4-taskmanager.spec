%global momorel 1

%global major 1.0
%global xfce4ver 4.11.0

Name:		xfce4-taskmanager
Version:	1.0.1
Release:	%{momorel}m%{?dist}
Summary:	Taskmanager for the Xfce desktop environment

Group:		User Interface/Desktops
License:        GPLv2+
URL:		http://goodies.xfce.org/projects/applications/%{name}
Source0:	http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	gettext, desktop-file-utils, perl-XML-Parser

%description
A simple taskmanager for the Xfce desktop environment.


%prep
%setup -qn %{name}-%{version}


%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'
%find_lang %{name}

desktop-file-install --vendor=""	        		  \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
  --add-category GTK                                      \
  --add-category Monitor                                  \
  --add-category X-Xfce                                   \
  --remove-category Application                           \
  --remove-category Utility                               \
  --delete-original                                       \
  --add-only-show-in=XFCE                                 \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1
- build against xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-6m)
- rebuild against xfce4-4.10.0

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-5m)
- rebuild aagainst xfce4 4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update
- rebuild aagainst xfce4 4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-2m)
- add only show in XFCE

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- import to Momonga from fedora

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.0-0.2.rc2
- Bump release for devel checkin.

* Tue Oct 03 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.0-0.1.rc2
- Update to 0.4-rc2.
- Add BR on perl(XML::Parser), drop intltool.

* Mon Oct 02 2006 Christoph Wickert <fedora christoph-wickert de> - 0.3.1-6
- Rebuild for XFCE 4.4.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.3.1-5
- Mass rebuild for Fedora Core 6.
- Fix %%defattr.

* Sat Jun 03 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-4
- Add intltool BR (#193444).

* Thu Feb 16 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-3
- Rebuild for Fedora Extras 5.

* Thu Dec 15 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-2
- Initial Fedora Extras version.
- Add xfce4-taskmanager.desktop.
- Remove useless NEWS from %%doc.
- Clean up specfile.

* Sat Jul 09 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-1.fc4.cw
- Updated to version 0.3.1.
- Rebuild for Core 4.

* Thu Apr 14 2005 Christoph Wickert <fedora wickert at arcor de> - 0.2.1-1.fc3.cw
- Initial RPM release.
