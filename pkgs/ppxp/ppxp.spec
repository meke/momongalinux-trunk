%global momorel 26

%global ppxpversion 2001080415
%global userlinkversion 0.99c

Summary: PPxP - PPP connection tool new generation
Name: ppxp
Version: 0.0%{ppxpversion}
Release: %{momorel}m%{?dist}
License: Apache
Group: System Environment/Daemons
Source0: http://www.dsl.gr.jp/~manabe/PPxP/packages/ppxp-%{ppxpversion}.tar.gz

#http://www.comp.nus.edu.sg/~cs4236/readings/out/html/asm_2checksum_8h-source.html
Source1: checksum.h

Patch1: ppxp-alpha.patch
Patch2: ppxp-errno.patch
Patch3: ppxp.gencat.lang.patch
Patch4: ppxp.rc.patch
Patch5: ppxp-gcc31.patch
Patch6: ppxp-kernel26.patch
Patch7: ppxp-gcc34.patch
Patch8: ppxp-0.02001080415-gcc4.patch
Patch9: ppxp_can-2005-0392.patch
Patch10: ppxp-osdep.patch
Patch11: ppxp-cflags.patch

URL: http://www.dsl.gr.jp/~manabe/PPxP/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: readline-devel >= 5.0
#Icon: ppxp.xpm
# NoSource: 0

%description 
PPxP is user level Point-to-Point  Protocol (PPP) module and
is most simple and fastest connecting in Linux environment.

See document directory if you will know more about PPxP.

This package contents CUI console, ppxp.
Optional packages tkppxp, xppxp are automatically maked.

* tkppxp: GUI console on tcl/tk (require tcl/tk package)
* xppxp : GUI console on xpm library (reqire xpm library package)

please install above package if you want.

%package devel
Summary: ppxp development libs without includes
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
ppxp development libs but thish package have no includes

%package -n tkppxp
Summary: tkppxp - ppxp's X console(tcl/tk version)
Group: Applications/Internet
Requires: tcl tk
Requires: %{name} = %{version}

%description -n tkppxp
tkppxp contents tcl library for control, and tk console for PPxP.

(require ppxp, tcl/tk packages)

%package -n xppxp
Summary: xppxp - ppxp's X console
Group: Applications/Internet
Requires: %{name} = %{version}

BuildRequires: autoconf213

%description -n xppxp
THis package contents GUI console for PPxP on X environment.

 * xppxp : normal version
 * xppxpm: small version

(require ppxp, xpm packages)

%prep

%setup -q -n ppxp
# %patch0 -p1
%ifarch alpha alphaev5
%patch1 -p1
%endif
%patch2 -p1 -b .errno
%patch3 -p1
%patch4 -p1
%patch5 -p1 -b .gcc31
%patch6 -p1 -b .kernel26
%patch7 -p1 -b .gcc34
%patch8 -p1 -b .gcc4
%patch9 -p1 -b .can-2005-0392
for i in console/inetd/html/*html; do
    iconv -f euc-jp -t iso-2022-jp $i > $i.tmp
    mv $i.tmp $i
done

cp %{SOURCE1} ./OS/Linux
%patch10 -p1
%patch11 -p1

%build
autoreconf-2.13
%configure
make 

%install
rm -rf --preserve-root %{buildroot}
make install virtual_root=%{buildroot}
cp lib/ppxp_so.a %{buildroot}%{_libdir}
mkdir -p  %{buildroot}%{_includedir}/ppxp
cp lib/xcio.h %{buildroot}%{_includedir}/ppxp
cp lib/xcmd.h %{buildroot}%{_includedir}/ppxp
cp lib/ppxp.h %{buildroot}%{_includedir}/ppxp
mv %{buildroot}%{_datadir}/locale/ja_JP %{buildroot}%{_datadir}/locale/ja
cd %{buildroot}%{_bindir}
ln -sf tkppxp qdial

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-   ,root,root)
%{_sbindir}/ppxpd
%{_sbindir}/in.ppxp
%{_bindir}/ppxp
%{_bindir}/plast
%{_datadir}/locale/*/*
%dir /var/log/ppxp
%dir %{_datadir}/ppxp
%dir %{_datadir}/ppxp/html
%{_datadir}/ppxp/html/*
%dir %{_sysconfdir}/ppxp
%dir %{_sysconfdir}/ppxp/chat
%dir %{_sysconfdir}/ppxp/conf
%dir %{_sysconfdir}/ppxp/ip
%dir %{_sysconfdir}/ppxp/modem
%dir %{_sysconfdir}/ppxp/rc
%{_sysconfdir}/ppxp/catcap
%config %{_sysconfdir}/ppxp/chat/*
%config %{_sysconfdir}/ppxp/conf/*
%config %{_sysconfdir}/ppxp/ip/*
%config %{_sysconfdir}/ppxp/keybind
%config %{_sysconfdir}/ppxp/modem/*
%config %{_sysconfdir}/ppxp/rc/*

%doc doc/*

%files devel
%defattr(-   ,root,root)
%{_libdir}/*.a
%{_includedir}/ppxp

%files -n tkppxp
%defattr(-   ,root,root)
%{_bindir}/qdial
%{_bindir}/tkppxp
%dir %{_libdir}/ppxp
%dir %{_libdir}/ppxp/tcl
%{_libdir}/ppxp/tcl/*
%dir %{_libdir}/ppxp/tkppxp
%{_libdir}/ppxp/tkppxp/*

%files -n xppxp
%defattr(-   ,root,root)
%{_bindir}/xppxp
%{_bindir}/xppxpm
%dir %{_datadir}/ppxp/label
%{_datadir}/ppxp/label/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02001080415-26m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02001080415-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.02001080415-24m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02001080415-23m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02001080415-22m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.02001080415-21m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.02001080415-20m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02001080415-19m)
- rebuild against gcc43

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.02001080415-18m)
- drop hard-coded CFLAGS

* Tue Aug  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.02001080415-17m)
- add Source1 from kernel 2.4.23 :-)

* Tue Aug  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.02001080415-16m)
- [SECURITY] CAN-2005-0392 (imported from VineSeed)

* Tue Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.02001080415-15m)
- rebuild against readline-5.0

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.02001080415-14m)
- revised installdir

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.02001080415-13m)
- add gcc4 patch
- Patch8: ppxp-0.02001080415-gcc4.patch

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.02001080415-12m)
- enable x86_64.

* Mon Oct 18 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.02001080415-11m)
- add patch7 for gcc 3.4

* Wed Sep 29 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (0.02001080415-10m)
- add patch6 for kernel 2.6

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.02001080415-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Jun  2 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.02001080415-8m)
- use autoreconf-old
- add gcc31.patch

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.02001080415-6k)
- nigittenu

* Sun Oct 14 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.02001080415-4k)
- add ppxp.rc.patch so as to work IP.START and IP.STOP work correctly
- convert character code of some html files to the code that is
  described in <HEAD>
- remove old changelog that is written in Japanese

* Sun Sep 30 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.02001080415-2k)

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (0.02001010912-10k)
- add alphaev5 support.

* Fri May 18 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.02001010912-6k)
- add ppxp.gencat.lang.patch for catalog files to be build correctly
- revised ppxp-alpha.patch and applied again :-P

* Wed May 02 2001 Motonobu Ichimura <famao@digitalfactory.co.jp>
- (0.02001010912-4k)
- import to Mary

* Wed May 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.02001010912-5k)
- some patch added for alpha

* Wed May 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.02001010912-3k)
- get sources from cvs.
- move qdial to tkppxp
- add some changes.

* Thu Dec 28 2000 Yoshito Komatsu <yoshito.komatsu>
- add ppxp.h in %files.
- fix URL etc.
- use %configure.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri May 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Source, URL )

* Thu Mar  9 2000 Shingo Akagaki <dora@kondara.org>
- more check

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Wed Jan 12 2000 Toru Hoshina <t@kondara.org>
- rebuild against snap shot, move message catalog to ja.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Oct 16 1999 binn <binn@kondara.org>
- mislinked (qdial->tkppxp) fixed (qdial->../X11R6/bin/tkppxp)
- added English description.

* Wed Jul 28 1999 Toru Hoshina <hoshina@best.com>
- version up.

* Sun Jun 27 1999 Toru Hoshina <hoshina@best.com>
- version up.

* Tue Apr 20 1999 Toru Hoshina <hoshina@best.com>
- separated userlink stuff.

* Mon Mar 30 1999 Toru Hoshina <hoshina@best.com>
- added kernel 2.2.4 patch.

* Sun Mar 07 1999 Toru Hoshina <hoshina@best.com>
- I misunderstood the version number :-P

* Sun Feb 28 1999 Toru Hoshina <hoshina@best.com>
- version up.
