#ifndef	_LIB_SUPPORT_H
#define	_LIB_SUPPORT_H

#ifdef	FALSE
#undef	FALSE
#undef	TRUE
#endif
typedef	enum {FALSE, TRUE} bool_t;

#ifndef	INADDR_ANY
# define	INADDR_ANY	0
#endif

#ifndef	INADDR_NONE
# define	INADDR_NONE	0xffffffff
#endif

#ifndef	HAVE_U_INT32_T
#if defined(linux) && ((__GNU_LIBRARY__ != 6) || (__GLIBC_MINOR__ != 2))
typedef	unsigned int	u_int32_t;
typedef	unsigned short	u_int16_t;
typedef	unsigned char	u_int8_t;
typedef	signed int	int32_t;
typedef	signed short	int16_t;
typedef	signed char	int8_t;
#endif
#endif

extern void *Malloc(), *Calloc(), *Realloc();
extern char *Strdup();
extern char *usrPPxP, *sysPPxP;

/*
#define	DEBUG_MALLOC
*/

#ifdef	DEBUG_MALLOC
inline static void
Free(void *p)
{
printf("Free  :%x\n", p);
    free(p);
}
#else
#ifdef Free
#undef Free
#endif
#define	Free(p)	free(p)
#endif

#define	TALLOC(type)	(type *)Malloc(sizeof(type))
#define	TCALLOC(type)	(type *)Calloc(1, sizeof(type))

struct list_s {
    struct list_s *next;
    char *name;
    char *data;
    void *priv;
};

typedef struct b256_s {
    u_int32_t b[8];
} b256_t;

static inline void
B256_SET(b256_t *bp, int type)
{
    bp->b[type>>5] |= 1 << (type&0x1F);
}

static inline void
B256_CLR(b256_t *bp, int type)
{
    bp->b[type>>5] &= ~(1 << (type&0x1F));
}

static inline bool_t
B256_ISSET(b256_t *bp, int type)
{
    return((bp->b[type>>5] & (1 << (type&0x1F))) ? TRUE: FALSE);
}

inline static void
B256_ZERO(b256_t *bp)
{
    memset((void *)bp, 0, sizeof(b256_t));
}

static inline void
B256_CPY(b256_t *dst, b256_t *src)
{
    memcpy((void *)dst, (void *)src, sizeof(b256_t));
}

extern int DecodeArgs();
extern void FreeArgs();
extern struct list_s *FileList(), *SortList();

#endif	/* _LIB_SUPPORT_H */
