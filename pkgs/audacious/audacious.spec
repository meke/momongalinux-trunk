%global         aud_ver 3.3.4
%global         momorel 1

Name:           audacious
Version:        %{aud_ver}
Release:        %{momorel}m%{?dist}
Summary:        A GTK2 based media player similar to xmms

Group:          Applications/Multimedia
License:        GPLv3
URL:            http://audacious-media-player.org/

Source0:        http://distfiles.audacious-media-player.org/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.6
BuildRequires:  zlib-devel, desktop-file-utils >= 0.9
BuildRequires:  libglade2-devel >= 2.4
BuildRequires:  GConf2-devel
BuildRequires:  gettext
BuildRequires:  mcs-devel >= 0.7.0
BuildRequires:  libmowgli-devel >= 0.9.50
BuildRequires:  dbus-devel >= 0.60, dbus-glib-devel >= 0.60
BuildRequires:  oniguruma-devel

#Requires:       audacious-plugins >= %{aud_ver}

Requires(post):   desktop-file-utils >= 0.9
Requires(postun): desktop-file-utils >= 0.9

Obsoletes:      bmp <= 0.9.7.1
Provides:       bmp = 0.9.7.1

Provides:       audacious-docklet
Obsoletes:      audacious-docklet < 0.1.1-3

%description
Audacious is a media player that currently uses a skinned
user interface based on Winamp 2.x skins. It is based on ("forked off")
BMP.

%package        libs
Summary:        Library files for Audacious
Group:          System Environment/Libraries

Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description    libs
Library files for Audacious

%package        devel
Summary:        Development files for Audacious
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}
Requires:       glib2-devel, gtk2-devel >= 2.6, GConf2-devel, libglade2-devel >= 2.4
Requires:       mcs-devel >= 0.1
Requires:       libmowgli-devel >= 0.4
Requires:       pkgconfig

Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig

Obsoletes:      bmp-devel <= 0.9.7.1
Provides:       bmp-devel = 0.9.7.1

%description    devel
Development files for Audacious

%prep
%setup -q

# Verify the value of the audacious(plugin-api) Provides.
api=$(grep '[ ]*#define[ ]*_AUD_PLUGIN_VERSION[ ]\+' src/audacious/plugin.h | sed 's!.*_AUD_PLUGIN_VERSION[ ]*\([0-9]\+\).*!\1!')
api_min=$(grep '[ ]*#define[ ]*_AUD_PLUGIN_VERSION_MIN' src/audacious/plugin.h | sed 's!.*_AUD_PLUGIN_VERSION_MIN[ ]*\([0-9]\+\).*!\1!')

perl -pi -e 's/^\.SILENT:.*$//' buildsys.mk.in

# From .desktop files remove MIME types the base build of the
# Fedora audacious-plugins package does not understand.
for t in \
    audio/mp3 \
    audio/mpeg \
    audio/mpegurl \
    audio/x-mp3 \
    audio/x-mpeg \
    audio/x-mpegurl \
    audio/x-ms-wma \
    audio/x-musepack \
    audio/prs.sid \
; do
    for f in audacious.desktop ; do
        cp ${f} ${f}.old
        sed -i "s!${t};!!g" $f
        diff -u ${f}.old ${f} || :
        rm -f ${f}.old
    done
done   

%build
%configure  \
    --with-buildstamp="Momonga package"  \
    --disable-sse2  \
    --disable-rpath \
    --disable-dependency-tracking \
    LIBS="-lm -lgmodule-2.0" \
    CPPFLAGS=-DGLIB_COMPILATION
make V=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

%find_lang %{name}

desktop-file-install --vendor=  \
    --dir %{buildroot}%{_datadir}/applications  \
    --remove-mime-type audio/mp3  \
    --remove-mime-type audio/mpeg  \
    --remove-mime-type audio/mpegurl  \
    --remove-mime-type audio/x-mp3  \
    --remove-mime-type audio/x-mpeg  \
    --remove-mime-type audio/x-mpegurl  \
    --remove-mime-type audio/x-ms-wma  \
    --remove-mime-type audio/x-scpls  \
    --remove-category Application \
    --add-category AudioVideo  \
    %{buildroot}%{_datadir}/applications/audacious.desktop

# install icon
mkdir %{buildroot}%{_datadir}/pixmaps
install -m 0644 %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/audacious.png %{buildroot}%{_datadir}/pixmaps/

#cd %%{buildroot}%%{_bindir}
#rm -f audacious audtool
#ln -s audacious2 audacious
#ln -s audtool2 audtool
#cd -

%clean
rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
update-desktop-database &> /dev/null || :

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL
%{_bindir}/audacious
%{_bindir}/audtool
%{_datadir}/audacious/
%{_mandir}/man[^3]/*
%{_datadir}/applications/*
%{_datadir}/pixmaps/*
%{_datadir}/icons/hicolor/*/apps/%{name}*.*

%files libs
%defattr(-,root,root,-)
#%{_libdir}/audacious
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/audacious/
%{_includedir}/libaudcore/
%{_includedir}/libaudgui/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.4-1m)
- update to 3.3.4

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-1m)
- update to 3.3.2

* Mon Aug 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3-1m)
- update to 3.3

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sun Mar 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-2m)
- build fix

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.4-1m)
- update to 3.0.4

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.2-1m)
- update to 3.0.2

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update to 3.0.1

* Fri Aug  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-1m)
- update to 3.0.0

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Sun Jul  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Thu Jun  2 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (2.5.1-2m)
- Fix Symlinks

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.4-2m)
- rebuild for new GCC 4.6

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-4m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-3m)
- rebuild against libmowgli-0.7.0

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3-2m)
- fix build on x86_64

* Tue Jun  1 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- update 2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-3m)
- build with --with-regexlib=oniguruma

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against rpm-4.6

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linu.org>
- (1.5.1-1m)
- update to 1.5.1
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc43

* Wed Apr 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.0-1m)
- updated to 1.5.0

* Sun Mar 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-2m)
- delete "Requires: audacious-plugins" temporary, 
  which causes a dependency loop between audacious and audacious-plugins

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.6-1m)
- import to Momonga from Fedora

* Mon Feb 11 2008 Ralf Ertzinger <ralf@skytale.net> 1.4.6-1
- Update to 1.4.6

* Sat Dec 29 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.5-1
- Update to 1.4.5

* Mon Dec 03 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.4-1
- Update to 1.4.4

* Sat Dec 01 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.2-3
- Add patch to fix streams sometimes being left open
- Obsolete: audacious-docklet

* Thu Nov 19 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.2-2
- Update to 1.4.2

* Tue Aug 28 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.3.2-3
- Rebuild for selinux ppc32 issue.

* Sat Aug 25 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.2-2
- Clarify License: tag

* Sat Apr 07 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.2-1.fc7
- Update to 1.3.2

* Mon Apr 02 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.1-2.fc7
- Add missing Requires: to -devel package

* Mon Apr 02 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.1-1.fc7
- Update to 1.3.1
- Rebase still necessary patches

* Sun Dec 24 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.2-2.fc7
- Remove audacious-1.1.1-controlsocket-name.patch due to request
  from upstream, xmms and audacious are not entirely compatible

* Sun Nov 30 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.2-1.fc7
- Update to 1.2.2

* Sun Nov 26 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.1-1.fc7
- Update to 1.2.1
- Split off libaudacious into a separate package to handle the
  (now externally provided and built) plugins better

* Wed Oct 18 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.2-2.fc6
- Add Obsoletes/Provides for BMP

* Wed Sep 06 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.2-1.fc6
- Update to 1.1.2

* Thu Aug 17 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.1-6.fc6
- Another go at the %%20 problem

* Mon Aug 14 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.1-4.fc6
- Fix %%20 in playlist entries

* Sun Jul 30 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.1-3.fc6
- Bump for rebuild

* Sun Jul 30 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.1-2.fc6
- Change the name of the control socket to "xmms" instead of
  audacious. This makes programs that remote control xmms
  (and compatibles) work.

* Sun Jul 30 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.1-1.fc6
- Update to 1.1.1
- Drop amidi path patch
- Add shaded playlist skin patch (seems like audacious needs it,
  too)

* Fri Jul 21 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.0-1.fc6
- Update to 1.1.0 final
- Rediff some patches

* Sun Jul 9 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.0-0.2.dr2.fc6
- Fix quoting of filenames

* Thu Jun 29 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.0-0.1.dr2.fc6
- Fixed version for Extras review
  - Build OSS, arts and jack output plugins
  - Split esd, arts and jack into separate packages
  - Fix rpath issue
  - Fix absolute symlinks

* Sat Jun 24 2006 Ralf Ertzinger <ralf@skytale.net> 1.1.0-0.0.dr2.fc6
- Initial build for Fedora Extras
