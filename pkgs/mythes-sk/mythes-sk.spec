%global momorel 4

Name: mythes-sk
Summary: Slovak thesaurus
%define upstreamid 20100204
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://www.sk-spell.sk.cx/thesaurus/download/OOo-Thesaurus2-sk_SK.zip
Group: Applications/Text
URL: http://www.sk-spell.sk.cx/thesaurus/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python, perl
License: BSD
BuildArch: noarch

%description
Slovak thesaurus.

%prep
%setup -q -c

%build
for i in README_th_sk_SK_v2.txt; do
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mythes
cp -p th_sk_SK_v2.* $RPM_BUILD_ROOT/%{_datadir}/mythes

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_th_sk_SK_v2.txt
%{_datadir}/mythes/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100204-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100204-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100204-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20100204-1m)
- sync with Fedora 13 (0.20100204-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090402-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090402-1m)
- update to 20090402

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20050503-3m)
- rebuild against rpm-4.6

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20050503-2m)
- modify %%files to avoid conflicting

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20050503-1m)
- import from Fedora

* Wed Nov 28 2007 Caolan McNamara <caolanm@redhat.com> - 0.20050503-1
- initial version
