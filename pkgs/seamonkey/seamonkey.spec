%global momorel 1
%global relname TokyoTownPages
%global seamonkey_release %{version}-%{release}.%{relname}
%global build_langpacks 1
%global langpackdate 20140617
%global subdir_name comm
%global subdir_ver release
%global sources_subdir %{subdir_name}-%{subdir_ver}
%global homepage http://www.momonga-linux.org/
%global _use_internal_dependency_generator 0
%global __find_requires %{SOURCE1000}
%global nspr_version 4.10.6
%global nss_version 3.16.1

Summary: Web browser and mail reader
Name: seamonkey

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change this configuration, please copy it to
# ${HOME}/rpm/specopt/seamonkey.specopt and edit it.

## Configuration
%{?!build_devel:        %global build_devel                0}

Version: 2.26.1
Release: %{momorel}m%{?dist}
License: "MPLv2.0 or GPLv2+ or LGPLv2+"
Group: Applications/Internet
URL: http://www.mozilla.org/projects/seamonkey/
Source0: ftp://ftp.mozilla.org/pub/%{name}/releases/%{version}/source/%{name}-%{version}.source.tar.bz2
NoSource: 0
Source1: %{name}-langpacks-%{version}-%{langpackdate}.tar.bz2
Source2: %{name}.sh.in
Source3: %{name}-icon.png
Source4: %{name}.desktop
Source5: mozilla-make-package.pl
Source10: %{name}-mozconfig
Source11: %{name}-mail.desktop
Source12: %{name}-mail-icon.png
Source13: %{name}-compose.desktop
Source14: %{name}-compose-icon.png
Source15: mozilla-xpcom-exclude-list
Source16: mozilla-psm-exclude-list
Source20: %{name}-xft.js
Source21: %{name}-momonga-default-prefs.js
Source100: %{name}-remote
Source101: %{name}-remote-tab
Source1000: find-external-requires
Patch0: %{name}-2.0-lang.patch
Patch11: mozilla-nongnome-proxies.patch
Patch12: mozilla-prefer_plugin_pref.patch
Patch13: mozilla-shared-nss-db.patch
Patch14: mozilla-sle11.patch
Patch15: mozilla-language.patch
Patch16: mozilla-ntlm-full-path.patch
Patch17: mozilla-ua-locale.patch
Patch18: mozilla-ppc.patch
Patch19: mozilla-icu-strncat.patch
Patch20: mozilla-libproxy-compat.patch
Patch21: mozilla-ppc64le-build.patch
Patch22: mozilla-ppc64le-javascript.patch
Patch23: mozilla-ppc64le-libffi.patch
Patch24: mozilla-ppc64le-mfbt.patch
Patch25: mozilla-ppc64le-webrtc.patch
Patch26: mozilla-ppc64le-xpcom.patch
Patch30: mozilla-aarch64-bmo-810631.patch
Patch31: mozilla-aarch64-bmo-962488.patch
Patch32: mozilla-aarch64-bmo-963027.patch
Patch33: mozilla-aarch64-bmo-963023.patch
Patch34: mozilla-aarch64-bmo-963024.patch
Patch40: %{name}-ua-locale.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(post): perl
Requires(post): psmisc
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): perl
Requires(postun): psmisc
Requires: nspr >= %{nspr_version}
Requires: nss >= %{nss_version}
Requires: zlib >= 1.2.2-2m
BuildRequires: ImageMagick
BuildRequires: ORBit2-devel
BuildRequires: alsa-lib-devel
BuildRequires: atk-devel
BuildRequires: cairo-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: gtk2-devel
BuildRequires: hunspell-devel
BuildRequires: krb5-devel
BuildRequires: libIDL-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libgnome-devel
BuildRequires: libgnomeui-devel
BuildRequires: libnotify-devel
BuildRequires: libpng-devel
BuildRequires: libproxy-devel
BuildRequires: libstdc++-devel
BuildRequires: nspr-devel >= %{nspr_version}
BuildRequires: nss-devel >= %{nss_version}
BuildRequires: pango-devel >= 1.8.0
BuildRequires: perl
BuildRequires: python >= 2.7.2
BuildRequires: sed
BuildRequires: startup-notification-devel
BuildRequires: yasm
BuildRequires: zip
BuildRequires: zlib-devel >= 1.2.2-2m
AutoProv: 0
Provides: webclient
Obsoletes: %{name}-chat
Obsoletes: %{name}-dom-inspector
Obsoletes: %{name}-js-debugger
Obsoletes: %{name}-mail
Obsoletes: %{name}-psm
Obsoletes: %{name}-extensions-copyurlplus
Obsoletes: %{name}-extensions-googlebar
Obsoletes: %{name}-extensions-linky
Obsoletes: %{name}-extensions-prefbar
Obsoletes: %{name}-extensions-tabextensions
Obsoletes: %{name}-language-pack-japanese
Obsoletes: %{name}-suite
%if ! %{build_devel}
Obsoletes: %{name}-devel
%endif

%description
Web-browser, advanced e-mail and newsgroup client, IRC chat client, 
and HTML editing made simple -- all your Internet needs in one application.

%if %{build_devel}
%package devel
Summary: SeaMonkey - files for developing
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: nspr-devel >= %{nspr_version}
Requires: nss-devel >= %{nss_version}

%description devel
Libraries and includes files for developing programs based on SeaMonkey.
%endif

%prep

%setup -q -c

pushd %{sources_subdir}

%patch0 -p0 -b .lang

# mozilla patches
pushd mozilla
%patch11 -p1
%patch12 -p1
%patch13 -p1
# hold
# %%patch14 -p1
%patch15 -p1
%patch16 -p1
%patch17 -p1
%patch18 -p1
%patch19 -p1
# hold
# %%patch20 -p1
%patch21 -p1
%patch22 -p1
%patch23 -p1
%patch24 -p1
%patch25 -p1
%patch26 -p1
%patch30 -p1
%patch31 -p1
%patch32 -p1
%patch33 -p1
%patch34 -p1
popd

# comm patches
# hold
# %%patch40 -p1

rm -f .mozconfig
install -m 644 %{SOURCE10} .mozconfig

popd

%build
pushd %{sources_subdir}

# use --enable-optimize option of mozconfig instead of CFLAGS and CXXFLAGS
export MOZ_OPT_FLAGS=$(echo "%{optflags}")

export PREFIX='%{_prefix}'
export LIBDIR='%{_libdir}'

MOZ_SMP_FLAGS=-j1

%ifnarch ppc ppc64 s390 s390x
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -gt 1 ] && MOZ_SMP_FLAGS=-j2
%endif

make -f client.mk build STRIP="/bin/true" MOZ_MAKE_FLAGS="$MOZ_SMP_FLAGS" BUILD_OFFICIAL=0 MOZILLA_OFFICIAL=0

popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

pushd %{sources_subdir}

make -C obj-%{_host} install DESTDIR=%{buildroot} STRIP="/bin/true" BUILD_OFFICIAL=0 MOZILLA_OFFICIAL=0 MOZ_PKG_FATAL_WARNINGS=0

# create a list of all of the different package and the files that
# will hold them

rm -f ../mozilla.list

echo %defattr\(-,root,root\) > ../mozilla.list

# build all of the default browser components
# base seamonkey package (mozilla.list)

%{SOURCE5} --package xpcom --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version} \
    --exclude-file=%{SOURCE15}

%{SOURCE5} --package browser --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version}

%{SOURCE5} --package spellcheck --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version}

%{SOURCE5} --package psm --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version} \
    --exclude-file=%{SOURCE16}

%{SOURCE5} --package mail --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version}

%{SOURCE5} --package chatzilla --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version}

%{SOURCE5} --package venkman --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version}

%{SOURCE5} --package inspector --output-file %{_builddir}/%{name}-%{version}/mozilla.list \
    --package-file obj-%{_host}/suite/installer/package-manifest \
    --install-dir %{buildroot}%{_libdir}/%{name}-%{version} \
    --install-root %{_libdir}/%{name}-%{version}

%if %{build_langpacks}
# Install langpacks 
tar xjf %{SOURCE1}
for langpack in `ls %{name}-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionfile=%{buildroot}%{_libdir}/%{name}-%{version}/extensions/langpack-$language@%{name}.mozilla.org.xpi

  tmpdir=`mktemp -d %{name}.XXXXXXXX`
  langtmp=$tmpdir/%{name}/langpack-$language
  mkdir -p $langtmp
  xpifile=%{name}-langpacks/$language.xpi
  unzip $xpifile -d $langtmp

  sed -i -e "s|browser.startup.homepage.*$|browser.startup.homepage=%{homepage}|g;" \
         $langtmp/chrome/$language/locale/$language/navigator-region/region.properties

  find $langtmp -type f | xargs chmod 644
  pushd $langtmp
  zip -r -D $extensionfile chrome chrome.manifest install.rdf
  popd
  rm -rf $tmpdir

  language=`echo $language | sed -e 's/-/_/g'`
  extensionfile=`echo $extensionfile | sed -e "s,^%{buildroot},,"`
  echo "%%lang($language) $extensionfile" >> ../mozilla.list
done
rm -rf %{name}-langpacks
%endif # build_langpacks

# set up our desktop files
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
mkdir -p %{buildroot}%{_datadir}/applications

# install icons
convert -scale 16x16 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}-icon.png
convert -scale 22x22 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}-icon.png
convert -scale 32x32 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}-icon.png
convert -scale 48x48 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}-icon.png
convert -scale 64x64 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}-icon.png
install -m 644 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/
ln -s ../icons/hicolor/48x48/apps/%{name}-icon.png \
  %{buildroot}%{_datadir}/pixmaps/%{name}-icon.png

ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}-icon.png \
  %{buildroot}%{_datadir}/pixmaps/%{name}.png

convert -scale 16x16 %{SOURCE12} \
  %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}-mail-icon.png
convert -scale 22x22 %{SOURCE12} \
  %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}-mail-icon.png
install -m 644 %{SOURCE12} \
  %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}-mail-icon.png
ln -s ../icons/hicolor/32x32/apps/%{name}-mail-icon.png \
  %{buildroot}%{_datadir}/pixmaps/%{name}-mail-icon.png

convert -scale 16x16 %{SOURCE14} \
  %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}-compose-icon.png
convert -scale 22x22 %{SOURCE14} \
  %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}-compose-icon.png
install -m 644 %{SOURCE14} \
  %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}-compose-icon.png
ln -s ../icons/hicolor/32x32/apps/%{name}-compose-icon.png \
  %{buildroot}%{_datadir}/pixmaps/%{name}-compose-icon.png

# klipper needs mozilla.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/mozilla.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/mozilla.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/mozilla.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/mozilla.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/mozilla.png
ln -s ./%{name}-icon.png \
  %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/mozilla.png
ln -s ../icons/hicolor/48x48/apps/%{name}-icon.png \
  %{buildroot}%{_datadir}/pixmaps/mozilla.png

# install desktop files
install -c -m 644  %{SOURCE4} %{SOURCE11} %{SOURCE13} \
  %{buildroot}%{_datadir}/applications/

if [ ! -d %{buildroot}%{_libdir}/%{name}-%{version}/icons/ ]; then
  mkdir -m 755 %{buildroot}%{_libdir}/%{name}-%{version}/icons
fi

if [ ! -d %{buildroot}%{_libdir}/%{name}-%{version}/plugins/ ]; then
  mkdir -m 755 %{buildroot}%{_libdir}/%{name}-%{version}/plugins
fi

# install our seamonkey.sh file
rm -f %{buildroot}%{_bindir}/%{name}
cat %{SOURCE2} | sed -e 's/MOZILLA_VERSION/%{version}/g' \
         -e 's,LIBDIR,%{_libdir},g' > \
  %{buildroot}%{_bindir}/%{name}

chmod 755 %{buildroot}%{_bindir}/%{name}

# install the defaults fonts for xft
install -m 644 %{SOURCE20} %{buildroot}%{_libdir}/%{name}-%{version}/defaults/pref/xft.js

# set up Momonga default preferences
cat %{SOURCE21} | sed -e 's/SEAMONKEY_RPM_VR/%{seamonkey_release}/g' > momonga-default-prefs
install -m 644 momonga-default-prefs %{buildroot}%{_libdir}/%{name}-%{version}/defaults/pref/all-momonga.js
rm -f momonga-default-prefs

# we own %%{_libdir}/%%{name}/plugins which is the version-independent
# place that plugins can be installed
mkdir -p %{buildroot}%{_libdir}/%{name}/plugins

install -m 755 %{SOURCE100} %{buildroot}%{_bindir}/
install -m 755 %{SOURCE101} %{buildroot}%{_bindir}/

# backward compatibility
pushd %{buildroot}%{_bindir}
ln -s %{name} mozilla
ln -s %{name}-remote mozilla-remote
ln -s %{name}-remote-tab mozilla-remote-tab
popd

popd

%if ! %{build_devel}
# remove unneeded stuff
rm -rf %{buildroot}%{_includedir}/%{name}-%{version}
rm -rf %{buildroot}%{_libdir}/%{name}-devel-%{version}
rm -rf %{buildroot}%{_datadir}/idl/%{name}-%{version}
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
rm -f mozilla.list

%post
# run ldconfig before regxpcom
/sbin/ldconfig >/dev/null 2>/dev/null
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
/sbin/ldconfig >/dev/null 2>/dev/null
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files -f mozilla.list
%defattr(-,root,root)
%{_bindir}/%{name}
%{_bindir}/%{name}-remote
%{_bindir}/%{name}-remote-tab
%{_bindir}/mozilla
%{_bindir}/mozilla-remote
%{_bindir}/mozilla-remote-tab

# for plugins
%{_libdir}/%{name}

%dir %{_libdir}/%{name}-%{version}

%dir %{_libdir}/%{name}-%{version}/chrome
%dir %{_libdir}/%{name}-%{version}/chrome/icons
%dir %{_libdir}/%{name}-%{version}/chrome/icons/default

%dir %{_libdir}/%{name}-%{version}/components

%dir %{_libdir}/%{name}-%{version}/defaults
%dir %{_libdir}/%{name}-%{version}/defaults/messenger

%{_libdir}/%{name}-%{version}/defaults/messenger/mailViews.dat

%dir %{_libdir}/%{name}-%{version}/defaults/pref

%{_libdir}/%{name}-%{version}/defaults/pref/all-momonga.js
%{_libdir}/%{name}-%{version}/defaults/pref/xft.js

%dir %{_libdir}/%{name}-%{version}/defaults/profile

%{_libdir}/%{name}-%{version}/defaults/profile/localstore.rdf
%{_libdir}/%{name}-%{version}/defaults/profile/panels.rdf

%dir %{_libdir}/%{name}-%{version}/dictionaries

%{_libdir}/%{name}-%{version}/dictionaries/en-US.aff
%{_libdir}/%{name}-%{version}/dictionaries/en-US.dic

%dir %{_libdir}/%{name}-%{version}/distribution
%dir %{_libdir}/%{name}-%{version}/distribution/extensions

%dir %{_libdir}/%{name}-%{version}/extensions

%dir %{_libdir}/%{name}-%{version}/icons

%dir %{_libdir}/%{name}-%{version}/isp

%dir %{_libdir}/%{name}-%{version}/plugins

%dir %{_libdir}/%{name}-%{version}/searchplugins

%{_libdir}/%{name}-%{version}/searchplugins/creativecommons.xml
%{_libdir}/%{name}-%{version}/searchplugins/google.xml
%{_libdir}/%{name}-%{version}/searchplugins/wikipedia.xml
%{_libdir}/%{name}-%{version}/searchplugins/yahoo.xml

%{_libdir}/%{name}-%{version}/chrome.manifest
%{_libdir}/%{name}-%{version}/omni.ja
%{_libdir}/%{name}-%{version}/removed-files

%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/%{name}-compose.desktop
%{_datadir}/applications/%{name}-mail.desktop
%{_datadir}/icons/hicolor/*/apps/mozilla.png
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}-compose-icon.png
%{_datadir}/icons/hicolor/*/apps/%{name}-icon.png
%{_datadir}/icons/hicolor/*/apps/%{name}-mail-icon.png
%{_datadir}/pixmaps/mozilla.png
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/pixmaps/%{name}-compose-icon.png
%{_datadir}/pixmaps/%{name}-icon.png
%{_datadir}/pixmaps/%{name}-mail-icon.png

%if %{build_devel}
%files devel
%defattr(-,root,root)
%{_includedir}/%{name}-%{version}
%{_libdir}/%{name}-devel-%{version}
%{_datadir}/idl/%{name}-%{version}
%endif

%changelog
* Tue Jun 17 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26.1-1m)
- version 2.26.1
- update langpacks
- set relname TokyoTownPages

* Sat May  3 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26-1m)
- version 2.26
- update langpacks
- import aarch64-bmo patches and icu-strncat.patch from opensuse
- update ppc64le patches and shared-nss-db.patch from opensuse
- set relname CityOfLight

* Tue Mar 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.25-1m)
- version 2.25
- update langpacks
- update seamonkey-2.0-lang.patch
- update mozilla-shared-nss-db.patch from opensuse
- update ppc64le(-build).patch from opensuse
- update ppc64le-libffi.patch from opensuse
- update ppc64le-xpcom.patch from opensuse
- import 3 new ppc64le patches from opensuse
- set relname Waltz

* Thu Feb  7 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24-1m)
- version 2.24
- update langpacks
- import 3 ppc64le patches from opensuse
- update mozilla-shared-nss-db.patch from opensuse
- set relname Ivory

* Sat Dec 14 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.23-1m)
- version 2.23
- update langpacks
- update mozilla-nongnome-proxies.patch from opensuse
- update mozilla-shared-nss-db.patch from opensuse
- set relname Starmine

* Tue Nov 19 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.22.1-1m)
- version 2.22.1
- update langpacks
- set relname HappyEnd

* Fri Nov  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.22-2m)
- fix build on i686

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.22-1m)
- version 2.22
- update langpacks
- set relname BeautifulMyLife

* Thu Sep 19 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.21-1m)
- version 2.21
- update langpacks
- update mozilla-ppc.patch from opensuse
- update mozilla-shared-nss-db.patch from opensuse
- set relname PandaMan

* Fri Aug  9 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20-1m)
- version 2.20
- update langpacks
- update mozilla-shared-nss-db.patch from opensuse
- update seamonkey-ua-locale.patch from opensuse
- remove seamonkey-shared-nss-db.patch
- set relname LongLongWay

* Fri Jul  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.19-1m)
- version 2.19
- update langpacks
- remove mozilla-gstreamer-760140.patch
- update mozilla-language.patch from opensuse
- update mozilla-nongnome-proxies.patch from opensuse
- update mozilla-ppc.patch from opensuse
- update mozilla-prefer_plugin_pref.patch from opensuse
- update mozilla-shared-nss-db.patch from opensuse
- set relname Rain

* Sat Apr 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.17.1-1m)
- version 2.17.1
- update langpacks
- set relname RollingStar

* Fri Apr 12 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.17-2m)
- apply mozilla-gstreamer-760140.patch from opensuse again

* Wed Apr  3 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.17-1m)
- version 2.17
- update langpacks
- update mozilla-shared-nss-db.patch
- remove mozilla-gstreamer-760140.patch
- remove mozilla-webrtc-ppc.patch
- set relname FeelMySoul

* Thu Mar 14 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16.2-1m)
- version 2.16.2
- update langpacks
- set relname FriendsAgain

* Sat Mar  9 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16.1-1m)
- version 2.16.1
- update langpacks
- set relname CoffeeMilkCrazy

* Fri Feb 22 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16-1m)
- version 2.16
- update langpacks
- update mozilla-gstreamer-760140.patch from opensuse
- remove mozilla-gstreamer-803287.patch
- update mozilla-libproxy-compat.patch from opensuse
- import mozilla-webrtc-ppc.patch from opensuse
- remove mozilla-webrtc.patch
- set relname Cloudy

* Tue Feb  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15.2-1m)
- version 2.15.2
- update langpacks
- set relname CameraCameraCamera

* Tue Jan 22 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15.1-1m)
- version 2.15.1
- update langpacks
- set relname DolphinSong

* Fri Jan 11 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15-1m)
- version 2.15
- update langpacks
- replace gstreamer.patch to mozilla-gstreamer-803287.patch from opensuse
- update mozilla-nongnome-proxies.patch from opensuse
- update mozilla-prefer_plugin_pref.patch from opensuse
- update mozilla-shared-nss-db.patch from opensuse
- update mozilla-language.patch from opensuse
- update mozilla-ua-locale.patch from opensuse
- update mozilla-gstreamer-760140.patch from opensuse
- import mozilla-webrtc.patch from opensuse
- import mozilla-libproxy-compat.patch from opensuse
- set relname GrooveTube

* Mon Dec  3 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.1-1m)
- version 2.14.1
- update langpacks
- set relname Heart

* Wed Nov 21 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14-1m)
- version 2.14
- update langpacks
- update mozilla-prefer_plugin_pref.patch from opensuse
- update mozilla-shared-nss-db.patch from opensuse
- update mozilla-ntlm-full-path.patch from opensuse
- update mozilla-gstreamer-760140.patch from opensuse
- update seamonkey-shared-nss-db.patch from opensuse
- set relname JustLikeThat

* Sun Oct 28 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-1m)
- version 2.13.2
- update langpacks
- set relname IndianChief

* Sat Oct 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.1-1m)
- version 2.13.1
- update langpacks
- set relname StarSurfer

* Wed Oct 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13-1m)
- version 2.13
- update langpacks
- update mozilla-shared-nss-db.patch, mozilla-gstreamer.patch from opensuse
- update mozilla-ntlm-full-path.patch and mozilla-ppc.patch from opensuse
- import mozilla-gstreamer-760140.patch from opensuse
- remove --with-system-jpeg for the moment to enable build
- set relname CrystalFall

* Tue Sep 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.1-1m)
- version 2.12.1
- update langpacks
- set relname EarthRise

* Wed Aug 29 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12-1m)
- version 2.12
- set --enable-gstreamer and --disable-updater
- update langpacks
- import gstreamer.patch and ppc.patch from opensuse
- update shared-nss-db.patch and ua-locale.patch from opensuse
- remove gcc47.patch and yarr-pcre.patch
- set relname HeavenlyStar

* Wed Jul 18 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11-1m)
- version 2.11
- update langpacks
- update gcc47.patch, prefer_plugin_pref.patch, shared-nss-db.patch and yarr-pcre.patch from opensuse
- rmeove revert_621446.patch and system-nspr.patch
- change License to MPLv2.0
- set relname LightBrownOfMapleSugar

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.1-2m)
- rebuild for glib 2.33.2

* Mon Jun 18 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.1-1m)
- version 2.10.1
- update langpacks
- set relname Antigua

* Thu Jun  7 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10-1m)
- [SECURITY] version 2.10
- set --enable-gstreamer
- update langpacks
- import system-nspr.patch from opensuse
- update gcc47.patch, nongnome-proxies.patch and shared-nss-db.patch from opensuse
- completely remove mailnews-literals.patch
- set relname LakePlacidBlue

* Tue May  1 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.1-1m)
- [CRITICAL BUG FIXES] version 2.9.1
- update langpacks, add two language supports (uk and zh_TW)
- set relname BurgundyMist

* Wed Apr 25 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9-1m)
- [SECURITY] version 2.9
- update langpacks
- revive package devel and set build_devel 0
- import sle11.patch, revert_621446.patch, gcc47.patch, yarr-pcre.patch and mailnews-literals.patch from opensuse
- and apply revert_621446.patch
- set relname ShellPink

* Wed Mar 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-1m)
- version 2.8
- update langpacks
- update nongnome-proxies.patch and shared-nss-db.patch from opensuse
- set relname DaphneBlue

* Sat Feb 18 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.2-1m)
- version 2.7.2
- update langpacks
- set relname CompetitionRed

* Tue Feb 14 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.1-2m)
- add BR: yasm

* Sun Feb 12 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-1m)
- version 2.7.1
- update langpacks
- set relname CandyAppleRed

* Thu Feb  2 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7-1m)
- version 2.7
- update langpacks
- update nongnome-proxies.patch, ntlm-full-path.patch, prefer_plugin_pref.patch,
  shared-nss-db.patch and ua-locale.patch from opensuse
- set relname Passion

* Fri Dec 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-1m)
- version 2.6.1
- update langpacks
- set relname Angel

* Wed Dec 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6-1m)
- version 2.6
- update langpacks
- update mozilla-shared-nss-db.patch from opensuse
- set relname SugarDaddyBaby

* Thu Nov 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5-1m)
- version 2.5
- update langpacks
- update mozilla-shared-nss-db.patch
- remove merged mozilla-cairo-return.patch
- set relname Cappuccino

* Sat Oct  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.1-1m)
- version 2.4.1
- update langpacks
- set relname Horizon

* Wed Sep 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4-1m)
- version 2.4
- update langpacks
- update shared-nss-db.patches, cairo-return.patch and language.patch imported from opensuse
- update Source3: %%{name}-icon.png
- remove cairo-lcd.patch
- set relname Reflections

* Wed Sep  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-1m)
- version 2.3.3
- update langpacks
- set relname OneMoreChance
- can not compile seamonkey/thunderbird by gcc-4.6.2-0.20110902.1m on i686
- revert the gcc please

* Wed Aug 31 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-1m)
- version 2.3.2
- update langpacks
- correct broken version string
- set relname OneMoreTime

* Wed Aug 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- version 2.3.1
- update langpacks
- set relname QuietStars

* Wed Aug 17 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3-1m)
- version 2.3
- update langpacks
- update prefer_plugin_pref.patch and shared-nss-db patches from opensuse
- remove gio.patch
- clean up spec for stable release
- set relname SoundFurniture

* Wed Jul 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-2m)
- own all directories again
- really fix up language support

* Mon Jul 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-1m)
- version 2.2
- update langpacks
- update language.patch from opensuse
- remove merged gcc46.patch
- set relname RetroMemory

* Mon Jun 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-10m)
- set --disable-gnomevfs again
- seamonkey-2.1 is working now
- it's time to go trunk for fine tuning
- everything is gonna be okay, go for it!

* Mon Jun 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-9m)
- optimize -O2 again

* Mon Jun 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-8m)
- reset mozconfig

* Mon Jun 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-7m)
- modify MOZ_OPT_FLAGS again

* Mon Jun 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-6m)
- modify MOZ_OPT_FLAGS to work
- apply all suse's patches to test

* Fri Jun 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-5m)
- modify mozconfig

* Sat Jun 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-4m)
- fix up mozconfig again

* Sat Jun 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-3m)
- fix up mozconfig

* Sat Jun 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-2m)
- fix up language support

* Sat Jun 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-1m)
- version 2.1
- update langpacks
- update mozconfig
- update seamonkey.sh.in
- remove path_len.patch and jemalloc.patch once
- remove merged cstddef.patch
- set relname StartingOver

* Fri Apr 29 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.14-1m)
- version 2.0.14
- [SECURITY] following issues were fixed
- MFSA 2011-18 XSLT generate-id() function heap address leak
- MFSA 2011-16 Directory traversal in resource: protocol
- MFSA 2011-15 Escalation of privilege through Java Embedding Plugin
- MFSA 2011-14 Information stealing via form history
- MFSA 2011-13 Multiple dangling pointer vulnerabilities
- MFSA 2011-12 Miscellaneous memory safety hazards (rv:2.0.1/ 1.9.2.17/ 1.9.1.19)
- update langpacks
- set relname Imagine

* Tue Apr 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.13-3m)
- modify MOZ_OPT_FLAGS to enable build by gcc46
- import cstddef.patch from Fedora
- disable system-cairo

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.13-2m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.13-1m)
- version 2.0.13
- [SECURITY] following issue was fixed
- MFSA 2011-11 Update to HTTPS certificate blacklist
- update langpacks
- set relname PowerToThePeople

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.12-1m)
- [SECURITY] following security issues were fixed in this version
- MFSA 2011-10 CSRF risk with plugins and 307 redirects
- MFSA 2011-08 ParanoidFragmentSink allows javascript: URLs in chrome documents
- MFSA 2011-07 Memory corruption during text run construction (Windows)
- MFSA 2011-06 Use-after-free error using Web Workers
- MFSA 2011-05 Buffer overflow in JavaScript atom map
- MFSA 2011-04 Buffer overflow in JavaScript upvarMap
- MFSA 2011-03 Use-after-free error in JSON.stringify
- MFSA 2011-02 Recursive eval call causes confirm dialogs to evaluate to true
- MFSA 2011-01 Miscellaneous memory safety hazards (rv:1.9.2.14/ 1.9.1.17)
- version 2.0.12 and update langpacks
- set relname PenguinWalk

* Fri Dec 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.11-1m)
- version 2.0.11
- [SECURITY] following issues were fixed
  MFSA 2010-84 XSS hazard in multiple character encodings
  MFSA 2010-83 Location bar SSL spoofing using network error page
  MFSA 2010-82 Incomplete fix for CVE-2010-0179
  MFSA 2010-81 Integer overflow vulnerability in NewIdArray
  MFSA 2010-80 Use-after-free error with nsDOMAttribute MutationObserver
  MFSA 2010-79 Java security bypass from LiveConnect loaded via data: URL meta refresh
  MFSA 2010-78 Add support for OTS font sanitizer
  MFSA 2010-77 Crash and remote code execution using HTML tags inside a XUL tree
  MFSA 2010-76 Chrome privilege escalation with window.open and <isindex> element
  MFSA 2010-75 Buffer overflow while line breaking after document.write with long string
  MFSA 2010-74 Miscellaneous memory safety hazards (rv:1.9.2.13/ 1.9.1.16)
- update langpacks
- set relname DolphinDance

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.10-2m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.10-1m)
- version 2.0.10
- [SECURITY] following issue was fixed
  MFSA 2010-73 Heap buffer overflow mixing document.write and DOM insertion
- update langpacks
- set relname DetourAhead

* Thu Oct 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.9-1m)
- version 2.0.9
- [SECURITY] following issues were fixed
  MFSA 2010-72 Insecure Diffie-Hellman key exchange
  MFSA 2010-71 Unsafe library loading vulnerabilities
  MFSA 2010-70 SSL wildcard certificate matching IP addresses
  MFSA 2010-69 Cross-site information disclosure via modal calls
  MFSA 2010-68 XSS in gopher parser when parsing hrefs
  MFSA 2010-67 Dangling pointer vulnerability in LookupGetterOrSetter
  MFSA 2010-66 Use-after-free error in nsBarProp
  MFSA 2010-65 Buffer overflow and memory corruption using document.write
  MFSA 2010-64 Miscellaneous memory safety hazards (rv:1.9.2.11/ 1.9.1.14)
- update langpacks
- set relname AutumnLeaves

* Thu Sep 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-1m)
- version 2.0.8
- update langpacks
- set relname WaltzForDebby

* Wed Sep  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-1m)
- version 2.0.7
- [SECURITY] following issues were fixed
  MFSA 2010-63 Information leak via XMLHttpRequest statusText
  MFSA 2010-62 Copy-and-paste or drag-and-drop into designMode document allows XSS
  MFSA 2010-61 UTF-7 XSS by overriding document charset using <object> type attribute
  MFSA 2010-60 XSS using SJOW scripted function
  MFSA 2010-58 Crash on Mac using fuzzed font in data: URL
  MFSA 2010-57 Crash and remote code execution in normalizeDocument
  MFSA 2010-56 Dangling pointer vulnerability in nsTreeContentView
  MFSA 2010-55 XUL tree removal crash and remote code execution
  MFSA 2010-54 Dangling pointer vulnerability in nsTreeSelection
  MFSA 2010-53 Heap buffer overflow in nsTextFrameUtils::TransformText
  MFSA 2010-52 Windows XP DLL loading vulnerability
  MFSA 2010-51 Dangling pointer vulnerability using DOM plugin array
  MFSA 2010-50 Frameset integer overflow vulnerability
  MFSA 2010-49 Miscellaneous memory safety hazards (rv:1.9.2.9/ 1.9.1.12)
- update langpacks
- set relname MyFoolishHeart

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.6-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.6-1m)
- version 2.0.6
- [SECURITY] following issues were fixed
  MFSA 2010-47 Cross-origin data leakage from script filename in error messages
  MFSA 2010-46 Cross-domain data theft using CSS
  MFSA 2010-45 Multiple location bar spoofing vulnerabilities
  MFSA 2010-42 Cross-origin data disclosure via Web Workers and importScripts
  MFSA 2010-41 Remote code execution using malformed PNG image
  MFSA 2010-40 nsTreeSelection dangling pointer remote code execution vulnerability
  MFSA 2010-39 nsCSSValue::Array index integer overflow
  MFSA 2010-37 Plugin parameter EnsureCachedAttrParamArrays remote code execution vulnerability
  MFSA 2010-36 Use-after-free error in NodeIterator
  MFSA 2010-35 DOM attribute cloning remote code execution vulnerability
  MFSA 2010-34 Miscellaneous memory safety hazards (rv:1.9.2.7/ 1.9.1.11)
- update langpacks
- set relname AllAroundMe

* Wed Jun 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.5-1m)
- version 2.0.5
- [SECURITY] following issues were fixed
  MFSA 2010-33 User tracking across sites using Math.random()
  MFSA 2010-32 Content-Disposition: attachment ignored if Content-Type: multipart also present
  MFSA 2010-31 focus() behavior can be used to inject or steal keystrokes
  MFSA 2010-30 Integer Overflow in XSLT Node Sorting
  MFSA 2010-29 Heap buffer overflow in nsGenericDOMDataNode::SetTextInternal
  MFSA 2010-28 Freed object reuse across plugin instances
  MFSA 2010-27 Use-after-free error in nsCycleCollector::MarkRoots()
  MFSA 2010-26 Crashes with evidence of memory corruption (rv:1.9.2.4/ 1.9.1.10)
  MFSA 2010-25 Re-use of freed object due to scope confusion
- update langpacks
- set relname Voices

* Wed May  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-3m)
- own all directories

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against libjpeg-8a

* Wed Mar 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-1m)
- version 2.0.4
- [SECURITY] following issues were fixed
  MFSA 2010-24 XMLDocument::load() doesn't check nsIContentPolicy
  MFSA 2010-23 Image src redirect to mailto: URL opens email editor
  MFSA 2010-22 Update NSS to support TLS renegotiation indication
  MFSA 2010-20 Chrome privilege escalation via forced URL drag and drop
  MFSA 2010-19 Dangling pointer vulnerability in nsPluginArray
  MFSA 2010-18 Dangling pointer vulnerability in nsTreeContentView
  MFSA 2010-17 Remote code execution with use-after-free in nsTreeSelection
  MFSA 2010-16 Crashes with evidence of memory corruption (rv:1.9.2.2/ 1.9.1.9/ 1.9.0.19)
- update langpacks
- set relname MidnightShuffle

* Fri Feb 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-1m)
- version 2.0.3
- [SECURITY] following issues were fixed
  MFSA 2010-05 XSS hazard using SVG document and binary Content-Type
  MFSA 2010-04 XSS due to window.dialogArguments being readable cross-domain
  MFSA 2010-03 Use-after-free crash in HTML parser
  MFSA 2010-02 Web Worker Array Handling Heap Corruption Vulnerability
  MFSA 2010-01 Crashes with evidence of memory corruption (rv:1.9.1.8/ 1.9.0.18)
- update langpacks
- Obsoletes: seamonkey-language-pack-japanese and seamonkey-suite
- Japanese translations were merged into upstream and this package is including Japanese language pack
- set relname Aquaria

* Thu Jan 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2
- update langpacks
- set relname NoGenerationGap

* Wed Dec 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-1m)
- version 2.0.1
- [SECURITY] following issues were fixed
  MFSA 2009-71 GeckoActiveXObject exception messages can be used to enumerate installed COM objects
  MFSA 2009-70 Privilege escalation via chrome window.opener
  MFSA 2009-69 Location bar spoofing vulnerabilities
  MFSA 2009-68 NTLM reflection vulnerability
  MFSA 2009-67 Integer overflow, crash in libtheora video library
  MFSA 2009-66 Memory safety fixes in liboggplay media library
  MFSA 2009-65 Crashes with evidence of memory corruption (rv:1.9.1.6/ 1.9.0.16)
- update langpacks
- set relname Stereocaster

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-4m)
- change "font.minimum-size.ja" from 13 to 12

* Sat Oct 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-3m)
- own few directories

* Sat Oct 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-2m)
- remove mozilla-rebuild-databases.pl
- Obsoletes: seamonkey-extensions-* for the moment

* Sat Oct 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- version 2.0
- update seamonkey.sh.in
- update seamonkey.desktop, seamonkey-mail.desktop and seamonkey-compose.desktop
- update seamonkey-momonga-default-prefs.js
- import mozilla-psm-exclude-list from Fedora
- import seamonkey-mozconfig from Fedora and unset BUILD_OFFICIAL, MOZILLA_OFFICIAL
- import mozilla-jemalloc.patch from Fedora
- import langpacks from Fedora
- re-write %%build section
- modify %%install section
- remove default-plugin-less-annoying.patch
- remove uifont.patch
- remove nspr-packages.patch
- remove static-sqlite.patch
- remove gtkXft-FontFamilyUtf8.patch
- remove xprint.patch
- update lang.patch
- update path_len.patch
- remove package devel
- set relname Jump

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.18-8m)
- fix regxpcom buffer overflow
-- https://bugzilla.mozilla.org/show_bug.cgi?id=412610
-- https://bugzilla.novell.com/show_bug.cgi?id=354150

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.18-7m)
- rebuild against libjpeg-7

* Tue Sep  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.18-6m)
- do not use absolute path for %%files -f with rpm471

* Mon Sep  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18-5m)
- fix "File listed twice:"

* Mon Sep  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18-4m)
- merge all sub-packages with the "seamonkey"

* Mon Sep  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18-3m)
- set AutoProv: 0 in all sub-packages

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.18-2m)
- do not provide internal libraries
-- add Source1000: find-external-requires from thunderbird
-- AutoProv: 0
-- _use_internal_dependency_generator 0
- License: MPLv1.1 or GPLv2+ or LGPLv2+
-- my mistake

* Sun Sep  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18-1m)
- version 1.1.18
- [SECURITY] following issues were fixed
  MFSA 2009-43 Heap overflow in certificate regexp parsing
  MFSA 2009-42 Compromise of SSL-protected communication
- update nspr-packages.patch
- remove merged nspr.m4.patch
- apply static-sqlite.patch to enable build
- set relname DayDreamBeliever

* Tue Jun 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.17-1m)
- version 1.1.17
- [SECURITY] following issues were fixed
  MFSA 2009-33 Crash viewing multipart/alternative message with text/enhanced part
  MFSA 2009-32 JavaScript chrome privilege escalation
  MFSA 2009-29 Arbitrary code execution using event listeners attached to an element whose owner document is null
  MFSA 2009-27 SSL tampering via non-200 responses to proxy CONNECT requests
  MFSA 2009-26 Arbitrary domain cookie access by local file: resources
  MFSA 2009-24 Crashes with evidence of memory corruption (rv:1.9.0.11)
  MFSA 2009-21 POST data sent to wrong site when saving web page with embedded frame
  MFSA 2009-17 Same-origin violations when Adobe Flash loaded via view-source: scheme
- remove merged mfsa-2009-13.patch

* Sat Jun 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16-2m)
- set relname MoonLikeAGinLime for Momonga Linux 6

* Fri Apr 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16-1m)
- version 1.1.16
- [SECURITY] following issue was fixed
  MFSA 2009-12 XSL Transformation vulnerability
- set relname MintShowerGel

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-2m)
- [SECURITY] MFSA 2009-12, MFSA 2009-13
- import security patches from Rawhide (1.15.1-3)

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-1m)
- version 1.1.15
- [SECURITY] following issues were fixed
  MFSA 2009-10 Upgrade PNG library to fix memory safety hazards
  MFSA 2009-09 XML data theft via RDFXMLDataSource and cross-domain redirect
  MFSA 2009-07 Crashes with evidence of memory corruption (rv:1.9.0.7)
  MFSA 2009-05 XMLHttpRequest allows reading HTTPOnly cookies
  MFSA 2009-01 Crashes with evidence of memory corruption (rv:1.9.0.6)
- set relname Replica

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.14-4m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.14-3m)
- update Patch14 for fuzz=0
- License: MPLv1.1 and GPLv2+ and LGPLv2+

* Sat Jan  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.14-2m)
- modify options of configure
 - add
  --disable-strip
  --with-pthreads
 - remove
  --disable-short-wchar
  --enable-nspr-autoconf
  --enable-strip
  --enable-strip-libs
  --enable-svg-renderer-libart
  --enable-xft
  --enable-xul
  --disable-freetype2
  --with-libart-prefix
  --with-system-mng
- sort options of configure

* Wed Dec 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.14-1m)
- version 1.1.14
- [SECURITY] following issues were fixed
  MFSA 2008-68 XSS and JavaScript privilege escalation
  MFSA 2008-67 Escaped null characters ignored by CSS parser
  MFSA 2008-66 Errors parsing URLs with leading whitespace and control characters
  MFSA 2008-65 Cross-domain data theft via script redirect error message
  MFSA 2008-64 XMLHttpRequest 302 response disclosure
  MFSA 2008-61 Information stealing via loadBindingDocument
  MFSA 2008-60 Crashes with evidence of memory corruption (rv:1.9.0.5/1.8.1.19)
- set relname SplashIntoTheBlue

* Thu Nov 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.13-1m)
- version 1.1.13
- [SECURITY] following issues were fixed
  MFSA 2008-58 Parsing error in E4X default namespace
  MFSA 2008-57 -moz-binding property bypasses security checks on codebase principals
  MFSA 2008-56 nsXMLHttpRequest::NotifyEventListeners() same-origin violation
  MFSA 2008-55 Crash and remote code execution in nsFrameManager
  MFSA 2008-54 Buffer overflow in http-index-format parser
  MFSA 2008-53 XSS and JavaScript privilege escalation via session restore
  MFSA 2008-52 Crashes with evidence of memory corruption (rv:1.9.0.4/1.8.1.18)
  MFSA 2008-50 Crash and remote code execution via __proto__ tampering
  MFSA 2008-49 Arbitrary code execution via Flash Player dynamic module unloading
  MFSA 2008-48 Image stealing via canvas and HTTP redirect
  MFSA 2008-47 Information stealing via local shortcut files
- set relname PreciousRainyDay

* Wed Sep 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.12-1m)
- version 1.1.12
- [SECURITY] following issues were fixed
  MFSA 2008-45 XBM image uninitialized memory reading
  MFSA 2008-44 resource: traversal vulnerabilities
  MFSA 2008-43 BOM characters stripped from JavaScript before execution
  MFSA 2008-42 Crashes with evidence of memory corruption (rv:1.9.0.2/1.8.1.17)
  MFSA 2008-41 Privilege escalation via XPCnativeWrapper pollution
  MFSA 2008-40 Forced mouse drag
  MFSA 2008-38 nsXMLDocument::OnChannelRedirect() same-origin violation
  MFSA 2008-37 UTF-8 URL stack buffer overflow
- set relname MagicCarpetRide

* Thu Sep 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-2m)
- [CRASH FIX]
- use -O1 instead of -O2 in optflags on i686 to avoid crashing

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-1m)
- version 1.1.11
- [SECURITY] following issue was fixed
  MFSA 2008-34 Remote code execution by overflowing CSS reference counter
- set relname NonstopToTokyo

* Wed Jul  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10-1m)
- version 1.1.10
- [SECURITY] following issues were fixed
  MFSA 2008-33 Crash and remote code execution in block reflow
  MFSA 2008-32 Remote site run as local file via Windows URL shortcut
  MFSA 2008-31 Peer-trusted certs can use alt names to spoof
  MFSA 2008-30 File location URL in directory listings not escaped properly
  MFSA 2008-29 Faulty .properties file results in uninitialized memory being used
  MFSA 2008-28 Arbitrary socket connections with Java LiveConnect on Mac OS X
  MFSA 2008-27 Arbitrary file upload via originalTarget and DOM Range
  MFSA 2008-25 Arbitrary code execution in mozIJSSubScriptLoader.loadSubScript()
  MFSA 2008-24 Chrome script loading from fastload file
  MFSA 2008-23 Signed JAR tampering
  MFSA 2008-22 XSS through JavaScript same-origin violation
  MFSA 2008-21 Crashes with evidence of memory corruption (rv:1.8.1.15)
  MFSA 2008-20 Crash in JavaScript garbage collector
- remove merged upstream patch
- set relname MessageSong

* Thu Jun 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-4m)
- add mozilla.png as link of seamonkey-icon.png, klipper needs it
- re-import Source2 from Fedora
- clean up spec file

* Wed Apr 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-3m)
- import seamonkey-1.1.9plus.patch from Fedora
 +* Thu Apr 17 2008 Kai Engert <kengert@redhat.com> - 1.1.9-3
 +- add several upstream patches, not yet released:
 +  425576 (crash), 323508, 378132, 390295, 421622

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.9-2m)
- rebuild against gcc43

* Wed Mar 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-1m)
- version 1.1.9
- [SECURITY] following issues were fixed
  MFSA 2008-19 XUL popup spoofing variant (cross-tab popups)
  MFSA 2008-18 Java socket connection to any local port via LiveConnect
  MFSA 2008-17 Privacy issue with SSL Client Authentication
  MFSA 2008-16 HTTP Referrer spoofing with malformed URLs
  MFSA 2008-15 Crashes with evidence of memory corruption (rv:1.8.1.13)
  MFSA 2008-14 JavaScript privilege escalation and arbitrary code execution
- remove merged gcc43.patch
- set relname Superstar

* Wed Mar 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.8-3m)
- remove Provides and Obsoletes: mozilla*

* Fri Feb 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-2m)
- add gcc43 patch <https://bugzilla.mozilla.org/show_bug.cgi?id=416463>

* Sun Feb 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.8-1m)
- version 1.1.8
- [SECURITY] following issues were fixed
  MFSA 2008-10 URL token stealing via stylesheet redirect
  MFSA 2008-09 Mishandling of locally-saved plain text files
  MFSA 2008-06 Web browsing history and forward navigation stealing
  MFSA 2008-05 Directory traversal via chrome: URI
  MFSA 2008-03 Privilege escalation, XSS, Remote Code Execution
  MFSA 2008-02 Multiple file input focus stealing vulnerabilities
  MFSA 2008-01 Crashes with evidence of memory corruption (rv:1.8.1.12)
- set relname PortableRock

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.7-2m)
- rebuild against perl-5.10.0-1m

* Sat Dec  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-1m)
- version 1.1.7
- [SECURITY] following issues were fixed
  MFSA 2007-39 Referer-spoofing via window.location race condition
  MFSA 2007-38 Memory corruption vulnerabilities (rv:1.8.1.10)
  MFSA 2007-37 jar: URI scheme XSS hazard
- set relname SunnySideOfTheStreet

* Tue Nov  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-1m)
- version 1.1.6
- bugfix release
- set relname SweetSoulRevue

* Sat Oct 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-1m)
- version 1.1.5
- [SECURITY] following issues were fixed
  MFSA 2007-36 URIs with invalid %%-encoding mishandled by Windows
  MFSA 2007-35 XPCNativeWrapper pollution using Script object
  MFSA 2007-34 Possible file stealing through sftp protocol
  MFSA 2007-33 XUL pages can hide the window titlebar
  MFSA 2007-32 File input focus stealing vulnerability
  MFSA 2007-31 Browser digest authentication request splitting
  MFSA 2007-30 onUnload Tailgating
  MFSA 2007-29 Crashes with evidence of memory corruption (rv:1.8.1.8)
  MFSA 2007-28 Code execution via QuickTime Media-link files
- set relname 7pmTokyo

* Sat Sep 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-2m)
- use system cairo (build fix)

* Sat Aug  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-1m)
- version 1.1.4
- [SECURITY] following issues were fixed
  MFSA 2007-27 Unescaped URIs passed to external programs
  MFSA 2007-26 Privilege escalation through chrome-loaded about:blank windows
  MFSA 2007-23 Remote code execution by launching SeaMonkey from Internet Explorer

* Fri Jul 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- version 1.1.3
- [SECURITY] following issues were fixed
  MFSA 2007-25 XPCNativeWrapper pollution
  MFSA 2007-24 Unauthorized access to wyciwyg:// documents
  MFSA 2007-22 File type confusion due to %%00 in name
  MFSA 2007-21 Privilege escalation using an event handler attached to an element not in the document
  MFSA 2007-20 Frame spoofing while window is loading
  MFSA 2007-19 XSS using addEventListener and setTimeout
  MFSA 2007-18 Crashes with evidence of memory corruption (rv:1.8.1.5)

* Thu May 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-1m)
- version 1.1.2
- [SECURITY] following issues were fixed
  MFSA 2007-17 XUL Popup Spoofing
  MFSA 2007-16 XSS using addEventListener
  MFSA 2007-15 Security Vulnerability in APOP Authentication
  MFSA 2007-14 Path Abuse in Cookies
  MFSA 2007-12 Crashes with evidence of memory corruption (rv:1.8.0.12/1.8.1.4)

* Sun May  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-2m)
- set relname HappySad for STABLE release

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1
- remove merged thunderbird-1.5.0.9-bug367203.patch

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-5m)
- fix link mozilla-remote-tab -> seamonkey-remote-tab

* Thu Jan 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-4m)
- set "general.useragent.vendor", "Momonga"
- set "general.useragent.vendorSub", %%{version}-%%{release}

* Tue Jan 23 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1-3m)
- add thunderbird-1.5.0.9-bug367203.patch for drag and drop broken at >gtk+-2.10.8
- see https://bugzilla.mozilla.org/show_bug.cgi?id=367203

* Mon Jan 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-2m)
- import mozilla-1.7.5-lang.patch.bz2 from cooker
  enable automatic language detection at startup (Debian)
- replace seamonkey-compose-icon.png

* Fri Jan 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- version 1.1
- remove merged with-system-nss.patch
- remove ppc64.patch
- remove merged x86_64-relocation.patch

* Sun Jan  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-10m)
- use BUILD_OFFICIAL=0 MOZILLA_OFFICIAL=0

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.7-9m)
- enable ppc64

* Fri Jan  5 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.7-8m)
- revised seamonkey.sh.in

* Mon Jan  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-7m)
- remove Obsoletes: mozilla-extensions
- seamonkey-extensions are now available

* Sat Dec 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-6m)
- use %%{optflags}
- add --enable-xinerama to configure

* Fri Dec 29 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.7-5m)
- add R_X86_64_PC32 relocations problem fix patch
- see https://bugzilla.mozilla.org/attachment.cgi?id=248494

* Thu Dec 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-4m)
- remove %%{_libdir}/mozilla, it's provided by firefox-2.0

* Thu Dec 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-3m)
- revise Provides and Obsoletes

* Wed Dec 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-2m)
- update rebuild-databases.pl.in

* Wed Dec 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-1m)
- SeaMonkey 1.0.7, good-bye mozilla
- modify and remove many patches

* Wed Jun 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.13-3m)
- import mozilla-xft.js from Centro (use sans-serif like firefox)
 +* Fri Aug 8 2003 Masaru Yokoi <masaru@turbolinux.co.jp>
 +- "font.default" was changed to sans-serif.
- import modern icons from VineSeed
 +* Mon Mar 28 2005 Daisuke SUZUKI <daisuke@linux.or.jp> 38:1.7.6-0vl1
 +- add modern icons

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.13-2m)
- add thunderbird-mime_extern.patch.(need newer gcc-4.1)

* Sat Apr 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.13-1m)
- update to 1.7.13
- [SECURITY] MFSA-2006-25 MFSA-2006-24 MFSA-2006-23 MFSA-2006-22 MFSA-2006-19 
-            MFSA-2006-18 MFSA-2006-17 MFSA-2006-16 MFSA-2006-15 MFSA-2006-14 
-            MFSA-2006-13 MFSA-2006-12 MFSA-2006-11 MFSA-2006-10 MFSA-2006-09 
- http://www.mozilla.org/projects/security/known-vulnerabilities.html#Mozilla

* Sat Apr 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.12.6m)
- remove unnecessary headers from mozilla-devel package
-- jpeg libart_lgpl png xlibrgb xprintutil

* Tue Feb 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.12-5m)
- import dumpstack.patch from opensuse
 +* Wed Jan 18 2006 - stark@suse.de
 +- fixed DumpStackToFile() for glibc 2.4

* Tue Feb 21 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7.12-4m)
- build fix for xprint support
- enable alpha build

* Mon Feb 20 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7.12-3m)
- enable system nspr and nss.

* Wed Jan 11 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.12-2m)
- add Patch113: mozilla-1.7.12-gcc4.patch
- remove configure option --enable-meta-components

* Fri Sep 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.12-1m)
- update to 1.7.12
- [Security] http://www.mozilla.org/security/announce/mfsa2005-58.html etc

* Sat Sep 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.11-3m)
- [Security] https://addons.mozilla.org/messages/307259.html

* Sun Aug  7 2005 TASHIRO Hideo <tashiron@momonga-linux.org>
- (1.7.11-2m)
- enable x86_64. (modify mozilla.sh.in)

* Sun Jul 31 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.11-1m)
- update to 1.7.11

* Sun Jul 24 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.10-1m)
- security fixes
-  http://www.mozilla.org/security/announce/mfsa2005-56.html
-  etc

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.8-2m)
- suppress AC_DEFUN warning.

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.8-1m)
- security fixes
-   http://www.mozilla.org/security/announce/mfsa2005-42.html

* Mon Apr 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.7-2m)
- add mozilla-libart_lgpl.tar.bz2 (from mozilla-1.7.6)

* Sat Apr 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.7-1m)
- security fixes
-   http://www.mozilla.org/security/announce/mfsa2005-37.html
-   http://www.mozilla.org/security/announce/mfsa2005-41.html
-   etc.

* Thu Mar 28 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.7.6-2m)
- enable build on ppc64 kernel

* Thu Mar 24 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.6-1m)
- security fix
-   http://www.mozilla.org/security/announce/mfsa2005-30.html
-   http://www.mozilla.org/security/announce/mfsa2005-32.html

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.5-2m)
- move desktop files to %%{_datadir}/applications/
- import new desktop files from VineSeed
- import png icons from VineSeed

* Sun Jan 16 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- source URL update. kossori.

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7.5-1m)
- update to 1.7.5

* Sun Nov 14 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7.3-4m)
- add patch111
  [bug:181697]
  https://bugzilla.mozilla.org/show_bug.cgi?id=181697

* Wed Sep 29 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.7.3-3m)
- rebuild against libstdc++-3.4.1

* Wed Sep 29 2004 kourin <kourin@fh.freeserve.ne.jp>
- (1.7.3-2m)
- bugfix. apply mozilla-printing.patch
  see http://lists.sourceforge.jp/mailman/archives/kazehakase-devel/2004-September/001579.html

* Thu Sep 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7.3-1m)
- security update.
  see. http://jt.mozilla.gr.jp/projects/security/known-vulnerabilities.html#mozilla1.7.3

* Mon Aug  9 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7.2-2m)
- change source URL.

* Sat Aug  7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7.2-1m)
- security update
  bug 251381, 249004, 250906, 253121
  see. http://jt.mozilla.gr.jp/projects/security/known-vulnerabilities.html#mozilla1.7.2
       http://bugzilla.mozilla.org/buglist.cgi?bug_id=251381,249004,250906,253121

* Mon Aug  2 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7-4m)
- security update
  [bug249004]
    http://bugzilla.mozilla.org/show_bug.cgi?id=249004
  [bug253121]
    http://bugzilla.mozilla.org/show_bug.cgi?id=253121

* Wed Jul 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7-3m)
- revise %%files

* Tue Jul  6 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7-2m)
- remove Epoch tag.

* Tue Jul  6 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6-6m)
- Epoch, the company that is known as "the ballpark" game vendor.

* Wed Mar 24 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6-5m)
- revised spec for rpm 4.2
- set _unpackaged_files_terminate_build as 0, avoid remove each messy stuff.

* Tue Mar  2 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6-4m)
- a little modify mozilla-1.6-ruby-support.patch

* Mon Mar  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.6-3m)
- fix typo in mozilla-remote-tab

* Mon Mar  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.6-2m)
- add mozilla-remote-tab

* Sat Jan 17 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6-1m)
- update to 1.6
- remove some unuse patch.

* Wed Dec 17 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5-11m)
- add patch12
  [bug-org#201209]
    http://bugzilla.mozilla.org/show_bug.cgi?id=201209
  [bug-jp#3551]
    http://bugzilla.mozilla.gr.jp/show_bug.cgi?id=3551

* Sat Dec 13 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5-10m)
- add patch11
  [bug-org#228176]
    http://bugzilla.mozilla.org/show_bug.cgi?id=228176

* Sun Nov 30 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5-9m)
- add patch8,9,10
  [bug-jp#3500]
    http://bugzilla.mozilla.gr.jp/show_bug.cgi?id=3500
  [bug-org#226637]
    http://bugzilla.mozilla.org/show_bug.cgi?id=226637
  [bug-jp#3498]
    http://bugzilla.mozilla.gr.jp/show_bug.cgi?id=3498

* Sat Nov 15 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5-8m)
- modify mozilla-rebuild-databases.pl for mozilla-extensions packages.

* Fri Nov 14 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5-7m)
- add patch7
  [bug-org#102578] [bug-jp#2997]
  see. http://bugzilla.mozilla.org/show_bug.cgi?id=102578
       http://bugzilla.mozilla.gr.jp/show_bug.cgi?id=2997

* Thu Oct 30 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.5-6m)
- revise %%files
   remove directories from mozilla-mail those are already included in main package
     %%dir %%{_libdir}/mozilla-%%{version}/chrome/icons/default
     %%dir %%{_libdir}/mozilla-%%{version}/chrome/icons
     %%dir %%{_libdir}/mozilla-%%{version}/chrome
     %%dir %%{_libdir}/mozilla-%%{version}/components
     %%dir %%{_libdir}/mozilla-%%{version}
   add directories under %%{_includedir}/mozilla-%%{version} to mozilla-devel

* Wed Oct 29 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.5-5m)
- append some patches from bugzilla-jp, bugzilla.
  [bug-jp#3211]
    see. http://bugzilla.mozilla.gr.jp/show_bug.cgi?id=3211
  [bug#208095]
     http://bugzilla.mozilla.org/show_bug.cgi?id=208095
  [bug#99094]
    http://bugzilla.mozilla.org/show_bug.cgi?id=99094
  [bug#223394]
    http://bugzilla.mozilla.org/show_bug.cgi?id=223394

* Mon Oct 20 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.5-4m)
- appended some patches.
  [bug#187899]
    see. http://bugzilla.mozilla.org/show_bug.cgi?id=187899
  [bug#85908]
    see. http://bugzilla.mozilla.org/show_bug.cgi?id=85908

* Sun Oct 19 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.5-3m)
- modify configure's options.

* Sun Oct 19 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.5-2m)
- append configure's option "--enable-strip". (Why removed?)

* Thu Oct 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.5-1m)
- update to 1.5

* Tue Aug 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.4-2m)
- add mozilla-remote which first execute mozilla with -remote, then without -remote if failed

* Wed Jul  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.4-1m)
- update to 1.4
- remade patches. (103,104)
- remove patch100. (we don't need it any more.)
- build with gcc3.2

* Fri May 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.1-1m)
- update to 1.3.1
- use %%{?_smp_mflags}

* Wed Apr 16 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.3-5m)
- update mozilla-gtk2im.patch
- add BuildPreReq libIDL-devel

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-4m)
- add BuildPreReq: libstdc++2.96-devel
- add Requires: zlib >= 1.1.4-5m

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-3m)
- rebuild against zlib 1.1.4-5m

* Fri Mar 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3-1m)
- version 1.3

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3b-9m)
- add URL
- use rpm macros

* Sun Mar  9 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3b-8m)
- enable SVG support again.
- enable MNG support again.
- modify default font setting to unix.js

* Fri Mar  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.3b-7m)
- update jlp

* Fri Mar  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3b-6m)
- fix -devel filelist.

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3b-5m)
- merge sub packages
- add mozilla-1.3b-gtkXft-FontFamilyUtf8.patch

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3b-4m)
- add some patches

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3b-3m)
- fix jlp

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3b-2m)
- add jlp

* Wed Mar  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3b-1m)
- initial for 1.3b with gtk2

* Wed Mar  5 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-11m)
- a little modify unix.js

* Tue Feb 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-10m)
- add BuildPreReq: unzip >= 5.50 for exec unzip in spec file

* Mon Feb 10 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-9m)
- stop %postun script. (suman ittekuru...)

* Mon Feb 10 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-8m)
- clean up script in "%%postun" (not %%preun)
- modify patch1 (for Xft2)

* Wed Feb  5 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.1-7m)
- use gcc2.96 for C++ ABI compatibility with plugins (except ppc)
  see [Momonga-devel.ja:01338]

* Fri Jan 24 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-6m)
- clean up in %%preun.

* Sun Jan 12 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-5m)
- add patch for Xft UTF-8 font family name.
  see http://www.kde.gr.jp/~akito/patch/fcpackage/Install-ja.txt
- 'pref("nglayout.initialpaint.delay", 0)' is default. (on all.js)

* Wed Dec 25 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-4m)
- add some patches.
  [bug128572] http://bugzilla.mozilla.org/show_bug.cgi?id=128572
  [bug181644] http://bugzilla.mozilla.org/show_bug.cgi?id=181644

* Wed Dec 11 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-3m)
- use system image libs.

* Sat Dec  7 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-2m)
- jlp update. (to kv0.4)
- import Xft source from fcpackages source tree.
- import freetype source from freetype2 source tree.

* Thu Dec  5 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-1m)
- update to version 1.2.1 (with Xft2)

* Thu Nov 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.1-11m)
- use gcc2.96 on ppc

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1-10m)
- use gcc-3.2

* Thu Nov  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1-9m)
- %%include_specopt requires %%name...

* Tue Nov  5 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-8m)
- change initial setting.
    freetype2 support is default. (on unix.js)
- delete some unused patches.

* Fri Nov  1 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-7m)
- Change default configure option (Xft not support)
  I tested other some option.
  However, all of them were dangerous. (T_T)

* Wed Oct 30 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-6m)
- modified configure option.
  added --enable-system-{jpeg,png,mng}

* Sun Oct 27 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-5m)
- modified configure option.
  added --enable-xft.
  default toolkit changed to gtk.
- JLP version up to 0.8.
- cleaned up old patches.

* Tue Oct  8 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-4m)
- fix petit wazilla patch. (sumanu... ;-p)

* Mon Oct  7 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-3m)
- add some patches from wazilla project.

* Wed Sep 18 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-2m)
- fix bug145579 (future_referer)
  http://bugzilla.mozilla.org/show_bug.cgi?id=145579

* Wed Sep 18 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1-1m)
- version up.

* Tue Aug 16 2002 Kenta MURATA <muraken2@nifty.com>
- (1.0-15m)
- implements openURL(url, new-tab).

* Tue Aug  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-14m)
- fix #bug 101827
  http://bugzilla.mozilla.org/show_bug.cgi?id=101827
- fix #bug 136087
  http://bugzilla.mozilla.org/show_bug.cgi?id=136087

* Tue Aug  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-13m)
- fix FTP View cross site scripting vulnerability.

* Fri Aug  2 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-12m)
- fix set number of CPUS statement.
- add some patches from wazilla.

* Tue Jul 30 2002 smbd <smbd@momonga-linux.org>
- (1.0-11m)
- [security]
- fix possible cookie stealing using javascript
- see http://bugzilla.mozilla.org/show_bug.cgi?id=152725

* Fri Jul  5 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-10m)
- sumaso... remake ruby support patch.

* Thu Jul  4 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-8k)
- XHTML 1.1 ruby module support
- fix too big font vulnerability.

* Sat Jun  8 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-6k)
  JLP 1.0-2

* Thu Jun  6 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-4k)
  JLP 1.0-1

* Thu Jun  6 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-2k)
  update to 1.0

* Sun Jun  2 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-0.3.8k)
- clean up patches.
- add patch #12 (Bug#143031)
  see -> http://bugzilla.mozilla.org/show_bug.cgi?id=143031
- remake patch#2

* Fri May 31 2002 Shigeyuki Yamashita <shige@kondara.org>
- (1.0-0.3.6k)
- rebuild against gcc 2.96
- remove patch #42 (mozilla-1.0.0rc3-gcc2.95.3.patch)

* Sat May 25 2002 Shigeyuki Yamashita <shige@kondara.org>
- add patch #42 (mozilla-1.0.0rc3-gcc2.95.3.patch)
- remove patches (#1,#6,#39)

* Fri May 17 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-0.2.6k)
- add patch39
  Bug#143607
  http://bugzilla.mozilla.org/show_bug.cgi?id=143607

* Mon May 13 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-0.2.4k)
- remake some patches (patch No. 15, 23, 24, 25)
- fix mozilla.sh (for emacs url open)

* Mon May 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-0.2.2k)
  update to 1.0rc2

* Fri May 10 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-0.1.12k)
- remove patch patch33

* Thu May  9 2002 Shigeyuki Yamashita <shige@kondara.org>
- (1.0-0.1.10k)
- add patch patch39
  Bug#105537 (see http://bugzilla.mozilla.org/show_bug.cgi?id=141061)

* Sat May  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-0.1.8k)
  jlp 1.0rc1 v1.0

* Sat Apr 27 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.0-0.1.6k)
- add xremote-client from official package
- update mozilla.sh from official package

* Fri Apr 26 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.0-0.1.4k)
- rebuild-databases.sh -> mozilla-rebuild-databases.pl
  as change in official src.rpm (1.0rc1-0)

* Fri Apr 26 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-0.1.2k)
  update to 1.0rc1
  jlp 1.0rc1 v0.1

* Thu Apr 25 2002 zunda <zunda@kondara.org>
- (0.9.8-44k)
- make export and make libs deleted to build wihtout mozilla-mail installed

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9.8-42k)
- rebuild against libpng 1.2.2.

* Wed Apr  3 2002 Kenta MURATA <muraken@kondara.org>
- (0.9.8-40k)
- BuildPreReq: ccache

* Sun Mar 31 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-38k)
- very verbose configure :-p

* Sat Mar 16 2002 Kenta MURATA <muraken@kondara.org>
- (0.9.8-36k)
- merge from MOZILLA_SVG_TEST branch.

* Sat Mar 16 2002 Kenta MURATA <muraken@kondara.org>
- (0.9.8-34.2k)
- use system libart_lgpl.

* Sat Mar 16 2002 Kenta MURATA <muraken@kondara.org>
- (0.9.8-34.1k)
- svg test branch.

* Tue Mar 12 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.8-34k)
- added configure param --with-system-zlib

* Sat Mar  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.8-32k)
- remove NO_XALF flag from all spec files

* Thu Mar  7 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-30k)
- fix install section

* Wed Mar  6 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.9.8-28k)
- enable to build

* Tue Mar  5 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-26k)
- miss packaging...

* Fri Mar  1 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-24k)
- add patch patch37
  modified chatzilla.properties of mozilla0.9.8-langjajp-v1.0.xpi

* Fri Mar  1 2002 Kenta MURATA <muraken@kondara.org>
- (0.9.8-22k)
- hack the MathML features. (add patch36)

* Wed Feb 27 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-20k)
- sumanu....

* Tue Feb 26 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-18k)
- add DOM Inspector package.
- modified startup script
- add patches
  mozilla-0.9.8-filepicker.patch
  mozilla-0.9.8-ja-JP-filepicker.patch (for lang ja-JP)

* Sun Feb 24 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-16k)
- fix font size of chatzilla (default is 14pt)

* Thu Feb 21 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-14k)
- add kondara icon png for search plugin.
- kondara.src become independent from a patch.

* Thu Feb 21 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-12k)
- update JLP(Japan Language Pack) v1.0
- update regjp-kondara.patch
- add patch mozilla-0.9.8-ctrl+enter-newtab.patch
  Bug#105537 (see http://bugzilla.mozilla.org/show_bug.cgi?id=105537)

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.8-10k)
- remove NO_XALF flag from mozilla.spec

* Fri Feb 15 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.8-8k)
- remade ui font patches
- patch from trunk
  problems with caret positioning on textarea.
  Bug#8365 (see -> http://bugzilla.mozilla.org/show_bug.cgi?id=83650)

* Thu Feb 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.8-6k)
- add overlay-menu.patch editor-overlay-menu.patch  mozilla-prefs-debug.patch
- update regjp-kondara.patch

* Wed Feb 06 2002 Toshiro Hikita <toshi@sodan.org>
- (0.9.8-4k)
- update JLP(Japan Language Pack) v0.1
- update regjp-kondara.patch

* Wed Feb 06 2002 Toshiro Hikita <toshi@sodan.org>
- (0.9.8-2k)
- updated to 0.9.8
- remove overlay-menu.patch editor-overlay-menu.patch
   font.patch ircjis.patch kondaraUIfont.patch customtab.patch
   UIfont.jar.mn.patch enable_mng.patch pref-languages-add.patch
   readonly-textinput.patch
- add extlibart.patch
- requires libart_lgpl-devel
- update mozilla-make-package.pl

* Tue Jan 22 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-28k)
- update JLP(Japan Language Pack) v1.0

* Fri Dec  7 2001  Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.9.6-24k)
- revised spec to act properly on the build environment regardless smp/ump.

* Fri Dec  7 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-22k)
- add mozilla-0.9.6-readonly-textinput.patch
  c.f http://bugzilla.mozilla.org/show_bug.cgi?id=109265

* Fri Dec  7 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.6-20k)
- more clean in %clean section

* Wed Dec  5 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.6-18k)
- sarani change gtktooltip.patch

* Wed Dec  5 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.6-16k)
- change gtktooltip.patch

* Tue Dec  4 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.6-14k)
- gtktooltip.patch

* Fri Nov 30 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-12k)
- remake langpack patch and mng patch
- chatzilla return

* Fri Nov 30 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-10k)
- mozilla-0.9.6-pref-languages-add.patch
  c.f. http://bugzilla.mozilla.org/show_bug.cgi?id=109629

* Thu Nov 29 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-8k)
- add enable mng patch(mozilla-0.9.6-enabele_mng.patch)

* Tue Nov 27 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-6k)
- add mozilla-0.9.6-formControl-fix.patch
  fix 0.9.6's form control.

* Tue Nov 27 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-4k)
- fixed Souece12: tag.

* Mon Nov 26 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6-2k)
- update to 0.9.6 and remake patches (%patch23,24,25)
- added CSS kondarafont.css for UI setting
- added patch kondaraUIfont.jar.mn.patch
  for modern and classic skin

* Fri Nov 23 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.5-20k)
- modified mozilla-0.9.5-kondara-all.js.patch
  changed pref 'browser.tabs.opentabfor.urlbar' in default
  setting.
- delete mozilla-0.9.5-kondara-closebox-fix.patch
- added mozilla-0.9.5-kondara-customtab.patch

* Sun Nov 18 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.5-18k)
- added a patch(mozilla-0.9.5-kondara-closebox-fix.patch)
  change an appearance when mouse was hover on closebox of
  tabview.
- modified UI font setting. (added patch for classic skin.)
- added a patch(mozilla-0.9.5-kondara-all.js.patch)
  + change middle button's action.
  + word wrap of souce view.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.95-16k)
- nigittenu

* Tue Nov 13 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- fix UI font setting again.(T_T)

* Fri Nov  9 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- modified UI setting again.

* Fri Nov  2 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.5-10k)
- modified UI setting (modified userChrome.css)
- add --with-pthreads --enable-mathml --enable-svg parameter

* Wed Oct 31 2001 Toshiro Hikita <toshi@sodan.org>
- (0.9.5-8k)
- add new patches from mozilla-0.9.5-0vl1.src.rpm from Vine
   mozilla-0.9.3-ircjis.patch
   c.f. http://bugzilla.mozilla.gr.jp/show_bug.cgi?id=71
- add new patches from bugzilla DB
   mozilla-0.9.5-mailsort.patch
   c.f. http://bugzilla.mozilla.org/show_bug.cgi?id=94103
   mozilla-0.9.5-smtpportcheck.patch
   c.f. http://bugzilla.mozilla.org/show_bug.cgi?id=96309
   mozilla-0.9.5-smtpportfix.patch
   c.f. http://bugzilla.mozilla.org/show_bug.cgi?id=106631
- add BuildPreReq fileutils >= 4.1 for cp -fRL

* Wed Oct 24 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update jlp to ver 1.0

* Tue Oct 16 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 0.9.5

* Sun Oct  7 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update langjajp and regjp to release version

* Tue Oct  2 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update langjajp and regjp to RC2

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- update JLP

* Sat Jun 16 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix SOURCE12 URI and SOURCE13 name

* Thu Jun 14 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.9.1

* Mon May 21 2001 Shingo Akagaki <dora@kondara.org>
- add  bug-78483.patch
- add  bug-80059.patch

* Sat May 19 2001 Shingo Akagaki <dora@kondara.org>
- update jlp

* Thu May 17 2001 Shingo Akagaki <dora@kondara.org>
- bug-74284.patch
- update jlp

* Wed May 16 2001 Shingo Akagaki <dora@kondara.org>
- bug-53989.patch

* Wed May 16 2001 Shingo Akagaki <dora@kondara.org>
- bug-79551.patch

* Tue May 15 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- dont use xalf

* Thu May 10 2001 Shingo Akagaki <dora@kondara.org>
- remerge rawhide spec

/* Sat Apr 28 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add script to do all the db rebuilds for us.
/- Add versioned prereqs
/
/* Tue Apr 24 2001 Christopher Blizzard <blizzard@redhat.com>
/- Move .so libraries that aren't components to /usr/lib
/- Move devel tools to /usr/bin
/
/* Wed Apr 18 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add /usr/lib/mozilla/plugins to LD_LIBRARY_PATH in mozilla.sh

* Mon Apr 16 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.8.1-2k)
- version 0.8.1

* Tue Apr  3 2001 Shingo Akagaki <dora@kondara.org>
- (0.8-5k)
- rawhide version

/* Thu Mar 29 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add --without-mng to configure lines to avoid picking up the mng
/library in rawhide.
/
/* Tue Mar 20 2001 Christopher Blizzard <blizzard@redhat.com>
/- Ignore the return value of regxpcom and regchrome.  They will
/  segfault because of binary incompatible changes during upgrades but
/  the last upgrade bits will always work so we should be OK.
/
/* Mon Mar 12 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add chatzilla rpms.
/
/* Wed Mar 07 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add LD_ASSUME_KERNEL hack to mozilla.sh so that the JVM will work.
/
/* Tue Feb 27 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add missing directories to file sections for mozilla and
/  mozilla-psm.
/
/* Fri Feb 16 2001 Christopher Blizzard <blizzard@redhat.com>
/- Remove requires since we have prereq.
/
/* Sat Feb 10 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add xpidl compiler + related tools to the devel package.
/- Check that regxpcom exists in postun sections.  There's no ordering
/  guarantee when you uninstall rpms.
/
/* Mon Feb 05 2001 Christopher Blizzard <blizzard@redhat.com>
/- Turn of parallel builds again.
/
/* Sun Jan 28 2001 Christopher Blizzard <blizzard@redhat.com>
/- Try with parallel builds again.
/- Use --enable-nspr-autoconf
/- In the clean section don't delete the building area.
/
/* Sat Jan 27 2001 Christopher Blizzard <blizzard@redhat.com>
/- Remove hacks for installed-chrome.txt and regchrome.  They are
/  listed in the packages files now.
/
/* Thu Jan 18 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add proper prereq for psm, mail and base package
/- Remove checks for programs before running post and postun
/
/* Tue Jan 16 2001 Christopher Blizzard <blizzard@redhat.com>
/- Check the existence of programs before running post and postun
/  scripts for sub-packages.
/
/* Mon Jan 15 2001 Christopher Blizzard <blizzard@redhat.com>
/- Remove mozilla-specific rdf toolbar items
/
/* Thu Jan 11 2001 Christopher Blizzard <blizzard@redhat.com>
/- Re-add rm in clean
/
/* Tue Jan 09 2001 Christopher Blizzard <blizzard@redhat.com>
/- Change tip serial to 8 and 0.7 release serial to 7.  This way we can
/  do upgrades properly.
/
/* Mon Jan 08 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add explicit paths for rm and mkdir
/
/* Sat Jan 06 2001 Christopher Blizzard <blizzard@redhat.com>
/- Remove patch for -fshort-wchar problems and use
/  --disable-short-wchar until the next compiler release.
/- Use cooler icons.
/
/* Tue Jan 02 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add patch to get default home page on 6.2 builds, too.
/
/* Mon Jan 01 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add patch to point default page at local file system and to point
/  mailnews at about:blank.
/
/* Mon Jan 01 2001 Christopher Blizzard <blizzard@redhat.com>
/- Add patch to remove debug menu from preferences.
/
/* Sun Dec 31 2000 Christopher Blizzard <blizzard@redhat.com>
/- Add patch to support building psm on ppc.
/
/* Fri Dec 29 2000 Christopher Blizzard <blizzard@redhat.com>
/- Add autoconf check for bad -fshort-wchar support in compiler.
/- Clean up a lot of cruft in the spec file that I don't need anymore.
/- Make regxpcom generated files non-config so that they aren't saved
/  as .rpmsave files.
/
/* Thu Dec 28 2000 Christopher Blizzard <blizzard@redhat.com>
/- Change the sparc patch to support sparc + sparc64.
/
/* Thu Dec 28 2000 Christopher Blizzard <blizzard@redhat.com>
/- Don't remove the CVS directories anymore since we're using the
/  package files now.
/
/* Thu Dec 28 2000 Christopher Blizzard <blizzard@redhat.com>
/- Breakout psm
/- Re-add the sparc config patch.
/
/* Wed Dec 20 2000 Christopher Blizzard <blizzard@redhat.com>
/- Add a bunch of dir entries to make sure that we don't leave a lot of
/  crap behind when we uninstall the rpm.
/
/* Tue Dec 19 2000 Christopher Blizzard <blizzard@redhat.com>
/- Finish with mail/news breakout.
/- Move component registration to package install time instead of at
/  package build time.
/
/* Wed Dec 13 2000 Christopher Blizzard <blizzard@redhat.com>
/- Break out mail/news
/
/* Tue Dec 12 2000 Christopher Blizzard <blizzard@redhat.com>
/- Remove Debug menus from mail/news, browser and editor.
/
/* Mon Dec 11 2000 Christopher Blizzard <blizzard@redhat.com>
/- Add new mozilla.sh script that tries to send commands to a running
/  instance first.
/- Add desktop files and icons.
/
/* Tue Dec 05 2000 Christopher Blizzard <blizzard@redhat.com>
/- Build on only one CPU until the parallel build problems are worked
/  out.
/- Don't try to build on ai64

* Fri Feb 16 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (0.8-3k)
- update to 0.8.
- enable irc.

* Sat Jan 20 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (0.7-9k)
- add JLP.
- modify kaigyo patch.
- add more patch from bugzilla.
- clean spec file.

* Wed Jan 17 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (0.7-7k)
- modify patch from bugzilla.
- modify font patch.
- modify desktop entry.

* Tue Jan 16 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (0.7-5k)
- enable psm.
- change version & url.
- add patch(bugzilla 63081,63789).

* Sun Jan 14 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (5.0-0.0070005k)
- add desktop entry for GNOME.

* Thu Dec 14 2000 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (5.0-0.0060003k)
- update to mozilla0.6.

* Mon Dec  4 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (5.0-0.0018005k)
- enable to build on Alpha and Mary(2.0). Thanx > a!

* Tue Nov 28 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix URL
- change version & release

* Thu Oct 26 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix build with new fileutils

* Fri Oct 13 2000 Christopher Blizzard <blizzard@redhat.com>
- Move to post M18 builds

* Mon Oct 2 2000 Christopher Blizzard <blizzard@redhat.com>
- start daily builds - remove the old patches
- delete installed directories for chrome since everything
  is in jar files now.

* Fri Aug 18 2000 Matt Wilson <msw@redhat.com>
- added patch to work around va_arg mess on gcc 2.96

* Wed Aug  9 2000 Christopher Blizzard <blizzard@redhat.com>
- Use official M17 source tarball

* Tue Aug  8 2000 Christopher Blizzard <blizzard@redhat.com>
- Add patch to fix crash when viewing some rgba images
- Add patch to fix sign warnings in gtkmozembed.h header

* Mon Aug  7 2000 Christopher Blizzard <blizzard@redhat.com>
- Build M17 official
- Add patch to fix crash when viewing some png images
- Add patch to fix context menu popup grab behaviour
- Add patch to fix focus problems with the file upload widget

* Thu Jul 27 2000 Christopher Blizzard <blizzard@redhat.com>
- Update to M17 snapshot

* Mon Jul 24 2000 Christopher Blizzard <blizzard@redhat.com>
- Update to most recent snapshot
- Added chrome rdf hacks

* Tue Mar 14 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- initial RPM
