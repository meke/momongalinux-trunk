// pref to override the default UI font because we're using xft
pref("font.uifont.name", "sans");
pref("font.uifont.pointheight", 12);
pref("font.minimum-size.ja", 12);
pref("font.minimum-size.zh-CN", 12);
pref("font.minimum-size.zh-TW", 12);
pref("font.default", "sans-serif");
