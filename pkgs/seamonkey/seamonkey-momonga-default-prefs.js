// Momonga Linux default settings
pref("app.update.enabled", false);
pref("app.update.autoInstallEnabled", false);
pref("browser.display.use_system_colors",   true);
pref("general.smoothScroll",                true);
pref("general.useragent.vendor", "Momonga");
pref("general.useragent.vendorSub", "SEAMONKEY_RPM_VR");
pref("update_notifications.enabled", false);
