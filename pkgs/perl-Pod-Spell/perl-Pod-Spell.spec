%global         momorel 2

Name:           perl-Pod-Spell
Version:        1.15
Release:        %{momorel}m%{?dist}
Summary:        Formatter for spellchecking Pod
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Pod-Spell/
Source0:        http://www.cpan.org/authors/id/X/XE/XENO/Pod-Spell-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-base
BuildRequires:  perl-Carp
BuildRequires:  perl-Class-Tiny
BuildRequires:  perl-constant
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-ShareDir-ProjectDistDir
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Pod-Escapes
BuildRequires:  perl-Pod-Parser
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Most
BuildRequires:  perl-Text-Tabs
Requires:       perl-base
Requires:       perl-Carp
Requires:       perl-constant
Requires:       perl-Pod-Escapes
Requires:       perl-Pod-Parser
Requires:       perl-Text-Tabs
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Pod::Spell is a Pod formatter whose output is good for spellchecking.
Pod::Spell rather like Pod::Text, except that it doesn't put much effort
into actual formatting, and it suppresses things that look like Perl
symbols or Perl jargon (so that your spellchecking program won't complain
about mystery words like "$thing" or "Foo::Bar" or "hashref").

%prep
%setup -q -n Pod-Spell-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CONTRIBUTING LICENSE META.json perlcritic.rc README
%{_bindir}/podspell
%{perl_vendorlib}/auto/share/dist/Pod-Spell
%{perl_vendorlib}/Pod/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-2m)
- rebuild against perl-5.18.2

* Sun Nov  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Fri Sep 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Sun Sep 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Fri Sep 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-2m)
- rebuild against perl-5.18.1

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-2m)
- rebuild against perl-5.18.0

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Thu May  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.01-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.01-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.01-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-2m)
- rebuild against rpm-4.6

* Sat May 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- Specfile autogenerated by cpanspec 1.75 for Momonga Linux.
