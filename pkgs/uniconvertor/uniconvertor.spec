%global momorel 5

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name: uniconvertor
Summary: Universal vector graphics translator
Group: Development/Libraries
Version: 1.1.4
Release: %{momorel}m%{?dist}
URL: http://sk1project.org/modules.php?name=Products&product=uniconvertor
Source0: http://sk1project.org/downloads/uniconvertor/v%{version}/uniconvertor-%{version}.tar.gz
NoSource: 0
Patch0: UniConvertor-1.1.0-simplify.patch
Patch1: UniConvertor-1.1.1-rename-in-help.patch
Patch2: UniConvertor-1.1.1-use-exec.patch
License: LGPLv2+ and GPLv2+ and MIT
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7
Requires: python-imaging 
Requires: python-reportlab

%description
UniConvertor is a universal vector graphics translator. It uses sK1
engine to convert one format to another.

%prep
%setup -q -n UniConvertor-%{version}
%patch0 -p1 -b .simplify
%patch1 -p1 -b .rename-in-help
%patch2 -p1 -b .use-exec

# Prepare for inclusion into documentation part
install -p -m644 src/COPYRIGHTS COPYRIGHTS
install -p -m644 src/GNU_GPL_v2 GNU_GPL_v2
install -p -m644 src/GNU_LGPL_v2 GNU_LGPL_v2

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --root=%{buildroot} --prefix=%{_prefix}

# Fix permissions
chmod a+x %{buildroot}%{python_sitearch}/uniconvertor/__init__.py
chmod g-w %{buildroot}%{python_sitearch}/uniconvertor/app/modules/*.so

# Don't duplicate documentation
rm -f %{buildroot}%{python_sitearch}/uniconvertor/{COPYRIGHTS,GNU_GPL_v2,GNU_LGPL_v2}

# due to confliction with netatalk
mv %{buildroot}%{_bindir}/uniconv %{buildroot}%{_bindir}/uniconvertor

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%doc COPYRIGHTS GNU_GPL_v2 GNU_LGPL_v2
%{_bindir}/uniconvertor
%{python_sitearch}/UniConvertor-*.egg-info
%{python_sitearch}/uniconvertor

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4 based on Fedora 13 (1.1.4-2)

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-4m)
- add %%dir

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3
- License: LGPLv2+ and GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.2-2m)
- rebuild against python-2.6.1-1m

* Sun Apr 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-3m)
- rebuild against gcc43

* Tue Mar 18 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-2m)
- rename scriptname ("uniconv" to "uniconvertor" to resolve confliction with netatalk)

* Sat Mar 15 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-1m)
- import to Momonga

* Fri Feb 01 2008 mrdocs at scribus.info
- new version release

* Fri Dec 22 2007 mrdocs at scribus.info 
- new version

* Tue Dec 04 2007 mrdocs at scribus.info 
- first stable release
- first OBS package


