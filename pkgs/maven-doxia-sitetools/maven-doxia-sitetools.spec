%global momorel 4

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define parent maven-doxia
%define subproj sitetools
%define namedversion 1.0-alpha-10

Name:           %{parent}-%{subproj}
Version:        1.0
Release:        0.1.%{momorel}m%{?dist}
Summary:        Doxia content generation framework
License:        "ASL 2.0"
Group:          Development/Tools
URL:            http://maven.apache.org/doxia/

# svn export \
#  http://svn.apache.org/repos/asf/maven/doxia/doxia-sitetools/tags/doxia-sitetools-1.0-alpha-10/ \
#  maven-doxia-sitetools
# tar czf maven-doxia-sitetools-1.0-alpha10-src.tar.gz maven-doxia-sitetools/
Source0:        %{name}-1.0-alpha10-src.tar.gz

Patch0:         %{name}-disablehtmlunit.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  itext >= 2.1.7
BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-site
BuildRequires:  maven2-plugin-surefire
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  maven-doxia >= 1.0-0.3.1m
BuildRequires:  plexus-maven-plugin >= 1.3.5-2m
BuildRequires:  modello-maven-plugin >= 1.0-0.2.2m
BuildRequires:  plexus-xmlrpc >= 1.0-0.4.4m
BuildRequires:  classworlds
BuildRequires:  jakarta-commons-collections
BuildRequires:  jakarta-commons-logging
BuildRequires:  jakarta-commons-validator
BuildRequires:  junit
BuildRequires:  oro
BuildRequires:  plexus-container-default
BuildRequires:  plexus-i18n
BuildRequires:  plexus-utils
BuildRequires:  plexus-velocity
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  velocity

Requires:       classworlds
Requires:       jakarta-commons-collections
Requires:       jakarta-commons-logging
Requires:       jakarta-commons-validator
Requires:       junit
Requires:       maven-doxia
Requires:       oro
Requires:       plexus-container-default
Requires:       plexus-i18n
Requires:       plexus-utils
Requires:       plexus-velocity
Requires:       velocity

Requires:       java >= 0:1.6.0
Requires:       jpackage-utils >= 0:1.7.2
Requires(post):   jpackage-utils >= 0:1.7.2
Requires(postun): jpackage-utils >= 0:1.7.2

BuildArch:      noarch

%description
Doxia is a content generation framework which aims to provide its
users with powerful techniques for generating static and dynamic
content. Doxia can be used to generate static sites in addition to
being incorporated into dynamic content generation systems like blogs,
wikis and content management systems.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{name}

%patch0

# Disable test that needs htmlunit, until we get it in Fedora
rm -f doxia-site-renderer/src/test/java/org/apache/maven/doxia/siterenderer/DefaultSiteRendererTest.java

%build

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
      -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
      -Dmaven.test.failure.ignore=true \
      install

for dir in doxia*; do
    pushd $dir
        mvn-jpp \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        javadoc:javadoc
    popd
done

%post
%update_maven_depmap

%postun
%update_maven_depmap

%install
rm -rf $RPM_BUILD_ROOT

# jars/poms
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms

install -m 644 -p pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-sitetools.pom
install -m 644 -p doxia-decoration-model/pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-decoration-model.pom
install -m 644 -p doxia-site-renderer/pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-site-renderer.pom
install -m 644 -p doxia-doc-renderer/pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-doc-renderer.pom

%add_to_maven_depmap org.apache.maven.doxia doxia-sitetools %{namedversion} JPP/%{parent} sitetools
%add_to_maven_depmap org.apache.maven.doxia doxia-decoration-model %{namedversion} JPP/%{parent} decoration-model
%add_to_maven_depmap org.apache.maven.doxia doxia-site-renderer %{namedversion} JPP/%{parent} site-renderer
%add_to_maven_depmap org.apache.maven.doxia doxia-doc-renderer %{namedversion} JPP/%{parent} doc-renderer

install -dm 755 $RPM_BUILD_ROOT%{_javadir}/%{parent}

install -m 644 -p doxia-decoration-model/target/doxia-decoration-model-%{namedversion}.jar $RPM_BUILD_ROOT%{_javadir}/%{parent}/decoration-model-%{namedversion}.jar
install -m 644 -p doxia-site-renderer/target/doxia-site-renderer-%{namedversion}.jar $RPM_BUILD_ROOT%{_javadir}/%{parent}/site-renderer-%{namedversion}.jar
install -m 644 -p doxia-doc-renderer/target/doxia-doc-renderer-%{namedversion}.jar $RPM_BUILD_ROOT%{_javadir}/%{parent}/doc-renderer-%{namedversion}.jar

(cd $RPM_BUILD_ROOT%{_javadir}/%{parent} && for jar in *-%{namedversion}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{namedversion}||g"`; done)

# javadoc (all javadocs are contained in the main module docs dir used below)
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}/decoration-model
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}/site-renderer
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}/doc-renderer

cp -pr doxia-decoration-model/target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}/decoration-model/
cp -pr doxia-site-renderer/target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}/site-renderer/
cp -pr doxia-doc-renderer/target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}/doc-renderer/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_javadir}/%{parent}/*
%{_datadir}/maven2/poms/JPP.%{parent}-sitetools.pom
%{_datadir}/maven2/poms/JPP.%{parent}-decoration-model.pom
%{_datadir}/maven2/poms/JPP.%{parent}-site-renderer.pom
%{_datadir}/maven2/poms/JPP.%{parent}-doc-renderer.pom
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.1.2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.1m)
- import from Fedora 13

* Mon Dec 21 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0-0.2.a10.2
- BR maven-surefire-provider-junit.

* Tue Sep 01 2009 Andrew Overholt <overholt@redhat.com> 1.0-0.2.a10.1
- Add itext, tomcat5, and tomcat5-servlet-2.4-api BRs

* Fri Aug 28 2009 Andrew Overholt <overholt@redhat.com> 1.0-0.2.a10
- First Fedora build

* Fri Jun 20 2000 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.a10.0jpp.1
- Initial build
