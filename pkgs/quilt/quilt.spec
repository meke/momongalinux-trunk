%global momorel 5

Name: quilt
Summary: Scripts for working with series of patches
License: GPLv2+
Group: Development/Tools
Version: 0.48
Release: %{momorel}m%{?dist}
Source0: http://savannah.nongnu.org/download/quilt/quilt-%{version}.tar.gz
NoSource: 0
URL: http://savannah.nongnu.org/projects/quilt
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext gawk util-linux-ng
Requires: coreutils
Requires: diffutils
Requires: gzip
Requires: bzip2
Requires: sed
Requires: gawk
Requires: diffstat
Requires: postfix
Requires: util-linux-ng
Requires: tar
Requires: rpm-build

%description
These scripts allow one to manage a series of patches by keeping track of the
changes each patch makes. Patches can be applied, un-applied, refreshed, etc.

The scripts are heavily based on Andrew Morton's patch scripts found at
http://www.zip.com.au/~akpm/linux/patches/

%prep
%setup

%build
%configure --with-mta=%{_sbindir}/sendmail --with-diffstat=%{_bindir}/diffstat
%make

%install
rm -rf %{buildroot}
make install BUILD_ROOT=%{buildroot}
%{find_lang} %{name}
mv %{buildroot}/%{_docdir}/%{name}-%{version}/* .
rm -rf %{buildroot}/%{_docdir}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root)
%doc README README.MAIL quilt.pdf
%doc AUTHORS COPYING TODO
%{_bindir}/guards
%{_bindir}/quilt
%{_datadir}/quilt/
%{_datadir}/emacs/site-lisp/quilt.el
%{_libdir}/quilt/
%{_sysconfdir}/bash_completion.d/quilt
%config %{_sysconfdir}/quilt.quiltrc
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.48-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.46-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-3m)
- %%NoSource -> NoSource

* Sat Feb 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-2m)
- modify %%files

* Mon Dec 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-1m)
- initial commit

