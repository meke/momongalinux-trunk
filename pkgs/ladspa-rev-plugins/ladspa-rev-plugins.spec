%global momorel 7

Summary: A set of audio plugins for LADSPA by Fons Adriaensen.
Name: ladspa-rev-plugins
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kokkinizita.linuxaudio.org/linuxaudio/
Group: Applications/Multimedia
Source0:  http://kokkinizita.linuxaudio.org/linuxaudio/downloads/REV-plugins-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel

%description
This reverb is based on gverb by Juhana Sadeharju, but the code (now C++) is entirely original. I added a second input for stereo operation, and some code to prevent FP denormalisation. This is a preliminary release, and this plugin will probably change a lot in future versions.

%prep
%setup -q -n REV-plugins-%{version}

%build
%make

%install
install -d %{buildroot}%{_libdir}/ladspa
install -m 755 *.so   %{buildroot}%{_libdir}/ladspa
cd ..

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Sun May 29 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.1-7m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.1-1m)
- Initial Build for Momonga Linux

