%global momorel 1

%define nm_version          0.9.8.10
%define dbus_version        1.1
%define gtk2_version        2.10.0
%define openconnect_version 4.06

%define svn_snapshot        %{nil}

Summary:   NetworkManager VPN integration for openconnect
Name:      NetworkManager-openconnect
Version:   0.9.8.6
Release:   %{momorel}m%{?dist}
License:   GPLv2+
Group:     System Environment/Base
URL:       http://www.gnome.org/projects/NetworkManager/
Source:    http://ftp.gnome.org/pub/GNOME/sources/NetworkManager-openconnect/0.9/%{name}-%{version}.tar.xz
NoSource:  0
BuildRoot: %{_tmppath}/%{name}-%{version}-root

BuildRequires: gtk2-devel             >= %{gtk2_version}
BuildRequires: dbus-devel             >= %{dbus_version}
BuildRequires: NetworkManager-devel   >= %{nm_version}
BuildRequires: NetworkManager-glib-devel >= %{nm_version}
BuildRequires: GConf2-devel
BuildRequires: gcr-devel
BuildRequires: libglade2-devel
BuildRequires: intltool gettext
BuildRequires: gnome-common
BuildRequires: autoconf automake libtool
BuildRequires: openconnect >= %{openconnect_version}
Requires: NetworkManager   >= %{nm_version}
Requires: openconnect      >= %{openconnect_version}

Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires(pre): shadow-utils


%description
This package contains software for integrating the openconnect VPN software
with NetworkManager and the GNOME desktop

%prep
%setup -q

%build
%configure --enable-more-warnings=yes
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la
rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.a

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%pre
%{_sbindir}/groupadd -r nm-openconnect &>/dev/null || :
%{_sbindir}/useradd  -r -s /sbin/nologin -d / -M \
                     -c 'NetworkManager user for OpenConnect' \
                     -g nm-openconnect nm-openconnect &>/dev/null || :

%post
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
      %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
      %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS ChangeLog COPYING
%{_libdir}/NetworkManager/lib*.so*
%{_libexecdir}/nm-openconnect-auth-dialog
%{_sysconfdir}/dbus-1/system.d/nm-openconnect-service.conf
%{_sysconfdir}/NetworkManager/VPN/nm-openconnect-service.name
%{_libexecdir}/nm-openconnect-service
%{_libexecdir}/nm-openconnect-service-openconnect-helper
%dir %{_datadir}/gnome-vpn-properties/openconnect
%{_datadir}/gnome-vpn-properties/openconnect/nm-openconnect-dialog.ui

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.6-1m)
- update to 0.9.8.6

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-1m)
- update to 0.9.8.4

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.2-1m)
- update to 0.9.8.2

* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6.2-1m)
- update 0.9.6.2

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.0-1m)
- update 0.9.6.0

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5.95-1m)
- update 0.9.5.95

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4.0-2m)
- rebuild for gcr-devel

* Sun Mar 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4.0-1m)
- update 0.9.4.0

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-1m)
- update 0.9.3.995

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.0-1m)
- update 0.9.2.0

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1.95-1m)
- update 0.9.1.95

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.999-1m)
- update to 0.8.999

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-3m)
- fix openconnect version 3.02

* Mon Apr 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-2m)
- add BuildRequires

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- update 0.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update 0.8.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Mon May 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0.997-1m)
- update 0.8.0.997

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-1m)
- update 0.8

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-2m)
- delete __libtoolize hack

* Thu Nov 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- update 0.7.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0.99-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.0.99-3m)
- modify Requires

* Sun Jun 28 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.7.0.99-2m)
- add define __libtoolize

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.0.99-1m)
- import from Fedora

* Wed Apr  1 2009 David Woodhouse <David.Woodhouse@intel.com> 1:0.7.0.99-2
- Update translations from SVN
- Accept 'lasthost' and 'autoconnect' keys in gconf

* Thu Mar  5 2009 Dan Williams <dcbw@redhat.com> 1:0.7.0.99-1
- Update to 0.7.1rc3

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0.97-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 19 2009 Dan Williams <dcbw@redhat.com> 0.7.0.97-1
- Update to 0.7.1rc1

* Mon Jan  5 2009 David Woodhouse <David.woodhouse@intel.com> 0.7.0-4.svn14
- Rebuild for updated NetworkManager
- Update translations from GNOME SVN

* Sun Dec 21 2008 David Woodhouse <David.Woodhouse@intel.com> 0.7.0-3.svn9
- Update from GNOME SVN (translations, review feedback merged)

* Wed Dec 17 2008 David Woodhouse <David.Woodhouse@intel.com> 0.7.0-2.svn3
- Review feedback

* Tue Dec 16 2008 David Woodhouse <David.Woodhouse@intel.com> 0.7.0-1.svn3
- Change version numbering to match NetworkManager
