%global momorel 1

Summary: libgpg-error
Name: libgpg-error
Version: 1.12
Release: %{momorel}m%{?dist}
URL: ftp://ftp.gnupg.org/gcrypt/libgpg-error/
Source0: ftp://ftp.gnupg.org/gcrypt/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Source2: wk@g10code.com
Group: System Environment/Libraries
License: LGPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a library that defines common error values for all GnuPG
components.  Among these are GPG, GPGSM, GPGME, GPG-Agent, libgcrypt,
pinentry, SmartCard Daemon and possibly more in the future.

%package devel
Summary: Development files for the %{name} package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This is a library that defines common error values for all GnuPG
components.  Among these are GPG, GPGSM, GPGME, GPG-Agent, libgcrypt,
pinentry, SmartCard Daemon and possibly more in the future. This package
contains files necessary to develop applications using libgpg-error.

%prep
%setup -q

%build
%configure --enable-shared --disable-static
%make

%install
rm -fr %{buildroot}
%makeinstall

%find_lang %{name}

find %{buildroot} -name "*.la" -delete

%clean
rm -fr %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING COPYING.LIB AUTHORS README INSTALL NEWS ChangeLog
%{_bindir}/gpg-error
%{_libdir}/libgpg-error.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/gpg-error-config
%{_libdir}/libgpg-error.so
%{_includedir}/gpg-error.h
%{_datadir}/aclocal/gpg-error.m4
%{_datadir}/common-lisp

%changelog
* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8-2m)
- full rebuild for mo7 release

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-1m)
- update to 1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-2m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-3m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-2m)
- delete libtool library

* Sun Dec 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Thu Oct  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-1m)
- update to 1.4
- delete Source1 entry in this SPEC file...

* Sat Jan  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- sync with fc-devel

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.0-1m)

* Fri May 07 2004 TAKAHASHI Tamotsu <tamo>
- (0.7-1m)

* Thu Sep 11 2003 TAKAHASHI Tamotsu <tamo>
- (0.4-1m)
- add tests

* Wed Sep  3 2003 Robert Schiele <rschiele@uni-mannheim.de>
- initial specfile.
