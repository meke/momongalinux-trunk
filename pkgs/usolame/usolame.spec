%global momorel 11

Summary : usolame is a fake library of lame
Name: usolame
Version: 1.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Multimedia
Source0: usolame-1.0.tar.gz
URL: http://www.momonga-linux.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: lame = %{version}-%{release}
Obsoletes: lame

%description
Usolame is just a fake library of lame.
You should install REAL lame library if you want to use MP3 encoding
feature.

%package devel
Summary : usolame is a fake library of lame
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: lame-devel
Obsoletes: lame-devel

%description devel
Usolame is just a fake library of lame.
You should install REAL lame library if you want to use MP3 encoding
feature.

But usolame-devel is imitation

%prep
%setup -q

libtoolize -f
aclocal
automake -a -c
autoconf
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_libdir}/libmp3lame.a
rm -f %{buildroot}%{_libdir}/libmp3lame.la
rm -f %{buildroot}%{_libdir}/libmp3lame.so

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr (-, root, root)
%doc COPYING
%{_libdir}/libmp3lame.so.*

%files devel
%defattr (-, root, root)
%doc COPYING

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- add libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc43

* Wed Jun 28 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-4m)
- add devel package

* Mon Feb 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-3m)
- Provides: lame = %%{version}-%%{release}

* Thu Feb 24 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-2m)
- remove devel package
- spec file cleanup

* Thu Feb 24 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.0-1m)
- first release
