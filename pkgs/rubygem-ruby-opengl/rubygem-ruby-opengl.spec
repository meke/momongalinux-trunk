%global momorel 6

%define	ruby_sitelib	%(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define	ruby_sitearch	%(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")
%define	rubyabi		1.9.1

%define	gemdir		%(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define	gemname		ruby-opengl
%define	geminstdir	%{gemdir}/gems/%{gemname}-%{version}

Summary:	OpenGL Interface for Ruby
Name:		rubygem-%{gemname}
Version:	0.60.1
Release:	%{momorel}m%{?dist}
Group:		Development/Languages
License:	MIT
URL:		http://ruby-opengl.rubyforge.org/
Source0:	http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource:	0

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	freeglut-devel
BuildRequires:	libGL-devel
BuildRequires:	libGLU-devel
BuildRequires:	ruby(abi) = %{rubyabi}
BuildRequires:	ruby(rubygems)
BuildRequires:	rubygem-mkrf
BuildRequires:	rubygem(rake)
BuildRequires:	ruby-devel >= 1.9.2
Requires:	ruby(abi) = %{rubyabi}
Requires:	ruby(rubygems)
Provides:	rubygem(%{gemname}) = %{version}-%{release}

%description
ruby-opengl consists of Ruby extension modules that are bindings 
for the OpenGL, GLU, and GLUT libraries. It is intended to be 
a replacement for -- and uses the code from -- Yoshi's ruby-opengl.

%package	doc
Summary:	Documentation for %{name}
Group:		Documentation
# Directory ownership issue
Requires:	%{name} = %{version}-%{release}

%description	doc
This package contains documentation for %{name}.

%package	-n ruby-opengl
Summary:	Non-Gem support package for %{gemname}
Group:		Development/Languages
Requires:	%{name} = %{version}-%{release}
Provides:	ruby(opengl) = %{version}-%{release}

%description	-n ruby-opengl
This package provides non-Gem support for %{gemname}.

%prep
%setup -q -T -c
mkdir -p ./%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
export rake="rake --trace"

gem install \
	--local \
	--install-dir ./%{gemdir} \
	-V --force \
	%{SOURCE0}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
cp -a ./%{gemdir}/* %{buildroot}%{gemdir}

mkdir -p %{buildroot}%{ruby_sitearch}
mv %{buildroot}%{geminstdir}/lib/*.so %{buildroot}%{ruby_sitearch}

# cleanups
pushd %{buildroot}%{geminstdir}
rm -rf \
	doc/build_install.txt \
	ext/ \

grep -rlv '^#!/' examples/ | grep 'rb$' | xargs chmod 0644
grep -rl '^#!/' examples/ | grep 'rb$' | xargs chmod 0755
find . -type f | xargs file | sed -n -e '/CRLF/s|:.*||p' | xargs sed -i -e 's|\r||'
popd

# The following method is completely copied from rubygem-gettext
# spec file
#
# Create symlinks

create_symlink_rec(){

ORIGBASEDIR=$1
TARGETBASEDIR=$2

## First calculate relative path of ORIGBASEDIR 
## from TARGETBASEDIR
TMPDIR=$TARGETBASEDIR
BACKDIR=
DOWNDIR=
num=0
nnum=0
while true
do
	num=$((num+1))
	TMPDIR=$(echo $TMPDIR | sed -e 's|/[^/][^/]*$||')
	DOWNDIR=$(echo $ORIGBASEDIR | sed -e "s|^$TMPDIR||")
	if [ x$DOWNDIR != x$ORIGBASEDIR ]
	then
		nnum=0
		while [ $nnum -lt $num ]
		do
			BACKDIR="../$BACKDIR"
			nnum=$((nnum+1))
		done
		break
	fi
done

RELBASEDIR=$( echo $BACKDIR/$DOWNDIR | sed -e 's|//*|/|g' )

## Next actually create symlink
pushd %{buildroot}/$ORIGBASEDIR
find . -type f | while read f
do
	DIRNAME=$(dirname $f)
	BACK2DIR=$(echo $DIRNAME | sed -e 's|/[^/][^/]*|/..|g')
	mkdir -p %{buildroot}${TARGETBASEDIR}/$DIRNAME
	LNNAME=$(echo $BACK2DIR/$RELBASEDIR/$f | \
		sed -e 's|^\./||' | sed -e 's|//|/|g' | \
		sed -e 's|/\./|/|' )
	ln -s -f $LNNAME %{buildroot}${TARGETBASEDIR}/$f
done
popd

}

create_symlink_rec %{geminstdir}/lib %{ruby_sitelib}

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%check
# Need X, disabling
exit 0

%files
%defattr(-,root, root,-)
%{ruby_sitearch}/*.so
%dir	%{geminstdir}/
%doc	%{geminstdir}/doc/
%{geminstdir}/lib/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files	doc
%defattr(-,root,root,-)
%{gemdir}/doc/%{gemname}-%{version}/
%{geminstdir}/Rakefile
%{geminstdir}/examples/
%{geminstdir}/test/

%files	-n ruby-opengl
%defattr(-,root,root,-)
%{ruby_sitelib}/opengl.rb

%changelog
* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.60.1-6m)
- remove .yard directory

* Mon Nov 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.60.1-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.60.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.60.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.60.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60.1-1m)
- import from Fedora 13 and build with ruby19

* Wed Nov 11 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.60.1-4
- Include ri files

* Sat Jul 25 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.60.1-3
- F-12: Mass rebuild

* Fri Jun 27 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.60.1-2
- Initial packaging
