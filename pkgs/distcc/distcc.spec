%global momorel 8

Summary: a free distributed C/C++ compiler system
Name: distcc
Version: 3.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Languages
URL: http://distcc.samba.org/

Source0: http://distcc.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource: 0
Source1:    hosts.sample
Source2: distccd.service
Patch0: distcc-xinetd.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool
BuildRequires: gtk2-devel
BuildRequires: python >= 2.7
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv
Requires: gcc

%description
distcc is a program to distribute compilation of C or C++ code across
several machines on a network. distcc should always generate the same
results as a local compile, is simple to install and use, and is often
two or more times faster than a local compile.

%package server
Summary: distributed C/C++ compiler server
Group: Development/Languages
Requires: xinetd
Requires: gcc
# directory ownership
Requires: logrotate

%description server
The distccd server may be started either from a super-server such as
inetd, or as a stand-alone daemon.

%package gnome
Summary: distributed C/C++ compiler monitor for gnome
Group: Development/Tools

%description gnome
The distcc monitor for gnome.

%prep
%setup -q

%patch0 -p0

%build
CFLAGS="`echo %{optflags} | sed -e 's/-Wp,-D_FORTIFY_SOURCE=2//'`"
ac_cv_func_sendfile=no ac_cv_header_sys_sendfile_h=no \
%configure --with-gtk --datadir=%{_datadir}/distcc
%make

%install
rm -rf %{buildroot}
%makeinstall datadir="%{buildroot}%{_datadir}/distcc"

# The remaining configuration files are installed here rather than by
# 'make install' because their nature and their locations are too
# system-specific.
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -m 644 packaging/RedHat/logrotate.d/distcc $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/distcc
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/xinetd.d
install -m 644 packaging/RedHat/xinetd.d/distcc $RPM_BUILD_ROOT%{_sysconfdir}/xinetd.d/distcc

# Install distcdd unit file
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
install -Dm 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/distccd.service

# Install sample hosts file
install -Dm 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/hosts

# Install sample distccd config file
install -Dm 0644 contrib/redhat/sysconfig $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/distccd

# Install distcdd unit file
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
install -Dm 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/distccd.service

%__mkdir_p %{buildroot}%{_libexecdir}/distcc
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/cc
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/gcc
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/c++
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/g++
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/gcc_3_2
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/g++_3_2
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/gcc_4_0
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/g++_4_0
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/%{_target_cpu}-%{_vendor}-%{_target_os}-gcc
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/%{_target_cpu}-%{_vendor}-%{_target_os}-c++
%__ln_s %{_bindir}/distcc %{buildroot}%{_libexecdir}/distcc/%{_target_cpu}-%{_vendor}-%{_target_os}-g++

# remove needless files
rm -rf %{buildroot}%{_datadir}/distcc/doc

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null ||:


%post server
/usr/sbin/useradd -d /var/run/distcc -m -r distcc &>/dev/null || :

if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun server
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable distccd.service > /dev/null 2>&1 || :
    /bin/systemctl stop distccd.service > /dev/null 2>&1 || :
fi

%postun server
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart distccd.service >/dev/null 2>&1 || :
fi


%postun
update-desktop-database &> /dev/null ||:

%triggerun -- distcc-server < 3.1-7m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply distccd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save distccd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del distccd >/dev/null 2>&1 || :
/bin/systemctl try-restart distccd.service >/dev/null 2>&1 || :


%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog* NEWS README* TODO survey.txt doc
%{_bindir}/distcc
%{_bindir}/distccmon-text
%{_bindir}/pump
%{_bindir}/lsdistcc
%{_libexecdir}/distcc
%{_mandir}/man1/distcc.1*
%{_mandir}/man1/distccmon-text.1*
%{_libdir}/python*/site-packages/*.egg-info
%dir %{_libdir}/python*/site-packages/include_server
%{_libdir}/python*/site-packages/include_server/*
%{_mandir}/man1/include_server.1.*
%{_mandir}/man1/pump.1.*

%files server
%defattr(-,root,root)
%{_bindir}/distccd
%{_mandir}/man1/distccd.1*
%config(noreplace) %{_sysconfdir}/xinetd.d/distcc
%{_sysconfdir}/default/distcc
%dir %{_sysconfdir}/distcc
%{_sysconfdir}/distcc/clients.allow
%{_sysconfdir}/distcc/commands.allow.sh
%{_sysconfdir}/distcc/hosts
%config %{_sysconfdir}/logrotate.d/distcc
%config(noreplace) %{_sysconfdir}/sysconfig/distccd
%{_unitdir}/*

%files gnome
%defattr(-,root,root)
%{_bindir}/distccmon-gnome
%{_datadir}/distcc

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-8m)
- support systemd 

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-7m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- use BuildRequires

* Wed Dec  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1-1m)
- update to 3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-6m)
- temporarily drop -Wp,-D_FORTIFY_SOURCE=2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0-4m)
- rebuild against python-2.6.1-1m

* Wed Sep 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-3m)
- fix %%post section. 

* Mon Sep  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-2m)
- release %%{_sysconfdir}/logrotate.d, it's already provided by logrotate
- /etc/init.d/distcc start fails, please check and fix up scripts and configuration files

* Wed Aug 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18.3-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.3-3m)
- %%NoSource -> NoSource

* Wed Jun 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.18.3-2m)
- add gcc3.2 and gcc4
- remove gcc2.95.3 and gcc2.96

* Wed Dec 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.18.3-1m)
  update to 2.18.3
  
* Sun Nov 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.18.2-1m)
- fixes small portability bugs

* Thu Nov  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.18.1-1m)

* Wed Oct 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.18-1m)
- major bugfixes

* Wed Aug 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.17-1m)
- minor feature enhancements

* Thu Jul  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.16-1m)
- security fix on 64-bit platforms

* Wed Jul  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.15-1m)
- major bugfixes

* Tue May  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.14-1m)
- minor bugfixes

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.13-1m)
- including security fixes

* Fri Jan 09 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1
- add distccmon-text.1* to man

* Mon Dec 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.12-2m)
- 'noreplace' /etc/xinetd.d/distcc

* Sat Dec 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.12-1m)
- minor feature enhancements
- add '--log-level=notice' to server_args in distcc.xinetd

* Fri Oct 24 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.11.2-1m)
- minor bugfixes

* Thu Oct  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.11.1-1m)
- minor bugfixes

* Mon Sep 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.11-1m)
- major featur enhancements

* Wed Aug 13 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.10-1m)
- minor security fixes

* Mon Jul 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.9-1m)
- major feature enhancements

* Thu Jul 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.8-1m)
  update to 2.8

* Sun Jul  6 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.7.1-2m)
- BuildRequire: gtk+ only
- rebuild against rpm-4.0.4-52m

* Wed Jul  2 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.7.1-1m)
  update to 2.7.1

* Mon Jun 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.7-1m)
- major feature enhancements
- add 'distcc-gnome' package

* Thu Jun 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6-1m)
  update to 2.6

* Fri Jun  6 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.1-1m)
- minor bugfixes

* Mon May 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.5-1m)
- major feature enhancements

* Fri May 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.2-1m)

* Fri May  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2-1m)
- make symlinks at '/usr/libexec/distcc'

* Mon Apr 21 2003 Kenta MURATA <muraken2@nifty.com>
- (2.0.1-1m)
- version up.

* Fri Dec 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.15-1m)
- minor bugfixes
- the default TCP port has changed to a new IANA-assigned standard of 3632

* Fri Nov 22 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.14-1m)
