%global momorel 1

Summary:	Video Acceleration (VA) API for Linux
Name:		libva
Version:        1.2.1
Release: 	%{momorel}m%{?dist}
License:        GPL
Group:          User Interface/Desktops
URL:            http://www.freedesktop.org/wiki/Software/vaapi/
Source0: 	http://www.freedesktop.org/software/vaapi/releases/libva/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       mesa-libGL, libdrm
BuildRequires:	mesa-libGL-devel, libdrm-devel
BuildRequires:	systemd-devel >= 187

%description
Video Acceleration (VA) API for Linux

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q 

autoreconf -ivf

%build

%configure \
	--enable-dummy-backend \
	--enable-dummy-driver \
	--enable-glx
%make

%install
make DESTDIR=%{buildroot} install

find %{buildroot}/* -name "*.la" -exec rm -rf {} \;

# remove test program
rm -rf %{buildroot}/%{_bindir}/test*

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/vainfo
%{_bindir}/avcenc
%{_bindir}/h264encode
%{_bindir}/mpeg2vaenc
%{_bindir}/mpeg2vldemo
%{_bindir}/loadjpeg
%{_bindir}/putsurface
%{_bindir}/putsurface_wayland
%{_libdir}/libva*.so.*
%{_libdir}/dri/dummy_drv_video.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/va
%{_libdir}/libva*.so
%{_libdir}/pkgconfig/libva*.pc

%changelog
* Thu Aug  1 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Sun Apr 14 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-3m)
- rebuild against systemd-187

* Wed Jul 25 2012 SANUKI  Masaru <sanuki@momonga-linux.org> 
- (1.1.0-2m)
- change NoSource to Source

* Mon Jun 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.15-1m)
- update 1.0.15

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.14-1m)
- update 1.0.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32.0-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.32.0-1m)
- update 0.32.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.1-4m)
- full rebuild for mo7 release

* Tue Aug  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.31.1-3m)
- update 0.31.1-patch4

* Mon Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.31.1-2m)
- build with "--enable-glx --enable-i965-driver" to fix build

* Mon Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.1-1m)
- update 0.31.1-patch2

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.0-3m)
- update 0.31.0-patch13

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.0-2m)
- update 0.31.0-patch12

* Tue Mar 16 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.0-1m)
- Initial commit Momonga Linux
