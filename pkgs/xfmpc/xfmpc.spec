%global momorel 2

%global xfce4ver 4.10.0
%global major 0.2

Summary:        A MPD client for the Xfce desktop environment
Name:           xfmpc
Version:        0.2.2
Release:        %{momorel}m%{?dist}

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libmpd-devel >= 0.18.0
BuildRequires:  gettext, perl-XML-Parser, intltool, desktop-file-utils
 
%description
Xfmpc is a a graphical GTK+ MPD client for the Xfce desktop environment.
It is focusing on low footprint and easy usage.

To connect to a remote machine set the environment variables MPD_HOST 
and MPD_PORT as described in the mpc(1) manpage.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT transform='s,x,x,'
%find_lang %{name}
desktop-file-install --vendor ""                          \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications    \
        --add-only-show-in=XFCE                             \
        --delete-original                                    \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog IDEAS NEWS README THANKS
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.2-2m)
- rebuild against xfce4-4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-6m)
- rebuild against xfce4 4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-5m)
- delete __libtoolize hack

* Fri Dec 18 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.0-4m)
- define __libtoolize (build fix) 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-1m)
- rebuild against libmpd-0.18.0

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-1m)
- update to 0.1.0

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.7-1m)
- import from Fedora to Momonga

* Tue Aug 26 2008 Christoph Wickert <fedora christoph-wickert de> - 0.0.7-1
- Update to 0.0.7
- Only show desktop file in Xfce

* Fri Jul 11 2008 Christoph Wickert <fedora christoph-wickert de> - 0.0.6-1
- Initial Fedora package
