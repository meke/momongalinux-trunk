%global momorel 3
%global libver 2.8
%global withodbc 0

Summary: The GTK+ port of the wxWidgets library
Name: wxGTK
Version: 2.8.12
Release: %{momorel}m%{?dist}
# The wxWindows licence is the LGPL with a specific exemption allowing
# distribution of derived binaries under any terms. (This will eventually
# change to be "wxWidgets License" once that is approved by OSI.)
License: "wxWidgets Library Licence"
Group: System Environment/Libraries
URL: http://www.wxwidgets.org/
Source0: http://dl.sourceforge.net/sourceforge/wxwindows/%{name}-%{version}.tar.bz2
NoSource: 0
Patch2: wxGTK-2.8.10-expat-utf8-parser-dos.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: glib2
Requires: gtk2
Requires: pango
Requires: %{name}-common = %{version}-%{release}
Requires: wxBase = %{version}-%{release}
BuildRequires: SDL-devel
BuildRequires: GConf2-devel
BuildRequires: autoconf
BuildRequires: expat-devel
BuildRequires: freetype-devel
BuildRequires: gcc-c++
BuildRequires: gettext
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: gtk2-devel
BuildRequires: libGL-devel
BuildRequires: libGLU-devel
BuildRequires: libSM-devel
BuildRequires: libgnomeprintui22-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: pango-devel
%if %{withodbc}
BuildRequires: unixODBC-devel
%endif
BuildRequires: zlib-devel
Provides: wxwin

%description
wxWidgets is a free C++ library for cross-platform GUI development.
With wxWidgets, you can create applications for different GUIs (GTK+,
Motif/LessTif, MS Windows, Mac) from the same source code.

This package contains the ansi(unicode-disabled) version of the library.

%package devel
Summary: Development files for the wxWidgets library
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-gl = %{version}-%{release}
Requires: %{name}-common-devel = %{version}-%{release}
Requires: wxBase = %{version}-%{release}
Requires(post): chkconfig
Requires(postun): chkconfig

%description devel
Header files for wxGTK, the GTK+ port of the wxWidgets library.

This package contains the ansi(unicode-disabled) version of the library.

%package gl
Summary: The GTK+ port of the wxWidgets library, OpenGl add-on.
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description gl
OpenGl add-on library for wxGTK, the GTK+ port of the wxWidgets library.

This package contains the ansi(unicode-disabled) version of the library.

%if %{withodbc}
%package        odbc
Summary:        ODBC add-on for the wxWidgets library
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description odbc
ODBC (a SQL database connectivity API) add-on for the wxWidgets library.

This package contains the ansi(unicode-disabled) version of the library.
%endif

%package        unicode
Group:          System Environment/Libraries
Summary:        GTK+ port of the unicode-enabled wxWidgets GUI library
Requires:       %{name}-common = %{version}-%{release}
Requires:       wxBase-unicode = %{version}-%{release}
Requires:       gtk2

%description   unicode
wxWidgets is a free C++ library for cross-platform GUI development.
With wxWidgets, you can create applications for different GUIs (GTK+,
Motif/LessTif, MS Windows, Mac) from the same source code.

This package contains the unicode-enabled version of the library.

%package        unicode-devel
Group:          Development/Libraries
Summary:        Development files for the unicode-enabled wxWidgets library
Requires:       %{name}-unicode = %{version}-%{release}
Requires:       %{name}-unicode-gl = %{version}-%{release}
Requires:       %{name}-common-devel = %{version}-%{release}
Requires:       wxBase-unicode = %{version}-%{release}
Requires:       gtk2-devel
Requires(post): chkconfig
Requires(postun): chkconfig

%description unicode-devel
Header files for wxGTK, the GTK+ port of the wxWidgets library.

This package contains the unicode-enabled version of the library.

%package        unicode-gl
Summary:        OpenGL add-on for the unicode-enabled unicode-enabled wxWidgets library
Group:          System Environment/Libraries
Requires:       %{name}-unicode = %{version}-%{release}

%description unicode-gl
OpenGl add-on library for wxGTK, the GTK+ port of the wxWidgets library.

This package contains the unicode-enabled version of the library.

%if %{withodbc}
%package        unicode-odbc
Summary:        ODBC add-on for the wxWidgets library
Group:          System Environment/Libraries
Requires:       %{name}-unicode = %{version}-%{release}

%description unicode-odbc
ODBC (a SQL database connectivity API) add-on for the wxWidgets library.

This package contains the unicode-enabled version of the library.
%endif

%package common
Summary:        Common files for wxWidgets library
Group:          System Environment/Libraries

%description common
Common files for wxWidgets library

%package common-devel
Summary:        Common development files for wxWidgets library
Group:          Development/Libraries

%description common-devel
Common development files for wxWidgets library

%package -n     wxBase
Summary:        Non-GUI support classes from the wxWidgets library
Group:          System Environment/Libraries

%description -n wxBase
Every wxWidgets application must link against this library. It contains
mandatory classes that any wxWidgets code depends on (like wxString) and
portability classes that abstract differences between platforms. wxBase can
be used to develop console mode applications -- it does not require any GUI
libraries or the X Window System.

%package -n     wxBase-unicode
Summary:        Non-GUI support classes from the wxWidgets unicode-enabled library
Group:          System Environment/Libraries

%description -n wxBase-unicode
Every wxWidgets application must link against this library. It contains
mandatory classes that any wxWidgets code depends on (like wxString) and
portability classes that abstract differences between platforms. wxBase can
be used to develop console mode applications -- it does not require any GUI
libraries or the X Window System.

This package contains the unicode-enabled version of the library.

%prep
%setup -q -a 0
%patch2 -p0 -b .expat

sed -i -e 's|/usr/lib\b|%{_libdir}|' wx-config.in configure

# for unicode-enabled version build
cd %{name}-%{version}
sed -i -e 's|/usr/lib\b|%{_libdir}|' wx-config.in configure
%patch2 -p0 -b .expat

%build
export GDK_USE_XFT=1

# this code dereferences type-punned pointers like there's no tomorrow.
CFLAGS="%{optflags} -fno-strict-aliasing"
CXXFLAGS="%{optflags} -fno-strict-aliasing"

# --disable-optimise prevents our %%{optglags} being overridden
# (see OPTIMISE in configure).

# for ansi version build
%configure \
%if %{withodbc}
  --with-odbc \
%endif
  --disable-unicode \
  --with-gtk=2 \
  --with-opengl \
  --with-sdl \
  --with-gnomeprint \
  --enable-shared \
  --enable-soname \
  --disable-optimise \
  --disable-debug_flag \
  --disable-debug_info \
  --disable-debug_gdb \
  --enable-intl \
  --enable-no_deps \
  --disable-rpath \
  --enable-geometry \
  --enable-graphics_ctx \
  --enable-sound \
  --enable-mediactrl \
  --enable-display \
  --enable-timer \
  --enable-compat24 \
  --enable-compat26 

make %{?_smp_mflags}
make %{?_smp_mflags} -C contrib/src/stc
make %{?_smp_mflags} -C contrib/src/ogl
make %{?_smp_mflags} -C contrib/src/gizmos
make %{?_smp_mflags} -C contrib/src/svg

# Why isn't this this part of the main build? Need to investigate.
make %{?_smp_mflags} -C locale allmo

# for unicode-enabled version build
cd %{name}-%{version}
%configure \
%if %{withodbc}
  --with-odbc \
%endif
  --enable-unicode \
  --with-gtk=2 \
  --with-opengl \
  --with-sdl \
  --with-gnomeprint \
  --enable-shared \
  --enable-soname \
  --disable-optimise \
  --disable-debug_flag \
  --disable-debug_info \
  --disable-debug_gdb \
  --enable-intl \
  --enable-no_deps \
  --disable-rpath \
  --enable-geometry \
  --enable-graphics_ctx \
  --enable-sound \
  --enable-mediactrl \
  --enable-display \
  --enable-timer \
  --enable-compat24 \
  --enable-compat26 

make %{?_smp_mflags}
make %{?_smp_mflags} -C contrib/src/stc
make %{?_smp_mflags} -C contrib/src/ogl
make %{?_smp_mflags} -C contrib/src/gizmos
make %{?_smp_mflags} -C contrib/src/svg

# Why isn't this this part of the main build? Need to investigate.
make %{?_smp_mflags} -C locale allmo

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# for ansi version build
%makeinstall
%makeinstall -C contrib/src/stc
%makeinstall -C contrib/src/ogl
%makeinstall -C contrib/src/gizmos
%makeinstall -C contrib/src/svg

mv %{buildroot}%{_bindir}/wxrc-%{libver} %{buildroot}%{_bindir}/wxrc-%{libver}-ansi
%find_lang wxstd
%find_lang wxmsw
cat wxmsw.lang >> wxstd.lang

# for unicode enabled version build
rm %{buildroot}%{_bindir}/wxrc
cd %{name}-%{version}

%makeinstall
%makeinstall -C contrib/src/stc
%makeinstall -C contrib/src/ogl
%makeinstall -C contrib/src/gizmos
%makeinstall -C contrib/src/svg

mv %{buildroot}%{_bindir}/wxrc-%{libver} %{buildroot}%{_bindir}/wxrc-%{libver}-unicode

# this ends up being a symlink into the buildroot directly -- 
# not what we want!
rm -f %{buildroot}%{_bindir}/{wx-config,wxrc}
ln -s %{_libdir}/wx/config/gtk2-ansi-release-%{libver} %{buildroot}%{_bindir}/wx-config-ansi
ln -s %{_libdir}/wx/config/gtk2-unicode-release-%{libver} %{buildroot}%{_bindir}/wx-config-unicode

# for dummy link so that unpgrading from wxGTK-devel-2.4 does not remove wx-config (c.f. [Momonga-devel.ja:01894])
touch %{buildroot}%{_bindir}/wx-config

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post gl
/sbin/ldconfig

%postun gl
/sbin/ldconfig

%if %{withodbc}
%post odbc
/sbin/ldconfig

%postun odbc
/sbin/ldconfig
%endif

%post unicode
/sbin/ldconfig

%postun unicode
/sbin/ldconfig

%post unicode-gl
/sbin/ldconfig

%postun unicode-gl
/sbin/ldconfig

%if %{withodbc}
%post unicode-odbc
/sbin/ldconfig

%postun unicode-odbc
/sbin/ldconfig
%endif

# use alternatives to switch ansi/unicode version.
# default wx-config is ansi version
# to build apps with unicode-enabled version, use "wx-config-unicode" or "wx-config --unicode=yes" instead of wx-config
%post devel
# work around for upgrading from 2.6
# delete symlink in wxGTK-2.6 once so that alternatives seems not to overwrite an existing symlink when upgrading from wxGTK-2.6
if [ -f %{_bindir}/wxrc-2.6-ansi ]; then
    rm -rf %{_bindir}/wx-config
    # dirty hack for upgrading from wxGTK-2.6
    rm -rf %{_var}/lib/alternatives/wx-config
fi

%{_sbindir}/update-alternatives --install %{_bindir}/wx-config wx-config %{_libdir}/wx/config/gtk2-ansi-release-%{libver} 20 --slave %{_bindir}/wxrc wxrc %{_bindir}/wxrc-%{libver}-ansi

%postun devel
if [ "$1" = "0" ]; then
  update-alternatives --remove wx-config %{_libdir}/wx/config/gtk2-ansi-release-%{libver} 
fi

%post unicode-devel
%{_sbindir}/update-alternatives --install %{_bindir}/wx-config wx-config %{_libdir}/wx/config/gtk2-unicode-release-%{libver} 15 --slave %{_bindir}/wxrc wxrc %{_bindir}/wxrc-%{libver}-unicode

%postun unicode-devel
if [ "$1" = "0" ]; then
  %{_sbindir}/update-alternatives --remove wx-config %{_libdir}/wx/config/gtk2-unicode-release-%{libver} 
fi

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files common -f wxstd.lang
%defattr (-,root,root,-)
%doc docs/changes.txt docs/gpl.txt docs/lgpl.txt docs/licence.txt
%doc docs/licendoc.txt docs/preamble.txt docs/readme.txt docs/todo.txt

%files common-devel
%defattr (-,root,root,-)
%doc samples/*
%{_datadir}/bakefile/presets/
%ghost %{_bindir}/wx-config
%{_includedir}/wx-%{libver}
%dir %{_libdir}/wx
%dir %{_libdir}/wx/config
%dir %{_libdir}/wx/include
%{_datadir}/aclocal/*

%files
%defattr(-,root,root,-)
%doc docs/changes.txt docs/gpl.txt docs/lgpl.txt docs/licence.txt
%doc docs/licendoc.txt docs/preamble.txt docs/readme.txt docs/todo.txt
%{_libdir}/libwx_gtk2_adv-*.so.*
%{_libdir}/libwx_gtk2_aui-*.so.*
%{_libdir}/libwx_gtk2_core-*.so.*
%{_libdir}/libwx_gtk2_gizmos-*.so.*
%{_libdir}/libwx_gtk2_gizmos_xrc*.so.*
%{_libdir}/libwx_gtk2_html-*.so.*
%{_libdir}/libwx_gtk2_media-*.so.*
%{_libdir}/libwx_gtk2_ogl-*.so.*
%{_libdir}/libwx_gtk2_qa-*.so.*
%{_libdir}/libwx_gtk2_richtext-*.so.*
%{_libdir}/libwx_gtk2_stc-*.so.*
%{_libdir}/libwx_gtk2_svg-*.so.*
%{_libdir}/libwx_gtk2_xrc-*.so.*

%files unicode
%defattr(-,root,root,-)
%doc docs/changes.txt docs/gpl.txt docs/lgpl.txt docs/licence.txt
%doc docs/licendoc.txt docs/preamble.txt docs/readme.txt docs/todo.txt
%{_libdir}/libwx_gtk2u_adv-*.so.*
%{_libdir}/libwx_gtk2u_aui-*.so.*
%{_libdir}/libwx_gtk2u_core-*.so.*
%{_libdir}/libwx_gtk2u_gizmos-*.so.*
%{_libdir}/libwx_gtk2u_gizmos_xrc*.so.*
%{_libdir}/libwx_gtk2u_html-*.so.*
%{_libdir}/libwx_gtk2u_media-*.so.*
%{_libdir}/libwx_gtk2u_ogl-*.so.*
%{_libdir}/libwx_gtk2u_qa-*.so.*
%{_libdir}/libwx_gtk2u_richtext-*.so.*
%{_libdir}/libwx_gtk2u_stc-*.so.*
%{_libdir}/libwx_gtk2u_svg-*.so.*
%{_libdir}/libwx_gtk2u_xrc-*.so.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/wx-config-ansi
%{_bindir}/wxrc*-ansi
%{_libdir}/libwx_base-*.so
%{_libdir}/libwx_base_net-*.so
%{_libdir}/libwx_base_xml-*.so
%{_libdir}/libwx_gtk2_adv-*.so
%{_libdir}/libwx_gtk2_aui-*.so
%{_libdir}/libwx_gtk2_core-*.so
%{_libdir}/libwx_gtk2_gizmos-*.so
%{_libdir}/libwx_gtk2_gizmos_xrc*.so
%{_libdir}/libwx_gtk2_html-*.so
%{_libdir}/libwx_gtk2_media-*.so
%{_libdir}/libwx_gtk2_ogl-*.so
%{_libdir}/libwx_gtk2_qa-*.so
%{_libdir}/libwx_gtk2_richtext-*.so
%{_libdir}/libwx_gtk2_stc-*.so
%{_libdir}/libwx_gtk2_svg-*.so
%{_libdir}/libwx_gtk2_xrc-*.so
%{_libdir}/libwx_gtk2_gl-*.so
%{_libdir}/wx/include/gtk2-ansi-release-*
%{_libdir}/wx/config/gtk2-ansi-release-*

%files unicode-devel
%defattr(-,root,root,-)
%{_bindir}/wx-config-unicode
%{_bindir}/wxrc*-unicode
%{_libdir}/libwx_baseu-*.so
%{_libdir}/libwx_baseu_net-*.so
%{_libdir}/libwx_baseu_xml-*.so
%{_libdir}/libwx_gtk2u_adv-*.so
%{_libdir}/libwx_gtk2u_aui-*.so
%{_libdir}/libwx_gtk2u_core-*.so
%{_libdir}/libwx_gtk2u_gizmos-*.so
%{_libdir}/libwx_gtk2u_gizmos_xrc*.so
%{_libdir}/libwx_gtk2u_html-*.so
%{_libdir}/libwx_gtk2u_media-*.so
%{_libdir}/libwx_gtk2u_ogl-*.so
%{_libdir}/libwx_gtk2u_qa-*.so
%{_libdir}/libwx_gtk2u_richtext-*.so
%{_libdir}/libwx_gtk2u_stc-*.so
%{_libdir}/libwx_gtk2u_svg-*.so
%{_libdir}/libwx_gtk2u_xrc-*.so
%{_libdir}/libwx_gtk2u_gl-*.so
%{_libdir}/wx/include/gtk2-unicode-release-*
%{_libdir}/wx/config/gtk2-unicode-release-*

%files gl
%defattr(-,root,root,-)
%{_libdir}/libwx_gtk2_gl-*.so.*

%files unicode-gl
%defattr(-,root,root,-)
%{_libdir}/libwx_gtk2u_gl-*.so.*

%if %{withodbc}
%files odbc
%defattr(-,root,root,-)
%{_libdir}/libwx_gtk2_odbc-*.so.*

%files unicode-odbc
%defattr(-,root,root,-)
%{_libdir}/libwx_gtk2u_odbc-*.so.*
%endif

%files -n wxBase
%defattr(-,root,root,-)
%doc docs/changes.txt docs/gpl.txt docs/lgpl.txt docs/licence.txt
%doc docs/licendoc.txt docs/preamble.txt docs/readme.txt
%{_libdir}/libwx_base-*.so.*
%{_libdir}/libwx_base_net-*.so.*
%{_libdir}/libwx_base_xml-*.so.*

%files -n wxBase-unicode
%defattr(-,root,root,-)
%doc docs/changes.txt docs/gpl.txt docs/lgpl.txt docs/licence.txt
%doc docs/licendoc.txt docs/preamble.txt docs/readme.txt
%{_libdir}/libwx_baseu-*.so.*
%{_libdir}/libwx_baseu_net-*.so.*
%{_libdir}/libwx_baseu_xml-*.so.*

%changelog
* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.12-3m)
- fix Require

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.12-2m)
- rebuild against libtiff-4.0.1

* Tue May 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.12-1m)
- update to 2.8.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.11-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.11-3m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.11-2m)
- separate out wxBase and wxBase-unicode

* Fri May 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.11-1m)
- update 2.8.11

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.10-7m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.10-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.10-5m)
- [SECURITY] Expat UTF-8 Parser DoS (Patch2)

* Thu Sep 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.10-4m)
- rebuild against glib-2.22.0
-- http://trac.wxwidgets.org/ticket/10883
- enable parallel build

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.10-3m)
- rebuild against libjpeg-7

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.10-2m)
- [SECURITY] CVE-2009-2369
- import upstream patch (Patch0)
- add Requires(post,postun): chkconfig

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.10-1m)
- update to 2.8.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9-2m)
- rebuild against rpm-4.6

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9-1m)
- update to 2.8.9

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.7-2m)
- change BuildRequires: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Thu May 15 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.8.7-1m)
- Update to 2.8.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.6-2m)
- %%NoSource -> NoSource

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.6-1m)
- version 2.8.6
- remove merged bad-g_free.patch

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.3-5m)
- import bad-g_free.patch from Fedora (to avoid crashing audacity-1.3.4)
 +* Mon Jul 16 2007 Matthew Miller <mattdm@mattdm.org> - 2.8.4-4
 +- patch from svn to fix rh bug #247414
- http://svn.wxwidgets.org/viewvc/wx?view=rev&revision=46513
- https://bugzilla.redhat.com/show_bug.cgi?id=247414

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.6.3-4m)
- rebuild against expat-2.0.0-1m

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.3-3m)
- new subpackge common and common-devel
- revised %%files

* Thu Apr  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.3-2m)
- revised packages owner

* Mon Apr 03 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.3-1m)
- update to bugfix 2.6.3 release.
- removed pathces (fixed in upstream)

* Sat Mar 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.2-1m)
- Update to version 2.6.2 (2.6 series)
- Build both of ansi(--disabled-unicode) and unicode(--enabled-unicode) version (like mandriva) since some apps want ansi version and others want unicode version.
- - Unicode-enabled version are packaged as wxGTK-unicode-* and they are independent of ansi versions (wxGTK-*)
- - wx-configs are installed as /usr/bin/wx-config-{ansi,unicode} and alternatives is used to switch to each other. (Note that default is ansi version and type "/usr/sbin/alternative --config wx-config" to configure.)
- - If some app require unicode version, use "Requires: wxGTK-unicode" etc. in spec file and replace "wx-config" to "wx-config-unicode" or "wx-config --unicode=yes" in build procedure (configure, Makefile etc...).
- Import patches from Fedora (wxGTK-2.6.2-5.fc5)
- - Patch0(wxGTK-2.6.2-intl_cpp.patch)
- - Patch1(wxGTK-2.6.2-socketclosefix.patch)
- - Patch2(wxGTK-2.6.2-gcc41stringh.patch)
- - %%changelog of fedora (which is related to 2.6.x) is following.
- -   * Mon Feb 13 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.2-5
- -   - rebuild in preparation for FC5
- -   * Mon Feb 06 2006 Matthew Miller <mattdm@mattdm.org> - 2.6.2-4
- -   - add wxGTK-2.6.2-socketclosefix.patch to fix aMule crashes. see
- -     bugzilla bug #178184
- -   - add wxGTK-2.6.2-gcc41stringh.patch (pulled from CVS) to make build on FC5 devel w/ gcc-4.1.
- -   * Wed Nov 30 2005 Matthew Miller <mattdm@mattdm.org> - 2.6.2-3
- -   - add wxGTK-2.6.2-intl_cpp.patch to deal with amule and probably other
- -     issues (see bug #154618 comment #47)
- -   - obsolete wxGTK2 < 2.6.2-1 specifically, at Matthias Saou's suggestion
- -   * Mon Nov 28 2005 Matthew Miller <mattdm@mattdm.org> - 2.6.2-2
- -   - implemented some suggestions from Matthias Saou:
- -   -   removed extraneous / from last line of ./configure 
- -   -   removed -n from setup macro, since we're now actually using the
- -       standard name
- -   -   don't use summary macro in opengl subpackage, as it's not clear which summary should get used
- -   -   don't bother setting CC, CXX, etc., as configure script does that move libdir/wx to devel subpackage
- -   * Thu Nov 24 2005 Matthew Miller <mattdm@mattdm.org> - 2.6.2-1
- -   - ready for actually putting into Extras
- -   - update mesa buildreqs for new split-up xorg packaging
- -   - libgnomeprint22-devel -> libgnomeprintui22-devel
- -   * Tue Oct 04 2005 Toshio Kuratomi <toshio-tiki-lounge.com> - 2.6.2-0.1
- -   - Update to 2.6.2.
- -   - Include the sample wx bakefiles.
- -   - Include new .mo files.
- -   - From Paul Johnson:
- -     Change license to wxWidgets due to concerns over trademark infringement.
- -     Add dist tag.
- -   - From Tom Callaway: Build and include libwx_gtk2u_animate-2.6.
- -   - update to 2.6.1
- -   - from Michael Schwendt in 2.4.2-11 package: build-require
- -     xorg-x11-Mesa-libGL and xorg-x11-Mesa-libGLU (the libGL and libGLU
- -     deps aren't provided in FC3, so not using that).
- -   - from Thorsten Leemhuis in 2.4.2-12 package: sed -i -e
- -     's|/usr/lib\b|%%{_libdir}|' in configure also to fix x86_64
- -   - properly include older 2.4.x changelog
- -   * Wed Apr 27 2005 Matthew Miller <mattdm@mattdm.org> - 2.6.0-0.1
- -   - include libwx_gtk2u_gizmos_xrc in file listing
- -   * Wed Apr 27 2005 Matthew Miller <mattdm@mattdm.org> - 2.6.0-0.0
- -   - update to 2.6.0 final release
- -   - configure now wants "--with-gtk=2" instead of "--enable-gtk2".

* Sun Nov 20 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-11m)
- gcc4-patch apply only gcc4

* Tue Nov  8 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.4.2-10m)
- import gcc4 patch (Patch3) from Fedora

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-9m)
- suppress AC_DEFUN warning.

* Wed Mar 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.2-8m)
- use a wildcard in making a symlink

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.4.2-7m)
- enable x86_64.

* Sat Nov 06 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-6m)
- s/wxWindows/wxWidgets/g (http://www.wxwidgets.org/name.htm)
- add Patch0 from fefora.us to work with GTK2 correctly
  (http://download.fedora.us/fedora/fedora/latest/i386/SRPMS.stable/wxGTK2-2.4.2-0.fdr.2.2.src.rpm)

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.4.2-5m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.2-4m)
- rebuild against for pango-1.4.0
- rebuild against for gtk+-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.2-3m)
- rebuild against for glib-2.4.0

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- revised spec for rpm 4.2.

* Sun Dec 14 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.4.2-1m)
- update to 2.4.2
- build with gtk2

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.9-9m)
- fix build problem against gcc-3.X

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.2.9-8k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.2.9-6k)
- gcc-3.1 patch(Patch0)

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.2.9-4k)
- rebuild against libpng 1.2.2.

* Wed Mar 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.2.9-2k)
- update to 2.2.9

* Thu Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.2.2-5k)
- modified robbed spec file

* Mon Sep 04 2000 Koichi Asano <koichi@comm.info.eng.osaka-cu.ac.jp>
- kondarize
