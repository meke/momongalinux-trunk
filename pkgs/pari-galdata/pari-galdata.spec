%global         momorel 1

Name:           pari-galdata
Version:        20080411
Release:        %{momorel}m%{?dist}
Summary:        PARI/GP Computer Algebra System Galois resolvents
Group:          System Environment/Libraries
License:         GPLv2+
URL:            http://pari.math.u-bordeaux.fr/packages.html
Source0:        http://pari.math.u-bordeaux.fr/pub/pari/packages/galdata.tgz
NoSource:       0
BuildArch:      noarch

%description
This package contains the optional PARI package galdata, which provides
the Galois resolvents for the polgalois function, for degrees 8 through 11.

%prep
%setup -cq

%build

%install
mkdir -p %{buildroot}%{_datadir}/pari/
cp -a data/galdata %{buildroot}%{_datadir}/pari/
%{_fixperms} %{buildroot}%{_datadir}/pari/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_datadir}/pari/galdata

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20080411-1m)
- import from Fedora

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080411-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080411-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080411-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jun  1 2012 Paul Howarth <paul@city-fan.org> - 20080411-3
- Drop conflict with old versions of pari, which cannot use this package but
  aren't broken by it either

* Wed May 23 2012 Paul Howarth <paul@city-fan.org> - 20080411-2
- Add dist tag
- Use %%{_fixperms} to ensure packaged files have sane permissions
- At least version 2.2.7 of pari is required to use this data, so conflict
  with older versions; can't use a versioned require here as it would lead to
  circular build dependencies with pari itself

* Fri May 18 2012 Paul Howarth <paul@city-fan.org> - 20080411-1
- Initial RPM package
