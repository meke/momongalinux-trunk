%global momorel 1

# Pass --with externalfuse to compile against system fuse lib
# Default is internal fuse-lite.
%define with_externalfuse %{?_with_externalfuse:1}%{!?_with_externalfuse:0}

Name:		ntfs-3g
Summary:	Linux NTFS userspace driver 
Version:	2014.2.15
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		System Environment/Base
Source0:	http://tuxera.com/opensource/%{name}_ntfsprogs-%{version}.tgz
NoSource:	0
URL:		http://www.ntfs-3g.org/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%if %{with_externalfuse}
BuildRequires:	fuse-devel
Requires:	fuse
%endif
BuildRequires:	libtool, libattr-devel
# ntfsprogs BuildRequires
BuildRequires:	libconfig-devel, libgcrypt-devel, gnutls-devel >= 3.2.0, libuuid-devel
Epoch:		2
Provides:	ntfsprogs-fuse = %{epoch}:%{version}-%{release}
Obsoletes:	ntfsprogs-fuse
Provides:	fuse-ntfs-3g = %{epoch}:%{version}-%{release}
Patch0:		ntfs-3g_ntfsprogs-2011.10.9-RC-ntfsck-unsupported-return-0.patch
Provides:	ntfsmount
Obsoletes:	ntfsmount

%description
NTFS-3G is a stable, open source, GPL licensed, POSIX, read/write NTFS 
driver for Linux and many other operating systems. It provides safe 
handling of the Windows XP, Windows Server 2003, Windows 2000, Windows 
Vista, Windows Server 2008 and Windows 7 NTFS file systems. NTFS-3G can 
create, remove, rename, move files, directories, hard links, and streams; 
it can read and write normal and transparently compressed files, including 
streams and sparse files; it can handle special files like symbolic links, 
devices, and FIFOs, ACL, extended attributes; moreover it provides full 
file access right and ownership support.

%package devel
Summary:	Development files and libraries for ntfs-3g
Group:		Development/Libraries
Requires:	%{name} = %{epoch}:%{version}-%{release}
Requires:	pkgconfig
Provides:      ntfsprogs-devel = %{epoch}:%{version}-%{release}
Obsoletes:     ntfsprogs-devel < %{epoch}:%{version}-%{release}

%description devel
Headers and libraries for developing applications that use ntfs-3g
functionality.

%package -n ntfsprogs
Summary:       NTFS filesystem libraries and utilities
Group:         System Environment/Base
# We don't really provide this. This code is dead and buried now.
Provides:      ntfsprogs-gnomevfs = %{epoch}:%{version}-%{release}
Obsoletes:     ntfsprogs-gnomevfs
# Needed to fix multilib issue
Obsoletes:     ntfsprogs < %{epoch}:%{version}-%{release}

%description -n ntfsprogs
The ntfsprogs package currently consists of a library and utilities such as 
mkntfs, ntfscat, ntfsls, ntfsresize, and ntfsundelete (for a full list of 
included utilities see man 8 ntfsprogs after installation).

%prep
%setup -q -n %{name}_ntfsprogs-%{version}%{?subver}
%patch0 -p1 -b .unsupported
autoreconf -if

%build
CFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64"
%configure \
	--disable-static \
	--disable-ldconfig \
%if 0%{?_with_externalfuse:1}
	--with-fuse=external \
%endif
	--exec-prefix=/usr \
	--enable-crypto \
	--enable-extras \
	--libdir=/%{_libdir}
make %{?_smp_mflags} LIBTOOL=%{_bindir}/libtool

%install
rm -rf %{buildroot}
make LIBTOOL=%{_bindir}/libtool DESTDIR=%{buildroot} install
rm -rf %{buildroot}/%{_libdir}/*.la
rm -rf %{buildroot}/%{_libdir}/*.a

# make the symlink an actual copy to avoid confusion
rm -rf %{buildroot}/usr/sbin/mount.ntfs-3g
cp -a %{buildroot}/usr/bin/ntfs-3g %{buildroot}/usr/sbin/mount.ntfs-3g

# Actually make some symlinks for simplicity...
# ... since we're obsoleting ntfsprogs-fuse
pushd %{buildroot}/usr/bin
ln -s ntfs-3g ntfsmount
popd
pushd %{buildroot}/usr/sbin
ln -s mount.ntfs-3g mount.ntfs-fuse
# And since there is no other package in Fedora that provides an ntfs 
# mount...
ln -s mount.ntfs-3g mount.ntfs
popd

# We get this on our own, thanks.
rm -rf %{buildroot}%{_defaultdocdir}/%{name}/README

mv  %{buildroot}/sbin/*  %{buildroot}/%{_sbindir}/
rmdir %{buildroot}/sbin/

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING CREDITS NEWS README
%{_sbindir}/mount.ntfs
%attr(754,root,root) %{_sbindir}/mount.ntfs-3g
%{_sbindir}/mount.ntfs-fuse
%{_sbindir}/mount.lowntfs-3g
%{_bindir}/ntfs-3g.probe
%{_bindir}/ntfs-3g.secaudit
%{_bindir}/ntfs-3g.usermap
%{_bindir}/lowntfs-3g
%{_bindir}/ntfs-3g
%{_bindir}/ntfsmount
%{_libdir}/libntfs-3g.so.*
%{_mandir}/man8/mount.lowntfs-3g.8*
%{_mandir}/man8/mount.ntfs-3g.8*
%{_mandir}/man8/ntfs-3g.8*
%{_mandir}/man8/ntfs-3g.probe.8*
%{_mandir}/man8/ntfs-3g.secaudit.8*
%{_mandir}/man8/ntfs-3g.usermap.8*

%files devel
%defattr(-,root,root,-)
%{_includedir}/ntfs-3g/
%{_libdir}/libntfs-3g.so
%{_libdir}/pkgconfig/libntfs-3g.pc

%files -n ntfsprogs
%doc AUTHORS COPYING CREDITS ChangeLog NEWS README
%{_bindir}/ntfscat
%{_bindir}/ntfscluster
%{_bindir}/ntfscmp
%{_bindir}/ntfsfix
%{_bindir}/ntfsinfo
%{_bindir}/ntfsls
# Extras
%{_bindir}/ntfsck
%{_bindir}/ntfsdecrypt
%{_bindir}/ntfsdump_logfile
%{_bindir}/ntfsmftalloc
%{_bindir}/ntfsmove
%{_bindir}/ntfstruncate
%{_bindir}/ntfswipe
%{_sbindir}/mkfs.ntfs
%{_sbindir}/mkntfs
%{_sbindir}/ntfsclone
%{_sbindir}/ntfscp
%{_sbindir}/ntfslabel
%{_sbindir}/ntfsresize
%{_sbindir}/ntfsundelete
%{_mandir}/man8/mkntfs.8*
%{_mandir}/man8/mkfs.ntfs.8*
%{_mandir}/man8/ntfscat.8*
%{_mandir}/man8/ntfsclone.8*
%{_mandir}/man8/ntfscluster.8*
%{_mandir}/man8/ntfscmp.8*
%{_mandir}/man8/ntfscp.8*
%{_mandir}/man8/ntfsfix.8*
%{_mandir}/man8/ntfsinfo.8*
%{_mandir}/man8/ntfslabel.8*
%{_mandir}/man8/ntfsls.8*
%{_mandir}/man8/ntfsprogs.8*
%{_mandir}/man8/ntfsresize.8*
%{_mandir}/man8/ntfsundelete.8*

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2014.2.15-1m)
- update 2014.2.15
- support usermove env

* Sat May  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2013.1.13-2m)
- rebuild against gnutls-3.2.0

* Sat Feb  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2013.1.13-1m)
- update 20130113

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2012.1.15-1m)
- update 20120115

* Thu Mar  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2011.10.9-2m)
- delete file for hal

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2011.10.9-1m)
- update 20111009

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2011.4.12-3m)
- add patch

* Mon Jul  4 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2011.4.12-2m)
- fix man files

* Sun Jul  3 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2011.4.12-1m)
- version up 2011.4.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2011.1.15-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2011.1.15-1m)
- update 2010.1.15

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.5.22-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2010.5.22-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2010.5.22-1m)
- update 2010.5.22

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2010.3.7-1m)
- update 2010.3.7

* Thu Jan 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2010.1.16-1m)
- update 2010.1.16

* Mon Dec  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2009.11.12-1m)
- update 2009.11.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2009.4.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Apr 19 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2009.4.4-1m)
- update 2009.4.4
- sync Fedora

* Fri Feb 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2009.2.1-1m)
- update 2009.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5130-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5130-1m)
- update 1.5130

* Tue Oct 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5012-1m)
- update 1.5012

* Thu Sep 11 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2812-1m)
- update 1.2812

* Mon Jul 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2712-2m)
- Provides and Obsoletes: ntfsmount

* Mon Jul 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2712-1m)
- update 1.2712

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2310-2m)
- rebuild against gcc43

* Sun Mar 30 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2310-1m)
- up to 1.2310

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1120-2m)
- %%NoSource -> NoSource

* Sun Dec 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1120-1m)
- update 1.1120

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.516-2m)
- fix duplicate files

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.516-1m)
- update to 1.516 (sync with FC-devel)

* Fri Feb 23 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-1m)
- initial commit

