%global         momorel 2

Name:           perl-Error
Version:        0.17022
Release:        %{momorel}m%{?dist}
Summary:        Error/exception handling in an OO-ish way
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Error/
Source0:        http://www.cpan.org/authors/id/S/SH/SHLOMIF/Error-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= v5.6.0
BuildRequires:  perl-Module-Build
BuildRequires:  perl-List-Util
Requires:       perl-List-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Error package provides two interfaces. Firstly Error provides a
procedural interface to exception handling. Secondly Error is a base class
for errors/exceptions that can either be thrown, for subsequent catch, or
can simply be recorded.

%prep
%setup -q -n Error-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog examples META.json README
%{perl_vendorlib}/Error
%{perl_vendorlib}/Error.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17022-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17022-1m)
- update to 0.17022
- rebuild against perl-5.18.2

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17021-1m)
- update to 0.17021

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17020-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17020-2m)
- rebuild against perl-5.18.0

* Sat May  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17020-1m)
- update to 0.17020

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17019-2m)
- rebuild against perl-5.16.3

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17019-1m)
- update to 0.17019

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17018-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17018-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17018-1m)
- update to 0.17018
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17017-1m)
- update to 0.17017

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17016-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17016-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17016-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17016-1m)
- update to 0.17016

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17015-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17015-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17015-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17015-1m)
- update to 0.17015

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17014-1m)
- update to 0.17014

* Sat May 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17013-1m)
- update to 0.17013

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17012-2m)
- rebuild against gcc43

* Sat Jan 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17012-1m)
- update to 0.17012

* Wed Dec 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17011-1m)
- update to 0.17011

* Fri Nov 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17010-1m)
- update to 0.17010

* Wed Aug 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17009-1m)
- update to 0.17009

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.17008-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17008-1m)
- update to 0.17008

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17004-1m)
- spec file was autogenerated
