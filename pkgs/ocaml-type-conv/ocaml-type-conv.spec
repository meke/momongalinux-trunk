%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-type-conv
Version:        3.0.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml base library for type conversion

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions" and Modified BSD
URL:            http://www.ocaml.info/home/ocaml_sources.html#type-conv
Source0:        http://hg.ocaml.info/release/type-conv/archive/release-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  dos2unix

%global __ocaml_requires_opts -i Asttypes -i Parsetree

%description
The type-conv mini library factors out functionality needed by
different preprocessors that generate code from type specifications,
because this functionality cannot be duplicated without losing the
ability to use these preprocessors simultaneously.


%prep
%setup -q -n type-conv-release-%{version}
dos2unix LICENSE.Tywith


%build
make


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE LICENSE.Tywith Changelog COPYRIGHT README.txt
%{_libdir}/ocaml/type-conv


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.10-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.10-1m)
- update to 1.6.10
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.7-1m)
- update to 1.6.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.5-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.5-1m)
- update to 1.6.5
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-1m)
- import from Fedora

* Mon May  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5.0-2
- Ignore Asttypes/Parsetree.

* Sat May  3 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5.0-1
- New upstream version 1.5.0.

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.4.0-2
- Rebuild for OCaml 3.10.2

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.4.0-1
- New version 1.4.0.
- Fixed upstream URL.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3.0-3
- Remove ExcludeArch ppc64.

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3.0-2
- Add missing BR for ocaml-camlp4-devel and test build in mock.

* Sun Feb 24 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3.0-1
- Initial RPM release.
