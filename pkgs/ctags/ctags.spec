%global momorel 4

Summary: A multi-language source code indexing tool
Name: ctags
Version: 5.8
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://ctags.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils chkconfig
Requires(postun): chkconfig
Conflicts: emacs <= 22.0.50-0.20060313.2m, xemacs <= 21.4.19-1m

%description
Exuberant Ctags generates an index (or tag) file of language objects
found in source files for many popular programming languages. This index
makes it easy for text editors and other tools to locate the indexed
items. Exuberant Ctags improves on traditional ctags because of its
multilanguage support, its ability for the user to define new languages
searched by regular expressions, and its ability to generate emacs-style
TAGS files.

Install ctags if you are going to use your system for programming.

%prep
%setup -q

%build
%configure
make

%install
test %{buildroot} != "/" && rm -rf %{buildroot}
%makeinstall
%__mkdir_p %{buildroot}%{_libdir}/%{name}
%__mv %{buildroot}%{_bindir}/ctags %{buildroot}%{_libdir}/%{name}/

%clean
test %{buildroot} != "/" && rm -rf %{buildroot}

%post
for i in ctags.1 etags.1 ; do
  if [ -L %{_mandir}/man1/$i ] ; then
    rm -f %{_mandir}/man1/$i
  fi
done
/usr/sbin/alternatives --install %{_bindir}/ctags ctags %{_libdir}/%{name}/ctags 80

%postun
[ -e %{_libdir}/%{name}/ctags ] || %{_sbindir}/alternatives --remove ctags %{_libdir}/%{name}/ctags

%files
%defattr(-,root,root)
%doc COPYING EXTENDING.html FAQ NEWS README ctags.html
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/ctags
#%%{_libdir}/%{name}/etags
%{_mandir}/man1/ctags.1*
#%%{_mandir}/man1/etags.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.8-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8-1m)
- update to 5.8

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.7-8m)
- fix up Requires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7-4m)
- rebuild against gcc43

* Sat Dec  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-3m)
- fixed alternatives

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-2m)
- revised alternatives

* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7-1m)
- update to 5.7
- do not use %%NoSource macro

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.6-3m)
- add PreReq: chkconfig

* Mon Jul 10 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6-2m)
- add support for alternatives.

* Sun Jun 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.6-1m)
- update to 5.6
- etags was deleted

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.5.4-1m)
- version 5.5.4

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (5.5.2-1m)
- version 5.5.2

* Sat May  3 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (5.5-1m)
- update to 5.5

* Fri Nov 22 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.4-1m)
- update to 5.4

* Sun Sep 15 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (5.3.1-1m)
- update to 5.3.1
- s/telia/us/

* Sun Aug 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.3-3m)
- remove old symlinks if exist
* Tue Aug  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (5.3-2m)
- enabling etags.

* Sun Jul 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.3-1m)
- minor feature enhancements

* Thu Feb 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (5.2.3-2k)

* Mon Jan 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (5.2.1-2k)

* Thu Nov  8 2001 Kazuhiko <kazuhiko@kondara.org>
- (5.1-2k)
- change URI of Source0

* Mon Sep 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (5.0.1-2k)

* Fri Dec 10 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 4)
- version 3.2

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 09 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 2.0.3

* Mon Nov 03 1997 Michael K. Johnson <johnsonm@redhat.com>
- removed etags.  Emacs provides its own; and needs to support
  more than just C.

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- updated from 1.5 to 1.6

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
