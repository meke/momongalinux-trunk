%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Summary: Objective CAML package manager and build helper
Name: ocaml-findlib
Version: 1.3.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Development/Libraries
URL: http://projects.camlcity.org/projects/findlib.html
Source0: http://download.camlcity.org/download/findlib-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-camlp4-devel >= %{ocamlver}
BuildRequires: ocaml-labltk-devel >= %{ocamlver}
BuildRequires: ocaml-ocamldoc >= %{ocamlver}
BuildRequires: ncurses-devel
BuildRequires: m4
BuildRequires: gawk

%global __ocaml_requires_opts -i Asttypes -i Parsetree

%description
Objective CAML package manager and build helper.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n findlib-%{version}

%build
ocamlc -version
ocamlc -where
(cd tools/extract_args && make)
tools/extract_args/extract_args -o src/findlib/ocaml_args.ml ocamlc ocamlcp ocamlmktop ocamlopt ocamldep ocamldoc ||:
cat src/findlib/ocaml_args.ml
./configure -config %{_sysconfdir}/ocamlfind.conf \
  -bindir %{_bindir} \
  -sitelib `ocamlc -where` \
  -mandir %{_mandir} \
  -with-toolbox
make all
%if %opt
make opt
%endif
rm doc/guide-html/TIMESTAMP

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
make install prefix=%{buildroot} OCAMLFIND_BIN=%{buildroot}%{_bindir}
mv %{buildroot}/%{buildroot}%{_bindir}/* %{buildroot}%{_bindir}

strip %{buildroot}%{_bindir}/ocamlfind

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE INSTALL doc/README doc/QUICKSTART
%config(noreplace) %{_sysconfdir}/ocamlfind.conf
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_libdir}/ocaml/*/META
%{_libdir}/ocaml/topfind
%{_libdir}/ocaml/findlib
%if %opt
%exclude %{_libdir}/ocaml/findlib/*.a
%exclude %{_libdir}/ocaml/findlib/*.cmxa
%endif
%exclude %{_libdir}/ocaml/findlib/*.mli
%exclude %{_libdir}/ocaml/findlib/Makefile.config
%exclude %{_libdir}/ocaml/findlib/make_wizard
%exclude %{_libdir}/ocaml/findlib/make_wizard.pattern
%{_libdir}/ocaml/num-top

%files devel
%defattr(-,root,root,-)
%doc LICENSE INSTALL doc/README doc/QUICKSTART doc/guide-html doc/ref-html
%if %opt
%{_libdir}/ocaml/findlib/*.a
%{_libdir}/ocaml/findlib/*.cmxa
%endif
%{_libdir}/ocaml/findlib/*.mli
%{_libdir}/ocaml/findlib/Makefile.config
%{_libdir}/ocaml/findlib/make_wizard
%{_libdir}/ocaml/findlib/make_wizard.pattern

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-4m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against ocaml-3.10.2

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against ocaml-3.10.1-1m

* Thu Nov 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- import to Momonga from Fedora devel
- update to 1.2.1

* Thu Sep  6 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-14
- Ignore Parsetree module, it's a part of the toplevel.

* Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-13
- Bump version to force rebuild against ocaml -6 release.

* Thu Aug 30 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-12
- Added BR: gawk.

* Thu Aug 30 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-11
- Force rebuild because of changed BRs in base OCaml.

* Thu Aug  2 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-10
- BR added ocaml-ocamldoc so that ocamlfind ocamldoc works.
- Fix path of camlp4 parsers in Makefile.

* Thu Jul 12 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-9
- Added ExcludeArch: ppc64

* Thu Jul 12 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-8
- Expanded tabs to spaces.
- Readded conditional opt section for files.

* Wed Jul 04 2007 Xavier Lamien <lxtnow[at]gmail.com> - 1.1.2pl1-7
- Fixed BR.

* Wed Jun 27 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-6
- Fix configure line.
- Install doc/guide-html.
- Added dependency on ncurses-devel.

* Mon Jun 11 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-5
- Build against 3.10.
- Update to latest package guidelines.

* Sat Jun  2 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-4
- Handle bytecode-only architectures.

* Sat May 26 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-3
- Missing builddep m4.

* Fri May 25 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-2
- Use OCaml find-requires and find-provides.

* Fri May 18 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.2pl1-1
- Initial RPM release.

