%global momorel 1
%global api_ver 0.24
%global priority 90

Name:           vala
Version:        0.24.0
Release: 	%{momorel}m%{?dist}
Summary:        A modern programming language for GNOME
Group: 		Development/Languages

# Most files are LGPLv2.1+, curses.vapi is 2-clause BSD
License:        LGPLv2+ and BSD
URL:            http://live.gnome.org/Vala
#VCS:           git:git://git.gnome.org/vala
# note: do not use a macro for directory name
# as it breaks Colin Walters' automatic build script
# see https://bugzilla.redhat.com/show_bug.cgi?id=609292
Source0:        http://download.gnome.org/sources/vala/0.24/vala-%{version}.tar.xz
NoSource:	0
Source1:        vala-mode.el
Source2:        vala-init.el
Source3:        emacs-vala-COPYING

BuildRequires:  flex
BuildRequires:  bison
BuildRequires:  glib2-devel
BuildRequires:  libxslt
# only if Vala source files are patched
# BuildRequires:  vala

# for Emacs modes
BuildRequires:  emacs emacs-el

# for tests
# BuildRequires:  dbus-x11

# alternatives
%global vala_binaries vala valac
%global vala_manpages valac
%global vala_tools_binaries vala-gen-introspect vapicheck vapigen
%global vala_tools_manpages vala-gen-introspect vapigen
Requires(posttrans):   %{_sbindir}/alternatives
Requires(preun):       %{_sbindir}/alternatives


%description
Vala is a new programming language that aims to bring modern programming
language features to GNOME developers without imposing any additional
runtime requirements and without using a different ABI compared to
applications and libraries written in C.

valac, the Vala compiler, is a self-hosting compiler that translates
Vala source code into C source and header files. It uses the GObject
type system to create classes and interfaces declared in the Vala source
code. It's also planned to generate GIDL files when gobject-
introspection is ready.

The syntax of Vala is similar to C#, modified to better fit the GObject
type system.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
Vala is a new programming language that aims to bring modern programming
language features to GNOME developers without imposing any additional
runtime requirements and without using a different ABI compared to
applications and libraries written in C.

This package contains development files for %{name}. This is not necessary for
using the %{name} compiler.


%package        tools
Summary:        Tools for creating projects and bindings for %{name}
License:        LGPLv2+
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       gnome-common intltool libtool pkgconfig

%description    tools
Vala is a new programming language that aims to bring modern programming
language features to GNOME developers without imposing any additional
runtime requirements and without using a different ABI compared to
applications and libraries written in C.

This package contains tools to generate Vala projects, as well as API bindings
from existing C libraries, allowing access from Vala programs.


%package        doc
Summary:        Documentation for %{name}
License:        LGPLv2+

BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Requires:       devhelp

%description    doc
Vala is a new programming language that aims to bring modern programming
language features to GNOME developers without imposing any additional
runtime requirements and without using a different ABI compared to
applications and libraries written in C.

This package contains documentation in a devhelp HTML book.


%package -n emacs-%{name}
Summary:        Vala mode for Emacs
License:        GPLv2+

BuildArch:      noarch
Requires:       emacs(bin) >= %{_emacs_version}


%description -n emacs-%{name}
An Emacs mode for editing Vala source code.

%package -n emacs-%{name}-el
Summary:        Elisp source files for emacs-%{name}
License:        GPLv2+

BuildArch:      noarch
Requires:       emacs-%{name} = %{version}-%{release}

%description -n emacs-%{name}-el
This package contains the elisp source files for Vala under GNU
Emacs. You do not need to install this package to run Vala. Install
the emacs-%{name} package to use Vala with GNU Emacs.


%prep
%setup -q


%build
%configure
# Don't use rpath!
sed -i 's|/lib /usr/lib|/lib /usr/lib /lib64 /usr/lib64|' libtool
make %{?_smp_mflags}
# Compile emacs module
mkdir emacs-vala && cd emacs-vala && cp -p %{SOURCE1} .
%{_emacs_bytecompile} vala-mode.el
# and copy its licensing file
cp -p %{SOURCE3} COPYING


%install
make install DESTDIR=$RPM_BUILD_ROOT
# remove symlinks, using alternatives
for f in %{vala_binaries} %{vala_tools_binaries};
do
    rm $RPM_BUILD_ROOT%{_bindir}/$f
    touch $RPM_BUILD_ROOT%{_bindir}/$f
done
for f in %{vala_manpages} %{vala_tools_manpages};
do
    rm $RPM_BUILD_ROOT%{_mandir}/man1/$f.1*
    touch $RPM_BUILD_ROOT%{_mandir}/man1/$f.1.bz2
done
# own this directory for third-party *.vapi files
mkdir -p $RPM_BUILD_ROOT%{_datadir}/vala/vapi
rm $RPM_BUILD_ROOT%{_libdir}/libvala-%{api_ver}.la

# Emacs mode files
mkdir -p $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{name}
mkdir -p $RPM_BUILD_ROOT%{_emacs_sitestartdir}
cp -p emacs-vala/*.el* $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{name}
cp -p %{SOURCE2} $RPM_BUILD_ROOT%{_emacs_sitestartdir}


%check
# make check


%posttrans
/sbin/ldconfig
for f in %{vala_binaries};
do
    %{_sbindir}/alternatives --install %{_bindir}/$f \
      $f %{_bindir}/$f-%{api_ver} %{priority}
done
for f in %{vala_manpages};
do
    %{_sbindir}/alternatives --install %{_mandir}/man1/$f.1.bz2 \
      $f.1.bz2 %{_mandir}/man1/$f-%{api_ver}.1.bz2 %{priority}
done

%posttrans tools
for f in %{vala_tools_binaries};
do
    %{_sbindir}/alternatives --install %{_bindir}/$f \
      $f %{_bindir}/$f-%{api_ver} %{priority}
done
for f in %{vala_tools_manpages};
do
    %{_sbindir}/alternatives --install %{_mandir}/man1/$f.1.bz2 \
      $f.1.bz2 %{_mandir}/man1/$f-%{api_ver}.1.bz2 %{priority}
done

%preun
/sbin/ldconfig
for f in %{vala_binaries};
do
    %{_sbindir}/alternatives --remove $f \
      %{_bindir}/$f-%{api_ver}
done
for f in %{vala_manpages};
do
    %{_sbindir}/alternatives --remove $f.1.bz2 \
      %{_mandir}/man1/$f-%{api_ver}.1.bz2
done

%preun tools
for f in %{vala_tools_binaries};
do
    %{_sbindir}/alternatives --remove $f \
      %{_bindir}/$f-%{api_ver}
done
for f in %{vala_tools_manpages};
do
    %{_sbindir}/alternatives --remove $f.1.bz2 \
      %{_mandir}/man1/$f-%{api_ver}.1.bz2
done


%files
%doc AUTHORS ChangeLog COPYING MAINTAINERS NEWS README THANKS
%ghost %{_bindir}/vala
%ghost %{_bindir}/valac
%{_bindir}/vala-%{api_ver}
%{_bindir}/valac-%{api_ver}
# owning only the directories, they should be empty
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala-%{api_ver}
%{_libdir}/libvala-%{api_ver}.so.*
%ghost %{_mandir}/man1/valac.1.bz2
%{_mandir}/man1/valac-%{api_ver}.1.bz2

%files devel
%{_includedir}/vala-%{api_ver}
%{_libdir}/libvala-%{api_ver}.so
%{_libdir}/pkgconfig/libvala-%{api_ver}.pc
# directory owned by filesystem
%{_datadir}/aclocal/vala.m4
%{_datadir}/vala/Makefile.vapigen

%files tools
%ghost %{_bindir}/vala-gen-introspect
%ghost %{_bindir}/vapicheck
%ghost %{_bindir}/vapigen
%{_bindir}/vala-gen-introspect-%{api_ver}
%{_bindir}/vapicheck-%{api_ver}
%{_bindir}/vapigen-%{api_ver}
%{_libdir}/vala-%{api_ver}
%{_datadir}/aclocal/vapigen.m4
%{_datadir}/pkgconfig/vapigen*.pc
%ghost %{_mandir}/man1/vala-gen-introspect.1.bz2
%ghost %{_mandir}/man1/vapigen.1.bz2
%{_mandir}/man1/vala-gen-introspect-%{api_ver}.1.bz2
%{_mandir}/man1/vapigen-%{api_ver}.1.bz2

%files doc
%doc %{_datadir}/devhelp/books/vala-%{api_ver}

%files -n emacs-%{name}
%doc emacs-vala/COPYING
%dir %{_emacs_sitelispdir}/%{name}
%{_emacs_sitelispdir}/%{name}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n emacs-%{name}-el
%{_emacs_sitelispdir}/%{name}/*.el


%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24.0-1m)
- update to 0.24.0

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.0-1m)
- update to 0.18.0

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17.7-1m)
- update to 0.17.7

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17.6-1m)
- update to 0.17.6

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17.5-1m)
- update to 0.17.5

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17.4-1m)
- update to 0.17.4

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17.2-1m)
- update to 0.17.2
- reimport from fedora

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.4-1m)
- update to 0.13.4

* Thu Jun  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Sat Apr  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Sat Jan 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Wed Dec 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-2m)
- rebuild for new GCC 4.5

* Wed Oct 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Sun Sep 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-2m)
- back to 0.9.3

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Tue Jun  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-2m)
- add %%dir

* Thu Apr 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.10-1m)
- update to 0.7.10

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.9-1m)
- update to 0.7.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.8-1m)
- update to 0.7.8

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.7-1m)
- update to 0.7.7

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.6-1m)
- initial build
