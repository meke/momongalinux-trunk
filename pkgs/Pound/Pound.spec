%global momorel 1

%define pound_user	pound
%define pound_group	%{pound_user}
%define pound_home	%{_localstatedir}/lib/pound

Name:		Pound
Version:	2.6
Release:	%{momorel}m%{?dist}
Summary:	Reverse proxy and load balancer

Group:		System Environment/Daemons
License:	GPLv3
URL:		http://www.apsis.ch/pound/

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	openssl-devel >= 1.0.0, pcre-devel
BuildRequires:  systemd-units
BuildRequires:  gperftools-devel

Requires(pre):	 shadow-utils
Requires(post):	 chkconfig
Requires(preun): chkconfig
Requires(post):  systemd-units
Requires(preun): systemd-units
Requires(postun):systemd-units

Source0:	http://www.apsis.ch/pound/%{name}-%{version}.tgz
NoSource:       0
Source1:  	pound.service
Source2:	pound.cfg
Patch0:		pound-remove-owner.patch

%description
The Pound program is a reverse proxy, load balancer and
HTTPS front-end for Web server(s). Pound was developed
to enable distributing the load among several Web-servers
and to allow for a convenient SSL wrapper for those Web
servers that do not offer it natively. Pound is distributed
under the GPL - no warranty, it's free to use, copy and
give away

%prep
%setup -q
%patch0 -p1 -b .remove-owner

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%{__install} -d %{buildroot}%{pound_home}
%{__install} -p -D -m 755 %{SOURCE1} %{buildroot}%{_unitdir}/pound.service
%{__install} -p -D -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/pound.cfg

mkdir -p %{buildroot}%{_sysconfdir}/pki/tls/certs
touch %{buildroot}%{_sysconfdir}/pki/tls/certs/pound.pem

%clean
rm -rf %{buildroot}

%pre
%{_sbindir}/groupadd -f -r %{pound_group}
id %{pound_user} >/dev/null 2>&1 || \
    %{_sbindir}/useradd -r -g %{pound_group} -d %{pound_home} -s /sbin/nologin \
    -c "Pound user" %{pound_user}

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi
# generate dummy certificate
exec > /dev/null 2> /dev/null
if [ ! -f %{_sysconfdir}/pki/tls/certs/pound.pem ] ; then
pushd %{_sysconfdir}/pki/tls/certs
umask 077
cat << EOF | make pound.pem
--
SomeState
SomeCity
Pound Example Certificate
SomeOrganizationalUnit
localhost.localdomain
root@localhost.localdomain
EOF
chown root:pound pound.pem
chmod 640 pound.pem
popd
fi
exit 0

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable pound.service > /dev/null 2>&1 || :
    /bin/systemctl stop pound.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart pound.service >/dev/null 2>&1 || :
fi

%triggerun -- pound < 2.5-6m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply pound
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save pound >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del pound >/dev/null 2>&1 || :
/bin/systemctl try-restart pound.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc CHANGELOG FAQ GPL.txt README
%{_mandir}/man8/pound.8*
%{_mandir}/man8/poundctl.8*
%{_sbindir}/pound
%{_sbindir}/poundctl
%{_unitdir}/pound.service
%config(noreplace) %{_sysconfdir}/pound.cfg
%ghost %config(noreplace) %{_sysconfdir}/pki/tls/certs/pound.pem
%attr(-,%{pound_user},%{pound_group}) %dir %{pound_home}

%changelog
* Sun Aug 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6
- rebuil with gperftools

* Tue Feb 21 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5-6m)
- support systemd servicefile

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-1m)
- update to 2.5
- apply openssl100 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.3-1m)
- version up 2.4.3

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-4m)
- rebuild against gcc43

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat May 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-2m)
- nodify Requires(pre): for mph-get-check

* Wed May 16 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.1-1m)
- import from fedora

* Mon Mar 12 2007 <ruben@rubenkerkhof.com> 2.2.7-1
- Sync with upstream
* Sun Mar 04 2007 <ruben@rubenkerkhof.com> 2.2.6-1
- Sync with upstream
* Wed Feb 21 2007 <ruben@rubenkerkhof.com> 2.2.5-1
- Sync with upstream
* Sat Feb 10 2007 <ruben@rubenkerkhof.com> 2.2.4-1
- Sync with upstream
* Sat Jan 20 2007 <ruben@rubenkerkhof.com> 2.2.3-1
- Fix problems in bad 2.2.2 release
* Mon Jan 15 2007 <ruben@rubenkerkhof.com> 2.2.2-1
- Sync with upstream
* Wed Jan 03 2007 <ruben@rubenkerkhof.com> 2.2.1-1
- Sync with new beta release from upstream
* Sun Dec 17 2006 <ruben@rubenkerkhof.com> 2.2-2
- Fixed empty debuginfo rpm (bz 219942)
* Sat Dec 16 2006 <ruben@rubenkerkhof.com> 2.2-1
- Sync with upstream
* Sat Dec 09 2006 <ruben@rubenkerkhof.com> 2.1.8-1
- Sync with upstream
* Thu Dec 07 2006 <ruben@rubenkerkhof.com> 2.1.7-1
- Sync with upstream
* Wed Nov 08 2006 <ruben@rubenkerkhof.com> 2.1.6-2
- Changed hardcoded paths into rpmmacros
* Mon Nov 06 2006 <ruben@rubenkerkhof.com> 2.1.6-1
- Synced with upstream version
- Changed Summary
- Added an init script
- Added pound.cfg with an example configuration
- Added pound user and group
- A self-signed ssl certificate is created in %%post

* Fri Nov 03 2006 <ruben@rubenkerkhof.com> 2.1.5-1
- initial version

