%global momorel 10
%global vjerel 0.1
%global ximdir %{_sysconfdir}/X11/xinit/xinput.d
%global ximbindir %{_prefix}/local/bin
%global _xinputconf %{ximdir}/vje.conf

Name: vje-support
Version: 3.0
Release: %{momorel}m%{?dist}
License: GPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: Applications/System
Summary: vje-support package
Requires: vje-delta = %{version}-%{vjerel}
BuildArchitectures: noarch

%description
vje-support package

%prep

%install
mkdir -p %{buildroot}/%{ximdir}

cat <<END > %{buildroot}/%{_xinputconf}
XIM=vje
XIM_PROGRAM="/usr/local/bin/vje"
XIM_ARGS=""
SHORT_DESC="VJE-Delta"
XMODIFIERS=@im=vje
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
END

mkdir -p %{buildroot}%{ximbindir}

ln -s ../vje30/bin/vadd %{buildroot}%{ximbindir}/vadd
ln -s ../vje30/bin/vdel %{buildroot}%{ximbindir}/vdel
ln -s ../vje30/bin/vdispd %{buildroot}%{ximbindir}/vdispd
ln -s ../vje30/bin/vje %{buildroot}%{ximbindir}/vje
ln -s ../vje30/bin/vjeacc %{buildroot}%{ximbindir}/vjeacc
ln -s ../vje30/bin/vjed %{buildroot}%{ximbindir}/vjed
ln -s ../vje30/bin/vjekill %{buildroot}%{ximbindir}/vjekill
ln -s ../vje30/bin/vmaked %{buildroot}%{ximbindir}/vmaked
ln -s ../vje30/bin/vpen %{buildroot}%{ximbindir}/vpen
ln -s ../vje30/bin/vpr %{buildroot}%{ximbindir}/vpr
ln -s ../vje30/bin/vprc %{buildroot}%{ximbindir}/vprc
ln -s ../vje30/bin/vpu %{buildroot}%{ximbindir}/vpu

%post
/etc/init.d/vje30 stop
sed -e s.\ \;touch\ /var/lock/subsys/vje30.. \
    -e s.\ \;rm\ -f\ /var/lock/subsys/vje30\;killall\ vjed.. /etc/init.d/vje30 | \
sed -e s.Start\".Start\"\ \;touch\ /var/lock/subsys/vje30. \
    -e s.Stop\".Stop\"\ \;rm\ -f\ /var/lock/subsys/vje30\;killall\ vjed. \
    > /tmp/vje30.new
mv /tmp/vje30.new /etc/init.d/vje30
chmod +x /etc/init.d/vje30

%postun
/etc/init.d/vje30 stop
sed -e s.\ \;touch\ /var/lock/subsys/vje30.. \
    -e s.\ \;rm\ -f\ /var/lock/subsys/vje30\;killall\ vjed.. /etc/init.d/vje30 > /tmp/vje30.new
mv /tmp/vje30.new /etc/init.d/vje30
chmod +x /etc/init.d/vje30

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%config %{_xinputconf}
%{ximbindir}/vadd
%{ximbindir}/vdel
%{ximbindir}/vdispd
%{ximbindir}/vje
%{ximbindir}/vjeacc
%{ximbindir}/vjed
%{ximbindir}/vjekill
%{ximbindir}/vmaked
%{ximbindir}/vpen
%{ximbindir}/vpr
%{ximbindir}/vprc
%{ximbindir}/vpu

%changelog
* Mon Aug 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-10m)
- kiss the new system

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-4m)
- rebuild against gcc43

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-3m)
- add GTK_IM_MODULE and QT_IM_MODULE to %%{ximdir}/vje

* Thu Oct 12 2000 MATSUDA, Daiki <dyky@df-usa.com>
- fix for FHS

* Sun Mar 26 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fix ( Group, BuildRoot, Summary, description )

* Sat Jan 29 2000 Shingo Akagaki <dora@kondara.org>
- vje-delta 3.0 Released (3.0-0.1)

* Fri Jan 14 2000 Shingo Akagaki <dora@kondara.org>
- support vie-delta-3.0t-0.1

* Wed Dec 08 1999 Shingo Akagaki <dora@kondara.org>
- first release