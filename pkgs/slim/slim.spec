%global momorel 1

Name:           slim
Version:        1.3.5
Release:        %{momorel}m%{?dist}
Summary:        Simple Login Manager

Group:          User Interface/X
License:        GPLv2+
URL:            http://slim.berlios.de/
Source0:        http://download.berlios.de/slim/%{name}-%{version}.tar.gz
NoSource:       0
# stolen from xdm
Source1:        %{name}.pam
# adapted from debian to use freedesktop
Source2:        slim-update_slim_wmlist
Source3:        slim-dynwm
Source4:        slim-momonga.txt
# logrotate entry (see bz#573743)
Source5:        slim.logrotate.d
Source6:        slim-tmpfiles.conf
# Fedora-specific patches
#Patch0:         slim-1.3.2-make.patch
Patch1:         slim-1.3.5-fedora.patch
Patch2:         slim-1.3.2-selinux.patch

Patch10:        slim-1.3.5-unitdir.patch
Patch11:        slim-1.3.5-momonga.patch

BuildRequires:  libXmu-devel libXft-devel libXrender-devel
BuildRequires:  libpng-devel libjpeg-devel >= 8a freetype-devel fontconfig-devel
BuildRequires:  pkgconfig gettext libselinux-devel pam-devel
BuildRequires:  xorg-x11-xwd xterm 
BuildRequires:  cmake >= 2.6.0 ConsoleKit-devel
Requires:       xorg-x11-xwd xterm chkconfig
# we use 'include' in the pam file, so
Requires:       pam >= 0.80
# for anaconda yum
Provides:       service(graphical-login)

%description
SLiM (Simple Login Manager) is a graphical login manager for X11.
It aims to be simple, fast and independent from the various
desktop environments.
SLiM is based on latest stable release of Login.app by Per Liden.

In the distribution, slim may be called through a wrapper, slim-dynwm,
which determines the available window managers using the freedesktop
information and modifies the slim configuration file accordingly,
before launching slim.

%prep
%setup -q
#%%patch0 -p1 -b .make
%patch10 -p1 -b .unit
%patch11 -p1 -b .momonga
%patch1 -p1 -b .fedora
%patch2 -p1 -b .selinux
cp -p %{SOURCE4} README.Momonga

%build
%{__mkdir} -p build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=%{_prefix} \
         -DMANDIR=%{_mandir} \
         -DSYSCONFDIR=%{_sysconfdir} \
         -DUNITDIR=%{_unitdir} \
         -DUSE_PAM:BOOL=ON \
         -DUSE_CONSOLEKIT:BOOL=ON

make %{?_smp_mflags} OPTFLAGS="$RPM_OPT_FLAGS"

%install
cd build
make install DESTDIR=%{buildroot}
install -p -m755 %{SOURCE2} %{buildroot}%{_bindir}/update_slim_wmlist
install -p -m755 %{SOURCE3} %{buildroot}%{_bindir}/slim-dynwm
chmod 0644 %{buildroot}%{_sysconfdir}/slim.conf
install -d -m755 %{buildroot}%{_sysconfdir}/pam.d
install -p -m644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pam.d/slim
mkdir -p %{buildroot}%{_localstatedir}/run/slim
# replace the background image
rm -f %{buildroot}%{_datadir}/slim/themes/default/background.jpg
ln -s ../../../backgrounds/tiles/default_blue.jpg %{buildroot}%{_datadir}/slim/themes/default/background.jpg
# install logrotate entry
install -m0644 -D %{SOURCE5} %{buildroot}/%{_sysconfdir}/logrotate.d/slim
  
install -p -D %{SOURCE6} %{buildroot}%{_sysconfdir}/tmpfiles.d/slim.conf

# do a slim wrapper which updates the window manager list before
# launching slim
cat > $RPM_BUILD_ROOT%{_bindir}/slim-dynwm << EOF
#!/bin/sh
update_slim_wmlist
if [ "x\$1" = "x-nodaemon" ]; then
  shift
  exec slim "\$@"
else
  slim -d "\$@"
fi
EOF

%post
[ -L %{_unitdir}/slim.service ] || rm -f /etc/systemd/system/display-manager.serice
%{_sbindir}/update-alternatives --install display-manager.service \
     %{_unitdir}/slim.service

%postun
[ -e %{_unitdir}/slim.service ] || %{_sbindir}/update-alternatives --remove display-manger.service \
     %{_unitdir}/slim.service

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README* THEMES TODO
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/pam.d/slim
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/slim.conf
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/logrotate.d/slim
%{_bindir}/slim*
%{_bindir}/update_slim_wmlist
%{_mandir}/man1/slim*.1*
%dir %{_datadir}/slim
%{_datadir}/slim/themes/
%config(noreplace) %{_sysconfdir}/tmpfiles.d/slim.conf
%{_unitdir}/slim.service

%changelog
* Tue May 21 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5
- update fedora patch
- add systemd service file patch
- NoSource 0
- add %%post %%postun scripts
- add requires chkconfig
- add build requires ConsoleKit-devel

* Mon Jun 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-8m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-7m)
- do not replace the background image and say good-bye to
  desktop-backgrounds-basic

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-6m)
- rebuild against libjpeg-8a

* Mon Jan  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-5m)
- [SECURITY] CVE-2009-1756
- sync with Rawhide (1.3.1-9)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-3m)
- [SECURITY] https://bugzilla.redhat.com/show_bug.cgi?id=505359
- import patches from Fedora 11 (1.3.1-8)

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-2m)
- rebuild against libjpeg-7

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-1m)
- import from Fedora 11

* Sat Feb 28 2009 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.1-5
- provide service(graphical-login) for anaconda yuminstall (#485789)

* Sun Feb 22 2009 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.1-4
- Add header for remove(3)

* Wed Feb 04 2009 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.1-3
- use small "default_blue" background, instead of large compat "default"

* Wed Oct 15 2008 Marco Pesenti Gritti <mpg@redhat.com>  1.3.1-2
- Enable pam_selinux

* Thu Oct 09 2008 Marco Pesenti Gritti <mpg@redhat.com>  1.3.1-1
- Update to 1.3.1

* Sun Oct 05 2008 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.0-7
- add compat req (#465631)

* Wed Sep 24 2008 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.0-6
- fix patch fuzz

* Fri May 16 2008 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.0-5
- all the images are now in desktop-backgrounds-basic

* Fri Feb 22 2008 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.0-4
- add header for strtol(3)

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.3.0-3
- Autorebuild for GCC 4.3

* Sat Jan 19 2008 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.0-2
- rebuild

* Mon Aug  6 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.3.0-1
- version upgrade

* Mon Aug  6 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.2.6-6
- require system-logos instead of fedora-logos (#250365)

* Tue May 22 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.2.6-5
- make sure to own datadir slim parent too

* Mon May 21 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.2.6-4
- use desktop background, instead of slim
- leave (unused) pam files in the package

* Mon May 14 2007 Anders F Bjorklund <afb@users.sourceforge.net>
- clean up spec file
- correct README user

* Sun May 13 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.2.6-3
- use slim background instead of default
- added more build dependencies / -devel
- add "README.Fedora"
- patch issue display

* Wed May 09 2007 Anders F Bjorklund <afb@users.sourceforge.net>
- clean up spec file
- noreplace slim.conf

* Tue May 08 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.2.6-2
- fixed source URL
- added libXft-devel
- removed xrdb dependency (left from wdm)
- added xwd dependency (for screenshots)

* Sun May 06 2007 Anders F Bjorklund <afb@users.sourceforge.net> 1.2.6-1
- initial package
- adopted wdm spec
