%global momorel 9
# Arches on which the multilib {sysdefs,types}.h hack is needed:
# (Update libast-wrapper.h when adding archs)

%define multilib_arches %{ix86} ia64 ppc ppc64 s390 s390x x86_64
%define cvs 20080502

Summary:        Library of Assorted Spiffy Things
Name:           libast
Version:        0.7.1
Release:        0.6.%{cvs}cvs.%{momorel}m%{?dist}
Group:          System Environment/Libraries
License:        Modified BSD
URL:            http://www.eterm.org/
# Sources are pulled from cvs:
# $ cvs -z3 -d :pserver:anonymous@anoncvs.enlightenment.org:/var/cvs/e \
#      co -d libast-20080502 -D 20080502 eterm/libast
# $ tar czvf libast-20080502.tar.gz libast-20080502
Source:        libast-%{cvs}.tar.gz
Source1:       libast-wrapper.h
BuildRequires: imlib2-devel pcre-devel >= 8.31 libXt-devel
BuildRequires: automake autoconf libtool
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
LibAST is the Library of Assorted Spiffy Things.  It contains various
handy routines and drop-in substitutes for some good-but-non-portable
functions.  It currently has a built-in memory tracking subsystem as
well as some debugging aids and other similar tools.

It's not documented yet, mostly because it's not finished.  Hence the
version number that begins with 0.

%package devel
Summary:  Header files, libraries and development documentation for %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%prep
%setup -q -n %{name}-%{cvs}

%build
./autogen.sh
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install

#find %{buildroot} -name "*.la" -delete

# Fix multiarch stuff
%ifarch %{multilib_arches}
for header in sysdefs types ; do
    mv %{buildroot}%{_includedir}/%{name}/$header.h \
       %{buildroot}%{_includedir}/%{name}/$header-%{_arch}.h
    %ifarch i486 i586 i686
        ln -sf $header-%{_arch}.h %{buildroot}%{_includedir}/%{name}/$header-i386.h
    %endif
    %{__install} -m 0644 -c %{SOURCE1} %{buildroot}%{_includedir}/%{name}/$header.h
    %{__sed} -i -e 's/<HEADER>/'$header'/g' %{buildroot}%{_includedir}/%{name}/$header.h
    touch -r ChangeLog %{buildroot}%{_includedir}/%{name}/$header.h
done
%{__sed} -i -e '/^LDFLAGS=/d' %{buildroot}%{_bindir}/%{name}-config
touch -r ChangeLog %{buildroot}%{_bindir}/%{name}-config
%endif

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog DESIGN README LICENSE
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root, -)
%dir %{_includedir}/%{name}
%{_bindir}/%{name}-config
%{_libdir}/%{name}.so
%{_includedir}/%{name}.h
%{_includedir}/%{name}/*.h
%{_datadir}/aclocal/%{name}.m4
%exclude %{_libdir}/*.la
%exclude %{_libdir}/*.a

%changelog
* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-0.6.20080502cvs.9m)
- rebuild against pcre-8.31

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-0.6.20080502cvs.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-0.6.20080502cvs.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-0.6.20080502cvs.6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-0.6.20080502cvs.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-0.6.20080502cvs.4m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-0.6.20080502cvs.3m)
- [BUILD FIX] add headers for i686

* Tue Sep 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-0.6.20080502.2m)
- fix up %%install and add %%clean

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-0.6.20080502.1m)
- update to cvs version (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-3m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7-2m)
- delete libtool library

* Tue Jan 31 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7-1m)
- [SECURITY] CVE-2006-0224
- update to 0.7

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-6m)
- add gcc4 patch
- Patch2: libast-0.5-gcc4.patch

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-5m)
- suppress AC_DEFUN warning.

* Thu Feb 24 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5-4m)
- role-back to 0.5 (Japanese font problem)

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.1-2m)
- enable x86_64.

* Wed Dec 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.1-1m)
- update to 0.6.1

* Wed May  5 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5-3m)
- rebuild against imlib2-1.1.0
- revise specfile

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.5-2m)
- revised spec for enabling rpm 4.2.

* Thu Oct 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5-1m)
- version 0.5

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-4k)
- add BuildPrereq: imlib2-devel

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-2k)
- create
