%global momorel 1

Summary: A secure replacement for inetd.
Name: xinetd
Version: 2.3.15
Release: %{momorel}m%{?dist}
License: "xinetd" and see "COPYRIGHT"
Group: System Environment/Daemons
URL: http://www.xinetd.org
Source0: http://www.xinetd.org/xinetd-%{version}.tar.gz 
NoSource: 0
Source1: xinetd.service
Source3: xinetd.sysconf

Patch0: xinetd-2.3.15-pie.patch
Patch4: xinetd-2.3.14-bind-ipv6.patch
Patch6: xinetd-2.3.14-man-section.patch
Patch7: xinetd-2.3.15-PIE.patch
Patch8: xinetd-2.3.14-ident-bind.patch
Patch9: xinetd-2.3.14-readable-debuginfo.patch
Patch10: xinetd-2.3.14-autoconf.patch
Patch11: xinetd-2.3.14-poll.patch
Patch12: xinetd-2.3.14-file-limit.patch
Patch13: xinetd-2.3.14-tcpmux.patch
Patch14: xinetd-2.3.14-clean-pfd.patch
Patch15: xinetd-2.3.14-ipv6confusion.patch
Patch16: xinetd-2.3.14-udp-reconfig.patch
Patch18: xinetd-2.3.14-rpc-specific-port.patch
Patch19: xinetd-2.3.14-signal-log-hang.patch
Patch20: xinetd-2.3.14-fix-type-punned-ptr.patch
Patch21: xinetd-2.3.14-leaking-fds.patch
Patch22: xinetd-2.3.14-many-services.patch
Patch23: xinetd-2.3.14-realloc-remove.patch
Patch24: xinetd-2.3.14-leaking-fds-2a.patch
Patch25: xinetd-2.3.14-instances.patch
Patch26: xinetd-2.3.14-retry-svc-activate-in-cps-restart.patch
Patch27: xinetd-2.3.15-bad-port-check.patch

BuildRequires: autoconf, automake
BuildRequires: libselinux-devel >= 1.30
BuildRequires: systemd-units
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%{!?tcp_wrappers:BuildRequires: tcp_wrappers-devel}
Requires: filesystem >= 2.0.1, initscripts, setup, fileutils
Provides: inetd
Requires: filesystem >= 2.0.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: inetd, netkit-base

%description
Xinetd is a secure replacement for inetd, the Internet services
daemon. Xinetd provides access control for all services based on the
address of the remote host and/or on time of access and can prevent
denial-of-access attacks. Xinetd provides extensive logging, has no
limit on the number of server arguments, and lets you bind specific
services to specific IP addresses on your host machine. Each service
has its own specific configuration file for Xinetd; the files are
located in the /etc/xinetd.d directory.


%prep
%setup -q
# SPARC/SPARC64 needs -fPIE/-PIE
# This really should be detected by configure.
%ifarch sparcv9 sparc64
%patch7 -p1 -b .PIE
%else
%patch0 -p1 -b .pie
%endif
%patch4 -p1 -b .bind
%patch6 -p1 -b .man-section
%patch8 -p1 -b .ident-bind
%patch9 -p1 -b .readable-debuginfo
%patch10 -p1 -b .autoconf
%patch11 -p1 -b .poll
%patch12 -p1 -b .file-limit
%patch13 -p1 -b .tcpmux
%patch14 -p1 -b .clean-pfd
%patch15 -p1 -b .ipv6confusion
%patch16 -p1 -b .udp-reconfig
%patch18 -p1 -b .rpc-specific-port
%patch19 -p1 -b .signal-log-hang
%patch20 -p1 -b .fix-type-punned-ptr
%patch21 -p1 -b .leaking-fds
%patch22 -p1 -b .many-services
%patch23 -p1 -b .realloc-remove
%patch24 -p1 -b .leaking-fds-2a
%patch25 -p1 -b .instances
%patch26 -p1 -b .retry-svc-activate
%patch27 -p1 -b .bad-port-check

aclocal
autoconf

%build
%configure --with-loadavg --with-inet6 %{!?tcp_wrappers:--with-libwrap} --with-labeled-networking
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT/etc/xinetd.d/
# Remove unneeded service
rm -f contrib/xinetd.d/ftp-sensor
%makeinstall DAEMONDIR=$RPM_BUILD_ROOT/usr/sbin MANDIR=$RPM_BUILD_ROOT/%{_mandir}
install -m 644 contrib/xinetd.conf $RPM_BUILD_ROOT/etc
install -m 644 contrib/xinetd.d/* $RPM_BUILD_ROOT/etc/xinetd.d
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}

rm -f $RPM_BUILD_ROOT/%{_mandir}/man8/itox*
rm -f $RPM_BUILD_ROOT/usr/sbin/itox
rm -f $RPM_BUILD_ROOT/%{_mandir}/man8/xconv.pl*
rm -f $RPM_BUILD_ROOT/usr/sbin/xconv.pl

mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
install -m 644 %SOURCE3 $RPM_BUILD_ROOT/etc/sysconfig/xinetd

%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation
    /bin/systemctl enable xinetd.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable xinetd.service > /dev/null 2>&1 || :
    /bin/systemctl stop xinetd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart xinetd.service >/dev/null 2>&1 || :
fi

%triggerun -- xinetd < 2.3.14-9m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply xinetd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save xinetd >/dev/null 2>&1 ||:
/bin/systemctl --no-reload enable xinetd.service >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del xinetd >/dev/null 2>&1 || :
/bin/systemctl try-restart xinetd.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc INSTALL CHANGELOG COPYRIGHT README xinetd/sample.conf contrib/empty.conf 
%config(noreplace) /etc/xinetd.conf
%config(noreplace) /etc/sysconfig/xinetd
%config(noreplace) /etc/xinetd.d/*
%{_unitdir}/xinetd.service
%{_sbindir}/xinetd
%{_mandir}/*/*

%changelog
* Sat Jun 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.15-1m)
- [SECURITY] CVE-2012-0862
- update to 2.3.15

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.14-9m)
-support systemd

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.14-8m)
- add patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.14-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.14-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.14-5m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.14-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.14-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.14-2m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.14-1m)
- update to 2.3.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.13-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.13-3m)
- %%NoSource -> NoSource

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.13-2m)
- add gcc4 patch. import from FC
- Patch0: xinetd-2.3.13-gcc4.patch

* Wed Feb  4 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.3.13-1m)
- update to 2.3.13
- add BuildRequires: tcp_wrappers >= 7.6

* Fri Dec 19 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.12-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Oct 24 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.3.12-3m)
- fix License: preamble

* Sun Aug 31 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.3.12-2m)
- Change License tag: Distributable -> "xinetd"
  see COPYRIGHT file

* Thu Aug 21 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.3.12-1m)
- update to 2.3.12
- use %%{momorel} macro
- use %%NoSource, %%make
- fix CHANGELOG, COPYRIGHT and add TODO in %%doc

* Thu Apr 17 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.3.11-1m)
  update to 2.3.11

* Fri Feb 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.3.10-2m)
  not depend xinetdconf

* Fri Jan 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.10-1m)
- major bugfixes
- cancel 'Requires: python'

* Sat Nov 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.9-2m)
- add Requires: python

* Thu Sep 26 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.9-1m)
- minor bugfixes

* Wed Sep 18 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.8-1m)
- update to 2.3.8
- remove --with-inet6 from configure options
  IPv6 is no longer listened by default.
  If you intend to use IPv6 socket, add "IPv6" to service flag
  in each cofiguration file.

* Wed Aug 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.7-1m)
- minor bugfixes

* Tue Aug  6 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.3.6-1m)
  update to 2.3.6

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.3.4-6k)
- Prereq: /sbin/chkconfig -> chkconfig

* Mon Apr  8 2002 Toru Hoshina <t@kondara.org>
- (2.3.4-4k)
- inetdconvert is not necessary.

* Mon Apr  1 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.3.4-2k)
  update to 2.3.4

* Tue Jan  8 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.3-14k)
- rename echo-udp to echo-uecho for xinetconf

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (2.3.3-10k)
- nigirisugi.txt
- revised spec file. /etc/xinetd.d is owned by filesystem at this time.

* Fri Sep 21 2001 Toru Hoshina <t@kondara.org>
- (2.3.3-10k)
- require xinetdconf.
- fix /etc/xinetd.d/time etc.

* Sun Sep  9 2001 MATAUDA, Daiki <dyky@df-usa.com>
- (2.3.3-6k)
- separate xinetd and xinetdconf packages

* Thu Aug 30 2001 Motonobu Ichimura <famao@kondara.org>
- (2.3.3-3k)
- up to 2.3.3 (another bugfix release)

* Thu Aug 30 2001 Motonobu Ichimura <famao@kondara.org>
- (2.3.2-3k)
- up to 2.3.2
- modify xinetdconf (keep up with original behavior)

* Fri Jul 13 2001 Motonobu Ichimura <famao@kondara.org>
- (2.3.0-3k)
- import to Jirai

* Fri Jul 13 2001 Motonobu Ichimura <famao@kondara.org>
- (2.3.0-2k)
- update (include bugfix)

* Sun Jun 17 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.8.9-0.0015002k)
- pre15.

* Sun May 20 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.1.8.9-0.0014008k)
- modified xinetconf not to read *.rpmorig and *.rpmsave files

* Sat May 19 2001 Motonobu Ichimura <famao@kondara.org>
- (2.1.8.9-0.0014006k)
- sync with rawhide. (fixed EFAULT error)

* Wed May  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.1.8.9-0.0014004k)
- modified xinetconf to pick up service in comment

* Fri Mar 02 2001 Motonobu Ichimura <famao@kondara.org>
- (2.1.8.9-0.0014003k)
- up to 2.1.8.9pre14
- remove v6config patch. (now xinetd already includes this)
- (plan) remove tcp_wrapper support in the future release?

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.1.8.9-0.0013006k)
- added /usr/sbin/xinetdconf

* Tue Nov 28 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}

* Wed Nov 15 2000 Motonobu Ichimura <famao@kondara.org>
- up to pre13
- remove v4handle,addr patch 'cause these are now in main tree
- added v6config patch ( this patch enable "v6only = yes" in config files )

* Tue Nov 14 2000 Motonobu Ichimura <famao@kondara.org>
- up to pre12 and added some patch

* Fri Nov 10 2000 Akira Higuchi <a@kondara.org>
- set sin_family AF_INET for v4-mapped v6 connections.

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- Kondarized.

* Tue Jul 25 2000 Bill Nottingham <notting@redhat.com>
- um, we *need* to prereq /etc/init.d

* Mon Jul 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Don't require /etc/init.d

* Sat Jul 22 2000 Bill Nottingham <notting@redhat.com>
- rebuild

* Tue Jul 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fix the sections of the man pages (#14244)

* Tue Jul 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- remove itox, as it wouldn't do the right thing with our
  configuration
- same with xconv.pl
- some changes to the installation process

* Mon Jul 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
- move initscript back to /etc/rc.d/init.d

* Fri Jul 14 2000 Trond Eivind Glomsrod <teg@redhat.com>
- change process name in init file

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jul  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- start the daemon with the "-reuse" flag

* Thu Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- "Prereq:", not "Requires:" for /etc/init.d

* Wed Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- require /etc/init.d

* Wed Jul  5 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- upper the number of instances to 60

* Sun Jul  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix a memory-allocation bug

* Wed Jun 28 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.8.9pre8

* Wed Jun 21 2000 Trond Eivind Glomsrod <teg@redhat.com>
- moved to /etc/init.d

* Wed Jun 21 2000 Trond Eivind Glomsrod <teg@redhat.com>
- changed specfile and initfile to implement conditional
  restart

* Sun Jun 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.8.9pre7
- now obsoletes inetd
- use %%{_tmppath}

* Sun Jun 04 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.8.9pre6
- added converter script which can convert specified or 
  remaing uncoverted services
- use %%{_mandir}
- removed +x on xinetd.conf

* Wed May 24 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.8.9pre4
- authpriv patch no longer needed

* Tue May 23 2000 Trond Eivind Glomsrod <teg@redhat.com>
- /etc/xinetd.d is now part of the filesystem package
- more fixes to xinetd.init

* Mon May 22 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fixed some obvious bugs in xinetd.init
- added a default xinetd.conf
- patched xinetd to understand LOG_AUTHPRIV

* Fri May 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- updated version
- removed a define %ver (we already have %version)
- removed some extra CFLAGS declarations
- added configuration directory, /etc/xinetd.d

* Mon Feb 21 2000 Tim Powers <timp@redhat.com>
- fixed broken postun sections, should have been *preun*
- fixed broken gzip of manpages

* Wed Jan 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.1.8.8p8
- Fix the init script (Bug #7277)
- remove our patches (no longer required)

* Tue Sep 21 1999 Bill Nottingham <notting@redhat.com>
- add -lnsl

* Tue Sep 7 1999 Tim Powers <timp@redhat.com>
- modification top install routine

* Mon Jul 26 1999 Tim Powers <timp@redhat.com>
- updated source to 2.1.8.6b6
- built for 6.1

* Mon Apr 26 1999 Bill Nottingham <notting@redhat.com>
- update to 2.1.8.6b5
- build for PowerTools

* Mon Jan 10 1999 Bill Nottingham <notting@redhat.com>
- update to 2.1.8.5p2

* Tue Dec  1 1998 Bill Nottingham <notting@redhat.com>
- intial build
