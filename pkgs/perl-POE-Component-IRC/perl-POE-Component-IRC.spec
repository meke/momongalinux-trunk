%global         momorel 1

Name:           perl-POE-Component-IRC
Version:        6.88
Release:        %{momorel}m%{?dist}
Summary:        Fully event-driven IRC client module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/POE-Component-IRC/
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/POE-Component-IRC-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IRC-Utils
BuildRequires:  perl-Object-Pluggable
BuildRequires:  perl-POE >= 1.287
BuildRequires:  perl-POE-Component-Client-DNS >= 0.99
BuildRequires:  perl-POE-Component-Client-Ident
BuildRequires:  perl-POE-Component-Syndicator
BuildRequires:  perl-POE-Filter-IRCD >= 2.42
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-IRC-Utils
Requires:       perl-Object-Pluggable
Requires:       perl-POE >= 1.287
Requires:       perl-POE-Component-Client-DNS >= 0.99
Requires:       perl-POE-Component-Client-Ident
Requires:       perl-POE-Component-Syndicator
Requires:       perl-POE-Filter-IRCD >= 2.42
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
POE::Component::IRC is a POE component (who'd have guessed?) which acts as
an easily controllable IRC client for your other POE components and
sessions. You create an IRC component and tell it what events your session
cares about and where to connect to, and it sends back interesting IRC
events when they happen. You make the client do things by sending it
events. That's all there is to it. Cool, no?

%prep
%setup -q -n POE-Component-IRC-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/POE/Component/*
%{perl_vendorlib}/POE/Filter/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.88-1m)
- rebuild against perl-5.20.0
- update to 6.88

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.83-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.83-2m)
- rebuild against perl-5.18.1

* Tue May 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.83-1m)
- update to 6.83

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.82-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.82-2m)
- rebuild against perl-5.16.3

* Sun Mar 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.82-1m)
- update to 6.82

* Sat Nov 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.81-1m)
- update to 6.81

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.80-2m)
- rebuild against perl-5.16.2

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.80-1m)
- update to 6.80

* Thu Sep 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.79-1m)
- update to 6.79

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.78-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.78-2m)
- rebuild against perl-5.16.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.78-1m)
- update to 6.78

* Fri Dec  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.77-1m)
- update to 6.77

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.76-1m)
- update to 6.76

* Mon Nov 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.75-1m)
- update to 6.75

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.74-1m)
- update to 6.74

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.73-1m)
- update to 6.73

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.72-1m)
- update to 6.72

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.71-2m)
- rebuild against perl-5.14.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.71-1m)
- update to 6.71

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.70-1m)
- update to 6.70

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.68-2m)
- rebuild against perl-5.14.1

* Mon May 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.68-1m)
- update to 6.68

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.66-1m)
- update to 6.66

* Thu May 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.65-1m)
- update to 6.65

* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.64-1m)
- update to 6.64

* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.63-1m)
- update to 6.63

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.62-2m)
- rebuild against perl-5.14.0-0.2.1m

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.62-1m)
- update to 6.62

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.61-1m)
- update to 6.61

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.59-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.59-1m)
- update to 6.59

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.57-1m)
- update to 6.57

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.56-1m)
- update to 6.56

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.54-1m)
- update to 6.54

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.52-2m)
- rebuild for new GCC 4.5

* Sat Nov  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.52-1m)
- update to 6.52

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.50-1m)
- update to 6.50

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.49-1m)
- update to 6.49

* Mon Oct  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.48-1m)
- update to 6.48

* Wed Sep 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.46-1m)
- update to 6.46
- add BuildRequires: and Requires: perl-Object-Pluggable

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.45-1m)
- update to 6.45
- rebuild against perl-5.12.2

* Sat Sep 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.42-1m)
- update to 6.42

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.41-1m)
- update to 6.41

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.40-1m)
- update to 6.40

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.36-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.36-1m)
- update to 6.36

* Sat Jun 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.34-1m)
- update to 6.34

* Tue Jun 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.33-1m)
- update to 6.33

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.32-2m)
- rebuild against perl-5.12.1

* Wed May 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.32-1m)
- [SECURITY] http://cpansearch.perl.org/src/HINRIK/POE-Component-IRC-6.32/Changes
- update to 6.32

* Tue May 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.30-1m)
- update to 6.30

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.28-2m)
- rebuild against perl-5.12.0

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.28-1m)
- update to 6.28

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.26-1m)
- update to 6.26

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.24-1m)
- update to 6.24

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.22-1m)
- update to 6.22

* Sun Jan 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.20-1m)
- update to 6.20
- Specfile re-generated by cpanspec 1.78.

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.18-1m)
- update to 6.18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.16-1m)
- update to 6.16

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.14-1m)
- update to 6.14

* Fri Sep 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.12-1m)
- update to 6.12

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.10-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.10-1m)
- update to 6.10

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.08-1m)
- update to 6.08

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.06-1m)
- update to 6.06

* Sun Mar  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.04-1m)
- update to 6.04

* Wed Mar  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.98-1m)
- update to 5.98

* Tue Feb 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.96-1m)
- update to 5.96

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.88-2m)
- rebuild against rpm-4.6

* Fri Aug 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.88-1m)
- update to 5.88

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.86-1m)
- update to 5.86

* Fri Jun 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.84-1m)
- update to 5.84
- add BuildRequires: and Requires: perl-POE-Component-Pluggable >= 1.10

* Sat Jun 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.82-1m)
- update to 5.82

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80-1m)
- update to 5.80

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.78-1m)
- update to 5.78

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.76-1m)
- update to 5.76

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.72-2m)
- rebuild against gcc43

* Sun Mar 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.72-1m)
- update to 5.72

* Tue Mar  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.70-1m)
- update to 5.70

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.68-1m)
- update to 5.68

* Wed Feb 20 2008 NARITA Koichi <pulasr@momonga-linux.org>
- (5.66-1m)
- update to 5.66

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.64-1m)
- update to 5.64

* Sun Feb 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.62-1m)
- update to 5.62

* Fri Feb  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.56-1m)
- update to 5.56

* Sun Jan 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.54-1m)
- update to 5.54

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.52-1m)
- update to 5.52

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.50-1m)
- update to 5.50

* Fri Jan  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.46-1m)
- update to 5.46

* Wed Jan  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.44-1m)
- update to 5.44

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.42-1m)
- update to 5.42

* Wed Dec 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.40-1m)
- update to 5.40

* Fri Dec  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.38-1m)
- update to 5.38

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.36-1m)
- update to 5.36

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.34-1m)
- update to 5.34

* Wed Jun 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.32-1m)
- update to 5.32

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.30-1m)
- update to 5.30

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.24-2m)
- use vendor

* Fri Apr 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.24-1m)
- update to 5.24

* Fri Apr 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.23-1m)
- update to 5.23

* Sat Feb  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.22-1m)
- update to 5.22

* Sat Dec 30 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.18-1m)
- update to 5.18

* Fri Dec 15 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.17-1m)
- update to 5.17

* Tue Dec 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.16-1m)
- update to 5.16

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.14-1m)
- update to 5.14

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.12-1m)
- update to 5.12

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11-1m)
- update to 5.11

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.04-1m)
- spec file was autogenerated
