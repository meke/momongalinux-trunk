%global momorel 1

%global sennahome /var/senna
%global charset utf-8
%global sfid 46945

Summary: Senna is an embeddable fulltext search engine
Name: senna
Version: 1.1.5
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Applications/Text
URL: http://qwik.jp/senna/
Source0: http://dl.sourceforge.jp/%{name}/%{sfid}/%{name}-%{version}.tar.gz
NoSource: 0
Source10: senna.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mecab-devel >= 0.99-1m
Requires: mecab

%description
Senna is an embeddable fulltext search engine, which you can use in
conjunction with various scripting languages and databases. Senna is
an inverted index based engine, and combines the best of n-gram
indexing and word indexing to achieve fast, precise searches. While
senna codebase is rather compact it is scalable enough to handle large
amounts of data and queries.

%package devel
Summary: Libraries and header files for Senna
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
Libraries and header files for Senna

%prep
%setup -q

%build
export CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
export CXXFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
%configure --with-charset=%{charset} 
SENNNA_HOME=%{sennahome} make

%install
%makeinstall 

%{__install} -d 755 %{buildroot}%{sennahome}
%{__install} -m 644 %{SOURCE10} %{buildroot}%{sennahome}

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig -n %{_libdir}

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README doc/
%{_libdir}/*.so.*
%{_bindir}/*
%config %{sennahome}/senna.conf

%files devel
%defattr(-, root, root)
%{_includedir}/senna
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5
- rebuild against mecab-0.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Mar 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-2m)
- rebuild against gcc43

* Thu Feb 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update

* Sat Feb  2 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- update

* Mon Oct  8 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-1m)
- update

* Thu Jul  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-1m)
- update

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.90-3m)
- delete libtool library

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.90-2m)
- rebuild against mecab-0.91-1m and new SVN version

* Fri Mar 3 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.90-1m)
- initial release

