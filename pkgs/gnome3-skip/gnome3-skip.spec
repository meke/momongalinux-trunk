%global momorel 6

Summary: Meta package for GNOME Obsoleted
Name: gnome3-skip
Version: 3.2.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Obsoletes: byzanz
Obsoletes: deskbar-applet deskbar-applet-devel
Obsoletes: fast-user-switch-applet
Obsoletes: glipper
Obsoletes: gnome-applet-vm
Obsoletes: gnome-mount gnome-mount-devel
Obsoletes: gnome-netstatus gnome-netstatus-devel
Obsoletes: gnubversion
Obsoletes: libgail-gnome libgail-gnome-devel
Obsoletes: libpanelappletmm libpanelappletmm-devel
Obsoletes: quick-lounge-applet
Obsoletes: sensors-applet sensors-applet-devel
Obsoletes: sound-juicer
Obsoletes: seahorse-plugins seahorse-plugins-devel
Obsoletes: xfce4-xfapplet-plugin

# does not work
Obsoletes: gnome-do gnome-do-devel

%description
GNOME 3.0 obsoletes many packages.

%files

%changelog
* Sat Apr  7 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-6m)
- come back gnome-applets

* Tue Oct 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-5m)
- come back shotwell

* Sat Oct  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-4m)
- come back epiphany-extensions

* Sat Oct  1 2011 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-3m)
- delete epiphany-extensions

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-2m)
- come back valadoc

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- come back gnote

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- delete shotwell

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-2m)
- comeback anjuta-extra

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- delete valadoc (for a while)

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-12m)
- delete gnome-do gnome-do-devel

* Thu May 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-13m)
- fix up ineffective Obsoletes

* Wed May 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-12m)
- delete xfce4-xfapplet-plugin
-- it is critical delete!!! please fix XFCE4

* Wed May 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-11m)
- delete quick-lounge-applet sensors-applet sound-juicer

* Tue May 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-10m)
- delete nautilus-image-converter

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-9m)
- delete glipper gnome-applet-vm

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-8m)
- delete gnubversion

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-7m)
- delete gnome-mount

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-6m)
- delete gnote libpanelappletmm

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-5m)
- delete fast-user-switch-applet

* Sun May  8 2011 Nishio Futosh <fotoshi@momonga-linux.org>
- (3.0.1-4m)
- delete byzanz

* Sun May  8 2011 Nishio Futosh <fotoshi@momonga-linux.org>
- (3.0.1-3m)
- delete anjuta-estras

* Sat May  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- delete dupulicated and sort Obso

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- initial build
