%global momorel 13
%global ocamlver 3.12.1

%global debug_package %{nil}

Summary: FastCGI and CGI library
Name: ocaml-camlgi
Version: 0.6
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://sourceforge.net/projects/ocaml-cgi/
Source0: http://dl.sourceforge.net/sourceforge/ocaml-cgi/CamlGI-0.6.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-camlp4-devel >= %{ocamlver}
BuildRequires: ocaml-findlib >= 1.2.1-2m

%description
CamlGI is a library to enable you to write CGI and FastCGI in OCaml. It is
written 100% in OCaml so should run on many platforms. The library supports
multiple simultaneous connections and request multiplexing while presenting an
easy to use interface.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%setup -q -n CamlGI-%{version}

%build
%{__make} PREFIX=%{_prefix}
%{__make} doc

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_libdir}/ocaml
%{__make} install OCAMLFIND_DESTDIR="-destdir %{buildroot}%{_libdir}/ocaml"
%{__install} -m 644 camlGI.a %{buildroot}%{_libdir}/ocaml/CamlGI
%{__install} -m 644 camlGI.cmx %{buildroot}%{_libdir}/ocaml/CamlGI

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE README
%dir %{_libdir}/ocaml/CamlGI
%{_libdir}/ocaml/CamlGI/META
%{_libdir}/ocaml/CamlGI/camlGI.cma
%{_libdir}/ocaml/CamlGI/camlGI.cmi

%files devel
%defattr(-,root,root)
%doc html
%{_libdir}/ocaml/CamlGI/camlGI.a
%{_libdir}/ocaml/CamlGI/camlGI.cmx
%{_libdir}/ocaml/CamlGI/camlGI.cmxa
%{_libdir}/ocaml/CamlGI/camlGI.mli

%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-13m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-10m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-9m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-7m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-6m)
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-5m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-4m)
- rebuild against ocaml-3.10.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-3m)
- %%NoSource -> NoSource
 
* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- rebuild against ocaml-3.10.1-1m

* Tue Nov 27 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-1m)
- initial packaging
