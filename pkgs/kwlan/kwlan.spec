%global momorel 15
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Network manager for kde
Name: kwlan
Version: 0.6.3
Release: %{momorel}m%{?dist}
License: GPLv2 and BSD
Group: Applications/System
URL: http://www.kde-apps.org/content/show.php?content=37041
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: admin.tar.bz2
Patch0: %{name}-0.6.1-desktop.patch
Patch1: %{name}-%{version}-make-doc.patch
Patch2: %{name}-%{version}-automake111.patch
Patch3: %{name}-%{version}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: wpa_supplicant
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: wireless-tools >= 29

%description
Network manager for kde.
 
 Allows you to configure different network profiles using all encryptions wpa_supplicant provides (wep, wpa, wpa2 etc) for wireless networks. 
 Since version 0.4.5 kwlan can connect to unencrypted networks if wpa_supplicant is not available. 
 
 Kwlan can also store profiles for wired networks.
 
 Dialup networks can be edited and connected to as well.
 
 Systray icons show connection statistics per interface (can be disabled).

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .make-doc

# fix for new automake
rm -rf admin
tar xf %{SOURCE1}

%patch2 -p1 -b .automake111
%patch3 -p1 -b .autoconf265

# for docs
make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --remove-category Network \
  --add-category System \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

# clean up
rm -rf %{buildroot}%{_docdir}/HTML/en/doc

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/%{name}
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-13m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-12m)
- move kwlan.desktop from Network to System on menu

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-11m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-10m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-8m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-7m)
- rebuild against rpm-4.6

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-6m)
- revise %%{_docdir}/HTML/*/kwlan/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.3-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-2m)
- %%NoSource -> NoSource

* Mon Dec 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-1m)
- version 0.6.3
- add make-doc.patch
- BPR wireless-tools >= 29

* Sat Jun 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-1m)
- version 0.6.2

* Thu May 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-3m)
- update desktop.patch, GenericName=WPA Wireless Lan Manager

* Thu May  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-2m)
- change "Categories" of kwlan.desktop from "System" to "Network"

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-1m)
- initial package for Momonga Linux
