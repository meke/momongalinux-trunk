%global momorel 7
%global ocamlver 3.12.1

%global debug_package %{nil}

Summary: LablGL is an OpenGL interface for Objective Caml
Name: ocaml-lablgl
Version: 1.04
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: Modified BSD
URL: http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/lablgl.html
Source0: http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/dist/lablgl-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: lablgl = %{version}-%{release}
Obsoletes: lablgl
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-labltk-devel >= %{ocamlver}
BuildRequires: ocaml-camlp4-devel >= %{ocamlver}
BuildRequires: tcl-devel
BuildRequires: tk-devel
BuildRequires: freeglut-devel 
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXmu-devel
BuildRequires: libXxf86vm-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel

%description
LablGL is is an Objective Caml interface to OpenGL. Support is
included for use inside LablTk, and LablGTK also includes specific
support for LablGL.  It can be used either with proprietary OpenGL
implementations (SGI, Digital Unix, Solaris...), with XFree86 GLX
extension, or with open-source Mesa.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ocaml-labltk-devel
Provides: lablgl = %{version}-%{release}
Obsoletes: lablgl

%description devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%setup -q -n lablGL-%{version}

%build
cat > Makefile.config <<EOF
%ifnarch ppc64
CAMLC = ocamlc.opt
CAMLOPT = ocamlopt.opt
%else
CAMLC = ocamlc
%endif
BINDIR = %{_bindir}
XINCLUDES = -I%{_includedir}
XLIBS = -L%{_libdir} -lXext -lXmu -lX11
TKINCLUDES = -I%{_includedir}
GLINCLUDES =
GLLIBS = -lGL -lGLU
GLUTLIBS = -lglut -lXxf86vm
RANLIB = :
LIBDIR = %{_libdir}/ocaml
DLLDIR = %{_libdir}/ocaml/stublibs
INSTALLDIR = %{_libdir}/ocaml/lablGL
TOGLDIR=Togl
COPTS = $RPM_OPT_FLAGS
EOF
%ifnarch ppc64
make all opt
%else
make all
%endif

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__mkdir_p %{buildroot}%{_libdir}/ocaml/lablGL
%__mkdir_p %{buildroot}%{_libdir}/ocaml/stublibs
make INSTALLDIR=%{buildroot}%{_libdir}/ocaml/lablGL \
     DLLDIR=%{buildroot}%{_libdir}/ocaml/stublibs \
     BINDIR=%{buildroot}%{_bindir} \
     install

# Make and install a META file.
cat <<EOM >META
version="%{version}"
directory="+lablgl"
archive(byte) = "lablgl.cma"
archive(native) = "lablgl.cmxa"

package "togl" (
  requires = "labltk lablgl"
  archive(byte) = "togl.cma"
  archive(native) = "togl.cmxa"
)

package "glut" (
  requires = "lablgl"
  archive(byte) = "lablglut.cma"
  archive(native) = "lablglut.cmxa"
)
EOM
cp META %{buildroot}%{_libdir}/ocaml/lablGL

# Remove unnecessary *.ml files (ones which have a *.mli).
pushd %{buildroot}%{_libdir}/ocaml/lablGL
for f in *.ml; do \
  b=`basename $f .ml`; \
  if [ -f "$b.mli" ]; then \
    rm $f; \
  fi; \
done
popd

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%dir %{_libdir}/ocaml/lablGL
%{_libdir}/ocaml/lablGL/*.cma
%{_libdir}/ocaml/lablGL/*.cmi
%{_libdir}/ocaml/stublibs/*.so
%{_bindir}/lablgl
%{_bindir}/lablglut

%files devel
%defattr(-,root,root,-)
%doc CHANGES COPYRIGHT README LablGlut/examples Togl/examples
%{_libdir}/ocaml/lablGL/META
%{_libdir}/ocaml/lablGL/*.a
%{_libdir}/ocaml/lablGL/*.cmxa
%{_libdir}/ocaml/lablGL/*.cmx
%{_libdir}/ocaml/lablGL/*.mli
%{_libdir}/ocaml/lablGL/build.ml

%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.04-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-3m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-8m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-7m)
- rebuild against ocaml-3.11.0

* Sat Apr 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-6m)
- import `Patch0: lablgl-tk8.5.patch' from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03-5m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-4m)
- rebuild against ocaml-3.10.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-3m)
- %%NoSource -> NoSource

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-2m)
- rebuild against ocaml-3.10.1-1m

* Mon Dec 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Mon Oct  1 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-5m)
- disable debug_package

* Sat Sep 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-4m)
- rename to ocaml-lablgl and split packages
- rebuild against ocaml-3.10.0-1m
- sync with Fedora devel
-- * Sat Jul  7 2007 Gerard Milmeister <gemi@bluewin.ch> - 1.02-12
-- - exclude arch ppc64
-- 
-- * Sat Jul  7 2007 Gerard Milmeister <gemi@bluewin.ch> - 1.02-11
-- - added buildreq ocaml-camlp4-devel
-- 
-- * Fri Jul  6 2007 Gerard Milmeister <gemi@bluewin.ch> - 1.02-10
-- - renamed package from lablgl to ocaml-lablgl

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.02-3m)
- disable opt for ppc64

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-2m)
- rebuild against freeglut

* Wed Jul 12 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-1m)
- import from FC-5

* Wed May 10 2006 Gerard Milmeister <gemi@bluewin.ch> - 1.02-7
- rebuilt for ocaml 3.09.2

* Sun Feb 26 2006 Gerard Milmeister <gemi@bluewin.ch> - 1.02-4
- Rebuild for ocaml 3.09.1

* Sat Feb 25 2006 Gerard Milmeister <gemi@bluewin.ch> - 1.02-3
- Rebuild for Fedora Extras 5

* Tue Nov  1 2005 Gerard Milmeister <gemi@bluewin.ch> - 1.02-2
- build opt libraries

* Tue Nov  1 2005 Gerard Milmeister <gemi@bluewin.ch> - 1.02-1
- New Version 1.02

* Sun Sep 11 2005 Gerard Milmeister <gemi@bluewin.ch> - 1.01-7
- Rebuild with new ocaml

* Thu May 26 2005 Toshio Kuratomi <toshio-tiki-lounge.com> - 1.01-6
- Bump and rebuild with new ocaml.
  
* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 1.01-5
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Fri Apr  1 2005 Gerard Milmeister <gemi@bluewin.ch> - 1.01-3
- Rebuild for ocaml 3.08.3

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 0:1.01-2
- Removed %{_smp_mflags} as it breaks the build

* Thu Aug 19 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:1.01-0.fdr.1
- New Version 1.01

* Mon Dec  1 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:1.00-0.fdr.4
- Patch to used GL/freeglut.h instead of GL/glut.h
- Add BuildRequires for labltk

* Fri Nov 28 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:1.00-0.fdr.3
- Add BuildRequires for camlp4
