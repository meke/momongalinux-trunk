%global momorel 15
Summary: Terminal Emulator With No Privilege
Name: libspt
Version: 1.1
Release: %{momorel}m%{?dist}
Source0: http://www.j10n.org/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-makefile.patch
Patch1: %{name}-%{version}-stropts.patch
License: Modified BSD
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.j10n.org/libspt/
BuildRequires: libtool

%description
On many platforms terminal emulators need some privilege for some
functionalities. Especially, they need root privilege on BSDs to prevent other
users from peeking user's input. With libspt you can drop all privileges of
your terminal emulator. Additionally libspt saves terminal emulator writers
many portability related headaches. (Yes, there is many headaches.)

From libspt-0.2 it also provides a minimal termios(termio/sgtty) wrapper
functionality. It does not support any customization nor complex settings but
enables you to implement easiest functionalities portably with least work.
# from web page

%package devel
Summary: Development files for libspt
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files and documentation needed to
develop applications that use libspt

%prep
%setup -q
%patch0 -p1 -b .makefile
%patch1 -p1 -b .stropts

%build
perl -pi -e 's/-Werror//g' Makefile.in
CFLAGS="%{optflags}" ./configure --libexecdir=%{_sbindir} --prefix=%{_prefix} --libdir=%{_libdir}
%make

%install
make install libexecdir=%{buildroot}%{_sbindir} prefix=%{buildroot}%{_prefix} libdir=%{buildroot}%{_libdir}
rm -rf %{buildroot}/usr/man

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYRIGHT CHANGES
#%%attr(4755,root,whell) %{_libexecdir}/sptagent
%attr(4755,root,wheel) %{_sbindir}/sptagent
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/libspt-config
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/*.la

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-13m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-12m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-10m)
- drop -Werror on i686 too

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-9m)
- rebuild against rpm-4.6

* Fri Apr  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-8m)
- stropts.h was removed from glibc; don't #include it anymore

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-7m)
- rebuild against gcc43

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-6m)
- retrived libtool library

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-5m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1-4m)
- enable ppc64

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1-3m)
- enable x86_64.

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1-2m)
- revised spec for enabling rpm 4.2.

* Fri Dec 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1m)
- Initial Momonga release
