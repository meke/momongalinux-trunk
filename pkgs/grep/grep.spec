%global         momorel 1

Summary:        Pattern matching utilities
Name:           grep
Version:        2.20
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Group:          Applications/Text
URL:            http://www.gnu.org/software/grep/
Source0:        ftp://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource:       0
Source1: colorgrep.sh
Source2: colorgrep.csh
Source3: GREP_COLORS
# upstream ticket 39444
Patch0: grep-2.20-man-fix-gs.patch
# upstream ticket 39445
Patch1: grep-2.20-help-align.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  pcre-devel >= 8.31
Requires(post): info

%description
'grep' searches the input files for lines containing a match to a given
pattern list.  When it finds a match in a line, it copies the line to
standard output (by default), or produces whatever other sort of output
you have requested with options.

%prep
%setup -q
%patch0 -p1 -b .man-fix-gs
%patch1 -p1 -b .help-align

%build
%global _bindir /bin
%configure
%make LDFLAGS=-s

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

mkdir -p %{buildroot}/bin
mkdir -p %{buildroot}%{_mandir}/man1
%makeinstall

gzip $RPM_BUILD_ROOT%{_infodir}/grep*

# remove
rm -f %{buildroot}%{_infodir}/dir

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -pm 644 %{SOURCE1} %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -pm 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}

%find_lang %{name}

%check
make check

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/grep.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/grep.info %{_infodir}/dir || :
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS TODO
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/profile.d/colorgrep.*sh
%config(noreplace) %{_sysconfdir}/GREP_COLORS
%{_infodir}/*.info*
%{_mandir}/*/*

%changelog
* Thu Jun  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.20-1m)
- update to 2.20
- sync with fc21

* Fri Feb 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18-1m)
- update to 2.18

* Mon Jan 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16-1m)
- update to 2.16

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-1m)
- update to 2.14
- rebuild against pcre-8.31

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12-1m)
- update to 2.12

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10-1m)
- update to 2.10

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9-1m)
- update to 2.9

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7-3m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7-2m)
- apply patch0 to fix range expressions issue

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.3-2m)
- full rebuild for mo7 release

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.4-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.4-2m)
- add Requires(post): info

* Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4
- License: GPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.3-5m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.3-4m)
- do not fetch Patch0, merged upstream
-- can not connect to www.openi18n.org

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.3-3m)
- run install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.3-2m)
- rebuild against gcc43

* Tue Aug 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3
- delete i18n patch (merged)

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (kossori)
- revise Patch0's URI

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.5.1a-1m)
  update to 2.5.1a

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.1-4m)
- revised spec for enabling rpm 4.2.

* Sun Oct 26 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.1-3m)
- pretty spec file.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.5.1-3m)
- put source into repository

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.1-2m)
- add URL tag
- use %%{_datadir}

* Tue Feb 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.1-1m)
- verion up to 2.5.1
- use bz2 Source
- use more rpm macros
- comment out "touch `find .`"
- add NoPatch: 0

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.5-2k)
- ver up.

* Tue Apr 30 2002 Motonobu Ichimura <famao@kondara.org>
- (2.4.2-16k)
- update i18n patch (0.13)

* Mon Feb  4 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.2-14k)
- autoconf automake

* Wed Jan 30 2002 Motonobu Ichimura <famao@kondara.org>
- (2.4.2-12k)
- i18n patch up to 0.12

* Tue Nov 20 2001 Motonobu Ichimura <famao@kondara.org>
- (2.4.2-10k)
- i18n patch up to 0.11

* Fri Oct 25 2001 Motonobu Ichimura <famao@kondara.org>
- (2.4.2-8k)
- change i18n patch to IBM's one

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (2.4.2-6k)
- rebuild against gettext 0.10.40.

* Wed Mar 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.4.2-5k)
- modified mandir bug (#864)
- modified License from distributable to GPL

* Fri Mar  2 2001 KUSUNOKI Masanori <nori@kondara.org>
- (2.4.2-3k)
- up to 2.4.2

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0-13k)
- modified spec file about man for compatibility

* Thu Oct 26 2000 Kenichi Matsubara <m@kondara.org>
- man.gz to man.bz2.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- added %clean process

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_grep-20000115

* Sat Dec 11 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Tue Nov 24 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_grep-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Sep 18 1999 Norihito Ohmori <ohmori@flatout.org>
- rebuild for new environment

* Sun Dec 6 1998 Jun Nishii <nishii@postman.riken.go.jp>
- change path to /bin

* Sat Nov 28 1998 Jun Nishii <nishii@postman.riken.go.jp>
- first release with grep-2.0-mb1.04.diff

* Fri Nov 25 1998 Jun Nishii <nishii@postman.riken.go.jp>
- added -l ja for description and summary

* Sun Sep 27 1998 Jun Nishii <nishii@postman.riken.go.jp>
- first release
