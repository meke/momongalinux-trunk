%global momorel 11
%define monoprefix %_prefix/lib

Summary: C# implementation of D-Bus
Name: ndesk-dbus
Version: 0.6.0
Release: %{momorel}m%{?dist}
URL: http://www.ndesk.org/DBusSharp
Source0: http://www.ndesk.org/archive/dbus-sharp/%{name}-%{version}.tar.gz
NoSource: 0
License: BSD
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: mono-core
BuildRequires: dbus-devel

# Obsoletes: dbus-sharp
# Provides: dbus-sharp

%description
D-Bus is a system for sending messages between applications. It is
used both for the systemwide message bus service, and as a
per-user-login-session messaging facility.

This package provides the C# bindings for using D-Bus from mono.

%package devel
Summary: %{name} devel
Group: Development/Libraries
Requires: %{name} >= %{version}

%description devel
%{name}-devel

%prep
%setup -q 

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

# gw wrong on x86_64
%if %_lib != lib
mkdir -p  %buildroot%_prefix/lib/
mv %buildroot%_libdir/mono %buildroot%_prefix/lib/
perl -pi -e "s^%_libdir^%_prefix/lib^" %buildroot%_libdir/pkgconfig/ndesk-dbus-1.0.pc
%endif

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{monoprefix}/mono/gac/NDesk.DBus
%{monoprefix}/mono/ndesk-dbus-1.0

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/ndesk-dbus-1.0.pc

%changelog
* Sat Jul 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-11m)
- remove Obsoletes and Provides dbus-sharp to enable build tomboy

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-10m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-7m)
- full rebuild for mo7 release

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-6m)
- split devel package

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-4m)
- rebuild against rpm-4.6

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-3m)
- good-bye debug symbol

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gcc43

* Sat Dec 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- rename dbus-sharp to ndesk-dbus

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70-4m)
- fix %%changelog section

* Wed May 23 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.19-3m)
- rebuild against monodoc-1.2.3-2m for moving monodoc dir

* Sat Sep  1 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.70-2m)
- revise %%install section for x86_64

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.70-1m)
- import from Mandrva

* Tue Aug  1 2006 Gotz Waschk <waschk@mandriva.org> 0.70-2mdv2007.0
- fix dll map

* Mon Jul 31 2006 Frederic Crozat <fcrozat@mandriva.com> 0.70-1mdv2007.0
- Initial package (based on dbus 0.62)
