%global momorel 17
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: A KDE tabulature editor
Name: kguitar
Version: 0.5.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://kguitar.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: admin.tar.bz2
Source2: %{name}.desktop
Patch0: %{name}-%{version}-gcc43.patch
Patch1: %{name}-%{version}-automake111.patch
Patch2: %{name}-%{version}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: libtool
BuildRequires: tse3-devel

%description
KGuitar is basically a guitar tabulature editor for K Desktop
Environment. It's much more than just a tab editor. It's features are:

* free GPLed K Desktop Environment GUI
* Powerful and convenient tabulature editing, including many effects
  and classical note score editing for classic instrument players;
* Full and very customizable MIDI to tabulature import and export;
* Support of extra data formats, such as ASCII tabulatures or popular
  programs' format, such as Guitar Pro's or TablEdit;
* Chord fingering construction tools - chord finder & chord analyzer;
* Highly customizable to suit a lot of possible instruments (not only
  6-stringed guitars, and even not only guitars), including drum tracks,
  lyrics and other MIDI events.

Well, right now KGuitar is in very alpha state and doesn't support
everything stated. However, it already features a nice tabulature
editor, though not ultimately powerful, loading/saving its own format (kg
files) and all the chord construction tools.

%prep
%setup -q

%patch0 -p1 -b .gcc43~

install -m 644 %{SOURCE2} %{name}_shell/

# build fix
rm -rf admin
tar xf %{SOURCE1}

%patch1 -p1 -b .automake111
%patch2 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category AudioVideo \
  --add-category Audio \
  --add-category Midi \
  --add-category Sequencer \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Multimedia/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS CODING-STANDARDS COPYING ChangeLog DEVELOPERS
%doc INSTALL README TRANSLATORS
%{_bindir}/%{name}
%{_libdir}/kde3/lib%{name}part.la
%{_libdir}/kde3/lib%{name}part.so
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/mimelnk/application/x-%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/services/%{name}_part.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-15m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.11-14m)
- use new automake

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.11-13m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.11-12m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-10m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-9m)
- fix build for autoconf-2.63

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-8m)
- update Patch0 for fuzz=0

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-7m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-5m)
- rebuild against gcc43

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-4m)
- support gcc-4.3

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1
- License: GPLv2

* Tue Mar  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-1m)
- initial package for guitar freaks using Momonga Linux
- Summary and %%description are imported from cooker
