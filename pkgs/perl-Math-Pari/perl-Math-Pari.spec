%global         momorel 1
%global         srcver 2.010808
%global         pari_version 2.3.5

Summary:        Math-Pari module for perl
Name:           perl-Math-Pari
Version:        2.010808
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Development/Languages
Source0:        http://www.cpan.org/authors/id/I/IL/ILYAZ/modules/Math-Pari-%{srcver}.zip
NoSource:       0
Patch0:		Math-Pari-2.010808-no-fake-version.patch
Patch1:		Math-Pari-2.010802-docs-and-testsuite.patch
Patch2:		Math-Pari-2.01080605-include-path.patch
URL:            http://www.cpan.org/modules/by-module/Math/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libpari23-devel >= %{pari_version}
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       libpari23 >= %{pari_version}
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
This package is a Perl interface to the famous library PARI for numerical/
scientific/ number-theoretic calculations. It allows use of most PARI functions
as Perl functions, and (almost) seamless merging of PARI and Perl data.

%prep
%setup -q -n Math-Pari-%{srcver}

# Don't use a fake version number when we can use a real one
%patch0 -p1
pari_int_version=$(pkg-config --modversion libpari23 | perl -p -e 's/(\d+)\.(\d+)\.(\d+)/sprintf("%d%03d%03d",$1,$2,$3)/e')
sed -i -e "s/@@@OUR-PARI-VERSION@@@/${pari_int_version}/" Makefile.PL

# We want to build the docs and test suite too
%patch1 -p1

# Use <pari/pari.h> as per pari upstream documentation
%patch2

%build
paridir=$(pkg-config --variable=paridir libpari23)
perl Makefile.PL \
	INSTALLDIRS=vendor \
	OPTIMIZE="$(pkg-config --cflags-only-I libpari23) -I${paridir}/src %{optflags}" \
	paridir="${paridir}" \
	parilib="$(pkg-config --libs libpari23)"
make %{?_smp_mflags}

%check
%{__make} test

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot}%{perl_vendorarch}/auto -name '.packlist' | xargs rm -f
find %{buildroot} -type f -name '*.bs' -a -empty -delete || :
find %{buildroot} -depth -type d -a -empty -delete || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%dir %{perl_vendorarch}/Math/
%exclude %doc %{perl_vendorarch}/Math/libPARI.dumb.pod
%doc %{perl_vendorarch}/Math/libPARI.pod
%{perl_vendorarch}/Math/*.pm
%{perl_vendorarch}/auto/Math/
%{_mandir}/man3/Math::Pari.3pm*
%{_mandir}/man3/Math::PariInit.3pm*
%{_mandir}/man3/Math::libPARI.3pm*
%exclude %{_mandir}/man3/Math::libPARI.dumb.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010808-1m)
- rebuild against perl-5.20.0
- update to 2.010808

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.07-1m)
- rebuild with libpari23-2.3.5 (NOT pari-2.7.0)
- update to 2.01060807

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-12m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-11m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-8m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-7m)
- change Source1 URI

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-6m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-5m)
- rebuild against perl-5.16.0

* Wed Oct 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.010806.05-4m)
- just increase release

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-2m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.05-1m)
- update to 2.01080605
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.010806.04-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.010806.04-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.04-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.010806.04-5m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.04-4m)
- use external pari (2.3.5)

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.04-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.04-2m)
- rebuild against perl-5.12.0

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.04-1m)
- update to 2.01080604

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.03-1m)
- update to 2.01080603

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806.02-1m)
- update to 2.01080602

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.010806-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010806-1m)
- update to 2.010806

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010805-1m)
- update to 2.010805

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010804-1m)
- update to 2.010804

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010802-1m)
- update to 2.010802

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.010801-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010801-2m)
- rebuild against perl-5.10.1

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010801-1m)
- update to 2.010801

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.010800-2m)
- rebuild against rpm-4.6

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010800-1m)
- update to 2.010800
- delete unused patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.010709-6m)
- rebuild against gcc43

* Sun Mar 16 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.010709-5m)
- import Patch0: Math-Pari-2.010709-perl510.patch from fc devel

* Fri Feb 22 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.010709-4m)
- temporary fix applying http://www.perlmonks.org/?node_id=667933

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.010709-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.010709-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.010709-1m)
- update to 2.010709

* Sun Oct 01 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.010706-2m)
- change Source1 URI

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.010706-1m)
- spec file was autogenerated
