%global momorel 1
Name:           gnome-search-tool
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        Utility for finding files for GNOME
Group:          Applications/System

License:        GPLv2+ and GFDL
#No URL for the package specifically, as of now
URL:            http://www.gnome.org/gnome-3/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/gnome-search-tool/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gtk3-devel
BuildRequires:  GConf2-devel
BuildRequires:  intltool
BuildRequires:  gnome-doc-utils
BuildRequires:  rarian-compat
BuildRequires:  docbook-dtds
BuildRequires:  libSM-devel
BuildRequires:  desktop-file-utils

Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2

Obsoletes: gnome-utils < 3.3
Obsoletes: gnome-utils-devel < 3.3
Obsoletes: gnome-utils-libs < 3.3

%description
GNOME Search Tool is a utility for finding files on your system. To perform a
basic search, you can type a filename or a partial filename, with or without
wildcards. To refine your search, you can apply additional search options.

GNOME Search Tool uses the find, grep, and locate UNIX commands. Since the
find, grep, and locate commands support the -i option, all searches are
case-insensitive.

%prep
%setup -q


%build
%configure
%make 


%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1

make install DESTDIR=%{buildroot}

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name} --with-gnome


%pre
%gconf_schema_prepare %{name}

%post
%gconf_schema_upgrade %{name}

%preun
%gconf_schema_remove %{name}


%files -f %{name}.lang
%doc AUTHORS COPYING COPYING.docs NEWS
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/gsearchtool
%{_datadir}/GConf/gsettings/gnome-search-tool.convert
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-search-tool.gschema.xml
%doc %{_mandir}/man1/%{name}.1.bz2
%doc %{_datadir}/help/*/*

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Aug 02 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- import from fedora

