%global momorel 1
Name:		xsettingsd
Version:	0
%define commit_id 7804894
Release: 	0.4.20091208git%{commit_id}.%{momorel}m%{?dist}
Summary:	Provides settings to X11 clients via the XSETTINGS specification

Group:		System Environment/Daemons
License:	BSD
URL:		http://code.google.com/p/xsettingsd/

# from http://github.com/derat/xsettingsd/tarball/%{commid_id}
Source0:	derat-xsettingsd-%{commit_id}.tar.gz

BuildRequires:	scons libstdc++-devel libX11-devel

%description
xsettingsd is a daemon that implements the XSETTINGS specification.

It is intended to be small, fast, and minimally dependent on other libraries.
It can serve as an alternative to gnome-settings-daemon for users who are not
using the GNOME desktop environment but who still run GTK+ applications and
want to configure things such as themes, font anti-aliasing/hinting, and UI
sound effects.

%prep
%setup -q -n derat-%{name}-%{commit_id}
sed -i -e "s|-Wall -Werror|%{optflags}|g" SConstruct

%build
scons

%install
install -Dm0755 xsettingsd		%{buildroot}%{_bindir}/xsettingsd
install -Dm0755 dump_xsettings		%{buildroot}%{_bindir}/dump_xsettings
install -Dm0644 xsettingsd.1		%{buildroot}%{_mandir}/man1/xsettingsd.1
install -Dm0644 dump_xsettings.1	%{buildroot}%{_mandir}/man1/dump_xsettings.1

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_mandir}/man1/*
%doc README COPYING

%changelog
* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-4.20091208git7804894.1m)
- import from fedora
