%global         momorel 3

Name: 		gstreamer
Version: 	0.10.36
%define		version_mm %(LC_ALL="C" echo %{version} | cut -d. -f1,2)
Release:	%{momorel}m%{?dist}
Summary: 	GStreamer streaming media framework runtime.
Group: 		System Environment/Libraries
License: 	LGPLv2+
URL:		http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/gstreamer/gstreamer-%{version}.tar.bz2
#Source0: http://ftp.gnome.org/pub/gnome/sources/%{name}/0.10/%{name}-%{version}.tar.bz2
NoSource: 0
Patch1:	   gstreamer-0.10.36-bison3.patch

BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: 	glib2-devel >= 2.18.1
BuildRequires: 	bison
BuildRequires: 	flex
BuildRequires: 	m4
BuildRequires: 	gettext
BuildRequires: 	zlib-devel >= 1.2.3
BuildRequires:	popt
BuildRequires:	autoconf
BuildRequires:	libgnomeui-devel >= 2.24.0
BuildRequires: gobject-introspection-devel >= 0.9.10
Requires(pre):		/sbin/ldconfig
Provides:       gstreamer010 = %{version}-%{release}
Obsoletes:      gstreamer010 <= 0.10.11-1m
Obsoletes:      gst-plugins-additional
Obsoletes:      gst-plugins
Obsoletes:      gstreamer-plugins-additional
Obsoletes:      gstreamer-plugins
Requires(pre): coreutils

%description
GStreamer is a streaming-media framework, based on graphs of filters which
operate on media data. Applications using this library can do anything
from real-time sound processing to playing videos, and just about anything
else media-related.  Its plugin-based architecture means that new data
types or processing capabilities can be added simply by installing new 
plugins.

%package devel
Summary: 	Libraries/include files for GStreamer streaming media framework.
Group: 		Development/Libraries
Requires: %{name} = %{version}-%{release}

Requires: 	glib-devel
Requires: 	libxml2-devel
Requires: 	libgnomeui-devel
Provides:       gstreamer010-devel = %{version}-%{release}
Obsoletes:      gstreamer010-devel <= 0.10.11-1m
Obsoletes:      gst-plugins-devel
Obsoletes:      gstreamer-plugins-devel

%description devel
GStreamer is a streaming-media framework, based on graphs of filters which
operate on media data. Applications using this library can do anything
from real-time sound processing to playing videos, and just about anything
else media-related.  Its plugin-based architecture means that new data
types or processing capabilities can be added simply by installing new   
plugins.

This package contains the libraries and includes files necessary to develop
applications and plugins for GStreamer.

%prep
%setup -q
%patch1 -p1 -b .bison3~

%build
export CFLAGS="-Wno-redundant-decls"
%configure \
    --enable-static=no \
    --disable-valgrind \
    --with-cashedir=%{_localstatedir}/cache/gstreamer-%{version_mm} \
    --enable-gtk-doc \
    --enable-introspection=yes \
    --disable-examples
%ifarch ppc
# can't build tests/memchunk...
perl -p -i -e 's, memchunk,,' tests/Makefile
%endif

%make

%install  
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO ABOUT-NLS RELEASE
%{_bindir}/gst-*-%{version_mm}
%dir %{_libdir}/gstreamer-%{version_mm}
%{_libdir}/gstreamer-%{version_mm}/*.so
%{_libdir}/gstreamer-%{version_mm}/*.la
%{_libdir}/lib*-%{version_mm}.so.*
%exclude %{_libdir}/*.la
%{_datadir}/locale/*/*/*-%{version_mm}.mo
%{_mandir}/man1/gst-*-%{version_mm}.1*

%{_bindir}/gst-feedback
%{_bindir}/gst-inspect
%{_bindir}/gst-launch
%{_bindir}/gst-typefind
%{_bindir}/gst-xmlinspect
%{_bindir}/gst-xmllaunch

%dir %{_libexecdir}/gstreamer-%{version_mm}
%{_libexecdir}/gstreamer-%{version_mm}/gst-plugin-scanner

#gir
%{_libdir}/girepository-1.0/Gst-0.10.typelib
%{_libdir}/girepository-1.0/GstBase-0.10.typelib
%{_libdir}/girepository-1.0/GstCheck-0.10.typelib
%{_libdir}/girepository-1.0/GstController-0.10.typelib
%{_libdir}/girepository-1.0/GstNet-0.10.typelib

%files devel
%defattr(-, root, root)
%{_includedir}/gstreamer-%{version_mm}
%{_libdir}/lib*-%{version_mm}.so
%{_libdir}/pkgconfig/*%{version_mm}.pc
%{_datadir}/aclocal/gst-element-check-%{version_mm}.m4
%{_datadir}/gtk-doc/html/gstreamer-0.10
%{_datadir}/gtk-doc/html/gstreamer-libs-0.10
%{_datadir}/gtk-doc/html/gstreamer-plugins-0.10

# gir
%{_datadir}/gir-1.0/Gst-0.10.gir
%{_datadir}/gir-1.0/GstBase-0.10.gir
%{_datadir}/gir-1.0/GstCheck-0.10.gir
%{_datadir}/gir-1.0/GstController-0.10.gir
%{_datadir}/gir-1.0/GstNet-0.10.gir

%changelog
* Tue Jan 21 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.36-3m)
- fix build failure

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.36-2m)
- rebuild for glib 2.33.2

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.36-1m)
- update to 0.10.36

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.35-1m)
- update to 0.10.35

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.34-1m)
- update to 0.10.34

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.33-1m)
- update to 0.10.33

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.32-2m)
- rebuild for new GCC 4.6

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.32-1m)
- update to 0.10.32

* Sun Nov 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30.2-1m)
- update to 0.10.30.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.30-5m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.30-4m)
- fix build failure; import patch from fedora

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30-3m)
- rebuild against gobject-introspection-0.9.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.30-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30-1m)
- update to 0.10.30

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.29-2m)
- add %dir

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.29-1m)
- update to 0.10.29

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.28-1m)
- update to 0.10.28

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.26-1m)
- update to 0.10.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.25-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.25-1m)
- update to 0.10.25

* Wed Aug  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.24-1m)
- update to 0.10.24

* Mon May 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.23-1m)
- update to 0.10.23

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.22-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.22-1m)
- update to 0.10.22
-- delete patch0 (fixed)

* Mon Dec 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.21-2m)
- apply bison241 patch (Patch0)
-- need to run autogen.sh

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.21-1m)
- update to 0.10.21

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.20-2m)
- add Obsoletes: gstreamer-plugins-additional
- add Obsoletes: gstreamer-plugins

* Fri Jun 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.20-1m)
- update to 0.10.20

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.19-1m)
- update to 0.10.19

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.18-2m)
- rebuild against gcc43

* Wed Mar 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18

* Fri Feb  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.17-1m)
- update to 0.10.17

* Tue Jan 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16

* Sun Nov 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.15-1m)
- update to 0.10.15

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.14-1m)
- update to 0.10.14

* Fri Jun  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Thu Mar 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.12-1m)
- update to 0.10.12

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-3m)
- add PreReq: coreutils

* Sun Jan 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-2m)
- rename gstreamer010 -> gstreamer

* Sat Dec 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Thu Oct 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Wed May 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Sat May  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5

* Wed Apr  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-2m)
- No more build manual that needs network access to www.oasis-open.org :-(

* Mon Apr  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4
- and rename gstreamer010 (gstreamer is gstreamer-0.8.x)

* Sun Feb 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.12-1m)
- version 0.8.12

* Sat Jan 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.11-2m)
- add --disable-valgrind at configure

* Tue Sep  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.11-1m)
- version 0.8.11

* Tue Aug  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.10-1m)
- version 0.8.10

* Wed Mar  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.9-1m)
- version 0.8.9

* Fri Jan  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-1m)
- version 0.8.8

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-1m)
- version 0.8.7
- add NEWS and DEVEL to %%doc

* Mon Sep 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- version 0.8.5
- add CFLAGS
- change URL

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.8.3-1m)
- version 0.8.3

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.1-2m)
- adjustment BuildPreReq

* Fri Apr 16 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1
- GNOME 2.6 Desktop
- change source URL
- change location of %%changelog

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.4-1m)
- version 0.6.4

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.3-1m)
- version 0.6.3

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.2-1m)
- version 0.6.2

* Tue Apr 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-1m)
- version 0.6.1

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-2m)
- rebuild against zlib 1.1.4-5m

* Mon Feb 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.0-1m)
- version 0.6.0

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.2-1m)
- version 0.5.2

* Thu Jan 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.1-2m)
- modify post script
- localstatedir -> /var

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.1-1m)
- version 0.5.1

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-1m)
- version 0.5.0

* Sun Dec  1 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.4.2-3m)
- fix not configure but aclocal.m4

* Sun Dec 01 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-2m)
- add gstreamer-configure-gtk-doc.patch for configure at detect gtk-doc version

* Wed Nov 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.2-1m)
- import

* Sat Oct 12 2002 Christian Schaller <Uraeus@linuxrising.org>
- Updated to work better with default RH8 rpm
- Added missing unspeced files
- Removed .a and .la files from buildroot

* Sat Sep 21 2002 Thomas Vander Stichele <thomas@apestaart.org>
- added gst-md5sum

* Tue Sep 17 2002 Thomas Vander Stichele <thomas@apestaart.org>
- adding flex to buildrequires

* Fri Sep 13 2002 Christian F.K. Schaller <Uraeus@linuxrising.org>
- Fixed the schedulers after the renaming
* Sun Sep 08 2002 Thomas Vander Stichele <thomas@apestaart.org>
- added transfig to the BuildRequires:

* Sat Jun 22 2002 Thomas Vander Stichele <thomas@apestaart.org>
- moved header location

* Mon Jun 17 2002 Thomas Vander Stichele <thomas@apestaart.org>
- added popt
- removed .la

* Fri Jun 07 2002 Thomas Vander Stichele <thomas@apestaart.org>
- added release of gstreamer to req of gstreamer-devel
- changed location of API docs to be in gtk-doc like other gtk-doc stuff
- reordered SPEC file

* Mon Apr 29 2002 Thomas Vander Stichele <thomas@apestaart.org>
- moved html docs to gtk-doc standard directory

* Tue Mar 5 2002 Thomas Vander Stichele <thomas@apestaart.org>
- move version defines of glib2 and libxml2 to configure.ac
- add BuildRequires for these two libs

* Sun Mar 3 2002 Thomas Vander Stichele <thomas@apestaart.org>
- put html docs in canonical place, avoiding %doc erasure
- added devhelp support, current install of it is hackish

* Sat Mar 2 2002 Christian Schaller <Uraeus@linuxrising.org>
- Added documentation to build

* Mon Feb 11 2002 Thomas Vander Stichele <thomas@apestaart.org>
- added libgstbasicscheduler
- renamed libgst to libgstreamer

* Fri Jan 04 2002 Christian Schaller <Uraeus@linuxrising.org>
- Added configdir parameter as it seems the configdir gets weird otherwise

* Thu Jan 03 2002 Thomas Vander Stichele <thomas@apestaart.org>
- split off gstreamer-editor from core
- removed gstreamer-gnome-apps

* Sat Dec 29 2001 Rodney Dawes <dobey@free.fr>
- Cleaned up the spec file for the gstreamer core/plug-ins split
- Improve spec file

* Sat Dec 15 2001 Christian Schaller <Uraeus@linuxrising.org>
- Split of more plugins from the core and put them into their own modules
- Includes colorspace, xfree and wav
- Improved package Require lines
- Added mp3encode (lame based) to the SPEC

* Wed Dec 12 2001 Christian Schaller <Uraeus@linuxrising.org>
- Thomas merged mpeg plugins into one
* Sat Dec 08 2001 Christian Schaller <Uraeus@linuxrising.org>
- More minor cleanups including some fixed descriptions from Andrew Mitchell

* Fri Dec 07 2001 Christian Schaller <Uraeus@linuxrising.org>
- Added logging to the make statement

* Wed Dec 05 2001 Christian Schaller <Uraeus@linuxrising.org>
- Updated in preparation for 0.3.0 release

* Fri Jun 29 2001 Christian Schaller <Uraeus@linuxrising.org>
- Updated for 0.2.1 release
- Split out the GUI packages into their own RPM
- added new plugins (FLAC, festival, quicktime etc.)

* Sat Jun 09 2001 Christian Schaller <Uraeus@linuxrising.org>
- Visualisation plugins bundled out togheter
- Moved files sections up close to their respective descriptions

* Sat Jun 02 2001 Christian Schaller <Uraeus@linuxrising.org>
- Split the package into separate RPMS, 
  putting most plugins out by themselves.

* Fri Jun 01 2001 Christian Schaller <Uraeus@linuxrising.org>
- Updated with change suggestions from Dennis Bjorklund

* Tue Jan 09 2001 Erik Walthinsen <omega@cse.ogi.edu>
- updated to build -devel package as well

* Sun Jan 30 2000 Erik Walthinsen <omega@cse.ogi.edu>
- first draft of spec file
