%global momorel 7

Name: docbook-simple
Version: 1.1
Release: %{momorel}m%{?dist}
Group: Applications/Text
Summary: Simplified DocBook is a small subset of the DocBook XML DTD.
License: "Distributable"
URL: http://www.oasis-open.org/docbook/xml/simple/
Source0: http://www.docbook.org/xml/simple/1.1/%{name}-%{version}.zip
NoSource: 0
Source1: %{name}.README.redhat
Source2: %{name}.xml
Source3: %{name}.cat
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: unzip
Requires: sgml-common
Requires(post): libxml2 >= 2.4.8
Requires(postun): libxml2 >= 2.4.8
Requires: docbook-dtds

%description
Simplified DocBook is an attempt to provide a proper subset of DocBook
that is simultaneously smaller and still useful. Documents written in
the subset must be 100% legal DocBook documents. This is a subset for
single documents (articles, white papers, etc.), so there's no need
for books or sets, just 'articles'. Simplified DocBook documents are 
viewable in online browsers if styled with CSS. (it's XML not SGML).


%prep
# splatter the files into a version-numbered directory
%setup -q -c -n %{version}

# see http://rpm-devel.colug.net/max-rpm/s1-rpm-inside-macros.html
# setup -c creates the dir then changes to it to expand SOURCE0

%build

%install

rm -rf $RPM_BUILD_ROOT

########## install versioned-numbered directory of dtd files ############

DESTDIR=$RPM_BUILD_ROOT/usr/share/xml/docbook/simple
mkdir -p $DESTDIR
cp -a ../%{version} $DESTDIR

########## install package catalogs  ################

XML_CAT_DIR=$RPM_BUILD_ROOT/etc/xml
mkdir -p $XML_CAT_DIR
install -p -m 644 %{SOURCE2} $XML_CAT_DIR

SGML_CAT_DIR=$RPM_BUILD_ROOT/etc/sgml
mkdir -p $SGML_CAT_DIR
install -p -m 644 %{SOURCE3} $SGML_CAT_DIR

####### FIXME: must copy README.redhat to source directory ########
#######        for %doc to find it, ${SOURCE1} doesn't work ########

cp -p %{SOURCE1} ./README.fedora

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf ../%{version}

%files
%defattr (-,root,root,-)
%doc sdocbook.css
%doc README.fedora
%{_datadir}/xml/docbook/simple/%{version}
%config(noreplace) %{_sysconfdir}/sgml/docbook-simple.cat
%config(noreplace) %{_sysconfdir}/xml/docbook-simple.xml


%post

##################  XML catalog registration #######################

## Define handy variables ##

ROOT_XML_CATALOG=%{_sysconfdir}/xml/catalog
PKG_XML_CATALOG=%{_sysconfdir}/xml/docbook-simple.xml

#### Root XML Catalog Entries ####
#### Delegate appropriate lookups to package catalog ####

if [ -w $ROOT_XML_CATALOG ]
then
        %{_bindir}/xmlcatalog --noout --add "delegatePublic" \
                "-//OASIS//DTD Simplified" \
                "file://$PKG_XML_CATALOG" $ROOT_XML_CATALOG

        %{_bindir}/xmlcatalog --noout --add "delegateURI" \
                "http://www.oasis-open.org/docbook/xml/simple/1.1/" \
                "file://$PKG_XML_CATALOG" $ROOT_XML_CATALOG

  # Next line because some resolvers misinterpret uri entries
        %{_bindir}/xmlcatalog --noout --add "delegateSystem" \
                "http://www.oasis-open.org/docbook/xml/simple/1.1/" \
                "file://$PKG_XML_CATALOG" $ROOT_XML_CATALOG
fi

####################################################################


#################  SGML catalog registration  ######################

ROOT_SGML_CATALOG=%{_sysconfdir}/sgml/catalog
PKG_SGML_CATALOG=%{_sysconfdir}/sgml/docbook-simple.cat

#### Root SGML Catalog Entries ####
#### "Delegate" appropriate lookups to package catalog ####


############## use install-catalog ######################

if [ -w $ROOT_SGML_CATALOG ]
then
# xmlcatalog deletes OVERRIDE YES directive, use install-catalog instead
#         /usr/bin/xmlcatalog --sgml --noout --add \
#     "/etc/sgml/docbook-simple.cat"

 install-catalog --add \
 $PKG_SGML_CATALOG \
 $ROOT_SGML_CATALOG 1>/dev/null

# Hack to workaround bug in install-catalog
   sed -i 's/^CATALOG.*log\"*$//g' $PKG_SGML_CATALOG  
fi

####################################################################


# Finally, make sure everything in /etc/*ml is readable!
/bin/chmod a+r  %{_sysconfdir}/sgml/*
/bin/chmod a+r  %{_sysconfdir}/xml/*

%postun
##
## SGML and XML catalogs
##
## Jobs: remove package catalog entries from both root catalogs &
##       remove package catalogs

##### SGML catalogs #####

## Remove package catalog entry from SGML root catalog
%{_bindir}/xmlcatalog --sgml --noout --del \
   %{_sysconfdir}/sgml/catalog \
  "%{_sysconfdir}/sgml/docbook-simple.cat" 
	
## Remove SGML package catalog
rm -f  %{_sysconfdir}/sgml/docbook-simple.cat


##### XML catalogs #####

## Remove package catalog entry from XML root catalog
%{_bindir}/xmlcatalog --noout --del \
  "file://%{_sysconfdir}/xml/docbook-simple.xml" \
   %{_sysconfdir}/xml/catalog 

## Remove XML package catalog
rm -f  %{_sysconfdir}/sgml/docbook-simple.xml

## Remove dtd directory
rm -rf %{_datadir}/xml/docbook/simple

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against rpm-4.6

* Wed Jul 16 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-2m)
- enable install-catalog execution in %%post again

* Wed Jul 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- update to 1.1
- sync with Fedora devel
-- * Mon Nov 05 2007 Ondrej Vasik <ovasik@redhat.com> - 1.1-3
-- - merge review(#225701)
-- - spec modified to follow guidelines
-- 
-- * Wed Oct 24 2007 Ondrej Vasik <ovasik@redhat.com> - 1.1-2
-- - rpmlint check
-- - /etc/ files marked as config, fixed bad requirements
-- - cosmetic cleanup of spec file
-- 
-- * Thu May 24 2007 Ondrej Vasik <ovasik@redhat.com> - 1.1-1.02
-- - fixed added error in docbook-simple.xml(wrong catalog version)
-- 
-- * Thu May 24 2007 Ondrej Vasik <ovasik@redhat.com> - 1.1-1
-- - rebuilt with latest stable upstream release(1.1)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0-2m)
- modify spec - commented out install-catalog execution
  so that not to append "CATALOG /etc/sgml/catalog" to docbook-simple.cat

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.0-2.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Sep 07 2004 Mark Johnson <mjohnson@redhat.com> 1.0-1
- Initial release

