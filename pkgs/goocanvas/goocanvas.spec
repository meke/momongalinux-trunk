%global momorel 1

Summary:        A new canvas widget for GTK+ that uses cairo for drawing
Name:           goocanvas
Version:        1.0.0
Release:        %{momorel}m%{?dist}

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://live.gnome.org/GooCanvas
Source0:        ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.0/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  pkgconfig, gettext, gtk2-devel

%description
GooCanvas is a new canvas widget for GTK+ that uses the cairo 2D library for
drawing. It has a model/view split, and uses interfaces for canvas items and
views, so you can easily turn any application object into canvas items.

%package devel
Group:          Development/Libraries
Summary:        A new canvas widget for GTK+ that uses cairo for drawing
Requires:       %{name} = %{version}-%{release} pkgconfig

%description devel
GooCanvas is a new canvas widget for GTK+ that uses the cairo 2D library for
drawing. It has a model/view split, and uses interfaces for canvas items and
views, so you can easily turn any application object into canvas items.

These are the files used for development.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_libdir}/lib%{name}.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}-1.0
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/gtk-doc/html/%{name}


%changelog
* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sun Feb 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.13-2m)
- add autoreconf (build fix)

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-1m)
- import from Fedora to Momonga for ruby-goocanvas (ruby-gnome.spec)

* Sat Jan 17 2009 Denis Leroy <denis@poolshark.org> - 0.13-1
- Update to upstream 0.13
- Updated URLs to gnome.org

* Tue Sep 30 2008 Bernard Johnson <bjohnson@symetrix.com> - 0.10-2
- demo application does not build; remove it

* Sun Jun 29 2008 Bernard Johnson <bjohnson@symetrix.com> - 0.10-1
- v 0.10

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9-4
- Autorebuild for GCC 4.3

* Mon Aug 27 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.9-3
- don't explicitely require gtk2; no special versioning needed since FC6+

* Sun Aug 26 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.9-2
- require gtk2 not gtk2-devel (bz #254239)

* Sun Aug 19 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.9-1
- 0.9
- update license tag to LGPLv2+

* Sat May 05 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.6-2
- bump for incorrect tag in buildsys

* Mon Mar 19 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.6-1
- initial release
