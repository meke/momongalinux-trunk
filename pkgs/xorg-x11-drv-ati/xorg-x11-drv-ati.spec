%global momorel 1
%define tarball xf86-video-ati
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

%ifarch %{ix86} x86_64 ia64 ppc ppc64 alpha
%define with_dri	1
%else
%define with_dri	0
%endif

Summary:   Xorg X11 ati video driver
Name:      xorg-x11-drv-ati
Version:   7.4.0
Release:   %{momorel}m%{?dist}
URL:       http://www.x.org/
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0

Source30: %{xorgurl}/driver/xf86-video-r128-6.9.1.tar.bz2 
NoSource: 30

Source40: %{xorgurl}/driver/xf86-video-mach64-6.9.3.tar.bz2 
NoSource: 40

Patch40: mach64-buildfix.patch

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: libpciaccess-devel
BuildRequires: systemd-devel >= 187
BuildRequires: xorg-x11-server-devel >= 1.13.0
BuildRequires: xorg-x11-glamor-devel
%if %{with_dri}
BuildRequires: mesa-libGL-devel >= 8.1.0-0.20120707.2m
BuildRequires: libdrm-devel >= 2.4.37
%endif

Requires:  xorg-x11-server-Xorg >= 1.12.3-3m
Requires:  libpciaccess

%description 
X.Org X11 ati video driver.

%prep
%setup -q -n %{tarball}-%{version} -b 30 -b 40

%build
%configure --disable-static
%make

pushd ../xf86-video-r128*
%configure --disable-static
%make
popd

pushd ../xf86-video-mach64*
patch -p1 < %{PATCH40}
%configure --disable-static
%make
popd

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

pushd ../xf86-video-r128*
make install DESTDIR=$RPM_BUILD_ROOT
popd

pushd ../xf86-video-mach64*
make install DESTDIR=$RPM_BUILD_ROOT
popd

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/ati_drv.so
%{driverdir}/mach64_drv.so
%{driverdir}/r128_drv.so
%{driverdir}/radeon_drv.so
%{_mandir}/man4/ati.4*
%{_mandir}/man4/r128.4*
%{_mandir}/man4/radeon.4*

%changelog
* Sat Jun 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4.0-1m)
- update 7.4.0

* Mon Feb 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3.0-1m)
- update 7.3.0

* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.2.0-1m)
- update 7.2.0

* Sat Feb  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1.0-1m)
- update 7.1.0

* Wed Nov  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.0.0-1m)
- update 7.0.0

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.99.99-0.2m)
- update 6.99.99-20121001
- update r128-6.9.1

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.99.99-0.1m)
- update 6.99.99
- update mach64, r128 driver
- drop radeonhd driver

* Sat Aug  4 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.14.6-2m)
- rebuild against systemd-187

* Sat Jun 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.6-1m)
- update 6.14.6

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.5-1m)
- update 6.14.5

* Fri Mar 30 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.14.4-2m)
- update xf86-video-r128-6.8.2 xf86-video-mach64-6.9.1 again
- comment out patch20

* Fri Mar 30 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.14.3-4m)
- back to 6.14.3
- but cannot build, maybe

* Fri Mar 30 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.14.4-1m)
- update xf86-video-r128-6.8.2 xf86-video-mach64-6.9.1

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.3-3m)
- vgahw patch

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.3-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.3-1m)
- update 6.14.3

* Mon Sep 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.2-4m)
- disable patch

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.2-3m)
- add patch
-- support any hardware

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.2-2m)
- rebuild against xorg-x11-server-1.10.99.901

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.2-1m)
- update to 6.14.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.14.1-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.14.1-1m)
- update to 6.14.1

* Sat Feb  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.14.0-1m)
- update to 6.14.0

* Sat Jan  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13.2-4m)
- add xf86-video-ati-6.13.2-HEAD.patch

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.13.2-3m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.13.2-2m)
- rebuild for new GCC 4.5

* Tue Oct  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13.2-1m)
- update 6.13.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.13.1-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13.1-1m)
- update 6.13.1

* Thu Jun 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13.0-2m)
- update ATI,radeonhd HEAD patch

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.13.0-1m)
- update 6.13.0

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.12.192-1m)
- rebuild against xorg-server-1.8

* Sat Mar 27 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.12.192-1m)
- update 6.12.192

* Thu Mar 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.12.191-1m)
- update 6.12.191

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.12.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.12.4-3m)
- rebuild against xorg-x11-server-1.7.1

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12.4-2m)
- update radeonhd-1.3.0

* Tue Sep 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12.4-1m)
- update 6.12.4
- update radeonhd-20090914

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12.1-2m)
- update mach64-6.8.1
- update radeonhd-20090728

* Thu Apr  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12.1-1m)
- update 6.12.1
- update radeonhd-20090409

* Tue Mar 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12.1-1m)
- update 6.12.1

* Sun Mar 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12.0-1m)
- update 6.12.0

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.11.0.0-2m)
- rebuild against xorg-x11-server-1.6.0
- update radeonhd driver

* Fri Feb 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.11.0.0-1m)
- update 6.11.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.10.0-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.10.0.0-1m)
- update 6.10.0

* Mon Dec 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0.91-1m)
- update 6.9.0.91(6.10.0-rc1)

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0-8m)
- update radeonhd-1.2.4

* Thu Dec  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.9.0-7m)
- add CPPFLAGS for radeonhd

* Wed Oct 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0-6m)
- update radeonhd-1.2.3

* Sun Sep 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0-5m)
- update radeon.xinf

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0-4m)
- update radeonhd-20080911
- Obsolete Avivo Driver

* Thu Aug 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0-3m)
- update radeonhd-20080821

* Sun Jul 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9.0-2m)
- update radeonhd-20080720

* Thu Jun 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.9.0-1m)
- update 6.9.0
- update xf86-video-radeonhd-20080626

* Wed Jun 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.192-2m)
- update 6.8.192

* Mon Jun 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.191-2m)
- fix momorel
- xf86-video-radeonhd-20080623

* Fri Jun 13 2008 Nisho Futoshi <futoshi@momonga-linux.org>
- (6.8.191-1m)
- update to 6.8.191
- add xf86-video-r128 xf86-video-mach64 source
- comment out patch1 patch2

* Sat May 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.0-6m)
- add upstream patch
- update radeonhd-1.2.1-20080531

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.0-5m)
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Wed Apr 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.0-4m)
- update radeonhd-1.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.8.0-3m)
- rebuild against gcc43

* Mon Mar 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.0-2m)
- update xf86-video-radeonhd-20080331

* Wed Feb 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.0-1m)
- update 6.8.0

* Sun Feb 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.197-5m)
- no use radeonhd.xinf. radeonhd driver still buggy.

* Sat Feb 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.197-5m)
- import upstream-fix patch
- update ati.xinf
- rename avivo.xinf to radeonhd.xinf
- update xf86-video-radeonhd-20080211.tar.bz2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.197-4m)
- %%NoSource -> NoSource

* Thu Jan 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.197-3m)
- update xf86-video-radeonhd-20080131.tar.bz2

* Sun Dec 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.197-2m)
- update xf86-video-radeonhd-1.1.0

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.197-1m)
- update 6.7.197
- update xf86-video-radeonhd-20071221

* Fri Nov 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.196-2m)
- update xf86-video-radeonhd-1.0

* Mon Nov 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.196-1m)
- update 6.7.196
- update xf86-video-radeonhd-20071105

* Fri Oct 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.195-2m)
- update xf86-video-radeonhd-20071026
- R5xx default avivo to radeonhd

* Sun Oct  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.195-1m)
- update 6.7.195
- xf86-video-radeonhd-20071007
- xf86-video-avivo-20071007

-- xf86-video-radeonhd
* Tue Sep 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.194-1m)
- update 6.7.194
- add Radeon HD video driver
-- xf86-video-radeonhd

* Sat Sep 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.193-1m)
- update 6.7.193
- update avivo-20070921

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.192-2m)
- rebuild against xorg-x11-server-1.4

* Mon Aug 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.192-1m)
- update 6.7.192

* Sat Aug 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.191-2m)
- update xf86-video-avivo-20070825

* Thu Aug 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.191-1m)
- update 6.7.191

* Sun Aug  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.193-1m)
- update 6.6.193

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.192-4m)
- add avivo.xinf
- update xf86-video-avivo 20070731

* Tue Jul 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.192-3m)
- update xf86-video-avivo 20070724

* Sun Jul  1 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.192-2m)
- unpatch radeon-6.6.192-dotclock-filter.patch.
-- SEGV X.org

* Sat Jun 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.192-1m)
- update 6.6.192
- import xf86-video-avivo(r5xx driver for Xorg) driver

* Sat Apr 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.191-3m)
- update radeon.xinf

* Tue Apr  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.191-2m)
- add xorg-x11-drv-ati-bts10442.patch
-- https://bugs.freedesktop.org/show_bug.cgi?id=10442

* Wed Mar 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.6.191-1m)
- update to 6.6.191

* Wed Oct  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.6.3-1m)
- update to 6.6.3

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.6.2-1m)
- update to 6.6.2

* Sun Jun 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.6.1-1m)
- update to 6.6.1

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.6.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.0-1m)
- update 6.6.0(Xorg-7.1RC1)

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.5.7.3-4m)
- import to Momonga

* Tue Feb 21 2006 Mike A. Harris <mharris@redhat.com> 6.5.7.3-4
- Added xorg-x11-drv-ati-6.5.7.3-radeon-metamodes-SEGV-fix.patch from CVS HEAD.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 6.5.7.3-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 6.5.7.3-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Sun Feb 05 2006 Mike A. Harris <mharris@redhat.com> 6.5.7.3-3
- Updated radeon.xinf to be up to date with the xf86PciInfo.h from the Xorg
  X server 1.0.1-1 source.  This should account for all supported Radeon
  models now modulo errors/omissions.

* Thu Feb 02 2006 Mike A. Harris <mharris@redhat.com> 6.5.7.3-2
- Added r128.xinf and radeon.xinf videoalias files to fix bug (#174101).
- Added "BuildRequires: libdrm-devel >= 2.0-1" to fix bug (#178613)

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 6.5.7.3-1
- Updated xorg-x11-drv-ati to version 6.5.7.3 from X11R7.0
- Added ati.xinf videoalias file for hardware autodetection.

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 6.5.7.2-1
- Updated xorg-x11-drv-ati to version 6.5.7.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 6.5.7-1
- Updated xorg-x11-drv-ati to version 6.5.7 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 6.5.6.1-1
- Updated xorg-x11-drv-ati to version 6.5.6.1 from X11R7 RC1
- Fix *.la file removal.
- Add "BuildRequires: mesa-libGL-devel >= 6.4-4 for DRI builds"

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 6.5.6-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Use _smp_mflags with make, to speed up SMP builds.
- Add "alpha sparc sparc64" to ExclusiveArch

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 6.5.6-0
- Initial spec file for ati video driver generated automatically
  by my xorg-driverspecgen script.
