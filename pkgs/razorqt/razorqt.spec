%global          momorel 1

%define qtxdglib libqtxdg
%define qtxdglibdevel libqtxdg-devel

Name:		razorqt
Version:	0.5.2
Release:	%{momorel}m%{?dist}
License:	GPL
URL:		http://razor-qt.org/
Source:		http://razor-qt.org/downloads/%{name}-%{version}.tar.bz2
NoSource:	0
Group:		User Interface/Desktops
Summary:	Razor a lightweight desktop toolbox
BuildRequires:	gcc 
BuildRequires:	cmake
BuildRequires:	make 
BuildRequires:	file-devel 
BuildRequires:	qt-devel >= 4.8.4
BuildRequires:	libXcomposite-devel
BuildRequires:	polkit-qt-devel
Requires:	%{name}-desktop, %{name}-panel, %{name}-session
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Razor's goal is to be a Desktop-Toolbox - a set of tools to improve user desktop 
experience.As mainstream desktop environments grows up to be more and more heavyweight
there is (as we believe) still place for a lightweight but nice and powerful 
set of tools to make user's desktop nicer and easier to use. A metapackage for Razor-qt DE. It will install all components.

%package	devel
Summary:	RazorQt development package
Group:		Development/Libraries
Requires:	%{name}-libraries
Obsoletes:	razorqt-x11info <= %{version}

%description	devel
A development environment for Razor-qt.

%package	libraries
Summary:	RazorQt shared library
Group:		System Environment/Libraries
Requires:	upower
# it's quite a big dependency - but there are manu bugreports with (I cannot see any
# icons...) and at least one icon theme has to be installed
Requires:	oxygen-icon-theme
# names before 0.4
Obsoletes:	razorqt-libs <= %{version}, librazorqt0 <= %{version}

%description	libraries
Base runtime libraries for Razor-qt DE

%package	-n %{qtxdglib}
Summary:	QtXdg library
Group:		System Environment/Libraries

%description	-n %{qtxdglib}
Implementation of XDG standards in Qt

%package	-n %{qtxdglibdevel}
Summary:	Development files for QtXdg library
Group:		Development/Libraries
Requires:	%{qtxdglib} = %{version}

%description    -n %{qtxdglibdevel}
A development environment for qtxdg.

%package	about
Summary:	GUI to provide basic info about DE
Group:		User Interface/X
Requires:	%{name}-data = %{version}

%description	about
A tool to display Razor-qt version, license, and technical background.

%package	appswitcher
Summary:	RazorQt application switcher
Group:		User Interface/X
Requires:	%{name}-data

%description	appswitcher
A alt+tab appliaction switcher for window managers where it is not available natively.

%package	desktop
Summary:	RazorQt desktop
Group:		User Interface/Desktops
Requires:	%{name}-data

%description	desktop
Razor-qt desktop implementation.

%package	notifications
Summary:	Razor-qt notification daemon
Group:		User Interface/Desktops
Requires:	%{name}-data = %{version}

%description	notifications
Desktop Independent notifications daemon from Razor-qt,

%package	openssh-askpass
Summary:	Razor-qt openssh ask password interface
Group:		User Interface/Desktops
Requires:	%{name}-data = %{version}

%description    openssh-askpass
Desktop Independent ssh ask password interface from Razor-qt,

%package	panel
Summary:	RazorQt panel
Group:		User Interface/Desktops
Requires:	%{name}-data
Requires:	xscreensaver

%description	panel
Razor-qt panel and its plugins.

%package	policykit-agent
Summary:	RazorQt policykit agent
Group:		System Environment/Libraries
BuildRequires:	polkit-devel
BuildRequires:	polkit-qt-devel

%description	policykit-agent
A lightweight PolicyKit agent primarily writen for Razor-qt DE. But it can be used standalone as well.

%package	power
Summary:	RazorQt power management apps
Group:		User Interface/Desktops
Requires:	%{name}-data

%description    power
Power management apps for Razor-qt DE

%package	data
Summary:	RazorQt resources and shared data
Group:		User Interface/Desktops
Obsoletes:	%{name}-resources <= %{version}

%description	data
Shared data for Razor-qt. For example: themes, images, etc.

%package	runner
Summary:	RazorQt runner application
Group:		User Interface/Desktops
Requires:	%{name}-data

%description	runner
Quick launch/runner application for Razor-qt.

%package	session
Summary:	RazorQt session
Group:		User Interface/Desktops
Requires:	%{name}-data
Requires:	openbox
Obsoletes:	%{name}-openbox <= %{version}, %{name}-wm <= %{version}, %{name}-eggwm <= %{version}

%description	session
Razor-qt session environment.

%package	config
Summary:	RazorQt config tools
Group:		User Interface/Desktops

%description	config
Razor-qt configuration GUI tools.

%package	autosuspend
Summary:	Razor-qt power management
Group:		User Interface/Desktops

%description	autosuspend
Razor-qt suspend manage application tool.

%package	globalkeyshortcuts
Summary:        Razor-qt global keyboard shortcuts tool
Group:		User Interface/Desktops

%description    globalkeyshortcuts
The Razor-qt global keyboard shortcuts desktop tool.

%prep
%setup -q -n %{name}-%{version}

%build
%{cmake} \
  -DCMAKE_INSTALL_PREFIX=/usr \
  .
%{make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}

%makeinstall DESTDIR=%{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post	libraries
ldconfig

%post	notifications
ldconfig

%post   -n %{qtxdglib}
ldconfig

%postun	libraries
ldconfig

%postun	notifications
ldconfig

%postun -n %{qtxdglib}
ldconfig

%files
%defattr(-,root,root,-)
%doc README

%files	libraries
%defattr(-,root,root,-)
%{_libdir}/librazor*.so.*
%{_datadir}/librazorqt

%files  -n %{qtxdglib}
%defattr(-,root,root,-)
%{_libdir}/libqtxdg.so.*
%{_datadir}/libqtxdg/

%files  -n %{qtxdglibdevel}
%defattr(-,root,root,-)
%{_libdir}/libqtxdg.so
%{_libdir}/pkgconfig/qtxdg.pc
%{_includedir}/qtxdg/

%files	devel
%defattr(-,root,root,-)
%{_libdir}/librazor*.so
%{_libdir}/pkgconfig/razor*.pc
%{_includedir}/razor*/
%{_bindir}/razor-x11info

%files	appswitcher
%defattr(-,root,root,-)
%{_bindir}/razor-appswitcher

%files	desktop
%defattr(-,root,root,-)
%{_bindir}/razor-desktop
%{_bindir}/razor-config-desktop
%{_libdir}/razor-desktop
%{_datadir}/applications/razor-config-desktop.desktop
%dir %{_datadir}/razor
%{_datadir}/razor/razor-desktop
%config(noreplace) %{_sysconfdir}/razor/desktop.conf

%files	about
%defattr(-,root,root,-)
%{_bindir}/razor-about
%{_datadir}/applications/razor-about.desktop

%files  openssh-askpass
%defattr(-,root,root,-)
%{_bindir}/razor-openssh-askpass
%{_datadir}/razor/razor-openssh-askpass

%files	notifications
%defattr(-,root,root,-)
%{_bindir}/razor-notificationd
%{_bindir}/razor-config-notificationd
%{_datadir}/applications/razor-config-notificationd.desktop
%{_datadir}/razor/razor-notificationd
%{_datadir}/razor/razor-config-notificationd

%files	panel
%defattr(-,root,root,-)
%{_bindir}/razor-panel
%{_libdir}/razor-panel
%{_datadir}/razor/razor-panel
%{_sysconfdir}/razor/razor-panel
%config(noreplace) %{_sysconfdir}/razor/razor-panel/panel*.conf

%files	policykit-agent
%defattr(-,root,root,-)
%{_bindir}/razor-policykit-agent
%{_datadir}/razor/razor-policykit-agent

%files	power
%defattr(-,root,root,-)
%{_bindir}/razor-power
%{_datadir}/razor/razor-power
%{_datadir}/applications/razor-power.desktop

%files	autosuspend
%defattr(-,root,root,-)
%{_bindir}/razor-autosuspend
%{_bindir}/razor-config-autosuspend
%{_datadir}/applications/razor-config-autosuspend.desktop
%{_datadir}/razor/razor-autosuspend
%{_datadir}/razor/razor-config-autosuspend
%dir %{_datadir}/icons/hicolor
%dir %{_datadir}/icons/hicolor/scalable
%dir %{_datadir}/icons/hicolor/scalable/apps
%{_datadir}/icons/hicolor/scalable/apps/razor-autosuspend.svg
%{_datadir}/icons/hicolor/scalable/apps/laptop-lid.svg

%files	runner
%defattr(-,root,root,-)
%{_bindir}/razor-runner
%{_datadir}/razor/razor-runner

%files  config
%defattr(-,root,root,-)
%{_bindir}/razor-config
%{_bindir}/razor-config-appearance
%{_bindir}/razor-config-mouse
%{_datadir}/applications/razor-config.desktop
%{_datadir}/applications/razor-config-appearance.desktop
%{_datadir}/applications/razor-config-mouse.desktop
%{_datadir}/applications/razor-config-qtconfig.desktop
%{_datadir}/razor/razor-config

%files	session
%defattr(-,root,root,-)
%{_bindir}/razor-session
%{_bindir}/razor-config-session
%{_bindir}/startrazor
%{_bindir}/razor-confupdate
%{_datadir}/xsessions/razor*.desktop
%dir %{_datadir}/apps
%dir %{_datadir}/apps/kdm
%dir %{_datadir}/apps/kdm/sessions
%{_datadir}/apps/kdm/sessions/razor*.desktop
%{_datadir}/applications/razor-config-session.desktop
%{_datadir}/razor/razor-session
%{_datadir}/razor/razor-config-session
%{_sysconfdir}/xdg/autostart/razor*.desktop
%config(noreplace) %{_sysconfdir}/razor/session.conf
%config(noreplace) %{_sysconfdir}/razor/windowmanagers.conf
%{_datadir}/razor/razor-confupdate
%{_libdir}/razor-confupdate_bin

%files	globalkeyshortcuts
%defattr(-,root,root,-)
%{_bindir}/razor-globalkeyshortcuts
%{_bindir}/razor-config-globalkeyshortcuts
%{_datadir}/applications/razor-config-globalkeyshortcuts.desktop
%{_datadir}/razor/razor-config-globalkeyshortcuts

%files	data
%defattr(-,root,root,-)
%{_datadir}/razor/graphics/
%{_datadir}/razor/themes/
%config %{_sysconfdir}/xdg/menus/razor-*.menu
%dir %{_sysconfdir}/xdg/menus
%{_datadir}/desktop-directories/razor*
%dir %{_datadir}/desktop-directories
%dir %{_sysconfdir}/razor
%config(noreplace) %{_sysconfdir}/razor/razor.conf
# temp files - it will be removed when it becomes part of upstream
%{_libdir}/razor-xdg-tools

%changelog
* Sun Jan 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2
- add some subpackages and cleanup spec file

* Mon Aug 27 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-2m)
- set Obsoletes: razorqt-eggwm

* Sun Aug 26 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.4.1-1m)
- version bump

* Sat Sep 10 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.3.0-3m)
- specfile corrections

* Sat Sep 10 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.3.0-2m)
- specfile corrections

* Fri Sep 09 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.3.0-1m)
- prep for initial momonga release
- Imported from Mandriva

* Thu Jun 09 2011 Falticska Florin <symbianflo@fastwebnet.it>  0.3.0-69mrb2010.2
- Imported from suse
- MRB-Mandriva Users.Ro

* Fri May 12 2011 Petr Vanek <petr@scribus.info> 0.3.0
- version bump. New packages structure

* Tue Feb 15 2011 TI_Eugene <ti.eugene@gmail.com> 0.2-206
- cleaning up spec
- openbox and eggwm session subpackages

* Mon Feb 07 2011 Petr Vanek <petr@scribus.info> 0.2-206
- suse fixes. Resources are split into library and resources package

* Sat Jan 29 2011 TI_Eugene <ti.eugene@gmail.com> 0.2-206
- appswitcher added

* Thu Jan 06 2011 TI_Eugene <ti.eugene@gmail.com> 0.2-190
- Next build

* Wed Mar 04 2009 TI_Eugene <ti.eugene@gmail.com> 0.1
- Initital build in OBS
