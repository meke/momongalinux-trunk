%global momorel 3
%global qtver 4.8.6
%global srcname QScintilla
%global qtdir %{_libdir}/qt4
%global src_ver 2.8

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: QScintilla is a port to Qt of Scintilla C++ editor class
Name: qscintilla
Version: %{src_ver}
Release: %{?snapshot_date:0.%{snapshot_date}.}%{momorel}m%{?dist}
License: GPLv2 and GPLv3
Group: System Environment/Libraries
URL: http://www.riverbankcomputing.co.uk/
Source0: http://downloads.sourceforge.net/project/pyqt/%{srcname}2/%{srcname}-%{version}/%{srcname}-gpl-%{version}%{?snapshot_date:-snapshot-%{snapshot_date}}.tar.gz
NoSource: 0
## Upstreamable patches
Patch50: QScintilla-gpl-2.7.1-qt4qt5_designer_incpath.patch
Patch51: QScintilla-gpl-2.7.2-api_path.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt >= %{qtver}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: PyQt4-devel >= 4.10.3
BuildRequires: chrpath
BuildRequires: python-devel >= 2.7
BuildRequires: sip-devel

%description
As well as features found in standard text editing components,
QScintilla includes features especially useful when editing and
debugging source code. These include support for syntax styling, error
indicators, code completion and call tips. The selection margin can
contain markers like those used in debuggers to indicate breakpoints
and the current line. Styling choices are more open than with many
editors, allowing the use of proportional fonts, bold and italics,
multiple foreground and background colours and multiple fonts.

%package devel
Summary: Libraries, include and other files to develop applications with QScintilla.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This packages contains the libraries, include and other files
you can use to develop applications with QScintilla.

%package designer
Summary: QScintilla designer plugin 
Group: Development/Tools
Requires: %{name} = %{version}-%{release}
Requires: qt-devel

%description designer
%{summary}.

%package python
Summary: QScintilla Qt4 python bindings
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: qt

%description python
%{summary}.

%prep 
%setup -q -n %{srcname}-gpl-%{src_ver}%{?snapshot_date:-snapshot-%{snapshot_date}}

%patch50 -p1 -b .qt4_designer_incpath
%patch51 -p1 -b .api_path

# fixups to support RPM_BUILD_ROOT/DESTDIR
sed -i "s/^DESTDIR = \$(QTDIR)/DESTDIR = \$(INSTALL_ROOT)\$(QTDIR)/" */*.pro

# fix permissions on doc files
find doc example-Qt3 example-Qt4Qt5  -type f -exec chmod 0644 {} ';'
find src include -type f -exec chmod 0644 {} ';'

%build
PATH=%{_qt4_bindir}:$PATH; export PATH

pushd Qt4Qt5
qmake-qt4 qscintilla.pro
make %{?_smp_mflags}
popd

pushd designer-Qt4Qt5
qmake-qt4 designer.pro
make %{?_smp_mflags}
popd

pushd Python
%{__python} configure.py \
            -c -j 3 \
            -n ../Qt4Qt5 \
            -o ../Qt4Qt5 \
            --no-timestamp
make %{?_smp_mflags}
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make -C Qt4Qt5 install INSTALL_ROOT=%{buildroot} 
make -C designer-Qt4Qt5 install INSTALL_ROOT=%{buildroot}
make -C Python install DESTDIR=%{buildroot} INSTALL_ROOT=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig
 
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc GPL_EXCEPTION*.TXT LICENSE.GPL* NEWS OPENSOURCE-NOTICE.TXT README
%{_libdir}/libqscintilla2.so.*
%{_datadir}/qt4/translations/qscintilla_*.qm

%files devel
%defattr(-,root,root,-)
%doc doc/html-Qt4Qt5 doc/Scintilla example-Qt4Qt5
%{_includedir}/Qsci
%{_libdir}/libqscintilla2.so
%{_libdir}/qt4/qsci

%files designer
%defattr(-,root,root,-)
%{_libdir}/qt4/plugins/designer/libqscintillaplugin.so

%files python
%defattr(-,root,root,-)
%doc LICENSE.GPL*
%{python_sitearch}/PyQt4/Qsci.so
%{_datadir}/sip/PyQt4/Qsci

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8-3m)
- rebuild against qt-4.8.6
- qscintilla links qt libraries dynamically, it does not depend on the qt version closely

* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8-2m)
- add REMOVE.PLEASE to link library correctly

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-2m)
- rebuild against qt-4.8.5

* Thu Jun 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-2m)
- rebuild against qt-4.8.3

* Fri Jun 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-3m)
- rebuild against qt-4.8.2

* Wed Apr  4 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.1-2m)
- rebuild against qt-4.8.1

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-5m)
- rebuild against qt-4.8.0

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-4m)
- rebuild against qt-4.7.4

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-3m)
- rebuild against qt-4.7.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.1-2m)
- rebuild for python-2.7

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux,org>
- (2.5.1-1m)
- update to 2.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5-1m)
- update to 2.5

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-2m)
- rebuild against qt-4.7.2

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.5-4m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.5-3m)
- rebuild against qt-4.7.1

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.5-2m)
- rebuild against PyQt4-4.7.7-2m

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.4-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-3m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-2m)
- rebuild against qt-4.7.0

* Sat Mar 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-3m)
- rebuild against qt-4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-2m)
- rebuild against qt-4.6.1

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Mon Nov 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-5m)
- rebuild against qt-4.6.0 once more

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-4m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-2m)
- rebuild against qt-4.5.2

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-0.20090525.1m)
- update to 2.4 snapshot 20090525

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-4m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-3m)
- update Patch0 for fuzz=0

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.2-2m)
- rebuild against python-2.6.1-1m

* Thu Nov 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-1m)
- version 2.3.2

* Sun Nov  9 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.1-1m)
- update to 2.3.1

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Sun Oct 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-5m)
- rebuild against qt-4.4.3-1m

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-4m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-3m)
- rebuild against gcc43

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-2m)
- rebuild against qt4-4.3.4

* Sun Mar  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-1m)
- version 2.2
- remove merged paths.patch
- License: GPLv2 and GPLv3

* Wed Feb  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-1m)
- version 2.1
- add 2 packages, qscintilla-designer and qscintilla-python
- import QScintilla-2-gpl-Qt4-incpath.patch from Fedora
- import qscintilla-1.73-gpl-2.1-paths.patch from Fedora

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.1-2m)
- rebuild against qt-3.3.7

* Tue Dec  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.1-1m)
- update to 1.7.1

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6-1m)
- update to 1.6

* Sun Mar 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5.1-1m)
- up to 1.5.1

* Fri Feb 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5-1m)
- v1.5

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4-1m)
- first import to Momonga (for scintilla supprot in PyQt)
- based on qscintilla-1.2-1mdk.src.rpm of Mandrake Cooker

#* Wed Aug 20 2003 Austin Acton <aacton@yorku.ca> 1.2-1mdk
#- 1.2
#- major 3

* Tue Jul 15 2003 Austin Acton <aacton@yorku.ca> 1.1-2mdk
- rebuild for rpm

* Fri May 23 2003 Austin Acton <aacton@yorku.ca> 1.1-1mdk
- new URL
- new version
- new major

* Fri May 02 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0-1mdk
- more mklibname
- from Jerome Martin <jerome.f.martin@free.fr> :
	- Version 1.0 (for sip/pyqt 3.6)

* Sat Mar  8 2003 Jerome Martin <jerome.f.martin@free.fr> 0.3-2mdk
- Fix pb

* Mon Dec  2 2002 Jerome Martin <jerome.f.martin@free.fr> 0.3-1mdk
- First release

