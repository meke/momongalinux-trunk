%global momorel 1
Name: libburn
Summary: libburn
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
Source0: http://files.libburnia-project.org/releases/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://libburnia-project.org/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig

%description
libburn is the library by which preformatted data get onto optical
media.  It uses either /dev/sgN (e.g. on kernel 2.4 with ide-scsi) or
/dev/srM or /dev/hdX (e.g. on kernel 2.6).  libburn is the foundation
of our cdrecord emulation. Its code is independent of cdrecord. Its
DVD capabilities are learned from studying the code of dvd+rw-tools
and MMC-5 specs. No code but only the pure SCSI knowledge has been
taken from dvd+rw-tools, though.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --program-prefix="" \
  --disable-static \
  --enable-libcdio
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr (-, root, root)
%doc AUTHORS COPYING COPYRIGHT ChangeLog NEWS README
%{_bindir}/cdrskin
%{_libdir}/%{name}.so.*
%exclude %{_libdir}/*.la
%{_mandir}/man1/cdrskin.1.*

%files devel
%defattr (-, root, root)
%{_includedir}/%{name}
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/libburn-1.pc

%changelog
* Thu Jun 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update to 1.2.2

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.8-1m)
- update to 1.1.8

* Sun Oct  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- initial build
