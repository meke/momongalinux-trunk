%global         momorel 1
%global         rapter2_ver 2.0.6
%global         rasqal_ver 0.9.28

Name:           redland
Version:        1.0.16
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Redland RDF Application Framework

Group:          System Environment/Libraries
License:        "LGPLv2+ or ASL 2.0"
URL:            http://librdf.org/
Source:         http://download.librdf.org/source/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libxml2-devel >= 2.4.0
BuildRequires:  curl-devel
BuildRequires:  rasqal-devel >= %{rasqal_ver}
BuildRequires:  raptor2-devel >= %{rapter2_ver}
BuildRequires:  libdb-devel >= 5.3.15
BuildRequires:  mysql-devel >= 5.5.10
BuildRequires:  sqlite-devel
BuildRequires:  postgresql-devel
BuildRequires:  gtk-doc
BuildRequires:  openldap-devel
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  yajl-devel >= 2.0.0

# to avoid /usr/lib64 rpath on x86_64
BuildRequires:  libtool

%description
Redland is a library that provides a high-level interface for RDF
(Resource Description Framework) implemented in an object-based API.
It is modular and supports different RDF/XML parsers, storage
mechanisms and other elements. Redland is designed for applications
developers to provide RDF support in their applications as well as
for RDF developers to experiment with the technology.

%package         devel
Summary:         Libraries and header files for programs that use Redland
Group:           Development/Libraries
Requires:        %{name} = %{epoch}:%{version}-%{release}
Requires:        raptor-devel >= %{rapter_ver}
Requires:        rasqal-devel >= %{rasqal_ver}
Requires:        pkgconfig

%description     devel
Header files for development with Redland.

%prep
%setup -q

%build
%if "%{_libdir}" != "/usr/lib"
sed -i -e 's|"/lib /usr/lib|"/%{_lib} %{_libdir}|' configure
%endif

%configure \
  --enable-release \
  --disable-static \
  --with-included-ltdl=no

# avoid getting /usr/lib64 rpath on x86_64 build
make LIBTOOL=/usr/bin/libtool %{?_smp_mflags}

%check
make check

%install
rm -rf --preserve-root %{buildroot}

make DESTDIR=%{buildroot} install
find %{buildroot} -name \*.la -exec rm {} \;

# remove .a files that we now get because of overriding libtool
find %{buildroot} -name \*.a -exec rm {} \;

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYING.LIB LICENSE.txt NEWS README
%doc LICENSE-2.0.txt NOTICE
%doc *.html
%{_libdir}/librdf*.so.*
%{_bindir}/rdfproc
%{_bindir}/redland-db-upgrade
%dir %{_datadir}/redland
%{_datadir}/redland/mysql-v1.ttl
%{_datadir}/redland/mysql-v2.ttl
%{_mandir}/man1/redland-db-upgrade.1*
%{_mandir}/man1/rdfproc.1*
%{_mandir}/man3/redland.3*
%dir %{_libdir}/redland
%{_libdir}/redland/librdf_storage_mysql.so
%{_libdir}/redland/librdf_storage_postgresql.so
%{_libdir}/redland/librdf_storage_sqlite.so
%{_libdir}/redland/librdf_storage_virtuoso.so

%files devel
%defattr(-,root,root,-)
%doc ChangeLog* RELEASE.html
%{_bindir}/redland-config
%{_libdir}/librdf*.so
%{_includedir}/redland.h
%{_includedir}/librdf.h 
%{_includedir}/rdf_*.h
%{_mandir}/man1/redland-config.1*
%{_libdir}/pkgconfig/redland.pc
%{_datadir}/redland/Redland.i
%{_datadir}/gtk-doc/html/*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.0.16-1m)
- update to 1.0.16

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.0.15-2m)
- rebuild against libdb-5.3.15

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.0.15-1m)
- update 1.0.15
- BR yajl

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1:1.0.13-2m)
- rebuild against libdb

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.0.13-1m)
- update to 1.0.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0.12-4m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.0.12-3m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0.12-2m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0.12-1m)
- update to 1.0.12

* Fri Oct 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.0.11-1m)
- update to 1.0.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.0.10-8m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.10-7m)
- add epoch to %%changelog

* Wed Jun  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0.10-6m)
- add LDFLAGS=-ldb to solve implicit linking issue

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.10-5m)
- explicitly link librasqal and libraptor

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.10-4m)
- rebuild against openssl-1.0.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.10-3m)
- rebuild against db-4.8.26

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.10-2m)
- move %%{_libdir}/redland to main

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.0.10-1m)
- update to 1.0.10, sync with Fedora devel
- now soprano and nepomuk work fine with redland-1.0.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0.8-4m)
- apply sqlite3617 patch

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:1.0.8-3m)
- add Epoch: 1 to enable upgrading on STABLE_6
- DO NOT REMOVE Epoch FOREVER

* Wed Jul 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-2m)
- specify --with-bdb

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- version down to 1.0.8, soprano and nepmuk does not work with 1.0.9

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9
- rebuild against rasqal-0.9.16

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-9m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-8m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-7m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.7-6m)
- rebuild against db4-4.7.25-1m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-5m)
- rebuild against openssl-0.9.8h-1m

* Mon Jun  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-4m)
- add BuildRequires: openldap-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-3m)
- rebuild against gcc43

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-2m)
- adjust duplicate files/directories

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-1m)
- import from Fedora devel and update to 1.0.7

* Tue Dec 04 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.0.6-3
- respin for openssl

* Tue Oct 16 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.6-2
- fix unpackaged files and unowned directory

* Tue Oct 16 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.6-1
- update to 1.0.6 (for Soprano 2, also some bugfixes)
- update minimum raptor and rasqal versions
- drop sed hacks for dependency bloat (#248106), fixed upstream

* Wed Aug 22 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.0.5-6
- respin (BuildID)

* Fri Aug 3 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.5-5
- specify LGPL version in License tag

* Sat Jul 14 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.5-4
- get rid of redland-config dependency bloat too (#248106)

* Sat Jul 14 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.5-3
- fix bug number in changelog

* Sat Jul 14 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.5-2
- add missing Requires: pkgconfig to the -devel package
- get rid of pkgconfig dependency bloat (#248106)

* Thu Jun 28 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.0.5-1
- update to 1.0.5 (1.0.6 needs newer raptor and rasqal than available)
- update minimum raptor version

* Fri Dec 15 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.0.4-3
- use DESTDIR

* Sat Jun 17 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.0.4-2
- fixed x86_64 rpath issue with an ugly hack
- removed OPTIMIZE from make invocation
- added smp flags
- added make check
- updated license

* Sun May 14 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.0.4-1
- update to new release, needs later raptor
- remove patch

* Sat Apr 08 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.0.3-1
- update to latest release
- include patch for fclose() double-free

* Sat Apr 08 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.0.2-1
- package for Fedora Extras

* Wed Feb 15 2006  Dave Beckett <dave@dajobe.org>
- Require db4-devel

* Thu Aug 11 2005  Dave Beckett <dave.beckett@bristol.ac.uk>
- Update Source:
- Do not require python-devel at build time
- Add sqlite-devel build requirement.
- Use configure and makeinstall

* Thu Jul 21 2005  Dave Beckett <dave.beckett@bristol.ac.uk>
- Updated for gtk-doc locations

* Mon Nov 1 2004  Dave Beckett <dave.beckett@bristol.ac.uk>
- License now LGPL/Apache 2
- Added LICENSE-2.0.txt and NOTICE

* Mon Jul 19 2004  Dave Beckett <dave.beckett@bristol.ac.uk>
- move perl, python packages into redland-bindings

* Mon Jul 12 2004  Dave Beckett <dave.beckett@bristol.ac.uk>
- put /usr/share/redland/Redland.i in redland-devel

* Wed May  5 2004  Dave Beckett <dave.beckett@bristol.ac.uk>
- require raptor 1.3.0
- require rasqal 0.2.0

* Fri Jan 30 2004  Dave Beckett <dave.beckett@bristol.ac.uk>
- require raptor 1.2.0
- update for removal of python distutils
- require python 2.2.0+
- require perl 5.8.0+
- build and require mysql
- do not build and require threestore

* Sun Jan 4 2004  Dave Beckett <dave.beckett@bristol.ac.uk>
- added redland-python package
- export some more docs

* Mon Dec 15 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- require raptor 1.1.0
- require libxml 2.4.0 or newer
- added pkgconfig redland.pc
- split redland/devel package shared libs correctly

* Mon Sep 8 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- require raptor 1.0.0
 
* Thu Sep 4 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- added rdfproc
 
* Thu Aug 28 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- patches added post 0.9.13 to fix broken perl UNIVERSAL::isa
 
* Thu Aug 21 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- Add redland-db-upgrade.1
- Removed duplicate perl CORE shared objects

* Sun Aug 17 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- Updates for new perl module names.

* Tue Apr 22 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- Updated for Redhat 9, RPM 4

* Fri Feb 12 2003 Dave Beckett <dave.beckett@bristol.ac.uk>
- Updated for redland 0.9.12

* Fri Jan 4 2002 Dave Beckett <dave.beckett@bristol.ac.uk>
- Updated for new Perl module names

* Fri Sep 14 2001 Dave Beckett <dave.beckett@bristol.ac.uk>
- Added shared libraries
