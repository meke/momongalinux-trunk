%global momorel 10

Summary: A program which will display a fortune.
Name: fortune-mod
Version: 1.99.1
Release: %{momorel}m%{?dist}
License: BSD
Group: Amusements/Games
Source0: http://www.redellipse.net/code/downloads/%{name}-%{version}.tar.gz 
Source1: kernelnewbies-fortunes.tar.gz
Source2: bofh-excuses.tar.bz2
Source3: fortune-tao.tar.gz
Source4: fortune-hitchhiker.tgz
Source5: osfortune.tar.gz
Source6: http://humorix.org/downloads/humorixfortunes-1.4.tar.gz
Patch0: fortune-mod-offense.patch
Patch1: fortune-mod-1.99-remove-offensive-option.patch
Patch2: fortune-mod-cflags.patch
Patch3: fortune-mod-1.99-move-offensive.patch
URL: http://www.redellipse.net/code/fortune/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: recode-devel

%description
Fortune-mod contains the ever-popular fortune program, which will
reveal a pithy quote or bit of advice. Fun-loving system
administrators can add fortune to users' .login files, so that the
users get their dose of wisdom each time they log in.

Install fortune if you want a program which will bestow these random
bits o' wit.

%prep
%setup -q
%patch0 -p1 -b .offense
%patch1 -p0 -b .remove-offensive-option
%patch2 -p1 -b .cflags
%patch3 -p0 -b .move-offensive

%build
make COOKIEDIR=%{_datadir}/games/fortunes FORTDIR=%{_bindir} BINDIR=%{_sbindir} 

%install
rm -rf %{buildroot}
# mkdir -p %{buildroot}/usr/{games,sbin,man/man1,man/man6,share/games/fortune}

make	COOKIEDIR=%{_datadir}/games/fortunes fortune/fortune.man
make	FORTDIR=%{buildroot}%{_bindir} \
	COOKIEDIR=%{buildroot}%{_datadir}/games/fortunes \
	BINDIR=%{buildroot}%{_sbindir} \
	BINMANDIR=%{buildroot}%{_mandir}/man1 \
	FORTMANDIR=%{buildroot}%{_mandir}/man6 \
	install

tar zxvf %{SOURCE1} -C %{buildroot}%{_datadir}/games/fortunes
rm -f %{buildroot}%{_datadir}/games/fortunes/men-women*

# remove debian...
rm -f %{buildroot}%{_datadir}/games/fortunes/debian*
rm -f %{buildroot}%{_datadir}/games/fortunes/off/debian*

bzcat %{SOURCE2} | tar xvf - -C %{buildroot}%{_datadir}/games/fortunes

# Non-standard source files, need to move things around
tar zxvf %{SOURCE3} -C %{buildroot}%{_datadir}/games/fortunes/ fortune-tao/tao*
mv %{buildroot}%{_datadir}/games/fortunes/fortune-tao/* %{buildroot}%{_datadir}/games/fortunes/
rmdir %{buildroot}%{_datadir}/games/fortunes/fortune-tao

tar zxvf %{SOURCE4} -C %{buildroot}%{_datadir}/games/fortunes/ fortune-hitchhiker/hitch*
mv %{buildroot}%{_datadir}/games/fortunes/fortune-hitchhiker/* %{buildroot}%{_datadir}/games/fortunes/
rmdir %{buildroot}%{_datadir}/games/fortunes/fortune-hitchhiker

tar zxvf %{SOURCE5} -C %{buildroot}%{_datadir}/games/fortunes/
chmod 644 %{buildroot}%{_datadir}/games/fortunes/osfortune*

tar zxvf %{SOURCE6} -C %{buildroot}%{_datadir}/games/fortunes/ humorixfortunes-1.4/*
mv %{buildroot}%{_datadir}/games/fortunes/humorixfortunes-1.4/* %{buildroot}%{_datadir}/games/fortunes/
rmdir %{buildroot}%{_datadir}/games/fortunes/humorixfortunes-1.4

# Recreate random access files for the added fortune files.
for i in \
    kernelnewbies bofh-excuses tao hitchhiker \
    osfortune humorix-misc humorix-stories \
; do util/strfile %{buildroot}%{_datadir}/games/fortunes/$i ; done

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog README TODO
%{_bindir}/fortune
%{_sbindir}/strfile
%{_sbindir}/unstr
%{_datadir}/games/fortunes
%{_mandir}/man*/*

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.99.1-10m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.1-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.1-3m)
- %%NoSource -> NoSource

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.1-2m)
- move fortune from /usr/games to %%{_bindir}
- import cflags.patch from Fedora

* Mon May 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.99.1-1m)
- update to 1.99.1
- import Source1-Source6 from FC
- import Patch0-Patch2 from FC

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0-17m)
- rebuild against new environment.

* Thu Feb 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-16k)
- fix URL

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.0-11).

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Jun 25 1999 Guido Flohr <gufl0000@stud.uni-sb.de>
- create fortune manpage without buildroot before installation

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 9)

* Thu Dec 17 1998 Michael Maher <mike@redhat.com>
- rebuilt for 6.0

* Sat Aug 15 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Oct 21 1997 Donnie Barnes <djb@redhat.com>
- new version
- spec file cleanups

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
