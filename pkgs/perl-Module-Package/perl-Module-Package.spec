%global         momorel 7

Name:           perl-Module-Package
Version:        0.30
Release:        %{momorel}m%{?dist}
Summary:        Postmodern Perl Module Packaging
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Module-Package/
Source0:        http://www.cpan.org/authors/id/I/IN/INGY/Module-Package-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.3
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO-All >= 0.41
BuildRequires:  perl-Module-Install >= 1.01
BuildRequires:  perl-Module-Install-AuthorRequires >= 0.02
BuildRequires:  perl-Module-Install-ManifestSkip >= 0.19
BuildRequires:  perl-Moo >= 0.009008
Requires:       perl-Cwd
Requires:       perl-IO-All >= 0.41
Requires:       perl-Module-Install >= 1.01
Requires:       perl-Module-Install-AuthorRequires >= 0.02
Requires:       perl-Module-Install-ManifestSkip >= 0.19
Requires:       perl-Moo >= 0.009008
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is a dropin replacement for Module::Install. It does everything
Module::Install does, but just a bit better.

%prep
%setup -q -n Module-Package-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/inc/Module/Package.pm
%{perl_vendorlib}/inc/Module/Package.pod
%{perl_vendorlib}/Module/Install/Package.pm
%{perl_vendorlib}/Module/Install/Package.pod
%{perl_vendorlib}/Module/Package
%{perl_vendorlib}/Module/Package.pm
%{perl_vendorlib}/Module/Package.pod
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.16.2

* Wed Sep 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
