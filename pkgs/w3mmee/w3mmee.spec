%global momorel 42
%define debug 0
%define w3mver 0.3.2
%define meever 24
%define meesubver 22
%define libmoever 1.5.8

Summary: WWW wo Miru Tool with Multiple character Encoding Extension
Name: w3mmee
Version: %{w3mver}
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/Internet
#Source0: http://pub.ks-and-ks.ne.jp/prog/pub/%{name}-p%{meever}.tar.gz
Source0: http://pub.ks-and-ks.ne.jp/prog/pub/%{name}-p%{meever}-%{meesubver}.tar.gz
#Source0: http://pub.ks-and-ks.ne.jp/prog/pub/%{name}-devel.tar.gz
NoSource: 0
Source1: w3mmee.config.param
Source2: w3mmee.config.param.lib64
%if %{debug}
Patch0: w3mmee.patch
%endif
Patch10: w3mman2html.patch
Patch11: w3mmee-p24-22-lib64.patch
Patch12: w3mmee-p24-22-gcc44.patch
Patch20: w3mmee-p24-22-CVE-2010-2074.patch
Patch21: w3m-0.5.2-ssl_verify_server_on.patch
Patch30: w3mmee-p24-22-file_handle.patch
URL: http://pub.ks-and-ks.ne.jp/prog/w3mmee/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: webclient, paer
Obsoletes: w3m-dev
BuildRequires: libmoe >= %{libmoever}, openssl-devel >= 1.0.0
BuildRequires: imlib-devel >= 1.9.14-4k
BuildRequires: ncurses-devel
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: gc-devel >= 6.2-9m
BuildRequires: gpm-devel >= 1.20.5
Requires: libmoe >= %{libmoever}, openssl, ncurses
Requires: jed
Requires(post): chkconfig
Requires(postun): chkconfig

%description
A variant of w3m with support for multiple character encodings etc.

%package img
Summary: image display supplement for w3mmee
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}, imlib >= 1.9.8

%description img
image display supplement for w3mmee.

%package cgi
Summary: dirlist cgi supplement for w3mmee
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}, perl

%description cgi
internal cgi supplement for w3mmee.

%prep
%__rm -rf %{buildroot}

#%setup -q -n w3mmee-p%{meever}
%setup -q -n w3mmee-p%{meever}-%{meesubver}
#%setup -q -n w3mmee-devel
%if %{debug}
%patch0 -p0
%endif
%patch10 -p1 -b .man2html
%if %{_lib} == "lib64"
%patch11 -p1 -b .lib64~
%__cp %{SOURCE2} config.param
%else
%__cp %{SOURCE1} config.param
%endif
%patch12 -p1 -b .gcc44~

%patch20 -p1 -b .CVE-2010-2074
%patch21 -p1 -b .ssl_verify_server 

%patch30 -p1 -b .file_handle

%build
CFLAGS=`echo %{optflags} | sed -e 's/-fomit-frame-pointer//g' -e 's/-fstack-protector//g'`
./configure -cflags="$CFLAGS" --gc-ldflags="-ldl" -yes -cpp="cpp -P"
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

make install DESTDIR="%{buildroot}"

# remove for alternatives.
rm -f %{buildroot}%{_mandir}/ja/man1/w3m.1*
rm -f %{buildroot}%{_mandir}/man1/w3m.1*

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/usr/sbin/alternatives --install %{_bindir}/pager pager %{_bindir}/w3mmee 50

%postun
if [ $1 = 0 ]; then
	/usr/sbin/alternatives --remove pager %{_bindir}/w3mmee
	/usr/sbin/alternatives --auto pager
fi

%files
%defattr(-, root, root)
%{_sysconfdir}/w3mmee
%{_bindir}/w3mmee
%dir %{_libdir}/w3mmee
%{_libdir}/w3mmee/w3mbookmark
%{_libdir}/w3mmee/w3mhelperpanel
%{_datadir}/w3mmee
%{_datadir}/locale/*/LC_MESSAGES/*
%doc 00INCOMPATIBLE.html ChangeLog NEWS* README doc-jp doc

%files img
%defattr(-, root, root)
%{_libdir}/w3mmee/w3mimgdisplay

%files cgi
%defattr(-, root, root)
%{_bindir}/w3mmeeman
%{_libdir}/w3mmee/dirlist.cgi
%{_libdir}/w3mmee/hlink.cgi
%{_libdir}/w3mmee/multipart.cgi
%{_libdir}/w3mmee/w3mhelp.cgi
%{_libdir}/w3mmee/w3mmail.cgi
%{_libdir}/w3mmee/w3mman2html.cgi
%{_libdir}/w3mmee/xface2xpm
%{_mandir}/man1/w3mmeeman.1*

%changelog
* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-42m)
- enable to build with glibc-2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-41m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-40m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-39m)
- full rebuild for mo7 release

* Wed Jun 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-38m)
- [SECURITY] CVE-2010-2074
- import security patches from Fedora 13 w3m (0.5.2-18)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-37m)
- use BuildRequires

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-36m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-35m)
- revise for gcc442

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-34m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-33m)
- rebuild against openssl-0.9.8k

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-32m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-31m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-30m)
- rebuild against gpm-1.20.5

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-29m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-28m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.2-27m)
- rebuild against perl-5.10.0-1m

* Thu Jul 20 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-26m)
- remove '-fstack-protector' from CFLAGS

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.2-25m)
- rebuild against openssl-0.9.8a

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.3.2-24m)
- rebuild against libtermcap and ncurses

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.3.2-23m)
- enable x86_64.

* Tue Feb  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-22m)
- update to w3mmee-p24-22
- remove 'Provides: w3m'
- remove w3mmee-p24-20-gcc34.patch
- remove w3m-external-gc.patch

* Wed Nov  3 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.3.2-21m)
- add patch for gcc 3.4

* Sat Jun 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-20m)
- cancel alternatives of w3m

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.2-19m)
- rebuild against ncurses 5.3.

* Wed Mar 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-18m)
- update to w3mmee-p24-20

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.2-17m)
- revised spec for rpm 4.2.

* Tue Oct 28 2003 zunda <zunda at freeshell.org>
- (0.3.2-16m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft):
  scrsize.c is under MIT/X license.

* Sat Oct 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-15m)
- disable xface

* Mon Sep 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-14m)
- update BuildPreReq gc-devel >= 6.2-9m

* Wed Jun 20 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.2-13m)
- use external gc library.

* Wed Jun 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.3.2-12m)
- update Boehm gc to 6.2alpha6

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.3.2-11m)
  rebuild against openssl 0.9.7a

* Fri Mar  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-10m)
- 20030305 snapshot

* Sat Feb 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-9m)
- add requires jed
- delete Requires gc
- delete BuildPreReq gc-devel

* Sat Feb 22 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.2-8m)
- use internal boehm-gc.

* Sat Jan 11 2003 Kenta MURATA <muraken@momonga-linux.org>
- (0.3.2-7m)
- fix w3mman2html.cgi

* Fri Dec  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-6m)
- w3mmee-p24-18
- security fix

* Fri Nov 29 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-5m)
- add version at requires chkconfig for alternatives

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-4m)
- w3mmee-p24-17
- security fix

* Wed Nov 22 2002 OZAWA -Crouton- Sakuro <crouton@momona-linux.org>
- (0.3.2-3m)
- fix postun script syntax error.

* Wed Nov 20 2002 OZAWA -Crouton- Sakuro <crouton@momona-linux.org>
- (0.3.2-2m)
- adopt the alternatives system (requires: chkconfig, provides: pager, w3m).
- fix Release:.

* Tue Nov  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-1m)
- w3mmee-p24-16

* Sat Oct  5 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.3.1-4m)
- fix build problem on gcc-3.X environment (Patch100: w3m-gc.patch)
  gc requires -ldl

* Tue Oct  1 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-3m)
- cvs stable version

* Sat Aug 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-2m)
- revise external browser setting

* Wed Jul 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-1m)
- p24-15

* Fri May 17 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-42k)
- p24-14

* Mon May 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-40k)
- p24-13

* Thu May  9 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-38k)
- p24-12

* Thu May  9 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-36k)
- p24-11

* Mon May  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-34k)
- p24-10

* Thu Apr 25 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-32k)
- p24-6

* Wed Apr 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-30k)
- p24-4

* Wed Apr 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-28k)
- p24-3

* Sun Apr 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-26k)
- p24-1

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.3-24k)
- BuildPreReq: imlib-devel >= 1.9.14-4k

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.3-22k)
- rebuild against libpng 1.2.2.

* Thu Apr 11 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-20k)
- p23-3

* Mon Apr  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-18k)
- p23-2

* Wed Apr  3 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-16k)
- p23-1

* Tue Apr  2 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-14k)
- p23

* Tue Mar 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-12k)
- p22-4
- revise requirements

* Sun Mar 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-10k)
- p22-3

* Fri Mar 22 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-8k)
- p22-2

* Thu Mar 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-6k)
- p22-1

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-4k)
- p22

* Mon Feb 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-2k)
- p21-26

* Fri Feb 15 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.5-14k)
- p21-18

* Fri Feb  8 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.2.5-12k)
- a tar ball was not found, it upgraded.
  Source0 was included.

* Wed Feb  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.5-10k)
- p21-9

* Sat Feb  2 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.5-2k)
- 0.2.5-p21-1
- never requires migemo-cs
- now requires imlib (w3mmee-img package)

* Tue Jan 22 2002 Toru Hoshina <t@kondara.org>
- (0.2.4-0.0020006k)
- don't need imlib...

* Sat Jan 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.4-0.0020004k)
- never provide w3m

* Wed Jan  9 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.4-0.0020002k)
- p20-8

* Tue Jan  1 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.3-0.0020008k)
- rename w3mman to w3mmeeman to avoid a conflict with w3m-cgi package

* Tue Dec 25 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.3-0.0020006k)
- p20-6

* Sat Dec 22 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.3-0.0020004k)
- p20-5

* Fri Dec 21 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.3-0.0020002k)
- w3mmee-p20-4

* Mon Dec 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.2-0.0020002k)
- 0.2.2-p20-3

* Sun Dec  9 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019028k)
- apply a security patch for frame.c

* Tue Dec  3 2001 Toru Hoshina <t@kondara.org>
- (0.2.1-0.0019026k)
- dirlist.cgi...

* Fri Nov 30 2001 Toru Hoshina <t@kondara.org>
- (0.2.1-0.0019024k)
- provide webclient as a virtual package.
- chmod -x dirlist.cgi

* Thu Nov 22 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019022k)
- fix a problem in ja_JP.EUC-JP locale

* Fri Nov  9 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019020k)
- p19-7

* Wed Oct 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019018k)
- p19-6

* Tue Oct 30 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019016k)
- p19-5
- remove post-header.patch because it has been merged

* Mon Oct 29 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019014k)
- apply post-header.patch from emacs-w3m/patches

* Fri Oct 26 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.2.1-0.0019012k)
- fix Source0 url

* Wed Sep 26 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019010k)
- p19-4

* Wed Jun 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019009k)
- p19-3

* Thu Jun  7 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019007k)
- p19-2

* Wed Jun  6 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019005k)
- p19-1

* Tue Jun  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0019003k)
- w3mmee-0.2.1-p19

* Sat Jun  2 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0018005k)
- w3mmee-0.2.1-p18-8

* Sun May 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-0.0018003k)
- update to w3m-0.2.1, w3mmee-0.2.1-p18-7

* Sat Mar 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11-0.0017009k)
- w3mmee-0.1.11p17-7

* Tue Mar 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11-0.0017003k)
- rename config.param to w3mmee.config.param

* Thu Mar  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11-0.0016011k)

* Tue Jan 23 2001 Kazuhiko <kazuhiko@kondara.org>
- remove wheel mouse patch (merged to w3mmee)
- remove conf_any_host patch (merged to w3mmee)

* Wed Dec  6 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11-0.0013014003k)

* Mon Dec  4 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11-0.0013013003k)
- sync to new specfile guidance

* Tue Nov 28 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13_11-1k)
- add meesubver information to Version
- remove zettaihendazo.patch again.
- remove w3mmee-0.1.11p13-7.intllib.patch

* Sun Nov 26 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-15k)
- configure script always read config.param on ANY host

* Sun Nov 26 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-13k)
- move architecture-independent data to /usr/share/w3mmee

* Sat Nov 25 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-11k)
- never use interactive configure to avoid potential risk of looping

* Fri Nov 24 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-9k)
- revive zettaihendazo.patch SORRY.

* Fri Nov 24 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-7k)
- I APOLOGISE FOR MY MISTAKE IN PREVIOUS RELEASE
- more strict checking for the existence of patch file
- change libiso2mb header file location to /usr/include/iso2mb
- remove w3mmee-0.1.11p13-6.zettaihendazo.patch to avoid build failure
- change BuildPreReq libiso2mb version to 0.8.1-3k because header file
  location is changed.

* Thu Nov 23 2000 Toru Hoshina <toru@df-usa.com>
- DO NOT USE NOPATCH for w3mmee.diff.gz anyway, because if appropriate
  patch file couldn't found, this would couse disk full!! don't you
  understand? if so, I suggest you don't revise spec file any more.

* Wed Nov 22 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-3k)
- add ChangeLog to %doc

* Tue Nov 21 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p13-1k)

* Fri Oct 20 2000 Kazuhiko <kazuhiko@kondara.org>
- (0.1.11p10-3k)
- change Copyright tag to License tag
- update source to 2000-10-19 16:18:03 version
- modify and add wheel patch

* Tue Oct 17 2000 Kazuhiko <kazuhiko@kondara.org>
- (w3mmee-0.1.11p10-1k)
- remove wheel patch temporally
