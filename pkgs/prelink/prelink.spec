%global momorel 1
Summary: An ELF prelinking utility
Name: prelink
Version: 0.5.0
Release: %{momorel}m%{?dist}
%global svnver 205
License: GPLv2+
Group: System Environment/Base
%define date 20130503
# svn export svn://sourceware.org/svn/prelink/trunk@%{svnver} prelink
# tar cf - prelink | bzip2 -9 > prelink-%{date}.tar.bz2
Source: http://people.redhat.com/jakub/prelink/prelink-%{date}.tar.bz2
NoSource: 0
Source2: prelink.conf
Source3: prelink.cron
Source4: prelink.sysconfig
Patch0: prelink-armhf-dynamic-linker.patch

BuildRequires: elfutils-libelf-devel-static
BuildRequires: libselinux-static, libselinux-utils
BuildRequires: glibc-static
Requires: glibc >= 2.2.4-18, coreutils, findutils
Requires: util-linux, gawk, grep
# For now
ExclusiveArch: %{ix86} alpha sparc sparcv9 sparc64 s390 s390x x86_64 ppc ppc64 %{arm}

%description
The prelink package contains a utility which modifies ELF shared libraries
and executables, so that far fewer relocations need to be resolved at runtime
and thus programs come up faster.

%prep
%setup -q -n prelink

# We have two possible dynamic linkers on ARM (soft/hard float ABI). For now,
# specifically patch the name of the linker on hard float systems. FIXME.
%ifarch armv7hl
%patch0 -p1 -b .armhfp-dynamic-linker
%endif

%build
sed -i -e '/^prelink_LDADD/s/$/ -lpthread/' src/Makefile.{am,in}
%configure --disable-shared
%make
echo ====================TESTING=========================
make -C testsuite check-harder
make -C testsuite check-cycle
echo ====================TESTING END=====================

%install
%{makeinstall}
mkdir -p %{buildroot}%{_sysconfdir}/rpm
cp -a %{SOURCE2} %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_sysconfdir}/{sysconfig,cron.daily,prelink.conf.d}
cp -a %{SOURCE3} %{buildroot}%{_sysconfdir}/cron.daily/prelink
cp -a %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/prelink
chmod 755 %{buildroot}%{_sysconfdir}/cron.daily/prelink
chmod 644 %{buildroot}%{_sysconfdir}/{sysconfig/prelink,prelink.conf}
cat > %{buildroot}%{_sysconfdir}/rpm/macros.prelink <<"EOF"
# rpm-4.1 verifies prelinked libraries using a prelink undo helper.
#       Note: The 2nd token is used as argv[0] and "library" is a
#       placeholder that will be deleted and replaced with the appropriate
#       library file path.
%%__prelink_undo_cmd     /usr/sbin/prelink prelink -y library
EOF
chmod 644 %{buildroot}%{_sysconfdir}/rpm/macros.prelink
mkdir -p %{buildroot}%{_mandir}/man5
echo '.so man8/prelink.8' > %{buildroot}%{_mandir}/man5/prelink.conf.5
chmod 644 %{buildroot}%{_mandir}/man5/prelink.conf.5

mkdir -p %{buildroot}/var/{lib,log}/prelink
touch %{buildroot}/var/lib/prelink/full
touch %{buildroot}/var/lib/prelink/quick
touch %{buildroot}/var/lib/prelink/force
touch %{buildroot}/var/log/prelink/prelink.log

#prelink wreaks havoc on sparc systems lets make sure its disabled by default there
%ifarch %{sparc}
sed -i -e 's|PRELINKING=yes|PRELINKING=no|g' %{buildroot}%{_sysconfdir}/sysconfig/prelink
%endif

%post
touch /var/lib/prelink/force

%files
%defattr(-,root,root)
%doc doc/prelink.pdf
%verify(not md5 size mtime) %config(noreplace) %{_sysconfdir}/prelink.conf
%verify(not md5 size mtime) %config(noreplace) %{_sysconfdir}/sysconfig/prelink
%{_sysconfdir}/rpm/macros.prelink
%dir %attr(0755,root,root) %{_sysconfdir}/prelink.conf.d
%{_sysconfdir}/cron.daily/prelink
%{_prefix}/sbin/prelink
%{_prefix}/bin/execstack
%{_mandir}/man5/prelink.conf.5*
%{_mandir}/man8/prelink.8*
%{_mandir}/man8/execstack.8*
%dir /var/lib/prelink
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/lib/prelink/full
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/lib/prelink/quick
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/lib/prelink/force
%dir /var/log/prelink
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/log/prelink/prelink.log

%changelog
* Tue Jul 23 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-2m)
- update to 20120628
- reimport from fedora

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-1m)
- update to 20111012

* Wed Jul 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-2m)
- update to 20110622
- rebuild against bintuils-2.21.52.0.2-2m 

* Wed Jun  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-2m)
- rebuild for new GCC 4.6

* Tue Feb 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-1m)
- update 0.4.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.3-2m)
- full rebuild for mo7 release

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3 (20100413)
- sync with Fedora devel
--
-- * Fri Apr 23 2010 Jakub Jelinek <jakub@redhat.com> 0.4.3-3
-- - move /var/lib/misc/prelink.{force,quick,full} to
--   /var/lib/prelink/{force,quick,full} (#584319)
--
-- * Tue Apr 13 2010 Jakub Jelinek <jakub@redhat.com> 0.4.3-2
-- - run restorecon on /var/lib/misc/prelink.force (#581959)
--
-- * Tue Apr 13 2010 Jakub Jelinek <jakub@redhat.com> 0.4.3-1
-- - add support for prelink -u -o - library
-- - add DWARF4 support
-- - fix up reloc{8,9}.sh for sparc64, reenable testing on sparc64
--
-- * Mon Apr 12 2010 Jakub Jelinek <jakub@redhat.com> 0.4.2-7
-- - BuildReq libselinux-utils (#571724)
-- - handle R_390_{PC32DBL,16,PC16,PC16DBL,8} relocations in 31-bit objects
--   (#552635)
-- - add prelink.conf(5) man page as a link to prelink(8) (#528933)
--
-- * Mon Mar 08 2010 Dennis Gilmore <dennis@ausil.us> 0.4.2-6
-- - disable tests on sparc64 bz#571551
-- - disable prelink from running on sparc arches
--
-- * Wed Dec 16 2009 Jakub Jelinek <jakub@redhat.com> 0.4.2-5
-- - change textrel tests, so that even if getenforce exists, but
--   fails, textrel tests aren't run
--
-- * Wed Nov  4 2009 Jakub Jelinek <jakub@redhat.com> 0.4.2-4
-- - add support for STT_GNU_IFUNC on ppc/ppc64, R_PPC_IRELATIVE and
--   R_PPC64_{IRELATIVE,JMP_IREL}

* Mon May 3 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- rebuild against gcc-4.4.4
- build fix for ImplicitDSOLinking-Matsuri

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-4m)
- sync with Rawhide
-- * Fri Sep 25 2009 Jakub Jelinek <jakub@redhat.com> 0.4.2-3
-- - fix DW_OP_implicit_value handling

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-1m)
- sync with Rawhide
-- 
-- * Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.2-2
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Thu Jul  9 2009 Jakub Jelinek <jakub@redhat.com> 0.4.2-1
-- - don't look for *_IRELATIVE relocations in .gnu.conflict section in binaries
-- 
-- * Sun Jul  5 2009 Jakub Jelinek <jakub@redhat.com> 0.4.1-1
-- - add support for STT_GNU_IFUNC on i?86/x86_64 and R_{386,X86_64}_IRELATIVE
-- - add support for DWARF3/DWARF4 features generated newly by recent
--   gccs
-- - temporarily link prelink against -lpthread to workaround -lselinux
--   issue
-- 
-- * Wed Mar 11 2009 Jakub Jelinek <jakub@redhat.com> 0.4.0-7
-- - fix prelinking on ppc64
-- 
-- * Tue Mar 10 2009 Jakub Jelinek <jakub@redhat.com> 0.4.0-6
-- - BuildRequire glibc-static
-- - rebuilt with gcc 4.4
-- - sparc64 and ARM TLS support
-- 
-- * Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-5
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Fri Aug 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.4.0-4
-- - fix license tag
-- 
-- * Tue Apr  8 2008 Jakub Jelinek <jakub@redhat.com> 0.4.0-3
-- - BuildRequire libselinux-static rather than libselinux-devel (#440749)

* Thu May 28 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.0-4m)
- libtool build fix

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc43

* Wed Oct 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.0-1)
- import from fc-devel
- add -D_GNU_SOURCE to CFLAGS for glibc-2.7
-* Tue Oct  9 2007 Jakub Jelinek <jakub@redhat.com> 0.4.0-1
-- add support for -c /etc/prelink.conf.d/*.conf in prelink.conf (#244452)  
-- remove no longer existent directories from default prelink.conf (#248694)
-- reenabled prelink C++ optimizations which have not triggered any longer
  since _ZT[IV]* moved into .data.rel.ro section from .data
-- fixed performance issues in C++ optimizations, especially on GCJ
  CNI programs (#314051) 
-- other performance improvements, prelink -avmR after prelink -ua now
  takes roughly 3m4s real time instead of 20m27s before
- don't run TEXTREL tests if SELinux is enforcing (#245928)
