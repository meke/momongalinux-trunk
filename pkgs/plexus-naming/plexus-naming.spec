%global momorel 4

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define with_maven 0

%define namedversion 1.0-alpha-3

%define parent plexus
%define subname naming

Name:           plexus-naming
Version:        1.0
Release:        0.1.%{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus Naming Component
License:        "ASL 2.0"
Group:          Development/Tools
URL:            http://plexus.codehaus.org/

# svn export \
# http://svn.codehaus.org/plexus/plexus-components/tags/plexus-naming-1.0-alpha-3/
# tar czf plexus-naming-1.0-alpha-3-src.tar.gz plexus-naming-1.0-alpha-3
Source0:        %{name}-%{namedversion}-src.tar.gz
Source1:        plexus-naming-1.0-build.xml
Source3:        plexus-naming-1.0-jpp-depmap.xml
Source4:        plexus-naming-components.xml

Patch0:         plexus-naming-1.0-pom.patch
# Some license headers were missing from the .java files.  They have
# been added in SVN trunk and we've back-ported that fix.
# http://jira.codehaus.org/browse/PLXCOMP-144
# http://jira.codehaus.org/secure/attachment/44043/plexus-naming-addlicenseheaders.patch
Patch1:         %{name}-addlicenseheaders.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  jpackage-utils >= 0:1.7.3
BuildRequires:  java-devel >= 0:1.6.0
BuildRequires:  ant >= 0:1.6.5
BuildRequires:  junit
BuildRequires:  hsqldb
BuildRequires:  jakarta-commons-logging
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-common-poms
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-release
BuildRequires:  maven2-plugin-surefire = 2.3
BuildRequires:  maven-surefire-provider-junit = 2.3
BuildRequires:  plexus-maven-plugin >= 1.3.5
BuildRequires:  maven-doxia
BuildRequires:  maven-doxia-sitetools
BuildRequires:  plexus-maven-plugin >= 1.3.5
BuildRequires:  qdox >= 1.5
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  avalon-logkit
BuildRequires:  avalon-framework
%endif
BuildRequires:  directory-naming
BuildRequires:  avalon-framework
BuildRequires:  jakarta-commons-collections
BuildRequires:  jakarta-commons-dbcp
BuildRequires:  jakarta-commons-digester
BuildRequires:  jakarta-commons-pool
BuildRequires:  plexus-cdc
BuildRequires:  plexus-classworlds
BuildRequires:  plexus-containers-container-default 
BuildRequires:  plexus-utils >= 1.4.5

Requires:  directory-naming
Requires:  jakarta-commons-pool
Requires:  plexus-classworlds
Requires:  plexus-container-default 
Requires:  plexus-utils >= 1.4.5
Requires:  jpackage-utils
Requires:  java >= 0:1.6.0
Requires(post):    jpackage-utils >= 0:1.7.3
Requires(postun):  jpackage-utils >= 0:1.7.3

%description
The Plexus project seeks to create end-to-end developer tools for 
writing applications. At the core is the container, which can be 
embedded or for a full scale application server. There are many 
reusable components for hibernate, form processing, jndi, i18n, 
velocity, etc. Plexus also includes an application server which 
is like a J2EE application server, without all the baggage.


%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{name}-%{namedversion}
cp %{SOURCE1} build.xml
mkdir -p target/classes/META-INF/plexus/
cp %{SOURCE4} target/classes/META-INF/plexus/components.xml
%patch0 -b .sav0

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

%if %{with_maven}
    mvn-jpp \
        -Dmaven.test.failure.ignore=true \
        -Dmaven2.jpp.depmap.file=%{SOURCE3} \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install javadoc:javadoc

%else
export CLASSPATH=$(build-classpath \
commons-collections \
commons-logging \
commons-dbcp \
commons-pool \
directory-naming/naming-config \
directory-naming/naming-core \
directory-naming/naming-factory \
directory-naming/naming-java \
hsqldb \
plexus/classworlds \
plexus/containers-container-default \
plexus/utils \
)
CLASSPATH=$CLASSPATH:target/classes:target/test-classes
ant -Dbuild.sysclasspath=only jar javadoc
%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus
install -pm 644 target/%{name}-%{namedversion}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{parent}/%{subname}-%{version}.jar
%add_to_maven_depmap org.codehaus.plexus %{name} %{version} JPP/%{parent} %{subname}

(cd $RPM_BUILD_ROOT%{_javadir}/plexus && for jar in *-%{version}*; do \
  ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# poms
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml \
    $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}.pom

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -pr target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}
ln -s %{name} $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/%{parent}/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.1.2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.1m)
- import from Fedora 13

* Fri Aug 28 2009 Andrew Overholt <overholt@redhat.com> 1.0-0.5.a3
- Add versioned BR and R on plexus-utils

* Fri Aug 28 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.4.a3
- Back-port license header fix.  See:
  http://jira.codehaus.org/browse/PLXCOMP-144

* Wed Aug 26 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.3.a3
- Add JDK and JRE requirements

* Wed Aug 26 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.2.a3
- Add jpackage-utils as a requirement

* Wed Aug 26 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.1.a3
- Fix release tag (it's newer than the old one) to meet Fedora
  guidelines

* Tue Aug 25 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.a3.5.1
- Import from JPackage and Deepak Bhole's work
- Build without maven for now
- Fixes from Yong Yang:
-- add missing BRs

* Fri Nov 30 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.a3.1jpp
- First JPP release

