%global momorel 13

%define festivalversion 1.96
# we ship the 1.4.2 docs for now.
%define docversion 1.4.2
%define speechtoolsversion 1.2.96

Name: festival
Summary: A free speech synthesis and text-to-speech system
Version: %{festivalversion}
Release: %{momorel}m%{?dist}

URL: http://www.cstr.ed.ac.uk/projects/festival/
Group: Applications/Multimedia
License: MIT/X


# Files needed for everything...
%define baseURL  http://festvox.org/packed/festival/%{festivalversion}
Source0: %{baseURL}/festival-%{festivalversion}-beta.tar.gz
Source1: %{baseURL}/speech_tools-%{speechtoolsversion}-beta.tar.gz

# Docs haven't been updated yet; here's the old ones
Source2: http://festvox.org/packed/festival/%{docversion}/festdoc-%{docversion}.tar.gz

# Our local site config files.
Source50: festival-1.96-0.7-fedora-siteinit.scm
Source51: festival-1.96-0.7-fedora-sitevars.scm

### DICTIONARIES
# Generic English dictionary
Source100: %{baseURL}/festlex_POSLEX.tar.gz
# American English dictionary
Source101: %{baseURL}/festlex_CMU.tar.gz
# OALD isn't included because it's got a more restrictive (non-commercial
# only) license. OALD voices not included for same reason.

# Note on voice versions: I'm simply using the file date of the newest file
# in each set of tarballs. It happens that the dates for all files from each
# source (diphone, cmu_arctic, etc.) match, which is handy.

### DIPHONE VOICES
%define diphoneversion 0.19990610
Source200: %{baseURL}/festvox_kallpc16k.tar.gz
Source202: %{baseURL}/festvox_kedlpc16k.tar.gz

### HTS VOICES (use Nagoya Institute of Technology's HTS based synthesizer)
# The Festvox site packages older versions of these as cmu_us_*_hts.
# These are from <http://hts.sp.nitech.ac.jp/>.
# And, ugh, the files seem to be only served via a script, not directly.
%define nitechbaseURL http://hts.sp.nitech.ac.jp/?plugin=attach&refer=Download&openfile=
%define nitechhtsversion 0.20061229
Source220: %{nitechbaseURL}/festvox_nitech_us_awb_arctic_hts.tar.bz2
Source221: %{nitechbaseURL}/festvox_nitech_us_bdl_arctic_hts.tar.bz2
Source222: %{nitechbaseURL}/festvox_nitech_us_clb_arctic_hts.tar.bz2
Source223: %{nitechbaseURL}/festvox_nitech_us_jmk_arctic_hts.tar.bz2
Source224: %{nitechbaseURL}/festvox_nitech_us_rms_arctic_hts.tar.bz2
Source225: %{nitechbaseURL}/festvox_nitech_us_slt_arctic_hts.tar.bz2

### Multisyn voices left out because they're ~ 100MB each.

### MBROLA voices left out, because they require MBROLA, which ain't free.


# Set defaults to American English instead of British English - the OALD
# dictionary (free for non-commercial use only) is needed for BE support
# Additionally, prefer the smaller (and I think nicer sounding) nitech hts
# voices.
Patch1: festival-1.96-nitech-american.patch

# Whack some buildroot references
Patch2: festival_buildroot.patch

# Use shared libraries
Patch3: festival-1.96-speechtools-shared-build.patch

# Build (but don't enable by default) the ESD module
Patch4: festival-1.96-speechtools-buildesdmodule.patch

# Fix a coding error (see bug #162137). Need to upstream.
Patch5: festival-1.96-speechtools-rateconvtrivialbug.patch

# Link libs with libm, libtermcap, and libesd (see bug #198190).
# Need to upstream this.
Patch6: festival-1.96-speechtools-linklibswithotherlibs.patch

# For some reason, CXX is set to gcc on everything but Mac OS Darwin,
# where it's set to g++. Yeah, well. We need it to be right too.
Patch7: festival-1.96-speechtools-ohjeezcxxisnotgcc.patch

# Look for siteinit and sitevars in /etc/festival
Patch8: festival-1.96-etcsiteinit.patch

# Alias old cmu names to new nitech ones
Patch9: festival-1.96-alias_cmu_to_nitech.patch

# Look for speech tools here, not back there.
Patch10: festival-1.96-findspeechtools.patch

# Build main library as shared, not just speech-tools
Patch11: festival-1.96-main-shared-build.patch

# This is a hack to make the shared libraries build with actual
# sonames. Should pretty much do the right thing, although note
# of course that the sonames aren't official upstream.
Patch12: festival-1.96-bettersonamehack.patch

# this updates speech_tools to a development version which fixes
# a 64-bit cleanliness issue (among other changes).
Patch20: festival-1.96-speechtools-1.2.96-beta+awb.patch

# This makes festival use /usr/lib[arch]/festival/etc for its
# arch-specific "etc-path", rather than /usr/share/festival/etc/system_type.
# Then I use sed to replace the token with actual arch-specific libdir.
# A better way would be to actually make this a flexible makefile parameter,
# but that's something to take up with upstream.
Patch31: festival-1.96-kludge-etcpath-into-libarch.patch

# For some reason, the Nitech voices (and the previous CMU versions) fail to
# define proclaim_voice, which makes them not show up in the voice
# descriptions, which makes gnome-speech not show them.
Patch90: festival-1.96-nitech-proclaimvoice.patch

# Cure "SIOD ERROR: unbound variable : f2b_f0_lr_start"
Patch91: festival-1.96-nitech-fixmissingrequire.patch

# An apparent copy-paste error in these voices -- slt is referenced
# in all of them.
Patch92: festival-1.96-nitech-sltreferences.patch

Patch93: gcc43.patch
Patch94: festival-speech_tools-glibc210.patch

BuildRequires: tetex
BuildRequires: ncurses-devel, esound-devel

Requires: festival-voice

# This is hard-coded as a requirement because it's the smallest voice (and,
# subjectively I think the most pleasant to listen to and so a good
# default).
#
# Ideally, this would be a "suggests" instead of a hard requirement.
#
# Update: with the new nitech versions of the voices, slt-arctic is no
# longer the smallest. But... AWB has a strong scottish accent, and JMK a
# kind of odd canadian one, so they're not great candidates for inclusion.
# And I find RMS a bit hard to understand. BDL isn't much smaller than SLT,
# and since I like it better, I think I'm going to keep it as the default
# for a price 12k. So, in case anyone later questions why this is the
# default, there's the answer. :)
Requires: festvox-slt-arctic-hts

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)



%package lib
Summary: The shared library for the Festival speech synthesis system
# this is here to make sure upgrades go cleanly. In other cases,
# the auto-deps should handle this just fine.
Requires: festival-speechtools-libs
Group: System Environment/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%package docs
Summary: HTML, Postscript, and Texinfo documentation for Festival
Group: Applications/Multimedia
Version: %{docversion}
Requires(post): info
Requires(preun): info

%package speechtools-libs
Summary: The Edinburgh Speech Tools libraries
Group: System Environment/Libraries
Version: %{speechtoolsversion}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%package speechtools-utils
Summary: Miscellaneous utilities from the Edinburgh Speech Tools 
Group: Applications/Multimedia
Version: %{speechtoolsversion}

%package speechtools-devel
Summary: Development files for Edinburgh Speech Tools libraries
Version: %{speechtoolsversion}
Group: Development/Libraries
# Note: rpmlint complains incorrectly about
# "no-dependency-on festival-speechtools".
Requires: festival-speechtools-libs = %{speechtoolsversion}

%package -n festvox-kal-diphone
Group: Applications/Multimedia
Summary: American English male speaker "Kevin" for Festival
Version: %{diphoneversion}
Provides: festival-voice
Provides: festvox-kallpc16k

%package -n festvox-ked-diphone
Group: Applications/Multimedia
Summary: American English male speaker "Kurt" for Festival
Version: %{diphoneversion}
Requires: festival
Provides: festival-voice
Provides: festvox-kedlpc16k

%package -n festvox-awb-arctic-hts
Group: Applications/Multimedia
Summary: Scottish-accent US English male speaker "AWB" for Festival
Version: %{nitechhtsversion}
Requires: festival
Provides: festival-voice

%package -n festvox-bdl-arctic-hts
Group: Applications/Multimedia
Summary: US English male speaker "BDL" for Festival
Version: %{nitechhtsversion}
Requires: festival
Provides: festival-voice

%package -n festvox-clb-arctic-hts
Group: Applications/Multimedia
Summary: US English female speaker "CLB" for Festival
Version: %{nitechhtsversion}
Requires: festival
Provides: festival-voice

%package -n festvox-jmk-arctic-hts
Group: Applications/Multimedia
Summary: Canadian-accent US English male speaker "JMK" for Festival
Version: %{nitechhtsversion}
Requires: festival
Provides: festival-voice

%package -n festvox-rms-arctic-hts
Group: Applications/Multimedia
Summary: US English male speaker "RMS" for Festival
Version: %{nitechhtsversion}
Requires: festival
Provides: festival-voice

%package -n festvox-slt-arctic-hts
Group: Applications/Multimedia
Summary: US English female speaker "SLT" for Festival
Version: %{nitechhtsversion}
Requires: festival
Provides: festival-voice

# This is last as a lovely hack to make sure Version gets set back
# to what it should be. Grr.
%package devel
Summary: Development files for the Festival speech synthesis system
Version: %{festivalversion}
Group: Development/Libraries
# Note: rpmlint complains incorrectly about
# "no-dependency-on festival"
Requires: festival-speechtools-devel = %{speechtoolsversion}
Requires: festival-lib



%description
Festival is a general multi-lingual speech synthesis system developed
at CSTR. It offers a full text to speech system with various APIs, as
well as an environment for development and research of speech synthesis
techniques. It is written in C++ with a Scheme-based command interpreter
for general control.

%description lib
The shared library used by the Festival text-to-speech and speech synthesis
system.

%description docs
HTML, Postscript, and Texinfo documentation for the Festival text-to-speech
and speech synthesis system.

%description speechtools-libs
The Edinburgh Speech Tools libraries, used by the Festival text-to-speech
and speech synthesis system.

%description speechtools-utils 
Miscellaneous utilities from the Edinburgh Speech Tools. Unless you have a
specific need for one of these programs, you probably don't need to install
this.

%description speechtools-devel
Development files for the Edinburgh Speech Tools Library, used by the
Festival speech synthesis system.


%description -n festvox-kal-diphone
American English male speaker ("Kevin") for Festival.

This voice provides an American English male voice using a residual excited
LPC diphone synthesis method. It uses the CMU Lexicon pronunciations.
Prosodic phrasing is provided by a statistically trained model using part of
speech and local distribution of breaks. Intonation is provided by a CART
tree predicting ToBI accents and an F0 contour generated from a model
trained from natural speech. The duration model is also trained from data
using a CART tree.


%description -n festvox-ked-diphone
American English male speaker ("Kurt") for Festival.

This voice provides an American English male voice using a residual excited
LPC diphone synthesis method. It uses the CMU Lexicon for pronunciations.
Prosodic phrasing is provided by a statistically trained model using part of
speech and local distribution of breaks. Intonation is provided by a CART
tree predicting ToBI accents and an F0 contour generated from a model
trained from natural speech. The duration model is also trained from data
using a CART tree.


%description -n festvox-awb-arctic-hts
US English male speaker ("AWB") for Festival. AWB is a native Scottish
English speaker, but the voice uses the US English front end.

This is a HMM-based Speech Synthesis System (HTS) voice from the Nagoya
Institute of Technology, trained using the CMU ARCTIC database. This voice
is based on 1138 utterances spoken by a Scottish English male speaker. The
speaker is very experienced in building synthetic voices and matched
prompted US English, though his vowels are very different from US English
vowels. Scottish English speakers will probably find synthesizers based on
this voice strange. Unlike the other CMU_ARCTIC databases this was recorded
in 16 bit 16KHz mono without EGG, on a Dell Laptop in a quiet office. The
database was automatically labelled using CMU Sphinx using the FestVox
labelling scripts. No hand correction has been made.


%description -n festvox-bdl-arctic-hts
US English male speaker ("BDL") for Festival.

This is a HMM-based Speech Synthesis System (HTS) voice from the Nagoya
Institute of Technology, trained using the CMU ARCTIC database. This voice
is based on 1132 utterances spoken by a US English male speaker. The speaker
is experienced in building synthetic voices. This was recorded at 16bit
32KHz, in a sound proof room, in stereo, one channel was the waveform, the
other EGG. The database was automatically labelled using CMU Sphinx using
the FestVox labelling scripts. No hand correction has been made.


%description -n festvox-clb-arctic-hts
US English female speaker ("CLB") for Festival.

This is a HMM-based Speech Synthesis System (HTS) voice from the Nagoya
Institute of Technology, trained using the CMU ARCTIC database. This voice
is based on 1132 utterances spoken by a US English female speaker. The
speaker is experienced in building synthetic voices. This was recorded at
16bit 32KHz, in a sound proof room, in stereo, one channel was the waveform,
the other EGG. The database was automatically labelled using CMU Sphinx
using the FestVox labelling scripts. No hand correction has been made.


%description -n festvox-jmk-arctic-hts
US English male speaker ("JMK") voice for Festival. JMK is a native Canadian
English speaker, but the voice uses the US English front end.

This is a HMM-based Speech Synthesis System (HTS) voice from the Nagoya
Institute of Technology, trained using the CMU ARCTIC database. This voice
is based on 1138 utterances spoken by a US English male speaker. The speaker
is experienced in building synthetic voices. This was recorded at 16bit
32KHz, in a sound proof room, in stereo, one channel was the waveform, the
other EGG. The database was automatically labelled using CMU Sphinx using
the FestVox labelling scripts. No hand correction has been made.

%description -n festvox-rms-arctic-hts
US English male speaker ("RMS") voice for Festival.

This is a HMM-based Speech Synthesis System (HTS) voice from the Nagoya
Institute of Technology, trained using the CMU ARCTIC database. This voice
is based on 1132 utterances spoken by a US English male speaker. The speaker
is experienced in building synthetic voices. This was recorded at 16bit
32KHz, in a sound proof room, in stereo, one channel was the waveform, the
other EGG. The database was automatically labelled using EHMM an HMM labeler
that is included in the FestVox distribution. No hand correction has been
made.

%description -n festvox-slt-arctic-hts
US English female speaker ("SLT") voice for Festival.

This is a HMM-based Speech Synthesis System (HTS) voice from the Nagoya
Institute of Technology, trained using the CMU ARCTIC database. This voice
is based on 1132 utterances spoken by a US English female speaker. The
speaker is experienced in building synthetic voices. This was recorded at
16bit 32KHz, in a sound proof room, in stereo, one channel was the waveform,
the other EGG. The database was automatically labelled using CMU Sphinx
using the FestVox labelling scripts. No hand correction has been made.

%description devel
Development files for the Festival speech synthesis system. Install
festival-devel if you want to use Festival's capabilities from within your
own programs, or if you intend to compile other programs using it. Note that
you can also interface with Festival in via the shell or with BSD sockets.



%prep
%setup -q -n festival -a 1

# speech tools
%setup -q -n festival -D -T -a 2
%patch94 -p0 -b .glibc210

# exit out if they've fixed this, so we can remove this hack.
[ -x speech_tools/base_class/string/EST_strcasecmp.c ] || exit 1
chmod -x speech_tools/base_class/string/EST_strcasecmp.c

# dictionaries
%setup -q -n festival -D -T -b 100
%setup -q -n festival -D -T -b 101

# voices
%setup -q -n festival -D -T -b 200
%setup -q -n festival -D -T -b 202
%setup -q -n festival -D -T -b 220
%setup -q -n festival -D -T -b 221
%setup -q -n festival -D -T -b 222
%setup -q -n festival -D -T -b 223
%setup -q -n festival -D -T -b 224
%setup -q -n festival -D -T -b 225

%patch1 -p1 -b .nitech
%patch2 -p1 -b .buildrootrefs
%patch3 -p1 -b .shared
%patch4 -p1 -b .esd
%patch5 -p1 -b .bugfix
%patch6 -p1 -b .liblinking
%patch7 -p1 -b .cxx
%patch8 -p1 -b .etc
%patch9 -p1 -b .cmu2nitech
# patch9 creates a new file; patch helpfully makes a "backup" of the
# non-existent "original", which then has bad permissions. zap.
rm -f lib/alias_cmu_to_nitech.scm.cmu2nitech
%patch10 -p1 -b .findspeechtools
%patch11 -p1 -b .shared
%patch12 -p1 -b .soname

%patch20 -p1 -b .awb

%patch31 -p1 -b .libarch
# finish the kludge for arch-specific "etc" (misc. binaries)
for f in speech_tools/main/siod_main.cc src/arch/festival/festival.cc; do
  sed -i -e 's,{{HORRIBLELIBARCHKLUDGE}},"%{_libdir}",' $f
done

# no backups for these patches because 
# the voice directories are copied wholesale
%patch90 -p1 
%patch91 -p1
%patch92 -p1
%patch93 -p1 -b .gcc43

# zero length
rm festdoc-%{docversion}/speech_tools/doc/index_html.jade
rm festdoc-%{docversion}/speech_tools/doc/examples_gen/error_example_section.sgml
rm festdoc-%{docversion}/speech_tools/doc/tex_stuff.jade



%build
RPM_OPT_FLAGS="`echo %{optflags} | sed -e 's/-Wp,-D_FORTIFY_SOURCE=2//'`"

# build speech tools (and libraries)
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/speech_tools/lib
pushd speech_tools
  %configure
  # -fPIC 'cause we're building shared libraries and it doesn't hurt
  # -fno-strict-aliasing because of a couple of warnings about code
  #   problems; if $RPM_OPT_FLAGS contains -O2 or above, this puts
  #   it back. Once that problem is gone upstream, remove this for
  #   better optimization.
  make \
    CFLAGS="$RPM_OPT_FLAGS -fPIC -fno-strict-aliasing" \
    CXXFLAGS="$RPM_OPT_FLAGS  -fPIC -fno-strict-aliasing"
popd

# build the main program
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/src/lib
# instead of doing this, maybe we should patch the make process
# so it looks in the right place explicitly:
export PATH=$(pwd)/bin:$PATH
%configure
make \
  FTLIBDIR="%{_datadir}/festival/lib" \
  CFLAGS="$RPM_OPT_FLAGS -fPIC" \
  CXXFLAGS="$RPM_OPT_FLAGS -fPIC"

# build the patched CMU dictionary
pushd lib/dicts/cmu
  make
popd


%install
# "make install" for this package is, um, "interesting". It seems geared for
# local user-level builds. So, rather than doing that and then patching it
# up, do the right parts by hand as necessary.

# install speech tools libs, binaries, and include files
pushd speech_tools

  make INSTALLED_LIB=$RPM_BUILD_ROOT%{_libdir} make_installed_lib_shared
  # no thanks, static libs.
  rm $RPM_BUILD_ROOT%{_libdir}/*.a

  make INSTALLED_BIN=$RPM_BUILD_ROOT%{_libexecdir}/speech-tools make_installed_bin_static
  # this list of the useful programs in speech_tools comes from
  # upstream developer Alan W. Black; the other stuff is to be removed.
  pushd $RPM_BUILD_ROOT%{_libexecdir}/speech-tools
    ls |
        grep -Evw "ch_wave|ch_track|na_play|na_record|wagon|wagon_test" |
        grep -Evw "make_wagon_desc|pitchmark|pm|sig2fv|wfst_build" |
        grep -Evw "wfst_run|wfst_run" |
        xargs rm
  popd

  pushd include
    for d in $( find . -type d | grep -v win32 ); do
      make -w -C $d INCDIR=$RPM_BUILD_ROOT%{_includedir}/speech_tools/$d install_incs
    done  
    # Um, yeah, so, "EST" is not a very meaningful name for the include dir.
    # The Red Hat / Fedora package has traditionally put this stuff under
    # "speech_tools", and that's what we're gonna do here too.
    mv $RPM_BUILD_ROOT%{_includedir}/speech_tools/EST/*.h \
       $RPM_BUILD_ROOT%{_includedir}/speech_tools/
    rmdir $RPM_BUILD_ROOT%{_includedir}/speech_tools/EST
  popd

  cp README ../README.speechtools

popd

# install the dictionaries
TOPDIR=$( pwd )
pushd lib/dicts
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/festival/lib/dicts
  # we want to put the licenses in the docs...
  cp COPYING.poslex $OLDPWD/COPYING.poslex
  cp cmu/COPYING $OLDPWD/COPYING.cmudict
  for f in wsj.wp39.poslexR wsj.wp39.tri.ngrambin ; do
    install -m 644 $f $RPM_BUILD_ROOT%{_datadir}/festival/lib/dicts/
  done
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/festival/lib/dicts/cmu
  pushd cmu
    # note I'm keeping cmudict-0.4.diff and cmudict_extensions.scm to
    # satisfy the "all changes clearly marked" part of the license -- these
    # are the changes. And yes, the ".out" file is the one actually used.
    # Sigh.
    for f in allowables.scm cmudict-0.4.diff cmudict-0.4.out \
             cmudict_extensions.scm cmulex.scm cmu_lts_rules.scm; do
      install -m 644 $f $RPM_BUILD_ROOT%{_datadir}/festival/lib/dicts/cmu/
    done
  popd
popd

# install the voices
pushd lib/voices
  # get the licenses. This is probably too clever by half, but oh well.
  for f in $( find . -name COPYING ); do
    n=$( echo $f | sed 's/.*\/\(.*\)\/COPYING/COPYING.\1/' )
    mv $f $OLDPWD/$n
  done
  # ditch the readme files -- these aren't very useful. 
  # Except keep a README.htsvoice, because it contains license information.
  cp us/nitech_us_awb_arctic_hts/hts/README.htsvoice $OLDPWD/README.htsvoice
  find . -name 'README*' -exec rm {} \;
popd
# kludge! nitech_us_awb_arctic_hts is missing its COPYING file. It should
# be the same as the other nitech files, though, so just copy one.
cp COPYING.nitech_us_bdl_arctic_hts COPYING.nitech_us_awb_arctic_hts
cp -a lib/voices $RPM_BUILD_ROOT%{_datadir}/festival/lib


# okay, now install the main festival program.

# binaries:
make INSTALLED_BIN=$RPM_BUILD_ROOT%{_bindir} make_installed_bin_static
install -m 755 bin/text2wave $RPM_BUILD_ROOT%{_bindir}

# install the shared library
cp -a src/lib/libFestival.so* $RPM_BUILD_ROOT%{_libdir}

# this is just nifty. and it's small.
install -m 755 examples/saytime $RPM_BUILD_ROOT%{_bindir}

# man pages
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
cp -a doc/*.1 $RPM_BUILD_ROOT%{_mandir}/man1

# lib: the bulk of the program -- the scheme stuff and so on
pushd lib
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/festival/lib
  for f in *.scm festival.el *.ent *.gram *.dtd *.ngrambin speech.properties ; do
    install -m 644 $f $RPM_BUILD_ROOT%{_datadir}/festival/lib/
  done
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/festival/lib/multisyn/
  install -m 644 multisyn/*.scm $RPM_BUILD_ROOT%{_datadir}/festival/lib/multisyn/
popd 

# "etc" -- not in the configuration sense, but in the sense of "extra helper
# binaries".
pushd lib/etc
  # not arch-specific
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/festival/lib/etc
  install -m 755 email_filter $RPM_BUILD_ROOT%{_datadir}/festival/lib/etc
  # arch-specific
  mkdir -p $RPM_BUILD_ROOT%{_libdir}/festival/etc
  install -m 755 */audsp $RPM_BUILD_ROOT%{_libdir}/festival/etc
popd

# the actual /etc. :)
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/festival
# use our version of this file
rm $RPM_BUILD_ROOT%{_datadir}/festival/lib/siteinit.scm 
install -m 644 %{SOURCE50} $RPM_BUILD_ROOT%{_sysconfdir}/festival/siteinit.scm
install -m 644 %{SOURCE51} $RPM_BUILD_ROOT%{_sysconfdir}/festival/sitevars.scm

# copy in the intro.text. It's small and makes (intro) work. in the future,
# we may want include more examples in an examples subpackage
mkdir -p $RPM_BUILD_ROOT%{_datadir}/festival/examples/
install -m 644 examples/intro.text $RPM_BUILD_ROOT%{_datadir}/festival/examples


# header files
mkdir -p $RPM_BUILD_ROOT%{_includedir}/festival
cp -a src/include/* $RPM_BUILD_ROOT%{_includedir}/festival

# Clean up some junk from the docs tarball.
pushd festdoc-%{docversion}/speech_tools/doc
  rm -fr CVS arch_doc/CVS man/CVS  speechtools/arch_doc/CVS
  rm -f .*_made .speechtools_html .tex_done
popd

# info pages
mkdir $RPM_BUILD_ROOT%{_infodir}
cp -p festdoc-%{docversion}/festival/info/* $RPM_BUILD_ROOT%{_infodir}



%clean
rm -rf $RPM_BUILD_ROOT



%post docs
/sbin/install-info %{_infodir}/festival.info --section "Accessibility" --entry="* Festival: (festival).           Speech synthesis system." %{_infodir}/dir || :

%post lib -p /sbin/ldconfig

%post speechtools-libs -p /sbin/ldconfig


%preun docs
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/festival.info %{_infodir}/dir || :
fi


%postun lib -p /sbin/ldconfig

%postun speechtools-libs -p /sbin/ldconfig



%files
%defattr(-,root,root)
%doc ACKNOWLEDGMENTS COPYING NEWS README
%doc COPYING.poslex COPYING.cmudict
%dir %{_sysconfdir}/festival
%config(noreplace)  %{_sysconfdir}/festival/siteinit.scm
%config(noreplace)  %{_sysconfdir}/festival/sitevars.scm
%{_bindir}/festival
%{_bindir}/festival_client
%{_bindir}/festival_server
%{_bindir}/festival_server_control
%{_bindir}/text2wave
%{_bindir}/saytime
%dir %{_datadir}/festival
%dir %{_datadir}/festival/lib
%{_datadir}/festival/lib/*.scm
%{_datadir}/festival/lib/festival.el
%{_datadir}/festival/lib/*.ent
%{_datadir}/festival/lib/*.gram
%{_datadir}/festival/lib/*.dtd
%{_datadir}/festival/lib/*.ngrambin
%{_datadir}/festival/lib/speech.properties
%{_datadir}/festival/lib/dicts
%{_datadir}/festival/lib/etc
%dir %{_datadir}/festival/lib/multisyn
%{_datadir}/festival/lib/multisyn/*.scm
%dir %{_datadir}/festival/lib/voices
%dir %{_datadir}/festival/lib/voices/english
%dir %{_datadir}/festival/lib/voices/us
%dir %{_datadir}/festival/examples
%{_datadir}/festival/examples/intro.text
%dir %{_libdir}/festival
%dir %{_libdir}/festival/etc
%{_libdir}/festival/etc/*
%{_mandir}/man1/*

%files lib
%defattr(-,root,root)
%doc COPYING
%{_libdir}/libFestival.so.*

%files docs
%defattr(-,root,root)
%doc festdoc-%{docversion}/festival/html/*html
%{_infodir}/*

%files speechtools-libs
%defattr(-,root,root)
%doc README.speechtools
%{_libdir}/libestbase.so.*
%{_libdir}/libestools.so.*
%{_libdir}/libeststring.so.*

%files speechtools-utils
%defattr(-,root,root)
%doc README.speechtools
%dir %{_libexecdir}/speech-tools
%{_libexecdir}/speech-tools/*

%files speechtools-devel
%defattr(-,root,root)
%doc festdoc-%{docversion}/speech_tools
%{_libdir}/libestbase.so
%{_libdir}/libestools.so
%{_libdir}/libeststring.so
%dir %{_includedir}/speech_tools
%{_includedir}/speech_tools/*

%files -n festvox-kal-diphone
%defattr(-,root,root)
%doc COPYING.kal_diphone
%{_datadir}/festival/lib/voices/english/kal_diphone

%files -n festvox-ked-diphone
%defattr(-,root,root)
%doc COPYING.ked_diphone 
%{_datadir}/festival/lib/voices/english/ked_diphone

%files -n festvox-awb-arctic-hts
%defattr(-,root,root)
%doc COPYING.nitech_us_awb_arctic_hts COPYING.hts README.htsvoice
%{_datadir}/festival/lib/voices/us/nitech_us_awb_arctic_hts

%files -n festvox-bdl-arctic-hts
%defattr(-,root,root)
%doc COPYING.nitech_us_bdl_arctic_hts COPYING.hts README.htsvoice
%{_datadir}/festival/lib/voices/us/nitech_us_bdl_arctic_hts

%files -n festvox-clb-arctic-hts
%defattr(-,root,root)
%doc COPYING.nitech_us_clb_arctic_hts COPYING.hts README.htsvoice
%{_datadir}/festival/lib/voices/us/nitech_us_clb_arctic_hts

%files -n festvox-jmk-arctic-hts
%defattr(-,root,root)
%doc COPYING.nitech_us_jmk_arctic_hts COPYING.hts README.htsvoice
%{_datadir}/festival/lib/voices/us/nitech_us_jmk_arctic_hts

%files -n festvox-rms-arctic-hts
%defattr(-,root,root)
%doc COPYING.nitech_us_rms_arctic_hts COPYING.hts README.htsvoice
%{_datadir}/festival/lib/voices/us/nitech_us_rms_arctic_hts

%files -n festvox-slt-arctic-hts
%defattr(-,root,root)
%doc COPYING.nitech_us_slt_arctic_hts COPYING.hts README.htsvoice
%{_datadir}/festival/lib/voices/us/nitech_us_slt_arctic_hts

%files devel
%defattr(-,root,root)
%doc COPYING
%{_libdir}/libFestival.so
%dir %{_includedir}/festival
%{_includedir}/festival/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.96-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.96-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.96-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.96-10m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96-8m)
- temporarily drop -Wp,-D_FORTIFY_SOURCE=2
- apply glibc210 patch

* Sat Feb 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.96-7m)
- drop festival-1.96-speechtools-fix-split.patch, which is no longer required

* Thu Feb  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.96-6m)
- add patch to fix compilation failure

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96-5m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96-4m)
- fix install-info

* Sun Apr  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.96-3m)
- modify %%files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.96-2m)
- rebuild against gcc43

* Fri Mar 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96-1m)
- sync with Fedora (1.96-4.fc9)

* Mon Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.95-4m)
- no use libtermcap

* Sat Apr  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-3m)
- sync with Fedora Core devel
 +* Sun Jan 22 2006 Ray Strode <rstrode@redhat.com> - 1.95-5
 +- get gnopernicus again. Patch from
 +  Fernando Herrera <fherrera@gmail.com> (bug 178312)
 +- add a lot of compiler flags and random cruft to get
 +  festival to build with gcc 4.1

* Wed Jan 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-2m)
- sync with Fedora Core devel
 +* Wed Apr 27 2005 Miloslav Trmac <mitr@redhat.com> - 1.95-2
 +- Fix build with gcc 4 (#156132)
 +- Require /sbin/install-info for scriptlets (#155698)
 +- Don't ship %{_bindir}/VCLocalRules (#75645)
 +* Tue Apr 28 2005  <johnp@redhat.com> - 1.95-3
 +- require info packages so the post does not fail
 +- remove /usr/bin/VCLocalRule from buildroot since it is
 +  an extranious file that does not need to be installed
- I can't build with gcc-4.1 yet...

* Sat Apr 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-1m)
- import from Fedora Core development for kdeaccessibility

* Fri Feb 25 2005  <jrb@redhat.com> - 1.95-1
- patch from Matthew Miller to update to 1.95.  Full changelog below

* Mon Feb  7 2005 Matthew Miller <mattdm@mattdm.org> 1.95-0.mattdm8
- put speech-tools binaries in /usr/libexec/speech-tools so as to not
  clutter /usr/bin. Another approach would be to make speech-tools a 
  separate package and to make these utilities a subpackage of that.
- macro-ize /usr/bin, /usr/lib, /usr/include

* Sun Feb  6 2005 Matthew Miller <mattdm@mattdm.org> 1.95-0.mattdm6
- worked on this some more
- made actually work -- put back rest of fsstnd patch which I had broken
- made kludge for lack of sonames in shared libraries -- I think I did the
  right thing
- put back american as the default -- british dicts are non-free.

* Wed Jan  5 2005 Matthew Miller <mattdm@mattdm.org> 1.95-0.mattdm1
- preliminary update to 1.95 beta
- add really nice CMU_ARCTIC HTS voices, which is the whole point of wanting
  to do this. (They have a free license.)
- switch to festvox.org north american upstream urls
- keep old doc files -- there's no new ones yet.
- add comment to specfile about reason for lack of OALD (British) voices --
  they've got a more restrictive license.
- change license to "X11-style", because that's how they describe it.
- remove exclusivearch. I dunno if this builds on other archs, but I
  also don't know why it wouldn't.
- fancier buildroot string, 'cause hey, why not.
- more "datadir" macros
- remove most of Patch0 (fsstnd) -- can be done by setting variables instead.
  there's some bits in speechtools still, though
- update Patch3 (shared-build)
- don't apply patches 20 and 21 -- no longer needed.
- disable adding "FreeBSD" and "OpenBSD" to the dictionary for now. Probably
  a whole list of geek words should be added. Also, the patch was applied
  in an icky kludgy way.

* Thu Jul 29 2004 Miloslav Trmac <mitr@redhat.com> - 1.4.2-25
- Update for gcc 3.4

* Wed Jul 28 2004 Miloslav Trmac <mitr@redhat.com> - 1.4.2-24
- Use shared libraries to reduce package size
- Don't ship patch backup files

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May  4 2004 Jonathan Blandford <jrb@redhat.com> 1.4.2-21
- Remove the spanish voices until we get clarification on the license

* Sat Apr 10 2004 Warren Togami <wtogami@redhat.com>
- BR libtermcap-devel #104722

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Aug 25 2003 Bill Nottingham <notting@redhat.com> 1.4.2-19
- clean up buildroot references (#75643, #77908, #102985)
- remove some extraneous scripts
- fix build with gcc-3.3

* Thu Jun 12 2003 Elliot Lee <sopwith@redhat.com> 1.4.2-17
- Rebuild

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Jan  9 2003 Tim Powers <timp@redhat.com> 1.4.2-15
- redirect install-info spewage

* Tue Jan  7 2003 Jens Petersen <petersen@redhat.com> 1.4.2-14
- put info files in infodir
- add post and postun script to install and uninstall info dir file entry
- drop postscript and info files from docs

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 1.4.2-13
- rebuild

* Thu Aug 15 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.4.2-12
- Adapt to current libstdc++

* Tue Jul 23 2002 Tim Powers <timp@redhat.com> 1.4.2-10
- build using gcc-3.2-0.1

* Wed Jul  3 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.4.2-9
- Add some missing helpprograms (# 67698)

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Jun 10 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.4.2-7
- Fix some rpmlint errors

* Mon Jun 10 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.4.2-6
- Fix ISO C++ compliance

* Mon Mar 18 2002 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Mar 14 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.4.2-2
- Get rid of CVS directiories in doc dir
- Fix broken symlinks for components from speech_tools

* Wed Mar  6 2002 Trond Eivind Glomsrod <teg@redhat.com>
- 1.4.2
- Lots of fixes to make it build, more needed
- Cleanups
- Update URL
- Fix docs inclusion
- Drop prefix
- Use %%{_tmppath}

* Wed Aug  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Add defattr (Bug #15033)

* Tue Jul 25 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- fix build on current 7.0

* Mon Jul 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- fix build on current 7.0

* Thu Jul  6 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- FHSify

* Mon Jun 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix build on non-x86

* Sun Apr 22 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- initial packaging
