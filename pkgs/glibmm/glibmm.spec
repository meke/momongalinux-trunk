%global momorel 1

Summary: A C++ interface for the GLib
Name: glibmm
Version: 2.34.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.34/%{name}-%{version}.tar.xz 
NoSource: 0
URL: http://gtkmm.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.23.1
BuildRequires: libsigc++-devel >= 2.2.4.2
BuildRequires: perl

%description
This package provides a C++ interface for GLib library.
The interface provides a convenient interface for C++
programmers to create GLib flexible object-oriented framework.
Features include type safe callbacks, widgets that are extensible using
inheritance and over 110 classes that can be freely combined to quickly
create complex user interfaces.


%package devel
Summary: Headers for developing programs that will use GLib
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: libsigc++-devel

%description    devel
This package contains the headers that programmers will need to develop
applications which will use GLib, the C++ interface to the GLib library.

%prep
%setup -q

%build
%configure --enable-silent-rules --enable-documentation
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_datadir}/glibmm-2.4/doctool
cp docs/doc-install.pl \
	docs/doc-postprocess.pl \
	docs/doxygen.css \
	docs/tagfile-to-devhelp2.xsl \
	%{buildroot}%{_datadir}/glibmm-2.4/doctool

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/libgiomm-2.4.so.*
%{_libdir}/libglibmm-2.4.so.*
%{_libdir}/libglibmm_generate_extra_defs-2.4.so.*
%exclude %{_libdir}/lib*.la
%dir %{_libdir}/%{name}-2.4
%dir %{_libdir}/%{name}-2.4/proc
%{_libdir}/%{name}-2.4/proc/pm
%{_libdir}/%{name}-2.4/proc/generate_wrap_init.pl
%{_libdir}/%{name}-2.4/proc/gmmproc

%files  devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/%{name}-2.4/proc/m4
%{_libdir}/%{name}-2.4/include
%dir %{_libdir}/giomm-2.4
%{_libdir}/giomm-2.4/include

%{_libdir}/pkgconfig/%{name}-2.4.pc
%{_libdir}/pkgconfig/giomm-2.4.pc

%{_includedir}/%{name}-2.4
%{_includedir}/giomm-2.4

%doc %{_datadir}/doc/%{name}-2.4
%{_datadir}/devhelp/books/%{name}-2.4/%{name}-2.4.devhelp2
%{_datadir}/glibmm-2.4/doctool

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.1-1m)
- update to 2.34.1

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.33.14-1m)
- update to 2.33.14

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.12-1m)
- update to 2.33.12

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.3-1m)
- update to 2.33.3

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.2-1m)
- update to 2.33.2

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.13-1m)
- update to 2.29.13

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Tue Apr 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-3m)
- add some files for document

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.2-2m)
- full rebuild for mo7 release

* Tue May  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.2-1m)
- update to 2.23.2

* Wed Jan 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.1-1m)
- update to 2.23.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Sep 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Sun Jul 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-3m)
- define __libtoolize :

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.20.0-2m)
- add autoreconf (build fix)

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.8-1m)
- update to 2.19.8

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.3-1m)
- update to 2.19.3

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.2-1m)
- update to 2.19.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.1-2m)
- rebuild against rpm-4.6

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Fri Jul  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Sun Apr 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.1-2m)
- rebuild against gcc43

* Sun Mar 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.2-3m)
- %%NoSource -> NoSource

* Tue Jan 29 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.2-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Tue Oct 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Fri Oct  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Thu Sep 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Mon Jun 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.10-1m)
- update to 2.12.10

* Thu May  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.9-1m)
- update to 2.12.9

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.7-1m)
- update to 2.12.7

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-1m)
- update to 2.12.6

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Fri Dec 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-1m)
- update to 2.12.4

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sat Sep 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Sat Sep 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Wed Aug 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Wed Mar 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-2m)
- add Patch0: glibmm-2.6.1-deprecated.patch due to glib-2.10.1

* Mon May 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-1m)
- update to  2.6.1
- come back to MAIN for inkscape

* Mon Nov 08 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-1m)
- version 2.4.0

* Thu Jan 22 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.3.2-1m)
- version 2.3.2

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0


