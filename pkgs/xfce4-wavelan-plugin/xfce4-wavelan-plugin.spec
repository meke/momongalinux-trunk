%global momorel 1

%global xfce4ver 4.11.0
%global major 0.5

Name: 		xfce4-wavelan-plugin
Version: 	0.5.11
Release:	%{momorel}m%{?dist}
Summary:	WaveLAN plugin for the Xfce panel

Group: 		User Interface/Desktops
License:        Modified BSD
URL:		http://goodies.xfce.org/projects/panel-plugins/xfce4-wavelan-plugin
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
BuildRequires:	gettext, perl-XML-Parser
#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxml2-devel
BuildRequires:  pkgconfig
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description 
A plugin for the Xfce panel that monitors a wireless LAN interface. It 
displays stats for signal state, signal quality and network name (SSID).

%prep
%setup -q

%build
%configure --disable-static LIBS="-lm"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -name "*.la" -delete
%find_lang %{name}


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.11-1m)
- update to 0.5.11
- build against xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.9-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.6-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-9m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.5-6m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-5m)
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.5-4m)
- fix build

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.5-3m)
- good-bye autoreconf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.5-1m)
- update to 0.5.5

* Tue May 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.4-7m)
- add autoreconf. need libtool-2.2.x

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-6m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-5m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-4m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-3m)
- drop Patch0, already merged upstream
- License: Modified BSD

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4
- rebuild against xfce4 4.4.2
- why only gziped source? (Source0)

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-3m)
- rebuild against xfce4 4.4.1

* Thu Mar  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.3-2m)
- Patch0: xfce4-wavelan-plugin-0.5.3-kh.patch

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-5m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-4m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-3m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-2m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against xfce4 4.1.90

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.0-1m)

* Sun Jun 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.2-1m)
- import to Momonga
