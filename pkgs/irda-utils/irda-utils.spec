%global momorel 11

Summary: Utilities for infrared communication between devices.
Name: irda-utils
Version: 0.9.18
Release: %{momorel}m%{?dist}
Source: http://dl.sourceforge.net/sourceforge/irda/irda-utils-%{version}.tar.gz
NoSource: 0
Source1: irda.init
Source2: irda.service
Source3: irda.sysconfig
Patch1: irda-utils-0.9.17-rootonly.patch
Patch2: irda-utils-0.9.15-rh1.patch
Patch3: irda-utils-0.9.16-io.patch
Patch4: irda-utils-0.9.17-makefile.patch
Patch5: irda-utils-0.9.18-smcdisable.patch
Patch6: irda-utils-0.9.18-root.patch
Patch7: irda-utils-0.9.18-man.patch
Patch8: irda-utils-0.9.18-PIE.patch

Url: http://irda.sourceforge.net/
License: GPL
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: grep, modutils >= 2.3.21-4
Requires(post): chkconfig
Requires(preun): chkconfig
ExcludeArch: s390 s390x
BuildRequires: glib2-devel, libtool, automake, docbook-utils
BuildRequires: systemd-units

Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
IrDA(TM) (Infrared Data Association) is an industry standard for
wireless, infrared communication between devices. IrDA speeds range
from 9600 bps to 4 Mbps, and IrDA can be used by many modern devices
including laptops, LAN adapters, PDAs, printers, and mobile phones.

The Linux-IrDA project is a GPL'd implementation, written from
scratch, of the IrDA protocols. Supported IrDA protocols include
IrLAP, IrLMP, IrIAP, IrTTP, IrLPT, IrLAN, IrCOMM and IrOBEX.

The irda-utils package contains a collection of programs that enable
the use of IrDA protocols. Most IrDA features are implemented in the
kernel, so IrDA support must be enabled in the kernel before any IrDA
tools or programs can be used. Some configuration outside the kernel
is required, however, and some IrDA features, like IrOBEX, are
actually implemented outside the kernel.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
gunzip man/irnet.4.gz man/irda.7.gz
%patch7 -p1
gzip -9 man/irnet.4 man/irda.7
%patch8 -p1

%build
make all RPM_OPT_FLAGS="$RPM_OPT_FLAGS" ROOT="$RPM_BUILD_ROOT" CFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT

for dir in %{_sbindir} %{_bindir} %{_initscriptdir} %{_sysconfdir}/sysconfig
do
    install -d $RPM_BUILD_ROOT$dir
done

make install  ROOT="$RPM_BUILD_ROOT" MANDIR="$RPM_BUILD_ROOT/%{_mandir}"

#install -p -m755 %{SOURCE1} $RPM_BUILD_ROOT/%{_initscriptdir}/irda
#chmod -x $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/irda
rm -f $RPM_BUILD_ROOT/%{_initscriptdir}/irda

install -d $RPM_BUILD_ROOT%{_unitdir}
install -p -m644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/irda.service
install -p -m644 %{SOURCE3} $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/irda

rm -f $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/network-scripts/ifcfg-irlan0

for i in irattach irdadump irdaping tekram
do
    [ -f $i/README ] && ln $i/README README.$i
done
iconv -f ISO8859-1 -t UTF-8 <README.irdadump >README.irdadump.new && \
        mv -f README.irdadump.new README.irdadump
mv etc/modules.conf.irda etc/modprobe.conf.irda
chmod -x etc/ifcfg-irlan0

%clean
rm -rf $RPM_BUILD_ROOT

%post
%systemd_post irda.service

%preun
%systemd_preun irda.service

%postun
%systemd_postun_with_restart irda.service

%files
%defattr(-,root,root)
%{_sbindir}/*
%{_bindir}/*
%{_mandir}/*/*
#%{_initscriptdir}/irda
%{_unitdir}/*
%config(noreplace) %{_sysconfdir}/sysconfig/irda
%doc README* etc/ifcfg-irlan0 etc/modprobe.conf.irda

%changelog
* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.18-11m)
- update post,preun,postun section

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.18-10m)
- rebuild for glib 2.33.2

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.18-9m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.18-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.18-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.18-6m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.18-5m)
- use Requires

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.18-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.18-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.18-2m)
- rebuild against gcc43

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.18-1m)
- version up

* Fri Jan 21 2005 mutecat <mutecat@momonga-linux.org>
- (0.9.16-2m)
- arrange ppc

* Mon Jan 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.16-1m)
- update to 0.9.16
- add Patch1: irda-utils-0.9.14-chkconfig.patch
- add Patch2: irda-utils-0.9.14-typo.patch
- add Patch3: irda-utils-0.9.13-i18n.patch
- add Patch4: irda-utils-0.9.16-rootonly.patch
- add Patch5: irda-utils-0.9.15-rh1.patch
- add Patch6: irda-utils-0.9.16-rh2.patch
- add Patch10: irda-utils-0.9.16-mo1.patch

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.15-2m)
- revised spec for enabling rpm 4.2.

* Fri Jun 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.15-1m)
- version 0.9.15

* Tue Sep  4 2001 Daisuke Yabuki <dxy@acm.org>
- (0.9.14-6k)
- rebuild against libtool 1.4.

* Mon Mar  5 2001 Daisuke Yabuki <dxy@acm.org>
- (0.9.14-3k)
- Just catching up with the latest
- irdaping/README.txt renamed to irdaping/README

* Mon Feb 19 2001 Daisuke Yabuki <dxy@acm.org>
- (0.9.13-5k)
- $DONGLE valuable evaluation modification in irda startup script: 
  from 'if $DONGLE ; then' to 'if [ -n "$DONGLE" ]; then'
- modified "chkconfig", "description" and some other comments in the startup
  script so that it could be handled by chkconfig appropriately

* Tue Feb 13 2001 Daisuke Yabuki <dxy@acm.org>
- (0.9.13-3k)
- Removed irmanager (not needed anymore)
- now irda startup script is covered by chkconfig

* Sun Mar 26 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fix ( Group, BuildRoot, Distribution, description )

* Wed Jan 19 2000 Ian Soboroff <ian@cs.umbc.edu>
- 0.9.8
- Added findchip to package
- Added READMEs for the various utilities to the doc directory

* Wed Nov 10 1999 Dag Brattli <dagb@cs.uit.no>
- 0.9.5
- Some fixes to irattach, so it works with the latest kernels and patches
- Removed OBEX which will now become its own distribution
- Removed irdadump-X11 which will be replaced with a GNOME version

* Wed Sep 8 1999 Bernhard Rosenkraenzer <bero@linux-mandrake.com>
- 0.9.4
- include new stuff (palm3, psion, obex_tcp, ...)
- various fixes

* Tue Sep 7 1999 Bernhard Rosenkraenzer <bero@linux-mandrake.com>
- Fix .spec bug

* Tue Sep 7 1999 Bernhard Rosenkraenzer <bero@linux-mandrake.com>
- add README to %doc
- compile gnobex, now in irda-utils-X11

* Tue Sep 7 1999 Bernhard Rosenkraenzer <bero@linux-mandrake.com>
- initial RPM:
  - handle RPM_OPT_FLAGS and RPM_BUILD_ROOT
  - fix build
  - split in normal and X11 packages
