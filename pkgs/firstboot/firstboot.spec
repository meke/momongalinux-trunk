%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Initial system configuration utility
Name: firstboot
Version: 17.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://fedoraproject.org/wiki/FirstBoot

Source0: %{name}-%{version}.tar.bz2
Patch1: %{name}-1.110-momonga.patch
Patch2: %{name}-16.4-no_i18n.patch
Patch3: %{name}-16.4-anaconda.patch

Patch10: %{name}-17.0-Momonga8-theme.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: gettext
BuildRequires: python-devel, python-setuptools-devel
BuildRequires: systemd-units

Requires: pygtk2, python
Requires: setuptool, libuser-python, system-config-users, system-config-date
Requires: authconfig-gtk, python-meh
Requires: system-config-keyboard
Requires: python-ethtool
Requires: cracklib-python
Requires(post): systemd-units systemd-sysv chkconfig
Requires(preun): systemd-units
Requires(postun): systemd-units

Obsoletes: firstboot-tui
BuildArch: noarch
ExclusiveOS: Linux

%define debug_package %{nil}

%description
The firstboot utility runs after installation.  It guides the
user through a series of steps that allows for easier
configuration of the machine. 

%prep
%setup -q

# yoshinani
%patch1 -p1 -b .momonga
perl -p -i -e 's|/rc.d/|/|' Makefile

# %patch2 -p1 -b .momonga
%patch3 -p1 -b .anaconda

# theme patch
%patch10 -p1 -b .motheme

%build

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} SITELIB=%{python_sitelib} install
rm %{buildroot}/%{_datadir}/firstboot/modules/additional_cds.py*

%find_lang %name

%clean
rm -rf %{buildroot}

%post
if [ $1 -ne 2 -a ! -f %{_sysconfdir}/sysconfig/firstboot ]
then
  platform="$(arch)"
  if [ "$platform" = "s390" -o "$platform" = "s390x" ]; then
    echo "RUN_FIRSTBOOT=YES" > /etc/sysconfig/firstboot
  else
    systemctl enable firstboot-graphical.service >/dev/null 2>&1 || :
    systemctl enable firstboot-text.service >/dev/null 2>&1 || :
  fi
fi
		
%preun
if [ $1 = 0 ]; then
  rm -rf %{_datadir}/firstboot/*.pyc
  rm -rf %{_datadir}/firstboot/modules/*.pyc
  /bin/systemctl --no-reload firstboot-graphical.service > /dev/null 2>&1 || :
  /bin/systemctl stop firstboot-graphical.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload > /dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    /bin/systemctl try-restart firstboot-graphical.service > /dev/null 2>&1 || :
fi

%triggerun -- firstboot < 1.117
%{_bindir}/systemd-sysv-convert --save firstboot > /dev/null 2>&1 ||:
/bin/systemctl enable firstboot-graphical.service > /dev/null 2>&1
/sbin/chkconfig --del firstboot > /dev/null 2>&1 || :
/bin/systemctl try-restart firstboot-graphical.service > /dev/null 2>&1 || :

%files -f %{name}.lang
%defattr(-,root,root)
%dir %{_datadir}/firstboot/
%dir %{_datadir}/firstboot/modules/
%dir %{_datadir}/firstboot/themes/
%dir %{_datadir}/firstboot/themes/default
%{python_sitelib}/*
%{_sbindir}/firstboot
%{_datadir}/firstboot/modules/create_user.py*
%{_datadir}/firstboot/modules/date.py*
%{_datadir}/firstboot/modules/eula.py*
%{_datadir}/firstboot/modules/welcome.py*
%{_datadir}/firstboot/themes/default/*
%{_unitdir}/firstboot-graphical.service
%ifarch s390 s390x
%dir %{_sysconfdir}/profile.d
%{_sysconfdir}/profile.d/firstboot.sh
%{_sysconfdir}/profile.d/firstboot.csh
%endif


%changelog
* Sat Mar 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (17.0-1m)
- update 17.0

* Sat Nov 12 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (16.4-5m)
- fix option mistake (firstboot-16.4-Momonga8-theme.patch)

* Fri Nov 11 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (16.4-4m)
- add Momonga8 theme patch (firstboot-16.4-Momonga8-theme.patch)

* Tue Oct 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4-3m)
- support new anaconda

* Tue Oct 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4-2m)
- support momonga-trunk

* Wed Sep 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4-1m)
- update 16.4

* Sun Aug 28 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (16.1-1m)
- update 16.1

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.119-2m)
- release a directory provided by system-config-keyboard

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.119-1m)
- update 1.119

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.117-2m)
- correct %%files

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.117-1m)
- update 1.117

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.110-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.110-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.110-3m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.110-2m)
- remove firstboot-1.106-Mo6-theme.patch
- add firstboot-1.110-Mo7-theme.patch

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.110-1m)
- update 1.110

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.106-6m)
- fix up Requires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.106-5m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.106-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.106-3m)
- Momonga themes was moved to momonga-logos
- delete firstboot-1.97-Mo5-theme.patch
- add firstboot-1.106-Mo6-theme.patch

* Sun Jul  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.106-2m)
- add Require: cracklib-python
- update firstboot-1.106-i18n.patch

* Fri May 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.106-1m)
- update 1.106

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.97-8m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.97-7m)
- rebuild against python-2.6.1-1m

* Thu Oct  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.97-6m)
- sleep 10sec

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.97-5m)
- modify patch0 for version up

* Sun Aug 24 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.97-4m)
- add Momonga Linux 5 theame images
- add Patch11 (firstboot-1.97-Mo5-theme.patch)

* Tue Aug 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.97-3m)
- add patch 10 (for no i18n)

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.97-2m)
- use %%{_initscriptdir} instead of %%{_initrddir}

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.97-1m)
- update 1.97

* Wed Apr 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.96-1m)
- update 1.96

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.35-4m)
- rebuild against gcc43

* Mon Jul 16 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.35-3m)
- update Requires: system-config-date >= 1.9.0-2m

* Sun Jul  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.35-2m)
- apply momonga.patch

* Tue Apr 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.35-1m)
- update 1.4.35

* Mon Apr  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.34-1m)
- update 1.4.34

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.30-1m)
- update 1.4.30

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.23-2m)
- delete pyc pyo

* Tue Oct 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.23-1m)
- update 1.4.23

* Thu Jul  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-1m)
- maki modosi 1.4.6

* Mon Jun 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.12-1m)
- update 1.4.12

* Sun Jun 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.8-1m)
- update 1.4.8

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.6-2m)
- revise %%files again

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.6-1m)
- revise %%file (conflict)

* Fri May 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-1m)
- downgrade 1.4.6
- need Anaconda-10.0.5

* Mon May  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.8-1m)
- update 1.4.8

* Mon Apr 11 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.33-4m)
- revised momonga.patch.

* Sat Apr  2 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.33-3m)
- fix ja.po and firstboot.pot.
- remove src/modules/additional_cds.py

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.33-2m)
- system-config-language is obsoleted.

* Wed Dec  8 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.33-1m)
  based FC3 1.3.33-1

* Mon Oct 18 2004 Adrian Likins <alikins@redhat.com> - 1.3.33-1
- #129885 (do the right thing on ia64)
- #129532 (typo in display file location)

* Fri Oct 15 2004 Adrian Likins <alikins@redhat.com> - 1.3.30-1
- merge some updates from rhel3 branch
- try enabling tui stuff again

* Tue Oct 5 2004 Adrian Likins <alikins@redhat.com> - 1.3.28-1
- text changes for #129885
- get rid of some deprecation warnings

* Tue Sep 28 2004 Nils Philippsen <nphilipp@redhat.com> - 1.3.27-1
- let timezone only be configured on --reconfig (#133748)
- require system-config-date >= 1.7.9

* Thu Sep 23 2004 Adrian Likins <alikins@redhat.com> - 1.3.26-1
- applied patch from #132736

* Wed Sep 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.4-3m)
  for python2.3

* Tue Sep 14 2004 Adrian Likins <alikins@redhat.com> - 1.3.25-1
- change finish screen to not show default "success" 
  message if were also showing errors
- fix forward/back behaviour

* Fri Sep 10 2004 Adrian Likins <alikins@redhat.com> - 1.3.23-1
- fix for finish screen
- ignore modules that throw exceptions on import
  (#129532 and other variations of "firstboot doesnt start)

* Fri Sep 3 2004 Adrian Likins <alikins@redhat.com> - 1.3.21-1
- more fixes for #131308

* Wed Sep 1 2004 Adrian Likins <alikins@redhat.com> - 1.3.20-1
- better fix for #131308 (works now, but needs some
  screen resizing)

* Tue Aug 31 2004 Adrian Likins <alikins@redhat.com> - 1.3.19-1
- #131308 (system-config-date changed, working to not
  traceback, still needs more fixing)

* Wed Jul 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.4-2m)
- add Requires: momonga-logos

* Thu Jul 15 2004 Adrian Likins <alikins@redhat.com> - 1.3.17-1
- allow screens to catch a signal when they are shown

* Wed Jun 30 2004 Adrian Likins <alikins@redhat.com> - 1.3.16-1
- apply patch to allow modules to go forward/back in
  the module order

* Mon Jun 21 2004 Brent Fox <bfox@redhat.com> - 1.3.15-1
- apply patch from mikem in bug #121489

* Wed May 12 2004 Elliot Lee <sopwith@redhat.com> 1.3.14-1
- Fix initscript (#121489)

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.4-1m)
- import from Fedora.

* Mon May  3 2004 Brent Fox <bfox@redhat.com> 1.3.13-1
- fix Norwegian translation bug (bug #122206)

* Thu Apr 22 2004 Brent Fox <bfox@redhat.com> 1.3.12-1
- look for xorg.conf, not XF86Config (bug #121489)

* Thu Apr 15 2004 Brent Fox <bfox@redhat.com> 1.3.11-1
- fix bug #120669

* Wed Apr  7 2004 Brent Fox <bfox@redhat.com> 1.3.10-2
- allow for correct text mode button translations (bug #120087)

* Wed Mar 17 2004 Jeremy Katz <katzj@redhat.com> 1.3.10-1
- fix password to be encrypted properly

* Wed Mar 17 2004 Jeremy Katz <katzj@redhat.com> 1.3.9-1
- more workarounds for selinux (don't use libuser at all for create_user.py 
  for right now)

* Wed Mar 17 2004 Brent Fox <bfox@redhat.com> 1.3.8-1
- workaround selinux - patch from jeremy

* Tue Mar  9 2004 Brent Fox <bfox@redhat.com> 1.3.7-2
- fix typo (bug #117867)

* Mon Mar  8 2004 Brent Fox <bfox@redhat.com> 1.3.7-1
- drop the verbose print statements

* Thu Mar  4 2004 Brent Fox <bfox@redhat.com> 1.3.6-1
- only call chkconfig -add if /etc/sysconfig/firstboot does not exist

* Mon Mar  1 2004 Brent Fox <bfox@redhat.com> 1.3.5-2
- remove Requires on system-config-mouse

* Tue Feb 17 2004 Brent Fox <bfox@redhat.com> 1.3.5-1
- call self.win.present() to allow initial keyboard input

* Mon Feb 16 2004 Brent Fox <bfox@redhat.com> 1.3.4-1
- UTF-8ify fr.po
- make sure the root window stays on the bottom (bug #105631)

* Tue Jan 27 2004 Tim Powers <timp@ragnarok.devel.redhat.com> 1.3.3-3
- fedora-logos -> redhat-logos since redhat-logos is a virtual
  provides (used so that we can switch out redhat-logos with
  fedora-logos easily). Will change to system-logos once the changes
  have been made.

* Fri Jan 23 2004 Bill Nottingham <notting@redhat.com> 1.3.3-2
- some more s/redhat/system/ on requires

* Mon Dec  1 2003 Brent Fox <bfox@redhat.com> 1.3.2-2
- redhat-logos hasn't been renamed to system-logos yet

* Mon Nov 24 2003 Brent Fox <bfox@redhat.com> 1.3.2-1
- make changes for Python2.3

* Sun Nov 23 2003 Brent Fox <bfox@redhat.com> 1.3.2-1
- update Requires for system-config name change
- make changes for Python2.3

* Mon Oct 27 2003 Brent Fox <bfox@redhat.com> 1.3.1-1
- fix initscript for text mode

* Fri Oct 24 2003 Brent Fox <bfox@redhat.com> 1.3.1-1
- bump version
- use CVS head now for Fedora Core 2
- made firstboot-cambridge branch for Fedora Core 1
- first stab at text mode

* Wed Oct 15 2003 Brent Fox <bfox@redhat.com> 1.2.4-1
- pull lightrays.png from a different location

* Wed Oct  8 2003 Brent Fox <bfox@redhat.com> 1.2.3-1
- override rhgb's background

* Wed Oct  8 2003 Brent Fox <bfox@redhat.com> 1.2.2-1
- remove up2date module from Fedora

* Mon Sep 22 2003 Brent Fox <bfox@redhat.com> 1.2.1-1
- remove some items from the additional cd's screen for now
- remove some references to Red Hat Linux from welcome.py and up2date.py

* Fri Aug 29 2003 Brent Fox <bfox@redhat.com> 1.1.16-2
- bump relnum and rebuild

* Fri Aug 29 2003 Brent Fox <bfox@redhat.com> 1.1.16-1
- call authconfig with --firstboot flag correctly (bug #103367)

* Thu Aug 28 2003 Brent Fox <bfox@bfox.devel.redhat.com> 1.1.15-2
- bump relnum and rebuild

* Thu Aug 28 2003 Brent Fox <bfox@bfox.devel.redhat.com> 1.1.15-1
- handle network timeouts better in the date.py screen

* Thu Aug 28 2003 Brent Fox <bfox@bfox.devel.redhat.com> 1.1.14-1
- handle cds without autorun files better in additional_cds.py

* Thu Aug 21 2003 Brent Fox <bfox@redhat.com> 1.1.13-3
- bump relnum and rebuild

* Thu Aug 21 2003 Brent Fox <bfox@redhat.com> 1.1.13-2
- bump relnum and rebuild

* Thu Aug 21 2003 Brent Fox <bfox@redhat.com> 1.1.13-1
- pass --firstboot flag to authconfig-gtk in create_user.py

* Tue Aug 19 2003 Brent Fox <bfox@redhat.com> 1.1.12-2
- bump relnum and rebuild

* Tue Aug 19 2003 Brent Fox <bfox@redhat.com> 1.1.12-1
- fix formatting bug in create_user.py

* Fri Aug 15 2003 Brent Fox <bfox@redhat.com> 1.1.11-2
- bump relnum and rebuild

* Fri Aug 15 2003 Brent Fox <bfox@redhat.com> 1.1.11-1
- run 'chkconfig --del firstboot' when it's done

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.1.10-1
- allow underscores and dashes in usernames (bug #99115)

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.1.9-1
- replace BuildRequires on python-tools with gettext

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.1.8-2
- bump relnum and rebuild

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.1.8-1
- apply patch from hfuchi@redhat.com for Japanese translation

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.1.7-2
- bump relnum and rebuild

* Thu Jul 31 2003 Brent Fox <bfox@redhat.com> 1.1.7-1
- change runPriority in welcome module

* Tue Jul 22 2003 Brent Fox <bfox@redhat.com> 1.1.6-2
- bump relnum and rebuild

* Tue Jul 22 2003 Brent Fox <bfox@redhat.com> 1.1.6-1
- resolve conflict between "Next" and "Forward" (bug #100498)

* Fri Jul 18 2003 Brent Fox <bfox@redhat.com> 1.1.5-2
- bump relnum and rebuild

* Fri Jul 18 2003 Brent Fox <bfox@redhat.com> 1.1.5-1
- fix conficting nmemonics (bug #99279)

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.1.4-2
- bump relnum and rebuild

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.1.4-1
- remove unneeded code from the neworking module

* Thu Jun 26 2003 Brent Fox <bfox@redhat.com> 1.1.3-1
- first stab at getting locale changing working (#91984)

* Thu Jun 19 2003 Brent Fox <bfox@redhat.com> 1.1.2-2
- bump number and rebuild

* Thu Jun 19 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- implement networking screen for reconfig mode (bug #91984)

* Mon Jun 16 2003 Brent Fox <bfox@redhat.com> 1.1.1-2
- bump number and rebuild

* Mon Jun 16 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- create a timezone module (bug #91984)

* Tue Jun 10 2003 Brent Fox <bfox@redhat.com> 1.0.12-1
- change "Forward" button to "Next"

* Fri May 30 2003 Brent Fox <bfox@redhat.com> 1.0.11-1
- fix traceback in username verification

* Thu May 29 2003 Brent Fox <bfox@redhat.com> 1.0.10-1
- updated deprecated function call in functions.py
- don't grab the whole screen in debug mode

* Thu May 22 2003 Brent Fox <bfox@redhat.com> 1.0.9-1
- remove explicit vt7 argument to X (bug #87636)

* Thu May 22 2003 Brent Fox <bfox@redhat.com> 1.0.8-1
- pass rhgb status into firstbootWindow.py
- don't draw background if rhgb isn't running

* Mon May 19 2003 Brent Fox <bfox@redhat.com> 1.0.7-1
- check to see if rhgb is running
- if rhgb is running, start up metacity and merge X resources

* Fri May 16 2003 Brent Fox <bfox@redhat.com> 1.0.6-2
- Added a mnemonic for network login button (bug #90636)
- added more mnemonics create_user.py (bug #90865)

* Mon Mar 24 2003 Brent Fox <bfox@redhat.com> 1.0.6-1
- add a button to create_user.py to launch authconfig-gtk

* Fri Mar  7 2003 Brent Fox <bfox@redhat.com> 1.0.5-13
- bump rev for 3.0E

* Fri Feb 28 2003 Brent Fox <bfox@redhat.com> 1.0.5-12
- fix bug #85358

* Tue Feb 25 2003 Jeremy Katz <katzj@redhat.com> 1.0.5-11
- background image name changed (#85160)
- don't traceback if background doesn't exist

* Mon Feb 24 2003 Nalin Dahyabhai <nalin@redhat.com> 1.0.5-10
- compare text to string.whitespace, not whitespace (#85038)

* Mon Feb 24 2003 Brent Fox <bfox@redhat.com> 1.0.5-9
- apply initscript patch from mikem@redhat.com.  Avoids calling telinit (bug #84848)

* Fri Feb 21 2003 Brent Fox <bfox@redhat.com> 1.0.5-8
- call RGB_DITHER_MAX (bug #84850)

* Thu Feb 20 2003 Brent Fox <bfox@redhat.com> 1.0.5-7
- fix traceback in create_users.py (bug #84722)

* Tue Feb 18 2003 Brent Fox <bfox@redhat.com> 1.0.5-6
- add mnemonics to up2date.py (bug #84487)

* Wed Feb 12 2003 Brent Fox <bfox@redhat.com> 1.0.5-5
- only launch r-c-xfree86 if in runlevel 3 and XF86Config does not exist (bug #84135)

* Tue Feb 11 2003 Brent Fox <bfox@redhat.com> 1.0.5-4
- fix bug #84068

* Wed Feb  5 2003 Brent Fox <bfox@redhat.com> 1.0.5-3
- check for empty soundcard list, not list = None

* Mon Feb  3 2003 Brent Fox <bfox@redhat.com> 1.0.5-2
- notting fixed the initscript to pull in the locale

* Fri Jan 31 2003 Brent Fox <bfox@redhat.com> 1.0.5-1
- new strings in up2date module

* Wed Jan 29 2003 Brent Fox <bfox@redhat.com> 1.0.4-8
- don't run firstboot in runlevel 3 at all (bug #78239)
- fix return tuple if no soundcards are found in soundcard_gui.py
- use backslash instead of forward slash in the init script

* Wed Jan 29 2003 Brent Fox <bfox@redhat.com> 1.0.4-6
- give the X server 1 second to close before we exit firstboot (bug #81313)

* Tue Jan 28 2003 Brent Fox <bfox@redhat.com> 1.0.4-5
- don't use Yes/No dialogs in create_user.py (bug #82680)
- only use root window mode in non-debug mode
- make module titles consistent with each other

* Fri Jan 24 2003 Brent Fox <bfox@redhat.com> 1.0.4-4
- better validity checking for user name and password

* Tue Jan 21 2003 Brent Fox <bfox@redhat.com> 1.0.4-3
- remove print statement that was causing a traceback

* Mon Jan 20 2003 Brent Fox <bfox@redhat.com> 1.0.4-2
- use the gdm background as the background image

* Fri Jan 17 2003 Brent Fox <bfox@redhat.com> 1.0.4-1
- make hboxes transparent and use a new background

* Mon Jan 13 2003 Brent Fox <bfox@redhat.com> 1.0.3-6
- make it so that we can paint the root window

* Thu Jan  9 2003 Brent Fox <bfox@redhat.com> 1.0.3-5
- sleep .5 sec on exit (bug #81313)

* Mon Dec 23 2002 Brent Fox <bfox@redhat.com> 1.0.3-4
- enforce 6 char user passwords

* Sun Dec 22 2002 Brent Fox <bfox@redhat.com> 1.0.3-3
- require an NTP server if NTP selected in date.py

* Tue Dec 17 2002 Brent Fox <bfox@redhat.com> 1.0.3-2
- don't run x tool if /etc/sysconfig/firstboot exists

* Mon Dec 02 2002 Brent Fox <bfox@redhat.com> 1.0.3-1
- Create a message for machines that boot in runlevel 3

* Tue Nov 26 2002 Brent Fox <bfox@redhat.com> 1.0.2-4
- Mark initscript strings as internationalizable (bug 77826)

* Fri Nov 22 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- exclude mainframe

* Wed Nov 13 2002 Brent Fox <bfox@redhat.com> 1.0.2-3
- fix soundcard and mouse problem

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.0.2-2
- explicitly kill X server pid
- Latest translations

* Thu Sep 05 2002 Brent Fox <bfox@redhat.com> 1.0.1-10
- moved some pixmaps to redhat-logos package
- added a requires for redhat-logos package

* Tue Sep 03 2002 Brent Fox <bfox@redhat.com> 1.0.1-9
- call dithering magic to make new pixmaps appear correctly

* Tue Sep 03 2002 Brent Fox <bfox@redhat.com> 1.0.1-8
- get new pixmaps from garrett

* Fri Aug 30 2002 Brent Fox <bfox@redhat.com> 1.0.1-7
- Wrap umount in a try/except in additional_cds.py

* Fri Aug 30 2002 Brent Fox <bfox@redhat.com> 1.0.1-6
- run chkconfig on starting/stopping ntpd

* Thu Aug 29 2002 Brent Fox <bfox@redhat.com> 1.0.1-5
- Make the date screen update the time widgets when the screen is entered

* Thu Aug 29 2002 Brent Fox <bfox@redhat.com> 1.0.1-4
- Convert po files to UTF-8

* Thu Aug 29 2002 Brent Fox <bfox@redhat.com> 1.0.1-3
- created a flag so that failed connections to NTP servers don't advance the screens
- Don't try to display the parent window pixmap gradient
- Make the parent window background a little darker

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-2
- Fix typo

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-1
- Convert to noarch
- Add a root window mode for firstbootWindow
- Use a gtkInvisible dialog to block on up2date and the package screen

* Wed Aug 28 2002 Karsten Hopp <karsten@redhat.de> 1.0.0-5
- don't require config-mouse, config-keyboard, config-soundcard
  on S390

* Tue Aug 27 2002 Brent Fox <bfox@redhat.com> 1.0.0-4
- fix hang on unreponsive ntp servers
 
* Thu Aug 22 2002 Brent Fox <bfox@redhat.com> 1.0.0-3
- Don't draw window decorations

* Wed Aug 21 2002 Brent Fox <bfox@redhat.com> 1.0.0-2
- pull translation domains from rhpl

* Wed Aug 21 2002 Brent Fox <bfox@redhat.com> 1.0.0-1
- Implement a lowres mode for 640x480 screens

* Thu Aug 15 2002 Brent Fox <bfox@rehdat.com> 0.9.9-13
- Don't show up2date screen if machine is already registered

* Wed Aug 14 2002 Brent Fox <bfox@redhat.com> 0.9.9-12
- fix reconfig mode
- change pixmap on finished screen

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-11
- pass None into startNtpService

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-10
- If there's no XF86Config file, start redhat-config-xfree86 first

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-9
- only start in runlevel 5

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-8
- include cd.png

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-7
- improved UI on additional cd screen

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-6
- Require redhat-config-packages
- Change string in finished module

* Mon Aug 12 2002 Brent Fox <bfox@redhat.com> 0.9.9-5
- Print out a message if user tries to run firstboot again
- move firstboot to firstboot.py and create a shell script for firstboot
- applied a patch from dburcaw@terraplex.com to check to see if we're run as root

* Mon Aug 12 2002 Brent Fox <bfox@redhat.com> 0.9.9-4
- Try to fix race condition
- Have init script return 0 instead of 1
- Make Forward button grab the focus

* Wed Aug 07 2002 Brent Fox <bfox@redhat.com> 0.9.9-1
- Rebuild for Jay, who is being a punk ;)
  
* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Make changes for new pam timestamp policy

* Thu Aug 01 2002 Brent Fox <bfox@redhat.com> 0.9.7-1
- fix typo in finished module
- create better text in the exception screen
- save traceback info in /tmp/firstboot.txt

* Tue Jul 30 2002 Brent Fox <bfox@redhat.com> 0.9.6-5
- merge Xresources on startup.  Fixes bug #68724

* Thu Jul 25 2002 Brent Fox <bfox@redhat.com> 0.9.6-4
- change background color
- give some padding to the icon box
- put new splash and text on welcome and finished modules

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-3
- fix Makefiles and spec files so that translations get installed

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- update spec file for public beta 2

* Tue Jul 23 2002 Brent Fox <bfox@redhat.com> 0.9.6-1
- removed register module
- added a finished module
- pulled in new icons

* Fri Jul 19 2002 Brent Fox <bfox@redhat.com> 0.9.5-1
- wire up register module
- wire up up2date module
- fix pointer pixmap bug
- create an exceptionWindow to capture tracebacks

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- bump rev num and rebuild

* Sat Jul 13 2002 Brent Fox <bfox@redhat.com> 0.9.4-1
- fixed preun script to not blow away runlevel symlinks on upgrades

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-2
- Update changelogs and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-1
- Update changelogs and rebuild

* Mon Jul 01 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Bump rev number

* Fri Jun 28 2002 Brent Fox <bfox@redhat.com> 0.9.1-4
- Require metacity

* Fri Jun 28 2002 Brent Fox <bfox@redhta.com> 0.9.1-3
- Backed out some changes from init script
- Fixed icon path in date module

* Thu Jun 27 2002 Brent Fox <bfox@redhat.com> 0.9.1-2
- Popup warning for unimplemented features

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Only run in runlevel 5

* Tue Jun 25 2002 Brent Fox <bfox@redhat.com> 0.9.0-5
- Change initscript to not start firstboot on runlevel changes

* Mon Jun 24 2002 Brent Fox <bfox@redhat.com> 0.9.0-4
- Fix spec file

* Fri Jun 21 2002 Brent Fox <bfox@redhat.com> 0.9.0-3
- Added snapsrc to makefile
- Rebuild for completeness

* Wed Jun 12 2002 Brent Fox <bfox@redhat.com> 0.2.0-3
- Fixed a string error in the welcome module

* Fri May 31 2002 Brent Fox <bfox@redhat.com> 0.2.0-2
- Some additions to hardware screen

* Fri May 31 2002 Brent Fox <bfox@redhat.com> 0.2.0-1
- Fix hardare screen's run priority

* Thu May 30 2002 Brent Fox <bfox@redhat.com> 0.1.0-8
- Created the beginnings of the hardware screen

* Thu May 30 2002 Brent Fox <bfox@redhat.com> 0.1.0-7
- Fixed Requires to not pull in pygnome

* Tue May 28 2002 Brent Fox <bfox@redhat.com> 0.1.0-6
- Rebuild for completeness
- Fix bug in init script

* Sun May 26 2002 Brent Fox <bfox@redhat.com> 0.1.0-4
- Get startup scripts ready to go
- Prepare package for placement into newest tree
- Install init script into correct place

* Tue Nov 28 2001 Brent Fox <bfox@redhat.com>
- initial coding and packaging

