%global rbname chasen
Summary: Ruby/Chasen extension module
Name: ruby-%{rbname}

%global momorel 5

Version: 1.7
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://raa.ruby-lang.org/list.rhtml?name=ruby-chasen

Source: http://www.itlb.te.noda.sut.ac.jp/~ikarashi/ruby/chasen%{version}.tar.gz
Patch0: ruby-chasen-1.7-ruby19.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: chasen-devel

%description
This extension module is a Ruby binding of Chasen.

%prep
%setup -q -n %{rbname}%{version}
%patch0 -p1 -b .ruby19

%build
ruby extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc chasen.html sample
%{ruby_sitearchdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7-3m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7-1m)
- update 1.7
- addd ruby-1.9 patch

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6-7m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-6m)
- rebuild against chasen-2.4.2-1m

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6-5m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.6-4m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-3m)
- rebuild against ruby-1.8.2

* Sun Feb 22 2004 Toru Hoshina <t@www.momonga-linux.org>
- (1.6-2m)
- No NoSource ;-)

* Thu Jan  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-1m)
- initial import
