%global momorel 7

Name: jwhois
Version: 4.0
Release: %{momorel}m%{?dist}
URL: http://www.gnu.org/software/jwhois/
Source0: ftp://ftp.gnu.org/gnu/jwhois/jwhois-%{version}.tar.gz
NoSource: 0
# download newer jwhois.conf
# http://cvs.savannah.gnu.org/viewvc/*checkout*/jwhois/jwhois/example/jwhois.conf
Source1: jwhois.conf
Patch0: jwhois-4.0-connect.patch
Patch1: jwhois-4.0-ipv6match.patch
License: GPLv3
Group: Applications/Internet
Summary: Internet whois/nicname client
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libidn-devel
Requires(post): info
Requires(preun): info

%description
A whois client that accepts both traditional and finger-style queries.

%prep
%setup -q
%patch0 -p1 -b .connect
%patch1 -p1 -b .ipv6match

iconv -f iso-8859-1 -t utf-8 < doc/sv/jwhois.1 > doc/sv/jwhois.1_
mv doc/sv/jwhois.1_ doc/sv/jwhois.1

# Install newer jwhois.conf
cp -p %{SOURCE1} example/jwhois.conf

%build
%configure
make %{?_smp_mflags}

%install
rm -fr $RPM_BUILD_ROOT
make install DESTDIR="$RPM_BUILD_ROOT"
rm -f "$RPM_BUILD_ROOT"%{_infodir}/dir
%find_lang jwhois

# Make "whois" jwhois.
ln -sf jwhois $RPM_BUILD_ROOT/%{_bindir}/whois
echo .so man1/jwhois.1 > $RPM_BUILD_ROOT/%{_mandir}/man1/whois.1

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/*
%{_mandir}/man1/*
%lang(sv) %{_mandir}/sv/man1/jwhois.1*
%{_infodir}/jwhois.info.*
%config(noreplace) %{_sysconfdir}/jwhois.conf

%post
/sbin/install-info %{_infodir}/jwhois.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/jwhois.info %{_infodir}/dir || :
fi

%clean
rm -fr $RPM_BUILD_ROOT

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-5m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-3m)
- rebuild against rpm-4.6

* Tue Jul 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-2m)
- update jwhois.conf

* Mon Jun 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-1m)
- sync Fedora
- update 4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-2m)
- rebuild against gcc43

* Thu Feb  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.3-1m)
- version up

* Wed Mar 30 2005 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.2.2-6m)
- Use CVS version of jwhois.conf (rev.1.114).
  This includes the change of whois servers of .jp domain
  (whois.nic.ad.jp -> whois.jprs.jp),
  IPv6 Sub-TLA Assignments, etc.
- Remove jwhois-3.2.2-typos.patch. 
  This fix has already been in original jwhois.conf.

* Wed Jul 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.2-5m)
- fix typos in jwhois.conf

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.2-4m)
- revised spec for enabling rpm 4.2.

* Mon Dec 22 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.2-3m)
- accept gdbm-1.8.0 or newer

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.2-2m)
- rebuild against gdbm-1.8.0

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.2-1m)
- update to 3.2.2

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.2.1-3m)
- rebuild against for gdbm

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.2.1-2m)
  rebuild against libgdbm

* Wed Feb  5 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.1-1m)
- upgrade to 3.2.1

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.0-1m)
- upgrade to 3.2.0

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.1.0-4k)

* Mon Jan 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.1.0-2k)

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (3.0.1-4k)
- rebuild against gettext 0.10.40.

* Sat Oct 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0.1-2k)

* Wed Mar 28 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4.2-3k)
- update to 2.4.2
- /var/tmp to %{_tmppath}
- changed Source: URL to ftp.ring.gr.jp (temporary)
- jwhois.info to jwhois.info*  (.info.bz2)

* Sun Jun 18 2000 AYUHANA Tomonori <l@kondara.org>
- remove Vender tag :-P

* Mon Jun 12 2000 Kenji Yamaguchi <yamk@sophiaworks.com>
  [jwhois-2.4.1-2k1]
- Get from RedHat contrib.
- for Kondara MNU/Linux 1.1.
- %pre and %post script stops at new rpm command.
  ORG:    [  ] && hoge
  NOW:    if [  ]; then  hoge ; fi
- NoSrc.
- speclint refresh: TAB to SPACE. and "License:" changes to "Copyright:"
