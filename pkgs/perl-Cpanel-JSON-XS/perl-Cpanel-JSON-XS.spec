%global         momorel 3

Name:           perl-Cpanel-JSON-XS
Version:        2.3403
Release:        %{momorel}m%{?dist}
Summary:        JSON::XS for Cpanel, fast and correct serialising, also for 5.6.2
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Cpanel-JSON-XS/
Source0:        http://www.cpan.org/authors/id/R/RU/RURBAN/Cpanel-JSON-XS-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-common-sense >= 3.5
BuildRequires:  perl-Encode >= 1.9801
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-podlators >= 2.08
BuildRequires:  perl-Pod-Usage >= 1.33
Requires:       perl-common-sense >= 3.5
Requires:       perl-Encode >= 1.9801
Requires:       perl-podlators >= 2.08
Requires:       perl-Pod-Usage >= 1.33
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module converts Perl data structures to JSON and vice versa. Its
primary goal is to be correct and its secondary goal is to be fast. To
reach the latter goal it was written in C.

%prep
%setup -q -n Cpanel-JSON-XS-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING META.json README
%{_bindir}/cpanel_json_xs
%{perl_vendorarch}/auto/Cpanel/JSON/XS/XS.so
%{perl_vendorarch}/Cpanel/JSON/XS.pm
%{perl_vendorarch}/Cpanel/JSON/XS
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3403-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3403-2m)
- rebuild against perl-5.18.2

* Sat Nov 09 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3403-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
