%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.9.90
%global         kdelibsrel 1m
%global         qtver 4.8.4
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 3.0
%global         sourcedir %{release_dir}/active/%{ftpdirver}/src

Name:           share-like-connect
Summary:        Share, like and connect concept for Plasma Active
Version:        0.3
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          User Interface/Desktops
URL:            https://projects.kde.org/projects/playground/base/share-like-connect
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
# Fix missing linking to KDE4_QTGUI_LIBS and fix messed qt stuff
# upstream me! -- rex
Patch0:         share-like-connect-0.2-fix-build.patch

BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  kde-runtime-devel >= %{kdever}
BuildRequires:  kactivities-devel >= %{kdever}
Requires:       kde-runtime >= %{kdever}

%description
Share, like and connect concept for Plasma Active.

Share-like-connect is part of Plasma Active project.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p1 -b .fix-build

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc LICENSE.LGPL-2 LICENSE.LGPL-2.1
%{_kde4_libdir}/kde4/imports/org/kde/plasma/slccomponents/
%{_kde4_libdir}/kde4/plasma_dataengine_sharelikeconnect.so
%{_kde4_libdir}/kde4/sharelikeconnect_provider_*.so
%{_kde4_libdir}/libactivecontentservice.so
%{_kde4_libdir}/libsharelikeconnect.so
%{_kde4_appsdir}/plasma/plasmoids/org.kde.sharelikeconnect
%{_kde4_appsdir}/plasma/services/slcservice.operations
%{_kde4_appsdir}/plasma/slcmenuitems
%{_kde4_datadir}/kde4/services/plasma-applet-sharelikeconnect.desktop
%{_kde4_datadir}/kde4/services/plasma-dataengine-sharelikeconnect.desktop
%{_kde4_datadir}/kde4/services/sharelikeconnect-provider-*.desktop
%{_kde4_datadir}/kde4/servicetypes/plasma-sharelikeconnect-provider.desktop

%files devel
%{_kde4_includedir}/activecontentservice

%changelog
* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Sat Oct 27 2012 Rex Dieter <rdieter@fedoraproject.org> 0.3-1
- 0.3

* Thu Aug 02 2012 Than Ngo <than@redhat.com> - 0.2-4
- don't link to nepomukdatamanagement

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed Mar 07 2012 Radek Novacek <rnovacek@redhat.com> 0.2-2
- Rebuild with 4.8.1
- Add BR: kde-runtime-devel

* Sat Feb 18 2012 Radek Novacek <rnovacek@redhat.com> 0.2-1
- Initial package
