%global momorel 3
Summary: MacOS X-style menu-based user-switching
Name: fast-user-switch-applet

Version: 2.24.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.24/%{name}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.12.8
BuildRequires: gnome-panel-devel >= 2.20.3
BuildRequires: libglade2-devel >= 2.6.2
BuildRequires: pango-devel >= 1.18.4
BuildRequires: libXau-devel >= 1.0.3
BuildRequires: libXmu-devel >= 1.0.4
BuildRequires: libX11-devel >= 1.1.4
BuildRequires: gdm
BuildRequires: GConf2
Requires: gdm

%description
The Fast User Switch Applet is an applet for the GNOME 2.10 panel
which allows for MacOS X-style menu-based user-switching.


%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    --disable-scrollkeeper \
    --with-users-admin="system-config-users" \
    --with-gdm-setup="gdmsetup" \
    --with-gdm-config="%{_sysconfdir}/X11/gdm/custom.conf"
make CFLAGS+="`pkg-config --cflags libgnomeui-2.0`" LIBS+="`pkg-config --libs libgnomeui-2.0`"


%install
rm -rf --preserve-root %{buildroot}
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%make DESTDIR=%{buildroot} install
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
for i in \
    fast-user-switch-applet.schemas \
    ; do
    gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/$i > /dev/null
done
scrollkeeper-update

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    for i in \
        fast-user-switch-applet.schemas \
        ; do
        gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/$i > /dev/null
    done
fi

%postun
/sbin/ldconfig
scrollkeeper-update

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README TODO
%{_sysconfdir}/gconf/schemas/fast-user-switch-applet.schemas
%{_libdir}/bonobo/servers/GNOME_FastUserSwitchApplet.server
%{_libexecdir}/fast-user-switch-applet
%{_datadir}/fast-user-switch-applet/ui.glade
%{_datadir}/gnome-2.0/ui/GNOME_FastUserSwitchApplet.xml
%{_datadir}/gnome/help/fast-user-switch-applet
%{_datadir}/locale/*/*/*
%{_datadir}/omf/fast-user-switch-applet

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-2m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0 (but too old source) TO.Orphan ?

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22.0-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-4m)
- rebuild against rpm-4.6

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-3m)
- change %%preun script

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Mon Mar 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.0-2m)
- %%NoSource -> NoSource

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-1m)
- update to 2.17.4

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.3-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.3-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.3-1m)
- update to 2.17.3 (unstable)

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.16.0-2m)
- add BuildPrereq: gdm

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- Initial Build
