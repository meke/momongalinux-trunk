%global momorel 1

Summary: Simple kernel loader which boots from a FAT filesystem
Name: syslinux
Version: 6.02
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://syslinux.zytor.com/
Source0: ftp://ftp.kernel.org/pub/linux/utils/boot/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0001: 0001-Add-install-all-target-to-top-side-of-HAVE_FIRMWARE.patch
Patch0002: 0002-Don-t-build-efi32.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{ix86} athlon x86_64
BuildRequires: nasm >= 0.98.35
BuildRequires: perl, netpbm-progs
BuildRequires: /usr/include/gnu/stubs-32.h
Requires: syslinux-nonlinux = %{version}-%{release}
AutoReq: 0
%ifarch %{ix86} athlon
Requires: mtools, libc.so.6
%endif
%ifarch x86_64
Requires: mtools, libc.so.6()(64bit)
%endif

Obsoletes: syslinux-devel < %{version}-%{release}
Provides: syslinux-devel


# NOTE: extlinux belongs in /sbin, not in /usr/sbin, since it is typically
# a system bootloader, and may be necessary for system recovery.
%define _sbindir /sbin

%description
SYSLINUX is a suite of bootloaders, currently supporting DOS FAT
filesystems, Linux ext2/ext3 filesystems (EXTLINUX), PXE network boots
(PXELINUX), or ISO 9660 CD-ROMs (ISOLINUX).  It also includes a tool,
MEMDISK, which loads legacy operating systems from these media.

%package perl
Summary: Syslinux tools written in perl
Group: Applications/System

%description perl
Syslinux tools written in perl

%package devel
Summary: Headers and libraries for syslinux development.
Group: Development/Libraries

%description devel
Headers and libraries for syslinux development.

%package extlinux
Summary: The EXTLINUX bootloader, for booting the local system.
Group: Applications/System
Requires: syslinux
Requires: syslinux-extlinux-nonlinux = %{version}-%{release}

%description extlinux
The EXTLINUX bootloader, for booting the local system, as well as all
the SYSLINUX/PXELINUX modules in /boot.

%package tftpboot
Summary: SYSLINUX modules in /tftpboot, available for network booting
Group: Applications/Internet
#BuildArch: noarch
Requires: syslinux

%description tftpboot
All the SYSLINUX/PXELINUX modules directly available for network
booting in the /tftpboot directory.

%package extlinux-nonlinux
Summary: The parts of the EXTLINUX bootloader which aren't run from linux.
Group: Applications/System
Requires: syslinux
#BuildArch: noarch

%description extlinux-nonlinux
All the EXTLINUX binaries that run from the firmware rather than
from a linux host.

%package nonlinux
Summary: SYSLINUX modules which aren't run from linux.
Group: Applications/System
Requires: syslinux
#BuildArch: noarch

%description nonlinux
All the SYSLINUX binaries that run from the firmware rather than from a
linux host. It also includes a tool, MEMDISK, which loads legacy operating
systems from media.

%ifarch %{x86_64}
%package efi64
Summary: SYSLINUX binaries and modules for 64-bit UEFI systems
Group: Applications/System

%description efi64
SYSLINUX binaries and modules for 64-bit UEFI systems
%endif

%prep
%setup -q

%patch0001 -p1
%patch0002 -p1

%build
make bios clean all
%ifarch %{x86_64}
make efi64 clean all
%endif

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_prefix}/lib/syslinux
mkdir -p %{buildroot}%{_includedir}
make bios install-all \
	INSTALLROOT=%{buildroot} BINDIR=%{_bindir} SBINDIR=%{_sbindir} \
	LIBDIR=%{_prefix}/lib DATADIR=%{_datadir} \
	MANDIR=%{_mandir} INCDIR=%{_includedir} \
	TFTPBOOT=/tftpboot EXTLINUXDIR=/boot/extlinux \
	LDLINUX=ldlinux.c32
%ifarch %{x86_64}
make efi64 install netinstall \
	INSTALLROOT=%{buildroot} BINDIR=%{_bindir} SBINDIR=%{_sbindir} \
	LIBDIR=%{_prefix}/lib DATADIR=%{_datadir} \
	MANDIR=%{_mandir} INCDIR=%{_includedir} \
	TFTPBOOT=/tftpboot EXTLINUXDIR=/boot/extlinux \
	LDLINUX=ldlinux.c32
%endif

mkdir -p %{buildroot}/%{_docdir}/%{name}/sample
install -m 644 sample/sample.* %{buildroot}/%{_docdir}/%{name}/sample/
mkdir -p %{buildroot}/etc
( cd %{buildroot}/etc && ln -s ../boot/extlinux/extlinux.conf . )

# don't ship libsyslinux, at least, not for now
rm -f %{buildroot}%{_prefix}/lib/libsyslinux*
rm -f %{buildroot}%{_includedir}/syslinux.h

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc NEWS README* COPYING 
%doc doc/* 
%doc sample
%{_mandir}/man1/gethostip*
%{_mandir}/man1/syslinux*
%{_mandir}/man1/extlinux*
%{_bindir}/gethostip
%{_bindir}/isohybrid
%{_bindir}/memdiskfind
%{_bindir}/syslinux
%dir %{_datadir}/syslinux
%dir %{_datadir}/syslinux/dosutil
%{_datadir}/syslinux/dosutil/*
%{_datadir}/syslinux/diag/*
%{_datadir}/doc/syslinux/sample/*
#%%ifarch %{ix86}
#%%{_datadir}/syslinux/syslinux.exe
#%%else
#%%{_datadir}/syslinux/syslinux64.exe
#%%endif

%files perl
%defattr(-,root,root)
%{_mandir}/man1/lss16toppm*
%{_mandir}/man1/ppmtolss16*
%{_mandir}/man1/syslinux2ansi*
%{_bindir}/keytab-lilo
%{_bindir}/lss16toppm
%{_bindir}/md5pass
%{_bindir}/mkdiskimage
%{_bindir}/ppmtolss16
%{_bindir}/pxelinux-options
%{_bindir}/sha1pass
%{_bindir}/syslinux2ansi
%{_bindir}/isohybrid.pl

%files devel
%defattr(-,root,root)
%dir %{_datadir}/syslinux/com32
%{_datadir}/syslinux/com32

%files extlinux
%{_sbindir}/extlinux
%config /etc/extlinux.conf

%files tftpboot
/tftpboot

%files nonlinux
%{_datadir}/syslinux/*.com
#%%{_datadir}/syslinux/*.exe
%{_datadir}/syslinux/*.c32
%{_datadir}/syslinux/*.bin
%{_datadir}/syslinux/*.0
%{_datadir}/syslinux/memdisk

%files extlinux-nonlinux
/boot/extlinux

%ifarch %{x86_64}
%files efi64
%dir %{_datadir}/syslinux/efi64
%{_datadir}/syslinux/efi64
%endif

%post extlinux
# If we have a /boot/extlinux.conf file, assume extlinux is our bootloader
# and update it.
if [ -f /boot/extlinux/extlinux.conf ]; then \
	extlinux --update /boot/extlinux ; \
elif [ -f /boot/extlinux.conf ]; then \
	mkdir -p /boot/extlinux && \
	mv /boot/extlinux.conf /boot/extlinux/extlinux.conf && \
	extlinux --update /boot/extlinux ; \
fi

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.02-1m)
- update to 6.02

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.01-1m)
- update to 5.01

* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.00-1m)
- update to 5.00

* Thu Dec 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.05-1m)
- update 4.05

* Fri Jul 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.04-1m)
- update 4.04

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.03-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.03-1m)
- update 4.03

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.02-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.02-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.02-1m)
- update 4.02

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.86-1m)
- update 3.86

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.75-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.75-1m)
- update to 3.75
- BuildRequires: /usr/include/gnu/stubs-32.h
- License: GPLv2+

* Mon Mar 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.73-1m)
- update 3.73

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.63-2m)
- rebuild against rpm-4.6

* Sat May 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.63-1m)
- version up

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.61-1m)
- version up

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.36-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.36-4m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.36-3m)
- delete Patch1(syslinux-3.31-time.patch)

* Mon Jun 18 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.36-2m)
- add patch3 and patch4
- fix countdown on boot images (#229491)
- switch to preferring 16bpp for graphical menu; this fixes the display for
  qemu, kvm, etc

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.36-1m)
- version up
- sync with fc-devel

* Sun Jan  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.10-1m)
- version up
- sync with fc-devel

* Mon Feb 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.11-1m)
- ver up.

* Mon Jan 12 2004 Kenta MURATA <muraken2@nifty.com>
- (2.08-1m)
- version 2.08.

* Wed Apr 23 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.04-1m)
  update to 2.04

* Sun Apr 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.03-1m)
  update to 2.03

* Sun Feb 16 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.02-1m)
  update to 2.02

* Fri Jul 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.67-7m)
- add more documents

* Sun Apr  21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.67-6k)
- BuildRequires: netpbm-progs

* Sun Apr  14 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.67-4k)
- nasm 0.98.22 hitsuyo-

* Fri Apr  12 2002 Toru Hoshina <t@kondara.org>
- (1.67-2k)
- ver up. from mandrake...

* Tue Jan 22 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.66-2k)
- update to 1.66

* Tue Oct  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.63-2k)
- update to 1.63

* Wed May 02 2001 Motonobu Ichimura <famao@kondara.org>
- up to 1.62

* Tue Jul 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- add %%defattr (release 4)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_tmppath}
- change application group (Applications/Internet doesn't seem
  right to me)
- added BuildRequires

* Tue Apr 04 2000 Erik Troan <ewt@redhat.com>
- initial packaging
