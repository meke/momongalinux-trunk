%global momorel 1

Summary: Live syncing (mirroring) daemon
Name: lsyncd
Version: 2.0.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/File
URL: http://code.google.com/p/lsyncd/
Source0: http://lsyncd.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rsync
BuildRequires: lua-devel

%description
Lsyncd uses rsync to synchronize local directories with a remote machine
running rsyncd. Lsyncd watches multiple directories trees through inotify.

The first step after adding the watches is to, rsync all directories with
the remote host, and then sync single file buy collecting the inotify events.
So lsyncd is a light-weight live mirror solution that should be easy to
install and use while blending well with your system.

%prep
%setup -q

%build
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc COPYING ChangeLog 
%{_datadir}/doc/lsyncd
%{_bindir}/lsyncd
%{_mandir}/man?/*.*

%changelog
* Fri Aug 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update 2.0.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.26-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-3m)
- change to new URL

* Mon Mar  9 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.26-2m)
- install lsyncd.conf.xml

* Mon Mar  9 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.26-1m)
- import from dag to Momonga
- update and Momonganize

* Thu Mar 27 2008 Dag Wieers <dag at wieers.com> - 1.0-1
- Initial package. (using DAR)
