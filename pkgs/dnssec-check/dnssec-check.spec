%global momorel 1

Summary: A tool to check DNSSEC capabilities of the local DNS resolvers
Name: dnssec-check
Version: 1.12
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.dnssec-tools.org/
Source0: https://www.dnssec-tools.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: dnssec-check.desktop
Patch1: dnssec-check-fix-install-path.1.12.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: qt-devel
BuildRequires: dnssec-tools-libs-devel >= 1.12
BuildRequires: openssl-devel
BuildRequires: desktop-file-utils
Requires: dnssec-tools-libs >= 1.12

%description
DNSSEC-Check provides a GUI mechansim for scanning the system's
configured DNS resolvers to see if they support DNSSEC.  For each test
the utility displays a graphical red/green light indicating the result
of the test.  Clicking on a light will explain the status of the light
and what was tested.

%prep
%setup -q 

%patch1 -p1 -b .installpath

%build
qmake-qt4 PREFIX=/usr
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install INSTALL_ROOT=%{buildroot}

%{__mkdir_p} %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/
%{__install} -p -m 644 images/dnssec-check.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%{__mkdir_p} %{buildroot}/%{_mandir}/man1
%{__install} -p -D -m 644 man/dnssec-check.1 %{buildroot}/%{_mandir}/man1/dnssec-check.1

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor &> /dev/null
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc COPYING
%doc %{_mandir}/man1/*
%{_bindir}/dnssec-check
%{_datadir}/icons/hicolor/*/apps/*.svg
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/applications/dnssec-check.desktop

%changelog
* Thu Mar 29 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-1m)
- import from Fedora devel

* Fri Jan 27 2012 Wes Hardaker <wjhns174@hardakers.net> - 1.12-1
- Upgraded to version 1.12

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.11.p2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Oct 27 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11.p2-1
- updated to upstream version with man page and COPYING file

* Mon Oct 17 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11-1
- Initial version for approval

