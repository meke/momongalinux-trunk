%global         momorel 1

Name:           perl-Net-DNS-SEC
Version:        0.19
Release:        %{momorel}m%{?dist}
Summary:        DNSSEC modules for Perl
License:        GPL+ or Artistic 
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-DNS-SEC/
Source0:	http://www.cpan.org/authors/id/N/NL/NLNETLABS/Net-DNS-SEC-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Crypt-OpenSSL-Bignum >= 0.03
BuildRequires:  perl-Crypt-OpenSSL-DSA >= 0.1
BuildRequires:  perl-Crypt-OpenSSL-RSA >= 0.19
BuildRequires:  perl-Digest-BubbleBabble >= 0.01
BuildRequires:  perl-Digest-SHA >= 5.23
BuildRequires:  perl-Digest-SHA1
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Math-BigInt
BuildRequires:  perl-MIME-Base32
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-Net-DNS >= 0.64
BuildRequires:  perl-Test-Simple >= 0.47
BuildRequires:  perl-Time-Local
Requires:       perl-Crypt-OpenSSL-Bignum >= 0.03
Requires:       perl-Crypt-OpenSSL-DSA >= 0.1
Requires:       perl-Crypt-OpenSSL-RSA >= 0.19
Requires:       perl-Digest-BubbleBabble >= 0.01
Requires:       perl-Digest-SHA >= 5.23
Requires:       perl-Digest-SHA1
Requires:       perl-Math-BigInt
Requires:       perl-MIME-Base32
Requires:       perl-MIME-Base64
Requires:       perl-Net-DNS >= 0.64
Requires:       perl-Test-Simple >= 0.47
Requires:       perl-Time-Local
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Net::DSN::SEC suite provides the resource records that are needed
for DNSSEC (RFC 4033, 4034 and 4035).

It also provides support for SIG0. That later is useful for dynamic
updates using key-pairs.

RSA and DSA crypto routines are supported.

%prep
%setup -q -n Net-DNS-SEC-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%doc demo/
%{perl_vendorlib}/Net/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- rebuild against perl-5.20.0
- update to 0.19

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-15m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-14m)
- all tests successful

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.10.1

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sun Feb 15 2009 NARITA Koichi <pulsar:momonga-linux.org>
- (0.14-2m)
- fix BuildRequres and duplications

* Tue Feb  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-1m)
- import from Fedora to Momonga for dnssec-tools

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.14-3
Rebuild for new perl

* Wed Jul 11 2007  Wes Hardaker <wjhns174@hardakers.net> - 0.14-2
- BuildRequire Digest::SHA
- include the demo scripts in the documentation

* Wed Apr 18 2007  Wes Hardaker <wjhns174@hardakers.net> - 0.14-1
- Initial version
