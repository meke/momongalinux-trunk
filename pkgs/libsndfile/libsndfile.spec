%global momorel 2
%global octave_version 3.6.1
%{?!with_octave:         %global with_octave         0}

Summary: C library for reading and writing files containing sampled sound
Name: libsndfile
Version: 1.0.25
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.mega-nerd.com/libsndfile/
Group: Development/Libraries
Source0: http://www.mega-nerd.com/%{name}/files/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-1.0.20-octave34.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: flac-devel >= 1.1.4
BuildRequires: libtool
%if %{with_octave}
BuildRequires: octave-devel = %{octave_version}
BuildRequires: atlas-devel
%else
Obsoletes: %{name}-octave < %{version}-%{release}
%endif

BuildRequires: pkgconfig

### include local configuration
%{?include_specopt}
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/libsndfile.specopt and edit it.
%{?!do_test: %global do_test 0}

%description
Libsndfile is a C library for reading and writing files containing
sampled sound (such as MS Windows WAV and the Apple/SGI AIFF format)
through one standard library interface.

%package devel
Summary: libsndfile header files and development documentation
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Header files and development documentation for libsndfile.

%if %{with_octave}
%package octave
Summary: libsndfile modules for octave
Group: Applications/Engineering
Requires: %{name} = %{version}-%{release}
Requires: octave = %{octave_version}

%description octave
A couple of script files for loading, saving, and playing sound
files from within Octave.
%endif

%prep
%setup -q

%patch0 -p1 -b .octave34~

# for Patch0
libtoolize
aclocal -I M4
autoconf
automake

%build
%configure \
	--enable-experimental \
%if %{with_octave}
	--enable-octave
%endif
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'

%if ! %{with_octave}
rm -rf %{buildroot}%{_datadir}/octave/site/m/*
%endif

# remove unnecessary files
rm -rf %{buildroot}%{_docdir}/libsndfile1-dev

find %{buildroot} -name "*.la" -delete

%check
%if %{do_test}
make check
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/sndfile-cmp
%{_bindir}/sndfile-concat
%{_bindir}/sndfile-convert
%{_bindir}/sndfile-deinterleave
%{_bindir}/sndfile-info
%{_bindir}/sndfile-interleave
%{_bindir}/sndfile-metadata-get
%{_bindir}/sndfile-metadata-set
%{_bindir}/sndfile-play
%{_bindir}/sndfile-regtest
%{_bindir}/sndfile-salvage

%{_libdir}/libsndfile.so.*
%{_mandir}/man1/sndfile-cmp.1*
%{_mandir}/man1/sndfile-concat.1*
%{_mandir}/man1/sndfile-convert.1*
%{_mandir}/man1/sndfile-deinterleave.1*
%{_mandir}/man1/sndfile-info.1*
%{_mandir}/man1/sndfile-interleave.1*
%{_mandir}/man1/sndfile-metadata-get.1*
%{_mandir}/man1/sndfile-metadata-set.1*
%{_mandir}/man1/sndfile-play.1*

%files devel
%defattr(-,root,root,-)
%doc doc/*.html doc/*.jpg
%{_includedir}/sndfile.h
%{_includedir}/sndfile.hh
%{_libdir}/pkgconfig/sndfile.pc
%{_libdir}/libsndfile.a
%{_libdir}/libsndfile.so

%if %{with_octave}
%files octave
%defattr(644,root,root,755)
%{_datadir}/octave/site/m/*
%defattr(755,root,root,-)
%{_libdir}/octave/%{octave_version}/site/oct/%{_arch}-*-linux-gnu/*
%endif

%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.25-2m)
- set build without octave as octave-3.6.1 is not supported

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.25-1m)
- update to 1.0.25

* Sun Jun 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24-5m)
- rebuild against octave-3.4.2

* Wed Jun 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.24-4m)
- rebuild for octave 3.4.1

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.24-3m)
- add testsuite

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.24-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.24-1m)
- update to 1.0.24

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.21-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.21-5m)
- full rebuild for mo7 release

* Thu Jun  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.21-4m)
- add BuildRequires

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.21-3m)
- add execute bit

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.21-2m)
- rebuild against readline6

* Sat Jan 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.21-1m)
- version 1.0.21
- remove merged sigfpe.patch
- fix up BuildRequires
- rebuild against octave-3.2.4

* Thu Dec 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-5m)
- rebuild against octave-3.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-3m)
- libsndfile package does not depend on octave

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-2m)
- [SECURITY] CVE-2009-4835
- import upstream changes (bzr r1340)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.20-1m)
- [SECURITY] CVE-2009-1788 CVE-2009-1791
- update to 1.0.20

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-4m)
- rebuild against octave-3.0.5

* Sat Mar  7 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.19-3m)
- revised %%files for x86_64

* Wed Mar  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-2m)
- enable octave
-- configure script is buggy

* Wed Mar  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-1m)
- [SECURITY] CVE-2009-0186
- update to 1.0.19
- License: LGPLv2+

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.18-1m)
- version 1.0.18
- delete patch0,1 (merged)
- delete patch2 (file was deleted)
- octave file was not created ;-P

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.17-7m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.17-6m)
- use autoreconf (for autoconf-2.63)
- add patch2 (for automake-1.10.2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.17-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.17-4m)
- %%NoSource -> NoSource

* Thu Sep 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.17-3m)
- [SECURITY] CVE-2007-4974
- import security patch from Gentoo

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.17-2m)
- fix %%changelog section

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-1m)
- version 1.0.17
- import libsndfile-1.0.17+flac-1.1.3.patch from cooker
 +* Mon Dec 11 2006 Gz Waschk <waschk@mandriva.org> 1.0.17-3mdv2007.1
 +- patch for new libflac

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.15-2m)
- delete libtool library

* Wed Apr 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.15-1m)
- update to 1.0.15

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.11-2m)
- enable x86_64.

* Mon Oct 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.11-1m)
- update to 1.0.11

* Mon Oct 04 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10
- make install transform='s,x,x,'
- with octave

* Fri Jun 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.9-3m)
- main package should include not 'lib*.so.*.*' but 'lib*.so.*'

* Thu May 20 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.9-2m)
- without octave.

* Tue May 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.9-1m)
- verup

* Sun Dec 14 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.5-1m)
- first import to Momonga

* Fri Jun 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.5-0.0.1m)
- verision 1.0.5

* Tue Feb 4 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.4-0.0.1m)
- adopted src.rpm of PLD Linux at rpmfind.net to version 1.0.4

- %{date} PLD Team <feedback@pld.org.pl>
- All persons listed below can be reached at <cvs_login>@pld.org.pl

Revision 1.26  2002/10/06 08:54:16  kloczek
- release 2: libsndfile1{,-devel} added to Obsletes for allow upgrade from MDK.

Revision 1.25  2002/09/14 20:18:25  qboosh
- amfix patch (fix for automake 1.6)
- added missing pkgconfig file
- moved devel docs to -devel

Revision 1.24  2002/09/14 09:35:17  zagrodzki
- upgraded to 1.0.1
- STBR

Revision 1.23  2002/06/23 20:32:31  kloczek
- perl -pi -e "s/^libtoolize --copy --force/\%\{__libtoolize\}/"

Revision 1.22  2002/05/21 23:13:59  kloczek
perl -pi -e "s/^automake -a -c -f --foreing/\%\{__automake\}/; \
             s/^automake -a -c -f/\%\{__automake\}/; \
	     s/^autoconf/\%\{__autoconf\}/"

Revision 1.21  2002/05/11 17:58:45  kloczek
- updated to 0.0.28.

Revision 1.20  2002/03/24 23:28:20  kloczek
- perl -pi -e "s/^automake -a -c$/automake -a -c -f/"

Revision 1.19  2002/02/22 23:29:12  kloczek
- removed all Group fields translations (oure rpm now can handle translating
  Group field using gettext).

Revision 1.18  2002/01/18 02:13:42  kloczek
perl -pi -e "s/pld-list\@pld.org.pl/feedback\@pld.org.pl/"

Revision 1.17  2001/11/21 11:34:13  kloczek
- updated to 0.0.27.

Revision 1.16  2001/10/06 21:03:28  kloczek
- updated to 0.0.26.

Revision 1.15  2001/09/24 09:11:35  kloczek
- updated to 0.0.25.

Revision 1.14  2001/08/27 18:39:38  kloczek
- updated to 0.0.24.

Revision 1.13  2001/07/19 21:56:09  kloczek
- updated to 0.0.23.

Revision 1.12  2001/06/27 07:56:25  kloczek
- regenerate libtool files.

Revision 1.11  2001/06/24 22:42:47  kloczek
p release 3: "rm -f missing" before run automake.

Revision 1.10  2001/01/22 09:38:24  kloczek
- release 2,
- regenerate automake, autoconf files (added automake to BuildRequires),
- removed "install -d .." line from top %%install.

Revision 1.9  2000/10/19 01:45:03  kloczek
- autoconf patch is back.

Revision 1.8  2000/10/18 21:50:14  kloczek
- updated to 0.0.22,
- emoved outdated autoconf patch,
- use new rpm automation.

Revision 1.7  2000/08/21 00:30:51  kloczek
- updated to 0.0.21.

Revision 1.6  2000/06/09 07:54:44  kloczek
- more %%{__make} macros.

Revision 1.5  2000/06/09 07:23:24  kloczek
- added using %%{__make} macro.

Revision 1.4  2000/06/05 16:23:26  kloczek
- updated to 0.0.20.

Revision 1.3  2000/05/30 01:33:28  kloczek
- updated to 0.0.19,
- spec adapterized.

Revision 1.2  2000/05/14 21:36:36  kloczek
- added autoconf patch with corrections which allow correctly pass
  $RPM_OPT_FLAGS to compile options,
- cosmetics.

Revision 1.1  2000/05/12 11:33:28  misiek
