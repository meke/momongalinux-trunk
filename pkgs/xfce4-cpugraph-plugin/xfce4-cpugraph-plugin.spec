%global momorel 1

%global xfce4ver 4.10.0
%global major 1.0

Summary: 	XFce4 cpugraph plugin
Name: 		xfce4-cpugraph-plugin
Version: 	1.0.5
Release:	%{momorel}m%{?dist}

Group: 		User Interface/Desktops
License:	GPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  gtk2-devel
BuildRequires:  libxml2-devel
BuildRequires:  pkgconfig

%description 
A CPU monitor plugin for the Xfce panel. It offers multiple display modes 
(LED, gradient, fire, etc...) to show the current CPU load of the system. The 
colors and the size of the plugin are customizable.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
%find_lang %{name}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README NEWS
%{_libdir}/xfce4/panel/plugins/libcpugraph.so
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/locale/*
%{_datadir}/xfce4/panel/plugins/*.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.5-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-11m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-8m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-7m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-5m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-4m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- update
- rebuild against xfce4 4.4.2
- only gz file
- add locale file at files section

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-7m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-6m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-5m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-4m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.2-3m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-2m)
- rebuild against xfce4 4.1.90

* Sun Jun 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.2-1m)
- import to Momonga
