%global         momorel 3

Name:           perl-Package-Stash
Version:        0.36
Release:        %{momorel}m%{?dist}
Summary:        Routines for manipulating stashes
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Package-Stash/
Source0:        http://www.cpan.org/authors/id/D/DO/DOY/Package-Stash-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Dist-CheckConflicts
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Package-DeprecationManager
BuildRequires:  perl-Package-Stash-XS >= 0.26
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Dist-CheckConflicts
Requires:       perl-Package-DeprecationManager
Requires:       perl-Package-Stash-XS >= 0.26
Requires:       perl-Scalar-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Manipulating stashes (Perl's symbol tables) is occasionally necessary, but
incredibly messy, and easy to get wrong. This module hides all of that
behind a simple API.

%prep
%setup -q -n Package-Stash-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/^perl(Package::Stash::Conflicts/d'

EOF
%define __perl_requires %{_builddir}/Package-Stash-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE README
%{_bindir}/package-stash-conflicts
%{perl_vendorlib}/Package/Stash
%{perl_vendorlib}/Package/Stash.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.18.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-2m)
- rebuild against perl-5.18.1

* Fri Jul 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-2m)
- rebuild against perl-5.16.3

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-2m)
- rebuild against perl-5.14.2

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Tue Sep  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Tue Aug  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Fri Jul 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.05-2m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- update to 0.04

* Sun Jun 06 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
