%global momorel 14

Summary: high quality television application for use with video capture cards
Name: tvtime
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/tvtime/tvtime-%{version}.tar.gz 
NoSource: 0
Patch0: tvtime-0.9.13-pie.patch
Patch1: tvtime-1.0.1-gcc4.1.patch
Patch2: tvtime-1.0.2-pie.patch
Patch3: tvtime-1.0.2-newheader.patch
Patch4: tvtime-1.0.2-glibc210.patch
URL: http://tvtime.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libpng-devel, freetype-devel, zlib-devel
BuildRequires: SDL-devel, libxml2-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXext-devel
BuildRequires: libXinerama-devel, libXtst-devel, libXv-devel, libXxf86vm-devel
ExcludeArch: s390 s390x

%description
tvtime is a high quality television application for use with video
capture cards. tvtime processes the input from a capture card and
displays it on a computer monitor or projector.  Unlike other television
applications, tvtime focuses on high visual quality making it ideal for
videophiles.

%prep
%setup -q
%patch1 -p1 -b .gcc
%ifarch x86_64
%patch2 -p1 -b .PIC
%endif
%patch3 -p1
%patch4 -p1 -b .glibc210

for i in docs/man/de/*.?; do
        iconv -f iso-8859-1 -t utf-8 < "$i" > "${i}_"
        mv "${i}_" "$i"
done

%build
autoreconf -fi
%configure --disable-dependency-tracking --disable-rpath
%make

%install
rm -rf %{buildroot}

%makeinstall

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi
  
%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root, 0755)
%doc AUTHORS ChangeLog COPYING NEWS README* data/COPYING*
%doc docs/html
%doc %{_mandir}/man?/*
%config(noreplace) %{_sysconfdir}/tvtime
%{_bindir}/tvtime-command
%{_bindir}/tvtime-configure
%{_bindir}/tvtime-scanner
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/tvtime.png
%{_datadir}/pixmaps/*
%{_datadir}/tvtime
%{_mandir}/*/*/*
%defattr(775, root,root, 0755)
%{_bindir}/tvtime

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-9m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-7m)
- revise for rpm46 (s/Patch/Patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-5m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-4m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-3m)
- add tvtime-1.0.2-newheader.patch

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- delete duplicated dir

* Fri May 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.2-1m)
- update to  1.0.2
- add Patch2: tvtime-1.0.2-pie.patch (x86_64 ONLY)

* Sun Jan  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-1m)
- update 1.0.1
- build for gcc4

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.15-2m)
- enable x86_64.

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.15-1m)
- version up

* Fri Oct 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.13-2m)
- add patch0 for gcc 3.4 with -fPIC option.

* Mon Sep 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.13-1m)
- update to 0.9.13

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-2m)
- rebuild against DirectFB-0.9.21

* Sun Apr 25 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.12-1m)
- update to 0.9.12
- disable ssp because it causes a crash when input is composite

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.10-2m)
- revised spec for rpm 4.2.

* Mon Sep 29 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.9.10-1m)
- version up
- use %%NoSource macro

* Thu Sep  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.9-1m)
- major feature enhancements

* Mon Jun  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8.3-1m)
- major bugfixes

* Sat May 24 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8.2-1m)

* Sun Nov 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.6-1m)
- initial import
