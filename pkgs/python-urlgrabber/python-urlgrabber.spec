%global momorel 2

#%%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%global python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global realname urlgrabber
%global pyver 2.7
%global pythonver 2.7.1

Summary: A high-level cross-protocol url-grabber
Name: python-urlgrabber
Version: 3.10
Release: %{momorel}m%{?dist}
Source0: http://linux.duke.edu/projects/urlgrabber/download/%{realname}-%{version}.tar.gz 
#NoSource: 0
Patch1: BZ-853432-single-conn-reset.patch
Patch2: BZ-1017491-respond-to-ctrl-c.patch
License: LGPL
Group: Development/Libraries
BuildRequires: python-devel >= %{pythonver}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Url: http://linux.duke.edu/projects/urlgrabber/
Provides: urlgrabber
Requires: python-pycurl
BuildRequires: python-pycurl

%description
A high-level cross-protocol url-grabber for python supporting HTTP, FTP 
and file locations.  Features include keepalive, byte ranges, throttling,
authentication, proxies and more.

%prep
%setup -q -n %{realname}-%{version}
%patch1 -p1
%patch2 -p1

%build
PATH=/usr/bin:$PATH

python setup.py build

%install
python setup.py install -O1 --root=%{buildroot}
rm -rf $RPM_BUILD_ROOT/%{_docdir}/urlgrabber-%{version}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog LICENSE README TODO
%dir %{python_sitelib}/urlgrabber
%{python_sitelib}/urlgrabber/*
%{python_sitelib}/urlgrabber-*.egg-info
%{_bindir}/urlgrabber
%attr(0755,root,root) %{_libexecdir}/urlgrabber-ext-down

%changelog
* Mon Jun 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10-2m)
- use /usr/bin/python

* Mon Jun 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10-1m)
- update 3.10

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.1-8m)
- add BuildRequires: python-pycurl

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.1-7m)
- update urlgrabber-HEAD.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.1-4m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.1-3m)
- update urlgrabber-HEAD.patch

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.1-2m)
- add ug-fix-hdr.patch
- Require: python-pycurl

* Thu Mar 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.1-1m)
- update 3.9.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.0-2m)
- rebuild agaisst python-2.6.1-1m

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0
- comment out patch0,1

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.9-8m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.9.9-7m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.9-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.9-5m)
- %%NoSource -> NoSource

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.9.9-4m)
- rebuild against python-2.5-9m

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.9-3m)
- rebuild against python-2.5

* Mon Nov 06 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.9-2m)
- add Provides: urlgrabber
- add Patch0: urlgrabber-read-error.patch
- add Patch1: urlgrabber-ssl-byterange-keepalive.patch
- change no make/use files list

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.9.9-1m)
- update to 2.9.9

* Sat Apr 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.6-3m)
- add -q at setup
- use global

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.9.6-2m)
- build against python-2.4.2

* Wed Mar 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.9.6-1m)
- import from Fedora

* Tue Mar  8 2005 Jeremy Katz <katzj@redhat.com> - 2.9.6-1
- update to 2.9.6

* Mon Mar  7 2005 Jeremy Katz <katzj@redhat.com> - 2.9.5-1
- import into dist
- make the description less of a book

* Mon Mar  7 2005 Seth Vidal <skvidal@phy.duke.edu> 2.9.5-0
- 2.9.5

* Thu Feb 24 2005 Seth Vidal <skvidal@phy.duke.edu> 2.9.3-0
- first package for fc3
- named python-urlgrabber for naming guideline compliance

