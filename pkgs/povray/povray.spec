%global momorel 12
%global major 3.6
%global minor 1

Summary: POV-Ray -- Full-Featured Ray Tracer
Name: povray
Version: %{major}%{minor}
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: see "povlegal.doc"
URL: http://www.povray.org/
Source0: ftp://ftp.povray.org/pub/povray/Official/Unix/povray-%{major}.%{minor}.tar.bz2 
NoSource: 0
Patch1: povray-3.6.1-glass.patch
Patch2: povray-3.6.1-libpng.patch
BuildRequires: zlib-devel, libpng-devel, libjpeg-devel >= 7, libtiff-devel >= 4.0.1
BuildRequires: libXpm-devel
BuildRequires: autoconf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
POV-Ray  is a free, full-featured ray tracer, written and maintained by
a team of volunteers on the Internet.  On the Unix platform POV-Ray can
be  compiled  with  support for preview capabilities using the X Window
System.  Under Linux, POV-Ray can optionally use the  SVGA  library  to
preview renderings.

%prep
%setup -q -n %{name}-%{major}.%{minor}

# fix typo in include/glass.inc
%patch1 -p1 -b .glass~

# update configure script to detect installed libpng correctly
%patch2 -p1 -b .libpng~
autoconf

%build
%configure \
	--enable-watch-cursor \
	--with-x \
	 COMPILED_BY="Momonga Project"
%make

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install
install -m 0644 AUTHORS COPYING ChangeLog NEWS \
    %{buildroot}%{_docdir}/%{name}-%{major}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%config(noreplace) %{_sysconfdir}/povray/%{major}/povray.conf
%config(noreplace) %{_sysconfdir}/povray/%{major}/povray.ini
%{_bindir}/povray
%{_mandir}/*/*
%{_datadir}/%{name}-%{major}
%doc %{_docdir}/%{name}-%{major}/*

%changelog
* Wed Apr 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.61-12m)
- rebuild against libtiff-devel >= 4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.61-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.61-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.61-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.61-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.61-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.61-6m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.61-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.61-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.61-3m)
- %%NoSource -> NoSource

* Fri Dec 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.61-2m)
- fixed libpng detection
- imported glass patch from debian

* Sat Oct  7 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.61-1m)
- Update to povray-3.6.1

* Fri Dec 19 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.50c-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Sep 18 2003 Kenta MURATA <muraken2@nifty.com>
- (3.50c-1m)
- change license notation for Momonga Linux Package License Notation
  Specification.

* Wed Dec 04 2002 Kenta MURATA <muraken2@nifty.com>
- (3.50c-1m)
- first specfile.
