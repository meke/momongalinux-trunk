%global momorel 3

Summary: Run-time library to control radio transceivers and receivers
Name: hamlib
Version: 1.2.12
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: System Environment/Libraries
URL: http://hamlib.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gd-devel
BuildRequires: libusb-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: swig

%description
Hamlib provides a standardised programming interface that applications 
can use to send the appropriate commands to a radio.
 
Also included in the package is a simple radio control program 'rigctl',
which lets one control a radio transceiver or receiver, either from
command line interface or in a text-oriented interactive interface.

%package devel
Summary: Development library to control radio transcievers and receivers
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Hamlib radio control library C and C++ development headers and libraries
for building C and C++ applications with Hamlib.

%prep
%setup -q

%build
%configure \
	--with-rigmatrix \
	--without-perl-binding \
	--without-tcl-binding \
	--without-python-binding

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

## do not remove *.la files, ktrack needs them
# # get rid of *.la files
# rm -f %%{buildroot}%%{_libdir}/*lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog INSTALL
%doc LICENSE NEWS PLAN README README.betatester THANKS TODO
%{_bindir}/rigctl
%{_bindir}/rigctld
%{_bindir}/rigmem
%{_bindir}/rigsmtr
%{_bindir}/rigswr
%{_bindir}/rotctl
%{_bindir}/rotctld
%{_libdir}/%{name}-*.la
%{_libdir}/%{name}-*.so
%{_libdir}/lib%{name}*.la
%{_libdir}/lib%{name}*.so.*
%{_sbindir}/rpc.rigd
%{_sbindir}/rpc.rotd
%{_mandir}/man1/rigctl.1*
%{_mandir}/man1/rigmem.1*
%{_mandir}/man1/rigsmtr.1*
%{_mandir}/man1/rigswr.1*
%{_mandir}/man8/rigctld.8*
%{_mandir}/man1/rotctl.1*
%{_mandir}/man8/rotctld.8*
%{_mandir}/man8/rpc.rigd.8*
%{_mandir}/man8/rpc.rotd.8*

%files devel
%defattr(-,root,root)
%doc README.developer
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}-*.a
%{_libdir}/lib%{name}*.a
%{_libdir}/lib%{name}*.so
%{_datadir}/aclocal/%{name}.m4

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.12-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.12-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.12-1m)
- version 1.2.12

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-2m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.11-1m)
- version 1.2.11
- re-enable parallel build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-1m)
- version 1.2.10
- License: GPLv2 and LGPLv2+

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.9-1m)
- version 1.2.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.8-1m)
- version 1.2.8

* Fri Apr 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7.1-1m)
- version 1.2.7.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7-1m)
- version 1.2.7

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6.2-3m)
- %%NoSource -> NoSource

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6.2-2m)
- use make instead of %%make, make -j4 doesn't work

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6.2-1m)
- version 1.2.6.2

* Sun Jun 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6.1-1m)
- version 1.2.6.1

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6-1m)
- initial package for ktrack-0.3.0
