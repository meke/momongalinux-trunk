%global momorel 1

Name:           gnome-paint
Version:        0.4.0
Release:        %{momorel}m%{?dist}
Summary:        Easy to use paint program

Group:          Applications/Multimedia
License:        GPLv3+
URL:            https://launchpad.net/gnome-paint
Source0:        http://launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


BuildRequires:  intltool gtk2-devel desktop-file-utils
Requires:       hicolor-icon-theme

%description
gnome-paint is a simple, easy to use paint program.

%prep
%setup -q
# remove icon extensions
sed -i 's|Icon=gp.png|Icon=gp|g' data/desktop/%{name}.desktop.in.in
sed -i 's|RasterGraphics;|2DGraphics;RasterGraphics;|g' data/desktop/%{name}.desktop.in.in

%build
%configure "LIBS=-lm"
make %{?_smp_mflags}


%install 
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
# remove docs, use rpmbuild instead
rm -rf %{buildroot}/%{_prefix}/doc
# remove unnecessary includedir files
rm -rf %{buildroot}/%{_includedir}

desktop-file-install \
        --add-only-show-in=GNOME \
        --dir=%{buildroot}%{_datadir}/applications \
        %{buildroot}%{_datadir}/applications/%{name}.desktop
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop

%find_lang gnome_paint


%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
update-desktop-database &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database &> /dev/null || :

%files -f gnome_paint.lang
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_bindir}/gnome-paint
%{_datadir}/applications/gnome-paint.desktop
%{_datadir}/gnome-paint/
%{_datadir}/icons/hicolor/16x16/apps/gp.png

%changelog
* Wed Mar 14 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.4.0-1m)
- initial build
