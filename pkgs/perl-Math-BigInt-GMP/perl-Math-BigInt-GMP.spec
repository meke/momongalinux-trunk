%global         momorel 2

Name:           perl-Math-BigInt-GMP
Version:        1.38
Release:        %{momorel}m%{?dist}
Summary:        Math::BigInt::GMP Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Math-BigInt-GMP/
Source0:        http://www.cpan.org/authors/id/P/PJ/PJACKLAM/Math-BigInt-GMP-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Math-BigInt >= 1.993
BuildRequires:  perl-XSLoader >= 0.02
Requires:       perl-Math-BigInt >= 1.993
Requires:       perl-XSLoader >= 0.02
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
This package contains a replacement (drop-in) module for Math::BigInt's
core, Math::BigInt::Calc.pm. It needs the new versions of Math::BigInt and
Math::BigFloat as they are from Perl 5.7.x onwards.

%prep
%setup -q -n Math-BigInt-GMP-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS CHANGES CREDITS LICENSE README TODO
%{perl_vendorarch}/auto/Math/BigInt/GMP
%{perl_vendorarch}/Math/BigInt/GMP.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-2m)
- rebuild against perl-5.20.0

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-10m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-9m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-8m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-7m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-4m)
- rebuild against perl-5.16.0

* Sat Feb 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-3m)
- set do_test 0 due to tests fail on i686 and x86_64

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-2m)
- rebuild against perl-5.14.2

* Tue Sep  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-2m)
- rebuild against perl-5.14.1

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-15m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-13m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.24-12m)
- rebuild against gmp-5.0.1

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.24-4m)
- rebuild against gcc43

* Mon Mar 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-3m)
- rebuild against perl-5.10.0-5m

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.21-2m)
- use vendor

* Fri Apr 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Tue Apr 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Sat Feb 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-2m)
- modify %%files to avoid directory conflicting

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19
- add Require: perl-Math-BigInt

* Tue Nov 28 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
