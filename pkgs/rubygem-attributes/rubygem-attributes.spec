%global momorel 7

%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname attributes
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Name:		rubygem-%{gemname}
Summary: 	Attributes RubyGem
Version: 	5.0.1
Release: 	%{momorel}m%{?dist}
Group: 		Development/Languages
License:	GPLv2+ or Ruby
URL: 		http://codeforpeople.com/lib/ruby/%{gemname}/
Source0:	http://codeforpeople.com/lib/ruby/%{gemname}/%{gemname}-%{version}/%{gemname}-%{version}.gem
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	ruby >= 1.9.2
Requires:	rubygems
BuildRequires:	rubygems
BuildArch:	noarch
Provides:	rubygem(%{gemname}) = %{version}

%description
Attributes RubyGem

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force %{SOURCE0}

chmod +x %{buildroot}/%{geminstdir}/install.rb

# Remove zero-length file
rm -rf %{buildroot}/%{geminstdir}/%{gemname}-%{version}.gem

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc %{gemdir}/doc/%{gemname}-%{version}/
%doc %{gemdir}/gems/%{gemname}-%{version}/README
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0.1-7m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.1-4m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.1-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.1-1m)
- import from Fedora to Momonga

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.0.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jul 29 2008 Jeroen van Meeuwen <kanarip@kanarip.com> - 5.0.1-3
- Rebuild for review (#457030)

* Sun Jul 13 2008 root <root@oss1-repo.usersys.redhat.com> - 5.0.1-1
- Initial package
