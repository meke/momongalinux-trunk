%global momorel 1

Summary: poppler encoding data
Name: poppler-data
Version: 0.4.6
Release: %{momorel}m%{?dist}
License: "See COPYING"
Group: Development/Libraries
URL: http://poppler.freedesktop.org/
Source0: http://poppler.freedesktop.org/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package consists of encoding files for use with poppler.  The
encoding files are optional and poppler will automatically read them
if they are present.  When installed, the encoding files enables
poppler to correctly render CJK and Cyrrilic properly.  While poppler
is licensed under the GPL, these encoding files are copyright Adobe
and licensed much more strictly, and thus distributed separately.

%prep
%setup -q

%build

%install
rm -rf --preserve-root %{buildroot}
make install datadir=%{_datadir} DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README
%{_datadir}/poppler/*

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-1m)
- update 0.4.6

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-1m)
- update 0.4.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-2m)
- full rebuild for mo7 release

* Fri May 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-1m)
- update 0.4.2

* Wed Dec 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.0-1m)
- update 0.4.0
- remove Require: poppler

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-1m)
- update 0.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-2m)
- rebuild against rpm-4.6

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-2m)
- rebuild against gcc43

* Sun Jan  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Fri Oct 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Tue Oct 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1-1m)
- initial build
