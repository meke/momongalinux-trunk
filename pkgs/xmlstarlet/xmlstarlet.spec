%global momorel 4

Name: xmlstarlet
Version: 1.0.2
Release: %{momorel}m%{?dist}
Summary: Command Line XML Toolkit
Group: Applications/Text
License: MIT
URL: http://xmlstar.sourceforge.net/
Source0: http://dl.sourceforge.net/project/xmlstar/xmlstarlet/%{version}/xmlstarlet-%{version}.tar.gz
NoSource: 0
Patch0: xmlstarlet-1.0.1-nostatic.patch
Patch1: xmlstarlet-1.0.1-cmdname.patch
Patch2: xmlstarlet-1.0.1-docs.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xmlto automake autoconf libxml2-devel libxslt-devel

%description
XMLStarlet is a set of command line utilities which can be used
to transform, query, validate, and edit XML documents and files
using simple set of shell commands in similar way it is done for
plain text files using UNIX grep, sed, awk, diff, patch, join, etc
commands.

%prep
%setup -q
# cope with inconsistent naming
%patch0 -p1 -b .nostatic
%patch1 -p1 -b .cmdname
%patch2 -p1 -b .docs

%build
autoreconf -i
%configure --with-libxml-prefix=%{_prefix} --with-libxslt-prefix=%{_prefix}
make %{?_smp_mflags}
cd $RPM_BUILD_DIR/%{name}-%{version}/doc
xmlto man xmlstarlet-man.xml
xmlto html-nochunks xmlstarlet-ug.xml
./gen-doc > xmlstarlet.txt


%install
rm -fr %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -fr %{buildroot}


%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog NEWS README Copyright TODO doc/xmlstarlet.txt doc/xmlstarlet-ug.html
%{_mandir}/man1/xmlstarlet.1*
%{_bindir}/xmlstarlet


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- import from Fedora 13 and update to 1.0.2

* Sun Jan 10 2010 Paul W. Frields <stickster@gmail.com> - 1.0.1-9
- Correct source URL

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Mar 21 2008 Paul W. Frields <stickster@gmail.com> - 1.0.1-6
- Rebuild to use FORTIFY_SOURCE correctly

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.1-5
- Autorebuild for GCC 4.3

* Sat Sep  2 2006 Paul W. Frields <stickster@gmail.com> - 1.0.1-4
- Bump release for FC6 mass rebuild

* Fri Feb 17 2006 Paul W. Frields <stickster@gmail.com> - 1.0.1-3
- FESCo mandated rebuild

* Wed Nov 23 2005 Paul W. Frields <stickster@gmail.com> - 1.0.1-2
- Minor changes per review

* Tue Nov 22 2005 Paul W. Frields <stickster@gmail.com> - 1.0.1-1.2
- Improve patching to conquer inconsistent naming

* Tue Nov 22 2005 Paul W. Frields <stickster@gmail.com> - 1.0.1-1.1
- Initial RPM version


