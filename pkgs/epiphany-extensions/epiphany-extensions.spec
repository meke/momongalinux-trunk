%global momorel 1
%global majorminor 3.6

Summary: Extensions for Epiphany
Name:    epiphany-extensions
#Version: %{majorminor}.0
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
#Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/%{majorminor}/%{name}-%{version}.tar.xz
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

URL: http://www.gnome.org/projects/epiphany/extensions.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: epiphany-devel >= 3.6.0
BuildRequires: opensp-devel >= 1.5.2
BuildRequires: GConf2
BuildRequires: webkitgtk-devel >= 1.4.0
Requires(pre): hicolor-icon-theme gtk2

%description
Epiphany Extensions is a collection of extensions for Epiphany, the
GNOME web browser.

%prep
%setup -q

# workaround for epiphany-3.6
sed --in-place -e 's,EPIPHANY_API_VERSION=3.5,EPIPHANY_API_VERSION=3.6,g' configure.ac

sh ./autogen.sh

%build
%configure \
    --disable-static \
    --disable-silent-rules \
    --disable-scrollkeeper
%make

%install
%{__rm} -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}-%{majorminor}

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
rarian-sk-update

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}-%{majorminor}.lang
%defattr(-, root,root)
%doc AUTHORS COPYING* ChangeLog NEWS README
%{_datadir}/epiphany-extensions
%{_datadir}/omf/epiphany-extensions/epiphany-extensions-*.omf
%{_libdir}/epiphany/%{majorminor}/extensions/*
%{_datadir}/gnome/help/epiphany-extensions
%{_datadir}/epiphany/icons/hicolor/*/*/*
%{_datadir}/glib-2.0/schemas/org.gnome.epiphanyextensions.gschema.xml

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.0-2m)
- rebuild for epiphany 3.6.0

* Sun Sep 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0

* Sat Oct  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu May 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- rebuild against epiphany-2.30.2-2m

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sat Feb 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-1m)
- update to 2.29.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Mar  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-2m)
- detele Patch1 not used

* Wed Mar  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91
- delete patch0 (fixed)

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-2m)
- rebuild against epiphany-2.25.5
- add patch0

* Sat Jan 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Feb 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.3-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Mon Jul  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Sun Apr 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.18.0-2m)
- rebuild against opensp
- modified BuildPrereq, openjade-devel -> opensp-devel

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-2m)
- revice %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-1m)
- update to 2.17.4 (unstable)

* Sat Nov 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Tue Apr 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0.1-1m)
- update 2.14.0.1

* Sat Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2
- enable gcc-4.1
- - Patch0: epiphany-extensions-1.8.2-gcc41.patch

* Sun Nov 27 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1 (for epiphany 1.8.2)

* Sat Jun 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-2m)
- rebuild against openjade-1.3.2-3m

* Thu Mar 31 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-1m)
- update to 1.4.5

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4.4-1m)
- Initial specfile
>>>>>>> .merge-right.r12647
