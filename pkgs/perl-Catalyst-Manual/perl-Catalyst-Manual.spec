%global         momorel 5

Name:           perl-Catalyst-Manual
Version:        5.9007
Release:        %{momorel}m%{?dist}
Summary:        Catalyst::Manual Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Manual/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/Catalyst-Manual-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is just the Catalyst manual.  If you want to develop Catalyst apps,
please install Catalyst::Devel.  If you'd like a tutorial and a full
example Catalyst application, please install Task::Catalyst::Tutorial.

%prep
%setup -q -n Catalyst-Manual-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%{perl_vendorlib}/Catalyst/Manual.pm
%{perl_vendorlib}/Catalyst/Manual
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9007-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9007-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9007-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9007-2m)
- rebuild against perl-5.18.0

* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9007-1m)
- update to 5.9007

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9006-2m)
- rebuild against perl-5.16.3

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9006-1m)
- update to 5.9006

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9005-2m)
- rebuild against perl-5.16.2

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9005-1m)
- update to 5.9005

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9004-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9004-1m)
- update to 5.9004
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9003-1m)
- update to 5.9003

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9002-2m)
- rebuild against perl-5.14.2

* Sun Sep  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9002-1m)
- update to 5.9002

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9001-1m)
- update to 5.9001

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9000-1m)
- update to 5.9000

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8008-1m)
- update to 5.8008

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8007-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8007-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8007-1m)
- update to 5.8007

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8005-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8005-2m)
- rebuild for new GCC 4.5

* Sat Oct 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8005-1m)
- update to 5.8005

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8004-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
