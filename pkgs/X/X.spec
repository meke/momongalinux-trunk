%global momorel 1

Summary: Meta package for X
Name: X
Version: 7.7
Release: %{momorel}m%{?dist}
#Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/Desktops
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

################################################

### libraries
# in pkgs
# ls -d libX*
%global Xlibs01 libX11 libXcomposite libXext libXi libXrandr libXv
%global Xlibs02 libXScrnSaver libXcursor libXfixes libXinerama libXrender libXvMC
%global Xlibs03 libXTrap libXdamage libXfont libXmu libXres libXxf86dga
%global Xlibs04 libXau libXdmcp libXfontcache libXp libXt libXxf86misc
%global Xlibs05 libXaw libXevie libXft libXpm libXtst libXxf86vm
%global Xlibs06 libSM libICE libFS libfontenc libdmx libdrm libxkbfile
# mesa sub pkgs
%global Xlibs07 mesa-libGLw mesa-libGL mesa-libGLU 
%global Xlibs08 mesa-libGLw-devel mesa-libGL-devel mesa-libGLU-devel

### misc
# in pkgs
# ls -d xorg-x11*|grep -v drv
%global Xpkgs01 xorg-x11-apps xorg-x11-filesystem
%global Xpkgs02 xorg-x11-font-utils 
#xorg-x11-fonts
%global Xpkgs03 xorg-x11-resutils
#%%global Xpkgs04 xorg-x11-server xorg-x11-server-utils
%global Xpkgs04 xorg-x11-twm xorg-x11-util-macros xorg-x11-utils
%global Xpkgs05 xorg-x11-xauth xorg-x11-xbitmaps xorg-x11-xdm
%global Xpkgs06 xorg-x11-xfs xorg-x11-xfwp xorg-x11-xinit xorg-x11-xinit-base
%global Xpkgs07 xorg-x11-xkb-utils xorg-x11-xsm xkeyboard-config
%global Xpkgs08 xorg-x11-xtrans-devel
%global Xpkgs09 xorg-x11-server-Xorg xorg-x11-server-utils
%global Xpkgs10 xorg-x11-server-Xephyr
%global Xpkgs11 xorg-x11-server-Xnest xorg-x11-server-Xvfb xorg-x11-server-Xdmx

### fonts
# rpm -qa | grep xorg-x11-fonts
%global Xfonts01 xorg-x11-fonts-misc xorg-x11-fonts-ethiopic
%global Xfonts02 xorg-x11-fonts-base xorg-x11-fonts-100dpi
%global Xfonts03 xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi
%global Xfonts04 xorg-x11-fonts-ISO8859-1-100dpi xorg-x11-fonts-ISO8859-1-75dpi
%global Xfonts05 xorg-x11-fonts-ISO8859-2-100dpi xorg-x11-fonts-ISO8859-2-75dpi
%global Xfonts06 xorg-x11-fonts-ISO8859-9-100dpi xorg-x11-fonts-ISO8859-9-75dpi
%global Xfonts07 xorg-x11-fonts-ISO8859-14-100dpi xorg-x11-fonts-ISO8859-14-75dpi
%global Xfonts08 xorg-x11-fonts-ISO8859-15-100dpi xorg-x11-fonts-ISO8859-15-100dpi
%global Xfonts09 xorg-x11-fonts-cyrillic
%global Xfonts10 fonts-japanese intlfonts

### drivers
%global Xpkgdrv01 xorg-x11-drivers

### etc
%global Xpkgetc01 imake

### X11 6.8.2 packages
%global Xobso01 xorg-x11 xorg-x11-75dpi-fonts xorg-x11-100dpi-fonts
%global Xobso02 xorg-x11-ISO8859-14-100dpi-fonts xorg-x11-ISO8859-14-75dpi-fonts
%global Xobso03 xorg-x11-ISO8859-15-100dpi-fonts xorg-x11-ISO8859-15-75dpi-fonts
%global Xobso04 xorg-x11-ISO8859-2-100dpi-fonts xorg-x11-ISO8859-2-75dpi-fonts
%global Xobso05 xorg-x11-ISO8859-9-100dpi-fonts xorg-x11-ISO8859-9-75dpi-fonts
%global Xobso06 xorg-x11-Mesa-libGL xorg-x11-Mesa-libGLU
%global Xobso07 xorg-x11-Xdmx xorg-x11-Xnest xorg-x11-Xvfb
%global Xobso08 xorg-x11-base-fonts xorg-x11-cyrillic-fonts 
%global Xobso09 xorg-x11-deprecated-libs xorg-x11-deprecated-libs-devel
%global Xobso10 xorg-x11-devel xorg-x11-doc
# xorg-x11-font-utils is same package name 6.8 and 7.x
%global Xobso11 xorg-x11-sdk xorg-x11-syriac-fonts xorg-x11-libs
%global Xobso12 xorg-x11-tools xorg-x11-truetype-fonts
# xorg-x11-twm is same package name 6.8 and 7.x
%global Xobso13 xorg-x11-xorgcfg
# xorg-x11-xauth, xorg-x11-xdm and xorg-x11-xfs is same package name between 6.8 and 7.x
# Obso Xgl Packages
%global Xobso14 xorg-x11-server-Xgl xgl-hardware-list momonga-xgl-settings
# liblbxutil and xorg-x11-lbxproxy are obsolete in X.Org 7.2
# welcome lbxproxy in X.Org 7.6
#%%global Xobso15 liblbxutil liblbxutil-devel xorg-x11-lbxproxy

################################################

###
# BuildRequires list

BuildRequires: %{Xlibs01} %{Xlibs02} %{Xlibs03} %{Xlibs04} %{Xlibs05} %{Xlibs06} %{Xlibs07}
BuildRequires: %{Xlibs08}

BuildRequires: %{Xpkgs01} %{Xpkgs02} %{Xpkgs03} %{Xpkgs04} %{Xpkgs05} %{Xpkgs06} %{Xpkgs07}
BuildRequires: %{Xpkgs09} %{Xpkgs10} %{Xpkgs11}

BuildRequires: %{Xpkgdrv01}

BuildRequires: %{Xpkgetc01}

BuildRequires: %{Xfonts01} %{Xfonts02} %{Xfonts03} %{Xfonts04} %{Xfonts05}
BuildRequires: %{Xfonts06} %{Xfonts07} %{Xfonts08} %{Xfonts09} %{Xfonts10} 

%ifarch %{ix86}
BuildRequires: %{Xnotix86drv01} %{Xnotix86drv02} %{Xnotix86drv03}
BuildRequires: %{Xnotix86drv04} %{Xnotix86drv05}
%endif #%%{ix86}

###
# Requires list (== BuildRequires list)
Requires: %{Xlibs01} %{Xlibs02} %{Xlibs03} %{Xlibs04} %{Xlibs05} %{Xlibs06} %{Xlibs07}

Requires: %{Xpkgs01} %{Xpkgs02} %{Xpkgs03} %{Xpkgs04} %{Xpkgs05} %{Xpkgs06} %{Xpkgs07}
Requires: %{Xpkgs09} %{Xpkgs10} %{Xpkgs11}

Requires: %{Xpkgdrv01}

Requires: %{Xpkgetc01}

Requires: %{Xfonts01} %{Xfonts02} %{Xfonts03} %{Xfonts04} %{Xfonts05}
Requires: %{Xfonts06} %{Xfonts07} %{Xfonts08} %{Xfonts09} %{Xfonts10} 

%ifarch %{ix86}
Requires: %{Xnotix86drv01} %{Xnotix86drv02} %{Xnotix86drv03}
Requires: %{Xnotix86drv04} %{Xnotix86drv05}
%endif #%%{ix86}

###
# Obsoletes list
Obsoletes: %{Xobso01} %{Xobso02} %{Xobso03} %{Xobso04} %{Xobso05} %{Xobso06} %{Xobso07}
Obsoletes: %{Xobso08} %{Xobso09} %{Xobso10} %{Xobso11} %{Xobso12} %{Xobso13} %{Xobso14}

################################################

%description
This is meta package for X.

#This spec file is use for not Requires only but BuildRequires also.
#So, we use many many %%global 

%files

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-1m)
- update 7.7 (version only)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-4m)
- do not obsolete xorg-x11-lbxproxy liblbxutil

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.5-2m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-1m)
- update to 7.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.4-7m)
- liblbxutil liblbxutil-devel xorg-x11-lbxproxy are obsolete in X.Org 7.2

* Sun Aug  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.4-6m)
- delete devel depend again

* Sat Aug  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.4-5m)
- delete devel depend

* Wed Mar 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.4-4m)
- delete some drivers (xorg-server-1.6)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.4-3m)
- rebuild against rpm-4.6

* Sat Oct 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.4-2m)
- add Requires: xorg-x11-drv-synaptics

* Thu Sep 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-1m)
- update 7.4

* Sat Aug 30 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-0.4m)
- Obso Xgl packages

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4-0.3m)
- Xorg-7.4 preview
- remove mesa-source

* Sat May 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (7.4-0.2m)
- remove dependency xorg-x11-drv-magictouch xorg-x11-drv-vga

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4-0.1m)
- Xorg-7.4 preview
- rename xorg-x11-server-sdk to xorg-x11-server-devel

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org
- (7.3-3m)
- remove dependency xorg-x11-fonts-syriac xorg-x11-fonts-truetype

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3-2m)
- rebuild against gcc43

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-1m)
- update Xorg-7.3

* Sun Jul  8 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3-0.3m)
- use xkeyboard-config

* Sat Jun 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (7.3-0.2m)
- modify dependency

* Sun Jun 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-0.1m)
- update

* Fri May 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-1m)
- version up :-P

* Fri May 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.1-0.2.4m)
- replace xorg-x11-xinit-additional to xorg-x11-xinit-base

* Wed May 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-0.2.3m)
- separate ifarch ix86 packages to Xnotix86drv01 .. Xnotix86drv05

* Wed May  3 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-0.2.2m)
- add xorg-x11-xinit-additional

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-0.2.1m)
- obsolete xinitrc

* Sat Apr 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-0.2.0m)
- creat
- add japanese-fonts latin-fonts intlfonts
