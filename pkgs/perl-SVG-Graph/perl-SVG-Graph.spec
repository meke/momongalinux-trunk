%global         momorel 9

Name:           perl-SVG-Graph
Version:        0.04
Release:        %{momorel}m%{?dist}
Summary:        Visualize your data in Scalable Vector Graphics (SVG) format
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SVG-Graph/
Source0:        http://www.cpan.org/authors/id/C/CJ/CJFIELDS/SVG-Graph-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Math-Derivative
BuildRequires:  perl-Math-Spline
BuildRequires:  perl-Statistics-Descriptive >= 2.6
BuildRequires:  perl-SVG >= 2.27
BuildRequires:  perl-Tree-DAG_Node >= 1.04
Requires:       perl-Math-Derivative
Requires:       perl-Math-Spline
Requires:       perl-Statistics-Descriptive >= 2.6
Requires:       perl-SVG >= 2.27
Requires:       perl-Tree-DAG_Node >= 1.04
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
SVG::Graph is a suite of perl modules for plotting data. SVG::Graph
currently supports plots of one-, two- and three-dimensional data, as well
as N-ary rooted trees. Data may be represented as:

%prep
%setup -q -n SVG-Graph-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README LICENSE
%doc eg
%{perl_vendorlib}/SVG/Graph.pm
%{perl_vendorlib}/SVG/Graph
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-2m)
- rebuild against perl-5.16.0

* Sun Jan  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- update to 0.04

* Sat Jan  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- update to 0.03

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.02-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-6m)
- rebuild against perl-5.12.0

* Fri Apr  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-5m)
- %%{perl_vendorlib}/SVG was owned by perl-SVG

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.02-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-2m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.02-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.02-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jun 14 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.02-1
- New upstream (0.02)
- Licensing terms clarified: Artistic 2.0

* Fri Feb 08 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.01-7
- rebuild for new perl

* Sat Apr 07 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.01-6
- Fixes BR for perl(Tree::DAG_Node)

* Sat Apr 07 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.01-5
- Add BR: perl(SVG), perl(Tree::DAG_Node) and versions

* Sat Apr 07 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.01-4
- Add BR: perl(Statistics::Descriptive).
- Add "eg" subdirectory as documentation.

* Fri Apr 06 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.01-3
- Added e-mail confirmation for license in package.

* Fri Apr 06 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.01-2
- Updated license.
- Add BR: perl(ExtUtils::MakeMaker)

* Fri Mar 23 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.01-1
- Specfile autogenerated by cpanspec 1.69.1.
