%global momorel 21

Summary: Windows MetaFile Library
Name: libwmf
Version: 0.2.8.4
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPLv2+
Source: http://dl.sourceforge.net/sourceforge/wvware/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://wvware.sourceforge.net/libwmf.html
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0: libwmf-0.2.8.3-nodocs.patch
Patch1: libwmf-0.2.8.3-relocatablefonts.patch
Patch2: libwmf-0.2.8.4-fallbackfont.patch
Patch3: libwmf-0.2.8.4-deps.patch
Patch4: libwmf-0.2.8.4-multiarchdevel.patch
Patch5: libwmf-0.2.8.4-intoverflow.patch
Patch6: libwmf-0.2.8.4-reducesymbols.patch
Patch7: libwmf-0.2.8.4-useafterfree.patch
Requires: urw-fonts
Requires: %{name}-lite = %{version}-%{release}
Requires(post): gtk2 >= 2.20.1-3m
Requires(postun): gtk2 >= 2.20.1-3m
BuildRequires: gtk2-devel, libtool, libxml2-devel, gd-devel, libpng-devel
BuildRequires: libjpeg-devel, libXt-devel, libX11-devel, dos2unix, libtool
 
%description
A library for reading and converting Windows MetaFile vector graphics (WMF).

%package lite
Summary: Windows Metafile parser library
Group: System Environment/Libraries

%description lite
A library for parsing Windows MetaFile vector graphics (WMF).

%package devel
Summary: Support files necessary to compile applications with libwmf
Group: Development/Libraries
Requires: libwmf = %{version}-%{release}
Requires: gtk2-devel, libxml2-devel, gd-devel, libjpeg-devel, pkgconfig

%description devel
Libraries, headers, and support files necessary to compile applications 
using libwmf.

%prep
%setup -q
%patch0 -p1 -b .nodocs
%patch1 -p1 -b .relocatablefonts
%patch2 -p1 -b .fallbackfont
%patch3 -p1 -b .deps
%patch4 -p1 -b .multiarchdevel
%patch5 -p1 -b .intoverflow
%patch6 -p1 -b .reducesymbols.patch
%patch7 -p1 -b .useafterfree.patch
f=README ; iconv -f iso-8859-2 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f

%build
rm configure.ac
ln -s patches/acconfig.h acconfig.h
autoreconf -i -f
%configure --with-libxml2 --disable-static --disable-dependency-tracking
export tagname=CC
make LIBTOOL=/usr/bin/libtool %{?_smp_mflags}
dos2unix doc/caolan/*.html

%install
rm -rf %{buildroot}
export tagname=CC
make DESTDIR=%{buildroot} LIBTOOL=/usr/bin/libtool install
rm -f %{buildroot}/%{_libdir}/*.a
rm -f %{buildroot}/%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/gtk-2.0/*/loaders/*.a
rm -f %{buildroot}%{_libdir}/gtk-2.0/*/loaders/*.la
rm -rf %{buildroot}%{_includedir}/libwmf/gd
find doc -name "Makefile*" -exec rm {} \;
#we're carrying around duplicate fonts
rm -rf %{buildroot}%{_datadir}/libwmf/fonts/*afm
rm -rf %{buildroot}%{_datadir}/libwmf/fonts/*pfb
sed -i %{buildroot}%{_datadir}/libwmf/fonts/fontmap -e 's#libwmf/fonts#fonts/default/Type1#g'

%post
/sbin/ldconfig
%{_bindir}/update-gdk-pixbuf-loaders %{_host} &>/dev/null || :

%post lite -p /sbin/ldconfig
 
%postun 
/sbin/ldconfig
%{_bindir}/update-gdk-pixbuf-loaders %{_host} &>/dev/null || :

%postun lite -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/libwmf-*.so.*
%{_libdir}/gtk-2.0/*/loaders/*.so
%{_bindir}/wmf2svg
%{_bindir}/wmf2gd
%{_bindir}/wmf2eps
%{_bindir}/wmf2fig
%{_bindir}/wmf2x
%{_bindir}/libwmf-fontmap
%{_datadir}/libwmf/

%files lite
%defattr(-,root,root,-)
%{_libdir}/libwmflite-*.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/*.html
%doc doc/*.png
%doc doc/*.gif
%doc doc/html
%doc doc/caolan
%{_libdir}/*.so
%{_libdir}/pkgconfig/libwmf.pc
%{_includedir}/libwmf
%{_bindir}/libwmf-config

%clean
rm -r %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8.4-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8.4-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.8.4-19m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.8.4-18m)
- modify Requires: gtk2 >= 2.20.1-3m

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.8.4-17m)
- import patch
- separate lite

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.4-16m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8.4-15m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.4-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.8.4-13m)
- Srebuild against libjpeg-7

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.4-12m)
- [SECURITY] CVE-2009-1364
- import a security patch (Patch3) from Fedora 11 (0.2.8.4-20)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8.4-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.8.4-10m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.8.4-9m)
- %%NoSource -> NoSource

* Thu Jun 14 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.8.4-8m)
- [SECURITY] CVE-2007-0455 CVE-2007-2756
- add patches

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8.4-7m)
- BuildPrereq: freetype2-devel -> freetype-devel

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8.4-6m)
- delete libtool library

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8.4-5m)
- rebuild against gtk+-2.10.3

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.8.4-4m)
- rebuild against expat-2.0.0-1m

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.8.4-3m)
- [SECURITY] import libwmf-0.2.8.4-intoverflow.patch from FC5 (CVE-2006-3376)

* Sat Jul  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8.4-2m)
- add %%post script

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.8.4-1m)
- update to 0.2.8.4

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.2.8.3-3m)
- enable x86_64. use directory macros.

* Wed Dec 22  2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.8.3-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Wed Nov 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.8.3-1m)

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.8-3m)
- rebuild against gtk+-2.4.0

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.2.8-2m)
- revised spec for enabling rpm 4.2.

* Mon Mar 31 2003 smbd <smbd@momonga-linux.org>
- (0.2.8-1m)
- up to 0.2.8

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.2.1-8k)
- rebuild against libpng 1.2.2.

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.2.1-6k)
- modify BuildPrereq,Requires Tag

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (0.2.1-4k)
- rebuild against libpng 1.2.0.

* Fri Aug 24 2001 Kusunoki Masanori <nori@kondara.org>
- up to 0.1.21

* Tue Nov 27 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.1.20

* Sat Jun 24 2000 Hiroki Harata <h-h@cablenet.ne.jp>
- [libwmf-0.1.17-1k]
- initialization of spec file.
