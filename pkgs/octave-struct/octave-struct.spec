%global momorel	2
%global octpkg struct

Name:           octave-%{octpkg}
Version:        1.0.9
Release:        %{momorel}m%{?dist}
Summary:        Structure handling for Octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/struct/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
# avoid stripping binaries to get useful debuginfo packages
# fedora specific - not upstreamed
Patch0:         %{name}-nostrip.patch

BuildRequires:  octave-devel 

Requires:       octave
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
Additional Structure manipulations functions. Octave includes support for organizing
data in structures. This package contains additional data structure manipulation
functions not in the octave core.

%prep
%setup -q -n %{octpkg}-%{version}
%patch0 -p0 -b .nostrip

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%{octpkglibdir}

%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.9-2m)
- rebuild against octave-3.6.1-1m

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-1m)
- initial import from fedora
