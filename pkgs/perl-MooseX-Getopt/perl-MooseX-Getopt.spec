%global         momorel 2

Name:           perl-MooseX-Getopt
Version:        0.63
Release:        %{momorel}m%{?dist}
Summary:        Moose role for processing command line options
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Getopt/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Getopt-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Carp
BuildRequires:  perl-Config-Any
BuildRequires:  perl-CPAN-Meta-Check >= 0.007
BuildRequires:  perl-Getopt-Long >= 2.37
BuildRequires:  perl-Getopt-Long-Descriptive >= 0.081
BuildRequires:  perl-IO
BuildRequires:  perl-IPC-Open3
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build-Tiny >= 0.030
BuildRequires:  perl-Module-Runtime
BuildRequires:  perl-Moose
BuildRequires:  perl-MooseX-Role-Parameterized
BuildRequires:  perl-Path-Tiny >= 0.009
BuildRequires:  perl-Test-CheckDeps >= 0.006
BuildRequires:  perl-Test-Deep
BuildRequires:  perl-Test-Fatal >= 0.003
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Test-Trap
BuildRequires:  perl-Test-Warn >= 0.21
BuildRequires:  perl-Test-Warnings
BuildRequires:  perl-Try-Tiny
Requires:       perl-Carp
Requires:       perl-Getopt-Long >= 2.37
Requires:       perl-Getopt-Long-Descriptive >= 0.081
Requires:       perl-Moose
Requires:       perl-MooseX-Role-Parameterized
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is a role which provides an alternate constructor for creating objects
using parameters passed in from the command line.

%prep
%setup -q -n MooseX-Getopt-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/MooseX/Getopt*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-2m)
- rebuild against perl-5.20.0

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Wed Feb 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60
- rebuild against perl-5.18.2

* Sun Dec 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Mon Sep  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-2m)
- rebuild against perl-5.16.3

* Sat Feb 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Sun Feb 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sun Feb  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Mon Jan 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Fri Dec 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Mon Dec 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-2m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.37-2m)
- rebuild for new GCC 4.6

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Thu Feb 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Wed Feb  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.33-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Wed Jun 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Sun Jun  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Fri Aug 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-3m)
- remove duplicate directories

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-2m)
- modify BuildRequires

* Fri May 29 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.18-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
