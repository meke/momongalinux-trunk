%global         momorel 1

Name:           perl-Log-Message-Simple
Version:        0.10
Release:        %{momorel}m%{?dist}
Epoch:          20
Summary:        Simplified interface to Log::Message
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Log-Message-Simple/
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/Log-Message-Simple-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Carp
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-if
BuildRequires:  perl-Log-Message
BuildRequires:  perl-Test-Simple
Requires:       perl-Carp
Requires:       perl-if
Requires:       perl-Log-Message
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides standardized logging facilities using the
Log::Message module.

%prep
%setup -q -n Log-Message-Simple-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES META.json README
%{perl_vendorlib}/Log/Message/Simple.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:0.10-1m)
- perl-Log-Message-Simple was removed perl core libraries
