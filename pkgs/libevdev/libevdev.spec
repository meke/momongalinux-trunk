%global momorel 1
Name:           libevdev
Version:        1.2.2
Release:		%{momorel}m%{?dist}
Summary:        Kernel Evdev Device Wrapper Library

Group:          System Environment/Libraries
License:        MIT
URL:            http://www.freedesktop.org/wiki/Software/libevdev
Source0:        http://www.freedesktop.org/software/%{name}/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  automake libtool
BuildRequires:  python

%description
%{name} is a library to wrap kernel evdev devices and provide a proper API
to interact with those devices.

%package devel
Summary:        Kernel Evdev Device Wrapper Library Development Package
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Kernel Evdev Device Wrapper Library Development Package.

%package utils
Summary:        Kernel Evdev Device Wrapper Library Utilities Package
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description utils
Utilities to handle and/or debug evdev devices.

%prep
%setup -q -n %{name}-%{version}

%build
autoreconf --force -v --install || exit 1
%configure --disable-static --disable-silent-rules --disable-gcov
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

# We intentionally don't ship *.la files
rm -f %{buildroot}%{_libdir}/*.la

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc COPYING 
%{_libdir}/libevdev.so.*

%files devel
%dir %{_includedir}/libevdev-1.0/
%dir %{_includedir}/libevdev-1.0/libevdev
%{_includedir}/libevdev-1.0/libevdev/libevdev.h
%{_includedir}/libevdev-1.0/libevdev/libevdev-uinput.h
%{_libdir}/libevdev.so
%{_libdir}/pkgconfig/libevdev.pc
%{_mandir}/man3/libevdev.3*

%files utils
%{_bindir}/touchpad-edge-detector

%changelog
* Thu Jun  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sat Mar 22 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.99.2-2m)
- merge from T4R

* Thu Mar 20 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.99.2-1m)
- update to 1.0.99.2

* Wed Feb 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Thu Dec 26 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-1m)
- import from fedora
