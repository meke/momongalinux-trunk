%global         momorel 2

Name:           qpdfview
Version:        0.4.3
Release:        %{momorel}m%{?dist}
Summary:        A tabbed PDF viewer, using the Qt framework
Group:          Applications/Publishing
License:        GPLv2
URL:            https://launchpad.net/qpdfview
Source0:        https://launchpad.net/qpdfview/trunk/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel  >= 4.8.4
BuildRequires:  cups-devel
BuildRequires:  poppler-devel
Requires:       cups
Requires:       poppler

%description

%prep
%setup -q
%patch0 -p1 -b .desktop

%build
## correct plugin install path
sed -i -e 's|/usr/lib/%{name}|%{_libdir}/%{name}|g' %{name}.pri

%{_qt4_qmake} %{name}.pro
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES CONTRIBUTORS COPYING README TODO
%{_bindir}/%{name}
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.so
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*

%changelog
* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-2m)
- remove patch1

* Mon Jun  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-1m)
- initial build for Momonga Linux
