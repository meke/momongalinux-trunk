%global         momorel 1

Name:           libspnav
Version:        0.2.2
Release:        %{momorel}m%{?dist}
Summary:        Open source alternative to 3DConnextion drivers
Group:          Development/Libraries
License:        BSD
URL:            http://spacenav.sourceforge.net/
Source:         http://downloads.sourceforge.net/spacenav/%{name}-%{version}.tar.gz
NoSource:       0
BuildRequires:  libX11-devel

%description
The spacenav project provides a free, compatible alternative to the proprietary
3Dconnexion device driver and SDK, for their 3D input devices (called "space
navigator", "space pilot", "space traveller", etc).

This package provides the library needed for applications to connect to the
user land daemon.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development files for %{name}.

%prep
%setup -q

%build
# Set libdir properly
sed -i "s/libdir=lib/libdir=%{_lib}/g" configure
%configure --prefix=%{buildroot}%{_prefix}
sed -i "s/CFLAGS =/CFLAGS +=/g" Makefile
make %{?_smp_mflags}

%install
# Makefile does not support DESTDIR or create directories
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_includedir}

make install

# Remove static library
rm -f %{buildroot}%{_libdir}/%{name}.a

# Extract library name from makefile
LIBNAME=$(make -f Makefile -f - <<<'lib_so:; @echo $(lib_so)' lib_so)
# Make sure LIBNAME is set and not null
: ${LIBNAME:?"LIBNAME variable could not be set."}

# Create symbolic link for -devel subpackage.
ldconfig -n %{buildroot}%{_libdir}
pushd %{buildroot}%{_libdir}
ln -s $LIBNAME %{name}.so
popd

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc README
%{_libdir}/*.so.0*

%files devel
%doc examples
%{_libdir}/*.so
%{_includedir}/*.h

%changelog
* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- import from Fedora

* Thu Jan 05 2012 Richard Shaw <hobbes1069@gmail.com> - 0.2.2-3
- Rebuild for GCC 4.7.0.

* Wed Aug 17 2011 Richard Shaw <hobbes1069@gmail.com> - 0.2.2-2
- Patched make file to honor Fedora CFLAGS defaults.
- Removed static library package.
- Other minor updates to the spec file.

* Mon Aug 15 2011 Richard Shaw <hobbes1069@gmail.com> - 0.2.2-1
- Initial release.
