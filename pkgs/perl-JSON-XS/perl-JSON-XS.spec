%global         momorel 3
%global         srcver 3.01

Name:           perl-JSON-XS
Version:        %{srcver}
Epoch:          1
Release:        %{momorel}m%{?dist}
Summary:        JSON serialising/deserialising, done correctly and fast
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/JSON-XS/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/JSON-XS-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-common-sense
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Types-Serialiser
Requires:       perl-common-sense
Requires:       perl-Types-Serialiser
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module converts Perl data structures to JSON and vice versa. Its
primary goal is to be correct and its secondary goal is to be fast. To
reach the latter goal it was written in C.

%prep
%setup -q -n JSON-XS-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README
%{_bindir}/*
%{perl_vendorarch}/auto/JSON/XS/*
%{perl_vendorarch}/JSON/XS*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.01-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.01-2m)
- rebuild against perl-5.18.2

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.01-1m)
- update to 3.01

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.34-2m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.34-1m)
- update to 2.34
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.33-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.33-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.33-2m)
- rebuild against perl-5.16.1

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.33-1m)
- update to 2.33

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.32-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.32-2m)
- rebuild against perl-5.14.2

* Fri Aug 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.32-1m)
- update to 2.32

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.31-1m)
- update to 2.31

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.30-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.30-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.30-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.30-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.30-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.30-1m)
- update to 2.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:2.29-5m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.29-4m)
- add epoch to %%changelog

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.29-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.29-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.29-1m)
- update to 2.29

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.28-1m)
- update to 2.28

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.27-1m)
- update to 2.27

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.26-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.26-1m)
- update to 2.26

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.25-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.25-1m)
- update to 2.25

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:2.24-2m)
- add Epoch: 1 to enable upgrading from STABLE_5
- DO NOT REMOVE Epoch FOREVER

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-1m)
- update to 2.24

* Mon Feb 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23.2-1m)
- update to 2.232

* Fri Feb 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23.11-1m)
- updateto 2.2311

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.23.1-2m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23.1-1m)
- update to 2.231

* Tue Sep 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23-1m)
- update to 2.23

* Mon Jul 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2222-1m)
- update to 2.2222

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.222-1m)
- update to 2.222

* Wed Jul 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-1m)
- update to 2.22

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-1m)
- update to 2.21

* Fri Apr 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-2m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-1m)
- update to 2.1

* Thu Dec  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Tue Dec  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Mon Oct 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
