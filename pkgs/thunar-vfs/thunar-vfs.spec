%global momorel 6

%global xfcever 4.10
%global xfce4ver 4.10.0
%global exover 0.9.0
%global thunarver 1.5.0 

Name:		 thunar-vfs
Version:	 1.2.0
Release:	 %{momorel}m%{?dist}
Summary:	 Thunar File Manager VFS part

Group:		 User Interface/Desktops
License:	 GPL
URL:		 http://thunar.xfce.org/
Source0:         http://archive.xfce.org/src/archive/%{name}/1.2/%{name}-%{version}.tar.bz2
NoSource:	 0
BuildRoot:	 %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:   exo-devel >= %{exover}
BuildRequires:   xfce4-panel-devel >= %{xfce4ver}
BuildRequires:   Thunar-devel >= %{thunarver}
Requires:        Thunar >= %{thunarver}

%description
Thunar is a modern file manager for the Unix/Linux desktop, aiming to be
easy-to-use and fast.

%package devel
Summary:         Development tools for Thunar VFS
Group:           Development/Libraries
Requires:        %{name} = %{version}-%{release}
Requires:        pkgconfig

%description devel
libraries and header files for the Thunar VFS.

%prep
%setup -q

%build
%configure --with-volume-manager=none --enable-dbus --enable-xsltproc --enable-gtk-doc LIBS="-lm"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir} transform='s,x,x,'

# make -C examples distclean
#
## 2 of the example files need to not be executable 
## so they don't pull in dependencies. 
#chmod 644 examples/thunar-file-manager.py
#chmod 644 examples/xfce-file-manager.py

rm -f %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL NEWS README TODO
%{_libdir}/*.so.*
%dir %{_libdir}/thunar-vfs-1
%{_libdir}/thunar-vfs-1/*
%doc %{_datadir}/doc/thunar-vfs/README.volumes
%doc %{_datadir}/doc/thunar-vfs/ThumbnailersCacheFormat.txt
%dir %{_datadir}/thumbnailers
%{_datadir}/thumbnailers/thunar-vfs-font-thumbnailer-1.desktop
%dir %{_datadir}/gtk-doc/html/thunar-vfs
%{_datadir}/gtk-doc/html/thunar-vfs/*
%{_datadir}/locale/*/*/*

%files devel
%dir %{_includedir}/thunar-vfs-1
%{_includedir}/thunar-vfs-1/*
%{_libdir}/libthunar-vfs-1.so
%{_libdir}/pkgconfig/thunar-vfs-1.pc

%changelog
* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-6m)
- BR: Thunar-devel >= 1.5.0
- BR: exo-devel >= 0.9.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-5m)
- rebuild against Thunar 1.5.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-4m)
- rebuild against xfce4-4.10.0

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-3m)
- change Source0 URI

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-2m)
- --with-volume-manager=none
-- HAL removed

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- update
- rebuild against xfce4-4.8

* Wed Dec 22 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-1m)
- initial made for Momonga Linux
