%global         momorel 9

Name:           perl-File-Remove
Version:        1.52
Release:        %{momorel}m%{?dist}
Summary:        Remove files and directories
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-Remove/
Source0:        http://www.cpan.org/authors/id/A/AD/ADAMK/File-Remove-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.00503
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-PathTools >= 3.28
BuildRequires:  perl-Test-Simple >= 0.42
Requires:       perl-PathTools >= 3.28
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
File::Remove::remove removes files and directories. It acts like /bin/rm,
for the most part. Although unlink can be given a list of files, it will
not remove directories; this module remedies that. It also accepts
wildcards, * and ?, as arguments for filenames.

%prep
%setup -q -n File-Remove-%{version}

find . -type f -exec sed -i 's/3.2701/3.28/' {} 2>/dev/null ';'

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/File/Remove.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-2m)
- rebuild against perl-5.16.0

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-2m)
- rebuild against perl-5.14.2

* Mon Jul 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.50

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.49-2m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-1m)
- update to 1.49

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48

* Sat Feb 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.42-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.42-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-2m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.40-2m)
- rebuild against gcc43

* Sun Feb 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Mon Oct 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38
- do not use %%NoSource macro

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.34-2m)
- use vendor

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.31-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
