%global         momorel 2
%global         srcver 6.06

Name:           perl-libwww-perl
Version:        %{srcver}0
Release:        %{momorel}m%{?dist}
Summary:        libwww::perl Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/libwww-perl/
Source0:        http://www.cpan.org/authors/id/M/MS/MSCHILLI/libwww-perl-%{srcver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008008
BuildRequires:  perl-Data-Dump
BuildRequires:  perl-Digest-MD5
BuildRequires:  perl-Encode >= 2.12
BuildRequires:  perl-Encode-Locale
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Listing >= 6
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-HTTP-Cookies >= 6
BuildRequires:  perl-HTTP-Date >= 6
BuildRequires:  perl-HTTP-Message >= 6
BuildRequires:  perl-HTTP-Negotiate >= 6
BuildRequires:  perl-IO
BuildRequires:  perl-IO-Socket-SSL >= 1.38
BuildRequires:  perl-libnet >= 1.22
BuildRequires:  perl-LWP-MediaTypes >= 6
BuildRequires:  perl-MIME-Base64 >= 2.1
BuildRequires:  perl-Mozilla-CA >= 20110101
BuildRequires:  perl-Net-HTTP >= 6
BuildRequires:  perl-NTLM >= 1.05
BuildRequires:  perl-URI >= 1.10
BuildRequires:  perl-WWW-RobotRules >= 6
Requires:       perl-Data-Dump
Requires:       perl-Digest-MD5
Requires:       perl-Encode >= 2.12
Requires:       perl-Encode-Locale
Requires:       perl-File-Listing >= 6
Requires:       perl-HTML-Parser
Requires:       perl-HTTP-Cookies >= 6
Requires:       perl-HTTP-Date >= 6
Requires:       perl-HTTP-Message >= 6
Requires:       perl-HTTP-Negotiate >= 6
Requires:       perl-IO
Requires:       perl-IO-Socket-SSL >= 1.38
Requires:       perl-libnet >= 1.22
Requires:       perl-LWP-MediaTypes >= 6
Requires:       perl-MIME-Base64 >= 2.1
Requires:       perl-Mozilla-CA >= 20110101
Requires:       perl-Net-HTTP >= 6
Requires:       perl-NTLM >= 1.05
Requires:       perl-URI >= 1.10
Requires:       perl-WWW-RobotRules >= 6
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The libwww-perl collection is a set of Perl modules which provides a
simple and consistent application programming interface to the World-Wide
Web.  The main focus of the library is to provide classes and functions
that allow you to write WWW clients. The library also contain modules that
are of more general use and even classes that help you implement simple
HTTP servers.

%prep
%setup -q -n libwww-perl-%{srcver}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;
rm -f %{buildroot}%{perl_vendorlib}/HTTP/Cookies/Microsoft.pm
rm -f %{buildroot}%{perl_vendorlib}/lwpcook.pod
rm -f %{buildroot}%{perl_vendorlib}/lwptut.pod


chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS Changes README README.SSL talk-to-ourself
%{_bindir}/*
%{perl_vendorlib}/LWP.pm
%{perl_vendorlib}/LWP/Authen
%{perl_vendorlib}/LWP/ConnCache.pm
%{perl_vendorlib}/LWP/Debug.pm
%{perl_vendorlib}/LWP/DebugFile.pm
%{perl_vendorlib}/LWP/MemberMixin.pm
%{perl_vendorlib}/LWP/Protocol.pm
%{perl_vendorlib}/LWP/Protocol
%{perl_vendorlib}/LWP/RobotUA.pm
%{perl_vendorlib}/LWP/Simple.pm
%{perl_vendorlib}/LWP/UserAgent.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.060-2m)
- rebuild against perl-5.20.0

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.060-1m)
- update to 6.06

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.050-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.050-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.050-2m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.050-1m)
- update to 6.05
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.040-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.040-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.040-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.040-1m)
- update to 6.04

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.030-1m)
- update to 6.03

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.020-2m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-1m)
- update to 6.02

* Thu Mar 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.010-1m)
- update to 6.01

* Wed Mar  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.000-1m)
- update to 6.00

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.837-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.837-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.837-1m)
- update to 5.837

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.836-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.836-2m)
- rebuild against perl-5.12.1

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.836-1m)
- update to 5.836

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.835-1m)
- [SECURITY] CVE-2010-2253
- update to 5.835

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.834-4m)
- add BuildRequires:  perl-HTML-TokeParser-Simple
- add Requires:       perl-NTLM

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.834-3m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.384-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.834-1m)
- update to 5.834

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.833-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.833-1m)
- update to 5.833

* Tue Sep 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.832-1m)
- update to 5.832

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.831-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.831-1m)
- update to 5.831

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.830-1m)
- update to 5.830

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.826-2m)
- version down to 5.826

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.828-1m)
- update to 5.828

* Tue Jun 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.827-1m)
- update to 5.827

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.826-1m)
- update to 5.826

* Wed Feb 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.825-1m)
- update to 5.825

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.824-1m)
- update to 5.824

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.823-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.823-1m)
- update to 5.823

* Sun Dec  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.822-1m)
- update to 5.822

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.821-1m)
- update to 5.821

* Sat Nov  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.820-1m)
- update to 5.820

* Tue Oct 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.819-1m)
- update to 5.819

* Thu Oct 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.818-1m)
- update to 5.818

* Tue Sep 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.816-1m)
- update to 5.816

* Sat Sep 27 2008 NARITA Koichi <pulsar@momonga-inuxx.org>
- (5.815-1m)
- update to 5.815

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.814-1m)
- update to 5.814

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.813-1m)
- update to 5.813

* Thu Apr 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.812-1m)
- update to 5.812

* Tue Apr 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.811-1m)
- update to 5.811

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.810-1m)
- update to 5.810

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.808-3m)
- rebuild against gcc43

* Sat Jan  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.808-2m)
- Specfile re-generated by cpanspec 1.74 for Momonga Linux.

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.808-1m)
- update to 5.808

* Wed Aug  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.807-1m)
- update to 5.807

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.806-1m)
- update to 5.806

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.805-4m)
- use vendor

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.805-3m)
- merge fc perl-libwww-perl-5.805-1.1.1

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.805-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (5.805-1m)
- update to 5.805

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.803-2m)
- rebuilt against perl-5.8.7

* Sat Jun 04 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.803-1m)
- update to 5.803

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.79-3m)
- rebuild against perl-5.8.5
- rebuild against perl-HTML-Parser 3.36-3m
- rebuild against perl-URI 1.30-4m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.79-2m)
- remove Epoch from BuildRequires

* Tue Apr 20 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.79-1m)
- update to 5.79
- add AUTHORS to %%doc

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (5.76-3m)
- use perl specific dependency check filter.

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (5.76-2m)
- revised spec for rpm 4.2.

* Wed Dec  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.76-1m)
- update to 5.76
- add BuildRequires: perl-MIME-Base64, perl-Digest-MD5, perl-libnet

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.75-3m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.75-2m)
- rebuild against perl-5.8.1

* Fri Oct 31 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.75-1m)
- update to 5.75
- use %%NoSource

* Fri Oct 24 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.73-1m)
- update to 5.73
- use %%{momorel}
- fix %%files
  %%{perl_vendorlib}/*/ have the possibility of another module files.

* Sun Jan 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.69-1m)

* Sun Jan  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.68-1m)

* Sun Dec 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.66-1m)

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.65-4m)
- rebuild against perl-5.8.0

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.65-3m)
- fix %files

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.65-2m)
- fix man install path

* Sat Jul  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.65-1m)
- rebuild against perl-HTML-Parser-3.26, perl-URI-1.19, perl-libnet-1.12,
  and perl-Digest-MD5-2.20

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (5.64-2k)
- version 5.64

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (5.53-2k)
- merge from rawhide. based on 5.53-3.

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 5.53-3
- imported from mandrake. tweaked man path.

* Sat Jul 07 2001 Stefan van der Eijk <stefan@eijk.nu> 5.53-2mdk
- BuildRequires:	perl-devel

* Tue Jul 03 2001 Francois Pons <fpons@mandrakesoft.com> 5.53-1mdk
- 5.53.

* Sun Jun 17 2001 Geoffrey Lee <snailtalk@mandrakesoft.com> 5.50-2mdk
- Rebuild for the latest perl.

* Tue Jan 30 2001 Francois Pons <fpons@mandrakesoft.com> 5.50-1mdk
- added requires on perl-URI >= 1.10.
- 5.50.

* Tue Nov 14 2000 Francois Pons <fpons@mandrakesoft.com> 5.48-3mdk
- fixed typo in summary.

* Thu Aug 03 2000 Francois Pons <fpons@mandrakesoft.com> 5.48-2mdk
- macroszifications.
- noarch.
- add doc.

* Tue Jun 27 2000 Jean-Michel Dault <jmdault@mandrakesoft.com> 5.48-1mdk
- updated to 5.48

* Thu Apr 20 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 5.47-3mdk
- fixed release tag

* Fri Mar 31 2000 Pixel <pixel@mandrakesoft.com> 5.47-2mdk
- remove file list
- rebuild for 5.6.0

* Mon Jan  3 2000 Jean-Michel Dault <jmdault@netrevolution.com>
- final cleanup for Mandrake 7

* Thu Dec 30 1999 Jean-Michel Dault <jmdault@netrevolution.com>
- updated to 5.47

* Sun Aug 29 1999 Jean-Michel Dault <jmdault@netrevolution.com>
- bzip2'd sources
- updated to 5.44

* Tue May 11 1999 root <root@alien.devel.redhat.com>
- Spec file was autogenerated. 
