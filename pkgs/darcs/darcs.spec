%global momorel 9

Summary:        David's advanced revision control system
Name:           darcs
Version:	2.2.1
#Release:	%%{momorel}m%{?dist}
Release:	%{momorel}m%{?dist}

Group:		Development/Tools
License:	GPLv2+
URL: 		http://www.darcs.net/
Source0: 	http://www.darcs.net/%{name}-%{version}.tar.gz 
#NoSource:	0
Patch0:		%{name}-%{version}-update-external-sh.patch
Patch1:		fix-issue458_sh-failing-on-systems-with-xattrs.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch:	%{ix86} x86_64 ppc

BuildRequires: autoconf
BuildRequires: ghc >= 6.10.4-4m
BuildRequires: %{_sbindir}/sendmail
BuildRequires: curl-devel >= 7.16.0, ncurses-devel, zlib-devel
BuildRequires: tetex-latex
# for make check
BuildRequires:  which
# for building the manual
BuildRequires: latex2html
Requires(post): policycoreutils
Requires: bash-completion
# for use
# %%{_sysconfdir}/bash_completion.d

%description
Darcs is a revision control system, along the lines of CVS
or arch. That means that it keeps track of various revisions
and branches of your project, allows for changes to
propagate from one branch to another. Darcs is intended to
be an ``advanced'' revision control system. Darcs has two
particularly distinctive features which differ from other
revision control systems: 1) each copy of the source is a
fully functional branch, and 2) underlying darcs is a
consistent and powerful theory of patches.

%package doc
Summary: David's advanced revision control system Server
Group: Documentation
Requires: httpd
# for use 
# %%{_localstatedir}/www/cgi-bin

%description doc
Darcs is a revision control system, along the lines of CVS
or arch. That means that it keeps track of various revisions
and branches of your project, allows for changes to
propagate from one branch to another. Darcs is intended to
be an ``advanced'' revision control system. Darcs has two
particularly distinctive features which differ from other
revision control systems: 1) each copy of the source is a
fully functional branch, and 2) underlying darcs is a
consistent and powerful theory of patches.

This package contains the darcs manual.


%package server
Summary: David's advanced revision control system Server
Group: Development/Tools
Requires: webserver

%description server
Darcs is a revision control system, along the lines of CVS
or arch. That means that it keeps track of various revisions
and branches of your project, allows for changes to
propagate from one branch to another. Darcs is intended to
be an ``advanced'' revision control system. Darcs has two
particularly distinctive features which differ from other
revision control systems: 1) each copy of the source is a
fully functional branch, and 2) underlying darcs is a
consistent and powerful theory of patches.

This package contains the darcs cgi server program.


%prep
%setup -q

%patch0 -p1 -b .fix-test

# Build fix for issue458.sh test
%patch1 -p1 -b .fix-issue458

%build
#%%{__autoconf}
%configure --libexecdir=%{_localstatedir}/www
%__make all


%check
%__make test


%install
[ %{buildroot} != "/" ] && %__rm -rf %{buildroot}
# convert text files to utf-8
for textfile in tools/zsh_completion_new AUTHORS
do
    mv -v $textfile $textfile.old
    iconv --from-code ISO8859-1 --to-code UTF-8 --output $textfile $textfile.old
    rm -fv $textfile.old
done 
%__make DESTDIR=%{buildroot} install installserver install-html


%clean
[ %{buildroot} != "/" ] && %__rm -rf %{buildroot}


%post
semanage fcontext -a -t unconfined_execmem_exec_t %{_bindir}/%{name} >/dev/null 2>&1
restorecon %{_bindir}/%{name}


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING tools/zsh_completion_*
#%%dir %{_sysconfdir}/bash_completion.d
%config(noreplace) %{_sysconfdir}/bash_completion.d/darcs
%{_bindir}/darcs
%{_mandir}/man1/*


%files doc
%defattr(-,root,root,-)
%{_docdir}/darcs


%files server
%defattr(-,root,root,-)
%doc COPYING AUTHORS
#%%{_localstatedir}/www/cgi-bin
%{_localstatedir}/www/cgi-bin/darcs.cgi
%config(noreplace) %{_sysconfdir}/darcs
%{_datadir}/darcs


%changelog
* Wed Jan 25 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.1-9m)
- add Patch1:fix-issue458_sh-failing-on-systems-with-xattrs.patch

* Thu Apr 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-8m)
- fix test failure to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-6m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-5m)
- rebuild against ghc-6.10.4-4m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-1m)
- update
- rebuild against ghc-6.10.1
- rebuild against rpm-4.7
- fix %%files section

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-0.2.6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-0.2.5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-0.2.4m)
- %%NoSource -> NoSource

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-0.2.3m)
- modify md5sum of Source0

* Tue Jan  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-0.2.2m)
- modify md5sum

* Fri Nov 17 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-0.2.1m)
- update to 1.0.9rc2
- rebuild against curl-7.16.0

* Fri Oct 13 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-0.1.1m)
- import from Fedora Extras devel
- update to 1.0.9rc1

* Thu Sep 28 2006 Jens Petersen <petersen@redhat.com> - 1.0.8-3
- rebuild for FC6
- enable make check

* Fri Jun 23 2006 Jens Petersen <petersen@redhat.com> - 1.0.8-2
- set unconfined_execmem_exec_t context to allow running under selinux targeted
  policy (#195820)

* Wed Jun 21 2006 Jens Petersen <petersen@redhat.com> - 1.0.8-1
- update to 1.0.8

* Sun May 14 2006 Jens Petersen <petersen@redhat.com> - 1.0.7-1
- update to 1.0.7
- fix typo of propagate in description (#189651)
- disable "make check" for now since it blows up in buildsystem

* Thu Mar  2 2006 Jens Petersen <petersen@redhat.com> - 1.0.6-1
- update to 1.0.6
  - darcs-createrepo is gone

* Thu Dec  8 2005 Jens Petersen <petersen@redhat.com> - 1.0.5-1
- 1.0.5 bugfix release

* Mon Nov 14 2005 Jens Petersen <petersen@redhat.com> - 1.0.4-1
- 1.0.4 release
  - skip tests/send.sh for now since it is failing in buildsystem

* Tue Jul  5 2005 Jens Petersen <petersen@redhat.com>
- drop superfluous doc buildrequires (Karanbir Singh, #162436)

* Fri Jul  1 2005 Jens Petersen <petersen@redhat.com> - 1.0.3-2
- fix buildrequires
  - add sendmail, curl-devel, ncurses-devel, zlib-devel, and
    tetex-latex, tetex-dvips, latex2html for doc generation

* Tue May 31 2005 Jens Petersen <petersen@redhat.com> - 1.0.3-1
- initial import into Fedora Extras
- 1.0.3 release
- include bash completion file in doc dir

* Sun May  8 2005 Jens Petersen <petersen@haskell.org> - 1.0.3-0.rc1.1
- 1.0.3rc1
  - build with ghc-6.4

* Wed Feb  8 2005 Jens Petersen <petersen@haskell.org> - 1.0.2-1
- update to 1.0.2

* Thu Jul 15 2004 Jens Petersen <petersen@haskell.org> - 0.9.22-1
- 0.9.22
- darcs-0.9.21-css-symlinks.patch no longer needed

* Thu Jun 24 2004 Jens Petersen <petersen@haskell.org> - 0.9.21-1
- update to 0.9.21
- replace darcs-0.9.13-mk-include.patch with darcs-0.9.21-css-symlinks.patch

* Wed Nov  5 2003 Jens Petersen <petersen@haskell.org>
- Initial packaging.
