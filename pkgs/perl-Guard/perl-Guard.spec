%global         momorel 10

Name:           perl-Guard
Version:        1.022
Release:        %{momorel}m%{?dist}
Summary:        Safe cleanup blocks
License:        "Distributable, see COPYING"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Guard/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/Guard-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module implements so-called "guards". A guard is something
(usually an object) that "guards" a resource, ensuring that it is
cleaned up when expected.

%prep
%setup -q -n Guard-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README
%{perl_vendorarch}/auto/Guard
%{perl_vendorarch}/Guard.*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-2m)
- rebuild against perl-5.14.2

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.022-1m)
- update to 1.022

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.021-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.021-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.021-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.021-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.021-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.021-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.021-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.021-4m)
- rebuild against perl-5.12.0

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.021-1m)
- update to 1.021

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-2m)
- rebuild against perl-5.10.1

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
