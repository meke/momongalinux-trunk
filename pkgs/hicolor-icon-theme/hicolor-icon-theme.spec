%global momorel 4

%define shortname	icon-theme
Summary: Basic requirement for icon themes
Name: hicolor-icon-theme
Version: 0.12
Release: %{momorel}m%{?dist}
URL: http://icon-theme.freedesktop.org/wiki/FrontPage
## http://icon-theme.freedesktop.org/wiki/HicolorTheme
Source0: http://icon-theme.freedesktop.org/releases/%{name}-%{version}.tar.gz 
NoSource: 0
NoSource: 0
License: GPL
Group: User Interface/Desktops
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: gtk2
Requires(post): coreutils gtk2

%description
Contains the basic directories and files needed for icon theme support.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
rm -f %{_datadir}/icons/hicolor/.icon-theme.cache
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache -q -t -f %{_datadir}/icons/hicolor || :
fi

%files
%defattr(-,root,root)
%doc README ChangeLog COPYING
%{_datadir}/icons/hicolor/index.theme
%dir %{_datadir}/icons/hicolor
%dir %{_datadir}/icons/hicolor/16x16
%dir %{_datadir}/icons/hicolor/16x16/*
%dir %{_datadir}/icons/hicolor/22x22
%dir %{_datadir}/icons/hicolor/22x22/*
%dir %{_datadir}/icons/hicolor/24x24
%dir %{_datadir}/icons/hicolor/24x24/*
%dir %{_datadir}/icons/hicolor/32x32
%dir %{_datadir}/icons/hicolor/32x32/*
%dir %{_datadir}/icons/hicolor/36x36
%dir %{_datadir}/icons/hicolor/36x36/*
%dir %{_datadir}/icons/hicolor/48x48
%dir %{_datadir}/icons/hicolor/48x48/*
%dir %{_datadir}/icons/hicolor/64x64
%dir %{_datadir}/icons/hicolor/64x64/*
%dir %{_datadir}/icons/hicolor/72x72
%dir %{_datadir}/icons/hicolor/72x72/*
%dir %{_datadir}/icons/hicolor/96x96
%dir %{_datadir}/icons/hicolor/96x96/*
%dir %{_datadir}/icons/hicolor/128x128
%dir %{_datadir}/icons/hicolor/128x128/*
%dir %{_datadir}/icons/hicolor/192x192
%dir %{_datadir}/icons/hicolor/192x192/*
%dir %{_datadir}/icons/hicolor/scalable
%dir %{_datadir}/icons/hicolor/scalable/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-2m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-9m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-7m)
- rebuild against rpm-4.6

* Tue Sep 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-6m)
- fix %%post scripts

* Tue Sep  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-5m)
- rename PreReq: gtk2-common to gtk2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-3m)
- %%NoSource -> NoSource

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-2m)
- add PreReq: gtk2-common

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Sat Feb 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Mon May 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.5-1m)
- version 0.5
- import from Fedora Core 1

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt
                                                                                
* Wed Feb  4 2004 Alexander Larsson <alexl@redhat.com> 0.3-1
- update to 0.3
                                                                                
* Fri Jan 16 2004 Alexander Larsson <alexl@redhat.com> 0.2-1
- Initial build.

