#! /bin/sh

version=$1

[ -z $version ] && exit 1

dir=agg-${version}
file=agg-${version}.tar.gz
result=agg-free-${version}.tar.gz

rm -rf agg-${version}
tar xzf $file
for file in copying.txt VERSIONS.TXT gpc.c gpc.h; do
   rm agg-${version}/gpc/$file
done
rm -f $result
tar czf $result $dir
