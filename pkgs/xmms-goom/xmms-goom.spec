%global momorel 1
%global debug_package %{nil}

Summary:     goom - visual effects plugin for xmms
Name:        xmms-goom
Version:     2k4
Release:     %{momorel}m%{?dist}
License:     LGPL
Group:       Applications/Multimedia
URL:         http://www.ios-software.com/
Source0:     http://dl.sourceforge.net/project/goom/goom2k4/0/goom-%{version}-0-src.tar.gz
NoSource:    0
Requires:    xmms >= 1.2.10-13m
BuildRequires: xmms-devel >= 1.2.10-13m, SDL-devel >= 1.2.7-10m
BuildRoot   : %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Goom is a visual effects generator for mp3 players, available for XMMS,
iTunes, Winamp and Windows Media Player.

%package devel
Summary:        development files for %{name}
Group:          Development/Libraries

%description devel
%{summary}.

%prep
%setup -q -n goom%{version}-0

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL KNOWNBUGS NEWS README
%{_bindir}/goom2
%{_libdir}/libgoom2.so.*
%{_libdir}/xmms/Visualization/libxmmsgoom2.so

%files devel
%defattr(-,root,root)
%{_includedir}/goom
%{_libdir}/libgoom2.so
%{_libdir}/pkgconfig/libgoom2.pc

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2k4-1m)
- update to 2k4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-23m)
- remove -Wno-unused-but-set-{variable,parameter} for gcc3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-22m)
- rebuild for new GCC 4.6

* Wed Mar  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-21m)
- revise CFLAGS

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.4-19m)
- full rebuild for mo7 release

* Sun Jun  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-18m)
- add BuildRequires: gcc3.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99.4-17m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-16m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.4-15m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.4-14m)
- drop --param=ssp-buffer-size=4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.4-13m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.4-12m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99.4-11m)
- rebuild against gcc43

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-10m)
- disabled debug_package

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.4-9m)
- support -mtune=generic

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.99.4-8m)
- delete libtool library

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.99.4-7m)
- change installdir (/usr/X11R6 -> /usr)

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.99.4-6m)
- rebuild against SDL-1.2.7-10m

* Sun Jan  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.99.4-5m)
- force using gcc_3_2.
- asm directive for gcc 3.4 is malfunction yet.

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.99.4-4m)
- rebuild against DirectFB-0.9.21

* Sun Nov 16 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99.4-3m)
- rebuild against xmms 1.2.8

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.99.4-2m)
  rebuild against DirectFB 0.9.18

* Tue Feb 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.99.4-1m)
  Imported.
