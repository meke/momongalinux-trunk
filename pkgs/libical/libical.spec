%global momorel 2
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name:		libical
Version:	0.47
Release:	%{momorel}m%{?dist}
Summary:	Reference implementation of the iCalendar data type and serialization format
Group:		System Environment/Libraries
License:	LGPLv2 or MPLv1.1
URL:		http://freeassociation.sourceforge.net/
Source0:	http://downloads.sourceforge.net/freeassociation/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	bison
BuildRequires:	byacc
BuildRequires:	flex

%description
Reference implementation of the iCalendar data type and serialization format
used in dozens of calendaring and scheduling products.

%package devel
Summary:	Development files for libical
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig

%description devel
The libical-devel package contains libraries and header files for developing 
applications that use libical.

%prep
%setup -q

%build
./bootstrap
%configure --disable-static -enable-cxx --enable-reentrant --enable-python
%make

%check
# make check
# Fails on x86_64 and ppc64.

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING LICENSE NEWS README THANKS TODO
%{_libdir}/%{name}.so.*
%{_libdir}/%{name}_cxx.so.*
%{_libdir}/libicalss.so.*
%{_libdir}/libicalss_cxx.so.*
%{_libdir}/libicalvcal.so.*
%exclude %{_libdir}/*.la

# python
%{python_sitelib}/%{name}
%{python_sitearch}/_LibicalWrap.*

%files devel
%defattr(-,root,root,-)
%doc doc/UsingLibical.txt
%{_includedir}/ical.h
%{_libdir}/%{name}.so
%{_libdir}/%{name}_cxx.so
%{_libdir}/libicalss.so
%{_libdir}/libicalss_cxx.so
%{_libdir}/libicalvcal.so
%{_libdir}/pkgconfig/libical.pc

%dir %{_includedir}/%{name}
%{_includedir}/%{name}/ical*.h
%{_includedir}/%{name}/icptrholder.h
%{_includedir}/%{name}/pvl.h
%{_includedir}/%{name}/sspm.h
%{_includedir}/%{name}/vcomponent.h

%{_includedir}/%{name}/port.h
%{_includedir}/%{name}/vcaltmp.h
%{_includedir}/%{name}/vcc.h
%{_includedir}/%{name}/vobject.h

%changelog
* Fri Dec  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-2m)
- enable to build on x86_64

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-5m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.43-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.43-2m)
- define __libtoolize (build fix)

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.43-1m)
- updated to 0.43
- delete patch0 (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-2m)
- rebuild against rpm-4.6

* Mon Nov 10 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (0.41-1m)
- updated to 0.41

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- import from Fedora devel

* Tue Oct 28 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.40-1
- Update to 0.40.
- Add patch from upstream to fix crash in icalvalue.c.
- Update makefile patch, remove the test part (already applied).
- Package libical.pc, add Requires: pkgconfig to -devel.

* Tue Sep 03 2008 Debarshi Ray <rishi@fedoraproject.org> - 0.32-1
- Version bump to 0.32.
- Parallel build problems fixed.

* Sun Jul 27 2008 Jeff Perry <jeffperry_fedora@sourcesink.com> - 0.31-3
- Added 'BuildRequires: bison byacc flex'.

* Sun Jul 27 2008 Debarshi Ray <rishi@fedoraproject.org> - 0.31-2
- Fixed linkage problems and disabled parallel build till upstream accepts fix.

* Thu Jul 17 2008 Jeff Perry <jeffperry_fedora@sourcesink.com> - 0.31-1
- Version bump to 0.31.

* Thu Jul 17 2008 Debarshi Ray <rishi@fedoraproject.org> - 0.30-4
- Changed value of License according to Fedora licensing guidelines.
- Enabled reentrant system calls and C++ bindings.
- Omitted unused direct shared library dependencies.
- Added ChangeLog, COPYING, LICENSE, NEWS and README to doc and dropped
  examples.

* Wed Apr 02 2008 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.30-3
- Source URL... Fixed

* Wed Apr 02 2008 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.30-2
- Removed untrue note about libical's homepage (to get rid of eventuall mess)

* Sat Feb 23 2008 David Nielsen <gnomeuser@gmail.com> - 0.30-1
- Switch to freeassociation libical
- bump to 0.30

* Sat Feb 09 2008 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.27-5
- Mass rebuild for new GCC... Done

* Sat Jan 19 2008 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.27-4
- Licence... Fixed

* Fri Jan 18 2008 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.27-3
- Files section... Fixed

* Thu Jan 17 2008 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.27-2
- Source... Changed
- Debug information in libical main package... Excluded
- Non-numbered .so files in libical main package... Moved
- libical-devel documentation... Added

* Mon Dec 24 2007 Jakub 'Livio' Rusinek <jakub.rusinek@gmail.com> - 0.27-1
- Initial release
