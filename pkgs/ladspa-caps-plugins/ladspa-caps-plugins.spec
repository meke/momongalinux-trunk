%global momorel 3

Summary: C* Audio Plugin Suite
Name: ladspa-caps-plugins
Version: 0.4.4
Release: %{momorel}m%{?dist}
License: GPL
URL: http://quitte.de/dsp/caps.html
Group: Applications/Multimedia
Source0: http://quitte.de/dsp/caps_%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel
BuildRequires: python

%description
CAPS, the C* Audio Plugin Suite, is a collection of refined LADSPA audio plugins capable of (and mainly intended for) realtime operation. The suite includes DSP units emulating instrument amplifiers, stomp-box classics, versatile 'virtual analogue' oscillators, fractal oscillation, reverb, equalization and more.

%prep
%setup -q -n caps-%{version}

%build
python configure.py
%make

%install
%make DEST=%{buildroot}%{_libdir}/ladspa RDFDEST=%{buildroot}%{_datadir}/ladspa/rdf install

%files
%defattr(-, root, root)
%doc COPYING CHANGES README caps.html
%{_libdir}/ladspa/*.so
%{_datadir}/ladspa/rdf/*

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.4.2-1m)
- initial build for Momonga Linux

