%global momorel 4
%define src_ver 2.1
%define src_rel 0

Summary: A unit testing framework for C
Name: CUnit
Version: %{src_ver}.%{src_rel}
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Libraries
URL: http://cunit.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/cunit/%{name}-%{src_ver}-%{src_rel}-src.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
CUnit is unit testing framework for C.


%prep
%setup -q -n %{name}-%{src_ver}-%{src_rel}


find Examples -type f -exec %__chmod -x {} \;


%build
%configure --enable-memtrace
%make


%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall
# Examples are already installed to docdir.
rm -rf %{buildroot}/usr/share/CUnit-2.1-0
# remove CUnit/*
rm -rf %{buildroot}%{_prefix}/doc

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}


%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL
%doc NEWS README TODO VERSION
%doc Examples
%{_includedir}/CUnit
%{_libdir}/libcunit*
%{_datadir}/CUnit
%{_mandir}/man*/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-3m)
- rebuild against gcc43

* Sun Mar 14 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.1-2m)
- revised spec for enabling rpm 4.2.

* Thu Dec 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.1-1m)
- update to 1.1.1.

* Sun Apr 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.0-1m)
- update to 1.1.0

* Tue Nov 19 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.8A-1m)
- update to 1.0.8A

* Thu Jun 13 2002 Kenta MURATA <muraken2@nifty.com>
- (1.0.6-2k)
- first spec file.
