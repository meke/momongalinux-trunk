%global momorel 1

Summary: gtkmm-documentation
Name: gtkmm-documentation
Version: 2.24.1
Release: %{momorel}m%{?dist}
Group: Documentation
License: GFDL
URL: http://www.gtkmm.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.24/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
BuildRequires: gnome-doc-utils-devel >= 0.9.0

%description
gtkmm-documentation

%prep
%setup -q

%build
%configure --disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update

%postun
rarian-sk-update

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.examples ChangeLog NEWS README
%{_datadir}/gnome/help/gtkmm-tutorial
%{_datadir}/doc/gtkmm-2.4/tutorial

%changelog
* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-4m)
- rebuild for new GCC 4.6

* Sun Mar 06 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.22.0-3m)
- add BR gnome-doc-utils-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.21.8.1-1m)
- update to 2.21.8.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-2m)
- full rebuild for mo7 release

* Mon Apr 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.17.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-1m)
- update to 2.17.4

* Wed Sep 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.3-1m)
- initial build
