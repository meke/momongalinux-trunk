%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

# enable tests
%global tests 1

Name: libkomparediff2
Summary: Library to compare files and strings
Version: %{kdever}
Release: %{momorel}m%{?dist}
# Library: GPLv2+ (some files LGPLv2+), CMake scripts: BSD
License: GPLv2+ and BSD
Group: System Environment/Libraries
URL: https://projects.kde.org/projects/kde/kdesdk/libkomparediff2
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRequires:  kdelibs-devel >= %{version}
Requires: diffutils
# Conflict with old Kompare builds which included libkomparediff2.
Conflicts: kompare < 4.11.4

%description
A shared library to compare files and strings using kdelibs and GNU diff,
used in Kompare and KDevelop.

%package devel
Summary: Developer files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
# Conflict with old Kompare builds which included libkomparediff2.
Conflicts: kompare-devel < 4.11.4

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} -DKDE4_BUILD_TESTS:BOOL=%{?tests}%{!?tests:0} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%check
%if 0%{?tests}
make test/fast -C %{_target_platform}
%endif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc COPYING*
%{_kde4_libdir}/libkomparediff2.so.*

%files devel
%{_kde4_includedir}/libkomparediff2
%{_kde4_libdir}/libkomparediff2.so
%{_kde4_libdir}/cmake/libkomparediff2

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- initial build (KDE 4.12.0)
