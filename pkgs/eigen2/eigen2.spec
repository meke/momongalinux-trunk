%global         momorel 4
%global         version_hash 9ca09dbd70ce

Name:		eigen2
Summary:	A lightweight C++ template library for vector and matrix math
Version:	2.0.16
Release:	%{momorel}m%{?dist} 
Epoch:          1
Group:		System Environment/Libraries
License:	"GPLv2+ or LGPLv3+"
URL:		http://eigen.tuxfamily.org/
Source0:	http://bitbucket.org/eigen/eigen/get/%{version}.tar.bz2
##NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/eigen2.specopt and edit it.
%{?!do_test: %global do_test 0}
%{?!with_doc: %global with_doc 0}

BuildRequires: cmake
%if %{with_doc}
BuildRequires: doxygen graphviz
BuildRequires: tetex-latex
%endif
%if %{do_test}
BuildRequires: blas-devel
BuildRequires: gsl-devel
BuildRequires: suitesparse-devel
BuildRequires: qt-devel
%endif

%description
%{summary}

%package devel
Summary: A lightweight C++ template library for vector and matrix math
Group:   Development/Libraries
# -devel subpkg only atm, compat with other distros
Provides: %{name} = %{version}-%{release}
Obsoletes: eigen
Obsoletes: eigen-devel

%description devel
%{summary}

%prep
%setup -q -n eigen-eigen-%{version_hash}

%build

unset flags
%if %{do_test}
cmakeflags="$cmakeflags -DEIGEN_BUILD_TESTS=ON"
%endif

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} $cmakeflags ..
popd

make -C %{_target_platform}

# docs
%if %{with_doc}
make %{?_smp_mflags} -C %{_target_platform}/doc
%endif

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}
%if %{with_doc}
rm -rf html
cp -r %{_target_platform}/doc/html .
%endif

%check
%if %{do_test}
export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
make -C %{_target_platform} test
%endif

%clean 
rm -rf %{buildroot}

%files devel
%defattr(-,root,root,-)
%doc COPYING COPYING.LESSER
%if %{with_doc}
%doc html/
%endif
%{_includedir}/eigen2/
%{_datadir}/pkgconfig/eigen2.pc

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1:2.0.16-4m)
- rebuild against graphviz-2.36.0-1m

* Thu Jul  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0.16-3m)
- no NoSource

* Wed Jul  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0.16-2m)
- revise Source0

* Wed Jul  6 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1:2.0.16-1m)
- update to 2.0.16

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0.15-6m)
- update source, use --rmsrc option

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.0.15-5m)
- rebuild for new GCC 4.6

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0.15-4m)
- eigen was OBSOLETED

* Sun Dec 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0.15-3m)
- add Epoch 

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.15-2m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15
-- see http://www.momonga-linux.org/archive/Momonga-devel.ja/msg04184.html
- revise spec
-- delete merged patch
-- fix document generation
-- fix %%check

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.52-0.20090729.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.52-0.20090729.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.52-0.20090729.2m)
- back to 20090729 snapshot

* Mon Aug  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.52-0.20090803.1m)
- update to recently snapshot
- no NoSource

* Wed Jul 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.52-0.20090729.1m)
- swith to upstream snapshot

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.52-0.20090609.1m)
- update to 2.0.52 svn snapshot (20090609)

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.3.2m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.3.1m)
- update to 2.0beta3

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.1.1m)
- import from Fedora devel

* Mon Sep 22 2008 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.4.beta1
- eigen-2.0-beta1

* Mon Aug 25 2008 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.3.alpha7
- disable buildtime tests, which tickle gcc bugs

* Fri Aug 22 2008 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.2.alpha7
- add working %%check

* Wed Aug 20 2008 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.1.alpha7
- eigen-2.0-alpha7
