%global momorel 2
%global itextvers 2.1.7

Summary:        The PDF Tool Kit
Name:           pdftk
Version:        1.44
Release:        %{momorel}m%{?dist}
License:        GPLv2+
URL:            http://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/
Source0:        http://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/%{name}-%{version}-src.zip
NoSource:       0
Patch0:         pdftk-use-internal-itext.patch
# Solves ".afm files not found" error. RHBZ#494785:
Patch4:         pdftk-classpath.patch
Group:          Applications/Publishing
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gcc-java
BuildRequires:  libgcj-devel
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  itext >= %{itextvers}
Requires:       itext >= %{itextvers}

%description
If PDF is electronic paper, then pdftk is an electronic staple-remover,
hole-punch, binder, secret-decoder-ring, and X-Ray-glasses. Pdftk is a simple
tool for doing everyday things with PDF documents. Keep one in the top drawer
of your desktop and use it to:

   * Merge PDF Documents
   * Split PDF Pages into a New Document
   * Decrypt Input as Necessary (Password Required)
   * Encrypt Output as Desired
   * Burst a PDF Document into Single Pages
   * Report on PDF Metrics, including Metadata and Bookmarks
   * Uncompress and Re-Compress Page Streams
   * Repair Corrupted PDF (Where Possible)

Pdftk is also an example of how to use a library of Java classes in a
stand-alone C++ program. Specifically, it demonstrates how GCJ and CNI allow
C++ code to use iText's (itext-paulo) Java classes.

%prep
%setup -q -n %{name}-%{version}-dist
%patch0 -p1
%patch4 -p0 -b .classpath

rm -rf java_libs

# Fix EOL encoding
for file in *.txt license_gpl_pdftk//*.txt; do
    sed 's|\r||' $file > $file.tmp
    touch -r $file $file.tmp
    mv $file.tmp $file
done

%build
# Requires as a workaround for gcc BZ #39380
export CFLAGS="${RPM_OPT_FLAGS}"
jar tf %{_javadir}/itext-%{itextvers}.jar | grep '\.class$' | sed 's/\.class//' | sed 's|/|\.|g' > classes
    gjavah -d java -cni -classpath=%{_javadir}/itext-%{itextvers}.jar \
       `cat classes`
    cd pdftk
    make -f Makefile.Redhat LIBDIR=%{_libdir} %{?_smp_mflags} ITEXTVERS="%{itextvers}" 

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1
install -m 0755 pdftk/pdftk $RPM_BUILD_ROOT/%{_bindir}/pdftk
install -m 0644 pdftk.1 $RPM_BUILD_ROOT/%{_mandir}/man1/pdftk.1

# momonga: remove an excess dependency
%define _use_internal_dependency_generator 0
cat << \EOF > %{name}.req
#!/bin/bash
grep -v %{_docdir} - | %{__find_requires} $* \
    | egrep -v 'itext-[0-9.]+\.jar\.so'
EOF

%define __find_requires %{_builddir}/%{name}-%{version}-dist/%{name}.req
chmod +x %{__find_requires}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc pdftk.1.html pdftk.1.txt
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44-2m)
- rebuild for new GCC 4.6

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41-5m)
- full rebuild for mo7 release

* Sat Nov 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41-4m)
- sync with Rawhide (1.41-23)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41-2m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.41-2m)
- rebuild against gcc43

* Tue Jun 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41-1m)
- update 1.41

* Mon Oct  9 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-4m)
- rebuild against libgcj-4.1.1-19m

* Sun Feb 19 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.12-3m)
- rebuild against gcc-4.1

* Mon Jan  2 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-2m)
- add gcc-4.1 patch
- Patch0 : pdftk-1.12-gcc41.patch

* Thu Jun 09 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Wed Oct 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.10-1m)
- minor bugfixes

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.00-2m)
- rebuild against gcc-java-3.4.2

* Thu Aug 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.00-1m)
- version up

* Thu Aug 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.941-1m)
- momonganize
