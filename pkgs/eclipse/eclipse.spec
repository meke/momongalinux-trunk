%global momorel 7
%global geckover 1.9.2

%global eclipse_build_srcname eclipse-build-0.5.0

#%%global pleiades_version   1.3.0
#%%global pleiades_relnum    38225

%global eclipse_dir %{_libdir}/%{name}

%ifarch %{ix86}
%global eclipse_arch x86
%else
%global eclipse_arch %{_arch}
%endif

Summary: Eclipse Integrated Development Environment
Name: eclipse
Version: 3.5.2
Release: %{momorel}m%{?dist}
License: EPL
Group: Development/Tools
URL: http://www.eclipse.org/
# http://wiki.eclipse.org/Linux_Tools_Project/Eclipse_Build
# downloading from download.eclipse.org is super slow
Source0: http://download.eclipse.org/technology/linuxtools/eclipse-build/%{eclipse_build_srcname}.tar.bz2
#NoSource: 0
Source1: http://download.eclipse.org/technology/linuxtools/eclipse-build/eclipse-%{version}-src.tar.bz2
#NoSource: 1
# http://mergedoc.sourceforge.jp/
#Source10: http://iij.dl.sourceforge.jp/mergedoc/%{pleiades_relnum}/pleiades_%{pleiades_version}.zip
#NoSource: 10
# r580 at http://svn.sourceforge.jp/svnroot/mergedoc/trunk/Pleiades/build/pleiades.zip
Source10: pleiades.zip
Source1000: eclipse.sh.in
Source1001: eclipserc.in
Patch0: %{eclipse_build_srcname}-glib-header.patch
Patch1: %{name}-%{version}-src-glib-header.patch
ExclusiveArch: %{ix86} x86_64
BuildRequires: coreutils tar unzip

BuildRequires: ant >= 1.7.1
BuildRequires: ant-antlr
BuildRequires: ant-apache-bcel
BuildRequires: ant-apache-bsf
BuildRequires: ant-apache-log4j
BuildRequires: ant-apache-oro
BuildRequires: ant-apache-regexp
BuildRequires: ant-apache-resolver
BuildRequires: ant-commons-logging
BuildRequires: ant-commons-net
BuildRequires: ant-javamail
BuildRequires: ant-jdepend
BuildRequires: ant-jsch
BuildRequires: ant-junit
BuildRequires: ant-nodeps
BuildRequires: ant-swing
BuildRequires: ant-trax
BuildRequires: junit
BuildRequires: junit4
BuildRequires: java-devel >= 1.6.0
BuildRequires: icu4j-eclipse >= 4.0.1-1m
BuildRequires: jsch >= 0.1.41-3m
BuildRequires: tomcat6-servlet-2.5-api >= 6.0.20-2m
BuildRequires: tomcat5-jsp-2.0-api >= 5.5.27-13m
BuildRequires: jakarta-commons-codec >= 1.4-1m
BuildRequires: jakarta-commons-el >= 1.0-10m
BuildRequires: jakarta-commons-httpclient >= 3.1-4m
BuildRequires: jakarta-commons-logging >= 1.0.4-7m
BuildRequires: tomcat5-jasper-eclipse >= 5.5.27-13m
BuildRequires: lucene >= 2.4.1-1m
BuildRequires: lucene-contrib >= 2.4.1-1m
BuildRequires: hamcrest >= 1.1-4m
BuildRequires: jetty >= 6.1.24-3m
BuildRequires: objectweb-asm >= 3.1-3m
BuildRequires: sat4j >= 2.1.1-1m

BuildRequires: GConf2-devel
BuildRequires: ORBit2-devel
BuildRequires: atk-devel
BuildRequires: audiofile-devel
BuildRequires: avahi-devel
BuildRequires: avahi-glib-devel
BuildRequires: cairo-devel
BuildRequires: dbus-glib-devel
BuildRequires: eggdbus-devel
BuildRequires: esound-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel >= 2.33.2
BuildRequires: glibc-devel
BuildRequires: glitz-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: gtk2-devel
BuildRequires: hunspell-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: libXtst-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libbonobo-devel
BuildRequires: libbonoboui-devel
BuildRequires: libgnome-devel
BuildRequires: libgnome-keyring-devel
BuildRequires: libgnomecanvas-devel
BuildRequires: libgnomeui-devel
BuildRequires: libgpg-error-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: libselinux-devel
BuildRequires: libstdc++-devel
BuildRequires: libuuid-devel
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: nspr-devel
BuildRequires: nss-devel
BuildRequires: openssl-devel
BuildRequires: pango-devel
BuildRequires: pixman-devel
BuildRequires: popt-devel
BuildRequires: sqlite-devel
BuildRequires: startup-notification-devel
BuildRequires: xcb-util-devel
BuildRequires: xulrunner-devel >= %{geckover}
BuildRequires: zlib-devel

Requires: java >= 1.6.0
Requires: icu4j-eclipse >= 4.0.1-1m
Requires: jsch >= 0.1.41-3m
Requires: tomcat6-servlet-2.5-api >= 6.0.20-2m
Requires: tomcat5-jsp-2.0-api >= 5.5.27-13m
Requires: jakarta-commons-codec >= 1.4-1m
Requires: jakarta-commons-el >= 1.0-10m
Requires: jakarta-commons-httpclient >= 3.1-4m
Requires: jakarta-commons-logging >= 1.0.4-7m
Requires: tomcat5-jasper-eclipse >= 5.5.27-13m
Requires: lucene >= 2.4.1-1m
Requires: lucene-contrib >= 2.4.1-1m
Requires: hamcrest >= 1.1-4m
Requires: jetty >= 6.1.24-3m
Requires: objectweb-asm >= 3.1-3m
Requires: sat4j >= 2.1.1-1m

Requires: gtk2
Requires: xulrunner >= %{geckover}

Requires(post): coreutils desktop-file-utils gtk2
Requires(postun): coreutils desktop-file-utils gtk2

Obsoletes: libswt3-gtk2 < 3.3.2-7m
Obsoletes: eclipse-rcp < 3.3.2-7m
Obsoletes: eclipse-cvs-client < 3.3.2-7m
Obsoletes: eclipse-platform < 3.3.2-7m
Obsoletes: eclipse-jdt < 3.3.2-7m
Obsoletes: eclipse-pde < 3.3.2-7m
Obsoletes: eclipse-pde-runtime < 3.3.2-7m
Obsoletes: eclipse-cdt < 4.0.3-3m
Obsoletes: eclipse-cdt-sdk < 4.0.3-3m
Obsoletes: eclipse-changelog < 2.6.1-3m
Obsoletes: eclipse-rpm-editor < 0.2.1-3m

%description
Eclipse is an open extensible IDE for anything and nothing in particular.

%prep
%setup -q -n %{eclipse_build_srcname}

%patch0 -p1 -b .apply_glib_h_patch

cp %{SOURCE1} .
cp %{PATCH1} patches

sed -e 's,@@ECLIPSE_DIR@@,%{eclipse_dir},g' %{SOURCE1000} > eclipse.sh
sed -e 's,@@ECLIPSE_DIR@@,%{eclipse_dir},g' %{SOURCE1001} > eclipserc

%build
./build.sh

%install
rm -rf %{buildroot}

ant -DdestDir=%{buildroot} \
    -Dprefix=%{_prefix} \
    -DbuildArch=%{eclipse_arch} \
    -Dmultilib=true \
    install

unzip -q %{SOURCE10} -d %{buildroot}%{eclipse_dir}

rm -f %{buildroot}%{_bindir}/%{name}
install -m 0755 ./eclipse.sh %{buildroot}%{_bindir}/%{name}
install -m 0644 ./eclipserc %{buildroot}%{eclipse_dir}/eclipserc

install -d -m 0755 %{buildroot}%{eclipse_dir}/links
install -d -m 0755 %{buildroot}%{_datadir}/%{name}/features
install -d -m 0755 %{buildroot}%{_datadir}/%{name}/plugins

rm -f %{buildroot}%{eclipse_dir}/plugins/com.ibm.icu_*.jar

%clean
rm -rf %{buildroot}

%post
update-desktop-database %{_datadir}/applications &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
update-desktop-database %{_datadir}/applications &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
 
%files
%defattr(-,root,root,-)
%{_sysconfdir}/%{name}.ini
%{_bindir}/%{name}
%{eclipse_dir}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/*/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sat Jun 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-7m)
- add a patch to enable build

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.2-3m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- add BuildRequires

* Fri Jul 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.2-1m)
- update to 3.5.2 with pleiades

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-9m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-7m)
- apply Patch10,11 for xulrunner-1.9.1 from Fedora 11 (1:3.4.2-12)

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-6m)
- rebuild against xulrunner-1.9.1

* Sun May 10 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-5m)
- switch to use %%{java_home}

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-4m)
- Obsoletes: eclipse-pde-runtime

* Wed May  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-3m)
- add Obsoletes to kill Fedora Eclipse

* Wed May  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-2m)
- add BuildRequires and Requires

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.2-1m)
- update to 3.4.2
-- back to momonga style
-- use pleiades
-- 
-- * Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
-- - (3.2.2-5m)
-- - rebuild against gcc43
-- 
-- * Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
-- - (3.2.2-4m)
-- - %%NoSource -> NoSource

* Wed Apr 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.2-6m)
- revive org.eclipse.jdt.core_*

* Mon Apr 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.2-5m)
- not provide eclipse-ecj since ecj is provided by ecj package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.2-4m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.2-3m)
- %%global _default_patch_fuzz 2

* Sun Jul 6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-2m)
- revise %%files to avoid conflicting

* Sun Jul 6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.2-1m)
- update 3.3.2

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.1.1-4m)
- rebuild against firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.1.1-3m)
- rebuild against gcc43

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1.1-2m)
- modify Requires and Obsoletes
- fix %%files to avoid conflicting

* Tue Mar 11 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.1.1-1m)
- version up 3.3.1.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.0-5m)
- %%NoSource -> NoSource

* Sat Sep 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-4m)
- disable debug_package
- import two bug-fix patches from fc-devel

* Tue Aug 7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.0-3m)
- add Requires for firefox

* Wed Jul 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0-2m)
- revise %%files to avoid conflicting

* Tue Jul 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.0-1m)
- update eclipse-3.3
- use NoSource

* Mon Jun 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-4m)
- eclipse-platform Provides and Obsoletes eclipse3
  eclipse3 is conflicting with eclipse-ecj

* Tue Jun 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-3m)
- remove Provides and Obsoletes: eclipse3

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-2m)
- modify Requires, Conflicts, Obsoletes and %%files

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.2-1m)
- import from Fedora

-- * Thu May 17 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-14
-- - BR/R tomcat5 >= 5.5.23.
-- - Fix broken symlinks for tomcat5 5.5.23.
-- 
-- * Tue May 15 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-13
-- - Another bug fix for launch-addplatformtotildeeclipse.patch.
-- - Add BR/B tomcat >= 5.5.20 instead of just = 5.5.20.
-- - Resolves: #240025.
-- 
-- * Wed May 02 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-12
-- - Fix additional problem with launcher-addplatformtotildeeclipse.patch.
-- - Resolves: #238109.
-- 
-- * Mon Apr 30 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-11
-- - Add workaround in launcher-addplatformtotildeeclipse.patch for problems
--   caused by bug #238109.
-- - Resolves: #238109.
-- 
-- * Fri Apr 27 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-10
-- - Fix problem in launcher-addplatformtotildeeclipse.patch.
-- - Resolves: #238109.
-- 
-- * Fri Apr 27 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-9
-- - Remove BR eclipse-pde.
-- - Related: #236895
-- 
-- * Wed Apr 11 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-8
-- - Link to system-installed jsch instead of including it.
-- - Fix links to system-installed javadocs.
-- 
-- * Tue Mar 20 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-7
-- - Remove search and processing for mac encoded files.
-- - Remove BR dos2unix.
-- 
-- * Mon Mar 19 2007 Thomas Fitzsimmons <fitzsim@redhat.com> 3.2.2-6
-- - Remove gjdoc build requirement.
-- 
-- * Fri Mar 16 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-5
-- - Update package-build releng script to work with mylar.
-- 
-- * Thu Mar 15 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-4
-- - Update to tomcat 5.5.20.
-- 
-- * Fri Mar 02 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-3
-- - Uncomment 'this.generatePackagesStructure = true;' in ecj-gcj patch.
-- 
-- * Mon Feb 26 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-2
-- - Add gcc messages to ecj-gcj patch.
-- 
-- * Wed Feb 21 2007 Ben Konrath <bkonrath@redhat.com> 3.2.2-1
-- - 3.2.2.
-- - Remove patch that disables Java 5 code.
-- - Add -DjavacSource=1.5 -DjavacTarget=1.5 to ant compile line.
-- 
-- * Wed Feb 07 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-37
-- - Move rcp feature to %%{_libdir} to avoid multilib conflict on ppc{,64}.
-- 
-- * Tue Feb 06 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-36
-- - Rework ppc64, s390{x} and sparc{64} hack again to try to fix multilib
--   problem.
-- 
-- * Thu Feb 01 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-35
-- - Use original name for the SWT JNI symlinks.
-- - Rework ppc64, s390{x} and sparc{64} hack to fix multilib problem.
-- - Update ecj [] patch to upstream version from 3.3.
-- 
-- * Tue Jan 30 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-34
-- - Create symlinks to the SWT JNI libs in %%{_libdir}/eclipse with sane
--   versions.
-- 
-- * Mon Jan 29 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-33
-- - Check for features directory in sdk postun script.
-- - Resolves: #224588.
-- 
-- * Fri Jan 26 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-32
-- - Fix bug in ecj [] patch.
-- 
-- * Tue Jan 16 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-31
-- - Add bugzilla reference to remove jars bug in comment.
-- - Update bugzilla refereces to [] bugs.
-- 
-- * Fri Jan 12 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-30
-- - Fix %%postun problem in the sdk sub-package.
-- 
-- * Thu Jan 11 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-29
-- - Remove SWT JNI symlinks from %%{libdir}.
-- 
-- * Wed Jan 10 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-28
-- - Rpmlint cleanups.
-- 
-- * Fri Jan 05 2007 Ben Konrath <bkonrath@redhat.com> 3.2.1-27
-- - Use /g in tomcat version replacement.
-- - Disable com.jcraft.jsch_0.1.28.jar and
--   org.eclipse.osgi_3.2.1.R32x_v20060919.jar on ia64.
-- 
-- * Tue Dec 19 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-26
-- - Use sed instead of patch for tomcat version.
-- - Add BuildRequires desktop-file-utils.
-- 
-- * Wed Dec 6 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-25
-- - Add %%{_libdir}/eclipse dir to files list of libswt-gtk2.
-- - Resolves: #211008.
-- 
-- * Tue Dec 5 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-24
-- - Specfile review with Ben Konrath.
-- - Lots of cleanups.
-- 
-- * Tue Nov 28 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-23
-- - Move back to ~/.eclipse for update site pending upstream comments.
-- - Add patch to add platform to ~/.eclipse's platform.xml. This maintains
--   user-installed plugins but allows us to remove the pre-configured
--   platform.xml in the OSGi configuration area.
-- 
-- * Mon Nov 20 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-22
-- - Use ~/.eclipseplugins instead of ~/.eclipse in update site - homedir patch.
-- - Bump release.
-- 
-- * Fri Nov 17 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-21
-- - Add patch to workaround an xml parsing bug in libgcj (gcc bug #29853).
-- - Resolves: #209393.
-- 
-- * Fri Nov 17 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-20
-- - Revise gre64 patch to just do ppc64 addition and not ordering change.
-- 
-- * Thu Nov 16 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-19
-- - Add patch to look at gre64.conf on ppc64.
-- 
-- * Fri Nov 10 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-18
-- - Remove SWT ON_TOP patch as it is fixed in 3.2.1.
-- 
-- * Thu Nov 09 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-17
-- - Add file level requirement for swt fragment to rcp and platform packages.
--   This is needed so that the rcp and platform packages pull in the swt package
--   of the correct word size.
-- 
-- * Mon Nov 06 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-16
-- - Move copy-platform back to %%{_datadir}/eclipse.
-- - Require gjdoc >= 0.7.7-14 as it generates consistent html across archs.
-- - Move most of the doc plugins back to %%{_datatdir}/eclipse now that gjdoc
--   is fixed.
-- 
-- * Fri Nov 03 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-15
-- - Make sdk require config.ini itself rather than the package to deal with the
--   bi-arch installation situation.
-- - Move sdk feature and plugin to %%{_libdir} so we can check for its existence
--   in the post scripts.
-- 
-- * Thu Nov 02 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-14
-- - Remove post sections that munge eclipse.product; always set it to
--   org.eclipse.platform.ide or org.eclipse.sdk.ide.
-- - Remove changelogs prior to 3.2.0.
-- 
-- * Thu Nov 02 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-12
-- - Move doc plugins to %%{_libdir}/eclipse/plugins because of html is being
--   generated differently on different arches.
-- - Fix multilib problem when there are two or more jars within a jar.
-- - BR dos2unix always (for mac2unix).
-- 
-- * Wed Nov 01 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-11
-- - Move copy-platform to %%{_libdir}/eclipse
-- - Move the platform.source, icu4j, icu4j.source, help.webapp and
--   update.core.linux plugins to %%{_libdir}/eclipse/plugins because these plugins
--   have platform specific content. Some of the platform specific content may be
--   a result of bugs in libgcj. These need to be investigated.
-- - Disable building the help indexes on all archs so that we have the same doc
--   plugins on all archs.
-- - Remove org.apache.ant_1.6.5/bin/runant.py to avoid multilib conflicts.
-- - Repack all the jars and the jars within those jars. This is needed
--   to make this package multilib compatible.
-- - Put SWT symlinks in %%{_libdir}/eclipse instead of
--   %%{_libdir}/eclipse/plugins.
-- 
-- * Wed Nov 01 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-11
-- - Use equinox initializer instead of old patch to core.runtime.
-- - Run initializer *after* splitting install into arch-specific and
--   arch-independent locations.
-- - Move copy-platform to arch-specific location.
-- - Get rid of broken symlinks in tomcat plugin.
-- 
-- * Tue Oct 31 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-11
-- - Fix copy-platform to work with split install.
-- 
-- * Tue Oct 31 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-10
-- - Add 3.2.1 splash screen.
-- - Sort the java source files before building (#209249).
-- - Remove Fedora ifdefs.
-- - Resolves: #209249.
-- 
-- * Tue Oct 31 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-9
-- - Re-enable building of the icu4j plugins.
-- 
-- * Mon Oct 30 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-8
-- - Temporarily disable building of icu4j plugin.
-- 
-- * Mon Oct 30 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-8
-- - Fix eclipse-ecj symlink to point to correct location.
-- - Put SWT symlinks in %%{_libdir} instead of %%{_datadir} as they're
--   target-dependent.
-- 
-- * Sat Oct 28 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-7
-- - Add patch for ecj [] classpath problem.
-- - Remove configuration files from rcp files list.
-- - Add patch set bindir and shared config patch to allow the eclipse binary
--   to sit in %%{_bindir} and remove the symlinks. This patch also allows us to
--   set osgi.sharedConfiguration.area config on a per build basis so that the
--   configuration directory can be arch dependant.
-- - Remove launcher link patch as the bindir patch removes the requirement for
--   this patch.
-- - Don't aot-compile org.eclipse.ui.ide to work around rh bug # 175547.
-- - Add Requies(post,postun) to all packages to ensure that no files are left
--   behind when eclipse is un-installed.
-- - Many spec file clean ups.
-- - Resolves: #199961, #202585, #210764, #207016.
-- - Related: #175547.
-- 
-- * Mon Oct 16 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-6
-- - Remove unneeded tomcat symlinks.
-- 
-- * Mon Oct 16 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-6
-- - Remove unnecessary bits of configuration.
-- - Resolves: #210764, #202585.
-- 
-- * Tue Oct 10 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-5
-- - Remove perl dependancy.
-- - Fix offsets in libswt-enableallandO2 patch.
-- - Add Requires(post,postun) java-gcj-compat to sdk to avoid errors when
--   uninstalling the java stack.
-- - Move jface and jface.databinding from libswt3-gtk2 to rcp because these
--   plugins have dependancies on some plugins in rcp.
-- 
-- * Sun Oct 01 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-4
-- - Fix triggerpostun to include epoch of previous releases.
-- 
-- * Fri Sep 29 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-3
-- - Add Requires(post,postun) to platform and sdk sub-packages so that post and
--   postun scripts work correctly.
-- 
-- * Fri Sep 29 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-3
-- - Add triggerpostun to deal with old rebuild-sdk-features postun crap
--   (rh#207442, rh#207013).
-- 
-- * Fri Sep 29 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-2
-- - Fix swt-firefox patch to not create DSOs with undefined symbols (rh#201778).
-- 
-- * Thu Sep 28 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-1
-- - 3.2.1 final.
-- 
-- * Thu Sep 28 2006 Andrew Overholt <overholt@redhat.com> 3.2.1-1
-- - Use new swt-firefox patch and consolidate others into one.
-- 
-- * Wed Sep 27 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-0.RC.3
-- - Fix typo in %%postun of -sdk and -platform.
-- - Disable help index generation on ia64.
-- 
-- * Tue Sep 26 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-0.RC.2
-- - Don't set eclipse.product incorrectly in %%postun of -sdk and -platform.
-- 
-- * Mon Sep 25 2006 Ben Konrath <bkonrath@redhat.com> 3.2.1-0.RC.1
-- - M20060921-0945 (3.2.1 pre-release).
-- - Upadate patches to 3.2.1.
-- - Add icu4j 3.4.5 sources.
-- - Add Fedora version to platform about.mappings as well as sdk.
-- 
-- * Mon Sep 25 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-8
-- - Remove unused eclipse.conf.
-- - Remove unused gre64.conf patch (applied upstream).
-- 
-- * Thu Sep 21 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-7
-- - Use real SWT version on ia64, ppc64, s390 and s390x.
-- - Remove the swt native libs from the rcp sub-package because they are
--   already in the libswt-gtk2 sub-package and rcp requires libswt-gtk2.
-- - Set correct eclipse.product in post and postun of sdk and platform
--   sub-packages (rh bug # 207442)
-- - Don't set the .eclipseproduct twice.
-- - Add Conflicts: mozilla to libswt3-gtk2 (rh bug # 207626).
-- - Move Requires: firefox to libswt3-gtk2.
-- 
-- * Thu Sep 21 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-7
-- - Add workaround for gjdoc Mac-encoded bug (gcc#29167) to make javadocs build.
-- - Fix tomcat symlinking in %%install to make help work (rh#199453).
-- 
-- * Mon Sep 11 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-6
-- - Update swt-firefox patch and remove libswt-xpcom patch (rh bug # 200929).
-- - Re-work files list to match upstream and remove rebuild-sdk-features
--   (rh bug # 205933).
-- 
-- * Thu Sep 07 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-5
-- - Add swt-firefox patch; moves SWT to NS_InitXPCOM3() from NS_InitEmbedding().
-- - Add sparc support (Dennis Gilmore <dennis@ausil.us>).
-- - Disable help index generation on s390.
-- 
-- * Wed Sep 06 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-4
-- - Re-add customBuildCallbacks.xml-add-pre.gather.bin.parts.patch because
--   it has not been applied upstream.
-- - Minor spec file clean ups.
-- - Add %%{name} to the -devel package Provides so that upgrading from
--   FC-5 to FC-6 works.
-- - Re-enable natively compiling the ant.ui plugin.
-- - Re-enable natively compiling the team.cvs.core plugin on ia64.
-- 
-- * Fri Sep 01 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-3
-- - Use the system tomcat on ppc64 and s390x.
-- 
-- * Wed Aug 30 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-3
-- - Don't use pkg_summary.
-- 
-- * Tue Aug 29 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-2
-- - Remove double Manifest file in com.jcraft.jsch_0.1.28.jar.
-- - Require java-gcj-compat >= 1.0.64.
-- 
-- * Thu Aug 17 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_12fc
-- - Fix tomcat55 patch to not conflict with wst.ws (commons-logging visibility).
-- 
-- * Thu Aug 10 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_11fc
-- - Fix eclipse-tomcat55.patch and eclipse-tomcat55-build.patch to not reference
--   jars that don't exist.
-- - Fix rebuild-sdk-features to work with 3.2.x feature versions.
-- 
-- * Tue Aug 01 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_10fc
-- - Use firefox-devel instead of mozilla-devel.
-- - Add patch for rh#200929 (include embed_base in xpcom's pkgconfig -I flags).
-- 
-- * Tue Jul 25 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-1jpp_9fc
-- - Use sed instead of patches for tomcat version numbers.
-- 
-- * Sun Jul 23 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-1jpp_8fc
-- - Disable build.index doc generation on i386.
-- 
-- * Sun Jul 23 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_7fc
-- - Remove org.eclipse.ant.ui* from %%files.
-- 
-- * Sun Jul 23 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-1jpp_7fc
-- - Exclude org.eclipse.ant.ui_3.2.0.v20060531.jar from aot-compile-rpm.
-- 
-- * Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> 3.2.0-1jpp_6fc
-- - Rebuilt
-- 
-- * Thu Jul 20 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_5fc
-- - New splash screen from Diana Fong (rh#199456).
-- 
-- * Tue Jul 18 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_4fc
-- - Add patch from Tom Tromey for ecj-gcj branch of gcj.
-- - Fix SWT symlinks.
-- 
-- * Tue Jul 18 2006 Igor Foox <ifoox@redhat.com> 3.2.0-1jpp_3fc
-- - Rebuild.
-- 
-- * Mon Jul 17 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-1jpp_2fc
-- - Rebuild.
-- 
-- * Thu Jul 06 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_1fc
-- - Temporarily add webapp patch for ppc64 and s390x.
-- - Bump tomcat5 to 5.5.17 both in BR/R and in patches.
-- 
-- * Wed Jul 05 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_1fc
-- - Don't require tomcat on ppc64, s390, or s390x until we've got it there.
-- - org.eclipse.tomcat -> org.eclipse.tomcat/lib.
-- - Update webapp build patch.
-- - Use commons-* instead of jakarta-commons-*.
-- - Don't delete jars in %%install.
-- - Don't apply tomcat and webapp patches on ppc64, s390, and s390x (for now).
-- - Don't include tomcat jars in %%files for ix86, ppc, x86_64, ia64.
-- - Use tomcat plugin version instead of org.eclipse.tomcat_*.
-- 
-- * Tue Jul 04 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-1jpp_1fc
-- - Fix tomcat symlinks.
-- 
-- * Tue Jul 04 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-1jpp_1fc
-- - 3.2.0.
-- - Remove Provides: eclipse-sdk from eclipse-platform.
-- - Use build-jar-repository where appropriate.
-- 
-- * Tue Jun 13 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.7
-- - Rename -devel packages to -sdk to match upstream names.
-- - Add Provides eclipse-sdk to platform-sdk.
-- - Remove zip re-pack code.
-- 
-- * Mon Jun 12 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.6
-- - Bump release again.
-- 
-- * Mon Jun 12 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.5
-- - Bump release.
-- 
-- * Mon Jun 12 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.4
-- - Require java-gcj-compat 1.0.52.
-- - Don't use '*' to set the quatifier name in the eclipse-ecj symlink.
-- - Really fix swt symlinks rh #194500.
-- 
-- * Wed Jun 07 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.3
-- - Enable ppc64, s390 and s390x.
-- - Add check for jars at the end of prep.
-- - Fix patch for rh #162177 (square brackets patch).
-- - Fix swt symlinks rh #194500.
-- - Add versionless pde.build symlink.
-- - Rename efj-wrapper.sh to efj.sh.in.
-- - Re-pack all zips after the build to ensure that zips have the same md5sum
--   across arch re-builds. This is needed to avoid multilib conflicts.
-- 
-- * Wed Jun 07 2006 Andrew Overholt <overholt@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.2
-- - Fix eclipse-ecj.jar symlink to include qualifier.
-- 
-- * Tue Jun 06 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC7.1
-- - 3.2RC7.
-- - Remove com.jcraft.jsch_0.1.28.jar and repackage with classes from the system
--   jsch.jar.
-- - Work around ia64 compile problem in aot-compile-rpm.
-- 
-- * Wed May 31 2006 Ben Konrath <bkonrath@redhat.com> 3.2.0-0jpp_0fc.3.2RC6
-- - 3.2RC6. 

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-3m)
- update eclipse3.desktop and link icon

* Mon May  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.2-2m)
- support gcj
- Obsoletes: ecj-compat

* Wed Feb 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct  4 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Fri Aug 18 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-1m)
- update to 3.2
  add Patch0: eclipse3-set-javac-properties.patch

* Thu Jun 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-2m)
- enable x86_64

* Wed Jun 28 2006 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (3.1.2-1m)
- version 3.1.2
- to main

* Fri Dec  2 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (3.1.1-1m)
- version 3.1.1
- add language packs.

* Sun Aug 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.1-1m)
- up to 3.1
- BuildPreReq: java-sun-j2se1.5-sdk

* Sat Oct 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.1-3m)
- build eclipse from source

* Mon Sep 27 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.1-2m)
- added 'export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/lib"' to eclipse3.sh.in
  in order to be loaded libjavahl-1.so by subclipse plugin
  If adding /usr/lib to LD_LIBRARY_PATH is not good for security,
  we should move libjavahl-1.so* to the isolated directory.

* Sun Sep 26 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.1-1m)
- 
