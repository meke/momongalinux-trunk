%global momorel 8

%define _x11fontdir %{_datadir}/X11/fonts
%define catalogue /etc/X11/fontpath.d

Name:           libdockapp
Version:        0.6.2
Release:        %{momorel}m%{?dist}
Summary:        DockApp Development Standard Library

Group:          System Environment/Libraries
License:        MIT
URL:            http://solfertje.student.utwente.nl/~dalroi/libdockapp/
Source0:        http://solfertje.student.utwente.nl/~dalroi/libdockapp/files/libdockapp-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libXpm-devel libXext-devel libX11-devel

%description
LibDockApp is a library that provides a framework for developing dockapps. 
It provides functions and structures to define and display command-line 
options, create a dockable icon, handle events, etc.

The goal of the library is to provide a simple, yet clean interface and 
standardize the ways in which dockapps are developed. A dockapp developed 
using libDockApp will automatically behave well under most window 
managers, and especially well under Window Maker.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       libXpm-devel
Requires:       libX11-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        fonts
Summary:        Fonts provided with %{name}
Group:          User Interface/X

%description fonts
Bitmap X fonts provided with libdockapp.


%prep
%setup -q
find . -depth -type d -name CVS -exec rm -rf {} ';'

%build
%configure --disable-static --without-examples --disable-rpath
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT XFONTDIR=$RPM_BUILD_ROOT%{_x11fontdir} \
   INSTALL='install -p'
rm $RPM_BUILD_ROOT%{_libdir}/libdockapp.la
# fonts.alias is empty, so unusefull.
rm $RPM_BUILD_ROOT%{_x11fontdir}/dockapp/fonts.alias

rm -rf __examples_dist
cp -a examples __examples_dist
rm __examples_dist/Makefile*

mkdir -p $RPM_BUILD_ROOT%{catalogue}
ln -sf %{_x11fontdir}/dockapp $RPM_BUILD_ROOT%{catalogue}/dockapp

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%post fonts
if [ -x /usr/bin/fc-cache ]; then
  /usr/bin/fc-cache %{_datadir}/X11/fonts
fi

%postun -p /sbin/ldconfig

%postun fonts
if [ "$1" = "0" ]; then
  if [ -x /usr/bin/fc-cache ]; then
    /usr/bin/fc-cache %{_datadir}/X11/fonts
  fi
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS COPYING NEWS README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc __examples_dist/*
%{_includedir}/*
%{_libdir}/*.so

%files fonts
%defattr(-,root,root,-)
%{_x11fontdir}/dockapp/
%{catalogue}/dockapp

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-8m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.2-5m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-2m)
- remove duplicate directory

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.2-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon May  5 2008 Patrice Dumas <pertusus@free.fr> 0.6.2-1
- update to 0.6.2

* Sat Feb  2 2008 Patrice Dumas <pertusus@free.fr> 0.6.1-6
- more portable stdincl patch

* Thu Dec 27 2007 Patrice Dumas <pertusus@free.fr> 0.6.1-5
- minor cleanups

* Sun Sep 30 2007 Patrice Dumas <pertusus@free.fr> 0.6.1-4
- new fontpath.d configuraton mechanism, change by ajax, Adam Jackson

* Thu May 24 2007 Patrice Dumas <pertusus@free.fr> 0.6.1-3
- fix libtool bug on ppc64

* Wed Jan 10 2007 Patrice Dumas <pertusus@free.fr> 0.6.1-2
- don't ship the empty fonts.alias

* Fri Jan  5 2007 Patrice Dumas <pertusus@free.fr> 0.6.1-1
- Initial release
