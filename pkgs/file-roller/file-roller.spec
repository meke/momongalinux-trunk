%global momorel 1

Summary:	An archive manager for GNOME.
Name:		file-roller
Version: 3.6.1.1
Release: %{momorel}m%{?dist}
License:	GPL
URL:		http://fileroller.sourceforge.net
Group:		Applications/Archiving
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): GConf2
Requires(pre): rarian
Requires(pre): desktop-file-utils
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gtk3-devel
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: nautilus-devel >= 3.0
BuildRequires: openssl-devel >= 0.9.8g
Requires(pre): hicolor-icon-theme gtk2

%description
File Roller is an archive manager for the GNOME environment.  This means that 
you can : create and modify archives; view the content of an archive; view a 
file contained in the archive; extract files from the archive.
File Roller is only a front-end (a graphical interface) to archiving programs 
like tar and zip. The supported file types are :
    * Tar archives uncompressed (.tar) or compressed with
          * gzip (.tar.gz , .tgz)
          * bzip (.tar.bz , .tbz)
          * bzip2 (.tar.bz2 , .tbz2)
          * compress (.tar.Z , .taz)
          * lzop (.tar.lzo , .tzo)
    * Zip archives (.zip)
    * Jar archives (.jar , .ear , .war)
    * Lha archives (.lzh)
    * Rar archives (.rar)
    * Single files compressed with gzip, bzip, bzip2, compress, lzop

%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    --disable-scrollkeeper \
    --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
update-desktop-database -q 2> /dev/null || :

%postun
update-desktop-database -q 2> /dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:
rarian-sk-update

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS NEWS README COPYING ChangeLog HACKING MAINTAINERS TODO
%{_bindir}/file-roller
%{_libexecdir}/%{name}
%{_datadir}/applications/file-roller.desktop
%{_libdir}/nautilus/extensions-3.0/libnautilus-fileroller.*
%doc %{_datadir}/help/*/file-roller
%{_datadir}/icons/hicolor/*/*/*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/icons
%{_datadir}/%{name}/packages.match
%{_datadir}/dbus-1/services/org.gnome.FileRoller.service
%{_datadir}/GConf/gsettings/file-roller.convert
%{_datadir}/glib-2.0/schemas/org.gnome.FileRoller.gschema.xml

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1.1-1m)
- update to 3.6.1.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-3m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- hide warning %%post

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.1-1m)
- update to 2.30.1.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.3-2m)
- touch src/typedefs.h

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Oct  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.4-1m)
- update to 2.22.4

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.4-1m)
- update to 2.18.4

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.14-2m)
- delete libtool library

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.14-1m)
- update to 2.14.14

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Wed Apr 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.3-1m)
- version 2.8.3
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.4-2m)
- revised spec for enabling rpm 4.2.

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.3-1m)
- version 2.4.3

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Wed Oct 01 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0.1-2m)
- added sigsegv-when-parent-window-destroyed-while-extracting.patch

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0.1-1m)
- initial import
