%global momorel 8
%define enable_debug 1

%global _sbindir /sbin

%global optflags %(echo %{optflags}|sed 's/-fstack-protector//')

Name: reiser4progs
Version: 1.0.7
Release: %{momorel}m%{?dist}
Summary: Utilities for reiser4 filesystems
License: GPL
Group: System Environment/Base
URL: http://www.namesys.com/
#Source0: ftp://ftp.namesys.com/pub/%{name}/%{name}-%{version}.tar.gz 
#NoSource: 0
Source0: http://www.kernel.org/pub/linux/utils/fs/reiser4/reiser4progs/reiser4progs-%{version}.tar.bz2
Patch0: dont-exec-run-ldconfig.patch
BuildRequires: libaal-devel >= 1.0.5-2m
BuildRequires: readline-devel >= 5.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Utilities for manipulating reiser4 filesystems.

%package devel
Summary: Development libraries and headers for developing reiser4 tools.
Group: Development/Libraries

%description devel
Development libraries and headers for developing reiser4 tools.

%prep
%setup -q
%patch0 -p1

%build
%ifnarch ppc
CFLAGS="$CFLAGS -fno-stack-protector %{optflags}"
%else
CFLAGS="$CFLAGS %{optflags}"
%endif
%configure \
%if %{enable_debug}
        --enable-debug \
%else
        --disable-debug \
%endif
	--enable-libminimal \
	--disable-plugins-check \
        --disable-fnv1-hash \
        --disable-rupasov-hash \
        --disable-tea-hash \
        --disable-deg-hash
make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING CREDITS INSTALL NEWS README THANKS TODO
%{_libdir}/libreiser4-1.0.so.*
%{_libdir}/libreiser4-minimal-1.0.so.*
%{_libdir}/librepair-1.0.so.*
# %{_sbindir}/cpfs.reiser4
%{_sbindir}/debugfs.reiser4
%{_sbindir}/fsck.reiser4
%{_sbindir}/make_reiser4
%{_sbindir}/measurefs.reiser4
%{_sbindir}/mkfs.reiser4
# %{_sbindir}/resizefs.reiser4
%{_mandir}/man8/*

%files devel
%defattr(-,root,root)
# %{_includedir}/aux/*.h
%dir %{_includedir}/reiser4
%{_includedir}/reiser4/*.h
%dir %{_includedir}/repair
%{_includedir}/repair/*.h
%{_datadir}/aclocal/libreiser4.m4
%{_libdir}/libreiser4.a
%{_libdir}/libreiser4.so
%{_libdir}/libreiser4-minimal.a
%{_libdir}/libreiser4-minimal.so
%{_libdir}/librepair.a
%{_libdir}/librepair.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-6m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-5m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-4m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-7m)
- update 1.0.7

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-5m)
- rebuild against rpm-4.6

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-4m)
- add Source URL
- current official server was 
-- http://www.kernel.org/pub/linux/utils/fs/reiser4/

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-2m)
- %%NoSource -> NoSource

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-1m)
- update 1.0.6

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-4m)
- delete libtool library

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.5-3m)
- rebuild against readline-5.0

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-2m)
- revise %%files for rpm-4.4.2

* Tue Nov  1 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-1m)
- update 1.0.5

* Sat Apr 30 2005 mutecat <mutecat@momonga-linux.org>
- (1.0.4-3m)
- ppc without -fno-stack-protector

* Wed Mar  2 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.4-2m)
- 1.0.4 requires libaal-devel >= 1.0.4

* Wed Mar  2 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.4-1m)
  update to 1.0.4

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.3-3m)
- enable x86_64.
- add %%{optflags} to CFLAGS (excepted -fstack-protector).

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-2m)
- add --enable-libminimal at %%configure for grub
- add CFLAGS="$CFLAGS -fno-stack-protector"
- more strict BuildPreReq: libaal-devel >= 1.0.3-2m

* Tue Dec 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.3-1m)
  update to 1.0.3

* Sat Nov 27 2004 TAKAHASHI Tamotsu <tamo>
- (1.0.0-2m)
- add defattr(-,root,root) to devel subpackage

* Fri Sep 17 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.0-1m)
  imported

* Fri Aug 29 2003 Yuey V Umanets <umka@namesys.com>
- Some cleanups and improvements inf this spec file
* Wed Aug 27 2003 David T Hollis <dhollis@davehollis.com>
- RPM package created
