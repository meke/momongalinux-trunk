%global         momorel 7

Name:           perl-Authen-SASL
Version:        2.16
Release:        %{momorel}m%{?dist}
Summary:        SASL Authentication framework
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Authen-SASL/
Source0:        http://www.cpan.org/authors/id/G/GB/GBARR/Authen-SASL-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.005
BuildRequires:  perl-Digest-HMAC
BuildRequires:  perl-Digest-MD5
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-GSSAPI
BuildRequires:  perl-Test-Simple
Requires:       perl-Digest-HMAC
Requires:       perl-Digest-MD5
Requires:       perl-GSSAPI
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
SASL is a generic mechanism for authentication used by several network
protocols. Authen::SASL provides an implementation framework that all
protocols should be able to share.

%prep
%setup -q -n Authen-SASL-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes api.txt compat_pl example_pl
%{perl_vendorlib}/Authen/SASL
%{perl_vendorlib}/Authen/SASL.pm
%{perl_vendorlib}/Authen/SASL.pod
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-2m)
- rebuild against perl-5.16.2

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-1m)
- update to 2.16

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.15-2m)
- full rebuild for mo7 release

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-1m)
- update to 2.15

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14.01-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14.01-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14.01-1m)
- update to 2.1401
- Specfile re-generated by cpanspec 1.78.

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-1m)
- update to 2.13

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12-2m)
- rebuild against rpm-4.6

* Tue Jul  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.10-2m)
- use vendor

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.10-1m)
- update to 2.10
- add BuildRequires: perl-GSSAPI

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.09-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.09-1m)
- version up to 2.09
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.06-5m)
- rebuild against perl-5.8.5
- rebuild against perl-Digest-HMAC 1.01-12m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.06-4m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.06-3m)
- revised spec for rpm 4.2.

* Mon Feb  9 2004 Kenta MURATA <muraken2@nifty.com>
- (2.06-2m)
- BuildRequires: perl-Digest-HMAC

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.06-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.02-3m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.02-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.02-1m)
- spec file was semi-autogenerated
