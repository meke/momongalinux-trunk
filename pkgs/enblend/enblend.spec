%global momorel 2
%global pregen_doc 1
%global program1name enfuse
%global majorver 4.1
%global srcname %{name}-%{program1name}
%global boost_version 1.55.0

Summary: Image Blending with Multiresolution Splines
Name: enblend
Version: 4.1.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://enblend.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{srcname}/%{srcname}-%{majorver}/%{srcname}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-no-doc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: freeglut-devel
BuildRequires: glew-devel >= 1.10.0
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: lcms-devel
BuildRequires: libXi-devel
BuildRequires: libXmu-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: plotutils-devel
%if ! %{pregen_doc}
BuildRequires: texinfo
BuildRequires: texinfo-tex
%endif
BuildRequires: zlib-devel
BuildRequires: gnuplot, gnu-free-sans-fonts
BuildRequires: vigra-devel >= 1.9.0

%description
Enblend is a tool for compositing images. Given a set of images that overlap in
some irregular way, Enblend overlays them in such a way that the seam between
the images is invisible, or at least very difficult to see. Enblend does not
line up the images for you. Use a tool like hugin to do that.

%prep
%setup -q -n %{srcname}-%{version}

%if %{pregen_doc}
%patch0 -p1 -b .no_doc
%endif

%build
# enblend-4.0 requires libboost_system when using boost-1.50+
export LDFLAGS="-lboost_system"
%configure 
%make 

%if ! %{pregen_doc}
cd doc && make %{name}.info %{program1name}.info
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

install -m 644 doc/%{name}.info -D %{buildroot}/%{_infodir}/%{name}.info
install -m 644 doc/%{program1name}.info -D %{buildroot}/%{_infodir}/%{program1name}.info

# get rid of %%{_infodir}/dir
rm -f %{buildroot}%{_infodir}/dir

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/%{program1name}.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ] ; then
  /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
  /sbin/install-info --delete %{_infodir}/%{program1name}.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README* VERSION
%{_bindir}/%{name}
%{_bindir}/%{program1name}
%{_infodir}/%{name}.info*
%{_infodir}/%{program1name}.info*
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{program1name}.1*

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.2-2m)
- rebuild against boost-1.55.0

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.2-1m)
- version 4.1.2
- use pregenerated info for the moment

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1-3m)
- rebuild against glew-1.10.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-2m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-1m)
- update to 4.1
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-16m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-15m)
- rebuild for glew-1.9.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-14m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-13m)
- fix build failure with boost-1.50.0
- add BuildRequires

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-12m)
- rebuild against libtiff-4.0.1

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-11m)
- rebuild against glew-1.7.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-10m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (4.0-9m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-8m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-7m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-6m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-5m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-4m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-3m)
- full rebuild for mo7 release

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-2m)
- rebuild against boost-1.43.0

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-1m)
- version 4.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-2m)
- rebuild against libjpeg-8a

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2-1m)
- initial package for hugin-2009.2.0
