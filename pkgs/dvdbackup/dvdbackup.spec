%global momorel 11

Summary: DVD backup tool
Name: dvdbackup
Version: 0.30
License: GPL
Release: %{momorel}m%{?dist}
Group:  Applications/System
Source0: http://download.berlios.de/lxdvdrip/%{name}-%{version}.tgz
Patch0: dvdbackup-fix_div_by_zero.patch
Patch1: dvdbackup-0.1.1-debian-FPE.patch
URL: http://dvd-create.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libdvdread-devel >= 4.1.2

%description
dvdbackup is a program that enables you to mirror your DVD-Video to
harddisk. The mirror can then be turned into a DVD-Video image with
help of mkisofs. The image can be burned to a DVD-/+R(W) disk and
played in your stand alone DVD-Video player. Dvdbackup is not just a
backup program but can also give you information about the DVD, or e.g
enable you to just backup the main feature of the DVD.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .fix_div_by_zero
%patch1 -p1 -b .FPE

%build
gcc %{optflags} -g -D_LARGEFILE_SOURCE -DLARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 \
      -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -D_REENTRANT -o dvdbackup -Wall \
      -I%{_includedir}/dvdread -L%{_libdir} -ldvdread dvdbackup.c

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%{__mkdir_p} -m 755 %{buildroot}%{_bindir}
%{__install} -m 755 dvdbackup %{buildroot}%{_bindir}/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-, root,root)
%doc COPYING README
%{_bindir}/dvdbackup

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.30-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.30-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.30-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.30-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-6m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.30-5m)
- rebuild against libdvdread-4.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.30-4m)
- rebuild against gcc43

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.30-3m)
- apply dvdbackup-0.1.1-debian-FPE.patch to fix the "floating point exception" error when a disc contains only one titleset. (original in Debian and imported from Gentoo)

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.30-2m)
- No NoSource

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.30-1m)
- version 0.30

* Wed Oct  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-5m)
- dynamic link again

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.1.1-4m)
- enable x86_64.

* Tue Jan 11 2005 Toru Hoshina <t@momonga-linux.org>
- (0.1.1-3m)
- when attempt to link libdvdread.so, unexplainable errer occurred.
- force static link.

* Sun Nov  9 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.1.1-2m)
- Nosource.

* Sun Nov  9 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.1.1-1m)
- Initial specfile.
