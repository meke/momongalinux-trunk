%global momorel 1
%define tarball xf86-video-modesetting
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir %{moduledir}/drivers
%global xorgurl http://xorg.freedesktop.org/releases/individual

Summary:   Xorg X11 modesetting video driver
Name:      xorg-x11-drv-modesetting
Version:   0.9.0
Release:   %{momorel}m%{?dist}
URL:       http://www.x.org
License:   MIT
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2
NoSource: 0

ExcludeArch: s390 s390x

BuildRequires: libtool automake autoconf
BuildRequires: xorg-x11-server-devel >= 1.13.0
BuildRequires: libdrm-devel >= 2.4.37
BuildRequires: mesa-libGL-devel >= 9.0
BuildRequires: kernel-headers >= 3.0
BuildRequires: systemd-devel >= 187

Requires:  hwdata
Requires:  xorg-x11-server-Xorg >= 1.13.0
Requires:  libdrm >= 2.4.37

%description 
X.Org X11 modesetting video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
autoreconf -v --install
%configure --disable-static --with-kms=yes
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -regex ".*\.la$" | xargs rm -f --

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/modesetting_drv.so
%{_mandir}/man4/modesetting.4*

%changelog
* Sat Jun 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-1m)
- update 0.9.0

* Mon Jul 15 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-1m)
- Initial Commit 
