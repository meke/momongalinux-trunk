%global momorel 5

Summary:	Remote control utility for Canon cameras
Name:		multican
Version:	0.0.5
Release:	%{momorel}m%{?dist}
Source0:	http://dl.sourceforge.net/sourceforge/multican/%{name}-%{version}.tar.gz
URL:		http://multican.sourceforge.net/
License:	GPLv2
Group:		Applications/Multimedia
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	libusb-devel >= 0.1.12
Requires:	libusb >= 0.1.12

%description
Multican is Canon EOS class USB remote control utility for 300D, 350D,
30D, 20D and 5D. It is possible to control more of Canon cameras in
the same time up to six cameras attached. Multican allows scripted
remote control of multiple cameras.

%package devel
Summary: Development library for multican
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files, libraries needed to develop
applications that use multican library to access Canon cameras.

%prep
%setup -q

%build
make CFLAGS="$RPM_OPT_FLAGS -fPIC"

%install
rm -rf $RPM_BUILD_ROOT
make	BINDIR="$RPM_BUILD_ROOT%{_bindir}" \
	LIBDIR="$RPM_BUILD_ROOT%{_libdir}" \
	INCLDIR="$RPM_BUILD_ROOT%{_includedir}" \
	install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING README
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%doc TODO ChangeLog
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.5-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.5-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Aug 02 2008 Jindrich Novy <jnovy@redhat.com> 0.0.5-5
- fix source URL

* Mon Feb 25 2008 Jindrich Novy <jnovy@redhat.com> 0.0.5-4
- manual rebuild because of gcc-4.3 (#434191)

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.0.5-3
- Autorebuild for GCC 4.3

* Thu Aug 23 2007 Jindrich Novy <jnovy@redhat.com> 0.0.5-2
- update License
- rebuild for BuildID

* Tue Feb 20 2007 Jindrich Novy <jnovy@redhat.com> 0.0.5-1
- update to 0.0.5
- new devel package now provides shared library and headers

* Sun Dec 17 2006 Jindrich Novy <jnovy@redhat.com> 0.0.4-2
- rebuild to avoid EVR problems with FC5 multican

* Tue Nov 07 2006 Jindrich Novy <jnovy@redhat.com> 0.0.4-1
- initial build
