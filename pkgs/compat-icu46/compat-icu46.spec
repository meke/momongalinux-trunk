%global momorel 1

Name:      compat-icu46
Version:   4.6
Release:   %{momorel}m%{?dist}
Summary:   International Components for Unicode
Group:     Development/Tools
License:   MIT and "UCD" and "Public Domain"
URL:       http://www.icu-project.org/
Source0:   http://download.icu-project.org/files/icu4c/4.6/icu4c-4_6-src.tgz
NoSource:  0
Source1:   icu-config
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen, autoconf

Patch1: icu.8198.revert.icu5431.patch
Patch2: icu.8320.freeserif.crash.patch
Patch3: icu.7601.Indic-ccmp.patch
Patch4: icu.8984.CVE-2011-4599.patch

%description
The International Components for Unicode (ICU) libraries provide
robust and full-featured Unicode services on a wide variety of
platforms. ICU supports the most current version of the Unicode
standard, and they provide support for supplementary Unicode
characters (needed for GB 18030 repertoire support).
As computing environments become more heterogeneous, software
portability becomes more important. ICU lets you produce the same
results across all the various platforms you support, without
sacrificing performance. It offers great flexibility to extend and
customize the supplied services.

%package  devel
Summary:  Development files for International Components for Unicode
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Includes and definitions for developing with icu.

%prep
%setup -q -n icu
%patch1 -p2 -R -b .icu8198.revert.icu5431.patch
%patch2 -p1 -b .icu8320.freeserif.crash.patch
%patch3 -p1 -b .icu7601.Indic-ccmp.patch
%patch4 -p1 -b .icu8984.CVE-2011-4599.patch

%build
cd source
autoconf
CFLAGS='%optflags -fno-strict-aliasing'
CXXFLAGS='%optflags -fno-strict-aliasing'
%configure --with-data-packaging=library --disable-samples
#rhbz#225896
sed -i 's|-nodefaultlibs -nostdlib||' config/mh-linux
#rhbz#681491
sed -i 's|^LIBS =.*|LIBS = -L../lib -licuuc -lpthread -lm|' i18n/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -licui18n -lc|' io/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -lc|' layout/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -licule -lc|' layoutex/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../../lib -licutu -licuuc -lc|' tools/ctestfw/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../../lib -licui18n -licuuc -lpthread -lc|' tools/toolutil/Makefile


make # %{?_smp_mflags} # -j(X>1) may "break" man pages as of 3.2, b.f.u #2357
make doc

%install
rm -rf %{buildroot} source/__docs
make -C source install DESTDIR=%{buildroot}
chmod +x $RPM_BUILD_ROOT%{_libdir}/*.so.*

rm -rf $RPM_BUILD_ROOT/%{_bindir} 
rm -rf $RPM_BUILD_ROOT/%{_sbindir} 
rm -rf $RPM_BUILD_ROOT/%{_mandir} 
rm -rf $RPM_BUILD_ROOT/%{_libdir}/*.so
rm -rf $RPM_BUILD_ROOT/%{_libdir}/pkgconfig
rm -rf $RPM_BUILD_ROOT/%{_libdir}/icu

mv $RPM_BUILD_ROOT/%{_datadir}/{icu,compat-icu46}

mkdir $RPM_BUILD_ROOT/%{_includedir}/icu46
mv $RPM_BUILD_ROOT/%{_includedir}/{layout,unicode} $RPM_BUILD_ROOT/%{_includedir}/icu46/

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc license.html readme.html
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/icu46/layout
%{_includedir}/icu46/unicode
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/%{version}
%{_datadir}/%{name}/%{version}/install-sh
%{_datadir}/%{name}/%{version}/mkinstalldirs
%{_datadir}/%{name}/%{version}/config
%doc %{_datadir}/%{name}/%{version}/license.html

%changelog
* Sun Mar 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6-1m)
- old icu-4.6 renamed "compat-icu46"

* Mon Jan 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6-2m)
- [SECURITY] CVE-2011-4599
- import patches (include security patch) from Fedora

* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.6-1m)
- version up 4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-2m)
- full rebuild for mo7 release

* Tue Apr 27 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1 and sync Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.1-2m)
- fix build failure with doxygen 1.6.1

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-1m)
- [SECURITY] CVE-2009-0153
- sync with Fedora 11 (4.0.1-3)

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- %%global _default_patch_fuzz 2

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-1m)
- [SECURITY] CVE-2008-1036
- update to 4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.1-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-4m)
- %%NoSource -> NoSource

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-3m)
- %%NoSource -> NoSource

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6-2m)
- Provides and Obsoletes icu-locales for upgrading

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-1m)
- update to 3.6

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.4.1-1m)
- update to 3.4.1
- add gnustack patch (from FC)
- add gcc41 patch (from FC)

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.2-4m)
- enable big-endian arch.

* Mon Aug  8 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (3.2-3m)
- %defattr(-,root,root)

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.2-2m)
- revise %%post and %%preun

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.2-1m)
- initial import to Momonga
