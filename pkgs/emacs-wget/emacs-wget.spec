%global momorel 34
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: An interface program for GNU wget on Emacs
Name: emacs-wget
Version: 0.5.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://pop-club.hp.infoseek.co.jp/emacs/emacs-wget/emacs-wget-%{version}.tar.gz 
URL: http://pop-club.hp.infoseek.co.jp/emacs/emacs-wget/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires: wget
Obsoletes: wget-emacs
Obsoletes: wget-xemacs

Obsoletes: elisp-wget
Provides: elisp-wget

%description
Emacs-wget is an interface program for GNU wget on Emacs.

%prep
%setup -q -n emacs-wget-%{version}

%build
make EMACS=emacs \
     prefix=%{_prefix} \
     datadir=%{_datadir} \
     lispdir=%{e_sitedir}/emacs-wget

%install
rm -rf %{buildroot}

make EMACS=emacs \
     prefix=%{buildroot}%{_prefix} \
     datadir=%{buildroot}%{_datadir} \
     lispdir=%{buildroot}%{e_sitedir}/emacs-wget \
     install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README README.ja USAGE USAGE.ja
%{e_sitedir}/emacs-wget

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-34m)
- add source

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-33m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-32m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.0-31m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-30m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-29m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-27m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-26m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-25m)
- merge wget-emacs to elisp-wget
- kill wget-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-24m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-23m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-22m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-21m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-20m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-19m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-18m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-17m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-16m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-15m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-14m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-13m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-11m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-10m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-2m)
- rebuild against emacs-22.0.90

* Mon Aug 28 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-1m)
- initial package
