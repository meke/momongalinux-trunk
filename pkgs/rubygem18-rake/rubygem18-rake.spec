# Generated from rake-0.9.2.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname rake

%global gemdir %(ruby18 -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.8

Summary: Ruby based make-like utility
Name: rubygem18-%{gemname}
Version: 0.9.2.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rake.rubyforge.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(rubygems) >= 1.3.2
Requires: ruby >= 1.8.6
BuildRequires: ruby(rubygems) >= 1.3.2
BuildRequires: ruby >= 1.8.6
BuildArch: noarch
Provides: rubygem18(%{gemname}) = %{version}
Provides: %{name}-doc
Obsoletes: %{name}-doc
Provides: rubygem-%{gemname}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}
Obsoletes: rubygem-%{gemname}-doc

%description
Rake is a Make-like program implemented in Ruby. Tasks and dependencies
arespecified in standard Ruby syntax.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem18 install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

mv %{buildroot}%{_bindir}/rake{,18}

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/rake18
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/MIT-LICENSE
%doc %{geminstdir}/TODO
%doc %{geminstdir}/CHANGES
%doc %{geminstdir}/doc/command_line_usage.rdoc
%doc %{geminstdir}/doc/glossary.rdoc
%doc %{geminstdir}/doc/proto_rake.rdoc
%doc %{geminstdir}/doc/rakefile.rdoc
%doc %{geminstdir}/doc/rational.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.4.14.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.4.15.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.5.0.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.5.3.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.5.4.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.6.0.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.7.0.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.7.1.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.7.2.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.7.3.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.0.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.2.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.3.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.4.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.5.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.6.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.8.7.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.9.0.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.9.1.rdoc
%doc %{geminstdir}/doc/release_notes/rake-0.9.2.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sat Nov 05 2011  <meke@pikachu>
- (0.9.2.2-2m)
- cleanup spec

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.2-1m)
- update 0.9.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-5m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-4m)
- rebuild against rubygems18-1.3.7-2m

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.7-3m)
- rename to rubygem-rake to rubygem18-rake

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Sat Mar 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-1m)
- version up 0.8.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-4m)
- rebuild against rpm-4.6

* Sat Aug 30 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.1-3m)
- remove Provides and Obsoletes

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.1-1m)
- version up 0.8.1

* Fri Jan 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-2m)
- Provides and Obsoletes rake for smooth upgrading
- fix "File listed twice"

* Fri Jan 11 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.3-1m)
- import from dlutter repo

* Thu Aug 23 2007 David Lutterkort <dlutter@redhat.com> - 0.7.3-2
- Fix license tag
- Remove bogus shebangs in lib/ and test/

* Mon Jun 18 2007 David Lutterkort <dlutter@redhat.com> - 0.7.3-1
- Initial package
