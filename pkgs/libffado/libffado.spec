%global momorel 2

%global date 20110426
%global svnrev 1983
%global appname ffado

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Summary: Firewire audio drivers for JACK
Name: libffado
Version: 2.1.0
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPLv2+ and GPLv3
Group: System Environment/Libraries
URL: http://www.ffado.org/
Source0: %{name}-%{version}-svn%{svnrev}.tar.bz2
Patch1: libffado-dso-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick
BuildRequires: PyQt4-devel
BuildRequires: alsa-lib-devel
BuildRequires: coreutils
BuildRequires: dbus-c++-devel
BuildRequires: dbus-devel
BuildRequires: dbus-python-devel
BuildRequires: glibmm-devel
BuildRequires: graphviz
BuildRequires: libconfig-devel
BuildRequires: libiec61883-devel
BuildRequires: libraw1394-devel
BuildRequires: libxml++-devel
BuildRequires: pkgconfig
BuildRequires: python-devel
BuildRequires: scons

%description
The FFADO library provides a generic, open-source solution for the
support of FireWire based audio devices for the Linux platform. It is the
successor of the FreeBoB project. Currently this library is used by the 
firewire backends of the jack audio connection kit sound server 
(jackit package). This backend provides audio and midi support,
and is available in both jack1 and jack2.

%package -n %{appname}
Summary: Firewire audio driver applications and utilities
Group: Applications/Multimedia
Requires(post): coreutils
Requires(postun): coreutils
Requires(postun): gtk2
Requires(posttrans): gtk2
Requires: %{name} = %{version}-%{release}
Requires: PyQt4
Requires: dbus
Requires: dbus-python

%description -n %{appname}
Configuration utilities for the FFADO firewire drivers.

%package devel
Summary: Header files and development libraries from libffado
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libffado.

%prep
%setup -q
%patch1 -p1 -b .fix-dso~

%build
scons %{?_smp_mflags} \
      COMPILE_FLAGS="%{optflags} -ffast-math" \
      PREFIX=%{_prefix} \
      LIBDIR=%{_libdir}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
scons install DESTDIR=%{buildroot}

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 support/xdg/%{appname}.org-%{appname}mixer.desktop %{buildroot}%{_datadir}/applications/

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64}/apps
convert -scale 16x16 support/xdg/hi64-apps-%{appname}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{appname}.png
convert -scale 32x32 support/xdg/hi64-apps-%{appname}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{appname}.png
convert -scale 48x48 support/xdg/hi64-apps-%{appname}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{appname}.png
install -m 644 support/xdg/hi64-apps-%{appname}.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{appname}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{appname}.png %{buildroot}%{_datadir}/pixmaps/%{appname}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post -n %{appname}
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun -n %{appname}
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans -n %{appname}
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root)
%{_libdir}/%{name}.so.*

%files -n %{appname}
%defattr(-,root,root)
%doc AUTHORS ChangeLog LICENSE* NEWS README TODO
%{_bindir}/*
%{python_sitelib}/%{appname}
%{_datadir}/applications/%{appname}.org-%{appname}mixer.desktop
%{_datadir}/dbus-1/services/org.ffado.Control.service
%{_datadir}/icons/hicolor/*/apps/%{appname}.png
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{appname}.png

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so

%changelog
* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-0.20110426.2m)
- fix build failure
-- import patch from fedora

* Mon Aug  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-0.20110426.1m)
- initial package for jack-1.9.7
