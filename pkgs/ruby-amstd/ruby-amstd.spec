%define rbname amstd

Summary: Aoki Minero's STandard library for Ruby
#'
Name: ruby-%{rbname}

%global momorel 14

Version: 2.0.0
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: LGPL
URL: http://www.loveruby.net/ja/prog/amstd.html

Source0: http://www.loveruby.net/archive/%{rbname}/%{rbname}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby

Requires: ruby >= 1.4

%description
Some libraries for the Object Oriented Scripting Language: Ruby.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n %{rbname}-%{version}

%build
ruby install.rb config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_libdir} \
    --so-dir=%{buildroot}%{ruby_archdir} \
    --ruby-path=%{_bindir}/ruby
ruby install.rb setup

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby install.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LGPL README.* manual.rd.ja
%{ruby_libdir}/amstd

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.0-14m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-11m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.0-10m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-5m)
- %%NoSource -> NoSource

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (2.0.0-4m)
- merge from ruby-1_8-branch.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (2.0.0-3m)
- rebuild against ruby-1.8.0.

* Tue Jan  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.0.0-2k)

* Fri Oct 19 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.10.0-2k)
- modify URI and License

* Sat Apr 7 2001 SAKUMA Junichi <fk5j-skm@asahi-net.or.jp>
- (1.9.7-5k)
- version 1.9.7

* Fri Dec  8 2000 AYUHANA Tomonori <l@kondara.org>
- (1.9.6-3k)
- version 1.9.6

* Sun Nov 19 2000 AYUHANA Tomonori <l@kondara.org>
- (1.9.5-1k)
- version 1.9.5
- add -q at %setup
- fix Source URI

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- it should be suitable to both ruby 1.4.x and 1.6.x.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Sat Jun 10 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- Upstream upgrade.

* Sun Apr 23 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot, Summary )

* Wed Apr 19 2000 Toru Hoshina <t@kondara.org>
- No architecture dependency.

* Sat Apr  8 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- First release for Kondara.

