%global momorel 11

Name:		libflashsupport
Summary: 	Optional Library Interfaces for Adobe Flash Player
Version:	000
Release:	0.5.svn20070904.%{momorel}m%{?dist}
License:	BSD
Group:		Applications/Internet
Source0:	libflashsupport-pulse-000.svn20070904.tar.gz
Patch0:		libflashsupport-disable-v4l.patch
URL:		http://pulseaudio.org
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libtool
# Fedora package name
#BuildRequires:	pulseaudio-lib-devel
BuildRequires:	pulseaudio-libs-devel
BuildRequires:  openssl-devel >= 1.0.0

%description
Optional Library Interfaces for Adobe Flash Player
- pulseaudio
- OpenSSL

If more interfaces are desired, please suggest patches in Fedora Bugzilla.

%prep
%setup -q -n libflashsupport-pulse-000
%patch0 -p1 -b .disable-v4l~

%build
%configure --disable-rpath
make LIBTOOL=/usr/bin/libtool

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
rm -rf $RPM_BUILD_ROOT%{_libdir}/*.a
rm -rf $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_libdir}/libflashsupport.so

%changelog
* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (000-0.5.svn20070904.11m)
- disable v4l

* Sat Apr 23 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (000-0.5.svn20070904.10m)
- Patch0: libflashsupport-disable-v4l.patch
- if use with kernel-2.6.38 or later enable kernel2638

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (000-0.5.svn20070904.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (000-0.5.svn20070904.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (000-0.5.svn20070904.7m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (000-0.5.svn20070904.6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (000-0.5.svn20070904.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (000-0.5.svn20070904.4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (000-0.5.svn20070904.3m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (000-0.5.svn20070904.2m)
- rebuild against openssl-0.9.8h-1m

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (000-0.5.svn20070904.1m)
- import from Fedora to Momonga
- use BuildRequires: pulseaudio-libs-devel in Momonga,
  instead of pulseaudio-lib-devel 

* Mon Apr 07 2008 Warren Togami <wtogami@redhat.com> 000-0.5.svn20070904
- undo deps from x86_64 to i386 because it upset Jesse
  For now this means x86-64 users need to manually install libflashsupport.i386
  and nspluginwrapper.i386 in order to use Flash.
  We need to discuss what to do for Fedora 9 release during FESCO.

* Sat Apr 05 2008 Warren Togami <wtogami@redhat.com> 000-0.4.svn20070904
- x86_64 requires i386 of itself and nspluginwrapper.i386 through file dependencies.
  This really sucks but we currently have no other way of doing cross-arch dependencies.

* Mon Feb 11 2008 Warren Togami <wtogami@redhat.com> 000-0.3.svn20070904
- rebuild with gcc-4.3

* Wed Dec  5 2007 Matthias Clasen <mclasen@redhat.com> 000-0.2.svn20070904
- Rebuild against new openssl

* Mon Sep 24 2007 Warren Togami <wtogami@redhat.com> 000-0.1.svn20070904
- Initial package
