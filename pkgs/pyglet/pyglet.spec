%global momorel 6
%global pyver 2.7

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}


Name:           pyglet
Version:        1.1.4
Release:        %{momorel}m%{?dist}
Summary:        pyglet provides an object-oriented programming interface for developing games and other visually-rich applications.
Group:          Development/Languages
License:        BSD
URL:            http://code.google.com/p/pyglet/
Source0:	http://pyglet.googlecode.com/files/%{name}-%{version}.tar.gz
Nosource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel

%description
pyglet provides an object-oriented programming interface for developing games and other visually-rich applications

%prep
%setup -q -n %{name}-%{version}

%install
python setup.py install \
    --root=%{buildroot} \
    --prefix=%{_prefix} \
    --install-lib=%{python_sitearch}

%files
%defattr(-, root, root, 0755)
%{python_sitearch}/%{name}-%{version}-py%{pyver}.egg-info
%{python_sitearch}/%{name}


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-3m)
- full rebuild for mo7 release

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-2m)
- enable to build on x86_64

* Wed Mar 11 2010 Daniel McLellan <daniel.mclellan@gmail.com> 1.1.4
- (1.1.4-1m)
- first spec file
