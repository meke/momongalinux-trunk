%global momorel 4

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define with_maven 1

Name:           maven-jxr
Version:        2.1
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Source cross referencing tool
License:        "ASL 2.0"
Group:          Development/Tools
URL:            http://maven.apache.org/doxia/

# svn export http://svn.apache.org/repos/asf/maven/jxr/tags/jxr-2.1
#   maven-jxr-%{version}
# tar cjf maven-jxr-%{version}.tar.gz maven-jxr-%{version}
Source0:        %{name}-%{version}.tar.bz2
Source1:        %{name}-jpp-depmap.xml
# Generated with mvn ant:ant and edited to match our JAR
# names/locations
Source2:        %{name}-%{version}-build.xml
Source3:        %{name}-%{version}-mavenbuild.xml

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  jpackage-utils >= 0:1.7.2
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-surefire
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-site
BuildRequires:  maven2-plugin-plugin
BuildRequires:  maven-doxia-sitetools
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  maven-shared-plugin-testing-harness
%else
BuildRequires:  ant, ant-nodeps
%endif
BuildRequires:  junit >= 3.8.2
BuildRequires:  jakarta-commons-collections >= 3.1
BuildRequires:  oro >= 2.0.8
BuildRequires:  plexus-utils >= 1.2
BuildRequires:  velocity >= 1.4

Requires:       junit >= 3.8.2
Requires:       jakarta-commons-collections >= 3.1
Requires:       oro >= 2.0.8
Requires:       plexus-utils >= 1.2
Requires:       velocity >= 1.4

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%description
Maven JXR is a source cross referencing tool.

%if %{with_maven}
%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
API documentation for %{name}.

%package -n maven-plugin-jxr
Summary:        Maven plugin for JXR
Group:          Development/Tools

%description -n maven-plugin-jxr
Maven plugin for JXR.
%endif

%prep
%setup -q
%if ! %{with_maven}
cp -p %{SOURCE2} maven-jxr/build.xml
cp -p %{SOURCE3} maven-jxr/maven-build.xml
%endif

%build

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

# The test failures seem to have something to do with:
# http://jira.codehaus.org/browse/MCHANGES-88
# We can investigate when we upgrade to 2.2.x to see if they still occur.
%if %{with_maven}
    mvn-jpp \
        -e \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        -Dmaven2-jpp.depmap.file=%{SOURCE1} \
        -Dmaven.test.failure.ignore=true \
        install javadoc:javadoc
%else

# Only build maven-jxr and not the plugin so we can bootstrap maven
# itself using maven-jxr
cd maven-jxr
mkdir lib
build-jar-repository -s -p lib/ \
                        commons-collections \
                        junit \
                        oro \
                        plexus/utils \
                        velocity

ant -Dmaven.mode.offline=true
%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars/poms
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT/%{_datadir}/maven2/poms

install -pm 644 maven-jxr/target/%{name}-%{version}.jar \
                $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
%add_to_maven_depmap org.apache.maven.jxr jxr %{version} JPP %{name}-parent
%add_to_maven_depmap org.apache.maven maven-jxr %{version} JPP %{name}

install -pm 644 pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}-parent.pom
install -pm 644 maven-jxr/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}.pom

%if %{with_maven}
# maven-jxr plugin
%add_to_maven_depmap org.apache.maven.plugins maven-jxr-plugin %{version} JPP maven-plugin-jxr
install -pm 644 maven-jxr-plugin/target/maven-jxr-plugin-%{version}.jar \
                $RPM_BUILD_ROOT%{_javadir}/maven-plugin-jxr-%{version}.jar
                
install -pm 644 maven-jxr-plugin/pom.xml \
        $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-maven-plugin-jxr.pom

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

cp -pr maven-jxr/target/site/apidocs/* \
                $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/

ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}
%endif

(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%if %{with_maven}
%post -n maven-plugin-jxr
%update_maven_depmap

%postun -n maven-plugin-jxr
%update_maven_depmap
%endif

%files
%defattr(-,root,root,-)
%{_javadir}/%{name}*.jar
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/%{name}

%if %{with_maven}
%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%files -n maven-plugin-jxr
%defattr(-,root,root,-)
%{_javadir}/maven-plugin-jxr*.jar
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-2m)
- full rebuild for mo7 release

* Mon Mar  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-1m)
- sync with Fedora 13 (0:2.1-6)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2jpp.5.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2jpp.5.4m)
- rebuild against rpm-4.6

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2jpp5.3m)
- fix file conflicts with jpackage-utils

* Wed May 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2jpp.2m)
- release %%{_mavendepmapfragdir} and %%{_javadir} provided by jpackage-utils

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2jpp.1m)
- import from Fedora to Momonga
- tmp %%define with_maven 0

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.0-2jpp.5
- Rebuild

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 1.0-2jpp.4
- Build without maven
- ExcludeArch ppc64

* Fri Aug 31 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-2jpp.3
- Build without maven (for initial ppc build)

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-2jpp.2
- Build with maven

* Tue Feb 27 2007 Tania Bento <tbento@redhat.com> 0:1.0-2jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Removed period from %%Summary.
- Removed %%Vendor.
- Removed %%Distribution.
- Removed %%post and %%postun sections for javadoc. 
- Added gcj support option.
- Fixed instructions on how to generate source drops.

* Tue Oct 17 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-2jpp
- Update for maven 9jpp.

* Fri Jun 16 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-1jpp
- Initial build
