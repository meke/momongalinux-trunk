%global momorel 2
%global	baseurl http://dl.sourceforge.jp/mix-mplus-ipa/

%global priority	65-4
%global fontname	migmix
%global	archivename	migmix
%global	date            20130617
%global	fontconf	%{priority}-%{fontname}
%global	common_desc	\
Meguri fonts are modified IPA and M+ fonts for Japanese.

Name:		%{fontname}-fonts
Version:	0.0.%{date}
Release:	%{momorel}m%{?dist}
Summary:	Modified IPA and M+ fonts

License:	"IPA" and "mplus"
Group:		User Interface/X
URL:		http://mix-mplus-ipa.sourceforge.jp/
Source0:	%{baseurl}/59021/migmix-1p-%{date}.zip
Source1:	%{baseurl}/59021/migmix-2p-%{date}.zip
Source2:	%{baseurl}/59021/migmix-1m-%{date}.zip
Source3:	%{baseurl}/59021/migmix-2m-%{date}.zip
NoSource:       0
NoSource:       1
NoSource:       2
NoSource:       3
Source10:       %{fontname}-fontconfig-gothic.conf
Source11:       %{fontname}-fontconfig-pgothic.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	fontpackages-devel
BuildRequires:	fontforge

Requires:	%{name}-common = %{version}-%{release}
%description
%common_desc

This package contains %{fontname} fonts.

%package	common
Summary:	Common files of meguri
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description	common
%common_desc

This package consists of files used by other %{name} packages.

%package -n	%{fontname}-p-fonts
Summary:	MigMix proportional font
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-p-fonts
%common_desc

This package contains %{fontname} proportional fonts.

%prep
%setup -q -n %{archivename}-1p-%{date} -b 1 -b 2 -b 3


%build

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}

pushd ../
for i in `ls -d migmix-*`; do
  pushd $i
    for k in `ls *.ttf`;do
      install -m 0644 $k %{buildroot}%{_fontdir}/
    done
  popd
done
popd

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir}
install -m 0755 -d %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE10} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-gothic.conf
install -m 0644 -p %{SOURCE11} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pgothic.conf

for fconf in %{fontconf}-gothic.conf %{fontconf}-pgothic.conf; do
    ln -s %{_fontconfig_templatedir}/$fconf %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -rf %{buildroot}

%files common
%defattr(-,root,root,-)
%doc migmix-README.txt
%dir %{_fontdir}
%{_fontdir}/migmix-1m-bold.ttf
%{_fontdir}/migmix-1p-bold.ttf
%{_fontdir}/migmix-2m-bold.ttf
%{_fontdir}/migmix-2m-regular.ttf
%{_fontdir}/migmix-2p-bold.ttf
%{_fontdir}/migmix-2p-regular.ttf

%_font_pkg -f %{fontconf}-gothic.conf migmix-1m-regular.ttf

%_font_pkg -n p -f %{fontconf}-pgothic.conf migmix-1p-regular.ttf

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.20130617-2m)
- fix Fontconfig warning

* Thu Jan 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.20130617-1m)
- update 20130617

* Thu May  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20130430-1m)
- update 20130430

* Wed Oct 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20121030-1m)
- update 20121030

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.20120411-3m)
- fix %%files to avoid conflicting
- what a strange packaging, make sure all

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20120411-2m)
- update 20120411-2
-- older version include licence issue
-- http://d.hatena.ne.jp/itouhiro/20120607 

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20120411-1m)
- update 20120411

* Mon Jan 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20111002-1m)
- Initial Commit Momonga Linux

