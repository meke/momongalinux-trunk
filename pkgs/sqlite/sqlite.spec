%global momorel 1

# bcond default logic is nicely backwards...
%bcond_without tcl
%bcond_with static
%bcond_without check

%global realver 3080500
%global rpmver 3.8.5
#%%define rpmver %(echo %{realver}|sed -e "s/00//g" -e "s/0/./g")

Summary: Library that implements an embeddable SQL database engine
Name: sqlite
Version: %{rpmver}
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Applications/Databases
URL: http://www.sqlite.org/
Source0: http://www.sqlite.org/2014/sqlite-src-%{realver}.zip
NoSource: 0
Source1: http://www.sqlite.org/2014/sqlite-doc-%{realver}.zip
NoSource: 1
# Support a system-wide lemon template
Patch1: sqlite-3.6.23-lemon-system-template.patch
# Shut up stupid tests depending on system settings of allowed open fd's
Patch2: sqlite-3.7.7.1-stupid-openfiles-test.patch
# Shut up pagecache overflow test whose expected result depends on compile
# options and whatnot. Dunno why this started failing in 3.7.10 but
# doesn't seem particularly critical...
Patch3: sqlite-3.7.10-pagecache-overflow-test.patch
# sqlite >= 3.7.10 is buggy if malloc_usable_size() is detected, disable it:
# https://bugzilla.redhat.com/show_bug.cgi?id=801981
# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=665363
Patch4: sqlite-3.7.11-no-malloc-usable-size.patch

# momonga's patch
Patch100: sqlite-3.7.2-pkgconfig.patch
BuildRequires: ncurses-devel readline-devel glibc-devel
# libdl patch needs
BuildRequires: autoconf
%if %{with tcl}
BuildRequires: /usr/bin/tclsh
BuildRequires: tcl-devel
%{!?tcl_version: %global tcl_version 8.5}
%{!?tcl_sitearch: %global tcl_sitearch %{_libdir}/tcl%{tcl_version}}
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
SQLite is a C library that implements an SQL database engine. A large
subset of SQL92 is supported. A complete database is stored in a
single disk file. The API is designed for convenience and ease of use.
Applications that link against SQLite can enjoy the power and
flexibility of an SQL database without the administrative hassles of
supporting a separate database server.  Version 2 and version 3 binaries
are named to permit each to be installed on a single host

%package devel
Summary: Development tools for the sqlite3 embeddable SQL database engine
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains the header files and development documentation 
for %{name}. If you like to develop programs using %{name}, you will need 
to install %{name}-devel.

%package doc
Summary: Documentation for sqlite
Group: Documentation

%description doc
This package contains most of the static HTML files that comprise the
www.sqlite.org website, including all of the SQL Syntax and the 
C/C++ interface specs and other miscellaneous documentation.

%package -n lemon
Summary: A parser generator
Group: Development/Tools

%description -n lemon
Lemon is an LALR(1) parser generator for C or C++. It does the same
job as bison and yacc. But lemon is not another bison or yacc
clone. It uses a different grammar syntax which is designed to reduce
the number of coding errors. Lemon also uses a more sophisticated
parsing engine that is faster than yacc and bison and which is both
reentrant and thread-safe. Furthermore, Lemon implements features
that can be used to eliminate resource leaks, making is suitable for
use in long-running programs such as graphical user interfaces or
embedded controllers.

%if %{with tcl}
%package tcl
Summary: Tcl module for the sqlite3 embeddable SQL database engine
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: tcl(abi) = %{tcl_version}

%description tcl
This package contains the tcl modules for %{name}.
%endif

%prep
%setup -q -a1 -n %{name}-src-%{realver}
%patch1 -p1 -b .lemon-system-template
%patch2 -p1 -b .stupid-openfiles-test
%patch3 -p1 -b .pagecache-overflow-test
%patch4 -p1 -b .no-malloc-usable-size

# momonga's patch
%patch100 -p1 -b .pkgconfig~

autoconf
%build
export CFLAGS="$RPM_OPT_FLAGS -DSQLITE_ENABLE_COLUMN_METADATA=1 -DSQLITE_DISABLE_DIRSYNC=1 -DSQLITE_ENABLE_FTS3=3 -DSQLITE_ENABLE_RTREE=1 -DSQLITE_SECURE_DELETE=1 -DSQLITE_ENABLE_UNLOCK_NOTIFY=1 -Wall -fno-strict-aliasing"
%configure %{!?with_tcl:--disable-tcl} \
           --enable-threadsafe \
           --enable-threads-override-locks \
           --enable-load-extension \
           %{?with_tcl:TCLLIBDIR=%{tcl_sitearch}/sqlite3}


# rpath removal
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%make DESTDIR=%{buildroot} install 

install -D -m0644 sqlite3.1 %{buildroot}/%{_mandir}/man1/sqlite3.1
install -D -m0755 lemon %{buildroot}/%{_bindir}/lemon
install -D -m0644 tool/lempar.c %{buildroot}/%{_datadir}/lemon/lempar.c

%if %{with tcl}
# fix up permissions to enable dep extraction
chmod 0755 %{buildroot}/%{tcl_sitearch}/sqlite3/*.so
# chmod 0644 %{name}-doc-%{realver}/search
%endif

%if ! %{with static}
rm -f %{buildroot}/%{_libdir}/*.{la,a}
%endif
 
%if %{with check}
%check
make test || :
%endif

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc README.md
%{_bindir}/sqlite3
%{_libdir}/*.so.*
%{_mandir}/man?/*

%files devel
%defattr(-, root, root)
%{_includedir}/*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%if %{with static}
%{_libdir}/*.a
%exclude %{_libdir}/*.la
%endif

%files doc
%defattr(-, root, root)
%doc %{name}-doc-%{realver}/*
 
%files -n lemon
%defattr(-, root, root)
%{_bindir}/lemon
%{_datadir}/lemon

%if %{with tcl}
%files tcl
%defattr(-, root, root)
%{tcl_sitearch}/sqlite3
%endif

%changelog
* Tue Jun 10 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.5-1m)
- update 3.8.5

* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.4.3-1m)
- update 3.8.4.3

* Wed Mar 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.4.1-1m)
- update 3.8.4.1

* Wed Sep 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.0.2-1m)
- update 3.8.0.2

* Sun Sep 01 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.0.1-1m)
- update 3.8.0.1

* Wed Jun 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.17-1m)
- update 3.7.17

* Fri Jan 11 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.15.2-1m)
- update 3.7.15.2

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.15-1m)
- update 3.7.15

* Fri Nov  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.14.1-1m)
- update 3.7.14.1

* Sat Sep  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.14-1m)
- update 3.7.14

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.13-1m)
- update 3.7.13

* Wed May 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.12.1-1m)
- update 3.7.12.1
- fixed SEGV issue. 3.7.12 crashed simple sql...

* Fri May 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.12-1m)
- update 3.7.12

* Mon May 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.11-1m)
- update 3.7.11

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.9-1m)
- update 3.7.9

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.8-1m)
- update 3.7.8

* Tue Jul  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.7.1-1m)
- update 3.7.7.1
-- bug fix release; see Momonga-devel.ja:04520

* Tue Jun 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.7-1m)
- update 3.7.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.5-3m)
- rebuild for new GCC 4.6

* Thu Feb  3 2011 ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7.5-2m)
- remove executable bits from %%{_docdir}/%%{name}-doc-%%{version}/search to resolve dependency

* Tue Feb  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.5-1m)
- update to 3.7.5

* Fri Dec 10 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.4-1m)
- update to 3.7.4, which includes an important bug fix
- merge changes from fedora's sqlite-3.7.4-1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.2-2m)
- rebuild for new GCC 4.5

* Mon Sep 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.2-1m)
- update to 3.7.2
- add "-ldl" in sqlite3.pc
- delete merged patches
- import upstream fix, see http://www.sqlite.org/src/ci/ba8ca9c9e2
- revise configuration
-- enable SQLITE_SECURE_DELETE, SQLITE_ENABLE_UNLOCK_NOTIFY for firefox 4
-- add --enable-tempstore=yes

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.23.1-5m)
- full rebuild for mo7 release

* Sat Aug 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.23.1-4m)
- incremental_vacuum bug fix and R-tree segfault bug fix

* Sat Aug  7 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (3.6.23.1-3m)
- version down 3.6.23.1

* Thu Jul 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.0-1m)
- update 3.7.0

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.23.1-2m)
- rebuild against readline6

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.23.1-1m)
- update 3.6.23.1

* Thu Mar 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.23-1m)
- update 3.6.23

* Tue Jan 12 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.22-1m)
- update 3.6.22

* Thu Dec 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.21-1m)
- update 3.6.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Nov 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.20-1m)
- update 3.6.20

* Sun Oct 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.19-1m)
- update 3.6.19

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.18-1m)
- update 3.6.18

* Wed Aug 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.17-1m)
- update 3.6.17

* Wed Jul 29 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.16-1m)
- update 3.6.16

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.15-1m)
- update 3.6.15

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.14.2-1m)
- update 3.6.14.2

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.14.1-1m)
- update 3.6.14.1

* Sat May 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.14-1m)
- update 3.6.14

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.13-1m)
- update 3.6.13

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.12-1m)
- update 3.6.12

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.11-1m)
- update 3.6.11

* Sun Feb  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.10-1m)
- update 3.6.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.7-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.7-2m)
- build with --enable-load-extension configure option
-- apply Patch0 from http://www.sqlite.org/cvstrac/tktview?tn=3555
-- need autoreconf
- add specopt do_test to run make check
  the default value is 0

* Thu Dec 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.7-1m)
- update to 3.6.7

* Thu Nov 27 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.6.2-1m)
- update to 3.6.6.2

* Sun Nov 23 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.6.1-1m)
- update to 3.6.6.1

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.6-1m)
- update to 3.6.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.5-1m)
- update to 3.6.5

* Sat Oct 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.4-1m)
- update 3.6.4

* Sun Sep  7 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update 3.6.2

* Sat Aug  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update 3.6.1

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Wed Jul  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-2m)
- rebuild to resolve dependency

* Sat May 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-1m)
- update to 3.5.8

* Wed Apr 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-1m)
- update to 3.5.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.7-2m)
- rebuild against gcc43

* Fri Mar 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.7-1m)
- update to 3.5.7

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Sun Feb  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sun Dec 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Fri Dec  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Fri Nov  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.2-1m)
- update to 3.5.2

* Fri Oct 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.1-1m)
- update to 3.5.1
- add sqlite-3.5.1-remove-docs.patch. 
-- document build error...

* Mon Aug 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.2-1m)
- update to 3.4.2

* Wed Jun 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.0-1m)
- update to 3.4.0

* Tue May  1 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.17-1m)
- update to 3.3.17

* Mon Feb 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.13-1m)
- update to 3.3.13

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.11-2m)
- delete libtool library

* Thu Jan 25 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.3.11-1m)
- version up 3.3.11

* Tue Jan 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.10-1m)
- update to 3.3.10

* Mon Aug 28 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (3.3.7-1m)
- update to 3.3.7

* Mon Jul 31 2006 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.6-1m)
- minor bugfixes

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.5-3m)
- rebuild against readline-5.0

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.5-2m)
- change package name

* Sun Apr  9 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.3.5-1m)
- update to 3.3.5

* Wed Feb 15 2006 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.4-1m)
- several bugfixes

* Thu Feb  9 2006 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.3-1m)
- new stable version

* Wed Dec 21 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.8-1m)
- update to 3.2.8

* Mon Sep 26 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.7-1m)
- update to 3.2.7

* Tue Sep 20 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.6-1m)
- update to 3.2.6

* Mon Aug 29 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.5-1m)
- update to 3.2.5

* Thu Aug 25 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.4-1m)
- update to 3.2.4

* Mon Aug 22 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.3-1m)
- update to 3.2.3
- %%{_libdir}/*.so moved from main package to devel package

* Tue Aug  2 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.2.2-1m)
- update to 3.2.2
- revised URL

* Mon Jun 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.1-1m)
- initial import to Momonga

