%global momorel 7

Summary: btnx configuration utility
Name: btnx-config
Version: 0.4.9
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.ollisalonen.com/btnx/
Group: Applications/System
Source0: http://www.ollisalonen.com/btnx/%{name}-%{version}.tar.gz
Source1: %{name}.pam
Source2: %{name}.consoleapp
Patch0: %{name}-0.4.7-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: btnx >= %{version}
Requires: usermode-gtk
BuildRequires: gtk2-devel
BuildRequires: libglade2-devel
BuildRequires: yelp

%description
btnx configuration utility.

btnx is a daemon that sniffs events from the mouse event handler.
If it recognizes a configured event, it sends a keyboard and/or mouse
combination event to uinput. This means you can configure certain
mouse buttons to send keyboard and mouse events to X.

It is useful for mice with more buttons than window managers can
handle. It also means you won't need to manually edit your window
manager and X configurations to get additional functionality from
extra buttons.

The configuration file for btnx is located at /etc/btnx/btnx_config.
The configuration file is edited with btnx-config. You must install
and run btnx-config before btnx will work.
After changing the config file, make sure to restart btnx in btnx-config
or by running

%prep
%setup -q
%patch0 -p1 -b .desktop~

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# fix permission
chmod 755 %{buildroot}%{_sbindir}/btnx-config

# install pam configuration
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sysconfdir}/pam.d
mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps
ln -s consolehelper %{buildroot}%{_bindir}/btnx-config
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pam.d/btnx-config
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/security/console.apps/btnx-config

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog INSTALL NEWS
%config(noreplace) %{_sysconfdir}/pam.d/btnx-config
%config(noreplace) %{_sysconfdir}/security/console.apps/btnx-config
%{_bindir}/btnx-config
%{_sbindir}/btnx-config
%{_datadir}/applications/btnx-config.desktop
%{_datadir}/btnx-config
%{_datadir}/locale/*/LC_MESSAGES/btnx-config.mo
%{_datadir}/omf/btnx-config
%{_datadir}/pixmaps/btnx.png

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.9-7m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.9-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.9-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-2m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-2m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Fri May  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-1m)
- version 0.4.9

* Fri Apr 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8-1m)
- version 0.4.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-1m)
- initial package for cutting-edge mouse freaks using Momonga Linux
