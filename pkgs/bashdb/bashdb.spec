%global momorel 2
%global mainver 4.2
%global subver 0.6
%global srcver %{mainver}-%{subver}

Name:           bashdb
Summary:        BASH debugger, the BASH symbolic debugger
Version:        %{mainver}.%{subver}
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Development/Debuggers
Url:            http://bashdb.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{srcver}.tar.bz2
NoSource:       0
Requires(post): info
Requires(preun): info
BuildRequires:  bash >= 4.2
Requires:       bash >= 4.2
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# bashdb-emacs is no longer available. Please check changelog for details
Obsoletes:	bashdb-emacs <= 4.0.0.4

%description
The Bash Debugger Project is a source-code debugger for bash,
which follows the gdb command syntax. 
The purpose of the BASH debugger is to check
what is going on inside a bash script, while it executes:
    * Start a script, specifying conditions that might affect its behavior.
    * Stop a script at certain conditions (break points).
    * Examine the state of a script.
    * Experiment, by changing variable values on the fly.
The 4.0 series is a complete rewrite of the previous series.
Bashdb can be used with ddd: ddd --debugger %{_bindir}/%{name} <script-name>.

%prep
%setup -q -n %{name}-%{srcver}

%build
%configure
make

%install
rm -rf %{buildroot}
make install INSTALL="install -p" DESTDIR=%{buildroot}
rm -f "%{buildroot}%{_infodir}/dir"

%if %{with tests}
%check
make check
%endif

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :

%postun
if [ "$1" = 0 ]; then
   /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc/*.html AUTHORS ChangeLog COPYING INSTALL NEWS README THANKS TODO
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_infodir}/%{name}.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.0.6-2m)
- rebuild for new GCC 4.6

* Mon Mar  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.0.6-4m)
- update for bash 4.2
- drop emacs sub-package; it seems to be separated into 
  https://github.com/rocky/emacs-dbgr

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.0.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0.4-3m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0.4-2m)
- rebuild against emacs-23.2

* Sun Jan 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.0.4-1m)
- update 4.0-0.4 to fix build failure

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0.3-4m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0.3-3m)
- rebuild against emacs 23.0.96

* Sat Jul 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0.3-2m)
- fix install-info

* Sat Jul 11 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.0.3-1m)
- version up 4.0-0.3

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0.09-6m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0.09-5m)
- rebuild against emacs-23.0.94

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0.09-4m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0.09-3m)
- rebuild against rpm-4.6

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0.09-2m)
- remove %%{_infodir}/dir to avoid conflicting on x86_64

* Fri May 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.0.09-1m)
- import to Momonga
