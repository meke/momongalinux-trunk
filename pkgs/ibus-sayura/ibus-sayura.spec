%global momorel 2

Name:       ibus-sayura
Version:    1.3.1
Release:    %{momorel}m%{?dist}
Summary:    The Sinhala engine for IBus input platform
License:    GPLv2+
Group:      System Environment/Libraries
URL:        https://fedorahosted.org/ibus-sayura
Source0:    https://fedorahosted.org/releases/i/b/ibus-sayura/%{name}-%{version}.tar.gz
NoSource:   0

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext-devel
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  ibus-devel >= 1.4.99.20121006

Requires:   ibus >= 1.4.99.20121006
%description
The Sayura engine for IBus platform. It provides Sinhala input method.

%prep
%setup -q

%build
%configure --disable-static
# make -C po update-gmo
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libexecdir}/ibus-engine-sayura
%{_datadir}/ibus-sayura
%{_datadir}/ibus/component/*

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-2m)
- rebuild against ibus-1.4.99.20121006

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0.20100716-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0.20100716-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0.20100716-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0.20100716-1m)
- import from Fedora 13

* Fri Jul 16 2010 Pravin Satpute <psatpute@redhat.com> - 1.3.0.20100716-1
- upstream new release

* Tue Jun 15 2010 Pravin Satpute <psatpute@redhat.com> - 1.2.99.20100209-2
- fixed bug 601568

* Tue Feb 09 2010 Pravin Satpute <pravin.d.s@gmail.com> - 1.2.99.20100209-1
- updated patches for code enhancements from phuang for ibus-1.2.99

* Mon Feb 08 2010 Adam Jackson <ajax@redhat.com> 1.2.0.20090703-4
- Rebuild for new libibus.so.2 ABI

* Tue Nov 17 2009 Pravin Satpute <psatpute@redhat.com> - @VERSON@-3
- fixed bug 528405

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0.20090703-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 03 2009 Pravin Satpute <psatpute@redhat.com> - @VERSON@-1
- upstream release 1.2.0

* Mon Jun 29 2009 Pravin Satpute <pravin.d.s@gmail.com> - @VERSON@-4
- fix for bug 507209

* Mon Jun 29 2009 Parag <panemade@gmail.com> - 1.0.0.20090326-3
- Rebuild against newer ibus

* Thu Mar 26 2009 Pravin Satpute <pravin.d.s@gmail.com> - @VERSON@-2
- updated as per fedora spec review

* Fri Mar 20 2009 Pravin Satpute <pravin.d.s@gmail.com> - 1.0.0.20090326-1
- The first version.
