%global    momorel 1
%global    qtver 4.8.5

Summary:   Qt support library for PackageKit
Name:      PackageKit-Qt
Version:   0.8.8
Release:   %{momorel}m%{?dist}
License:   LGPLv2+
Group:     System Environment/Libraries
URL:       http://www.packagekit.org/
Source0:   http://www.packagekit.org/releases/%{name}-%{version}.tar.xz
NoSource:  0
BuildRequires: cmake
BuildRequires: qt-devel >= %{qtver}

Obsoletes: PackageKit-qt < 0.8.6
Provides:  PackageKit-qt = %{version}-%{release}
Provides:  PackageKit-qt = %{version}-%{release}

# required for /usr/share/dbus-1/interfaces/*.xml
BuildRequires: PackageKit

%description
PackageKit-qt is a Qt support library for PackageKit

%package devel
Summary: Development headers for PackageKit-Qt
Group:   Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

Obsoletes: PackageKit-qt-devel < 0.8.6
Provides:  PackageKit-qt-devel = %{version}-%{release}

%description devel
Development headers and libraries for PackageKit-Qt.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
  -DCMAKE_INSTALL_LIBDIR:PATH=%{_libdir} \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc AUTHORS NEWS COPYING
%{_libdir}/libpackagekit-qt2.so.%{version}
%{_libdir}/libpackagekit-qt2.so.6*

%files devel
%{_libdir}/libpackagekit-qt2.so
%{_libdir}/pkgconfig/packagekit-qt2.pc
%dir %{_includedir}/PackageKit
%{_includedir}/PackageKit/packagekit-qt2/
%dir %{_libdir}/cmake
%{_libdir}/cmake/packagekit-qt2/

%changelog
* Thu Aug  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- import from Fedora

* Mon Nov 26 2012 Rex Dieter <rdieter@fedoraproject.org> 0.8.6-3
- fixup dir ownership
- use pkgconfig-style deps
- tighten subpkg dep via %%?_isa

* Mon Nov 26 2012 Richard Hughes <richard@hughsie.com> 0.8.6-2
- Added obsoletes/provides as required for the Fedora package review

* Mon Nov 26 2012 Richard Hughes <richard@hughsie.com> 0.8.6-1
- Initial version for Fedora package review
