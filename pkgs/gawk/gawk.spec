%global momorel 1

Summary: The GNU version of the awk text processing utility
Name: gawk
Version: 4.1.1
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Text
URL: http://www.gnu.org/software/gawk/gawk.html
Source0: ftp://ftp.gnu.org/gnu/gawk/gawk-%{version}.tar.xz
NoSource: 0

Requires(post): info
Requires(preun): info
Provides: awk
Provides: /bin/awk
Provides: /bin/gawk
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The gawk packages contains the GNU version of awk, a text processing
utility.  Awk interprets a special-purpose programming language to do
quick and easy text pattern matching and reformatting jobs.

Install the gawk package if you need a text processing utility. Gawk is
considered to be a standard Linux tool for processing text.

%prep
%setup -q

%build

# gawk-3.1.8 has a problem when using gcc 4.6 with -O2 or higher
# see http://gcc.gnu.org/bugzilla/show_bug.cgi?id=47390
export CFLAGS="`echo $RPM_OPT_FLAGS| sed -e 's,\-O[^ ]*,-O1,g'`"
%configure  --disable-libsigsegv
%make

%install
rm -rf %{buildroot}
%makeinstall

rm -f %{buildroot}%{_infodir}/dir

cat <<EOF > %{buildroot}%{_mandir}/man1/awk.1
.so man1/gawk.1
EOF

find %{buildroot} -name "*.la" -delete

%check
# cant run pty1 in chroot env
sed -i 's/ pty1 / /g' test/Makefile

make check

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/gawk.info %{_infodir}/dir
/sbin/install-info %{_infodir}/gawkinet.info %{_infodir}/dir

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gawk.info %{_infodir}/dir
  /sbin/install-info --delete %{_infodir}/gawkinet.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS POSIX.STD README
%doc README_d
%{_bindir}/*
%{_prefix}/bin/*
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_includedir}/gawkapi.h
%{_infodir}/gawk.info*
%{_infodir}/gawkinet.info*
%{_libdir}/gawk
%{_libexecdir}/awk
%{_datadir}/awk
%{_datadir}/locale/*/*/gawk.mo

%changelog
* Wed Apr 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.1-1m)
- update 4.1.1

* Sat Mar 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.0-2m)
- support UserMove env

* Mon May 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1.0-1m)
- update 4.1.0

* Thu Jan 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0.2-1m)
- update 4.0.2

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0.1-1m)
- update 4.0.1

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0.0-1m)
- update 4.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-5m)
- rebuild for new GCC 4.6

* Mon Feb  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-4m)
- import two bug fix patches from fedora
- use -O1 in CFLAGS to avoid a possible miscompilation with gcc 4.6
- add %%check

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.8-2m)
- full rebuild for mo7 release

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.8-1m)
- update to 3.1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-2m)
- add --disable-libsigsegv for glibc210

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.7-1m)
- update to 3.1.7
- remove ps

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.6-1m)
- update to 3.1.6
- License: GPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.5-2m)
- %%NoSource -> NoSource

* Sun May 14 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.1.5-1m)
- update to 3.1.5
- revise source URL
- delete unused patches
- add new patches from FC5

* Wed Nov  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.4-3m)
- Provides: /bin/gawk for mph-get-check

* Mon Feb 28 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.4-2m)
  import some patches(FC dev. 3.1.4-4)

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.4-1m)
  update to 3.1.4
  
* Tue Sep 28 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.1.3-5m)
- When %%{_datadir} is redifined in spec, 
  %%{_mandir} and %%{_infodir} are also redifined (see macros.momonga),
  and thus mans and infos are installed into incorrect location.
  This should be the problem of macros.momonga, but I tentatively
  redifine %%{_mandir} and %%{_infodir} in this spec.
- install gawkinet.info

* Tue Nov 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.3-4m)
- fix %%files section for /usr/bin/*
- BTS report id=5

* Sun Oct 26 2003 Kenta MURATA <muraken2@nifty.com>
- (3.1.3-3m)
- pretty spec file.
- include gettext message database.

* Thu Oct 23 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (3.1.3-2m)
- append "Provides: /usr/bin/gawk"

* Wed Oct 22 2003 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.1.3-1m)
- update to 3.1.3
- remove IBM i18n patch (appear to be marged to original distribution)
- add patches from RawHide (3.1.3-3)
-
- * Mon Sep 22 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- - add even more patches from the mailinglist
-
- * Tue Jul 15 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- - add first bug-fixes from the mailinglist
-
- * Sun Jul 13 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- - update to 3.1.3
- - pgawk man-page fix and /proc fix are obsolete

* Tue Dec 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.0-13m)
- provides /bin/awk.

* Tue Apr 30 2002 Toru Hoshina <t@kondara.org>
- (3.1.0-12k)
- provides awk.

* Tue Apr 30 2002 Motonobu Ichimura <famao@kondara.org>
- (3.1.0-10k)
- update i18n patch (0.3)

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.1.0-8k)
- /sbin/install-info -> info in PreReq.

* Fri Jan 11 2002 Shingo Akagaki <dora@kondara.org>
- (3.1.0-6k)
- automake autoconf 1.5

* Wed Nov 07 2001 Motonobu Ichimura <famao@kondara.org>
- (3.1.0-4k)
- fix man problem
- modify %doc

* Fri Oct 25 2001 Motonobu Ichimura <famao@kondara.org>
- (3.1.0-2k)
- up to 3.1.0
- added patch from IBM

* Fri Oct 25 2001 Motonobu Ichimura <famao@kondara.org>
- (3.06-8k)
- added patch from IBM

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Wed Aug 16 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.06

* Tue Aug 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- /usr/bin/gawk can't point at gawk - infinite symlink
- /usr/bin/awk can't point at gawk - infinite symlink

* Mon Aug 14 2000 Preston Brown <pbrown@redhat.com>
- absolute --> relative symlinks

* Tue Aug  8 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- fix paths for "configure" call

* Thu Jul 13 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- add another bugfix

* Thu Jul 13 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.0.5 with bugfix

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jun 30 2000 Matt Wilson <msw@redhat.com>
- revert to 3.0.4.  3.0.5 misgenerates e2fsprogs' test cases

* Wed Jun 28 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.0.5

* Mon Jun 19 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- add defattr

* Mon Jun 19 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- FHS

* Tue Mar 14 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- add bug-fix

* Thu Feb  3 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix man page symlinks
- Fix description
- Fix download URL

* Wed Jun 30 1999 Jeff Johnson <jbj@redhat.com>
- update to 3.0.4.

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- make sure all binaries are stripped

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 6)

* Fri Feb 19 1999 Jeff Johnson <jbj@redhat.com>
- Install info pages (#1242).

* Fri Dec 18 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1
- don't package /usr/info/dir

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 3.0.3
- added documentation and buildroot

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
