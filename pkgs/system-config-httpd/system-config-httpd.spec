%global momorel 1

Summary: Apache configuration tool
Name: system-config-httpd
Version: 1.5.5
Release: %{momorel}m%{?dist}
URL: http://www.redhat.com/
Source0: system-config-httpd-%{version}.tar.gz
License: GPL
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.2, python-devel >= 2.2
BuildRequires: libglade2-devel, gettext, intltool >= 0.17
BuildArch: noarch
Requires: python >= 2.2, libglade2, pygtk2-libglade, pygtk2
Requires: gnome-python2, gnome-python2-canvas, httpd, usermode, libxslt-python
Obsoletes: apacheconf
Provides: apacheconf
Obsoletes: redhat-config-httpd
Provides: redhat-config-httpd

%description
system-config-httpd is a graphical user interface that allows the user to
configure a httpd server.

%prep
%setup -q -n system-config-httpd-%{version}

%define _target_platform        %{nil}   
%configure 
%undefine       _target_platform  

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

%find_lang system-config-httpd

%clean
rm -rf $RPM_BUILD_ROOT

%pre
if [ -h /etc/pam.d/system-config-httpd ] ; then
  mv -f /etc/pam.d/system-config-httpd /etc/pam.d/system-config-httpd.rpmsave
fi

%preun
if [ $1 = 0 ]; then
  if [ -d /var/cache/alchemist/apache ] ; then
    rm -rf /var/cache/alchemist/apache/*
  fi

  if [ -d /usr/share/system-config-httpd ] ; then
    rm -rf /usr/share/system-config-httpd/*.pyc
    rm -rf /usr/share/system-config-httpd/*.md5
  fi
fi

%files -f system-config-httpd.lang
%defattr(-,root,root)
%doc %{_docdir}/system-config-httpd
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/*
%config(noreplace) %{_sysconfdir}/alchemist/switchboard/system-config-httpd.switchboard.adl
%config(noreplace) %{_sysconfdir}/alchemist/namespace/system-config-httpd/local.adl
%config %{_sysconfdir}/alchemist/namespace/system-config-httpd/rpm.adl
%dir %{_sysconfdir}/alchemist/namespace/system-config-httpd
%{_mandir}/man1/*
%{_datadir}/system-config-httpd
%{_datadir}/pixmaps/system-config-httpd.png
%{_datadir}/kontrol-panel
%{_datadir}/applications/system-config-httpd.desktop
%dir /var/cache/alchemist/apache

%changelog
* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.5-1m)
- update 1.5.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1
-- no requires alchemist

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-1m)
- update 1.4.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-4m)
- rebuild against gcc43

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-3m)
- modify %%files again

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-2m)
- modify %%files

* Sun Jun  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-1m)
- import from Fedra

* Thu Jan 11 2007 Phil Knirsch <pknirsch@redhat.com> 1.4.3-1.fc7
- Small manpage fix to be docbook compatible (#221191)

* Mon Nov 20 2006 Phil Knirsch <pknirsch@redhat.com> 1.4.2-1.fc7
- Fixed description.

* Mon Nov 13 2006 Phil Knirsch <pknirsch@redhat.com> 1.4.0-2.fc7
- Fixed missing buildrequires on gettext

* Fri Nov 10 2006 Phil Knirsch <pknirsch@redhat.com> 1.4.0-1.fc7
- Finished SSL related bugfixes finally, too
- Fixed problem with s-c-h removing config and .pyc files after update

* Wed Oct 25 2006 Phil Knirsch <pknirsch@redhat.com>
- Large update for system-config-httpd with lots of bugfixes

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 5:1.3.3-1.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Nov 04 2005 Phil Knirsch <pknirsch@redhat.com> 1.3.3-1
- Rewrote XSLT backend to use libxslt-python instead of 4Suite
- Fixed wrongly claimed "file has been manually modified" for fresh 1st run
  (#167633)
- Updated pam config file to latest pam recommendation (#170629)

* Mon May 23 2005 Phil Knirsch <pknirsch@redhat.com> 1.3.2-2
- Fixed the KeepAlive bug that was already fixed as errata for RHEL3 (#137815)
- Fixed use of True and False instead of gtk.TRUE and gtk.FALSE (#153039)
- Fixed traceback when no X-server is running
- Fixed problem with ServerSignature setting (#156639)
- Removed static /manual alias from httpd.conf template (#144239)
- Updated gtk.mainloop() and gtk.mainquit() to new API names
- Added pa translation file (#157826)

* Tue Sep 28 2004 Phil Knirsch <pknirsch@redhat.com> 1.3.1-1
- Added a bunch of new translations

* Fri Sep 10 2004 Phil Knirsch <pknirsch@redhat.com> 1.3.0-1
- Huge HIG 2 compliance changes by Vladimir Djokic

* Wed Sep 01 2004 Phil Knirsch <pknirsch@redhat.com> 1.2.1-3
- Fixed DirectoryIndex problem (#89947)

* Mon Aug 16 2004 Phil Knirsch <pknirsch@redhat.com> 1.2.1-2
- Really fixed SSLLog and SSLLogLevel problem (#115221)

* Fri Jul 30 2004 Phil Knirsch <pknirsch@redhat.com> 1.2.1-1
- Fixed problem with Error documents for non English languages (#115220)
- Removed deprecated SSLLog and SSLLogLevel parameters (#115221)
- Fixed SSL stuff (#115223)
- Fixed not well-formed XSLT file (#119849)
- Obsoletes redhat-config-httpd (#124106)
- system-config-httpd docdir now is owned by package (#125750)

* Tue Mar 04 2004 Karsten Hopp <karsten@redhat.com>
- update translations (#118809)

* Tue Mar 04 2004 Karsten Hopp <karsten@redhat.com>
- fix console.apps file (#116023)

* Wed Feb 04 2004 Phil Knirsch <pknirsch@redhat.com>
- Added missing requires line for ghome-python2-canvas package.

* Mon Dec 08 2003 Phil Knirsch <pknirsch@redhat.com> 1.2.0-1
- Finished name switch to system-config-httpd from redhat-config-httpd

* Wed Dec 03 2003 Phil Knirsch <pknirsch@redhat.com>
- Fixed gnome.url_show() call for help pages (#110128).
- Updated httpd.conf.xsl to latest httpd package (#107539).

* Fri Nov 28 2003 Phil Knirsch <pknirsch@redhat.com>
- Include HIG patch from Jens Knutson and fixed remaining problems with it.
- Really added Crotatin and a few other languages this time (#106601).

* Fri Oct 31 2003 Phil Knirsch <pknirsch@redhat.com> 1.1.0-6
- Fixed dangling symlink problem in /etc/pam.d (#108704).

* Wed Oct 29 2003 Phil Knirsch <pknirsch@redhat.com> 1.1.0-5
- Fixed problem with Allow from and Deny from (double from).
- Included Croation translation (#106601).
- Big change in 4Suite requires fix for the Xslt processing.

* Fri Oct  3 2003 Jeremy Katz <katzj@redhat.com> 1.1.0-4
- rebuild

* Fri Aug 01 2003 Phil Knirsch <pknirsch@redhat.com> 1.1.0-3
- Fixed main window title (#98028)
- Remove debug output
- Fixed startup error messages (#101076)

* Thu Jul 10 2003 Phil Knirsch <pknirsch@redhat.com> 1.1.0-2
- Removed duplicate docs (#85225).

* Thu Jul 10 2003 Phil Knirsch <pknirsch@redhat.com> 1.1.0-1.1
- rebuilt

* Thu Jul 10 2003 Phil Knirsch <pknirsch@redhat.com> 1.1.0-1
- Switched to redhat-config-httpd internally now, too.

* Thu Jun 26 2003 Phil Knirsch <pknirsch@redhat.com> 1.0.1-19
- Fixed desktop file
- Rebuilt with updated docs.

* Mon Feb 17 2003 Phil Knirsch <pknirsch@redhat.com> 1.0.1-18
- Fixed unecessary ScoreBoard entry (#82349)

* Fri Feb 07 2003 Phil Knirsch <pknirsch@redhat.com> 1.0.1-17
- Fixed ServerName bug (#82349)

* Thu Feb 06 2003 Phil Knirsch <pknirsch@redhat.com> 1.0.1-16
- Fixed absolute symlink for config files.
- Fixed problem with startup error message without X11.
- Fixed AllowOverride problem.
- Fixed Listen parameter handling.
- Fixed Allow and Deny parameter handling.

* Mon Jan 27 2003 Phil Knirsch <pknirsch@redhat.com> 1.0.1-15
- Included updated documentation from Tammy Fox.

* Wed Dec 04 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-14
- Fixed quite a few packaging problems with latest release.

* Tue Sep 03 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-13
- Fixed a few problems with the interface and the XSLT file.

* Thu Aug 29 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.0.1-12
- update translations

* Tue Aug 20 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-11
- Fixed another pam timestamp support bug.
- Remove some debug output.

* Wed Aug 14 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-10
- Fixed missing Alias entry in XSLT conversion file (#71423).
- Fixed wrong menu entry (apacheconf instead of redhat-config-httpd) (#71424).

* Fri Aug 09 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-9
- Some more desktop file fixes by Tammy Fox.

* Thu Aug 08 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-8
- Fixed wrong width-request entries in glade file.

* Wed Aug 07 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-7
- Fixed desktop file stuff.

* Tue Aug 06 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-6
- Tammy Fox updated the documentation, built new version.

* Wed Jul 24 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-4
- Fixed intltool problems.
- Changed package name to redhat-config-httpd

* Wed Jul 17 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-3
- Fixed a problem with pango and UTF8 characters (#68939).

* Wed Jul 03 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.1-2
- Now with new and improved use of intltool.
- Fixed the wrong documentation variable.

* Wed Jul 03 2002 Phil Knirsch <pknirsch@redhat.com> 1.0.0-1
- Completed update to apache 2.0 config file style.
- Fixed some minor problems on the way.

* Tue Jun 25 2002 Phil Knirsch <pknirsch@redhat.com> 0.9.0-2
- Changed Requires from apache to httpd as the name has changed.

* Mon Jun 24 2002 Phil Knirsch <pknirsch@redhat.com> 0.9.0-1
- First complete updated to gtk2.

* Tue May 14 2002 Phil Knirsch <pknirsch@redhat.com> 0.8.3-3
- Fixed the hopefully last po related problem.

* Mon Apr 22 2002 Phil Knirsch <pknirsch@redhat.com> 0.8.3-2
- Fixed wrong documentation location (#63905).

* Wed Apr 17 2002 Phil Knirsch <pknirsch@redhat.com> 0.8.3-1
- Removed the Distutils require again. Should be in 4Suite.

* Sat Apr 13 2002 Phil Knirsch <pknirsch@redhat.com> 0.8.2-2
- Fixed a tons of problems with new autoFOO stuff.

* Fri Apr 12 2002 Phil Knirsch <pknirsch@redhat.com>
- Fixed a problem in our rpm.xml default file (#63233)

* Wed Apr 10 2002 Bill Nottingham <notting@redhat.com> 0.8.2-1
- fixed missing dependency on Distutils in the specfile (#62794)

* Mon Nov 19 2001 Phil Knirsch <phil@redhat.de> 0.8.1-2
- Fixed missing dependancy on 4Suite in the specfile.

* Thu Aug 30 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.8.1-1
- don't install desktop files in the kontrol-panel directory, it 
  now reads the standard location as well (#52719)
- nn and no summaries in .desktop files were switched
- Fix character set for es.po, pt.po and it.po
- The COPYING filed listed as a doc-file weren't present in the tree
- Mark various configuration files as %%config

* Sun Aug 26 2001 Philipp Knirsch <pknirsch@redhat.de> 0.8-7
- Added symlink to redhat-config-apache for tool nameing compatibility.
- Fixed typo in po/pt_BR.po that prevented doing a make dist.

* Thu Aug  9 2001 Alexander Larsson <alexl@redhat.com> 0.8-6
- Added my changes to cvs at sources.redhat.com

* Wed Aug  8 2001 Alexander Larsson <alexl@redhat.com> 0.8-5
- Install desktop file in serverconf:
- Install icon in /usr/share/pixmaps

* Tue Jun 26 2001 Philipp Knirsch <pknirsch@redhat.de> 0.8-4/3
- Fixed not working directory options removal (#45160)
- Fixed missing libglade-devel build prereq (#44725)
- Fixed missing ServerAlias entries in vhosts (#44320)

* Mon Jun 16 2001 Tammy Fox <tfox@redhat.com> 0.8-3/3
- Minor changes to documentation

* Tue May 14 2001 Philipp Knirsch <pknirsch@redhat.com> 0.8-3/2
- Fixed some autofoo issues in Makefile.am, specfile and autogen.sh 
- Fixed Bugzilla bug #40004 (small typo in src/ApacheControl.py)

* Tue Mar 20 2001 Philipp Knirsch <pknirsch@redhat.com> 0.8-2/1
- Added the Epoch: tag to avoid update problems with previous versions.
- Added a nice srpm feature in the Makefile.am with which we can now easily
  make SRPMS directly from CVS (borrowed from the kernel CVS Makefile ;)

* Sun Mar 18 2001 Yukihiro Nakai <ynakai@redhat.com>
- Update Japanese translation
- Add .mo files to the file section in .spec

* Fri Mar  9 2001 Philipp Knirsch <pknirsch@redhat.com>
- Final fix for window close box

* Thu Mar  8 2001 Jonathan Blandford <jrb@redhat.com>
- Hooked up helpfiles

* Thu Mar  8 2001 Philipp Knirsch <pknirsch@redhat.com>
- Added documentation to the installation and RPM
- New version

* Mon Mar  5 2001 Philipp Knirsch <pknirsch@redhat.com>
- Added the apacheconf.png
- Installation of kontrol-panel files

* Mon Mar  5 2001 Jonathan Blandford <jrb@redhat.com>
- really include desktop file

* Tue Feb 27 2001 Jonathan Blandford <jrb@redhat.com>
- Include desktop file

* Fri Feb 23 2001 Philipp Knirsch <pknirsch@redhat.com>
- Added manpage and reworked parts of the spec file to look cleaner.

* Tue Feb 13 2001 Philipp Knirsch <pknirsch@redhat.com>
- Updated and fixed all of the bugzilla entries we had. Cleaned up some other
  parts as well.

* Fri Feb  9 2001 Trond Eivind Glomsrod <teg@redhat.com>
- new version

* Thu Feb  8 2001 Jonathan Blandford <jrb@redhat.com>
- new version

* Thu Feb  1 2001 Jonathan Blandford <jrb@redhat.com>
- new version

* Wed Jan 24 2001 Jonathan Blandford <jrb@redhat.com>
- new version

* Thu Jan 18 2001 Jonathan Blandford <jrb@redhat.com>
- Initial build.
