%global momorel 6

Summary: xosd - X on-screen display library
Name: xosd
Version: 2.2.14
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.ignavus.net/software.html
Source0: http://ftp.debian.org/debian/pool/main/x/%{name}/%{name}_%{version}.orig.tar.gz
NoSource: 0
Patch0: lib%{name}.m4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gdk-pixbuf
Requires: gtk+1
Requires: xorg-x11-fonts-base
BuildRequires: gdk-pixbuf-devel
BuildRequires: gtk+1-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXinerama-devel
BuildRequires: xmms-devel >= 1.2.10-10m

%description
xosd is a library for displaying an on-screen display (like the one on many TVs) on your X display.

%package devel
Summary: Header files and libraries for developing apps which will use %{name}.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libX11-devel
Requires: libXext-devel
Requires: libXinerama-devel

%description devel
The %{name}-devel package contains the header files and libraries needed
to develop programs that use the %{name} library.

Install the %{name}-devel package if you want to develop applications that
will use the %{name} library.

%package -n xmms-xosd
Summary: xosd plugin for xmms
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: gtk+1
Requires: xmms

%description -n xmms-xosd
xmms-xosd is an xmms plugin to display various things whenever they change (volume, track, paused/shuffle/repeat etc...)

%prep
%setup -q
%patch0 -p0 -b .m4~

# convert docs to UTF-8
for f in ChangeLog man/xosd_{create,destroy,display,is_onscreen,set_bar_length}.3 ; do
    iconv -f iso-8859-1 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f
done

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la file
find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_bindir}/osd_cat
%{_libdir}/libxosd.so.*
%{_mandir}/man1/osd_cat.1*
%{_datadir}/xosd

%files devel
%defattr(-,root,root)
%{_bindir}/xosd-config
%{_includedir}/xosd.h
%{_libdir}/libxosd.a
%{_libdir}/libxosd.so
%{_datadir}/aclocal/libxosd.m4
%{_mandir}/man1/xosd-config.1*
%{_mandir}/man3/xosd*.3*

%files -n xmms-xosd
%defattr(-,root,root)
%{_libdir}/xmms/General/libxmms_osd.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.14-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.14-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.14-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.14-1m)
- version 2.2.14
- use debian's tar-ball
- fix man files splitting

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.12-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.12-4m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.12-3m)
- delete libtool library

* Tue Mar 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.12-2m)
- rebuild against xmms-1.2.10-10m

* Mon Jun 27 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.12-1m)
- update 2.2.12

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.8-3m)
- suppress AC_DEFUN warning.

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.8-2m)
- enable x86_64.

* Mon Jul  5 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.8-1m)
- version up

* Wed Oct  8 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.2.5-2m)
- add defattr (reported by Ichiro Nakai)

* Sat Oct  4 2003 Junichiro Kita <kita@momonga-linux.org>
- (2.2.5-1m)
- ver up

* Sun Apr 20 2003 Junichiro Kita <kita@momonga-linux.org>
- (2.2.1-1m)
- ver up

* Sun Apr 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
* (2.0.1-2m)
- add BuildRequires: gdk-pixbuf, libglade1

* Thu Jan  9 2003 Junichiro Kita <kita@momonga-linux.org>
* (2.0.1-1m)
- ver up

* Wed Oct 23 2002 Junichiro Kita <kita@momonga-linux.org>
* (1.0.3-2m)
- NoSource yame!

* Sun Sep  1 2002 Junichiro Kita <kita@momonga-linux.org>
* (1.0.3-1m)
- update to 1.0.3

* Thu Aug 15 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.0.1-2m)
- apply ba-tari- patch
- see http://u.dhis.portside.net/~uh/diary/?date=20020815#p01

* Wed Aug 14 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Sat Jul 27 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (1.0.0-1m)
- update to 1.0.0

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.7.0-4k)
- PreReq: /sbin/ldconfig -> glibc

* Sun Mar 10 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.7.0-2k)
- Jirai to STABLE

* Tue Mar 5 2002 TABUCHI Takaaki <tab@kondara.org>
- (0.7.0-5k)
- add xmms-devel at BuildRequires for xmms-config

* Tue Feb 26 2002 kitaj <kita@kitaj.no-ip.com>
- initial build
