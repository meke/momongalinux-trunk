%global         momorel 21

Name:           perl-perlmenu
Version:        4.0
Release:        %{momorel}m%{?dist}
Summary:        Perl library module for curses-based menus & data-entry templates
Group:          Development/Libraries
License:        LGPLv2 or "Artistic clarified"
URL:            http://www.public.iastate.edu/~skunz/PerlMenu/homepage.html
Source0:        ftp://ftp.iastate.edu/pub/perl/perlmenu.v%{version}.tar.Z
NoSource:	0
Patch0:         getcapforperl5.patch
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
PerlMenu - Perl library module for curses-based menus & data-entry templates.

%prep
%setup -q -n perlmenu.v%{version}
%patch0 -p0
mkdir examples
mv demo* ez* template_* menuutil.pl examples/
find examples -type f -exec chmod 644 {} \;
sed -i -e 's|/usr/local/bin/perl5|%{__perl}|' examples/*

%build
%{__perl} create_menu.pl 

%install
rm -rf %{buildroot}
install -d %{buildroot}%{perl_vendorlib}
install -p -m 644 perlmenu.pm %{buildroot}%{perl_vendorlib}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc FAQ MENUUTIL_DOC MENU_DOC README RELEASE_NOTES TO_DO COPYING examples
%{perl_vendorlib}/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-16m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-15m)
- change primary site and download URIs

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-13m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-2m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Aug 29 2008 Parag Nemade <panemade@gmail.com>- 4.0-7
- Fix Patch tag

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 4.0-6
- Rebuild for new perl

* Tue Aug 21 2007 Parag Nemade <panemade@gmail.com>- 4.0-5
- update License tag 

* Mon Jan 26 2007 Parag Nemade <panemade@gmail.com>- 4.0-4
- Added pactch to enable getcap for perl5 for bug rh#233541

* Fri Sep 01 2006 Parag Nemade <panemade@gmail.com>- 4.0-4
- Corrected License tag

* Mon Jul 31 2006 Parag Nemade <panemade@gmail.com>- 4.0-3
- Removed parameters passed to create_menu.pl
- Changed installation path of menuutil.pl to examples directory

* Thu Jul 20 2006 Parag Nemade <panemade@gmail.com>- 4.0-2
- Added examples directory

* Tue Jul 18 2006 Parag Nemade <panemade@gmail.com>- 4.0-1
- Initial Release

