%global momorel 2
%global majorminor 0.10

%global srcname gst-plugins-gl

Name: gstreamer-plugins-gl
Version: 0.10.3
Release: %{momorel}m%{?dist}
Summary: GStreamer streaming media framework OpenGL plug-ins

Group: Applications/Multimedia
License: LGPL
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer

# plugins
BuildRequires: e2fsprogs-devel
BuildRequires: glew-devel >= 1.10.0
BuildRequires: glib2-devel
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXdamage-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libdrm-devel
BuildRequires: libpng-devel
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: zlib-devel

# documentation
BuildRequires:  gtk-doc

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%description
This module contains integration libraries and plug-ins for using
OpenGL within GStreamer pipelines.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure \
    --disable-static \
    --enable-gtk-doc \
    --enable-experimental
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README RELEASE REQUIREMENTS TODO

# plugins
%{_libdir}/gstreamer-%{majorminor}/libgstopengl.so
%{_libdir}/gstreamer-%{majorminor}/libgstlibvisualgl.so
%exclude %{_libdir}/gstreamer-%{majorminor}/*.la

%{_datadir}/locale/*/*/*

# library
%{_libdir}/libgstgl-0.10.so.*
%exclude %{_libdir}/libgstgl-0.10.la

%files devel
%defattr(-, root, root)
%{_includedir}/gstreamer-%{majorminor}/gst/gl
%{_libdir}/libgstgl-0.10.so
%{_libdir}/pkgconfig/gstreamer-gl-0.10.pc
%{_datadir}/gtk-doc/html/gst-plugins-gl-libs-0.10/
%{_datadir}/gtk-doc/html/gst-plugins-gl-plugins-0.10/

%changelog
* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.3-2m)
- rebuild against glew-1.10.0

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-7m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-6m)
- rebuild for glew-1.9.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-5m)
- rebuild for glib 2.33.2

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-4m)
- rebuild against glew-1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-2m)
- rebuild for new GCC 4.5

* Fri Oct 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-2m)
- fix BPR

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- initial build
