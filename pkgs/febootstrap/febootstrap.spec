%global momorel 1

Summary:     Bootstrapping tool for creating supermin appliances
Name:        febootstrap
Version:     3.20
Release:     %{momorel}m%{?dist}
License:     GPLv2+
Group:       Development/Tools
URL:         http://people.redhat.com/~rjones/febootstrap/
Source0:     http://people.redhat.com/~rjones/febootstrap/files/%{name}-%{version}.tar.gz
NoSource:    0
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: perl
BuildRequires: yum >= 3.2
BuildRequires: e2fsprogs
BuildRequires: e2fsprogs-devel
BuildRequires: glibc-static
BuildRequires: ocaml, ocaml-findlib-devel
BuildRequires: prelink

ExclusiveArch:  alpha armv4l %{ix86} ia64 x86_64 ppc sparc sparcv9 ppc64

Requires:    yum >= 3.2
Requires:    yum-utils
Requires:    febootstrap-supermin-helper = %{version}-%{release}

# These are suggestions.  However making them hard requirements
# pulls in far too much stuff.
#Requires:    qemu
#Requires:    filelight
#Requires:    baobab     # Not as nice as filelight.


%description
febootstrap is a tool for building supermin appliances.  These are
tiny appliances (similar to virtual machines), usually around 100KB in
size, which get fully instantiated on-the-fly in a fraction of a
second when you need to boot one of them.


%package supermin-helper
Summary:     Runtime support for febootstrap
Group:       Development/Tools
Requires:    util-linux-ng
Requires:    cpio
Requires:    e2fsprogs
Obsoletes:   febootstrap <= 3.3-3


%description supermin-helper
%{name}-supermin-helper contains the runtime support for %{name}.


%prep
%setup -q

%build
%configure
make


%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# febootstrap-supermin-helper is marked as requiring an executable
# stack.  This happens because we use objcopy to create one of the
# component object files from a data file.  The program does not in
# fact require an executable stack.  The easiest way to fix this is to
# clear the flag here.
execstack -c %{buildroot}%{_bindir}/febootstrap-supermin-helper


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/febootstrap
%{_mandir}/man8/febootstrap.8*


%files supermin-helper
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/febootstrap-supermin-helper
%{_mandir}/man8/febootstrap-supermin-helper.8*


%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.20-1m)
- version up 3.20

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.12-1m)
- version up 3.12

* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.8-1m)
- version up 3.8

* Mon Jul  4 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.4-1m)
- import from Fedora 15

* Fri Mar 18 2011 Richard Jones <rjones@redhat.com> - 3.4-2
- Don't fail if objects are created in a symlinked dir (RHBZ#698089).

* Fri Mar 18 2011 Richard Jones <rjones@redhat.com> - 3.4-1
- New upstream version 3.4.
- febootstrap-supermin-helper Obsoletes older versions of febootstrap.

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Jan 14 2011 Richard Jones <rjones@redhat.com> - 3.3-4
- Split package into febootstrap (for building) and febootstrap-supermin-helper
  (for running).  Note that febootstrap depends on febootstrap-supermin-helper,
  but you can install febootstrap-supermin-helper on its own.

* Fri Jan 14 2011 Richard Jones <rjones@redhat.com> - 3.3-3
- Clear executable stack flag on febootstrap-supermin-helper.

* Thu Jan 13 2011 Dan Horák <dan[at]danny.cz> - 3.3-2
- add the ocaml's ExclusiveArch

* Sat Dec 11 2010 Richard Jones <rjones@redhat.com> - 3.3-1
- New upstream version 3.3.

* Tue Dec  7 2010 Richard Jones <rjones@redhat.com> - 3.2-1
- New upstream version 3.2.
- Remove upstream patches.

* Tue Dec  7 2010 Richard Jones <rjones@redhat.com> - 3.1-5
- Previous fix for RHBZ#654638 didn't work, fix it correctly.

* Mon Dec  6 2010 Richard Jones <rjones@redhat.com> - 3.1-4
- Properly ignore .*.hmac files (accidental reopening of RHBZ#654638).

* Mon Dec  6 2010 Richard Jones <rjones@redhat.com> - 3.1-3
- Uses yumdownloader at runtime, so require yum-utils.

* Mon Dec  6 2010 Richard Jones <rjones@redhat.com> - 3.1-2
- New upstream version 3.1.
- BR insmod.static.

* Sun Dec  5 2010 Richard Jones <rjones@redhat.com> - 3.0-2
- New upstream version 3.0 (note this is incompatible with 2.x).
- Fix upstream URLs.
- fakeroot, fakechroot no longer required.
- insmod.static is required at runtime (missing dependency from earlier).
- The only programs are 'febootstrap' and 'febootstrap-supermin-helper'.
- BR ocaml, ocaml-findlib-devel.
- No examples are provided with this version of febootstrap.

* Thu Nov 25 2010 Richard Jones <rjones@redhat.com> - 2.11-1
- New upstream version 2.11.
- Fixes "ext2fs_mkdir .. No free space in directory" bug which affects
  libguestfs on rawhide.

* Thu Oct 28 2010 Richard Jones <rjones@redhat.com> - 2.10-1
- New upstream version 2.10.
- Adds -u and -g options to febootstrap-supermin-helper which are
  required by virt-v2v.

* Fri Aug 27 2010 Richard Jones <rjones@redhat.com> - 2.9-1
- New upstream version 2.9.
- Fixes directory ordering problem in febootstrap-supermin-helper.

* Tue Aug 24 2010 Richard Jones <rjones@redhat.com> - 2.8-1
- New upstream version 2.8.

* Sat Aug 21 2010 Richard Jones <rjones@redhat.com> - 2.8-0.2
- New pre-release version of 2.8.
  + Note this is based on 2.7 + mailing list patches.
- New BRs on mke2fs, libext2fs, glibc-static.

* Fri May 14 2010 Richard Jones <rjones@redhat.com> - 2.7-2
- New upstream version 2.7.
- febootstrap-supermin-helper shell script rewritten in C for speed.
- This package contains C code so it is no longer 'noarch'.
- MAKEDEV isn't required.

* Fri Jan 22 2010 Richard Jones <rjones@redhat.com> - 2.6-1
- New upstream release 2.6.
- Recheck package in rpmlint.

* Thu Oct 22 2009 Richard Jones <rjones@redhat.com> - 2.5-2
- New upstream release 2.5.
- Remove BR upx (not needed by upstream).
- Two more scripts / manpages.

* Thu Jul 30 2009 Richard Jones <rjones@redhat.com> - 2.4-1
- New upstream release 2.4.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jun 15 2009 Richard Jones <rjones@redhat.com> - 2.3-1
- New upstream release 2.3.

* Mon Jun 15 2009 Richard Jones <rjones@redhat.com> - 2.2-1
- New upstream release 2.2.

* Mon May 11 2009 Richard Jones <rjones@redhat.com> - 2.0-1
- New upstream release 2.0.

* Thu May  7 2009 Richard Jones <rjones@redhat.com> - 1.9-1
- New upstream release 1.9.

* Fri May  1 2009 Richard Jones <rjones@redhat.com> - 1.8-1
- New upstream release 1.8.

* Mon Apr 20 2009 Richard Jones <rjones@redhat.com> - 1.7-1
- New upstream release 1.7.

* Tue Apr 14 2009 Richard Jones <rjones@redhat.com> - 1.5-3
- Configure script has (unnecessary) BuildRequires on fakeroot,
  fakechroot, yum.

* Tue Apr 14 2009 Richard Jones <rjones@redhat.com> - 1.5-2
- Initial build for Fedora.
