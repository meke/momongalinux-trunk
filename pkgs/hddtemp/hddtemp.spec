%global momorel 14
%global betaver 15
%global migfile %{_var}/run/systemd-migr_%{name}-%{version}-%{release}.%{_arch}

Summary: Hard disk temperature tool
Name: hddtemp
Version: 0.3
Release: 0.%{betaver}.%{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
Source0: http://www.guzu.net/files/hddtemp-%{version}-beta%{betaver}.tar.bz2 
NoSource: 0
# Source1 : http://www.guzu.net/linux/hddtemp.db
Source1: %{name}.db
Source2: %{name}.service
Source3: %{name}.sysconfig
Source4: %{name}.pam
Source5: %{name}.consoleapp
Patch0:  %{name}-db.patch
Patch1:  http://ftp.debian.org/debian/pool/main/h/hddtemp/hddtemp_0.3-beta15-48.diff.gz
Patch2:         %{name}-0.3-beta15-autodetect-717479.patch
Patch3:         %{name}-0.3-beta15-linguas-de.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): chkconfig
Requires(pre): usermode
Requires(preun): chkconfig
Requires(preun): usermode
BuildRequires: gettext
BuildRequires: perl

%description
hddtemp is a tool that gives you the temperature of your hard drive by
reading S.M.A.R.T. information.

%prep
%setup -q -n %{name}-%{version}-beta%{betaver}

cp -p %{SOURCE1} ./hddtemp.db
%patch0 -p0
%patch1 -p1 
%patch2 -p1
%patch3 -p1

perl -pi -e 's|/usr/share/misc/hddtemp\.db\b|%{_datadir}/misc/hddtemp.db|' \
  src/db.h doc/hddtemp.1
perl -pi -e \
  's|__ETCDIR__|%{_sysconfdir}|g ;
   s|__SBINDIR__|%{_sbindir}|g ;
   s|__INITDIR__|%{_initscriptdir}|g ;
   s|__LOCKDIR__|%{_localstatedir}/lock|g' \
  hddtemp.init

%build
%configure --with-db-path=%{_datadir}/misc/hddtemp.db
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -Dpm 644 %{SOURCE1} \
  %{buildroot}%{_datadir}/misc/hddtemp.db
install -Dpm 755 %{SOURCE2} \
  %{buildroot}%{_unitdir}/hddtemp.service
install -Dpm 644 %{SOURCE3} \
  %{buildroot}%{_sysconfdir}/sysconfig/hddtemp
install -dm 755 %{buildroot}%{_bindir}
ln -s consolehelper %{buildroot}%{_bindir}/hddtemp
install -Dpm 644 %{SOURCE4} \
  %{buildroot}%{_sysconfdir}/pam.d/hddtemp
install -Dpm 644 %{SOURCE5} \
  %{buildroot}%{_sysconfdir}/security/console.apps/hddtemp
%find_lang %{name}

%pre
rm -f %{migfile} &>/dev/null
if [ $1 -gt 1 ] && [ ! -e %{_unitdir}/hddtemp.service ] && \
   [ -e %{_initddir}/hddtemp ] ; then
    systemd-sysv-convert --save hddtemp &>/dev/null
    chkconfig --del hddtemp &>/dev/null
    touch %{migfile} &>/dev/null || :
fi

%post
[ $1 -eq 1 ] && systemctl daemon-reload &>/dev/null || :

%preun
if [ $1 -eq 0 ] ; then
  systemctl --no-reload disable hddtemp.service &>/dev/null
  systemctl stop hddtemp.service &>/dev/null || :
fi

%postun
systemctl daemon-reload &>/dev/null
[ $1 -gt 0 ] && systemctl try-restart hddtemp.service &>/dev/null || :

%triggerpostun -- %{name}
if [ $1 -gt 0 ] && [ -e %{migfile} ] ; then
  systemctl status hddtemp.service
  systemctl daemon-reload &>/dev/null
  systemctl status hddtemp.service
  systemctl try-restart hddtemp.service &>/dev/null
  systemctl status hddtemp.service
fi
rm -f %{migfile} &>/dev/null || :


%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc ChangeLog COPYING README TODO contribs
%config(noreplace) %{_sysconfdir}/sysconfig/hddtemp
%config %{_sysconfdir}/pam.d/hddtemp
%config %{_sysconfdir}/security/console.apps/hddtemp
%{_unitdir}/hddtemp.service
%{_bindir}/hddtemp
%{_sbindir}/hddtemp
%config %{_datadir}/misc/hddtemp.db
%{_mandir}/man8/hddtemp.8*

%changelog
* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-0.15.14m)
- update service file

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-0.15.13m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-0.15.12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-0.15.11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-0.15.10m)
- full rebuild for mo7 release

* Mon Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-0.15.9m)
- update hddtemp.db

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-0.15.8m)
- fix build error

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-0.15.7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-0.15.6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-0.15.5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-0.15.4m)
- %%NoSource -> NoSource

* Mon Oct 15 2007 YAMAZAKI Makoto <zaki@zakky.org>
- (0.3-0.15.3m)
- Apply patches from Debian containing bunch of hddtemp.db updates

* Tue Apr 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3-0.15.2m)
- add libata patch for new kernel

* Thu May  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3-0.15.1m)
- version up

* Wed Jul 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3-0.13.2m)
- add hddtemp.db

* Tue Jul  5 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-0.13.1m)
- update to 0.3-beta13

* Sun Nov  7 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-0.12.1m)
- update to 0.3-beta12

* Thu Jun  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.3-0.11.1m)
- update to 0.3-beta11
- update hddtemp.db to 03C (01/01/2004)

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3-0.10.1m)
- import from fc.

* Sun Dec 21 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.3-0.fdr.0.10.beta10
- Use mkstemp() to create the backtrace filename (bug 1112).

* Mon Dec 15 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.3-0.fdr.0.9.beta10
- Update to 0.3 beta 10.

* Sun Dec  7 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.9.beta9
- Update to 0.3 beta 9 and db version 03B.
- Move db to %%{_datadir}/misc to follow upstream.

* Sun Nov 23 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.9.beta8
- Use GUI=no in console.apps/hddtemp to avoid broken pipe messages.

* Thu Oct 30 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.8.beta8
- Update db (version 039).
- Specfile cleanups.

* Thu Aug 14 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.7.beta8
- Make daemon bind to 127.0.0.1 by default (bug 550).
- db is %%config again, still in %%{_datadir}/hddtemp (cf. hwdata) (bug 550).
- Init script cleanups (bug 550).
- Use chkconfig instead of LSB install/remove_initd until the script is LSB'd.

* Tue Aug 12 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.6.beta8
- Update to 0.3 beta 8.
- Move database to %%{_datadir}/hddtemp, it's not a config file.

* Sat Aug  2 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.5.beta7
- Use pam/consolehelper, grant local users access to hddtemp.
- Update db (version 036).

* Sat Jul 12 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.4.beta7
- Moved service condrestart to %%post in order to be able to cleanly upgrade
  from previous Fedora releases (bug 382).
- More init script cleanups according to new template.

* Fri Jun 20 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.3.beta7
- Update db (version 033), forgot it from 0.fdr.0.2.beta7.

* Tue Jun 17 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.2.beta7
- Update to 0.3beta7.
- Use %%{_initrddir} instead of %%{_sysconfdir}/init.d.
- Spec file and init script cleanups.

* Sun Apr 13 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.2.beta6
- Update db (version 02B).
- Save .spec in UTF-8.

* Fri Apr  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.1.beta6
- Update to 0.3beta6, new db.

* Sat Mar 22 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3-0.fdr.0.1.beta5
- Update to 0.3beta5 and current Fedora guidelines.
- Use LSB initd stuff instead of RH-specific.

* Fri Feb  7 2003 Ville Skytta <ville.skytta at iki.fi> - 0.3-0.beta3.fedora.1
- First Fedora release.
