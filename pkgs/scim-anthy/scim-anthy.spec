%global momorel 7
%global reldir 37309
%global scim_major_version 1.4.0

Summary: Scim-anthy is an SCIM IMEngine module for anthy
Name: scim-anthy
Version: 1.2.7
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://sourceforge.jp/projects/scim-imengine/
Source0: http://osdn.dl.sourceforge.jp/scim-imengine/%{reldir}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-1.2.6-romkan.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: scim >= 1.4.6 anthy >= 8300 kasumi >= 2.2
BuildRequires: scim-devel >= 1.4.6
BuildRequires: anthy-devel >= 8700
BuildRequires: automake19
BuildRequires: coreutils
Obsoletes: skim-scim-anthy

%description
Scim-anthy is an SCIM IMEngine module for anthy.
It supports Japanese input.

%prep
%setup -q 

%patch0 -p1 -b .romkan

cp -f %{_datadir}/automake-1.9/mkinstalldirs .

%build
%configure --disable-static
%make

%install
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of la file
find %{buildroot} -name "*.la" -delete

%clean
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/scim-1.0/%{scim_major_version}/IMEngine/*
%{_libdir}/scim-1.0/%{scim_major_version}/SetupUI/*
%{_libdir}/scim-1.0/%{scim_major_version}/Helper/*
%{_datadir}/scim/icons/*
%{_datadir}/locale/*/*/*
%{_datadir}/scim/Anthy

%changelog
* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-7m)
- Obsoletes: skim-scim-anthy

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-4m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-2m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Tue Apr 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-1m)
- update 1.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.5-2m)
- rebuild against gcc43

* Tue Mar  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-1m)
- update 1.2.5

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-3m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Jun  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-1m)
- version 1.2.4

* Sun Apr 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-1m)
- update 1.2.3

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-2m)
- delete libtool library

* Wed Nov 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-1m)
- up to 1.2.2

* Sat Oct 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-2m)
- Patch1: scim-anthy-1.2.1-Helper.patch

* Wed Oct 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-1m)
- up to 1.2.1

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-2m)
- rebuild against scim-1.4.4-0.20060301.5m

* Tue Aug  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- up to 1.2.0

* Sun Apr  2 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-1m)
- up to 1.0.0

* Sun Jan 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-1m)
- up to 0.9.0

* Wed Nov 30 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-1m)
- up to 0.8.0

* Tue Oct 18 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-1m)
- up to 0.7.1

* Thu Sep 29 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-1m)
- up to 0.7.0

* Sun Aug 14 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-1m)
- up to 0.6.1

* Thu Aug 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- up to 0.6.0

* Fri Jul 29 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-1m)
- up to 0.5.3

* Thu Jun 30 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-1m)
- up to 0.5.0

* Mon Jun 20 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.3-1m)
- up to 0.4.3

* Thu May 19 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.4.2-1m)
- up to 0.4.2

* Thu May 12 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.3.2-1m)
- up to 0.3.2

* Tue Apr 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.3.1-2m)
- apply roman-kana patch, "cya" line

* Tue Feb 4 2005 Meke <meke@apost.plala.or.jp>
- (0.3.1-1m)
- up 0.3.1

* Tue Dec 1 2004 Meke <meke@apost.plala.or.jp>
- (0.2.0-1m)
- merge: scim-uim & Mandrake scim-anthy
