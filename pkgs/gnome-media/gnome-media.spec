%global momorel 1
%define glib2_version 2.4.0
%define pango_version 1.4.0
%define gtk3_version 2.99.0
%define libgnome_version 2.13.7
%define libgnomeui_version 2.13.2
%define gail_version 1.2
%define desktop_file_utils_version 0.2.90
%define gstreamer_version 0.10.3

%define gettext_package gnome-media-2.0

Summary:        GNOME media programs
Name:           gnome-media
Version:        3.4.0
Release: %{momorel}m%{?dist}
License:        GPLv2+ and GFDL
Group:          Applications/Multimedia
#VCS: git:git://git.gnome.org/gnome-media
Source:         http://download.gnome.org/sources/gnome-media/3.4/gnome-media-%{version}.tar.xz
NoSource: 0

URL:            http://www.gnome.org
ExcludeArch:    s390 s390x

Requires(post):  GConf2 >= 2.14
Requires(pre):   GConf2 >= 2.14
Requires(preun): GConf2 >= 2.14

Requires: gnome-icon-theme-symbolic

BuildRequires:  gtk3-devel >= %{gtk3_version}
BuildRequires:  GConf2-devel
BuildRequires:  desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires:  gstreamer-devel >= %{gstreamer_version}
BuildRequires:  gstreamer-plugins-base-devel >= %{gstreamer_version}
BuildRequires:  gstreamer-plugins-good
BuildRequires:  libgnome-media-profiles-devel
BuildRequires:  gnome-doc-utils
BuildRequires:  intltool
BuildRequires:  control-center-devel
BuildRequires:  scrollkeeper

Obsoletes:	gnome-media-devel
Obsoletes:	gnome-media-libs

%description
This package contains a few media utilities for the GNOME desktop,
including a volume control and a configuration utility for audio profiles.

%package apps
Summary: Some media-related applications for the GNOME desktop
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description apps
This package contains an application to record and play sound files
in various formats and a configuration utility for the gstreamer media
framework.

%prep
%setup -q

%build
%configure \
        --disable-schemas-install \
        --enable-gnomecd=no \
        --enable-cddbslave=no
%make 

%install
make install DESTDIR=%{buildroot}

# show in all
desktop-file-install --vendor gnome --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
  %{buildroot}%{_datadir}/applications/gnome-sound-recorder.desktop

desktop-file-install --vendor "" --delete-original          \
  --dir %{buildroot}%{_datadir}/applications             \
  %{buildroot}%{_datadir}/applications/gstreamer-properties.desktop

rm -f %{buildroot}%{_datadir}/applications/vumeter.desktop
rm -f %{buildroot}%{_datadir}/applications/reclevel.desktop
rm -f %{buildroot}%{_bindir}/vumeter

rm -rf %{buildroot}%{_datadir}/gnome-media/sounds/
rm -rf %{buildroot}%{_datadir}/sounds/

rm -rf %{buildroot}/var/scrollkeeper

%find_lang %{gettext_package}
%find_lang gnome-sound-recorder --with-gnome
%find_lang gstreamer-properties --with-gnome
cat gnome-sound-recorder.lang >> apps.lang
cat gstreamer-properties.lang >> apps.lang

%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%post apps
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas \
    > /dev/null || :
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-cd.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-cd.schemas \
      %{_sysconfdir}/gconf/schemas/CDDB-Slave2.schemas \
      > /dev/null || :
  fi
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas \
      > /dev/null || :
  fi
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-volume-control.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-volume-control.schemas \
      > /dev/null || :
  fi
fi

%pre apps
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas \
      > /dev/null || :
  fi
fi

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-cd.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-cd.schemas \
      %{_sysconfdir}/gconf/schemas/CDDB-Slave2.schemas \
      > /dev/null || :
  fi
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas \
      > /dev/null || :
  fi
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-volume-control.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-volume-control.schemas \
      > /dev/null || :
  fi
fi

%preun apps
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  if [ -f %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas ] ; then
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas \
      > /dev/null || :
  fi
fi

%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor >&/dev/null || :
fi

%postun apps
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor >&/dev/null || :

%posttrans apps
gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor >&/dev/null || :

%files -f %{gettext_package}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING NEWS README

%files apps -f apps.lang
%defattr(-, root, root)
%{_bindir}/gnome-sound-recorder
%{_datadir}/gnome-sound-recorder
%config %{_sysconfdir}/gconf/schemas/gnome-sound-recorder.schemas
%{_datadir}/applications/gnome-sound-recorder.desktop
%{_datadir}/icons/hicolor/*/apps/gnome-sound-recorder*

%{_bindir}/gstreamer-properties
%{_datadir}/gstreamer-properties
%{_datadir}/applications/gstreamer-properties.desktop
%{_datadir}/icons/hicolor/*/apps/gstreamer-properties*


%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-9m)
- rebuild for glib 2.33.2

* Sat Dec 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-8m)
- add Require: sound-theme-freedesktop

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-7m)
- goodbye glade3 modules

* Thu Sep  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-6m)
- remove conflict files

* Sat May 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-5m)
- remove conflict icons

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-4m)
- remove conflict files in %%{_datadir}/sounds/gnome/default/alerts

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Mon Oct  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add g_debug patch

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0-7m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-6m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-5m)
- build fix

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-4m)
- fix req

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-3m)
- split libs

* Mon May 31 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0-2m)
- add BuildRequires

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Wed Jan 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.5-1m)
- update to 2.28.5

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
- delete patch0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-4m)
- fix autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-3m)
- good-bye gnome-volume-manager

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Fri Feb 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-2m)
- add patch0 for build (gnome-desktop change)

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-1m)
- update to 2.25.5

* Sat Jan 24 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0.1-3m)
- fix BuildPrereq

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0.1-2m)
- rebuild against rpm-4.6

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0.1-1m)
- update to 2.24.0.1

* Thu Sep  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-9m)
- good-bye vumeter from menu

* Wed Sep  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-8m)
- good-bye vumeter

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-7m)
- change %%preun script

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-6m)
- change BuildPrereq: gst-plugins-base-devel to gstreamer-plugins-base-devel
- change BuildPrereq: gst-plugins-good to gstreamer-plugins-good
- change Requires: gst-ffmpeg to gstreamer-ffmpeg

* Wed Jun 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-5m)
- add Requires: gst-ffmpeg

* Tue Apr  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-4m)
- add BuildPrereq

* Fri Apr  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-3m)
- fix BuildPreReq

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sun Jan 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-2m)
- remove dependency to gst-plugins-devel

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- remove category X-Red-Hat-Base X-Ximian-Main Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-m)
- delete libtool library

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-3m)
- rebuild against dbus-0.61

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Tue Jan  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-2m)
- rebuild against dbus-0.60-2m

* Mon Nov 21 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Sep  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.0-4m)
- rebuild against gstreamer-0.8.11-1m

* Fri Mar  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.0-3m)
- rebuild against gstreamer-0.8.9-1m

* Sun Feb 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.0-2m)
- add GNOME to Categories of gnome-volume-control.desktop
- BuildPrereq: desktop-file-utils

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0
- GNOME 2.8 Desktop

* Fri Jan  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.2-5m)
- rebuild against gstreamer-0.8.8-1m

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.2-4m)
- add auto commands before %%build for fix missing lib*.so problem

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.2-3m)
- rebuild against gstreamer-0.8.7-1m

* Sun Sep 19 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.2-2m)
- rebuild against gstreamer-0.8.5-1m

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Fri May 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.0-3m)
- del Obsoletes: gnome.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-2m)
- adjustment BuildPreReq

* Sat Apr 17 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0
- GNOME 2.6 Desktop
- change schemas from gnome-volume-control.schemas to gnome-audio-profiles.schemas
- change location of %%changelog

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.1-1m)
- version 2.4.1.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.7-1m)
- version 2.3.7

* Thu Jul 24 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.3.5-2m)
- add gstreamer-properties-segv.patch

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Fri Jul 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Thu May 29 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-2m)
- fix CDDB-Slave2.schemas (japanese only)

* Mon Mar 31 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.1-4m)
- rebuild against for XFree86-4.3.0

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.1.1-3m)
  rebuild against openssl 0.9.7a

* Wed Mar 05 2003 zunda <zunda at freeshell.org>
- (kossori)
- compilation fails without /usr/lib/libgstgconf-0.6.so

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.1-1m)
- version 2.2.1.1

* Mon Feb  3 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-2m)
- rebuild against for gstreamer,gst-plugins

* Sat Jan 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-2m)
- rebuild against for gstreamer,gst-plugins

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Wed Sep  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2.5-1m)
- version 2.0.2.5

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-20m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-19m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-18m)
- add BuildPrereq: gail-devel

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-17m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-16k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-14k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-12k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-10k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.999999999.0-10k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.999999999.0-8k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.999999999.0-6k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.999999999.0-4k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.999999999.0-2k)
- version 1.999999999.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.547.0-10k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.547.0-8k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.547.0-6k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.547.0-4k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.547.0-2k)
- version 1.547.0

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.520.2-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.520.2-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.520.2-2k)
- version 1.520.2

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.520.2-4k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.520.2-2k)
- version 1.520.2

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.520.0-2k)
- version 1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.287.113-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.287.113-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.287.113-2k)
- version 1.287.113

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-36k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-34k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-32k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-30k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-28k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-26k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-24k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-22k)
- change schema handring

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-20k)
- change depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-18k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-16k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-14k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-12k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-10k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-8k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-6k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-28k)
- rebuild against for bonobo-activation-0.9.4

* Fri Feb 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.176.0-2k)
- version 1.176.0

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-26k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-24k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-22k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-20k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-18k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-16k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-14k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-12k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-10k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-8k)
- rebuild against for libbonoboui-1.110.2

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-6k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-4k)
- rebuild against for libIDL-0.7.4

* Mon Feb  4 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0)
- version 1.112.0

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.3-8k)
- move zh_CN.GB2312 => zh_CN
- move zh_TW.Big5 => zh_TW

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.2.3-4k)
- rebuild against gettext 0.10.40.

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.2.3

* Fri Apr 27 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.2.2

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-5k)
- rebuild against audiofile-0.2.1.

* Thu Mar 29 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Sun Dec 24 2000 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (1.2.0-3k)
- update to 1.2.0.
- use %%configure and %%makeinstall.
- Add docs for gtcd(from rawhide).
- fix libs.

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.51-7k)
- fixed for FHS

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- chack spec file

* Sun Feb 20 2000 Shingo Akagaki <dora@kondara.org>
- added gtcd fontset patch

* Tue Sep 21 1999 Havoc Pennington <hp@redhat.com>
- Fixed gtcd so it works without corba-gtcd
- add DrMike's no-g_error() patch 

* Mon Sep 20 1999 Elliot Lee <sopwith@redhat.com>
- Update to 1.0.40

* Fri Sep 17 1999 Owen Taylor <otaylor@redhat.com>
- Don't keep device open in gtcd

* Thu Sep 9 1999 Owen Taylor <otaylor@redhat.com>
- Fixed warnings with previous

* Wed Sep 8 1999 Owen Taylor <otaylor@redhat.com>
- added --play option to gtcd

* Mon Aug 16 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.9.1

* Fri Mar 19 1999 Michael Fulbright <drmike@redhat.com>
- strip binaries

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.8

* Tue Jan 19 1999 Michael Fulbright <drmike@redhat.com>
- fixed building on sparc and RH 5.2 - seems to get confused into thinking
  we have cd changer support when we don't

* Wed Jan 06 1999 Michael Fulbright <drmike@redhat.com>
- updated to 0.99.1

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- updated for GNOME freeze

* Sat Nov 21 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>
- added spanish and french translations for rpm

* Wed Sep 23 1998 Michael Fulbright <msf@redhat.com>
- Updated to 0.30 release

* Mon Mar 16 1998 Marc Ewing <marc@redhat.com>
- Integrate into gnome-media CVS source tree
