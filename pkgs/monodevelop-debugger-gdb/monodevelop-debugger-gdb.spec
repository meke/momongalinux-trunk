%global momorel 2

Summary: monodevelop debugger gdb support
Name: monodevelop-debugger-gdb
Version: 2.6.0.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPL
URL: http://www.monodevelop.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: mono-devel >= 2.6.7
Requires: mono-core >= 2.6
Requires: mono-addins
BuildRequires: pkgconfig
BuildRequires: gtk-sharp2-devel >= 2.12.4
BuildRequires: gnome-sharp-devel >= 2.24.0
BuildRequires: gnome-desktop-sharp-devel
BuildRequires: gecko-sharp-2.0 >= 0.12
BuildRequires: mono-tools-devel >= 2.4
BuildRequires: mono-addins >= 0.5
BuildRequires: monodevelop >= 2.6
Obsoletes: monodevelop-debugger-mdb

%description
This is MonoDevelop which is intended to be a full-featured integrated
development environment (IDE) for mono and Gtk#.  It was originally a
port of SharpDevelop 0.98.  See http://monodevelop.com/ for more info.

%prep
%setup -q

%build
./configure --prefix=/usr --libdir=%{_libdir}
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{_prefix}/lib/monodevelop/AddIns/MonoDevelop.Debugger/*

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0.1-2m)
- rebuild for mono-2.10.9

* Fri Sep 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0.1-1m)
- update to 2.6.0.1
- Obsoletes: monodevelop-debugger-mdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-2m)
- use BuildRequires

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Sat Jan 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.1-5m)
- add patch0 (for rpm-4.6 pkgconfig)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-3m)
- rebuild against rpm-4.6

* Mon Dec 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-2m)
- specify --libdir at configure

* Sun Dec 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.1-1m)
- initial build (unstable Alpha 2)
