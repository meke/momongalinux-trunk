%global momorel 4

Name:       saxpath
Version:    1.0
Release:    %{momorel}m%{?dist}
Summary:    Simple API for XPath

Group:      Development/Libraries
License:    "Saxpath"
URL:        http://sourceforge.net/projects/saxpath/
Source0:    http://dl.sourceforge.net/saxpath/saxpath-%{version}.tar.gz
NoSource:   0
Source1:    %{name}-%{version}.pom
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ant
BuildRequires:  ant-junit
BuildRequires:  ant-trax

BuildArch:      noarch

%description
The SAXPath project is a Simple API for XPath. SAXPath is analogous to SAX
in that the API abstracts away the details of parsing and provides a simple
event based callback interface.

%package javadoc
Summary:    Javadoc for saxpath
Group:      Documentation

%description javadoc
Java API documentation for saxpath.

%prep
%setup -q -n %{name}-%{version}-FCS

find -type f -name "*.jar" -exec rm -f '{}' \;

%build
mkdir src/conf
touch src/conf/MANIFEST.MF

ant

# fix rpmlint warings: saxpath-javadoc.noarch: W: wrong-file-end-of-line-encoding /usr/share/javadoc/saxpath/**/*.css
for file in `find build/doc -type f | grep .css`; do
    %{__sed} -i 's/\r//g' $file
done

%install
rm -rf $RPM_BUILD_ROOT

# install jar
install -dm 755 $RPM_BUILD_ROOT/%{_javadir}
cp -p build/saxpath.jar $RPM_BUILD_ROOT/%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT/%{_javadir}/%{name}.jar

#install pom
install -dm 755 $RPM_BUILD_ROOT/%{_datadir}/maven2/poms
cp -p %{SOURCE1} $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-saxpath.pom

#depmap entry
%add_to_maven_depmap saxpath saxpath %{version}-FCS JPP saxpath

# install javadoc
install -dm 755 $RPM_BUILD_ROOT/%{_javadocdir}/%{name}
cp -a build/doc/* $RPM_BUILD_ROOT/%{_javadocdir}/%{name}/

%post
%update_maven_depmap

%postun
%update_maven_depmap

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_javadir}/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- import from Fedora 13

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0-1.6
- Fix GROUPS.
- Wrap changelog.

* Mon Jun 8 2009 Yong Yang <yyang@redhat.com> 1.0-1.5
- Fix "saxpath-javadoc.noarch: W: wrong-file-end-of-line-encoding
  /usr/share/javadoc/saxpath/**/*.css"
- Fix "saxpath.src: W: mixed-use-of-spaces-and-tabs
  (spaces: line 1, tab: line 6)"

* Wed May 13 2009 Fernando Nasser <fnasser@redhat.com> 1.0-1.4
- Fix license

* Tue Mar 10 2009 Yong Yang <yyang@redhat.com> 1.0-1.3
- rebuild with maven2 2.0.8 built in bootstrap mode

* Tue Jan 06 2009 Yong Yang <yyang@redhat.com> 1.0-1.2
- Import from dbhole's maven 2.0.8 packages

* Wed Dec 03 2008 Deepak Bhole <dbhole@redhat.com> 1.0-1.1
- Initial build.
