%global momorel 12
%global plugindir %{_libdir}/xmms/Input

Name: xmms-mad
Summary: Input plugin for xmms that uses libmad to decode MPEG layer 1/2/3 file and streams.
Version: 0.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL:    http://xmms-mad.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/xmms-mad/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: xmms-mad-0.8-bootstrap.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xmms >= 1.2.10-13m
Requires: libid3tag >= 0.15.0
Requires: libmad >= 0.15.0
BuildRequires: xmms-devel >= 1.2.10-13m
BuildRequires: gtk+1 >= 1.2.10-17m
BuildRequires: libid3tag-devel >= 0.15.0
BuildRequires: libmad >= 0.15.0
BuildRequires: pkgconfig

%description
xmms-mad is an input plugin for xmms that uses libmad to decode MPEG layer 1/2/3 file and streams.
 Current featured include:
    * local mp3 file playback
    * shoutchast/icecast stream playback
    * seeking
    * ID3 tag parsing
    * http header parsing

%prep
%setup -q
%patch0 -p1
./bootstrap

%build
%configure
%make libdir=%{plugindir}

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%make DESTDIR=%{buildroot} libdir=%{plugindir} install

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS INSTALL
%{plugindir}/libxmmsmad.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-7m)
- rebuild against rpm-4.6

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-6m)
- change BR from libid3tag to libid3tag-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-4m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-3m)
- delete libtool library

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8-2m)
- change installdir (/usr/X11R6 -> /usr)

* Thu Oct  6 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8-1m)
- version 0.8
- use %%{_lib}

* Sat Jan  1 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.5.5-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.5.5-1m)
- rebuild against xmms-1.2.8
- requires libmad, libid3tag

* Sat Jul 27 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (0.0.9-5m)
- add source tar ball.

* Sun May 12 2002 Toshiro HIKITA <toshi@sodan.org>
- (0.0.9-4k)
- requires mad-devel >= 0.14.2b

* Sun May 12 2002 Toshiro HIKITA <toshi@sodan.org>
- (0.0.9-2k)
- Initail Build,Kondarize
