%global momorel 1

Summary: FireWire interface
Name: libraw1394
Version: 2.1.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://sourceforge.net/projects/libraw1394/
#Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{name}-%{version}.tar.gz
Source0: https://www.kernel.org/pub/linux/libs/ieee1394/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: kernel-headers
BuildRequires: libtool
BuildRequires: pkgconfig

%description 
libraw1394 is the only supported interface to the kernel side raw1394 of
the Linux IEEE-1394 subsystem, which provides direct access to the connected
1394 buses to user space.  Through libraw1394/raw1394, applications can
directly send to and receive from other nodes without requiring a kernel driver
for the protocol in question.

The reason for making a library the interface to the kernel is to avoid
a program dependancy on the kernel version, which would hinder development and
optimization of raw1394.  If development changed the protocol and made it
incompatible with previous versions only the libraw1394 has to be upgraded to
match the kernel version (instead of all applications).

%package devel
Summary: Development and include files for libraw1394
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
libraw1394 is the only supported interface to the kernel side raw1394 of
the Linux IEEE-1394 subsystem, which provides direct access to the connected
1394 buses to user space.  Through libraw1394/raw1394, applications can
directly send to and receive from other nodes without requiring a kernel driver
for the protocol in question.

This archive contains the header-files and static libraries for libraw1394 development

%prep
%setup -q

%build
autoreconf -vif
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README
%{_bindir}/dumpiso
%{_bindir}/sendiso
%{_bindir}/testlibraw
%{_libdir}/libraw1394.so.*
%{_mandir}/man1/dumpiso.1*
%{_mandir}/man1/sendiso.1*
%{_mandir}/man1/testlibraw.1*
%{_mandir}/man5/isodump.5*

%files devel
%defattr(-,root,root)
%doc doc/libraw1394.sgml
%{_includedir}/libraw1394
%{_libdir}/libraw1394.a
%{_libdir}/libraw1394.so
%{_libdir}/pkgconfig/libraw1394.pc

%changelog
* Thu May 15 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-2m)
- update source (maybe source was replaced)

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.7-1m)
- update to 2.0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.5-3m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-2m)
- import patch from Fedora
- fix build issues

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2
-- remove all patches

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-3m)
- add libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-1m)
- version 1.3.0
- import 5 patches from Fedora
- modify %%post and %%postun
- add BR

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-2m)
- delete libtool library

* Tue Dec 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat May 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.0-1m)
- update to 1.2.0

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.0-1m)
- ver up.

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-5m)
- add Patch1: Patch0: libraw1394-0.9.0.gcc33.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.0-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-3m)
- move libraw1394.so to libraw1394-devel

* Thu Mar 21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9.0-2k)
- kondarize
- add libraw1394/ieee1394.h to %files section

* Mon Jun 04 2001 Harald Welte <laforge@gnumonks.org>
- redhat 7 compile...

* Mon Jun 04 2001 Harald Welte <laforge@conectiva.com>
+ libraw1394=0.9.0-1cl
- updated to 0.9.0

* Thu Apr 05 2001 Harald Welte <laforge@conectiva.com>
+ libraw1394-0.8.2-1cl
- initial conectiva RPM
- split devel package in devel / devel-static
- updated to 0.8.2

* Fri Jan 05 2001 Harald Welte <laforge@gnumonks.org>
- ported SPECfile to RedHat RPM
- libraw1394 0.8.1

* Mon Sep 11 2000 Lenny Cartier <lenny@mandrakesoft.com> 0.6-2mdk
- clean spec

* Mon Jul 03 2000 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.6-1mdk
- initial spec
