%global momorel 11

%define fontname serafettin-cartoon
%define fontconf 66-%{fontname}.conf
%define archivename %{name}-%{version}
%define fontforge_ver 20110222
%define fontforge_rel 2m.mo7

Name:          %{fontname}-fonts
Version:       0.5.1
Release:       %{momorel}m%{?dist}
Summary:       Sans-serif Cartoon Fonts
Group:         User Interface/X
License:       GPLv2+
URL:           http://serafettin.sourceforge.net/
Source0:       http://downloads.sourceforge.net/serafettin/%{archivename}.tar.bz2
BuildRoot:     %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:     noarch

BuildRequires: fontforge >= %{fontforge_ver}-%{fontforge_rel}
BuildRequires: fontpackages-devel

Requires:      fontpackages-filesystem

%description
Serafettin aims to be a collection of free Latin fonts for daily usage.
Currently it contains a free cartoon sans-serif font. It is based on Thukkaram 
Gopalrao's TSCu_Comic of tamillinux project. 

%prep
%setup -q -n %{archivename}


%build
make %{?_smp_flags}


%install
rm -fr %{buildroot}

DESTDIR=%{buildroot} make install

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{fontname}.conf \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc ChangeLog.txt COPYING.txt FONTLOG.txt README.txt
%dir %{_fontdir}


%changelog
* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-11m)
- use ">=" instead of "=" to specify fontforge version

* Fri Apr 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-10m)
- rebuild against fontforge-20110222-2m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-9m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-8m)
- rebuild against fontforge-20090923-7m

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-7m)
- rebuild for new GCC 4.5

* Wed Sep  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-6m)
- [BUILD FIX] specify Release of fontforge

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-5m)
- full rebuild for mo7 release

* Mon Apr  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-4m)
- specify fontforge version

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.1-1m)
- import from Fedora

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 0.5.1-2
- Make sure F11 font packages have been built with F11 fontforge

* Sat Feb 28 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.5.1-1
- New version with cleanups and fixes

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 13 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.5-1
- New version with various fixes suggested by review in RHBZ#479596

* Sun Jan 11 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.4-1
- New version with a name change (comic -> cartoon)

* Sun Dec 28 2008 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.3-1
- Initial release
