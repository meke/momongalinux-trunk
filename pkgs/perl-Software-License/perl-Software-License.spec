%global         momorel 2

Name:           perl-Software-License
Version:        0.103010
Release:        %{momorel}m%{?dist}
Summary:        Software::License Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Software-License/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Software-License-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Data-Section
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Sub-Install
BuildRequires:  perl-Test-Simple >= 0.96
BuildRequires:  perl-Text-Template
Requires:       perl-Data-Section
Requires:       perl-Sub-Install
Requires:       perl-Text-Template
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Software::License Perl module

%prep
%setup -q -n Software-License-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/Software/License
%{perl_vendorlib}/Software/License*.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103010-2m)
- rebuild against perl-5.20.0

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103010-1m)
- update to 0.103010

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103009-1m)
- update to 0.103009

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103008-2m)
- rebuild against perl-5.18.2

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103008-1m)
- update to 0.103008

* Sun Oct 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103007-1m)
- update to 0.103007

* Mon Oct 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103006-1m)
- update to 0.103006

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103005-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103005-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103005-2m)
- rebuild against perl-5.16.3

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103005-1m)
- update t0.103005

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103004-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103004-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103004-2m)
- rebuild against perl-5.16.0

* Thu Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103004-1m)
- update to 0.103004

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103003-1m)
- update to 0.103003

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103002-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103002-2m)
- rebuild against perl-5.14.1

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103002-1m)
- update to 0.103002

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103001-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103001-1m)
- update to 0.103001

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.103000-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103000-1m)
- update to 0.103000

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.102341-1m)
- update to 0.102341

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.102340-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.102340-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.102340-1m)
- update to 0.102340

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.101620-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.101620-1m)
- update to 0.101620

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.101600-1m)
- update to 0.101600

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.012-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.012-2m)
- rebuild against perl-5.12.0

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.012-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
