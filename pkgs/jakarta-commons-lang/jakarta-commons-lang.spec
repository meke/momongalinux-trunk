%global momorel 4

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define gcj_support 1

# If you don't want to build with maven, and use straight ant instead,
# give rpmbuild option '--without maven'

%define with_maven 0

%define base_name       lang
%define short_name      commons-%{base_name}

Name:           jakarta-%{short_name}
Version:        2.4
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Provides a host of helper utilities for the java.lang API
License:        "ASL 2.0"
Group:          Development/Libraries
URL:            http://commons.apache.org/lang/
Source0:        http://archive.apache.org/dist/commons/%{base_name}/source/%{short_name}-%{version}-src.tar.gz
Source1:        pom-maven2jpp-depcat.xsl
Source2:        pom-maven2jpp-newdepmap.xsl
Source3:        pom-maven2jpp-mapdeps.xsl
Source4:        %{short_name}-%{version}-jpp-depmap.xml
Patch0:         %{name}-notarget.patch
Patch1:         %{name}-addosgimanifest.patch
Patch2:         %{name}-encoding.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  ant
BuildRequires:  ant-junit
BuildRequires:  %{__perl}
%if %{with_maven}
BuildRequires:  maven >= 0:1.1
BuildRequires:  saxon
BuildRequires:  saxon-scripts
BuildRequires:  maven-plugin-changelog
BuildRequires:  maven-plugin-changes
BuildRequires:  maven-plugin-xdoc
%endif
Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2
%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description
The standard Java libraries fail to provide enough methods for
manipulation of its core classes. The Commons Lang Component provides
these extra methods.
The Commons Lang Component provides a host of helper utilities for the
java.lang API, notably String manipulation methods, basic numerical
methods, object reflection, creation and serialization, and System
properties. Additionally it contains an inheritable enum type, an
exception structure that supports multiple types of nested-Exceptions
and a series of utilities dedicated to help with building methods, such
as hashCode, toString and equals.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
BuildRequires:  java-javadoc

%description    javadoc
Javadoc for %{name}.

%if %{with_maven}
%package manual
Summary:        Documents for %{name}
Group:          Development/Documentation

%description manual
%{summary}.
%endif

%prep
%setup -q -n %{short_name}-%{version}-src
%{__perl} -pi -e 's/\r//g' *.txt

%patch0
%patch1
%patch2

%build
%if %{with_maven}
if [ ! -f %{SOURCE4} ]; then
export DEPCAT=$(pwd)/%{short_name}-%{version}-depcat.new.xml
echo '<?xml version="1.0" standalone="yes"?>' > $DEPCAT
echo '<depset>' >> $DEPCAT
for p in $(find . -name project.xml); do
    pushd $(dirname $p)
    /usr/bin/saxon project.xml %{SOURCE1} >> $DEPCAT
    popd
done
echo >> $DEPCAT
echo '</depset>' >> $DEPCAT
/usr/bin/saxon $DEPCAT %{SOURCE2} > %{short_name}-%{version}-depmap.new.xml
fi

for p in $(find . -name project.xml); do
    pushd $(dirname $p)
    cp project.xml project.xml.orig
    /usr/bin/saxon -o project.xml project.xml.orig %{SOURCE3} map=%{SOURCE4}
    popd
done

maven \
    -Dmaven.javadoc.source=1.4 \
    -Dmaven.repo.remote=file:/usr/share/maven/repository \
    -Dmaven.home.local=$(pwd)/.maven \
    jar javadoc xdoc:transform
%else

# FIXME: There are failures with gcj. Ignore them for now.
%if %{gcj_support}
  %ant \
    -Djunit.jar=$(find-jar junit) \
    -Dfinal.name=%{short_name} \
    -Djdk.javadoc=%{_javadocdir}/java \
    -Dtest.failonerror=false \
    jar javadoc
%else
  %ant \
    -Djunit.jar=$(find-jar junit) \
    -Dfinal.name=%{short_name} \
    -Djdk.javadoc=%{_javadocdir}/java \
    jar javadoc
%endif
#    test dist
%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
%if %{with_maven}
cp -p target/%{short_name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
%else
cp -p dist/%{short_name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
%endif
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|jakarta-||g"`; done)
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

%if %{with_maven}
%add_to_maven_depmap %{base_name} %{base_name} %{version} JPP %{name}

# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -m 644 pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}.pom
%endif

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
%if %{with_maven}
cp -pr target/docs/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
%else
cp -pr dist/docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
%endif
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

## manual
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
cp -p *.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
%if %{with_maven}
rm -rf target/docs/apidocs
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/site
cp -pr target/docs/* $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/site
%endif

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%if %{with_maven}
%post
%update_maven_depmap
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%update_maven_depmap
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif
%endif

%files
%defattr(-,root,root,-)
%dir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/*.txt
#%doc PROPOSAL.html LICENSE.txt NOTICE.txt RELEASE-NOTES.txt
%{_javadir}/*
%if %{with_maven}
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*
%endif
%if %{gcj_support}
%{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%if %{with_maven}
%files manual
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}/site
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-1m)
- sync with Fedora 13 (0:2.4-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2jpp.1.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2jpp.1.3m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3-2jpp.1.2m)
- release %%{_mavendepmapfragdir}, it's provided by jpackage-utils

* Thu Jul 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-2jpp.1.1m)
- sync Fedora
- changelog is below
-
- * Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0:2.3-2jpp.1
- - Autorebuild for GCC 4.3
- 
- * Tue Jan 22 2008 Permaine Cheung <pcheung@redhat.com> - 0:2.3-1jpp.1
- - Merge with upstream
- 
- * Mon Aug 13 2007 Ralph Apel <r.apel at r-apel.de> - 0:2.3-1jpp
- - Upgrade to 2.3
- - Build with maven by default
- - Add pom anf depmap frag
- - Make Vendor, Distribution based on macro

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-6jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-6jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1-6jpp.1m)
- import from Fedora

* Thu Mar 29 2007 Permaine Cheung <pcheung@redhat.com> - 0:2.1-6jpp.1
- Merge from upstream and rpmlint cleanup

* Thu Aug 10 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.1-5jpp.1
- Added missing requirements.
- Added missing postun section for javadoc.

* Thu Aug 10 2006 Karsten Hopp <karsten@redhat.de> - 0:2.1-4jpp_3fc
- Requires(post): coreutils

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:2.1-4jpp_2fc
- Rebuilt

* Wed Jul 19 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.1-4jpp_1fc
- Remove name/release/version defines as applicable.

* Mon Jul 17 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.1-3jpp
- Added conditional native compiling.
- By jkeating: Patched to not use taget= in build.xml

* Mon Feb 27 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.1-2jpp
- First JPP 1.7 build

* Sat Aug 20 2005 Ville Skytta <scop at jpackage.org> - 0:2.1-1jpp
- 2.1, javadoc crosslink patch applied upstream.
- Use the %%ant macro.

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:2.0-2jpp
- Rebuild with ant-1.6.2

* Sun Oct 12 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.0-1jpp
- Update to 2.0.
- Add non-versioned javadocs dir symlink.
- Crosslink with local J2SE javadocs.
- Convert specfile to UTF-8.

* Fri Apr  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0.1-3jpp
- Rebuild for JPackage 1.5.

* Tue Mar  4 2003 Ville Skytta <ville.skytta at iki.fi> - 1.0.1-2jpp
- Repackage to recover from earlier accidental overwrite with older version.
- No macros in URL and SourceX tags.
- Remove spurious api/ from installed javadoc path.
- Spec file cleanups.
- (from 1.0.1-1jpp) Update to 1.0.1.
- (from 1.0.1-1jpp) Run JUnit tests when building.

* Thu Feb 27 2003 Henri Gomez <hgomez@users.sourceforge.net> 1.0-3jpp
- fix ASF license and add packager tag

* Mon Oct 07 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0-2jpp
- missed to include changelog

* Mon Oct 07 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0-1jpp
- release 1.0

* Tue Aug 20 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0.b1.1-1jpp
- fist jpp release
