%global         momorel 1

Name:           perl-Catalyst-Runtime
Version:        5.90065
Release:        %{momorel}m%{?dist}
Summary:        Catalyst Framework Runtime
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Runtime/
Source0:        http://www.cpan.org/authors/id/J/JJ/JJNAPIORK/Catalyst-Runtime-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.4
BuildRequires:  perl-Carp
BuildRequires:  perl-CGI-Simple >= 1.109
BuildRequires:  perl-CGI-Struct
BuildRequires:  perl-Class-C3-Adopt-NEXT >= 0.07
BuildRequires:  perl-Class-Data-Inheritable
BuildRequires:  perl-Class-Load >= 0.12
BuildRequires:  perl-Data-Dump
BuildRequires:  perl-Data-OptList
BuildRequires:  perl-Encode >= 2.49
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Encode >= 2.49
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-HTTP-Body >= 1.06
BuildRequires:  perl-HTTP-Message >= 6.00
BuildRequires:  perl-HTTP-Request-AsCGI >= 1.0
BuildRequires:  perl-IO-stringy
BuildRequires:  perl-JSON-MaybeXS >= 1.000000
BuildRequires:  perl-libwww-perl >= 5.814
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Module-Pluggable >= 3.9
BuildRequires:  perl-Moose >= 2.0001
BuildRequires:  perl-MooseX-Emulate-Class-Accessor-Fast >= 0.00903
BuildRequires:  perl-MooseX-Getopt >= 0.48
BuildRequires:  perl-MooseX-MethodAttributes >= 0.24
BuildRequires:  perl-MooseX-Role-WithOverloading >= 0.09
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-namespace-autoclean >= 0.09
BuildRequires:  perl-namespace-clean >= 0.23
BuildRequires:  perl-Path-Class >= 0.09
BuildRequires:  perl-Plack >= 0.9991
BuildRequires:  perl-Plack-Middleware-FixMissingBodyInRedirect >= 0.09
BuildRequires:  perl-Plack-Middleware-MethodOverride
BuildRequires:  perl-Plack-Middleware-RemoveRedundantBody >= 0.03
BuildRequires:  perl-Plack-Middleware-ReverseProxy >= 0.04
BuildRequires:  perl-Plack-Test-ExternalServer
BuildRequires:  perl-Safe-Isa >= 1.000002
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Stream-Buffered
BuildRequires:  perl-String-RewritePrefix >= 0.004
BuildRequires:  perl-Sub-Exporter
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Text-Balanced
BuildRequires:  perl-Text-SimpleTable >= 0.03
BuildRequires:  perl-Time-HiRes
BuildRequires:  perl-Tree-Simple >= 1.15
BuildRequires:  perl-Tree-Simple-VisitorFactory
BuildRequires:  perl-Try-Tiny >= 0.17
BuildRequires:  perl-URI >= 1.35
Requires:       perl-Carp
Requires:       perl-CGI-Simple >= 1.109
Requires:       perl-CGI-Struct
Requires:       perl-Catalyst-DispatchType-Regex >= 5.90032
Requires:       perl-Catalyst-Plugin-Params-Nested
Requires:       perl-Class-C3-Adopt-NEXT >= 0.07
Requires:       perl-Class-Load >= 0.12
Requires:       perl-Data-Dump
Requires:       perl-Data-OptList
Requires:       perl-Encode >= 2.49
Requires:       perl-Hash-MultiValue
Requires:       perl-HTML-Parser
Requires:       perl-HTTP-Body >= 1.06
Requires:       perl-HTTP-Message >= 6.00
Requires:       perl-HTTP-Request-AsCGI >= 1.0
Requires:       perl-JSON-MaybeXS >= 1.000000
Requires:       perl-libwww-perl >= 5.814
Requires:       perl-List-MoreUtils
Requires:       perl-Module-Pluggable >= 3.9
Requires:       perl-Moose >= 2.0001
Requires:       perl-MooseX-Emulate-Class-Accessor-Fast >= 0.00903
Requires:       perl-MooseX-Getopt >= 0.48
Requires:       perl-MooseX-MethodAttributes >= 0.24
Requires:       perl-MooseX-Role-WithOverloading >= 0.09
Requires:       perl-MRO-Compat
Requires:       perl-namespace-autoclean >= 0.09
Requires:       perl-namespace-clean >= 0.23
Requires:       perl-Path-Class >= 0.09
Requires:       perl-Plack >= 0.9991
Requires:       perl-Plack-Middleware-FixMissingBodyInRedirect >= 0.09
Requires:       perl-Plack-Middleware-MethodOverride
Requires:       perl-Plack-Middleware-RemoveRedundantBody >= 0.03
Requires:       perl-Plack-Middleware-ReverseProxy >= 0.04
Requires:       perl-Plack-Test-ExternalServer
Requires:       perl-Path-Class >= 0.09
Requires:       perl-Safe-Isa >= 1.000002
Requires:       perl-Scalar-Util
Requires:       perl-Stream-Buffered
Requires:       perl-String-RewritePrefix >= 0.004
Requires:       perl-Sub-Exporter
Requires:       perl-Task-Weaken
Requires:       perl-Text-Balanced
Requires:       perl-Text-SimpleTable >= 0.03
Requires:       perl-Time-HiRes
Requires:       perl-Tree-Simple >= 1.15
Requires:       perl-Tree-Simple-VisitorFactory
Requires:       perl-Try-Tiny >= 0.17
Requires:       perl-URI >= 1.35

Obsoletes:      perl-Catalyst-Plugin-Unicode-Encoding

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is the primary class for the Catalyst-Runtime distribution,
version 5.80.

%prep
%setup -q -n Catalyst-Runtime-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/^perl(Catalyst::Plugin::Unicode::Encoding/d'

EOF
%define __perl_requires %{_builddir}/AnyEvent-%{real_ver}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

# make common directory
mkdir -p %{buildroot}%{perl_vendorlib}/Catalyst/Plugin

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{_bindir}/catalyst.pl
%{perl_vendorlib}/*
%{perl_vendorlib}/Catalyst/Plugin
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90065-1m)
- rebuild against perl-5.20.0
- update to 5.90065

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90063-1m)
- update to 5.90063

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90062-1m)
- update to 5.90062

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90061-1m)
- update to 5.90061

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90060-1m)
- update to 5.90060
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90053-1m)
- update to 5.90053

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90051-1m)
- update to 5.90051

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90042-3m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90042-2m)
- add Obsoletes: perl-Catalyst-Plugin-Unicode-Encoding
- perl-Catalyst-Plugin-Unicode-Encoding is merged into perl-Catalyst-Runtime

* Sun Jun 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90042-1m)
- update to 5.90042

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90041-1m)
- update to 5.90041

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90030-2m)
- rebuild against perl-5.18.0

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90030-1m)
- update to 5.90030

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90020-2m)
- rebuild against perl-5.16.3

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90020-1m)
- update to 5.90020

* Thu Dec  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90019-1m)
- update to 5.90019

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90018-2m)
- rebuild against perl-5.16.2

* Wed Oct 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90018-1m)
- update to 5.90018

* Sat Oct 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90017-1m)
- update to 5.90017

* Fri Aug 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90016-1m)
- update to 5.90016

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90015-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90015-1m)
- update to 5.90015
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90011-1m)
- update to 5.90011

* Tue Jan 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90007-2m)
- enable test

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90007-1m)
- update to 5.90007

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90006-2m)
- add BuildRequires: perl-Class-Load >= 0.12
- add BuildRequires: perl-HTTP-Message >= 6.00
- add BuildRequires: perl-MooseX-Types-LoadableClass >= 0.003
- add BuildRequires: perl-Plack >= 0.9974

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90006-1m)
- update to 5.90006

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.irg>
- (5.90005-1m)
- update to 5.90005

* Wed Oct 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90004-1m)
- update to 5.90004

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90003-1m)
- update to 5.90003

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90002-2m)
- rebuild against perl-5.14.2

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90002-1m)
- update to 5.90002

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.90001-1m)
- update to 5.90001

* Tue Aug  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80033-1m)
- update to 5.80033

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80032-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80032-4m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80032-3m)
- rebuild against perl-Moose-2.0001

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.80032-2m)
- rebuild for new GCC 4.6

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80032-1m)
- update to 5.80032

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80031-1m)
- update to 5.80031

* Wed Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80030-1m)
- update to 5.80030

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.80029-2m)
- rebuild for new GCC 4.5

* Mon Oct  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80029-1m)
- update to 5.80029

* Wed Sep 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80028-1m)
- update to 5.80028

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80027-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80027-1m)
- update to 5.80027

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.80025-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80025-1m)
- update to 5.80025

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80024-2m)
- rebuild against perl-5.12.1

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80024-1m)
- update to 5.80024

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80023-1m)
- update to 5.80023

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80022-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80022-1m)
- update to 5.80022

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80021-1m)
- update to 5.80021

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80020-1m)
- update to 5.80020

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80019-1m)
- update to 5.80019

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80018-1m)
- update to 5.80018

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80017-1m)
- update to 5.80017

* Fri Jan  1 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.80016-2m)
- update BuildRequires:  perl-HTTP-Request-AsCGI >= 1.0
- add BuildRequires:  perl-MooseX-Getopt >= 0.25

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80016-1m)
- update to 5.80016

* Sat Dec 12 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.80015-2m)
- revised BR

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80015-1m)
- update to 5.80015

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80014-1m)
- update to 5.80014

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.80013-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.80013-2m)
- revised BR version

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80013-1m)
- update to 5.80013

* Fri Sep 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80012-1m)
- update to 5.80012

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80011-1m)
- update to 5.80011

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80007-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80007-1m)
- update to 5.80007

* Sun Jul  5 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.80006-2m)
- add BuildRequires:  perl-namespace-autoclean

* Wed Jul  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80006-1m)
- update to 5.80006

* Tue Jun 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80005-3m)
- remove BuildRequires: perl-Text-Balanced and Requires: perl-Text-Balanced

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80005-2m)
- modify BuildRequires and Requires

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.80005-1m)
- update to 5.80005

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.71001-1m)
- update to 5.71001

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.71000-1m)
- update to 5.71000

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7015-2m)
- rebuild against rpm-4.6

* Thu Oct 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7015-1m)
- update to 5.7015

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7014-1m)
- update to 5.7014

* Sun May 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7013-1m)
- update to 5.7013

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7012-2m)
- rebuild against gcc43

* Fri Dec 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7012-1m)
- update to 5.7012

* Fri Oct 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7011-1m)
- update to 5.7011

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7010-1m)
- update to 5.7010

* Tue Aug 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7008-2m)
- update perl-HTTP-Body >= 0.9

* Tue Aug 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7008-1m)
- update to 5.7008

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.7007-4m)
- use vendor

* Sat Mar 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7007-1m)
- update to 5.7007

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7003-3m)
- rebuild against perl-CGI-Simple-0.078-1m
- perl-Cgi-Simple changes its name to "perl-CGI-Simple"

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7003-2m)
- delete dupclicate directory

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.7003-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
