%global momorel 7
Summary: Ruby Manual
Name: ruby-man
Version: 1.8.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Documentation
Source0: http://www7.tok2.com/home/misc/files/ruby/refm/temp/ruby-refm-rdp-%{version}-ja-html.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Obsoletes: ruby-man-jp

%description
Ruby is the interpreted scripting language for quick and easy object-oriented
programming. It has many features to process text files and to do system
management tasks (as in perl). It is simple, straight-forward, and extensible.

This package provide the Reference Manual of Ruby.

%prep
%setup -q -c -T
tar xzf %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(- ,root, root)
%doc *.html *.css *.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.2-2m)
- rebuild against gcc43

* Wed Apr 13 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-1m)
- update 1.8.2

* Sat May 10 2003 Kenta MURATA <muraken@momonga-linux.org>
- (1.6.8-2m)
- update source file.

* Wed Jan 22 2003 Kenta MURATA <muraken@momonga-linux.org>
- (1.6.8-1m)
- update to 1.6.8 (separated HTML version).

* Tue May 14 2002 Kenta MURATA <muraken@kondara.org>
- (1.6.7-2k)
- update to 1.6.7 (separated HTML version).

* Wed Feb 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.6.6-2k)
- update to 1.6.6

* Sun Feb 18 2001 Kenichi Matsubara <m@kondara.org>
- Obsoletes ruby-man-jp.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1. meanless...

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Sep 26 1999 Toru Hoshina <hoshina@best.com>
- version up.

* Tue Aug 24 1999 Toru Hoshina <hoshina@best.com>
- 1st release.
