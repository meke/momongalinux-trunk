%global momorel 6

Summary: BLOP comprises a set of LADSPA plugins
Name: ladspa-blop-plugins
Version: 0.2.8
Release: %{momorel}m%{?dist}
License: GPL
URL: http://blop.sourceforge.net/
Group: Applications/Multimedia
Source0:  http://dl.sourceforge.net/sourceforge/blop/blop-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel
BuildRequires: gettext

%description
BLOP comprises a set of LADSPA plugins that generate bandlimited sawtooth, square, variable pulse and slope-variable triangle waves, for use in LADSPA hosts, principally for use with one of the many modular software synthesisers available.

They are wavetable based, and are designed to produce output with harmonic content as high as possible over a wide pitch range.

%prep
%setup -q -n blop-%{version}

%build
%configure --with-ladspa-prefix=%{_prefix} \
           --with-ladspa-plugin-dir=%{_libdir}/ladspa
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-, root, root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL README
%doc NEWS THANKS TODO
%{_libdir}/ladspa/*.so
%{_libdir}/ladspa/blop_files
%{_datadir}/locale/*/LC_MESSAGES/blop.mo

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.8-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.8-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.2.8-1m)
- initial build for Momonga Linux
