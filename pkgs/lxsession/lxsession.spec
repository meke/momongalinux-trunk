%global momorel 5

Name:           lxsession
Version:        0.4.4
Release:        %{momorel}m%{?dist}
Summary:        Lightweight X11 session manager

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.org/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel > 2.6 dbus-glib-devel
BuildRequires:  docbook-utils intltool gettext
# name changed back from lxsession-lite to lxsession
Obsoletes:      lxsession-lite < 0.3.6-5m
Provides:       lxsession-lite = %{version}-%{release}
# lxde-settings-daemon was merged into lxsession
Obsoletes:      lxde-settings-daemon < 0.4.1-4m
Provides:       lxde-settings-daemon = 0.4.1-4m

%description
LXSession is a standard-compliant X11 session manager with shutdown/
reboot/suspend support via HAL. In connection with gdm it also supports user 
switching.

LXSession is derived from XSM and is developed as default X11 session manager 
of LXDE, the Lightweight X11 Desktop Environment. Though being part of LXDE, 
it's totally desktop-independent and only has few dependencies.

%prep
%setup -q

%build
%configure LIBS="-lXau -lX11"
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'
%find_lang %{name}
mkdir -p -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/xdg/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README desktop.conf.example
%{_bindir}/%{name}
%{_bindir}/%{name}-logout
%{_datadir}/%{name}/
%{_mandir}/man*/%{name}*.*
# we need to own
%dir %{_sysconfdir}/xdg/%{name}

%changelog
* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-5m)
- remove BR hal

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4
- obsolete and provide lxde-settings-daemon

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-3m)
- explicitly link libXau

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-1m)
- import from Fedora 11

* Mon May 18 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.8-1
- Update to 0.3.8 and remove all patches
- Rename back to lxsession again (upstream)

* Mon Apr 27 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.6-5
- Fix Session name (Andrew Lee)

* Mon Apr 27 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.6-4
- Support user switching also with newer gdm
- Add patch to fix icon search path (Marty Jack)

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Oct 09 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.6-2
- Preserve timestamps during install

* Thu Jun 12 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.6-1
- Update to 0.3.6
- Remove docbook hack

* Sun Apr 20 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.5-1
- Switch to LXSession lite and drop xorg-x11-xsm requirement again.

* Sat Apr 12 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2-2
- Require xorg-x11-xsm

* Sat Apr 12 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2-1
- Update to 0.3.2

* Mon Mar 10 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-1
- Initial Fedora RPM
