%global momorel 1

%define lang en
%define langrelease 0
%define aspellversion 6
Summary: English dictionaries for Aspell
Name: aspell-%{lang}
Version: 7.1
Release: %{momorel}m%{?dist}
License: MIT and BSD
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell%{aspellversion}-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60.6-8m
Requires: aspell >= 0.60.6-8m
Obsoletes: aspell-en-gb <= 0.33.7.1
Obsoletes: aspell-en-ca <= 0.33.7.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: English, Canadian
English, British English

%prep
%setup -q -n aspell%{aspellversion}-%{lang}-%{version}-%{langrelease}

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Wed Sep  7 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (7.1-1m)
- version up 7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.0-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-5m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (6.0-4m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0.0-3m)
- rebuild against gcc43

* Sat Aug 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (6.0.0-1m)

* Sun May  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.51.1-1m)
- update

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.51-0.3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- allow users to see the documents

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.51-0.2m)
- change source URL(reported by SIVA)

* Thu Mar 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.51-0.1m)
- initial version 0.51-0
