%global momorel 1
%global docdeveldir %{_docdir}/%{name}-devel-%{version}
%global docdir %{_docdir}/%{name}-%{version}

Summary: A Portable Open Source UPnP Development Kit
Name: libupnp
Version: 1.6.18
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://pupnp.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/pupnp/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: upnp
Obsoletes: upnp

%description
The UPnP SDK for Linux provides developers with an API and open source
code for building control points, devices, and bridges that are
compliant with Version 1.0 of the UPnP Device Architecture
Specification.  Intel Corporation originally developed this SDK and
has donated it to the open source community through SourceForge.

For general information about the UPnP SDK for Linux, and to learn more
about how it helps the industry, please visit Intel's Universal Plug
and Play web site at http://www.intel.com/ial/upnp/.

%package devel
Summary: UPnP SDK devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: upnp-devel
Obsoletes: upnp-devel

%description devel
UPnP SDK

%prep
%setup -q

%build
%configure --with-documentation --enable-static=no

%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} LIBDIR=%{_libdir} install

#create the doc devel dir
%{__mkdir_p} %{buildroot}%{docdeveldir}

#mv examples dir and pdf file to the doc devel dir
mv %{buildroot}%{docdir}/examples \
	%{buildroot}%{docdeveldir}/
mv %{buildroot}%{docdir}/UPnP_Programming_Guide.pdf \
	%{buildroot}%{docdeveldir}/
mv %{buildroot}%{docdir}/IXML_Programming_Guide.pdf \
	%{buildroot}%{docdeveldir}/
mv %{buildroot}%{docdir}/html \
	%{buildroot}%{docdeveldir}/

rm %{buildroot}%{_libdir}/{libixml.la,libthreadutil.la,libupnp.la}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc %{docdir}
%{_libdir}/libixml.so.*
%{_libdir}/libthreadutil.so.*
%{_libdir}/libupnp.so.*

%files devel
%defattr(-,root,root)
%doc %{docdeveldir}
%{_includedir}/upnp/
%{_libdir}/libixml.so
%{_libdir}/libthreadutil.so
%{_libdir}/libupnp.so
%{_libdir}/pkgconfig/libupnp.pc

%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.18-1m)
- [SECURITY] CVE-2012-5958 CVE-2012-5959 CVE-2012-5960 CVE-2012-5961 CVE-2012-5962
- [SECURITY] CVE-2012-5963 CVE-2012-5964 CVE-2012-5965
- update to 1.6.18

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.13-1m)
- update 1.6.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.6-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.6-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.6-1m)
- update 1.6.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5-2m)
- rebuild against gcc43

* Fri Feb 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.5-1m)
- rename package. upnp to libupnp
- update 1.6.5

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-4m)
- add gcc4 patch
- Patch3: upnp-1.0.4-gcc4.patch

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.4-3m)
- enable x86_64.

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- s/Copyright:/License:/

* Sun Jan 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.4-1m)
- version 1.0.4
