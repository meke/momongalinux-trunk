%global momorel 4

%define fontname paktype-naskh-basic
%global fontconf 67-%{fontname}
%define fontdir %{_datadir}/fonts/%{fontname}

# Common description
%define common_desc \
The paktype-naskh-basic-fonts package contains fonts for the display of \
Arabic, Farsi, Urdu and Sindhi from PakType by Lateef Sagar.

Name:    %{fontname}-fonts
Version: 3.0
Release: %{momorel}m%{?dist}
License: "GPLv2 with exceptions"
URL: https://sourceforge.net/projects/paktype/
Source0: http://downloads.sourceforge.net/project/paktype/NaskhBasic-3.0.tar.gz
Source1: %{fontconf}-sa.conf
Source2: %{fontconf}-sindhi.conf
Source3: %{fontconf}-farsi.conf
Source4: %{fontconf}-urdu.conf
Source5: %{fontconf}.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires:  fontpackages-devel
Requires:       %{name}-common 
Group: User Interface/X
Summary: Fonts for Arabic, Farsi, Urdu and Sindhi from PakType

 
%description
%common_desc

%package common
Summary:  Common files for paktype-naskh fonts
Group:  User Interface/X
Requires: fontpackages-filesystem
Obsoletes: paktype-fonts
%description common
%common_desc


%package -n %{fontname}-farsi-fonts
Summary: Font for Farsi from PakType
Group: User Interface/X
Requires: %{name}-common = %{version}-%{release}
%description -n %{fontname}-farsi-fonts
%common_desc 
This package provides a free Farsi truetype/opentype font 

%_font_pkg -n farsi -f %{fontconf}-farsi.conf PakTypeNaskhBasicFarsi.ttf

%package -n %{fontname}-sa-fonts
Summary: Fonts for Arabic, Farsi, Urdu and Sindhi from PakType
Group: User Interface/X
Requires: %{name}-common = %{version}-%{release}
%description -n %{fontname}-sa-fonts
%common_desc

%_font_pkg -n sa -f %{fontconf}-sa.conf PakTypeNaskhBasicSA.ttf

%package -n %{fontname}-sindhi-fonts
Summary: Font for Sindhi from PakType
Group: User Interface/X
Requires: %{name}-common = %{version}-%{release}
%description -n %{fontname}-sindhi-fonts
%common_desc 
This package provides a free Sindhi truetype/opentype font 

%_font_pkg -n sindhi -f %{fontconf}-sindhi.conf PakTypeNaskhBasicSindhi.ttf

%package -n %{fontname}-urdu-fonts
Summary: Font for Urdu from PakType
Group: User Interface/X
Requires: %{name}-common = %{version}-%{release}
%description -n %{fontname}-urdu-fonts
%common_desc 
This package provides a free Urdu truetype/opentype font 

%_font_pkg -n urdu -f %{fontconf}-urdu.conf PakTypeNaskhBasicUrdu.ttf

%_font_pkg  -f %{fontconf}.conf PakTypeNaskhBasic.ttf

%prep
%setup -q -c
mv NaskhBasic-3.0/* .
mv Ready\ to\ use\ fonts/* .
mv PakType\ Naskh\ Basic\ Farsi.ttf PakTypeNaskhBasicFarsi.ttf
mv PakType\ Naskh\ Basic.ttf PakTypeNaskhBasic.ttf
mv PakType\ Naskh\ Basic\ SA.ttf PakTypeNaskhBasicSA.ttf
mv PakType\ Naskh\ Basic\ Urdu.ttf PakTypeNaskhBasicUrdu.ttf
mv PakType\ Naskh\ Basic\ Sindhi.ttf PakTypeNaskhBasicSindhi.ttf
mv License\ files/* .
mv PakType\ Naskh\ Basic\ Comparison\ Chart.htm  PakType_Naskh_Basic_Comparison_Chart.htm
mv PakType\ Naskh\ Basic\ Comparison\ Chart.pdf PakType_Naskh_Basic_Comparison_Chart.pdf
%{__sed} -i 's/\r//'  PakType_Naskh_Basic_Comparison_Chart.htm
mv PakType\ Naskh\ Basic\ License.txt  PakType_Naskh_Basic_License.txt
%{__sed} -i 's/\r//' PakType_Naskh_Basic_License.txt
chmod a-x PakType_Naskh_Basic_Comparison_Chart.htm PakType_Naskh_Basic_License.txt PakType_Naskh_Basic_Comparison_Chart.pdf
for txt in Readme.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\x92//g' $txt.new
   sed -i 's/\x93//g' $txt.new
   sed -i 's/\x94//g' $txt.new
   sed -i 's/\x96//g' $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done

%build
echo "Nothing to do in Build."

%install
rm -rf $RPM_BUILD_ROOT
install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0644 -p PakTypeNaskhBasicFarsi.ttf PakTypeNaskhBasic.ttf PakTypeNaskhBasicSA.ttf PakTypeNaskhBasicUrdu.ttf PakTypeNaskhBasicSindhi.ttf $RPM_BUILD_ROOT%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-sa.conf


install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-sindhi.conf

install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-farsi.conf


install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-urdu.conf

install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf


for fconf in %{fontconf}-sa.conf \
             %{fontconf}-sindhi.conf \
             %{fontconf}-farsi.conf \
             %{fontconf}-urdu.conf \
             %{fontconf}.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done



%clean
rm -rf $RPM_BUILD_ROOT


%files common
%defattr(-,root,root,-)
%dir %{fontdir}
%doc PakType_Naskh_Basic_Comparison_Chart.htm PakType_Naskh_Basic_License.txt PakType_Naskh_Basic_Comparison_Chart.pdf Readme.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-1m)
- import from Fedora 13
- Obsoletes: paktype-fonts

* Thu May 6 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-7
- remove binding="same" from all *.conf files
- resolves Bug#586797

* Fri Mar 12 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-6
- Changes in summary and description

* Fri Mar 12 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-5
- changed the name of package from paktype-nashk-basic to paktype-naskh-basic

* Fri Mar 9 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-4
- removed redundant  BuildRequires from specfile
- removed unnecessary rm/rmdir's from specfile
- Sane updates in docs.

* Fri Mar 5 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-3
- removed all cd's
- changes w.r.t sed in prep section
- added .conf file for PakTypeNaskhBasic.ttf
- files section added for common
- removed space from 67-*-sindhi.conf

* Mon Feb 15 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-2
- Re-packing with updated License information.
- Changes in Spec file with new upstream source.
- Added conf files

* Mon Feb 15 2010 Naveen Kumar <nkumar@redhat.com> - 3.0-1
- Initial packaging for version-3.0
