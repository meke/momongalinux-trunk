%global momorel 1

Summary:       Gnome Partition Editor
Name:          gparted
Version:       0.16.1
Release:       %{momorel}m%{?dist}
Group:         Applications/System
License:       GPLv2+
URL:           http://gparted.sourceforge.net
Source0:       http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource:      0
Source1:       gparted-console.apps
Source2:       gparted-pam.d
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtkmm-devel parted-devel >= 3.1
BuildRequires: libuuid-devel gettext perl(XML::Parser) 
BuildRequires: desktop-file-utils gnome-doc-utils intltool
BuildRequires:  rarian-compat
BuildRequires:  pkgconfig

%description
GParted stands for Gnome Partition Editor and is a graphical frontend to
libparted. Among other features it supports creating, resizing, moving
and copying of partitions. Also several (optional) filesystem tools provide
support for filesystems not included in libparted. These optional packages
will be detected at runtime and don't require a rebuild of GParted

%prep
%setup -q 

%build
%configure --enable-libparted-dmraid
make %{?_smp_mflags} 

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

sed -i 's#_X-GNOME-FullName#X-GNOME-FullName#' %{buildroot}%{_datadir}/applications/%{name}.desktop

desktop-file-install --delete-original                 \
        --vendor=                                      \
        --dir %{buildroot}%{_datadir}/applications     \
        --mode 0644                                    \
        --remove-category GNOME                        \
        --add-category System                          \
        %{buildroot}%{_datadir}/applications/%{name}.desktop
sed -i 's#sbin#bin#' %{buildroot}%{_datadir}/applications/%{name}.desktop

#### consolehelper stuff
mkdir -p %{buildroot}%{_bindir}
ln -s consolehelper %{buildroot}%{_bindir}/gparted

mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps
cp %{SOURCE1} %{buildroot}%{_sysconfdir}/security/console.apps/gparted

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
cp %{SOURCE2} %{buildroot}%{_sysconfdir}/pam.d/gparted
 
%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/gparted
%{_sbindir}/gparted
%{_sbindir}/gpartedbin
%{_datadir}/applications/gparted.desktop
%{_datadir}/icons/hicolor/*/apps/gparted.*
%{_datadir}/gnome/help/gparted/
%{_datadir}/omf/gparted/
%{_mandir}/man8/gparted.*
%config(noreplace) %{_sysconfdir}/pam.d/gparted
%config(noreplace) %{_sysconfdir}/security/console.apps/gparted

%changelog
* Wed May  1 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.1-1m)
- version up 0.16.1

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.1-1m)
- version up 0.12.1

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.0-1m)
- version up 0.12.0

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.0-1m)
- version up 0.11.0

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.0-1m)
- version up 0.10.0

* Mon Oct 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1-1m)
- version up 0.9.1

* Tue Sep  8 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.0-1m)
- version up 0.9.0

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-4m)
- rebuild for parted-2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update to 0.7.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-2m)
- full rebuild for mo7 release

* Tue Mar 16 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.1-3m)
- rebuild against xfsprogs-3.0.1-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Fri May 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-2m)
- rebuild against gcc43

* Thu Mar 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.5-1m)
- update to 0.3.5

* Mon Feb 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-8m)
- add gcc43 patch

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-7m)
- %%NoSource -> NoSource

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-6m)
- add intltoolize (intltool-0.36.0)

* Sun Jul 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-5m)
- fix desktop file (spec full path)

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-4m)
- update gparted.pam for new pam

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.3-3m)
- rebuild against parted-1.8.6

* Wed Feb 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-2m)
- rebuild against parted-1.8.2

* Fri Dec 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.3-1m)
- 0.3.3 has been released

* Sat Oct 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.1-1m)
- 0.3.1 has been released

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.5-5m)
- remove category Application

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.5-4m)
- rebuild against expat-2.0.0-1m

* Tue Jun 06 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.5-3m)
- update ja.po to an official CVS version

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.5-2m)
- rebuild against gtkmm-2.8.8

* Fri May 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.5-1m)
- import to Momonga
- added ja.po
