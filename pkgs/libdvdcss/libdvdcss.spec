%global momorel 1

Summary: Library for accessing DVDs like block devices with transparent decryption
URL: http://www.videolan.org/libdvdcss/
Name: libdvdcss
Version: 1.2.12
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
Source0: ftp://www.videolan.org/pub/videolan/libdvdcss/%{version}/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-1.2.11-dvdcss.h-latex.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: texlive-dvipsk
BuildRequires: texlive-makeindexk
BuildRequires: texlive-texmf-fonts-amsfonts
BuildRequires: texlive-texmf-fonts-cm-super
BuildRequires: texlive-texmf-fonts-jknappen
BuildRequires: texlive-texmf-latex
BuildRequires: texlive-texmf-latex-fancyhdr
BuildRequires: texlive-texmf-latex-float
BuildRequires: texlive-texmf-latex-graphics
BuildRequires: texlive-texmf-latex-listings
BuildRequires: texlive-texmf-latex-ntgclass
# BuildRequires: texlive-texmf-latex-refman

%description
libdvdcss is a simple library designed for accessing DVDs like a block device
without having to bother about the decryption. The important features are:
 * Portability: currently supported platforms are GNU/Linux, FreeBSD, NetBSD,
   OpenBSD, BSD/OS, BeOS, Windows 95/98, Windows NT/2000, MacOS X, Solaris,
   HP-UX and OS/2.
 * Adaptability: unlike most similar projects, libdvdcss doesn't require the
   region of your drive to be set and will try its best to read from the disc
   even in the case of a region mismatch.
 * Simplicity: a DVD player can be built around the libdvdcss API using no
   more than 4 or 5 library calls.

%package devel
Summary: Development tools for programs which will use the %{name} library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package includes the header files and static libraries
necessary for developing programs which will manipulate DVDs files using
the %{name} library.
 
If you are going to develop programs which will manipulate DVDs, you
should install %{name}-devel.  You'll also need to have the %{name}
package installed.

%prep
%setup -q

%patch0 -p1 -b .acute-e

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README doc/html
%{_libdir}/libdvdcss.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/dvdcss
%{_libdir}/pkgconfig/libdvdcss.pc
%{_libdir}/libdvdcss.a
%{_libdir}/libdvdcss.so

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.12-1m)
- update 1.2.12

* Tue Feb 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.11-2m)
- fix BR tetex-dvipsk to texlive-dvipsk

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.11-1m)
- update 1.2.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-3m)
- fix up BuildRequires for MNPB

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-2m)
- full rebuild for mo7 release

* Wed Apr 28 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-1m)
- update 1.2.10

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.9-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-7m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.9-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.9-5m)
- %%NoSource -> NoSource

* Thu May  3 2007 zunda <zunda at freeshell.org>
- (1.2.9-4m)
- added Patch1: libdvdcss-1.2.9-dvdcss.h-latex.patch not to let
  LaTeX handle upper-ascii characters.

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.9-3m)
- delete libtool library

* Mon Dec 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.9-2m)
- BuildPreReq: tetex-dvipsk

* Tue Aug  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.9-1m)
- update to 1.2.9
- add %%post and %%postun

* Thu Jul 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.8-1m)
- minor bugfixes

* Thu Jun 26 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.7-1m)
  update to 1.2.7
  
* Tue Mar 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.6-1m)
  update to 1.2.6

* Tue Feb  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.5-1m)
- minor feature enhancements

* Sun Dec 08 2002 Kenta MURATA <muraken@momonga-linux.org>
- (1.2.4-1m)
- version up.

* Fri Oct 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.3-1m)
- minor bugfixes

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.2-2m)
- add more documents

* Mon Aug 12 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (1.2.2-1m)
- version up

* Fri Jun 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.2.1-2k)
  update to 1.2.1

* Thu Apr  4 2002 Toru Hoshina <t@kondara.org>
- add alpha support.

* Fri Mar  1 2002 Tsutomu Yasuda <tom@kondara.org>
- first release
