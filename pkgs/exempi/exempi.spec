%global momorel 11

Summary:        Library for easy parsing of XMP metadata
Name:           exempi
Version:        2.1.1
Release:        %{momorel}m%{?dist}
License:        Modified BSD
Group:          System Environment/Libraries
URL:            http://libopenraw.freedesktop.org/wiki/Exempi
Source0:        http://libopenraw.freedesktop.org/download/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  boost-devel
BuildRequires:  expat-devel
BuildRequires:  zlib-devel
BuildRequires:  pkgconfig

%description
Exempi provides a library for easy parsing of XMP metadata. It is a port of 
Adobe XMP SDK to work on UNIX and to be build with GNU automake.
It includes XMPCore and XMPFiles.

%package devel
Summary:        Headers for developing programs that will use %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
This package contains the libraries and header files needed for
developing with exempi

%prep
%setup -q

%build
%configure LDFLAGS="-L%{_libdir}" CPPFLAGS="-I%{_includedir}"
# Disable rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}
										
%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}%{_libdir}/*.la
rm -rf %{buildroot}%{_libdir}/*.a

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root, -)
%{_includedir}/exempi-2.0/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-11m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (2.1.1-10m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-9m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (2.1.1-8m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-7m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-6m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-5m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-4m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-liunx.org>
- (2.1.1-1m)
- update to 2.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-2m)
- add gcc44 patch
- delete unused patch

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sun Jan 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-3m)
- apply gcc44 patch
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.0-1m)
- import from Fedora

* Wed Apr 02 2008 Deji Akingunola <dakingun@gmail.com> - 2.0.0-1
- Update to 2.0.0

* Fri Feb 08 2008 Deji Akingunola <dakingun@gmail.com> - 1.99.9-1
- Update to 1.99.9

* Sun Jan 13 2008 Deji Akingunola <dakingun@gmail.com> - 1.99.7-1
- Update to 1.99.7

* Mon Dec 03 2007 Deji Akingunola <dakingun@gmail.com> - 1.99.5-1
- Update to 1.99.5

* Wed Sep 05 2007 Deji Akingunola <dakingun@gmail.com> - 1.99.4-2
- Rebuild for expat 2.0

* Wed Aug 22 2007 Deji Akingunola <dakingun@gmail.com> - 1.99.4-1
- Update tp 1.99.4

* Tue Jul 10 2007 Deji Akingunola <dakingun@gmail.com> - 1.99.3-1
- Initial packaging for Fedora
