%global momorel 1
%global src1name wireless-regdb
%global crda_version 1.1.3
%global regdb_version 2013.02.13

Summary: Regulatory compliance daemon for 802.11 wireless networking
Name: crda
Version: %{crda_version}
Release: %{momorel}m%{?dist}
License: "ISC"
Group: System Environment/Base
URL: http://www.linuxwireless.org/en/developers/Regulatory/CRDA
Source0: http://wireless.kernel.org/download/%{name}/%{name}-%{crda_version}.tar.bz2
NoSource: 0
Source1: http://wireless.kernel.org/download/%{src1name}/%{src1name}-%{regdb_version}.tar.bz2
# NoSource: 1
Source2: setregdomain
Source3: setregdomain.1
Patch0: regulatory-rules-setregdomain.patch
BuildRoot: %{_tmppath}/%{name}-%{crda_version}-%{release}-root-%(%{__id_u} -n)
Requires: iw
Requires: udev
BuildRequires: coreutils
BuildRequires: kernel-devel >= 2.6.27
BuildRequires: libnl-devel >= 1.1
BuildRequires: libgcrypt-devel
BuildRequires: pkgconfig python m2crypto

%description
CRDA acts as the udev helper for communication between the kernel
and userspace for regulatory compliance. It relies on nl80211
for communication. CRDA is intended to be run only through udev
communication from the kernel.

%prep
%setup -q -c
%setup -q -T -D -a 1

%patch0 -p1 -b .setregdomain

%build
# Use our own signing key to generate regulatory.bin
pushd %{src1name}-%{regdb_version}
make %{?_smp_mflags} CFLAGS="%{optflags}" maintainer-clean
make %{?_smp_mflags} CFLAGS="%{optflags}" REGDB_PRIVKEY=key.priv.pem REGDB_PUBKEY=key.pub.pem
popd

# Build CRDA using the new key and regulatory.bin from above
pushd %{name}-%{crda_version}
cp ../%{src1name}-%{regdb_version}/key.pub.pem pubkeys
make %{?_smp_mflags} OPTFLAGS="%{optflags}" REG_BIN=../%{src1name}-%{regdb_version}/regulatory.bin
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

pushd %{name}-%{crda_version}
mv README README.%{name}
make install DESTDIR=%{buildroot}
popd

pushd %{src1name}-%{regdb_version}
mv README README.%{src1name}
make install PREFIX=%{buildroot}%{_prefix}
popd

install -m 0755 %{SOURCE2} %{buildroot}/sbin/

mkdir -p %{buildroot}%{_mandir}/man1
install -m 0644 %{SOURCE3} %{buildroot}%{_mandir}/man1/setregdomain.1

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc %{name}-%{crda_version}/LICENSE %{name}-%{crda_version}/README.%{name}
%doc %{src1name}-%{regdb_version}/README.%{src1name}
/lib/udev/rules.d/85-regulatory.rules
/sbin/%{name}
/sbin/regdbdump
/sbin/setregdomain
%{_prefix}/lib/%{name}
%{_mandir}/man1/setregdomain.1*
%{_mandir}/man5/regulatory.bin.5*
%{_mandir}/man8/%{name}.8*
%{_mandir}/man8/regdbdump.8*

%changelog
* Mon Oct 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- version 1.1.3
- update setregdomain and regulatory-rules-setregdomain.patch
- remove libnl3.patch

* Tue Apr  3 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (1.1.2-2m)
- add patch1 (for libnl3)

* Mon Feb  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update crda to 1.1.2
- update wireless-regdb to 2011.04.28

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-1m)
- update crda to 1.1.1
- update wireless-regdb to 2009.11.25

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-0.20090417.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.20090417.1m)
- initial package for Momonga Linux

* Wed May 13 2009 John W. Linville <linville@redhat.com> 1.1.0_2009.04.17-11
- Update crda version to version 1.1.0
- Update wireless-regdb to version 2009.04.17 

* Fri Apr 17 2009 John W. Linville <linville@redhat.com> 1.0.1_2009.04.16-10
- Update wireless-regdb version to pick-up recent updates and fixes (#496392)

* Tue Mar 31 2009 John W. Linville <linville@redhat.com> 1.0.1_2009.03.09-9
- Add Requires line for iw package (#492762)
- Update setregdomain script to correctly check if COUNTRY is set

* Thu Mar 19 2009 John W. Linville <linville@redhat.com> 1.0.1_2009.03.09-8
- Add setregdomain script to set regulatory domain based on timezone
- Expand 85-regulatory.rules to invoke setregdomain script on device add

* Tue Mar 10 2009 John W. Linville <linville@redhat.com> 1.0.1_2009.03.09-7
- Update wireless-regdb version to pick-up recent updates and fixes (#489560)

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1_2009.01.30-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 John W. Linville <linville@redhat.com> 1.0.1_2009.01.30-5
- Recognize regulatory.bin files signed with the upstream key (#484982)

* Tue Feb 03 2009 John W. Linville <linville@redhat.com> 1.0.1_2009.01.30-4
- Change version to reflect new wireless-regdb upstream release practices
- Update wireless-regdb version to pick-up recent updates and fixes (#483816)

* Tue Jan 27 2009 John W. Linville <linville@redhat.com> 1.0.1_2009_01_15-3
- Update for CRDA verion 1.0.1
- Account for lack of "v" in upstream release tarball naming
- Add patch to let wireless-regdb install w/o being root

* Thu Jan 22 2009 John W. Linville <linville@redhat.com> v0.9.5_2009_01_15-2
- Revamp based on package review comments

* Tue Jan 20 2009 John W. Linville <linville@redhat.com> v0.9.5_2009_01_15-1
- Initial build
