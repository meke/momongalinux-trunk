%global momorel 5

%define         with_python_version %(echo `python -c "import sys; print sys.version[:3]"`)

Summary:        An open source cryptography library.
Name:           beecrypt
Version:        4.2.1
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
License:        LGPL
URL:            http://sourceforge.net/projects/beecrypt
Source0:        http://dl.sourceforge.net/sourceforge/beecrypt/%{name}-%{version}.tar.gz
Source1:        http://dl.sourceforge.net/sourceforge/beecrypt/%{name}-%{version}.tar.gz.sig
NoSource:       0
NoSource:       1
Patch0:         beecrypt-4.1.2-biarch.patch
Patch1:         beecrypt-4.2.1-no-c++.patch
BuildRequires:  doxygen
BuildRequires:  python-devel >= %{with_python_version}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes:      beecrypt-java =< 4.1.2-2

%description
Beecrypt is a general-purpose cryptography library.

%package devel
Summary:        Files needed for developing applications with beecrypt.
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Beecrypt is a general-purpose cryptography library.  This package contains
files needed for developing applications with beecrypt.

%package apidocs
Summary:        API documentation for beecrypt toolkit and library
Group:          Documentation

%description apidocs
Beecrypt is a general-purpose cryptography library. This package contains
API documentation for developing applications with beecrypt.

%package python
Summary:        Files needed for python applications using beecrypt.
Group:          Development/Libraries
Requires:       python >= %{with_python_version}
Requires:       %{name} = %{version}-%{release}

%description python
Beecrypt is a general-purpose cryptography library.  This package contains
files needed for using python with beecrypt.

%prep
%setup -q
%patch0 -p1 -b .biarch
%patch1 -p1 -b .no-c++

%build
autoreconf -vif
%configure --with-cplusplus=no \
           --with-java=no

make %{?_smp_mflags} pythondir=%{python_sitelib}

pushd include/beecrypt
doxygen
popd

%install
rm -fr %{buildroot}
make install DESTDIR=%{buildroot} pythondir=%{python_sitelib}

iconv -f ISO-8859-1 -t UTF-8 CONTRIBUTORS -o CONTRIBUTORS.utf8
mv -f CONTRIBUTORS.utf8 CONTRIBUTORS

find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "*.a" -delete

# XXX delete next line to build with legacy, non-check aware, rpmbuild.
%check
make check || :
cat /proc/cpuinfo
make bench || :

%clean
rm -fr %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS BENCHMARKS CONTRIBUTORS
%doc COPYING COPYING.LIB NEWS README
%{_libdir}/libbeecrypt.so.*

%files devel
%defattr(-,root,root,-)
%doc BUGS
%{_includedir}/%{name}
%{_libdir}/libbeecrypt.so

%files apidocs
%defattr(-,root,root,-)
%doc docs/html

%files python
%defattr(-,root,root,-)
%{python_sitelib}/_bc.so

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.2-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2-6m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.2-5m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.2-4m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.1.2-3m)
- delete libtool library

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.2-2m)
- rebuild against python-2.5

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.2-1m)
- version up

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.0-2m)
- rebuild against python-2.4.2

* Sat Sep 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.0-1m)
- upgrade 3.1.0
- rebuild against python2.3

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.0.1-0.20030630.2m)
- remove Epoch

* Sat Feb 28 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.1-0.20030630.1m)
- import from Fedora Core 1

* Mon Jun 30 2003 Jeff Johnson <jbj@redhat.com> 3.0.1-0.20030630
- upstream fixes for DSA and ppc64.

* Mon Jun 23 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-2
- upgrade to 3.0.0 final.
- fix for DSA (actually, modulo inverse) sometimes failing.

* Fri Jun 20 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-1.20030619
- avoid asm borkage on ppc64.

* Thu Jun 19 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-1.20030618
- rebuild for release bump.

* Tue Jun 17 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-1.20030616
- try to out smart libtool a different way.
- use $bc_target_cpu, not $bc_target_arch, to detect /usr/lib64.

* Mon Jun 16 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-1.20030615
- use -mcpu=powerpc64 on ppc64.

* Fri Jun 13 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-1.20030613
- upgrade to latest snapshot.

* Fri Jun  6 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-1.20030605
- rebuild into another tree.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jun  3 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030603
- update to 3.0.0 snapshot, fix mpmod (and DSA) on 64b platforms.

* Mon Jun  2 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030602
- update to 3.0.0 snapshot, merge patches, fix gcd rshift and ppc problems.

* Thu May 29 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030529
- update to 3.0.0 snapshot, fix ia64/x86_64 build problems.

* Wed May 28 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030528
- upgrade to 3.0.0 snapshot, adding rpm specific base64.[ch] changes.
- add PYTHONPATH=.. so that "make check" can test the just built _bc.so module.
- grab cpuinfo and run "make bench".
- continue ignoring "make check" failures, LD_LIBRARY_PATH needed for _bc.so.
- skip asm build failure on ia64 for now.
- ignore "make bench" exit codes too, x86_64 has AES segfault.

* Wed May 21 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030521
- upgrade to 3.0.0 snapshot, including python subpackage.
- ignore "make check" failure for now.

* Fri May 16 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030516
- upgrade to 3.0.0 snapshot, including ia64 and x86_64 fixes.
- add %%check.
- ignore "make check" failure on ia64 for now.

* Mon May 12 2003 Jeff Johnson <jbj@redhat.com> 3.0.0-0.20030512
- upgrade to 3.0.0 snapshot.
- add doxygen doco.
- use /dev/urandom as default entropy source.
- avoid known broken compilation for now.

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Nov 19 2002 Tim Powers <timp@redhat.com>
- rebuild on all arches

* Fri Aug  2 2002 Jeff Johnson <jbj@redhat.com> 2.2.0-6
- install types.h (#68999).

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jun  5 2002 Jeff Johnson <jbj@redhat.com>
- run ldconfig when installing/erasing (#65974).

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon May 13 2002 Jeff Johnson <jbj@redhat.com>
- upgrade to latest 2.2.0 (from cvs.rpm.org).

* Mon Jan 21 2002 Jeff Johnson <jbj@redhat.com>
- use the same beecrypt-2.2.0 that rpm is using internally.

* Thu Jan 10 2002 Nalin Dahyabhai <nalin@redhat.com> 2.1.0-1
- initial package
