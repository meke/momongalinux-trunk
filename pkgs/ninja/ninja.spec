%global momorel 6

Name: ninja
Version: 1.5.8.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Summary: Text based Internet Relay Chat (IRC) client
Group: Applications/Internet
URL: http://qoop.org/ftp/ninja/
Source0: http://qoop.org/ftp/ninja/sources/%{name}-%{version}.tar.gz
Patch0: %{name}-%{version}-doc.patch
Patch1: %{name}-%{version}-build.patch
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel

%description
Ninja IRC is yet another ircII-based IRC client. Its many extra features
include enhanced socket handling, additional resolving capabilities,
ANSI and MIRC color support, MIRC-style DCC RESUME, cloak mode, a friend
list, an enemy list, SOCKS v4&5 proxy support, more ircii $func()
functions, auto-rejoin, cycling auto-reconnect, auto-dcc get, improved
ban/unban handling, cached information, NDCC file offering, and much
more.

Install Ninja IRC if you want to participate (troll, lurk, hang out) in
irc channels.  Especially if you want to have power features.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
%configure
make

%install
rm -rf %{buildroot}
%makeinstall

ln -sf %{name}-%{version} %{buildroot}%{_bindir}/%{name}

mkdir -p %{buildroot}%{_mandir}/man1/
mv %{buildroot}%{_mandir}/ninja.* %{buildroot}%{_mandir}/man1/
rm %{buildroot}/%{_datadir}/%{name}/help/.date

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root, 755)
%{_bindir}/%{name}
%{_bindir}/%{name}-%{version}
%{_bindir}/%{name}io
%{_bindir}/%{name}flush
%{_bindir}/%{name}wserv
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/script
%{_datadir}/%{name}/translation
%{_datadir}/%{name}/help
%{_mandir}/man1/*
%defattr(644,root,root, 755)
%doc README ChangeLog BUGS+TODO COPYING

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.8.1-6m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.8.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.8.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.8.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.8.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.8.1-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.8.1-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 14 2008 Adrian Reber <adrian@lisas.de> - 1.5.8.1-8
- rebuilt for gcc43
- license is probably GPLv2+
- made rpmlint happy
- added patch to actually build again

* Wed Aug 22 2007 Adrian Reber <adrian@lisas.de> - 1.5.8.1-7
- fix URLs (#251285)

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 1.5.8.1-6
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Sat Sep 16 2006 Adrian Reber <adrian@lisas.de> - 1.5.8.1-5
- rebuilt

* Sun Mar 12 2006 Adrian Reber <adrian@lisas.de> - 1.5.8.1-4
- rebuilt

* Fri Apr  1 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 1.5.8.1-3
- Include ninja directory in datadir.

* Tue Feb 25 2003 Adrian Reber <adrian@lisas.de> - 1.5.8.1-0.fdr.1
- applied fedora naming conventions

* Mon Sep 23 2002 Vladimir Furtado Krachinski <vladimir@conectiva.com.br>
+ 2002-09-23 15:33:37 (15665)
- Changed %%doc permissions

* Fri Sep 20 2002 Vladimir Furtado Krachinski <vladimir@conectiva.com.br>
+ 2002-09-20 14:50:25 (15574)
- First Build for Conectiva Linux
- Added documentation patch

* Fri Sep 20 2002 Vladimir Furtado Krachinski <vladimir@conectiva.com.br>
+ 2002-09-20 14:39:39 (15573)
- Created snapshot directory for ninja.
