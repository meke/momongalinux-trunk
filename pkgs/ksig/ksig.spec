%global momorel 8
%global date 20090629
%global patchdate 20.03.09
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m

Summary: A graphical application to manage multiple email signatures
Name: ksig
Version: 1.1
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://extragear.kde.org/
Source0: %{name}-%{version}.tar.bz2
Patch0: %{name}-%{patchdate}-desktop.patch
Patch1: %{name}-%{patchdate}-docdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: gettext

%description
KSig is a graphical tool for keeping track of many different email signatures.
The signatures themselves can be edited through KSig's graphical user 
interface. A command-line interface is then available for generating random 
or daily signatures from a list. The command-line interface makes a suitable 
plugin for generating signatures in external mail clients such as KMail.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .fix-docdir

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING*
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-0.20090629.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-0.20090629.7m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.20090629.6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-0.20090629.5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.20090629.4m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.20090629.3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-0.20090629.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.20090629.1m)
- update to 20090629 svn snapshot

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.20090320.1m)
- update to 20090320 svn snapshot
- update desktop.patch
- fix up documentation's directory
- remove merged cmake.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-0.20080516.3m)
- rebuild against rpm-4.6

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.20080516.2m)
- add DocPath to desktop file

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.20080516.1m)
- initial package for Momonga Linux
- import and modify Fedora's cmake.patch
