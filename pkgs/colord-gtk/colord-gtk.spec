%global    momorel 1
Summary:   GTK support library for colord
Name:      colord-gtk
Version:   0.1.22
Group: 	   System Environment/Libraries
Release:   %{momorel}m%{?dist}
License:   LGPLv2+
URL:       http://www.freedesktop.org/software/colord/
Source0:   http://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz
NoSource: 0

# Upstream already
Patch0: 0001-Do-not-reuse-__COLORD_H_INSIDE__-as-this-causes-incl.patch

BuildRequires: docbook-utils
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: colord-devel >= 0.1.22
BuildRequires: intltool
BuildRequires: lcms2-devel >= 2.2
BuildRequires: gobject-introspection-devel
BuildRequires: vala-tools
BuildRequires: gtk3-devel
BuildRequires: gtk-doc

%description
colord-gtk is a support library for colord and provides additional
functionality that requires GTK+.

%package devel
Summary: Development package for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Files for development with %{name}.

%prep
%setup -q
%patch0 -p1 -b .includes

%build
%configure \
        --enable-gtk-doc \
        --disable-static \
        --disable-rpath \
        --disable-dependency-tracking

%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

# Remove static libs and libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'
find %{buildroot} -name '*.a' -exec rm -f {} ';'

%find_lang %{name}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f %{name}.lang
%doc README AUTHORS NEWS COPYING
%{_libdir}/libcolord-gtk.so.*
%{_libdir}/girepository-1.0/ColordGtk-1.0.typelib

%files devel
%{_libdir}/libcolord-gtk.so
%{_libdir}/pkgconfig/colord-gtk.pc
%dir %{_includedir}/colord-1
%{_includedir}/colord-1/colord-gtk.h
%dir %{_includedir}/colord-1/colord-gtk
%{_includedir}/colord-1/colord-gtk/*.h
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/ColordGtk-1.0.gir
%doc %{_datadir}/gtk-doc/html/colord-gtk
%{_datadir}/vala/vapi/colord-gtk.vapi
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.22-1m)
- import from fedora

