%global         momorel 8

Name:           perl-MooseX-Types-DateTime-ButMaintained
Version:        0.16
Release:        %{momorel}m%{?dist}
Summary:        DateTime related constraints and coercions for Moose
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Types-DateTime-ButMaintained/
Source0:        http://www.cpan.org/authors/id/E/EC/ECARROLL/MooseX-Types-DateTime-ButMaintained-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-DateTime >= 0.70
BuildRequires:  perl-DateTime-Locale >= 0.45
BuildRequires:  perl-DateTime-TimeZone >= 0.96
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 0.41
BuildRequires:  perl-MooseX-Types >= 0.04
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Olson-Abbreviations >= 0.03
BuildRequires:  perl-Test-Exception >= 0.27
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-use-ok >= 0.02
Requires:       perl-DateTime >= 0.70
Requires:       perl-DateTime-Locale >= 0.45
Requires:       perl-DateTime-TimeZone >= 0.96
Requires:       perl-Moose >= 0.41
Requires:       perl-MooseX-Types >= 0.04
Requires:       perl-namespace-autoclean
Requires:       perl-Olson-Abbreviations >= 0.03
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module packages several Moose::Util::TypeConstraints with coercions,
designed to work with the DateTime suite of objects.

%prep
%setup -q -n MooseX-Types-DateTime-ButMaintained-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/MooseX/Types/DateTime/ButMaintained*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.16.1

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15
- rebuild against perl-5.16.0

* Sat Feb  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14 again

* Sun Jan  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-14m)
- version down to 0.11

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Tue Dec 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-13m)
- rebuild against perl-5.14.2

* Sun Jun 26 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-12m)
- more strict BuildRequires

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-2m)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-2m)
- modify BuildRequires

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
