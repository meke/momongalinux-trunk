%global		momorel 1
Name:           gjs
Version:        1.34.0
Release: 	%{momorel}m%{?dist}
Summary:        Javascript Bindings for GNOME

Group:          System Environment/Libraries
# The following files contain code from Mozilla which
# is triple licensed under MPL1.1/LGPLv2+/GPLv2+:
# The console module (modules/console.c)
# Stack printer (gjs/stack.c)
License:        MIT and (MPLv1.1 or GPLv2+ or LGPLv2+)
URL:            http://live.gnome.org/Gjs/
#VCS:           git://git.gnome.org/gjs
Source0:        http://download.gnome.org/sources/%{name}/1.34/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires: js-devel
BuildRequires: cairo-gobject-devel
BuildRequires: gobject-introspection-devel >= 1.31.22
BuildRequires: readline-devel
BuildRequires: dbus-glib-devel
BuildRequires: intltool
BuildRequires: pkgconfig
# Bootstrap requirements
BuildRequires: gtk-doc gnome-common

%description
Gjs allows using GNOME libraries from Javascript. It's based on the
Spidermonkey Javascript engine from Mozilla and the GObject introspection
framework.

%package devel
Summary: Development package for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Files for development with %{name}.

%prep
%setup -q

rm -f configure

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; fi;
 %configure --disable-static)

make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%check
#make check

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc COPYING NEWS README
%{_bindir}/gjs
%{_bindir}/gjs-console
%{_libdir}/*.so.*
%{_libdir}/gjs
%{_libdir}/gjs-1.0
%{_datadir}/gjs-1.0

%files devel
%doc examples/*
%{_includedir}/gjs-1.0
%{_libdir}/pkgconfig/gjs-1.0.pc
%{_libdir}/pkgconfig/gjs-dbus-1.0.pc
%{_libdir}/pkgconfig/gjs-internals-1.0.pc
%{_libdir}/*.so

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.34.0-1m)
- update to 1.34.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.14-1m)
- update to 1.33.14

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.10-1m)
- update to 1.33.10

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.9-1m)
- update to 1.33.9

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.4-1m)
- update to 1.33.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.3-2m)
- reimport from fedora

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.30.1-2m)
- fix build failure with glib 2.33+

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.30.1-1m)
- update 1.30.1

* Mon Dec 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.30.0-8m)
- do not use autogen.sh
- add --with-js-package=mozjs185 to configure

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30.0-7m)
- now gjs is depend on js not xulrunner

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30.0-6m)
- rebuild against xulrunner-8.0.1

* Wed Nov  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30.0-5m)
- rebuild against xulrunner-8.0

* Thu Oct 27 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.30.0-4m)
- require adjustment

* Sun Oct  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.30.0-3m)
- just increase Release

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30.0-2m)
- rebuild against xulrunner-7.0.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.30.0-1m)
- update to 1.30.0

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29.18-2m)
- rebuild aginst xulrunner-7.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.18-1m)
- update to 1.29.18

* Mon Sep 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.29.17-3m)
- explicitly Provides: %%{_bindir}/%%{name} for gtk-vnc

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.29.17-2m)
- add BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.17-1m)
- update to 1.29.17

* Thu Aug 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.14-5m)
- rebuild against xulrunner-6.0

* Fri Jul  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.14-4m)
- rebuild against xulrunner-5.0
-- add patch0 and 1 for build fix

* Mon May  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.14-3m)
- rebuild against xulrunner-2.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.14-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.14-1m)
- update to 0.7.14

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.7-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.7-1m)
- update to 0.7.7

* Wed Nov 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.6-1m)
- update to 0.7.6

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (0.7.2-1m)
- update to 0.7.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-2m)
- full rebuild for mo7 release

* Thu May 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-2m)
- rebuild against readline6

* Fri Mar 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-0.1.1m)
- update to git snapshot (a16783b) for xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-2m)
- to main

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-1m)
- initial build
