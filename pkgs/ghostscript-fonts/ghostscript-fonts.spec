%global momorel 9

Summary: Fonts for GNU/ESP Ghostscript
Name: ghostscript-fonts
Version: 6.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Publishing
Source0: ftp://tug.ctan.org/pub/tex-archive/support/ghostscript/fonts/ghostscript-fonts-other-6.0.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: fontpackages-filesystem

%description
These fonts can be used by the GhostScript interpreter during text
rendering. They are in addition to the shared fonts between GhostScript
and X11.

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_datadir}/fonts/default
tar xzf %{SOURCE0} -C %{buildroot}%{_datadir}/fonts/default
mv %{buildroot}%{_datadir}/fonts/default/fonts \
    %{buildroot}%{_datadir}/fonts/default/ghostscript
mkdir -p %{buildroot}%{_datadir}/ghostscript
ln -sf %{_datadir}/fonts/default/ghostscript %{buildroot}/%{_datadir}/ghostscript/fonts

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%dir %{_datadir}/fonts/default/ghostscript
%{_datadir}/fonts/default/ghostscript/*
%{_datadir}/ghostscript/fonts

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0-5m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0-3m)
- rebuild against gcc43

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (6.0-2m)
- remove Epoch

* Sat Jan  4 2003 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1:6.0-1m)
- separate ghostscript-fonts into another spec.
- make Version be source tar ball's one (6.0).
  this makes "version down", so bump Epoch to 1.
