%global momorel 7

%define fontname dustin-dustismo
%define fontconf 63-%{fontname}

%define common_desc General purpose fonts by Dustin Norlander available in \
serif and sans-serif versions. The fonts cover all European Latin characters.

Name:          %{fontname}-fonts
Version:       20030318
Release:       %{momorel}m%{?dist}
Summary:       General purpose sans-serif font with bold, italic and bold-italic variations

Group:         User Interface/X
License:       GPLv2+
URL:           http://www.dustismo.com
# Actual download URL
#URL:           http://ftp.de.debian.org/debian/pool/main/t/ttf-dustin/ttf-dustin_20030517.orig.tar.gz 
Source0:       Dustismo.zip
Source1:       %{name}-sans-fontconfig.conf
Source2:       %{name}-roman-fontconfig.conf
BuildRoot:     %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel

%description
%common_desc

%package common
Summary:       Common files for %{name}
Group:         User Interface/X
Requires:      fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%package -n %{fontname}-sans-fonts
Summary:       General purpos sans-serif fonts
Group:         User Interface/X
Requires:      %{name}-common = %{version}-%{release}
Provides:      %{name} = 20030318-3
Obsoletes:     %{name} < 20030318-3

%description -n %{fontname}-sans-fonts
%common_desc

General purpose sans-serif font with bold, italic and bold-italic variations

%_font_pkg -n sans -f %{fontconf}-sans.conf dustismo_bold_italic.ttf dustismo_bold.ttf dustismo_italic.ttf Dustismo.ttf

%package -n %{fontname}-roman-fonts
Summary:       General purpose serif font
Group:         User Interface/X
Requires:      %{name}-common = %{version}-%{release}
Provides:      %{name}-roman = 20030318-3
Obsoletes:     %{name}-roman < 20030318-3

%description -n %{fontname}-roman-fonts
%common_desc

General purpose serif font with bold, italic and bold-italic variations

%_font_pkg -n roman -f %{fontconf}-roman.conf Dustismo_Roman_Bold.ttf Dustismo_Roman.ttf Dustismo_Roman_Italic_Bold.ttf Dustismo_Roman_Italic.ttf      

%prep
%setup -q -c %{name}
sed -i 's/\r//' license.txt

%build

%install
rm -rf $RPM_BUILD_ROOT

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-sans.conf
install -m 0644 -p %{SOURCE2} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-roman.conf

for fontconf in %{fontconf}-sans.conf %{fontconf}-roman.conf ; do
  ln -s %{_fontconfig_templatedir}/$fontconf %{buildroot}%{_fontconfig_confdir}/$fontconf
done

%clean
rm -rf $RPM_BUILD_ROOT

%files common
%defattr(0644,root,root,0755)
%doc license.txt

%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030318-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030318-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20030318-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030318-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030318-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030318-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20030318-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20030318-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Sven Lankes <sven@lank.es> - 20030318-3
- rename according to new naming-guidelines

* Wed Jan 07 2009 Sven Lankes <sven@lank.es> - 20030318-2
- Change package-name to dustin-dustistmo-fonts

* Sun Jan 04 2009 Sven Lankes <sven@lank.es> - 20030318-1
- Use newer debian-source as source
- Convert to -multi spec

* Wed Dec 31 2008 Sven Lankes <sven@lank.es> - 20030207-1
- Initial packaging

