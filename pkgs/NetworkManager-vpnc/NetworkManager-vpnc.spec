%global momorel 2

%define nm_version          0.9.8.10
%define dbus_version        1.1
%define gtk2_version        2.10.0
%define vpnc_version        0.4
%define shared_mime_version 0.16-3

%define svn_snapshot svn4296

Summary:   NetworkManager VPN integration for vpnc
Name:      NetworkManager-vpnc
Version:   0.9.8.6
Release:   %{momorel}m%{?dist}
License:   GPLv2+
Group:     System Environment/Base
URL:       http://www.gnome.org/projects/NetworkManager/
Source:    http://ftp.gnome.org/pub/GNOME/sources/NetworkManager-vpnc/0.9/%{name}-%{version}.tar.xz
NoSource:  0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel             >= %{gtk2_version}
BuildRequires: dbus-devel             >= %{dbus_version}
BuildRequires: NetworkManager-devel   >= %{nm_version}
BuildRequires: NetworkManager-glib-devel   >= %{nm_version}
BuildRequires: GConf2-devel
BuildRequires: libgnomeui-devel
BuildRequires: gcr-devel
BuildRequires: libglade2-devel
BuildRequires: libpng-devel
BuildRequires: intltool gettext

Requires: gtk2             >= %{gtk2_version}
Requires: dbus             >= %{dbus_version}
Requires: NetworkManager   >= %{nm_version}
Requires: vpnc             >= %{vpnc_version}
Requires: shared-mime-info >= %{shared_mime_version}
Requires: GConf2
Requires: gnome-keyring
Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig


%description
This package contains software for integrating the vpnc VPN software
with NetworkManager and the GNOME desktop

%prep
%setup -q

%build
%configure --enable-more-warnings=yes
make %{?_smp_mflags}

%install

make install DESTDIR=$RPM_BUILD_ROOT

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la
rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.a

%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
      %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
      %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%files -f %{name}.lang
%defattr(-, root, root)

%doc AUTHORS ChangeLog
%{_libdir}/NetworkManager/lib*.so*
%{_libexecdir}/nm-vpnc-auth-dialog
%{_sysconfdir}/dbus-1/system.d/nm-vpnc-service.conf
%{_sysconfdir}/NetworkManager/VPN/nm-vpnc-service.name
%{_libexecdir}/nm-vpnc-service
%{_libexecdir}/nm-vpnc-service-vpnc-helper
%{_datadir}/gnome-vpn-properties/vpnc/nm-vpnc-dialog.ui
%{_datadir}/icons/hicolor/48x48/apps/gnome-mime-application-x-cisco-vpn-settings.png
%{_datadir}/applications/nm-vpnc-auth-dialog.desktop

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.6-2m)
- rebuild against NetworkManager

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.6-1m)
- update to 0.9.8.6

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.2-1m)
- update to 0.9.8.2

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.0-1m)
- update 0.9.6.0

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5.95-1m)
- update 0.9.5.95

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4.0-2m)
- rebuild for gcr-devel

* Sun Mar 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4.0-1m)
- update 0.9.4.0

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-1m)
- update 0.9.3.995

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.0-1m)
- update 0.9.2.0

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1.95-1m)
- update 0.9.1.95

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.999-1m)
- update to 0.8.999

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- update 0.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update 0.8.2

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-3m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Mon May 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0.997-1m)
- update 0.8.0.997

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-1m)
- update 0.8

* Thu Nov 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- update 0.7.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-2m)
- add autoreconf. need libtool-2.2.x

* Tue Apr 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.99-1m)
- update 0.7.0.99

* Mon Mar  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.97-1m)
- update 0.7.0.97

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update 0.7.0-release

* Thu Nov 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn4296.1m)
- update svn4296

* Sat Jul 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn3846.1m)
- update svn3846
- change version number

* Sat May 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.0-6.7.svn3549.1m)
- import from Fedora

* Wed Apr 09 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-6.7.svn3549
- Update for compat with new NM bits

* Tue Mar 25 2008 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.6.7.svn3502
- Send suggested MTU to NetworkManager

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1:0.7.0-0.7.7.svn3204
- Autorebuild for GCC 4.3

* Fri Jan  4 2008 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.6.7.svn3204
- Support new vpnc 0.4 Cisco UDP Encapsulation option
- Fix another crash in the properties applet
- Remove upstreamed pcfimport patch

* Mon Nov 26 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.6.3.svn3109
- Rebuild for updated NetworkManager

* Tue Nov 13 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.6.2.svn3083
- Rebuild for updated NetworkManager

* Sat Oct 27 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.4.svn3030
- Fix a crash when editing VPN properties a second time

* Tue Oct 23 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.3.svn3014
- Rebuild

* Wed Oct 17 2007 Bill Nottingham <notting@redhat.com> - 1:0.7.0-0.3.svn2970
- rebuild (#336261)

* Wed Oct 10 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.2.svn2970
- Fix default username

* Thu Sep 28 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.1.svn2914
- Fix .name file on 64-bit systems

* Fri Sep 28 2007 Jesse Keating <jkeating@redhat.com> - 1:0.7.0-0.2.svn2910
- BuildRequire NetworkManager-glib-devel

* Thu Sep 27 2007 Dan Williams <dcbw@redhat.com> - 1:0.7.0-0.1.svn2910
- New snapshot; ported to NM 0.7 API

* Fri Aug 17 2007 Denis Leroy <denis@poolshark.org> - 1:0.6.4-4
- Updated License tag
- Added patch to make properties multilib friendly (#243535)

* Thu Mar 22 2007 Denis Leroy <denis@poolshark.org> - 1:0.6.4-3
- Added patch to improve configuration GUI, add NAT traversal and single DES options

* Sun Feb 18 2007 Denis Leroy <denis@poolshark.org> - 1:0.6.4-2
- Readded NAT-keepalive support patch from SVN branch

* Wed Feb 14 2007 Denis Leroy <denis@poolshark.org> - 1:0.6.4-1
- Downgrading to 1:0.6.4 to keep par with core NM version

* Mon Dec  4 2006 Dan Williams <dcbw@redhat.com> - 0.7.0-0.cvs20061204
- Allow "NAT-Keepalive packet interval" config option

* Sat Oct 21 2006 Denis Leroy <denis@poolshark.org> - 0.7.0-0.cvs20060929.3
- Added patch to support saving group password only

* Thu Oct  5 2006 Denis Leroy <denis@poolshark.org> - 0.7.0-0.cvs20060929.2
- Leave .so link alone, needed by nm

* Fri Sep 29 2006 Denis Leroy <denis@poolshark.org> - 0.7.0-0.cvs20060929.1
- Update to CVS snapshot 060929
- Some rpmlint cleanups

* Fri Sep 29 2006 Denis Leroy <denis@poolshark.org> - 0.7.0-0.cvs20060529.4
- Added XML::Parser BR

* Fri Sep 29 2006 Denis Leroy <denis@poolshark.org> - 0.7.0-0.cvs20060529.3
- Added gettext BR

* Wed Sep 27 2006 Warren Togami <wtogami@redhat.com> - 0.7.0-0.cvs20060529.2
- rebuild for FC6

* Thu Jul 20 2006 Warren Togami <wtogami@redhat.com> - 0.7.0-0.cvs20060529.1
- rebuild for new dbus

* Mon May 29 2006 Dan Williams <dcbw@redhat.com> - 0.7.0-0.cvs20060529
- Gnome.org #336913: HIG tweaks for vpn properties pages

* Sun May 21 2006 Dan Williams <dcbw@redhat.com> 0.7.0-0.cvs20060521
- Update to CVS snapshot
- Honor user-specified rekeying intervals

* Mon May 15 2006 Dan Williams <dcbw@redhat.com> 0.6.2-1
- New release for NM 0.6.2 compat

* Fri Apr 21 2006 Dan Williams <dcbw@redhat.com> 0.6.0-3
- Add dist tag to RPM release

* Wed Apr 12 2006 Christopher Aillon <caillon@redhat.com> 0.6.0-2
- Rekey every 2 hours

* Tue Mar 14 2006 Dan Williams <dcbw@redhat.com> - 0.6.0-1
- Update to CVS snapshot of 0.6 for NM compatibility

* Fri Jan 27 2006 Dan Williams <dcbw@redhat.com> - 0.5.0-1
- CVS snapshot for compatibility new NetworkManager

* Tue Dec  6 2005 Jeremy Katz <katzj@redhat.com> - 0.3-3
- rebuild for new dbus

* Mon Oct 17 2005 Dan Williams <dcbw@redhat.com> 0.3-2
- Rebuild to test new Extras buildsystem

* Thu Aug 18 2005 David Zeuthen <davidz@redhat.com> 0.3-1
- New upstream release
- Bump some versions for deps

* Fri Jul  1 2005 David Zeuthen <davidz@redhat.com> 0.2-2
- Add missing changelog entry for last commit
- Temporarily BuildReq libpng-devel as it is not pulled in by gtk2-devel
  (should be fixed in Core shortly)
- Pull in latest D-BUS (which features automatic reloading of policy files)
  so users do not have to restart the messagebus after installing this package

* Thu Jun 30 2005 David Zeuthen <davidz@redhat.com> 0.2-1
- New upsteam version
- Add the new gnome-mime-application-x-cisco-vpn-settings.png icon and call
  gtk-update-icon-cache as appropriate

* Fri Jun 17 2005 David Zeuthen <davidz@redhat.com> 0.1-2.cvs20050617
- Add Prereq: /usr/bin/update-desktop-database
- Nuke .la and .a files
- Use find_lang macro to handle locale files properly
- Add Requires for suitable version of shared-mime-info since our desktop
  file depends on the application/x-cisco-vpn-settings MIME-type

* Fri Jun 17 2005 David Zeuthen <davidz@redhat.com> 0.1-1.cvs20050617
- Latest CVS snapshot

* Thu Jun 16 2005 David Zeuthen <davidz@redhat.com> 0.1-1
- Initial build
