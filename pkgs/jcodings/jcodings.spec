%global momorel 4

%global git_commit 8a3ca9e
%global cluster jruby

# Prevent brp-java-repack-jars from being run.
%define __jar_repack %{nil}

Name:           jcodings
Version:        1.0.2
Release:        %{momorel}m%{?dist}
Summary:        Java-based codings helper classes for Joni and JRuby

Group:          Development/Libraries
License:        MIT
URL:            http://github.com/%{cluster}/%{name}
Source0:        %{url}/tarball/%{version}/%{cluster}-%{name}-%{git_commit}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  ant
BuildRequires:  java-devel
BuildRequires:  jpackage-utils

Requires:       java
Requires:       jpackage-utils

%description
Java-based codings helper classes for Joni and JRuby.


%prep
%setup -q -n %{cluster}-%{name}-%{git_commit}

find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;

%build
echo "See %{url} for more info about the %{name} project." > README.txt

%{ant}


%install
%__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_javadir}

%__cp -p target/%{name}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
pushd %{buildroot}%{_javadir}/
  %__ln_s %{name}-%{version}.jar %{name}.jar
popd


%clean
%__rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_javadir}/*
%doc README.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Fri Feb 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- import from Fedora 13

* Tue Feb 09 2010 Victor G. Vasilyev <victor.vasilyev@sun.com> - 1.0.2-2
- Fix the clean up code in the prep section
- Fix typo
- Save changelog

* Thu Jan 28 2010 Victor G. Vasilyev <victor.vasilyev@sun.com> - 1.0.2-1
- 1.0.2
- Remove gcj bits
- New URL
- Update summary and description
- Use macros in all sections of the spec
- Add README.txt generated on the fly

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 15 2009 Conrad Meyer <konrad@tylerc.org> - 1.0.1-1
- Bump to 1.0.1 for jruby 1.1.6.

* Wed Dec 17 2008 Conrad Meyer <konrad@tylerc.org> - 1.0-2
- Add gcj bits.

* Fri Nov 28 2008 Conrad Meyer <konrad@tylerc.org> - 1.0-1
- Initial package (needed for jruby 1.1.5 and joni 1.1.1).
