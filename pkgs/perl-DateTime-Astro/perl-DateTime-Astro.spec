%global         momorel 7
%global         srcver 1.00
#global         prever 3

Summary:        Functions For Astromical Calendars
Name:           perl-DateTime-Astro
Version:        %{srcver}000
Release:        %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License:        GPL+ or Artistic
Group:          Development/Languages
Source0:        http://www.cpan.org/authors/id/D/DM/DMAKI/DateTime-Astro-%{srcver}%{?prever:_0%{prever}}.tar.gz
NoSource:       0
URL:            http://www.cpan.org/modules/by-module/DateTime-Astro/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Set
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-DateTime
Requires:       perl-DateTime-Set
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-DateTime-Event-SolarTerm

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DateTime::Astro implements functions used in astronomical calendars:
Solar Longitude
Solar Terms
Lunar Longitude
New Moons
...etc

This module is best used in environments where a C compiler and the MPFR arbitrary
precision math library is installed. It can fallback to using Math::BigInt, but that
would pretty much render it useless because of its speed and loss of accuracy that
may creep up while doing Perl to C struct conversions.

%prep
%setup -q -n DateTime-Astro-%{srcver}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes MANIFEST META.yml MYMETA.yml
%{perl_vendorarch}/auto/DateTime/Astro
%{perl_vendorarch}/DateTime/Astro*.pm
%{perl_vendorarch}/DateTime/Event/SolarTerm.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00000-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00000-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00000-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00000-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00000-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00000-2m)
- rebuild against perl-5.16.2

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-llinux.og>
- (1.00000-1m)
- update to 1.00

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99999-0.3.3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99999-0.3.2m)
- rebuild against perl-5.16.0

* Wed Oct 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99999-0.3.1m)
- initial build for Momonga Linux
