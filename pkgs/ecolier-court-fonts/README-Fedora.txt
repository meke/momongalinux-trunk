[Unofficial English translation of lisez_moi.txt]

WARNING

The two fonts named:

— Ecolier court
— Ecolier lignes court

are released under the OFL (Open Font Licence):
http://scripts.sil.org/OFL

An (unofficial) translation in French is available here:
http://fontforge.sourceforge.net/OFL-Unofficial-fr.html

Please read the conditions of use of the two fonts listed here.

If you want to display my fonts on your site, please add it the OFL logo (with
its link), the OFL translation and the lisez_moi.txt file.

You can also (and this is often preferable), link to my site:
http://perso.orange.fr/jm.douteau/

The author makes those fonts available to everyone free of charge and disclaims
any responsibility for possible malfunctions.

Those fonts were created by Jean-Marie Douteau.
e-mail : douteau.ecolier@orange.fr
