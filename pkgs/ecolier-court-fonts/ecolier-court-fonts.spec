%global momorel 7

%global fontname ecolier-court
%global fontconf 61-%{fontname}

%global common_desc\
Ecolier court fonts were created by Jean-Marie Douteau to mimic the \
traditional cursive writing French children are taught in school. \
\
He kindly released two of them under the OFL, which are redistributed in \
this package.


Name:    %{fontname}-fonts
Version: 20070702
Release: %{momorel}m%{?dist}
Summary: Schoolchildren cursive fonts

Group:     User Interface/X
License:   OFL
URL:       http://perso.orange.fr/jm.douteau/page_ecolier.htm
# The author links to third-party licence documents not included there
Source0:   http://perso.orange.fr/jm.douteau/polices/ecl_cour.ttf
Source1:   http://perso.orange.fr/jm.douteau/polices/ec_cour.ttf
Source2:   http://perso.orange.fr/jm.douteau/polices/lisez_moi.txt
Source3:   README-Fedora.txt
Source4:   %{name}-fontconfig.conf
Source5:   %{name}-lignes-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      %{name}-common = %{version}-%{release}

%description
%common_desc

%_font_pkg -f %{fontconf}-lignes.conf ecl_cour.ttf


%package common
Summary:  Common files of the Ecolier Court font set
Group:     User Interface/X
Requires: fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.


%package -n %{fontname}-lignes-fonts
Summary:  Schoolchildren cursive fonts with lines
Group:     User Interface/X
Requires: %{name}-common = %{version}-%{release}

Obsoletes: %{name}-lignes < 20070702-7

%description -n %{fontname}-lignes-fonts
%common_desc

The lignes (lines) Ecolier court font variant includes the Seyes lining
commonly used by schoolchildren notepads.

%_font_pkg -n lignes -f %{fontconf}.conf ec_cour.ttf


%prep
%setup -q -c -T
iconv -f iso-8859-15 -t utf-8 %{SOURCE2} > lisez_moi.txt
touch -r %{SOURCE2} lisez_moi.txt
for txt in *.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done
install -m 0644 -p %{SOURCE3} .


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{SOURCE1} %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf
install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-lignes.conf

for fconf in %{fontconf}.conf \
             %{fontconf}-lignes.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc *.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070702-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070702-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20070702-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070702-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070702-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070702-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20070702-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20070702-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070702-8
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Sat Jan 17 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070702-7
- Convert to new naming guidelines

* Wed Dec 17 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070702-5
- Workaround RHEL5 rpm unicode bug to fix koji build

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070702-4
- 'rpm-fonts' renamed to "fontpackages"

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070702-3
- Rebuild using new rpm-fonts

* Sat Jul 19 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070702-1
- Stop waitting for upstream to answer distribution change requests
- FESCO chickened out on UTF-8 names
- FESCO decision unimplementable due to bug #455119
- Sync spec style with the way our font packaging guidelines have evolved
- Package both fonts in a single package
  I'm going to consider they are two faces of the same font
- Register in new fontconfig generic families
- Add upstream readme translation as asked in review

* Sun Nov 25 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.00-0.4.20070628
- sync with packaging guidelines

* Sat Sep 8 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.00-0.2.20070628
- use more accurate naming
- add an ASCII alias as suggested in review

* Tue Aug 28 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.00-0.1.20070628
- Initial packaging (based on DejaVu & Charis specs)
