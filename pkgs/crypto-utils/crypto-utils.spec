%global momorel 22

%define crver 1.3

Summary: SSL certificate and key management utilities
Name: crypto-utils
Version: 2.4.1
Release: %{momorel}m%{?dist}
Source: crypto-rand-%{crver}.tar.gz
Source1: genkey.pl
Source2: certwatch.c
Source3: certwatch.cron
Source4: certwatch.xml
Source5: genkey.xml
Source6: keyrand.c
Source7: COPYING
Source8: keyrand.xml
Source9: pemutil.c
Source10: keyutil.c
Source11: certext.c
Source12: secutil.c
Source13: secerror.c
Source14: keyutil.h
Source15: secutil.h
Source16: NSPRerrs.h
Source17: SECerrs.h
Source18: copying
Group: Applications/System
License: MIT and GPLv2+
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: nss-devel, pkgconfig, newt-devel, xmlto
BuildRequires: perl-devel, perl(Newt), perl-ExtUtils-MakeMaker
Requires: perl(Newt), nss >= 3.12.2
Requires: %(eval `perl -V:version`; echo "perl(:MODULE_COMPAT_$version)")
Obsoletes: crypto-rand

%description
This package provides tools for managing and generating
SSL certificates and keys.

%prep
%setup -q -n crypto-rand-%{crver}

%build 
%configure --with-newt=%{_prefix} CFLAGS="$CFLAGS -fPIC"
make -C librand

cc $RPM_OPT_FLAGS -Wall -Werror -I/usr/include/nspr4 -I/usr/include/nss3 \
   $RPM_SOURCE_DIR/certwatch.c $RPM_SOURCE_DIR/pemutil.c \
   -o certwatch -lnspr4 -lnss3

cc $RPM_OPT_FLAGS -Wall -Werror -I/usr/include/nspr4 -I/usr/include/nss3 \
   $RPM_SOURCE_DIR/keyutil.c \
   $RPM_SOURCE_DIR/certext.c \
   $RPM_SOURCE_DIR/secutil.c \
   $RPM_SOURCE_DIR/secerror.c \
   -o keyutil -lplc4 -lnspr4 -lnss3

cc $RPM_OPT_FLAGS -Wall -Werror \
   $RPM_SOURCE_DIR/keyrand.c -o keyrand -lnewt -lslang

date +"%e %B %Y" | tr -d '\n' > date.xml
echo -n %{version} > version.xml

for m in certwatch.xml genkey.xml keyrand.xml; do
  cp $RPM_SOURCE_DIR/${m} .
  xmlto man ${m} 
done

pushd Makerand
perl -pi -e "s/Stronghold/Crypt/g" *
perl Makefile.PL PREFIX=%{buildroot}/usr OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=vendor
make
popd

%install
rm -rf %{buildroot}

sed -n '1,/^ \*\/$/p' librand/qshs.c > LICENSE.librand
cp -p $RPM_SOURCE_DIR/COPYING .

pushd Makerand
make install
popd

find %{buildroot} -name Makerand.so | xargs chmod 755

find %{buildroot} \( -name perllocal.pod -o -name .packlist \) -exec rm -v {} \;
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null ';'

mkdir -p %{buildroot}%{_sysconfdir}/cron.daily \
         %{buildroot}%{_mandir}/man1 \
         %{buildroot}%{_bindir}

# install keyrand
install -c -m 755 keyrand %{buildroot}%{_bindir}/keyrand

# install certwatch
install -c -m 755 certwatch %{buildroot}%{_bindir}/certwatch
install -c -m 755 $RPM_SOURCE_DIR/certwatch.cron \
   %{buildroot}%{_sysconfdir}/cron.daily/certwatch
for f in certwatch genkey keyrand; do 
   install -c -m 644 ${f}.1 %{buildroot}%{_mandir}/man1/${f}.1
done

# install keyutil
install -c -m 755 keyutil %{buildroot}%{_bindir}/keyutil

# install genkey
sed -e "s|^\$bindir.*$|\$bindir = \"%{_bindir}\";|" \
    -e "s|^\$ssltop.*$|\$ssltop = \"/etc/pki/tls\";|" \
    -e "s|^\$sslconf.*$|\$sslconf = \"/etc/pki/tls/openssl.cnf\";|" \
    -e "s|^\$cadir.*$|\$cadir = \"/etc/pki/CA\";|" \
    -e "1s|.*|\#\!/usr/bin/perl|g" \
    -e "s/'Challenge',/'Email','Challenge',/g" \
    -e "/@EXTRA@/d" \
  < $RPM_SOURCE_DIR/genkey.pl > %{buildroot}%{_bindir}/genkey

chmod -R u+w %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(0755,root,root) %{_bindir}/*
%attr(0755,root,root) %{_sysconfdir}/cron.daily/certwatch
%{_mandir}/man*/*
%doc LICENSE* COPYING
%{perl_vendorarch}/Crypt
%{perl_vendorarch}/auto/Crypt

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-13m)
- rebuild against perl-5.14.1

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-12m)
- update genkey.pl

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.1-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-6m)
- rebuild against perl-5.12.1

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-5m)
- explicitly link liblplc4 and libslang

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-2m)
- rebuild against perl-5.10.1

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-1m)
- sync with Fedora 11 (2.4.1-18)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-5m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3-2m)
- rebuild against perl-5.10.0-1m

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-1m)
- import from fc-devel to Momonga
- delete perl-devel, perl(Newt) from BuildRequires
- use %%{buildroot}

* Thu Mar  1 2007 Joe Orton <jorton@redhat.com> 2.3-2
- various cleanups; require perl(Newt) throughout not newt-perl

* Thu Aug 17 2006 Joe Orton <jorton@redhat.com> 2.3-1
- add GPL-licensed keyrand replacement (#20254)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2.2-9.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.2-9.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.2-9.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Nov 24 2005 Joe Orton <jorton@redhat.com> 2.2-9
- rebuild for new slang

* Tue Nov  8 2005 Tomas Mraz <tmraz@redhat.com> - 2.2-8
- rebuilt with new openssl

* Mon Oct  3 2005 Petr Rockai <prockai@redhat.com> - 2.2-7
- rebuild against newt 0.52

* Thu May 26 2005 Joe Orton <jorton@redhat.com> 2.2-6
- certwatch: use UTC time correctly (Tomas Mraz, #158703)

* Fri May 13 2005 Joe Orton <jorton@redhat.com> 2.2-5
- genkey(1): fix paths to use /etc/pki

* Wed Apr 27 2005 Joe Orton <jorton@redhat.com> 2.2-4
- genkey: create private key files with permissions 0400
- genkey: tidy up error handling a little

* Tue Apr 26 2005 Joe Orton <jorton@redhat.com> 2.2-3
- pass $OPTIONS to $HTTPD in certwatch.cron
- man page tweaks

* Tue Apr 26 2005 Joe Orton <jorton@redhat.com> 2.2-2
- add configuration options for certwatch (#152990)
- allow passing options in certwatch.cron via $CERTWATCH_OPTS
- require openssl with /etc/pki/tls

* Mon Apr 25 2005 Joe Orton <jorton@redhat.com> 2.2-1
- adapt to use /etc/pki

* Fri Mar  4 2005 Joe Orton <jorton@redhat.com> 2.1-6
- rebuild

* Tue Feb 15 2005 Joe Orton <jorton@redhat.com> 2.1-5
- certwatch: prevent warnings for duplicate certs (#103807)
- make /etc/cron.daily/certwatch 0755 (#141003)
- add genkey(1) man page (#134821)

* Tue Oct 19 2004 Joe Orton <jorton@redhat.com> 2.1-4
- make certwatch(1) warning distro-neutral
- update to crypto-rand 1.1, fixing #136093

* Wed Oct 13 2004 Joe Orton <jorton@redhat.com> 2.1-3
- send warnings To: root rather than root@localhost (#135533)

* Wed Oct  6 2004 Joe Orton <jorton@redhat.com> 2.1-2
- add BuildRequire newt-devel, xmlto (#134695)

* Fri Sep 10 2004 Joe Orton <jorton@redhat.com> 2.1-1
- add /usr/bin/certwatch
- support --days argument to genkey (#131045)

* Tue Aug 17 2004 Joe Orton <jorton@redhat.com> 2.0-6
- add perl MODULE_COMPAT requirement

* Mon Aug 16 2004 Joe Orton <jorton@redhat.com> 2.0-5
- rebuild

* Mon Sep 15 2003 Joe Orton <jorton@redhat.com> 2.0-4
- hide private key passwords during entry
- fix CSR generation

* Mon Sep  1 2003 Joe Orton <jorton@redhat.com> 2.0-3
- fix warnings when in UTF-8 locale

* Tue Aug 26 2003 Joe Orton <jorton@redhat.com> 2.0-2
- allow upgrade from Stronghold 4.0

* Mon Aug  4 2003 Joe Orton <jorton@redhat.com> 2.0-1
- update for RHEL

* Wed Sep 11 2002 Joe Orton <jorton@redhat.com> 1.0-12
- rebuild

* Thu Aug 22 2002 Joe Orton <jorton@redhat.com> 1.0-11
- fix location of OpenSSL configuration file in gencert

* Mon Jul 15 2002 Joe Orton <jorton@redhat.com> 1.0-10
- fix getca SERVERROOT, SSLTOP expansion (#68870)

* Mon May 13 2002 Joe Orton <jorton@redhat.com> 1.0-9
- improvements to genkey

* Mon May 13 2002 Joe Orton <jorton@redhat.com> 1.0-8
- add php.ini handling to stronghold-config 

* Mon May 13 2002 Joe Orton <jorton@redhat.com> 1.0-7
- restore stronghold-config

* Tue May 07 2002 Gary Benson <gbenson@redhat.com> 1.0-6
- remove stronghold-config

* Tue Apr 09 2002 Gary Benson <gbenson@redhat.com> 1.0-5
- change the group to match crypto-rand
- change Copyright to License

* Mon Mar 25 2002 Gary Benson <gbenson@redhat.com> 1.0-4
- hack to clean up some cruft that gets left in the docroot after we
  install.

* Fri Mar 22 2002 Gary Benson <gbenson@redhat.com>
- excise interchange.

* Wed Feb 13 2002 Gary Benson <gbenson@redhat.com> 1.0-3
- ask about interchange too.
- make /etc/sysconfig/httpd nicer.

* Thu May 17 2001 Joe Orton <jorton@redhat.com>
- Redone for Red Hat Linux.

* Mon Mar 20 2001 Mark Cox <mjc@redhat.com>
- Changes to make genkey a perl script

* Mon Dec 04 2000 Joe Orton <jorton@redhat.com>
- Put the stronghold/bin -> stronghold/ssl/bin symlink in the %files section
  rather than creating it in %post.

* Fri Nov 24 2000 Mark Cox <mjc@redhat.com>
- No need for .configure scripts, do the substitution ourselves

* Tue Nov 21 2000 Mark Cox <mjc@redhat.com>
- First version. Because this depends on a build environment
- We won't worry about ni-scripts for now, they're not used anyhow

