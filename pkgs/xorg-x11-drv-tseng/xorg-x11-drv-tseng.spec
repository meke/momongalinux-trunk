%global momorel 1
%define tarball xf86-video-tseng
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 tseng video driver
Name:      xorg-x11-drv-tseng
Version:   1.2.5
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
Source1:   tseng.xinf
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} alpha

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.11.99

Requires:  xorg-x11-server-Xorg >= 1.11.99

%description 
X.Org X11 tseng video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
autoreconf -ivf
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_datadir}/hwdata/videoaliases
install -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/hwdata/videoaliases/

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/tseng_drv.so
%{_datadir}/hwdata/videoaliases/tseng.xinf
%{_mandir}/man4/tseng.4*

%changelog
* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5
- rebuild with xorg-x11-server-1.13.0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-7m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-6m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-5m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-4m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-2m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-2m)
- rebuild against xorg-x11-server-1.7.1

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update 1.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against rpm-4.6

* Sun May  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-2m)
- rebuild against xorg-x11-server-1.4

* Sun Nov 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0.5-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.5-1.1m)
- import to Momonga

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0.5-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.0.5-1
- Updated xorg-x11-drv-tseng to version 1.0.0.5 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.4-1
- Updated xorg-x11-drv-tseng to version 1.0.0.4 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.2-1
- Updated xorg-x11-drv-tseng to version 1.0.0.2 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-tseng to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Tue Oct 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for tseng video driver generated automatically
  by my xorg-driverspecgen script.
