%global momorel 1

Summary: Spell checker
Name: aspell
Version: 0.60.6.1
Release: %{momorel}m%{?dist}
# LGPLv2+ .. aspell-0.60.6/misc/po-filter.c, ltmain.sh, modules/speller/default/affix.cpp
# GPLv2+  .. aspell-0.60.6/misc/po-filter.c, aspell-0.60.6/ltmain.sh
# BSD     .. myspell/munch.c
License: LGPLv2 and LGPLv2+ and GPLv2+ and MIT
Group: Applications/Text
URL: http://aspell.net/
Source0: ftp://ftp.gnu.org/gnu/aspell/aspell-%{version}.tar.gz
NoSource: 0
Patch3: aspell-0.60.6-install_info.patch
Patch5: aspell-0.60.5-fileconflict.patch
Patch7: aspell-0.60.5-pspell_conf.patch
Patch8: aspell-0.60.6-zero.patch
Patch9: aspell-0.60.6-mp.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext, ncurses-devel, pkgconfig
BuildRequires: chrpath
Requires(pre): info
Requires(preun): info

%description
GNU Aspell is a spell checker designed to eventually replace Ispell. It can
either be used as a library or as an independent spell checker. Its main
feature is that it does a much better job of coming up with possible
suggestions than just about any other spell checker out there for the
English language, including Ispell and Microsoft Word. It also has many
other technical enhancements over Ispell such as using shared memory for
dictionaries and intelligently handling personal dictionaries when more
than one Aspell process is open at once.

%package	devel
Summary: Libraries and header files for Aspell development
Group: Development/Libraries
Requires: aspell = %{version}-%{release}
Requires: pkgconfig
Requires(pre): info
Requires(preun): info

%description	devel
Aspell is a spelling checker. The aspell-devel package includes
libraries and header files needed for Aspell development.

%prep
%setup -q -n aspell-%{version}
%patch3 -p1 -b .iinfo
%patch5 -p1 -b .fc
%patch7 -p1 -b .mlib
%patch8 -p1 -b .zero
%patch9 -p1 -b .ai
iconv -f windows-1252 -t utf-8 manual/aspell.info -o manual/aspell.info.aux
mv manual/aspell.info.aux manual/aspell.info

%build
%configure --disable-rpath
make %{?_smp_mflags}
cp scripts/aspell-import examples/aspell-import
chmod 644 examples/aspell-import
cp manual/aspell-import.1 examples/aspell-import.1

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60

mv ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60/ispell ${RPM_BUILD_ROOT}%{_bindir}
mv ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60/spell ${RPM_BUILD_ROOT}%{_bindir}

chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60//nroff-filter.so
chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60//sgml-filter.so
chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60//context-filter.so
chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60//email-filter.so
chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60//tex-filter.so
chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60//texinfo-filter.so
chrpath --delete ${RPM_BUILD_ROOT}%{_bindir}/aspell
chrpath --delete ${RPM_BUILD_ROOT}%{_libdir}/libpspell.so.*


rm -f ${RPM_BUILD_ROOT}%{_libdir}/libaspell.la
rm -f ${RPM_BUILD_ROOT}%{_libdir}/libpspell.la
rm -f ${RPM_BUILD_ROOT}%{_libdir}/aspell-0.60/*-filter.la
rm -f ${RPM_BUILD_ROOT}%{_bindir}/aspell-import
rm -f ${RPM_BUILD_ROOT}%{_mandir}/man1/aspell-import.1

%find_lang %{name}

%post
/sbin/ldconfig
if [ -f %{_infodir}/aspell.info* ]; then
    /sbin/install-info %{_infodir}/aspell.info %{_infodir}/dir --entry="* Aspell: (aspell). "  || : 
fi

%post        devel
if [ -f %{_infodir}/aspell-dev.info* ]; then
    /sbin/install-info %{_infodir}/aspell-dev.info %{_infodir}/dir --entry="* Aspell-dev: (aspell-dev). " || :
fi

%preun
if [ $1 = 0 ]; then
    if [ -f %{_infodir}/aspell.info* ]; then
        /sbin/install-info --delete %{_infodir}/aspell.info %{_infodir}/dir || :
    fi
fi

%preun       devel
if [ $1 = 0 ]; then
    if [ -f %{_infodir}/aspell-dev.info* ]; then
        /sbin/install-info --delete %{_infodir}/aspell-dev.info %{_infodir}/dir || :
    fi
fi

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README TODO COPYING examples/aspell-import examples/aspell-import.1
%dir %{_libdir}/aspell-0.60
%{_bindir}/a*
%{_bindir}/ispell
%{_bindir}/pr*
%{_bindir}/run-with-aspell
%{_bindir}/spell
%{_bindir}/word-list-compress
%{_libdir}/lib*.so.*
%{_libdir}/aspell-0.60/*
%{_infodir}/aspell.*
%{_mandir}/man1/aspell.1.*
%{_mandir}/man1/run-with-aspell.1*
%{_mandir}/man1/word-list-compress.1*
%{_mandir}/man1/prezip-bin.1.*

%files		devel
%defattr(-,root,root,-)
%dir %{_includedir}/pspell
%{_bindir}/pspell-config
%{_includedir}/aspell.h
%{_includedir}/pspell/pspell.h
%{_libdir}/lib*spell.so
%{_libdir}/pkgconfig/*
%{_infodir}/aspell-dev.*
%{_mandir}/man1/pspell-config.1*

%changelog
* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60.6.1-1m)
- update to 0.60.6.1

* Wed Sep  7 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.60.6-8m)
- sync Fedora 16 (12:0.60.6-15)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.60.6-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.60.6-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.60.6-5m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60.6-4m)
- add executable flag to aspell-import

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60.6-2m)
- fix %%files

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60.6-1m)
- update to 0.60.6
- import Patch8 from Fedora 11 (12:0.60.6-5)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60.5-6m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60.5-5m)
- drop Patch1 for fuzz=0, already merged upstream
- update Patch3 for fuzz=0

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.60.5-4m)
- sync Fedora
- some patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.60.5-3m)
- rebuild against gcc43

* Wed Jan  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.60.5-2m)
- added patch for gcc-4.3
- updated alternatives handling

* Sun May 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60.5-1m)
- update 0.60.5

* Mon Apr 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.60.4-3m)
- revise BuildPrereq

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60.4-2m)
- delete libtool library

* Sun Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.60.4-1m)
- update 0.60.4
- support gcc-4.1
-- add aspell-0.60.4-nroff-gcc41.patch
- remove aspell-0.60.3-long_gettext.patch

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (0.60.2-3m)
- pkgdatadir and pkglibdir should be /usr/lib because of noarch.

* Sat Jan 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.60.2-2m)
- revise alternatives

* Thu Jan  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.60.2-1m)
- major bugfixes

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.60-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sat Aug 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.60-1m)
- major feature enhancements

* Mon May 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.50.5-1m)
- version up

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.50.3-5m)
- rebuild against ncurses 5.3.

* Sun Jul 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.50.3-4m)
- include assert.h (aspell-0.50.3-assert.patch)

* Tue Apr 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.50.3-3m)
- remove Conflicts tag. (are..

* Thu Mar 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.50.3-2m)
- alternatives

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.50.3-1m)
- version 0.50.3

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (.33.7.1-5m)
- update define pspell to .12.2-9m
- add pspell at BuildPrereq

* Mon Feb 24 2002 Shingo Akagaki <dora@kondara.org>
- (.33.7.1-4k)
- just rebuild for .la fix

* Tue Oct 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (.33.7.1-2k)
- update to .33.7.1

* Wed Jan 31 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (.32.6-3k)
- update to .32.6

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (.32.5-9k)
- rebuild against gcc 2.96.

* Thu Dec  7 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (.32.5-7k)
- fixed document file problerm (jitterbug #802)

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (.32.5-5k)
- modified the bug that failed if %{_docdir} is /usr/doc

* Thu Nov 23 2000 Kenichi Matsubara <m@kondara.org>
- modified %Build section.

* Sat Sep 09 2000 Toru Hoshina <t@kondara.org>
- do not need to copy stl_rope.h on gcc 2.96 environment.
- version up [32.5-0.1]

* Mon May 02 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- Kondara fix.

* Mon May  1 2000 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.30.1-1]
- Ported Aspell to Win32 platforms.
- Portability fixes which may help aspell compile on other platforms.
- Aspell will no longer fail if for some reason the mmap fails, instead it
  will just read the file in as normal and free the memory when done.
- Minor changes in the format of the main word list as a result of the
  changes, the old format should still work in most cases.
- Fixed a bug when aspell was ignoring the extension of file names such as
  .html or .tex when checking files.
- Fixed a bug when aspell will go into an infinite loop when creating the
  main word list from a word list which has duplicates in it.
- Minor changes to the manual for better clarity.

* Mon Apr  3 2000 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.30-1]
- Fixed many of the capitalization bugs found in previous versions of Aspell.
- Changed the format of the main word list yet again.
- Fixed a bug so that ``aspell check'' will work on the PowerPC.
- Added ability to change configuration options in the middle of a session.
- Added words from /usr/dict/words found on most Linux systems as well as
  a bunch of commonly used abbreviation to the word list.
- Fixed a bug when aspell will dump core after reporting certain errors
  when compiled with gcc 2.95 or higher. This involved reworked the
  Exception heritage to get around a bug in gcc 2.95.
- Added a few more commands to the list of default commands the TEX
  filter knows about.
- Aspell will now check if a word only contains valid characters before
  adding it to any dictionaries. This might mean that you have to
  manually delete a few words from your personal word list.
- Added option to ignore case when checking a document.
- Adjusted the parameters of the ``normal'' suggest mode to so that
  significantly less far fetched results are returned in cases such as
  tomatoe, which went from 100 suggestions down to 32, at the expense of
  getting slightly lower results (less than 1%),
- Improved the edit distance algorithm for slightly faster results.
- Removed the $$m command in pipe mode, you should now use ``$$cs mode,
  mode '' to set the mode and ``$$cr mode'' to find out the current mode.
- Reworked parts of Aspell to use Pspell services to avoid duplicating code.
- Added a module for the newly released Pspell. It will get installed
  with the rest of aspell.
- Miscellaneous other bug fixes.

* Mon Feb 21 2000 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.29.1-1]
- Improved the TEX filter so that it will accept '@' at the begging of a
  command name and ignore trailing '*'s. It also now has better defaults
  for which parameters to skip.
- Reworked the main dictionary so that it can be memory mapped in. This
  decreases startup time and allows multiple aspell processes to use the
  same memory for the main word list. This also also made Aspell 64 bit
  clean so that it should work on an alpha now.
- Fix so that aspell can compile on platforms that gnu as is not
  available for.
- Fixed issue with flock so it would compile on FreeBSD.
- Minor changes in the code to make it more C++ compliant although I am
  sure there will still be problems when using some other compiler other
  than gcc or egcs.
- Added some comments to the header files to better document a few of the
  classes.

* Mon Feb  7 2000 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.29-1]
- Fixed a bug in the pipe mode with lines that start with ``^$$''.
- Added support for ignoring all words less than or equal to a specified
  length
- New soundslike code based thanks to the contribution of Bj rn Jacke. It
  now gets all of its data from a table making it easier for other people
  to add soundslike code for their native language. He also converted the
  metaphone algorithm to table form, eliminating the need for the old
  metaphone code.
- Major redesign of the suggestion code for better results.
- Changed the format of the personal word lists. In most cases it should
  be converted automatically.
- Changed the format of the main word list.
- Name space cleanup for more consistent naming. I now use name spaces
  which means that gcc 2.8.* and egcs 1.0.* will no longer cut it.
- Used file locks when reading and saving the personal dictionaries so
  that it truly multiprocess safe.
- Added rudimentary filter support.
- Reworked the configuration system once again. However, the changes to
  the end user who does not directly use my library should be minimal.
- Rewrote my code that handles parsing command line parameters so that it
  no longer uses popt as it was causing to many problems and didn't
  integrate well with my new configuration system.
- Fixed pipe mode so that it will properly ignore lines starting with '~'
  for better ispell compatibility.
- Aspell now has a new home page at http://aspell.sourceforge.net/.
  Please make note of the new URL.
- Miscellaneous manual fixes and clarifications.

* Mon Nov 22 1999 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.28.3-1]
- Fixed a bug that caused aspell to crash when spell checking words
  over 60 characters long.
- Reworked ``aspell check'' so that
  1. You no longer have to hit enter when making a choice.
  2. It will now overwrite the orignal file instead of creating a
     new file. An optional backup can be made by using the -b option.
- Fixed a few bugs in data.cc.

* Thu Aug 26 1999 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.28.2.1-1]
- Fixed the version number for the shared library.
- Fixed a problem with undefined refrences when linking to the
  shared library.

  [aspell-.28.2]
- Fixed a bunch of bugs in the language and configuration classes.
- Minor changed in the code so that it could compile with the new
  gcc 2.95(.1).
- Changed the output of ``dump config'' so that default values are
  given the value "<default>". This means that the output can be
  used to created a configuration file.
- Added notes on using aspell with VIM.

* Wed Jul 28 1999 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.28.1-2]
- Opps. Wasn't patching stl_rope.h on RedHat-6.0.
- Passing make the RPM_OPT_FLAGS now.

* Wed Jul 28 1999 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.28.1-1]
- Changes from .28 to .28.1 (July 27, 1999)
- Removed some debug output.
- Changed notes on compiling with gcc 2.8.* as I managed to get it
  to compile on my school account.
- Avoided included stdexcept in const_string.hh so that I could get
  to compile on my schools account with gcc 2.8.1.

* Mon Jul 26 1999 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.28-1]
- Changes from .27.2 to .28 (July 25, 1999)
- Provided an iterator for the replacement classes.
- Added support for dumping and creating and merging the personal
  and replacement word lists.
- Changed the aspell utility command line a bit, it now used popt.
- Totally reworked aspell configuration system. Now aspell can get
  configuration from any of 5 sources: the command line, the
  environmental variable ASPELL_CONF, the personal configuration
  file, the global configuration file, and finally the compiled in
  defaults.
- Totally reworked the language class in preparation for my new
  language code. See
  http://metalab.unc.edu/kevina/aspell/international/ for more
  information of what I have in store.
- Added some options to the configure script:
  -language-enable-dict-dir=DIR, -enable-doc-dir=DIR, -enable-debug,
  and -enable-opt
- Removed some old header files.
- Reoriginized the directory structure a bit
- Made the text version of the manual pages slightly easier to read
- Usded the \url command for urls for better formating of the
  printed version.

* Tue Mar  2 1999 Ryan Weaver <ryanw@infohwy.com>
  [aspell-.27.2-2]
- Changes from .27.1 to .27.2 (Mar 1, 1999)
- Fixed a major bug that caused aspell to dump core when used
  without any arguments
- Fixed another major bug that caused aspell to do nothing when used
  in interactive mode.
- Added an option to exit in Aspell's interactive mode.
- Removed some old documentation files from the distribution.
- Minor changes on to the section on using Aspell with egcs.
- Minor changes to remove -Wall warnings.
