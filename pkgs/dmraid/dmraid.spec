%global momorel 6

Summary: Device-mapper RAID tool and library
Name: dmraid
Version: 1.0.0.rc16
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://people.redhat.com/heinzm/sw/dmraid
Source: http://people.redhat.com/~heinzm/sw/dmraid/src/dmraid-%{version}.tar.bz2
Patch0: dmraid-1.0.0.rc16-test_devices.patch
Patch1: ddf1_lsi_persistent_name.patch
Patch2: pdc_raid10_failure.patch
Patch3: return_error_wo_disks.patch
Patch4: fix_sil_jbod.patch
Patch5: avoid_register.patch
Patch6: move_pattern_file_to_var.patch
Patch7: libversion.patch
Patch8: libversion-display.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: device-mapper-devel >= 1.02.02, libselinux-devel, libsepol-devel, zlib-devel
BuildRequires: device-mapper-event-devel
Requires: device-mapper
Requires: dmraid-events
Requires: kpartx
Obsoletes: dmraid-libs < %{version}-%{release}
Provides: dmraid-libs = %{version}-%{release}

%description
DMRAID supports RAID device discovery, RAID set activation, creation,
removal, rebuild and display of properties for ATARAID/DDF1 metadata on
Linux >= 2.4 using device-mapper.

%package devel
Summary: Development libraries and headers for dmraid.
Group: Development/Libraries
License: GPLv2+
Requires: %{name} = %{version}-%{release}

%description devel
dmraid-devel provides a library interface for RAID device discovery,
RAID set activation, creation, removal, rebuild and display of
properties for ATARAID/DDF1 metadata.

%package -n dmraid-events
Summary: dmevent_tool (Device-mapper event tool) and DSO
Group: System Environment/Base
Requires: dmraid = %{version}-%{release}, sgpio
Requires: device-mapper-event

%description -n dmraid-events
Provides a dmeventd DSO and the dmevent_tool to register devices with it
for device monitoring.  All active RAID sets should be manually registered
with dmevent_tool.

%package -n dmraid-events-logwatch
Summary: dmraid logwatch-based email reporting
Group: System Environment/Base
Requires: dmraid-events = %{version}-%{release}, logwatch, cronie

%description -n dmraid-events-logwatch
Provides device failure reporting via logwatch-based email reporting.
Device failure reporting has to be activated manually by activating the
/etc/cron.d/dmeventd-logwatch entry and by calling the dmevent_tool
(see manual page for examples) for any active RAID sets.

%prep
%setup -q -n dmraid/%{version}

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1

%build
%define _libdir /%{_lib}

%configure --prefix=${RPM_BUILD_ROOT}/usr --sbindir=${RPM_BUILD_ROOT}/sbin --libdir=${RPM_BUILD_ROOT}/%{_libdir} --mandir=${RPM_BUILD_ROOT}/%{_mandir} --includedir=${RPM_BUILD_ROOT}/%{_includedir} --enable-debug --enable-libselinux --enable-libsepol --disable-static_link --enable-led --enable-intel_led
make DESTDIR=$RPM_BUILD_ROOT 

%install
rm -rf %{buildroot}
install -m 755 -d $RPM_BUILD_ROOT{%{_libdir},/sbin,%{_sbindir},%{_bindir},%{_libdir},%{_includedir}/dmraid/,/var/lock/dmraid,/etc/cron.d/,/etc/logwatch/conf/services/,/etc/logwatch/scripts/services/,/var/cache/logwatch/dmeventd}
make DESTDIR=$RPM_BUILD_ROOT install
ln -s dmraid $RPM_BUILD_ROOT/sbin/dmraid.static

# Provide convenience link from dmevent_tool
(cd $RPM_BUILD_ROOT/sbin ; ln -f dmevent_tool dm_dso_reg_tool)
(cd $RPM_BUILD_ROOT/%{_mandir}/man8 ; ln -f dmevent_tool.8 dm_dso_reg_tool.8 ; ln -f dmraid.8 dmraid.static.8)

install -m 644 include/dmraid/*.h $RPM_BUILD_ROOT%{_includedir}/dmraid/

# Install the libdmraid and libdmraid-events (for dmeventd) DSO
# Create version symlink to libdmraid.so.1 we link against
install -m 755 lib/libdmraid.so \
	$RPM_BUILD_ROOT%{_libdir}/libdmraid.so.%{version}
(cd $RPM_BUILD_ROOT/%{_libdir} ; ln -sf libdmraid.so.%{version} libdmraid.so ; ln -sf libdmraid.so.%{version} libdmraid.so.1)
install -m 755 lib/libdmraid-events-isw.so \
	$RPM_BUILD_ROOT%{_libdir}/libdmraid-events-isw.so.%{version}
(cd $RPM_BUILD_ROOT/%{_libdir} ; ln -sf libdmraid-events-isw.so.%{version} libdmraid-events-isw.so; ln -sf libdmraid-events-isw.so.%{version} libdmraid-events-isw.so.1)

# Install logwatch config file and script for dmeventd
install -m 644 logwatch/dmeventd.conf $RPM_BUILD_ROOT/etc/logwatch/conf/services/dmeventd.conf
install -m 755 logwatch/dmeventd $RPM_BUILD_ROOT/etc/logwatch/scripts/services/dmeventd
install -m 644 logwatch/dmeventd_cronjob.txt $RPM_BUILD_ROOT/etc/cron.d/dmeventd-logwatch
install -m 0700 /dev/null $RPM_BUILD_ROOT/var/cache/logwatch/dmeventd/syslogpattern.txt

rm -f $RPM_BUILD_ROOT/%{_libdir}/libdmraid.a

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
 
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc CHANGELOG CREDITS KNOWN_BUGS LICENSE LICENSE_GPL LICENSE_LGPL README TODO doc/dmraid_design.txt
/%{_mandir}/man8/dmraid*
/sbin/dmraid
/sbin/dmraid.static
%{_libdir}/libdmraid.so*
%{_libdir}/libdmraid-events-isw.so*
/var/lock/dmraid

%files -n dmraid-devel
%defattr(-,root,root)
%dir %{_includedir}/dmraid
%{_includedir}/dmraid/*

%files -n dmraid-events
%defattr(-,root,root)
/%{_mandir}/man8/dmevent_tool*
/%{_mandir}/man8/dm_dso_reg_tool*
/sbin/dmevent_tool
/sbin/dm_dso_reg_tool

%files -n dmraid-events-logwatch
%defattr(-,root,root)
%config(noreplace) /etc/logwatch/conf/services/dmeventd.conf
%config(noreplace) /etc/logwatch/scripts/services/dmeventd
%config(noreplace) /etc/cron.d/dmeventd-logwatch
%dir /var/cache/logwatch/dmeventd
%ghost /var/cache/logwatch/dmeventd/syslogpattern.txt

%changelog
* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.rc16-6m)
- fix BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.rc16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.rc16-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.rc16-3m)
- full rebuild for mo7 release

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.rc16-2m)
- modify Requires of events-logwatch
- release /etc/logwatch/* from package dmraid-events-logwatch
- it's already provided by logwatch and dmraid-events-logwatch Requires: logwatch
- and specify two targets of %%config(noreplace)

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.rc16-1m)
- update 1.0.0.rc16
- split package. dmraid-events, dmraid-events-logwatch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.rc15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-rc15-2m)
- add LDFLAGS (build fix)

* Wed May  5 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0.rc15-1m)
- sync with fedora 1.0.0rc15-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.rc14-4m)
- rebuild against rpm-4.6

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.rc14-3m)
- import fedora patches(dmraid-1.0.0.rc14-6)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.rc14-2m)
- rebuild against gcc43

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.rc14-1m)
- update dmraid-1.0.0.rc14

* Tue Dec 12 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0.rc13-1m)
- sync with dmraid-1.0.0.rc13-1.fc6

* Wed Oct 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.rc9-3m)
- maki modosi

* Wed Oct 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.rc13-1m)
- update 1.0.0.rc13

* Wed May 24 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- fix build. new "install" command fail with -o, -g option.

* Wed Dec 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0.rc9-1m)
- import from fc-devel

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sun Dec  3 2005 Peter Jones <pjones@redhat.com> 1.0.0.rc9-FC5_4
- rebuild for device-mapper-1.02.02-2

* Fri Dec  2 2005 Peter Jones <pjones@redhat.com> 1.0.0.rc9-FC5_3
- rebuild for device-mapper-1.02.02-1

* Thu Nov 10 2005 Peter Jones <pjones@redhat.com> 1.0.0.rc9-FC5_2
- update to 1.0.0.rc9
- make "make install" do the right thing with the DSO
- eliminate duplicate definitions in the headers
- export more symbols in the DSO
- add api calls to retrieve dm tables
- fix DESTDIR for 'make install' 
- add api calls to identify degraded devices
- remove several arch excludes

* Sat Oct 15 2005 Florian La Roche <laroche@redhat.com>
- add -lselinux -lsepol for new device-mapper deps

* Fri May 20 2005 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0.rc8-FC4_2
- specfile change to build static and dynamic binray into one package
- rebuilt

* Thu May 19 2005 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0.rc8-FC4_1
- nv.c: fixed stripe size
- sil.c: avoid incarnation_no in name creation, because the Windows
         driver changes it every time
- added --ignorelocking option to avoid taking out locks in early boot
  where no read/write access to /var is given

* Wed Mar 16 2005 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 15 2005 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0.rc6.1-4_FC4
- VIA metadata format handler
- added RAID10 to lsi metadata format handler
- "dmraid -rD": file device size into {devicename}_{formatname}.size
- "dmraid -tay": pretty print multi-line tables ala "dmsetup table"
- "dmraid -l": display supported RAID levels + manual update
- _sil_read() used LOG_NOTICE rather than LOG_INFO in order to
  avoid messages about valid metadata areas being displayed
  during "dmraid -vay".
- isw, sil filed metadata offset on "-r -D" in sectors rather than in bytes.
- isw needed dev_sort() to sort RAID devices in sets correctly.
- pdc metadata format handler name creation. Lead to
  wrong RAID set grouping logic in some configurations.
- pdc RAID1 size calculation fixed (rc6.1)
- dos.c: partition table code fixes by Paul Moore
- _free_dev_pointers(): fixed potential OOB error
- hpt37x_check: deal with raid_disks = 1 in mirror sets
- pdc_check: status & 0x80 doesn't always show a failed device;
  removed that check for now. Status definitions needed.
- sil addition of RAID sets to global list of sets
- sil spare device memory leak
- group_set(): removal of RAID set in case of error
- hpt37x: handle total_secs > device size
- allow -p with -f
- enhanced error message by checking target type against list of
  registered target types

* Fri Jan 21 2005 Alasdair Kergon <agk@redhat.com> 1.0.0.rc5f-2
- Rebuild to pick up new libdevmapper.

* Fri Nov 26 2004 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0.rc5f
- specfile cleanup

* Tue Aug 20 2004 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0-rc4-pre1
- Removed make flag after fixing make.tmpl.in

* Tue Aug 18 2004 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0-rc3
- Added make flag to prevent make 3.80 from looping infinitely

* Thu Jun 17 2004 Heinz Mauelshagen <heinzm@redhat.com> 1.0.0-pre1
- Created
