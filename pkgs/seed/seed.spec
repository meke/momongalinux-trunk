%global momorel 1

Summary: WebKit JavaScriptCore engine
Name: seed
Version: 3.2.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: User Interface/Desktops
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: webkitgtk-devel
BuildRequires: gobject-introspection-devel >= 0.9.6
BuildRequires: gnome-js-common-devel
BuildRequires: cairo-devel >= 1.10.0
BuildRequires: gtk3-devel
BuildRequires: gmp-devel >= 5.0.0
BuildRequires: sqlite-devel
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: libxml2-devel
BuildRequires: gtk-doc >= 1.15
BuildRequires: webkitgtk-devel >= 1.3.4

Requires: gobject-introspection
Requires: gnome-js-common

%description
Seed is a library and interpreter, dynamically bridging (through
gobject-introspection) the WebKit JavaScriptCore engine with the
GObject type system. In a more concrete sense, Seed enables you to
immediately write applications around a significant portion of the
GNOME platform, and easily embed JavaScript as a scripting language in
your GObject library.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --disable-static \
    --enable-silent-rules \
    --enable-gtk-doc \
    --with-webkit=3.0
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/seed
%{_libdir}/libseed-gtk3.so.*
%exclude %{_libdir}/*.la
%{_libdir}/%{name}-gtk3
%{_mandir}/man1/%{name}.1.*
%{_datadir}/%{name}-gtk3

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}-gtk3
%{_libdir}/*.so
%{_libdir}/pkgconfig/seed.pc
%{_datadir}/doc/%{name}
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.1-1m)
- update to 3.2.0

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-3m)
- rebuild against webkitgtk-1.3.13-7m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.91-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.31.91-3m)
- rebuild against gmp-5.0.1

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.91-2m)
- rebuild against webkitgtk-1.3.4

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.91-1m)
- update to 2.31.91

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-5m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-4m)
- add Requires: gir-repository

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.30.0-3m)
- rebuild against readline6

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.30.0-2m)
- explicitly link libglib-2.0 and libgthread-2.0

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91.1-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- initial build
