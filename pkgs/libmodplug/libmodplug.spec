%global momorel 1


Summary: libmodplug - a MOD playing library
Name: libmodplug
Version: 0.8.8.4
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/project/modplug-xmms/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.8.4-timiditypaths.patch
URL: http://modplug-xmms.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
Obsoletes: xmms-modplug <= 2.03-1m

%description
Libmodplug is the library behind -- Modplug-XMMS is a fully featured,
complete input plugin for XMMS which plays mod-like music formats. It
is based on the mod rendering code from ModPlug, a popular windows mod
player written by Olivier Lapicque, found at http://www.modplug.com

%package devel
Group: Development/Libraries
Summary: Header files for compiling against Modplug library
Provides: %{name}-devel = %{version}
Requires: %{name} = %{version}

%description devel
This is the development package of libmodplug. Install it if you want to
compile programs using this library.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .timiditypaths

%build

%configure
%make

%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README COPYING ChangeLog COPYING NEWS TODO
%{_libdir}/%{name}.so*

%files devel
%defattr(-,root,root)
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8.4-1m)
- [SECURITY] CVE-2011-1761 CVE-2011-2911 CVE-2011-2912 CVE-2011-2913
- [SECURITY] CVE-2011-2914 CVE-2011-2915
- update to 0.8.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.8.7-2m)
- add define __libtoolize

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-1m)
- [SECURITY] CVE-2009-1438 CVE-2009-1513
- update to 0.8.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.4-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-2m)
- delete libtool library

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4
- [SECURITY] CVE-2006-4192, fix buffer overflow vulnerabilities

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.7-1m)
- Initial specfile.
