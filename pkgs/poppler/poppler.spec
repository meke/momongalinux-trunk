%global momorel 1

Summary: PDF rendering library
Name: poppler
Version: 0.24.3
Release: %{momorel}m%{?dist}
License: GPLv2 or GPLv3
Group: Development/Libraries
URL:     http://poppler.freedesktop.org/
Source0: http://poppler.freedesktop.org/poppler-%{version}.tar.xz
NoSource: 0

## upstreamable patches
# http://bugzilla.redhat.com/show_bug.cgi?id=480868
Patch1: poppler-0.12.4-annot-appearance.patch

Requires: poppler-data >= 0.4.0
BuildRequires: automake libtool
BuildRequires: gettext-devel
BuildRequires: libjpeg-devel
BuildRequires: openjpeg-devel >= 1.3-5
BuildRequires: pkgconfig(cairo) >= 1.10.0
BuildRequires: pkgconfig(gobject-introspection-1.0) 
BuildRequires: pkgconfig(gtk+-2.0)
BuildRequires: pkgconfig(lcms)
BuildRequires: pkgconfig(QtGui) pkgconfig(QtXml)
BuildRequires: pkgconfig(Qt5Xml) pkgconfig(Qt5Widgets) pkgconfig(Qt5Gui) pkgconfig(Qt5Core)


%description
Poppler, a PDF rendering library, is a fork of the xpdf PDF
viewer developed by Derek Noonburg of Glyph and Cog, LLC.

%package devel
Summary: Libraries and headers for poppler
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
You should install the poppler-devel package if you would like to
compile applications based on poppler.

%package glib
Summary: Glib wrapper for poppler
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description glib
%{summary}.

%package glib-devel
Summary: Development files for glib wrapper
Group: Development/Libraries
Requires: %{name}-glib%{?_isa} = %{version}-%{release}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}

%description glib-devel
%{summary}.


%package qt4
Summary: Qt4 wrapper for poppler
Group:   System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: qt
%description qt4
%{summary}.

%package qt4-devel
Summary: Development files for Qt4 wrapper
Group:   Development/Libraries
Requires: %{name}-qt4%{?_isa} = %{version}-%{release}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Requires: qt-devel
%description qt4-devel
%{summary}.

%package qt5
Summary: Qt5 wrapper for poppler
Group:   System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: qt5-qtbase
Requires: qt5-qtbase-x11
%description qt5
%{summary}.

%package qt5-devel
Summary: Development files for Qt5 wrapper
Group:   Development/Libraries
Requires: %{name}-qt5%{?_isa} = %{version}-%{release}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Requires: qt5-qtbase-devel
Requires: qt5-qtbase-x11
%description qt5-devel
%{summary}.


%package cpp
Summary: Pure C++ wrapper for poppler
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description cpp
%{summary}.

%package cpp-devel
Summary: Development files for C++ wrapper
Group: Development/Libraries
Requires: %{name}-cpp%{?_isa} = %{version}-%{release}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}

%description cpp-devel
%{summary}.

%package utils
Summary: Command line utilities for converting PDF files
Group: Applications/Text
Requires: %{name}%{?_isa} = %{version}-%{release}
%if 0%{?fedora} < 11 && 0%{?rhel} < 6
#  last seen in fc8
Provides: pdftohtml = 0.36-11
Obsoletes: pdftohtml < 0.36-11
#  last seen in fc7
Provides: xpdf-utils = 3.03-2m
Obsoletes: xpdf-utils < 3.03-2m
# even earlier?
Conflicts: xpdf < 3.03-2m
%endif
%description utils
Poppler, a PDF rendering library, is a fork of the xpdf PDF
viewer developed by Derek Noonburg of Glyph and Cog, LLC.

This utils package installs a number of command line tools for
converting PDF files to a number of other formats.

%prep
%setup -q

#patch1 -p1 -b .annot

chmod -x goo/GooTimer.h

iconv -f iso-8859-1 -t utf-8 < "utils/pdftohtml.1" > "utils/pdftohtml.1.utf8"
mv "utils/pdftohtml.1.utf8" "utils/pdftohtml.1"

# hammer to nuke rpaths, recheck on new releases
autoreconf -i -f


%build

# Hack around borkage, http://cgit.freedesktop.org/poppler/poppler/commit/configure.ac?id=9250449aaa279840d789b3a7cef75d06a0fd88e7
PATH=%{_qt4_bindir}:$PATH; export PATH

%configure \
  --disable-static \
  --enable-cairo-output \
  --enable-libjpeg \
  --enable-libopenjpeg \
  --enable-poppler-qt4 \
  --enable-xpdf-headers \
  --disable-zlib \
  --enable-introspection=yes

%make


%install
make install DESTDIR=$RPM_BUILD_ROOT

rm -fv $RPM_BUILD_ROOT%{_libdir}/lib*.la


%check
# verify pkg-config sanity/version
export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion poppler)" = "%{version}"
test "$(pkg-config --modversion poppler-cairo)" = "%{version}"
test "$(pkg-config --modversion poppler-cpp)" = "%{version}"
test "$(pkg-config --modversion poppler-glib)" = "%{version}"
test "$(pkg-config --modversion poppler-qt4)" = "%{version}"
test "$(pkg-config --modversion poppler-qt5)" = "%{version}"
test "$(pkg-config --modversion poppler-splash)" = "%{version}"


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post glib -p /sbin/ldconfig

%postun glib -p /sbin/ldconfig

%post qt4 -p /sbin/ldconfig

%postun qt4 -p /sbin/ldconfig

%post qt5 -p /sbin/ldconfig

%postun qt5 -p /sbin/ldconfig

%post cpp -p /sbin/ldconfig

%postun cpp -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_libdir}/libpoppler.so.43*

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/poppler.pc
%{_libdir}/pkgconfig/poppler-splash.pc
%{_libdir}/libpoppler.so
%dir %{_includedir}/poppler/
# xpdf headers
%{_includedir}/poppler/*.h
%{_includedir}/poppler/fofi/
%{_includedir}/poppler/goo/
%{_includedir}/poppler/splash/
%{_datadir}/gtk-doc/

%files glib
%defattr(-,root,root,-)
%{_libdir}/libpoppler-glib.so.8*
%{_libdir}/girepository-1.0/Poppler-0.18.typelib

%files glib-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/poppler-glib.pc
%{_libdir}/pkgconfig/poppler-cairo.pc
%{_libdir}/libpoppler-glib.so
%{_datadir}/gir-1.0/Poppler-0.18.gir
%{_includedir}/poppler/glib/

%files qt4
%defattr(-,root,root,-)
%{_libdir}/libpoppler-qt4.so.4*

%files qt4-devel
%defattr(-,root,root,-)
%{_libdir}/libpoppler-qt4.so
%{_libdir}/pkgconfig/poppler-qt4.pc
%{_includedir}/poppler/qt4/

%files qt5
%defattr(-,root,root,-)
%{_libdir}/libpoppler-qt5.so.1*

%files qt5-devel
%defattr(-,root,root,-)
%{_libdir}/libpoppler-qt5.so
%{_libdir}/pkgconfig/poppler-qt5.pc
%{_includedir}/poppler/qt5/

%files cpp
%defattr(-,root,root,-)
%{_libdir}/libpoppler-cpp.so.0*

%files cpp-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/poppler-cpp.pc
%{_libdir}/libpoppler-cpp.so
%{_includedir}/poppler/cpp

%files utils
%defattr(-,root,root,-)
%{_bindir}/*
%{_mandir}/man1/*


%changelog
* Fri Nov  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24.3-1m)
- [SECURITY] CVE-2013-4474
- update to 0.24.3
- add qt5 and qt5-devel sub packages

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22.3-1m)
- [SECURITY] CVE-2013-1788 CVE-2013-1789 CVE-2013-1790
- update to 0.22.3

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.22.0-1m)
- update 0.22.0

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20.5-1m)
- update 0.20.5

* Sun Aug  5 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.20.1-3m)
- fix up Conflicts

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.1-2m)
- revise Requires

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.1-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.4-3m)
- rebuild for glib 2.33.2

* Sun Apr  8 2012 NARITA koichi <pulsar@momonga-linux.org>
- (1.18.4-2m)
- rebuild against libtiff-1.0.4

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.4-1m)
- update to 1.18.4
- commentout patch0

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.2-3m)
- add BuildRequires 

* Wed Dec 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18.2-2m)
- remove a line of automake to enable build, automake version mismatch issue

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18.2-1m)
- update to 0.18.2

* Thu Nov 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.0-2m)
- move typelib file to main package

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18.0-1m)
- update to 0.18.0

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17.4-1m)
- update to 0.17.4
-- remove qt packages and pdftoabw

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.7-1m)
- update to 0.16.7

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.5-1m)
- rebuild against openjpeg-1.4

* Mon May  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.5-1m)
- update to 0.16.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.4-2m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.4-1m)
- update to 0.16.4

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.0-1m)
- update 0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.4-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.4-1m)
- [SECURITY] CVE-2010-3702 CVE-2010-3703 CVE-2010-3704
- update to 0.14.4

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.0-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.0-5m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.0-4m)
- split utils subpackage

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.0-3m)
- rebuild against qt-4.6.3-1m

* Thu Jun 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.0-2m)
- [SECURITY] CVE-2009-3938
- import a security patch from Debian unstable (0.12.4-1)

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0
- add --disable-gtk-test to configure (gcc-4.4.4)

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.4-3m)
- revise %%files to avoid conflicting
- please check BuildRequires of all packages depending on poppler
- poppler-glib-devel has %%{_includedir}/poppler/glib
- some packages may need BuildRequires: poppler-glib-devel to build

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.4-2m)
- split package

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.4-1m)
- update to 0.12.4

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.3-3m)
- rebuild against libjpeg-8a

* Wed Mar  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.3-2m)
- add patch0 (from vine linux)
-- http://pc11.2ch.net/test/read.cgi/linux/1188293074/709

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.3-1m)
- update 0.12.3

* Tue Dec 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-2m)
- enable-gdk (for gimp)

* Wed Dec 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.2-1m)
- update 0.12.2
- [SECURITY] CVE-2009-3607
- add Require poppler-data

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-1m)
- [SECURITY] CVE-2009-1188 CVE-2009-3603 CVE-2009-3604 CVE-2009-3606
- [SECURITY] CVE-2009-3607 CVE-2009-3608 CVE-2009-3609
- update to 0.12.1

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.0-2m)
- rebuild against libjpeg-7

* Sat Sep 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0
-- enable-static=no

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.7-1m)
- update to 0.10.7

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6
- [SECURITY] CVE-2009-0799, CVE-2009-0800, CVE-2009-1179, CVE-2009-1180
- CVE-2009-1181, CVE-2009-1182, CVE-2009-1183, CVE-2009-1187, CVE-2009-1188

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5

* Fri Feb 13 2009 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.4-1m)
- [SECURITY] CVE-2009-0755 CVE-2009-0756
- update 0.10.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.3-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.3-1m)
- update 0.10.3

* Mon Dec 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.2-1m)
- update 0.10.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Fri Sep 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Sun Jun 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Sat Jun 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-2m)
- rebuild against qt-4.4.0-1m

* Sun May 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Sun Apr 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-3m)
- must specify QTDIR for qt3 under KDE4 environment

* Mon Apr 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-2m)
- support 64bit arch

* Sun Apr 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.4-2m)
- rebuild against gcc43

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Sun Jan  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Tue Nov 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2, security fixes are included

* Fri Nov  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-2m)
- [SECURITY] CVE-2007-4352 CVE-2007-5392 CVE-2007-5393
- import security patch

* Mon Oct 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Thu Sep  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-2m)
- update to 0.6 again

* Thu Sep  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.4-6m)
- version down to 0.5.4

* Thu Sep  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Tue Jul 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-5m)
- [SECURITY] CVE-2007-3387
- apply security patch (poppler-0.5.4-CVE-2007-3387.patch)

* Sun Apr 22 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.4-4m)
- BuildRequires: qt-devel, qt4-devel

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-3m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Mon Jan 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-2m)
- [SECURITY] MOAB-06-01-2007
- Multiple Vendor PDF Document Catalog Handling Vulnerability

* Tue Oct 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-3m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.5.3-2m)
- rebuild against expat-2.0.0-1m

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sat Feb 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-2m)
- enable xpdf header

* Fri Feb 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- update to Beta version

* Sun Feb 12 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.5-1m)
- [SECURITY] CVE-2006-0301
- update to 0.4.5

* Wed Feb  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4
-- Security update; CVE-2005-3624, CVE-2005-3625, CVE-2005-3627.
-- Fix KDE bug #119569, endless loop in jpeg decoder.
- add patch1 CharCodeToUnicode
-- http://lists.freedesktop.org/archives/poppler/2005-July/000660.html
- delete *.la file (for libtool)

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-3m)
- add gcc-4.1 patch
- Patch0: poppler-0.4.2-gcc41.patch

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-2m)
- delete autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- start
