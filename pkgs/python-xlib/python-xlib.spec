%global momorel 5
%global rcver 1
%global prever 1

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-xlib
Version:        0.15
Release:        %{?prever:0.%{prever}.}%{momorel}m%{?dist}
Summary:        X client library for Python

Group:          Development/Languages
License:        GPLv2+
URL:            http://python-xlib.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/python-xlib/python-xlib-%{version}%{?rcver:rc%{rcver}}.tar.gz
NoSource:       0
Source1:        defs
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
BuildRequires:  texinfo-tex tetex-dvips

%package doc
Summary:        Documentation and examples for python-xlib
Group:          Documentation
Requires:       %{name} = %{version}-%{release}


%description
The Python X Library is a complete X11R6 client-side implementation, 
written in pure Python. It can be used to write low-levelish X Windows 
client applications in Python.

%description doc
Install this package if you want the developers' documentation and examples
that tell you how to program with python-xlib.

%prep
%setup -q -n %{name}-%{version}%{?rcver:rc%{rcver}}


%build
%{__python} setup.py build
cp %{SOURCE1} doc/src/
cd doc
make html ps
cd html
rm Makefile texi2html

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
chmod a-x examples/*.py
 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
# For noarch packages: sitelib
%{python_sitelib}/*

%files doc
%defattr(-,root,root,-)
%doc COPYING examples doc/html doc/ps/python-xlib.ps


%changelog
* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15-0.1.5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-0.1.2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-0.1.1m)
- update to 0.15rc1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13-2m)
- rebuild against python-2.6.1

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- import from Fedora devel

* Tue Apr 10 2007 Jef Spaleta <jspaleta@gmail.com> - 0.13-3
- Created doc subpackage per suggestion in review

* Mon Mar 26 2007 Jef Spaleta <jspaleta@gmail.com> - 0.13-2
- Review Cleanup

* Sat Mar 24 2007 Jef Spaleta <jspaleta@gmail.com> - 0.13-1
- Initial packaging

