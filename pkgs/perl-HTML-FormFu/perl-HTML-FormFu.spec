%global         momorel 1

Name:           perl-HTML-FormFu
Version:        2.01
Release:        %{momorel}m%{?dist}
Summary:        HTML Form Creation, Rendering and Validation Framework
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTML-FormFu/
Source0:        http://www.cpan.org/authors/id/C/CF/CFRANKS/HTML-FormFu-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Captcha-reCAPTCHA >= 0.93
BuildRequires:  perl-CGI
BuildRequires:  perl-Class-Accessor-Chained
BuildRequires:  perl-Clone >= 0.31
BuildRequires:  perl-Config-Any >= 0.18
BuildRequires:  perl-Crypt-CBC
BuildRequires:  perl-Crypt-DES
BuildRequires:  perl-Data-Visitor >= 0.26
BuildRequires:  perl-Date-Calc
BuildRequires:  perl-DateTime >= 0.54
BuildRequires:  perl-DateTime-Format-Builder >= 0.81
BuildRequires:  perl-DateTime-Format-Natural
BuildRequires:  perl-DateTime-Format-Strptime >= 1.2000
BuildRequires:  perl-DateTime-Locale >= 0.45
BuildRequires:  perl-Email-Valid
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-ShareDir
BuildRequires:  perl-Hash-Flatten
BuildRequires:  perl-HTML-Scrubber
BuildRequires:  perl-HTML-TokeParser-Simple >= 3.14
BuildRequires:  perl-HTTP-Message >= 1.64
BuildRequires:  perl-IO-Interactive
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Locale-Maketext
BuildRequires:  perl-Module-Pluggable
BuildRequires:  perl-Moose >= 2.0001
BuildRequires:  perl-MooseX-Aliases
BuildRequires:  perl-MooseX-Attribute-Chained >= 1.0.1
BuildRequires:  perl-MooseX-SetOnce
BuildRequires:  perl-Number-Format
BuildRequires:  perl-Path-Class
BuildRequires:  perl-Readonly
BuildRequires:  perl-Regexp-Common
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Template-Toolkit
BuildRequires:  perl-Test-More >= 0.92
BuildRequires:  perl-Test-NoWarnings
BuildRequires:  perl-YAML-LibYAML >= 0.32
Requires:       perl-Captcha-reCAPTCHA >= 0.93
Requires:       perl-Class-Accessor-Chained
Requires:       perl-Clone >= 0.31
Requires:       perl-Config-Any >= 0.18
Requires:       perl-Crypt-CBC
Requires:       perl-Crypt-DES
Requires:       perl-Data-Visitor >= 0.26
Requires:       perl-Date-Calc
Requires:       perl-DateTime >= 0.54
Requires:       perl-DateTime-Format-Builder >= 0.81
Requires:       perl-DateTime-Format-Natural
Requires:       perl-DateTime-Format-Strptime >= 1.2000
Requires:       perl-DateTime-Locale >= 0.45
Requires:       perl-Email-Valid
Requires:       perl-File-ShareDir
Requires:       perl-Hash-Flatten
Requires:       perl-HTML-Scrubber
Requires:       perl-HTML-TokeParser-Simple >= 3.14
Requires:       perl-HTTP-Message >= 1.64
Requires:       perl-IO-Interactive
Requires:       perl-List-MoreUtils
Requires:       perl-Locale-Maketext
Requires:       perl-Module-Pluggable
Requires:       perl-Moose >= 2.0001
Requires:       perl-MooseX-Aliases
Requires:       perl-MooseX-Attribute-Chained >= 1.0.1
Requires:       perl-MooseX-SetOnce
Requires:       perl-Number-Format
Requires:       perl-Path-Class
Requires:       perl-Readonly
Requires:       perl-Regexp-Common
Requires:       perl-Scalar-Util
Requires:       perl-Task-Weaken
Requires:       perl-Template-Toolkit
Requires:       perl-YAML-LibYAML >= 0.32
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
HTML::FormFu is a HTML form framework which aims to be as easy as possible
to use for basic web forms, but with the power and flexibility to do
anything else you might want to do (as long as it involves forms).

%prep
%setup -q -n HTML-FormFu-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{_bindir}/*
%{perl_vendorlib}/auto/share/dist/HTML-FormFu
%{perl_vendorlib}/HTML/FormFu
%{perl_vendorlib}/HTML/FormFu.pm
%{perl_vendorlib}/MooseX/Attribute/FormFuChained.pm
%{perl_vendorlib}/MooseX/FormFuChainedAccessors.pm
%{perl_vendorlib}/MooseX/Traits/Attribute/FormFuChained.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- rebuild against perl-5.20.0
- update to 2.01

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update to 2.00

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09010-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09010-5m)
- rebuild against perl-5.18.0

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09010-4m)
- rebuild against perl-DateTime-Format-Builder-0.81

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09010-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09010-2m)
- rebuild against perl-5.16.2

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09010-1m)
- update to 0.09010

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09009-1m)
- update to 0.09009

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09007-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09007-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09007-1m)
- update to 0.09007

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09005-2m)
- rebuild against perl-5.14.2

* Wed Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09005-1m)
- update to 0.09005

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-1m)
- update to 0.09004

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-2m)
- rebuild against perl-5.14.1

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-1m)
- update to 0.09003

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09002-4m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09002-3m)
- rebuild against perl-Moose-2.0001

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09002-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09002-1m)
- update to 0.09002

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09001-1m)
- update to 0.09001

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09000-1m)
- update to 0.09000

* Sun Mar 06 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.08002-4m)
- add BR  perl-YAML-LibYAML >= 0.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08002-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08002-2m)
- rebuild against perl-5.12.2

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08002-1m)
- update to 0.08002

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07003-1m)
- update to 0.07003

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.07002-3m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07002-2m)
- force rebuild with perl-Captcha-reCAPTCHA-0.92

* Thu Jun 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07002-1m)
- update to 0.07002

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07001-2m)
- rebuild against perl-5.12.1

* Mon May 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07001-1m)
- update to 0.07001

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-4m)
- rebuild against perl-5.12.0
- disable test for a while...

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-3m)
- rebuild against perl-DateTime-Format-Builder-0.80

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-2m)
- drop test patch

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06001-1m)
- update to 0.06001

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06000-1m)
- update to 0.06000

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05004-1m)
- update to 0.05004

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05002-1m)
- update to 0.05002

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05001-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05001-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05001-1m)
- update to 0.05001

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05000-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
