%global momorel 1

Summary: The GNU macro processor
Name: m4
%{?include_specopt}
%{?!do_test: %global do_test 0}
Version: 1.4.17
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Text
URL: http://www.gnu.org/software/m4/
Source0: ftp://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
# workaround for "FAIL: test-readlink"
# see http://www.mail-archive.com/bug-m4@gnu.org/msg02846.html for details
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info
BuildRequires: libsigsegv-devel >= 2.7

%description
A GNU implementation of the traditional UNIX macro processor.  M4 is
useful for writing text files which can be logically parsed, and is used
by many programs as part of their build process.  M4 has built-in
functions for including files, running shell commands, doing arithmetic,
etc.  The autoconf program needs m4 for generating configure scripts, but
not for running configure scripts.

Install m4 if you need a macro processor.

%prep
%setup -q

%build
%configure
make

%if %{do_test}
%check
make check
%endif

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/m4.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/m4.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS TODO
%{_bindir}/m4
%{_infodir}/m4.info*
%{_mandir}/man1/m4.1.*

%changelog
* Fri Mar 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.17-1m)
- update 1.4.17

* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.16-4m)
- add patch from Fedora to fix build failure

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.16-3m)
- add patch for "FAIL: test-readlink" issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.16-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.16-1m)
- update to 1.4.16

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.14-2m)
- full rebuild for mo7 release

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.14-1m)
- [SECURITY] CVE-2009-4029
- update to 1.4.14
- import from trunk

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-5m)
- use Requires

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-4m)
- rebuild against libsigsegv-2.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-2m)
- fix install-info

* Sun May 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.13-1m)
- update to 1.4.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.12-2m)
- rebuild against rpm-4.6

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.12-1m)
- update to 1.4.12

* Wed Apr  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.11-1m)
- update to 1.4.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.10-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.10-2m)
- %%NoSource -> NoSource

* Sat Jul 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.10-1m)
- update to 1.4.10

* Sat Mar 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.9-1m)
- update to 1.4.9

* Tue Dec 26 2006 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (1.4.8-2m)
- fix %%files (good-by info/dir)

* Sat Dec 23 2006 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (1.4.8-1m)
- version up

* Tue Dec 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.4-1m)
- version up

* Tue Aug 24 2004 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (1.4.2-1m)
- version up

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-21m)
- change URL to http://www.gnu.org/software/m4/

* Mon Oct 15 2001 Masaru Sato <masachan@kondara.org>
- add url tag

* Mon Feb  5 2001 WATABE Toyokazu <toy2@kondara.org>
- (1.4-20k)
- fix format string bugs.
- use macros.

* Thu Oct 26 2000 Takaaki Tabuchi <tab@kondara.org>
- (1.4-17k)
- change man&info compress command gzip to bzip2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Oct 29 1999 Norihito Ohmori <nono@kondara.org>
- add defattr

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 12)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build against glibc 2.1

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Apr 10 1998 Cristian Gafton <gafton@redhat.com>
- Manhattan build

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- added info file handling and BuildRoot

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc

