%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Summary:       Nepomuk  Widgets
Name:          nepomuk-widgets
Version:       %{kdever}
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         User Interface/Desktops
URL:           http://www.kde.org/
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:      0
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:      kdelibs >= %{version}-%{kdelibsrel}
Requires:      nepomuk-core >= %{kdever}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: kdelibs-devel >= %{version}-%{kdelibsrel}
BuildRequires: nepomuk-core-devel%{?_isa} >= %{kdever}

%description
%{summary}.

%package devel
Group:    Development/Libraries
Summary:  Developer files for %{name}
Requires: kdelibs-devel
Requires: %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd
make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc 
%{_kde4_libdir}/libnepomukwidgets.so.4*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/nepomuk2/*.h
%{_kde4_libdir}/cmake/NepomukWidgets
%{_kde4_libdir}/libnepomukwidgets.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Sun Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-2m)
- remove Requires: nepomuk-core-devel
- add Requires: nepomuk-core

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- initial build (KDE 4.10 beta2 (4.9.90))
