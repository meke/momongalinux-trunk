%global         momorel 2

Summary:        Install Debian, Slackware, and Stampede packages with rpm
Name:           alien
Version:        8.90
Release:        %{momorel}m%{?dist}
URL:            http://kitenet.net/programs/alien/
Source0:        http://ftp.debian.org/debian/pool/main/a/%{name}/%{name}_%{version}.tar.gz
License:        GPL
Group:          Applications/File
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       perl
BuildRequires:  perl >= 5.16.0
BuildArch:      noarch

%description
Alien allows you to convert Debian, Slackware, and Stampede Packages into Red
Hat packages, which can be installed with rpm.

It can also convert into Slackware, Debian and Stampede packages.

This is a tool only suitable for binary packages.

%prep
%setup -q -n alien
CFLAGS="%{optflags}" perl Makefile.PL
make
make test

%install
rm -rf %{buildroot}
make pure_install INSTALLDIRS=vendor PERL_INSTALL_ROOT=%{buildroot} \
  PREFIX=%{buildroot}%{_prefix} \
  INSTALLARCHLIB=%{buildroot}/killme \
  VARPREFIX=%{buildroot}
rm -rf %{buildroot}/killme
find %{buildroot} -not -type d -printf "/%%P\n" > manifest
chmod 644 %{buildroot}%{_mandir}/man?/*

rm -f %{buildroot}%{perl_vendorarch}/auto/Alien/.packlist

# remove unwanting file
rm -rf %{buildroot}/var/tmp

%files
%defattr(-,root,root)
%doc debian/changelog README alien.lsm
%{_bindir}/alien
%dir %{perl_vendorlib}/Alien
%dir %{perl_vendorlib}/Alien/Package
%{perl_vendorlib}/Alien/Package/Deb.pm
%{perl_vendorlib}/Alien/Package/Lsb.pm
%{perl_vendorlib}/Alien/Package/Pkg.pm
%{perl_vendorlib}/Alien/Package/Rpm.pm
%{perl_vendorlib}/Alien/Package/Slp.pm
%{perl_vendorlib}/Alien/Package/Tgz.pm
%{perl_vendorlib}/Alien/Package.pm
%{_mandir}/man1/alien.1*
%{_mandir}/man3/Alien::Package.3*
%{_mandir}/man3/Alien::Package::Deb.3*
%{_mandir}/man3/Alien::Package::Rpm.3*
%{_mandir}/man3/Alien::Package::Slp.3*
%{_mandir}/man3/Alien::Package::Tgz.3*
%{_mandir}/man3/Alien::Package::Lsb.3*
%{_mandir}/man3/Alien::Package::Pkg.3*
%dir %{_datadir}/alien

%clean
rm -rf %{buildroot}

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (8.90-2m)
- rebuild against perl-5.20.0

* Sat May  3 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (8.90-1m)
- update to 8.90

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (8.89-2m)
- rebuild against perl-5.18.2

* Sun Oct 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8.89-1m)
- update to 8.89

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8.88-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8.88-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8.88-2m)
- rebuild against perl-5.16.3

* Sat Dec  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.88-1m)
- update to 8.88

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.86-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.86-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.86-2m)
- rebuild against perl-5.16.0

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.86-1m)
- update to 8.86

* Sun Nov  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.85-1m)
- update to 8.85

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.84-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.84-2m)
- rebuild against perl-5.14.1

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.84-1m)
- update to 8.84

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.83-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.83-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.83-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.83-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.83-1m)
- update to 8.83

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.81-2m)
- full rebuild for mo7 release

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.81-1m)
- update to 8.81

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.80-2m)
- rebuild against perl-5.12.1

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.80-1m)
- update to 8.80

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.79-3m)
- use vendorlib

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.79-2m)
- rebuild against perl-5.12.0

* Sun Dec 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.79-1m)
- update to 8.79

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.78-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.78-1m)
- update to 8.78

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (8.76-2m)
- rebuild against perl-5.10.1

* Sun Jun 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.76-1m)
- update to 8.76

* Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.75-1m)
- update to 8.75

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.74-1m)
- update to 8.74

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.73-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.73-1m)
- update to 8.73

* Wed May 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.72-1m)
- update to 8.72

* Tue Apr 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.71-1m)
- update to 8.71

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.69-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.69-2m)
- rebuild against perl-5.10.0-1m

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (8.69-1m)
- update to 8.69

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (8.68-1m)
- update to 8.68

* Thu Apr 20 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (8.64-1m)
- update to 8.64
- No NoSource

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.61-2m)
- rebuild against perl-5.8.8

* Sun Jan  8 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (8.61-1m)
- update to 8.61

* Thu Dec 15 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (8.60-1m)
- update to 8.60

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.56-1m)
- up to 8.56

* Mon Jun 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (8.52-2m)
- rebuild against perl-5.8.7

* Wed May 18 2005 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (8.52-1m)
- up to 8.52

* Thu Mar 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.51-1m)
- up to 8.51

* Thu Nov 04 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.47-1m)
- up to 8.47

* Sat Aug 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (8.46-2m)
- rebuild against perl-5.8.5

* Sat Aug 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (8.46-1m)
  update to 8.46

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (8.44-2m)
- remove Epoch from BuildPrereq

* Mon Mar 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (8.44-1m)
- update to 8.44

* Tue Mar 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.43-1m)
- update to 8.43

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (8.41-2m)
- revised spec for enabling rpm 4.2.

* Sat Dec  6 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (8.41-1m)
- update to 8.41

* Mon Nov 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.40-1m)
- update to 8.40

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.34-3m)
- rebuild against perl-5.8.2

* Sat Nov  8 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.34-2m)
- rebuild against perl-5.8.1

* Sat Aug 2 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (8.34-1m)
- version up

* Tue Jul  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (8.33-1m)

* Tue Jul  8 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (8.31-1m)
- update to 8.31

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.25-1m)
- update to 8.25

* Mon Mar 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.24-1m)
- update to 8.24

* Tue Dec 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.21-2m)
- 'BuildArch: noarch'

* Mon Dec 02 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.21-1m)
- update to 8.21

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.20-2m)
- rebuild against perl-5.8.0

* Tue Aug 27 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (8.20-1m)
- up to 8.20

* Wed Jul 24 2002 smbd <smbd@momonga-linux.org>
- (8.16-1m)
- up to 8.16

* Tue Jul  2 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.12-1m)

* Mon May 20 2002 Kenta MURATA <muraken@kondara.org>
- (8.07-2k)
- up to 8.07

* Wed Apr 17 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (8.05-2k)
- up to 8.05

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.0-8k)
- correct file list

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- rebuild against for perl 5.6.1

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- update to 8.0

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- nigittenu

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Jul  2 2000 Toru Hoshina <t@kondara.org>
- version up.
- rebuild against perl 5.6.0

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P
