%global momorel 4

# Copyright (c) 2000-2008, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:           kxml
Version:        2.2.2
Release:        %{momorel}m%{?dist}
Summary:        Small XML pull parser
License:        BSD
URL:            http://kxml.sourceforge.net/
Group:          Development/Libraries
Source0:        http://dl.sourceforge.net/sourceforge/kxml/kxml2-src-2.2.2.zip
NoSource:       0
Source1:        http://repo1.maven.org/maven2/net/sf/kxml/kxml2/2.2.2/kxml2-2.2.2.pom
BuildRequires:  jpackage-utils >= 0:1.7.4
BuildRequires:  java-devel = 0:1.5.0
BuildRequires:  ant >= 0:1.6.5
BuildRequires:  xpp3 >= 0:1.1.3.1
Requires:  java >= 0:1.5.0
Requires:  xpp3
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot

Requires(post):   jpackage-utils >= 0:1.7.4
Requires(postun): jpackage-utils >= 0:1.7.4

%description
kXML is a small XML pull parser, specially designed for constrained
environments such as Applets, Personal Java or MIDP devices.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
API documentation for %{name}.

%prep
%setup -q -c
for j in $(find . -name "*.jar"); do
    mv $j $j.no
done
ln -sf $(build-classpath xpp3) lib/xmlpull_1_1_3_1.jar

%build
ant

%install
rm -rf $RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms

%add_to_maven_depmap net.sf.kxml %{name}2 %{version} JPP %{name}
install -m 644 %{SOURCE1} \
        $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.%{name}.pom

# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 dist/%{name}2-%{version}.jar \
        $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
install -m 644 dist/%{name}2-min-%{version}.jar \
        $RPM_BUILD_ROOT%{_javadir}/%{name}-min-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do \
ln -sf ${jar} ${jar/-%{version}/}; done)

# javadoc
install -p -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr www/kxml2/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
(cd $RPM_BUILD_ROOT%{_javadocdir} && ln -sf %{name}-%{version} %{name})

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc license.txt
%{_javadir}/*.jar
%{_datadir}/maven2/poms/JPP.%{name}.pom
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%dir %{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}-%{version}/*
%dir %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-2m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.2-1m)
- import from Fedora 13

* Thu Sep 3 2009 Alexander Kurtakov <akurtako@redhat.com> 2.2.2-5
- Fix Summary and description.
- Fix line length.
- Use pom from the URL.

* Thu Sep 3 2009 Alexander Kurtakov <akurtako@redhat.com> 2.2.2-4
- Adapt for Fedora.

* Mon Dec 08 2008 Will Tatam <will.tatam@red61.com> 2.2.2-3
- Auto rebuild for JPackage 5 in mock

* Wed May 07 2008 Ralph Apel <r.apel@r-apel.de> 0:2.2.2-2jpp
- Add xpp3 (B)R

* Wed May 07 2008 Ralph Apel <r.apel@r-apel.de> 0:2.2.2-1jpp
- 2.2.2

* Thu Aug 26 2004 Fernando Nasser <fnasser@redhat.com> 0:2.1.8-4jpp
- Pro-forma rebuild with Ant 1.6.2

* Mon Jan 26 2004 David Walluck <david@anti-microsoft.org> 0:2.1.8-3jpp
- remove fractal reference

* Sun Jan 25 2004 David Walluck <david@anti-microsoft.org> 0:2.1.8-2jpp
- fix license

* Sun Jan 25 2004 David Walluck <david@anti-microsoft.org> 0:2.1.8-1jpp
- release
