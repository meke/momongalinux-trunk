%global momorel 1

Summary: OpenSource implementation of Service Location Protocol V2
Name: openslp

Version: 2.0.0
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://sourceforge.net/projects/openslp/

Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: slpd.init
# Source1,2: simple man pages (slightly modified help2man output)
Source2: slpd.8.bz2
Source3: slptool.1.bz2
# Source3: service file
Source4: slpd.service

# Patch1: creates script from upstream init script that sets multicast
#     prior to the start of the service
Patch1:  openslp-2.0.0-multicast-set.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: flex
BuildRequires: openssl-devel >= 1.0.0

%description
Service Location Protocol is an IETF standards track protocol that
provides a framework to allow networking applications to discover the
existence, location, and configuration of networked services in
enterprise networks.

OpenSLP is an open source implementation of the SLPv2 protocol as defined 
by RFC 2608 and RFC 2614.  This package include the daemon, libraries, header
files and documentation

%package devel
Summary: Development tools for programs which will use the openslp library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The openslp-devel package includes the header files and static libraries
necessary for developing programs using the openslp library.

If you are going to develop programs, you should install openslp-devel.
You'll also need to have the openslp package installed.

%package server
Summary: OpenSLP server daemon
Group:   System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Requires(preun): chkconfig, initscripts
Requires(post): chkconfig
Requires(postun): initscripts

%description server
OpenSLP server daemon to dynamically register services.

#'

%prep
%setup -q

%patch1 -p1 -b .multicast-set

autoreconf -ivf 

%build
# for x86_64
export CFLAGS="-fPIC -fno-strict-aliasing -fPIE -DPIE $RPM_OPT_FLAGS"
# for slpd
export LDFLAGS="-pie -Wl,-z,now"

%configure \
  --prefix=%{_prefix} \
  --libdir=%{_libdir} \
  --sysconfdir=%{_sysconfdir} \
  --localstatedir=/var \
  --disable-dependency-tracking \
  --disable-static \
  --enable-slpv2-security \
  --disable-rpath \
  --enable-async-api

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

install -p -D -m755  %{SOURCE1} $RPM_BUILD_ROOT%{_initscriptdir}/slpd

mkdir -p ${RPM_BUILD_ROOT}/%{_sysconfdir}/slp.reg.d

# install script that sets multicast
mkdir -p ${RPM_BUILD_ROOT}/usr/lib/%{name}-server
install -m 0755 etc/slpd.all_init ${RPM_BUILD_ROOT}/usr/lib/%{name}-server/slp-multicast-set.sh

# install service file
mkdir -p ${RPM_BUILD_ROOT}/%{_unitdir}
install -p -m 644 %{SOURCE4} ${RPM_BUILD_ROOT}/%{_unitdir}/slpd.service

# install man page
mkdir -p ${RPM_BUILD_ROOT}/%{_mandir}/man8/
mkdir -p ${RPM_BUILD_ROOT}/%{_mandir}/man1/
cp %SOURCE2 ${RPM_BUILD_ROOT}/%{_mandir}/man8/
cp %SOURCE3 ${RPM_BUILD_ROOT}/%{_mandir}/man1/

# nuke unpackaged/unwanted files
rm -rf $RPM_BUILD_ROOT/usr/doc
rm -f  $RPM_BUILD_ROOT%{_libdir}/lib*.la

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post server
%systemd_post slpd.service

%preun server
%systemd_preun slpd.service

%postun server
%systemd_postun_with_restart slpd.service

%files
%defattr(-,root,root)
%doc AUTHORS COPYING FAQ NEWS README THANKS
%config(noreplace) %{_sysconfdir}/slp.conf
%{_bindir}/slptool
%{_libdir}/libslp.so.1*
%{_mandir}/man1/*

%files server
%defattr(-,root,root)
%doc doc/doc/html/IntroductionToSLP
%doc doc/doc/html/UsersGuide
%doc doc/doc/html/faq*
%{_sbindir}/slpd
%config(noreplace) %{_sysconfdir}/slp.reg
%config(noreplace) %{_sysconfdir}/slp.spi
%config(noreplace) %{_initscriptdir}/slpd
%{_unitdir}/slpd.service
%{_mandir}/man8/*
/usr/lib/%{name}-server/slp-multicast-set.sh

%files devel
%defattr(-,root,root)
%doc doc/doc/html/ProgrammersGuide
%doc doc/doc/rfc
%{_includedir}/slp.h
%{_libdir}/libslp.so

%changelog
* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0

* Fri Dec  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-15m)
- split server package

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-14m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-13m)
- [SECURITY] CVE-2010-3609
- import security patch from openSuSE

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-11m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-10m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-9m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-6m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-3m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-2m)
- delete libtool library

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.1-1m)
- update to 1.2.1

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.3-5m)
- enable x86_64.

* Wed Apr 14 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.3-4m)
- revise docdir permission

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.3-3m)
- pretty spec file.

* Fri Aug  1 2003 KOMATSU Shinichiro <koma2@momonga-linux.org> 
- (1.1.3-3m)
- this package is openslp, not cups ;D
  (i.e  write correct %%description and Summary,
   by obtaining them from mdk)
- add BuildRequires: flex
- use %%make

* Sat May 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.3-2m)
- %%config(noreplace)

* Fri May 23 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.3-1m)
- write spec for momonga
