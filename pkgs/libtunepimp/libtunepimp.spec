%global momorel 24

%global pythonver 2.7
%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A library for creating MusicBrainz enabled tagging applications 
Name: libtunepimp 
Version: 0.5.3
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://musicbrainz.org/doc/libtunepimp
Group: System Environment/Libraries
Source0: ftp://ftp.musicbrainz.org/pub/musicbrainz/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: libtunepimp-0.5.3-gcc43.patch
Patch2: libtunepimp-0.5.3-glibc210.patch
Patch3: libtunepimp-0.5.3-mp4v2.patch
Patch4: libtunepimp-0.5.3-curl.patch
Requires: libmusicbrainz >= 2.1.2
BuildRequires: curl-devel
BuildRequires: expat-devel
BuildRequires: faac-devel
BuildRequires: flac-devel >= 1.1.4
BuildRequires: libmad
BuildRequires: libmp4v2-devel >= 1.9.1
BuildRequires: libmpcdec-devel >= 1.2.6
BuildRequires: libmusicbrainz-devel >= 2.1.2
BuildRequires: libofa-devel
BuildRequires: libvorbis-devel
BuildRequires: readline-devel >= 5.0
BuildRequires: taglib-devel
BuildRequires: python-devel >= %{pythonver}
BuildRequires: speex-devel
BuildRequires: zlib-devel

%description
The TunePimp library is a development library geared towards developers 
who wish to create MusicBrainz enabled tagging applications.

%package devel
Summary: Headers for developing programs that will use libtunepimp 
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Headers for developing programs that will use libtunepimp.

%package -n python-tunepimp
Summary: Python bindings for developing programs that will use libtunepimp
Group: Development/Libraries
License: GPL
%define python_ver %(%{__python} -c "import sys ; print sys.version[:3]")
%if "%{?python_ver}" > "2"
Requires: python-abi = %{python_ver}
%endif

%description -n python-tunepimp
Python bindings for developing programs that will use libtunepimp.

%prep
%setup -q
%patch1 -p1 -b .gcc43~
%patch2 -p1 -b .glibc210
%patch3 -p1 -b .mp4v2
%patch4 -p1 -b .curl~

%build
%configure
%make

pushd python
%{__python} setup.py build 
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

pushd python
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
popd

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README* TODO 
%{_bindir}/puid
%{_libdir}/tunepimp
%{_libdir}/libtunepimp.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/tunepimp-0.5
%{_libdir}/libtunepimp.a
%{_libdir}/libtunepimp.so

%files -n python-tunepimp
%defattr(-,root,root,-)
%doc python/examples/trm.py
%{python_sitelib}/tunepimp
%{python_sitelib}/tunepimp-*.egg-info

%changelog
* Sat Jul  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-24m)
- fix build failure; add patch for curl

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-23m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-22m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-20m)
- full rebuild for mo7 release

* Mon Dec 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-19m)
- enable to build with libmp4v2-1.9.1

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-18m)
- rebuild against libmp4v2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-17m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-15m)
- apply glibc210 patch

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-14m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-13m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.3-12m)
- rebuild against python-2.6.1-1m

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-11m)
- rebuild against libmpcdec-1.2.6
- sort BR

* Sat May 24 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-10m)
- add BuildRequires: libmp4v2-devel
- update gcc43 patch

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-9m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-8m)
- add egg-info for new python

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-7m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-6m)
- %%NoSource -> NoSource

* Sun Jan 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-5m)
- add patch for gcc43, generated by gen43patch(v1)

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-4m)
- rebuild against libvorbis-1.2.0-1m

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-3m)
- make python-tunepimp package

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-2m)
- rebuild against flac-1.1.4

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-1m)
- version 0.5.3
- remove gcc4.patch and lookuptools-buffer-overflow.diff

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-4m)
- delete libtool library

* Thu Aug  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-3m)
- [SECURITY] CVE-2006-3600
  import libtunepimp-lookuptools-buffer-overflow.diff from opensuse

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.1-2m)
- rebuild against readline-5.0

* Fri Jun 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-1m)
- version down to 0.4.1 for stable release
- remove fix-detect-taglib.patch

* Sun May 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-1m)
- version 0.4.2
- add fix-detect-taglib.patch
- BuildRequires: taglib-devel
- remove gcc41.patch

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.0-3m)
- rebuild against libmusicbrainz-2.1.2

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-2m)
- add gc-4.1 patch
-- Patch1: libtunepimp-0.3.0-gcc41.patch

* Tue Nov 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version down for amarok-1.3.6

* Tue Nov 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- initial package for Momonga Linux
- import libtunepimp-0.3.0-gcc4.patch from Fedora Extras devel
