%global momorel 30

Summary: An X Window System based image viewer.
Name: xloadimage
Version: 4.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Amusements/Graphics
Source: ftp://ftp.x.org/R5contrib/xloadimage.4.1.tar.gz
Patch0: xloadimage.4.1-linux.patch
Patch1: xloadimage.4.1-nobr.patch
Patch2: xloadimage-1.4-errno.patch
Patch3: xloadimage-1.4-varargs.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0
BuildRequires: libtiff-devel >= 4.0.1

%description
The xloadimage utility displays images in an X Window System window,
loads images into the root window, or writes images into a file.
Xloadimage supports many images types (GIF, TIFF, JPEG, XPM, XBM,
etc.).

Install the xloadimage package if you need a utility for displaying
images or loading images into the root window.

%prep
%setup -q -n xloadimage.4.1
%patch0 -p1
%patch1 -p1 -b .nobr~
%patch2 -p1 -b .errno~
%patch3 -p1 -b .varargs~

cd jpeg
mv Makefile Makefile.orig
ln -s makefile.ansi Makefile

%build
xmkmf
cd jpeg
make libjpeg.a
cd ..
make 

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_datadir}/X11/app-defaults

make	DESTDIR=%{buildroot} \
	SYSPATHFILE=%{buildroot}/%{_datadir}/X11/app-defaults/Xloadimage \
	install install.man

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README
%{_datadir}/X11/app-defaults/Xloadimage
%{_bindir}/xloadimage
%{_bindir}/xview
%{_bindir}/xsetbg
%{_bindir}/uufilter
%{_mandir}/man1/uufilter.*
%{_mandir}/man1/xloadimage.*

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-30m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-27m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-25m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-24m)
- rebuild against gcc43

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2-23m)
- revise for xorg-7.0
- change install dir

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (4.2-22m)
- revised spec for rpm 4.2.

* Fri Oct 10 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (4.2-21m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Jul 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.1-20m)
- use stdarg.h instrad of varargs.h

* Wed May 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.1-19m)
- add xloadimage-1.4-errno.patch

* Fri Nov 10 2000 Kenichi Matsubara <m@kondara.org>
- bugfix Source:.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (4.1-13).

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 12)

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- build for 6.0

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Aug 25 1997 Erik Troan <ewt@redhat.com>
- built against glibc
