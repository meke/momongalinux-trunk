%global momorel 1

%define gs_ver 9.10
%define gs_dot_ver 9.10
%{expand: %%define build_with_freetype %{?_with_freetype:1}%{!?_with_freetype:0}}

Summary: A PostScript interpreter and renderer
Name: ghostscript
Version: %{gs_ver}
Release: %{momorel}m%{?dist}
# CMap is redistributable, no modification permitted
License: GPLv3+
URL: http://www.ghostscript.com/
Group: Applications/Publishing
Source0: http://downloads.ghostscript.com/public/ghostscript-%{version}.tar.bz2
NoSource: 0
Source2: CIDFnmap
Source4: cidfmap
Patch1: ghostscript-multilib.patch
Patch2: ghostscript-scripts.patch
Patch3: ghostscript-noopt.patch
Patch4: ghostscript-runlibfileifexists.patch
Patch5: ghostscript-icc-missing-check.patch
Patch6: ghostscript-Fontmap.local.patch
Patch7: ghostscript-iccprofiles-initdir.patch
Patch8: ghostscript-gdevcups-debug-uninit.patch
Patch9: ghostscript-wrf-snprintf.patch

# neverembed patch
# http://www.aihara.co.jp/~taiji/gyve/tmp/modify_ps2pdfwr.patch
# modified for 8.71
Patch200: ghostscript-9.05-ps2pdfwr_neverembed.patch
Requires: ipa-gothic-fonts, ipa-mincho-fonts
Requires: urw-fonts >= 1:2.4-1m, ghostscript-fonts
BuildRequires: xz
BuildRequires: libjpeg-devel >= 8a, libXt-devel
BuildRequires: zlib-devel, libpng-devel, unzip, gtk2-devel
BuildRequires: glib2-devel, gnutls-devel
# Omni requires libxml
BuildRequires: libxml2-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: cups-devel >= 1.5.2-3m
BuildRequires: libtool
BuildRequires: jasper-devel
%{?_with_freetype:BuildRequires: freetype-devel}
Obsoletes: %{name}-resource < 8.64
#Provides: %{name}-resource = %{version}-%{release}
Obsoletes: ijs
Provides: ijs
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Ghostscript is a set of software that provides a PostScript
interpreter, a set of C procedures (the Ghostscript library, which
implements the graphics capabilities in the PostScript language) and
an interpreter for Portable Document Format (PDF) files. Ghostscript
translates PostScript code into many common, bitmapped formats, like
those understood by your printer or screen. Ghostscript is normally
used to display PostScript files and to print PostScript files to
non-PostScript printers.

If you need to display PostScript files or print them to
non-PostScript printers, you should install ghostscript. If you
install ghostscript, you also need to install the ghostscript-fonts
package.

%package devel
Summary: Files for developing applications that use ghostscript
Requires: %{name} = %{version}-%{release}
Group: Development/Libraries
Obsoletes: ijs-devel
Provides: ijs-devel

%description devel
The header files for developing applications that use ghostscript.

%package doc
Summary: Documentation for ghostscript
Requires: %{name} = %{version}-%{release}
Group: Documentation
BuildArch: noarch

%description doc
The documentation files that come with ghostscript.

%package gtk
Summary: A GTK-enabled PostScript interpreter and renderer
Requires: %{name} = %{version}-%{release}
Group: Applications/Publishing

%description gtk
A GTK-enabled version of Ghostscript, called 'gsx'.

%prep
%setup -q -n %{name}-%{gs_ver}
rm -rf libpng zlib jpeg jasper

# Fix ijs-config not to have multilib conflicts (bug #192672)
%patch1 -p1 -b .multilib

# Fix some shell scripts
%patch2 -p1 -b .scripts

# Build igcref.c with -O0 to work around bug #150771.
%patch3 -p1 -b .noopt

# Define .runlibfileifexists.
%patch4 -p1

# Fixed missing error check when setting ICC profile.
%patch5 -p1 -b .icc-missing-check

# Restored Fontmap.local patch, incorrectly dropped after
# ghostscript-8.15.4-3 (bug #610301).
# Note: don't use -b here to avoid the backup file ending up in the
# package manifest.
%patch6 -p1

# Don't assume %%rom%% device is available for initial ICC profile dir.
%patch7 -p1 -b .iccprofiles-initdir

# gdevcups: don't use uninitialized variables in debugging output.
%patch8 -p1 -b .gdevcups-debug-uninit

# Use more caution when converting floats to strings (bug #980085).
%patch9 -p1 -b .wrf-snprintf

# neverembed Kanji
%patch200 -p1 -b .neverembed

# Convert manual pages to UTF-8
from8859_1() {
        iconv -f iso-8859-1 -t utf-8 < "$1" > "${1}_"
        mv "${1}_" "$1"
}
for i in man/de/*.1; do from8859_1 "$i"; done

%build
# Compile without strict aliasing opts due to these files:
# gdevescv.c gdevl4v.c gdevopvp.c gdevbbox.c gdevdbit.c gdevddrw.c 
# gdevp14.c gdevpdfd.c gdevpdfi.c gdevpdfo.c gdevpdft.c gdevpdfv.c 
# gdevpdte.c gdevpdtt.c gdevps.c gdevpx.c gscoord.c gscparam.c gscrd.c 
# gsdps1.c gsimage.c gspath1.c gsptype1.c gsptype2.c gstype2.c 
# gstype42.c gxccache.c gxchar.c gxclimag.c gxclpath.c gxfcopy.c 
# gximag3x.c gximage3.c gxipixel.c gxshade1.c gxstroke.c gxtype1.c 
# ibnum.c iscanbin.c zchar1.c zchar.c zcharx.c zfapi.c zfont32.c 
# zfunc0.c zfunc3.c zfunc4.c zpcolor.c zshade.c
EXTRACFLAGS="-fno-strict-aliasing"

FONTPATH=
for path in \
        %{_datadir}/fonts/default/%{name} \
        %{_datadir}/fonts/default/Type1 \
        %{_datadir}/fonts/default/amspsfnt/pfb \
        %{_datadir}/fonts/default/cmpsfont/pfb \
        %{_datadir}/fonts \
        %{_datadir}/%{name}/conf.d \
        %{_sysconfdir}/%{name} \
        %{_sysconfdir}/%{name}/%{gs_dot_ver} \
        %{_datadir}/poppler/cMap/*
do
  FONTPATH="$FONTPATH${FONTPATH:+:}$path"
done
%configure --with-ijs --enable-dynamic --with-fontpath="$FONTPATH" \
        --with-drivers=ALL --disable-compile-inits --with-system-libtiff \
        --with-install-cups \
        CFLAGS="$CFLAGS $EXTRACFLAGS"

# Build IJS
cd ijs
autoreconf -vfi
#./autogen.sh
%configure --enable-shared --disable-static
make
cd ..

%if %{build_with_freetype}
FT_CFLAGS=$(pkg-config --cflags freetype2)
make so RPM_OPT_FLAGS="$RPM_OPT_FLAGS $EXTRAFLAGS" prefix=%{_prefix} \
        FT_BRIDGE=1 FT_CFLAGS="$FT_CFLAGS" FT_LIB=freetype
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS $EXTRAFLAGS" prefix=%{_prefix} \
        FT_BRIDGE=1 FT_CFLAGS="$FT_CFLAGS" FT_LIB=freetype
%else
make so RPM_OPT_FLAGS="$RPM_OPT_FLAGS $EXTRAFLAGS" prefix=%{_prefix}
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS $EXTRAFLAGS" prefix=%{_prefix}
%endif
make cups

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
mkdir -p %{buildroot}/{%{_mandir},%{_bindir},%{_datadir},%{_docdir}}
mkdir -p %{buildroot}/{%{_libdir},%{_includedir}/ijs}

make install soinstall \
%{?_with_freetype:FT_BRIDGE=1} \
        prefix=%{buildroot}%{_prefix} \
        mandir=%{buildroot}%{_mandir} \
        datadir=%{buildroot}%{_datadir} \
        gsincludedir=%{buildroot}%{_includedir}/ghostscript/ \
        bindir=%{buildroot}%{_bindir} \
        libdir=%{buildroot}%{_libdir} \
        docdir=%{buildroot}%{_docdir}/%{name}-doc-%{gs_dot_ver} \
        gsdir=%{buildroot}%{_datadir}/%{name} \
        gsdatadir=%{buildroot}%{_datadir}/%{name}/%{gs_dot_ver} \
        gssharedir=%{buildroot}%{_libdir}/%{name}/%{gs_dot_ver} \
        CUPSSERVERROOT=%{buildroot}`cups-config --serverroot` \
        CUPSSERVERBIN=%{buildroot}`cups-config --serverbin` \
        CUPSDATA=%{buildroot}`cups-config --datadir`

mv -f %{buildroot}%{_bindir}/gsc %{buildroot}%{_bindir}/gs

cd ijs
%makeinstall
cd ..

echo ".so man1/gs.1" > %{buildroot}/%{_mandir}/man1/ghostscript.1
ln -sf gs %{buildroot}%{_bindir}/ghostscript

# Rename an original cidfmap to cidfmap.GS
mv %{buildroot}%{_datadir}/%{name}/%{gs_dot_ver}/Resource/Init/cidfmap{,.GS}
# Install our own cidfmap to allow the separated
# cidfmap which the font packages own.
install -m0644 %{SOURCE2} %{buildroot}%{_datadir}/%{name}/%{gs_dot_ver}/Resource/Init/CIDFnmap
install -m0644 %{SOURCE4} %{buildroot}%{_datadir}/%{name}/%{gs_dot_ver}/Resource/Init/cidfmap

# Documentation
install -m0644 doc/COPYING %{buildroot}%{_docdir}/%{name}-%{gs_dot_ver}

# Don't ship pkgconfig or libtool la files.
rm -f %{buildroot}%{_libdir}/libijs.la

# Don't ship ijs example client or server
rm -f %{buildroot}%{_bindir}/ijs_{client,server}_example

# Don't ship URW fonts; we already have them.
rm -rf %{buildroot}%{_datadir}/ghostscript/%{gs_dot_ver}/Resource/Font

mkdir -p %{buildroot}%{_sysconfdir}/ghostscript/%{gs_dot_ver}
touch %{buildroot}%{_sysconfdir}/ghostscript/%{gs_dot_ver}/Fontmap.local
touch %{buildroot}%{_sysconfdir}/ghostscript/%{gs_dot_ver}/cidfmap.local
touch %{buildroot}%{_sysconfdir}/ghostscript/%{gs_dot_ver}/CIDFnmap.local

# The man/de/man1 symlinks are broken (bug #66238).
find %{buildroot}%{_mandir}/de/man1 -type l | xargs rm -f

# Don't ship fixmswrd.pl as it pulls in perl (bug #463948).
rm -f %{buildroot}%{_bindir}/fixmswrd.pl

# Don't ship CMaps (instead poppler-data paths are in search path).
rm -f %{buildroot}%{_datadir}/ghostscript/%{gs_dot_ver}/Resource/CMap/*

MAIN_PWD=`pwd`
(cd %{buildroot}; find .%{_datadir}/ghostscript/%{gs_dot_ver}/Resource -type f | \
                sed -e 's/\.//;' | grep -v Fontmap | grep -v gs_init.ps > $MAIN_PWD/rpm.sharelist
 find .%{_bindir}/ | sed -e 's/\.//;' | \
                grep -v '/$\|/hpijs$\|/gsx$\|/ijs-config$' \
                >> $MAIN_PWD/rpm.sharelist)

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f rpm.sharelist
%defattr(-,root,root)
%doc doc/COPYING
%dir %{_sysconfdir}/ghostscript
%dir %{_sysconfdir}/ghostscript/%{gs_dot_ver}
%dir %{_datadir}/ghostscript/%{gs_dot_ver}
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/CMap
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/ColorSpace
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/Decoding
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/Encoding
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/Init
%dir %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/SubstCID
%config %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/Init/gs_init.ps
%config %{_datadir}/ghostscript/%{gs_dot_ver}/Resource/Init/Fontmap*
%{_datadir}/ghostscript/%{gs_dot_ver}/examples
%{_datadir}/ghostscript/%{gs_dot_ver}/iccprofiles
%{_datadir}/ghostscript/%{gs_dot_ver}/lib
%{_mandir}/man*/*
%lang(de) %{_mandir}/de/man*/*
%{_libdir}/libgs.so.*
%{_libdir}/libijs-*.so*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/%{gs_dot_ver}
%config(noreplace) %{_sysconfdir}/ghostscript/%{gs_dot_ver}/*

%files doc
%defattr(-,root,root)
%{_docdir}/%{name}-doc-%{gs_dot_ver}

%files gtk
%defattr(-,root,root)
%{_bindir}/gsx

%files devel
%defattr(-,root,root)
%dir %{_includedir}/ghostscript
%{_includedir}/ghostscript/*.h
%dir %{_includedir}/ijs
%{_includedir}/ijs/*
%{_bindir}/ijs-config
%{_libdir}/pkgconfig/ijs.pc
%{_libdir}/libijs.so
%{_libdir}/libgs.so

%changelog
* Mon Sep 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.10-1m)
- update to 9.10

* Mon Aug 26 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.09-1m)
- update to 9.09

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.05-2m)
- rebuild for glib 2.33.2

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.05-1m)
- update to 9.05
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.71-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.71-9m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (8.71-8m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.71-7m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.71-6m)
- [SECURITY] CVE-2010-1628
- import a security patch from Fedora 13 (8.71-10)

* Fri Jun 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.71-5m)
- remove Obsoletes: %%{name}-cups

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.71-4m)
- build with cups-1.4

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.71-3m)
- roll back cidfmap

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.71-2m)
- remove dups
- install cjk examples
- revive doc

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.71-1m)
- [SECURITY] Multiple null pointer dereferences in JBIG2 decoder
- [SECURITY] CVE-2009-4270 CVE-2010-1628 CVE-2010-1869 CVE-2009-4897
- update to 8.71
-- import patches from Fedora 13 (8.71-9)
- include ijs

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.64-8m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.64-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.64-6m)
- Requires: urw-fonts >= 1:2.4-1m

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.64-5m)
- rebuild against libjpeg-7

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.64-4m)
- merge the following T4R changes
-- 
-- * Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (8.64-3m)
-- - grip %%{_datadir}/ghostscript/%%{gs_dot_ver}/Resource/{CMap,ColorSpace,Decoding,Encoding,Init,SubstCID}
-- 
-- * Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (8.64-2m)
-- - drop cjkv patches, merged upstream
-- - unprovide ghostscript-resource
-- - import Patch12 from Fedora 11 (8.64-8)
-- 
-- * Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (8.64-1m)
-- - [SECURITY] CVE-2009-0792 CVE-2009-0196
-- - update to 8.64
-- -- XXX cjkv patch does not work
-- -- Debian (and Ubuntu) excludes cjkv patch
-- - partially sync with Fedora 11 (8.64-6)
-- - update Patch2,4,7-11 from Fedora 11 (8.64-6)
-- - update Patch200
-- - update cjkv patches (Patch10000,10001)
-- - drop Patch10002, merged upstream
-- - remove resource subpackage
-- - set EXTRACFLAGS="-fno-strict-aliasing"
-- - run make cups
-- - License: GPLv2

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.63-4m)
- release %%{_datadir}/ghostscript/{,conf.d} for filesystem-2.4.21

* Wed May 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.63-3m)
- cleanup FONTPATH

* Mon May 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.63-2m)
- Requires ipa-{gothic,mincho}-fonts instead of IPA fonts which were
  provided by opfc-ModuleHP
- TODO: FONTPATH

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.63-1m)
- update to 8.63
-- 8.64 does not work with cjk patches
- TODO: FONTPATH and IPA fonts

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.62-6m)
- [SECURITY] CVE-2007-6725
- import Patch100 from Fedora 9 (8.62-4)

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.62-6m)
- [SECURITY] CVE-2009-0792 CVE-2009-0196 CVE-2008-6679
- import Patch7,9-11 from Fedora 9 (8.63-3)
- set EXTRACFLAGS="-fno-strict-aliasing"
- License: GPLv2

* Sat Mar 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.62-5m)
- [SECURITY] CVE-2009-0583 CVE-2009-0584
- import a security patch from Debian stable (8.62.dfsg.1-3.2lenny1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.62-4m)
- rebuild against rpm-4.6

* Tue Sep 02 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (8.62-3m)
- modify cidmap.ja cidmap.ja.ipa FAPIcidfmap.ja

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.62-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.62-1m)
- update to 8.62
- partially sync with Fedora (8.62-2)
- -* Tue Mar  4 2008 Tim Waugh <twaugh@redhat.com> 8.62-2
- -- No longer need CVE-2008-0411 patch.
- -- Don't ship URW fonts; we already have them.
- -* Tue Mar  4 2008 Tim Waugh <twaugh@redhat.com> 8.62-1
- -- 8.62.  No longer need IJS KRGB patch, or patch for gs bug 689577.
- -* Wed Feb 27 2008 Tim Waugh <twaugh@redhat.com> 8.61-10
- -- Applied patch to fix CVE-2008-0411 (bug #431536). 
- -* Fri Feb 22 2008 Tim Waugh <twaugh@redhat.com> 8.61-9
- -- Build with jasper again (bug #433897).  Build requires jasper-devel, and a patch to remove jas_set_error_cb reference.
- -* Wed Feb 13 2008 Tim Waugh <twaugh@redhat.com> 8.61-8
- -- Rebuild for GCC 4.3.

* Mon Mar  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.61-4m)
- remove Provides: ghostscript-cups

* Tue Feb 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.61-3m)
- modify %%files to enable build on x86_64

* Tue Feb 26 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.61-2m)
- hardcoded ResourcDir in lib/gs_res.ps
- move /usr/share/ghostscript/conf.d/cidfmap.ja.ipa to opfc-ModuleHP

* Wed Feb 13 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.61-1m)
- CJKV-ready ghostscript-8.61 (applied Mr. Koji Otani's gs-cjk patch from http://www.ghostscript.com/pipermail/gs-devel/2007-August/003642.html)
- not tested sufficiently yet

* Tue Feb 12 2008 nosanosa <nosanosa@momonga-linux.org>
- (8.61-0m)
- ghostscript 8.61 test based on the package of Fedora Development (8.61-7)
- change log in Fedora is as follows: 
-
-* Mon Jan 28 2008 Tim Waugh <twaugh@redhat.com> 8.61-7
-- Don't build with jasper support.
-- Remove bundled libraries.
-
-* Tue Dec 11 2007 Tim Waugh <twaugh@redhat.com> 8.61-6
-- Applied upstream patch for bug #416321.
-
-* Fri Nov 30 2007 Tim Waugh <twaugh@redhat.com> 8.61-5
-- Fixed runlibfileifexists patch.
-
-* Fri Nov 30 2007 Tim Waugh <twaugh@redhat.com> 8.61-4
-- Revert previous change, but define .runlibfileifexists, not just
-  runlibfileifexists.
-
-* Wed Nov 28 2007 Tim Waugh <twaugh@redhat.com> 8.61-3
-- No longer need runlibfileifexists.
-- Use runlibfile in cidfmap.
-
-* Wed Nov 28 2007 Tim Waugh <twaugh@redhat.com> 8.61-2
-- Add /usr/share/fonts to fontpath (bug #402551).
-- Restore cidfmap-switching bits, except for FAPIcidfmap which is no
-  longer used.
-- Add runlibfileifexists to gs_init.ps.
-- Build with --disable-compile-inits (bug #402501).
-
-* Fri Nov 23 2007 Tim Waugh <twaugh@redhat.com> 8.61-1
-- 8.61.
-
-* Tue Oct 23 2007 Tim Waugh <twaugh@redhat.com> 8.60-5
-- Applied patch from upstream to fix CVE-2007-2721 (bug #346511).
-
-* Tue Oct  9 2007 Tim Waugh <twaugh@redhat.com> 8.60-4
-- Marked localized man pages as %%lang (bug #322321).
-
-* Thu Sep 27 2007 Tim Waugh <twaugh@redhat.com> 8.60-3
-- Back-ported mkstemp64 patch (bug #308211).
-
-* Thu Aug 23 2007 Tim Waugh <twaugh@redhat.com> 8.60-2
-- More specific license tag.
-
-* Fri Aug  3 2007 Tim Waugh <twaugh@redhat.com> 8.60-1
-- 8.60.
-
-* Mon Jul 16 2007 Tim Waugh <twaugh@redhat.com> 8.60-0.r8112.2
-- Own %%{_libdir}/ghostscript (bug #246026).
-
-* Tue Jul 10 2007 Tim Waugh <twaugh@redhat.com> 8.60-0.r8112.1
-- 8.60 snapshot from svn.  Patches dropped:
-  - big-cmap-post
-  - split-cidfnmap
-  - exactly-enable-cidfnmap
-  - Fontmap.local
-  No longer needed:
-  - gxcht-64bit-crash
-
-* Tue Apr 17 2007 Tim Waugh <twaugh@redhat.com> 8.15.4-3
-- Apply fonts in CIDFnmap even if the same fontnames are already registered
-  (bug #163231).
-- New file CIDFmap (bug #233966).
-- Allow local overrides for FAPIcidfmap, cidfmap and Fontmap (bug #233966).
-
-* Tue Apr  3 2007 Tim Waugh <twaugh@redhat.com> 8.15.4-2
-- Fixed configuration file locations (bug #233966).
-
-* Wed Mar 14 2007 Tim Waugh <twaugh@redhat.com> 8.15.4-1
-- 8.15.4.
-
-* Thu Jan 25 2007 Tim Waugh <twaugh@redhat.com> 8.15.3-7
-- dvipdf script fixes (bug #88906).
-- Moved libijs.so and libgs.so into devel package (bug #203623).
-
-* Wed Jan 24 2007 Tim Waugh <twaugh@redhat.com> 8.15.3-6
-- Configure with --with-drivers=ALL since the advertised default is not
-  what gets used (bug #223819).
-
-* Thu Jan 18 2007 Tim Waugh <twaugh@redhat.com> 8.15.3-5
-- Backported gxcht 64bit crash fix from GPL trunk (bug #177763).
-
-* Fri Jan 12 2007 Tim Waugh <twaugh@redhat.com> 8.15.3-4
-- Own cjkv directory (bug #221380, bug #222375).
-
-* Tue Dec  5 2006 Tim Waugh <twaugh@redhat.com> 8.15.3-3
-- Added split-cidfnmap patch (bug #194592).
-
-* Thu Nov 16 2006 Tim Waugh <twaugh@redhat.com> 8.15.3-2
-- 8.15.3.  No longer need gtk2, ps2epsi, badc, pagesize,
-  use-external-freetype, split-font-configuration or cjkv patches.
-- Renumbered patches.
-
-* Tue Oct  3 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-9
-- Apply CJKV patch from svn164:165 plus the fix from svn173:174 (bug #194592,
-  bug #203712, possibly bug #167596).
-
-* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 8.15.2-8.1
-- rebuild
-
-* Fri Jun 23 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-8
-- Revert CJKV patch.

* Tue Jun 19 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.15.4-1m)
- version 8.15.4
- NOTICE! This package was based on that of Fedora at version 8.15.2 and it may be needed to resync again.
- modify_ps2pdfwr.patch is updated for 8.15.x
- New setting files for fonts: cidfmap, cidfmap.ja (see http://tagoh.jp/w/wiliki.cgi?ghostscript8 for detail.)
- Freetype API should not be used since it does not work well.(Rendering are miserable.)

* Wed Jun 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.15.4-0m)
- version 8.15.4 test
- 
-* Fri Oct 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- - (8.15.3-0m)
- - 8.15.3 test
-* Wed Jun 21 2006 Masayuki SANO <nosanosa@momonga-linux.org>
-- (8.15.2-0m)
-- sync with Fedora Core
-* Wed Jun 14 2006 Tomas Mraz <tmraz@redhat.com> - 8.15.2-7
-- rebuilt with new gnutls
-* Tue Jun 13 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-6
-- Undo svn sync.
-- Apply CJKV patch from svn164:165.
-* Fri Jun  9 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-5
-- Sync to svn165.
-* Fri May 26 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-4
-- Fix ijs-config not to have multilib conflicts (bug #192672)
-* Tue May  2 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-3
-- Remove adobe-cmaps and acro5-cmaps, since latest CMaps are already
-  included (bug #190463).
-* Tue Apr 25 2006 Tim Waugh <twaugh@redhat.com> 8.15.2-2
-- 8.15.2.
-- No longer need build, krgb, pdfwrite, str1570 patches.
-* Mon Apr 24 2006 Tim Waugh <twaugh@redhat.com> 8.15.1-10
-- Fix emacs interaction (bug #189321, STR #1570).
-* Mon Apr 10 2006 Tim Waugh <twaugh@redhat.com> 8.15.1-9
-- Add %%{_datadir}/fonts/japanese to font path (bug #188448).
-- Spec file cleanups (bug #188066).
-* Sat Apr  8 2006 Tim Waugh <twaugh@redhat.com>
-- Build requires libtool (bug #188341).
-* Thu Apr  6 2006 Tim Waugh <twaugh@redhat.com> 8.15.1-8
-- Fix pdfwrite (bug #187834).
-- CUPS filters go in /usr/lib/cups/filter even on lib64 platforms.
-* Thu Mar  2 2006 Tim Waugh <twaugh@redhat.com> 8.15.1-7
-- BuildRequires: gnutls-devel
-- Updated KRGB patch for gdevijs.
-* Tue Feb 28 2006 Karsten Hopp <karsten@redhat.de> 8.15.1-6
-- BuildRequires: libXt-devel
-* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 8.15.1-5.2
-- bump again for double-long bug on ppc(64)
-* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 8.15.1-5.1
-- rebuilt for new gcc4.1 snapshot and glibc changes
-* Mon Jan 30 2006 Tim Waugh <twaugh@redhat.com> 8.15.1-5
-- Updated adobe-cmaps to 200406 (bug #173613).
-* Fri Jan 27 2006 Tim Waugh <twaugh@redhat.com> 8.15.1-4
-- Support reading a big cmap/post table from a TrueType font.
-* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
-- rebuilt
-* Wed Nov  9 2005 Tomas Mraz <tmraz@redhat.com> 8.15.1-3
-- Build does not explicitly require xorg-x11-devel.
-* Wed Nov  9 2005 Tomas Mraz <tmraz@redhat.com> 8.15.1-2
-- rebuilt with new openssl
-* Mon Sep 26 2005 Tim Waugh <twaugh@redhat.com> 8.15.1-1
-- Some directories should be "8.15" not "8.15.1" (bug #169198).
-* Thu Sep 22 2005 Tim Waugh <twaugh@redhat.com> 8.15.1-0.1
-- 8.15.1.
-- No longer need overflow patch.
-* Tue Aug 16 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc4.3
-- Rebuilt for new cairo.
-* Mon Aug 15 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc4.2
-- Parametrize freetype, and disable it (bug #165962).
-* Fri Aug 12 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc4.1
-- 8.15rc4.
-- Fixed lips4v driver (bug #165713).
-* Tue Aug  9 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.7
-- Install adobe/acro5 CMaps (bug #165428).
-* Mon Jul 18 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.6
-- Fixed split font configuration patch (bug #161187).
-* Wed Jul 13 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.5
-- Split font configuration (bug #161187).
-- Reverted this change:
-  - Build requires xorg-x11-devel, not XFree86-devel.
-* Tue Jul 12 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.4
-- Add Japanese fonts to FAPIcidfmap (bug #161187).
-- Moved Resource directory.
-- Added use-external-freetype patch (bug #161187).
-* Mon Jul 11 2005 Tim Waugh <twaugh@redhat.com>
-- Build requires libtiff-devel (bug #162826).
-* Thu Jun  9 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.3
-- Build requires xorg-x11-devel, not XFree86-devel.
-- Include ierrors.h in the devel package.
-* Wed Jun  8 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.2
-- Drop 'Provides: libijs.so' because it is incorrect.
-- Build igcref.c with -O0 to work around bug #150771.
-- Renumber patches.
-* Fri Jun  3 2005 Tim Waugh <twaugh@redhat.com> 8.15-0.rc3.1
-- Switch to ESP Ghostscript.
-- 8.15rc3.
-- Lots of patches dropped.  Perhaps some will need to be re-added.

* Sat Jun 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.07-20m)
- %%{_prefix}/lib/cups/filter/* --> %%{_libdir}/cups/filter/*

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.07-19m)
- fix gs_fontpath and %%files

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.07-18m)
- add %%dir %{_sysconfdir}/ghostscript for fonts

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.07-17m)
- revise %%files to avoid conflicting
- /usr/share/man/de/man1 is already provided by man

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.07-16m)
- rebuild against openssl-0.9.8a

* Mon Feb 28 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (7.07-15m)
- patch from fc3.
- fix for ppc segfault.

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (7.07-14m)
- rebuild against libstdc++-3.4.1
- add BuildPrereq: libstdc++-devel
- add gcc34 patch

* Tue Jul 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (7.07-13m)
- use sazanami instead of kochi

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (7.07-12m)
- revised spec for enabling rpm 4.2.

* Thu Dec 18 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-11m)
- apply the patch for gdevlips
  see http://takeno.iee.niit.ac.jp/~shige/FreeBSD/LBP/lbp.html
  for details.

* Sun Nov 23 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-10m)
- update ESP ghostscript to CVS version as of 2003/11/19
- update eplaser-3.1.2.patch (diff between eplaser-3.1.1 and eplaser-3.1.2) 
- remove ghostscript-7.05-png.patch
  this version of espgs contains this fix

* Mon Nov 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (7.07-9m)
- apply eplaser-3.1.2.patch for more EPSON printers

* Wed Nov  5 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-8m)
- update ESP ghostscript to CVS version as of 2003/10/26
  (eplaser driver was updated to 3.1.1)
- remove eplaser-3.0.6.diff

* Tue Oct 21 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-7m)
- add the following three patches
  (handle NeverEmbed CJK TrueType into PDF,
   write correct glyph widths of CJK TrueType into PDF,
   and not to write CIDToGIDMap of NeverEmbed CJK TrueType and 
   Encoding of CIDFont into PDF).

    - fix_cidfontname_Encoding_CIDToGIDMap_DW_W.patch, 
    - fix_rename_font_gs_cidfn.ps.patch
    - modify_ps2pdfwr.patch

  see http://www.aihara.co.jp/~taiji/gyve/, [gs-cjk 02110], [gs-cjk 02111]
  for details.

* Wed Oct 15 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-6m)
- upgrade espgs to 7.07.1
- remove espgs-7.07-ps2pdfwr-gsoption.patch because this bug was fixed.
  cf. http://www.cups.org/str.php?L225
      http://www.cups.org/str.php?L230
- add BuildPrereq: libtiff-devel

* Wed Sep 17 2003 zunda <zunda at freeshell.org>
- (7.07-5m)
- espgs-7.07-ps2pdfwr-gsoption.patch to run ps2pdf,
  thanks to yamagoo at bf7.so-net.ne.jp

* Thu Aug 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (7.07-4m)
- TEMPORALLY use TTF version of Kochi fonts instead of CID one
- TEMPORALLY cancel 'Requires: kochi-cidfont'

* Thu Aug 21 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (7.07-3m)
- modify %{gs_fontpath} to adjust to change of urw-font directory

* Sat Aug 16 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-2m)
- update ESP ghostscript to 7.07rc1
  remove espgs-7.07-no_pstopcl6.patch accordingly
- add ghostscript-7.07-coverage-glyphcount.patch
  and update ghostscript-7.07-gsublookuptable.patch (see [gs-cjk 02097])

* Sun Jul 27 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.07-1m)
- Update ESP ghostscript to CVS version as of 2003/07/20
  (ghostscript-7.07 has been imported to espgs).
- Add ghostscript-7.07-bigposttable.patch.
  With this patch applied, kochi-substitute can be used correctly.
  See the followings for details:
  Ref. - [gs-cjk 02088]
       - http://tagoh.jp/d/?date=20030723
         http://tagoh.jp/d/?date=20030724
         http://tagoh.jp/d/?date=20030725
  ghostscript-7.07-gsublookuptable.patch is also added,
  but disabled for now because it causes promblem in vertical writing mode.
- Add the knob to disable for building stp (%%with_stp).
  Those who do not want to require gimp-print can set this macro to 0
  in specopt. Note that ijsgimpprint can be used instead of stp,
  and stp will be obsoleted on gimp-print-4.3.
- use %%make and %%setup

* Wed May 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (7.05-12m)
- separate ghostscript-resource package

* Fri May 23 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.05-11m)
- [security] add ghostscript-pipe.patch to fix %%pipe%% security problem
  contained in GPL (GNU) ghostscript-7.07.
  see http://www.ghostscript.com/article/41.html

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.05-10m)
- rebuild against zlib 1.1.4-5m

* Thu Mar 27 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.05-9m)
- update ESP ghostscript to CVS version as of 2003/03/24 (post-7.05.6)
- update eplaser to 3.0.6
- depends on kochi-cidfont instead of truetype-fonts-ja

* Fri Mar 21 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (7.05-8m)
- explicitly supply --enable-cups/--disable-cups to configure
  if %%with_cups is set to 1/0 respectively.
- set %%with_cups to 1 by default

* Sat Mar 15 2003 Kikutani Makoto <poo@momonga-linux.org>
- (7.05-7m)
- subpackage ghostscript-cups 

* Tue Jan 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (7.05-6m)
- add png patch

* Sat Jan  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (7.05-5m)
- update eplaser to 3.0.4-651

* Sat Jan  4 2003 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (7.05-4m)
- Now, we use ESP ghostscript instead of GNU ghostscript
  since the former contains almost all nessesary printer drivers 
  and some bug fixes against the latter.

  The version of ESP ghostscript at this time is
  CVS snapshot as of 2002/12/28 (pre 7.05.6).

- separate ghostscript-fonts into another spec.
- remove some BuildPrereq:'s which seem to be unneeded.

* Tue Dec 24 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (7.05-3m)
- separate hpijs

* Fri Dec 20 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (7.05-2m)
- separate ijs
- Obsoletes: ghostscript-pdfencrypt
- make hpijs be NoSource

* Thu Dec  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (7.05-1m)
- import from rawhide
- drop VFlib support
- revise CIDFnmaps
- update eplaser to 3.0.4-651

* Tue Nov 26 2002 Tim Waugh <twaugh@redhat.com> 7.05-25
- Fix level 1 PostScript output (bug #78450).
- No need to carry gomni.c, since it comes from the patch.

* Mon Nov 11 2002 Tim Waugh <twaugh@redhat.com> 7.05-24
- Omni 071902 patch.

* Mon Nov 11 2002 Tim Waugh <twaugh@redhat.com> 7.05-23
- hpijs-1.3, with updated rss patch.
- Fix XLIBDIRS.

* Fri Oct 25 2002 Tim Waugh <twaugh@redhat.com> 7.05-22
- hpijs-rss 1.2.2.

* Mon Oct 14 2002 Tim Waugh <twaugh@redhat.com> 7.05-21
- Set libdir when installing.

* Thu Aug 15 2002 Tim Waugh <twaugh@redhat.com> 7.05-20
- Add cups device (bug #69573).

* Mon Aug 12 2002 Tim Waugh <twaugh@redhat.com> 7.05-19
- Fix the gb18030 patch (bug #71135, bug #71303).

* Sat Aug 10 2002 Elliot Lee <sopwith@redhat.com> 7.05-18
- rebuilt with gcc-3.2 (we hope)

* Fri Aug  9 2002 Tim Waugh <twaugh@redhat.com> 7.05-17
- Add CIDnmap for GB18030 font (bug #71135).
- Fix URL (bug #70734).

* Tue Jul 23 2002 Tim Waugh <twaugh@redhat.com> 7.05-16
- Rebuild in new environment.

* Tue Jul  9 2002 Tim Waugh <twaugh@redhat.com> 7.05-15
- Remove the chp2200 driver again, to fix cdj890 (bug #67578).

* Fri Jul  5 2002 Tim Waugh <twaugh@redhat.com> 7.05-14
- For CJK font support, use CIDFnmap instead of CIDFont
  resources (bug #68009).

* Wed Jul  3 2002 Tim Waugh <twaugh@redhat.com> 7.05-13
- Build requires unzip and gtk+-devel (bug #67799).

* Wed Jun 26 2002 Tim Waugh <twaugh@redhat.com> 7.05-12
- File list tweaking.
- More file list tweaking.

* Tue Jun 25 2002 Tim Waugh <twaugh@redhat.com> 7.05-10
- Rebuild for bootstrap.

* Wed Jun 19 2002 Tim Waugh <twaugh@redhat.com> 7.05-9
- Omni 052902 patch.

* Mon Jun 10 2002 Tim Waugh <twaugh@redhat.com> 7.05-8
- Requires recent version of patchutils (bug #65947).
- Don't ship broken man page symlinks (bug #66238).

* Wed May 29 2002 Tim Waugh <twaugh@redhat.com> 7.05-7
- Put gsx in its own package.

* Tue May 28 2002 Tim Waugh <twaugh@redhat.com> 7.05-6
- New gomni.c from IBM to fix an A4 media size problem.
- Use new Adobe CMaps (bug #65362).

* Sun May 26 2002 Tim Powers <timp@redhat.com> 7.05-5
- automated rebuild

* Wed May 22 2002 Tim Waugh <twaugh@redhat.com> 7.05-4
- New gomni.c from IBM to fix bug #65269 (again).

* Tue May 21 2002 Tim Waugh <twaugh@redhat.com> 7.05-2
- Don't apply bogus parts of vflib patch (bug #65268).
- Work around Omni -sPAPERSIZE=a4 problem (bug #65269).

* Mon May 20 2002 Tim Waugh <twaugh@redhat.com> 7.05-1
- 7.05.
- No longer need mkstemp, vflib.fixup, quoting, or PARANOIDSAFER
  patches.
- Don't apply CJK patches any more (no longer needed).
- Updated Source15, Patch0, Patch10, Patch5, Patch24, Patch14, Patch12.
- Made gdevdmpr.c compile again.
- Move gimp-print to a separate package.
- Ship the shared object too (and a .so file that is dlopened).
- Update Omni patch.  No longer need Omni_path, Omni_quiet, Omni_glib patches.
- Require Omni >= 0.6.1.
- Add patch to fix gtk+ initial window size.
- Add devel package with header files.
- Turn on IJS support.
- Update hpijs to 1.1.
- Don't ship the hpijs binary in the ghostscript package.
- Use -fPIC when building ijs.

* Wed Apr  3 2002 Tim Waugh <twaugh@redhat.com> 6.52-8
- New CIDFonts (bug #61015).

* Wed Apr  3 2002 Tim Waugh <twaugh@redhat.com> 6.52-7
- Fix release numbers of sub packages.
- Handle info files, use ldconfig (bug #62574).

* Tue Mar 19 2002 Tim Waugh <twaugh@redhat.com> 6.52-6
- Fix config patch so that gs --help displays the right thing.
- Don't ship sysvlp.sh.
- Fix some shell scripts.
- Ship escputil man page (bug #58919).

* Mon Feb 11 2002 Tim Waugh <twaugh@redhat.com> 6.52-5
- Add CHP2200 driver (bug #57516).
- Fix gimp-print-4.2.0 so that it builds without cups-config.

* Sat Feb  2 2002 Bill Nottingham <notting@redhat.com> 6.52-4
- do condrestart in %postun, not %post

* Fri Feb  1 2002 Bernhard Rosenkraenzer <bero@redhat.com> 6.52-3
- Restart service cups after installing gimp-print-cups

* Sun Jan 27 2002 Bernhard Rosenkraenzer <bero@redhat.com> 6.52-2
- hpijs is finally free - support it.
- Add extra package for CUPS support

* Mon Jan 21 2002 Bernhard Rosenkraenzer <bero@redhat.com> 6.52-1
- Updates:
  - ghostscript 6.52
  - hpdj 2.6 -> pcl3 3.3
  - CJK Patchlevel 3, adobe-cmaps 200109
  - gimp-print 4.2.0
- Adapt patches
- Fix various URLs
- Begin cleaning up spec file
- Fix bugs #21879 and #50923

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Oct 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-16
- update the Omni driver, and patch it to seek in /usr/lib/Omni/ first
- require Omni

* Mon Oct 01 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-15
- change -dPARANOIDSAFER to punch a hole for OutputFile

* Mon Sep 17 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-14
- add -dPARANOIDSAFER to let us breathe a little easier in the print spooler.

* Thu Sep 13 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-13
- apply jakub's fix to ghostscript's jmp_buf problems; #49591

* Wed Sep  5 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-12
- fix lprsetup.sh; #50925

* Fri Aug 24 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-11
- added Epson's old eplaseren drivers,
- pointed out by Till Kamppeter <till.kamppeter@gmx.net>

* Tue Aug 21 2001 Paul Howarth <paul@city-fan.org> 6.51-10
- included Samsung GDI driver for ML-4500 printer support.

* Sun Aug 19 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-9
- applied IBM's glib patches for Omni, which now works.
- BE AWARE: we now link against libstdc++ and glib for this, and use a c++
- link stage to do the dirty.
- added glib-devel buildreq and glib req, I don't think we require everything
- yet, I could pull in sasl.

* Sun Aug 19 2001 David Suffield <david_suffield@hp.com> 6.51-8
- Added gs device hpijs and updated gdevhpij.c to hpijs 0.97

* Wed Aug 15 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-7
- pull in ynakai's update to the cjk resources.

* Thu Aug  9 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-6
- turn dmprt and cdj880 back on. for some reason, they work now.
- voodoo, who knows.

* Thu Aug  9 2001 Yukihiro Nakai <ynakai@redhat.com> 6.51-5
- Add cjk resources

* Thu Aug  1 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-4
- applied drepper@redhat.com's patch for #50300
- fixed build deps on zlib-devel and libpng-devel, #49853
- made gs_init.ps a config file; #25096
- O\^/nZ the daTa directorieZ now; #50693

* Tue Jul 24 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-3
- wired up the Resource dir and the Font and CIDFont maps.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 6.51-2
- luckily, I had a spare chicken. Thanks to some work by Nakai, and one last
- desperate search through google, everything /seems/ to be working. I know
- that there are going to be problems in the japanese code, and I need to turn
- on the cjk font map from adobe, but it /works/ at the moment.

* Thu Jun 21 2001 Crutcher Dunnavant <crutcher@redhat.com>
- upgraded to 6.51, a major version upgrade
- rewrote spec file, threw out some patches
- turned on IBM's Omni print drivers interface
- turned on HP's hpijs print drivers interface
- turned on every driver that looked usable from linux
- sacrificed a chicken to integrate the old Japanese drivers
- - This didn't work. The japanese patches are turned off, pending review.
- - I can do loops with C, but the bugs are in Postscript init files

* Wed Apr 11 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added P. B. West's lx5000 driver

* Tue Feb 27 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added xtt-fonts requirement (for VFlib)

* Fri Feb  9 2001 Adrian Havill <havill@redhat.com>
- cmpskit removed as a build prereq

* Thu Feb  8 2001 Crutcher Dunnavant <crutcher@redhat.com>
- merged in some patches that got away:
-	* Fri Sep  1 2000 Mitsuo Hamada <mhamada@redhat.com>
	- add support JIS B size
	- fix the problem of reconverting GNUPLOT output

* Thu Feb  8 2001 Crutcher Dunnavant <crutcher@redhat.com>
- switched to japanese for everybody

* Thu Feb  8 2001 Crutcher Dunnavant <crutcher@redhat.com>
- tweaked time_.h to test for linux, and include the right
- header

* Wed Feb  7 2001 Crutcher Dunnavnat <crutcher@redhat.com>
- added the lxm3200 driver

* Mon Dec 11 2000 Crutcher Dunnavant <crutcher@redhat.com>
- merged in the (accendental) branch that contained the mktemp
- and LD_RUN_PATH bug fixes.

* Tue Oct 17 2000 Jeff Johnson <jbj@redhat.com>
- tetex using xdvi with ghostscript patch (#19212).

* Tue Sep 12 2000 Michael Stefaniuc <mstefani@redhat.com>
- expanded the gcc296 patch to fix a compilation issue with the new stp
  driver

* Mon Sep 11 2000 Michael Stefaniuc <mstefani@redhat.com>
- added the stp driver from the gimp-print project.
  It supports high quality printing especialy with Epson Stylus Photo.

* Wed Aug  2 2000 Matt Wilson <msw@redhat.com>
- rebuilt against new libpng

* Wed Aug  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up the cdj880 patch (Bug #14978)
- Fix build with gcc 2.96

* Fri Jul 21 2000 Bill Nottingham <notting@redhat.com>
- turn off japanese support

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jul 07 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fixed the broken inclusion of files in /usr/doc
- Build requires freetype-devel

* Fri Jun 16 2000 Matt Wilson <msw@redhat.com>
- build japanese support in main distribution
- FHS manpage paths

* Sun Mar 26 2000 Chris Ding <cding@redhat.com>
- enabled bmp16m driver

* Thu Mar 23 2000 Matt Wilson <msw@redhat.com>
- added a boatload of Japanese printers

* Thu Mar 16 2000 Matt Wilson <msw@redhat.com>
- add japanese support, enable_japanese macro

* Mon Feb 14 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 5.50 at last...
- hpdj 2.6
- Added 3rd party drivers:
  - Lexmark 5700 (lxm5700m)
  - Alps MD-* (md2k, md5k)
  - Lexmark 2050, 3200, 5700 and 7000 (lex2050, lex3200, lex5700, lex7000)

* Fri Feb  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild to compress man page
- fix gs.1 symlink

* Wed Jan 26 2000 Bill Nottingham <notting@redhat.com>
- add stylus 740 uniprint files

* Thu Jan 13 2000 Preston Brown <pbrown@redhat.com>
- add lq850 dot matrix driver (#6357)

* Thu Oct 28 1999 Bill Nottingham <notting@redhat.com>
- oops, include oki182 driver.

* Tue Aug 24 1999 Bill Nottingham <notting@redhat.com>
- don't optimize on Alpha. This way it works.

* Thu Jul 29 1999 Michael K. Johnson <johnsonm@redhat.com>
- added hpdj driver
- changed build to use tar_cat so adding new drivers is sane

* Thu Jul  1 1999 Bill Nottingham <notting@redhat.com>
- add OkiPage 4w+, HP 8xx drivers
* Mon Apr  5 1999 Bill Nottingham <notting@redhat.com>
- fix typo in config patch.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 6)

* Mon Mar 15 1999 Cristian Gafton <gafton@redhat.com>
- added patch from rth to fix alignement problems on the alpha.

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Mon Feb 08 1999 Bill Nottingham <notting@redhat.com>
- add uniprint .upp files

* Sat Feb 06 1999 Preston Brown <pbrown@redhat.com>
- fontpath update.

* Wed Dec 23 1998 Preston Brown <pbrown@redhat.com>
- updates for ghostscript 5.10

* Fri Nov 13 1998 Preston Brown <pbrown@redhat.com>
- updated to use shared urw-fonts package.
* Mon Nov 09 1998 Preston Brown <pbrown@redhat.com>
- turned on truetype (ttf) font support.

* Thu Jul  2 1998 Jeff Johnson <jbj@redhat.com>
- updated to 4.03.

* Tue May 05 1998 Cristian Gafton <gafton@redhat.com>
- enabled more printer drivers
- buildroot

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Mar 03 1997 Erik Troan <ewt@redhat.com>
- Made /usr/share/ghostscript/3.33/Fontmap a config file.
