%global momorel 6

Name:           desktop-effects
Version:        0.8.7
Release:        %{momorel}m%{?dist}
Summary:        Switch GNOME window management and effects

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://git.fedoraproject.org/git/desktop-effects.git
Source0:        https://fedorahosted.org/released/desktop-effects/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:		desktop-effects-0.8.7-momonga.patch
Patch1:		desktop-effects-0.8.7-no_compiz_option.patch
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:  desktop-file-utils
BuildRequires:  GConf2-devel
BuildRequires:  gtk2-devel
BuildRequires:  intltool
BuildRequires:  libglade2-devel
BuildRequires:  libXcomposite-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:	autoconf automake libtool

Requires:       gnome-session
Requires:       hicolor-icon-theme

%description
desktop-effects provides a preference dialog to allow switching the GNOME
desktop between three different window managers: Metacity (the standard
GNOME 2 window manager), Compiz (offering 3D acceleration and special
effects), and GNOME Shell, which offers a preview of the GNOME 3 user
experience.

%prep
%setup -q

%patch0 -p1 -b .momonga
%patch1 -p1 -b .no_compiz_option

%build
%configure

%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

desktop-file-validate %{buildroot}%{_datadir}/applications/desktop-effects.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/desktop-effects
%{_datadir}/icons/hicolor/*/apps/desktop-effects.png
%{_datadir}/applications/desktop-effects.desktop
%{_datadir}/desktop-effects/

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ] ; then
  /usr/bin/gtk-update-icon-cache -q %{_datadir}/icons/hicolor;
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ] ; then
  /usr/bin/gtk-update-icon-cache -q %{_datadir}/icons/hicolor;
fi

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-4m)
- full rebuild for mo7 release

* Sat Aug 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.7-3m)
- add no_compiz_option patch.
- these option can't setting compiz-0.9

* Thu Aug 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.7-2m)
- fix can not run compiz

* Wed Aug 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.7-1m)
- Initial commit Momonga Linux

* Wed Jun 23 2010 Owen Taylor <otaylor@redhat.com> - 0.8.7-2
- Remove call to autogen.sh (not needed for 'make distcheck' tarball)

* Wed Jun 23 2010 Owen Taylor <otaylor@redhat.com> - 0.8.7-1
- Update to 0.8.7
- Resolves #533807 (Crash in glDestroyContext)
- Resolves #574500 (BadMatch from CreateWindow)

* Tue Apr 13 2010 Adel Gadllah <adel.gadllah@gmail.com> - 0.8.6-1
- Update to 0.8.6
- Adds missing translations (RH #581395)

* Wed Apr 06 2010 Adel Gadllah <adel.gadllah@gmail.com> - 0.8.5-2
- Fix changelog

* Tue Apr 06 2010 Adel Gadllah <adel.gadllah@gmail.com> - 0.8.5-1
- Update to 0.8.5
- Fixes RH #532618
- Includes new icons
- Drop upstreamed patch

* Tue Feb 09 2010 Adam Jackson <ajax@redhat.com> 0.8.4-3
- desktop-effects-0.8.4-add-needed.patch: Fix FTBFS from --no-add-needed

* Fri Sep 18 2009 Owen Taylor <otaylor@redhat.com> - 0.8.4-1
- Update to 0.8.4 (fixes #524102)

* Mon Sep 14 2009 Owen Taylor <otaylor@redhat.com> - 0.8.3-1
- Update to 0.8.3 (translations)

* Fri Sep  4 2009 Owen Taylor <otaylor@redhat.com> - 0.8.2-2
- Add missing BuildRequires on mesa-libGL-devel

* Fri Sep  4 2009 Owen Taylor <otaylor@redhat.com> - 0.8.2-1
- Update to 0.8.2

* Mon Aug 24 2009 Owen Taylor <otaylor@redhat.com> - 0.8.1-1
- Update to 0.8.1 (fixes leftover debugging hack)

* Mon Aug 24 2009 Owen Taylor <otaylor@redhat.com> - 0.8.0-2
- Add dist to release
- BuildRequire intltool and desktop-file-utils
- Point Source: at https://fedorahosted.org/released

* Sun Aug 23 2009 Owen Taylor <otaylor@redhat.com> - 0.8.0-1
- Initial version

