%global momorel 7
# This file is encoded in UTF-8.  -*- coding: utf-8 -*-
Summary: GNU Emacs text editor
Name: emacs
Version: 24.2
Release: %{momorel}m%{?dist}
License: GPLv3+
URL: http://www.gnu.org/software/emacs/
Group: Applications/Editors
Source0: ftp://ftp.gnu.org/gnu/emacs/emacs-%{version}.tar.bz2
NoSource: 0
Source1: emacs.desktop
Source2: emacsclient.desktop
Source3: dotemacs.el
Source4: site-start.el
# rpm-spec-mode from XEmacs
Source10: rpm-spec-mode.el
Source11: rpm-spec-mode-init.el
Source13: focus-init.el
Source18: default.el
# Emacs Terminal Mode, #551949, #617355
Source19: emacs-terminal.desktop
Source20: emacs-terminal.sh
Patch0: glibc-open-macro.patch
Patch1: rpm-spec-mode.patch
Patch2: rpm-spec-mode-utc.patch
Patch3: rpm-spec-mode-changelog.patch
# rhbz#713600
Patch7: emacs-spellchecker.patch
# rhbz#830162
Patch8: emacs-locate-library.patch

BuildRequires: atk-devel, cairo-devel, freetype-devel, fontconfig-devel, dbus-devel, giflib-devel, glibc-devel, gtk3-devel, libpng-devel
BuildRequires: libjpeg-devel, libtiff-devel, libX11-devel, libXau-devel, libXdmcp-devel, libXrender-devel, libXt-devel
BuildRequires: libXpm-devel, ncurses-devel, xorg-x11-proto-devel, zlib-devel, gnutls-devel >= 3.2.0
BuildRequires: librsvg2-devel, m17n-lib-devel, libotf-devel, ImageMagick-devel >= 6.8.8.10, libselinux-devel
BuildRequires: GConf2-devel, alsa-lib-devel, gpm-devel, liblockfile-devel, libxml2-devel
BuildRequires: autoconf, automake, bzip2, cairo, texinfo, gzip
# Desktop integration
BuildRequires: desktop-file-utils
# Buildrequire both python2 and python3 since below we turn off the
# brp-python-bytecompile script
BuildRequires: python2-devel python3-devel
%ifarch %{ix86}
BuildRequires: util-linux
%endif
Requires: desktop-file-utils
# Emacs doesn't run without these fonts, rhbz#732422
Requires: xorg-x11-fonts-misc
Requires(preun): %{_sbindir}/alternatives
Requires(posttrans): %{_sbindir}/alternatives
Requires: emacs-common = %{version}-%{release}
Provides: emacs(bin) = %{version}-%{release}

# Turn off the brp-python-bytecompile script since this script doesn't
# properly dtect the correct python runtime for the files emacs2.py and
# emacs3.py
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

%define paranoid 1
%if 0%{?fedora}
%define expurgate 0
%else
%define expurgate 1
%endif

%define site_lisp %{_datadir}/emacs/site-lisp
%define site_start_d %{site_lisp}/site-start.d
%define bytecompargs -batch --no-init-file --no-site-file -f batch-byte-compile
%define pkgconfig %{_datadir}/pkgconfig
%define emacs_libexecdir %{_libexecdir}/emacs/%{version}/%{_host}

%description
Emacs is a powerful, customizable, self-documenting, modeless text
editor. Emacs contains special code editing features, a scripting
language (elisp), and the capability to read mail, news, and more
without leaving the editor.

This package provides an emacs binary with support for X windows.

%package nox
Summary: GNU Emacs text editor without X support
Group: Applications/Editors
Requires(preun): %{_sbindir}/alternatives
Requires(posttrans): %{_sbindir}/alternatives
Requires: emacs-common = %{version}-%{release}
Provides: emacs(bin) = %{version}-%{release}

%description nox
Emacs is a powerful, customizable, self-documenting, modeless text
editor. Emacs contains special code editing features, a scripting
language (elisp), and the capability to read mail, news, and more
without leaving the editor.

This package provides an emacs binary with no X windows support for running
on a terminal.

%package common
Summary: Emacs common files
Group: Applications/Editors
Requires(preun): /sbin/install-info
Requires(preun): %{_sbindir}/alternatives
Requires(posttrans): %{_sbindir}/alternatives
Requires(post): /sbin/install-info
Requires: %{name}-filesystem

%description common
Emacs is a powerful, customizable, self-documenting, modeless text
editor. Emacs contains special code editing features, a scripting
language (elisp), and the capability to read mail, news, and more
without leaving the editor.

This package contains all the common files needed by emacs or emacs-nox.

%package el
Summary: Lisp source files included with GNU Emacs
Group: Applications/Editors
Requires: %{name}-filesystem
BuildArch: noarch

%description el
Emacs-el contains the emacs-elisp sources for many of the elisp
programs included with the main Emacs text editor package.

You need to install emacs-el only if you intend to modify any of the
Emacs packages or see some elisp examples.

%package terminal
Summary: A desktop menu item for GNU Emacs terminal.
Group: Applications/Editors
Requires: emacs = %{version}-%{release}
BuildArch: noarch

%description terminal

Contains a desktop menu item running GNU Emacs terminal. Install
emacs-terminal if you need a terminal with Malayalam support.

Please note that emacs-terminal is a temporary package and it will be
removed when another terminal becomes capable of handling Malayalam.

%package filesystem
Summary: Emacs filesystem layout
Group: Applications/Editors
BuildArch: noarch

%description filesystem
This package provides some directories which are required by other
packages that add functionality to Emacs.

%prep
%setup -q

%patch0 -p1 -b .glibc-open-macro
%patch7 -p1 -b .spellchecker
%patch8 -p1 -b .locate-library

# Install site-lisp files
cp %SOURCE10 site-lisp
pushd site-lisp
%patch1 -p0
%patch2 -p0
%patch3 -p0
popd

# We prefer our emacs.desktop file
cp %SOURCE1 etc/emacs.desktop

grep -v "tetris.elc" lisp/Makefile.in > lisp/Makefile.in.new \
   && mv lisp/Makefile.in.new lisp/Makefile.in

# Avoid trademark issues
%if %{paranoid}
rm -f lisp/play/tetris.el lisp/play/tetris.elc
%endif

%if %{expurgate}
rm -f etc/sex.6 etc/condom.1 etc/celibacy.1 etc/COOKIES etc/future-bug etc/JOKES
%endif

%define info_files ada-mode auth autotype calc ccmode cl dbus dired-x ebrowse ede ediff edt eieio efaq eintr elisp emacs emacs-gnutls emacs-mime epa erc ert eshell eudc flymake forms gnus idlwave info mairix-el message mh-e newsticker nxml-mode org pcl-cvs pgg rcirc reftex remember sasl sc semantic ses sieve smtpmail speedbar tramp url vip viper widget woman

if test "$(perl -e 'while (<>) { if (/^INFO_FILES/) { s/.*=//; while (s/\\$//) { s/\\//; $_ .= <>; }; s/\s+/ /g; s/^ //; s/ $//; print; exit; } }' Makefile.in)" != "%info_files"; then
  echo Please update info_files >&2
  exit 1
fi

%ifarch %{ix86}
%define setarch setarch %{_arch} -R
%else
%define setarch %{nil}
%endif

# Avoid duplicating doc files in the common subpackage
ln -s ../../%{name}/%{version}/etc/COPYING doc
ln -s ../../%{name}/%{version}/etc/NEWS doc

%build
# Remove unpatched files as all files in the lisp directory are
# installed.
rm lisp/textmodes/ispell.el.spellchecker

export CFLAGS="-DMAIL_USE_LOCKF $RPM_OPT_FLAGS"

# We patch configure.in so we have to do this
autoconf

# Build GTK+3 binary
mkdir build-gtk && cd build-gtk
ln -s ../configure .

%configure --with-dbus --with-gif --with-jpeg --with-png --with-rsvg \
           --with-tiff --with-xft --with-xpm --with-x-toolkit=gtk3 --with-gpm=no \
	   --with-wide-int
make bootstrap
%{setarch} make %{?_smp_mflags}
cd ..

# Build binary without X support
mkdir build-nox && cd build-nox
ln -s ../configure .
%configure --with-x=no
%{setarch} make %{?_smp_mflags}
cd ..

# Make sure patched lisp files get byte-compiled
build-gtk/src/emacs %{bytecompargs} site-lisp/*.el

# Remove versioned file so that we end up with .1 suffix and only one DOC file
rm build-{gtk,nox}/src/emacs-%{version}.*

# Create pkgconfig file
cat > emacs.pc << EOF
sitepkglispdir=%{site_lisp}
sitestartdir=%{site_start_d}

Name: emacs
Description: GNU Emacs text editor
Version: %{version}
EOF

# Create macros.emacs RPM macro file
cat > macros.emacs << EOF
%%_emacs_version %{version}
%%_emacs_ev %{?epoch:%{epoch}:}%{version}
%%_emacs_evr %{?epoch:%{epoch}:}%{version}-%{release}
%%_emacs_sitelispdir %{site_lisp}
%%_emacs_sitestartdir %{site_start_d}
%%_emacs_bytecompile /usr/bin/emacs -batch --no-init-file --no-site-file --eval '(progn (setq load-path (cons "." load-path)))' -f batch-byte-compile
EOF

%install
cd build-gtk
make install INSTALL="%{__install} -p" DESTDIR=%{buildroot}
cd ..

# Let alternatives manage the symlink
rm %{buildroot}%{_bindir}/emacs
touch %{buildroot}%{_bindir}/emacs

# Do not compress the files which implement compression itself (#484830)
gunzip %{buildroot}%{_datadir}/emacs/%{version}/lisp/jka-compr.el.gz
gunzip %{buildroot}%{_datadir}/emacs/%{version}/lisp/jka-cmpr-hook.el.gz

# Install the emacs without X
install -p -m 0755 build-nox/src/emacs %{buildroot}%{_bindir}/emacs-%{version}-nox

# Make sure movemail isn't setgid
chmod 755 %{buildroot}%{emacs_libexecdir}/movemail

mkdir -p %{buildroot}%{site_lisp}
install -p -m 0644 %SOURCE4 %{buildroot}%{site_lisp}/site-start.el
install -p -m 0644 %SOURCE18 %{buildroot}%{site_lisp}

# This solves bz#474958, "update-directory-autoloads" now finally
# works the path is different each version, so we'll generate it here
echo "(setq source-directory \"%{_datadir}/emacs/%{version}/\")" \
 >> %{buildroot}%{site_lisp}/site-start.el

mv %{buildroot}%{_bindir}/{etags,etags.emacs}
mv %{buildroot}%{_mandir}/man1/{ctags.1.gz,gctags.1.gz}
mv %{buildroot}%{_mandir}/man1/{etags.1.gz,etags.emacs.1.gz}
mv %{buildroot}%{_bindir}/{ctags,gctags}

# Install site-lisp files
install -p -m 0644 site-lisp/*.el{,c} %{buildroot}%{site_lisp}

mkdir -p %{buildroot}%{site_lisp}/site-start.d
install -p -m 0644 %SOURCE11 %SOURCE13 %{buildroot}%{site_lisp}/site-start.d

# Default initialization file
mkdir -p %{buildroot}%{_sysconfdir}/skel
install -p -m 0644 %SOURCE3 %{buildroot}%{_sysconfdir}/skel/.emacs

# Install pkgconfig file
mkdir -p %{buildroot}/%{pkgconfig}
install -p -m 0644 emacs.pc %{buildroot}/%{pkgconfig}

# Install emacsclient desktop file
install -p -m 0644 %SOURCE2 %{buildroot}/%{_datadir}/applications/emacsclient.desktop

# Install rpm macro definition file
mkdir -p %{buildroot}%{_sysconfdir}/rpm
install -p -m 0644 macros.emacs %{buildroot}%{_sysconfdir}/rpm/

# Installing emacs-terminal binary
install -p -m 755 %SOURCE20 %{buildroot}%{_bindir}/emacs-terminal

# Removes ruby-mode.el*, which is provided by ruby.spec
rm -f %{buildroot}%{_datadir}/emacs/%{version}/lisp/progmodes/ruby-mode.el*

# After everything is installed, remove info dir
rm -f %{buildroot}%{_infodir}/dir
rm %{buildroot}%{_localstatedir}/games/emacs/*

# Install desktop files
mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --dir=%{buildroot}%{_datadir}/applications \
                     %SOURCE1
desktop-file-install --dir=%{buildroot}%{_datadir}/applications \
                     %SOURCE19

# Byte compile emacs*.py with correct python interpreters
%py_byte_compile %{__python} %{buildroot}%{_datadir}/%{name}/%{version}/etc/emacs.py
%py_byte_compile %{__python} %{buildroot}%{_datadir}/%{name}/%{version}/etc/emacs2.py
%py_byte_compile %{__python3} %{buildroot}%{_datadir}/%{name}/%{version}/etc/emacs3.py

#
# Create file lists
#
rm -f *-filelist {common,el}-*-files

( TOPDIR=${PWD}
  cd %{buildroot}

  find .%{_datadir}/emacs/%{version}/lisp \
    .%{_datadir}/emacs/%{version}/leim \
    .%{_datadir}/emacs/site-lisp \( -type f -name '*.elc' -fprint $TOPDIR/common-lisp-none-elc-files \) -o \( -type d -fprintf $TOPDIR/common-lisp-dir-files "%%%%dir %%p\n" \) -o \( -name '*.el.gz' -fprint $TOPDIR/el-bytecomped-files -o -fprint $TOPDIR/common-not-comped-files \)

)

# Put the lists together after filtering  ./usr to /usr
sed -i -e "s|\.%{_prefix}|%{_prefix}|" *-files
cat common-*-files > common-filelist
cat el-*-files common-lisp-dir-files > el-filelist

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
[ -L %{_bindir}/emacs ] || rm -f %{_bindir}/emacs
%{_sbindir}/update-alternatives --install %{_bindir}/emacs emacs %{_bindir}/emacs-%{version} 80

%postun
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
[ -e %{_bindir}/emacs-%{version} ] || \
  %{_sbindir}/update-alternatives --remove emacs %{_bindir}/emacs-%{version}

%post nox
[ -L %{_bindir}/emacs ] || rm -f %{_bindir}/emacs
%{_sbindir}/update-alternatives --install %{_bindir}/emacs emacs %{_bindir}/emacs-%{version}-nox 70

%postun nox
[ -e %{_bindir}/emacs-%{version}-nox ] || \
  %{_sbindir}/update-alternatives --remove emacs %{_bindir}/emacs-%{version}-nox


%post common
for f in %{info_files}; do
  /sbin/install-info %{_infodir}/$f %{_infodir}/dir 2> /dev/null || :
done
[ -L %{_bindir}/etags ] || rm -f %{_bindir}/etags
%{_sbindir}/update-alternatives --install %{_bindir}/etags emacs.etags %{_bindir}/etags.emacs 80 \
       --slave %{_mandir}/man1/etags.1.bz2 emacs.etags.man %{_mandir}/man1/etags.emacs.1.bz2

%preun common
if [ "$1" = 0 ]; then
  for f in %{info_files}; do
    /sbin/install-info --delete %{_infodir}/$f %{_infodir}/dir 2> /dev/null || :
  done
fi

%postun common
[ -e %{_bindir}/etags.emacs ] || \
  %{_sbindir}/update-alternatives --remove emacs.etags %{_bindir}/etags.emacs


%post terminal
update-desktop-database &> /dev/null || :

%postun terminal
update-desktop-database &> /dev/null || :

%files
%{_bindir}/emacs-%{version}
%attr(0755,-,-) %ghost %{_bindir}/emacs
%{_datadir}/applications/emacs.desktop
%{_datadir}/applications/emacsclient.desktop
%{_datadir}/icons/hicolor/*/apps/emacs.png
%{_datadir}/icons/hicolor/*/apps/emacs22.png
%{_datadir}/icons/hicolor/scalable/apps/emacs.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/emacs-document.svg

%files nox
%{_bindir}/emacs-%{version}-nox
%attr(0755,-,-) %ghost %{_bindir}/emacs

%files -f common-filelist common
%config(noreplace) %{_sysconfdir}/skel/.emacs
%config(noreplace) %{_sysconfdir}/rpm/macros.emacs
%doc doc/NEWS BUGS README doc/COPYING
%{_bindir}/ebrowse
%{_bindir}/emacsclient
%{_bindir}/etags.emacs
%{_bindir}/gctags
%{_bindir}/grep-changelog
%{_bindir}/rcs-checkin
%{_mandir}/*/*
%{_infodir}/*
%dir %{_datadir}/emacs/%{version}
%{_datadir}/emacs/%{version}/etc
%{_datadir}/emacs/%{version}/site-lisp
%{_libexecdir}/emacs
%attr(0644,root,root) %config(noreplace) %{_datadir}/emacs/site-lisp/default.el
%attr(0644,root,root) %config %{_datadir}/emacs/site-lisp/site-start.el

%files -f el-filelist el
%{pkgconfig}/emacs.pc
%doc etc/COPYING
#%dir %{_datadir}/emacs/%{version}

%files terminal
%{_bindir}/emacs-terminal
%{_datadir}/applications/emacs-terminal.desktop

%files filesystem
%dir %{_datadir}/emacs
%dir %{_datadir}/emacs/site-lisp
%dir %{_datadir}/emacs/site-lisp/site-start.d

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (24.2-7m)
- rebuild against ImageMagick-6.8.8.10

* Fri Jun 14 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (24.2-6m)
- fix up desktop files, good-bye "Lost & Found"

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.2-5m)
- rebuild against gnutls-3.2.0

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.2-4m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.2-3m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (24.2-2m)
- rebuild against ImageMagick-6.8.0.10

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.2-1m)
- update to 24.2
- fix alternatives support

* Tue Aug 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.1-4m)
- [SECURITY] CVE-2012-3479
- merge the following changes from fedora
-- remove php-mode
-- switch from GTK2 to GTK3

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.1-3m)
- rebuild for librsvg2 2.36.1

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (24.1-2m)
- update to 24.1
-- merge from T4R/emacs r59447

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (23.4-3m)
- Requires modefied. dev to udev

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (23.4-2m)
- rebuild against libtiff-4.0.1

* Thu Mar 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (23.4-1m)
- update 23.4

* Sat Jan 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.3-10m)
- [SECURITY] CVE-2012-0035

* Sun Jan  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (23.3-9m)
- modify emacs-erminal.desktop

* Mon Dec  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.3-8m)
- import some fixes from fedora's emacs-23.3-18
-- add support for subversion-1.7
-- update php-model.el
-- add a new command rpm-goto-add-change-log-entry (C-c C-w) to rpm-spec mode.
-- fix rhbz#711739
-- fix rhbz#713600

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (23.3-7m)
- update to 23.3b

* Sun Oct  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.3-6m)
- delete bundled ruby-model.el

* Fri Sep 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (23.3-5m)
- revise Source19: emacs-terminal.desktop to avoid entry in Lost & Found
- telepathy-kde-send-file.desktop in Lost & Found should be revised ASAP too

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.3-4m)
- revise obsoletes and provides tags

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.3-3m)
- add obsoletes and provides tags for elisp-php-mode
- add with_dbus option

* Wed Sep  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.3-2m)
- re-import from fedora's emacs-23.3-8.fc17
- update to 23.3a

* Sat May  7 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (23.3-1m)
- update to 23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.2.91-2m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (23.2.91-1m)
- update to 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.2-4m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (23.2-3m)
- add -lfontconfig to fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (23.2-2m)
- full rebuild for mo7 release

* Sat May  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.2-1m)
- update to 23.2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.1-7m)
- rebuild against libjpeg-8a

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.1-6m)
- s/BuildPreReq/BuildRequires/
- s/PreReq/Requires(post,postun)/

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.1-4m)
- add gcc442 patch

* Sun Oct 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (23.1-3m)
- update BuildPreReq: libtiff-devel >= 3.9.1-2m for rebuild against libjpeg

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (23.1-2m)
- rebuild against libjpeg-7

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (23.1-1m)
- update to 23.1

* Sat Jul 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (23.0.96-1m)
- update to 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.0.95-1m)
- update to 23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.0.94-1m)
- update to 23.0.94

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.0.93-1m)
- update to 23.0.93
-- now we can use -O2 because conding.c was fixed

* Tue Apr 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (23.0.92-3m)
- use -O1 instead of -O2 in optflags

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (23.0.92-2m)
- merge the following T4R changes
-
-- * Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.92-1m)
-- - update to 23.0.92
-- 
-- * Thu Mar 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.91-3m)
-- - support xz
-- 
-- * Tue Mar 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.91-2m)
-- - merge trunk changes
-- - * Tue Mar 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
-- - - (22.3-8m)
-- - - add %%{_datadir}/emacs/site-lisp/site-start.d
-- 
-- * Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.91-1m)
-- - update to 23.0.91
-- 
-- * Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.90-2m)
-- - add Provides: easypg-emacs
--       Obsoletes: easypg-emacs
-- 
-- * Tue Feb  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.90-1m)
-- - update to 23.0.90
-- 
-- * Wed Nov 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
-- - (23.0.60-0.20081119.1m)
-- - update to cvs snapshot
-- 
-- * Tue Jun 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
-- - (23.0.60-0.20080624.1m)
-- - update to cvs snapshot
-- - delete --with-gcc from configure
-- - add Requires: hicolor-icon-theme for %%{_datadir}/icons/hicolor/*/apps/*
-- - add %%{_datadir}/icons/hicolor/scalable/mimetypes/* into files
--   
-- * Sun Feb  3 2008 TABUCHI Takaaki <tab@momonga-linux.org>
-- - (23.0.60-0.20080203.1m)
-- - update to cvs snapshot
-- 
-- * Sat Jan  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.50-0.20080105.2m)
-- - correct installation of EmacsIconCollections
-- 
-- * Sat Jan  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.50-0.20080105.1m)
-- - update to cvs snapshot
-- - use EmacsIconCollections-3.0 (experimental packaging)
-- - merge a %%postun change on trunk into this
-- - * Sat Dec  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
-- - - (22.1-5m)
-- - - fixed alternatives
-- 
-- * Thu Nov 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.50-0.20071129.1m)
-- - update to cvs snapshot
-- - merge a %%postun change on trunk into this
-- - * Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
-- - - (22.1-4m)
-- - - revised alternatives
-- 
-- * Sun Sep  2 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (23.0.50-0.20070902.1m)
-- - update to 23.0.50 (include multi-tty support)
-- - switch license to GPLv3
-- - modify BuildPreReq (inaccurate...)

* Tue Mar 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.3-8m)
- add %%{_datadir}/emacs/site-lisp/site-start.d

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-7m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-6m)
- Requires: gzip bzip2 lzma
-- when open info files, these commands are needed
- cleanup info files installation

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-5m)
- revise for rpm46 (s|%%configure|./configure|)

* Tue Nov 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-4m)
- apply lzma support patch (Patch1)

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-3m)
- add specopt with_source to install c langauge files
  the default value is 0

* Wed Nov 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-2m)
- correct installation of info files

* Sun Sep  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.3-1m)
- [SECURITY] CVE-2008-3949
- update to 22.3

* Wed Aug 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (22.2-3m)
- del Requires: gtk+-common

* Mon Apr 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.2-2m)
- [SECURITY] CVE-2008-1694
- add Patch1: emacs-22.2-CVE-2008-1694.patch

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.2-1m)
- update to 22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.1-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (22.1-7m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (22.1-6m)
- rebuild against perl-5.10.0-1m

* Sat Dec  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.1-5m)
- fixed alternatives

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.1-4m)
- revised alternatives

* Mon Nov  5 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.1-3m)
- [SECURITY] CVE-2007-5795
- <http://secunia.com/advisories/27508/>
- add Patch1: emacs-22.1-CVE-2007-5795.patch

* Wed Jun 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.1-2m)
- add kludge hack "Provides: emacs-common" for package dependency

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.1-1m)
- update

* Sat May 19 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (22.0.96-2m)
- rebuild against glibc-2.6

* Mon May 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.990-1m)
- update

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.99-1m)
- update

* Tue Apr 17 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.98-1m)
- update

* Wed Apr  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.97-1m)
- update

* Fri Mar 23 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.96-1m)
- update

* Sun Mar 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.95-1m)
- update

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.94-1m)
- update
- No NoSource

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.93-0.20070126.1m)
- cvs update

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.92-0.20061221.1m)
- cvs update

* Wed Nov 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.91-0.20061122.1m)
- cvs update

* Wed Nov 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.91-0.20061121.1m)
- cvs update

* Wed Nov 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.90-0.20061114.1m)
- cvs update

* Sun Nov  5 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.0.90-0.20061028.2m)
- enable sound support explicitly.
-- add BuildPreReq and Requires tags for alsa-lib
-- add configure option: --with-sound
-- add specopt: with_sound
- fix alternatives

* Sat Oct 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.90-0.20061028.1m)
- cvs update

* Sat Oct 21 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20061021.1m)
- cvs update

* Sun Jul 10 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.0.50-0.20060313.3m)
- add %%{?include_specopt}
- ctags and etags are now handled by alternatives.

* Tue Jun 27 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.0.50-0.20060313.2m)
- add gnuserv-del-strerror.patch
- gnus come-back

* Tue Mar 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20060313.1m)
- cvs update

* Thu Jan 04 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20060103.1m)
- cvs update

* Thu Dec 22 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (22.0.50-0.20051222.1m)
- cvs update

* Fri Nov 18 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.200501118.1m)
- cvs update

* Sun Oct 16 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.200501015.1m)
- cvs update

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20050514.1m)
- cvs update
- add BuildRequires: cairo-devel

* Sun Mar 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20050319.1m)
- cvs update

* Thu Mar 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20050303.1m)
- cvs update

* Sat Feb 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.0.50-0.20050211.1m)
- update to 22.0.50

* Sat Jan 22 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (21.3.50-0.20041123.5m)
- set %%undefine _smp_mflags before first %%make

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (21.3.50-0.20041123.4m)
- import new emacs.desktop from Fedora Core

* Sun Dec  5 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (21.3.50-0.20041123.3m)
- add Requires: gtk+ gtk+-common atk for use
  libgtk-x11-2.0.so.0
  libatk-1.0.so.0
  libgdk_pixbuf-2.0.so.0

* Sun Dec  5 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (21.3.50-0.20041123.2m)
- modify %%build section for i686

* Tue Nov 23 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (21.3.50-0.20041123m)
- update source tarball.
  cvs check out at 2004-11-23

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (21.3.50-0.20041120m)
- update to cvs version.
  cvs check out at 2004-11-230

* Thu Jul 29 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (21.3-8m)
- remove dependency

* Wed Dec 24 2003 Kenta MURATA <muraken2@nifty.com>
- (21.3-7m)
- gnuserv 3.12.6.

* Tue Dec 23 2003 Kenta MURATA <muraken2@nifty.com>
- (21.3-6m)
- libexecdir.

* Tue Dec 23 2003 Kenta MURATA <muraken2@nifty.com>
- (21.3-5m)
- fix gnuserv build.

* Tue Dec 23 2003 Kenta MURATA <muraken2@nifty.com>
- (21.3-4m)
- includes gnuserv.
- fixes alternatives.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (21.3-3m)
- work around gcc(3.2.3-9m) internal compiler error

* Fri Oct  3 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (21.3-2m)
- adapt alternatives for b2m and rcs-checkin.

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (21.3-1m)
- version up to 21.3

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (21.2-9m)
- add URL tag
- use macros

* Tue Aug  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (21.2-8m)
- disabling etags symlinks

* Thu Aug  1 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.2-7m)
- Momonga

* Sat Apr 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.2-6k)
- rebuild against libpng-1.2.2

* Wed Mar 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.2-4k)
- remove 'Provides: emacs'

* Tue Mar 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.2-2k)
- emacs-21.2
- elisp-manual-21-2.8
- update I18N_menu.patch that is formerly called fontset.patch
- remove composite.patch that seems to have been merged

* Mon Feb 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (21.1-36k)
- never handle ctags and ctags.1

* Sat Feb  2 2002 Hidetomo Machi <mcHT@kondara.org>
- (21.1-34k)
- modify emacs.desktop (correct Icons file)

* Mon Jan 21 2002 Hidetomo Machi <mcHT@kondara.org>
- (21.1-32k)
- apply browse-url.el patch

* Sat Dec 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.1-30k)
- apply composite patch from bitmap-mule-8.5 (Patch5)

* Sun Dec 23 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.1-28k)
- %{manver} is not 21-2.7a but 21-2.7, and SOURCE2 is "%{manver}a"
- apply xim status patch (Patch4)

* Wed Dec 19 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.1-26k)
- remove dotemacs
- add dot.Xdefaults etc

* Tue Dec 11 2001 TABUCHI Takaaki <tab@kondara.org>
- (21.1-24k)
- add dotemacs-0.8.tar.gz

* Tue Dec 11 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.1-22k)
- add a config-sample

* Thu Nov 29 2001 Kazuhiko <kazuhiko@fdiary.net>
- (21.1-20k)
- add elisp-manual

* Sat Nov 10 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.1-18k)
- modify "--x-includes", is not /usr/include but /usr/X11R6/include

* Sat Nov 10 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.1-16k)
- add emacs-21.1-fontset.patch.gz

* Fri Nov  2 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.1-14k)
- remove /usr/share/emacs/etc (never use)

* Tue Oct 30 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (21.1-12k)
- modify %post and %pre section for install-info, because problem is occured by info.*

* Tue Oct 30 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.1-10k)
- instead of doing "cd src/ ;make ; cd ../elc ; make elc ; cd .. ;make "
  Just do "make bootstrap"

* Mon Oct 29 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.1-8k)
- add BuildPreReq:
- remove nodebug.patch
- remove loadup.patch

* Wed Oct 24 2001 Toru Hoshina <t@kondara.org>
- (21.1-6k)
- add shinpyousei zero patch for PPC.
- I'd like emacs guru to answer why PPC couldn't accept -z nocombreloc.

* Wed Oct 24 2001 Hidetomo Machi <mcHT@kondara.org>
- (21.1-4k)
- use macro in %files
- modify %BuildRoot tag
- change installed icon

* Mon Oct 22 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.1-2k)
- Update to 21.1

* Fri Sep 28 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.106-3k)
- Update to 21.0.106

* Mon Sep 03 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.105-3k)
- Update to 21.0.105
- modify kbdbuffer.patch for 21.0.105

* Mon Jul 16 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.104-3k)
- Update to 21.0.104

* Sun May 13 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.103-3k)
- Update to 21.0.103

* Wed Apr 16 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.102-3k)
- Update to 21.0.102
- modified nodebug.patch

* Wed Apr 04 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.101-3k)
- Update to 21.0.101

* Wed Mar 21 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.100-3k)
- Update to 21.0.100

* Sun Mar 11 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.99-3k)
- Update to 21.0.99

* Sun Feb 18 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.98-3k)
- Update to 21.0.98

* Mon Feb 05 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.97-3k)
- Update to 21.0.97

* Wed Jan 31 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.96-3k)
- Update to 21.0.96
- remove display.patch

* Sun Jan 14 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.95-5k)
- add nodebug.patch

* Thu Jan 11 2001 Toshiro HIKITA <toshi@sodan.org>
- (21.0.95-3k)
- Update to 21.0.95
- remove nodebug.patch
- remove tmprace.patch
- remove dired.patch

* Thu Dec 21 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.94-3k)
- revive nodebug.patch and remove info.patch, and buf.patch
- revised dired.patch.

* Fri Dec 14 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.0.93-3k)
- remove nodebug.patch

* Tue Dec 12 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.92-7k)
- add info patch.

* Wed Dec 06 2000 KIM Hyeong Cheol <kim@kondara.org>
- add buf patch
- add /usr/share/emacs/etc for icons or pixmaps.

* Mon Dec 04 2000 Toshiro HIKITA <toshi@sodan.org>
- (21.0.92-3k)
- fixed SOURCE file name.

* Fri Dec  1 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.92-1k)

* Fri Dec  1 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.91-13k)
- recompile all the lisp files for safety. (this may take 5-10min.)
- revive a patch for dired.el.

* Thu Nov 23 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.91-3k)
- remove gnus again
- apply some temporally patch
- resolve ccmode info files problem(%post)

* Sun Nov 19 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.90-3k)
- clean up spec files.

* Sat Nov 18 2000 KIM Hyeong Cheol <kim@kondara.org>
- (21.0.90-1k)
- many changes :-)
- get back gnus (temporally)

* Wed Nov 15 2000 Toshiro HIKITA <toshi@sodan.org>
- (20.7-17k)
- new patch (emacs-20.7-info.bz2.patch)
- add lisp/info.el for recompile

* Wed Oct 25 2000 Kenichi Matsubara <m@kondara.org>
- info.gz to info.bz2.

* Fri Oct 20 2000 Toshiro HIKITA <toshi@sodan.org>
- (20.7-13k)
- new patch (emacs-20.4-linespace-patch)
- do recompile elc in build section.
- remove all precompiled elc's
- comment out unused Sources:

* Fri Oct 20 2000 Hidetomo Machi <mcHT@kondara.org>
- (20.7-11k)
- new patch (emacs20-xim-20000713.diff)

* Thu Oct 19 2000 KIM Hyeong Cheol <hkimu-tky@umin.ac.jp>
- (20.7-9k)
- removed gnus. instead, we use t-gnus.
- use %{License} instead of %{Copyright}

* Thu Sep 14 2000 KIM Hyeong Cheol <hkimu-tky@umin.ac.jp>
- (20.7-7k)
- add dired.el binary
- using external file for emacs.desktop

* Thu Sep 14 2000 KIM Hyeong Cheol <hkimu-tky@umin.ac.jp>
- (20.7-5mule4.1)
- added emacs-20.7-dired.patch (resolves dired probrem under japanese locale)
- encode spec file with utf-8

* Tue Aug 15 2000 Toshiro HIKITA <toshi@sodan.org>
- Changed to Emacs/Mule-4.1
- added emacs-mule-4.1 patch
- recomplied ange-ftp.elc international/ccl.elc international/mule-cmds.elc \
             international/mule.elc international/titdic-cnv.elc
- removed emcws.patch
- removed Mule-UCS patch
- removed BuildPreReq:
- fixed configure

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Thu Jun 22 2000 TABUCHI Takaaki <tab@kondara.org>
- add extension-for-20_6.patch (Patch22) from Mule-UCS-0.80

* Wed Jun 21 2000 TABUCHI Takaaki <tab@kondara.org>
- version up to 20.7
- update emcws patch for 20.7
- merge from rawhide(20.6-16, 20.7-1) ia64 patch and etc. ,
  but I don't use RedHat's Packaging
  for each Japanese IM(Wnn{4,6},Canna)

* Tue Jun 13 2000 Trond Eivind Glomsrod <teg@redhat.com>
- Version 20.7
- Add requirement for final newline to the default .emacs
- redid the Xaw3d patch
- checked all patches, discarded those we've upstreamed

* Mon Jun 12 2000 Matt Wilson <msw@redhat.com>
- edit japanese patch not to patch configure
- fixed a missing escaped " in a wc string
- merge japanese support to head of development

* Wed Jun 07 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_mandir} and %%{_infodir}

* Fri Jun  2 2000 Bill Nottingham <notting@redhat.com>
- add yet another ia64 patch

* Mon May 22 2000 Bill Nottingham <notting@redhat.com>
- add another ia64 patch

* Fri May 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- Disabled the compile patch for 20.6

* Thu May 18 2000 Bill Nottingham <notting@redhat.com>
- add in ia64 patch

* Thu May 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- don't apply the unexelf patch - use a new unexelf.c file
  from the 21 source tree (this will go into the 20.7 tree)

* Wed May 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
- added patch by jakub to make it work with glibc2.2

* Mon May 08 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fixed a problem with ange-ftp and kerberized ftp

* Mon May 08 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild with new Xaw3d

* Thu Apr 20 2000 TABUCHI Takaaki <tab@kondara.org>
- add security patch.

* Thu Apr 20 2000 Trond Eivind Glomsrod <teg@redhat.com>
- let the build system handle gzipping man pages and stripping
- added patch to increase keyboard buffer size

* Thu Apr 20 2000 Trond Eivind Glomsrod <teg@redhat.com>
- gzip man pages

* Thu Apr 20 2000 Trond Eivind Glomsrod <teg@redhat.com>
- added a security patch from RUS-CERT, which fixes
  bugs mentioned in "Advisory 200004-01: GNU Emacs 20"

* Tue Apr 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- patched to detect bash2 scripts.

* Thu Apr 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- removed configuraton file status from /usr/share/pixmaps/emacs.png

* Sat Mar 11  2000 TABUCHI Takaaki <tab@kondara.org>
- merge kondara 20.5's fixes.

* Fri Mar 10  2000 Hidetomo Machi <mcHT@kondara.org>
- avoid to conflict with xemacs packages's files (look this spec)
- add emacs.spec and emacs-old.png(from gnome-core-1.0.55.tar.gz)
- remove "mule" from Provides tag
- modified reference of Emacs info

* Mon Feb 28 2000 TABUCHI Takaaki <tab@kondara.org>
- version up to 20.6

* Wed Feb 23 2000 Hidetomo Machi <mcHT@kondara.org>
- fix spec file

* Mon Feb 21 2000 Preston Brown <pbrown@redhat.com>
- add .emacs make the delete key work to delete forward character for X ver.

* Wed Feb 16 2000 Cristian Gafton <gafton@redhat.com>
- fix bug #2988
- recompile patched .el files (suggested by Pavel.Janik@linux.cz)
- prereq /sbin/install-info

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- wmconfig gone

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions and summary
- fix permissions for emacs niaries (what the hell does 1755 means for a
  binary?)
- added missing, as per emacs Changelog, NCURSES_OSPEED_T compilation
  flag; without it emacs on Linux is making global 'ospeed' short which
  is not the same as 'speed_t' expected by libraries. (reported by Michal
  Jaegermann <michal@harddata.com>)

* Sat Jan 29 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Mon Jan 10 2000 David S. Miller <davem@redhat.com>
- Revert src/unexecelf.c to 20.4 version, fixes SPARC problems.

* Tue Dec 27 1999 TABUCHI Takaaki <tab@kondara.org>
- add Canna-devel at BuildPreReq

* Sun Dec 19 1999 TABUCHI Takaaki <tab@kondara.org>
- upgrade 20.5a.
- Update cws patch (emcws-20.5-19991210.gz).

* Thu Dec  9 1999 TABUCHI Takaaki <tab@kondara.org>
- Use cws patch (emcws-20.5-19991208.gz).
- Add BuildPreReq (Canna,libwnn6,libwnn6-devel)
- Obsolete timezone.el{,c}

* Wed Dec  8 1999 TABUCHI Takaaki <tab@kondara.org>
- upgrade 20.5.
- add RPM_OPT_FLAGS.
- change BuildRoot.
- delete leim's version , use same version of emacs.

* Sun Dec  5 1999 Hidetomo Hosono <h@kondara.org>
- restart making filelist.

* Sun Dec  5 1999 Hidetomo Hosono <h@kondara.org>
- bug fix in %post phase.

* Sun Dec  5 1999 Hidetomo Hosono <h@kondara.org>
- add phase of renaming etags and b2m to getags and gb2m.

* Sun Dec  5 1999 Hidetomo Hosono <h@kondara.org>
- Change the category.
- Remove the unnesessary comment lines.
- re-format.
- using %defattr
- stop making filelist (I think filelist in this file is the easier way) .

* Sat Dec  4 1999 Hidetomo Hosono <h@kondara.org>
- Pachire Melos version, thanks for VineSeed Developers ;^) .
- Be a NoSrc :-P .

* Wed Dec  1 1999 MATSUBAYASHI 'Shaolin' Kohji <shaolin@rins.st.ryukoku.ac.jp>
- [emacs-20.4-14]
- Change Group to Applications/Editors/Emacs

* Sat Nov  6 1999 MATSUBAYASHI 'Shaolin' Kohji <shaolin@rins.st.ryukoku.ac.jp>
- [emacs-20.4-13]
- included Y2K savvy timezone.{el,elc},
  modified by Mr. Youichi Teranishi <teranishi@gohome.org>

* Tue Oct 12 1999 Yasuhide OOMORI <dasen@typhoon.co.jp>
- [emacs-20.4-12]
- Modified info-dir.
- Removed -c option from %setup.

* Sun Sep 20 1999 Kazuhisa TAKEI<takei@hh.iij4u.or.jp>
- fixed XIM support patch
* Sun Sep 19 1999 Kazuhisa TAKEI<takei@hh.iij4u.or.jp>
- fixed postinst bug and fontset patch
* Sun Jul 18 1999 Kazuhisa TAKEI<takei@hh.iij4u.or.jp>
- first build
