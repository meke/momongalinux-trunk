; ~/.emacs

;;;
;;; font / coding-system
;;;

(set-language-environment 'Japanese)
(set-default-coding-systems 'euc-jp-unix)
(set-terminal-coding-system 'euc-jp-unix)
(set-keyboard-coding-system 'euc-jp-unix)

;;;
;;; miscellaneous setting
;;;

; gnuserv
(autoload 'gnuserv-start "gnuserv-compat"
  "Allow this Emacs process to be a server for client processes."
  t)

; don't add a new line by moving a cursor
(setq next-line-add-newlines nil)

; locate a scroll bar on right
(set-scroll-bar-mode  'right)

; don't display a menu bar in non window system
(if window-system (menu-bar-mode 1) (menu-bar-mode 0))

; highlight a region
(transient-mark-mode t)

; wheel mouse please
(mouse-wheel-mode 1)
(setq mouse-wheel-follow-mouse t)

; no tool bar please
(tool-bar-mode 0)

; don't blink a cursor
(blink-cursor-mode 0)
(setq cursor-in-non-selected-windows nil)

; additional load-path
(setq load-path
      (append '("~/lisp/emacs")
	      load-path))

; set directory where the C source files of Emacs
(setq find-function-C-source-directory
      (let ((ver (if (string-match "\\.[0-9]+$" emacs-version)
		     (substring emacs-version 0 (match-beginning 0))
		   emacs-version)))
	(concat "/usr/share/emacs/" ver "/src")))
