%global momorel 6

Name: hyphen-pl
Summary: Polish hyphenation rules
%define upstreamid 20060726
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://pl.openoffice.org/pliki/hyph_pl_PL.zip
NoSource: 0
Group: Applications/Text
URL: http://wiki.services.openoffice.org/wiki/Dictionaries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+
BuildArch: noarch
Requires: hyphen

%description
Polish hyphenation rules.

%prep
%setup -q -c -n hyphen-pl

%build
unzip hyph_pl_PL.zip
tr -d '\r' < README_hyph_pl_PL.txt > README_hyph_pl_PL.txt.new
iconv -f ISO-8859-2 -t UTF-8 README_hyph_pl_PL.txt.new > README_hyph_pl_PL.txt

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p hyph_pl_PL.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_hyph_pl_PL.txt
%{_datadir}/hyphen/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20060726-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20060726-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20060726-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20060726-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20060726-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20060726-1m)
- import from Fedora

* Fri Nov 23 2007 Caolan McNamara <caolanm@redhat.com> - 0.20060726-1
- initial version
