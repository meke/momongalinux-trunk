%global momorel 5

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-enum
Version:        0.4.4
Release:        %{momorel}m%{?dist}
Summary:        Robust enumerated type support in Python

Group:          Development/Languages
License:        GPLv2+ or "Python"
URL:            http://cheeseshop.python.org/pypi/enum/
Source0:        http://pypi.python.org/packages/source/e/enum/enum-%{version}.tar.gz
NoSource:       0
Source1:        python-enum-README.txt
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel

%description
This package provides a module for robust enumerations in Python.
    
%prep
%setup -q -n enum-%{version}
cp -p %{SOURCE1} README

%build
# Remove CFLAGS=... for noarch packages (unneeded)
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

 
%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE.GPL LICENSE.PSF PKG-INFO README
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.3-1m)
- import from Fedora

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.4.3-4
- Rebuild for Python 2.6

* Thu Nov 22 2007 Matej Cepl <mcepl@redhat.com> 0.4.3-3
- fix %%changelog

* Thu Nov 22 2007 Matej Cepl <mcepl@redhat.com> 0.4.3-2
- reduce %%description and put the stuff into README file.

* Thu Nov 22 2007 Matej Cepl <mcepl@redhat.com> 0.4.3-1.fc9
- The initial attempt to package.

