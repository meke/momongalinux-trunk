%global         momorel 3

Name:           perl-Business-ISBN
Version:        2.07
Release:        %{momorel}m%{?dist}
Summary:        Work with International Standard Book Numbers
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Business-ISBN/
Source0:        http://www.cpan.org/authors/id/B/BD/BDFOY/Business-ISBN-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Business-ISBN-Data >= 20081208
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-URI
Requires:       perl-Business-ISBN-Data >= 20081208
Requires:       perl-Test-Simple
Requires:       perl-URI
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This modules handles International Standard Book Numbers, including ISBN-10
and ISBN-13.

%prep
%setup -q -n Business-ISBN-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc bad-isbn13s.txt bad-isbns.txt Changes isbn13s.txt isbns.txt LICENSE META.json MYMETA.json MYMETA.yml README
%{perl_vendorlib}/Business/ISBN.pm
%{perl_vendorlib}/Business/ISBN10.pm
%{perl_vendorlib}/Business/ISBN13.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-2m)
- rebuild against perl-5.18.2

* Sat Jan  4 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-2m)
- rebuild against perl-5.18.1

* Sun Jun 02 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-1m)
- update to 2.06

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.05-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.05-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.05-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.05-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-3m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-1m)
- update to 2.05

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-2m)
- rebuild against rpm-4.6

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.03-2m)
- rebuild against gcc43

* Tue Oct  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03
- do not use %%NoSource macro

* Thu Aug 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Tue Aug 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.84-2m)
- use vendor

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.84-1m)
- update to 1.84

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.82-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.82-1m)
- update to 1.82

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.80-1m)
- version up to 1.80
- rebuilt against perl-5.8.7

* Sat Dec 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.79-2m)
- add 'BuildRequires: perl-Business-ISBN-Data >= 1.09'

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.79-1m)
- update to 1.79

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.72-3m)
- rebuild against perl-5.8.5
- rebuild against  perl-File-Find-Rule >= 0.24-5m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.72-2m)
- remove Epoch from BuildRequires

* Mon May 17 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.72-1m)
- version up

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (1.70-5m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-4m)
- rebuild against perl-5.8.2

* Sun Nov 02 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-3m)
- delete 'make test' for obsolete perl-Test-Pod package
- delete 'BuildRequires: perl-Test-Pod >= 0.72'

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.70-3m)
- rebuild against perl-5.8.1

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.70-2m)
- rebuild against perl-5.8.0

* Thu Nov 7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.70-1m)
- spec file was semi-autogenerated
