%global momorel 2

Summary: An Open Source implementation of the GDI+ API
Name: mono-addins
Version: 0.6.2
Release: %{momorel}m%{?dist}
URL: http://www.mono-project.com/Mono.Addins
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
License: GPL
Group: System Environment/Libraries
Patch1: mono-addins-0.5-gacutil.patch
BuildRequires: mono-core
BuildRequires: gtk-sharp2-devel >= 2.12.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Mono.Addins is a generic framework for creating extensible applications,
and for creating libraries which extend those applications.

%package devel
Summary: mono-addins-devel
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
mono-addins-devel

%prep
%setup -q
%patch1 -p1 -b .gacutil

%build
autoreconf -vfi
%configure \
    --enable-gui
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} libdir=%{_prefix}/lib pkgconfigdir=%{_libdir}/pkgconfig install
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README
%{_bindir}/mautil
%{_prefix}/lib/mono/gac/Mono.Addins.CecilReflector
%{_prefix}/lib/mono/gac/Mono.Addins.Gui
%{_prefix}/lib/mono/gac/Mono.Addins.MSBuild
%{_prefix}/lib/mono/gac/Mono.Addins.Setup
%{_prefix}/lib/mono/gac/Mono.Addins

%{_prefix}/lib/mono/gac/policy.0.2.Mono.Addins
%{_prefix}/lib/mono/gac/policy.0.2.Mono.Addins.CecilReflector
%{_prefix}/lib/mono/gac/policy.0.2.Mono.Addins.Gui
%{_prefix}/lib/mono/gac/policy.0.2.Mono.Addins.MSBuild
%{_prefix}/lib/mono/gac/policy.0.2.Mono.Addins.Setup
%{_prefix}/lib/mono/gac/policy.0.3.Mono.Addins
%{_prefix}/lib/mono/gac/policy.0.3.Mono.Addins.CecilReflector
%{_prefix}/lib/mono/gac/policy.0.3.Mono.Addins.Gui
%{_prefix}/lib/mono/gac/policy.0.3.Mono.Addins.MSBuild
%{_prefix}/lib/mono/gac/policy.0.3.Mono.Addins.Setup
%{_prefix}/lib/mono/gac/policy.0.4.Mono.Addins
%{_prefix}/lib/mono/gac/policy.0.4.Mono.Addins.CecilReflector
%{_prefix}/lib/mono/gac/policy.0.4.Mono.Addins.Gui
%{_prefix}/lib/mono/gac/policy.0.4.Mono.Addins.MSBuild
%{_prefix}/lib/mono/gac/policy.0.4.Mono.Addins.Setup
%{_prefix}/lib/mono/gac/policy.0.5.Mono.Addins
%{_prefix}/lib/mono/gac/policy.0.5.Mono.Addins.CecilReflector
%{_prefix}/lib/mono/gac/policy.0.5.Mono.Addins.Gui
%{_prefix}/lib/mono/gac/policy.0.5.Mono.Addins.MSBuild
%{_prefix}/lib/mono/gac/policy.0.5.Mono.Addins.Setup

%{_prefix}/lib/mono/%{name}
%{_prefix}/lib/mono/xbuild/Mono.Addins.targets
%{_mandir}/man1/mautil.1.*

%files devel
%{_libdir}/pkgconfig/mono-addins-gui.pc
%{_libdir}/pkgconfig/mono-addins-setup.pc
%{_libdir}/pkgconfig/mono-addins.pc
%{_libdir}/pkgconfig/mono-addins-msbuild.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-2m)
- rebuild for mono-2.10.9

* Fri Sep 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-4m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-3m)
- modify %%files

* Fri Jul 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- fix build on x86_64

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jan 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-1m)
- add patch1 (for gacutils)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-1m)
- update to 0.4

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-4m)
- good-bye debug symbol

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-3m)
- rebuild against gcc43

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-2m)
- rebuild against gtk-sharp2-2.12.0

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Sat Dec 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3-2m)
- add pkgconfig patch for lib64

* Thu Dec 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-1m)
- initial build

