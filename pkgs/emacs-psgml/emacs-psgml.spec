%global momorel 31
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: A major mode for editing SGML documents
Name: emacs-psgml
Version: 1.3.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://www.lysator.liu.se/~lenst/about_psgml/psgml-%{version}.tar.gz
NoSource: 0
URL: http://www.lysator.liu.se/projects/about_psgml.html
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: psgml-emacs

Obsoletes: elisp-psgml
Provides: elisp-psgml

%description
PSGML is a major mode for editing SGML and XML documents.  It works
with GNU Emacs 19.34, 20.3 and later or with XEmacs 19.9 and later.
PSGML contains a simple SGML parser and can work with any DTD.
Functions provided includes menus and commands for inserting tags with
only the contextually valid tags, identification of structural errors,
editing of attribute values in a separate window with information about
types and defaults, and structure based editing.

%prep
%setup -q -n psgml-%{version}

%build
./configure --prefix=%{_prefix}
make EMACS=emacs

%install
%{__rm} -rf %{buildroot}
make lispdir=%{buildroot}%{e_sitedir}/psgml install
%{__install} -m 644 *.el %{buildroot}%{e_sitedir}/psgml

mkdir -p %{buildroot}%{_infodir}
%{__install} -m 644 *.info %{buildroot}%{_infodir}

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/psgml.info %{_infodir}/dir
/sbin/install-info %{_infodir}/psgml-api.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/psgml.info %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/psgml-api.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc ChangeLog INSTALL README.psgml psgml.ps
%{_datadir}/emacs/site-lisp/psgml
%{_infodir}/*.info*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-31m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-30m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.2-29m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-28m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-27m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-26m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-25m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-24m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-23m)
- merge psgml-emacs to elisp-psgml

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-21m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-20m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-19m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-18m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-17m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-16m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-15m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-14m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-13m)
- rebuild against emacs-22.3

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-12m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-11m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-10m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-2m)
- rebuild against emacs-22.0.90

* Wed Jun 15 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.2-1m)
- ver. up and remove patches

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.1-6m)
- revised docdir permission

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-5m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.1-4m)
- rebuild against emacs-21.3.50

* Thu Nov 20 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.1-3m)
- add psgml-xpointer.el
- add patch1.
  - sgml-make-character-reference did not work
     using together with Mule-UCS.
    (encode-char/decode-char were obsolete.)
  - add function (sgml-unmake-character-reference)

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-2m)
- rebuild against emacs-21.3
- update apel version

* Wed Feb 12 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.1-1m)
- update to 1.3.1. (this is alpha version)

* Mon May  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.5-2k)
- version up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.4-4k)
- /sbin/install-info -> info in PreReq.

* Sat Jan 12 2002 Hidetomo Machi <mcHT@kondara.org>
- (1.2.4-2k)
- update version 1.2.4

* Sun Nov 11 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.2.3-2k)
- update 1.2.3 (fix for Emacs21)
- include multibyte patch but not apply ;p

* Fri Nov  9 2001 Hidetomo Machi <mcHT@kondara.org>
- (elisp-psgml-1.2.2-4k)
- new package

* Mon Apr  2 2001 Kenta MURATA <muraken2@nifty.com>
- (1.2.2-3k)
- version up to 1.2.2

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1-9k)
- modified spec file and errased info file from %{name}.files

* Sat Nov 25 2000 KIM Hyeong Cheol <kim@kondara.org>
- (1.2.1-7k)
- fixed for bzip2ed info files.

* Thu Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1-5k)
- fixed for bzip2ed man

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (1.2.1-3k)
- modify specfile (License, Group, BuildPreReq, PreReq)

* Mon Jun 12 2000 SAKA Toshihide <saka@yugen.org>
- First release.
