%global momorel 1

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")
%global pkgpythondir  %{python_sitelib}/%{name}

%global        libauditver     2.3
%global        libsepolver     2.3
%global        libsemanagever  2.3
%global        libselinuxver   2.3
%global        sepolgenver     1.2.1

Summary: SELinux policy core utilities
Name:	 policycoreutils
Version: 2.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group:	 System Environment/Base
URL:	 http://www.selinuxproject.org
#Source:  git://oss.tresys.com/git/selinux/policycoreutils-%{version}.tgz
#Source1: git://oss.tresys.com/git/selinux/sepolgen-%{sepolgenver}.tgz
Source0: http://userspace.selinuxproject.org/releases/20140506/policycoreutils-%{version}.tar.gz
Source1: http://userspace.selinuxproject.org/releases/20140506/sepolgen-%{sepolgenver}.tar.gz
NoSource: 0
NoSource: 1
Source2: policycoreutils_man_ru2.tar.bz2
Source3: system-config-selinux.png
Source4: sepolicy-icons.tgz
Patch:	 policycoreutils-rhat.patch
Patch1:  0001-Fix-setfiles-to-work-correctly-if-r-option-is-define.patch
Obsoletes: policycoreutils < 2.0.61-2
Conflicts: filesystem < 3
Provides: /sbin/fixfiles
Provides: /sbin/restorecon

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  pam-devel libcgroup-devel libsepol-static >= %{libsepolver} libsemanage-static >= %{libsemanagever} libselinux-devel >= %{libselinuxver}  libcap-devel audit-libs-devel >=  %{libauditver} gettext
BuildRequires: desktop-file-utils dbus-devel dbus-glib-devel
BuildRequires: python-devel setools-devel >= 3.3.7-10m
Requires:       /bin/mount /bin/egrep /bin/awk /usr/bin/diff rpm /bin/sed
BuildRequires:  systemd-units
Requires: libsepol >= %{libsepolver} coreutils libselinux-utils >=  %{libselinuxver}
Requires(post): systemd-units systemd-sysv 
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
Security-enhanced Linux is a feature of the Linux® kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement®, Role-based Access
Control, and Multi-level Security.

policycoreutils contains the policy core utilities that are required
for basic operation of a SELinux system.  These utilities include
load_policy to load policies, setfiles to label filesystems, newrole
to switch roles.

%prep
%setup -q -a 1
%patch -p2 -b .rhat
%patch1 -p2 -b .setfiles

cp %{SOURCE3} gui/
tar xvf %{SOURCE4}

%build
make LSPP_PRIV=y SBINDIR="%{_sbindir}" LIBDIR="%{_libdir}" CFLAGS="%{optflags} -fPIE" LDFLAGS="-pie -Wl,-z,relro -Wl,-z,now" SEMODULE_PATH="/usr/sbin" all
make -C sepolgen-%{sepolgenver} SBINDIR="%{_sbindir}" LSPP_PRIV=y LIBDIR="%{_libdir}" CFLAGS="%{optflags} -fPIE" LDFLAGS="-pie -Wl,-z,relro" all

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/var/lib/selinux
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/man5
mkdir -p %{buildroot}%{_mandir}/man8
%{__mkdir} -p %{buildroot}/%{_usr}/share/doc/%{name}/
cp COPYING %{buildroot}/%{_usr}/share/doc/%{name}/

make LSPP_PRIV=y  DESTDIR="%{buildroot}" SBINDIR="%{buildroot}%{_sbindir}" LIBDIR="%{buildroot}%{_libdir}" SEMODULE_PATH="/usr/sbin" install
make PYTHON=python3 LSPP_PRIV=y  DESTDIR="%{buildroot}" SBINDIR="%{buildroot}%{_sbindir}" LIBDIR="%{buildroot}%{_libdir}" SEMODULE_PATH="/usr/sbin" CFLAGS="%{optflags} -fPIE -Wno-error=declaration-after-statement" install

# Systemd
rm -rf %{buildroot}/%{_sysconfdir}/rc.d/init.d/restorecond

make -C sepolgen-%{sepolgenver} DESTDIR="%{buildroot}" SBINDIR="%{buildroot}%{_sbindir}" LIBDIR="%{buildroot}%{_libdir}" install
make -C sepolgen-%{sepolgenver} DESTDIR="%{buildroot}" SBINDIR="%{buildroot}%{_sbindir}" LIBDIR="%{buildroot}%{_libdir}" install

tar -jxf %{SOURCE2} -C %{buildroot}/
rm -f %{buildroot}/usr/share/man/ru/man8/genhomedircon.8.*
rm -f %{buildroot}/usr/share/man/ru/man8/open_init_pty.8.*
rm -f %{buildroot}/usr/share/man/man8/open_init_pty.8
rm -f %{buildroot}/usr/sbin/open_init_pty
rm -f %{buildroot}/usr/sbin/run_init
rm -f %{buildroot}/usr/share/man/ru/man8/run_init.8*
rm -f %{buildroot}/usr/share/man/man8/run_init.8*
rm -f %{buildroot}/etc/pam.d/run_init*

ln -sf /usr/share/system-config-selinux/polgengui.py %{buildroot}%{_bindir}/selinux-polgengui

desktop-file-install --dir ${RPM_BUILD_ROOT}%{_datadir}/applications	\
			--add-category Settings				\
%{buildroot}%{_datadir}/system-config-selinux/system-config-selinux.desktop

desktop-file-install --dir ${RPM_BUILD_ROOT}%{_datadir}/applications	\
			--add-category Settings				\
    %{buildroot}%{_datadir}/system-config-selinux/sepolicy.desktop

desktop-file-install --dir ${RPM_BUILD_ROOT}%{_datadir}/applications	\
%{buildroot}%{_datadir}/system-config-selinux/selinux-polgengui.desktop

rm -f %{buildroot}%{_datadir}/system-config-selinux/selinux-polgengui.desktop
rm -f %{buildroot}%{_datadir}/system-config-selinux/sepolicy.desktop
rm -f %{buildroot}%{_datadir}/system-config-selinux/system-config-selinux.desktop

%find_lang %{name}

%package python3
Summary: SELinux policy core python3 interfaces
Group:	 System Environment/Base
Requires:policycoreutils = %{version}-%{release}
Requires:libsemanage-python3 >= %{libsemanagever} libselinux-python3 libcgroup
Requires:audit-libs-python >=  %{libauditver}

%description python3
The policycoreutils-python3 package contains the interfaces that can be used
by python 3 in an SELinux environment.

%files python3
%dir %{python3_sitelib}/seobject
%{python3_sitelib}/seobject/__init__.py*
%{python3_sitelib}/seobject/__pycache__/*
%{python3_sitelib}/seobject*.egg-info
%dir %{python3_sitearch}/sepolicy
%{python3_sitearch}/sepolicy/*so
%{python3_sitearch}/sepolicy/templates
%dir %{python3_sitearch}/sepolicy/help
%{python3_sitearch}/sepolicy/help/*
%{python3_sitearch}/sepolicy/__init__.py*
%{python3_sitearch}/sepolicy/booleans.py*
%{python3_sitearch}/sepolicy/communicate.py*
%{python3_sitearch}/sepolicy/interface.py*
%{python3_sitearch}/sepolicy/manpage.py*
%{python3_sitearch}/sepolicy/network.py*
%{python3_sitearch}/sepolicy/transition.py*
%{python3_sitearch}/sepolicy/sedbus.py*
%{python3_sitearch}/sepolicy*.egg-info
%{python3_sitearch}/sepolicy/__pycache__/*

%package python
Summary: SELinux policy core python utilities
Group:	 System Environment/Base
Requires:policycoreutils = %{version}-%{release} 
Requires:libsemanage-python >= %{libsemanagever} libselinux-python libcgroup
Requires:audit-libs-python >=  %{libauditver} 
Requires(pre): python >= 2.6
Obsoletes: policycoreutils < 2.0.61-2
Requires: python-IPy yum

%description python
The policycoreutils-python package contains the management tools use to manage an SELinux environment.

%files python
%defattr(-,root,root,-)
%{_sbindir}/semanage
%{_bindir}/chcat
%{_bindir}/sandbox
%{_bindir}/audit2allow
%{_bindir}/audit2why
%{_mandir}/man1/audit2allow.1*
%{_mandir}/ru/man1/audit2allow.1*
%{_bindir}/semodule_package
%{_mandir}/man8/semodule_package.8*
%{_mandir}/ru/man8/semodule_package.8*
%{_mandir}/man1/audit2why.1*
%dir %{python_sitelib}/seobject
%{python_sitelib}/seobject/__init__.py*
%{python_sitelib}/seobject*.egg-info
%{python_sitearch}/sepolgen
%dir %{python_sitearch}/sepolicy
%{python_sitearch}/sepolicy/*so
%{python_sitearch}/sepolicy/templates
%{python_sitearch}/sepolicy/__init__.py*
%{python_sitearch}/sepolicy/booleans.py*
%{python_sitearch}/sepolicy/communicate.py*
%{python_sitearch}/sepolicy/interface.py*
%{python_sitearch}/sepolicy/manpage.py*
%{python_sitearch}/sepolicy/network.py*
%{python_sitearch}/sepolicy/transition.py*
%{python_sitearch}/sepolicy/sedbus.py*
%{_sysconfdir}/dbus-1/system.d/org.selinux.conf
%{python_sitearch}/%{name}*.egg-info
%{python_sitearch}/sepolicy*.egg-info
%{python_sitearch}/%{name}
%dir  /var/lib/selinux
%{_mandir}/man8/chcat.8*
%{_mandir}/ru/man8/chcat.8*
%{_mandir}/man8/sandbox.8*
%{_mandir}/man8/semanage*.8*
%{_mandir}/ru/man8/semanage.8*
%{_datadir}/bash-completion/completions/semanage
%{_datadir}/bash-completion/completions/setsebool

%package devel
Summary: SELinux policy core policy devel utilities
Group:	 System Environment/Base
Requires: policycoreutils-python = %{version}-%{release} 
Requires: /usr/bin/make
Requires: checkpolicy
Requires: selinux-policy-devel selinux-policy-doc

%description devel
The policycoreutils-devel package contains the management tools use to develop policy in an SELinux environment.

%files devel
%{_bindir}/sepolgen
%{_bindir}/sepolgen-ifgen
%{_bindir}/sepolgen-ifgen-attr-helper
%dir  /var/lib/sepolgen
/var/lib/sepolgen/perm_map
%{_bindir}/sepolicy
%{python_sitearch}/sepolicy/generate.py*
%{python3_sitearch}/sepolicy/generate.py*
%{_mandir}/man8/sepolgen.8*
%{_mandir}/man8/sepolicy-booleans.8*
%{_mandir}/man8/sepolicy-generate.8*
%{_mandir}/man8/sepolicy-interface.8*
%{_mandir}/man8/sepolicy-network.8*
%{_mandir}/man8/sepolicy.8*
%{_mandir}/man8/sepolicy-communicate.8*
%{_mandir}/man8/sepolicy-manpage.8*
%{_mandir}/man8/sepolicy-transition.8*
%{_usr}/share/bash-completion/completions/sepolicy
%{_bindir}/semodule_deps
%{_bindir}/semodule_expand
%{_bindir}/semodule_link
%{_bindir}/semodule_unpackage
%{_mandir}/man8/semodule_deps.8*

%post devel
selinuxenabled && [ -f /usr/share/selinux/devel/include/build.conf ] && /usr/bin/sepolgen-ifgen 2>/dev/null 
exit 0

%triggerin devel -- selinux-policy-devel
selinuxenabled && [ -f /usr/share/selinux/devel/include/build.conf ] && /usr/bin/sepolgen-ifgen 2>/dev/null
exit 0

%package sandbox
Summary: SELinux sandbox utilities
Group:	 System Environment/Base
Requires: policycoreutils-python = %{version}-%{release} 
Requires: xorg-x11-server-Xephyr /usr/bin/rsync /usr/bin/xmodmap
Requires: openbox
BuildRequires: libcap-ng-devel

%description sandbox
The policycoreutils-sandbox package contains the scripts to create graphical sandboxes

%files sandbox
%config(noreplace) %{_sysconfdir}/sysconfig/sandbox
%{_datadir}/sandbox/sandboxX.sh
%{_datadir}/sandbox/start
%caps(cap_setpcap,cap_setuid,cap_fowner,cap_dac_override,cap_sys_admin,cap_sys_nice=pe) %{_sbindir}/seunshare
%{_mandir}/man8/seunshare.8*
%{_mandir}/man5/sandbox.5*

%package newrole
Summary: The newrole application for RBAC/MLS
Group: System Environment/Base
Requires: policycoreutils = %{version}-%{release}

%description newrole
RBAC/MLS policy machines require newrole as a way of changing the role
or level of a logged in user.

%files newrole
%defattr(-,root,root)
%attr(0755,root,root) %caps(cap_dac_read_search,cap_setpcap,cap_audit_write,cap_sys_admin,cap_fowner,cap_chown,cap_dac_override=pe) %{_bindir}/newrole
%{_mandir}/man1/newrole.1.*
%config(noreplace) %{_sysconfdir}/pam.d/newrole

%package gui
Summary: SELinux configuration GUI
Group: System Environment/Base
Requires: policycoreutils-devel = %{version}-%{release} 
Requires: gnome-python2-gnome, pygtk2, pygtk2-libglade, gnome-python2-canvas
Requires: usermode-gtk
Requires: python >= 2.6
BuildRequires: desktop-file-utils

%description gui
system-config-selinux is a utility for managing the SELinux environment

%files gui
%{_bindir}/system-config-selinux
%{_bindir}/selinux-polgengui
%{_datadir}/applications/sepolicy.desktop
%{_datadir}/applications/system-config-selinux.desktop
%{_datadir}/applications/selinux-polgengui.desktop
%{_datadir}/icons/hicolor/24x24/apps/system-config-selinux.png
%{_datadir}/pixmaps/system-config-selinux.png
%dir %{_datadir}/system-config-selinux
%{_datadir}/system-config-selinux/system-config-selinux.png
%{_datadir}/system-config-selinux/*.py*
%{_datadir}/system-config-selinux/*.glade
%{python_sitearch}/sepolicy/gui.py*
%{python_sitearch}/sepolicy/sepolicy.glade
%dir %{python_sitearch}/sepolicy/help
%{python_sitearch}/sepolicy/help/*
%{python3_sitearch}/sepolicy/gui.py*
%{python3_sitearch}/sepolicy/sepolicy.glade
%{_datadir}/icons/hicolor/*/apps/sepolicy.png
%{_datadir}/pixmaps/sepolicy.png
%{_mandir}/man8/system-config-selinux.8*
%{_mandir}/man8/selinux-polgengui.8*
%{_mandir}/man8/sepolicy-gui.8*
%{_datadir}/system-config-selinux/selinux_server.py
%{_datadir}/dbus-1/system-services/org.selinux.service
%{_datadir}/polkit-1/actions/org.selinux.policy
%{_datadir}/polkit-1/actions/org.selinux.config.policy

%files -f %{name}.lang
%{_sbindir}/restorecon
%{_sbindir}/fixfiles
%{_sbindir}/setfiles
%{_sbindir}/load_policy
%{_sbindir}/genhomedircon
%{_sbindir}/setsebool
%{_sbindir}/semodule
%{_sbindir}/sestatus
%{_bindir}/secon
%{_bindir}/semodule_deps
%{_bindir}/semodule_expand
%{_bindir}/semodule_link
%{_bindir}/semodule_package
%{_bindir}/semodule_unpackage
%config(noreplace) %{_sysconfdir}/sestatus.conf
# selinux-policy Requires: policycoreutils, so we own this set of directories and our files within them
%{_mandir}/man5/selinux_config.5.*
%{_mandir}/man5/sestatus.conf.5.*
%{_mandir}/man8/fixfiles.8*
%{_mandir}/ru/man8/fixfiles.8*
%{_mandir}/man8/load_policy.8*
%{_mandir}/ru/man8/load_policy.8*
%{_mandir}/man8/restorecon.8*
%{_mandir}/ru/man8/restorecon.8*
%{_mandir}/man8/semodule.8*
%{_mandir}/ru/man8/semodule.8*
%{_mandir}/man8/semodule_deps.8*
%{_mandir}/ru/man8/semodule_deps.8*
%{_mandir}/man8/semodule_expand.8*
%{_mandir}/ru/man8/semodule_expand.8*
%{_mandir}/man8/semodule_link.8*
%{_mandir}/ru/man8/semodule_link.8*
%{_mandir}/man8/semodule_package.8*
%{_mandir}/ru/man8/semodule_package.8*
%{_mandir}/man8/semodule_unpackage.8*
%{_mandir}/man8/sestatus.8*
%{_mandir}/ru/man8/sestatus.8*
%{_mandir}/man8/setfiles.8*
%{_mandir}/ru/man8/setfiles.8*
%{_mandir}/man8/setsebool.8*
%{_mandir}/ru/man8/setsebool.8*
%{_mandir}/man1/secon.1*
%{_mandir}/ru/man1/secon.1*
%{_mandir}/man8/genhomedircon.8*
%doc %{_usr}/share/doc/%{name}

%package restorecond
Summary: SELinux restorecond utilities
Group:	 System Environment/Base

%description restorecond
The policycoreutils-restorecond package contains the restorecond service.

%files restorecond
%defattr(-,root,root,-)
%{_sbindir}/restorecond
%{_unitdir}/restorecond.service
%config(noreplace) %{_sysconfdir}/selinux/restorecond.conf
%config(noreplace) %{_sysconfdir}/selinux/restorecond_user.conf
%{_sysconfdir}/xdg/autostart/restorecond.desktop
%{_datadir}/dbus-1/services/org.selinux.Restorecond.service
%{_mandir}/man8/restorecond.8*
%{_mandir}/ru/man8/restorecond.8*

%post restorecond
%systemd_post restorecond.service

%preun restorecond
%systemd_preun restorecond.service

%postun restorecond
%systemd_postun_with_restart restorecond.service

%triggerun -- restorecond < 2.0.86-3m
%{_bindir}/systemd-sysv-convert --save restorecond >/dev/null 2>&1 ||:
%{_bindir}/systemctl enable restorecond.service >/dev/null 2>&1
%{_sbindir}/chkconfig --del restorecond >/dev/null 2>&1 || :
%{_bindir}/systemctl try-restart restorecond.service >/dev/null 2>&1 || :

%changelog
* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- update 2.3

* Thu Mar 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.14-3m)
- delete "fix me later" workaround to fix build failure

* Wed Jan 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.14-2m)
- rm /lib/systemd/system/restorecond.service

* Mon Mar 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.14-1m)
- update to 2.1.14
- restorecond.service is temporarily in both  /usr/lib/systemd/ 
  and /lib/systemd
- commented out Conflicts line for now

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.4-6m)
- add BuildRequires

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-5m)
- update patches

* Fri Dec  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-4m)
- update patches

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-3m)
- update sepolgen
- update patches

* Mon Nov 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.4-2m)
- modify spec

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-1m)
- update 2.1.4

* Sun Sep 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.86-4m)
- modify Requires

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.86-3m)
- support systemd

* Thu Jun 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.86-2m)
- modify Requires

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.86-1m)
- update 2.0.86

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.82-8m)
- add patch for glibc-2.14

* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.82-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.82-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.82-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.82-4m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.82-3m)
- stop auto starting service (sandbox) at initial system startup

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.82-2m)
- fix %%files to avoid conflicting

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.82-1m)
- update 2.0.82

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.62-4m)
- rebuild against audit-2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.62-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.62-2m)
- import Fedora 11 patches (2.0.62-12.11)

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.62-1m)
- update to 2.0.62 based on Fedora 11 (2.0.62-12.9)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.52-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.52-3m)
- rebuild against python-2.6.1-1m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.52-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Fri Jul 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.52-1m)
- update 2.0.52

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.49-1m)
- update 2.0.49

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.47-1m)
- update 2.0.47

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.44-3m)
- remove vendor="fedora" from desktop files

* Wed Apr 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.44-2m)
- modify %%files to avoid conflicting
- use %%{_initscriptdir} instead of %%{_initrddir}
- add %%defattr(-,root,root)
- modify Requires

* Wed Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.44-1m)
- version up 2.0.44
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.22-3m)
- rebuild against gcc43

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.22-3m)
- add Requires: chkconfig

* Sat Jul 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.22-2m)
- add category System

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.22-1m)
- update 2.0.22

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.16-1m)
- update 2.0.16

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.9-1m)
- update 2.0.9

* Fri Mar  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-2m)
- modify %%build, %%install and %%files

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-1m)
- update 2.0.7

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.33.6-3m)
- delete pyc pyo

* Wed Jan 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.33.6-2m)
- revise system-config-selinux.desktop

* Wed Dec 27 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.33.6-1m)
- update 1.33.6

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30.1-3m)
- rebuild against python-2.5

* Thu May 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.30.1-2m)
- /etc/rc.d/init.d/restorecond -> /etc/init.d/restorecond

* Mon May 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.30.1-1m)
- update 1.30.1

* Sat Dec 31 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.23.10-2m)
- enable x86_64.

* Fri Dec 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.23.10-1m)
- version 1.23.10
- add libsepol.patch

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
-  (1.18.1-3m)
- rebuild against libselinux-1.26

* Mon Mar  7 2005 TAKAHASHI Tamotsu <tamo>
- (1.18.1-2m)
- sync with FC3
 + * Tue Feb 8 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.9
 + - Fix restorecon segfault on unlabeled file systems
 + 
 + * Fri Jan 27 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.8
 + - Back port of fixfiles diff_filecontext 
 + - Back port of restorecon -e switch
 + 
 + * Thu Jan 27 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.7
 + - Fix genhomedircon selection of STARTING_UID
 + 
 + * Mon Jan 24 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.6
 + - Merge upstream changes for fixfiles,restorecon and setfiles
 + 
 + * Wed Jan 12 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.5
 + - Fix another segfault in restorecon if no context is defined
 + 
 + * Mon Jan 10 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.4
 + - Fix segfault in restorecon if no context is defined
 + 
 + * Mon Jan 3 2005 Dan Walsh <dwalsh@redhat.com> 1.18.1-2.2
 + - backport restorecon and fixfiles from rawhide. to eliminate bad warning 
 + - messages and fix handling of rpm files

* Mon Nov 29 2004 TAKAHASHI Tamotsu <tamo>
- (1.18.1-1m)
- merge Fedora 1.18.1-3
 + * Thu Nov 18 2004 Dan Walsh <dwalsh@redhat.com> 1.18.1-3
 + - Fix run_init.8 to refer to correct location of initrc_context
 + 
 + * Wed Nov 3 2004 Dan Walsh <dwalsh@redhat.com> 1.18.1-1
 + - Upgrade to latest from NSA

* Sat Nov 20 2004 TAKAHASHI Tamotsu <tamo>
- (1.18-1m)
- import
- Note: you may want to do 'fixfiles relabel' immediately

* Wed Oct 27 2004 Steve Grubb <sgrubb@redhat.com> 1.17.7-3
- Add code to sestatus to output the current policy from config file

* Fri Oct 22 2004 Dan Walsh <dwalsh@redhat.com> 1.17.7-2
- Patch audit2allow to return self and no brackets if only one rule

* Fri Oct 22 2004 Dan Walsh <dwalsh@redhat.com> 1.17.7-1
- Update to latest from NSA
- Eliminate fixfiles.cron

* Tue Oct 12 2004 Dan Walsh <dwalsh@redhat.com> 1.17.6-2
- Only run fixfiles.cron once a week, and eliminate null message

* Fri Oct 1 2004 Dan Walsh <dwalsh@redhat.com> 1.17.6-1
- Update with NSA
	* Added -l option to setfiles to log changes via syslog.
	* Merged -e option to setfiles to exclude directories.
	* Merged -R option to restorecon for recursive descent.
* Fri Oct 1 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-6
- Add -e (exclude directory) switch to setfiles 
- Add syslog to setfiles

* Fri Sep 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-5
- Add -R (recursive) switch to restorecon.

* Thu Sep 23 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-4
- Change to only display to terminal if tty is specified

* Tue Sep 21 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-3
- Only display to stdout if logfile not specified

* Mon Sep 9 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-2
- Add Steve Grubb patch to cleanup log files.

* Mon Aug 30 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-1
- Add optargs
- Update to match NSA

* Wed Aug 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.4-1
- Add fix to get cdrom info from /proc/media in fixfiles.

* Wed Aug 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.3-4
- Add Steve Grub patches for 
	* Fix fixfiles.cron MAILTO
	* Several problems in sestatus

* Wed Aug 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.3-3
- Add -q (quiet) qualifier to load_policy to not report warnings

* Tue Aug 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.3-2
- Add requires for libsepol >= 1.1.1

* Tue Aug 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.3-1
- Update to latest from upstream

* Mon Aug 23 2004 Dan Walsh <dwalsh@redhat.com> 1.17.2-1
- Update to latest from upstream
- Includes Colin patch for verifying file_contexts

* Sun Aug 22 2004 Dan Walsh <dwalsh@redhat.com> 1.17.1-1
- Update to latest from upstream

* Mon Aug 16 2004 Dan Walsh <dwalsh@redhat.com> 1.15.7-1
- Update to latest from upstream

* Thu Aug 12 2004 Dan Walsh <dwalsh@redhat.com> 1.15.6-1
- Add Man page for load_policy

* Tue Aug 10 2004 Dan Walsh <dwalsh@redhat.com> 1.15.5-1
-  new version from NSA uses libsepol

* Mon Aug 2 2004 Dan Walsh <dwalsh@redhat.com> 1.15.3-2
- Fix genhomedircon join command

* Thu Jul 29 2004 Dan Walsh <dwalsh@redhat.com> 1.15.3-1
- Latest from NSA

* Mon Jul 26 2004 Dan Walsh <dwalsh@redhat.com> 1.15.2-4
- Change fixfiles to not change when running a check

* Tue Jul 20 2004 Dan Walsh <dwalsh@redhat.com> 1.15.2-3
- Fix restorecon getopt call to stop hang on IBM Arches

* Mon Jul 19 2004 Dan Walsh <dwalsh@redhat.com> 1.15.2-2
- Only mail files less than 100 lines from fixfiles.cron
- Add Russell's fix for genhomedircon

* Fri Jul 16 2004 Dan Walsh <dwalsh@redhat.com> 1.15.2-1
- Latest from NSA

* Thu Jul 8 2004 Dan Walsh <dwalsh@redhat.com> 1.15.1-2
- Add ro warnings 

* Thu Jul 8 2004 Dan Walsh <dwalsh@redhat.com> 1.15.1-1
- Latest from NSA
- Fix fixfiles.cron to delete outfile

* Tue Jul 6 2004 Dan Walsh <dwalsh@redhat.com> 1.14.1-2
- Fix fixfiles.cron to not run on non SELinux boxes
- Fix several problems in fixfiles and fixfiles.cron

* Wed Jun 30 2004 Dan Walsh <dwalsh@redhat.com> 1.14.1-1
- Update from NSA
- Add cron capability to fixfiles

* Fri Jun 25 2004 Dan Walsh <dwalsh@redhat.com> 1.13.4-1
- Update from NSA

* Thu Jun 24 2004 Dan Walsh <dwalsh@redhat.com> 1.13.3-2
- Fix fixfiles to handle no rpm file on relabel

* Wed Jun 23 2004 Dan Walsh <dwalsh@redhat.com> 1.13.3-1
- Update latest from NSA
- Add -o option to setfiles to save output of any files with incorrect context.

* Tue Jun 22 2004 Dan Walsh <dwalsh@redhat.com> 1.13.2-2
- Add rpm support to fixfiles
- Update restorecon to add file input support

* Fri Jun 18 2004 Dan Walsh <dwalsh@redhat.com> 1.13.2-1
- Update with NSA Latest

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sat Jun 12 2004 Dan Walsh <dwalsh@redhat.com> 1.13.1-2
- Fix run_init to use policy formats

* Wed Jun 2 2004 Dan Walsh <dwalsh@redhat.com> 1.13.1-1
- Update from NSA

* Tue May 25 2004 Dan Walsh <dwalsh@redhat.com> 1.13-3
- Change location of file_context file

* Tue May 25 2004 Dan Walsh <dwalsh@redhat.com> 1.13-2
- Change to use /etc/sysconfig/selinux to determine location of policy files

* Fri May 21 2004 Dan Walsh <dwalsh@redhat.com> 1.13-1
- Update to latest from NSA
- Change fixfiles to prompt before deleteing /tmp files

* Tue May 18 2004 Dan Walsh <dwalsh@redhat.com> 1.12-2
- have restorecon ingnore <<none>>
- Hand matchpathcon the file status

* Thu May 14 2004 Dan Walsh <dwalsh@redhat.com> 1.12-1
- Update to match NSA

* Mon May 10 2004 Dan Walsh <dwalsh@redhat.com> 1.11-4
- Move location of log file to /var/tmp

* Mon May 10 2004 Dan Walsh <dwalsh@redhat.com> 1.11-3
- Better grep command for bind

* Fri May 7 2004 Dan Walsh <dwalsh@redhat.com> 1.11-2
- Eliminate bind and context mounts

* Wed May 5 2004 Dan Walsh <dwalsh@redhat.com> 1.11-1
- update to match NSA

* Wed Apr 28 2004 Dan Walsh <dwalsh@redhat.com> 1.10-4
- Log fixfiles to the /tmp directory

* Wed Apr 21 2004 Colin Walters <walters@redhat.com> 1.10-3
- Add patch to fall back to authenticating via uid if
  the current user's SELinux user identity is the default
  identity
- Add BuildRequires pam-devel

* Mon Apr 12 2004 Dan Walsh <dwalsh@redhat.com> 1.10-2
- Add man page, thanks to Richard Halley

* Thu Apr 8 2004 Dan Walsh <dwalsh@redhat.com> 1.10-1
- Upgrade to latest from NSA

* Fri Apr 2 2004 Dan Walsh <dwalsh@redhat.com> 1.9.2-1
- Update with latest from gentoo and NSA

* Thu Apr 1 2004 Dan Walsh <dwalsh@redhat.com> 1.9.1-1
- Check return codes in sestatus.c

* Mon Mar 29 2004 Dan Walsh <dwalsh@redhat.com> 1.9-19
- Fix sestatus to not double free
- Fix sestatus.conf to be unix format

* Mon Mar 29 2004 Dan Walsh <dwalsh@redhat.com> 1.9-18
- Warn on setfiles failure to relabel.

* Mon Mar 29 2004 Dan Walsh <dwalsh@redhat.com> 1.9-17
- Updated version of sestatus

* Mon Mar 29 2004 Dan Walsh <dwalsh@redhat.com> 1.9-16
- Fix fixfiles to checklabel properly

* Fri Mar 26 2004 Dan Walsh <dwalsh@redhat.com> 1.9-15
- add sestatus

* Thu Mar 25 2004 Dan Walsh <dwalsh@redhat.com> 1.9-14
- Change free call to freecon
- Cleanup

* Tue Mar 23 2004 Dan Walsh <dwalsh@redhat.com> 1.9-12
- Remove setfiles-assoc patch
- Fix restorecon to not crash on missing dir

* Thu Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-11
- Eliminate trailing / in restorecon

* Thu Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-10
- Add Verbosity check

* Thu Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-9
- Change restorecon to not follow symlinks.  It is too difficult and confusing
- to figure out the file context for the file pointed to by a symlink.

* Wed Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-8
- Fix restorecon
* Wed Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-7
- Read restorecon patch

* Wed Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-6
- Change genhomedircon to take POLICYSOURCEDIR from command line

* Wed Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-5
- Add checkselinux
- move fixfiles and restorecon to /sbin

* Wed Mar 17 2004 Dan Walsh <dwalsh@redhat.com> 1.9-4
- Restore patch of genhomedircon

* Mon Mar 15 2004 Dan Walsh <dwalsh@redhat.com> 1.9-3
- Add setfiles-assoc patch to try to freeup memory use

* Mon Mar 15 2004 Dan Walsh <dwalsh@redhat.com> 1.9-2
- Add fixlabels

* Mon Mar 15 2004 Dan Walsh <dwalsh@redhat.com> 1.9-1
- Update to latest from NSA

* Wed Mar 10 2004 Dan Walsh <dwalsh@redhat.com> 1.6-8
- Increase the size of buffer accepted by setfiles to BUFSIZ.

* Tue Mar 9 2004 Dan Walsh <dwalsh@redhat.com> 1.6-7
- genhomedircon should complete even if it can't read /etc/default/useradd

* Tue Mar 9 2004 Dan Walsh <dwalsh@redhat.com> 1.6-6
- fix restorecon to relabel unlabled files.

* Fri Mar 5 2004 Dan Walsh <dwalsh@redhat.com> 1.6-5
- Add genhomedircon from tresys
- Fixed patch for restorecon

* Thu Feb 26 2004 Dan Walsh <dwalsh@redhat.com> 1.6-4
- exit out when selinux is not enabled

* Thu Feb 26 2004 Dan Walsh <dwalsh@redhat.com> 1.6-3
- Fix minor bugs in restorecon

* Thu Feb 26 2004 Dan Walsh <dwalsh@redhat.com> 1.6-2
- Add restorecon c program 

* Tue Feb 24 2004 Dan Walsh <dwalsh@redhat.com> 1.6-1
- Update to latest tarball from NSA

* Thu Feb 19 2004 Dan Walsh <dwalsh@redhat.com> 1.4-9
- Add sort patch

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jan 29 2004 Dan Walsh <dwalsh@redhat.com> 1.4-7
- remove mods to run_init since init scripts don't require it anymore

* Wed Jan 28 2004 Dan Walsh <dwalsh@redhat.com> 1.4-6
- fix genhomedircon not to return and error 

* Wed Jan 28 2004 Dan Walsh <dwalsh@redhat.com> 1.4-5
- add setfiles quiet patch

* Tue Jan 27 2004 Dan Walsh <dwalsh@redhat.com> 1.4-4
- add checkcon to verify context match file_context

* Wed Jan 7 2004 Dan Walsh <dwalsh@redhat.com> 1.4-3
- fix command parsing restorecon

* Tue Jan 6 2004 Dan Walsh <dwalsh@redhat.com> 1.4-2
- Add restorecon

* Sat Dec 6 2003 Dan Walsh <dwalsh@redhat.com> 1.4-1
- Update to latest NSA 1.4

* Tue Nov 25 2003 Dan Walsh <dwalsh@redhat.com> 1.2-9
- Change run_init.console to run as run_init_t

* Tue Oct 14 2003 Dan Walsh <dwalsh@redhat.com> 1.2-8
- Remove dietcc since load_policy is not in mkinitrd
- Change to use CONSOLEHELPER flag

* Tue Oct 14 2003 Dan Walsh <dwalsh@redhat.com> 1.2-7
- Don't authenticate run_init when used with consolehelper

* Wed Oct 01 2003 Dan Walsh <dwalsh@redhat.com> 1.2-6
- Add run_init consolehelper link

* Wed Sep 24 2003 Dan Walsh <dwalsh@redhat.com> 1.2-5
- Add russell spead up patch to deal with file path stems

* Fri Sep 12 2003 Dan Walsh <dwalsh@redhat.com> 1.2-4
- Build load_policy with diet gcc in order to save space on initrd

* Fri Sep 12 2003 Dan Walsh <dwalsh@redhat.com> 1.2-3
- Update with NSA latest

* Thu Aug 7 2003 Dan Walsh <dwalsh@redhat.com> 1.2-1
- remove i18n
- Temp remove gtk support

* Thu Aug 7 2003 Dan Walsh <dwalsh@redhat.com> 1.1-4
- Remove wnck requirement

* Thu Aug 7 2003 Dan Walsh <dwalsh@redhat.com> 1.1-3
- Add gtk support to run_init

* Tue Aug 5 2003 Dan Walsh <dwalsh@redhat.com> 1.1-2
- Add internationalization

* Mon Jun 2 2003 Dan Walsh <dwalsh@redhat.com> 1.0-1
- Initial version
