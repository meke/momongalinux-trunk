%global momorel 6

Summary: LaTeX plugin for the MoinMoin wiki
Name:    moin-latex
Version: 0
Release: 0.20051126.3.%{momorel}m%{?dist}
License: GPL
Group:   Applications/Internet
URL:     http://johannes.sipsolutions.net/Projects/new-moinmoin-latex
#
#  Please note that upstream does not provide a tar or similar 
#  file.  Instead, it provides the four files individually as 
#  links from the project page.  Here, those four files have 
#  been collected and a simple README.latex-plugins added.
#
Source0: moin-latex-20051126.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires:  moin, tetex

%description
This MoinMoin plugin adds embedded LaTeX content (as PNG images) and a 
raw formatting mode.  For details please see:
  http://johannes.sipsolutions.net/Projects/new-moinmoin-latex

%prep
%setup -q -n moin-latex-20051126

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/moin/data/plugin/parser/
cp *latex*.py %{buildroot}%{_datadir}/moin/data/plugin/parser/
chmod +x %{buildroot}%{_datadir}/moin/data/plugin/parser/latex.py

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,0755)
%doc README.latex-plugins
%{_datadir}/moin/data/plugin/parser/*latex*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.20051126.3.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.20051126.3.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.20051126.3.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.20051126.3.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.20051126.3.2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20051126.3.1m)
- import from Fedora to Momonga

* Thu Aug 31 2006 Ed Hill <ed@eh3.com> - 0-0.20051126.3
- rebuild for imminent FC-6 release

* Fri Feb 17 2006 Ed Hill <ed@eh3.com> 0-0.20051126.2
- rebuild

* Sat Nov 26 2005 Ed Hill <ed@eh3.com> 0-0.20051126.1
- initial package creation
