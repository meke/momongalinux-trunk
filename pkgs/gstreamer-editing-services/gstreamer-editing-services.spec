%global momorel 3
%global majorminor 0.10

Name: gstreamer-editing-services
Version: 0.10.1
Release: %{momorel}m%{?dist}
Summary: GStreamer Editing Services

Group: Applications/Multimedia
License: LGPL
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/gst-editing-services/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer >= 0.10.32

# plugins
BuildRequires: gstreamer-devel >= 0.10.32
BuildRequires: gstreamer-plugins-base-devel >= 0.10.32
BuildRequires: glib2-devel gtk2-devel

# documentation
BuildRequires:  gtk-doc

%description
This is a high-level library for facilitating the creation of audio/video
non-linear editors.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --disable-static \
    --enable-gtk-doc \
    --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install


%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README RELEASE
%{_bindir}/ges-launch-%{majorminor}
%{_libdir}/libges-%{majorminor}.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/GES-%{majorminor}.typelib

%files devel
%defattr(-,root,root)
%{_includedir}/gstreamer-%{majorminor}/ges
%{_libdir}/libges-%{majorminor}.so
%{_datadir}/gtk-doc/html/ges-%{majorminor}
%{_libdir}/pkgconfig/gst-editing-services-0.10.pc
%{_datadir}/gir-1.0/GES-0.10.gir

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-3m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-2m)
- rebuild for new GCC 4.6

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- initial build

