%global momorel 7
%define py_ver 2.7
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")
%define tarname pygobject

Summary: Python bindings for GObject
Name: pygobject2
Version: 2.28.6
Release: %{momorel}m%{?dist}
URL: http://www.pygtk.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{tarname}/2.28/%{tarname}-%{version}.tar.xz
NoSource: 0
Patch0: pygobject-2.28.6-gobject.patch
License: LGPL
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2 >= 2.16.1
Requires: python >= %{pyver}
BuildRequires: glib2-devel >= 2.20.1
BuildRequires: python-devel >= %{pyver}
BuildRequires: gobject-introspection-devel >= 0.9.6
BuildRequires: pycairo-devel

Obsoletes: pygobject228
Provides: pygobject228 = %{version}-%{release}

Provides: pygobject2-codegen  = %{version}-%{release}

%description
This archive contains bindings for the GObject, to be used in Python
It is a fairly complete set of bindings, it's already rather useful,
and is usable to write moderately complex programs.  (see the
examples directory for some examples of the simpler programs you could
write).
#'

%package devel
Summary: Development files for pygobject
Group: Development/Languages
Requires: %name = %{version}-%{release}
Requires: glib2-devel

Obsoletes: pygobject228-devel
Provides: pygobject228-devel = %{version}-%{release} 

%description devel
Development files for %{name}.
 
%prep
%setup -q -n %{tarname}-%{version}
%patch0 -p1 -b .gobject

%build
export PYTHON=%{_bindir}/python%{py_ver}
%configure --enable-docs \
	--enable-cairo \
	--enable-introspection \
	LIBS="-lpython2.7"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(755, root, root, 755)
%doc AUTHORS  COPYING  ChangeLog  NEWS  README
%{_bindir}/pygobject-codegen-2.0
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la
%{python_sitearch}/glib
%{python_sitearch}/gobject
%exclude %{python_sitearch}/gi
%{python_sitearch}/gtk-2.0/dsextras.p*
%{python_sitearch}/pygtk.p*
%{python_sitearch}/gtk-2.0/gio
%{_datadir}/%{tarname}/2.0
%{_datadir}/%{tarname}/xsl


%files devel
%defattr(755, root, root, 755)
%{_includedir}/pygtk-2.0/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/pygobject-2.0.pc
%doc %{_datadir}/gtk-doc/html/pygobject

%changelog
* Thu Aug  1 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.6-7m)
- Provide pygobject2-codegen

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.6-6m)
- change package name

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.6-5m)
- rebuild for glib 2.33.2

* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-4m)
- delete conflict dir

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-3m)
- copy from pygobject

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-2m)
- back to 2.28.6 (add patch0 from 2.90.3

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90.3-1m)
- update to 2.90.3

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-1m)
- update to 2.28.6

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.28.4-1m)
- update to 2.28.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.3-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Sun Mar 06 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.26.0-4m)
- reviced BR

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-3m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.21.3-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.21.3-1m)
- update to 2.21.3

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.21.1-3m)
- add LIBS="-lpython2.6"

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.21.1-2m)
- use BuildRequires

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.21.1-1m)
- update to 2.21.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Mon May 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.0-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.15.4-3m)
- rebuild against python-2.6.1-1m

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.4-2m)
- fix %%files

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.4-1m)
- update to 2.15.4

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.1-2m)
- rebuild against gcc43

* Fri Jan  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.3-2m)
- rebuild against python-2.5

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Wed Aug 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-2m)
- delete duplicated dir

* Mon Apr 10 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.10.0-1m)
- version 2.10.0
