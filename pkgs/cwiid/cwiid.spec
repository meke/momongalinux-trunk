%global momorel 9

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: CWiid Wiimote Interface
Name: cwiid
Version: 0.6.00
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: http://abstrakraft.org/cwiid/wiki/libcwiid
Source0: http://abstrakraft.org/%{name}/downloads/%{name}-%{version}.tgz
NoSource: 0
Patch0: %{name}-%{version}-bluetooth_api_fix.diff
Patch1: %{name}-%{version}-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python-%{name}
BuildRequires: bison
BuildRequires: bluez-libs-devel
BuildRequires: flex
BuildRequires: gtk2-devel
BuildRequires: python-devel >= 2.7

%description
CWiid is a Wiimote Interface.
The cwiid package contains the following parts:
1.cwiid library - abstracts the interface to the wiimote by hiding
  the details of the underlying Bluetooth connection
2.wmif - provides a simple text-based interface to the wiimote.
3.wmgui - provides a simple GTK gui to the wiimote.

%package devel
Summary: Development headers and libraries for CWiid
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files and libraries needed for
developing programs using the CWiid Wiimote library.

%package -n python-%{name}
Summary: Python bindings for the cwiid Wiimote library
Group: System Environment/Libraries

%description -n	python-%{name}
This package contains Python bindings for the %{oname} Wiimote
library.

%prep
%setup -q

%patch0 -p0 -b .bt-api-fix
%patch1 -p1 -b .linking

%build
%configure --disable-ldconfig

make %{?_smp_mflags} WARNFLAGS="%{optflags} -Wall"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# clean up docs
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%doc doc/Xmodmap doc/wminput.list
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/wminput
%config(noreplace) %{_sysconfdir}/%{name}/wminput/*
%{_bindir}/lswm
%{_bindir}/wmgui
%{_bindir}/wminput
%{_libdir}/%{name}
%{_libdir}/lib%{name}.so.*
%{_mandir}/man1/wmgui.1*
%{_mandir}/man1/wminput.1*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so

%files -n python-%{name}
%defattr(-,root,root)
%{python_sitearch}/%{name}-*.egg-info
%{python_sitearch}/%{name}.so

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.00-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.00-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.00-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.00-6m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.00-5m)
- touch up spec file

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.00-4m)
- explicitly link libbluetooth and libpthread

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.00-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.00-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.00-1m)
- initial package for ardour-2.7.1
- Summary, %%description and a patch are imported from cooker
