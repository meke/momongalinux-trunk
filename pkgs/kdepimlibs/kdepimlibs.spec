%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

%define apidocs 1
%global akonadi_subpkg 1
%global akonadi_version 1.12.1
%global shared_desktop_ontologies_version 0.11.0

Name: kdepimlibs
Version: %{kdever}
Release: %{momorel}m%{?dist}
Summary: K Desktop Environment 4 - PIM Libraries
License: LGPLv2
Group: System Environment/Libraries
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
## upstream patches
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{version}
BuildRequires: akonadi-devel >= %{akonadi_version}
BuildRequires: boost-devel >= 1.50.0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: cyrus-sasl-devel
BuildRequires: gpgme-devel >= 1.3.2
BuildRequires: phonon-backend-gstreamer
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: libical-devel >= 0.33
BuildRequires: libXpm-devel
BuildRequires: libXtst-devel
BuildRequires: nepomuk-core-devel >= %{version}
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: prison-devel
BuildRequires: shared-desktop-ontologies-devel >= %{shared_desktop_ontologies_version}
%if 0%{?apidocs}
BuildRequires: doxygen
BuildRequires: graphviz
BuildRequires: qt-doc
%else
Obsoletes: kdepimlibs-apidocs
%endif

%description
Personal Information Management (PIM) libraries for the
K Desktop Environment 4.

%package devel
Group:    Development/Libraries
Summary:  Header files for kdepimlibs
Requires: %{name} = %{version}-%{release}
Requires: kdelibs-devel

%description devel
Header files for developing applications using %{name}.

%package akonadi
Summary: Akonadi runtime support for %{name}
Group: System Environment/Libraries
# when pkg split occurrs, not sure if this is really needed, but... -- Rex
#Obsoletes: kdepimlibs < 4.2.0-3
Requires: %{name} = %{version}-%{release}
Requires: akonadi >= %{akonadi_version}

%description akonadi
%{summary}.

%package apidocs
Group: Documentation
Summary: kdepimlibs API documentation
BuildArch: noarch

%description apidocs
This package includes the kdepimlibs API documentation in HTML
format for easy browsing.

%package kxmlrpcclient
Summary: Simple XML-RPC Client support
# when spilt out
Conflicts: kdepimlibs < 4.9.2
License: BSD

%description kxmlrpcclient
This library contains simple XML-RPC Client support. It is used mainly
by the egroupware module of kdepim, but is a complete client and is
quite easy to use.

%prep
%setup -q -n %{name}-%{version}%{?svnrel:svn%{svnrel}}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

# build apidocs
%if 0%{?apidocs}
export QTDOCDIR=`pkg-config --variable=docdir Qt`
kde4-doxygen.sh --doxdatadir=%{_kde4_docdir}/HTML/en/common .
%endif

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

# move devel symlinks
mkdir -p %{buildroot}%{_kde4_libdir}/kde4/devel
pushd %{buildroot}%{_kde4_libdir}
for i in lib*.so
do
  case "$i" in
    libkdeinit4_*.so)
      ;;
    *)
      linktarget=`readlink "$i"`
      rm -f "$i"
      ln -sf "../../$linktarget" "kde4/devel/$i"
      ;;
  esac
done
popd

# install apidocs
%if 0%{?apidocs}
mkdir -p %{buildroot}%{_kde4_docdir}/HTML/en
cp -prf kdepimlibs-%{version}%{?svnrel:svn%{svnrel}}-apidocs %{buildroot}%{_kde4_docdir}/HTML/en/kdepimlibs-apidocs
%endif

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun
/sbin/ldconfig ||:
if [ $1 -eq 0 ] ; then
update-mime-database %{_kde4_datadir}/mime &> /dev/null
fi

%posttrans
update-mime-database %{_kde4_datadir}/mime >& /dev/null

%if 0%{?akonadi_subpkg}
%post akonadi -p /sbin/ldconfig

%postun akonadi
/sbin/ldconfig ||:
if [ $1 -eq 0 ] ; then
update-mime-database %{_kde4_datadir}/mime &> /dev/null
fi

%posttrans akonadi
update-mime-database %{_kde4_datadir}/mime >& /dev/null
%endif

%post kxmlrpcclient -p /sbin/ldconfig

%postun kxmlrpcclient -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_kde4_bindir}/akonadi_benchmarker
%{_kde4_bindir}/akonaditest
%{_kde4_appsdir}/kabc
%{_kde4_appsdir}/kconf_update/*
%{_kde4_appsdir}/libkholidays
%{_kde4_datadir}/mime/packages/kdepimlibs-mime.xml
%{_kde4_datadir}/config.kcfg/*
%{_datadir}/dbus-1/interfaces/*
%{_kde4_datadir}/kde4/services/*
%{_kde4_datadir}/kde4/servicetypes/*
%{_kde4_docdir}/HTML/en/kcontrol/kresources
%{_kde4_docdir}/HTML/en/kioslave/*
%{_kde4_libdir}/lib*.so.*
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/kde4/plugins/designer/*.so
%{_kde4_libdir}/gpgmepp
%exclude %{_kde4_libdir}/libkxmlrpcclient.so.*
%exclude %{_kde4_libdir}/kde4/devel
%if 0%{?akonadi_subpkg}
%exclude %{_kde4_bindir}/akonadi2xml
%exclude %{_kde4_libdir}/libakonadi-*.so.*
%exclude %{_kde4_libdir}/kde4/akonadi
%exclude %{_kde4_libdir}/kde4/akonadi_serializer_socialfeeditem.so
%exclude %{_kde4_libdir}/kde4/kcm_mailtransport.so
%exclude %{_kde4_libdir}/kde4/kcm_akonadicontact_actions.so
%exclude %{_kde4_datadir}/kde4/services/kcm_mailtransport.desktop
%exclude %{_kde4_datadir}/kde4/services/akonadicontact_actions.desktop
%exclude %{_kde4_datadir}/config.kcfg/mailtransport.kcfg
%exclude %{_kde4_appsdir}/kconf_update/mailtransports.upd
%exclude %{_kde4_appsdir}/kconf_update/migrate-transports.pl

%files akonadi
%defattr(-,root,root,-)
%endif
%{_kde4_bindir}/akonadi2xml
%{_kde4_libdir}/libakonadi-*.so.*
%{_kde4_libdir}/kde4/akonadi
%{_kde4_libdir}/kde4/akonadi_serializer_socialfeeditem.so
%{_kde4_libdir}/kde4/kcm_mailtransport.so
%{_kde4_libdir}/kde4/kcm_akonadicontact_actions.so
%{_kde4_datadir}/akonadi/agents/*.desktop
%{_kde4_datadir}/kde4/services/kcm_mailtransport.desktop
%{_kde4_datadir}/kde4/services/akonadicontact_actions.desktop
%{_kde4_datadir}/config.kcfg/mailtransport.kcfg
%{_kde4_appsdir}/akonadi_knut_resource
%{_kde4_appsdir}/kconf_update/mailtransports.upd
%{_kde4_appsdir}/kconf_update/migrate-transports.pl
%{_kde4_appsdir}/akonadi
%{_kde4_appsdir}/akonadi-kde
%{_kde4_datadir}/mime/packages/x-vnd.akonadi.socialfeeditem.xml

%files devel
%defattr(-,root,root,-)
%{_kde4_appsdir}/cmake/modules/*
%{_kde4_includedir}/KDE/Akonadi
%{_kde4_includedir}/KDE/KABC
%{_kde4_includedir}/KDE/KAlarmCal
%{_kde4_includedir}/KDE/KBlog
%{_kde4_includedir}/KDE/KCal
%{_kde4_includedir}/KDE/KCalCore
%{_kde4_includedir}/KDE/KCalUtils
%{_kde4_includedir}/KDE/KHolidays
%{_kde4_includedir}/KDE/KIMAP
%{_kde4_includedir}/KDE/KLDAP
%{_kde4_includedir}/KDE/KMime
%{_kde4_includedir}/KDE/KPIMIdentities
%{_kde4_includedir}/KDE/KPIMTextEdit
%{_kde4_includedir}/KDE/KPIMUtils
%{_kde4_includedir}/KDE/KResources
%{_kde4_includedir}/KDE/KTNEF
%{_kde4_includedir}/KDE/KontactInterface
%{_kde4_includedir}/KDE/Mailtransport
%{_kde4_includedir}/KDE/Syndication
%{_kde4_includedir}/akonadi/private/*
%{_kde4_includedir}/akonadi/*.h
%{_kde4_includedir}/akonadi/calendar
%{_kde4_includedir}/akonadi/contact
%{_kde4_includedir}/akonadi/kabc
%{_kde4_includedir}/akonadi/kcal
%{_kde4_includedir}/akonadi/kmime
%{_kde4_includedir}/akonadi/notes
%{_kde4_includedir}/akonadi/socialutils
%{_kde4_includedir}/akonadi/xml
%{_kde4_includedir}/gpgme++
%{_kde4_includedir}/kabc
%{_kde4_includedir}/kalarmcal
%{_kde4_includedir}/kblog
%{_kde4_includedir}/kcal
%{_kde4_includedir}/kcalcore
%{_kde4_includedir}/kcalutils
%{_kde4_includedir}/kholidays
%{_kde4_includedir}/kimap
%{_kde4_includedir}/kimaptest
%{_kde4_includedir}/kldap
%{_kde4_includedir}/kmbox
%{_kde4_includedir}/kmime
%{_kde4_includedir}/kontactinterface
%{_kde4_includedir}/kpimidentities
%{_kde4_includedir}/kpimtextedit
%{_kde4_includedir}/kpimutils
%{_kde4_includedir}/kresources
%{_kde4_includedir}/ktnef
%{_kde4_includedir}/kxmlrpcclient
%{_kde4_includedir}/mailtransport
%{_kde4_includedir}/microblog
%{_kde4_includedir}/qgpgme
%{_kde4_includedir}/syndication
%{_kde4_libdir}/libkimaptest.a
%{_kde4_libdir}/kde4/devel/lib*.so
%{_kde4_libdir}/cmake/KdepimLibs

%if 0%{?apidocs}
%files apidocs
%defattr(-,root,root,-)
%{_kde4_docdir}/HTML/en/kdepimlibs-apidocs
%endif

%files kxmlrpcclient
%defattr(-,root,root,-)
%doc kxmlrpcclient/README 
%{_kde4_libdir}/libkxmlrpcclient.so.*

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.12.97-2m)
- rebuild against graphviz-2.36.0-1m

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-2m)
- import upstream patches

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-2m)
- rebuild against gpgme-1.3.2

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.4-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (4.8.4-2m)
- rebuild for boost 1.50.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-2m)
- add BuildRequires

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.90-2m)
- rebuild for boost-1.48.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to kDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.0-2m)
- rebuild for boost

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-2m)
- rebuild against boost-1.46.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARIT:A Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-3m)
- modify %%files to avoid conflicting

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-2m)
- fix kde#245123

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.2-2m)
- rebuild against boost-1.44.0

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- set %%global akonadi_version 1.2.0 for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rbuild with new release source
- - separate kdepimlibs-akonadi sub package

* Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-2m)
- specify cmke version up to 2.6.4

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rbuild with new release source
- - separate kdepimlibs-akonadi sub package

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sat Apr 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Sat Oct 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.71-2m)
- change BR from phonon-backend-gst to phonon-backend-gstreamer

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sun Aug 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Sun Aug 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.61

* Sun Aug 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- rebuild against akonadi-1.0.0

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.1-2m)
- rebuild against openldap-2.4.8

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- KDE4
- import from Fedora devel

* Tue Dec 11 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.97.0-2
- rebuild for changed _kde4_includedir

* Wed Dec 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.97.0-1
- kde-3.97.0

* Thu Nov 29 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.2-1
- kde-3.96.2

* Tue Nov 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.1-2
- kde-3.96.1

* Thu Nov 15 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.0-1
- kde-3.96.0

* Fri Nov 09 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.95.2-1
- kde-3.95.2

* Mon Nov 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.95.0-1
- kde-3.95.0 (kde4 dev platform rc1)

* Thu Oct 18 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.94.0-1
- update to 3.94.0
- add new %%{_kde4_libdir}/Gpgmepp directory to file list

* Thu Oct 4 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-4
- drop ExcludeArch: ppc64 (#300591)

* Fri Sep 21 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-3
- ExcludeArch: ppc64 (#300591)

* Thu Sep 13 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-2
- delete KMail/KNode transport migration scripts which break KDE 3

* Sun Sep 9 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-1
- update to 3.93.0
- drop kde4home patch (no longer applied)
- list BR strigi-devel only once
- move devel symlinks to %%{_kde4_libdir}/kde4/devel/

* Tue Aug 14 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.92.0-4
- use macros.kde4
- License: LGPLv2

* Mon Jul 30 2007 Than Ngo <than@redhat.com> 3.92.0-2
- add BR: gpgme-devel

* Sat Jul 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.92.0-1
- kde-3.92 (kde-4-beta1)

* Thu Jun 29 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-3
- fix %%_sysconfdir for %%_prefix != /usr case.

* Thu Jun 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-2
- updated kde4home.diff
- CMAKE_BUILD_TYPE=RelWithDebInfo (we're already using %%optflags)
- drop SNPRINTF hack

* Wed Jun 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-1
- kde-3.91.0
- CMAKE_BUILD_TYPE=debug

* Sat Jun 23 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.90.1-2
- specfile cleanup (%%prefix issues mostly)

* Sun May 13 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.90.1-1
- update to 3.90.1
- bump cmake BR to 2.4.5 as required upstream now
- don't set execute bits by hand anymore, cmake has been fixed
- use multilibs in /opt/kde4
- add BR boost-devel

* Fri Mar 23 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.3-4
- restore minimum version requirement for cmake
- don't set QT4DIR and PATH anymore, qdbuscpp2xml has been fixed

* Mon Mar 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.80.3-3
- +eXecute perms for %%{prefix}/lib/*

* Fri Feb 23 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.3-2
- rebuild for patched FindKDE4Internal.cmake

* Wed Feb 21 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.3-1
- update to 3.80.3
- update and improve parallel-installability patch
- readd BR cyrus-sasl-devel
- don't set LD_LIBRARY_PATH
- set QT4DIR and PATH so CMake's direct $QT4DIR/qdbuscpp2xml calls work
- define HAVE_SNPRINTF to work around vsnprintf.c build failure

* Wed Nov 29 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.80.2-0.3.20061003svn
- dropped -DCMAKE_SKIP_RPATH=TRUE from cmake
- compiling with QA_RPATHS=0x0003; export QA_RPATHS
- added libXtst-devel libXpm-devel as BR

* Fri Nov 24 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.80.2-0.2.20061003svn
- parallel build support
- added -DCMAKE_SKIP_RPATH=TRUE to cmake to skip rpath
- dropped qt4-devel >= 4.2.0, cyrus-sasl-devel  as BR
- spec file cleanups and added clean up in %%install
- fixed PATH for libkdecore.so.5; cannot open shared object file;

* Sat Oct 07 2006 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.2-0.1.20061003svn
- first Fedora RPM (parts borrowed from the OpenSUSE kdepimlibs 4 RPM and the Fedora kdelibs 3 RPM)
- apply parallel-installability patch
