%global module	Parse-Yapp
%global momorel 36
Summary:	%{module} module for perl
Name:		perl-%{module}
Version:	1.05
Release: %{momorel}m%{?dist}
License:	GPL or Artistic
Group:		Development/Languages
Source0:	http://www.cpan.org/modules/by-module/Parse/%{module}-%{version}.tar.gz
NoSource: 0
URL:		http://www.cpan.org/modules/by-module/Parse/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl >= 5.8.5
BuildArchitectures: noarch

%description
%{module} module for perl

%prep
%setup -q -n %{module}-%{version}

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make


%clean 
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/Parse/Yapp/.packlist

%files
%defattr(-,root,root)
%doc Changes README
%{_bindir}/*
%{_mandir}/man?/*
%{perl_vendorlib}/Parse/Yapp*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-36m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-35m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-34m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-33m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-32m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-31m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-30m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-29m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-28m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-27m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-26m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.05-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.05-24m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-23m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.05-22m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-21m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-20m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-18m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-17m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.05-16m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.05-15m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.01-10m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.05-13m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.05-12m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.05-11m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.05-10m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.05-9m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.05-8m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.05-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.05-6m)
- kill %%define version
- kill %%define release

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.05-5m)
- rebuild against perl-5.8.0
- revise %files

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.05-4m)
- fix %files

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.05-3m)
- fix man install path

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (1.05-2k)
- version up.

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.04-4k)
- rebuild against for perl-5.6.1

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (1.04-2k)
- merge from rawhide.

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.04-2
- import from mandrake. tweak file list for man pages.

* Mon Jun 18 2001 Till Kamppeter <till@mandrakesoft.com> 1.04-1mdk
- Updated to 1.04 (for Foomatic).

* Sun Jun 17 2001 Geoffrey Lee <snailtalk@mandrakesoft.com> 1.02-4mdk
- Rename spec file.
- Remove the Distribution and Vendor tag.
- Rebuild for the latest perl.

* Tue Mar 13 2001 Jeff Garzik <jgarzik@mandrakesoft.com> 1.02-3mdk
- BuildArch: noarch

* Sat Sep 16 2000 Stefan van der Eijk <s.vandereijk@chello.nl> 1.02-2mdk
- Call spec-helper before creating filelist

* Wed Aug 09 2000 Jean-Michel Dault <jmdault@mandrakesoft.com> 1.02-1mdk
- Macroize package
