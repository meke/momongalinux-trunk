%global momorel 1

%global gtkxfceenginever 3.0.1
%global exover 0.10.2
%global thunarver 1.6.3
%global thunarvfsver 1.2.0
%global terminalver 0.6.3
%global mousepadver 0.3.0
%global xfmpcver 0.2.2
%global gigolover 0.4.1
%global midoriver 0.5.2
%global wafver 1.6.10
%global ristrettover 0.6.3
%global xfbibver 0.0.2
%global xfburnver 0.5.2

####

Name:    xfce4
Version: 4.11.0
Release: %{momorel}m%{?dist}
#Release: 0.%{momorel}m%{?dist}
Summary: Meta package for Xfce4.

Group:   User Interface/Desktops
License: GPLv2+
URL:     http://www.xfce.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

################################################

Requires: gtk-xfce-engine >= %{gtkxfceenginever}
Requires: dbh >= 1.0.24-12m
Requires: libxfce4util >= %{version}
Requires: xfconf
#Requires: libxfcegui4 >= 4.10.0-1m

#Requires: libxfce4menu >= %{version}
Requires: exo >= %{exover}
Requires: libxfce4ui >= %{version}
Requires: xfce4-settings >= %{version}
Requires: xfce4-session >= %{version}

Requires: xfwm4 >= %{version}
Requires: garcon >= 0.2.0-1m
Requires: xfce4-panel >= %{version}

Requires: Thunar >= %{thunarver}
Requires: thunar-vfs >= %{thunarvfsver}

Requires: xfdesktop >= %{version}

Requires: xfce4-appfinder >= %{version}
#
# noarch
Requires: xfce4-dev-tools >= %{version}

Requires: xfprint >= 4.6.1-11m
Requires: xfce4-mixer >= 4.11.0-1m
#
# noarch
Requires: xfwm4-themes >= 4.10.0-1m
Requires: xfwm4-theme-nodoka >= 0.2-6m

Requires: orage >= 4.10.0-1m

Requires: Terminal >= %{terminalver}

Requires: mousepad >= %{mousepadver}

Requires: xfmpc >= %{xfmpcver}

Requires: gigolo >= %{gigolover}

Requires: midori >= %{midoriver}
#
# noarch
Requires: waf >= %{wafver}

Requires: ristretto >= %{ristrettover}

Requires: xfbib >= %{xfbibver}

Requires: xfburn >= %{xfburnver}

################################################

# misc

# plugin packages that have not "plugin" in own package name

#skip Requires: xfce4-volstatus-icon >= 0.1.0-6m
Obsoletes: xfce4-volstatus-icon

Requires: xfce4-taskmanager >= 1.0.1-1m

Requires: xfce4-screenshooter >= 1.8.1-1m

# plugins
# sorted by A to Z

# Requires: gsynaptics-mcs-plugin >= 1.0.0-1m

Requires: thunar-archive-plugin >= 0.3.1-1m
Requires: thunar-media-tags-plugin >= 0.2.1-1m
#skip Requires: thunar-shares-plugin  >= 0.2.0-9m
Obsoletes: thunar-shares-plugin

Requires: thunar-thumbnailers >= 0.4.1-9m

Requires: thunar-volman >= 0.8.0-1m
# # |- xarchiver
# #  |- arj
Requires: thunar-vcs-plugin >= 0.1.4-2m

Requires: xfce4-battery-plugin >= 1.0.5-1m
#skip Requires: xfce4-cddrive-plugin >= 0.0.1-8m
Obsoletes: xfce4-cddrive-plugin
Requires: xfce4-cellmodem-plugin >= 0.0.5-11m
Requires: xfce4-clipman-plugin >= 1.2.5-1m
Requires: xfce4-cpufreq-plugin >= 1.1.0-1m
Requires: xfce4-cpugraph-plugin >= 1.0.5-1m

Requires: xfce4-datetime-plugin >= 0.6.2-1m
Requires: xfce4-dict >= 0.7.0-1m
Requires: xfce4-diskperf-plugin >= 2.5.4-1m

Requires: xfce4-eyes-plugin >= 4.4.2-1m
Requires: xfce4-fsguard-plugin >= 1.0.1-1m
Requires: xfce4-genmon-plugin >= 3.4.0-1m

Requires: xfce4-icon-theme >= 4.4.3-7m
# (noarch)

# http://spuriousinterrupt.org/projects/mailwatch
Requires: xfce4-mailwatch-plugin >= 1.2.0-1m

# Requires: xfce4-modemlights-plugin >= 0.1.3.99-4m
# in OBSO-trunk

Requires: xfce4-mount-plugin >= 0.6.4-1m
Requires: xfce4-mpc-plugin >= 0.4.4-1m
Requires: xfce4-netload-plugin >= 1.2.0-1m

Requires: xfce4-notes-plugin >= 1.7.7-1m
Requires: xfce4-notifyd >= 0.2.2-1m

Requires: xfce4-places-plugin >= 1.6.0-1m
Requires: xfce4-power-manager >= 1.2.0-1m

Requires: xfce4-quicklauncher-plugin >= 1.9.4-11m

Requires: xfce4-radio-plugin >= 0.5.1-1m
Requires: xfce4-remmina-plugin >= 0.8.1-1m

Requires: xfce4-sensors-plugin >= 1.2.5-1m
Requires: xfce4-smartbookmark-plugin >= 0.4.5-1m
Requires: xfce4-smartpm-plugin >= 0.4.0-9m
#skip Requires: xfce4-stopwatch-plugin >= 0.2.0-5m
Obsoletes: xfce4-stopwatch-plugin
Requires: xfce4-systemload-plugin >= 1.1.1-1m

Requires: xfce4-time-out-plugin >= 1.0.1-1m

Requires: xfce4-timer-plugin >= 1.5.0-1m

# tarball name is "verve-plugin"
Requires: xfce4-verve-plugin >= 1.0.0-1m
Requires: xfce4-volumed >= 0.1.13-1m
# |- keybinder-devel

Requires: xfce4-wavelan-plugin >= 0.5.11-1m
Requires: xfce4-weather-plugin >= 0.8.3-1m
#Requires: xfce4-websearch-plugin >= 0.1.1-0.2.20070428.11m

# Fix me
# Requires: xfce4-xfapplet-plugin >= 0.1.0-8m
Requires: xfce4-xfswitch-plugin >= 0.0.1-5m
Requires: xfce4-xkb-plugin >= 0.5.4.1-1m
Requires: xfce4-xmms-plugin >= 0.5.3-1m

#skip Requires: xfmedia >= 0.9.2-14m
Obsoletes: xfmedia
Obsoletes: xfmedia-devel
Obsoletes: xfmedia-plugins
#skip Requires: xfmedia-remote-plugin >= 0.2.2-12m
Obsoletes: xfmedia-remote-plugin

################################################

Requires: gtk2 >= 2.24.19-1m
# Requires: rxvt
# Requires: sdr

Requires: cairo >= 1.12.16-1m
Requires: dbus >= 1.6.18-1m
Requires: dbus-glib >= 0.98-1m
Requires: gstreamer >= 0.10.36-3m
Requires: gstreamer-plugins-base >= 0.10.36-3m
Requires: keybinder >= 0.2.2-2m
Requires: libnotify >= 0.7.5-1m
Requires: libglade2 >= 2.6.4-8m
Requires: libwnck >= 2.30.7-2m
Requires: startup-notification >= 0.12-2m
Requires: xorg-x11-xrandr >= 1.4.2-1m
Requires: xorg-x11-xinit >= 1.3.2-1m

Obsoletes: xfce

####

## BUILD order: (up to down, left to right)
# gtk-xfce-engine, dbh
# libxfce4util -> xfconf -> libxfcegui4
## libxfce4ui (will replace libxfcegui4)
#
# exo -> libxfce4ui -> xfce4-settings -> xfce4-session 
# (libxfce4menu is obsoleted in 4.8, we Obsoletes: by xfce4-settings)
#
# xfwm4 , garcon -> xfce4-panel 
# Thunar, thunar-vfs -> xfdesktop -> xfce-utils
# xfce4-appfinder
# xfce4-dev-tools (noarch)
#
# xfprint[obso?]
# xfce4-mixer[obso?]
# xfwm4-themes (noarch)
# xfwm4-theme-nodoka (noarch)
# orage
# Terminal
# mousepad
# xfmpc
# gigolo
# midori
# waf (noarch)
# ristretto
# xfbib
# xfburn
 
# many plugins
# (left over if exist)
# xfce4 (this meta package) myself
 
%description
This is a meta package for XFce4.

%files

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to xfce4-4.10.0

* Wed Mar 21 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-6m)
- update exo libxfce4ui and xfce-utils

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.0-5m)
- remove BR hal

* Tue Oct 18 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-4m)
- update thunarver 1.3.0
- update terminalver 0.4.8

* Thu Jun  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8.0-3m)
- remove Obsoletes: libxfce4menu
- libxfce4menu is obsoleted by xfce4-settings

* Wed Jun  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-2m)
- obsoletes

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update to xfce 4.8

* Sun May 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.2-5m)
- comment out Req for xfce4-xfapplet-plugin (Fix me)

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-0.1m)
- tmp entry
- update to xfce 4.8pre2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update to 4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.1-2m)
- Req change from devel to core
-- hal-devel startup-notification-devel libwnck-devel cairo-deve

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update to 4.6.1

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update to 4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update to 4.5.93

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update to 4.5.92

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update to 4.5.91

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update to 4.5.90

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- update to 4.4.2
- add new applications
- add Requires: xfce4-smart-plugin (OBSOLETE by xfce4-smartpm-plugin)
- add Requires: notification-daemon-xfce >= 0.3.7-1m and comment out by conflict

* Sat Apr 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-2m)
- update xfprint to 4.4.1

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1
- add release nunmber except use %%version

* Wed Jan 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-2m)
- remove Obsoletes section, it's in xfce4-panel.spec

* Tue Jan 30 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- delete Obsolete: xffm
  xffm is provide by Thunar now
- delete Obsoletes: xfcalendar
  xfcalendar is provide by orage now
- add new plugin packages

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-0.2m)
- add Obsoletes packages
- delete xfce4-extras -> separate xfce4-mixer and xfwm4-themes
- delete xfce4-artwork

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-0.1m)
- update to 4.4.0
- comment out Requires: dbh
- comment out Requires: xffm
- comment out Requires: xfcalendar (xfce4-extras)
- comment out Requires: xfce4-mixer (xfce4-extras)
- comment out Requires: xfce4-systray (xfce4-extras)
- comment out Requires: xfce4-toys (xfce4-extras)
- comment out Requires: xfce4-trigger-launcher (xfce4-extras)
- comment out Requires: xfce4-iconbox (xfce4-extras)
- add Requires: exo >= 0.3.2
- add Requires: Terminal >= 0.2.6
- add Requires: xfce4-dev-tools >= %{version}
- add Requires: Thunar >= 0.8.0
- revive dbh, dbh was depend by orage

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.3.2-2m)
- change Requires: xinitrc -> xorg-x11-xinit

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3.2-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Wed Mar 23 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.1.1-2m)
- xfce4-artwork-0.0.4-8m, xfce4-taskbar-plugin-0.2.2-4m

* Sat Mar 19 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.1.1-1m)
- xfce-panel 4.2.1.1

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.1-1m)
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.0-1m)
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.2BETA1

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)
- minor bugfixes

* Sun Jun 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.0.5-2m)
- add new plugins from xfce-goodies

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.5-1m)
- version update to 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-3m)
- rebuild against for libxml2-2.6.8

* Fri Mar 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.4-2m)
- add Requires: rxvt for xfterm4 script

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.4-1m)

* Sat Mar 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3.1-2m)
- add Requires: xinitrc >= 3.20-28m

* Tue Jan 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3.1-1m)
- version up to 4.0.3.1

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- version up to 4.0.3

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- sort

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Wed Dec 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-2m)
- add Requires: xfce4-fsguard-plugin >= 0.2.0-1m
- add Requires: xfce4-xmms-plugin >= 0.1.1-1m

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.0.0-4m)
- expand Requires: xfce4-extras to
  xfce4-themes, xffm-icons, xfwm4-themes, xfce4-mixer, xfce4-systray,
  xfce4-toys, xfce4-trigger-launcher, xfce4-iconbox, and xfcalendar

* Wed Oct 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-3m)
- add Requires: gtk-xfce-engine
- add Requires: xfprint
- expand Requires: xfce4-extras to
  xfce4-themes, xffm-icons, xfwm4-themes, xfce4-mixer, xfce4-systray,
  xfce4-toys, xfce4-trigger-launcher, xfce4-iconbox, and xfcalendar

* Wed Oct 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-2m)
- add Requires: xfce4-extras
- add Requires: xffm
- add Requires: xfce-utils
- add Requires: xfce4-diskperf-plugin
- add Requires: xfce4-clipman-plugin
- add Requires: xfce4-notes-plugin
- add Requires: xfce4-datetime-plugin

* Mon Oct 06 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-1m)
- initial version
