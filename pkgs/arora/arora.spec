%global momorel 4

Name:           arora
Version:        0.11.0
Release:        %{momorel}m%{?dist}
Summary:        A cross platform web browser

Group:          Applications/Internet
License:        GPLv2+
URL:            http://code.google.com/p/arora/
Source0:        http://arora.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  qt-devel >= 4.7.0
BuildRequires:  qt-webkit-devel

Obsoletes: %{name}-gnome

%description
Arora is a simple, cross platform web browser based on the QtWebKit engine.
Currently, Arora is still under development, but it already has support for
browsing and other common features such as web history and bookmarks.


%prep
%setup -q

%build
qmake-qt4 PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make INSTALL_ROOT=$RPM_BUILD_ROOT install 

# imo, shouldn't add/change upstream vendor here, but changing
# once released this way is bad too. :(  -- Rex
desktop-file-install --vendor= \
      --dir $RPM_BUILD_ROOT%{_datadir}/applications\
      --delete-original\
      $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop


%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  update-desktop-database -q &> /dev/null
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q -t -f &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :


%files
%defattr(-,root,root,-)
%doc AUTHORS README ChangeLog
%doc LICENSE.GPL2 LICENSE.GPL3
%{_bindir}/arora
%{_bindir}/arora-placesimport
%{_bindir}/htmlToXBel
%{_bindir}/arora-cacheinfo
%{_datadir}/applications/arora.desktop    
%{_datadir}/icons/hicolor/128x128/apps/arora.png
%{_datadir}/icons/hicolor/16x16/apps/arora.png
%{_datadir}/icons/hicolor/32x32/apps/arora.png
%{_datadir}/icons/hicolor/scalable/apps/arora.svg
%{_datadir}/arora/
%{_datadir}/pixmaps/arora.xpm
%{_mandir}/man1/*

%changelog
* Sun May  8 2011 Nishio Futosh <fotoshi@momonga-linux.org>
- (0.11.0-4m)
- good-bye gnome

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.0-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.0-1m)
- update to 0.11.0

* Mon Sep 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-7m)
- fix build error with qt-4.7 again

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.2-6m)
- full rebuild for mo7 release

* Fri Aug 20 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.2-5m)
- use BuildRequires: gnome-control-center-devel
- use Requires: gnome-control-center

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-4m)
- rebuild against qt-4.6.3-1m

* Mon May 10 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-3m)
- fix build failure with qt-4.7
- add BuildReq

* Tue Dec  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.2-2m)
- remove duplicate directory

* Mon Dec  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.2-1m)
- import from Rawhide
-- drop all patches

* Wed Nov 18 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.10.1-4
- Build over latest qtwebkit trunk from qt 4.6-rc1

* Wed Nov 18 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.10.1-3
- respin against newer qt

* Thu Oct 15 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.10.1-2
- add dep on qt4 version built against

* Tue Oct 06 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.10.1-1
- Update to 0.10.1

* Fri Oct 02 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.10.0-2
- Fedorahome patch rebased

* Fri Oct 02 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.10.0-1
- Update to 0.10.0

* Sun Sep 30 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.9.0-2
- add icon/mime scriptlets
- don't own %%{_mandir}/man1
- BR: qt4-devel

* Mon Jul 31 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.9.0-1
- Update to 0.9.0

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 21 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.8.0-1
- Update to 0.8.0
- Removed custom arora.xml

* Thu Jul 16 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.7.1-3
- Arora-gnome subpackage now requires Arora package

* Tue Jun 09 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.7.1-2
- Adds arora-gnome subpackage to support preferred app selection
  in Gnome

* Mon Jun 01 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.7.1-1
- Update to 0.7.1
- Parallel build reenabled, fixed in 0.7.1

* Mon May 25 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.7.0-1
- Update to 0.7.0
- Googlesuggest removed

* Sat May 09 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.6.1-2
- Correct Changelog date

* Sat Apr 09 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.6.1-1
- Update to 0.6.1

* Mon Mar 30 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.6-2
- Add arora-cacheinfo to package

* Mon Mar 30 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.6-1
- Update to 0.6

* Tue Feb 24 2009 Jaroslav Reznik <jreznik@redhat.com> - 0.5-1
- Update to 0.5
- SPEC file cleanup

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec 02 2008 Jaroslav Reznik <jreznik@redhat.com> - 0.4-3
- owns arora and locale directories (bz#473621)

* Mon Oct 06 2008 Jaroslav Reznik <jreznik@redhat.com> - 0.4-2
- Fedora Project & Red Hat bookmarks
- Fedora Project homepage
- GIT version patch

* Wed Oct 01 2008 Jaroslav Reznik <jreznik@redhat.com> - 0.4-1
- Updated to version 0.4

* Thu Sep 25 2008 Jaroslav Reznik <jreznik@redhat.com> - 0.3-2
- Location bar's double click selects all

* Wed Aug 13 2008 Jaroslav Reznik <jreznik@redhat.com> - 0.3-1  
- Initial spec file 
