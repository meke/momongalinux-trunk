%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-sexplib
Version:        6.1.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for converting OCaml values to S-expressions

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions and BSD"
URL:            http://www.ocaml.info/home/ocaml_sources.html#sexplib310
Source0:        http://hg.ocaml.info/release/sexplib310/archive/release-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-type-conv >= 3.0.1-1m
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  dos2unix


%description
This library contains functionality for parsing and pretty-printing
S-expressions. In addition to that it contains an extremely useful
preprocessing module for Camlp4, which can be used to automatically
generate code from type definitions for efficiently converting
OCaml-values to S-expressions and vice versa. In combination with the
parsing and pretty-printing functionality this frees users from having
to write their own I/O-routines for datastructures they
define. Possible errors during automatic conversions from
S-expressions to OCaml-values are reported in a very human-readable
way. Another module in the library allows you to extract and replace
sub-expressions in S-expressions.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n sexplib310-release-%{version}
dos2unix LICENSE.Tywith


%build
make


%check
./lib_test/conv_test
./lib_test/sexp_test < lib_test/test.sexp


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE LICENSE.Tywith
%{_libdir}/ocaml/sexplib
%if %opt
%exclude %{_libdir}/ocaml/sexplib/*.a
%exclude %{_libdir}/ocaml/sexplib/*.cmxa
%endif
%exclude %{_libdir}/ocaml/sexplib/*.mli
%exclude %{_libdir}/ocaml/sexplib/*.ml


%files devel
%defattr(-,root,root,-)
%doc LICENSE LICENSE.Tywith Changelog COPYRIGHT README.txt
%if %opt
%{_libdir}/ocaml/sexplib/*.a
%{_libdir}/ocaml/sexplib/*.cmxa
%endif
%{_libdir}/ocaml/sexplib/*.mli
%{_libdir}/ocaml/sexplib/*.ml


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.1-1m)
- update to 6.1.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.17-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.17-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.17-1m)
- update to 4.2.17
- rebuild against ocaml-3.11.2
- rebuild against ocaml-type-conv-1.6.10-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.11-1m)
- update to 4.2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.10-1m)
- update to 4.2.10
- rebuild against ocaml-type-conv-1.6.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2
-- import Fedora devel changes (4.0.1-1)
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.4-1m)
- import from Fedora

* Mon May 10 2008 Richard W.M. Jones <rjones@redhat.com> - 3.7.4-2
- Added BR ocaml-camlp4-devel.
- Added a check section to run the included tests.

* Sat May  3 2008 Richard W.M. Jones <rjones@redhat.com> - 3.7.4-1
- New upstream version 3.7.4.

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 3.7.1-1
- New upstream version 3.7.1.
- Fixed upstream URL.
- Depend on latest type-conv.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 3.5.0-2
- Remove ExcludeArch ppc64.

* Sun Feb 24 2008 Richard W.M. Jones <rjones@redhat.com> - 3.5.0-1
- Initial RPM release.
