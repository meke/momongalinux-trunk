%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global strigiver 0.7.8

Name: kde-dev-scripts
Summary: KDE SDK scripts
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
Requires: kde-workspace >= %{version}
Requires: advancecomp
Requires: optipng
BuildRequires:  kdelibs-devel >= %{version}
BuildRequires:  kdepimlibs-devel >= %{version}
BuildRequires:  kde-workspace-devel >= %{version}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  antlr-tool
BuildRequires:  boost-devel >= 1.50.0
BuildRequires:  flex
BuildRequires:  gettext-devel >= 0.18.2
BuildRequires:  apr-devel
BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
BuildRequires:  libtool-ltdl-devel
BuildRequires:  qca2-devel
BuildRequires:  strigi-devel >= %{strigiver}
BuildRequires:  subversion-devel >= 1.6.3-2m
BuildRequires:  antlr-C++
# for libiberty (used by kmtrace for cp_demangle)
# IMPORTANT: check licensing from time to time, currently libiberty is still
#            GPLv2+/LGPLv2+
BuildRequires:  binutils-devel
BuildArch: noarch

Obsoletes: kdesdk-scripts < %{version}-%{release}
Conflicts: kdesdk < 4.10.90

%description
Scripts and setting files useful during development of KDE software.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

# unpackaged files
# This one fits better into krazy2 (it requires krazy2), and the version in
# kdesdk does not understand lib64.
rm -f %{buildroot}%{_kde4_bindir}/krazy-licensecheck

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING* README
%{_kde4_bindir}/svnrevertlast
%{_kde4_bindir}/fixuifiles
%{_kde4_bindir}/cvscheck
%{_kde4_bindir}/extend_dmalloc
%{_kde4_bindir}/extractattr
%{_kde4_bindir}/noncvslist
%{_kde4_bindir}/pruneemptydirs
%{_kde4_bindir}/cvsrevertlast
%{_kde4_bindir}/create_makefile
%{_kde4_bindir}/colorsvn
%{_kde4_bindir}/cvslastchange
%{_kde4_bindir}/svngettags
%{_kde4_bindir}/create_svnignore
%{_kde4_bindir}/svnchangesince
%{_kde4_bindir}/build-progress.sh
%{_kde4_bindir}/package_crystalsvg
%{_kde4_bindir}/svnbackport
%{_kde4_bindir}/svnlastlog
%{_kde4_bindir}/cxxmetric
%{_kde4_bindir}/kdemangen.pl
%{_kde4_bindir}/cvsforwardport
%{_kde4_bindir}/includemocs
%{_kde4_bindir}/svnlastchange
%{_kde4_bindir}/wcgrep
%{_kde4_bindir}/qtdoc
%{_kde4_bindir}/nonsvnlist
%{_kde4_bindir}/svnforwardport
%{_kde4_bindir}/create_cvsignore
%{_kde4_bindir}/svnintegrate
%{_kde4_bindir}/kdekillall
%{_kde4_bindir}/create_makefiles
%{_kde4_bindir}/cvsbackport
%{_kde4_bindir}/fixkdeincludes
%{_kde4_bindir}/kde-systemsettings-tree.py
%{_kde4_bindir}/zonetab2pot.py
%{_kde4_bindir}/kde_generate_export_header
%{_kde4_bindir}/cvs-clean
%{_kde4_bindir}/kdelnk2desktop.py
%{_kde4_bindir}/findmissingcrystal
%{_kde4_bindir}/adddebug
%{_kde4_bindir}/cvsversion
%{_kde4_bindir}/cheatmake
%{_kde4_bindir}/cvsblame
%{_kde4_bindir}/optimizegraphics
%{_kde4_bindir}/cvsaddcurrentdir
%{_kde4_bindir}/fix-include.sh
%{_kde4_bindir}/kdedoc
%{_kde4_bindir}/svn-clean
%{_kde4_bindir}/png2mng.pl
%{_kde4_bindir}/extractrc
%{_kde4_bindir}/makeobj
%{_kde4_bindir}/cvslastlog
%{_kde4_bindir}/svnversions
%{_kde4_bindir}/draw_lib_dependencies
%{_kde4_bindir}/reviewboard-am
%{_mandir}/man1/adddebug.1.*
%{_mandir}/man1/cheatmake.1.*
%{_mandir}/man1/create_cvsignore.1.*
%{_mandir}/man1/create_makefile.1.*
%{_mandir}/man1/create_makefiles.1.*
%{_mandir}/man1/cvscheck.1.*
%{_mandir}/man1/cvslastchange.1.*
%{_mandir}/man1/cvslastlog.1.*
%{_mandir}/man1/cvsrevertlast.1.*
%{_mandir}/man1/cxxmetric.1.*
%{_mandir}/man1/extend_dmalloc.1.*
%{_mandir}/man1/extractrc.1.*
%{_mandir}/man1/fixincludes.1.*
%{_mandir}/man1/pruneemptydirs.1.*
%{_mandir}/man1/qtdoc.1.*
%{_mandir}/man1/reportview.1.*
%{_mandir}/man1/transxx.1.*
%{_mandir}/man1/zonetab2pot.py.1.*

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- initial build for Momonga Linux
