%global momorel 1
%global kdever 4.7.90
%global kdelibsrel 1m
%global qtver 4.8.0
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global srcname polkit-qt-1
%global sourcedir stable/apps/KDE4.x/admin
#global date 20101120

Summary: PolicyKit-qt library
Name: polkit-qt
Version: 0.103.0
Release: %{?date:0.%{date}.}%{momorel}m%{?dist}
License: GPLv2
URL: http://www.kde.org/
Group: System Environment/Libraries
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{srcname}-%{version}%{?date:-%{date}}.tar.bz2
NoSource: 0
Source1: Doxyfile
Source10: macros.polkit-qt
Patch0: polkit-qt-0.95.1-install-cmake-find.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automoc4
BuildRequires: qt-devel >= %{qtver}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: polkit-devel >= 0.98

%description
Polkit-qt is a library that lets developers use the PolicyKit API through a nice
Qt-styled API. It is mainly a wrapper around QAction and QAbstractButton that
lets you integrate those two component easily with PolicyKit

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the libraries, include files and other resources
needed to develop applications using %{name}.

%package doc
Summary: Doxygen documentation for the PolkitQt API
Group: Documentation
BuildArch: noarch

%description doc
%{summary}.

%prep
%setup -q -n %{srcname}-%{version}
# temporary patch - installs FindPolkitQt-1.cmake until we decide how to deal with cmake 
# module installation
%patch0 -p1 -b .install-cmake-find

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} \
    -DBUILD_EXAMPLES:BOOL=0 \
    -DDATA_INSTALL_DIR:PATH=%{_datadir} \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

doxygen %{SOURCE1}
# Remove installdox file - it is not necessary here
rm html/installdox

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}
install -p -m644 -D %{SOURCE10} %{buildroot}%{_sysconfdir}/rpm/macros.polkit-qt

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/libpolkit-qt-agent-1.so.*
%{_libdir}/libpolkit-qt-core-1.so.*
%{_libdir}/libpolkit-qt-gui-1.so.*

%files devel
%defattr(-,root,root,-)
%{_sysconfdir}/rpm/macros.polkit-qt
%{_includedir}/polkit-qt-1
%{_libdir}/cmake/PolkitQt-1
%{_libdir}/libpolkit-qt-agent-1.so
%{_libdir}/libpolkit-qt-core-1.so
%{_libdir}/libpolkit-qt-gui-1.so
%{_libdir}/pkgconfig/polkit-qt*.pc
%{_datadir}/cmake/Modules/*.cmake

%files doc
%defattr(-,root,root,-)
%doc html/*

%changelog
* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.103.0-1m)
- update to 0.103.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.0-2m)
- rebuild for new GCC 4.6

* Mon Dec 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.0-1m)
- update to 0.99.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98.1-0.20101120.2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.1-0.20101120.1m)
- update to 0.98.1 (20101120 snapshot)

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96.1-4m)
- specify PATH for Qt4

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96.1-3m)
- fix build failure

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96.1-2m)
- update patch1

* Sun Oct 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96.1-1m)
- update to 0.96.1
- use %%{cmake} macro instead of %%{cmake_kde4}

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95.1-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.95.1-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95.1-4m)
- rebuild against qt-4.6.3-1m

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95.1-3m)
- use BuildRequires

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95.1-2m)
- import patch0 from Fedora devel
-- Installs FindPolkitQt-1.cmake

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95.1-1m)
- update to 0.95.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- initial package for Momonga Linux
