%global momorel 1
Name:           seahorse-sharing
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        Sharing of PGP public keys via DNS-SD and HKP
# daemon is GPLv2+
# libegg is LGPLv2+
License:        GPLv2+ and LGPLv2+
Group:		User Interface/Desktops
URL:            https://live.gnome.org/Seahorse
Source0:        http://ftp.gnome.org/pub/gnome/sources/seahorse-sharing/3.6/%{name}-%{version}.tar.xz
NoSource: 0

Provides:       bundled(egglib)

BuildRequires:  gtk3-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gnupg2
BuildRequires:  gpgme-devel >= 1.0
BuildRequires:  libsoup-devel
BuildRequires:  avahi-glib-devel
BuildRequires:  intltool
BuildRequires:  libSM-devel

Obsoletes: seahorse < 3.1.4

%description
This package ships a session daemon that allows users to share PGP public keys
via DNS-SD and HKP.


%prep
%setup -q


%build
%configure
%make 


%install
make install DESTDIR=%{buildroot}

desktop-file-validate %{buildroot}%{_sysconfdir}/xdg/autostart/%{name}.desktop

%find_lang %{name} --with-gnome


%files -f %{name}.lang
%doc AUTHORS COPYING NEWS README
%{_sysconfdir}/xdg/autostart/%{name}.desktop
%{_bindir}/%{name}
%{_datadir}/pixmaps/seahorse/
%{_mandir}/man1/%{name}.1.bz2


%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update 3.6.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- initial build

