%global momorel 1
%global rbname narray

Summary: Ruby/NArray - Fast and easy calclation for large numerical array
Name: ruby-%{rbname}
Version: 0.6.0.1
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPLv2+ or Ruby
URL: http://narray.rubyforge.org/
# Download github source
# tar jxvf masa16-narray-XXXXXXXXXXX.tar.gz
# mv  masa16-narray-XXXXXXXXXXX narray-%{version}
# tar cfJ narray-%{version}.tar.xz narray-%{version}
Source0: narray-%{version}.tar.xz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2, fftw-devel

%description
+ Fast and easy calclation for large numerical array.
+ Accepting Elements:
  8,16,32 bit integer, single/double float/complex, Ruby Object.
+ Easy extraction/substitution of array subset,
  using assignment with number, range, array index.
+ Operator: +,-,*,/,%,**, etc.
+ FFTW interface.
+ NImage: Image viewer class.

%prep
%setup -q -n narray-%{version}

%build
ruby extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc ChangeLog README* SPEC* test
%{ruby_sitelibdir}/narray_ext.rb
%{ruby_sitelibdir}/nmatrix.rb
%{ruby_sitearchdir}/narray.so
%{ruby_sitearchdir}/narray.h
%{ruby_sitearchdir}/narray_config.h

%changelog
* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0.1-1m)
- update 0.6.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.9p7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.9p7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.9p7-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.9p7-1m)
- update 0.5.9p7

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.9p6-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.9p6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.9p6-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.9p6-1m)
- update to 0.5.9p6
-- why is nimage missing?
- change URL and License

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.9-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.9-3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.9-2m)
- rebuild against ruby-1.8.6-4m

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5.9-1m)
- update to 0.5.9

* Tue Jun 28 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.7-6m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.7-5m)
- version up to 0.5.7p3
- rebuild against ruby-1.8.2

* Thu May 20 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.7-4m)
- miner verup

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.7-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.7-2m)
- rebuild against ruby-1.8.0.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.7-1m)
- version up.

* Sun Nov 24 2002 Kenta MURATA <muraken@momonga-linux.org>
- (0.5.6p2-1m)
- version 0.5.6p2 ([Momonga-devel.ja:00846]; thanks KOZUKA).

* Tue Mar 26 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.5.6p1-2k)
- version 0.5.6p1.

* Tue Mar 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5.6-2k)

* Sun Nov  9 2001 Kenta MURATA <muraken2@nifty.com>
- (0.5.4-2k)
- verion 0.5.4.

* Sun Apr 1 2001 SAKUMA Junichi <fk5j-skm@asahi-net.or.jp>
- (0.5.3-5k)
- version 0.5.3
- revised spec file.
- revised patch file.

* Fri Jan 19 2001 Toru Hoshina <toru@df-usa.com>
- (0.3.5.p1-5k)
- revised spec file.

* Fri Nov  3 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.3.5.p1-1k)
- first release.
