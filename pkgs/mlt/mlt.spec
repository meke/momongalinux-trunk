%global momorel 2
%global qtver 4.8.0
%global qtlib %{_libdir}
%global qtinclude %{_includedir}

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Mutton Lettuce Tomato Nonlinear Video Editor
Name: mlt
Version: 0.9.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.mltframework.org/twiki/bin/view/MLT/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.4.2-sox1430.patch
Patch1: %{name}-%{version}-freetype.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick-devel >= 6.4.2.1
BuildRequires: SDL-devel
BuildRequires: coreutils
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: freetype-devel >= 2.5.3
BuildRequires: jack-devel
BuildRequires: ladspa-devel
BuildRequires: lame-devel
BuildRequires: libdv-devel
BuildRequires: libmad
BuildRequires: libquicktime-devel
BuildRequires: libsamplerate-devel
BuildRequires: libvorbis-devel
BuildRequires: libxml2-devel
BuildRequires: pango-devel
BuildRequires: perl
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ruby-devel
BuildRequires: sox-devel >= 14.3.0
Provides: %{name}++ = %{version}-%{release}
Obsoletes: %{name}++ < %{version}-%{release}

%description
Nonlinear Video Editor.

%package devel
Summary: MLT Video Editor headers/development files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Provides: %{name}++-devel = %{version}-%{release}
Obsoletes: %{name}++-devel < %{version}-%{release}

%description devel
Headers for MLT.

%package python
Summary: Python Bindings for MLT Video Editor
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description python
Python bindings for MLT Video Editor.

%prep
%setup -q

%patch0 -p1 -b .sox1430
%patch1 -p1 -b .freetype

%build
%configure \
	--enable-gpl \
	--enable-gpl3 \
	--luma-compress \
	--enable-avformat \
	--avformat-shared=%{_prefix} \
	--avformat-swscale \
	--enable-motion-est \
	--qimage-libdir=%{qtlib} \
	--qimage-includedir=%{qtinclude} \
	--rename-melt=melt-%{name} \
	--disable-debug \
	--disable-qimage \
	--swig-languages=python

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install man file
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 docs/melt.1 %{buildroot}%{_mandir}/man1/melt-%{name}.1

# python
%{__mkdir_p} %{buildroot}%{python_sitelib}
%{__mkdir_p} %{buildroot}%{python_sitearch}
install -m 644 src/swig/python/%{name}.py %{buildroot}%{python_sitelib}
install -m 755 src/swig/python/_%{name}.so %{buildroot}%{python_sitearch}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog GPL NEWS README
%{_bindir}/melt-%{name}
%{_libdir}/%{name}
%{_libdir}/lib%{name}++.so.*
%{_libdir}/lib%{name}.so.*
%{_mandir}/man1/melt-%{name}.1*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root)
%doc docs/*.txt
%{_includedir}/%{name}
%{_includedir}/%{name}++
%{_libdir}/pkgconfig/%{name}++.pc
%{_libdir}/pkgconfig/%{name}-framework.pc
%{_libdir}/lib%{name}++.so
%{_libdir}/lib%{name}.so

%files python
%defattr(-,root,root)
%{python_sitelib}/%{name}.py*
%{python_sitearch}/_%{name}.so

%changelog
* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-2m)
- enable to build with freetype-2.5.3

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-1m)
- version 0.9.0

* Mon Jan 28 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.8-1m)
- version 0.8.8
-- FIXME disabled-qimage module 

* Wed Oct 24 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.7.8-4m)
- rebuild for sox 14.4.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.8-3m)
- rebuild for glib 2.33.2

* Sat Mar 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.8-2m)
- fix ffmpeg version check

* Sat Mar 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.8-1m)
- version 0.7.8

* Sat Jan 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.6-2m)
- build fix for x86_64 (workaround. FIX ME!!)

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.6-1m)
- version 0.7.6

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-2m)
- rebuild against ffmpeg-0.7.0-0.20110705

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- version 0.7.2

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-2m)
- rebuild against python-2.7

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- version 0.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.10-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.10-2m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.10-1m)
- version 0.5.10

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.6-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.6-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.6-1m)
- version 0.5.6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-2m)
- rebuild against qt-4.6.3-1m

* Thu Feb 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0

* Sun Feb 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.6-4m)
- enable python bindings

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-2m)
- set BuildRequires: ffmpeg-devel >= 0.5.1-1.20091011.1m
  for automatic building of STABLE_6

* Sat Oct 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- version 0.4.6

* Tue Jul 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-1m)
- version 0.4.4

* Sun Jul  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-5m)
- rebuild against sox-14.3.0

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-4m)
- version fix sox-devel = 14.2.0

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-3m)
- rebuild against ffmpeg-0.5.1-0.20090622

* Thu Jun  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-2m)
- move melt to melt-mlt (to avoid conflicting with freeze-2.5.0)

* Wed Jun  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-1m)
- version 0.4.2 including mlt++, good-bye separate mlt++

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.8-2m)
- rebuild against sox

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.8-1m)
- version 0.3.8
- remove noO3.patch

* Tue Feb  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-1m)
- version 0.3.6
- update noO4.patch to noO3.patch
- remove ppc.patch
- remove merged avcodec.patch and sox1410.patch
- re-apply sox-header.patch
- build qimage module
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-1m)
- update 0.3.0
- rebuild against sox-14.1.0

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-0.20080123.10m)
- rebuild against ImageMagick-6.4.2.1

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-0.20080123.9m)
- rebuild against sox-14.0.1

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-0.20080123.8m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.5-0.20080123.7m)
- rebuild against gcc43

* Sat Mar 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-0.20080123.6m)
- kdenlive crash fix

* Tue Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-0.20080123.5m)
- fix gcc-4.3

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-0.20080123.4m)
- BuildRequires: lame-devel

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-0.20080123.3m)
- build with libquicktime

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-0.20080123.2m)
- disable mmx ifnarch ix86

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-0.20080123.1m)
- initial package for kdenlive-0.5
- import tar-ball and patches from cooker
