%global momorel 8

Summary: ttyrec is a tty recorder.
Name: ttyrec
Version: 1.0.6
Release: %{momorel}m%{?dist}
Source0: http://namazu.org/~satoru/ttyrec/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://namazu.org/~satoru/ttyrec/
License: BSD
Group: Applications/Text
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ttyrec is a tty recorder. Recorded data can be played back with the included 
ttyplay command. It can record emacs -nw, vi, lynx, 
or any programs running on tty.

%prep
%setup -q

%build
make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_bindir}
%__mkdir_p %{buildroot}%{_mandir}/man1
%__install -m 755 ttyrec  %{buildroot}%{_bindir}
%__install -m 755 ttyplay %{buildroot}%{_bindir}
%__install -m 755 ttytime %{buildroot}%{_bindir}
%__install -m 644 ttyrec.1 %{buildroot}%{_mandir}/man1
%__install -m 644 ttyplay.1 %{buildroot}%{_mandir}/man1

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/ttyrec
%{_bindir}/ttyplay
%{_bindir}/ttytime
%{_mandir}/man1/*
%doc README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-3m)
- rebuild against gcc43

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- fix %%changelog section

* Tue Oct 22 2002 kitaj <kita@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Fri Oct 18 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (1.0.5-5m)
- Added skip patch.
  (http://pc.2ch.net/test/read.cgi/unix/1022864341/776 via yendot)
- Do not strip during %%install, rpm does it.

* Sun Mar 10 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.0.5-4k)
- Applied my command option(-e) patch:-)
- Fixed the date for 1.0.5-2k in %changelog.
- Stripped binaries.

* Sat Mar  6 2002 kitaj <kita@kitaj.no-ip.com>
- (1.0.5-2k)
- Jirai to STABLE

* Sat Feb 16 2002 kitaj <kita@kitaj.no-ip.com>
- (1.0.5-3k)

* Sun Feb 10 2002 kitaj <kita@kitaj.no-ip.com>
- initial build
