%global momorel 1

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global xfcever 4.10
%global xfce4ver 4.10.0

Summary:	Application library for the Xfce desktop environment
Name:		exo
Version:	0.10.2
Release:	%{momorel}m%{?dist}
# libexo-hal exo-helper mount-notify and exo-mount are all GPLv2+
# everything else is LGPLv2+
License:        LGPLv2+ and GPLv2+
URL:		http://xfce.org/
#Source0:	http://archive.xfce.org/xfce/%{xfcever}/src/%{name}-%{version}.tar.bz2
Source0:	http://archive.xfce.org/src/xfce/%{name}/0.10/%{name}-%{version}.tar.bz2
NoSource:	0
# internet-mail icon taken from GNOME, license is LGPLv3
Source1:        internet-mail-24.png
Source2:        internet-mail-48.png
# FIXME: upstream this patch
#Patch0:         exo-0.5.4-x86_64-build.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=748277
#Patch1:         exo-0.6.2-thunar-as-default.patch
#Patch2:		exo-0.3.107-linbotify-0.7.2.patch
Group:		Development/Libraries
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: dbus-devel >= 1.2.4
BuildRequires: gettext >= 0.17
BuildRequires: gtk-doc
BuildRequires: imake
BuildRequires: intltool >= 0.31
BuildRequires: libXt-devel
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: libxfce4util-devel >= %{xfce4ver}
BuildRequires: libxfce4ui-devel >= 4.10.0-1m
BuildRequires: perl-URI
BuildRequires: pygobject-devel >= 2.28.4
BuildRequires: pygtk2-devel
BuildRequires: python-devel >= 2.7
BuildRequires: xfconf-devel >= %{xfce4ver}

Requires: dbus-glib >= 0.76
Requires: libxfce4ui

Obsoletes: python-exo

%description
Extension library for Xfce, targeted at application development.

%package devel
Summary: Development tools for exo library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libxfce4util-devel >= %{xfce4ver}
BuildRequires: libxfce4ui-devel >= 4.9.0-1m
Requires: pkgconfig

%description devel
Development tools and static libraries and header files for the exo library.

%package -n python-exo
Summary: Python tools for exo library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description -n python-exo
Python libraries and header files for the exo library.


%prep
%setup -q
#%%patch0 -p1 -b .x86_64-build
#%%patch1 -p1 -b .thunar-as-default

%build
%configure --enable-gtk-doc --disable-static
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,' INSTALL='install -p'
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

chrpath --delete $RPM_BUILD_ROOT%{_bindir}/exo-desktop-item-edit
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/exo-open
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/xfce4/exo-*/exo-helper-*
#tmp chrpath --delete $RPM_BUILD_ROOT%{_libdir}/python*/site-packages/exo-*/_exo.so
#tmp chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gio/modules/libexo-module-*.so

%find_lang exo-1


for file in $RPM_BUILD_ROOT%{_datadir}/applications/%{name}-*.desktop ; do
   perl -pi -e 's|Categories=XFCE;|Categories=|' $file
   desktop-file-validate $file
done

install -Dpm 0644 %{SOURCE1} ${RPM_BUILD_ROOT}%{_datadir}/icons/hicolor/24x24/apps/internet-mail.png
install -Dpm 0644 %{SOURCE2} ${RPM_BUILD_ROOT}%{_datadir}/icons/hicolor/48x48/apps/internet-mail.png

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor || :


%postun
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f exo-1.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS README THANKS COPYING
#%%doc %{_docdir}/exo/
#%%doc %{_docdir}/exo-%{version}/
# AUTHORS  COPYING  ChangeLog  NEWS  README  THANKS
%dir %{_sysconfdir}/xdg/xfce4
%config(noreplace) %{_sysconfdir}/xdg/xfce4/helpers.rc
%{_bindir}/exo-csource
%{_bindir}/exo-desktop-item-edit
%{_bindir}/exo-open
%{_bindir}/exo-preferred-applications
%{_libdir}/lib*.so.*
%{_libdir}/xfce4/
#%%{_libdir}/gio/modules/libexo-module-*.so
%{_datadir}/xfce4/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/pixmaps/exo-*/
%{_mandir}/man1/exo-open.1.*

%files devel
%defattr(-,root,root,-)
%doc TODO
%doc %{_datadir}/gtk-doc
%{_includedir}/exo*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
#%%{_datadir}/pygtk/*/defs/exo-*/
%{_mandir}/man1/exo-csource.1.*

#%%files -n python-exo
#%%defattr(-, root, root,-)
#%%{python_sitearch}/exo-*/
#%%{python_sitearch}/pyexo.*


%changelog
* Wed Feb 20 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Fri Dec  7 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Thu Sep  6 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.0-1m)
- update to version 0.8.0
- build against xfce4-4.10.0

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-2m)
- fix %%files to avoid conflicting

* Wed Mar 21 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-1m)
- update

* Wed Mar 21 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-1m)
- update

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-1m)
- update
- tmp comment out Patch2: exo-0.3.107-linbotify-0.7.2.patch
- use exo-pyver but cat not build and rollback at python-exo

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.107-7m)
- rebuild against python-2.7

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-1m)
- update
- tmp comment out Patch0:		%{name}-%{version}-x86_64-build.patch
- tmp comment out Patch1:         %{name}-%{version}-autoconf.patch
- tmp comment out %%find_lang part
- fix %%files

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.107-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.107-5m)
- rebuild for new GCC 4.6

* Sun Mar 06 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.107-4m)
- add BR libnotify-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.107-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.107-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.107-1m)
- update

* Mon Apr 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.101-6m)
- add patch1 for autoconf

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.101-5m)
- delete __libtoolize hack

* Mon Dec 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.101-4m)
- fix build on x86_64

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.101-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.101-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.101-1m)
- update
- rebuild against xfce4-4.6.1

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.100-1m)
- update
- rebuild against xfce4-4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.93-1m)
- update

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.92-2m)
- rebuild against python-2.6.1-1m

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.92-1m)
- update
- add old, broken, kludge libtools hack "%%define __libtoolize :"

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.91-2m)
- fix libraries version dependency

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.80-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-1m)
- update
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-3m)
- rebuild against xfce4 4.4.1

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.2-2m)
- delete libtool library

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-1m)
- import to Momonga from fc

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 0.3.2-1
- Upgrade to 0.3.2

* Fri Dec  8 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.12-0.3.rc2
- Rebuild for new python

* Thu Nov 16 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.12-0.2.rc2
- Rebuild with fixed xfce-mcs-manager-devel
- Add exo-preferred-applicatons-settings.so

* Thu Nov  9 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.12-0.1.rc2
- Update to 0.3.1.12rc2

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.10-0.6.rc1
- Added libxfce4util-devel Requires for the devel package

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.10-0.5.rc1
- Added gtk-update-icon-cache

* Wed Oct  4 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.10-0.4.rc1
- Bump release for devel checkin

* Thu Sep 28 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.10-0.3.rc1
- Remove libxfce4gui/libxfce4gui-devel Requires/BuildRequires

* Sun Sep  7 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.10-0.1.rc1
- Upgrade to 0.3.1.10-0.1.rc1

* Tue Aug 29 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.8-0.3.beta2
- Add perl-URI BuildRequires

* Wed Aug  2 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.8-0.1.beta2
- Fix release numbering
- General cleanup for devel push
- Mark helpers.rc as a configfile

* Wed Jul 12 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.8-0.beta2
- Upgrade to 0.3.1.8beta2
- Removed unneeded patch

* Mon May  8 2006 Kevin Fenzi <kevin@tummy.com> - 0.3.1.6beta1
- Upgrade to 0.3.1.6beta1

* Sat Jan 21 2006 Kevin Fenzi <kevin@scrye.com> - 0.3.0-11.fc5
- Add imake to BR to allow detection of modular xorg

* Wed Aug 17 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-10.fc5
- Rebuild for new libcairo and libpixman

* Fri Jul  1 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-9.fc5
- Bump release for a new build

* Mon Jun 20 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-8.fc5
- Add patch to make x86_64 package build

* Thu Jun  2 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-7.fc5
- Change python_sitelib to python_sitearch

* Tue May 31 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-6
- Add python_sitelib to build on x86_64
- Add dist to release

* Tue May 31 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-5
- Add python-devel buildrequires

* Mon May 30 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-4
- Fixed exo gtk-doc directory not being included in devel
- Changed pygtk defs dir 
- Added Requires to devel for pkg-config dependency

* Mon May 30 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-3
- Added gettext to buildrequires
- Moved devel docs to devel package only
- Added find_lang for locale files
- Added more docs to base package

* Fri May 27 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-2
- Added lang to files
- Fixed some file paths
- Remove unneeded la files
- Added pygtk2-devel buildrequires

* Sat Mar 19 2005 Kevin Fenzi <kevin@tummy.com> - 0.3.0-1
- Upgraded to 0.3.0 version

* Tue Mar  8 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.0-3
- Removed generic INSTALL doc from %%doc

* Sun Mar  6 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.0-2
- Inital Fedora Extras version
