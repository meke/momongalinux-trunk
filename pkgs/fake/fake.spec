%global momorel 12

Summary: Switches in redundant servers using arp spoofing
Name: fake
Version: 1.1.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: http://www.vergenet.net/linux/%{name}/download/%{version}/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.ca.us.vergenet.net/linux/fake/
NoSource: 0

%description
Fake is a utility that enables the IP address be taken over
by bringing up a second interface on the host machine and
using gratuitous arp. Designed to switch in backup servers
on a LAN.


%prep
%setup -q -n %{name}-%{version}

%build
make patch
CFLAGS="%{optflags} -s"
make

%install
rm -rf %{buildroot}

make ROOT_DIR=%{buildroot} MAN8_DIR=%{buildroot}%{_mandir}/man8 HB_RESOURCE_DIR=%{buildroot}/%{_libdir}/heartbeat install

# remove
rm -f %{buildroot}/etc/fake/instance_config/192.168.89.19.cfg
rm -f %{buildroot}/etc/fake/instance_config/203.12.97.7.cfg
rm -f %{buildroot}/usr/doc/fake-1.1.8/AUTHORS
rm -f %{buildroot}/usr/doc/fake-1.1.8/COPYING
rm -f %{buildroot}/usr/doc/fake-1.1.8/ChangeLog
rm -f %{buildroot}/usr/doc/fake-1.1.8/README

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post

%postun

%files
%defattr(644,root,root,755)
%doc AUTHORS 
%doc README
%doc ChangeLog
%doc docs/arp_fun.txt
%doc docs/redundant_linux.txt
%doc instance_config/203.12.97.7.cfg
%doc instance_config/192.168.89.19.cfg
%doc heartbeat/fake
%defattr(-,root,root)
%{_mandir}/man8/*
%dir /etc/fake
%dir /etc/fake/instance_config
%config /etc/fake/.fakerc
%config /etc/fake/clear_routers
%{_libdir}/heartbeat/fake
%defattr(755,root,root,-)
%{_sbindir}/fake
%{_sbindir}/send_arp

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.8-10m)
- full rebuild for mo7 release

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-9m)
- add execute bit

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-7m)
- fix %%files

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.8-5m)
- rebuild against gcc43

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.8-4m)
- revised docdir permission

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.8-3m)
- enable x86_64.

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.8-2m)
- revised spec for enabling rpm 4.2.

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.8-1m)
- update to 1.1.8

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.6-9m)
- change source URL

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.6-8k)
- nigittenu

* Mon Sep 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- change Group

* Wed Apr 11 2001 Akira Higuchi <a@kondara.org>
- (1.1.6-4k)
- Changed the source URL

* Mon Mar 12 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.6-2k)
- Kondara Initiation
