%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary:        Python PDF generation library
Name:           python-reportlab
Version:        2.4
Release:        %{momorel}m%{?dist}
Group:          Development/Libraries
License:        BSD
URL:            http://www.reportlab.org/
Source0:        http://www.reportlab.com/ftp/ReportLab_2_4.tar.gz
Patch0:         reportlab-2.3-font-locations.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel


%description
Python PDF generation library.


%package docs
Summary:        Documentation files for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}


%description docs
The %{name}-docs package contains the documentation for ReportLab in PDF format.


%prep
%setup -q -n ReportLab_2_4
%patch0 -p1 -b .fonts
# clean up hashbangs from libraries
find src -name '*.py' | xargs sed -i -e '/^#!\//d'


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build
# a bit of a horrible hack due to a chicken-and-egg problem. The docs
# require reportlab, which isn't yet installed, but is at least built.
PYTHONPATH="`pwd`/`ls -d build/lib*`" %{__python} docs/genAll.py


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
# Remove bundled fonts
rm -rf $RPM_BUILD_ROOT%{python_sitearch}/reportlab/fonts


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README.txt CHANGES.txt LICENSE.txt
%{python_sitearch}/reportlab
%{python_sitearch}/reportlab-2.4-py?.?.egg-info
%{python_sitearch}/*.so


%files docs
%defattr(-,root,root,-)
%doc docs/*.pdf demos tools LICENSE.txt


%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-6m)
- add source

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1-2m)
- rebuild against python-2.6.1-1m

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-1m)
- import from Fedora to Momonga

* Mon Jan  7 2008 Brian Pepple <bpepple@fedoraproject.org> - 2.1-2
- Remove luxi font. (#427845)
- Add patch to not search for the luxi font.

* Sat May 26 2007 Brian Pepple <bpepple@fedoraproject.org> - 2.1-1
- Update to 2.1.

* Wed Dec 27 2006 Brian Pepple <bpepple@fedoraproject.org> - 2.0-2
- Make docs subpackage.

* Wed Dec 27 2006 Brian Pepple <bpepple@fedoraproject.org> - 2.0-1
- Update to 2.0.

* Fri Dec  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.21.1-2
- Rebuild against new python.

* Thu Sep  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 1.21.1-1
- Update to 1.20.1.

* Tue Feb 14 2006 Brian Pepple <bdpepple@ameritech.net> - 1.20-5
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Dec 26 2005 Brian Pepple <bdpepple@ameritech.net> - 1.20-4
- Add dist tag. (#176479)

* Mon May  9 2005 Brian Pepple <bdpepple@ameritech.net> - 1.20-3.fc4
- Switchback to sitelib patch.
- Make package noarch.

* Thu Apr  7 2005 Brian Pepple <bdpepple@ameritech.net> - 1.20-2.fc4
- Use python_sitearch to fix x86_64 build.

* Wed Mar 30 2005 Brian Pepple <bdpepple@ameritech.net> - 1.20-1.fc4
- Rebuild for Python 2.4.
- Update to 1.20.
- Switch to the new python macros for python-abi
- Add dist tag.

* Sat Apr 24 2004 Brian Pepple <bdpepple@ameritech.net> 0:1.19-0.fdr.2
- Removed %ghosts.

* Sat Mar 20 2004 Brian Pepple <bdpepple@ameritech.net> 0:1.19-0.fdr.1
- Initial Fedora RPM build.

