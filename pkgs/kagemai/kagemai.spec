%global momorel 12

Name: kagemai

%global rewrite_path_script rpm-rewrite_path.rb

%global kagemai_root_dir /usr/share/%{name}
%global kagemai_bin_dir %{kagemai_root_dir}/bin
%global kagemai_data_dir /var/lib/%{name}
%global kagemai_etc_dir /etc/%{name}
%global kagemai_html_dir /var/www/%{name}
%global kagemai_lib_dir %{kagemai_root_dir}/lib
%global kagemai_project_dir %{kagemai_data_dir}/project
%global kagemai_resource_dir %{kagemai_root_dir}/resource

%global kagemai_config_file kagemai.conf
%global kagemai_mailif_logfile mailif.log

%global httpdconfdir /etc/httpd/conf.d

Summary: Ruby based bug tracking system
Version: 0.8.8
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Productivity
URL: http://www.daifukuya.com/kagemai/
Source0: http://www.daifukuya.com/archive/kagemai/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: %{kagemai_config_file}
Source2: kagemai.conf.dist
Patch0: kagemai-0.8.8-gdchart.patch
# http://www.daifukuya.com/kagemai/guest.rbx?action=view_report&id=363&project=kagemai
Patch1: kagemai-0.8.8-summary.patch
Patch2: kagemai-0.8.8-config.rb.patch
Patch3: kagemai-0.8.8-avoid-undisclosed-recipients.patch
# http://www.daifukuya.com/kagemai/guest.rbx?action=view_report&id=107&project=kagemai
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: webserver
Requires: ruby >= 1.9.2
Requires: ruby-GD
Requires: ruby-gdchart
Requires: ruby-amrita
Requires: meguri-fonts
BuildRequires: ruby >= 1.9.2
BuildRequires: httpd
BuildArchitectures: noarch

%description
Yet another BTS written in Ruby

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

cp -p %{SOURCE1} %{kagemai_config_file}

%build
## first, we make rewrite script.
kagemai_root_dir=%{kagemai_root_dir} \
kagemai_config_file=%{kagemai_etc_dir}/%{kagemai_config_file} \
cat <<EOF > %{rewrite_path_script}
#!/usr/bin/ruby

## script update function
def update_file(filename, regexp, replace)
	stat = File.stat(filename)
	src = File.open(filename){|file| file.read}
	File.open(filename, 'w') do |file|
		file.puts src.sub(regexp, replace)
	end
	File.chmod(stat.mode, filename)
	File.utime(stat.atime, stat.mtime, filename)
end

ARGV.each do |file|
	if File.exist?(file)
		update_file(file,/^\#!.+?$/m,"#!/usr/bin/ruby -Ke")
		update_file(file,/^kagemai_root\s*=.*\# setup$/,"kagemai_root = \"${kagemai_root_dir}\"")
		update_file(file,/^config_file\s*=.*\# setup$/,"config_file = \"${kagemai_config_file}\"")
		update_file(file,/^old_lib\s*=.*\# setup$/,"old_lib = \"/var/lib/kagemai/lib\"")
		update_file(file,/^\$LOGFILE\s*=.*\# setup$/,"\$LOGFILE = '#{$mailif_logfile}'")
	end
end

exit(0)
EOF

## setup bin/*.rb html/*.cgi
ruby -Ke %{rewrite_path_script} bin/*.rb html/*.cgi

%install
test %{buildroot} != / && rm -rf %{buildroot}
#RUBYVER=`ruby -e "print RUBY_VERSION.split('.')[0,2].join('.')"`

_mkdirs() {
	mkdir -p %{buildroot}%{kagemai_root_dir}
	mkdir -p %{buildroot}%{kagemai_bin_dir}
	mkdir -p %{buildroot}%{kagemai_data_dir}
	mkdir -p %{buildroot}%{kagemai_etc_dir}
	mkdir -p %{buildroot}%{kagemai_html_dir}
	mkdir -p %{buildroot}%{kagemai_lib_dir}
	mkdir -p %{buildroot}%{kagemai_project_dir}
	mkdir -p %{buildroot}%{kagemai_resource_dir}
	return
}

_install_root() {
	## nothing to do
	return
}

_install_bin() {
	cp -pr bin/*.rb %{buildroot}%{kagemai_bin_dir}
	chmod +x %{buildroot}%{kagemai_bin_dir}/*.rb
	return
}

_install_data() {
	## nothing to do
	return
}

_install_etc() {
	## nothing to do
	cp -pr %{kagemai_config_file} %{buildroot}%{kagemai_etc_dir}
	touch %{buildroot}%{kagemai_etc_dir}/user.passwd
	touch %{buildroot}%{kagemai_etc_dir}/admin.passwd
	return
}

_install_html () {
	cp -pr html/*.{cgi,css,gif} %{buildroot}%{kagemai_html_dir}
	cp -p html/dot.htaccess %{buildroot}%{kagemai_html_dir}/.htaccess
	return
}

_install_lib() {
	cp -pr lib/* %{buildroot}%{kagemai_lib_dir}
	return
}

_install_project() {
	## nothing to do
	return
}

_install_resource() {
	cp -pr resource/* %{buildroot}%{kagemai_resource_dir}
	return
}

## make directories and install files
_mkdirs
for i in root bin data etc html lib project resource; do
	_install_${i}
done

## let me make a symbolic link, like /usr/bin/X11
mkdir -p %{buildroot}%{_bindir}
ln -sf %{kagemai_bin_dir} %{buildroot}%{_bindir}/%{name}

## Install the httpd configuration file
mkdir -p %{buildroot}%{httpdconfdir}
install -m755 -d %{buildroot}%{httpdconfdir}
install -m644 %{SOURCE2} %{buildroot}%{httpdconfdir}/

%clean
test %{buildroot} != / && rm -rf %{buildroot}

%post
kagemai_etc_dir=%{kagemai_etc_dir} uname=`uname -n` cat <<EOF
You need to add users as follows:
    htpasswd -c ${kagemai_etc_dir}/admin.passwd admin
    htpasswd -c ${kagemai_etc_dir}/user.passwd user
Setting httpd.conf might have to have
    mv %{httpdconfdir}/kagemai.conf.dist %{httpdconfdir}/kagemai.conf
    and edit %{httpdconfdir}/kagemai.conf
Finally, you can test kagemai on
    http://${uname}/kagemai/guest.cgi
EOF

%files
%defattr(-,root,root)
%dir %{kagemai_root_dir}
%dir %{kagemai_bin_dir}
%attr(2775,apache,apache) %dir %{kagemai_data_dir}
%attr(2775,apache,apache) %dir %{kagemai_etc_dir}
%attr(0775,apache,apache) %dir %{kagemai_html_dir}
%dir %{kagemai_lib_dir}
%attr(2775,apache,apache) %dir %{kagemai_project_dir}
%dir %{kagemai_resource_dir}

%{kagemai_bin_dir}/*.rb
%attr(664,apache,apache) %config(noreplace) %{kagemai_etc_dir}/%{kagemai_config_file}
%attr(664,apache,apache) %config(noreplace) %{kagemai_etc_dir}/*.passwd
%attr(664,apache,apache) %{kagemai_html_dir}/*
%attr(664,apache,apache) %config %{kagemai_html_dir}/.htaccess
%{kagemai_lib_dir}/*
%{kagemai_resource_dir}/*
%{_bindir}/%{name}

%config(noreplace) %{httpdconfdir}/*

%doc README COPYING GPL doc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8-10m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-9m)
- rebuild against ruby-1.9.2

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-8m)
- touch up spec file
- License: GPLv2+

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8-6m)
- add Patch3: kagemai-0.8.8-avoid-undisclosed-recipients.patch

* Sun Aug 23 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.8-5m)
- add kagemai-0.8.8-gdchart.patch
- add kagemai-0.8.8-summary.patch

* Thu Mar  5 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8-4m)
- add Source2: kagemai.conf.dist
- add Patch4: kagemai-0.8.8-config.rb.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-3m)
- rebuild against rpm-4.6

* Tue Sep 02 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.8-2m)
- use VLGothic-fonts

* Sat Apr 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Sat Apr 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-5m)
- change kagemai_html_dir
- delete Requires: truetype-fonts-ja
- add Requires: fonts-japanese

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-3m)
- %%NoSource -> NoSource

* Tue Aug 30 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.6-2m)
- added Requires: ruby-amrita

* Tue Apr 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.8.6-1m)
- up to 0.8.6

* Thu Nov 25 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.8.4-2m)
- enable no reply option ([kagemai-users:0308])

* Tue Nov  9 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.4-1m)
- update to 0.8.4

* Thu Jul  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.3-7m)
- use sazanami instead of kochi

* Wed May 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.3-6m)
- kagemai.conf should be 'noreplace'

* Wed May 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.3-5m)
- convert strings for gd to UTF-8
- enable gdchart by default
- fix font for gd

* Tue May 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.3-4m)
- import patch to fix summary page error
  http://www.daifukuya.com/kagemai/guest.rbx?project=kagemai&action=view_report&id=185

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-3m)
- s/Copyright:/License:/

* Wed Nov 19 2003 zunda <zunda at freeshell.org>
- (0.8.3-2m)
- send e-mails to the address with 'Get replys via e-mail'. See BTS:165
	http://www.daifukuya.com/kagemai/guest.rbx?project=kagemai&action=view_report&id=165

* Sat Aug 16 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.3-1m)
- version up

* Sun Aug 10 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.2-2m)
- note: use not kagemai-0.8.2.tar.gz
  but kagemai-0.8.2-1.tar.gz (see [kagemai-users:0149])

* Sun Aug 10 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.2-1m)
- version up
- s/%%define/%%global/g
- remove Source2: stringio.rb

* Wed Jul  9 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.1-2m)
- add stringio.rb for fake StringIO class ;-), thanks to zunda
- change: "cp -pr %%{SOURCE1} %%{kagemai_config_file}" ->
          "cp -p %%{SOURCE1} %%{kagemai_config_file}"

* Wed Apr 16 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.1-1m)
- note: use not kagemai-0.8.1.tar.gz
  but kagemai-0.8.1-1.tar.gz (see [kagemai-users:0058])
- remove Patch0 and Patch1
- fix file/directory mode and permission

* Wed Apr  9 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.0-2m)
- change Patch0 (see also: [kagemai-users:0039])
- fix changelog date of 0.8.0-1m

* Wed Apr  9 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.8.0-1m)
- add Source1, Patch0, Patch1 (with some bug fixes)

* Fri Feb 14 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.1-4m)
- rebuild against httpd-2.0.43

* Tue Feb  4 2003 HOSONO Hidetomo <h12o@h12o.org>
- change directories
- remove unnecessary files from the file list
- change my mail address on the previous changelog entry,
  "h@h12o.org" to "h12o@h12o.org" :D

* Sat Oct 26 2002 HOSONO Hidetomo <h12o@h12o.org>
- (0.7.1-3m)
- add Patch20021026

* Fri Oct 25 2002 HOSONO Hidetomo <h12o@h12o.org>
- (0.7.1-2m)
- spec file clean up

* Thu Oct 24 2002 HOSONO Hidetomo <h12o@h12o.org>
- (0.7.1-1m)
- version up

* Sat Sep 28 2002 Kikutani Makoto <poo@momonga-linux.org>
- (0.7.0-2m)
- add Conflicts: mod_ruby

* Sat Sep 28 2002 Kikutani Makoto <poo@momonga-linux.org>
- (0.7.0-1m)
- 1st Momonga version
