%global momorel 1

Summary: A C++ interface for the GConf
Name: gconfmm
Version: 2.28.3
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://gtkmm.sourceforge.net/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.28/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glibmm-devel >= 2.22.1
BuildRequires: GConf2-devel >= 2.28.0
BuildRequires: gtkmm-devel >= 2.18.1

%description
This package provides a C++ interface for GTK+ (the Gimp ToolKit) GUI
library.  The interface provides a convenient interface for C++
programmers to create GUIs with GTK+'s flexible object-oriented framework.
Features include type safe callbacks, widgets that are extensible using
inheritance and over 110 classes that can be freely combined to quickly
create complex user interfaces.
#'

%package devel
Summary: Headers for developing programs that will use Gtk--.
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibmm-devel
Requires: gtkmm-devel
Requires: GConf2-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use gconfmm, the C++ interface to the GConf
library.

%prep
%setup -q

%build
%configure MMDOCTOOLDIR=%{_datadir}/mm-common/doctool
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README
%{_libdir}/libgconfmm-2.6.so.*
%exclude %{_libdir}/libgconfmm-2.6.la

%files devel
%defattr(-, root, root)
%{_includedir}/gconfmm-2.6
%{_libdir}/libgconfmm-2.6.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/gconfmm-2.6
%doc %{_datadir}/devhelp/books/gconfmm-2.6
%doc %{_datadir}/doc/gconfmm-2.6

%changelog
* Fri Oct 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Fri Aug  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-5m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.2-2m)
- full rebuild for mo7 release

* Sun Jun  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.28.0-4m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.24.0-3m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.0-2m)
- %%NoSource -> NoSource

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Thu Sep 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-2m)
- To Main

* Sat Mar 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Aug 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update 2.14.2

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- merge from OBSOLETE

* Tue Jun  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- initial build (no tumori datta)

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.0-1m)
- version 2.6.0

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Jan 06 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Mon Dec 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.10-2m)
- just rebuild for gtkmm

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.10-1m)
- version 1.2.10

* Thu Nov 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.9-1m)
- version 1.3.9

* Sat Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.8-1m)
- version 1.3.8

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.7-1m)
- version 1.3.7

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.6-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.6-2m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.6-1m)
- version 1.3.6

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-24k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-22k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-20k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-18k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.3.3-16k)
- cancel gcc-3.1 autoconf-2.53

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-14k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.3.3-12k)
  applied gcc 3.1 patch

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-10k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Mon May 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-8k)
- remove convert*.m4

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-6k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-2k)
- version 1.3.3

* Fri Apr 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-2k)
- create
