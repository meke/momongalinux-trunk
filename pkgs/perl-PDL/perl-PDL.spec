%global         momorel 3

Name:           perl-PDL
Version:        2.007
Release:        %{momorel}m%{?dist}
Summary:        The Perl Data Language

Group:          Development/Libraries
License:        GPL+ or Artistic
URL:            http://search.cpan.org/dist/PDL/
Source0:	http://www.cpan.org/authors/id/C/CH/CHM/PDL-%{version}.tar.gz
NoSource:	0
Patch1:         perl-PDL-2.4.7-hdf.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl >= 1:5.6.1
BuildRequires:  perl-Astro-FITS-CFITSIO
BuildRequires:  perl-Convert-UU
BuildRequires:  perl-Data-Dumper >= 2.121
BuildRequires:  perl-ExtUtils-F77
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-PathTools >= 0.6
BuildRequires:  perl-Filter-Util-Call
BuildRequires:  perl-Inline >= 0.43
BuildRequires:  perl-Module-Compile
BuildRequires:  perl-OpenGL >= 0.63
BuildRequires:  perl-Pod-Parser
BuildRequires:  perl-Storable >= 1.03
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Text-Balanced
BuildRequires:  perl-version
BuildRequires:  fftw2-devel
BuildRequires:  gcc-gfortran
BuildRequires:  gd-devel
BuildRequires:  gsl-devel
BuildRequires:  hdf-devel
BuildRequires:  libGLU-devel
BuildRequires:  plplot-devel
BuildRequires:  proj-devel
BuildRequires:  proj-nad
BuildRequires:  ncurses-devel
BuildRequires:  sharutils
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  f2c-libs
Requires:       perl-Astro-FITS-CFITSIO
Requires:       perl-Convert-UU
Requires:       perl-Data-Dumper >= 2.121
Requires:       perl-PathTools >= 0.6
Requires:       perl-Filter-Util-Call
Requires:       perl-Inline >= 0.43
Requires:       perl-Module-Compile
Requires:       perl-OpenGL >= 0.63
Requires:       perl-Pod-Parser
Requires:       perl-Storable >= 1.03
Requires:       perl-Text-Balanced
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Provides:       perl(PDL::Config) perl(PDL::PP::CType) perl(PDL::PP::Dims)
Provides:       perl(PDL::PP::PDLCode) perl(PDL::PP::SymTab) perl(PDL::PP::XS)
Provides:       perl(PGPLOT) perl(PDL::LiteF) perl(PDL::Lite)
Provides:       perl(PDL::Graphics::TriD) perl(PDL::Graphics::TriD::GL) 
Provides:       perl(PDL::Graphics::TriD::Objects) perl(PDL::Graphics::TriD::Contours)
Provides:       perl(PDL::Graphics::TriD::Image) perl(PDL::Graphics::TriD::Tk)

%{?!DEBUG:      %define DEBUG 0}

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
PDL ("Perl Data Language") gives standard Perl the ability to
compactly store and speedily manipulate the large N-dimensional data
arrays which are the bread and butter of scientific computing.  PDL
turns perl into a free, array-oriented, numerical language similar to
such commercial packages as IDL and MatLab.


%prep
%setup -q -n PDL-%{version}
%patch1 -p1 -b .hdf

# Provides: explicitly filter perl(Inline)
cat <<__EOF__ > %{name}-findperlprovides
#!/bin/sh
%{__perl_provides} \$* | grep -v 'perl(Inline)'
__EOF__
%define __perl_provides %{_builddir}/PDL-%{version}/%{name}-findperlprovides
chmod +x %{__perl_provides}

# Requires: explicitly filter perl(Tk)
cat <<__EOF__ > %{name}-findperlrequires
#!/bin/sh
%{__perl_requires} \$* | \
	grep -v 'perl(Tk)' | \
	grep -v 'perl(Win32::DDE::Client)' | \
	grep -v 'perl(PDL::Graphics::OpenGL::Perl::OpenGL)' | \
	grep -v 'perl(OpenGL::Config)' | \
	grep -v 'perl(PDL::Demos::Screen)' | \
	grep -v 'perl(PDL::Graphics::Gnuplot)' | \
	grep -v 'perl(Prima::MsgBox)'
__EOF__
%define __perl_requires %{_builddir}/PDL-%{version}/%{name}-findperlrequires
chmod +x %{__perl_requires}

%if %{DEBUG}
%define debug_package %{nil}
%endif

%build
export PERL5LIB=`pwd`/blib/lib:`pwd`/blib/arch:
#^- can no longer build if perl-PDL is not installed without above
#
%if %{DEBUG}
CFLAGS=`echo "$RPM_OPT_FLAGS -Wno-unused" | sed 's/-O2 -g/-g3 -gdwarf-2/'`
%else
CFLAGS="$RPM_OPT_FLAGS -Wno-unused"
%endif
%ifarch ppc ppc64 s390 s390x
CFLAGS="$CFLAGS -fsigned-char"
%endif
CFLAGS="$CFLAGS -DNCURSES"
CFLAGS="$CFLAGS" %{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$CFLAGS"
make OPTIMIZE="$CFLAGS"
# smp flags blows up spectacularly (2.4.1-10 May 7th 2005)

%install
make pure_install PERL_INSTALL_ROOT=%{buildroot}
%{__perl} -Mblib Doc/scantree.pl %{buildroot}%{perl_vendorarch}
%{__perl} -pi -e "s|%{buildroot}/|/|g" \
  %{buildroot}%{perl_vendorarch}/PDL/pdldoc.db
find %{buildroot}%{perl_vendorarch} -type f -name "*.pm" | xargs chmod -x
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -empty -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'
chmod -R u+w %{buildroot}/*

for file in %{buildroot}%{_mandir}/man3/PDL::Func.3pm %{buildroot}%{_mandir}/man3/PDL::Complex.3pm; do
  iconv -f iso-8859-1 -t utf-8 < "$file" > "${file}_"
  mv -f "${file}_" "$file"
done

%if %{DEBUG}
/usr/lib/rpm/brp-compress
exit 0
%endif

# remove conflicting files with perl-PDL-Graphics-PLplot
rm -fr %{buildroot}%{perl_vendorarch}/auto/PDL/Graphics/PLplot
rm -f %{buildroot}%{perl_vendorarch}/PDL/Graphics/PLplot.pm
rm -f %{buildroot}%{_mandir}/man3/PDL::Graphics::PLplot.3pm*

%check
%if %{do_test}
unset DISPLAY
export PERL5LIB=`pwd`/blib/lib
make test || :
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Bugs.pod COPYING Changes DEPENDENCIES DEVELOPMENT INSTALL
%doc INTERNATIONALIZATION Known_problems Release_Notes README TODO
%{_bindir}/*
%{perl_vendorarch}/Inline/*
%{perl_vendorarch}/PDL*
%{perl_vendorarch}/auto/PDL/
%{_mandir}/man1/*.1*
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.007-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.007-2m)
- rebuild against perl-5.18.2

* Sun Oct 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.007-1m)
- update to 2.007

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.006-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.006-2m)
- rebuild against perl-5.18.0

* Sun Mar 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.006-1m)
- update to 2.006

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.10-1m)
- update to 2.4.10

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-1m)
- update to 2.4.9

* Mon Apr 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.8-4m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.8-3m)
- rebuild for new GCC 4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.8-2m)
- add BuildRequires: f2c-libs
-- perl-PDL needs f2c-libs when using GCC 4.6

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-1m)
- update to 2.4.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.7-7m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-3m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.6-2m)
- rebuild against libjpeg-8a

* Sun Jan  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6
- perl-PDL-Graphics-PLplot was separated as an external package

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-4m)
- temporarily ignore some test failures due to libjpeg-7

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.4-3m)
- rebuild against libjpeg-7

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-2m)
- rebuild against perl-5.10.1

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4 (sync with Fedora devel)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-3m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-2m)
- use NoSource and change source URI

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.3-1m)
- import from Fedora for plplot->gdl

* Mon Mar 10 2008 Marcela Maslanova <mmaslano@redhat.com> - 2.4.3-12
- PERL_DL_NONLAZY=0 was uncommented in check part of spec.

* Sat Mar 08 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.4.3-11
- PERL_DL_NONLAZY=0 for all the GL tests
- don't run GL tests if DISPLAY is unset

* Sat Mar 08 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.4.3-9
- patch to fix build against perl 5.10, get useful random numbers

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.4.3-8
- Rebuild for new perl

* Tue Mar 04 2008 Orion Poplawski <orion@cora.nwra.com> - 2.4.3-7
- Add patch to build GSL support with GSL 1.10
- unset DISPLAY in %%check for mock builds

* Tue Feb 26 2008 Marcela Maslanova <mmaslano@redhat.com> - 2.4.3-6
- remove two of hdf test for some time, because can't be build

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.4.3-5
- Autorebuild for GCC 4.3

* Wed Aug 15 2007 Robin Norwood <rnorwood@redhat.com> - 2.4-3-4
- Updated perl-PDL-2.4.3-test.patch from Orion to fix ppc and ppc64
- Fixed license string
- Fixed old changelog version number

* Fri Aug 10 2007 Robin Norwood <rnorwood@redhat.com> - 2.4.3-3
- More changes from Orion Poplawski
- BuildRequires and patch for fortran/f77 support
- Added Provides: perl(PDL::Graphics::TriD::Object
- Filter perl(Win32::DDE::Client) from Requires

* Mon Aug 06 2007 Robin Norwood <rnorwood@redhat.com> - 2.4.3-2
- Apply changes from package review
- untabify spec file
- Add various files to %%doc
- turn on 3D/GL
- turn on IO Browser
- add a bunch of BRs to enable more modules
- remove unneeded Provides
- perl-PDL-2.4.3-hdf.patch to look for hdf devel files in the right place
- perl-PDL-2.4.3-test.patch to fix some tests
- perl-PDL-2.4.3-x86_64.patch to find 64bit libraries for some modules
- perl-PDL-2.4.3-Xext.patch to remove -lXext from GL linking options

* Sat Dec 02 2006 Robin Norwood <rnorwood@redhat.com> - 2.4.3-1
- Latest version from CPAN: 2.4.3

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2.4.2-4.fc5.1
- rebuild

* Fri Mar 10 2006 Jason Vas Dias <jvdias@redhat.com> - 2.4.2-4
- Further code cleanup & CFLAGS settings required to enable tests 
  to succeed on all platforms

* Thu Mar 09 2006 Jason Vas Dias <jvdias@redhat.com> - 2.4.2-4
- Enable tests to succeed on ia64 (remove casts from int to * !)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.4.2-2.fc5.1.2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.4.2-2.fc5.1.2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Feb 03 2006 Jason Vas Dias <jvdias@redhat.com> - 2.4.2-2.fc5.1.2
- rebuild for new perl-5.8.8
- enable build to succeed without perl-PDL being installed :-)

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Sun Sep 25 2005 Warren Togami <wtogami@redhat.com> - 2.4.2-2
- Ship pdldoc.db, tune build dependencies and file permissions (#163219 scop)

* Fri May 27 2005 Warren Togami <wtogami@redhat.com> - 2.4.2-1
- 2.4.2
- filter perl(Inline) from provides (#158733)

* Wed May 11 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 2.4.1-11
- Add missing perl(PDL::Graphics::TriD*) provides. (#156482)
- Explicitly filter perl(Tk). (#156482)

* Sat Apr 30 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 2.4.1-10
- Bring up to date with current Fedora.Extras perl spec template. (#156482)
- disable SMP flags so it actually builds

* Sat Dec 18 2004 Miloslav Trmac <mitr@redhat.com> - 2.4.1-9
- Rebuild with fixed gsl-devel (#142695)

* Sun Dec 12 2004 Miloslav Trmac <mitr@redhat.com> - 2.4.1-8
- Fix more bugs on 64-bit platforms
- BuildRequires: gsl-devel

* Sun Dec 12 2004 Miloslav Trmac <mitr@redhat.com> - 2.4.1-7
- Fix rangeb on 64-bit platforms (I hope) (#141413)

* Thu Nov 25 2004 Miloslav Trmac <mitr@redhat.com> - 2.4.1-6
- Convert man page to UTF-8

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Chip Turner <cturner@redhat.com> 2.4.1-1
- update to 2.4.1

* Mon Jun 16 2003 Chip Turner <cturner@redhat.com> 2.4.0-2
- move to 2.4.0, integrate dependency fixes from other tree

* Wed Jan 29 2003 Chip Turner <cturner@redhat.com>
- bump

* Mon Jan 27 2003 Chip Turner <cturner@redhat.com>
- version bump and rebuild

* Wed Nov 20 2002 Chip Turner <cturner@redhat.com>
- move to 2.3.4

* Tue Aug  6 2002 Chip Turner <cturner@redhat.com>
- automated release bump and build

* Tue Jul 16 2002 Chip Turner <cturner@redhat.com>
- updated %%description

* Thu Jun 27 2002 Chip Turner <cturner@redhat.com>
- description update

* Fri Jun 07 2002 cturner@redhat.com
- Specfile autogenerated

