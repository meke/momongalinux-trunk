%global momorel 7

Summary:  A program for recovering corrupt partition tables
Name: gpart
Version: 0.1h
Release: %{momorel}m%{?dist}
ExclusiveArch: %{ix86}
URL: http://www.stud.uni-hannover.de/user/76201/gpart/
Source: http://www.stud.uni-hannover.de/user/76201/%{name}/%{name}-%{version}.tar.gz
Patch0: gpart-0.1h-varname.patch
Patch1: gpart-0.1h-cflags.patch
Patch2: gpart-0.1h-errno.patch
Patch3: gpart-0.1h-syscall.patch
Patch4: gpart-0.1h-largefile.patch
Patch5: gpart-0.1h-makefile.patch
BuildRequires: glibc-kernheaders

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPL
Group: Applications/System

%description
Gpart is a small tool which tries to guess what partitions are on a PC
type harddisk in case the primary partition table was damaged.

%prep
%setup -q
%patch0 -p1 -b .varname
%patch1 -p1 -b .cflags
%patch2 -p1 -b .errno
%patch3 -p1 -b .syscall
%patch4 -p1 -b .largefile
%patch5 -p1 -b .makefile

%build
make

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc COPYING Changes LSM README
%{_bindir}/gpart
%{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1h-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1h-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1h-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1h-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1h-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1h-2m)
- rebuild against gcc43

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1h-1m)
- import to Momonga from fc-devel

* Sat Feb 03 2007 David Cantrell <dcantrell@redhat.com> - 0.1h-5
- Fix spec file problems with merge review (#225853)

* Sun Oct 22 2006 David Cantrell <dcantrell@redhat.com> - 0.1h-4
- Compile with large file support (#211746)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.1h-3.1
- rebuild

* Tue Jun 06 2006 Chris Lumens <clumens@redhat.com> 0.1h-3
- Fix building on i386 by using the right syscall stuff.

* Tue Jun 06 2006 Jesse Keating <jkeating@redhat.com> - 0.1h-2
- Added missing BR glibc-kernheaders

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.1h-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Jun 10 2005 Chris Lumens <clumens@redhat.com> 0.1h-1
- Initial build.
