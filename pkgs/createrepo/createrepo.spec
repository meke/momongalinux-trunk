%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global momorel 1

Summary: Creates a common metadata repository
Name: createrepo
Version: 0.10.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
Source0: http://createrepo.baseurl.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: ten-changelog-limit.patch
Patch2: createrepo-HEAD.patch
URL: http://createrepo.baseurl.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
Requires: python >= 2.7, rpm-python, rpm >= 0:4.1.1, libxml2-python
Requires: yum-metadata-parser, yum >= 3.4.3, python-deltarpm, deltarpm
Requires: bash-completion, pyliblzma
BuildRequires: python

%description
This utility will generate a common metadata repository from a directory of rpm
packages

%prep
%setup -q
%patch1 -p0
%patch2 -p1

%build

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} sysconfdir=%{_sysconfdir} install

%clean
rm -rf %{buildroot}


%files
%defattr(-, root, root)
%dir %{_datadir}/%{name}
%doc ChangeLog README COPYING COPYING.lib
%{_datadir}/bash-completion/completions/*
%{_datadir}/%{name}
%{_bindir}/%{name}
%{_bindir}/modifyrepo
%{_bindir}/mergerepo
%{_mandir}/*/*
%{python_sitelib}/%{name}

%changelog
* Tue Jun 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.3-1m)
- update 0.10.3

* Fri Oct 25 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-1m)
- update 0.10

* Tue Aug  6 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-8m)
- update createrepo-head.patch

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-7m)
- update createrepo-head.patch

* Mon Mar 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-6m)
- update createrepo-head.patch
-- limit 128 worker
-- fix float pkgs. (for example 9 workers and 5 pkgs)

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-5m)
- update createrepo-head.patch

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-4m)
- update createrepo-head.patch

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-3m)
- update createrepo-head.patch

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-2m)
- update createrepo-head.patch

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.9-2m)
- release a directory %%{_sysconfdir}/bash_completion.d, it's provided by bash-completion

* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.9-1m)
- version up 0.9.9

* Tue Aug  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-13m)
- update createrepo-HEAD.patch

* Fri Jul 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-11m)
- update createrepo-HEAD.patch

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-10m)
- update createrepo-HEAD.patch

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-10m)
- add 10 limit changelog patch

* Tue Jul  5 2011 Masaru SANUKI <sanuki@momonga-linux.org> 
- (0.9.8-9m)
- Requires add python-deltarpm, deltarpm

* Fri Jun 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-8m)
- add createrepo-HEAD.patch

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-4m)
- full rebuild for mo7 release

* Fri Jan 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8-3m)
- change source URI

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-1m)
- update 0.9.8

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-3m)
- add readMetadata_for_ext4.patch.
-- support --update option on ext4 filesystem
-- thanks h_nakamura 

* Fri May 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-2m)
- update createrepo-head.patch. from fedora 0_9_7-7

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-1m)
- update to 0.9.7

* Sat Mar  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6-3m)
- add upstream patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6-1m
-- drop Patch1, merged upstream
- License: GPLv2+
- change URL

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.5-3m)
- rebuild agaisst python-2.6.1-1m

* Tue Oct 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-2m)
- import a bug fix from upstream
-- see https://bugzilla.redhat.com/show_bug.cgi?id=446040

* Sun Apr 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-1m)
- update 0.9.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-2m)
- rebuild against gcc43

* Thu Jan 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4

* Thu Jan 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-1m)
- update 0.9.3

* Fri Jan 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-1m)
- update 0.9.2

* Thu Jan 10 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.9.1-2m)
- add lib64 patch

* Thu Jan 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-1m)
- update 0.9.1

* Fri Nov 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.11-1m)
- update 0.4.11

* Sat Jun  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.10-1m)
- update 0.4.10

* Sat May 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-1m)
- update 0.4.9

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.8-1m)
- update 0.4.8

* Tue Feb 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.7-1m)
- update 0.4.7

* Wed Dec 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.6-1m)
- update 0.4.6

* Wed May 03 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-1m)
- update 0.4.4

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.2-3m)
- import from rawhide

* Tue Jan 18 2005 Jeremy Katz <katzj@redhat.com> - 0.4.2-2
- add the manpage

* Tue Jan 18 2005 Jeremy Katz <katzj@redhat.com> - 0.4.2-1
- 0.4.2

* Thu Oct 21 2004 Paul Nasrat <pnasrat@redhat.com>
- 0.4.1, fixes #136613
- matched ghosts not being added into primary.xml files

* Mon Oct 18 2004 Bill Nottingham <notting@redhat.com>
- 0.4.0, fixes #134776

* Thu Sep 30 2004 Paul Nasrat <pnasrat@redhat.com>
- Rebuild new upstream release - 0.3.9

* Thu Sep 30 2004 Seth Vidal <skvidal@phy.duke.edu>
- 0.3.9
- fix for groups checksum creation

* Sat Sep 11 2004 Seth Vidal <skvidal@phy.duke.edu>
- 0.3.8

* Wed Sep  1 2004 Seth Vidal <skvidal@phy.duke.edu>
- 0.3.7

* Fri Jul 23 2004 Seth Vidal <skvidal@phy.duke.edu>
- make filelists right <sigh>


* Fri Jul 23 2004 Seth Vidal <skvidal@phy.duke.edu>
- fix for broken filelists

* Mon Jul 19 2004 Seth Vidal <skvidal@phy.duke.edu>
- re-enable groups
- update num to 0.3.4

* Tue Jun  8 2004 Seth Vidal <skvidal@phy.duke.edu>
- update to the format
- versioned deps
- package counts
- uncompressed checksum in repomd.xml


* Fri Apr 16 2004 Seth Vidal <skvidal@phy.duke.edu>
- 0.3.2 - small addition of -p flag

* Sun Jan 18 2004 Seth Vidal <skvidal@phy.duke.edu>
- I'm an idiot

* Sun Jan 18 2004 Seth Vidal <skvidal@phy.duke.edu>
- 0.3

* Tue Jan 13 2004 Seth Vidal <skvidal@phy.duke.edu>
- 0.2 - 

* Sat Jan 10 2004 Seth Vidal <skvidal@phy.duke.edu>
- first packaging

