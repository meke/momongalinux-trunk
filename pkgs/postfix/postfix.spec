%global momorel 1

%bcond_without mysql
%bcond_with pgsql
%bcond_without ldap
%bcond_without pcre
%bcond_without sasl
%bcond_without tls
%bcond_without ipv6
%bcond_without pflogsumm

# hardened build if not overrided
%{!?_hardened_build:%global _hardened_build 1}

# Postfix requires one exlusive uid/gid and a 2nd exclusive gid for its own
# use.  Let me know if the second gid collides with another package.
# Be careful: Redhat's 'mail' user & group isn't unique!
%define postfix_uid	89
%define postfix_user	postfix
%define postfix_gid	89
%define postfix_group	postfix
%define maildrop_group	postdrop
%define maildrop_gid	90

%define postfix_config_dir	%{_sysconfdir}/postfix
%define postfix_daemon_dir	%{_libexecdir}/postfix
%define postfix_command_dir	%{_sbindir}
%define postfix_queue_dir	%{_var}/spool/postfix
%define postfix_data_dir	%{_var}/lib/postfix
%define postfix_doc_dir		%{_docdir}/%{name}-%{version}
%define postfix_sample_dir	%{postfix_doc_dir}/samples
%define postfix_readme_dir	%{postfix_doc_dir}/README_FILES

%if %{?_hardened_build:%{_hardened_build}}%{!?_hardened_build:0}
%global harden -pie -Wl,-z,relro,-z,now
%endif

Name: postfix
Summary: Postfix Mail Transport Agent
Version: 2.10.3
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
URL: http://www.postfix.org/
License: "IBM Public License" and see "LICENSE"
Requires(post): chkconfig
Requires(pre): shadow-utils
Requires(preun): chkconfig
Requires(preun): systemd-units
Requires(postun): systemd-units

Provides: MTA smtpd smtpdaemon server(smtp)

# very slow
#Source0: ftp://ftp.porcupine.org/mirrors/postfix-release/official/%{name}-%{version}.tar.gz
Source0: http://mirror.postfix.jp/postfix-release/official/%{name}-%{version}.tar.gz
NoSource: 0
Source1: postfix-etc-init.d-postfix
Source2: postfix.service
Source3: README-Postfix-SASL-RedHat.txt
Source4: postfix.aliasesdb

# Sources 50-99 are upstream [patch] contributions

%define pflogsumm_ver 1.1.5

%if %{with pflogsumm}
# Postfix Log Entry Summarizer: http://jimsun.linxnet.com/postfix_contrib.html
Source53: http://jimsun.linxnet.com/downloads/pflogsumm-%{pflogsumm_ver}.tar.gz
NoSource: 53
%endif

# Sources >= 100 are config files

Source100: postfix-sasl.conf
Source101: postfix-pam.conf

# Patches

Patch1: postfix-2.7.0-config.patch
Patch2: postfix-2.6.1-files.patch
Patch3: postfix-alternatives.patch
Patch8: postfix-large-fs.patch
Patch9: pflogsumm-1.1.5-datecalc.patch

# Optional patches - set the appropriate environment variables to include
#		     them when building the package/spec file

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Determine the different packages required for building postfix
BuildRequires: libdb-devel >= 5.3.15, pkgconfig, zlib-devel
BuildRequires: systemd-units

Requires: setup >= 2.8.15-1m
BuildRequires: setup >= 2.8.15-1m

%{?with_ldap:BuildRequires: openldap-devel}
%{?with_sasl:BuildRequires: cyrus-sasl-devel}
%{?with_pcre:BuildRequires: pcre-devel >= 8.31}
%{?with_mysql:BuildRequires: mysql-devel}
%{?with_pgsql:BuildRequires: postgresql-devel}
%{?with_tls:BuildRequires: openssl-devel}

%description
Postfix is a Mail Transport Agent (MTA), supporting LDAP, SMTP AUTH (SASL),
TLS

%package sysvinit
Summary: SysV initscript for postfix
Group: System Environment/Daemons
BuildArch: noarch
Requires: %{name} = %{version}-%{release}
Requires(preun): chkconfig
Requires(post): chkconfig

%description sysvinit
This package contains the SysV initscript.

%package perl-scripts
Summary: Postfix utilities written in perl
Group: Applications/System
Requires: %{name} = %{version}-%{release}
# perl-scripts introduced in 2:2.5.5-2
Obsoletes: postfix < 2.6.2-1m
%if %{with pflogsumm}
Provides: postfix-pflogsumm = %{version}-%{release}
Obsoletes: postfix-pflogsumm < 2:2.5.5-2
%endif
%description perl-scripts
This package contains perl scripts pflogsumm and qshape.

Pflogsumm is a log analyzer/summarizer for the Postfix MTA. It is
designed to provide an over-view of Postfix activity. Pflogsumm
generates summaries and, in some cases, detailed reports of mail
server traffic volumes, rejected and bounced email, and server
warnings, errors and panics.

qshape prints Postfix queue domain and age distribution.
 
%prep
%setup -q
# Apply obligatory patches
%patch1 -p1 -b .config
%patch2 -p1 -b .files
%patch3 -p1 -b .alternatives
%patch8 -p1 -b .large-fs

%if %{with pflogsumm}
gzip -dc %{SOURCE53} | tar xf -
pushd pflogsumm-%{pflogsumm_ver}
%patch9 -p1 -b .datecalc
popd
%endif

for f in README_FILES/TLS_{LEGACY_,}README TLS_ACKNOWLEDGEMENTS; do
	iconv -f iso8859-1 -t utf8 -o ${f}{_,} &&
		touch -r ${f}{,_} && mv -f ${f}{_,}
done

%build
CCARGS=-fPIC
AUXLIBS=

%ifarch s390 s390x ppc
CCARGS="${CCARGS} -fsigned-char"
%endif

%if %{with ldap}
  CCARGS="${CCARGS} -DHAS_LDAP -DLDAP_DEPRECATED=1"
  AUXLIBS="${AUXLIBS} -lldap -llber"
%endif
%if %{with pcre}
  # -I option required for pcre 3.4 (and later?)
  CCARGS="${CCARGS} -DHAS_PCRE -I%{_includedir}/pcre"
  AUXLIBS="${AUXLIBS} -lpcre"
%endif
%if %{with mysql}
  CCARGS="${CCARGS} -DHAS_MYSQL -I%{_includedir}/mysql"
  AUXLIBS="${AUXLIBS} -L%{_libdir}/mysql -lmysqlclient -lm"
%endif
%if %{with pgsql}
  CCARGS="${CCARGS} -DHAS_PGSQL -I%{_includedir}/pgsql"
  AUXLIBS="${AUXLIBS} -lpq"
%endif
%if %{with sasl}
  CCARGS="${CCARGS} -DUSE_SASL_AUTH -DUSE_CYRUS_SASL -I%{_includedir}/sasl"
  AUXLIBS="${AUXLIBS} -L%{_libdir}/sasl2 -lsasl2"
  %global sasl_config_dir %{_sysconfdir}/sasl2
%endif
%if %{with tls}
  if pkg-config openssl ; then
    CCARGS="${CCARGS} -DUSE_TLS `pkg-config --cflags openssl`"
    AUXLIBS="${AUXLIBS} `pkg-config --libs openssl`"
  else
    CCARGS="${CCARGS} -DUSE_TLS -I/usr/include/openssl"
    AUXLIBS="${AUXLIBS} -lssl -lcrypto"
  fi
%endif
%if ! %{with ipv6}
  CCARGS="${CCARGS} -DNO_IPV6"
%endif

CCARGS="${CCARGS} -DDEF_CONFIG_DIR=\\\"%{postfix_config_dir}\\\""
CCARGS="${CCARGS} $(getconf LFS_CFLAGS)"

AUXLIBS="${AUXLIBS} %{?harden:%{harden}}"

make -f Makefile.init makefiles CCARGS="${CCARGS}" AUXLIBS="${AUXLIBS}" \
  DEBUG="" OPT="$RPM_OPT_FLAGS -Wno-comment"

make %{?_smp_mflags} 

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

# install postfix into %{buildroot}

# Move stuff around so we don't conflict with sendmail
for i in man1/mailq.1 man1/newaliases.1 man1/sendmail.1 man5/aliases.5; do
  dest=$(echo $i | sed 's|\.[1-9]$|.postfix\0|')
  mv man/$i man/$dest
  sed -i "s|^\.so $i|\.so $dest|" man/man?/*.[1-9]
done

sh postfix-install -non-interactive \
       install_root=%{buildroot} \
       config_directory=%{postfix_config_dir} \
       daemon_directory=%{postfix_daemon_dir} \
       command_directory=%{postfix_command_dir} \
       queue_directory=%{postfix_queue_dir} \
       data_directory=%{postfix_data_dir} \
       sendmail_path=%{postfix_command_dir}/sendmail.postfix \
       newaliases_path=%{_bindir}/newaliases.postfix \
       mailq_path=%{_bindir}/mailq.postfix \
       mail_owner=%{postfix_user} \
       setgid_group=%{maildrop_group} \
       manpage_directory=%{_mandir} \
       sample_directory=%{postfix_sample_dir} \
       readme_directory=%{postfix_readme_dir} || exit 1

# This installs into the /etc/rc.d/init.d directory
mkdir -p %{buildroot}%{_initscriptdir}
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/postfix

# Systemd
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE2} %{buildroot}%{_unitdir}
install -m 755 %{SOURCE4} %{buildroot}%{postfix_daemon_dir}/aliasesdb

install -c auxiliary/rmail/rmail %{buildroot}%{_bindir}/rmail.postfix

for i in active bounce corrupt defer deferred flush incoming private saved maildrop public pid saved trace; do
    mkdir -p %{buildroot}%{postfix_queue_dir}/$i
done

# install performance benchmark tools by hand
for i in smtp-sink smtp-source ; do
  install -c -m 755 bin/$i %{buildroot}%{postfix_command_dir}/
  install -c -m 755 man/man1/$i.1 %{buildroot}%{_mandir}/man1/
done

## RPM compresses man pages automatically.
## - Edit postfix-files to reflect this, so post-install won't get confused
##   when called during package installation.
sed -i -r "s#(/man[158]/.*.[158]):f#\1.gz:f#" %{buildroot}%{postfix_daemon_dir}/postfix-files

cat %{buildroot}%{postfix_daemon_dir}/postfix-files
%if %{with sasl}
# Install the smtpd.conf file for SASL support.
mkdir -p %{buildroot}%{sasl_config_dir}
install -m 644 %{SOURCE100} %{buildroot}%{sasl_config_dir}/smtpd.conf
%endif

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -m 644 %{SOURCE101} %{buildroot}%{_sysconfdir}/pam.d/smtp.postfix

# prepare documentation
mkdir -p %{buildroot}%{postfix_doc_dir}
cp -p %{SOURCE3} COMPATIBILITY LICENSE TLS_ACKNOWLEDGEMENTS TLS_LICENSE %{buildroot}%{postfix_doc_dir}

mkdir -p %{buildroot}%{postfix_doc_dir}/examples{,/chroot-setup}
cp -pr examples/{qmail-local,smtpd-policy} %{buildroot}%{postfix_doc_dir}/examples
cp -p examples/chroot-setup/LINUX2 %{buildroot}%{postfix_doc_dir}/examples/chroot-setup

cp conf/{main,bounce}.cf.default %{buildroot}%{postfix_doc_dir}
sed -i 's#%{postfix_config_dir}\(/bounce\.cf\.default\)#%{postfix_doc_dir}\1#' %{buildroot}%{_mandir}/man5/bounce.5
rm -f %{buildroot}%{postfix_config_dir}/{TLS_,}LICENSE

find %{buildroot}%{postfix_doc_dir} -type f | xargs chmod 644
find %{buildroot}%{postfix_doc_dir} -type d | xargs chmod 755

%if %{with pflogsumm}
install -c -m 644 pflogsumm-%{pflogsumm_ver}/pflogsumm-faq.txt %{buildroot}%{postfix_doc_dir}/pflogsumm-faq.txt
install -c -m 644 pflogsumm-%{pflogsumm_ver}/pflogsumm.1 %{buildroot}%{_mandir}/man1/pflogsumm.1
install -c pflogsumm-%{pflogsumm_ver}/pflogsumm.pl %{buildroot}%{postfix_command_dir}/pflogsumm
%endif

# install qshape
mantools/srctoman - auxiliary/qshape/qshape.pl > qshape.1
install -c qshape.1 %{buildroot}%{_mandir}/man1/qshape.1
install -c auxiliary/qshape/qshape.pl %{buildroot}%{postfix_command_dir}/qshape

# remove alias file
rm -f %{buildroot}%{postfix_config_dir}/aliases

# create /usr/lib/sendmail
mkdir -p %{buildroot}/usr/lib
pushd %{buildroot}/usr/lib
ln -sf ../sbin/sendmail.postfix .
popd

mkdir -p %{buildroot}%{_var}/lib/misc
touch %{buildroot}%{_var}/lib/misc/postfix.aliasesdb-stamp

# prepare alternatives ghosts 
for i in %{postfix_command_dir}/sendmail %{_bindir}/{mailq,newaliases,rmail} \
	%{_sysconfdir}/pam.d/smtp /usr/lib/sendmail \
	%{_mandir}/{man1/{mailq.1,newaliases.1},man5/aliases.5,man8/sendmail.8}
do
	touch %{buildroot}$i
done

%post
[ $1 -eq 1 ] && bin/systemctl daemon-reload >/dev/null 2>&1 || :

# upgrade configuration files if necessary
%{_sbindir}/postfix set-permissions upgrade-configuration \
	daemon_directory=%{postfix_daemon_dir} \
	command_directory=%{postfix_command_dir} \
	mail_owner=%{postfix_user} \
	setgid_group=%{maildrop_group} \
	manpage_directory=%{_mandir} \
	sample_directory=%{postfix_sample_dir} \
	readme_directory=%{postfix_readme_dir} &> /dev/null

%{_sbindir}/alternatives --install %{postfix_command_dir}/sendmail mta %{postfix_command_dir}/sendmail.postfix 30 \
	--slave %{_bindir}/mailq mta-mailq %{_bindir}/mailq.postfix \
	--slave %{_bindir}/newaliases mta-newaliases %{_bindir}/newaliases.postfix \
	--slave %{_sysconfdir}/pam.d/smtp mta-pam %{_sysconfdir}/pam.d/smtp.postfix \
	--slave %{_bindir}/rmail mta-rmail %{_bindir}/rmail.postfix \
	--slave /usr/lib/sendmail mta-sendmail /usr/lib/sendmail.postfix \
	--slave %{_mandir}/man1/mailq.1.bz2 mta-mailqman %{_mandir}/man1/mailq.postfix.1.bz2 \
	--slave %{_mandir}/man1/newaliases.1.bz2 mta-newaliasesman %{_mandir}/man1/newaliases.postfix.1.bz2 \
	--slave %{_mandir}/man8/sendmail.8.bz2 mta-sendmailman %{_mandir}/man1/sendmail.postfix.1.bz2 \
	--slave %{_mandir}/man5/aliases.5.bz2 mta-aliasesman %{_mandir}/man5/aliases.postfix.5.bz2 \
	--initscript postfix

%if %{with sasl}
# Move sasl config to new location
if [ -f %{_libdir}/sasl2/smtpd.conf ]; then
	mv -f %{_libdir}/sasl2/smtpd.conf %{sasl_config_dir}/smtpd.conf
	/sbin/restorecon %{sasl_config_dir}/smtpd.conf 2> /dev/null
fi
%endif

exit 0

%pre
# Add user and groups if necessary
%{_sbindir}/groupadd -g %{maildrop_gid} -r %{maildrop_group} 2>/dev/null
%{_sbindir}/groupadd -g %{postfix_gid} -r %{postfix_group} 2>/dev/null
%{_sbindir}/groupadd -g 12 -r mail 2>/dev/null
%{_sbindir}/useradd -d %{postfix_queue_dir} -s /sbin/nologin -g %{postfix_group} -G mail -M -r -u %{postfix_uid} %{postfix_user} 2>/dev/null
exit 0

%preun
if [ "$1" = 0 ]; then
    /bin/systemctl --no-reload disable postfix.service > /dev/null 2>&1 || :
    /bin/systemctl stop postfix.service > /dev/null 2>&1 || :
    %{_sbindir}/alternatives --remove mta %{postfix_command_dir}/sendmail.postfix
fi     
exit 0

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ "$1" -ge 1 ]; then
    /bin/systemctl try-restart postfix.service >/dev/null 2>&1 || :
fi
exit 0

%post sysvinit
/sbin/chkconfig --add postfix >/dev/null 2>&1 ||:

%preun sysvinit
if [ "$1" = 0 ]; then
    %{_initscriptdir}/postfix stop >/dev/null 2>&1 ||:
    /sbin/chkconfig --del postfix >/dev/null 2>&1 ||:
fi

%postun sysvinit
[ "$1" -ge 1 ] && %{_initscriptdir}/postfix condrestart >/dev/null 2>&1 ||:

%triggerun -- postfix < %{sysv2systemdnvr}
%{_bindir}/systemd-sysv-convert --save postfix >/dev/null 2>&1 ||:
/bin/systemctl enable postfix.service >/dev/null 2>&1
/sbin/chkconfig --del postfix >/dev/null 2>&1 || : 
/bin/systemctl try-restart postfix.service >/dev/null 2>&1 || :

%triggerpostun -n postfix-sysvinit -- postfix < 2.8.7
/sbin/chkconfig --add postfix >/dev/null 2>&1 || :
 

%clean
rm -rf %{buildroot}


%files

# For correct directory permissions check postfix-install script.
# It reads the file postfix-files which defines the ownership
# and permissions for all files postfix installs.

%defattr(-, root, root)

# Config files not part of upstream

%if %{with sasl}
%config(noreplace) %{sasl_config_dir}/smtpd.conf
%endif
%config(noreplace) %{_sysconfdir}/pam.d/smtp.postfix
%{_unitdir}/postfix.service

# Documentation

%{postfix_doc_dir}
%if %{with pflogsumm}
%exclude %{postfix_doc_dir}/pflogsumm-faq.txt
%endif

# Misc files

%dir %attr(0755, root, root) %{postfix_config_dir}
%dir %attr(0755, root, root) %{postfix_daemon_dir}
%dir %attr(0755, root, root) %{postfix_queue_dir}
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/active
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/bounce
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/corrupt
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/defer
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/deferred
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/flush
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/hold
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/incoming
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/saved
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/trace
%dir %attr(0730, %{postfix_user}, %{maildrop_group}) %{postfix_queue_dir}/maildrop
%dir %attr(0755, root, root) %{postfix_queue_dir}/pid
%dir %attr(0700, %{postfix_user}, root) %{postfix_queue_dir}/private
%dir %attr(0710, %{postfix_user}, %{maildrop_group}) %{postfix_queue_dir}/public
%dir %attr(0700, %{postfix_user}, root) %{postfix_data_dir}

%attr(0644, root, root) %{_mandir}/man1/post*.1*
%attr(0644, root, root) %{_mandir}/man1/smtp*.1*
%attr(0644, root, root) %{_mandir}/man1/*.postfix.1*
%attr(0644, root, root) %{_mandir}/man5/access.5*
%attr(0644, root, root) %{_mandir}/man5/[b-v]*.5*
%attr(0644, root, root) %{_mandir}/man5/*.postfix.5*
%attr(0644, root, root) %{_mandir}/man8/[a-qt-v]*.8*
%attr(0644, root, root) %{_mandir}/man8/s[ch-p]*.8*

%attr(0755, root, root) %{postfix_command_dir}/smtp-sink
%attr(0755, root, root) %{postfix_command_dir}/smtp-source

%attr(0755, root, root) %{postfix_command_dir}/postalias
%attr(0755, root, root) %{postfix_command_dir}/postcat
%attr(0755, root, root) %{postfix_command_dir}/postconf
%attr(2755, root, %{maildrop_group}) %{postfix_command_dir}/postdrop
%attr(0755, root, root) %{postfix_command_dir}/postfix
%attr(0755, root, root) %{postfix_command_dir}/postkick
%attr(0755, root, root) %{postfix_command_dir}/postlock
%attr(0755, root, root) %{postfix_command_dir}/postlog
%attr(0755, root, root) %{postfix_command_dir}/postmap
%attr(0755, root, root) %{postfix_command_dir}/postmulti
%attr(2755, root, %{maildrop_group}) %{postfix_command_dir}/postqueue
%attr(0755, root, root) %{postfix_command_dir}/postsuper
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/access
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/canonical
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/generic
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/header_checks
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/main.cf
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/master.cf
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/relocated
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/transport
%attr(0644, root, root) %config(noreplace) %{postfix_config_dir}/virtual
%attr(0755, root, root) %{postfix_daemon_dir}/[^mp]*
%attr(0644, root, root) %{postfix_daemon_dir}/main.cf
%attr(0644, root, root) %{postfix_daemon_dir}/master.cf
%attr(0755, root, root) %{postfix_daemon_dir}/master
%attr(0755, root, root) %{postfix_daemon_dir}/pickup
%attr(0755, root, root) %{postfix_daemon_dir}/pipe
%attr(0755, root, root) %{postfix_daemon_dir}/post-install
%attr(0644, root, root) %{postfix_daemon_dir}/postfix-files
%attr(0755, root, root) %{postfix_daemon_dir}/postfix-script
%attr(0755, root, root) %{postfix_daemon_dir}/postfix-wrapper
%attr(0755, root, root) %{postfix_daemon_dir}/postmulti-script
%attr(0755, root, root) %{postfix_daemon_dir}/postscreen
%attr(0755, root, root) %{postfix_daemon_dir}/proxymap
%attr(0755, root, root) %{_bindir}/mailq.postfix
%attr(0755, root, root) %{_bindir}/newaliases.postfix
%attr(0755, root, root) %{_bindir}/rmail.postfix
%attr(0755, root, root) %{_sbindir}/sendmail.postfix
%attr(0755, root, root) /usr/lib/sendmail.postfix

%ghost %{_sysconfdir}/pam.d/smtp

%ghost %{_mandir}/man1/mailq.1.*
%ghost %{_mandir}/man1/newaliases.1.*
%ghost %{_mandir}/man5/aliases.5.*
%ghost %{_mandir}/man8/sendmail.8.*

%ghost %attr(0755, root, root) %{_bindir}/mailq
%ghost %attr(0755, root, root) %{_bindir}/newaliases
%ghost %attr(0755, root, root) %{_bindir}/rmail
%ghost %attr(0755, root, root) %{_sbindir}/sendmail
%ghost %attr(0755, root, root) /usr/lib/sendmail

%ghost %attr(0644, root, root) %{_var}/lib/misc/postfix.aliasesdb-stamp

%files sysvinit
%defattr(-, root, root, -)
%{_initscriptdir}/postfix

%files perl-scripts
%defattr(-, root, root)
%attr(0755, root, root) %{postfix_command_dir}/qshape
%attr(0644, root, root) %{_mandir}/man1/qshape*
%if %{with pflogsumm}
%doc %{postfix_doc_dir}/pflogsumm-faq.txt
%attr(0644, root, root) %{_mandir}/man1/pflogsumm.1*
%attr(0755, root, root) %{postfix_command_dir}/pflogsumm
%endif

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.4-2m)
- rebuild against pcre-8.31

* Sun Aug  5 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.4-1m)
- update to 2.9.4

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.3-1m)
- update to 2.9.3

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1
- rebuild against libdb-5.3.15

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.7-1m)
- update 2.8.7

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.6-1m)
- update 2.8.6

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.5-3m)
- rebuild against perl-5.14.2

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7.5-2m)
- rebuild against libdb

* Tue Jul 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.5-1m)
- update to 2.7.5 (contains Linux kernel version 3 support)

* Sun Jun 26 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.7.4-3m)
- build fix for Linux 3.0

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-2m)
- rebuild against perl-5.14.1

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-1m)
- [SECURITY] CVE-2011-1720
- update to 2.7.4

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.3-3m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-2m)
- rebuild against mysql-5.5.10

* Thu Mar 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- [SECURITY] CVE-2011-0411
- update to 2.7.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.2-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.2-1m)
- update to 2.7.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.1-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update to 2.7.1

* Tue Apr  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-3m)
- mark initscript as %%config

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-2m)
- rebuild against openssl-1.0.0

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0
- do no use /etc/mail/aliases which is one of momonga specific
  settings. i do not like that.
- setup >= 2.8.15-1m
- the priority of alternatives is 70

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-3m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5

* Thu Aug  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Wed Jul 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-3m)
- revive /etc/postfix/aliases
- replace /etc/aliases to /etc/postfix/aliases
- revise initscripts

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.2-2m)
- stop daemon

* Tue Jun 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-1m)
- sync with Rawhide (2:2.6.2-1)
-- 
-- * Thu Jun 18 2009 Miroslav Lichvar <mlichvar@redhat.com> 2:2.6.2-1
-- - update to 2.6.2
-- 
-- * Tue May 26 2009 Miroslav Lichvar <mlichvar@redhat.com> 2:2.6.1-1
-- - update to 2.6.1
-- - move non-config files out of /etc/postfix (#490983)
-- - fix multilib conflict in postfix-files (#502211)
-- - run chroot-update script in init script (#483186)
-- - package examples (#251677)
-- - provide all alternatives files
-- - suppress postfix output in post script

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.6-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.6-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.6-1m)
- update to 2.5.6
-- update Patch1,5 for fuzz=0

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.5-2m)
- rebuild against db4-4.7.25-1m

* Wed Sep  3 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.5-1m)
- [SECURITY] CVE-2008-3889
- update to 2.5.5

* Sun Aug 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.4-1m)
- [SECURITY] CVE-2008-2936 CVE-2008-2937
- update to 2.5.4

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.1-2m)
- rebuild against gcc43

* Thu Mar 20 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.5.1-1m)
- up to 2.5.1

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-3m)
- rebuild against openldap-2.4.8

* Tue Dec 11 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.4.6-2m)
- fixed segfault on startup when gethostbyaddr() failed

* Fri Nov 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.6-1m)
- up to 2.4.6

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.1-3m)
- rebuild against db4-4.6.21

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.1-2m)
- rebuild against pam-0.99.7-1m
- update postfix-pam.conf 

* Wed May  9 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Thu Mar 29 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.4.0-1m)
- version up

* Sat Mar 03 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.8-1m)
- version up

* Wed Jan 31 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.7-1m)
- version up 2.3.7

* Wed Jan 03 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.6-1m)
- version up 2.3.6

* Tue Dec 12 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.5-1m)
- version up 2.3.5

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.4-2m)
- rebuild against db-4.5

* Fri Nov 03 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.4-1m)
- version up 2.3.4

* Fri Sep 08 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.3-1m)
- version up 2.3.3

* Sat Aug  5 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-2m)
- fix alternatives

* Fri Aug  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.2-1m)
- version up 2.3.2

* Wed Jul 26 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.3.0-2m)
- to use cyrus-sasl

* Mon Jul 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.0-1m)
- version up 2.3.0

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.10-1m)
- version up 2.2.10
- rebuild against openldap-2.3.19
- rebuild against openssl-0.9.8a

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.8-1m)
- version up 2.2.8
- rebuild against openldap-2.3.11

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.1-4m)
- rebuild against db4.3

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.2.1-3m)
- rebuild against for MySQL-4.1.14

* Thu Apr  7 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.1-2m)
- fix chown warning message

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.1-1m)
- up to 2.2.1

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.1-7m)
- revised docdir permission

* Tue Jul 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.1-6m)
- specify PreReq by package name instead of path

* Mon Jul 12 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.1-5m)
- requires shadow-utils

* Sun Jul 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.1-4m)
- modify spec

* Sat Jul 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.1-3m)
- import patch and specfile from FC2

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.1-2m)
- rebuild against db4.2

* Wed May 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (2.1.0-2m)
- several files are missed. See added at 2.1.0.

* Fri Apr 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0
- nename and update Patch0: postfix-config.patch
- fix deleted from tarball files: 0README, examples/virtual-setup/*, conf/sample-*
- add files: AAAREADME

* Thu Feb 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-4m)
- update patch1: ipv6 and TLS patch
- change default %%define TLS_SUPPORT 0
- delete conf/sample-tls.cf.ipv6

* Mon Feb  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.18-3m)
- listen only localhost
- support for Maildir

* Fri Jan 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-2m)
- fix %%if %%{TLS_SUPPORT}

* Thu Jan 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18
- security fix

* Wed Jan 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17
- update tls+ipv6 patch to 1.20-pf-2.0.16

* Fri Dec 26 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.16-6m)
- add New tls+ipv6 patch
- comment out about old tls patch
- new patch is combind tls and ipv6 patch,
  but default is tls=1 and ipv6=0
  now ipv6 flag is integrated to tls flag,
  then default tls=1 make ipv6=1
- add BuildRequires: groff for use nroff
- update jmanname postfix_jman_man to 2.0.16

* Fri Dec 19 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.16-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Nov 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.16-4m)
- force commet out
- use %%global

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.16-3m)
- rebuild against openldap

* Fri Oct 24 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.0.16-2m)
- fix License: preamble

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.16-1m)
- update to 2.0.16

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.0.13-4m)
- rebuild against for MySQL-4.0.14-1m
- fix License: "postfix"
  IBM Public License - see LICENSE file

* Wed Aug 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.13-3m)
- update pfixtls to 0.8.15
- set chgrp and chmod to sasldb2 on startup

* Fri Jul  4 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.13-2m)
- copy cyrus-sasl2 database.

* Wed Jul  2 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.13-1m)
  update to 2.0.13
  tls 0.8.14-2.0.12-0.9.7b
  jman 2.0.13

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.12-2m)
  rebuild against cyrus-sasl2

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.12-1m)
- update to 2.0.12

* Thu Jun 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.11-1m)
  update to 2.0.11

* Fri May 23 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.10-1m)
  update to 2.0.10

* Fri Apr 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8
- update jmanname to postfix_jman_man-2.0.8
- delete define name postfix
- move summary and name tag to top of spec file for specopt
- comment out Epoch tag
- add NoSource: 100

* Sun Mar 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7
- use old patch (where defined tls_name)
- tmp comment out %%doc IPv6_README and ifdef ipv6

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.6-1m)
  update to 2.0.6

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.12-4m)
- rebuild against for gdbm

* Thu Dec 26 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.12-3m)
- change location of Source9

* Sun Dec 22 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.12-2m)
- apply patch from Yonekawa Susumu [Momonga-devel.ja:01094]
  support specopt
  update TLS patches
- always include all patches

* Wed Nov 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.12-1m)
- update to  1.1.12
  bugfix

* Wed Aug  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.11-4m)
- suppress error message 

* Mon Aug  5 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.11-3m)
- copy /etc/sasldb to chroot environment at boot

* Fri May 31 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.11-2k)
- update to postfix 1.1.11
- update japanese manual to 1.1.11 (http://www.kobitosan.net/postfix/)
- comment out _ipv6 macro

* Wed May 15 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.1.10-2k)
- update to 1.1.10

* Wed May 15 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.1.9-2k)
- update to 1.1.9

* Wed May 08 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.1.8-6k)
- update IPv6 patch to 1.1.8 from [postfix-jp:01604]

* Mon May 06 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.1.8-4k)
- remove chmod,chown in %post section
- fix owner,group attribute in %files section

* Sun May 05 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.8-2k)
- update to 1.1.8

* Wed May 01 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.1.7-8k)
- stop copying libdb.so to chroot environment

* Wed May 01 2002 Toru Hoshina <t@kondara.org>
- (1.1.7-6k)
- add prereq findutils.

* Sat Apr 27 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.1.7-4k)
- new IPv6 patch include from [postfix-jp:01568],
  but default setting isn't apply this patch
- add man-pages-ja >= 0.5-6k at BuildRequires

* Tue Apr 02 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.7-2k)
- update to 1.1.7
- add %{SO_SUFFIX} for alpha
- update japanese manpage-1.1.6

* Wed Mar 27 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.1.6-4k)
- libresolv.so.2.1 for alpha.

* Wed Mar 27 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.1.6-2k)
- update to 1.1.6

* Wed Mar 13 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.5-2k)
- update to 1.1.5

* Sat Mar 09 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.4-6k)
- update URL postfix-TLS patch
- remove postfix-config-tls.patch (append sample config)

* Thu Mar 07 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.4-4k)
- fix on update

* Mon Feb 25 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.1.4-2k)
- update to 1.1.4

* Sat Feb 23 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.3-4k)
- use db4!
- requires openldap version 2.0.18-8k
- rewrite main.cf daemon_directory = /usr/lib/postfix -> /usr/libexec/postfix

* Mon Feb 18 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.3-2k)
- update to official release postfix-1.1.3
- add japanese manual (http://www.kobitosan.net/postfix/)
- use postdrop gid.
- remove postfix-README_maildrop_security.txt (postdrop is default)
- merge rawhide postfix-1.1.2-3 spec.
- build with chroot environment

* Wed Jan 22 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.1-2k)
- update to official release postfix-1.1.1
- refresh chroot files at init.d/postfix start (postfix-init_script.sh)
- restart after update.

* Tue Jan 22 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.0-2k)
- update to official release postfix-1.1.0
- use db3

* Fri Jan 18 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (20010228-pl08-4k)
  build /etc/postfix/aliases.db in firsttime.

* Fri Nov 16 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl08-2k)
- update to postfix 20010228 pl08 for bugfix

* Mon Nov 05 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl06-2k)
- update to postfix 20010228 pl06 for bugfix

* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
- (20010228-pl05-4k)
- better handling resolv.conf 
- chkconfig -> /sbin/chkconfig
- add Requires (setup for /etc/services, diffutils for cmp, glibc for /etc/localtime ,etc)

* Sat Oct 27 2001 Toru Hoshina <t@kondara.org>
- (20010228-pl05-4k)
- fixed spec file.

* Thu Sep 19 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl05-2k)
- update to 20010228pl05 for bugfix

* Tue Aug 06 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl04-4k)
- copy libraries by %triggerin
- (thanks to Toshiro Hikita)

* Tue Aug 04 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl04-2k)
- update to 20010228pl04.

* Tue Jul 17 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl03-8k)
- rebuild all *.db (Berkeley db3) files at %post

* Sun Jul  8 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl03-6k)
- enable LDAP
- enable SMTP AUTH (SASL)
- append documents

* Thu Jul  5 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (20010228-pl03-4k)
- use install instead of ln (undo 20010228-pl01-4k change)
  since symlink to /etc is meaningless on chroot environment
- do not install localtime to %{chroot_directory}%{_libdir}/zoneinfo
  because postfix warns when running on chroot environment
- add BuildRequires: db1-devel, db3-devel
- add noreplace to configs
- add more docs
- edit grep pattern in %%pre

* Mon Jun 18 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl03-2k)
- update to postfix-20010228-pl03
- fix to use /usr/bin/procmail as default

* Mon May  7 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl02-2k)
- update to postfix-20010228-pl2
- update to pfixtls-0.7.2-20010228-pl02-0.9.6a

* Mon Apr 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (20010228-pl01-4k)
- modified spec file not to install but to ln

* Sat Mar 31 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-pl01-3k)
- update to official postfix-20010228-pl01.
- fixed source URL.

* Thu Mar 12 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-5k)
- fixed License, etc.
- IPv6 patch temporary disabled.

* Thu Mar  8 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (20010228-3k)
- postfix-20010228 is 1st official release.
- IPv6 patch from kame.net (a few fixed for linux)
- TLS patch from ftp.aet.tu-cottbus.de
- IPv6 patch and TLS patch may be conflict...

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (20001217-5k)
- rebuild against rpm-3.0.5-39k

* Wed Jan 17 2001 WATABE Toyokazu <toy2@kondara.org> 20010117-3k
- update 20010117
- update ipv6 patch (ported from ftp://ftp.pld.org.pl/PLD-1.0/)
- use %_initscriptdir

* Mon Oct 16 2000 Toyokazu WATABE <toy2@kondara.org> 20001005-3k
- add ipv6 patch.

* Mon Oct 16 2000 Toyokazu WATABE <toy2@kondara.org> 20001005-2k
- merge nori's version.
- add documents of pfixtls.

* Wed Oct 11 2000 Toyokazu WATABE <toy2@kondara.org> 20001005-1k
- optionalize TLSv1
- update 20001005

* Wed Oct 04 2000 KUSUNOKI, Masanori <nori@kondara.org>
- update to 20000924 SNAPSHOT.
- enable TLSv1.

* Thu Sep 14 2000 Toyokazu WATABE <toy2@kondara.org> 20000531-3k
- unable TLSv1.

* Thu Jun 29 2000 Masanori Kusunoki <nori@kondara.org> 20000531
- updated to snapshot-20000531
- enable TLSv1.

* Tue Jun  6 2000 Toyokazu Watabe <toy2@kondara.org> 19991231-pl08-1k1
- Add README.txt

* Fri Jun  2 2000 Toyokazu Watabe <toy2@kondara.org> 19991231-pl08-0k1
- Update to 19991231-pl08

* Thu Jun  1 2000 Toyokazu Watabe <toy2@kondara.org> 19991231-pl07-1k2
- fix chroot problems, etc.

* Tue May 16 2000 Toyokazu Watabe <toy2@kondara.org> 19991231-pl07-1k1
- merge with postfix-19991231_pl07-1.src.rpm.
  (very thanks to Simon Mudd <sjmudd@pobox.com>)

* Wed May 10 2000 Toyokazu Watabe <toy2@kondara.org> 19991231-5k2
- add -Wno-comment to OPT, etc.

* Fri May 05 2000 Yoann Vandoorselaere <yoann@mandrakesoft.com>
- Fix an aliases.db problem...

* Tue May  2 2000 Toyokazu Watabe <toy2@kondara.org> 19991231-5k1
- customize spec and main.cf.patch for Kondara.
- be a NoSrc :-P

* Tue Mar 21 2000 Yoann Vandoorselaere <yoann@mandrakesoft.com> 19991231-5mdk
- Fix group.

* Wed Mar  8 2000 Pixel <pixel@mandrakesoft.com> 19991231-4mdk
- add prereq wc

* Mon Jan  3 2000 Jean-Michel Dault <jmdault@netrevolution.com>
- updated to 19991231
- added postfix group
- corrected aliases.db bug

* Mon Dec 27 1999 Jerome Dumonteil <jd@mandrakesoft.com>
- Add postfix check in post to create sub dirs in spool

* Mon Dec 20 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- Add -a $DOMAIN -d $LOGNAME to procmail (philippe).
- New banner.

* Wed Nov 10 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- fix if conflicts with sendmail.

* Sat Jun  5 1999 Axalon Bloodstone <axalon@linux-mandrake.com>
- install bins from libexec/

* Sat Jun  5 1999 Bernhard Rosenkraenzer <bero@mandrakesoft.com>
- 19990601
- .spec cleanup for easier updates

* Wed May 26 1999 Axalon Bloodstone <axalon@linux-mandrake.com>
- created link from /usr/sbin/sendmail to /usr/lib/sendmail
  so it doesn't bug out when i rpm -e sendmail
- Now removes /var/lock/subsys/postfix like a good little prog
  upon rpm -e

* Fri Apr 23 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>

- Mandrake adptations.

* Tue Apr 13 1999 Arne Coucheron <arneco@online.no>
  [19990317-pl04-1]

* Tue Mar 30 1999 Arne Coucheron <arneco@online.no>
  [19990317-pl03-2]
- Castro, Castro, pay attention my friend. You're making it very hard
  maintaining the package if you don't follow the flow of the releases

* Thu Mar 25 1999 Arne Coucheron <arneco@online.no>
  [19990317-pl02-1]

* Tue Mar 23 1999 Arne Coucheron <arneco@online.no>
  [19990317-3]
- added bugfix patch01

* Sat Mar 20 1999 Arne Coucheron <arneco@online.no>
  [19990317-2]
- removed the mynetworks line in main.cf, let postfix figure it out
- striping of the files in /usr/sbin
- alias database moved to /etc/postfix and made a symlink to it in /etc
- enabled procmail support in main.cf and added it to Requires:
- check status on master instead of postfix in the init script
- obsoletes postfix-beta
- had to move some of my latest changelog entries up here since Edgard Castro
  didn't follow my releases

* Thu Mar 18 1999 Edgard Castro <castro@usmatrix.net>
  [19990317]

* Tue Mar 16 1999 Edgard Castro <castro@usmatrix.net>
  [alpha-19990315]

* Tue Mar  9 1999 Edgard Castro <castro@usmatrix.net>
  [19990122-pl01-2]
- shell and gecho information changed to complie with Red Hat stardand
- changed the name of the rpm package to postfix, instead of postfix-beta

* Tue Feb 16 1999 Edgard Castro <castro@usmatrix.net>
  [19990122-pl01-1]

* Sun Jan 24 1999 Arne Coucheron <arneco@online.no>
  [19990122-1]
- shell for postfix user changed to /bin/true to avoid logins to the account
- files in /usr/libexec/postfix moved to /usr/lib/postfix since this complies
  more with the Red Hat standard

* Wed Jan 06 1999 Arne Coucheron <arneco@online.no>
  [19981230-2]
- added URL for the source
- added a cron job for daily check of errors
- sample config files moved from /etc/postfix/sample to the docdir 
- dropped making of symlinks in /usr/sbin and instead installing the real
  files there
- because of the previous they're not needed anymore in /usr/libexec/postfix,
  so they are removed from that place

* Fri Jan 01 1999 Arne Coucheron <arneco@online.no>
  [19981230-1]

* Tue Dec 29 1998 Arne Coucheron <arneco@online.no>
  [19981222-1]
- first build of rpm version


