%global        momorel 1
%global        base_name kde-telepathy
%global        unstable 0
%if 0%{unstable}
%global        release_dir unstable
%else
%global        release_dir stable
%endif
%global        kdever 4.13.0
%global        ftpdirver 0.8.1
%global        sourcedir %{release_dir}/%{base_name}/%{ftpdirver}/src
%global        qtver 4.8.5
%global        qtrel 1m
%global        cmakever 2.8.5
%global        cmakerel 2m

Name:          ktp-desktop-applets
Summary:       KDE Telepathy desktop applets
Version:       %{ftpdirver}
Release:       %{momorel}m%{?dist}
License:       LGPLv2+
Group:         System Environment/Libraries
URL:           http://www.kde.org
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:      0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: ktp-common-internals-devel >= %{version}
BuildRequires: ktp-contact-list >= %{version}
BuildRequires: telepathy-qt4-devel >= 0.9.3
Obsoletes:     telepathy-kde-contact-applet
Obsoletes:     ktp-contact-applet < %{version}
Obsoletes:     ktp-presence-applet < %{version}
Provides:      ktp-contact-applet = %{version}
Provides:      ktp-presence-applet = %{version}

%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%files
%doc COPYING COPYING.LIB
%{_kde4_libdir}/kde4/plasma_applet_ktp_contact.so
%{_kde4_libdir}/kde4/plasma_applet_ktp_presence.so
%{_kde4_appsdir}/plasma-desktop/updates/50-renameKTpApplets.js
%{_kde4_appsdir}/plasma/plasmoids/org.kde.ktp-chat
%{_kde4_appsdir}/plasma/plasmoids/org.kde.ktp-contact
%{_kde4_appsdir}/plasma/plasmoids/org.kde.ktp-contactlist
%{_kde4_datadir}/kde4/services/plasma-applet-ktp-*.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/plasma_applet_org.kde.ktp-contact.mo
%{_kde4_datadir}/locale/*/LC_MESSAGES/plasma_applet_org.kde.ktp-presence.mo

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Wed Mar 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Wed Feb 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.80-1m)
- update to 0.7.80

* Fri Nov 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-2m)
- revise source URI

* Mon Oct 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Thu Sep 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.80-1m)
- update to 0.6.80

* Wed Aug  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Apr  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- initial build for Momonga Linux
