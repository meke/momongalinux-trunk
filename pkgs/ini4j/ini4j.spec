%global momorel 4

# Prevent brp-java-repack-jars from being run.
%global __jar_repack %{nil}

%global servlet_jar %{_javadir}/servlet.jar
%global jetty_jar %{_javadir}/jetty/jetty.jar

Name:           ini4j
Version:        0.4.1
Release:        %{momorel}m%{?dist}
Summary:        Java API for handling files in Windows .ini format

Group:          Development/Libraries
License:        "ASL 2.0"
URL:            http://www.ini4j.org/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}-src.zip
Source1:        http://www.apache.org/licenses/LICENSE-2.0.txt
Source2:        %{name}-%{version}-build.xml

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

# See http://ini4j.sourceforge.net/dependencies.html
BuildRequires:  jpackage-utils
BuildRequires:  ant
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  tomcat5-servlet-2.4-api >= 5.5
BuildRequires:  jetty >= 4.2.2

Requires:       jpackage-utils
Requires:       java >= 1.6.0
Requires:       tomcat5-servlet-2.4-api >= 5.5

%description
The [ini4j] is a simple Java API for handling configuration files in Windows 
.ini format. Additionally, the library includes Java Preferences API 
implementation based on the .ini file.

%package javadoc
Summary:        Javadocs for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
Requires:       jpackage-utils

%description javadoc
This package contains the API documentation for %{name}.

%prep

%setup -q

cp -a %{SOURCE1} ./LICENSE-2.0.txt
cp -a %{SOURCE2} ./build.xml

find . -type f \( -iname "*.jar" -o -iname "*.class" \) | xargs -t %{__rm} -f

# remove test sources
%{__rm} -rf src/test
# remove site sources
%{__rm} -rf src/site

%build

%ant -Dbuild.servlet.jar=%{servlet_jar} -Dbuild.jetty.jar=%{jetty_jar} build javadoc

%install
%{__rm} -rf %{buildroot}

# JAR
%{__mkdir_p} %{buildroot}%{_javadir}
%{__cp} -p dist/%{name}-%{version}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
(cd %{buildroot}%{_javadir} && %{__ln_s} %{name}-%{version}.jar %{name}.jar)

# Javadoc
%{__mkdir_p} %{buildroot}%{_javadocdir}/%{name}
%{__cp} -rp build/doc/api/* %{buildroot}%{_javadocdir}/%{name}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_javadir}/*
%doc LICENSE-2.0.txt src/main/java/org/ini4j/package.html

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- full rebuild for mo7 release

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-1m)
- sync with Rawhide (0.4.1-2) for netbeans-6.7.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-1m)
- import from Fedora 11 for netbeans

* Mon Mar 02 2009 Victor Vasilyev <victor.vasilyev@sun.com> 0.3.2-6
- BR tomcat5, because tomcat-tomcat-parent.pom is required

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Aug 27 2008 Victor Vasilyev <victor.vasilyev@sun.com> 0.3.2-4
- Versionless symbolic link to the jar is added
- Redundant user-defined macro for the poms directory is removed
- Description is formatted
- The LICENSE file added

* Thu Aug 21 2008 Victor Vasilyev <victor.vasilyev@sun.com> 0.3.2-3
- Ability to switch on the alternative implementations is removed
- The %%{__install} macro is used everywhere instead of the install command
- All macroses reflecting a folders layout of the project are removed
- Explicit scriptlet dependencies for /bin/sh are removed
- Owning of /etc/maven/fragments/ini4j is removed

* Fri Aug 15 2008 Victor Vasilyev <victor.vasilyev@sun.com> 0.3.2-2
- Documentation added
- Appropriate values of Group Tags are chosen from the official list
- The /etc/maven/fragments/ini4j file is attributed as a config(noreplace)

* Fri Jun 06 2008 Victor Vasilyev <victor.vasilyev@sun.com> 0.3.2-1
- Initial version.
  The spec provides target jar and javadoc, but 
  it doesn't provide tests and site that are 
  also defined in the original project.
