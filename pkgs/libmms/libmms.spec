%global momorel 2	

Summary: MMS stream protocol library
Name:	 libmms
Version: 0.5
Release: %{momorel}m%{?dist}
License: LGPL
Group:	 System Environment/Libraries
Source0: http://launchpad.net/libmms/trunk/%{version}/+download/libmms-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-instance.patch
URL:	  https://launchpad.net/libmms/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
LibMMS is a common library for parsing mms:// and mmsh:// type network streams. These are commonly used to stream Windows Media Video content over the web. LibMMS itself is only for receiving MMS stream, it doesn't handle sending at all.

%package devel
Summary:	Header files and libraries for a development with %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
Header files and libraries for a development with %{name}

%prep
#'
%setup -q
%patch0 -p1 -b .instance

%build
%configure
make

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

# remove libtool la
rm -rf %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc 
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/libmms
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.so
%{_libdir}/lib*.*a

%changelog
* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-2m)
- apply upstream modification

* Sun Jul 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-7m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-4m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-2m)
- rebuild against gcc43

* Wed Apr 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4-1m)
- import to Momonga for Audacious
