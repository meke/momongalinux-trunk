%global momorel 7
%global prever 2
%global betaver 4
%global svndate 09.08.10
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m
%global kdegraphicsrel 1m

Summary: (La)TeX source editor and TeX shell
Name: kile
Version: 2.1
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Publishing
URL: http://kile.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/unstable/%{name}-%{version}b%{betaver}/%{name}-%{version}b%{betaver}.tar.bz2
NoSource: 0
Source1: %{name}-%{svndate}-momonga-language-pack.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: kdegraphics >= %{kdever}-%{kdegraphicsrel}
Requires: gnuplot
Requires: texlive-pdftex
# xfig Requires: xpdf, it's broken
# Requires: xfig-plain
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: gettext
BuildRequires: libxml2

%description
Kile is a user friendly (La)TeX editor.  The main features are:
  * Compile, convert and view your document with one click.
  * Auto-completion of (La)TeX commands
  * Templates and wizards makes starting a new document very little work.
  * Easy insertion of many standard tags and symbols and the option to define
    (an arbitrary number of) user defined tags.
  * Inverse and forward search: click in the DVI viewer and jump to the
    corresponding LaTeX line in the editor, or jump from the editor to the
    corresponding page in the viewer.
  * Finding chapter or sections is very easy, Kile constructs a list of all
    the chapter etc. in your document. You can use the list to jump to the
    corresponding section.
  * Collect documents that belong together into a project.
  * Easy insertion of citations and references when using projects.
  * Advanced editing commands.

%prep
%setup -q -n %{name}-%{version}b%{betaver} -a 1

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

# build lang-pack
pushd %{name}-%{svndate}
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install lang-pack
pushd %{name}-%{svndate}
make install DESTDIR=%{buildroot} -C %{_target_platform}
popd

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# clean up
rm -rf %{buildroot}%{_kde4_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-, root, root)
%doc AUTHORS CODE-FORMATTING-STYLE.txt COPYING*
%doc ChangeLog README README.cwl doc/TODO
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/kconf_update/%{name}*
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_datadir}/dbus-1/interfaces/net.sourceforge.%{name}.main.xml
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_kde4_iconsdir}/*/*/*/%{name}.svgz
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/mimelink/text/x-%{name}pr.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-0.2.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-0.2.6m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-0.2.5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-0.2.4m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.2.3m)
- remove Requires: xfig-plain
- xfig Requires: xpdf, it's broken

* Mon Aug  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.2.2m)
- update momonga-language-pack
- change Requires for texlive

* Wed May 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.2.1m)
- update to version 2.1 beta 4

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-5m)
- touch up spec file

* Mon Dec  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.1.2m)
- add translations

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.1.1m)
- update to version 2.1 beta 3
- remove desktop.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-2m)
- License: GPLv2+

* Sun Dec  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-1m)
- version 2.0.3
- License: GPLv2

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-2m)
- change Requires from xfig to xfig-plain
- change Requires from kdelibs3 to kdegraphics3

* Fri Aug 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-2m)
- revise %%{_docdir}/HTML/*/kile/common

* Sat May 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-1m)
- version 2.0.1

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-2m)
- %%NoSource -> NoSource

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- version 2.0
- remove merged desktop.patch
- add fix-desktop.patch

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-1m)
- initial package for Momonga Linux
