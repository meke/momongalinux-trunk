%global momorel 17

%{!?with_x:%define with_x 1}

Summary: A document formatting system
Name: groff
Version: 1.18.1.4
Epoch: 1
Release: %{momorel}m%{?dist}
License: GPLv2+ and GFDL
Group: Applications/Publishing
Source0: ftp://ftp.gnu.org/gnu/groff/groff-%{version}.tar.gz
Source3: mandocj.tar.gz
Source4: man-pages-ja-GNU_groff-20000115.tar.gz
Source6: hyphen.cs
Source7: nroff
Patch1: groff-1.16-safer.patch
Patch3: groff_1.18.1-15.diff
Patch4: groff-1.18-info.patch
Patch5: groff-1.18.1.4-nohtml.patch
Patch6: groff-1.18-pfbtops_cpp.patch
Patch7: groff-1.18-gzip.patch
Patch9: groff-1.18.1-fixminus.patch
Patch11: groff-1.18.1-8bit.patch
Patch12: groff-1.18.1-korean.patch
Patch13: groff-1.18.1-gzext.patch
Patch14: groff-xlibs.patch
Patch15: groff-1.18.1-fix15.patch
Patch16: groff-1.18.1-devutf8.patch
#Patch17: groff-1.18.1.3-revision.patch
Patch18: groff-1.18.1.1-do_char.patch
#Patch19: groff-1.18.1.1-grn.patch
#Patch20: groff-1.18.1.1-tempfile.patch
#Patch21: groff-1.18.1.1-gcc41.patch
Patch22: groff-1.18.1.1-bigendian.patch
Patch23: groff-1.18.1.1-spacefix.patch
Patch24: groff-1.18.1.4-sectmp.patch
Patch25: groff-1.18.1.4-gcc43.patch
Patch50: groff-1.18.1.4-texinfo51.patch

URL: http://groff.ffii.org/
Requires: mktemp
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: groff-tools
Provides: nroff-i18n
BuildRequires: netpbm-progs zlib-devel texinfo
BuildRequires: byacc

%description
Groff is a document formatting system. Groff takes standard text and
formatting commands as input and produces formatted output. The
created documents can be shown on a display or printed on a printer.
Groff's formatting commands allow you to specify font type and size,
bold type, italic type, the number and size of columns on a page, and
more.

Groff can also be used to format man pages. If you are going to use
groff with the X Window System, you will also need to install the
groff-gxditview package.

%package perl
Summary: Parts of the groff formatting system that require Perl
Group: Applications/Publishing

%description perl
The groff-perl package contains the parts of the groff text processor
package that require Perl. These include the afmtodit font processor
for creating PostScript font files, the grog utility that can be used
to automatically determine groff command-line options, and the
troff-to-ps print filter.

%if %{with_x}
%package gxditview
Summary: An X previewer for groff text processor output
Group: Applications/Publishing
BuildRequires: imake xorg-x11-proto-devel libX11-devel libXaw-devel
BuildRequires: libXt-devel libXpm-devel libXext-devel

%description gxditview
Gxditview displays the groff text processor's output on an X Window
System display.
%endif

%prep
%setup -q -a 4
%patch1 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch9 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1 -b .gzext
#%patch14 -p1
%patch15 -p1 -b .fix9
%patch16 -p1 -b .devutf8
#%patch17 -p1 -b .revision
%patch18 -p1 -b .do_char
#%patch19 -p1 -b .grn
#%patch20 -p1 -b .tempfile
#%patch21 -p1 -b .gcc41
#%patch22 -p1 -b .bigendian
%patch23 -p1 -b .spacefix
%patch24 -p1 -b .sectmp
%patch25 -p1 -b .gcc43~
%patch50 -p1 -b .texinfo51

for i in contrib/mm/{groff_mm,groff_mmse,mmroff}.man \
		src/devices/grolbp/grolbp.man; do
	iconv -f iso-8859-1 -t utf-8 < "$i" > "${i}_"
	mv "${i}_" "$i"
done

%build
#PATH=$PATH:%{_prefix}/X11R6/bin
#autoconf
%configure --enable-multibyte
make
(cd doc && makeinfo groff.texinfo)
%if %{with_x}
cd src/xditview
xmkmf && make
%endif

%install
rm -rf ${RPM_BUILD_ROOT}
#PATH=$PATH:%{_prefix}/X11R6/bin
mkdir -p ${RPM_BUILD_ROOT}%{_prefix} ${RPM_BUILD_ROOT}%{_infodir}
%makeinstall manroot=${RPM_BUILD_ROOT}/%{_mandir}
#install -m 644 doc/groff.info* ${RPM_BUILD_ROOT}/%{_infodir}
%if %{with_x}
cd src/xditview
%makeinstall DESTDIR=$RPM_BUILD_ROOT
cd ../..
%endif
#mv $RPM_BUILD_ROOT%{_prefix}/man $RPM_BUILD_ROOT%{_prefix}/share
ln -s s.tmac	${RPM_BUILD_ROOT}%{_datadir}/groff/%version/tmac/gs.tmac
ln -s mse.tmac	${RPM_BUILD_ROOT}%{_datadir}/groff/%version/tmac/gmse.tmac
ln -s m.tmac	${RPM_BUILD_ROOT}%{_datadir}/groff/%version/tmac/gm.tmac
ln -s troff	${RPM_BUILD_ROOT}%{_bindir}/gtroff
ln -s tbl	${RPM_BUILD_ROOT}%{_bindir}/gtbl
ln -s pic	${RPM_BUILD_ROOT}%{_bindir}/gpic
ln -s eqn	${RPM_BUILD_ROOT}%{_bindir}/geqn
ln -s neqn	${RPM_BUILD_ROOT}%{_bindir}/gneqn
ln -s refer	${RPM_BUILD_ROOT}%{_bindir}/grefer
ln -s lookbib	${RPM_BUILD_ROOT}%{_bindir}/glookbib
ln -s indxbib	${RPM_BUILD_ROOT}%{_bindir}/gindxbib
ln -s soelim	${RPM_BUILD_ROOT}%{_bindir}/gsoelim
ln -s soelim    ${RPM_BUILD_ROOT}%{_bindir}/zsoelim
ln -s nroff	${RPM_BUILD_ROOT}%{_bindir}/gnroff

ln -s eqn.1	${RPM_BUILD_ROOT}%{_mandir}/man1/geqn.1
ln -s indxbib.1	${RPM_BUILD_ROOT}%{_mandir}/man1/gindxbib.1
ln -s lookbib.1	${RPM_BUILD_ROOT}%{_mandir}/man1/glookbib.1
ln -s nroff.1 	${RPM_BUILD_ROOT}%{_mandir}/man1/gnroff.1
ln -s pic.1	${RPM_BUILD_ROOT}%{_mandir}/man1/gpic.1
ln -s refer.1	${RPM_BUILD_ROOT}%{_mandir}/man1/grefer.1
ln -s soelim.1	${RPM_BUILD_ROOT}%{_mandir}/man1/gsoelim.1
ln -s soelim.1	${RPM_BUILD_ROOT}%{_mandir}/man1/zsoelim.1
ln -s tbl.1	${RPM_BUILD_ROOT}%{_mandir}/man1/gtbl.1
ln -s troff.1 	${RPM_BUILD_ROOT}%{_mandir}/man1/gtroff.1

ln -s devnippon ${RPM_BUILD_ROOT}%{_datadir}/groff/%{version}/font/devkorean

cat debian/mandoc.local >> ${RPM_BUILD_ROOT}%{_datadir}/groff/site-tmac/mdoc.local
cat debian/mandoc.local >> ${RPM_BUILD_ROOT}%{_datadir}/groff/site-tmac/man.local

find ${RPM_BUILD_ROOT}%{_bindir} ${RPM_BUILD_ROOT}%{_mandir} -type f -o -type l | \
	grep -v afmtodit | grep -v grog | grep -v mdoc.samples |\
	grep -v mmroff |\
	grep -v gxditview |\
	sed "s|${RPM_BUILD_ROOT}||g" | sed "s|\.[0-9]|\.*|g" > groff-files

install -m 644 %SOURCE6 $RPM_BUILD_ROOT%{_datadir}/groff/%version/tmac/hyphen.cs

install -m 755 %SOURCE7 $RPM_BUILD_ROOT%{_bindir}/nroff

ln -sf doc.tmac $RPM_BUILD_ROOT%{_datadir}/groff/%version/tmac/docj.tmac
# installed, but not packaged in rpm
rm -fr $RPM_BUILD_ROOT%{_datadir}/doc/groff $RPM_BUILD_ROOT%{_infodir}/dir $RPM_BUILD_ROOT/usr/lib/X11/app-defaults

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
/sbin/install-info %{_infodir}/groff %{_infodir}/dir;
exit 0

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/groff %{_infodir}/dir
fi
exit 0

%files -f groff-files
%defattr(-,root,root)
%doc	BUG-REPORT NEWS PROBLEMS README TODO VERSION
%doc	doc/meintro.me doc/meref.me doc/pic.ms
%{_datadir}/groff
%{_infodir}/groff*
%{_libdir}/groff

%files perl
%defattr(-,root,root)
%{_bindir}/grog
%{_bindir}/mmroff
%{_bindir}/afmtodit
%{_mandir}/man1/afmtodit.*
%{_mandir}/man1/grog.*
%{_mandir}/man1/mmroff*

%if %{with_x}
%files gxditview
%defattr(-,root,root)
%{_bindir}/gxditview
%{_datadir}/X11/app-defaults/GXditview
%endif

%changelog
* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.18.1.4-17m)
- fix makeinfo problem

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.18.1.4-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.18.1.4-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.18.1.4-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.18.1.4-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.18.1.4-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.18.1.4-11m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.18.1.4-10m)
- add epoch to %%changelog

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.18.1.4-9m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.18.1.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Apr  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.18.1.4-7m)
- do not specify man file compression format (2nd)

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.18.1.4-6m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.18.1.4-5m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.18.1.4-4m)
- update Patch5,7 for fuzz=0
- change URL
- License: GPLv2+ and GFDL

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.18.1.4-3m)
- rebuild against gcc43

* Thu Nov 29 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.18.1.4-2m)
- added a patch for gcc-4.3

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1:1.18.1.4-1m)
- rewind 1.18.1.4

* Tue Mar 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.1-7m)
- always add -ffriend-injection

* Tue Jul  4 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.19.1-6m)
- [SECURITY] CVE-2004-0969
- add Patch2: groff-1.19.1-CVE-2004-0969.patch

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.19.1-5m)
- delete symlink to /etc/X11 (old behavior)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.19.1-4m)
- revise for xorg-7.0
- change install dir

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.1-3m)
- enable gcc-4.1. 
-- export CXXFLAGS="%{optflags} -ffriend-injection"

* Wed Feb 23 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.19.1-2m)
- repair euc-jp.tmac

* Wed Feb 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.19.1-1m)
- update to 1.19.1.

* Tue Nov 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.19-6m)
- %%global _numjobs 1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.19-6m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.19-5m)
- edit spec to include GXditview

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.19-4m)
- build against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.19-3m)
- remove Epoch from BuildPrereq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.19-2m)
- revised spec for enabling rpm 4.2.

* Sun Feb 22 2004 Kenta MURATA <muraken2@nifty.com>
- (1.19-1m)
- version 1.19.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.18-4m)
- rebuild against perl-5.8.2

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.18-3m)
- change URL

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.18-2m)
- rebuild against perl-5.8.0

* Sat Oct 12 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.18-1m)
- rewound to 1.18
- sync with redhat-8.0(groff-1.18-6)

* Wed Oct 09 2002 Kenta MURATA <muraken2@nifty.com>
- (1.18.1-1m)
- version up.

* Fri May 31 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.17.2-4k)
- Added macro which enables building this package without XFree86.

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (1.17.2-2k)
- version up.
- for printconf.

* Wed Aug 15 2001 Mike A. Harris <mharris@redhat.com> 1.17.2-3
- Added symlink from soelim to zsoelim, fixing bug (#51037)

* Tue Aug 14 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- fixes security bug #50494

* Sun Aug 12 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.17.2
- strerror patch is not needed anymore
- apply newest debian patch

* Fri Apr 27 2001 Bill Nottingham <notting@redhat.com>
- rebuild for C++ exception handling on ia64

* Tue Apr 03 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- do not change groff to use /etc/papersize. Deleted the changes
  in the debian patch.

* Fri Mar 30 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add hyphen.cs - file generated as described in Czech how-to, 6.7

* Wed Mar 28 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- updated to newest debian patch to get nippon/ascii8 support
  better working

* Fri Feb  9 2001 Crutcher Dunnavant <crutcher@redhat.com>
- switch to printconf filtration rules

* Tue Jan 09 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- change app-defaults to /usr/X11R6/lib/X11/app-defaults/
  and do not mark it as config file

* Thu Dec 14 2000 Yukihiro Nakai <ynakai@redhat.com>
- Add Japanese patch from RHL7J

* Fri Aug  4 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to bug-fix release 1.16.1

* Fri Jul 28 2000 Tim Waugh <twaugh@redhat.com>
- Install troff-to-ps.fpi in /usr/lib/rhs-printfilters (#13634).

* Wed Jul 19 2000 Jeff Johnson <jbj@redhat.com>
- rebuild with gcc-2.96-41.

* Mon Jul 17 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to fix miscompilation manifesting in alpha build of tcltk.

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul  4 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Fri Jun  9 2000 Bill Nottingham <notting@redhat.com>
- move mmroff to -perl

* Wed Jun  7 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix build
- FHS
- 1.16

* Sun May 14 2000 Jeff Johnson <jbj@redhat.com>
- install tmac.mse (FWIW tmac.se looks broken) to fix dangling symlink (#10757).
- add README.A4, how to set up for A4 paper (#8276).
- add other documents to package.

* Thu Mar  2 2000 Jeff Johnson <jbj@redhat.com>
- permit sourcing on regular files within cwd tree (unless -U specified).

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- fix incorrectly installed tmac.m file (#8362).

* Mon Feb  7 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- check if build system is sane again

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- fix description and summary
- man pages are compressed. This is ugly.

* Mon Jan 31 2000 Bill Nottingham <notting@redhat.com>
- put the binaries actually in the package *oops*

* Fri Jan 28 2000 Bill Nottingham <notting@redhat.com>
- split perl components into separate subpackage

* Wed Dec 29 1999 Bill Nottingham <notting@redhat.com>
- update to 1.15

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 9)

* Tue Feb 16 1999 Cristian Gafton <gafton@redhat.com>
- glibc 2.1 patch for xditview (#992)

* Thu Oct 22 1998 Bill Nottingham <notting@redhat.com>
- build for Raw Hide

* Thu Sep 10 1998 Cristian Gafton <gafton@redhat.com>
- fix makefiles to work with bash2

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- use g++ for C++ code

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- manhattan and buildroot

* Mon Nov  3 1997 Michael Fulbright <msf@redhat.com>
- made xdefaults file a config file

* Thu Oct 23 1997 Erik Troan <ewt@redhat.com>
- split perl components into separate subpackage

* Tue Oct 21 1997 Michael Fulbright <msf@redhat.com>
- updated to 1.11a
- added safe troff-to-ps.fpi

* Tue Oct 14 1997 Michael Fulbright <msf@redhat.com>
- removed troff-to-ps.fpi for security reasons.

* Fri Jun 13 1997 Erik Troan <ewt@redhat.com>
- built against glibc

