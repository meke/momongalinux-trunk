%global momorel 7

Summary: Dutch dictionaries for Aspell
Name: aspell-nl
# Have to bump this to make it newer than the old, bad version.
Version: 0.1e
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
URL: http://packages.debian.org/unstable/text/%{name}
Source0: http://ftp.debian.org/debian/pool/main/d/dutch/dutch_%{version}.orig.tar.gz
Patch0: dutch-debian.patch
Patch1: aspell-nl-0.1e-dictdir.patch
Patch2: dutch-0.1e-nl.patch
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil} 

%description
Provides the word list/dictionaries for the following: Dutch

%prep
%setup -q -n dutch-%{version}
%patch0 -p1
%patch1 -p1 -b .dd
%patch2 -p1 -b .nl
cp ./aspell/dutch.dat ./aspell/nl_affix.dat

%build
./configure prefix=/usr 
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT libdir=%{_libdir}
rm $RPM_BUILD_ROOT/usr/share/ispell/dutch.aff
rm $RPM_BUILD_ROOT/usr/dict/dutch
rm $RPM_BUILD_ROOT/usr/info/*
cp $RPM_BUILD_ROOT%{_libdir}/aspell-0.60/nl.dat $RPM_BUILD_ROOT%{_libdir}/aspell-0.60/nl_affix.dat

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog
%{_libdir}/aspell-0.60/*

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1e-7m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1e-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1e-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1e-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1e-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1e-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1e-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 51:0.1e-6
- Autorebuild for GCC 4.3

* Thu Jun 14 2007 Ivana Varekova <varekova@redhat.com> - 51:0.1e-5
- Resolves: #244159
  add nl.rws

* Fri Mar 30 2007 Ivana Varekova <varekova@redhat.com> - 51:0.1e-4
- add nl_affix.dat

* Fri Mar 30 2007 Ivana Varekova <varekova@redhat.com> - 51:0.1e-3
- use configure script to create Makefile
- update default buildroot
- some minor spec changes

* Tue Jan 30 2007 Ivana Varekova <varekova@redhat.com> - 51:0.1e-2
- remove debuginfo 

* Mon Jul 31 2006 Tom "spot" Callaway <tcallawa@redhat.com> - 51:0.1e-1
- move to GPL dictionary

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-8.2.1
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-8.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-8.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 16 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-8
- add nederlands alias (bug 175863)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 19 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-7
- build with aspell-0.60.3

* Mon Apr 11 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-6
- rebuilt

* Tue Sep 28 2004 Adrian Havill <havill@redhat.com> 50:0.50-5
- rebuilt, remove debuginfo

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun 23 2003 Adrian Havill <havill@redhat.com> 0.50-3
- first build for new aspell (0.50)
