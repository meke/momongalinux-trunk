%global momorel 1

Summary: X.Org X11 libXfont runtime library
Name: libXfont
Version: 1.4.8
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: libXfont-1.1.0-cidfonts.diff
BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
BuildRequires: xorg-x11-proto-devel >= 7.2
BuildRequires: xorg-x11-xtrans-devel
BuildRequires: libfontenc-devel
BuildRequires: freetype-devel

%description
X.Org X11 libXfont runtime library

%package devel
Summary: X.Org X11 libXfont development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: xorg-x11-proto-devel
Requires: libfontenc-devel
Requires: freetype-devel

%description devel
X.Org X11 libXfont development package

%prep
%setup -q
# %patch0 -p1
# Disable static library creation by default.
%define with_static 0

%build
%configure \
%if ! %{with_static}
	--disable-static
%endif
%make 

%install

rm -rf --preserve-root %{buildroot}
%makeinstall

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
# FIXME:  Missing README/INSTALL %doc AUTHORS COPYING README INSTALL ChangeLog
%doc AUTHORS COPYING ChangeLog
%{_libdir}/libXfont.so.1
%{_libdir}/libXfont.so.1.4.1

%files devel
%defattr(-,root,root,-)
%{_includedir}/X11/fonts/bdfint.h
%{_includedir}/X11/fonts/bitmap.h
%{_includedir}/X11/fonts/bufio.h
%{_includedir}/X11/fonts/fntfil.h
%{_includedir}/X11/fonts/fntfilio.h
%{_includedir}/X11/fonts/fntfilst.h
%{_includedir}/X11/fonts/fontconf.h
%{_includedir}/X11/fonts/fontencc.h
%{_includedir}/X11/fonts/fontmisc.h
%{_includedir}/X11/fonts/fontshow.h
%{_includedir}/X11/fonts/fontutil.h
%{_includedir}/X11/fonts/fontxlfd.h
%{_includedir}/X11/fonts/pcf.h
%{_includedir}/X11/fonts/ft.h
%{_includedir}/X11/fonts/ftfuncs.h
%if %{with_static}
%{_libdir}/libXfont.a
%endif
%{_libdir}/libXfont.so
%{_libdir}/pkgconfig/xfont.pc

%changelog
* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.5-1m)
- update 1.4.5

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.4-1m)
- update 1.4.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-1m)
- update 1.4.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- update 1.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1

* Sat Feb 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-3m)
- rebuild against rpm-4.6

* Thu Oct  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3-2m)
- add Requires: libfontenc-devel freetype-devel

* Thu Jul  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3-1m)
- update 1.3.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-1m)
- update 1.3.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- %%NoSource -> NoSource

* Wed Sep 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-1m)
- update 1.3.1

* Wed Jul  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Sat May  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.8-1m)
- update 1.2.8

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7-2m)
- BuildRequires: freetype2-devel -> freetype-devel

* Wed Jan 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-1m)
- update 1.2.7

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-1m)
- update 1.2.5

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-1m)
- update 1.2.3

* Tue Sep 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-3m)
- security fix
- CVE-ID 2006-3739, 2006-3740, bugzilla #8000, #8001.

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete duplicate dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 23 2006 Mike A. Harris <mharris@redhat.com> 1.0.0-2
- Bumped and rebuilt

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libXfont to version 1.0.0 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated libXfont to version 0.99.3 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.
- Removed libXfont-0.99.2-fontdir-attrib-fix-bug-172997.patch, which is now
  integrated upstream.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 14 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Added libXfont-0.99.2-fontdir-attrib-fix-bug-172997.patch to remove
  conditionalization of FONTDIRATTRIB from sources instead of tweaking
  CFLAGS for bug (#172997, fdo#5047).

* Mon Nov 14 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Added "-DFONTDIRATTRIB" to CFLAGS, to work around bug (#172997, fdo#5047)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated libXfont to version 0.99.2 from X11R7 RC2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated libXfont to version 0.99.1 from X11R7 RC1
- Remove libfontcache* from file manifests, as it is static linked into Xfont

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-5
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro
- Fix BuildRequires to use new libX* style package names

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-4
- Added xorg-x11-proto-devel dependency to 'devel' subpackage, because the
  libXfont headers use some of the protocol headers, but the autotooling of
  libXfont doesn't autodetect this yet.  Discovered when bdftopcf failed to
  compile while trying to package xorg-x11-font-utils, even though libXfont
  headers were installed.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Changed all virtual BuildRequires to the "xorg-x11-" prefixed non-virtual
  package names, as we want xorg-x11 libs to explicitly build against
  X.Org supplied libs, rather than "any implementation", which is what the
  virtual provides is intended for.
- Added freetype-devel build dependency

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
