%global momorel 14

Name: docbook-utils
Version: 0.6.14
Release: %{momorel}m%{?dist}
Group: Applications/Text

Summary: Shell scripts for managing DocBook documents.
URL: ftp://sources.redhat.com/pub/docbook-tools/new-trials/

License: GPL

Requires: docbook-style-dsssl >= 1.72
Requires: docbook-dtds
Requires: perl-SGMLSpm >= 1.03ii
Requires: which
Requires: elinks
# %{_bindir}/jw requires jade
Requires: openjade

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl-SGMLSpm, openjade, docbook-style-dsssl

BuildArch: noarch
Source0: ftp://sources.redhat.com/pub/docbook-tols/new-trials/SOURCES/%{name}-%{version}.tar.gz
Source1: db2html
Source2: gdp-both.dsl

Obsoletes: stylesheets
Provides: stylesheets

Patch0: docbook-utils-spaces.patch
Patch1: docbook-utils-2ndspaces.patch
Patch2: docbook-utils-w3mtxtconvert.patch
Patch3: docbook-utils-grepnocolors.patch
Patch4: docbook-utils-newgrep.patch
%Description
This package contains scripts are for easy conversion from DocBook
files to other formats (for example, HTML, RTF, and PostScript), and
for comparing SGML files.

%package pdf
Requires: docbook-utils = %{version}
Requires: texlive-pdftex
Requires: texlive-dvipsk
Requires: texlive-texmf-jadetex
Requires: texlive-texmf-pdftex
Requires: texlive-texmf-oberdiek
Requires: texlive-texmf-fonts-adobe
Requires: texlive-texmf-fonts-urw
Requires: texlive-texmf-latex-psnfss
Requires: texlive-texmf-latex-hyperref
Group: Applications/Text
Obsoletes: stylesheets-db2pdf
Provides: stylesheets-db2pdf
Summary: A script for converting DocBook documents to PDF format.
URL: ftp://sources.redhat.com/pub/docbook-tools/new-trials/

%description pdf
This package contains a script for converting DocBook documents to
PDF format.

%prep
%setup -q
%patch0 -p1 -b .spaces
%patch1 -p1 -b .2ndspaces
%patch2 -p1 -b .w3mtxtconvert
%patch3 -p1 -b .grepnocolors
%patch4 -p1 -b .newgrep

%build
./configure --prefix=%{_prefix} --mandir=%{_mandir} --libdir=%{_libdir}
%make

%install
export DESTDIR=$RPM_BUILD_ROOT
rm -rf $DESTDIR
make install prefix=%{_prefix} mandir=%{_mandir} docdir=/tmp
for util in dvi html pdf ps rtf
do
	ln -s docbook2$util $RPM_BUILD_ROOT%{_bindir}/db2$util
	ln -s jw.1.gz $RPM_BUILD_ROOT/%{_mandir}/man1/db2$util.1
done
# db2html is not just a symlink, as it has to create the output directory
rm -f $RPM_BUILD_ROOT%{_bindir}/db2html
install -c -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/db2html
install -c -m 644 %{SOURCE2} $RPM_BUILD_ROOT/usr/share/sgml/docbook/utils-%{version}/docbook-utils.dsl

rm -rf $RPM_BUILD_ROOT/tmp

%Clean
DESTDIR=$RPM_BUILD_ROOT
rm -rf $DESTDIR


%Files
%defattr (-,root,root)
%doc README COPYING TODO
%{_bindir}/jw
%{_bindir}/docbook2html
%{_bindir}/docbook2man
%{_bindir}/docbook2rtf
%{_bindir}/docbook2tex
%{_bindir}/docbook2texi
%{_bindir}/docbook2txt
%{_bindir}/db2html
%{_bindir}/db2rtf
%{_bindir}/sgmldiff
/usr/share/sgml/docbook/utils-%{version}
%{_mandir}/*/db2dvi.*
%{_mandir}/*/db2html.*
%{_mandir}/*/db2ps.*
%{_mandir}/*/db2rtf.*
%{_mandir}/*/docbook2html.*
%{_mandir}/*/docbook2rtf.*
%{_mandir}/*/docbook2man.*
%{_mandir}/*/docbook2tex.*
%{_mandir}/*/docbook2texi.*
%{_mandir}/*/jw.*
%{_mandir}/*/sgmldiff.*
%{_mandir}/*/*-spec.*

%files pdf
%defattr (-,root,root)
%{_bindir}/docbook2pdf
%{_bindir}/docbook2dvi
%{_bindir}/docbook2ps
%{_bindir}/db2dvi
%{_bindir}/db2pdf
%{_bindir}/db2ps
%{_mandir}/*/db2pdf.*
%{_mandir}/*/docbook2pdf.*
%{_mandir}/*/docbook2dvi.*
%{_mandir}/*/docbook2ps.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-13m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.14-12m)
- import patch4 from Fedora devel

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-11m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.6.14-10m)
- Change Requires: for texlive

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-8m)
- import bug fixes from fedora's docbook-utils-0_6_14-15_fc11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-7m)
- rebuild against rpm-4.6

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-6m)
- import Fedora patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-6m)
- rebuild against gcc43

* Mon Feb 18 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-5m)
- add Requires: openjade

* Mon May 21 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.14-4m)
- sync Fedora

* Thu Apr 26 2007 Yohsuke Ooi <meke@momonga-linuc.org>
- (0.6.14-3m)
- BuildReq openjade

* Mon Jul 31 2006 Nishio Futoshi <futoshi@momonga-linuc.org>
- (0.6.14-2m)
- duplicate make (Is it OK?)

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6.14-1m)
- Sync with FC3.
  * Thu Aug 19 2004 Tim Waugh <twaugh@redhat.com> 0.6.14-4
  - Apply CVS patch to protect spaces in jw (bug #130329).

* Wed Apr 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.13-7m)
- Sync with FC1.
  * Thu May 22 2003 Tim Waugh <twaugh@redhat.com> 0.6.13-4
  - Require elinks (bug #91472).
- use w3m instead of elinks. No more text browser.
  * Fri Feb 14 2003 Elliot Lee <sopwith@redhat.com> 0.6.12-6
  - tetex-dvips requirement should go on main package (not just pdf 
    subpackage) because docbook2ps requires dvips too.

* Wed Mar 17 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.9-13m)
- revised spec for enabling rpm 4.2.

* Wed May 15 2002 Toru Hoshina <t@kondara.org>
- (0.6.9-12k)
- rebuild for release adjustment.
- docbook-utils still needed for kdelibs, kdelibs2 and gnome-user-docs...

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.6.9-8k)
- http://rhn.redhat.com/errata/RHSA-2002-062.html

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (0.6.9-10k)
- remove kondara.dsl (OBSOLETE)

* Tue Oct 23 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.6.9-6k)
- add onehtml style in kondara.dsl

* Tue Oct 23 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.6.9-4k)
- add kondara.dsl

* Mon Aug 13 2001 Tim Waugh <twaugh@redhat.com> 0.6.9-2
- Larger bottom margin in gdp-both.dsl to fix RTF output (bug #49677).

* Tue Jul  3 2001 Tim Waugh <twaugh@redhat.com> 0.6.9-1
- 0.6.9.
- With --nochunks, send output to a file instead of stdout (bug #46913).

* Mon Jun  4 2001 Tim Waugh <twaugh@redhat.com> 0.6.8-2
- Make sure COPYING isn't installed as a symlink.

* Mon May 21 2001 Tim Waugh <twaugh@redhat.com> 0.6.8-1
- 0.6.8.

* Mon May 21 2001 Tim Waugh <twaugh@redhat.com> 0.6-14
- db2html: copy admon graphics to output directory (bug #40143).
- Require docbook-style-dsssl 1.64-2 for symbolic link used by db2html.
- db2html: handle arguments with spaces better.

* Sat Mar 24 2001 Tim Waugh <twaugh@redhat.com> 0.6-13
- Fix man pages (bug #32820).

* Mon Mar 12 2001 Tim Waugh <twaugh@redhat.com>
- Fix argument parsing in docbook2xxx (bug #31518).
- Fix argument passing in db2html (bug #31520).
- Fix pdf generation (bug #31524).

* Fri Feb 23 2001 Tim Waugh <twaugh@redhat.com>
- Allow the use of custom backends and frontends (bug #29067).

* Fri Feb 16 2001 Tim Waugh <twaugh@redhat.com>
- Use gdp-both.dsl as the default stylesheet.

* Mon Feb 12 2001 Tim Waugh <twaugh@redhat.com>
- REALLY only create output directory for db2html (duh).
- Handle filenames with dots in properly.

* Sun Feb 11 2001 Tim Waugh <twaugh@redhat.com>
- Only create output directory for db2html (bug #27092). (docbook2html
  does not create an output directory in the upstream version, but
  the compatibility script has been made to do so.)

* Mon Jan 22 2001 Tim Waugh <twaugh@redhat.com>
- Move the jadetex requirement to the -pdf subpackage.

* Tue Jan 16 2001 Tim Waugh <twaugh@redhat.com>
- Put output files in new directory instead of current directory.

* Mon Jan 15 2001 Tim Waugh <twaugh@redhat.com>
- Don't play so many macro games.
- Be sure to own utils directory.

* Fri Jan 12 2001 Tim Waugh <twaugh@redhat.com>
- Split off docbook2pdf into subpackage for dependency reasons.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Change group.
- Use %%{_mandir} and %%{_prefix}.
- db2* symlinks.
- Obsolete stylesheets (and -db2pdf).
- Change Copyright: to License:.
- Remove Packager: line.
- Reword description.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Based on Eric Bischoff's new-trials packages.
