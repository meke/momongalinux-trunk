%global momorel 1

Summary: Simultaneous connection limiting module for Apache
Name: mod_limitipconn
Version: 0.24
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
License: "ASL 2.0"
URL: http://dominia.org/djao/limitipconn2.html
Source0: http://dominia.org/djao/limit/mod_limitipconn-%{version}.tar.bz2
NoSource: 0
Source1: mod_limitipconn.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: httpd >= 2.4.3
Requires: httpd-mmn = %([ -a %{_includedir}/httpd/.mmn ] && cat %{_includedir}/httpd/.mmn || echo missing)
BuildRequires: httpd-devel >= 2.4.3

%description
Apache module which allows web server administrators to limit the number of
simultaneous downloads permitted from a single IP address. 

%prep
%setup -q

%build
%{_httpd_apxs} -Wc,"%{optflags}" -c mod_limitipconn.c

%install
rm -rf %{buildroot}
install -D -p -m 0755 .libs/mod_limitipconn.so \
    %{buildroot}%{_libdir}/httpd/modules/mod_limitipconn.so
install -D -p -m 0644 %{SOURCE1} \
    %{buildroot}%{_sysconfdir}/httpd/conf.d/limitipconn.conf.dist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README INSTALL LICENSE
%config %{_sysconfdir}/httpd/conf.d/limitipconn.conf.dist
%{_libdir}/httpd/modules/mod_limitipconn.so

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24
- rebuild against httpd-2.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-1m)
- update to 0.23 based on Fedora 11 (0.23-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22-5m)
- rebuild against gcc43

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.22-4m)
- large file support. rebuild against httpd 2.0.53.

* Thu Aug 26 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.22-3m)
- rebuild against gcc-c++-3.4.1 (add BuildPrereq: gcc-c++, libtool)

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.22-2m)
- stop service

* Thu Jul 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.22-1m)
- initial import to momonga
