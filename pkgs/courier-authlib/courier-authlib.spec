%global momorel 6

Summary: Courier authentication library
Name: courier-authlib
Version: 0.63.0
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: System Environment/Daemons
URL: http://www.courier-mta.org/
Source0: http://dl.sourceforge.net/sourceforge/courier/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: courier-authlib.sysvinit.patch
Patch1: courier-authlib-0.62.4-libdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: zlib-devel
BuildRequires: postgresql-devel >= 8.2.3
BuildRequires: gdbm-devel
BuildRequires: gamin-devel
BuildRequires: expect
BuildRequires: gcc-c++
BuildRequires: openssl-devel >= 1.0.0
Requires(post): chkconfig
Requires(preun): chkconfig

%description
The Courier authentication library provides authentication services for
other Courier applications.

%package devel
Summary: Development libraries for the Courier authentication library
Group: Development/Libraries
Requires: courier-authlib = 0:%{version}-%{release}

%description devel
This package contains the development libraries and files needed to compile
Courier packages that use this authentication library.  Install this
package in order to build the rest of the Courier packages.  After they are
built and installed this package can be removed.  Files in this package
are not needed at runtime.

%package userdb
Summary: userdb support for the Courier authentication library
Group: System Environment/Daemons
Requires: courier-authlib = 0:%{version}-%{release}

%description userdb
This package installs the userdb support for the Courier authentication
library.  Userdb is a simple way to manage virtual mail accounts using
a GDBM-based database file.

Install this package in order to be able to authenticate with userdb.
%package ldap
Summary: LDAP support for the Courier authentication library
Group: System Environment/Daemons
Requires: courier-authlib = 0:%{version}-%{release}

%description ldap
This package installs LDAP support for the Courier authentication library.
Install this package in order to be able to authenticate using LDAP.

%package mysql
Summary: MySQL support for the Courier authentication library
Group: System Environment/Daemons
Requires: courier-authlib = 0:%{version}-%{release}

%description mysql
This package installs MySQL support for the Courier authentication library.
Install this package in order to be able to authenticate using MySQL.

%package pgsql
Summary: MySQL support for the Courier authentication library
Group: System Environment/Daemons
Requires: courier-authlib = 0:%{version}-%{release}

%description pgsql
This package installs PostgreSQL support for the Courier authentication
library.
Install this package in order to be able to authenticate using PostgreSQL.

%package pipe
Summary: External authentication module that communicates via pipes
Group: System Environment/Daemons
Requires: courier-authlib = 0:%{version}-%{release}

%description pipe
This package installs the authpipe module, which is a generic plugin
that enables authentication requests to be serviced by an external
program, then communicates through messages on stdin and stdout.

%prep
%setup -q
%patch0 -p0 -b .sysvinit
%patch1 -p1 -b .libdir

%build
%configure --with-redhat --without-authvchkpw

%{__make} %{_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
MAKEFLAGS= %{__make} -j 1 install DESTDIR=$RPM_BUILD_ROOT
%{__rm} -f $RPM_BUILD_ROOT%{_libdir}/*.a
%{__install} -m 555 sysconftool $RPM_BUILD_ROOT%{_libexecdir}/courier-authlib
%{__install} -m 555 authmigrate $RPM_BUILD_ROOT%{_libexecdir}/courier-authlib

./courierauthconfig --configfiles >configtmp
. ./configtmp

./authmksock $RPM_BUILD_ROOT%{_localstatedir}/spool/authdaemon/socket || exit 1
touch $RPM_BUILD_ROOT%{_localstatedir}/spool/authdaemon/pid.lock || exit 1
touch $RPM_BUILD_ROOT%{_localstatedir}/spool/authdaemon/pid || exit 1
%{__chmod} 777 $RPM_BUILD_ROOT%{_localstatedir}/spool/authdaemon/socket || exit 1

cat >configfiles.base <<EOF
%defattr(-,$mailuser,$mailgroup,-)
%{_sysconfdir}/authlib
%{_libexecdir}/courier-authlib
%dir %attr(750,$mailuser,$mailgroup) %{_localstatedir}/spool/authdaemon
EOF

echo "%defattr(-,$mailuser,$mailgroup,-)" >configfiles.mysql
echo "%defattr(-,$mailuser,$mailgroup,-)" >configfiles.ldap
echo "%defattr(-,$mailuser,$mailgroup,-)" >configfiles.pgsql
echo "%defattr(-,$mailuser,$mailgroup,-)" >configfiles.userdb
echo "%defattr(-,$mailuser,$mailgroup,-)" >configfiles.pipe
echo "%defattr(-,$mailuser,$mailgroup,-)" >configfiles.devel

for f in $RPM_BUILD_ROOT%{_sbindir}/*
do
	fn=`basename $f`
	case "$fn" in
	*userdb*)
		echo "%{_sbindir}/$fn" >>configfiles.userdb
		;;
	*)
		echo "%{_sbindir}/$fn" >>configfiles.base
		;;
	esac
done

find %{buildroot} -name "*.la" -delete

for f in $RPM_BUILD_ROOT%{_libdir}/*
do
	fn=`basename $f`
	case "$fn" in
	libauthpipe*)
		echo "%{_libdir}/$fn" >>configfiles.pipe
		;;
	libauthldap*)
		echo "%{_libdir}/$fn" >>configfiles.ldap
		;;
	libauthmysql*)
		echo "%{_libdir}/$fn" >>configfiles.mysql
		;;
	libauthpgsql*)
		echo "%{_libdir}/$fn" >>configfiles.pgsql
		;;
	libauthldap*)
		echo "%{_libdir}/$fn" >>configfiles.ldap
		;;
	libauthuserdb*)
		echo "%{_libdir}/$fn" >>configfiles.userdb
		;;
	*)
		echo "%{_libdir}/$fn" >>configfiles.base
		;;
	esac
done
%{__mkdir_p} $RPM_BUILD_ROOT%{_sysconfdir}/init.d
%{__install} -m 555 courier-authlib.sysvinit \
	 $RPM_BUILD_ROOT%{_sysconfdir}/init.d/courier-authlib

%post
%{_libexecdir}/courier-authlib/authmigrate >/dev/null
%{_libexecdir}/courier-authlib/sysconftool %{_sysconfdir}/authlib/*.dist >/dev/null

/sbin/chkconfig --del courier-authlib
/sbin/chkconfig --add courier-authlib

/sbin/ldconfig

%preun
if test -x %{_sbindir}/authdaemond
then
	%{_sbindir}/authdaemond >/dev/null 2>&1 || /bin/true
fi

if test "$1" = "0"
then
	/sbin/chkconfig --del courier-authlib
fi

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files -f configfiles.base
%defattr(-,root,root)
%doc README README*html README.authmysql.myownquery README.ldap
%doc NEWS COPYING* AUTHORS ChangeLog authldap.schema
/etc/init.d/*
%ghost %attr(600, root, root) %{_localstatedir}/spool/authdaemon/pid.lock
%ghost %attr(644, root, root) %{_localstatedir}/spool/authdaemon/pid
%ghost %attr(-, root, root) %{_localstatedir}/spool/authdaemon/socket
%{_mandir}/man1/*

%files -f configfiles.userdb userdb
%{_mandir}/man8/*userdb*

%files -f configfiles.devel devel
%defattr(-,root,root)
%{_bindir}/courierauthconfig
%{_includedir}/*
%{_mandir}/man3/*
%doc authlib.html auth_*.html

%files -f configfiles.ldap ldap
%defattr(-,root,root)
%doc authldap.schema

%files -f configfiles.mysql mysql

%files -f configfiles.pgsql pgsql

%files -f configfiles.pipe pipe

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.63.0-6m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63.0-5m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.63.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.63.0-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.63.0-2m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.63.0-1m)
- update to 0.63.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.4-1m)
- update to 0.62.4

* Sat Jul 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.2-5m)
- revise for bash-4.0

* Fri May 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.62.2-4m)
- define __libtoolize (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.2-3m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.2-2m)
- rebuild against mysql-5.1.32

* Thu Mar 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.2-1m)
- update to 0.62.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.1-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62.1-1m)
- [SECURITY] CVE-2008-2380 CVE-2008-2667
- update to 0.62.1
- License: GPLv3+

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60.2-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.60.2-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.60.2-1m)
- update 0.60.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.59.1-3m)
- %%NoSource -> NoSource

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.59.1-2m)
- rebuild against postgresql-8.2.3

* Thu Feb 15 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.59.1-1m)
- up to 0.59.1
- change library directory

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.58-3m)
- delete libtool library

* Sat May 20 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.58-2m)
- stop daemon

* Fri May 19 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.58-1m)
- import to Momonga

* Sun Oct  3 2004 Mr. Sam <sam@email-scan.com> - 0.50-1
- Initial build.

