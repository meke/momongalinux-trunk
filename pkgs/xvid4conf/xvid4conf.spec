%global momorel 7

Summary: xvid configuration panel for transcode
Name:    xvid4conf
Version: 1.12
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPL
URL:     http://zebra.fh-weingarten.de/~transcode/xvid4conf/
Source0: http://www.mirrorservice.org/sites/www.ibiblio.org/gentoo/distfiles/%{name}-%{version}.tar.gz
Requires: gtk2
BuildRequires: gtk2-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
xvid configuration panel for transcode.

%prep
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README INSTALL ChangeLog AUTHORS COPYING NEWS
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-2m)
- rebuild against gcc43

* Tue Apr 17 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.12-1m)
- import to Momonga for dvd::rip
