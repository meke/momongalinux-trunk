%global momorel 2
%define pkgname xfwp

Summary: X.Org X11 X firewall proxy
Name: xorg-x11-%{pkgname}
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/xfwp-1.0.2.tar.bz2 
NoSource: 0
Source1: %{xorgurl}/app/proxymngr-1.0.2.tar.bz2 
NoSource: 1
Source2: %{xorgurl}/app/xfindproxy-1.0.2.tar.bz2 
NoSource: 2
Patch01: proxymngr-1.0.1-lbxproxy-die-die-die.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# FIXME: The BuildRequires are all missing here and need to be figured out.
# That's low priority for now though, unless we encounter real build
# failures in beehive.

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros >= 1.0.0
BuildRequires: xorg-x11-lbxproxy

# FIXME: This seems odd to require lbxproxy to *build*
# proxymngr needs lbxproxy at build time 
#BuildRequires: lbxproxy
#lbxproxy command provides by xorg-x11-server-utils
BuildRequires: xorg-x11-server-utils
#BuildRequires: xorg-x11-lbxproxy-devel

# FIXME: check if still needed for X11R7
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

# NOTE: These utilities used to be in XFree86, xorg-x11
Obsoletes: XFree86
#Obsoletes: xorg-x11

%description
The  X firewall proxy (xfwp) is an application layer gateway proxy that
may be run on a network firewall host to forward X traffic  across  the
firewall.

%prep
%setup -q -c %{name}-%{version} -a1 -a2
#%%patch01 -p0 -b .lbx 

%build
# Build everything
{
   for pkg in * ; do
      pushd $pkg
	case $pkg in
 	proxymngr*)
 	aclocal ; libtoolize --force ; automake ; autoconf
 	;;
 	*)
 	;;
 	esac 
      %configure
      make
      popd
   done
}

%install
rm -rf $RPM_BUILD_ROOT

# Install everything
{
   for pkg in * ; do
      pushd $pkg
      %makeinstall
      popd
   done
}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/proxymngr
%{_bindir}/xfindproxy
%{_bindir}/xfwp
%dir %{_libdir}/X11/proxymngr
%{_libdir}/X11/proxymngr/pmconfig
%{_mandir}/man1/proxymngr.1*
%{_mandir}/man1/xfindproxy.1*
%{_mandir}/man1/xfwp.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-2m)
- rebuild for new GCC 4.6

* Sun Jan  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
- update to xfindproxy-1.0.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-10m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-9m)
- update proxymngr-1.0.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.1-6m)
- copy from fc devel
  Patch01: proxymngr-1.0.1-lbxproxy-die-die-die.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-3m)
- %%NoSource -> NoSource

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga
- add BuildRequires: xorg-x11-server-utils

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated all tarballs to version 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all tarballs to version 1.0.0 from X11R7 RC4.
- Changed manpage dir from man1x to man1 to match RC4 default.

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.1-3
- require newer filesystem package (#172610)

* Sun Nov 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Add "Obsoletes: XFree86, xorg-x11", as these utils used to be there in
  in monolithic X packaging.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Initial build of xfwp, proxymngr, xfindproxy from X11R7 RC2.
- Added "BuildRequires: lbxproxy" for proxymngr.
