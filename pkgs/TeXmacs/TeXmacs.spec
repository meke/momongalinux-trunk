%global momorel 2

Summary: A wysiwyg mathematical text editor
Name: TeXmacs
Version: 1.0.7.18
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://www.texmacs.org/
Source0: ftp://ftp.texmacs.org/pub/TeXmacs/targz/%{name}-%{version}-src.tar.gz
NoSource: 0
Patch1: TeXmacs-psfix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libXext-devel
BuildRequires: libX11-devel
BuildRequires: freetype-devel
BuildRequires: guile-devel >= 2.0.9
BuildRequires: desktop-file-utils
BuildRequires: qt-devel
#Requires: tetex 
Requires: texlive-pdftex
Requires: texlive-web2c
Requires: texlive-texmf-plain
Requires: texlive-texmf-latex
Requires: texlive-texmf-latex-elsevier
Requires: texlive-texmf-latex-amscls
Requires: texlive-texmf-latex-graphics
Requires: texlive-texmf-fonts-amsfonts
Requires: texlive-texmf-fonts-ecc
Requires: texlive-texmf-fonts-adobe
Requires: guile, freetype
Obsoletes: texmacs < %{version}-%{release}
Provides: texmacs = %{version}-%{release}

%description
GNU TeXmacs is a free scientific text editor, which was both inspired by 
TeX and GNU Emacs. The editor allows you to write structured documents via 
a wysiwyg (what-you-see-is-what-you-get) and user friendly interface. New 
styles may be created by the user. The program implements high-quality 
typesetting algorithms and TeX fonts, which help you to produce professionally 
looking documents.

The high typesetting quality still goes through for automatically generated 
formulas, which makes TeXmacs suitable as an interface for computer algebra 
systems. TeXmacs also supports the Guile/Scheme extension language, so that 
you may customize the interface and write your own extensions to the editor.

%package devel
Summary: Development files for TeXmacs
Requires: %{name} = %{version}-%{release}
Group: Development/Libraries

%description devel
Development files required to create TeXmacs plugins.

%prep

%setup -q -n %{name}-%{version}-src
%patch1 -p1
sed -i "s|LDPATH = \@CONFIG_BPATH\@|LDPATH =|" src/makefile.in
sed -i "s|5\.14\.\*|5.15.*|" plugins/maxima/bin/tm_maxima

%build
export QMAKE=%{_libdir}/qt4/bin/qmake-qt4
export MOC=%{_libdir}/qt4/bin/moc-qt4
export UIC=%{_libdir}/qt4/bin/uic-qt4
%configure --enable-optimize="$RPM_OPT_FLAGS -fpermissive"
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
export GUILE_DATA_PATH=`guile-config info pkgdatadir`
export GUILE_LOAD_PATH=`find $GUILE_DATA_PATH -type d | grep ice-9`

# icons

# menu
mkdir -p %{buildroot}%{_datadir}/pixmaps
cp -f %{buildroot}%{_datadir}/TeXmacs/misc/pixmaps/TeXmacs-gnu.xpm %{buildroot}%{_datadir}/pixmaps

mkdir -p %{buildroot}%{_datadir}/applications
cat << EOF > %{name}.desktop
[Desktop Entry]
Name=TeXmacs
GenericName=Scientific Editor
Comment=A WYSIWIG scientific text editor
Exec=/usr/bin/texmacs
Icon=TeXmacs-gnu.xpm
Terminal=false
Encoding=UTF-8
Type=Application
EOF
# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --remove-category Editor \
  --add-category Science \
  --add-category Math \
    ./%{name}.desktop

## remove unwanted files
rm -rf %{buildroot}%{_datadir}/mime
rm -f %{buildroot}%{_iconsdir}/gnome/icon-theme.cache

%clean
rm -rf %{buildroot}

%post
/usr/bin/update-mime-database /usr/share/mime > /dev/null 2>&1 || :
/usr/bin/update-desktop-database /usr/share/applications > /dev/null 2>&1 || :


%postun
/usr/bin/update-mime-database /usr/share/mime > /dev/null 2>&1 || :
/usr/bin/update-desktop-database /usr/share/applications > /dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%{_bindir}/fig2ps
%{_bindir}/texmacs
%{_libexecdir}/TeXmacs/bin/bbox_add.pl
%{_libexecdir}/TeXmacs/bin/maxima_detect
%{_libexecdir}/TeXmacs/bin/perl-tm_asy
%{_libexecdir}/TeXmacs/bin/r_install
%{_libexecdir}/TeXmacs/bin/realpath
%{_libexecdir}/TeXmacs/bin/realpath.py
%{_libexecdir}/TeXmacs/bin/texmacs.bin
%{_libexecdir}/TeXmacs/bin/tm_asy
%{_libexecdir}/TeXmacs/bin/tm_asy2
%{_libexecdir}/TeXmacs/bin/tm_axiom
%{_libexecdir}/TeXmacs/bin/tm_dratex
%{_libexecdir}/TeXmacs/bin/tm_eukleides
%{_libexecdir}/TeXmacs/bin/tm_eukleides.old
%{_libexecdir}/TeXmacs/bin/tm_feynmf
%{_libexecdir}/TeXmacs/bin/tm_gnuplot
%{_libexecdir}/TeXmacs/bin/tm_graphviz
%{_libexecdir}/TeXmacs/bin/tm_gs
%{_libexecdir}/TeXmacs/bin/tm_lisp
%{_libexecdir}/TeXmacs/bin/tm_lush
%{_libexecdir}/TeXmacs/bin/tm_maple
%{_libexecdir}/TeXmacs/bin/tm_maple_5
%{_libexecdir}/TeXmacs/bin/tm_mathematica
%{_libexecdir}/TeXmacs/bin/tm_matlab
%{_libexecdir}/TeXmacs/bin/tm_maxima
%{_libexecdir}/TeXmacs/bin/tm_mupad
%{_libexecdir}/TeXmacs/bin/tm_mupad_help
%{_libexecdir}/TeXmacs/bin/tm_octave
%{_libexecdir}/TeXmacs/bin/tm_octave.bat
%{_libexecdir}/TeXmacs/bin/tm_python
%{_libexecdir}/TeXmacs/bin/tm_python.bat
%{_libexecdir}/TeXmacs/bin/tm_r
%{_libexecdir}/TeXmacs/bin/tm_reduce
%{_libexecdir}/TeXmacs/bin/tm_sage
%{_libexecdir}/TeXmacs/bin/tm_shell
%{_libexecdir}/TeXmacs/bin/tm_texgraph
%{_libexecdir}/TeXmacs/bin/tm_xypic
%dir %{_datadir}/TeXmacs
%dir %{_datadir}/TeXmacs/examples
%{_datadir}/TeXmacs/LICENSE
%{_datadir}/TeXmacs/doc
%{_datadir}/TeXmacs/examples/texts
%{_datadir}/TeXmacs/fonts
%{_datadir}/TeXmacs/langs
%{_datadir}/TeXmacs/misc
%{_datadir}/TeXmacs/packages
%{_datadir}/TeXmacs/plugins
%{_datadir}/TeXmacs/progs
%{_datadir}/TeXmacs/styles
%{_datadir}/TeXmacs/texts
%{_datadir}/application-registry/texmacs.applications
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/texmacs.desktop
%{_datadir}/mime-info/texmacs.keys
%{_datadir}/mime-info/texmacs.mime
%{_datadir}/pixmaps/TeXmacs-gnu.xpm
%{_datadir}/pixmaps/TeXmacs.xpm
%{_iconsdir}/gnome/scalable/apps/TeXmacs.svg
%{_iconsdir}/gnome/scalable/mimetypes/text-texmacs.svg
%{_mandir}/man1/fig2ps.1*
%{_mandir}/man1/texmacs.1*

%files devel
%{_includedir}/TeXmacs.h
%{_datadir}/TeXmacs/examples/plugins

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.7.18-2m)
- rebuild against graphviz-2.36.0-1m

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7.18-1m)
- update to 1.0.7.18
- rebuild against guile-2.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7.4-5m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.0.7.4-4m)
- fix up Requires again

* Thu Aug 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7.4-3m)
- fix up Requires, please check

* Wed Aug 18 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.0.7.4-2m)
- Change Requires: for texlive

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7.4-1m)
- update to 1.0.7.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7.2-1m)
- update to 1.0.7.2
- renamed from texmacs

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6.12-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6.12-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.6.12-1m)
- import to Momonga

* Sat Mar 24 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.6.9-1m)
- import to Momonga
- version 1.0.6.9

* Wed Oct 11 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.6.6-0.0.1m)
- version 1.0.6.6

* Sat Jul 15 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.6.4-0.0.1m)
- version 1.0.6.4

* Tue Jun 28 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.5.4-0.0.1m)
- version 1.0.5.4

* Wed Nov 03 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.4.3-0.0.1m)
- version 1.0.4.3

* Wed Nov  5 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.2.6-0.0.1m)
- version 1.0.2.6

* Fri Oct 17 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.2.3-0.0.1m)
- build for Momonga
- modified TeXmacs-1.0.2.3-1mdk of Mandrake Cooker

* Mon Oct 13 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.2.3-1mdk
- 1.0.2.3

* Wed Sep 17 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.24-1mdk
- 1.0.1.24

* Sun Aug 24 2003 Michael Scherer <scherer.michael@free.fr> 1.0.1.21-2mdk
- BuildRequires ( libxfree86-devel ) 

* Tue Aug 05 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.21-1mdk
- 1.0.1.21

* Thu Jun 26 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.15-1mdk
- 1.0.1.15

* Wed May 07 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.13-1mdk
- 1.0.1.13

* Fri May 02 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.12-2mdk
- 1.0.1.12

* Thu Apr 10 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.10-1mdk
- 1.0.1.10

* Thu Apr 03 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.8-1mdk
- 1.0.1.8
- requires libguile12-devel

* Tue Feb 25 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.6-1mdk
- 1.0.1.6

* Thu Feb 20 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.5-1mdk
- 1.0.1.5

* Sun Feb 16 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.4-1mdk
- 1.0.1.4

* Fri Feb 07 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.3-1mdk
- 1.0.1.3

* Wed Jan 29 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.2-1mdk
- 1.0.1.2

* Wed Jan 22 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1.1-1mdk
- 1.0.1.1

* Thu Dec 19 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.1-1mdk
- 1.0.1

* Mon Dec 16 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.25-1mdk
- 1.0.0.25

* Fri Dec 06 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.24-1mdk
- 1.0.0.24

* Wed Dec  4 2002 Gotz Waschk <waschk@linux-mandrake.com> 1.0.0.23-2mdk
- rebuild with new guile

* Thu Nov 28 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.23-1mdk
- 1.0.0.23-1mdk

* Tue Nov 19 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.22-1mdk
- 1.0.0.22

* Wed Nov 06 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.21-1mdk
- 1.0.0.21

* Wed Oct 30 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.20-1mdk
- 1.0.0.20

* Tue Oct 22 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.19-1mdk
- 1.0.0.19


* Mon Oct 07 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.18-1mdk
- 1.0.0.18

* Sat Sep 21 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.17-1mdk
- 1.0.0.17

* Mon Aug 26 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.15-1mdk
- from Austin Acton <aacton@yorku.ca> :
	- update

* Tue Jul 30 2002 Austin Acton <aacton@yorku.ca> 1.0.0.11-1mdk
- release 1.0.0.11
- rename build target (dynamic link build is now default in makefile)
- revert to default RPM compile flags

* Mon Jul 15 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.9-1mdk
- 1.0.0.9

* Thu Jul 04 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.8-1mdk
- 1.0.0.8

* Fri Apr 12 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.0.3-1mdk
- new release
- replaces "english" language by "british" and "american"

* Tue Apr 09 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.0.2-1mdk
- new release

* Mon Apr 08 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.0.1-1mdk
- 1.0.0.1

* Thu Mar 14 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0-1mdk
- 1.0

* Tue Mar 05 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.14-1mdk
- 0.3.5.14

* Fri Feb 22 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.13-1mdk
- 0.3.5.13

* Tue Feb 12 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.12-2mdk
- use TeXmacs authors compilation options

* Mon Feb 11 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.12-1mdk
- 0.3.5.12
- make MIXED_TEXMACS

* Sat Jan 19 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.11-2mdk
- updated desc.
- convert xpms to pngs

* Tue Dec 18 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.11-1mdk
- 0.3.5.11

* Fri Dec 14 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.10-1mdk
- 0.3.5.10

* Wed Nov 28 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.8-1mdk
- 0.3.5.8

* Fri Nov 23 2001  Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.7-1mdk
- 0.3.5.7

* Thu Nov 08 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.5-1mdk
- 0.3.5.5

* Mon Oct 29 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.3-1mdk
- 0.3.5.3

* Thu Oct 25 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.2-2mdk
- fixes from Thomas Leclerc <leclerc@linux-mandrake.com> :
	- remove BuildRequires: TeXmacs-fonts
	- Obsoletes: TeXmacs-fonts.
	Fonts are indeed generated at 1st start, and the font files of the package
	interfere with LaTeX if fonts were not previously built, or conflict with
	old files if they were.

* Wed Oct 17 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.2-1mdk
- 0.3.5.2

* Fri Oct 12 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.1-1mdk
- 0.3.5.1

* Thu Oct 11 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.3.5.0-1mdk
- added by Thomas Leclerc <leclerc@linux-mandrake.com> :
	- First Mandrake build
	- specfile adapted from official one
