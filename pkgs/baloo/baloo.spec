%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global akonadi_version 1.12.0
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name:    baloo
Summary: A framework for searching and managing metadata
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2
Group:   User Interface/Desktops
URL:     https://projects.kde.org/projects/kde/kdelibs/baloo
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
## upstream patches

BuildRequires: doxygen
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kdepimlibs-devel >= %{version}
BuildRequires: kfilemetadata-devel >= %{version}
BuildRequires: akonadi-devel >= %{akonadi_version}
BuildRequires: qjson-devel
# for %%{_polkit_qt_policydir} macro
BuildRequires: polkit-qt-devel
BuildRequires: xapian-core-devel
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs >= %{version}

# kio_tags/kio_timeline moved here from kde-runtime
Conflicts: kde-runtime < %{version}

%description
%{summary}.

%package devel
Summary:  Developer files for %{name}
Group:    Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel

%description devel
%{summary}.

%package libs
Summary:  Runtime libraries for %{name}
Group:    System Environment/Libraries
Requires: kdelibs >= %{version}
Requires: %{name} = %{version}-%{release}

%description libs
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING COPYING.LIB
%{_sysconfdir}/dbus-1/system.d/org.kde.baloo.filewatch.conf
%{_kde4_bindir}/akonadi_baloo_indexer
%{_kde4_bindir}/baloo_file
%{_kde4_bindir}/baloo_file_cleaner
%{_kde4_bindir}/baloo_file_extractor
%{_kde4_bindir}/balooctl
%{_kde4_bindir}/baloosearch
%{_kde4_bindir}/balooshow
%{_kde4_libexecdir}/kde_baloo_filewatch_raiselimit
%{_kde4_datadir}/akonadi/agents/akonadibalooindexingagent.desktop
%{_kde4_datadir}/autostart/baloo_file.desktop
%{_datadir}/dbus-1/interfaces/org.kde.baloo.file.indexer.xml
%{_datadir}/dbus-1/system-services/org.kde.baloo.filewatch.service
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_datadir}/kde4/services/baloo_contactsearchstore.desktop
%{_kde4_datadir}/kde4/services/baloo_emailsearchstore.desktop
%{_kde4_datadir}/kde4/services/baloo_filesearchstore.desktop
%{_kde4_datadir}/kde4/services/baloo_notesearchstore.desktop
%{_kde4_datadir}/kde4/services/baloosearch.protocol
%{_kde4_datadir}/kde4/services/kcm_baloofile.desktop
%{_kde4_datadir}/kde4/services/plasma-runner-baloosearch.desktop
%{_kde4_datadir}/kde4/services/tags.protocol
%{_kde4_datadir}/kde4/services/timeline.protocol
%{_kde4_datadir}/kde4/servicetypes/baloosearchstore.desktop
%{_polkit_qt_policydir}/org.kde.baloo.filewatch.policy
%{_kde4_libdir}/kde4/akonadi/akonadi_baloo_searchplugin.so
%{_kde4_libdir}/kde4/akonadi/akonadibaloosearchplugin.desktop
%{_kde4_libdir}/kde4/baloo_contactsearchstore.so
%{_kde4_libdir}/kde4/baloo_emailsearchstore.so
%{_kde4_libdir}/kde4/baloo_filesearchstore.so
%{_kde4_libdir}/kde4/baloo_notesearchstore.so
%{_kde4_libdir}/kde4/kcm_baloofile.so
%{_kde4_libdir}/kde4/kio_baloosearch.so
%{_kde4_libdir}/kde4/kio_tags.so
%{_kde4_libdir}/kde4/kio_timeline.so
%{_kde4_libdir}/kde4/krunner_baloosearchrunner.so

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/baloo
%{_kde4_libdir}/libbaloocore.so
%{_kde4_libdir}/libbaloofiles.so
%{_kde4_libdir}/libbaloopim.so
%{_kde4_libdir}/libbalooxapian.so
%{_kde4_libdir}/cmake/Baloo/

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libbaloocore.so.4*
%{_kde4_libdir}/libbaloofiles.so.4*
%{_kde4_libdir}/libbaloopim.so.4*
%{_kde4_libdir}/libbalooxapian.so.4*

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- initial build (KDE 4.13 RC)
