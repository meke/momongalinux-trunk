%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global obso_name PyKDE4

%global python_ver %(%{__python} -c "import sys ; print sys.version[:3]")
%if 0%{?python3}
%global python3_inc %(%{__python3} -c "from distutils.sysconfig import get_python_inc; print(get_python_inc(1))")
%global python3_ver %(%{__python3} -c "import sys ; print (\\"%s%s\\" % (sys.version[:3],getattr(sys,'abiflags','')))")
%global python3_pyqt4_version %(%{__python3} -c 'import PyQt4.pyqtconfig; print(PyQt4.pyqtconfig._pkg_config["pyqt_version_str"])' 2> /dev/null || echo %{pyqt4_version_min})
%endif

%global pykde4_akonadi 1
%global pyqt4_version 4.10.3
%global sip_version 4.15.3
%global qscintilla_ver 2.8
%global akonadiver 1.12.1
%global phononver 4.7.1
%global sopranover 2.9.4
%global attica_ver 0.4.2
%global qimageblitzver 0.0.6

Summary: Python bindings for KDE4
Name: pykde4
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2
URL: http://developer.kde.org/language-bindings/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
# See https://reviewboard.kde.org/r/101903
# hard-codes sip path to /usr/share/sip, instead of respecting system path
Patch1: 0001-Ensure-SIP-files-are-installed-to-the-right-path-bas.patch
# debian patches
Patch201: add_qt_kde_definitions.diff
Patch202: fix_kpythonpluginfactory_build.diff
Patch203: make_pykde4_respect_sip_flags.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  kdelibs-devel >= %{version}
BuildRequires:  kde-workspace-devel >= %{version}
BuildRequires:  kdegraphics-devel >= %{version}
BuildRequires:  kdevplatform-devel
BuildRequires:  akonadi-devel >= %{akonadiver}
BuildRequires:  attica-devel >= %{attica_ver}
BuildRequires:  soprano-devel >= %{sopraanover}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  phonon-devel >= %{phononver}
BuildRequires:  python-devel
%if 0%{?python3}
BuildRequires:  python3-devel
BuildRequires:  python3-PyQt4-devel >= %{pyqt4_version}, python3-sip-devel >= %{sip_version}
%endif
BuildRequires:  PyQt4-devel >= %{pyqt4_version}
BuildRequires:  qimageblitz-devel >= %{qimageblitzver}
BuildRequires:  qscintilla-devel >= %{qscintilla_ver}
BuildRequires:  qtscriptgenerator
BuildRequires:  sip-devel >= %{sip_version}
BuildRequires:  qwt-devel >= 5.2.1

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: kdelibs >= %{version}
Requires: kde-workspace >= %{version}

Obsoletes: %{obso_name} < %{version}
Provides:  %{obso_name} = %{version}-%{release}

%description
%{summary}.

%package akonadi
Summary: Akonadi runtime support for pykde4
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: kdepimlibs-akonadi >= %{version}
Obsoletes: %{obso_name}-akonadi < %{version}
Provides:  %{obso_name}-akonadi = %{version}-%{release}

%description akonadi
%{summary}.

%package devel
Summary:  Files needed to build other bindings based on KDE4
Group:    Development/Languages
Requires: PyQt4-devel
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-devel < %{version}
Provides:  %{obso_name}-devel = 5{version}-%{release}

%description devel
Files needed to build other bindings for C++ classes that inherit from any
of the KDE4 classes.

%if 0%{?python3}
%package -n python3-pykde4
Summary: Python 3 bindings for KDE
Group:   Development/Languages
Requires: python3-PyQt4 >= %{pyqt4_version}
Requires: python3-sip >= %{sip_version}
%if ! 0%{?pykde4_akonadi}
Provides: python3-pykde4-akonadi = %{version}-%{release}
Requires: kdepimlibs-akonadi >= %{version}
%endif
Obsoletes: python3-%{obso_name} < %{version}
Provides:  python3-%{obso_name} = %{version}-%{release}

%description -n python3-pykde4
%{summary}.

%package -n python3-pykde4-akonadi
Summary: Akonadi runtime support for pykde4
Group: Development/Languages
Requires: python3-pykde4 = %{version}-%{release}
Requires: kdepimlibs-akonadi >= %{version}
Obsoletes: python3-%{obso_name}-akonadi < %{version}
Provides:  python3-%{obso_name}-akonadi = %{version}-%{release}

%description -n python3-pykde4-akonadi
%{summary}.

%package -n python3-pykde4-devel
Group:    Development/Languages
Summary:  Files needed to build PyKDE4-based applications
Requires: python3-PyQt4-devel
Requires: python3-pykde4 = %{version}-%{release}
%if 0%{?pykde4_akonadi}
Requires: python3-pykde4-akonadi = %{version}-%{release}
%endif
Obsoletes: python3-%{obso_name}-devel < %{version}
Provides:  python3-%{obso_name}-devel = %{version}-%{release}

%description -n python3-pykde4-devel
%{summary}.
%endif

%prep
%setup -q
%patch1 -p1 -R -b .use_system_sip_dir
%patch201 -p1 -b .201
%patch202 -p1 -b .202
%patch203 -p1 -b .203

%build
%if 0%{?python3}
mkdir -p %{_target_platform}-python3
pushd    %{_target_platform}-python3
%{cmake_kde4} \
  -DPYTHON_EXECUTABLE:PATH=%{__python3} \
  -DPython_ADDITIONAL_VERSIONS=%{python3_ver} \
  -DPYTHON_LIBRARY=%{_libdir}/libpython%{python3_ver}.so.1.0 \
  -DPYTHON_LIBRARIES=%{_libdir}/libpython%{python3_ver}.so.1.0 \
  -DPYTHON_INCLUDE_PATH=%{_includedir}/python%{python3_ver} \
  ..

  make %{?_smp_mflags} -C python/
popd
%endif

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
  -DPYTHON_LIBRARY=%{_libdir}/libpython%{python_ver}.so.1.0 \
  -DPYTHON_LIBRARIES=%{_libdir}/libpython%{python_ver}.so.1.0 \
  -DPYTHON_INCLUDE_PATH=%{_includedir}/python%{python_ver} \
  ..

  make %{?_smp_mflags}
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%if 0%{?python3}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}-python3/python/

# not python3 compat yet
rm -fv %{buildroot}%{_kde4_libdir}/kde4/kpythonpluginfactory.so

# HACK: fix multilib conflict, similar to PyQt4's http://bugzilla.redhat.com/509415
rm -fv %{buildroot}%{_bindir}/pykdeuic4
mv %{buildroot}%{python3_sitearch}/PyQt4/uic/pykdeuic4.py \
   %{buildroot}%{_bindir}/python3-pykdeuic4
ln -s %{_bindir}/python3-pykdeuic4 \
      %{buildroot}%{python3_sitearch}/PyQt4/uic/pykdeuic4.py

# install pykde4 examples under correct dir
mkdir -p %{buildroot}%{_docdir}
rm -fv %{buildroot}%{_kde4_appsdir}/pykde4/examples/*.py?
mv %{buildroot}%{_kde4_appsdir}/pykde4 %{buildroot}%{_docdir}/python3-pykde4
%endif

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

# HACK: fix multilib conflict, similar to PyQt4's http://bugzilla.redhat.com/509415
rm -fv %{buildroot}%{_bindir}/pykdeuic4
mv %{buildroot}%{python_sitearch}/PyQt4/uic/pykdeuic4.py \
   %{buildroot}%{_bindir}/pykdeuic4
ln -s %{_bindir}/pykdeuic4 \
      %{buildroot}%{python_sitearch}/PyQt4/uic/pykdeuic4.py

# install pykde4 examples under correct dir
mkdir -p %{buildroot}%{_docdir}
rm -fv %{buildroot}%{_kde4_appsdir}/pykde4/examples/*.py?
mv %{buildroot}%{_kde4_appsdir}/pykde4 %{buildroot}%{_docdir}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig ||:

%postun
/sbin/ldconfig ||:

%pre devel
if [ -d %{_bindir}/pykdeuic4 ]; then
rm -rf %{_bindir}/pykdeuic4/
fi

%files
%defattr(-,root,root,-)
%{python_sitearch}/PyKDE4/
%{python_sitearch}/PyQt4/uic/widget-plugins/kde4.py*
%dir %{_docdir}/pykde4
%{_kde4_libdir}/kde4/kpythonpluginfactory.so

%if 0%{?pykde4_akonadi}
%exclude %{python_sitearch}/PyKDE4/akonadi.so
%files akonadi
%defattr(-,root,root,-)
%{python_sitearch}/PyKDE4/akonadi.so
%endif

%files devel
%defattr(-,root,root,-)
%{_kde4_bindir}/pykdeuic4
%{_kde4_bindir}/pykdeuic4-2.7
%{python_sitearch}/PyQt4/uic/pykdeuic4.py*
%{_docdir}/pykde4/examples/
%{_kde4_datadir}/sip/PyKDE4/

%if 0%{?python3}
%files -n python3-pykde4
%defattr(-,root,root,-)
%doc COPYING
%{python3_sitearch}/PyKDE4/
%{python3_sitearch}/PyQt4/uic/widget-plugins/kde4.py*
%dir %{_docdir}/python3-pykde4

%if 0%{?pykde4_akonadi}
%exclude %{python3_sitearch}/PyKDE4/akonadi.so
%files -n python3-pykde4-akonadi
%defattr(-,root,root,-)
%{python3_sitearch}/PyKDE4/akonadi.so
%endif

%files -n python3-pykde4-devel
%defattr(-,root,root,-)
%{_kde4_bindir}/python3-pykdeuic4
%{python3_sitearch}/PyQt4/uic/pykdeuic4.py*
%{_docdir}/python3-pykde4/examples/
%{_kde4_datadir}/python3-sip/PyKDE4/
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Thu Jun 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-2m)
- rebuild against sip-4.14.7

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-2m)
- rebuild against sip-4.14

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- rename from PyKDE4 to pykde4

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- import from Fedora for KDE 4.7.0

* Tue Jul 26 2011 Jaroslav Reznik <jreznik@redhat.com> 4.7.0-1
- 4.7.0

* Fri Jul 08 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-1
- 4.6.95

* Thu Jul 07 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.90-1
- first try
