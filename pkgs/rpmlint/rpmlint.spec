%global momorel 1

Name:           rpmlint
Version:        1.3
Release:        %{momorel}m%{?dist}
Summary:        Tool for checking common errors in RPM packages

Group:          Development/Tools
License:        GPLv2
URL:            http://rpmlint.zarb.org/
Source0:        http://rpmlint.zarb.org/download/%{name}-%{version}.tar.xz
NoSource:       0
Source1:        %{name}.config
Source2:        %{name}-CHANGES.package.old
Source3:        %{name}-etc.config
# EL-4 specific config
Source4:        %{name}.config.el4
# EL-5 specific config
Source5:        %{name}.config.el5

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python >= 2.4
BuildRequires:  rpm-python >= 4.4
BuildRequires:  sed >= 3.95
Requires:       rpm-python >= 4.4.2.2
Requires:       python >= 2.4
Requires:       python-magic
Requires:       cpio
Requires:       binutils
Requires:       desktop-file-utils
Requires:       gzip
Requires:       bzip2
Requires:       xz

%description
rpmlint is a tool for checking common errors in RPM packages.  Binary
and source packages as well as spec files can be checked.


%prep
%setup -q
sed -i -e /MenuCheck/d Config.py
cp -p config config.example
install -pm 644 %{SOURCE2} CHANGES.package.old
install -pm 644 %{SOURCE3} config


%build
make COMPILE_PYC=1


%install
rm -rf $RPM_BUILD_ROOT
touch rpmlint.pyc rpmlint.pyo # just for the %%exclude to work everywhere
make install DESTDIR=$RPM_BUILD_ROOT ETCDIR=%{_sysconfdir} MANDIR=%{_mandir} \
  LIBDIR=%{_datadir}/rpmlint BINDIR=%{_bindir}
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/rpmlint/config

install -pm 644 %{SOURCE4} $RPM_BUILD_ROOT%{_datadir}/rpmlint/config.el4
install -pm 644 %{SOURCE5} $RPM_BUILD_ROOT%{_datadir}/rpmlint/config.el5
pushd $RPM_BUILD_ROOT%{_bindir}
ln -s rpmlint el4-rpmlint
ln -s rpmlint el5-rpmlint
popd

%check
make check


%files
%doc AUTHORS COPYING ChangeLog CHANGES.package.old README config.example
%config(noreplace) %{_sysconfdir}/rpmlint/
%{_sysconfdir}/bash_completion.d/rpmlint
%{_bindir}/rpmdiff
%{_bindir}/el*-rpmlint
%{_bindir}/rpmlint
%{_datadir}/rpmlint/
%exclude %{_datadir}/rpmlint/rpmlint.py[co]
%{_mandir}/man1/rpmlint.1*


%changelog
* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3-1m)
- version up 1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.98-1m)
- update 0.87

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.87-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.87-1m)
- sync with Fedora 11 (0.87-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.85-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.85-1m)
- sync with Rawhide (0.85-3)

* Tue May  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.82-2m)
- release %%{_sysconfdir}/bash_completion.d
- it's already provided by bash-completion

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.82-1m)
- import from Fedora

* Mon Mar  3 2008 Ville Skytta <ville.skytta at iki.fi> - 0.82-3
- Sync Fedora license list with Revision 0.69 (Wiki rev 110) (#434690).

* Thu Dec  6 2007 Ville Skytta <ville.skytta at iki.fi> - 0.82-2
- Remove leftover "Affero GPL" from last license list sync (Todd Zullinger).

* Thu Dec  6 2007 Ville Skytta <ville.skytta at iki.fi> - 0.82-1
- 0.82, fixes #362441, #388881, #399871, #409941.
- Sync Fedora license list with Revision 0.61 (Wiki rev 98).

* Fri Sep 28 2007 Todd Zullinger <tmz@pobox.com>
- Sync Fedora license list with Revision 0.55 (Wiki rev 92).

* Mon Sep  3 2007 Ville Skytta <ville.skytta at iki.fi> - 0.81-1
- 0.81, fixes #239611, #240840, #241471, #244835.
- Improve Fedora license check (Todd Zullinger).
- Sync Fedora license list with Wiki rev 87.

* Wed Aug 29 2007 Ville Skytta <ville.skytta at iki.fi>
- Sync Fedora license list with Wiki rev 84 (Todd Zullinger).

* Thu Aug 16 2007 Ville Skytta <ville.skytta at iki.fi> - 0.80-3
- Sync Fedora license list with Wiki rev 68.
- Move pre-2006 changelog entries to CHANGES.package.old.

* Tue Jul 31 2007 Tom "spot" Callaway <tcallawa@redhat.com> - 0.80-2
- new fedora licensing scheme

* Thu May 31 2007 Ville Skytta <ville.skytta at iki.fi>
- Filter hardcoded-library-path errors for /lib/udev.

* Thu Apr 12 2007 Ville Skytta <ville.skytta at iki.fi> - 0.80-1
- 0.80, fixes #227389, #228645, #233795.
- Accept "Redistributable, no modification permitted" as a valid license.
- Filter messages about doc file dependencies on /bin/sh.
- Add missing dependency on file.

* Fri Feb  2 2007 Ville Skytta <ville.skytta at iki.fi> - 0.79-1
- 0.79, fixes #211417, #212491, #214605, #218250, #219068, #220061, #221116,
  #222585, and #226879.
- Accept *.elX disttags in default config.

* Sun Oct 15 2006 Ville Skytta <ville.skytta at iki.fi> - 0.78-2
- Accumulated bugfixes since 0.78: #209876, #209889, #210110, 210261.
- Filter messages about gpg-pubkeys for now.

* Sun Sep 24 2006 Ville Skytta <ville.skytta at iki.fi> - 0.78-1
- 0.78, fixes #198605, #198616, #198705, #198707, #200032, #206383.
- /etc/profile.d/* filtering no longer needed.

* Sat Sep 16 2006 Ville Skytta <ville.skytta at iki.fi> - 0.77-2
- Filter false positives for /etc/profile.d/* file modes.
- Ship *.pyc and *.pyo as usual.

* Thu Jun 29 2006 Ville Skytta <ville.skytta at iki.fi> - 0.77-1
- 0.77, fixes #194466, #195962, #196008, #196985.
- Make "disttag" configurable using the DistRegex config file option.
- Sync standard users and groups with the FC setup package.
- Disable MenuCheck by default, it's currently Mandriva specific.
- Use upstream default valid License tag list, fixes #191078.
- Use upstream default valid Group tag list (dynamically retrieved from
  the GROUPS file shipped with rpm).
- Allow /usr/libexec, fixes #195992.

* Tue Apr 11 2006 Ville Skytta <ville.skytta at iki.fi> - 0.76-1
- 0.76.

* Mon Mar 27 2006 Ville Skytta <ville.skytta at iki.fi>
- Don't pass -T to objdump for *.debug files (#185227).
- lib64 library path fixes (#185228).

* Wed Mar 15 2006 Ville Skytta <ville.skytta at iki.fi>
- Accept zlib License (#185501).

* Tue Feb 28 2006 Ville Skytta <ville.skytta at iki.fi>
- Accept Ruby License (#183384) and SIL Open Font License (#176405).

* Sat Feb 18 2006 Ville Skytta <ville.skytta at iki.fi> - 0.75-1
- 0.75 + -devel Epoch version check patch from CVS.

* Tue Jan 17 2006 Ville Skytta <ville.skytta at iki.fi> - 0.71-3
- Sync with upstream CVS as of 2006-01-15, includes improved versions of
  most of the earlier patches.
- Add dependency on binutils.
