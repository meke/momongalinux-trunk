%global momorel 2

Summary: PGF (Progressive Graphics File) library
Name: libpgf
Version: 6.11.42
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.libpgf.org/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}-src.zip
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: doxygen
BuildRequires: libtool
BuildRequires: pkgconfig

%description
libPGF contains an implementation of the Progressive Graphics File (PGF)
which is a new image file format, that is based on a discrete, fast
wavelet transform with progressive coding features. PGF can be used
for lossless and lossy compression.

%package devel
Summary: Header files and static libraries from libpgf
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libpgf.

%prep
%setup -q -n %{name}

sh autogen.sh

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING INSTALL README
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%doc doc/html
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_mandir}/man3/*.3*

%changelog
* Sat Jun  9 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.11.42-2m)
- add doc/html to %%doc of package devel

* Sat Jun  9 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.11.42-1m)
- initial package for digikam-2.6.0