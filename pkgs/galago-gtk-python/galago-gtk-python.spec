%global momorel 15

Summary: galago-gtk library for python 
Name: galago-gtk-python
Version: 0.5.0
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/ 
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: libgalago-gtk-devel >= 0.5.0
BuildRequires: galago-python-devel >= 0.5.0
BuildRequires: libgalago-devel >= 0.5.2

%description
galago-gtk library for python

%package devel
Summary:	Files for development using %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains pkg-config file for development of programs
using %{name}.

%prep
%setup -q

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}

make DESTDIR=%{buildroot} install

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/python2.7/site-packages/gtk-2.0/galago/gtk.so
%{_libdir}/python2.7/site-packages/gtk-2.0/galago/gdk.so
%{_datadir}/pygtk/2.0/defs/galago-gtk.defs
%{_datadir}/pygtk/2.0/defs/galago-gdk.defs

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/galago-gtk-python.pc

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.0-14m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-12m)
- full rebuild for mo7 release

* Sun Jun  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-11m)
- fix BuildRequires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-8m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-7m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.0-6m)
- rebuild against python-2.6.1-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-4m)
- %%NoSource -> NoSource

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-3m)
- rebuild against libgalago-0.5.2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-2m)
- rebuild against python-2.5

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initila build
