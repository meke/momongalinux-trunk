%global momorel 13

Summary: free replacement for the Adobe's enscript program
Name: enscript
Version: 1.6.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://people.ssh.com/mtr/genscript/
Source0: http://www.iki.fi/mtr/genscript/enscript-%{version}.tar.gz 
NoSource: 0
Source1: http://www.codento.com/people/mtr/genscript/hl.tar.gz 
NoSource: 1
Patch0: enscript-1.6.4-install.patch
Patch3: enscript-1.6.1-locale.patch
Patch4: enscript-doublefree.patch
Patch6: enscript-1.6.1-CAN-2004-1185.patch
Patch7: enscript-1.6.1-CAN-2004-1186.patch
Patch9: enscript-CVE-2008-3863+CVE-2008-4306.patch
# http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/10443
Patch10: enscript-1.6.4-ruby_syntax.patch
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GNU enscript is a free replacement for the Adobe's enscript program. 
Enscript converts ASCII files to PostScript and spools generated 
PostScript output to the specified printer or leaves it to file. 
Enscript can be easily extended to handle different output media
and it has many options that can be used to customize printouts.

%prep
%setup -q
pushd states
rm -rf hl
tar zxf %{SOURCE1}
popd

%patch0 -p1 -b .install
%patch3 -p1 -b .locale
%patch4 -p1 -b .doublefree
%patch6 -p1 -b .CAN-2004-1185
%patch7 -p1 -b .CAN-2004-1186
%patch9 -p0 -b .CVE-2008-3863+CVE-2008-4306
%patch10 -p1 -b .ruby

%build
%configure --with-media=A4
make %{?_smp_mflags} || make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/{%{_bindir},%{_mandir}/man1,%{_libdir},%{_includedir}}
make install DESTDIR=%{buildroot}

# remove
rm -f %{buildroot}/usr/share/info/dir

%post
/sbin/install-info %{_infodir}/enscript.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/enscript.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/*
%{_datadir}/enscript
%{_infodir}/*.info*
%{_datadir}/locale/*/*/*.mo
%config(noreplace) %{_sysconfdir}/enscript.cfg

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-10m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-8m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-6m)
- update Patch4,6 for fuzz=0

* Fri Nov  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-6m)
- [SECURITY] CVE-2008-3863 CVE-2008-4306
- [SECURITY] CAN-2004-1185 CAN-2004-1186
- import security patches (Patch6,7,9) and bugfix patches (Patch3,4)
  from Fedora 9 updates

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-4m)
- %%NoSource -> NoSource

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.4-3m)
- revised spec for enabling rpm 4.2.

* Wed Mar  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.6.4-2m)
- add ruby syntax patch

* Wed Aug 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6.4-1m)
- import to momonga
