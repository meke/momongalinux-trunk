%global momorel 11

Name:   	geomview	
Summary:	An interactive 3D viewing program
Version:	1.9.4
Release:        %{momorel}m%{?dist}
License:	GPL
URL:       	http://geomview.sourceforge.net/
Group:     	Applications/Engineering
Source:    	http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource:	0
Source1:	%{name}.png
Source2:	brp-compress
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	openmotif
BuildRequires:	openmotif-devel
Requires(post): info
Requires(preun): info

%description
Geomview is an interactive 3D viewing program for Unix. It lets you view and manipulate 3D objects: you use the mouse to rotate, translate, zoom in and out, etc. It can be used as a standalone viewer for static objects or as a display engine for other programs which produce dynamically changing geometry. It can display objects described in a variety of file formats. It comes with a wide selection of example objects, and you can create your own objects too.

%package devel
Summary: Header files for geomview
Group:  Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package includes the header files to compile applications for Geomview.

# do not bzip2 %{_infodir}/figs/*.png
%define __os_install_post %{SOURCE2}; \
	/usr/lib/rpm/momonga/brp-strip; \
	/usr/lib/rpm/brp-strip-static-archive; \
	/usr/lib/rpm/momonga/brp-strip-shared; \
	/usr/lib/rpm/momonga/brp-strip-comment-note; \
	/usr/lib/rpm/momonga/modify-init.d; \
	/usr/lib/rpm/momonga/modify-la

%prep
rm -rf %{buildroot}

%setup -q

%build
%configure --enable-shared
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} transform='s,x,x,' install

rm -f %{buildroot}/%{_datadir}/info/dir

# remove .la
rm -rf %{buildroot}%{_libdir}/*.la

# app icon 
install -D -m644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/%{name}.png

# desktop file
cat > %{name}.desktop <<EOF
[Desktop Entry]
Name=Geomview
GenericName=Geomview 3D viewer
Comment=An interactive 3D viewer
Exec=%{name}
Icon=/usr/share/pixmaps/geomview.png
Type=Application
EOF
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --add-category Engineering \
  --add-category Science \
  --add-category Math \
    ./geomview.desktop

mv %{buildroot}%{_datadir}/doc/geomview %{buildroot}%{_datadir}/doc/geomview-%{version}

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/geomview %{_infodir}/dir || :
/sbin/install-info %{_infodir}/geomview-pt_BR --entry="* Geomview(pt_BR): (geomview-pt_BR).         Programa interativo de visualizac,a~o tridimensional." %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/geomview %{_infodir}/dir || :
  /sbin/install-info --delete %{_infodir}/geomview-pt_BR %{_infodir}/dir || :
fi

%postun
/sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README NEWS AUTHORS ChangeLog COPYING INSTALL INSTALL.Geomview
%{_bindir}/*
%{_libexecdir}/%{name}
%{_libdir}/libgeomview-%{version}.so
%{_datadir}/%{name}
%{_datadir}/applications/geomview.desktop
%{_infodir}/geomview*
%{_infodir}/figs/*.png
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_mandir}/man5/*
%{_datadir}/pixmaps/*

%files devel
%defattr(-,root,root)
%doc doc
%{_libdir}/libgeomview.so
%{_libdir}/lib*.a
%{_includedir}/geomview

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.4-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.4-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.4-9m)
- full rebuild for mo7 release

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-8m)
- modify __os_install_post for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Mar 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-6m)
- update Source2: brp-compress to enable xz compression

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-5m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-4m)
- run install-info

* Wed Nov 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-3m)
- do not bzip2 %%{_infodir}/figs/*.png

* Sun May 18 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.9.4-2m)
- fix conflict files (/usr/share/info/dir)

* Wed May 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.9.4-1m)
- import to Momonga

* Fri Oct 20 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.2-0.0.0.rc9.1m)
- update to 1.8.2-rc9
- - build procedure is completery overhauled
- temporary disabled orrery and maniview and wait for released version.(http://sourceforge.net/mailarchive/forum.php?thread_id=30650233&forum_id=6159)

* Tue Apr 18 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.2-0.0.0.20040221.3m)
- rebuild
- rearrange files in orrery module

* Sat Mar 13 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.2-0.0.0.20040221.2m)
- orrery-0.9.3 (essentially same as 0.9.2)

* Thu Mar 11 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.2-0.0.0.20040221.1m)
- try cvs version (1.8.2-alpha)

* Thu Mar 11 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.1-0.0.7m)
- revise specfile

* Tue Feb 11 2003 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.1-0.0.6m)
- build for Momonga Linux
- build against xforms-1.0
- still does not work correctly...it crashes at start up...
- gcc3.2 work around is suggensted at http://sourceforge.net/mailarchive/forum.php?thread_id=1250370&forum_id=6159
- gcc2.96 works for compiling but geomview crashes
- gcc2.95.3 is same as gcc2.96...

* Fri Nov 1 2002 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.1-0.0.1m)
- build against xforms-1.0RC4-0.1m

* Sun Mar 17 2002 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.1-0.4kl)
- rebuild for Kondara MNU/Linux 2.1 (Asumi RC2)
- change the version of required xforms
- omitt patch2
- omitt /usr/bin/clip (conflicts with  Mesa-demos-3.4.2-8k)

* Wed Nov 21 2001 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.1-0.3kl)
- get klein.wa for maniview from http://www.uni-essen.de/hrz/sw/sgi_52/geomview-1.5/data/groups/
- fixed missing groups/{hook.off,spaceshipSpace.geom,wholecube.geom}
- link geomview/groups to geomview/geom/groups

* Sat Nov 17 2001 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.8.1-0.2kl)
- with orrery-0.9.2 and maniview-2.0.0
* Fri Nov 16 2001 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- take from rpmfind.net (http://www.rpmfind.net/linux/rpm2html/search.php?query=geomview)
- (1.8.1-0.1kl)
- Build for Kondara MNU/Linux 2.0(Mary)
- patch Makefile to link with /usr/X11R6/lib/libOSMesa.so.3.3 (-lOSMesa)

* Fri Oct 5 2001 Rex Dieter <rdieter@unl.edu> 1.8.1-1
- cleanup specfile
- make icon/desktop files
- include option to link xforms-static (untested)

* Fri Sep 28 2001 Rex Dieter <rdieter@unl.edu> 1.8.1-0
- first try. 
- TODO: make subpkgs manual(html), modules, modules-xforms

