%{!?python_sitelib: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global _unitdir /usr/lib/systemd/system

%global momorel 3
%global livearches %{ix86} x86_64 ppc ppc64

Summary: Graphical system installer
Name:    anaconda
Version: 19.31.77
Release: %{momorel}m%{?dist}
License: GPLv2+
Group:   Applications/System
URL:     http://fedoraproject.org/wiki/Anaconda

# To generate Source0 do:
# git clone http://git.fedorahosted.org/git/anaconda.git
# git checkout -b archive-branch anaconda-%{version}-%{release}
# ./autogen.sh
# make dist
Source0: %{name}-%{version}.tar.bz2
Source10: momonga.py

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch4:   anaconda-18.6.5-momonga.patch
Patch5:   anaconda-15.27-fix-isys.patch
Patch11:  anaconda-15.27-no_mediacheck.patch
Patch15:  anaconda-16.22-default_japanese.patch
Patch16:  anaconda-16.22-grub.patch
Patch17:  anaconda-16.25-install-msg-ja.patch
Patch18:  anaconda-16.25-no_lang.patch
Patch20:  anaconda-15.27-support_nilfs_reiser4fs.patch
Patch21:  anaconda-16.25-usb-storage.patch
Patch302: anaconda-15.27-arch.patch
Patch500: anaconda-18.6.5-install-class.patch
Patch700: anaconda-15.29-momonga-efi.patch
Patch701: anaconda-19.31.77-add_pkg.patch
Patch702: anaconda-19.31.77-no_term.patch
Patch703: anaconda-19.31.77-nopkg.patch
Patch704: anaconda-19.31.77-ready.patch
Patch1000: anaconda-19.31.77-79.patch
## for libarchive >= 3.0.0
Patch800: anaconda-16.25-libarchive-3.0.x.patch

# Versions of required components (done so we make sure the buildrequires
# match the requires versions of things).
%global gettextver 0.11
%global gconfversion 2.28.1
%global intltoolver 0.31.2
%global libnlver 1.0
%global pykickstartver  1.99.43.10
%global yumver 3.4.3
%global partedver 1.8.1
%global pypartedver 2.5-2
%global pythonpyblockver 0.45
%global nmver 0.7.2
%global dbusver 1.2.3
%global yumutilsver 1.1.11
%global mehver 0.23-1
%global sckeyboardver 1.3.1
%global firewalldver 0.3.5-1
%global pythonurlgrabberver 3.9.1
%global utillinuxver 2.15.1
%global dracutver 033-26
%global isomd5sum 1.0.10
%global fcoeutilsver 1.0.12
%global iscsiver 6.2.0.870
%global rpmver 4.8.0
%global libarchivever 3.0.4
%global langtablever 0.0.13

BuildRequires: audit-libs-devel
BuildRequires: gettext >= %{gettextver}
BuildRequires: gtk3-devel
BuildRequires: gtk-doc
BuildRequires: gobject-introspection-devel
BuildRequires: glade-devel
BuildRequires: pygobject3
BuildRequires: intltool >= %{intltoolver}
BuildRequires: libX11-devel
BuildRequires: libXt-devel
BuildRequires: libXxf86misc-devel
BuildRequires: libgnomekbd-devel
BuildRequires: libnl-devel >= %{libnlver}
BuildRequires: libxklavier-devel
BuildRequires: pango-devel
BuildRequires: pykickstart >= %{pykickstartver}
BuildRequires: python-devel
BuildRequires: python-urlgrabber >= %{pythonurlgrabberver}
BuildRequires: python-nose
BuildRequires: systemd
BuildRequires: yum >= %{yumver}
BuildRequires: NetworkManager-devel >= %{nmver}
BuildRequires: NetworkManager-glib-devel >= %{nmver}
BuildRequires: dbus-devel >= %{dbusver}
BuildRequires: dbus-python
BuildRequires: rpm-devel >= %{rpmver}
BuildRequires: libarchive-devel >= %{libarchivever}
%ifarch %livearches
BuildRequires: desktop-file-utils
%endif
%ifarch s390 s390x
BuildRequires: s390utils-devel
%endif

Requires: anaconda-widgets = %{version}-%{release}
Requires: python-blivet >= 0.18.30
Requires: gnome-icon-theme-symbolic
Requires: python-meh >= %{mehver}
Requires: libreport-anaconda >= 2.0.21-1
Requires: libselinux-python
Requires: rpm-python
Requires: parted >= %{partedver}
Requires: pyparted >= %{pypartedver}
Requires: yum >= %{yumver}
Requires: python-urlgrabber >= %{pythonurlgrabberver}
Requires: system-logos
Requires: pykickstart >= %{pykickstartver}
Requires: langtable-data >= %{langtablever}
Requires: langtable-python >= %{langtablever}
Requires: libuser-python
Requires: authconfig
Requires: firewalld >= %{firewalldver}
Requires: util-linux >= %{utillinuxver}
Requires: dbus-python
Requires: python-pwquality
Requires: python-IPy
Requires: python-nss
Requires: tigervnc-server-minimal
Requires: pytz
Requires: libxklavier
Requires: libgnomekbd
Requires: realmd
Requires: teamd
Requires: keybinder3
%ifarch %livearches
Requires: usermode
Requires: zenity
%endif
Requires: GConf2 >= %{gconfversion}
%ifarch s390 s390x
Requires: openssh
%endif
Requires: isomd5sum >= %{isomd5sum}
Requires: yum-utils >= %{yumutilsver}
Requires: createrepo
Requires: NetworkManager >= %{nmver}
# Requires: nm-connection-editor
Requires: dhclient
Requires: kbd
Requires: chrony
Requires: ntpdate
Requires: rsync
Requires: hostname
Requires: systemd
%ifarch %{ix86} x86_64
Requires: fcoe-utils >= %{fcoeutilsver}
%endif
Requires: iscsi-initiator-utils >= %{iscsiver}
%ifarch %{ix86} x86_64 ia64
Requires: dmidecode
%if ! 0%{?rhel}
Requires: hfsplus-tools
%endif
%endif

Requires: python-coverage

Obsoletes: anaconda-images <= 10
Provides: anaconda-images = %{version}-%{release}
Obsoletes: anaconda-runtime < %{version}-%{release}
Provides: anaconda-runtime = %{version}-%{release}
Obsoletes: booty <= 0.107-1
Obsoletes: anaconda-yum-plugins <= 1.0
Obsoletes: preupgrade <= 1.1.4

%description
The anaconda package contains the program which was used to install your
system.

%package widgets
Summary: A set of custom GTK+ widgets for use with anaconda
Group: System Environment/Libraries
Requires: pygobject3
Requires: python

%description widgets
This package contains a set of custom GTK+ widgets used by the anaconda installer.

%package widgets-devel
Summary: Development files for anaconda-widgets
Group: Development/Libraries
Requires: glade
Requires: anaconda-widgets = %{version}-%{release}

%description widgets-devel
This package contains libraries and header files needed for writing the anaconda
installer.  It also contains Python and Glade support files, as well as
documentation for working with this library.

%package dracut
Summary: The anaconda dracut module
Requires: dracut >= %{dracutver}
Requires: dracut-network
Requires: xz
Requires: pykickstart

%description dracut
The 'anaconda' dracut module handles installer-specific boot tasks and
options. This includes driver disks, kickstarts, and finding the anaconda
runtime on NFS/HTTP/FTP servers or local disks.

%prep
%setup -q

#%%patch4 -p1 -b .momonga
#%patch5 -p1 -b .fix-isys
#%patch11 -p1 -b .no_mediacheck
#%patch15 -p1 -b .default_japanese
#%patch16 -p1 -b .grub
#%patch17 -p1 -b .install-msg
#%patch18 -p1 -b .no_lang
#%%patch20 -p1 -b .fs_support
#%patch21 -p1 -b .usb_storage
#%%patch302 -p1 -b .arch

#%%patch500 -p1

# use lorax
#%%patch700 -p1
%patch701 -p1 -b .add_pkg
%patch702 -p1 -b .no_team
%patch703 -p1 -b .no_pkg
%patch704 -p1 -b .ready

%patch1000 -p1

## for libarchive >= 3.0.0
#%patch800 -p1

#perl -p -i -e "s|reiserfs|reiserfs reiser4 nilfs2 btrfs|" scripts/mk-images*

# no need anaconda-18
#%ifarch i686
##cp -f scripts/mk-images.x86 scripts/mk-images.i686
#cp -f data/fonts/screenfont-i386.gz data/fonts/screenfont-i686.gz
#%endif

cp %{SOURCE10} pyanaconda/installclasses/

sed -i 's/rpm >= 4.10.0/rpm >= 4.8.0/g' configure configure.ac

%build
%configure --disable-static \
           --enable-introspection \
           --enable-gtk-doc
%{__make} %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" | xargs %{__rm}

%ifarch %livearches
desktop-file-install ---dir=%{buildroot}%{_datadir}/applications %{buildroot}%{_datadir}/applications/liveinst.desktop
%else
%{__rm} -rf %{buildroot}%{_bindir}/liveinst %{buildroot}%{_sbindir}/liveinst
%endif

%find_lang %{name}

rm -rf %{buildroot}/usr/lib64/python2.7/site-packages/pyanaconda/ui/tui/spokes/software.*

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%ifarch %livearches
%post
update-desktop-database &> /dev/null || :
%endif

%ifarch %livearches
%postun
update-desktop-database &> /dev/null || :
%endif

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
#%%doc docs/command-line.txt
#%%doc docs/install-methods.txt
#%%doc docs/mediacheck.txt
%{_unitdir}/*
%{_prefix}/lib/systemd/system-generators/*
#%%{_prefix}/lib/udev/rules.d/70-anaconda.rules
%{_bindir}/anaconda-cleanup
%{_bindir}/analog
%{_bindir}/instperf
%{_sbindir}/anaconda
%{_sbindir}/handle-sshpw
%{_sbindir}/logpicker
#%%{_sbindir}/anaconda-cleanup-initramfs
%{_datadir}/anaconda
%{python_sitearch}/pyanaconda
%{python_sitearch}/log_picker
%{_libexecdir}/anaconda
%ifarch %livearches
%{_bindir}/liveinst
%{_sbindir}/liveinst
%config(noreplace) %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/*
%{_sysconfdir}/X11/xinit/xinitrc.d/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*
%endif

%files widgets
%{_libdir}/libAnacondaWidgets.so.*
%{_libdir}/girepository*/AnacondaWidgets*typelib
%{_libdir}/python*/site-packages/gi/overrides/*
%{_datadir}/anaconda/tzmapdata/*

%files widgets-devel
%{_libdir}/libAnacondaWidgets.so
%{_includedir}/*
%{_datadir}/glade/catalogs/AnacondaWidgets.xml
%{_datadir}/gtk-doc

%files dracut
%dir /usr/lib/dracut/modules.d/80%{name}
/usr/lib/dracut/modules.d/80%{name}/*

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (19.31.77-3m)
- update 19.31.xx-HEAD
- Obso anaconda-yum-plugins, preupgrade

* Wed Jun 11 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (19.31.77-1m)
- update 19.31.77

* Tue May 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.37.12-6m)
- install momonga-release-development, subversion

* Tue May 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.37.12-5m)
- install buildsys-build Group

* Tue May 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.37.12-4m)
- install base packages

* Wed May 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.37.12-3m)
- update momonga.py

* Wed May 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (18.37.12-2m)
- enable to build with glib2 >= 2.36

* Tue May 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.37.12-1m)
- update 18.37.12

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (18.37.8-1m)
- update 18.37.8

* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (18.6.5-2m)
- rebuild against syslinux-5.00-1m

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (18.6.5-1m)
- update 18.6.5

* Tue May  8 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (16.25-5m)
- add anaconda-16.25-usb-storage.patch

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.25-4m)
- rebuild against libarchive-3.0.4

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.25-3m)
- add anaconda-16.25-no_lang.patch

* Tue Jan 17 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (16.25-2m)
- add anaconda-16.25-install-msg-ja.patch

* Thu Nov 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.25-1m)
- update 16.25

* Mon Nov 14 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (16.22-3m)
- update momonga.py
--add LXDE and XFCE for Graphical Desktop
--add LXDE and XFCE for Software Development

* Tue Oct 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.22-3m)
- add anaconda-16.22-default_japanese.patch

* Tue Oct 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.22-2m)
- update anaconda-16.22-momonga.patch

* Sat Oct 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.22-1m)
- update 16.22

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.21-1m)
- update 16.21

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.20-1m)
- update 16.20

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.18-1m)
- update 16.18

* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (15.29-9m)
- correct momonga-efi.patch

* Sun Jul 31 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (15.29-8m)
- fix up for x86_64 again (fix efi-issue)

* Sun Jul 31 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (15.29-7m)
- really fix up for x86_64

* Sun Jul 31 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (15.29-6m)
- fix up for x86_64

* Tue Jul 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.29-5m)
- fix LVM exception

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.29-4m)
- update momonga.py

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (15.29-3m)
- use python_sitearch

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.29-2m)
- Require: transifex-client

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.29-1m)
- update Anaconda-15.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (13.42-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (13.42-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.42-10m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (13.42-9m)
- btrfs enable "jirai" option

* Fri Aug 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (13.42-8m)
- install screenfont-i686.gz

* Fri Aug 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (13.42-7m)
- add Obsoletes: rhpl rhpxl

* Fri Aug 20 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (13.42-6m)
- clean up spec
- add Requires: and BuildRequires:

* Fri Aug 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (13.42-5m)
- Requires: python-nss
- clean up spec
-- remove i586 section
-- remove cannot apply patches

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (13.42-4m)
- remove Requires: busybox-anaconda
-- anaconda does not require busybox from version 12.2

* Mon Aug  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (13.42-3m)
- hack for minimal install

* Thu Jun 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (13.42-2m)
- remove Requires: dhcpv6-client

* Sat May 22 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.42-1m)
- update 13.42

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.41-3m)
- remove and obsolete booty

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.41-2m)
- remove and obsolete rhpl and rhpxl

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.41-1m)
- update 13.41

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.39-2m)
- update anaconda-13.37.2-upd-instroot.patch

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.39-1m)
- update 13.39

* Tue Apr  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (13.37.2-2m)
- --libdir=/usr/lib

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (13.37.2-1m)
- update 13.37.2

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.5.0.59-9m)
- apply glibc212 patch

* Wed Dec 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.59-8m)
- rebuild against audit-2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.5.0.59-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.59-6m)
- change upd-instroot: add momonga-release

* Mon Jul 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.59-5m)
- add nilfs-utils commands

* Sun Jul 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (11.5.0.59-4m)
- remove Requires: momonga-images

* Mon Jul  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.59-3m)
- support nilfs2/reiser4fs

* Wed Jun 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.5.0.59-2m)
- drop ipa-gothic-fonts (Patch2)
- remove gcc44 workaround
- update liveinst patch (Patch21)

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.59-1m)
- update 11.5.0.59

* Fri May 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.54-2m)
- update momonga.py anaconda-11.5.0.54-momonga.patch

* Sat May 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.54-1m)
- update 11.5.0.54

* Mon May 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.52-1m)
- update 11.5.0.52

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.45-1m)
- update 11.5.0.45

* Mon Mar 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.30-1m)
- update 11.5.0.30

* Sat Mar  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.5.0.22-4m)
- rebuild against NetworkManager-glib-devel-0.7.0.98

* Mon Feb 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.5.0.22-3m)
- remove %%global _default_patch_fuzz 2
- apply momonganize liveinst patch (Patch21)
- bytecomp installclasses/momonga.py
- remove -Werror when gcc44
  remove -fno-strict-aliasing workaround for gcc44

* Mon Feb 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.22-2m)
- fix 64bit env

* Mon Feb 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.5.0.22-1m)
- update 11.5.0.22

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.4.0.82-20m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.4.0.82-19m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.4.0.82-18m)
- %%global _default_patch_fuzz 2
- License: GPLv2+

* Sun Dec 28 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.4.0.83-1m)
- update 11.4.0.83

* Sun Dec 28 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (11.4.0.82-17m)
- enabled kernel2627 
- add patch Patch402: anaconda-11.4.0.82-auditd-kernel2628.patch

* Sun Sep 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (11.4.0.82-16m)
- modify spec
- modify mk-image.efi

* Sun Sep 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (11.4.0.82-15m)
- remove obsolated patch

* Sat Sep 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (11.4.0.82-14m)
- add patch401: anaconda-11.4.0.82-kernel2627.patch
  enabled by setting kernel2627 for 1 (default: 0)

* Wed Aug 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.4.0.82-13m)
- get rid of gtk2-common from upd-instroot.patch

* Sat Aug  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.4.0.82-12m)
- update upd-instroot patch

* Mon Jul 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.82-11m)
- remove unused code upd-instroot.patch
- update fs-support.patch

* Mon Jul 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.4.0.82-10m)
- revise upd-instroot for new fonts again

* Mon Jul 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.82-9m)
- update upd-instroot.patch
- update fs-support.patch

* Sun Jul 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.4.0.82-8m)
- update upd-instroot.patch for new fonts

* Fri Jul 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.4.0.82-7m)
- fix fsset.py Exception
- update anaconda-11.4.0.70-upd-instroot.patch

* Thu Jul 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.4.0.82-6m)
- initial any fs support

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (11.4.0.82-5m)
- Require squashfs-tools

* Mon Jun 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.82-4m)
- maybe use Momonga repos

* Tue May 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.82-3m)
- update config and Requires
- worked!

* Sat May 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.82-2m)
- update any config. but exception is generated still. 

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.82-1m)
- update 11.4.0.82

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.81-1m)
- update 11.4.0.81

* Thu May  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.4.0.75-2m)
- Newt changed what it wants as far as things being const or not in newt-0.52.9. *sigh*

* Wed Apr 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.75-1m)
- update 11.4.0.75

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.4.0.37-2m)
- rebuild against gcc43

* Sun Feb 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.37-1m)
- update 11.4.0.37

* Sun Feb 24 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.4.0.27-3m)
- add patch for gcc43 (backported from anaconda-11.4.0.37)

* Tue Jan 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.27-2m)
- remove Req: libdhcp-static 

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.27-1m)
- update 11.4.0.27

* Fri Jan 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.18-2m)
- BuildPreReq isomd5sum

* Fri Jan 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (11.4.0.18-1m)
- update 11.4.0.18

* Wed Oct  3 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (11.2.0.66-6m)
- add patch16

* Sun Aug  5 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (11.2.0.66-5m)
- add patch15 (modify blank label format ... konna patch de suman orz)

* Mon Jul  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.2.0.66-4m)
- update anaconda-11.2.0.66-upd-instroot.patch
-- add 'vnc' package

* Thu Jun 28 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (11.2.0.66-3m)
- add patch14

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.2.0.66-2m)
- good-bye cdrtools and welcome cdrkit

* Wed May 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.2.0.66-1m)
- update 11.2.0.66

* Fri May 18 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (11.2.0.58-1m)
- update 11.2.0.58

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.57-1m)
- update 11.1.1.57

* Sun Apr 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.54-1m)
- update 11.1.1.54

* Mon Apr  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.40-1m)
- update 11.1.1.40

* Thu Mar 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.39-1m)
- update 11.1.1.39

* Sun Mar  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.26-1m)
- Install dekita!

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-liunx.org>
- (11.1.1.19-3m)
- clean up requires
- BuildPerReq, higher version of rhpl 

* Mon Feb 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.19-2m)
- It corresponded to "Rename Maturi"

* Mon Feb  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.19-1m)
- toriaezu

* Fri Jan 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.3-4m)
- support python-2.5

* Mon Dec 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.3-3m)
- support python-2.5

* Tue Dec 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.3-2m)
- add anaconda-11.1.1.3-no_remove_locales.patch
-- Thanks Dai san

* Wed Oct 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1.3-1m)
- update 11.1.1.3

* Tue Aug  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-21m)
- add patch304(anaconda-11.0.5-no_remove_locales.patch)
-- fix Moji bake. dakedo, dasa dasa.

* Fri Aug  4 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-20m)
- unpatch patch301(anaconda-11.0.5-disp_LANG.patch)

* Thu Aug  3 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-19m)
- add patch301(anaconda-11.0.5-disp_LANG.patch)
- modefied anaconda-11.0.5-upd-instroot.patch
-- no remove libunicode-lite*

* Sun Jul 30 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (11.0.5-18m)
- add patch303(anaconda-11.0.5-gettext.patch)

* Sun Jul 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-17m)
- patch13(anaconda-11.0.5-mkimage_i686.patch)

* Sat Jul 29 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (11.0.5-16m)
- add patch302(anaconda-11.0.5-arch.patch)

* Sun Jul 16 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-15m)
- text installer use TERM=xterm
-- TERM=linux was character collapses

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-14m)
- M+1P+IPAG.ttf was added to upd-instroot.
-- use anaconda graphical installer.
-- M+2P+IPAG.ttf ha 'g' ga iya !

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-13m)
- M+2P+IPAG.ttf was added to upd-instroot.
-- use anaconda graphical installer.

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.0.5-12m)
- remove ipagp.ttf from upd-instroot (IPA P Gothic couldn't display some characters)
- make Resucue image

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.0.5-11m)
- add truetype-fonts-indic to upd-instroot

* Fri Jul  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-10m)
- fix anaconda-11.0.5-upd-instroot.patch

* Thu Jul  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-9m)
- downgrade 11.0.5

* Tue Jul  4 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.0.51-1m)
- update 11.1.0.51
- disable "gfs"

* Thu Jun 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.0.49-1m)
- add Patch13: anaconda-11.1.48-to_i686.patch
-- support i386/i586/i686

* Tue Jun 27 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.0.48-3m)
- add Patch13: anaconda-11.1.48-to_i686.patch
-- support i386/i586/i686

* Mon Jun 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.0.48-2m)
- move glib library /lib -> /usr/lib
-- update anaconda-11.1.48-upd-instroot.patch
- disable Patch6: anaconda-11.1.0.8-fix-locales2.patch
-- toriaezu

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.0.48-1m)
- update 11.1.0.48

* Thu Jun  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.0.5-8m)
- update anaconda-11.0.5-upd-instroot.patch

* Fri May 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-7m)
- change anaconda-11.0.5-upd-instroot.patch
-- add sqlite3 into stage2.img

* Sat May 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.0.5-6m)
- cancel anaconda-11.0.5-productivity-momonga3.patch

* Sat May 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.0.5-5m)
- modify "Office and Productivity" for engineering-and-scientific

* Sat May 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.0.5-4m)
- modify "Software Development" for KDE, XFCE and ruby

* Wed May 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-3m)
- disable mediacheck 

* Sun May 14 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-2m)
- create anaconda-11.0.5-momonga3.patch, not write /etc/sysconfig/i18n.

* Fri May 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (11.0.5-1m)
- update 11.0.5

* Wed Nov 2 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (10.1.0.2-25m)
- Patch300: uwabami-10.1.0.2-nishikihebi2.4.patch
- build against python-2.4
- rpm must be python-2.4 ready to build this package

* Tue Aug 30 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (10.1.0.2-24m)
- ppc support.

* Sat Apr 23 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-23m)
- colinux support, just started and doesn't work yet.

* Mon Apr 11 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-22m)
- fix loader2/kbd.c as well as isys.c

* Sun Apr 10 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-21m)
- fix po again, over and over... orz

* Sun Apr  3 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-20m)
- fix po again.

* Fri Apr 02 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-19m)
- revised fix-locales.patch.
- ja.po and anaconda.pot is fixed.
- lvm error is fixed partially.

* Thu Mar 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (10.1.0.2-18m)
- BuildPrereq: slang-devel >= 1.4.9-8m

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-17m)
- fix locale names.

* Tue Mar  8 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-16m)
  fix isys bug(keymap loading)

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-15m)
- split by 700MB.
- revised anaconda-10.1.0.2-momonga1.patch, not write /etc/sysconfig/i18n.

* Fri Mar  4 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-14m)
  add libaal-1.0.so* to upd-instroot

* Tue Mar  1 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-13m)
  use sazanami-gothic

* Sun Feb 20 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-12m)
- revised spec for i586/i686 support.

* Fri Feb 18 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-11m)
- revised spec so the static library avoid stack protection.
- nossp patch is no longer applied, because each diet libraries have
  same name as FC3's.

* Mon Feb 14 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-10m)
- revised upd-instroot.patch due to location of libattr.so.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (10.1.0.2-9m)
- enable x86_64. toriaezu...

* Fri Jan 28 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-8m)
  support i686

* Tue Jan 25 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-7m)
  added /usr/X11R6/lib/libXfixes.so* to upd-instroot

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1.0.2-6m)
- enable i586/i686.

* Thu Dec 16 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-5m)
  rebuild against kudzu-1.1.72

* Mon Dec 13 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-4m)
  added /usr/lib/libattr.so*
  support reiser4

* Tue Dec  7 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-3m)
  fix screenfont-i586
  require pyparted

* Fri Nov 26 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-2m)
  fix upd-instroot patch
  fix for xorg-x11

* Sat Nov  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.1.0.2-1m)
  update to 10.1.0.2

* Wed Nov  3 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.0-3m)
  __nonnull macro was emptied

* Wed Sep 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (10.0-2m)
- revised anaconda-10.0-nossp.patch

* Wed Sep 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (10.0-1m)
- imported from FC2

* Wed Sep 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (9.2-25m)
- requires pump-devel 0.8.20-2m or newer

* Thu Sep  9 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (9.2-24m)
- add patch202 for glibc-kernheaders

* Fri Aug 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (9.2-23m)
  applied rpm-4.3.x support patch.
  
* Fri Jul 30 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (9.2-22m)
- add patch5
- kde-desktop added into personal desktop class
- kde-software-development added into workstation class

* Thu Jul 29 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.2-21m)
- update Requires: rhpl >= 0.121-3m

* Sat Jul 24 2004 mutecat <mutecat@momonga-linux.org>
- (9.2-20m)
- revised anaconda-9.2-momonga5.patch

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (9.2-19m)
- update momonga-script patch

* Mon Jul 19 2004 mutecat <mutecat@momonga-linux.org>
- (9.2-18m)
- Red Hat -> Momonga shuusei.

* Sat Jul 17 2004 mutecat <mutecat@momonga-linux.org>
- (9.2-17m)
- add ppc patch.

* Mon Jul 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (kossori)
- move tools directory: comps.xml.momonga, mkmoiso.i586

* Sun Jul 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (9.2-16m)
- remove ruby-acl ruby-faq ruby-irc from comps.xml.momonga

* Sun Jul 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (9.2-15m)
- update mkmoiso.i586 script

* Sat Jul 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (9.2-14m)
- add comps.xml.momonga and mkmoiso.i586 script

* Thu Jul  8 2004 zunda <zunda at freeshell.org>
- (kossori)
- BuildPreReq: newt-devel, slang-devel

* Tue Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (9.2-13m)
- update new comps.xml.core

* Thu Jul  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (9.2-12m)
- use sazanami instead of kochi

* Thu Jun 17 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (9.2-11m)
- change modutils-devel -> module-init-tools-devel

* Mon May 17 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-10m)
- revised momonga script patch to enable UTF-8 locales.

* Sun May 16 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-9m)
- revised dietlibc patch not to use embed slang-newt.

* Sat May 15 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-8m)
- revised momonga patch for terminfo.
- revised momonga script patch for jfs, reiserfs and xfs.

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-7m)
- redhat-config -> system-config.

* Sun May  2 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-6m)
- revised script patch.

* Sat May  1 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-5m)
- revised patch.

* Wed Apr 28 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-4m)
- revised momonga script patch.

* Mon Apr 26 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-3m)
- revised momonga patch.

* Thu Mar 11 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2-1m)
- import from FC1.

* Tue Oct  8 2002 Jeremy Katz <katzj@redhat.com>
- back to mainstream rpm instead of rpm404

* Mon Sep  9 2002 Jeremy Katz <katzj@redhat.com>
- can't buildrequire dietlibc and kernel-pcmcia-cs since they don't always
  exist

* Wed Aug 21 2002 Jeremy Katz <katzj@redhat.com>
- added URL

* Thu May 23 2002 Jeremy Katz <katzj@redhat.com>
- add require and buildrequire on rhpl

* Tue Apr 02 2002 Michael Fulbright <msf@redhat.com>
- added some more docs

* Fri Feb 22 2002 Jeremy Katz <katzj@redhat.com>
- buildrequire kernel-pcmcia-cs as we've sucked the libs the loader needs 
  to there now

* Thu Feb 07 2002 Michael Fulbright <msf@redhat.com>
- goodbye reconfig

* Thu Jan 31 2002 Jeremy Katz <katzj@redhat.com>
- update the BuildRequires a bit

* Fri Jan  4 2002 Jeremy Katz <katzj@redhat.com>
- ddcprobe is now done from kudzu

* Wed Jul 18 2001 Jeremy Katz <katzj@redhat.com>
- own /usr/lib/anaconda and /usr/share/anaconda

* Fri Jan 12 2001 Matt Wilson <msw@redhat.com>
- sync text with specspo

* Thu Aug 10 2000 Matt Wilson <msw@redhat.com>
- build on alpha again now that I've fixed the stubs

* Wed Aug  9 2000 Michael Fulbright <drmike@redhat.com>
- new build

* Fri Aug  4 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- allow also subvendorid and subdeviceid in trimpcitable

* Fri Jul 14 2000 Matt Wilson <msw@redhat.com>
- moved init script for reconfig mode to /etc/init.d/reconfig
- move the initscript back to /etc/rc.d/init.d
- Prereq: /etc/init.d

* Thu Feb 03 2000 Michael Fulbright <drmike@redhat.com>
- strip files
- add lang-table to file list

* Wed Jan 05 2000 Michael Fulbright <drmike@redhat.com>
- added requirement for rpm-python

* Mon Dec 06 1999 Michael Fulbright <drmike@redhat.com>
- rename to 'anaconda' instead of 'anaconda-reconfig'

* Fri Dec 03 1999 Michael Fulbright <drmike@redhat.com>
- remove ddcprobe since we don't do X configuration in reconfig now

* Tue Nov 30 1999 Michael Fulbright <drmike@redhat.com>
- first try at packaging reconfiguration tool

