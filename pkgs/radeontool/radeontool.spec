%global momorel 6

Summary: Backlight and video output configuration tool for radeon cards
Name: radeontool
Version: 1.5
Release: %{momorel}m%{?dist}
License: "zlib"
URL: http://fdd.com/software/radeon/
Group: System Environment/Base
Source0: http://fdd.com/software/radeon/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}.diff
Patch1: %{name}-fix-option-handling.diff
Patch2: %{name}-get-rid-of-lspci.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pciutils-devel
BuildRequires: zlib-devel

%description
adeontool may switch the backlight and external video output on and off.  Use
radeontool at your own risk, it may damage your hardware.

%prep
%setup -q

%patch0 -p0 -b .volatile
%patch1 -p0 -b .options
%patch2 -p0 -b .no-lspci

%build
gcc %{optflags} -o radeontool radeontool.c -lpci -lz

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}/%{_sbindir}
install -m 755 radeontool %{buildroot}%{_sbindir}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES
%{_sbindir}/radeontool

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against rpm-4.6

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5-1m)
- initial package for Momonga Linux
- split from pm-utils
