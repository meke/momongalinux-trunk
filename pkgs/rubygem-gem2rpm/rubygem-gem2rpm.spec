%global momorel 1
%global abi_ver 1.9.1

# Generated from gem2rpm-0.5.2.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname gem2rpm
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Generate rpm specfiles from gems
Name: rubygem-%{gemname}
Version: 0.8.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyforge.org/projects/gem2rpm/
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Source1: momonga.spec.erb
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
BuildRequires: ruby >= 1.9
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Obsoletes: %{gemname}

%description
Generate source rpms and rpm spec files from a Ruby Gem.  The spec file
tries to follow the gem as closely as possible, and be compliant with the
Momonga rubygem packaging guidelines

%prep

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

install %{SOURCE1} %{buildroot}%{geminstdir}/templates/default.spec.erb
#mv %{buildroot}%{geminstdir}/templates/{fedora.spec.erb,momonga.spec.erb}
#mv %{buildroot}%{geminstdir}/templates/default.spec.erb{,.orig}
#cp %{buildroot}%{geminstdir}/templates/{momonga,default}.spec.erb

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/gem2rpm
%doc %{gemdir}/doc/%{gemname}-%{version}
%dir %{geminstdir}
%doc %{geminstdir}/AUTHORS
%{geminstdir}/bin
%{geminstdir}/lib
%{geminstdir}/templates
%doc %{geminstdir}/README
%doc %{geminstdir}/LICENSE
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.1-6m)
- use RbConfig

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-5m)
- update momonga template
-- obsoletes -doc package

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-4m)
- add momonga template

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-3m)
- update gem2rpm-HEAD.patch

* Wed Aug 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-2m)
- add gem2rpm-HEAD.patch
- update template

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-5m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-4m)
- update rpm template.
-- add abi_ver value

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 13 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-1m)
- import from Fedora to Momonga
- add Patch0

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Oct  6 2008 David Lutterkort <dlutter@redhat.com> - 0.6.0-1
- New version

* Tue Mar 11 2008 David Lutterkort <dlutter@redhat.com> - 0.5.3-1
- Bring in accordance with Fedora guidelines

* Thu Jan  3 2008 David Lutterkort <dlutter@redhat.com> - 0.5.2-2
- Own geminstdir
- Fix Source URL

* Mon Dec 10 2007 David Lutterkort <dlutter@redhat.com> - 0.5.1-1
- Initial package
