%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global momorel 2

Name:           pygpgme
Version:        0.3
Release:        %{momorel}m%{?dist}
Summary:        Python module for working with OpenPGP messages

Group:          Development/Languages
License:        LGPL
URL:            http://cheeseshop.python.org/pypi/pygpgme/
Source0:        http://cheeseshop.python.org/packages/source/p/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel >= 2.7
BuildRequires:  python3-devel
BuildRequires:  gpgme-devel

%description
PyGPGME is a Python module that lets you sign, verify, encrypt and decrypt
files using the OpenPGP format.  It is built on top of GNU Privacy Guard and
the GPGME library.

%package -n python3-pygpgme
Summary: Python3 module for working with OpenPGP messages
Group:   Development/Languages

%description -n python3-pygpgme
PyGPGME is a Python module that lets you sign, verify, encrypt and decrypt
files using the OpenPGP format.  It is built on top of GNU Privacy Guard and
the GPGME library.  This package installs the module for use with python3.

%prep
%setup -q

rm -rf %{py3dir}
cp -a . %{py3dir}

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

pushd %{py3dir} 
CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build
popd

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# No need to ship the tests
rm -rf %{buildroot}%{python_sitearch}/gpgme/tests/
 
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root $RPM_BUILD_ROOT
chmod 0755 $RPM_BUILD_ROOT%{python3_sitearch}/gpgme/*.so
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README PKG-INFO examples tests
%{python_sitearch}/*

%files -n python3-pygpgme
%defattr(-,root,root,-)
%doc README PKG-INFO examples tests
%{python3_sitearch}/*

%changelog
* Tue Mar 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- support python3

* Fri May 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-1m)
- update 0.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1-3m)
- rebuild against python-2.6.1-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-2m)
- rebuild against gcc43

* Fri Jan 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-1m)
- Initial commit Momonga Linux.

* Thu Jan 3 2008 Toshio Kuratomi <toshio@fedoraproject.org> - 0.1-7
- Include egg-info files.

* Fri May 18 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.1-6
- Rebuild to pick up enhancements from gcc on F-8.
- Update licensing to conform to new guidelines.

* Fri May 18 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.1-5
- Rebuild because of a bug in linking to an early version of the python-2.5
  package,

* Mon Oct 23 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.1-4
- Bump and rebuild for python 2.5 on devel.

* Mon Oct 23 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.1-3
- Add a patch to work under Python 2.3.
- Stop shipping the tests as they are useless to the end user.

* Fri Oct 13 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.1-2
- Change URL to cheeseshop

* Sun Oct 08 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.1-1
- Initial build for Fedora Extras. 
