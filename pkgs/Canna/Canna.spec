%global momorel 33

Summary: Canna KANA KANJI convert system
Name: Canna

# use Version: %{majorver}.%{minorver}p%{patchver}
%global majorver 3
%global minorver 7
%global patchver 3

%global cannadir Canna%{majorver}%{minorver}p%{patchver}
%global pubdicrhver 20021028
%global pubdicrhverold1 20021025
%global skkdic4cannaver 0.1.0
%global ut_dic_date 20110409

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# /etc/rpm/specopt/Canna.specopt and edit it.

Version: %{majorver}.%{minorver}p%{patchver}
Release: %{momorel}m%{?dist}
Group: Applications/System
URL: http://canna.sourceforge.jp/
# http://sourceforge.jp/projects/canna/
# original URL: http://www.nec.co.jp/japanese/product/computer/soft/canna/

Source0: http://osdn.dl.sourceforge.jp/canna/9565/%{cannadir}.tar.bz2 
NoSource: 0

Source1: rc.canna
Source2: candoc.tgz

Source5: http://bonobo.gnome.gr.jp/~nakai/canna/pubdic-bonobo-%{pubdicrhver}.tar.bz2

# http://winnie.kuis.kyoto-u.ac.jp/members/ri/hokuto/hokuto/dic/hokuto.t

Source300: canna-tmpfiles.conf
Source301: canna.service

###################################################################

Patch0: Canna-conf.patch
Patch2: Canna-3.6-sharedir.patch
Patch4: Canna-3.6-dont-grab-ctrl-o.patch
Patch5: Canna-3.6-fix-warnings.patch
## some dictionaries were ported from SKK-JISYO.*
Patch17: skk-dictionaries.patch
Patch18: Canna-3.6-cannadic.patch
Patch19: Canna-3.6-shion.patch
Patch21: Canna-3.6-bonobo.patch
# Fix for buffer overrun
Patch23: Canna-3.6-wconv.patch
Patch25: Canna-x86_64.patch
Patch27: Canna-3.7p1-notimeout.patch
Patch28: Canna-oldsock.patch
# Patch from the upstream
Patch40: Canna-3.7p1-fix-duplicated-strings.patch
Patch41: Canna-3.7p3-yenbs.patch
Patch42: Canna-3.7p3-redecl.patch
Patch43: Canna-3.7p3-fix-gcc4-warning.patch
Patch44: Canna-3.7p3-no-strip.patch
Patch50: %{name}-aarch64.patch

#Patch123: Canna37-conf_debug.patch
#
###

Source10: http://www.coolbrain.net/dl/shion/shion.tar.bz2
# NoSource: 10

# ftp://ftp.mickey.ai.kyutech.ac.jp/pub/emacs/lisp/skk/skk9.6.tar.gz
#%%define skk_ver 9.6
#Source20: skk-%{skk_ver}_canna-dic-kondara.tar.bz2

Source20: http://www.math.kochi-u.ac.jp/docky/momonga/skkdic4canna-%{skkdic4cannaver}.tar.gz 
NoSource: 20

# http://cl.aist-nara.ac.jp/lab/nlt/chasen/package/chasen-sys2.02-ipadic2.1.tar.gz
%define chasen_ver 2.02
Source30: chasen-%{chasen_ver}_canna-dic-kondara.tar.bz2
# NoSource: 30

# ftp://ftp.icot.or.jp/ifs/natural-lang/unix/morphdic.tar.gz
%define icot_ver 1.1
Source40: icot-%{icot_ver}_canna-dic-kondara.tar.bz2
# NoSource: 40

Source50: http://dl.sourceforge.net/sourceforge/mdk-ut/anthy-ut-patches-%{ut_dic_date}.tar.bz2
NoSource: 50

###################################################################
# Distribution: Momonga

Requires: cpp
# cpp is used by cannacheck parsing rkc.conf
#
Requires: libspt
Requires: coreutils
Requires(post): chkconfig, initscripts
Requires(preun): Canna-libs, chkconfig

BuildRequires: ncurses-devel
BuildRequires: libspt-devel
BuildRequires: imake
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#Copyright: NEC Corp., Canna Project
License: MIT/X

###################################################################
%description
kana to kanji convert system

%package libs
Summary: Library of Canna
Group: System Environment/Libraries
Provides: Canna-library
Obsoletes: Canna-library

%description libs
Library tool of Canna

%package devel
Summary: Development tool of Canna
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: Canna-libs = %{version}-%{release}

%description devel
Development tool of Canna

###################################################################
%prep
%setup -q -a 2 -a 5 -a 10 -a 20 -a 30 -a 40 -a 50 -n %{cannadir}

%patch0 -p1 -b .conffix
%patch2 -p1 -b .share
%patch4 -p1 -b .ctrl-o
%patch5 -p1 -b .warnings
%patch17 -p1 -b .skk
%patch18 -p1 -b .cannadic
%patch19 -p1 -b .shion
%patch21 -p1 -b .bonobo
%patch23 -p1 -b .wconv
%ifarch x86_64
%patch25 -p1 -b .x86_64
%endif
%patch27 -p1 -b .notimeout
%patch28 -p1 -b .oldsock
%patch40 -p1 -b .duplicate
%patch41 -p1 -b .yenbs
%patch42 -p1 -b .redecl
%patch43 -p1 -b .gcc4
%patch44 -p1 -b .no-strip
cd ..
%patch50 -p1 -b .aarch64

cat %{_builddir}/%{cannadir}/anthy-ut-patches-%{ut_dic_date}/zipcode.t | sort \
    > %{_builddir}/%{cannadir}/dic/ideo/words/zipcode.t

cat %{_builddir}/%{cannadir}/pubdic-bonobo/*.p | sort \
    >> %{_builddir}/%{cannadir}/dic/ideo/pubdic/y.p

###################################################################
%build
%ifarch ppc64
cp %{_datadir}/libtool/config.* canuum
%endif

export _POSIX2_VERSION=199209

xmkmf
make Makefile
### MUST NOT USE THE %%{_smp_mflags}.
%undefine _smp_mflags
%make canna

cd canuum
xmkmf -a
%make
cd ..

export RPM_CANNAIDEO_DIR=%{_builddir}/%{cannadir}/dic/ideo
export RPM_CANNACMD_DIR=%{_builddir}/%{cannadir}/cmd
export RPM_CANNA_POD=%{_builddir}/%{cannadir}/dic/ideo/pubdic/pod

pushd anthy-ut-patches-%{ut_dic_date}
for i in %{_builddir}/%{cannadir}/cmd/*; do \
if [ -d $i ]; then \
  export PATH=$PATH:$i; \
fi \
done;
sed -e "s|crxdic|crxdic -D ${RPM_CANNAIDEO_DIR}/grammar/cnj.bits|" \
        ${RPM_CANNACMD_DIR}/mkbindic/mkbindic > \
        ${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp
chmod +x ${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp

# Momonga hack for exec crxdic
#  egrep -v '(D2T15|D2T30|N2KY|TKN)' gcannaf.ctd > gcannaf.ctd.$$
#  mv gcannaf.ctd.$$ gcannaf.ctd

#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m gcanna.ctd
#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -s gcannaf.ctd

#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m chimei.ctd
#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m ekimei.ctd
#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m kaisha.ctd
#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m gijutu.ctd
#${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m names-ut.ctd
popd

pushd pubdic-bonobo
cat *.p | ${RPM_CANNA_POD} - -p -i -2 > bonobo.spl
${RPM_CANNACMD_DIR}/mergewd/mergeword< bonobo.spl > bonobo.t
rm -rf bonobo.spl
${RPM_CANNACMD_DIR}/mkbindic/mkbindic.tmp -m bonobo.t
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_includedir}
%{__mkdir_p} %{buildroot}%{_unitdir}
%{__mkdir_p} %{buildroot}%{_sysconfdir}/sysconfig
%{__mkdir_p} %{buildroot}%{_mandir}/ja/{man1,man3}
%{__mkdir_p} %{buildroot}/var/lib/canna/dic/canna

make DESTDIR="%{buildroot}" libCannaDir=%{_libdir} install
make DESTDIR="%{buildroot}" cannaManDir=%{_mandir} MANSUFFIX=1 LIBMANSUFFIX=3 FILEMANSUFFIX=5 install.man

for i in `find $RPM_BUILD_ROOT%{_mandir}/ja -type f`; do
  iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

# Delete misc files except manual.sed
# This is RedHat method
#%%{__mv} misc/manual.sed .
#%%{__rm} -fr misc
#%%{__mkdir} misc
#%%{__mv} manual.sed misc

install -p -m 0644 %{SOURCE301} %{buildroot}%{_unitdir}/canna.service
echo 'OPTIONS="-syslog -u bin"' > %{buildroot}%{_sysconfdir}/sysconfig/canna
#ln -sf %{_libdir}/canna/default.canna %{buildroot}/etc/default.canna

install -d -m 0755 %{buildroot}%{_sysconfdir}/tmpfiles.d
install -p -m 0644 %{SOURCE300} %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf

touch %{buildroot}/etc/rkc.conf
cat <<EOF > %{buildroot}/etc/rkc.conf
host "*" {
  server_timeout 10000;
};
cannahost "unix";
EOF
# cat <<EOF > %{buildroot}/etc/rkc.conf
# // sample rkc.conf
# host "*" {
#   server_timeout 1000;
# };
# host "foo,bar" {
#   server_timeout 300;
# };
# host "foo" {
#   server_timeout 700;
# };
# cannahost "bar";
# cannahost "unix,foo";
# EOF

cd canuum
make DESTDIR="%{buildroot}" install
cd ..

#install cannadic
pushd anthy-ut-patches-%{ut_dic_date}/
#%{__install} -m 644 gcanna*.c[bl]d %{buildroot}/var/lib/canna/dic/canna
#%{__install} -m 644 chimei.c[bl]d %{buildroot}/var/lib/canna/dic/canna
#%{__install} -m 644 ekimei.c[bl]d %{buildroot}/var/lib/canna/dic/canna
#%{__install} -m 644 kaisha.c[bl]d %{buildroot}/var/lib/canna/dic/canna
#%{__install} -m 644 gijutu.c[bl]d %{buildroot}/var/lib/canna/dic/canna
#%{__install} -m 644 names-ut.c[bl]d %{buildroot}/var/lib/canna/dic/canna
popd

pushd pubdic-bonobo
%{__install} -m 644 bonobo*.c[bl]d \
        %{buildroot}/var/lib/canna/dic/canna
cp README ../README.bonobo
popd

#install shion dic
pushd shion
make DESTDIR=%{buildroot} install
cp README ../README.shion
cp COPYRIGHT ../COYRIGHT.shion
popd

##install skk dic
#pushd skk-%{skk_ver}_canna-dic-kondara
#cp dic/* %{buildroot}/var/lib/canna/dic/canna
#cp doc/ReadMe ../ReadMe.skk
#cp doc/ReadMe.English ../ReadMe.English.skk
#cp HOW_TO_MAKE_DIC ../HOW_TO_MAKE_DIC.skk
#popd

#install skk dic
pushd skkdic4canna-%{skkdic4cannaver}
cp skk.cbd %{buildroot}/var/lib/canna/dic/canna
cp skk.cld %{buildroot}/var/lib/canna/dic/canna
cp skk.ctd %{buildroot}/var/lib/canna/dic/canna
popd

#install chasen dic
pushd chasen-%{chasen_ver}_canna-dic-kondara
cp dic/* %{buildroot}/var/lib/canna/dic/canna
cp doc/README ../README.chasen
cp doc/README.ja ../README.ja.chasen
cp doc/manual-j.pdf ../manual-j.pdf.chasen
cp doc/manual-j.tex ../manual-j.tex.chasen
cp doc/manual.pdf ../manual.pdf.chasen
cp doc/manual.tex ../manual.tex.chasen
cp HOW_TO_MAKE_DIC ../HOW_TO_MAKE_DIC.chasen
popd

#install icot dic
pushd icot-%{icot_ver}_canna-dic-kondara
cp dic/* %{buildroot}/var/lib/canna/dic/canna
cp doc/COPYRIGHT ../COPYRIGHT.icot
cp doc/COPYRIGHT.j ../COPYRIGHT.j.icot
cp doc/README ../README.icot
cp doc/README.j ../README.j.icot
cp HOW_TO_MAKE_DIC ../HOW_TO_MAKE_DIC.icot
popd

cat > $RPM_BUILD_ROOT%{_sysconfdir}/hosts.canna << EOF
unix
EOF

cat > $RPM_BUILD_ROOT%{_sysconfdir}/canna/cannahost << EOF
unix
EOF

for bin in addwords cpdic delwords lsdic mkdic mvdic rmdic syncdic ; do
	ln -sf catdic $RPM_BUILD_ROOT%{_bindir}/${bin}
done
ln -sf ../bin/catdic $RPM_BUILD_ROOT%{_sbindir}/cannakill

chmod +w -R $RPM_BUILD_ROOT%{_sysconfdir}/canna/sample
mv $RPM_BUILD_ROOT%{_sysconfdir}/canna/sample $RPM_BUILD_DIR/%{cannadir}
%{__mkdir_p} $RPM_BUILD_ROOT%{_var}/run/.iroha_unix

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%triggerun -- Canna < 3.7p3-32m
/usr/bin/systemd-sysv-convert --save canna >/dev/null 2>&1 || :
/sbin/chkconfig --del canna >/dev/null 2>&1 || :
/bin/systemctl try-restart canna.service >/dev/null 2>&1 || :

%pre
getent group canna >/dev/null || groupadd -r canna
getent passwd cannna >/dev/null || useradd -r -g canna -d %{_localstatedir}/lib/canna -s /sbin/nologin -c 'Canna Service User' canna
exit 0

%post
if ! grep -q canna /etc/services
then
        echo "canna             5680/tcp" >>/etc/services
fi
%systemd_post canna.service

%preun
%systemd_preun canna.service

%postun
%systemd_postun_with_restart canna.service

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc CHANGES.jp INSTALL INSTALL.jp
%doc README{,.jp,.bonobo} 
%doc WHATIS WHATIS.jp
%doc Canna.conf 
%doc *chasen
%doc *shion
%doc HOW_TO_MAKE_DIC.icot
%doc $RPM_SOURCE_DIR/candoc.tgz
%doc $RPM_BUILD_DIR/%{cannadir}/sample
%config(noreplace) %{_sysconfdir}/hosts.canna
%dir %{_sysconfdir}/canna
%config %{_sysconfdir}/canna/default.canna
%config(noreplace) %{_sysconfdir}/canna/cannahost
%config(noreplace) %{_sysconfdir}/sysconfig/canna
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf
%config /etc/rkc.conf
%dir %{_datadir}/canna
%{_datadir}/canna/*
%attr(-,bin,bin) %{_sbindir}/cannaserver
%{_bindir}/*
# for multilib
%{_sbindir}/cannakill
%attr(-,bin,bin) /var/lib/canna/dic
%attr(-,bin,bin) %dir /var/log/canna
%attr(-,bin,bin) %dir %{_var}/run/.iroha_unix
%{_mandir}/man1/*
%{_mandir}/ja/man1/*
%{_unitdir}/canna.service

%files libs
%defattr(-,root,root)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/canna
%{_mandir}/man3/*
%{_mandir}/ja/man3/*

%changelog
* Wed Nov 13 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7p3-33m)
- add canna user

* Tue Nov 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7p3-32m)
- support systemd
- rename Canna-library to Canna-libs
- update any patches

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7p3-31m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7p3-30m)
- rebuild for new GCC 4.5

* Sat Sep  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7p3-29m)
- [BUG FIX] fix up %%post and %%preun for OmoiKondara

* Sat Sep  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7p3-28m)
- [CRITICAL BUG FIX] import and modify sharedir.patch from vine
- Canna is working fine now

* Sat Sep  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7p3-27m)
- [CRITICAL BUG FIX] set %%attr(-,bin,bin) %%dir %%{_var}/run/.iroha_unix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7p3-26m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-25m)
- use Requires and BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7p3-24m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7p3-23m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7p3-22m)
- update Patch16 for fuzz=0

* Mon Sep  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-21m)
- add Source50: http://dl.sourceforge.net/sourceforge/mdk-ut/anthy-ut-patches-%{ut_dic_date}.tar.bz2
- delete zipcode, zip99
- delete cannadic

* Tue Aug 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-20m)
- re-fix %%doc
- fix x86_64 build, update Pach25: Canna-x86_64.patch

* Tue Aug 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-19m)
- fix %%doc

* Mon Aug 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-18m)
- update Patch0: Canna-conf.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-17m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7p3-16m)
- remove BPR libtermcap-devel

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7p3-15m)
- %%NoSource -> NoSource

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.7p3-14m)
- convert ja.man(UTF-8)

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7p3-13m)
- NoSouce to Source : shion.tar.bz2

* Tue Jan  2 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.7p3-12m)
- patch25 for lib64 arch
- enable ppc64

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7p3-11m)
- revise %%files for rpm-4.4.2

* Sun Oct 16 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-10m)
- add Requires: libspt

* Sat Aug 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-9m)
- renew skkdic

* Tue Aug 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-8m)
- remade Patch17: Canna36-sample-canna.patch for new skk dic

* Mon Jul 04 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-7m)
- use new skkdic (http://www.math.kochi-u.ac.jp/docky/momonga/skk_mine.tar.gz)

* Sat Jul  2 2005 TASHIRO Hideo <tashiron@momonga-linux.org>
- (3.7p3-6m)
- move Canna library directory from %%{_prefix}/lib/canna
   to %%{libdir}/canna in %%files section

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.7p3-5m)
- enable x86_64.

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-4m)
- use cp -p instead of cp -pR

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-3m)
- use cp -pR at copy %%{S:1} %%{buildroot}%%{_initscriptdir}/canna
  requested by [momongaja:00102]
- renumber Patch0: Canna37-conf.patch and Patch1: Canna36-adapt.patch
- add Patch5: Canna-3.6-fix-warnings.patch
- add Patch25: Canna-x86_64.patch
- delete BuildPrereq: XFree86-devel, use BuildPrereq: xorg-x11-devel
- add Patch27: Canna37-notimeout.patch

* Wed Dec 01 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-2m)
- update cannadicver to 0.95c
- add Requires: cpp

* Thu May 20 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p3-1m)
- update to 3.7p3

* Wed Apr  7 2004 Toru Hoshina <t@momonga-linux.org>
- (3.7p1-6m)
- add PreReq: coreutils

* Fri Apr 02 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p1-5m)
- update cannadic to 0.95b

* Mon Jan 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p1-4m)
- use condrestart

* Mon Jan 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p1-3m)
- chmod 0755 %{_initscriptdir}/canna

* Mon Jan 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p1-2m)
- merged Patch22: Canna37-rkc-conf-etc.patch into Patch23

* Sat Jan 03 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7p1-1m)
- update to 3.7p1

* Wed Dec 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7-3m)
- add Patch22: Canna37-rkc-conf-etc.patch
- add Patch23: Canna37-rkc-conf-etc.patch
- use /etc/rkc.conf instead of ~/.cannax/rkc.conf
- tmp add Patch123: Canna37-conf_debug.patch

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7-3m)
- make new rkc.conf sample: resolve I can not exec rxvt on xfce4 env.
- reported KDE env. too
- short timeout setting in rkc.conf cause error exec 'cannacheck -v'
  but sometimes done not always

* Fri Dec 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7-2m)
- update zipcode to 20030204 (New Source)
- add touch %%{buildroot}/etc/rkc.conf

* Fri Dec 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7-1m)
- update to 3.7
- use %%NoSource
- update Patch1: Canna37-conf.patch
- comment out no need Patch5: Canna36-canuumfix.patch
- comment out no need Patch18: Canna35b2-Imake-xf420.patch
- add BuildRequires: libspt-devel for use libspt-config at canuum
-
- add tmp #%%config(noreplace) rkc.conf
- why not included tarball
- I do: 'mkdir ~/.cannax; touch ~/.cannax/rkc.conf
- and check: cannacheck -v

* Tue Sep 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p4-2m)
- update cannadicver to 0.95a

* Tue Sep 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p4-1m)
- update 3.6p4
- update Patch17: Canna36-sample-canna.patch
- comment out misc/ files move section
- s/cannadirname/cannadir/g
- fix files
- change License: MIT/X for speclint

* Thu Jul 17 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p3-7m)
- use specopt

* Mon Jul 14 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (3.6p3-6m)
- s/$RPM_BUILD_DIR/%{_builddir}/g
- modify /etc/default.canna

* Mon Jul 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p3-5m)
- s/$RPM_BUILD_ROOT/%{buildroot}/g
- use rpm macros
- update cannadic to 0.95
- s/gcanna.t/gcanna.ctd/g

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.6p3-4m)
- export _POSIX2_VERSION=199209

* Tue Jan 28 2003 zunda <zunda@freeshell.org>
- (3.6p3-3m)
- man paths are cleaned up
- C man pages are added

* Tue Jan 28 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.6p3-2m)
- revice rc.canna and add %%{_sysconfdir}/sysconfig/canna
  read %%{_sysconfdir}/sysconfig/canna in %%{_initscriptdir}/canna
  $OPTIONS defined in %%{_sysconfdir}/sysconfig/canna is passed to cannaserver as argument

* Sat Jan 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p3-1m)
- version up to 3.6p3
- revert "comment out mv man1x/* to man1/*" for 3.6p2 Canna.conf typo
- change symlink /etc/default.canna -> /usr/lib/canna/default.canna
  because /usr/lib/canna/sample/default.canna was obsoleted
- change patch name for cvs
  Canna36p2-conf.patch to Canna36-conf.patch
  Canna36p2-canuumfix.patch to Canna36-canuumfix.patch
  Canna36p2-latin.patch to Canna36-latin.patch
  Canna36p2-wconv.patch to Canna36-wconv.patch

* Fri Jan 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p2-1m)
- version up to 3.6p2
- adapt Patch1: Canna36-conf.patch.bz2 to Canna36p2 and stop bzip2ed
  and rename to Canna36p2-conf.patch
- adapt Patch2: Canna36-adapt.patch.bz2  to Canna36p2 and stop bzip2ed
  this is catch up canuum in Canna36p2 was merged from FreeWnn
- comment out: Patch3: Canna36p1-catdic-imake.patch, now tarball include this
- adapt: Patch5: Canna35b2-canuumfix.patch to Canna36p2
  and rename to Patch5: Canna36p2-canuumfix.patch
- merged Patch10: Canna36-notsu.patch to Patch1
  and delete Patch10
- adapt Patch12: Canna35b2-latin.patch to Canna36p2
  and rename to Canna36p2-latin.patch
- adapt Patch21: Canna36-wconv.patch to Canna36p2
  and rename to Canna36p2-wconv.patch
- change make method of cannuum
- comment out mv man1x/* to man1/*
- memory was exhosted by sort command in mkbindic...,
  Uso CPU was over load by sort command in mkbindic...why?

* Tue Jan 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p1-5m)
- revive canuum (cf. [Momonga-devel.ja:01269])

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p1-4m)
- update pubdicrhver and delete NoSource: 5
- update zipcodever and delete NoSource: 6

* Fri Dec 13 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p1-3m)
- roll back Patch17: Canna36-sample-canna.patch

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p1-2m)
- update cannadicver to 0.94f
- delete included Patch19: cannadic-%{cannadicver}.wrongword.patch

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6p1-1m)
- update to 3.6p1
- change Patch2: Canna36-adapt.patch.bz2
- delete Patch3: Canna36-catdic-imake.patch
- make Canna36-catdic-imake.patch for aboid symlink problem in rpm rebuild
- adapt cannakill moved /usr/sbin to /usr/bin
- define cannadirname Canna36p1

* Sat Nov 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-10m)
- change use pushd and popd
- thanks to tamo [Momonga-devel.ja:00470]

* Wed Nov 13 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-9m)
- change patch for make zipcode.ctd in dic/ideo/words/Imakefile
- revised
- import skk-dictionaries.patch, and cut zipcode.t,
  because this part is overwrite by zipcode-*.tar.gz's zipcode.t

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-8m)
- update zipcode dic by kazuhiko
- update pubdic-bonobo dic by kazuhiko
- add patch for fix pubdic-bonobo typo

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-7m)
- add zipcode dic
- comment out zip99
- add pubdic-bonobo dic
- check if canna server is running by existence of
  /var/lock/subsys/canna (wxg...) by kazuhiko

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-6m)
- s/%{buildroot}/$RPM_BUILD_ROOT/g
- fix typo s/cannum/canuum/g
- comment out make canuum and make install canuum
- delete hokuto dic from dics.dir and sample.canna

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-5m)
- NoSource: 10 (shion.tar.bz2)

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-4m)
- update cannadic to 0.94e and NoSource
- osbolete kondara-cannadic.tar.bz2
- osbolete README.kondara
- thanx to RedHat using self export and build method
- add README.gcanna
- change Canna36-dics.dir.patch
- delete do strip

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-3m)
- Distribution: Momonga
- update shion dic 2000-12-12 to 2001-09-16
- use binanry dic shion.tar.bz2 (shion.tar.gz is text dic)
- obsolete Patch16: Canna35b2-default-canna.patch
- change symlink to /etc/default.canna
  old /usr/lib/canna/default.canna
  new /usr/lib/canna/sample/default.canna
- fit Canna35b2-dics.dir.patch to Canna-3.6 and shion dic
- fit Canna35b2-sample-canna.patch to Canna-3.6 and shion dic

* Tue Nov 12 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-2m)
- add -syslog -u bin option at starting daemon in rc.canna(/etc/init.d/canna)

* Mon Nov 11 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-1m)
- import buffer overrun fix patch from RedHat 3.5b2-70.8.0,
  thnax to Yukihiro Nakai <ynakai@redhat.com> 
- adapt Canna-3.5b2-wconv.patch and rename to Canna36-wconv.patch
- add Canna Project to Copyright

* Mon Nov 11 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-0.3m)
- comment out install and install.man about canuum. Is it ture?
- reviseed
- make DESTDIR="%{buildroot}" canuum.jtmp, but I wanna obsolete canuum...
- changed the way of protect manual.sed from RedHat
- revise make DESTDIR="%{buildroot}" canuum.jtmp
- comment out install and install.man about cannum, again
- fix catdic-imake.patch cannakill path bin(cannaBinDir) to sbin(cannaSrvDir)

* Mon Nov 11 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-0.2m)
- delete gcc2.95.3 from BuildPrereq
- adapt patch3: Canna35b2-2.patch.bz2 for Canna36 
  to Canna36-conf.patch.bz2 and Canna36-adapt.patch.bz2
- delete Patch4: Canna35b2-glibc2.1.patch was included Canna36 tarball
- adapt Patch10: Canna35b2-notsu.patch to Canna36
- delete Patch11: Canna35b2-nobcopy.patch was obsoleted
- delete Patch13: Canna35b2-security-20000629.patch was merged
- fix and make Patch4: Canna36-catdic-imake.patch
- add MANDIR=%{_mandir} to make DESTDIR="%{buildroot}" install.man

* Mon Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>             
- (3.5-0.0002065m)
- use gcc-3.2 instead of gcc-2.95.3

* Fri Nov 01 2002 Yukihiro Nakai <ynakai@rehdat.com> 3.5b2-70.8.0
- Add wconvert.c buffer overrun fix patch

* Fri Oct 25 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-0.1m)
- First release from Canna Project 
- chnage Source0 to sourceforge and NoSource0
- change URL to sourceforge
- obsolete unoff1 patch, because this patch was integrated to tarball
- obsolete unoff2 patch, because this patch was integrated to tarball
- obsolete hack1 patch, because this patch was integrated to tarball

* Sat Oct 19 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5-0.0002064m)
- chnage Source0 tokyonet to sra.
- Because server reply message below.
  Invalid reply: "You are not welcome to use proftpd from `hostname`."
  ncftpls: cannot open ftp.tokyonet.ad.jp: invalid reply from server.

* Mon Oct  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.5-0.0002063m)
- use gcc-3.2 instead of gcc-2.95.3

* Tue May 14 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.5-0.0002062k)
- bzip2ed and cvs add Canna35b2-2.patch, 
  because Canna35b2.patch has Id expanded problem
- change ja_JP.ujis to ja_JP.eucJP
- change /usr/man/ja_JP.ujis/man{1,3}x to /usr/share/man/ja/man3x

* Mon May 13 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.5-0.0002060k)
- remove zip99 from alpha (from Vine 3.5b2-39)

* Wed Apr 17 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.5-0.0002058k)
- add Canna35b2-Imake-xf420.patch
- s/coolbrain/shion/ in coment line

* Sat Mar 30 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.5-0.0002056k)
- add shion dic's URI in comment line
- add cannadic's URI in comment line
- Change Source0 URI, 
  ftp.nec.co.jp is not found, but mirror site is exist

* Tue Feb 12 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.5-0.0002054k)
- Canna35b2.tar.gz was included.
  ftp.nec.co.jp not found :-<

* Mon Jan  7 2002 Masahiro Takahata <takahata@kondara.org>
- (3.5-0.0002052k)
- modify to rc.canna

* Thu Nov  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.5-0.0002046k)
- modify to use not gcc_2_95_3 but gcc -V 2.95.3

* Tue May  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.5-0.0002044k)
- move Canna-library from Require tag to PreReq tag

* Thu Apr 12 2001 Shingo Akagaki <dora@kondara.org>
- (3.5-0.0002038k)
- /etc/rc.d/init.d/functions -> /etc/init.d/functions

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.5-0.0002037k)
- build against rpm-3.0.5-39k

* Fri Dec 22 2000 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (3.5-0.0002035k)
- update shion.
- modify Canna35b2-default-canna.patch.
- modify Canna35b2-sample-canna.patch.
- modify Canna35b2.patch.

* Thu Dec  7 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.5-0.0002033k)
- fixed japanese man location bug (jitterbug #803)

* Sat Dec  2 2000 AYUHANA Tomonori <l@kondara.org>
- (3.5-0.02031k)
- version up cannadic dits-20000801 to shion-20001129
- zip99 kondara-cannadic shouldn't be NoSource.

* Mon Nov 27 2000 Toru Hoshina <toru@df-usa.com>
- (3.5b2-27k)
- use _initscriptdir macro to keep backword compatibility.

* Wed Nov 22 2000 Toru Hoshina <toru@df-usa.com>
- (3.5b2-25k)
- *canna-dic* shouldn't be NoSource.

* Sat Nov 18 2000 TABUCHI Takaaki <tab@kondara.org>
- (3.5b2-23k)
- fix typo caom to kaom at dics.dir (jitterbug #785)

* Mon Nov 13 2000 AYUHANA Tomonori <l@kondara.org>
- (3.5b2-21k)
- pre build dic files (chasen,coolbrain,skk,icot)

* Sun Nov 05 2000 TABUCHI Takaaki <tab@kondara.org>
- (3.5b2-19k)
- comment out Distribution tag.
- change /etc/rc.d/init.d to /etc/init.d
- NoSource: 3
- NoSource: 4

* Sat Nov  4 2000 AYUHANA Tomonori <l@kondara.org>
- (3.5b2-17k)
- add SOURCE1* http://www.coolbrain.net/canna_dic.html
- add SOURCE2* http://www-nuclth.phys.sci.osaka-u.ac.jp/network_and_computers/net3/node71.html
- add SOURCE3*,4* http://www.tcn.zaq.ne.jp/sheepman/diary/200003.htm#22-1
- SPEC fixed ( Lincense, URL )

* Sun Oct 15 2000 TABUCHI Takaaki <tab@kondara.org>
- (3.5b2-15k)
- add Patch16: Canna35b2-default-canna.patch
- add Patch17: Canna35b2-sample-canna.patch

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<

* Wed Oct 11 2000 Toru Hoshina <tru@df-usa.com>
- added require Canna-library for mph-get upgrade.

* Tue Oct 10 2000 TABUCHI Takaaki <tab@kondara.org>
- (3.5b2-13k)
- add zip dic
- add kondara dic (gpl_canna and hokuto)
- add -q at %setup
- add patch for dics.dir (Canna35b2-dics.dir.patch)
- change BuildRoot (Canna-root to %{name}-%{version}-root)
- comment out "Copyright: NEC Corp.", please don't delete
 
* Thu Sep 21 2000 Akira Higuchi <a@kondara.org>
- use egcs

* Wed Jul 07 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- library was separated
- tiny fixes (symlinks in /etc/rc.d dir is removed)

* Wed Jul 07 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Jun 30 2000 TABUCHI Takaaki <tab@kondara.org>
- security bug fixes
- add -q at %setup

* Wed Jun 14 2000 Akira Higuchi <a@kondara.org>
- bug fixes

* Wed Mar  1 2000 Tenkou N. Hattori <tnh@kondara.org>
- chkconfiged again ;-P

* Fri Feb 25 2000 Tenkou N. Hattori <tnh@kondara.org>
- chkconfiged.

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- checnnk spec file

* Sat Jan 29 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Fri Nov 19 1999 Toru Hoshina <t@kondara.org>
- Use ja instead of ja_JP.ujis.

* Wed Oct 27 1999 Tsutomu Yasuda <tom@kondara.org>
- 1st release for Kondara
