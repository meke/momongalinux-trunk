%global momorel 2
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m

%global apidocs 1

Summary: Qt wrapper API to different RDF storage solutions
Name:    soprano
Version: 2.9.4
Release: %{momorel}m%{?dist}
Group:   System Environment/Libraries
License: LGPL
URL:     http://sourceforge.net/projects/soprano
Source0: http://downloads.sourceforge.net/soprano/%{name}-%{version}.tar.bz2
Patch1:  redland_raptor2_support.diff
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# glibc/open issues
Requires: dbus
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: dbus-devel
BuildRequires: redland-devel >= 1:1.0.15
BuildRequires: raptor2-devel >= 2.0.6
BuildRequires: qt-devel >= %{qtver}
BuildRequires: clucene-core-devel >= 2.3.3.4
BuildRequires: libiodbc-devel
%if "%{?apidocs}" == "1"
BuildRequires: doxygen
BuildRequires: graphviz
BuildRequires: qt-doc
%endif

%description
%{summary}.

%package devel
Summary: Developer files for %{name}
Group:   Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: qt-devel
Requires: pkgconfig

%description devel
%{summary}.

%package apidocs
Group: Documentation
Summary: Soprano API documentation
Requires: %{name} = %{version}
BuildArch: noarch

%description apidocs
This package includes the Soprano API documentation in HTML
format for easy browsing.

%prep
%setup -q
%patch1 -p1 -b .raptor2

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
  -DQT_DOC_DIR=`pkg-config --variable=docdir Qt` \
  -DSOPRANO_BUILD_API_DOCS:BOOL=%{?apidocs} \
  ..
popd

make %{?_smp_mflags} VERBOSE=1 -C %{_target_platform}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING* README TODO
%{_bindir}/sopranocmd
%{_bindir}/sopranod
%{_bindir}/onto2vocabularyclass
%{_libdir}/lib*.so.*
%{_libdir}/soprano
%{_datadir}/dbus-1/interfaces/org.soprano.*
%{_datadir}/soprano/
%exclude %{_datadir}/soprano/cmake

%files devel
%defattr(-,root,root,-)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/soprano.pc
%{_libdir}/pkgconfig/sopranoclient.pc
%{_libdir}/pkgconfig/sopranoindex.pc
%{_libdir}/pkgconfig/sopranoserver.pc
%{_datadir}/soprano/cmake
%{_includedir}/Soprano
%{_includedir}/soprano

%if "%{?apidocs}" == "1"
%files apidocs
%defattr(-,root,root,-)
%doc %{_target_platform}/docs/html
%endif

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.9.4-2m)
- rebuild against graphviz-2.36.0-1m

* Wed Nov  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.4-1m)
- update to 2.9.4

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.3-1m)
- update to 2.9.3

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-1m)
- update to 2.9.2

* Sat May  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1

* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0

* Sun Sep 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-3m)
- rebuild against clucene-2.3.3.4

* Sat Aug  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-2m)
- fix build failure; add BuildRequires

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Wed Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.5-1m)
- update to 2.7.5

* Thu Dec  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-2m)
- modify BuildRequires (raptor2 and redland version)

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-1m)
- update to 2.7.4

* Thu Nov  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- update to 2.7.3

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2

* Mon Sep 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Mon Aug 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-2m)
- force rebuild for korundom nepomuk module

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0

* Thu Jul  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.51-1m)
- update to 2.6.51
- change BuildRequires: from raptor-devel to raptor2-devel

* Thu Jul  7 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.6.0-3m)
- add patch redland_raptor2_support.diff for building

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-2m)
- rebuild for new GCC 4.6

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.63-2m)
- rebuild for new GCC 4.5

* Thu Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.63-1m)
- update to 2.5.63

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.63-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.63-2m)
- rebuild against qt-4.6.3-1m

* Fri Jun 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.63-1m)
- update to 2.4.63

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0.1-1m)
- update to 2.4.0.1

* Wed Feb 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Sun Feb  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.73-1m)
- update to 2.3.73

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.70-2m)
- rebuild against redland-1.0.10

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.70-1m)
- update to 2.3.70

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-2m)
- set %%global qtver 4.5.2 and BR: redland-devel = 1:1.0.8 for automatic building of STABLE_6

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.69-1m)
- update to 2.2.69

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.67-1m)
- update to 2.2.67

* Fri Jun  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.67-0.977536.1m)
- update to 2.2.67 svn977536 version

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.67-0.972745.1m)
- update to 2.2.67 svn972745 version

* Sun Apr 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.64-1m)
- update to 2.2.64

* Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.64-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.64-1m)
- update to 2.1.64

* Sat Aug 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.98-1m)
- update to 2.0.98

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-4m)
- rebuild against qt-4.4.0-1m

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-3m)
- release %%{_datadir}/dbus-1/interfaces

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-2m)
- rebuild against gcc43

* Sun Mar  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3
- update patch

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- remove rcver
- Requires: dbus and BuildRequires: dbus-devel

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.99-0.2.1m)
- import from Fedora devel and update to 1.99rc2

* Sun Dec 2 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.98.0-1
- soprano-1.98.0 (soprano 2 rc 1)
- update glibc/open patch

* Sat Nov 10 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.97.1-2
- glibc/open patch

* Sat Nov 10 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 1.97.1-1
- soprano-1.97.1 (soprano 2 beta 4)

* Fri Oct 26 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.95.0-3
- BR clucene-core-devel >= 0.9.20-2 to make sure we get a fixed package

* Fri Oct 26 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.95.0-2
- drop findclucene patch, fixed in clucene-0.9.20-2

* Tue Oct 16 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 1.95.0-1
- update to 1.95.0 (Soprano 2 beta 2)
- new BRs clucene-core-devel, raptor-devel >= 1.4.15
- now need redland-devel >= 1.0.6
- add patch to find CLucene (clucene-config.h is moved in the Fedora package)
- new Requires: pkg-config for -devel

* Wed Aug 22 2007 Rex Dieter <rdietr[AT]fedoraproject.org> 0.9.0-4
- respin (BuildID)

* Fri Aug 3 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.9.0-3
- specify LGPL version in License tag

* Sun Jul 15 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 0.9.0-2
- BR: cmake (doh)

* Wed Jun 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 0.9.0-1
- soprano-0.9.0
- first try

