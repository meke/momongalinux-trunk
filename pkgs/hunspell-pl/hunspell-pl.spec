%global momorel 4

Name: hunspell-pl
Summary: Polish hunspell dictionaries
%define upstreamid 20100118
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://sjp.pl/slownik/ort/sjp-myspell-pl-%{upstreamid}.zip
Group: Applications/Text
URL: http://www.kurnik.pl/dictionary/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+ or GPL+ or MPLv1.1
BuildArch: noarch

Requires: hunspell

%description
Polish hunspell dictionaries.

%prep
%setup -q -c hunspell-pl

%build
unzip pl_PL.zip

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_pl_PL.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100118-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100118-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100118-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20100118-1m)
- sync with Fedora 13 (0.20100118-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090315-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090315-1m)
- update to 20090315

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080407-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20080407-1m)
- import from Fedora to Momonga

* Mon Apr 07 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080407-1
- latest version

* Tue Mar 04 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080304-1
- source file name changed, update to latest version

* Wed Feb 13 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080213-1
- new version

* Wed Jan 09 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080109-1
- new version

* Sat Dec 08 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071208-1
- new version

* Thu Nov 15 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071114-1
- new version

* Fri Oct 05 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071004-1
- new version

* Mon Sep 03 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070903-1
- new version

* Thu Aug 09 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070803-1
- clarify that is tri-licensed

* Sun Jul 01 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070701-1
- latest version
- near daily updates => move to a pick it up the 1st of the month cycle

* Fri Jun 29 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070629-1
- latest version

* Fri Jun 22 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070622-1
- latest version
