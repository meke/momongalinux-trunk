%global momorel 1

%define major 2
%define minor 2
%define micro 9
%define extralevel %{nil}
%define release_name firmware-addon-dell
%define release_version %{major}.%{minor}.%{micro}%{extralevel}

# required by suse build system
# norootforbuild

%define run_unit_tests 1
%{?_without_unit_tests: %define run_unit_tests 0}
%{?_with_unit_tests:    %define run_unit_tests 1}

%define python_xml_BR %{nil}
# Some variable definitions so that we can be compatible between SUSE Build service and Fedora build system
# SUSE: fedora_version  suse_version rhel_version centos_version sles_version
# Fedora: fedora dist fc8 fc9
%if 0%{?suse_version} || 0%{?sles_version}
    %define python_xml_BR python-xml
%endif

# per fedora python packaging guidelines
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

# no debuginfo package, as there are no compiled binaries.
%define debug_package %{nil}


Name:           %{release_name}
Version:        %{release_version} 
Release:        %{momorel}m%{?dist}
Summary:        A firmware-tools plugin to handle BIOS/Firmware for Dell systems

Group:          Applications/System
License:        GPLv2+ or "OSL 2.1"
URL:            http://linux.dell.com/libsmbios/download/
Source0:        http://linux.dell.com/libsmbios/download/%{name}/%{name}-%{version}/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Dell only sells Intel-compat systems, so this package doesnt make much sense
# on, eg. PPC.  Also, we rely on libsmbios, which is only avail on Intel-compat
ExclusiveArch: x86_64 ia64 %{ix86}

# SUSE build has anal directory ownership check. RPM which owns all dirs *must*
# be installed at buildtime. This means we have to BuildRequire them, even if
# we dont really need them at build time.
%if 0%{?suse_version}
BuildRequires: firmware-tools
%endif

Requires: smbios-utils python-smbios
Requires: firmware-tools >= 0:2.0.0
Provides: firmware_inventory(system_bios)  = 0:%{version}
BuildRequires:  python-smbios >= 2.2.26, python-devel >= 2.7, firmware-tools >= 2.1.14, %{python_xml_BR}

%description
The firmware-addon-dell package provides plugins to firmware-tools which enable
BIOS updates for Dell system, plus pulls in standard inventory modules
applicable to most Dell systems.

%prep
%setup -q


%build
# this line lets us build an RPM directly from a git tarball
[ -e ./configure ] || ./autogen.sh

# fix problems when buildsystem time is out of sync. ./configure will
# fail if newly created files are older than the packaged files.
# this should normally be a no-op on proper buildsystems.
touch configure
find . -type f -newer configure -print0 | xargs -r0 touch

%configure RELEASE_MAJOR=%{major} RELEASE_MINOR=%{minor} RELEASE_MICRO=%{micro} RELEASE_EXTRA=%{extra}
make %{?_smp_mflags}

%check
%if 0%{?run_unit_tests}
make %{?_smp_mflags} check
%endif

%install
# Fedora Packaging guidelines
rm -rf $RPM_BUILD_ROOT
# SUSE Packaging rpmlint
mkdir $RPM_BUILD_ROOT

make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/firmware/dell/bios


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING-GPL COPYING-OSL
%{python_sitelib}/*
%config(noreplace) %{_sysconfdir}/firmware/firmware.d/*.conf
%{_datadir}/firmware/dell
%{_datadir}/firmware-tools/*


%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.9-1m)
- update 2.2.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.1-1m)
- sync with Fedora 13 (2.2.1-2)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.8-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.8-2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.4.8-2.1
- Rebuild for Python 2.6

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.4.8-1
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Michael E Brown <michael_e_brown at dell.com> - 1.4.7-1
- rebase to latest upstream

* Fri Aug 17 2007 Michael E Brown <michael_e_brown at dell.com> - 1.4.4-1
- rebase to latest upstream

* Wed Jul 11 2007 Michael E Brown <michael_e_brown at dell.com> - 1.3.1-1
- up2date_repo_autoconf is now obsolete. dell-*-repository files no longer
  use it.

* Sat Apr 7 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.11-1
- enhance up2date_repo_autoconf by populating default configuration file

* Fri Apr 6 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.10-1
- Couple of changes so that the dell sysid plugin work on yum 2.4.3
  prior versions didnt crash, but didnt properly substitute mirrolist 
  because the name of mirrolist var is different in 2.4.3.
- Per discussion on mailing list, convert to arch-specific pkg
- package bin/up2date_repo_autoconf only for RHEL{3,4} releases

* Fri Apr 6 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.9-1
- downgrade api needed to 2.1
- Added up2date_repo_autoconf binary
- fix changes from 1.2.7 that were accidentally reverted in 1.2.8. :(

* Fri Apr 6 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.8-1
- sysid plugin: Zero pad value for sysid up to 4 chars
- sysid plugin: Add 0x to signify that it is a hex value

* Fri Mar 30 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.7-1
- yum plugin didnt work on FC5 due to extra, unneeded import.
- dont need plugin api 2.5, 2.2 will do

* Wed Mar 28 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.6-1
- Add yum plugins for setting system ID variables. repos can use $sys_ven_id
  $sys_dev_id in their baseurl= or mirrorlist= arguments.

* Sat Mar 17 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.5-1
- Add ExcludeArch for s390
- Remove python-abi dep for RHEL3 (it was broken)
- fix sitelib path missing /lib/ dir

* Fri Mar 16 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.4-1
- Add ExcludeArch to fix problem where f-a-d was being added to ppc repo

* Thu Mar 15 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.2-1
- Trivial changes to add specific {_datadir}/firmware/dell 

* Thu Mar 15 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.1-1
- Trivial changes to make rpmlint happier

* Wed Mar 14 2007 Michael E Brown <michael_e_brown at dell.com> - 1.2.0-1
- Fedora-compliant packaging changes.
