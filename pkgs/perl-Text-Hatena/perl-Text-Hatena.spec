%global momorel 24

Summary: Text-Hatena module for perl 
Name: perl-Text-Hatena
Version: 0.20
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Languages
Source0: http://www.cpan.org/modules/by-module/Text/Text-Hatena-%{version}.tar.gz 
NoSource: 0
URL: http://www.cpan.org/modules/by-module/Text/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 1:5.8.8
BuildArch: noarch
BuildRequires: perl-Class-Data-Inheritable
BuildRequires: perl-Net-Amazon
BuildRequires: perl-Template-Toolkit
BuildRequires: perl-Text-VimColor
BuildRequires: perl-Regexp-Assemble

%description
Text-Hatena module for perl

%prep
%setup -q -n Text-Hatena-%{version} 

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot}%{perl_vendorarch}/auto -name '.packlist' | xargs rm -f

find %{buildroot}/usr -type f -print | \
	sed "s@^%{buildroot}@@g" | \
	grep -v perllocal.pod | \
	sed -e 's,\(.*/man/.*\),\1*,' | \
	grep -v "\.packlist" > Text-Hatena-%{version}-filelist
if [ "$(cat Text-Hatena-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

%clean 
rm -rf %{buildroot}

%files -f Text-Hatena-%{version}-filelist
%defattr(-,root,root)
%doc Changes README*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20-2m)
- use vendor

* Thu Mar  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20
- add BuildRequires: perl-Regexp-Assemble

* Sat Dec 30 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15-1m)
- spec file was autogenerated
