%global momorel 6

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define beta            b5

Name:           puretls
Version:        0.9
Release:        0.2.%{beta}.5jpp.%{momorel}m%{?dist}
Summary:        Java implementation of SSLv3 and TLSv1
License:        BSD
Group:          Development/Libraries
Source0:        puretls-0.9b5.tar.gz
#http://www.rtfm.com/cgi-bin/distrib.cgi?puretls-0.9b5.tar.gz
#note: rpm cannot handle the url properly since it gets the file using cgi
Url:            http://www.rtfm.com/puretls
Source1:        puretls-demo-README
# source1 is the part of the INSTALL that explains how to use the demo
Patch0:         puretls-build_xml.patch
Requires:       cryptix
Requires:       cryptix-asn1
BuildRequires:  ant
BuildRequires:  cryptix
BuildRequires:  cryptix-asn1
BuildRequires:  gnu.getopt
BuildRequires:  java-devel >= 1.4.0
%if ! %{gcj_support}
Buildarch:      noarch
%endif
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:          java-gcj-compat-devel
Requires(post):         java-gcj-compat
Requires(postun):       java-gcj-compat
%endif

%description
PureTLS is a free Java-only implementation of the SSLv3 and TLSv1
(RFC2246) protocols.

%package javadoc
Group:  Development/Libraries
Summary:        Javadoc for %{name}

%description javadoc
Javadoc for %{name}.

%package demo
Group:  Development/Libraries
Summary:        Demo for %{name}
Requires:       %{name}

%if %{gcj_support}
BuildRequires:          java-gcj-compat-devel
Requires(post):         java-gcj-compat
Requires(postun):       java-gcj-compat
%endif

%description demo
Demo for %{name}.

%prep
%setup -q -n %{name}-%{version}%{beta}
%patch0
# fix perl location
find . -type f |
    xargs grep -l "/usr/local/bin/perl5" | \
    xargs perl -pi -e "s|/usr/local/bin/perl5|%{_bindir}/perl|g;"
find . -type f |
    xargs grep -l "/usr/local/bin/perl" | \
    xargs perl -pi -e "s|/usr/local/bin/perl|%{_bindir}/perl|g;"

# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;

# remove an empty .java source file that upsets javadoc
rm src/COM/claymoresystems/sslg/CertVerifyPolicy.java

# delete test as it contains sun.* classes which will not work on different jvms
rm src/COM/claymoresystems/provider/test/DSATest.java

%build
mkdir dist

for javaver in 1.4 ; do
        export JAVA_HOME=$(find %{_jvmdir} -name "java-$javaver*" -type d | sort | tail -1)
        export CLASSPATH=$(build-classpath cryptix cryptix-asn1 gnu.getopt)
        ant -Djdk.version=$javaver clean compile
        mv build/%{name}.jar dist/%{name}$javaver.jar
done

ant javadoc

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}-ext

install -m 644 dist/%{name}1.4.jar $RPM_BUILD_ROOT%{_javadir}-ext/%{name}1.4-%{version}.%{beta}.jar

%add_jvm_extension %{name}1.4-%{version}.%{beta} %{name} 1.4.0 1.4.1 1.4.2
%add_jvm_extension %{name}1.4-%{version}.%{beta} %{name}-%{version}.%{beta} 1.4.0 1.4.1 1.4.2

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/doc/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name} 

# data
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-demo-%{version}
install -m 644 build/%{name}demo.jar $RPM_BUILD_ROOT%{_datadir}/%{name}-demo-%{version}/%{name}-demo.jar
install -m 644 *.pem $RPM_BUILD_ROOT%{_datadir}/%{name}-demo-%{version}
install -m 755 test.pl $RPM_BUILD_ROOT%{_datadir}/%{name}-demo-%{version}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/%{name}-demo-%{version}/README

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%post demo
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun demo
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(-,root,root)
%doc ChangeLog COPYRIGHT LICENSE README
%{_javadir}-*/*.jar


%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/puretls1.4-0.9.b5.jar.*
%endif

%files javadoc
%defattr(-,root,root)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%files demo
%defattr(-,root,root)
%{_datadir}/%{name}-demo-%{version}
%doc %{_datadir}/%{name}-demo-%{version}/README

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/puretls-demo.jar.*
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-0.2.b5.5jpp.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-0.2.b5.5jpp.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-0.2.b5.5jpp.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-0.2.b5.5jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-0.2.b5.5jpp.2m)
- rebuild against rpm-4.6

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-0.2.b5.5jpp.1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9-0.2.b5.5jpp.1
- Autorebuild for GCC 4.3

* Thu Mar 22 2007 Matt Wringe <mwringe@redhat.com> - 0.9-0.1.b5.5jpp.1
- Merge with latest jpp version

* Thu Mar 22 2007 Matt Wringe <mwringe@redhat.com> - 0.9-0.b5.5jpp
- Add patch to build.xml to allow detection of gnugetopt so that
  the demo subpackage jar is not empty
- Fix some rpmlint issues
- Update copyright year
- Remove vendor and distribution tags

* Thu Aug 10 2006 Matt Wringe <mwringe at redhat.com> - 0.9-0.b5.4jpp.1
- Merge with upstream version
 - Add missing requires for javadoc

* Thu Aug 10 2006 Karsten Hopp <karsten@redhat.de> 0.9-0.b5.3jpp_2fc
- Requires(post/postun): coreutils

* Mon Jul 24 2006 Matt Wringe <mwringe at redhat.com> - 0.9-0.b5.3jpp_1fc
- Merge with upstream version
- Natively compile packages

* Mon Jul 24 2006 Matt Wringe <mwringe at redhat.com> - 0.9-0.b5.3jpp
- Add conditional native compiling
- Remove test that depends on sun.* classes
- Remove additional "/" from perl path

* Mon Jul 24 2006 Fernando Nasser <fnasser@redhat.com> - 0.9-0.b5.2jpp
- First JPP 1.7 build

* Mon Oct 17 2005 Jason Corley <jason.corley@gmail.com> - 0.9-0.b5.1jpp
- 0.9b5

* Wed Aug 25 2004 Fernando Nasser <fnasser@redhat.com> - 0.9-0.b4.2jpp
- Remove support for JDK 1.3
- Rebuild with Ant 1.6.2

* Wed Jul 23 2003 Ville Skytta <ville.skytta at iki.fi> - 0.9-0.b4.1jpp
- Update to 0.9b4.
- Include JPP Java 1.4.2 extension.
- Add non-versioned javadoc dir symlink.
- Add distribution tag.
- Fix Group tags.

* Wed Mar 26 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> 0.9-0.b3.2jpp
- For jpackage-utils 1.5

* Thu Mar 28 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 0.9-0.b2.1jpp
- fixed demo jar location

* Thu Mar 28 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 0.9-0.b2.1jpp 
- fixed demo jar location
- fixed version
- section macro

* Tue Mar 26 2002 Henri Gomez <hgomez@slib.fr> 0.9b2-1jpp
- puretls 0.9b2

* Fri Jan 18 2002 Henri Gomez <hgomez@slib.fr> 20020113-2jpp
- add version to javadoc subdir

* Tue Jan 15 2002 Henri Gomez <hgomez@slib.fr> 20020113-1jpp
- snapshot 20020113
- fix perl location in demo

* Fri Dec 14 2001 Henri Gomez <hgomez@slib.fr> 20011130-1jpp
- first JPackage release


