%global momorel 5

Name:           printer-filters
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Collection of filters for various printer drivers

Group:          System Environment/Libraries
License:        Public Domain
URL:            http://fedoraproject.org/wiki/Printing

BuildArch:      noarch

# Lexmark
Requires:       pbm2l2030 c2050 c2070 lx pbm2l7k
# Canon
Requires:       cjet
# Minolta
Requires:       min12xxw
# Brother
Requires:       ptouch-driver
# Printer independent converters
Requires:       ghostscript netpbm-progs psutils pnm2ppa

%files
# No files

%description
This is a meta-package that depends on all available printer filters.
Installing it via tool that resolves dependencies, such as yum or anaconda,
ensures that all printer drivers available from foomatic package that
require external filters will work.

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jul 27 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.1-1
- Added a dependency on ptouch-driver

* Wed Jul 4 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.0-1
- Changed the name to printer-filters from printer-drivers

* Wed Jul 4 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.0-1
- Initial package
