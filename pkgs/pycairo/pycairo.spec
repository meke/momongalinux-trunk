%global momorel 8

%define py_ver %(python -c 'import sys;print(sys.version[0:3])')


Summary: Python bindings for the cairo library
Name: pycairo
Version: 1.8.8
Release: %{momorel}m%{?dist}
URL: http://cairographics.org/
Source0: http://cairographics.org/releases/%{name}-%{version}.tar.gz
NoSource: 0
License: LGPL
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.7
BuildRequires: cairo-devel
BuildRequires: python-devel
Requires: cairo

%description
Python bindings for the cairo library
 
%package devel
Summary: Libraries and headers for pycairo
Group: Development/Languages
Requires: %name = %{version}-%{release}
Requires: cairo-devel
Requires: python-devel
 
%description devel
Headers for pycairo
 
%prep
%setup -q

%build
%configure LIBS="-lpython2.7"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_libdir}/python%{py_ver}/site-packages/cairo/

%files devel
%defattr(-,root,root,-)

%{_libdir}/pkgconfig/pycairo.pc
%dir %{_includedir}/pycairo
%{_includedir}/pycairo/pycairo.h

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.8-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.8-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.8-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.8-5m)
- full rebuild for mo7 release

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.8-4m)
- add LIBS="-lpython2.6"

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.8-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.8-1m)
- update to 1.8.8

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.6-1m)
- update to 1.8.6

* Sun Jun  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.4-2m)
- rebuild against python-2.6.1-1m

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update to 1.6.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.12-2m)
- rebuild against gcc43

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.12-1m)
- update to 1.4.12

* Tue Apr 10 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-2m)
- delete libtool library

* Thu Feb  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- rebuild against python-2.5

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Mon Apr 10 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-1m)
- version 1.0.2
