%global momorel 10
%define pkg muse
%bcond_without xemacs

Name: emacs-common-muse
Version: 3.20
Release: %{momorel}m%{?dist}
Summary: Emacs Muse is an authoring and publishing environment for Emacs
Group: Applications/Editors
License: GPLv2+
URL: http://www.mwolson.org/projects/MuseMode.html           
Obsoletes: muse = 3.02.6b
Requires(pre): /sbin/install-info 
Requires(post): /sbin/install-info 

BuildArch: noarch
Source0: http://download.gna.org/muse-el/%{pkg}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs emacs-el
BuildRequires: texinfo-tex
BuildRequires: texlive-texmf-latex
%if %{with_xemacs}
BuildRequires: xemacs xemacs-devel xemacs-packages-extra
%endif

%description 
Muse is an authoring and publishing environment for (X)Emacs. It
simplifies the process of writings documents and publishing them to
various output formats. Muse uses a very simple Wiki-like format as
input.  Muse consists of two main parts: an enhanced text-mode for
authoring documents and navigating within Muse projects, and a set of
publishing styles for generating different kinds of output.

This package contains the files common to both the Emacs and XEmacs
installations of Muse. You need to install either (or both) of
emacs-%{pkg} and xemacs-%{pkg} to use Muse.

%package -n emacs-%{pkg}
Summary: Compiled Muse lisp files for Emacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs(bin) >= %{_emacs_version}
Requires: texlive-texmf-latex
Obsoletes: elisp-muse
Provides: elisp-muse

%description -n emacs-%{pkg}
This package contains the files required to use Muse with Emacs.

%package -n emacs-%{pkg}-el
Summary: Muse lisp source files for Emacs
Group: Applications/Editors
Requires: emacs-%{pkg} = %{version}-%{release}

%description -n emacs-%{pkg}-el
This package contains the source lisp files for Muse for Emacs.

%if %{with_xemacs}
%package -n xemacs-%{pkg}
Summary: Compiled Muse lisp files for XEmacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: xemacs >= %{_xemacs_version}
Requires: texlive-texmf-latex

%description -n xemacs-%{pkg}
This package contains the files required to use Muse with XEmacs.

%package -n xemacs-%{pkg}-el
Summary: Muse lisp source files for XEmacs
Group: Applications/Editors
Requires: xemacs-%{pkg} = %{version}-%{release}

%description -n xemacs-%{pkg}-el
This package contains the source lisp files for Muse for XEmacs.
%endif

%prep
%setup -q -n %{pkg}-%{version}

%build 
# Note that %{_smp_mflags} causes make to hang sometimes here.
make all doc examples experimental

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_infodir}

make install DESTDIR=%{buildroot} PREFIX=%{_usr} INFODIR=%{buildroot}%{_infodir}

# This shouldn't be necessary, but currently the package makefiles don't 
# install the compiled lisp files
(cd experimental ; make install PREFIX=%{buildroot}/%{_usr})

# Create startup file.
mkdir -p %{buildroot}/%{_emacs_sitestartdir}

cat > muse-init.el << EOF
;; Load muse-mode
(require 'muse-mode)

;; Load publishing styles
(require 'muse-html)
(require 'muse-latex)
(require 'muse-texinfo)
(require 'muse-docbook)
(require 'muse-wiki)
(require 'muse-journal)
EOF

cp muse-init.el %{buildroot}/%{_emacs_sitestartdir}

# Xemacs files
%if %{with_xemacs}
make clean
make lisp contrib autoloads experimental EMACS=xemacs SITEFLAG=-no-site-file 
make install-bin ELISPDIR=%{buildroot}/%{_xemacs_sitelispdir}/%{pkg}

# This shouldn't be necessary, but currently the package makefiles don't 
# install the compiled lisp files
(cd experimental ; make install ELISPDIR=%{buildroot}/%{_xemacs_sitelispdir}/%{pkg})

mkdir -p %{buildroot}/%{_xemacs_sitestartdir}
cp muse-init.el %{buildroot}/%{_xemacs_sitestartdir}
%endif 

# Remove info dir creating by make install
rm -f %{buildroot}/usr/share/info/dir

# Fix files we package as documentation to not be executeable
for i in examples contrib/pyblosxom scripts ; do 
  find $i -type f -perm +111 -exec chmod -x {} \;
done

# Fix file encoding
pushd examples/johnw
iconv -f ISO-8859-1 -t UTF8 muse-init.el > muse-init.el.utf8 && mv muse-init.el.utf8 muse-init.el
popd

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/muse %{_infodir}/dir 2>/dev/null || :

%preun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/muse %{_infodir}/dir 2>/dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc README NEWS AUTHORS ChangeLog ChangeLog.1 ChangeLog.2 ChangeLog.3 
%doc examples etc contrib/pyblosxom scripts
%doc %{_infodir}/muse.*

%files -n emacs-%{pkg}
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitelispdir}/%{pkg}/contrib/*.elc
%{_emacs_sitelispdir}/%{pkg}/experimental/*.elc
%{_emacs_sitestartdir}/muse-init.el
%dir %{_emacs_sitelispdir}/%{pkg}
%dir %{_emacs_sitelispdir}/%{pkg}/contrib
%dir %{_emacs_sitelispdir}/%{pkg}/experimental

%files -n emacs-%{pkg}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el
%{_emacs_sitelispdir}/%{pkg}/experimental/*.el
%{_emacs_sitelispdir}/%{pkg}/contrib/*.el

%if %{with_xemacs}
%files -n xemacs-%{pkg}
%defattr(-,root,root,-)
%{_xemacs_sitelispdir}/%{pkg}/*.elc
%{_xemacs_sitelispdir}/%{pkg}/contrib/*.elc
%{_xemacs_sitelispdir}/%{pkg}/experimental/*.elc
%{_xemacs_sitestartdir}/muse-init.el
%dir %{_xemacs_sitelispdir}/%{pkg}
%dir %{_xemacs_sitelispdir}/%{pkg}/contrib
%dir %{_xemacs_sitelispdir}/%{pkg}/experimental

%files -n xemacs-%{pkg}-el
%defattr(-,root,root,-)
%{_xemacs_sitelispdir}/%{pkg}/*.el
%{_xemacs_sitelispdir}/%{pkg}/contrib/*.el
%{_xemacs_sitelispdir}/%{pkg}/experimental/*.el
%endif

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-10m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-9m)
- rename the package name
- reimport from fedora

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.20-8m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-7m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.20-6m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.20-4m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.20-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.20-2m)
- merge muse-emacs to elisp-muse
- kill muse-xemacs

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.20-1m)
- update to 3.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.12-13m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.12-12m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-11m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-10m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-9m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-8m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-6m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-4m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.12-2m)
- rebuild against gcc43

* Tue Feb 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-1m)
- update to 3.12

* Sat Aug 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.11-1m)
- update to 3.11
- switch license to GPLv3

* Mon Jul  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.03-1m)
- update to 3.03

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.93-3m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.02.93-2m)
- rebuild against emacs-22.1

* Sat Apr 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.93-1m)
- initial packaging
