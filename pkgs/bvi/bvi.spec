%global momorel 8

Summary: binary editor with vi-like keybindings
Name: bvi
Version: 1.3.2
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.src.tar.gz 
NoSource: 0
Patch0: bvi-1.3.1-getcmdstr.patch
Patch1: bvi-1.3.0-path.patch
License: GPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: Applications/Editors
URL: http://bvi.sourceforge.net/
NoSource: 0

%description
  bvi is a fast, lightweight binary editor based on vi. 
If you are familiar with vi, this is the best binary editor. 

%prep
%setup -q
%patch0 -p1 -b .patch0-org
%patch1 -p1 -b .patch1-org

%build
CFLAGS="%{optflags} -DHAVE_LOCALE_H"	%configure \
--datadir=%{_datadir}/%{name} --enable-FEATURE=yes
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
%makeinstall prefix=%{buildroot}%{_prefix} datadir=%{buildroot}%{_datadir}/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES COPYING CREDITS README html
%{_bindir}/bvi
%{_bindir}/bvedit
%{_bindir}/bmore
%{_bindir}/bview
%{_libdir}/bmore.help
%{_mandir}/man1/bvi.1*
%{_mandir}/man1/bmore.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-2m)
- %%NoSource -> NoSource

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Wed Nov  5 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.3.1-2m)
- use %%{momorel}

* Wed Aug 14 2002 WATABE Toyokazu <toy2@nidone.org>
- (1.3.1-1k)
- update to 1.3.1

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.3.0-6k)
- nigittenu

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.3.0-3k)
- %configure and %makeinstall macros

* Wed Oct 25 2000 Toyokazu WATABE <toy2@kondara.org> 1.3.0-1k
- use macros.
- add bvedir and bview.
- move bmore.help to %{_datadir}/%{name}
- add bvi-1.3.0-path.patch

* Mon Jul 17 2000 AYUHANA Tomonori <l@kondara.org>
- (1.3.0beta-2k)
- use %makeinstall :-(
- add * at man pages

* Fri Jul 14 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- update to 1.3.0beta
- add bvi-1.3.0beta-getcmdstr.patch
- fixed %description English
- SPEC fixed ( URL )
- add -q at %setup
- use %configure

* Thu May  4 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, description )

* Thu Apr 27 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- 1st release

