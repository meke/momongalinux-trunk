%global momorel 1

Summary: The basic directory layout for a Linux system
Name: filesystem
Version: 3.2
Release: %{momorel}m%{?dist}
License: Public Domain
URL: https://fedorahosted.org/filesystem
Group: System Environment/Base
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Raw source1 URL: https://fedorahosted.org/filesystem/browser/lang-exceptions?format=raw
Source1: https://fedorahosted.org/filesystem/browser/lang-exceptions
Source2: iso_639.sed
Source3: iso_3166.sed
Source4: filesystem.conf
Requires(Pre): setup >= 2.5.4-1
BuildRequires: iso-codes

%description
The filesystem package is one of the basic packages that is installed
on a Linux system. Filesystem contains the basic directory layout
for a Linux operating system, including the correct permissions for
the directories.

%prep
rm -f $RPM_BUILD_DIR/filelist

%build

%install
rm -rf %{buildroot}
mkdir %{buildroot}
install -p -c -m755 %SOURCE2 %{buildroot}/iso_639.sed
install -p -c -m755 %SOURCE3 %{buildroot}/iso_3166.sed
mkdir -p %{buildroot}/etc/tmpfiles.d
install -p -c -m644 %SOURCE4 %{buildroot}/etc/tmpfiles.d/filesystem.conf

cd %{buildroot}

mkdir -p boot dev \
        etc/{X11/{applnk,fontpath.d},xdg/autostart,opt,pm/{config.d,power.d,sleep.d},xinetd.d,skel,sysconfig,pki,bash_completion.d} \
        home media mnt opt proc root run srv sys tmp \
        usr/{bin,games,include,%{_lib}/{games,sse2,tls,X11,pm-utils/{module.d,power.d,sleep.d}},lib/{debug/usr,games,locale,modules,sse2},libexec,local/{bin,etc,games,lib,%{_lib},sbin,src,share/{applications,man/man{1,2,3,4,5,6,7,8,9,n,1x,2x,3x,4x,5x,6x,7x,8x,9x},info},libexec,include,},sbin,share/{aclocal,appdata,applications,augeas/lenses,backgrounds,desktop-directories,dict,doc,empty,games,ghostscript/conf.d,gnome,icons,idl,info,man/man{1,2,3,4,5,6,7,8,9,n,1x,2x,3x,4x,5x,6x,7x,8x,9x,0p,1p,3p},mime-info,misc,omf,pixmaps,sounds,themes,xsessions,X11},src,src/kernels,src/debug} \
        var/{adm,empty,gopher,lib/{games,misc,rpm-state},local,log,nis,preserve,spool/{mail,lpd},tmp,db,cache,opt,games,yp}

#do not create the symlink atm.
#ln -snf etc/sysconfig etc/default
ln -snf ../var/tmp usr/tmp
ln -snf spool/mail var/mail
ln -snf usr/bin bin
ln -snf usr/sbin sbin
ln -snf usr/lib lib
ln -snf usr/%{_lib} %{_lib}
ln -snf ../run var/run
ln -snf ../run/lock var/lock
ln -snf usr/bin usr/lib/debug/bin
ln -snf usr/lib usr/lib/debug/lib
ln -snf usr/%{_lib} usr/lib/debug/%{_lib}
ln -snf ../.dwz usr/lib/debug/usr/.dwz
ln -snf usr/sbin usr/lib/debug/sbin

sed -n -f %{buildroot}/iso_639.sed /usr/share/xml/iso-codes/iso_639.xml \
  >%{buildroot}/iso_639.tab
sed -n -f %{buildroot}/iso_3166.sed /usr/share/xml/iso-codes/iso_3166.xml \
  >%{buildroot}/iso_3166.tab

grep -v "^$" %{buildroot}/iso_639.tab | grep -v "^#" | while read a b c d ; do
    [[ "$d" =~ "^Reserved" ]] && continue
    [[ "$d" =~ "^No linguistic" ]] && continue

    locale=$c
    if [ "$locale" = "XX" ]; then
        locale=$b
    fi
    echo "%lang(${locale})	/usr/share/locale/${locale}" >> $RPM_BUILD_DIR/filelist
    echo "%lang(${locale}) %ghost %config(missingok) /usr/share/man/${locale}" >>$RPM_BUILD_DIR/filelist
done
cat %{SOURCE1} | grep -v "^#" | grep -v "^$" | while read loc ; do
    locale=$loc
    locality=
    special=
    [[ "$locale" =~ "@" ]] && locale=${locale%%%%@*}
    [[ "$locale" =~ "_" ]] && locality=${locale##*_}
    [[ "$locality" =~ "." ]] && locality=${locality%%%%.*}
    [[ "$loc" =~ "_" ]] || [[ "$loc" =~ "@" ]] || special=$loc

    # If the locality is not official, skip it
    if [ -n "$locality" ]; then
        grep -q "^$locality" %{buildroot}/iso_3166.tab || continue
    fi
    # If the locale is not official and not special, skip it
    if [ -z "$special" ]; then
        egrep -q "[[:space:]]${locale%%_*}[[:space:]]" \
           %{buildroot}/iso_639.tab || continue
    fi
    echo "%lang(${locale})	/usr/share/locale/${loc}" >> $RPM_BUILD_DIR/filelist
    echo "%lang(${locale})  %ghost %config(missingok) /usr/share/man/${loc}" >> $RPM_BUILD_DIR/filelist
done

rm -f %{buildroot}/iso_639.tab
rm -f %{buildroot}/iso_639.sed
rm -f %{buildroot}/iso_3166.tab
rm -f %{buildroot}/iso_3166.sed

cat $RPM_BUILD_DIR/filelist | grep "locale" | while read a b ; do
    mkdir -p -m 755 %{buildroot}/$b/LC_MESSAGES
done

cat $RPM_BUILD_DIR/filelist | grep "/share/man" | while read a b c d; do
    mkdir -p -m 755 %{buildroot}/$d/man{1,2,3,4,5,6,7,8,9,n,1x,2x,3x,4x,5x,6x,7x,8x,9x,0p,1p,3p}
done

for i in man{1,2,3,4,5,6,7,8,9,n,1x,2x,3x,4x,5x,6x,7x,8x,9x,0p,1p,3p}; do
   echo "/usr/share/man/$i" >>$RPM_BUILD_DIR/filelist
done

%clean
rm -rf %{buildroot}

%pretrans -p <lua>
--# If we are running in pretrans in a fresh root, there is no /usr and
--# symlinks. We cannot be sure, to be the very first rpm in the
--# transaction list. Let's create the needed base directories and symlinks
--# here, to place the files from other packages in the right locations.
--# When our rpm is unpacked by cpio, it will set all permissions and modes
--# later.
posix.mkdir("/usr")
posix.mkdir("/usr/bin")
posix.mkdir("/usr/sbin")
posix.mkdir("/usr/lib")
posix.mkdir("/usr/lib/debug")
posix.mkdir("/usr/lib/debug/usr/")
posix.mkdir("/usr/%{_lib}")
posix.symlink("usr/bin", "/bin")
posix.symlink("usr/sbin", "/sbin")
posix.symlink("usr/lib", "/lib")
posix.symlink("usr/bin", "/usr/lib/debug/bin")
posix.symlink("usr/lib", "/usr/lib/debug/lib")
posix.symlink("usr/%{_lib}", "/usr/lib/debug/%{_lib}")
posix.symlink("../.dwz", "/usr/lib/debug/usr/.dwz")
posix.symlink("usr/sbin", "/usr/lib/debug/sbin")
posix.symlink("usr/%{_lib}", "/%{_lib}")
posix.mkdir("/run")
posix.symlink("../run", "/var/run")
posix.symlink("../run/lock", "/var/lock")
return 0

%files -f filelist
%defattr(0755,root,root,-)
%dir %attr(555,root,root) /
/bin
%attr(555,root,root) /boot
/dev
%dir /etc
/etc/X11
/etc/xdg
/etc/opt
/etc/pm
/etc/xinetd.d
/etc/skel
/etc/sysconfig
/etc/pki
/etc/bash_completion.d/
/home
/lib
%ifarch x86_64 ppc64 sparc64 s390x aarch64 ppc64le
/%{_lib}
%endif
/media
%dir /mnt
%dir /opt
%attr(555,root,root) /proc
%attr(550,root,root) /root
/run
/sbin
/srv
%attr(555,root,root) /sys
%attr(1777,root,root) /tmp
%dir /usr
%attr(555,root,root) /usr/bin
/usr/games
/usr/include
%dir %attr(555,root,root) /usr/lib
%dir /usr/lib/debug
%ghost /usr/lib/debug/bin
%ghost /usr/lib/debug/lib
%ghost /usr/lib/debug/%{_lib}
%ghost /usr/lib/debug/usr
%ghost /usr/lib/debug/usr/.dwz
%ghost /usr/lib/debug/sbin
%attr(555,root,root) /usr/lib/games
%attr(555,root,root) /usr/lib/sse2
%ifarch x86_64 ppc64 sparc64 s390x aarch64 ppc64le
%attr(555,root,root) /usr/%{_lib}
%else
%attr(555,root,root) /usr/lib/tls
%attr(555,root,root) /usr/lib/X11
%attr(555,root,root) /usr/lib/pm-utils
%endif
/usr/libexec
/usr/local
%attr(555,root,root) /usr/sbin
%dir /usr/share
/usr/share/aclocal
/usr/share/appdata
/usr/share/applications
/usr/share/augeas
/usr/share/backgrounds
/usr/share/desktop-directories
/usr/share/dict
/usr/share/doc
%attr(555,root,root) %dir /usr/share/empty
/usr/share/games
/usr/share/ghostscript
/usr/share/gnome
/usr/share/icons
/usr/share/idl
/usr/share/info
%dir /usr/share/locale
%dir /usr/share/man
/usr/share/mime-info
/usr/share/misc
/usr/share/omf
/usr/share/pixmaps
/usr/share/sounds
/usr/share/themes
/usr/share/xsessions
/usr/share/X11
/usr/src
/usr/tmp
%dir /var
/var/adm
/var/cache
/var/db
/var/empty
/var/games
/var/gopher
/var/lib
/var/local
%ghost /var/lock
/var/log
/var/mail
/var/nis
/var/opt
/var/preserve
%ghost /var/run
%dir /var/spool
%attr(755,root,root) /var/spool/lpd
%attr(775,root,mail) /var/spool/mail
%attr(1777,root,root) /var/tmp
/var/yp
%config /etc/tmpfiles.d/filesystem.conf

%changelog
* Mon May 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-1m)
- update 3.2

* Mon Jun 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.40-1m)
- update 2.4.40

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.21-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.21-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.21-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.21-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.21-3m)
- modify for iso-codes-3.10
- add %%{_datadir}/locale/*

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.21-2m)
- good-bye %%{_datadir}/locale/*

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.21-1m)
- sync with Fedora 11 (2.4.21-1)
-- add /usr/share/backgrounds, /usr/share/ghostscript/{,conf.d}

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.19-1m)
- update for fontpackages-filessytem
- drop conflicts dir /etc/fonts, /etc/fonts/conf.d, /usr/share/fonts
  with fontpackages-filessytem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.13-2m)
- rebuild against rpm-4.6

* Sun Apr 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.13-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.6-3m)
- rebuild against gcc43

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-2m)
- add /usr/share/theme

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-1m)
- sync fedora

* Sun Feb 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.7-3m)
- remove /usr/lib/locale, it's already provided by glibc-common

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.7-2m)
- revised installdir
- sync with fc-devel

* Tue Jan 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.7-1m)
- sync with fc-devel

* Fri Feb 04 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.0-2m)
- readd /usr/share/config-sample
- /usr/X11R6/lib/X11, /usr/X11R6/lib/X11/app-defaults and /etc/X11/app-defaults
  is provided by xorg-x11-libs now

* Wed Jan 26 2005 Toru Hoshina <t@momonga-linux.org>
- (2.3.0-1m)
- import From FC3.

* Fri Sep 10 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.4-2m)
  added /etc/X11/app-defaults and /usr/X11R6/lib/X11/app-defaults

* Thu Jul 29 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.4-1m)
- upgrade and import from FC2

* Wed Nov  5 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.1.6-15m)
- use %%{momorel}

* Fri Jan 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.1.6-14k)
- obey FHS (move /usr/dict to /usr/share/dict)

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (2.1.6-12k)
- still need /var/ftp...

* Sat Nov 24 2001 Toru Hoshina <t@kondara.org>
- (2.1.6-10k)
- lock... is it owned by uucp, right?

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.1.6-8k)
- nigittenu

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (2.1.6-4k)
- lock...

* Mon Oct 22 2001 Toru Hoshina <t@kondara.org>
- (2.1.6-2k)
- merge from rawhide [filesystem-2.1.6-2].
- add /usr/share/config-sample for dotfiles.

* Mon Aug 20 2001 Bill Nottingham <notting@redhat.com>
- %%ghost /mnt/cdrom, /mnt/floppy (fixes #52046)

* Wed Aug 15 2001 Bill Nottingham <notting@redhat.com>
- add /usr/X11R6/share (#51830)

* Mon Aug 13 2001 Bill Nottingham <notting@redhat.com>
- prereq a particular version of the setup package

* Thu Aug  9 2001 Bill Nottingham <notting@redhat.com>
- remove /mnt/cdrom, /mnt/floppy (updfstab will create them if needed)
- make it noarch again

* Wed Aug  8 2001 Bill Nottingham <notting@redhat.com>
- /var/lock needs to be root.lock, not lock.lock

* Mon Aug  6 2001 Jeff Johnson <jbj@redhat.com>
- lock.lock ownership, 0775 permissions, for /var/lock.

* Tue Jul 17 2001 Bill Nottingham <notting@redhat.com>
- add /etc/sysconfig, /var/yp, /usr/share/pixmaps

* Tue Jul 10 2001 Bill Nottingham <notting@redhat.com>
- add stuff under /etc/X11
- remove extraneous /usr/X11R6/doc (#47490)

* Mon Jun 25 2001 Bill Nottingham <notting@redhat.com>
- don't conflict with rpm

* Fri Jun 22 2001 Bill Nottingham <notting@redhat.com>
- don't own /var/lib/rpm (#43315)
- add some stuff in /usr/local (#36522)

* Thu Jun 21 2001 Bill Nottingham <notting@redhat.com>
- add /initrd

* Thu Jun 07 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- remove noarch
- do not include /mnt/cdrom and /mnt/floppy for s390/s390x

* Mon Apr 16 2001 Bill Nottingham <notting@redhat.com>
- take the group write off of /var/lock

* Fri Jul 21 2000 Bill Nottingham <notting@redhat.com>
- add /usr/share/empty

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jun 28 2000 Preston Brown <pbrown@redhat.com>
- remove /usr/doc

* Thu Jun 22 2000 Preston Brown <pbrown@redhat.com>
- remove /usr/info

* Sun Jun 19 2000 Bill Nottingham <notting@redhat.com>
- remove /usr/man

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- /var/spool/lpd should have normal perms (#12272)

* Tue Jun  6 2000 Bill Nottingham <notting@redhat.com>
- add /etc/skel

* Thu Jun 01 2000 Preston Brown <pbrown@redhat.com>
- add /var/spool/lpd to filesystem, owned by user/group lp, tight permissions

* Tue May 23 2000 Trond Eivind Glomsrod <teg@redhat.com>
- Added /etc/xinetd.d

* Mon May 15 2000 Preston Brown <pbrown@redhat.com>
- /etc/opt, /usr/share/{info,man/man*,misc,doc} (FHS 2.1)
- added /var/games.  Data should move from /var/lib/games to there (FHS 2.1)
- bump version up to 2.0 already!

* Thu Apr 13 2000 Jakub Jelinek <jakub@redhat.com>
- removed /var/state, added /var/opt, /var/mail for FHS 2.1 compliance

* Mon Aug 28 1999 Preston Brown <pbrown@redhat.com>
- added /opt, /var/state, /var/cache for FHS compliance (#3966)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Sun Jan 17 1999 Jeff Johnson <jbj@redhat.com>
- don't carry X11R6.1 as directory on sparc.
- /var/tmp/build root (#811)

* Wed Jan 13 1999 Preston Brown <pbrown@redhat.com>
- font directory didn't belong, which I previously misunderstood.  removed.

* Fri Nov 13 1998 Preston Brown <pbrown@redhat.com>
- /usr/share/fonts/default added.

* Fri Oct  9 1998 Bill Nottingham <notting@redhat.com>
- put /mnt/cdrom back in

* Wed Oct  7 1998 Bill Nottingham <notting@redhat.com>
- Changed /root to 0750

* Wed Aug 05 1998 Erik Troan <ewt@redhat.com>
- added /var/db
- set attributes in the spec file; don't depend on the ones in the cpio
  archive
- use a tarball instead of a cpioball

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Sep 09 1997 Erik Troan <ewt@redhat.com>
- made a noarch package

* Wed Jul 09 1997 Erik Troan <ewt@redhat.com>
- added /

* Wed Apr 16 1997 Erik Troan <ewt@redhat.com>
- Changed /proc to 555
- Removed /var/spool/mqueue (which is owned by sendmail)
