%global momorel 2

Summary: TNEF is a program for unpacking MIME attachments of type "application/ms-tnef". This is a Microsoft only attachment.
Name: tnef
Version: 1.4.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
URL: http://tnef.sourceforge.net/
Source0: http://heanet.dl.sourceforge.net/sourceforge/tnef/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

NoSource: 0

%description
TNEF is a program for unpacking MIME attachments of type
"application/ms-tnef". This is a Microsoft only attachment.

Due to the proliferation of Microsoft Outlook and Exchange mail servers,
more and more mail is encapsulated into this format.

The TNEF program allows one to unpack the attachments which were
encapsulated into the TNEF attachment.  Thus alleviating the need to use
Microsoft Outlook to view the attachment.

TNEF is mainly tested and used on GNU/Linux and CYGWIN systems.  It
'should' work on other UNIX and UNIX-like systems.

See the file COPYING for copyright and warranty information.

See the file INSTALL for instructions on installing TNEF.  The short form
for installation is the standard:


%prep
%setup -q

%build
%configure
%make

%install
rm -rf $RPM_BUILD_ROOT
make "DESTDIR=${RPM_BUILD_ROOT}" install
install -d %{buildroot}%{_mandir}/man1
install doc/tnef.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING ChangeLog AUTHORS NEWS TODO BUGS
%{_bindir}/tnef
%{_mandir}/man1/tnef.1*



%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-2m)
- rebuild for new GCC 4.6

* Sun Apr 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-1m)
- update to 1.4.7

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 21 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3.1-3m)
- rebuild against gcc43

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.3.1-2m)
- revised spec for rpm 4.2.

* Tue Jan 27 2004  <yohgaki@momonga-linux.org> - 
- Initial build.

