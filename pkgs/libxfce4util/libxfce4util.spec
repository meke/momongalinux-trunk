%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0

Name: 		libxfce4util
Version: 	4.11.0
Release:	%{momorel}m%{?dist}
Summary: 	Utility library for the XFce4 desktop environment

Group: 		Development/Libraries
License:	BSD
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: 	gtk2-devel
BuildRequires: 	glib2-devel
BuildRequires:  libxml2-devel >= 2.7.2
BuildRequires:  pkgconfig
BuildRequires:  gtk-doc

%description
Basic utility non-GUI functions for XFce4.

%package devel
Summary:	developpment tools for libxfce4util library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
Static libraries and header files for the libxfce4util library.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS
%{_libdir}/lib*.so.*
%{_sbindir}/*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/xfce4
%{_includedir}/xfce4/libxfce4util
%{_datadir}/locale/*/*/*
%{_datadir}/gtk-doc/html/libxfce4util/

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.1-1m)
- update to 4.11.1

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-3m)
- change Source0 URI

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.1-2m)
- rebuild for glib 2.33.2

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.1-1m)
- update

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.4-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-2m)
- fix unneed patch0

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3.2-2m)
- delete libtool library
- rebuild against gtk+-2.10.3 glib-2.12.3

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3.2-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90
- add BuildRequires:  gtk-doc
- delete %%{_datadir}/xfce4/i18n/nls.alias
- add %%{_sbindir}/*
- add %%{_datadir}/gtk-doc
- TODO: add --enable-gtk-doc at configure
 
* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)
- minor bugfixes

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.5-1m)
- version update to 4.0.5.

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-4m)
- rebuild against for gtk+-2.4.0

* Sun Apr 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.4-3m)
- more restrict BuildRequires and Requires

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-2m)
- rebuild against for libxml2-2.6.8
- rebuild against for glib-2.4.0

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.4-1m)

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- version up to 4.0.3

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-1m)
- version up to 4.0.2

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-1m)
- version up to 4.0.1

* Fri Sep 26 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (4.0.0-1m)
- version up

* Sat Sep 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.99.4-1m)
- XFce4 RC4 release
