%global momorel 1
%global date 20131207
%global gstver 1.0
%global simple_name crystalhd

Summary: Broadcom Crystal HD device interface library
Name: lib%{simple_name}
Version: 3.10.0
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: System Environment/Libraries
URL: ttp://www.broadcom.com/support/crystal_hd/
# This tarball and README are inside the above zip file...
# Patch generated from http://git.linuxtv.org/jarod/crystalhd.git
Source0: %{simple_name}-%{date}.tar.xz
Source1: README_07032010
# LICENSE file is copy-n-pasted from http://www.broadcom.com/support/crystal_hd/
Source2: LICENSE
Source3: %{simple_name}-dkms.conf
Patch0: %{name}-nosse2.patch
# https://patchwork2.kernel.org/patch/2247431/
Patch1: %{simple_name}-gst-Port-to-GStreamer-1.0-API.patch
# http://ubuntuforums.org/showthread.php?t=2160032
Patch2: http://www.alexboehm.de/wetab/devinitFix.patch
Patch10: %{name}-%{date}-unset-libdir.patch
Patch11: %{name}-%{date}-optflags.patch
Patch12: %{name}-%{date}-bootstrap.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{simple_name}-firmware
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gstreamer1-devel
BuildRequires: gstreamer1-plugins-base-devel
BuildRequires: libtool
BuildRequires: sed

%description
The %{name} library provides userspace access to Broadcom Crystal HD
video decoder devices. The device supports hardware decoding of MPEG-2,
h.264 and VC1 video codecs, up to 1080p at 40fps for the first-generation
bcm970012 hardware, and up to 1080p at 60fps for the second-generation
bcm970015 hardware.

%package devel
Summary: Header files and development libraries from %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on %{name}.

%package -n %{simple_name}-firmware
Summary: Firmware for the Broadcom Crystal HD video decoder
License: "Redistributable, no modification permitted"
Group: System Environment/Kernel
BuildArch: noarch

%description -n %{simple_name}-firmware
Firmwares for the Broadcom Crystal HD (bcm970012 and bcm970015)
video decoders.

%package -n gstreamer-plugin-%{simple_name}
Summary: Gstreamer %{simple_name} decoder plugin
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: gstreamer1-plugins-base

%description -n gstreamer-plugin-%{simple_name}
Gstreamer %{simple_name} decoder plugin.

%package -n %{simple_name}-dkms
Summary: Crystal HD kernel module using dkms
Group: System Environment/Kernel
Requires: dkms
Requires: %{name} = %{version}-%{release}

%description -n %{simple_name}-dkms
This package provides dkms support for Crystal HD kernel module

%prep
%setup -q -n %{simple_name}-%{date}

%ifnarch %{ix86} ia64 x86_64
%patch0 -p1 -b .nosse2
sed -i -e 's|-msse2||' linux_lib/%{name}/Makefile
%endif

%patch1 -p1 -b .gst1
%patch2 -p0 -b .devinit

%patch10 -p1 -b .libdir
%patch11 -p1 -b .optflags
%patch12 -p1 -b .nocheck-libs

# preparing docs
install -m 644 %{SOURCE1} .
install -m 644 %{SOURCE2} .

# preparing dkms
pushd driver/linux
autoconf
popd

%build
pushd linux_lib/%{name}
make %{?_smp_mflags} \
	LIBDIR=%{_libdir} \
	RPM_OPT_FLAGS="%{optflags}"
popd

pushd filters/gst/gst-plugin
./autogen.sh
%configure
make %{?_smp_mflags} \
	CFLAGS="%{optflags} -I%{_builddir}/%{buildsubdir}/include -I%{_builddir}/%{buildsubdir}/linux_lib/%{name}" \
	BCMDEC_LDFLAGS="-L%{_builddir}/%{buildsubdir}/linux_lib/%{name} -l%{simple_name}"
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
pushd linux_lib/%{name}
make install LIBDIR=%{_libdir} DESTDIR=%{buildroot}
popd

pushd filters/gst/gst-plugin
make install DESTDIR=%{buildroot}
popd

# get rid of *.*a file
rm -f %{buildroot}%{_libdir}/gstreamer-1.0/*.*a

# install udev rules file
mkdir -p %{buildroot}%{_sysconfdir}/udev/rules.d
install -m 644 driver/linux/20-%{simple_name}.rules %{buildroot}%{_sysconfdir}/udev/rules.d/

# install firmawares
mkdir -p %{buildroot}/lib/firmware
install -m 644 firmware/fwbin/70012/bcmFilePlayFw.bin %{buildroot}/lib/firmware/
install -m 644 firmware/fwbin/70012/bcm70012fw.bin %{buildroot}/lib/firmware/
install -m 644 firmware/fwbin/70015/bcm70015fw.bin %{buildroot}/lib/firmware/

# prepare for dkms
mkdir -p %{buildroot}/usr/src
cp -rf driver/linux %{buildroot}/usr/src/%{simple_name}-%{version}
cp -rf include/*/*.h %{buildroot}/usr/src/%{simple_name}-%{version}/
cp -rf include/*.h %{buildroot}/usr/src/%{simple_name}-%{version}/
sed --in-place -e 's,$(shell uname -r),$(KERNELRELEASE),g'\
    %{buildroot}/usr/src/%{simple_name}-%{version}/Makefile.in
cat %{SOURCE3} \
    | sed -e 's,@@VERSION@@,%{version},g' \
    > %{buildroot}/usr/src/%{simple_name}-%{version}/dkms.conf

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post -n %{simple_name}-dkms
%{_sbindir}/dkms add -m %{simple_name} -v %{version} || :
%{_sbindir}/dkms build -m %{simple_name} -v %{version} || :
%{_sbindir}/dkms install -m %{simple_name} -v %{version} || :

%preun -n %{simple_name}-dkms
%{_sbindir}/dkms remove -m %{simple_name} -v %{version} --all || :

%files
%defattr(-,root,root)
%doc LICENSE README_07032010
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/%{name}.so

%files -n %{simple_name}-firmware
%defattr(-,root,root)
%doc LICENSE
%config(noreplace) %{_sysconfdir}/udev/rules.d/20-%{simple_name}.rules
/lib/firmware/bcmFilePlayFw.bin
/lib/firmware/bcm70012fw.bin
/lib/firmware/bcm70015fw.bin

%files -n gstreamer-plugin-crystalhd
%defattr(-,root,root)
%{_libdir}/gstreamer-%{gstver}/*.so

%files -n %{simple_name}-dkms
%doc LICENSE README_07032010
/usr/src/%{simple_name}-%{version}

%changelog
* Sat Dec  7 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.0-1m)
- initial package for Crystal HD: Broadcom BCM70012 and BCM70015
- import some patches and parts of spec from fedora
