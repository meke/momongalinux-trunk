%global momorel 7
%global majorminor 0.10

%global srcname gst-plugins-bad

Name: gstreamer-plugins-bad
Version: 0.10.23
Release: %{momorel}m%{?dist}
Summary: GStreamer streaming media framework bad plug-ins

Group: Applications/Multimedia
License: LGPL
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2 
NoSource: 0

Requires: gstreamer

# for upstream patches
BuildRequires: autoconf

# plugins
BuildRequires: bzip2-devel
BuildRequires: celt-devel
BuildRequires: dirac-devel
BuildRequires: exempi-devel
BuildRequires: faac-devel
BuildRequires: faad2-devel >= 2.7
BuildRequires: fftw-devel
BuildRequires: flac-devel
BuildRequires: gdbm-devel
BuildRequires: glib2-devel
BuildRequires: gnutls-devel
BuildRequires: gsm-devel
BuildRequires: gstreamer-devel >= 0.10.33
BuildRequires: gstreamer-plugins-base-devel >= 0.10.33
BuildRequires: jack-devel
BuildRequires: jasper-devel
BuildRequires: krb5-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXt-devel
BuildRequires: libass-devel
BuildRequires: libasyncns-devel
BuildRequires: libcap-devel
BuildRequires: libcdaudio-devel
BuildRequires: libdc1394-devel
BuildRequires: libdvdnav-devel
BuildRequires: libdvdread-devel
BuildRequires: libexif-devel
BuildRequires: libgpg-error-devel
BuildRequires: libiptcdata-devel
BuildRequires: libjpeg-devel
BuildRequires: libkate-devel >= 0.3.7-3m
BuildRequires: libmms-devel
BuildRequires: libmodplug-devel >= 0.8.8.4
BuildRequires: libmpcdec-devel
BuildRequires: libmusicbrainz-devel
BuildRequires: libofa-devel
BuildRequires: libogg-devel
BuildRequires: liboil-devel
BuildRequires: libsamplerate-devel
BuildRequires: libsndfile-devel
BuildRequires: libtasn1-devel
BuildRequires: libvdpau-devel
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: mjpegtools-devel >= 2.1.0
BuildRequires: nas-devel
BuildRequires: neon-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: opus-devel
BuildRequires: pakchois-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: x264-devel >= 0.0.1195
BuildRequires: schroedinger-devel
BuildRequires: slv2-devel
BuildRequires: libvpx-devel >= 1.0.0
BuildRequires: orc-devel
BuildRequires: flite-devel
BuildRequires: opencv-devel >= 2.4.2
# documentation
BuildRequires:  gtk-doc

Requires: gstreamer-plugins-good >= 0.10.16

%description
GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par
compared to the rest. They might be close to being good quality, but
they're missing something - be it a good code review, some
documentation, a set of tests, a real live maintainer, or some actual
wide use.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure \
    --disable-static \
    --disable-gtk-doc \
    --disable-flite \
    --enable-experimental \
    --disable-divx \
    --disable-xvid \
    OPENCV_CFLAGS="`pkg-config --cflags opencv`" \
    OPENCV_LIBS="`pkg-config --libs opencv`" \
    LDFLAGS="$LDFLAGS -fprofile-arcs -ftest-coverage"

#    --disable-ladspa \

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README RELEASE REQUIREMENTS
# plugins
%{_libdir}/gstreamer-%{majorminor}/libgstaiff.so
%{_libdir}/gstreamer-%{majorminor}/libgstapexsink.so
%{_libdir}/gstreamer-%{majorminor}/libgstassrender.so
%{_libdir}/gstreamer-%{majorminor}/libgstbayer.so
%{_libdir}/gstreamer-%{majorminor}/libgstbz2.so
%{_libdir}/gstreamer-%{majorminor}/libgstcdaudio.so
%{_libdir}/gstreamer-%{majorminor}/libgstcdxaparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstcelt.so
%{_libdir}/gstreamer-%{majorminor}/libgstdccp.so
%{_libdir}/gstreamer-%{majorminor}/libgstdirac.so
%{_libdir}/gstreamer-%{majorminor}/libgstdtsdec.so
%{_libdir}/gstreamer-%{majorminor}/libgstdvb.so
%{_libdir}/gstreamer-%{majorminor}/libgstdvdspu.so
%{_libdir}/gstreamer-%{majorminor}/libgstfaac.so
%{_libdir}/gstreamer-%{majorminor}/libgstfaad.so
%{_libdir}/gstreamer-%{majorminor}/libgstfbdevsink.so
%{_libdir}/gstreamer-%{majorminor}/libgstfestival.so
%{_libdir}/gstreamer-%{majorminor}/libgstfreeze.so
%{_libdir}/gstreamer-%{majorminor}/libgstgsm.so
%{_libdir}/gstreamer-%{majorminor}/libgsth264parse.so
%{_libdir}/gstreamer-%{majorminor}/libgstjp2k.so
%{_libdir}/gstreamer-%{majorminor}/libgstkate.so
%{_libdir}/gstreamer-%{majorminor}/libgstlegacyresample.so
%{_libdir}/gstreamer-%{majorminor}/libgstmms.so
%{_libdir}/gstreamer-%{majorminor}/libgstmodplug.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpeg2enc.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegdemux.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegpsmux.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegtsmux.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegvideoparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstmplex.so
%{_libdir}/gstreamer-%{majorminor}/libgstmusepack.so
%{_libdir}/gstreamer-%{majorminor}/libgstmve.so
%{_libdir}/gstreamer-%{majorminor}/libgstmxf.so
%{_libdir}/gstreamer-%{majorminor}/libgstnassink.so
%{_libdir}/gstreamer-%{majorminor}/libgstneonhttpsrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstnsf.so
%{_libdir}/gstreamer-%{majorminor}/libgstnuvdemux.so
%{_libdir}/gstreamer-%{majorminor}/libgstofa.so
%{_libdir}/gstreamer-%{majorminor}/libgstpcapparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstpnm.so
%{_libdir}/gstreamer-%{majorminor}/libgstrawparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstreal.so
%{_libdir}/gstreamer-%{majorminor}/libgstrfbsrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstrsvg.so
%{_libdir}/gstreamer-%{majorminor}/libgstscaletempoplugin.so
%{_libdir}/gstreamer-%{majorminor}/libgstsdl.so
%{_libdir}/gstreamer-%{majorminor}/libgstsdpelem.so
%{_libdir}/gstreamer-%{majorminor}/libgstsndfile.so
%{_libdir}/gstreamer-%{majorminor}/libgstspeed.so
%{_libdir}/gstreamer-%{majorminor}/libgststereo.so
%{_libdir}/gstreamer-%{majorminor}/libgstsubenc.so
%{_libdir}/gstreamer-%{majorminor}/libgsttrm.so
%{_libdir}/gstreamer-%{majorminor}/libgsttta.so
%{_libdir}/gstreamer-%{majorminor}/libgstvcdsrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideosignal.so
%{_libdir}/gstreamer-%{majorminor}/libgstvmnc.so
%{_libdir}/gstreamer-%{majorminor}/libresindvd.so
%{_libdir}/gstreamer-%{majorminor}/libgstautoconvert.so
%{_libdir}/gstreamer-%{majorminor}/libgstcamerabin.so
%{_libdir}/gstreamer-%{majorminor}/libgstdtmf.so
%{_libdir}/gstreamer-%{majorminor}/libgstliveadder.so
%{_libdir}/gstreamer-%{majorminor}/libgstrtpmux.so
%{_libdir}/gstreamer-%{majorminor}/libgstsiren.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideomeasure.so
%{_libdir}/gstreamer-%{majorminor}/libgstdebugutilsbad.so
%{_libdir}/gstreamer-%{majorminor}/libgstadpcmdec.so
%{_libdir}/gstreamer-%{majorminor}/libgsthdvparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstid3tag.so
%{_libdir}/gstreamer-%{majorminor}/libgstasfmux.so
%{_libdir}/gstreamer-%{majorminor}/libgstfrei0r.so
%{_libdir}/gstreamer-%{majorminor}/libgstvdpau.so
%{_libdir}/gstreamer-%{majorminor}/libgstlv2.so
%{_libdir}/gstreamer-%{majorminor}/libgstschro.so
%{_libdir}/gstreamer-%{majorminor}/libgstdc1394.so

%{_libdir}/gstreamer-%{majorminor}/libgstadpcmenc.so
%{_libdir}/gstreamer-%{majorminor}/libgstdataurisrc.so
#{_libdir}/gstreamer-#{majorminor}/libgstflite.so
%{_libdir}/gstreamer-%{majorminor}/libgstjpegformat.so
%{_libdir}/gstreamer-%{majorminor}/libgstladspa.so
%{_libdir}/gstreamer-%{majorminor}/libgstsegmentclip.so
%{_libdir}/gstreamer-%{majorminor}/libgstcog.so
%{_libdir}/gstreamer-%{majorminor}/libgstvp8.so
%{_libdir}/gstreamer-%{majorminor}/libgstopus.so

%{_libdir}/gstreamer-%{majorminor}/libgstcoloreffects.so
%{_libdir}/gstreamer-%{majorminor}/libgstgaudieffects.so
%{_libdir}/gstreamer-%{majorminor}/libgstgeometrictransform.so
%{_libdir}/gstreamer-%{majorminor}/libgstgsettingselements.so
%{_libdir}/gstreamer-%{majorminor}/libgstivfparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstshm.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideomaxrate.so

%{_libdir}/gstreamer-%{majorminor}/libgstcamerabin2.so
%{_libdir}/gstreamer-%{majorminor}/libgstcolorspace.so
%{_libdir}/gstreamer-%{majorminor}/libgstdvbsuboverlay.so
%{_libdir}/gstreamer-%{majorminor}/libgstinterlace.so
%{_libdir}/gstreamer-%{majorminor}/libgstjp2kdecimator.so
%{_libdir}/gstreamer-%{majorminor}/libgstopencv.so
%{_libdir}/gstreamer-%{majorminor}/libgsty4mdec.so

%{_libdir}/gstreamer-%{majorminor}/libgstcurl.so
%{_libdir}/gstreamer-%{majorminor}/libgstdecklink.so
%{_libdir}/gstreamer-%{majorminor}/libgstfieldanalysis.so
%{_libdir}/gstreamer-%{majorminor}/libgstfragmented.so
%{_libdir}/gstreamer-%{majorminor}/libgstlinsys.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegtsdemux.so
%{_libdir}/gstreamer-%{majorminor}/libgstpatchdetect.so
%{_libdir}/gstreamer-%{majorminor}/libgstrtpvp8.so
%{_libdir}/gstreamer-%{majorminor}/libgstsdi.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideofiltersbad.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideoparsersbad.so

%{_libdir}/gstreamer-%{majorminor}/libgstaudiovisualizers.so
%{_libdir}/gstreamer-%{majorminor}/libgstfaceoverlay.so
%{_libdir}/gstreamer-%{majorminor}/libgstfreeverb.so
%{_libdir}/gstreamer-%{majorminor}/libgstinter.so
%{_libdir}/gstreamer-%{majorminor}/libgstopenal.so
%{_libdir}/gstreamer-%{majorminor}/libgstremovesilence.so
%{_libdir}/gstreamer-%{majorminor}/libgstsmooth.so
%{_libdir}/gstreamer-%{majorminor}/libgstteletextdec.so

%exclude %{_libdir}/gstreamer-%{majorminor}/*.la

#%{_libdir}/gstreamer-%{majorminor}/libgstselector.so
#%{_libdir}/gstreamer-%{majorminor}/libgstvalve.so
#%{_libdir}/gstreamer-%{majorminor}/libgstalsaspdif.so
#%{_libdir}/gstreamer-%{majorminor}/libgstjack.so
#%{_libdir}/gstreamer-%{majorminor}/libgstmetadata.so
#%{_libdir}/gstreamer-%{majorminor}/libgstinvtelecine.so
#%{_libdir}/gstreamer-%{majorminor}/libgstmpeg4videoparse.so

# Depend Nonfree
#%{_libdir}/gstreamer-%{majorminor}/libgstdivxdec.so
#%{_libdir}/gstreamer-%{majorminor}/libgstdivxenc.so

%{_datadir}/locale/*/*/*


# library
%{_libdir}/libgstphotography-%{majorminor}.so.*
%{_libdir}/libgstbasevideo-%{majorminor}.so.*
%{_libdir}/libgstsignalprocessor-%{majorminor}.so.*
%{_libdir}/libgstvdp-%{majorminor}.so.*
%{_libdir}/libgstbasecamerabinsrc-%{majorminor}.so.*
%{_libdir}/libgstcodecparsers-0.10.so.*

%exclude %{_libdir}/*.la

%{_datadir}/glib-2.0/schemas/org.freedesktop.gstreamer-0.10.default-elements.gschema.xml

%files devel
%defattr(-,root,root)
# library
%{_libdir}/libgstphotography-%{majorminor}.so
%{_libdir}/libgstbasevideo-%{majorminor}.so
%{_libdir}/libgstsignalprocessor-%{majorminor}.so
%{_libdir}/libgstvdp-%{majorminor}.so
%{_libdir}/libgstbasecamerabinsrc-%{majorminor}.so
%{_libdir}/libgstcodecparsers-0.10.so

# header
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/photography.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/photography-enumtypes.h

%{_includedir}/gstreamer-%{majorminor}/gst/video/gstbasevideocodec.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/gstbasevideodecoder.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/gstbasevideoencoder.h

%{_includedir}/gstreamer-%{majorminor}/gst/signalprocessor
%{_includedir}/gstreamer-%{majorminor}/gst/vdpau
%{_includedir}/gstreamer-%{majorminor}/gst/basecamerabinsrc

%{_includedir}/gstreamer-%{majorminor}/gst/codecparsers/gsth264parser.h
%{_includedir}/gstreamer-%{majorminor}/gst/codecparsers/gstmpeg4parser.h
%{_includedir}/gstreamer-%{majorminor}/gst/codecparsers/gstmpegvideoparser.h
%{_includedir}/gstreamer-%{majorminor}/gst/codecparsers/gstvc1parser.h

%{_includedir}/gstreamer-%{majorminor}/gst/video/gstbasevideoutils.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/gstsurfacebuffer.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/gstsurfaceconverter.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/videocontext.h

# pkg-config
%{_libdir}/pkgconfig/gstreamer-plugins-bad-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-basevideo-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-codecparsers-%{majorminor}.pc

# document
#{_datadir}/gtk-doc/html/gst-plugins-bad-plugins-#{majorminor}
%{_datadir}/gtk-doc/html/gst-plugins-bad-libs-%{majorminor}

%changelog
* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.23-7m)
- rebuild without zbar

* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.23-6m)
- rebuild against mjpegtools-2.1.0

* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.23-5m)
- rebuild with opus

* Sat Oct 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.23.4m)
- disable gtk-doc for a while

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.23-3m)
- rebuild against opencv-2.4.2
- disable flite pugin for a while...

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.23-2m)
- rebuild for glib 2.33.2

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.23-1m)
- update to 0.10.23

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.22-6m)
- rebuild against libmodplug-0.8.8.4

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.22-5m)
- rebuild against libvpx-1.0.0

* Mon Aug 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.22-4m)
- rebuild against opencv 2.3.1
- add patch for opencv 2.3.1

* Sat Jun  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.22-3m)
- rebuild against mjpegtools-2.0.0
- import 2 upstream patches to enable build with mjpegtools-2.0.0
- https://bugzilla.gnome.org/show_bug.cgi?id=650970

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.22-2m)
- set --disable-xvid

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.22-1m)
- update to 0.10.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.21-3m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.21-2m)
- add BR flite-devel

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.21-1m)
- update to 0.10.21

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.20-3m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.20-2m)
- enable-lv2

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.20-1m)
- update to 0.10.20
- disable-lv2 (REMOVE.PLEASE)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.19-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.19-3m)
- build with libass, libcdaudio, libkate and zbar

* Thu Jul 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.19-2m)
- fix build failure: add Cog and VP8 plugins explicitly

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.19-1m)
- update to 0.10.19

* Thu Jun  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.18-3m)
- set --disable-oss4 to avoid conflicting with gstreamer-plugins-good
- libgstoss4audio.so (sys/oss4) was moved to gstreamer-plugins-good

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-2m)
- add %%dir

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18
- add devel package

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.17-7m)
- rebuild against openssl-1.0.0

* Mon Mar 15 2010 Masanobu Sato <satoshiga@momonga-linux.prg>
- (0.10.17-6m)
- add dc1394 plugin

* Fri Feb 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.17-5m)
- add schroedinger plugin

* Mon Feb  8 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.17-4m)
- add BuildRequires for lv2 plugin

* Sun Jan 31 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.17-3m)
- add lv2 plugin

* Sun Dec 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.17-2m)
- add BR libvdpau-devel

* Thu Nov 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.17-1m)
- update to 0.10.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.14-1m)
- update to 0.10.14

* Wed Aug  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.12-3m)
- rebuild against x264

* Wed Jun 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.12-2m)
- delete libgstladspa.so (SIGSEGV)

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.12-1m)
- update to 0.10.12

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.11-3m)
- rebuild against mjpegtools-1.9.0-1m
- remove patch0

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.11-2m)
- rebuild against faad2-2.7

* Thu Apr 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- [SECURITY] CVE-2009-1438 CVE-2009-1513
- update to 0.10.11
-- gst-plugins-bad-plugins < 0.10.11 has a vulnerabe copy of libmodplug.

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.10-9m)
- rebuild against openssl-0.9.8k

* Sun Mar 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-8m)
- fix BPR

* Sun Mar 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-7m)
- TO main

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-6m)
- rebuild against x264-0.0.1127

* Sun Mar  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-5m)
- rebuild against x264-0.0.1114-0.20090218.1m

* Sun Feb 22 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.10-4m)
- remove --enable-gcov option
- exclude la files

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-3m)
- TO.Nonfree

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-2m)
- fix BuildRequires:

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-1m)
- initial build
