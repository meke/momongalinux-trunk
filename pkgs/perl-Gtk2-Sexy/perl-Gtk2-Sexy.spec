%global momorel 22
Name:           perl-Gtk2-Sexy
Version:        0.05
Release:	%{momorel}m%{?dist}
Summary:        Perl interface to the sexy widget collection 

Group:          Development/Libraries
License:        LGPL 
URL:            http://search.cpan.org/dist/Gtk2-Sexy            
Source0:	http://www.cpan.org/modules/by-module/Gtk2/Gtk2-Sexy-%{version}.tar.gz
NoSource: 0
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl
BuildRequires:  perl-Gtk2, perl-ExtUtils-Depends, perl-ExtUtils-PkgConfig
BuildRequires:  libsexy-devel, libxml2-devel

Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:  perl-Gtk2

%description
This module allows a perl developer to access the widgets of the sexy widget
collection.

%prep
%setup -q -n Gtk2-Sexy-%{version}

# keep rpmlint happy...
chmod -x examples/*

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

# make -debuginfo whole....
cp xs/* .

%install
rm -rf %{buildroot}
make pure_install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'
chmod -R u+w %{buildroot}/*
chmod 755 examples

%check
make test


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc ChangeLog examples
%{perl_vendorarch}/auto/Gtk2/*
%{perl_vendorarch}/Gtk2/*
%{_mandir}/man3/*.3*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.05-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-4m)
- rebuild against perl-5.10.1

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (0.05-3m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05-2m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Wed Sep  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- update to 0.04

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- update to 0.03

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.02-6m)
- %%NoSource -> NoSource

* Mon May 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.02-5m)
- fix doc/examples permission

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02-4m)
- fix files dup

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.02-3m)
- use vendor

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.02-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.02-1m)
- import to Momonga from Fedora Development


* Thu Aug 31 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-5
- bump for mass rebuild

* Mon Jul 31 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-4
- bump for build

* Mon Jul 31 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-3
- add an explicit dep on perl(Gtk2)
- add additional br, plus fix -debuginfo package

* Fri Jul 28 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-2
- add missing br

* Thu Jul 27 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-1
- Initial spec file for F-E
