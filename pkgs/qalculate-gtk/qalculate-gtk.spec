%global momorel 4

Summary:	A multi-purpose desktop calculator for GNU/Linux
Name:		qalculate-gtk
Version:	0.9.7
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		Applications/Engineering
URL:		http://qalculate.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/qalculate/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		qalculate-gtk-desktop.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	libgnome-devel
BuildRequires:	libglade2-devel
BuildRequires:	libgnomeui-devel
BuildRequires:	libqalculate-devel >= %{version}
BuildRequires:	gettext
BuildRequires:	desktop-file-utils
BuildRequires:	scrollkeeper
BuildRequires:	perl-XML-Parser
BuildRequires:	pkgconfig
BuildRequires:	intltool
BuildRequires:	libtool
BuildRequires:	automake
BuildRequires:	autoconf
Requires:	gnuplot
Requires(post):	scrollkeeper
Requires(postun):scrollkeeper

%description
Qalculate! is a multi-purpose desktop calculator for GNU/Linux. It is
small and simple to use but with much power and versatility underneath.
Features include customizable functions, units, arbitrary precision, plotting.
This package provides a (GTK+) graphical interface for Qalculate! 

%prep
%setup -q
%patch0 -p0 -b .desktop

%build

intltoolize --copy --force --automake
libtoolize --force --copy
aclocal
autoheader
automake
autoconf
%configure 
make %{?_smp_mflags}
										
%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

desktop-file-install --delete-original			\
	--vendor=""					\
	--remove-category Application			\
	--dir %{buildroot}%{_datadir}/applications	\
	--mode 0644					\
	%{buildroot}%{_datadir}/applications/qalculate-gtk.desktop

%find_lang qalculate-gtk
rm -rf %{buildroot}/%{_bindir}/qalculate

%post
scrollkeeper-update -q -o %{_datadir}/omf/qalculate-gtk || :

%postun
scrollkeeper-update -q || :

%clean
rm -rf %{buildroot}

%files -f qalculate-gtk.lang
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog COPYING TODO
%doc %{_datadir}/gnome/help/qalculate-gtk/
%{_bindir}/qalculate-gtk
%{_datadir}/applications/qalculate-gtk.desktop
%{_datadir}/pixmaps/qalculate.png
%{_datadir}/omf/qalculate-gtk/
%{_datadir}/qalculate-gtk/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- full rebuild for mo7 release

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-4m)
- rebuild gainst cln-1.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-3m)
- rebuild against rpm-4.6

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-2m)
- enable to build with -O2 option on x86_64

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- import from Fedora devel

* Wed Feb 27 2008 Deji Akingunola <dakingun@gmail.com> - 0.9.6-4
- Rebuild (with patch) for cln-1.2

* Sun Feb 10 2008 Deji Akingunola <dakingun@gmail.com> - 0.9.6-3
- Rebuild for gcc43

* Sat Aug 25 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.6-2
- Rebuild

* Fri Aug 03 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.6-2
- License tag update

* Sun Jul 01 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.6-1
- Update to new release

* Sat Jun 09 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.5-2
- Modify the Name field in the desktop file to distinguish it from that of
  qalculate-kde (BZ #241024)
- Require 'yelp' for the Help menu (BZ #243395)

* Tue Jan 02 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.5-1
- New release

* Mon Aug 30 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-5
- Add perl(XML::Parser) BR

* Mon Aug 28 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-4
- Rebuild for FC6

* Wed Jun 28 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-3
- Properly package up missing file

* Wed Jun 28 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-2
- Add missing BR on libgnomeui

* Wed Jun 27 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-1
- New version 0.9.4

* Thu Mar 30 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.3-1
- Update to newer version

* Mon Feb 13 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.2-2
- Rebuild for Fedora Extras 5

* Tue Dec 27 2005 Deji Akingunola <dakingun@gmail.com> - 0.9.2-1
- Upgrade to new version

* Sat Nov 05 2005 Deji Akingunola <dakingun@gmail.com> - 0.9.0.1
- Upgrade to new version

* Thu Nov 2 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.2.1-2
- Rebuild with new cln package

* Thu Oct 13 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.2.1-2
- Update to a new release that handles new behaviour in pango >= 1.10.0

* Thu Oct 13 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.2-4
- Fix for yelp error (Niklas Knutsson)

* Thu Oct 13 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.2-3
- Rmove explicit requires for gnome-vfs2 and libqalculate
- Install desktop file properly

* Tue Oct 11 2005 Paul Howarth <paul@city-fan.org> - 0.8.2-2
- Use "make DESTDIR=%%{buildroot}" instead of %%makeinstall
- Expand most references to %%{name} for clarity
- Make sure scriptlets complete successfully
- Add scriptlet deps
- Include license text
- Remove redundant doc files NEWS & README
- Fix directory ownership

* Tue Oct 11 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.2-1
- Upgraded to new version
- Remove redundant buildrequires - make libglade2 requires them all
- Remove the -export-dynamic configure option, now done upstream
- Add gnome-vfs2 to require.
- Install the desktop file

* Wed Oct 05 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.1
- Initial package
