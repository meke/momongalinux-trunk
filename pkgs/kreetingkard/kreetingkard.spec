%global momorel 18
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global kdebaserel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global templatesver 0.2.0
%global enable_gcc_check_and_hidden_visibility 0

Summary: A tool for making greeting cards easily
Name: kreetingkard
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Publishing
URL: http://linux-life.net/program/cc/kde/app/kreetingkard/
Source0: http://dl.sourceforge.jp/%{name}/18105/%{name}-%{version}.tar.gz
NoSource: 0
Source1: http://dl.sourceforge.jp/%{name}/18013/%{name}_templates-%{templatesver}.tar.gz
NoSource: 1
Source2: %{name}-48.png
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-gcc41.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: lha
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase3 >= %{kdever}-%{kdebaserel}
BuildRequires: desktop-file-utils
BuildRequires: libart_lgpl-devel
BuildRequires: libpng-devel
BuildRequires: libidn-devel
BuildRequires: zlib-devel

%description
A tool for making greeting cards easily.

%prep
# clean up kreetingkard_templates
rm -rf %{_builddir}/%{name}_templates-%{templatesver}

%setup -q -b 1

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .gcc41

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

# build kreetingkard_templates
cd %{_builddir}/%{name}_templates-%{templatesver}

./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--disable-rpath

make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install kreetingkard_templates
cd %{_builddir}/%{name}_templates-%{templatesver}
make install DESTDIR=%{buildroot}

# install icon
mkdir -p %{buildroot}%{_datadir}/icons/crystalsvg/48x48/apps
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/icons/crystalsvg/48x48/apps/%{name}.png

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/crystalsvg/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category Office \
  --add-category KDE \
  --add-category Qt \
  --remove-key=Patterns \
  %{buildroot}%{_datadir}/applnk/Office/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_bindir}/%{name}
%{_datadir}/applications/kde/*.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/mimelnk/application/*.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-16m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-15m)
- build fix with desktop-file-utils-0.16

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-14m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-12m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-11m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-10m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-9m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-8m)
- %%NoSource -> NoSource

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-7m)
- update desktop.patch (add DocPath)

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-6m)
- remove Application from Categories of desktop file

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-5m)
- rebuild against kdelibs-3.5.4-3mkdebase-3.5.4-11m

* Fri Mar 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-4m)
- remove koffice from Requires

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-3m)
- modify %%prep (clean up kreetingkard_templates)

* Sun Jan  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-2m)
- add gcc-4.1 patch
- Patch1: kreetingkard-0.7.1-gcc41.patch

* Tue Dec 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1

* Sun Dec 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.0-1m)
- version 0.7.0
- update kreetingkard_templates to 0.2.0

* Sun Dec 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.4-1m)
- version 0.6.4

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-1m)
- version 0.6.3
- merge kreetingkard_templates

* Tue Nov 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-1m)
- version 0.6.2

* Mon Nov 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-1m)
- version 0.6.1
- add desktop.patch

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-5m)
- rebuild against KDE 3.5 RC1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-4m)
- add --disable-rpath to configure

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-3m)
- add --with-qt-libraries=%%{qtdir}/lib to configure

* Wed Feb  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-2m)
- Requires: lha

* Wed Feb  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- initial package
- import Source1: kreetingkard-48.png from UT Extra Packages for Mandrake
