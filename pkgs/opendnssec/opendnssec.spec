%global momorel 1

# For RHEL/CentOS 5 users sticking to the official too-old sqlite distribution:
# To build and statically link the bundled SQLite version, please use 
#	rpmbuild --with static_sqlite

%define logmsg logger -t %{name}/rpm

#%%if 0%{?_with_static_sqlite:1}
#%%define sqlite_version 3.6.20
#%%define sqlite_tmp_install_dir %{_tmppath}/sqlite-for-opendnssec
#%%endif

Summary: Open-source turn-key solution for DNSSEC signing of zones
Name: opendnssec
Version: 1.2.1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Daemons
Source0: http://www.opendnssec.org/files/source/opendnssec-%{version}.tar.gz
NoSource: 0
Patch0: fix_localstate_dir.patch
Patch1: runas_opendnssec_etc.patch
Patch2: ods-control_chkconfig_support.patch
#Patch3: fix_conditional_config_install.patch
#Patch4: opendnssec-1.1.1-init.patch

%if 0%{?_with_static_sqlite:1}
Source100: http://sqlite.org/sqlite-%{sqlite_version}.tar.gz
Patch100: enforcer_configure_sqlite3_libs.patch
Patch101: enforcer_static_sqlite3.patch
Patch201: static_sqlite3_binary.patch
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Following are optional fields
URL: http://www.opendnssec.org/files/source/%{name}-%{version}.tar.gz
Requires(pre): shadow-utils
Requires: python, libxml2 >= 2.6.16, ruby18, rubygem18-dnsruby >= 1.52, ldns >= 1.6.4
Requires: python-4Suite-XML
BuildRequires: rubygem18-dnsruby >= 1.52, ldns-devel >= 1.6.4
BuildRequires: libxml2-devel, ruby18-devel , python-4Suite-XML
BuildRequires: libxslt

%if 0%{?_with_static_sqlite:1}
BuildRequires: ncurses-devel, readline-devel, glibc-devel
%else
Requires: sqlite >= 3.4.2
BuildRequires: sqlite-devel
%endif


%description
OpenDNSSEC was created as an open-source turn-key solution for DNSSEC. 
It secures zone data just before it is published in an authoritative 
name server.  OpenDNSSEC takes in unsigned zones, adds the signatures 
and other records for DNSSEC and passes it on to the authoritative name 
servers for that zone. DNS is complicated, and so is digital signing; 
their combination in DNSSEC is of course complex as well. The idea of 
OpenDNSSEC is to handle such difficulties, to relieve the administrator 
of them after a one-time effort for setting it up.  The storage of keys 
is done through a PKCS #11 standard interface. To deploy OpenDNSSEC, an 
implementation of this interface is needed, for example a software 
library, an HSM or perhaps a simpler token.

%if 0%{?_with_static_sqlite:1}
This package has been built with SQLite3 libraries statically linked
into the binaries that require them.
%endif

%package devel
Summary: Development package that includes the OpenDNSSEC header files etc.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The devel package contains the OpenDNSSEC library and the include files.

%prep
%setup -q %{?_with_static_sqlite:-a 100}

# find . -type f | xargs grep -l full_localstatedir\/opendnssec | xargs perl -p -i -e "s|\$full_localstatedir/opendnssec|\$full_localstatedir/lib/opendnssec|"

%patch0 -p1 -b .fix_localstate_dir
%patch1 -p1 -b .runas_opendnssec_etc
%patch2 -p1 -b .ods-control_chkconfig_support
#%%patch3 -p1 -b .fix_conditional_config_install
#%%patch4 -p1 -b .stop-service

%if 0%{?_with_static_sqlite:1}
%patch100 -p1 -b .enforcer_configure_sqlite3_libs
%patch101 -p1 -b .enforcer_static_sqlite
%endif

%build
%if 0%{?_with_static_sqlite:1}
[ -e "%{sqlite_tmp_install_dir}" ] && %{__rm} -rf "%{sqlite_tmp_install_dir}"
cd sqlite-%{sqlite_version}
patch -p1 < "$RPM_SOURCE_DIR"/static_sqlite3_binary.patch
# Imitating Fedora's sqlite-3.6.20-1.fc12:
CFLAGS="$RPM_OPT_FLAGS \
	-DSQLITE_ENABLE_COLUMN_METADATA=1 \
	-DSQLITE_DISABLE_DIRSYNC=1 \
	-DSQLITE_ENABLE_FTS3=3 \
	-DSQLITE_ENABLE_RTREE=1 \
	-Wall -fno-strict-aliasing" \
./configure \
	--prefix="%{sqlite_tmp_install_dir}" \
	%{!?with_tcl:--disable-tcl} \
	--enable-threadsafe \
	--enable-threads-override-locks \
	--with-ruby=ruby18 \
#	--enable-load-extension \
	%{?with_tcl:TCLLIBDIR=%{tcl_sitearch}/sqlite3} \
	LDFLAGS="`pkg-config sqlite3 --libs`"

%{__make} %{?_smp_mflags} 
%{__make} install
cd ..
%endif

# RHEL5 autoconf is too old to be able to patch configure.ac and `autoreconf`.
# Thus, modify conf/configure here directly.
#sed -i.fix_localstate_dir -re 's|^opendnsseclocalstatedir=\$full_localstatedir/opendnssec|opendnsseclocalstatedir=$full_localstatedir/lib/opendnssec|g' \
#	conf/configure \
#	enforcer/configure
#
sed -i.fix_localstate_dir -re 's|^OPENDNSSEC_LOCALSTATE_DIR="\$full_localstatedir/opendnssec"|OPENDNSSEC_LOCALSTATE_DIR="$full_localstatedir/lib/opendnssec"|g' \
	plugins/eppclient/configure plugins/eppclient/configure \
	auditor/configure auditor/m4/opendnssec_common.m4 \
	m4/opendnssec_common.m4 configure
sed -i.ods-ksmutil_sqlite3_bin_path -re 's|^(#define SQL_BIN) "\$SQLITE3"|\1 "$libexecdir/opendnssec/sqlite3"|' configure

CFLAGS="$RPM_OPT_FLAGS" %{configure} \
	--with-ruby=ruby18 \
	%{?_with_static_sqlite:--with-sqlite3="%{sqlite_tmp_install_dir}"} \
	LDFLAGS="`pkg-config sqlite3 --libs`"

%{?_with_static_sqlite:LIBTOOL_LINKFLAGS="$LIBTOOL_LINKFLAGS -static-libtool-libs"} \
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install
mkdir -p %{buildroot}/%{_localstatedir}/run/opendnssec
mkdir -p %{buildroot}/%{_initscriptdir}
%{__install} -c -m 755 tools/ods-control %{buildroot}/%{_initscriptdir}/ods-control
%{__rm} -f %{buildroot}/%{_datadir}/opendnssec.spec
%{__rm} -f %{buildroot}/%{_libdir}/*.la

%if 0%{?_with_static_sqlite:1}
%{__install} -c -m 755 "%{sqlite_tmp_install_dir}"/bin/sqlite3 %{buildroot}/%{_libexecdir}/opendnssec/
%endif

%clean
%if 0%{?_with_static_sqlite:1}
%{__rm} -rf "%{_tmppath}/sqlite"
%endif

[ "$RPM_BUILD_ROOT" != "/" ] && %{__rm} -rf "$RPM_BUILD_ROOT"

%pre
# Add user account and group opendnssec.
if ! getent group opendnssec >/dev/null; then
  %logmsg "Adding group opendnssec."
  if ! groupadd -r opendnssec; then
    %logmsg "Unexpected error adding group opendnssec. Installation aborted."
    exit 255
  fi
fi
if ! getent passwd opendnssec >/dev/null; then
  %logmsg "Adding user opendnssec."
  if ! useradd -r -g opendnssec -d %{_localstatedir}/lib/opendnssec \
    -s /sbin/nologin -c "OpenDNSSEC unpriviledged user" opendnssec; then
    %logmsg "Unexpected error adding user opendnssec. Installation aborted."
    exit 255
  fi
fi
exit 0

%post
# Add the init script only when installing (rpm -i) the package, not
# when upgrading (rpm -U).
if [ $1 -eq 1 ] ; then
  /sbin/chkconfig --add ods-control
fi

%preun
# Remove the init script only when removing (rpm -e) the package, not
# when upgrading (rpm -U).
if [ $1 -eq 0 ] ; then
  /sbin/chkconfig --del ods-control
fi

%files
%defattr(-,root,root,-)
%doc KNOWN_ISSUES NEWS README
# configs:
%attr(0750,root,opendnssec) %dir %{_sysconfdir}/opendnssec
%attr(0640,root,opendnssec) %config(noreplace) %{_sysconfdir}/opendnssec/*.xml
%attr(0640,root,opendnssec) %{_sysconfdir}/opendnssec/*.xml.sample
# init script:
%{_initscriptdir}/ods-control
# pid file storage dir:
%attr(0755,opendnssec,opendnssec) %dir %{_localstatedir}/run/opendnssec
# executables:
%{_bindir}/*
%{_sbindir}/*
#%%dir %{_libexecdir}/opendnssec
#%%{_libexecdir}/opendnssec/*
# libraries required for running opendnssec:
#%%{_libdir}/libhsm.so.*
#%%{_libdir}/libhsm.a
%dir %{_libdir}/opendnssec
%{_libdir}/opendnssec/*.rb
%dir %{_libdir}/opendnssec/kasp_auditor
%{_libdir}/opendnssec/kasp_auditor/*.rb
#%%dir %{_libdir}/opendnssec/signer
#%%{_libdir}/opendnssec/signer/*
# manuals:
%{_mandir}/man?/*.*
# config validation schemas:
%dir %{_prefix}/share/opendnssec
%{_prefix}/share/opendnssec/*
# working directories:
%defattr(0644,opendnssec,opendnssec,0755)
%dir %{_localstatedir}/run/opendnssec
%dir %{_localstatedir}/lib/opendnssec/signconf
%dir %{_localstatedir}/lib/opendnssec/signed
%dir %{_localstatedir}/lib/opendnssec/tmp
%dir %{_localstatedir}/lib/opendnssec/unsigned

%files devel
%defattr(-,root,root,-)
#%%{_includedir}/*.h
#%%{_libdir}/libhsm.so

%changelog
* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-8m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-7m)
- update CFLAGS and LDFLAGS for sqlite3, again.

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-6m)
- update CFLAGS and LDFLAGS for sqlite3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-5m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-4m)
- stop auto starting service at initial system startup

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-3m)
- use rubygem18-dnsruby

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-2m)
- use ruby18

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-1m)
- update
- add BuildRequires: libxslt

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- import to Momonga

* Thu Apr 15 2010 Ville Mattila <vmattila@csc.fi> - 1.0.0-4
- sqlite3 tool is used by ods-ksmutil, thus include it in the package
  as $libexecdir/opendnssec/sqlite3 (statically linked, patch #201)

* Tue Apr 13 2010 Ville Mattila <vmattila@csc.fi> - 1.0.0-3
- Fix for conditional installation of configuration files (patch #3)
- Workaround SQLite version dependency problem for RHEL/CentOS 5 builds
  with 'rpmbuild --with static_sqlite' flag to build and install
  sqlite-3.6.20 into a temporary location and statically link the libraries
  into enforcer daemon and ods-ksmutil.

* Mon Apr 12 2010 Ville Mattila <vmattila@csc.fi> - 1.0.0-2
- Update to 1.0.0.
- Updates to Requires: and BuildRequires: definitions
- Move headers and libhsm.so into separate package opendnssec-devel.
- Use /var/lib/opendnssec instead of /var/opendnssec (FHS / Fedora EPEL
  compliance, stealed patch #0 from Debian/Ubuntu package by Ondrej Sury).
- Changes to default config (copied from Debian/Ubuntu packaging):
  + conf.xml: Make make enforcer and signer to run as user opendnssec (default
    config has <Privileges> commented out, thus daemons would be started
    as root).
  + conf.xml: Comment out <Repository name="softHSM">
  + signconf.xml: Comment out <Zone name="opendnssec.org">
- Create opendnssec user account and group in %pre if they don't exist
  (otherwise RPM would make root:root the owner of /var/lib/opendnssec).
- Add chkconfig: and description: tags in tools/ods-control.in to make
  it chkconfig(8) compliant.
- Run 'chkconfig --add ods-control' and 'chkconfig --del ods-control' only 
  when installing (rpm -i) and removing (rpm -e) the package, not when 
  upgrading (rpm -U).
- Assign /etc/opendnssec and the config files to root:opendnssec with
  umask 007 instead (I thinks it's best not to give anyone but root
  write access anywhere in /etc).
- Use config(noreplace) protection for configuration files.
- Explicitly define the directories for libraries in %{_libdir}/%{name},
  config validation schemas in %{_prefix}/share/opendnssec etc.
- Remove /usr/share/opendnssec.spec from the package.
- Ditch the *.la files.
- Plus some other minor cleanups.

* Wed Nov 25 2009  <andyh@nominet.org.uk>
- Cleaned up and updated for 1.0.0b8

* Thu Oct 29 2009  <andyh@nominet.org.uk>
- Initial spec file
