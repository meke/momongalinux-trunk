%global momorel 5

Summary: momonga nonfrees package build tool
Name: mnpb
Version: 0.0.13
Release: %{momorel}m%{?dist}
License: GPLv3
URL: http://www.momonga-linux.org/
Group: Development/Tools
Source0: %{name}-%{version}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: redhat-lsb
Requires: rpm
Requires: ruby
Requires: ruby-rpm
Requires: subversion
Requires: wget
Requires: yum
Requires: polkit
BuildRequires: PackageKit-glib-devel >= 0.8.9

# Obsoletes: momonga-nonfree-package-builder

%description
%{name}
It's a test release...

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README ChangeLog
%{_bindir}/%{name}
%{_libexecdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/glib-2.0/schemas/org.momonga.%{name}.gschema.xml

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org> 
- (0.0.13-5m)
- rebuild against PackageKit-0.9.2

* Thu Jul 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.13-4m)
- rebuild against PackageKit-0.8.9

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.13-3m)
- rebuild against PackageKit-0.8.6

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.13-2m)
- rebuild agaisnt PackageKit-0.8.1

* Mon May  7 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.13-1m)
- update to 0.0.13

* Sun Apr 22 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.12-1m)
- update to 0.0.12

* Sun Apr 22 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.11-1m)
- update to 0.0.11

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.10-1m)
- update to 0.0.10

* Wed Feb 22 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.9-1m)
- update to 0.0.9

* Thu Feb 16 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.8-1m)
- update to 0.0.8

* Sun Feb 12 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.7-1m)
- update to 0.0.7

* Sun Feb 12 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.6.1-1m)
- update to 0.0.6.1

* Sun Feb 12 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.6-1m)
- update to 0.0.6

* Sat Feb 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.5-1m)
- update to 0.0.5

* Sat Feb 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4-1m)
- update to 0.0.4

* Sat Feb 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.3-1m)
- update to 0.0.3

* Wed Feb  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.2-1m)
- update to 0.0.2

* Tue Feb  7 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.1-1m)
- update to 0.0.1

* Mon Jan 30 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.0-3m)
- update source...

* Mon Jan 30 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.0-2m)
- update source...

* Fri Jan 27 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.0-1m)
- initial commit
