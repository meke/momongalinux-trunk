%global momorel 4

Name:		slapi-nis
Version:	0.17
Release:	%{momorel}m%{?dist}
Summary:	NIS Server and Schema Compatibility plugins for Directory Server
Group:		System Environment/Daemons
License:	GPLv2
URL:		http://slapi-nis.fedorahosted.org/
Source0:	https://fedorahosted.org/releases/s/l/slapi-nis/slapi-nis-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	389-ds-base-devel, mozldap-devel, tcp_wrappers-devel

%description
This slapi-nis package contains two plugins which aim to ease the
transition of a network from NIS to LDAP.  The NIS Server plugin
(nisserver-plugin) allows the directory server itself to act as a NIS
server, and the Schema Compatibility plugin (schemacompat-plugin)
allows the directory server to provide modified views of data stored
in the directory.

%prep
%setup -q

%build
%configure --disable-static --with-tcp-wrappers
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT/%{_libdir}/dirsrv/plugins/*.la

%if 0
# ns-slapd doesn't want to start in koji
%check
make check
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING NEWS README STATUS doc/*.txt doc/*.ldif
%{_libdir}/dirsrv/plugins/*.so
%{_sbindir}/nisserver-plugin-defs

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-1m)
- import from Rawhide

* Fri Nov  7 2008 Nalin Dahyabhai <nalin@redhat.com> - 0.9-1
- update to 0.9

* Fri Oct  3 2008 Nalin Dahyabhai <nalin@redhat.com> - 0.8.1-1
- update to 0.8.1 to fix a heap corruption (Rich Megginson)

* Wed Aug  6 2008 Nalin Dahyabhai <nalin@redhat.com> - 0.8-1
- update to 0.8

* Wed Aug  6 2008 Nalin Dahyabhai <nalin@redhat.com> - 0.7-1
- update to 0.7

* Wed Jul 23 2008 Nalin Dahyabhai <nalin@redhat.com> - 0.6-1
- rebuild (and make rpmlint happy)

* Wed Jul  9 2008 Nalin Dahyabhai <nalin@redhat.com> - 0.2-1
- initial package
