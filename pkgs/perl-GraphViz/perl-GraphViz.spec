%global         momorel 4

Name:           perl-GraphViz
Version:        2.15
Release:        %{momorel}m%{?dist}
Summary:        Interface to AT&T's GraphViz. Deprecated. See GraphViz2
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/GraphViz/
Source0:        http://www.cpan.org/authors/id/R/RS/RSAVAGE/GraphViz-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-File-Which >= 1.09
BuildRequires:  perl-Getopt-Long >= 2.38
BuildRequires:  perl-IO >= 1.14
BuildRequires:  perl-IPC-Run >= 0.6
BuildRequires:  perl-libwww-perl >= 6
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Parse-RecDescent >= 1.965.001
BuildRequires:  perl-Pod-Parser >= 1.36
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Test-Pod >= 1.44
BuildRequires:  perl-Time-HiRes >= 1.9724
BuildRequires:  perl-XML-Twig >= 3.38
BuildRequires:  perl-XML-XPath >= 1.13
Requires:       perl-File-Which >= 1.09
Requires:       perl-Getopt-Long >= 2.38
Requires:       perl-IO >= 1.14
Requires:       perl-IPC-Run >= 0.6
Requires:       perl-libwww-perl >= 6
Requires:       perl-Parse-RecDescent >= 1.965.001
Requires:       perl-Pod-Parser >= 1.36
Requires:       perl-Time-HiRes >= 1.9724
Requires:       perl-XML-Twig >= 3.38
Requires:       perl-XML-XPath >= 1.13
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides an interface to layout and image generation of
directed and undirected graphs in a variety of formats (PostScript, PNG,
etc.) using the "dot", "neato", "twopi", "circo" and "fdp" programs from
the GraphViz project (http://www.graphviz.org/ or
http://www.research.att.com/sw/tools/graphviz/).

%prep
%setup -q -n GraphViz-%{version}

find . -type f | xargs %{__perl} -p -i -e 's|/usr/local/bin/perl|%{__perl}|'

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changelog.ini Changes examples META.json README
%{perl_vendorlib}/Devel/GraphVizProf.pm
%{perl_vendorlib}/GraphViz.pm
%{perl_vendorlib}/GraphViz
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-4m)
- rebuild against perl-5.20.0

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.15-3m)
- rebuild against graphviz-2.36.0-1m

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-1m)
- update to 2.15

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-2m)
- rebuild against perl-5.16.3

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-1m)
- update to 2.14

* Thu Nov  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-2m)
- rebuild against perl-5.16.2

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-2m)
- rebuild against perl-5.16.0

* Mon Mar 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Thu Dec 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-1m)
- update to 2.09

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Sun Oct 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.04-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.03-2m)
- rebuild against gcc43

* Mon Nov 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.02-2m)
- use vendor

* Fri Sep 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02-1m)
- spec file was autogenerated
