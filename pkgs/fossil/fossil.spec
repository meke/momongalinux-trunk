%global momorel 1
%global src_date 20140612172556

Summary: There are plenty of open-source version control systems available on the internet these days
Name: fossil
Version: 1.26
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Tools
URL: http://www.fossil-scm.org/

Source: http://www.fossil-scm.org/download/fossil-src-%{src_date}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: gcc
BuildRequires: make
BuildRequires: openssl-devel
BuildRequires: sqlite-devel
BuildRequires: zlib-devel

%description
There are plenty of open-source version control systems available on the internet these days.
What makes Fossil worthy of attention?

* Bug Tracking And Wiki 
* Web Interface
* Autosync 
* Self-Contained
* Simple Networking
* CGI Enabled
* Robust & Reliable 

%prep
%setup -n %{name}-src-%{src_date}

%build
CFLAGS="$RPM_OPT_FLAGS" \
./configure --disable-internal-sqlite
%make

%install
rm -rf %{buildroot}
install -m755 -d %{buildroot}%{_bindir}
install -m755 fossil %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc VERSION *.txt www
%{_bindir}/*

%changelog
* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.26-1m)
- update 1.26

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.25-1m)
- update 1.25

* Wed Dec 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.24-1m)
- update 1.24

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.22-1m)
- update 1.22

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.21-1m)
- update 1.21

* Mon Nov  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20-1m)
- Initial Commit Momonga Linux
