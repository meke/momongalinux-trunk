%global momorel 16

%global src_ver 1.3

%global ruby18_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Name:           gonzui
Version:        %{src_ver}
Release:        %{momorel}m%{?dist}
Summary:        Source code search engine
License:        GPLv2
Group:          Development/Tools
URL:            http://gonzui.sourceforge.net/
#Source0:        http://dl.sourceforge.net/sourceforge/%%{name}/%%{name}-%%{version}.tar.gz
#NoSource:       0
Source0:        %{name}-%{version}.tar.gz
Source1:        gonzui.init
Source2:        gonzuirc
Source3:        gonzui.logrotate
Patch1:         gonzui-1.2-multi_checkout.diff
Patch2:         gonzui-1.3-name_tagging.diff 
Patch3:         gonzui-1.2-svn_ssh.diff 
Patch4:         gonzui-1.3-ruby18.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ruby18-devel
BuildRequires:  ruby18-bdb
BuildRequires:  langscan >= 1.2-11m
Requires:       ruby18-bdb
Requires:       langscan >= 1.2-11m
Requires(pre):  shadow-utils   
Requires(post): chkconfig coreutils
Requires(preun): chkconfig
Requires(postun): chkconfig

%description
Gonzui is a source code search engine for accelerating open source software 
development. In the open source software development, programmers frequently 
refer to source codes written by others.
 
Our goal is to help programmers develop programs effectively by creating a 
source code search engine that covers vast quantities of open source codes 
available on the Internet.

%prep

%setup -q
# more than one cvs
%patch1
# --name
%patch2
# svn+ssh support
%patch3

%patch4 -p1 -b .ruby18

%build
autoreconf -fi
%configure \
    --with-ruby=%{_bindir}/ruby18 \
    --with-rubydir=%{ruby18_sitelibdir} \
    --with-rubyarchdir=%{ruby18_sitearchdir}

%make

%check
#TODO

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_initscriptdir}
cp %{SOURCE1} %{buildroot}%{_initscriptdir}/%{name}
cp %{SOURCE2} %{buildroot}%{_sysconfdir}/%{name}rc
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
cp %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

mkdir -p %{buildroot}%{_localstatedir}/%{name}
mkdir -p %{buildroot}%{_localstatedir}/lib/%{name}

%clean
rm -rf %{buildroot}

%pre
/usr/sbin/userdel %{name} || :
/usr/sbin/groupadd -r %{name} || :
/usr/sbin/useradd -M -g %{name} -d %{_datadir}/%{name} -r -s /bin/false %{name}

%post
/sbin/chkconfig --add %{name}
/bin/chmod 0755 /var/lib/gonzui
/bin/touch /var/log/gonzui/gonzui.log
/bin/touch /var/log/gonzui/access.log

%preun
/sbin/chkconfig --del %{name}

%postun 
/usr/sbin/userdel %{name}  

%files
%defattr(-,root,root,-)
%doc README AUTHORS ChangeLog NEWS
%config(noreplace) %{_sysconfdir}/logrotate.d/*
%attr(0755,root,root) %{_initscriptdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}rc*
%{_datadir}/%{name}
%{_bindir}/%{name}-*
%{ruby18_sitearchdir}/%{name}
%{ruby18_sitelibdir}/%{name}
%{ruby18_sitelibdir}/%{name}.rb
%{_localstatedir}/lib/%{name}
%{_localstatedir}/log/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-14m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-13m)
- rebuild against ruby18-bdb-0.6.5-6m

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-12m)
- build with ruby18

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-11m)
- fix up Requires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-10m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3-8m)
- revised logrotate

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-6m)
- rebuild against gcc43

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-5m)
- PreReq: chkconfig, shadow-utils

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-4m)
- modify %%files for new ruby

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3-3m)
- rebuild against ruby-1.8.6-4m

* Tue Jan 09 2007 Kazuhiko <kazuhiko@fdiary.net>
- (1.3-2m)
- create 'gonzui' group at %%pre

* Thu Oct 12 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-1m)
- update to 1.3
  update Patch2: gonzui-1.3-name_tagging.diff   

* Fri Jul 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-3m)
- create system account at %%pre

* Tue Jul 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-2m)
- do not start daemon at default

* Mon Jul 24 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2-1m)
- initial release to Momonga Linux
- spec and resource copied from mandriva and modified
- need to be superuser to import source files

* Thu Jan 12 2006 Michael Scherer <misc@mandriva.org> 1.2-2mdk
- do not own ruby libdir
- add a initscript, with a user
- add patch1, to have more than one cvs/svn repository
- add patch2, to have --name tagging
- add patch3, to add ssh:// for svn+ssh repository 
- add a logrotate file 

* Wed Nov 02 2005 Michael Scherer <misc@mandriva.org> 1.2-1mdk
- first package
