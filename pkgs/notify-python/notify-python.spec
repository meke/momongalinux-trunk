%global momorel 15

Summary: notification library for python 
Name: notify-python
Version: 0.1.1
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/ 
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: notify-python-0.1.1-libnotify-0.7.2.patch
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pygtk2-devel >= 2.10.3-2m
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: glib2-devel
BuildRequires: python >= 2.7

%description
notification library for python

%package devel
Summary:	Files for development using %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:       glib2-devel >= 2.12.1

%description devel
This package contains pkg-config file for development of programs
using %{name}.

%prep
%setup -q
%patch0 -p1 -b .libnotify

%build
# regenerate pynotify.c (gentoo bug #212128)
rm -f src/pynotify.c

automake-1.9
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/python?.?/site-packages/gtk-2.0/pynotify
%{_datadir}/pygtk/2.0/defs/pynotify.defs

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/notify-python.pc

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-15m)
- rebuild for glib 2.33.2

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-14m)
- rebuild against python-2.7

* Tue Apr 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-13m)
- fix patch0 (guake was broken)

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-12m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-9m)
- full rebuild for mo7 release

* Wed May 12 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.1.1-8m)
- revised spec for fixing bug.
- see http://bugs.gentoo.org/show_bug.cgi?id=212128

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.1-4m)
- rebuild against python-2.6.1-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- %%NoSource -> NoSource

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-2m)
- rebuild against python-2.5

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.0-1m)
- initila build
