%global momorel 1

%global xfce4ver 4.10.0
%global major 0.2

Name:           xfce4-notifyd
Version:        0.2.2
Release:        %{momorel}m%{?dist}
Summary:        Simple notification daemon for Xfce

Group:          User Interface/Desktops
License:        GPLv2
URL:            http://spuriousinterrupt.org/projects/xfce4-notifyd
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
# Patch0:         xfce4-notifyd-0.2.0-dbus-service-name.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.10.0
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  xfconf-devel >= %{xfce4ver}
BuildRequires:  dbus-glib-devel >= 0.72
BuildRequires:  libglade2-devel >= 2.6.0
BuildRequires:  libsexy-devel >= 0.1.6
BuildRequires:  desktop-file-utils
BuildRequires:  intltool
Requires:       dbus
Requires:       hicolor-icon-theme
# for compatibility this package provides
Provides:       desktop-notification-daemon
# and obsoletes all notification-daemon-xfce releases
Obsoletes:      notification-daemon-xfce <= 0.3.7


%description
Xfce4-notifyd is a simple, visually-appealing notification daemon for Xfce 
that implements the freedesktop.org desktop notifications specification.
Features:
* Themable using the GTK+ theming mechanism
* Visually appealing: rounded corners, shaped windows
* Supports transparency and fade effects


%prep
%setup -q
#%%patch0 -p1 -b .dbus-service-name


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
desktop-file-install                                            \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --add-only-show-in=XFCE                                 \
        --delete-original                                       \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/xfce4-notifyd-config.desktop
%find_lang %{name}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :



%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_bindir}/xfce4-notifyd-config
%{_libdir}/xfce4/notifyd/
%{_datadir}/applications/xfce4-notifyd-config.desktop
%{_datadir}/dbus-1/services/org.xfce.xfce4-notifyd.Notifications.service
%{_datadir}/icons/hicolor/48x48/apps/xfce4-notifyd.png
%{_datadir}/themes/Default/xfce-notify-4.0/
%{_datadir}/themes/Smoke/
%{_datadir}/themes/ZOMG-PONIES!/
%{_mandir}/man1/xfce4-notifyd-config.1.*



%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.2-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.1-1m)
- update
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-1m)
- import from Fedora to Momonga

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Feb 23 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-2
- Patch to rename dbus-service file to avoid conflict with notification-daemon
- Add Debian's patch support the reason arg in libnotify 0.4.5

* Mon Feb 23 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Initial Fedora Package
