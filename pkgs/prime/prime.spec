%global momorel 13
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: A Japanese PRedictive Input Method Editor
Name: prime
Version: 1.0.0.1
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://taiyaki.org/prime/
Source: http://prime.sourceforge.jp/src/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: prime-makefile.patch
Patch1: prime-1.0.0.1-ruby18.patch
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby18, ruby18-sary, suikyo-ruby18 >= 2.0.0.1, ruby18-progressbar
BuildRequires: ruby18

%description
PRIME predicts user's input words using the knowledge of natural
language and the history of user's operations, and reduces the
cost of typing by the user.  For example, if a user wants to input
"application" and types "ap" as the beginning characters of the word,
PRIME might predict some candidate words like "apple", "application",
"appointment", etc...  And then the user can input "application"
easily by selecting the word from the candidate words by PRIME.

%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1 -b .make
%patch1 -p1 -b .ruby18

%build
%configure --with-rubydir=%{ruby18_sitelibdir}
%make

%install
make DESTDIR=%{buildroot} rubydir=%{ruby18_sitelibdir} \
install install-etc

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO doc 
%doc %{_docdir}/%{name}
%config(noreplace) %{_sysconfdir}/prime/Custom_prime.rb
%{_bindir}/*
%{ruby18_sitelibdir}/prime
%{_libdir}/pkgconfig/*
%{_datadir}/prime

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.1-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.1-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.1-11m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.1-10m)
- build with ruby18

* Thu Aug  5 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.0.1-9m)
- fix %%files section for x86_64

* Wed Aug  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0.1-8m)
- rebuild against ruby-1.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.1-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.1-5m)
- rebuild against gcc43

* Tue Oct 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.1-4m)
- remove uim from Requires

* Thu Sep 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.1-3m)
- remove xim.d/PRIME

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.0.1-2m)
- /usr/lib/ruby

* Wed Mar 30 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.0.1-1m)
- up to 1.0.0.1

* Tue Mar  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6.1-1m)
- version 0.8.6.1

* Wed Dec 15 2004 TAKAHASHI Tamotsu <tamo>
- (0.8.5-1m)

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-3m)
- change xim.d/prime (add QT_IM_MODULE)

* Fri Nov  5 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.8.4-2m)
- change xim.d/prime (add GTK_IM_MODULE)

* Wed Sep 29 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.8.4-1m)
- version up

* Thu Jun 17 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.8.3-1m)
- version up

* Fri Mar  5 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.7.a.1-2m)
- rm Makefile in %%doc

* Wed Mar  3 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.7.a.1-1m)
- version up

* Wed Dec 31 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.7.2-1m)
- version up

* Tue Dec 16 2003 Masaki Yatsu <yatsu@digital-genes.com>
- (0.6.3-1)
- version up

* Mon Nov  3 2003 Masaki Yatsu <yatsu@digital-genes.com>
- (0.6.2-1)
- version up

* Sun May 18 2003 Masaki Yatsu <yatsu@borogrammers.net>
- (0.5.1-1 / 0.5.1-0pa1)
- version up

* Tue May 13 2003 Masaki Yatsu <yatsu@borogrammers.net>
- (0.5.0-0pa1)
- first build
