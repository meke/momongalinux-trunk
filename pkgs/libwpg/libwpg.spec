%global momorel 1


Name:           libwpg
Version:        0.2.2
Release:        %{momorel}m%{?dist}
Summary:        Library for reading WordPerfect Graphics images

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://libwpg.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/libwpg/libwpg-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libwpd-devel >= 0.9.9
BuildRequires:  doxygen

%description
Libwpg project is a library and to work with graphics in WPG
(WordPerfect Graphics) format. WPG is the format used among others
in Corel sofware, such as WordPerfect and Presentations.


%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package tools
Summary:        Tools to convert WordPerfect Graphics images
Group:          Applications/Multimedia

%description tools
This package contains tools to work with graphics in WPG (WordPerfect
Graphics) format. WPG is the format used among others in Corel sofware,
such as WordPerfect and Presentations.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}
sed 's/\r//' -i ChangeLog


%install
rm -rf $RPM_BUILD_ROOT
# Documentation is intentionally not installed here,
# it is included as -devel %%doc
make SUBDIRS="" install DESTDIR=$RPM_BUILD_ROOT
make -C src install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING.*
%{_libdir}/*.so.*


%files devel
%defattr(-,root,root,-)
%doc COPYING.* docs/doxygen/html
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc


%files tools
%defattr(-,root,root,-)
%doc COPYING.*
%{_bindir}/*


%changelog
* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-1m)
- update 0.2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.0-1m)
- update 0.2.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-4m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-1m)
- import from Fedora 11 for inkscape

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 6 2009 Lubomir Rintel <lkundrak@v3.sk> - 0.1.3-1
- Initial packaging
