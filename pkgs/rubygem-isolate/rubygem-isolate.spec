# Generated from isolate-3.2.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname isolate

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Isolate is a very simple RubyGems sandbox
Name: rubygem-%{gemname}
Version: 3.2.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/jbarnette/isolate
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.8.2
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.8.2
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Isolate is a very simple RubyGems sandbox. It provides a way to
express and automatically install your project's Gem dependencies.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/CHANGELOG.rdoc
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-1m)
- update 3.2.2

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-1m)
- update 3.2.0

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.2-3m)
- BR hoe-2.12

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.2-2m)
- rm .yardoc

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-1m)
- import from fedora for rubygem-rake-compiler
