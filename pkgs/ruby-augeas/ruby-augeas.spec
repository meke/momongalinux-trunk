%global momorel 1

Name:           ruby-augeas
Version:        0.4.1
Release:        %{momorel}m%{?dist}
Summary:        Ruby bindings for Augeas
Group:          Development/Languages

License:        LGPLv2+
URL:            http://augeas.net/
Source0:        ruby-augeas-%{version}.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ruby ruby-devel
BuildRequires:  augeas-devel >= 0.0.6
BuildRequires:  pkgconfig
Requires:       ruby(abi) = 1.9.1
Requires:       augeas-libs >= 0.9.0
Provides:       ruby(augeas) = %{version}

%description
Ruby bindings for augeas.

%prep
%setup -q


%build
export CFLAGS="$RPM_OPT_FLAGS"
rake build

%install
rm -rf %{buildroot}
install -d -m0755 %{buildroot}%{ruby_sitelibdir}
install -d -m0755 %{buildroot}%{ruby_sitearchdir}
install -p -m0644 lib/augeas.rb %{buildroot}%{ruby_sitelibdir}
install -p -m0755 ext/augeas/_augeas.so %{buildroot}%{ruby_sitearchdir}

%check
rake test || exit 0

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING README.rdoc
%{ruby_sitelibdir}/augeas.rb
%{ruby_sitearchdir}/_augeas.so


%changelog
* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-1m)
- update 0.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-2m)
- Requires: ruby(abi)-1.9.1

* Wed Aug  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-1m)
- update 0.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-1m)
- import from Fedora 11 for puppet

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Aug 26 2008 David Lutterkort <dlutter@redhat.com> - 0.2.0-1
- New version

* Fri May  9 2008 David Lutterkort <dlutter@redhat.com> - 0.1.0-1
- Fixed up in accordance with Fedora guidelines

* Mon Mar 3 2008 Bryan Kearney <bkearney@redhat.com> - 0.0.1-1
- Initial specfile
