%global momorel 5

# Generated from cobbler-0.0.1.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname cobbler
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}
%define installroot %{buildroot}%{geminstdir}

Summary: 	An interface for interacting with a Cobbler server
Name: 		rubygem-%{gemname}
Version: 	1.6.1
Release: 	%{momorel}m%{?dist}
Group: 		Development/Languages
License: 	LGPLv2+
URL: 		http://cobbler.et.redhat.com/
Source0: 	http://fedorapeople.org/~mcpierce/%{gemname}-%{version}.gem
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: 	rubygems
BuildRequires:  rubygem-flexmock >= 0.8.7-1m
BuildRequires: 	rubygems
BuildRequires:  rubygem-rake
BuildArch: 	noarch
Provides: 	rubygem(%{gemname}) = %{version}

%description
Provides Ruby bindings to interact with a Cobbler server.

%prep

%build

%check 

cd %{installroot}

rake test

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} --force %{SOURCE0}

chmod 0755 %{installroot}/examples/*.rb

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/COPYING
%doc %{geminstdir}/ChangeLog
%doc %{geminstdir}/NEWS
%doc %{geminstdir}/README
%doc %{geminstdir}/TODO

%{geminstdir}/config/cobbler.yml


%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.1-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-4m)
- rebuild against ruby19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- fix build

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-1m)
- sync with Fedora 11 (1.6.0-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-1m)
- Initial Commit Momonga Linux

* Mon Sep 08 2008 Darryl Pierce <dpierce@redhat.com> - 0.1.0-2
- Fix for broken build that was not finding the gem.

* Mon Sep 08 2008 Darryl Pierce <dpierce@redhat.com> - 0.1.0-1
- First official build for Fedora.

* Fri Sep 05 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.2-4
- Bad BuildRequires slipped into the last version.

* Wed Sep 03 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.2-3
- Added a build requirement for rubygem-rake.

* Tue Aug 26 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.2-2
- Fixed the licensing in each source module to show the code is released under
  LGPLv2.1.
- Added %check to the spec file to run tests prior to creating the RPM.

* Thu Aug 21 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.2-1
- Added a call to update prior to saving or updating a system. If the update
  fails, then an Exception is raised.

* Wed Aug 13 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.1-3
- Added caching for the auth_token to prevent extraneous calls to login.
- Reworked and refined how cobbler_collection fields are processed, adding 
  support for both array and has properties.
- Rewrote the documentation for Cobbler::Base to make it easier to understand
  how to extend it to support other Cobbler types.
- Refactored the examples to clean up the code.

* Wed Aug 13 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.1-2
- Removed markup of cobbler.yml and a config file. Fixed a few small bugs 
  in the code for using it as a gem.

* Mon Aug 04 2008 Darryl Pierce <dpierce@redhat.com> - 0.0.1-1
- Initial package
