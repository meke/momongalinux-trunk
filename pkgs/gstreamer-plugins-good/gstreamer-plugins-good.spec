%global         momorel 1

%global majorminor      0.10
%global _gst            0.10.36
%global _gstpb          0.10.36
%global srcname         gst-plugins-good

Name: 		gstreamer-plugins-good
Version: 	0.10.31
Release:	%{momorel}m%{?dist}
Summary: 	GStreamer plug-ins with good code and licensing

Group: 		Applications/Multimedia
License: 	LGPL
URL:		http://gstreamer.freedesktop.org/
Source0:	http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2 
NoSource:	0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: 	gstreamer >= %{_gst}
Requires:       dbus >= 1.2.4
Requires:       libxml2
Requires:       wavpack
Requires:       gstreamer-plugins-base
Requires(post): GConf2

BuildRequires: 	gstreamer-devel >= %{_gst}
BuildRequires: 	gstreamer-plugins-base-devel >= %{_gstpb}

BuildRequires:	liboil-devel >= 0.3.14
BuildRequires:  gettext
BuildRequires:  gcc-c++

BuildRequires:  cairo-devel
# cairo pulls in Xrender, but -devel has a missing dep on libX11-devel
BuildRequires:  libX11-devel
BuildRequires:  libcdio-devel
BuildRequires:  esound-devel >= 0.2.8
BuildRequires:  flac-devel >= 1.1.4
BuildRequires: 	GConf2-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  libpng-devel >= 1.2.0
BuildRequires:  mikmod
BuildRequires:  glibc-devel
BuildRequires:	speex-devel
BuildRequires:	autoconf
BuildRequires:	wavpack-devel
BuildRequires:	libshout-devel
BuildRequires:	taglib-devel
BuildRequires:  gnutls-devel >= 2.4.1

%ifnarch s390 s390x
BuildRequires:  libdv-devel
BuildRequires:  libraw1394-devel >= 2.0.2
BuildRequires:  libavc1394-devel
BuildRequires:  libiec61883-devel
%endif

# documentation
BuildRequires:  gtk-doc
BuildRequires:  PyXML

Obsoletes: gst-plugins-good

%description
GStreamer is a streaming media framework, based on graphs of filters which
operate on media data. Applications using this library can do anything
from real-time sound processing to playing videos, and just about anything
else media-related.  Its plugin-based architecture means that new data
types or processing capabilities can be added simply by installing new
plug-ins.

GStreamer Good Plug-ins is a collection of well-supported plug-ins of
good quality and under the LGPL license.

%package devel
Summary:        Documentation for GStreamer Good Plug-ins
Group:          Development/Libraries

Requires:       %{name} = %{version}-%{release}
Obsoletes:      gst-plugins-good-devel

%description devel
GStreamer is a streaming media framework, based on graphs of filters which
operate on media data. Applications using this library can do anything
from real-time sound processing to playing videos, and just about anything
else media-related.  Its plugin-based architecture means that new data
types or processing capabilities can be added simply by installing new
plug-ins.

GStreamer Good Plug-ins is a collection of well-supported plug-ins of
good quality and under the LGPL license.

This package contains documentation for the GStreamer Good Plug-ins.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure \
    --enable-gtk-doc \
    --disable-aalib \
    --disable-libcaca \
    --disable-sunaudio \
    --disable-schemas-install \
    --enable-experimental
%make
                                                                                
%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/gstreamer-%{majorminor}.schemas > /dev/null

%preun
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gstreamer-%{majorminor}.schemas > /dev/null

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README REQUIREMENTS
%{_libdir}/gstreamer-%{majorminor}/libgstalaw.so
%{_libdir}/gstreamer-%{majorminor}/libgstalpha.so
%{_libdir}/gstreamer-%{majorminor}/libgstalphacolor.so
%{_libdir}/gstreamer-%{majorminor}/libgstaudioparsers.so
%{_libdir}/gstreamer-%{majorminor}/libgstauparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstautodetect.so
%{_libdir}/gstreamer-%{majorminor}/libgstavi.so
%{_libdir}/gstreamer-%{majorminor}/libgstcutter.so
%{_libdir}/gstreamer-%{majorminor}/libgstdebug.so
%{_libdir}/gstreamer-%{majorminor}/libgstefence.so
%{_libdir}/gstreamer-%{majorminor}/libgsteffectv.so
%{_libdir}/gstreamer-%{majorminor}/libgstequalizer.so
%{_libdir}/gstreamer-%{majorminor}/libgstflxdec.so
%{_libdir}/gstreamer-%{majorminor}/libgstgoom.so
%{_libdir}/gstreamer-%{majorminor}/libgstisomp4.so
%{_libdir}/gstreamer-%{majorminor}/libgstlevel.so
%{_libdir}/gstreamer-%{majorminor}/libgstmatroska.so
%{_libdir}/gstreamer-%{majorminor}/libgstmulaw.so
%{_libdir}/gstreamer-%{majorminor}/libgstmultifile.so
%{_libdir}/gstreamer-%{majorminor}/libgstmultipart.so
%{_libdir}/gstreamer-%{majorminor}/libgstnavigationtest.so
%{_libdir}/gstreamer-%{majorminor}/libgstrtp.so
%{_libdir}/gstreamer-%{majorminor}/libgstrtsp.so
%{_libdir}/gstreamer-%{majorminor}/libgstsmpte.so
%{_libdir}/gstreamer-%{majorminor}/libgstspectrum.so
%{_libdir}/gstreamer-%{majorminor}/libgstudp.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideobox.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideomixer.so
%{_libdir}/gstreamer-%{majorminor}/libgstwavenc.so
%{_libdir}/gstreamer-%{majorminor}/libgstwavparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstid3demux.so
%{_libdir}/gstreamer-%{majorminor}/libgstcairo.so
%{_libdir}/gstreamer-%{majorminor}/libgstesd.so
%{_libdir}/gstreamer-%{majorminor}/libgstflac.so
%{_libdir}/gstreamer-%{majorminor}/libgstjpeg.so
%{_libdir}/gstreamer-%{majorminor}/libgstpng.so
%{_libdir}/gstreamer-%{majorminor}/libgstossaudio.so
%{_libdir}/gstreamer-%{majorminor}/libgstspeex.so
%{_libdir}/gstreamer-%{majorminor}/libgstgconfelements.so
%{_libdir}/gstreamer-%{majorminor}/libgstannodex.so
%{_libdir}/gstreamer-%{majorminor}/libgstgdkpixbuf.so
%{_libdir}/gstreamer-%{majorminor}/libgsticydemux.so
%{_libdir}/gstreamer-%{majorminor}/libgsttaglib.so
%{_libdir}/gstreamer-%{majorminor}/libgstximagesrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstapetag.so
%{_libdir}/gstreamer-%{majorminor}/libgstshout2.so
%{_libdir}/gstreamer-%{majorminor}/libgstaudiofx.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideocrop.so
%{_libdir}/gstreamer-%{majorminor}/libgstwavpack.so
%{_libdir}/gstreamer-%{majorminor}/libgstmonoscope.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideo4linux2.so
%{_libdir}/gstreamer-%{majorminor}/libgstgoom2k1.so
%{_libdir}/gstreamer-%{majorminor}/libgstsouphttpsrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstinterleave.so
%{_libdir}/gstreamer-%{majorminor}/libgstpulse.so
%{_libdir}/gstreamer-%{majorminor}/libgstreplaygain.so
%{_libdir}/gstreamer-%{majorminor}/libgstrtpmanager.so
%{_libdir}/gstreamer-%{majorminor}/libgstdeinterlace.so
%{_libdir}/gstreamer-%{majorminor}/libgstflv.so
%{_libdir}/gstreamer-%{majorminor}/libgsty4menc.so
%{_libdir}/gstreamer-%{majorminor}/libgstshapewipe.so
%{_libdir}/gstreamer-%{majorminor}/libgstimagefreeze.so
%{_libdir}/gstreamer-%{majorminor}/libgstoss4audio.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideofilter.so
%{_libdir}/gstreamer-%{majorminor}/libgstjack.so
%{_libdir}/gstreamer-%{majorminor}/*.la

%ifnarch s390 s390x
%{_libdir}/gstreamer-%{majorminor}/libgstdv.so
%{_libdir}/gstreamer-%{majorminor}/libgst1394.so
%endif

# locale
%{_datadir}/locale/*/*/*

# schema files
%{_sysconfdir}/gconf/schemas/gstreamer-%{majorminor}.schemas

# documents
%doc %{_datadir}/gtk-doc/html/gst-plugins-good-plugins-%{majorminor}

# new file (0.10.14)
%{_datadir}/gstreamer-%{majorminor}/presets/GstIirEqualizer10Bands.prs
%{_datadir}/gstreamer-%{majorminor}/presets/GstIirEqualizer3Bands.prs

%changelog
* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.31-1m)
- update to 0.10.31

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.30-4m)
- remove hal support

* Thu Feb  2 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.30-2m)
- add BuildRequires

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30-1m)
- update to 0.10.30

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.29-1m)
- update to 0.10.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.28-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.28-1m)
- update to 0.10.28

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.27-1m)
- update to 0.10.27

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.25-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.25-1m)
- update to 0.10.25

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.24-3m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.10.24-2m)
- add Requires: gstreamer-plugins-base

* Sat Jul 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.24-1m)
- update to 0.10.24

* Wed Jun  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.23-1m)
- update to 0.10.23

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.22-1m)
- update to 0.10.22

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.21-1m)
- update to 0.10.21

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.18-2m)
- rebuild against libjpeg-8a

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18

* Thu Nov 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.17-1m)
- update to 0.10.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.16-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.16-1m)
-  rebuild against libjpeg-7

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16
-- delete patch0 (merged)

* Fri Jul 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.15-3m)
- [SECURITY] CVE-2009-1932
- import upstream patch (Patch0)

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.15-2m)
- rebuild against libraw1394-2.0.2

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.15-1m)
- update to 0.10.15

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.14-1m)
- update to 0.10.14

* Mon Jan 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13
- delete patch0 (merged)

* Fri Jan 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-3m)
- [SECURITY] CVE-2009-0386 CVE-2009-0387 CVE-2009-0397
- add patch0 from upstream

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.11-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Thu Oct 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.8-3m)
- rename gst-plugins-good to gstreamer-plugins-good

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.8-2m)
- rebuild against gnutls-2.4.1

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.7-3m)
- rebuild against gcc43

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-2m)
- add expermental modules

* Wed Mar 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7
- delete patch1 (merged)

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.6-5m)
- %%NoSource -> NoSource

* Sun Nov 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-4m)
- add patch0 (for gstreamer-0.10.15)

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.6-3m)
- rebuild against libvorbis-1.2.0-1m

* Fri Jul 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-2m)
- add wavpack module

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.5-3m)
- rebuild against flac-1.1.4
- import gst-plugins-good-0.10.5-flac-1.1.3.patch from cooker
 +* Sat Dec 30 2006 Gz Waschk <waschk@mandriva.org> 0.10.5-1mdv2007.0
 ++ Revision: 102876
 +- rediff the flac patch
 +* Mon Dec 11 2006 Gz Waschk <waschk@mandriva.org> 0.10.4-3mdv2007.1
 +- patch for new flac

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-2m)
- roll back dbus-0.92

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5
- delete libtool library

* Sun Dec 17 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.4-2m)
- add some buildrequires; libiec61883-devel, libcdio-devel
- disable some plugins explicitly; aasink, cacasink and sunaudio

* Thu Oct 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-2m)
- rebuild against dbus 0.92

* Sat May  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Sun Feb 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- import from FC
- delete devel package

* Fri Feb 10 2006 Christopher Aillon <caillon@redhat.com> - 0.10.2-1
- Update to 0.10.2

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.10.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 John (J5) Palmieri <johnp@redhat.com>  0.10.1-1
- Upgrade to 0.10.1
- Add libgstid3demux.so to the files section

* Wed Jan 04 2006 Warren Togami <wtogami@redhat.com>  0.10.0-2
- exclude 1394 stuff from s390 and s390x

* Sat Dec 17 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.0-1
- rebuilt for FC devel

* Wed Dec 14 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.0-0.gst.2
- glib 2.8
- added cairo

* Mon Dec 05 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.0-0.gst.1
- new release

* Thu Dec 01 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.7-0.gst.1
- new release for major/minor 0.10
- removed pango
- removed videofilter
- added cutter, multipart

* Sat Nov 12 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.5-0.gst.1
- new release

* Mon Oct 24 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.4-0.gst.1
- new release
- added alphacolor, debug, flxdec, matroska, navigationtest, videomixer
  plug-ins

* Mon Oct 03 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.3-0.gst.1
- new release
- fdsrc moved back to core
- added auparse and efence plugins
- added gtk-doc

* Fri Sep 09 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- Initial package

* Fri Sep 02 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- clean up for splitup
