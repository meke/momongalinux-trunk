%global momorel 1

Name:		tokyocabinet
Summary:	DBM for Petit Hackers
Version:	1.4.48
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Libraries
URL:		http://fallabs.com/tokyocabinet/
Source0:	http://fallabs.com/tokyocabinet/%{name}-%{version}.tar.gz 
NoSource:       0

# BuildRequires:	qdbm-devel >= 1.8.75, mecab-devel
# Requires:	qdbm >= 1.8.75, mecab
# Requires:         chkconfig,shadow-utils
BuildRequires: zlib-devel, bzip2-devel

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Tokyo Cabinet is a library of routines for managing a database.

%prep
%setup -q

%build
%configure --libexecdir=%{_libdir}
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*
%{_includedir}/*
%{_libdir}/lib*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/tcawmgr.cgi
%{_mandir}/*/*
%{_datadir}/%{name}

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.48-1m)
- update 1.4.48

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.47-1m)
- update 1.4.47

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.46-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.46-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.46-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.46-1m)
- update 1.4.46

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.45-1m)
- update 1.4.45

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.35-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.35-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.35-1m)
- update to 1.4.35

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.34-1m)
- update to 1.4.34

* Thu Aug 13 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.31-1m)
- update to 1.4.31

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.27-1m)
- update to 1.4.27

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.22-1m)
- update to 1.4.22
- License: LGPLv2+

* Fri May  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.19-1m)
- update to 1.4.19

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-2m)
- rebuild against rpm-4.6

* Mon Sep  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.4-1m)
- update to 1.3.4

* Mon Jun 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8
- add --libexecdir=%%{_libdir} at configure for %%{_libdir}/tcawmgr.cgi

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc43

* Thu Feb 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- update

* Thu Feb 21 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.15-1m)
- update

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-2m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-1m)
- update

* Fri Sep  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-2m)
- own %%{_datadir}/%%{name}

* Wed Sep  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-1m)
- initial package
