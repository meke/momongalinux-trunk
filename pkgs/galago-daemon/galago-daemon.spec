%global momorel 10

Summary: galago-daemon
Name: galago-daemon
Version: 0.5.1
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.12.1
BuildRequires: libgalago-devel >= 0.5.2
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-glib-devel >= 0.71

%description
galago-daemon

%prep
%setup -q

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/dbus-1/system.d/galago-daemon.conf
%{_libexecdir}/%{name}
%{_datadir}/dbus-1/services/org.freedesktop.Galago.service

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-10m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-2m)
- %%NoSource -> NoSource

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initila build
