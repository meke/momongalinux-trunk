%global momorel 7
%global _sysconfdir /etc/X11
%global _libexecdir /usr/libexec/mlterm
%global pixmapdir  /usr/share/pixmaps

Summary:     Multi Lingual TERMinal emulator for X
Name:        mlterm

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/mlterm.specopt and edit it.

## Configuration
%{?!w3mopt:           %global w3mopt           1}
%{?!build_uim:        %global build_uim        1}

Version:     3.0.1
Release:     %{momorel}m%{?dist}
License:     Modified BSD
Group:       User Interface/X
URL:         http://mlterm.sourceforge.net/
Source0:     http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource:    0
Patch1:      %{name}-%{version}-xim.patch
Patch4:	     %{name}-%{version}-glibc28.patch
Patch10:     %{name}-%{version}-linking.patch
Requires:    freetype2 >= 2.0.2, fribidi, gtk2
Requires:    fontconfig, xorg-x11-server-Xorg
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXcursor-devel
BuildRequires: libXext-devel, libXfixes-devel, libXft-devel, libXi-devel
BuildRequires: libXinerama-devel, libXrandr-devel, libXrender-devel
BuildRequires: freetype-devel >= 2.3.1-2m
BuildRequires: fribidi-devel >= 0.10.4-12m
BuildRequires: fontconfig-devel
BuildRequires: expat-devel
BuildRequires: autoconf
%if %{build_uim}
BuildRequires: uim-devel >= 1.8.4
%endif

%description
mlterm is a multi-lingual terminal emulator written from
scratch, which supports various character sets and encodings
in the world.  It also supports various unique feature such as
anti-alias using FreeType, multiple windows, scrollbar API,
scroll by mouse wheel, automatic selection of encoding,
and so on. Multiple xims are also supported. 
You can dynamically change various xims.

%prep
%setup -q
find doc -name '*.cvs' | xargs rm -f
%patch1 -p1 -b .xim
rm -rf doc/{en,ja,term}/CVS

%patch4 -p1 -b .glibc28~
%patch10 -p1 -b .link

%build
DEBUGOPT=""
[ "x${OMOI_KONDARA_DEBUG}" = "x1" ] && DEBUGOPT="--enable-debug"
export CFLAGS="%{optflags} $(freetype-config --cflags)"
%configure --with-imagelib=gdk-pixbuf2 \
           --enable-anti-alias \
           --enable-fribidi \
           --disable-static \
%if %{build_uim}
           --enable-uim \
%endif
           --enable-scim \
           --enable-iiimf \
%if %{w3mopt}
           --enable-optimize-redrawing \
%endif
           $DEBUGOPT
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# regular icons
mkdir -p %{buildroot}%{pixmapdir}
install -m 644 doc/icon/mlterm* %{buildroot}%{pixmapdir}

# icons for fvwm, gnome, gnome2, kde and twm
mkdir -p %{buildroot}%{pixmapdir}/mlterm
rm -rf contrib/icon/CVS
### distributing this icon seems to require mail to marco@windowmaker.org
rm -f  contrib/icon/mlterm-icon-wmaker.png
install -c -D -m 644 contrib/icon/* %{buildroot}%{pixmapdir}/mlterm

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog LICENCE README doc/{en,ja,term}
%{_bindir}/mlterm
%{_bindir}/mlcc
%{_bindir}/mlclient
%{_bindir}/mlclientx
%{_libdir}/libkik.*
%{_libdir}/libmkf.*
%{_libdir}/mkf/
%{_libdir}/mlterm/
%{_libexecdir}/
%{_sysconfdir}/mlterm/
%{_mandir}/man1/mlterm.1*
%{_mandir}/man1/mlclient.1*
%{pixmapdir}/mlterm*
%{_datadir}/locale/*/*/*

%changelog
* Sun Jan 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-7m)
- rebuild against uim-1.8.4

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-6m)
- fix require:

* Wed May 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-5m)
- rebuild against uim-1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-2m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.4-8m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.4-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.9.4-5m)
- add libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.4-4m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.4-3m)
- update Patch1 for fuzz=0

* Tue Jun  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.4-2m)
- build with uim again

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.4-1m)
- version 2.9.4
- build without uim-1.5.0 for the moment

* Wed Apr  9 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.3-8m)
- add patch for glibc-2.7.90

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.3-7m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.3-6m)
- %%NoSource -> NoSource

* Sat Jan 12 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.3-5m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.3-4m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.3-3m)
- delete libtool library

* Wed Feb  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.3-2m)
- rebuild against uim-1.4.0

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.3-1m)
- update to 2.9.3

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.9.2-6m)
- rebuild against expat-2.0.0-1m

* Sat Aug  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.2-5m)
- rebuild against uim-1.2.0

* Sat Jun 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.2-4m)
- rebuild against uim-1.1.0

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.9.2-3m)
- revised installdir

* Wed Sep 15 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.9.2-2m)
- support SCIM

* Sat Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.2-1m)
- update to 2.9.2

* Sat Feb  5 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.9.0-3m)
- enable x86_64.

* Tue Nov 16 2004 TAKAHASHI Tamotsu <tamo>
- (2.9.0-2m)
- update to the current CVS version (minor bugfixes)

* Thu Nov  4 2004 TAKAHASHI Tamotsu <tamo>
- (2.9.0-1m)
- update
- add REMOVE.PLEASE to avoid requring old libkik
- stop overwriting __libtoolize
- enable-optimize-redrawing by default
- enable-uim
- - BuildPreReq: uim-devel >= 0.4.4
-  (for uim_get_default_im_name)
- remove viewsfml

* Fri May 07 2004 TAKAHASHI Tamotsu <tamo>
- (2.8.0-5m)
- today's CVS version (I'm sure this is the most stable ever)
- add BuildPreReq: expat-devel, autoconf
- enable optimize-redrawing if w3mopt is 1 (default: 0)
- add "if (d == NULL) return 1;" to xim patch
 [Momonga-devel.ja:02517]

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (2.8.0-4m)
- revised spec for enabling rpm 4.2.

* Tue Jan 20 2004 TAKAHASHI Tamotsu <tamo>
- (2.8.0-3m)
- fixed: "mlclient -e w3m -v" was equivalent to "mlclient -v"

* Mon Nov 03 2003 TAKAHASHI Tamotsu <tamo>
- (2.8.0-1m)
- this version contains many bug fixes and optimization for w3m-img
- source0 seems not on telia. use umn
- add locale data
- add viewsfml as doc (if you want to install it, configure
 "--with-tools=mlclient,mlconfig,viewsfml,mlterm-menu,mlcc")
- use freetype-config
- Req: fontconfig
- global momorel
- use global instead of define
- note: w3mmlconfig is not installed by this spec
- note2: wrong fonts in ".mlterm/font" file
 can cause strange behaviour.

* Sat Jun 14 2003 TAKAHASHI Tamotsu <tamo>
- (2.7.0-1m)
- this version fixes a possibility of security weakness

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.3-3m)
- rebuild against for XFree86-4.3.0

* Sun Jan 26 2003 TAKAHASHI Tamotsu
- (2.6.3-2m)
- cvs 20030126
- mlconfig bug and 16bit-depth transparent bug are fixed

* Wed Jan 15 2003 TAKAHASHI Tamotsu
- (2.6.3-1m)

* Wed Oct 09 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (2.6.2-1m)
- minor bugfixes

* Fri Sep 13 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (2.6.1-1m)
- minor bugfixes

* Mon Sep 09 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (2.6.0-1m)
- a few bugfixes ported from cvs
- mlterm-icon-wmaker.png removed (read README in %{pixmapdir}/mlterm)
- more macro used
- "--imagelib=gdk-pixbuf2" used instead of "--enable-gdk-pixbuf"
- doc/term added as doc (i don't know anything about terminfo)

* Fri Sep 06 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (2.5.0-4m)
- copy_lines bug fixed (modifying mlterm-cvs.patch)

* Wed Sep 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (2.5.0-3m)
- cvs 20020904 (icons added [as source1] -> /usr/share/pixmap/mlterm/* )
- no longer imlib, but gdk-pixbuf used
- very close to 2.6.0

* Mon Jun 17 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (2.5.0-2k)
- just a verup
- note: maxpty is obsolete

* Wed May 22 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (2.4.0-6k)
- enabled fribidi

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.4.0-2k)
- rebuild against imlib 1.9.14-4k (implies libpng >= 1.2.2)

* Wed Apr 17 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (2.4.0-2k)
- while OmoiKondara -G, add some option
- sumaso m(__)m

* Fri Mar 22 2002 Toru Hoshina <t@kondara.org>
- (2.3.1-2k)
- based on 2.3.1 official release.
- add famao's xim-kondara patch.

* Sat Feb 16 2002 Hiroyuki Ikezoe <zoe@kasumi.sakura.ne.jp>
- CVS(20020216) version

* Tue Jan 29 2002 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 2.2.0

* Wed Jan 2 2002 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 2.1.2

* Sun Dec 30 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 2.1.1

* Sat Dec 29 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 2.1.0

* Thu Nov 29 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 2.0.0

* Mon Nov 26 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 1.9.47

* Sat Nov 24 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 1.9.46

* Fri Nov 23 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 1.9.45

* Sat Nov 17 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 1.9.44

* Wed Nov 14 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 1.9.43

* Tue Nov 13 2001 Araki Ken <j00v0113@ip.media.kyoto-u.ac.jp>
- Source version 1.9.42pl6
