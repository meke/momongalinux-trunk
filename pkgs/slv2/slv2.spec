%global momorel 6

Summary: A library for simple use of LV2 plugins
Name: slv2
Version: 0.6.6
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://drobilla.net/software/slv2/
Group: System Environment/Libraries
Source0: http://download.drobilla.net/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: doxygen
BuildRequires: jack-devel
BuildRequires: liblrdf-devel
BuildRequires: libtool
BuildRequires: lv2core-devel
BuildRequires: pkgconfig
BuildRequires: python
BuildRequires: raptor2-devel
BuildRequires: rasqal-devel
BuildRequires: redland-devel
BuildRequires: sed

%description
Files required for compiling programs which use SLV2, and developer
documentation.

%package devel
Summary: Header files and static libraries from SLV2
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: lv2core-devel
Requires: pkgconfig
Requires: redland-devel

%description devel
Libraries and includes files for developing programs based on SLV2.

%prep
%setup -q

# fix possible multilib issues
sed -i 's|/lib/|/%{_lib}/|g' src/world.c
sed -i "s|/lib'|/%{_lib}'|" autowaf.py

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

./waf -j1 configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--build-docs \
	--htmldir=%{_docdir}/%{name}-devel-%{version}

./waf -j1 build --verbose

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
export DESTDIR=%{buildroot}

./waf -j1 install --verbose

# fix up permission
chmod 755 %{buildroot}%{_libdir}/lib%{name}.so.*

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_bindir}/lv2_inspect
%{_bindir}/lv2_jack_host
%{_bindir}/lv2_list
%{_bindir}/lv2_simple_jack_host
%{_libdir}/lib%{name}.so.*
%{_mandir}/man1/lv2_inspect.1*
%{_mandir}/man1/lv2_jack_host.1*
%{_mandir}/man1/lv2_list.1*
%{_mandir}/man1/lv2_simple_jack_host.1*

%files devel
%defattr(-,root,root)
%doc %{_docdir}/%{name}-devel-%{version}
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.so
%{_mandir}/man3/%{name}.3*
%{_mandir}/man3/%{name}_collections.3*
%{_mandir}/man3/%{name}_data.3*
%{_mandir}/man3/%{name}_library.3*
%{_mandir}/man3/%{name}_util.3*
%{_mandir}/man3/%{name}_world.3*

%changelog
* Tue Jun 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-6m)
- add -j1 to avoid a possible dead lock with python-2.7.2

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.6-5m)
- use raptor2 instea of raptor

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-2m)
- full rebuild for mo7 release

* Mon Jan 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.6-1m)
- initial package for qtractor-0.4.4
