%global momorel 2

%global qtver 4.8.5
%global qtdir %{_libdir}/qt4
%global pjname PyQt
%global srcname %{pjname}-x11-gpl
%global src_ver 4.10
%global minor .3
#global snapshot_date 20100110
%global with_python3 1

Summary: Python bindings for Qt4
Name: 	 PyQt4
Version: %{src_ver}%{?minor:%{minor}}
Release: %{?snapshot_date:0.%{snapshot_date}.}%{momorel}m%{?dist}

License: GPLv2
Group: 	 Development/Languages
Url: 	 http://www.riverbankcomputing.co.uk/
Source0: http://dl.sourceforge.net/project/pyqt/%{name}/%{pjname}-%{version}/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# HACK! FIXME: ping upstream why this isn't working right. -- Rex
Patch2:  PyQt-x11-gpl-4.5.2-QT_SHARED.patch
Patch4:  PyQt-x11-gpl-4.10-pyuic_shebang.patch
# fix multilib conflict because of timestamp
Patch5:  PyQt-x11-gpl-4.9.5-timestamp-multilib.patch

BuildRequires: qt-devel >= %{qtver}
BuildRequires: qt-assistant-adp-devel
BuildRequires: qt-webkit-devel >= %{qtver}
BuildRequires: chrpath
BuildRequires: dbus-devel dbus-python-devel
BuildRequires: qscintilla-python >= 2.7.2-1m
BuildRequires: findutils

BuildRequires: python-devel >= 2.7
%global python_ver %(%{__python} -c "import sys ; print sys.version[:3]")
%global python_sitelib  %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

%if 0%{?with_python3}
BuildRequires: python3-devel >= 3.4
BuildRequires: python3-sip-devel >= 4.15.2
%endif # with_python3

BuildRequires: sip-devel >= 4.15.2
%global sip_ver %(sip -V 2>/dev/null | cut -d' ' -f1)
%if "%{?sip_ver}" > "3"
# To be paranoid, could change >= to = -- Rex
Requires: sip >= %{sip_ver}
%endif

Requires: dbus-python

%description
These are Python bindings for Qt4.

%package devel
Summary: Files needed to build other bindings based on Qt4
Group:	 Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: qt-devel >= %{qtver}
Requires: sip-devel
%description devel
Files needed to build other bindings for C++ classes that inherit from any
of the Qt4 classes (e.g. KDE or your own).

# The bindings are imported as "PyQt4", hence it's reasonable to name the
# Python 3 subpackage "python3-PyQt4", despite the apparent tautology
%package -n python3-PyQt4
Summary: Python 3 bindings for Qt4
Group:   Development/Languages
# The dbus Python bindings have not yet been ported to Python 3:
# Requires: dbus-python
%{?_qt4_version:Requires: qt >= %{qtver}}

%description -n python3-PyQt4
These are Python 3 bindings for Qt4.

%package -n python3-PyQt4-devel
Summary: Python 3 bindings for Qt4
Group:   Development/Languages
Requires: python3-PyQt4 = %{version}-%{release}
Requires: python3-sip-devel

%description -n python3-PyQt4-devel
Files needed to build other Python 3 bindings for C++ classes that inherit
from any of the Qt4 classes (e.g. KDE or your own).

%prep
%setup -q -n PyQt-x11-gpl-%{version}%{?snapshot_date:-snapshot-%{snapshot_date}}

%patch2 -p1 -b .QT_SHARED
%patch4 -p1 
%patch5 -p1 -b .timestamp

# mark examples non-executable
find examples/ -name "*.py" | xargs chmod a-x

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif # with_python3

%build

QT4DIR=%{qtdir}
PATH=%{qtdir}/bin:$PATH ; export PATH

# configure.py isn't smart enough to either use qmake or QMAKEPATH
QMAKESPEC=$QT4DIR/mkspecs/linux-g++; export QMAKESPEC
%if "%{_lib}" == "lib64"
QMAKESPEC=$QT4DIR/mkspecs/linux-g++-64 ; export QMAKESPEC
%endif

echo yes | %{__python} configure.py --verbose 

make %{?_smp_mflags}

# Python 3 build:
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} configure.py \
  --assume-shared \
  --confirm-license \
  --qmake=%{_qt4_qmake} \
  --verbose 

make %{?_smp_mflags}
popd
%endif # with_python3

%install
rm -rf %{buildroot}

InstallPyQt4() {
  PySiteArch=$1

  make install DESTDIR=%{buildroot} INSTALL_ROOT=%{buildroot}

  # fix/remove rpaths
  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtCore.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtCore.so ||:

  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtGui.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtGui.so ||:

  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtDesigner.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtDesigner.so ||:
}

# Install Python 3 first, and move aside any executables, to avoid clobbering
# the Python 2 installation:
%if 0%{?with_python3}
pushd %{py3dir}
InstallPyQt4 %{python3_sitearch}
popd
%endif # with_python3

InstallPyQt4 %{python_sitearch}

# DBus bindings only work for Python 2 so far:
chrpath --list   %{buildroot}%{python_sitelib}/dbus/mainloop/qt.so && \
chrpath --delete %{buildroot}%{python_sitelib}/dbus/mainloop/qt.so ||:

# non-executable script
chmod 755 %{buildroot}%{python_sitearch}/PyQt4/uic/pyuic.py

# missing shebang
cp -a %{buildroot}%{_bindir}/pyuic4 %{buildroot}%{_bindir}/pyuic4.ORIG
cat > %{buildroot}%{_bindir}/pyuic4 <<EOF
#!/bin/sh

EOF
cat %{buildroot}%{_bindir}/pyuic4.ORIG >> %{buildroot}%{_bindir}/pyuic4
rm -f %{buildroot}%{_bindir}/pyuic4.ORIG

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc GPL_EXCEPTION* LICENSE* NEWS OPENSOURCE-NOTICE.TXT README THANKS
%{python_sitearch}/PyQt4/
%{python_sitelib}/dbus/mainloop/qt.so

%files devel
%defattr(-,root,root,-)
%doc doc/*
%doc examples/
%{_bindir}/*
%{_datadir}/sip/PyQt4/
%{qtdir}/plugins/designer/*

%if 0%{?with_python3}
%files -n python3-PyQt4
%defattr(-,root,root,-)
%doc NEWS README
%doc OPENSOURCE-NOTICE.TXT
%doc LICENSE.GPL2 GPL_EXCEPTION*.TXT
%doc LICENSE.GPL3
%{python3_sitearch}/PyQt4/
%exclude %{python3_sitearch}/PyQt4/uic/pyuic.py*

%files -n python3-PyQt4-devel
%defattr(-,root,root,-)
%doc doc/*
%doc examples/
%{python3_sitearch}/PyQt4/uic/pyuic.py*
%{_datadir}/python3-sip/PyQt4/
%{_qt4_prefix}/qsci/api/python/PyQt4.api
%endif

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.10.3-2m)
- rebuild against python-3.4

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to 4.10.3

* Thu Jun 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to 4.10.2

* Mon Apr 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to 4.10.1

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10-1m)
- update to 4.10

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.6-1m)
- update to 4.9.6

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.5-1m)
- update to 4.9.5

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.4-1m)
- update to 4.9.4

* Fri Jun 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to 4.9.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to 4.9.1

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9-1m)
- update to 4.9

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.6-1m)
- update to 4.8.6

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.5-2m)
- rebuild against python-3.2

* Thu Aug 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.5-1m)
- update to 4.8.5

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.8.4-2m)
- rebuild against python-2.7

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to 4.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.3-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to 4.8.3

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to 4.8.2

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-3m)
- enable python3 bindings

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.1-2m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to 4.8.1

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8-1m)
- update to 4.8
- Patch7 was merged to upstream

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.7-2m)
- import a bug fix patch from upstream
-- see https://bugs.kde.org/show_bug.cgi?id=252366

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.7-1m)
- update to 4.7.7

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.6-2m)
- specify sip version

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.6-1m)
- update to 4.7.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.4-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.4-1m)
- update to 4.7.4

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-4m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-3m)
- rebuils against qt-4.7.0
- import 2 patches from Fedora devel

* Thu Apr 29 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.3-2m)
- strict BuildRequires: qscintilla-python >= 2.4.3-1m

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to 4.7.3

* Sat Mar 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to 4.7.2

* Fri Feb 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-3m)
- revive BuildRequires: qscintilla-python

* Fri Feb 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-2m)
- drop BuildRequires: qscintilla-python

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to 4.7 official release

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.20100110.3m)
- update snapshot source

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.20100110.2m)
- no NoSource again and again...

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.20100110.1m)
- update to 4.7 snapshot 20100110
- NoSource again

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.20100108.2m)
- no NoSource

* Sun Jan 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.0-0.20100108.1m)
- update to 4.7 snapshot 20100108

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.20091231.1m)
- update to 4.7 snapshot 20091231

* Mon Nov 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.2-2m)
- rebuild against qt-4.6.0

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to 4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to 4.6.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.4-1m)
- update to 4.5.4

* Wed Jun 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-3m)
- no NoSource

* Thu Jun 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- BuildRequires: sip-devel >= 4.8.0

* Fri Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to 4.5

* Fri Jun  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20090604.1m)
- update to 4.5 snapshot 20090604

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20090601.1m)
- update to 4.5 snapshot 20090601

* Fri May 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20090527.1m)
- update to 4.5 snapshot 20090527

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20090525.1m)
- update to 4.5 snapshot 20090525 again

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-6m)
- version down to 4.4.4

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20090525.1m)
- update to 4.5 snapshot 20090525

* Mon May 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20090518.1m)
- update to 4.5 snapshot 20090518

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.4-3m)
- update Patch3 for fuzz=0

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.4.4-2m)
- rebuild against python-2.6.1-1m

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to 4.4.4

* Sat Aug 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-1m)
- update to 4.4.3

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-2m)
- rebuild against opessl-0.9.8h-1m

* Thu May 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to 4.4.2

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-4m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.3-3m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-2m)
- modify %%files

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- import from Fedora devel

* Wed Dec 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.3.3-1
- PyQt-4.3.3

* Thu Nov 22 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.3.1-3
- dbus support (#395741)

* Mon Nov 12 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.3.1-1
- PyQt-4.3.1

* Thu Oct 04 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-8
- drop ExcludeArch: ppc64 , qt4 bug is (hopefully) fixed.

* Thu Oct 04 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-7
- fix QtDesigner plugin install

* Wed Oct 03 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-6
- 64bit QtDesigner patch

* Mon Aug 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-5
- -devel: Requires: qt4-devel

* Sun Aug 26 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-4
- use %%python_sitearch
- License: GPLv2

* Thu Aug 02 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-3
- fix python_sitelib typo (wrt chrpath call)
- move %%_bindir stuff to -devel
- mark %%doc examples non-executable
- add shebang to %%_bindir/pyuic4

* Tue Jul 17 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-2
- remove rpath from QtDesigner.so
- BR: qt4-devel > 4.3.0-8

* Wed Apr 11 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.2-1
- PyQt4-4.2

* Wed Feb 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 4.1.1-2
- fix build against multilib'd qt4

* Mon Dec 11 2006 Rex Dieter <rexdieter[AT]usres.sf.net> 4.1.1-1
- PyQt4-4.1.1
- BR: sip-devel >= 4.5.1

* Mon Nov 06 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1-1
- PyQt4-4.1

* Wed Oct 04 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0.1-4
- don't own %%_datadir/sip (bug #206633)

* Mon Aug 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0.1-3
- BR: qt4-devel < 4.2

* Sat Jul 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0.1-2
- fix reference(s) to qmake(4)

* Sun Jul 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0.1-1
- PyQt-4.0.1

* Mon Jun 12 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0-1
- PyQt-4.0(final)
- BR: sip-devel >= 4.4.4 (see bug #199430)

* Fri May 12 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0-0.6.beta1
- drop BR: qt4-MySQL qt4-ODBC qt4-PostgreSQL
- drop usage of (undefined) %%sip_min

* Fri Apr 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0-0.5.beta1
- cleanup for Extras

* Fri Apr 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0-0.4.beta1
- 4.0beta1

* Thu Apr 27 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0-0.3.20060421 
- respin for sip-4.4.3
- use sip-abi, sip-abi-min

* Mon Apr 24 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.0-0.2.20060421
- 20060421 snapshot

* Wed Apr 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 0.0-0.1.20060417
- first try, using 20060417 snapshot

