%global momorel 8

Summary: glibc plugin for local name resolution
Name: nss-mdns
Version: 0.10
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://0pointer.de/lennart/projects/nss-mdns/
Group: System Environment/Libraries
Source0: http://0pointer.de/lennart/projects/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): perl
Requires(preun): perl
Requires: avahi bash
BuildRequires: glibc-devel

%description
nss-mdns is a plugin for the GNU Name Service Switch (NSS) functionality of
the GNU C Library (glibc) providing host name resolution via Multicast DNS
(aka Zeroconf, aka Apple Rendezvous, aka Apple Bonjour), effectively allowing 
name resolution by common Unix/Linux programs in the ad-hoc mDNS domain .local.

nss-mdns provides client functionality only, which means that you have to
run a mDNS responder daemon separately from nss-mdns if you want to register
the local host name via mDNS (e.g. Avahi).

%prep
%setup -q

%build
%configure --libdir=%{_libdir}  --enable-avahi=yes --enable-legacy=no
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
# Perl-fu to add mdns4_minimal to the hosts line of /etc/nsswitch.conf
if [ -f /etc/nsswitch.conf ] ; then
        sed -i.bak '
                /^hosts:/ !b
                /\<mdns\(4\|6\)\?\(_minimal\)\?\>/ b
                s/\([[:blank:]]\+\)dns\>/\1mdns4_minimal [NOTFOUND=return] dns/g
                ' /etc/nsswitch.conf
fi

%preun
# sed-fu to remove mdns4_minimal from the hosts line of /etc/nsswitch.conf
if [ "$1" -eq 0 -a -f /etc/nsswitch.conf ] ; then
        sed -i.bak '
                /^hosts:/ !b
                s/[[:blank:]]\+mdns\(4\|6\)\?\(_minimal\( \[NOTFOUND=return\]\)\?\)\?//g
        ' /etc/nsswitch.conf
fi

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE README doc/README.html doc/style.css
%{_libdir}/libnss_mdns*.so.2

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-8m)
- support UserMove env

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-5m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- rebuild against rpm-4.6

* Thu Jun 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-1m)
- initial package for Momonga Linux
- import %%post and %%preun from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.10-4
- Autorebuild for GCC 4.3

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.10-3
- Rebuild for selinux ppc32 issue.

* Fri Jun 22 2007 - Lennart Poettering <lpoetter@redhat.com> - 0.10-2
- Fix up post/preun/postun dependencies, add "avahi" to the dependencies, 
  include dist tag in Release field, use _lib directory instead of literal /lib.

* Fri Jun 22 2007 - Lennart Poettering <lpoetter@redhat.com> - 0.10-1
- Update to 0.10, replace perl script by simpler and more robust versions,
  stolen from the Debian package

* Thu Jul 13 2006 - Bastien Nocera <hadess@hadess.net> - 0.8-2
- Make use of Ezio's perl scripts to enable and disable mdns4 lookups
  automatically, patch from Pancrazio `Ezio' de Mauro <pdemauro@redhat.com>

* Tue May 02 2006 - Bastien Nocera <hadess@hadess.net> - 0.8-1
- Update to 0.8, disable legacy lookups so that all lookups are made through
  the Avahi daemon

* Mon Apr 24 2006 - Bastien Nocera <hadess@hadess.net> - 0.7-2
- Fix building on 64-bit platforms

* Tue Dec 13 2005 - Bastien Nocera <hadess@hadess.net> - 0.7-1
- Update to 0.7, fix some rpmlint errors

* Thu Nov 10 2005 - Bastien Nocera <hadess@hadess.net> - 0.6-1
- Update to 0.6

* Tue Dec 07 2004 - Bastien Nocera <hadess@hadess.net> 0.1-1
- Initial package, automatically adds and remove mdns4 as a hosts service
