%global momorel 12

Summary: Ruby reference manual referer
Name: refe

Version: 0.8.0
Release: %{momorel}m%{?dist}
Group: Documentation
License: LGPLv2+
URL: http://www.loveruby.net/ja/prog/refe.html

Source0: http://i.loveruby.net/archive/refe/%{name}-%{version}-withdocsrc.tar.gz 
NoSource: 0
Patch0: refe-0.8.0-method_origin.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BUildArchitectures: noarch
BuildRequires: ruby

Requires: ruby

%description
Ruby reference manual referer. This tool is like `ri' but can be used
for Japanese and RD documents.

The tool has a completion capability so that you do not have to type
long class names and method names. For example, reference manual for
String#gsub! can be found `refe s gsu'.

This tool also can be used to index extension libraries, API references,
and functions in the Ruby source. For example, you can read the manual
for Data_Get_Struct using `refe -e data get'.

%define ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%define ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q
%patch0 -p0

%build
ruby setup.rb --quiet config \
    --bin-dir=%{_bindir} \
    --rb-dir=%{ruby_libdir} \
    --data-dir=%{_datadir}
ruby setup.rb setup

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby setup.rb --quiet config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_libdir} \
    --data-dir=%{buildroot}%{_datadir}
ruby setup.rb install

%__mkdir_p %{buildroot}%{_mandir}/man1
%__install -m 644 man/*.1 %{buildroot}%{_mandir}/man1

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog NEWS README.en README.ja TODO
%{_bindir}/*
%{ruby_libdir}/refe
%{_datadir}/refe
%{_mandir}/man?/*

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0-12m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-5m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-3m)
- %%NoSource -> NoSource

* Mon Oct 20 2003 zunda <zunda at freeshell.org>
- (0.8.0-2m)
- patch from http://yowaken.dip.jp/tdiary/20031017.html#p03
  Thank you yowa-san.

* Mon Oct 20 2003 zunda <zunda at freeshell.org>
- (0.8.0-1m)
- update
- please rmove the old package by `rpm -e refe' if you see
  error: unpacking of archive failed on file ... while `rpm -Uvh'

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.1-3m)
- merge from ruby-1_8-branch.

* Sun Aug 03 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.1-2m)
- rebuild against ruby-1.8.0.

* Mon May 19 2003 zunda <zunda at freeshell.org>
- (0.7.1-1m)
- update

* Mon Apr  7 2003 zunda <zunda at freeshell.org>
- (0.7.0-2m)
- refe-0.6.0-REFE_DATA_DIR.patch no longer needed
- man page added

* Fri Apr  4 2003 zunda <zunda at freeshell.org>
- (0.7.0-1m)
- update

* Wed Apr  2 2003 zunda <zunda at freeshell.org>
- (0.6.0-1m)
- update

* Wed Dec 11 2002 zunda <zunda at freeshell.org>
- (0.5.2-1m)
- update

* Fri Apr 19 2002 zunda <zunda@kondara.org>
- (0.5.0-2k)
- first release
- reference to the C source code does not work for some reason
