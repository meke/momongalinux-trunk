%global momorel 23

Summary: Talk client for one-on-one Internet chatting.
Name: talk
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-ntalk-%{version}.tar.gz
NoSource: 0
Source2: talk-xinetd
Source3: ntalk-xinetd
Patch0: talk.glibc222.time.patch
Patch1: netkit-ntalk-0.17-strip.patch
Provides: ntalk
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The talk package provides client and daemon programs for the Internet
talk protocol, which allows you to chat with other users on different
systems.  Talk is a communication program which copies lines from one
terminal to the terminal of another user.

Install talk if you'd like to use talk for chatting with users on
different systems.

%package server
Group: System Environment/Daemons
Provides: ntalk
Requires: xinetd
Summary: The talk server for one-on-one Internet chatting.

%description server
The talk-server package provides daemon programs for the Internet talk
protocol, which allows you to chat with other users on different
machines.  Talk is a communication program which copies lines from one
terminal to the terminal of another user.

%prep
%setup -q -n netkit-ntalk-%{version}
%patch0 -p1
%patch1 -p1 -b .strip

%build
sh configure --with-c-compiler=gcc
perl -pi -e 's,-O2,\$(RPM_OPT_FLAGS),' MCONFIG

%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/{bin,sbin}
mkdir -p %{buildroot}%{_mandir}/{man1,man8}

make INSTALLROOT=%{buildroot} MANDIR=%{_mandir} install

mkdir -p %{buildroot}/etc/xinetd.d
install -m644 %SOURCE2 %{buildroot}/etc/xinetd.d/talk
install -m644 %SOURCE3 %{buildroot}/etc/xinetd.d/ntalk

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/bin/talk
%{_mandir}/man1/*

%files server
%defattr(-,root,root)
%attr(0711,root,root)%{_sbindir}/in.ntalkd
%{_sbindir}/in.talkd
%{_mandir}/man8/*
%config(missingok,noreplace) /etc/xinetd.d/talk
%config(missingok,noreplace) /etc/xinetd.d/ntalk

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-23m)
- change primary site and downoad URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-22m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-20m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-18m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-17m)
- revise for rpm46 (s/Patch/Patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-16m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-15m)
- %%NoSource -> NoSource

* Tue Aug 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-14m)
- delete Obsoletes: ntalk, for fix conflicts talk and talk-server problem

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-13m)
- rebuild against new environment.

* Thu Nov 22 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.17-12k)
- default off (xinetd: disable = yes)

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (0.17-10k)
- merge from Jirai.

* Thu Mar 15 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.16-12k)
- rebuild against glibc-2.2.2 and add talk.glibc222.time.patch

* Thu Jan  4 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.16-11k)
- set disable on default

* Sun Dec  3 2000 Daiki Matsuda <dyky@df-usa.com>
- (0.16-8k)
- errased talk.fhs.patch and modified spec file for compatibility

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91 with xinetd.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.16-4).
- use compiler gcc, insted of egcs (at configure).

* Fri Feb 25 2000 Shingo Akagaki <dora@kondara.org>
- check files section

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages (again).

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions
- man pages are compressed

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Wed Dec 22 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Thu Dec 2 1999 Masako Hattori <maru@kondara.org>
-- add man-pages-ja-netkit-ntalk-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Aug 21 1999 Jeff Johnson <jbj@redhat.com>
- build for 6.1.

* Sun Jun 20 1999 Jeff Johnson <jbj@redhat.com>
- handle both talk and otalk packets (#2799).

* Fri Apr  9 1999 Jeff Johnson <jbj@redhat.com>
- update to multi-homed 0.11 version.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 14 1998 Erik Troan <ewt@redhat.com>
- built against new ncurses

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
