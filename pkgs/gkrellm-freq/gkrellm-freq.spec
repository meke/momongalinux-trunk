%global momorel 7
%global gkplugindir %{_libdir}/gkrellm2/plugins

Summary: CPU frequency display plugin for GKrellM
Name: gkrellm-freq
%global srcname gkrellm-gkfreq
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL+
Group: Applications/System
URL: http://www.peakunix.net/gkfreq/
Source: http://www.peakunix.net/downloads/%{srcname}-%{version}.tar.gz
Patch0: gkrellm-gkfreq-1.0-rmasm.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gkrellm >= 2.2.0
BuildRequires: gkrellm-devel >= 2.2.0
BuildRequires: gkrellm >= 2.2.0

%description
This plugin for GKrellM, the GNU Krell Monitor, displays the current CPU speed
using the "dynamic" cpufrequency scheme.


%prep
%setup -q -n gkrellm-gkfreq-%{version}
%patch0 -p1 -b .noasm


%build
# Trivial manual build, the "build" script does the same without our optflags
%{__cc} %{optflags} -fPIC `pkg-config gtk+-2.0 --cflags` -c gkfreq.c
%{__cc} %{optflags} -fPIC -shared -o gkfreq.so gkfreq.o


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m 0755 gkfreq.so \
    %{buildroot}%{gkplugindir}/gkfreq.so


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc CHANGES COPYING README
%{gkplugindir}/gkfreq.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0-8
- Autorebuild for GCC 4.3

* Wed Sep 12 2007 Matthias Saou <http://freshrpms.net/> 1.0-7
- Fix License field, it's actually GPL+ which is okay for GPLv3 gkrellm.

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 1.0-6
- Rebuild for new BuildID feature.

* Sun Aug  5 2007 Matthias Saou <http://freshrpms.net/> 1.0-5
- Update License field... GPLv2 only, will be a problem with GPLv3 gkrellm.

* Fri Jun 22 2007 Matthias Saou <http://freshrpms.net/> 1.0-4
- Remove dist, as it might be a while before the next rebuild.

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 1.0-3
- FC6 rebuild.

* Mon Mar  6 2006 Matthias Saou <http://freshrpms.net/> 1.0-2
- FC5 rebuild.

* Wed Feb  8 2006 Matthias Saou <http://freshrpms.net/> 1.0-1
- Update to 1.0.
- Update the rmasm patch.
- Update original name from gkfreq to gkrellm-gkfreq.

* Fri Jul 15 2005 Matthias Saou <http://freshrpms.net/> 0.1.1-2
- Include patch to remove unused function containing an asm call that made
  build fail on PPC.

* Tue Jul 12 2005 Matthias Saou <http://freshrpms.net/> 0.1.1-1
- Initial RPM release, split out from my old gkrellm-plugins package.

