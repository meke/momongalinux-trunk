% -*- SLang -*-         ruby.sl

% Author:	MAEDA Shugo (shugo@po.aianet.ne.jp)
% Version:	0.03

% `Ruby mode for Jed' is FREESOFTWARE.
% Please use AT YOUR OWN RISK.

% [What's ruby?]
%
%  Ruby is the interpreted scripting language for quick and
%  easy object-oriented programming.  It has many features to
%  process text files and to do system management tasks (as in
%  perl).  It is simple, straight-forward, and extensible.
%
%  The ruby distribution can be found on
% 
%    ftp://ftp.netlab.co.jp/pub/lang/ruby/

% [Install]
%
% Please add these lines to your `.jedrc' file.
%
%   ~/.jedrc
%
%     % amount of space to indent within block.
%     variable ruby_indent_level = 2;
%
%     % Load ruby mode when openning `.rb' files.   
%     autoload("ruby_mode", "ruby");
%     add_mode_for_extension ("ruby", "rb");

variable ruby_mode_version = "0.0.3";

!if (is_defined ("ruby_indent_level"))
{
    variable ruby_indent_level = 2;
}

define ruby_show_version()
{
    message(Sprintf("ruby mode for Jed version %s", ruby_mode_version, 1));
}

define ruby_indent_to(n)
{
    variable step;
   
    step = what_column();
    bol_skip_white();
    step -= what_column();
    if (what_column != n) {
	bol_trim ();
	n--;
	whitespace (n);
    }
    if (step > 0) go_right(step);
}

define ruby_looking_keyword_at(keyword)
{
    push_spot;
    EXIT_BLOCK {
	pop_spot;
    }
   
    if (looking_at(keyword)) {
	go_right(strlen(keyword));
	return (orelse
	       { looking_at(" ") }
	       { looking_at("\t") }
	       { looking_at(";") }
	       { eolp() }
		);
    } else {
	return 0;
    }
}
	  
define ruby_calculate_indent()
{
    variable indent = 0;
    variable extra_indent = 0;
    variable ch;
    variable par_level;
    
    CASE_SEARCH = 0;
    push_spot();
    EXIT_BLOCK {
	pop_spot();
    }
    
    bol_skip_white();
    indent = what_column();
    if (orelse
       { ruby_looking_keyword_at("end") }
       { ruby_looking_keyword_at("else") }
       { ruby_looking_keyword_at("elsif") }
       { ruby_looking_keyword_at("rescue") }
       { ruby_looking_keyword_at("ensure") }
       { ruby_looking_keyword_at("when") }
       { looking_at("}") }
	) {
	extra_indent -= ruby_indent_level;
    }
    !if (up_1()) return indent;
    
    eol();
    par_level = 0;
    forever {
	if (eolp()) {
	    forever {
		bol();
		if (looking_at("#")) {
		    !if (up_1()) return indent;
		    eol();
		} else {
		    eol();
		    break;
		}
	    }
	}
	go_left_1();
	ch = what_char();
	if (ch == ')') {
	    par_level--;
	} else if (ch == '(') {
	    par_level++;
	    if (par_level == 1) return what_column() + 1;
	}
	
	if (bolp() and (par_level == 0)) {
	    skip_white();
	    indent = what_column();
	    break;
	}
    }
    
    if (looking_at("#")) return what_column();
    
    if (orelse
       { ruby_looking_keyword_at("class") }
       { ruby_looking_keyword_at("module") }
       { ruby_looking_keyword_at("def") }
       { ruby_looking_keyword_at("if") }
       { ruby_looking_keyword_at("else") }
       { ruby_looking_keyword_at("elsif") }
       { ruby_looking_keyword_at("unless") }
       { ruby_looking_keyword_at("case") }
       { ruby_looking_keyword_at("when") }
       { ruby_looking_keyword_at("while") }
       { ruby_looking_keyword_at("until") }
       { ruby_looking_keyword_at("for") }
       { ruby_looking_keyword_at("begin") }
       { ruby_looking_keyword_at("rescue") }
       { ruby_looking_keyword_at("ensure") }
	) {
	eol();
	bskip_white();
	!if (orelse
	    { blooking_at(" end") }
	    { blooking_at("\tend") }
	     ) {
	    extra_indent += ruby_indent_level;
	}
    } else {
	eol();
	bskip_white();
	if (blooking_at("{"))
	    extra_indent += ruby_indent_level;
	else if (blooking_at("|"))
	    extra_indent += ruby_indent_level;
	else if (blooking_at(" do"))
	    extra_indent += ruby_indent_level;
    }
    
    return indent + extra_indent;
}

define ruby_indent_line()
{
    ruby_indent_to(ruby_calculate_indent());
}

define ruby_newline_and_indent()
{
    variable step;
    step = what_column();
    bol_skip_white();
    step -= what_column();
    if (orelse
       { looking_at("end") }
       { looking_at("else") }
       { looking_at("elsif") }
       { looking_at("rescue") }
       { looking_at("ensure") }
       { looking_at("when") }
       { looking_at("}") }
	) {
	ruby_indent_line();
    }
    go_right(step);
    newline();
    ruby_indent_line();
}

define ruby_self_insert_cmd()
{
    variable step;
    
%    insert(LAST_CHAR);
    insert(char(LAST_CHAR));                                                    
    step = what_column();
    bol_skip_white();
    step -= what_column();
    if (orelse
       { looking_at("end") }
       { looking_at("else") }
       { looking_at("elsif") }
       { looking_at("rescue") }
       { looking_at("ensure") }
       { looking_at("when") }
       { looking_at("}") }
	) {
	ruby_indent_line();
    }
    go_right(step);
}

% Define keymap.
$1 = "ruby";
!if (keymap_p($1)) make_keymap($1);
definekey ("ruby_show_version", "^Cv", $1);
definekey ("ruby_self_insert_cmd", "0", $1);
definekey ("ruby_self_insert_cmd", "1", $1);
definekey ("ruby_self_insert_cmd", "2", $1);
definekey ("ruby_self_insert_cmd", "3", $1);
definekey ("ruby_self_insert_cmd", "4", $1);
definekey ("ruby_self_insert_cmd", "5", $1);
definekey ("ruby_self_insert_cmd", "6", $1);
definekey ("ruby_self_insert_cmd", "7", $1);
definekey ("ruby_self_insert_cmd", "8", $1);
definekey ("ruby_self_insert_cmd", "9", $1);
definekey ("ruby_self_insert_cmd", "a", $1);
definekey ("ruby_self_insert_cmd", "b", $1);
definekey ("ruby_self_insert_cmd", "c", $1);
definekey ("ruby_self_insert_cmd", "d", $1);
definekey ("ruby_self_insert_cmd", "e", $1);
definekey ("ruby_self_insert_cmd", "f", $1);
definekey ("ruby_self_insert_cmd", "g", $1);
definekey ("ruby_self_insert_cmd", "h", $1);
definekey ("ruby_self_insert_cmd", "i", $1);
definekey ("ruby_self_insert_cmd", "j", $1);
definekey ("ruby_self_insert_cmd", "k", $1);
definekey ("ruby_self_insert_cmd", "l", $1);
definekey ("ruby_self_insert_cmd", "m", $1);
definekey ("ruby_self_insert_cmd", "n", $1);
definekey ("ruby_self_insert_cmd", "o", $1);
definekey ("ruby_self_insert_cmd", "p", $1);
definekey ("ruby_self_insert_cmd", "q", $1);
definekey ("ruby_self_insert_cmd", "r", $1);
definekey ("ruby_self_insert_cmd", "s", $1);
definekey ("ruby_self_insert_cmd", "t", $1);
definekey ("ruby_self_insert_cmd", "u", $1);
definekey ("ruby_self_insert_cmd", "v", $1);
definekey ("ruby_self_insert_cmd", "w", $1);
definekey ("ruby_self_insert_cmd", "x", $1);
definekey ("ruby_self_insert_cmd", "y", $1);
definekey ("ruby_self_insert_cmd", "z", $1);
definekey ("ruby_self_insert_cmd", "A", $1);
definekey ("ruby_self_insert_cmd", "B", $1);
definekey ("ruby_self_insert_cmd", "C", $1);
definekey ("ruby_self_insert_cmd", "D", $1);
definekey ("ruby_self_insert_cmd", "E", $1);
definekey ("ruby_self_insert_cmd", "F", $1);
definekey ("ruby_self_insert_cmd", "G", $1);
definekey ("ruby_self_insert_cmd", "H", $1);
definekey ("ruby_self_insert_cmd", "I", $1);
definekey ("ruby_self_insert_cmd", "J", $1);
definekey ("ruby_self_insert_cmd", "K", $1);
definekey ("ruby_self_insert_cmd", "L", $1);
definekey ("ruby_self_insert_cmd", "M", $1);
definekey ("ruby_self_insert_cmd", "N", $1);
definekey ("ruby_self_insert_cmd", "O", $1);
definekey ("ruby_self_insert_cmd", "P", $1);
definekey ("ruby_self_insert_cmd", "Q", $1);
definekey ("ruby_self_insert_cmd", "R", $1);
definekey ("ruby_self_insert_cmd", "S", $1);
definekey ("ruby_self_insert_cmd", "T", $1);
definekey ("ruby_self_insert_cmd", "U", $1);
definekey ("ruby_self_insert_cmd", "V", $1);
definekey ("ruby_self_insert_cmd", "W", $1);
definekey ("ruby_self_insert_cmd", "X", $1);
definekey ("ruby_self_insert_cmd", "Y", $1);
definekey ("ruby_self_insert_cmd", "Z", $1);
definekey ("ruby_self_insert_cmd", "_", $1);
definekey ("ruby_self_insert_cmd", "{", $1);
definekey ("ruby_self_insert_cmd", "}", $1);
definekey ("ruby_self_insert_cmd", ";", $1);
definekey ("ruby_indent_line", "\t", $1);

% Create syntax table.
create_syntax_table ($1);
define_syntax ("#", Null_String, '%', $1);
define_syntax ("([{<", ")]}>", '(', $1);
define_syntax ('"', '"', $1);
define_syntax ('\'', '\'', $1);
define_syntax ('\\', '\\', $1);
define_syntax ("$0-9a-zA-Z_", 'w', $1);
define_syntax ("-+0-9a-fA-F.xXL", '0', $1);
define_syntax (",;.?:", ',', $1);
define_syntax ("%-+/&*=<>|!~^", '+', $1);
set_syntax_flags ($1, 4);

#ifdef HAS_DFA_SYNTAX
static define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache("ruby.dfa", name);
   dfa_define_highlight_rule("#.*$", "comment", name);
   dfa_define_highlight_rule("([\\$%&@\\*]|\\$#)[A-Za-z_0-9]+", "normal", name);
   dfa_define_highlight_rule(strcat("\\$([_\\./,\"\\\\#\\*\\?\\]\\[;!@:\\$<>\\(\\)",
				    "%=\\-~\\^\\|&`'\\+]|\\^[A-Z])"), "normal", name);
   dfa_define_highlight_rule("[A-Za-z_][A-Za-z_0-9]*", "Knormal", name);
   dfa_define_highlight_rule("[0-9]+(\\.[0-9]+)?([Ee][\\+\\-]?[0-9]*)?", "number",
			     name);
   dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*", "number", name);
   dfa_define_highlight_rule("[\\(\\[\\{\\<\\>\\}\\]\\),;\\.\\?:]", "delimiter", name);
   dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);
   dfa_define_highlight_rule("-[A-Za-z]", "keyword0", name);
   dfa_define_highlight_rule("'[^']*'", "string", name);
   dfa_define_highlight_rule("'[^']*$", "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("m?/([^/\\\\]|\\\\.)*/[gio]*", "string", name);
   dfa_define_highlight_rule("m/([^/\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("s/([^/\\\\]|\\\\.)*(/([^/\\\\]|\\\\.)*)?/[geio]*",
			     "string", name);
   dfa_define_highlight_rule("s/([^/\\\\]|\\\\.)*(/([^/\\\\]|\\\\.)*)?\\\\?$",
			     "string", name);
   dfa_define_highlight_rule("(tr|y)/([^/\\\\]|\\\\.)*(/([^/\\\\]|\\\\.)*)?/[cds]*",
			     "string", name);
   dfa_define_highlight_rule("(tr|y)/([^/\\\\]|\\\\.)*(/([^/\\\\]|\\\\.)*)?\\\\?$",
			     "string", name);
   dfa_define_highlight_rule(".", "normal", name);
   dfa_build_highlight_table (name);
}
dfa_set_init_callback (&setup_dfa_callback, "ruby");
#endif

% Type 0 keywords
() = define_keywords_n ($1, "doifinor", 2, 0);
() = define_keywords_n ($1, "anddefendfornilnot", 3, 0);
() = define_keywords_n ($1, "caseelsefailloadloopnextredoselfthenwhen", 4, 0);
() = define_keywords_n ($1, "aliasbeginbreakclasselsifraiseretrysuperundefuntilwhileyield", 5, 0);
() = define_keywords_n ($1, "ensuremodulerescuereturnunless", 6, 0);
() = define_keywords_n ($1, "includerequire", 7, 0);
() = define_keywords_n ($1, "autoload", 8, 0);
% Type 1 keywords (commonly used libc functions)
() = define_keywords_n($1, "TRUE", 4, 1);
() = define_keywords_n($1, "FALSE", 5, 1);

define ruby_mode()
{
    variable kmap = "ruby";
    set_mode(kmap, 2);
    use_keymap(kmap);
    use_syntax_table(kmap);
    set_buffer_hook("indent_hook", "ruby_indent_line");
    set_buffer_hook("newline_indent_hook", "ruby_newline_and_indent"); 
    run_mode_hooks("ruby_mode_hook");
}
