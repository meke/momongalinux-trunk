% Note the functions used here are not available on 16 bit systems.

static variable Kill_Buffer_Number = -1;
static variable Kill_Buffer_Yank_Number = -1;
static variable Kill_Buffer_Max_Number = -1;
static variable Default_Target_String = Null_String;

% Instead of using a new user mark, I should probably set aside a bookmark
% to allow the user to goto it.
static variable Kill_Buffer_User_Mark;

define yp_copy_region_as_kill ()
{
   variable kill_fun = "%kill%";
   if (strcmp (LAST_KBD_COMMAND, kill_fun))
     {
	Kill_Buffer_Number++;
	if (Kill_Buffer_Number == KILL_ARRAY_SIZE)
	  {
	     Kill_Buffer_Number = 0;
	  }
	
	if (Kill_Buffer_Number > Kill_Buffer_Max_Number)
	  Kill_Buffer_Max_Number = Kill_Buffer_Number;

	copy_region_to_kill_array (Kill_Buffer_Number);
	Kill_Buffer_Yank_Number = Kill_Buffer_Number;
     }
   else
     {
	append_region_to_kill_array (Kill_Buffer_Number);
     }
   
   set_current_kbd_command (kill_fun);
}

define yp_kill_region ()
{
   if (() = dupmark ())
     {
	yp_copy_region_as_kill ();
	del_region ();
     }
   else
     {
	Kill_Buffer_Yank_Number--;
	insert(Default_Target_String);
     }
}

define yp_kill_line ()
{
   variable one;
   variable kill_fun = "%kill%";

   one = eolp () or (KILL_LINE_FEATURE and bolp ());

   mark_to_visible_eol ();
   go_right (one);
   yp_kill_region ();
}

define yp_yank ()
{
   Kill_Buffer_User_Mark = create_user_mark ();
   insert_from_kill_array (Kill_Buffer_Yank_Number);
   set_current_kbd_command ("%yank%");
}

define yp_yank_pop ()
{
   if (strcmp (LAST_KBD_COMMAND, "%yank%"))
     {
	error ("The last command must be a yank one.");
     }

   Kill_Buffer_Yank_Number--;
   if (Kill_Buffer_Yank_Number < 0)
     {
	Kill_Buffer_Yank_Number = Kill_Buffer_Max_Number;
     }
   
   %  Delete the previous yank 
   push_mark ();
   goto_user_mark (Kill_Buffer_User_Mark);
   del_region ();
   
   yp_yank ();
}   

define yp_kill_word ()
{
   push_mark(); skip_word(); 
   yp_kill_region ();
}

define yp_bkill_word ()
{
   push_mark(); bskip_word(); 
   yp_kill_region ();
}

define get_token ()
{
   variable dflt;

   push_spot();
   define_word("[^0-9a-zA-Z_]");
   bskip_word_chars();
   push_mark();
   skip_word_chars();
   Default_Target_String = bufsubstr();
   pop_spot();
}
