/* -*- mode: C; mode: fold; -*- */
/* Copyright (c) 1992, 1998 John E. Davis
 * This file is part of JED editor library source.
 *
 * You may distribute this file under the terms the GNU General Public
 * License.  See the file COPYING for more information.
 */
#include "config.h"
#include "jed-feat.h"

#include <stdio.h>
#include <slang.h>

#include "jdmacros.h"

#include <string.h>
#if defined(IBMPC_SYSTEM) && !defined(__CYGWIN32__) && !defined(__IBMC__)
# include <dos.h>
#endif

#include "buffer.h"
#include "screen.h"
#include "window.h"
#include "paste.h"

#include "ins.h"
#include "ledit.h"
#include "display.h"
#include "sysdep.h"
#include "misc.h"
#include "file.h"
#include "hooks.h"
#include "menu.h"
#include "kanji.h"

#if JED_HAS_SUBPROCESSES
# include "jprocess.h"
#endif

#include "sig.h"

#define JED_VERSION "B0.99.10+J0.6.9"
int Jed_Version_Number = 9910;
int Jed_JVersion_Number = 609;
char *Jed_Version_String = JED_VERSION;

volatile int Jed_Resize_Pending;
char *MiniBuf_Get_Response_String;

#ifndef IBMPC_SYSTEM
int Jed_Simulate_Graphic_Chars;
#endif

typedef struct Screen_Type
{
   Line *line;		       /* buffer line structure */
   int is_modified;
   int m;
   unsigned char *hi0, *hi1;	       /* beg end of hilights */
}
Screen_Type;

static Screen_Type *JScreen;
static Screen_Type *JScreen2;

int Jed_Num_Screen_Rows;
int Jed_Num_Screen_Cols;

int Screen_Row = 1;
int Screen_Col = 1;
int Cursor_Motion;    /* indicates cursor movement only -1 ^ v +1 < > */
int Jed_Dollar = '$';
int User_Prefers_Line_Numbers = 0;
int Mode_Has_Syntax_Highlight;
int Wants_Syntax_Highlight;	       /* if non-zero, highlight the syntax.
					*/
int Wants_Attributes = 1;
int Wants_HScroll = 20;		       /* controls automatic horizontal
					* scrolling.  If positive, scroll
					* only line, if negative, whole wind
					*/
int Term_Supports_Color = 1;	       /* optimistic assumption */

int Goal_Column;

int Want_Eob = 0;
int Display_Time = 1;                  /* Turn on %t processing in status line */

void (*X_Update_Open_Hook)(void);      /* hooks called when starting */
void (*X_Update_Close_Hook)(void);     /* and finishing update */


/* site.sl should modify this */
char Default_Status_Line[80] =
  " ^Ke: quit, ^Kg: get file, ^K^W: write file | %b  (%m%n%o) %p";

static Line *HScroll_Line;
static int HScroll;		       /* amount to scroll line by */
static int Absolute_Column;

static int Point_Cursor_Flag = 1;      /* if non-zero, point cursor */

static Line Eob_Line =
{
   NULL, NULL, (unsigned char *) "[EOB]", 5
#ifdef KEEP_SPACE_INFO
     ,5
#endif
};

static unsigned char Char_Width[256];
static int Display_Eight_Bit = 0x7FFF;
static int Kana_Changed = 0;

#define FIX_CHAR_WIDTH \
     if ((SLsmg_Display_Eight_Bit != Display_Eight_Bit) \
	 || (Kana_Changed != SKanaToDKana)) fix_char_width ()


int Display_Wrap_Width = -1;
int DWrap;
int Row_Scroll = 1;
int More_Row;

static int wrap_num;
static int row_max;
static int Last_Display_Row;
static int Start_Display_Row;
static int Last_i;
static int full;
static int Begin_no;
static int End_no;
static int Lno;
typedef struct LineInfo_Type {
	int lno;
	int n;
	Line *line;
} LineInfo_Type;
static LineInfo_Type *LineInfo0, *LineInfo;
static int n_before;
typedef struct RowInfo_Type {
	unsigned char *p;
} RowInfo_Type;
static RowInfo_Type *RowInfo;
static int MaxRowInfo;
static int isTop;
static struct {
	int m;
	int dcol;
	unsigned char *p;
} CursorPos;
static struct {
	int m;
	unsigned char *mp;
} BeginLine;
#define	LNW	8
#define	ROWINFO(x)	RowInfo[(x) % MaxRowInfo]

static void jed_write_start (int row, int col)
{
	Start_Display_Row = Last_Display_Row = row;
	isTop = (row == JWindow->top - 1);
	SLsmg_gotorc (row, col);
	Last_i = col;
	full = 0;
/*
	{
	char buf[9];
	sprintf(buf, "%6d  ", Lno);
	jed_write_nchars(buf, LNW, 0);
	Last_i = LNW;
	}
*/
}


static void jed_write_start2 (int row, int m, int col)
{
	if (row == JWindow->top - 1) {
		if ((m -= BeginLine.m) < 0) {
			m = 0;
			col = 0;
		}
	}
	SLsmg_gotorc ((Last_Display_Row = row + m), col);
	Last_i = col;
}

static int jed_write_end ()
{
	return Last_Display_Row - Start_Display_Row + 1;
}

int jed_write_nchars (char *data, int len, int color)
{
   register unsigned char *p, *pmax, *prev;
   int tab, w;
	int row, i;
   
   if (DWrap == 0)
     {
     	SLsmg_write_nchars(data, len);
     	return 1;
     }

	row = Last_Display_Row;
	i = Last_i;
/*
   FIX_CHAR_WIDTH;
*/
	if (row == Jed_Num_Screen_Rows - 1) {
		SLsmg_write_nchars(data, len);
		return 1;
	}
   
	if (row > row_max) {
		return 1;
	}

   if (len == 0)
     {
	return 1;
     }

   p = data;
   pmax = p + (len - 1);
   if (*pmax != '\n') pmax++;
   
	if (isTop) {
		if (pmax <= BeginLine.mp)
			return 1;

		if (p < BeginLine.mp) {
			p = BeginLine.mp;
			i = 0;
		}
	}

   tab = Buffer_Local.tab;
	prev = p;
   while(p < pmax)
     {
	if(iskanji(*p))
	  {
	     if(p == (pmax - 1)) break;
	     else
	       {
		  w = Char_Width[*p];
		  i += w + (w == 1 ? 1 : Char_Width[*(p+1)]);
		  p++;
			if (i > DWrap) {
				p -= 2;
			}
	       }
	  }
	else if ((*p == '\t') && tab)
	  {
	     i = tab * (i / tab + 1);
	  }
	else
	  {
	     w = Char_Width[*p];
	     i +=  w;
/*	     if (w == 0) p++; */
	  }
	
	p++;
	if (i >= DWrap) {
		if (full) {
			char dollar = (char) Jed_Dollar;
			if (!dollar) dollar = ' ';
	     		SLsmg_set_color (JDOLLAR_COLOR);
			SLsmg_write_nchars(&dollar, 1);
   			SLsmg_set_color (0);
			SLsmg_erase_eol ();
			row++;
			SLsmg_gotorc (row, 0);
		}
		full = 1;
		if (row > row_max) {
			prev = p;
			break;
		}
   		SLsmg_set_color (color);

		SLsmg_write_nchars(prev, p - prev);
/*
		if (i > DWrap) {
			char pad = (char) ' ';
			SLsmg_write_nchars(&pad, 1);
		}
*/
		full = 1;
		prev = p;
		i = 0;
	}
     }
	if (p > prev) {
		if (full) {
			char dollar = (char) Jed_Dollar;
			if (!dollar) dollar = ' ';
	     		SLsmg_set_color (JDOLLAR_COLOR);
			SLsmg_write_nchars(&dollar, 1);
   			SLsmg_set_color (0);
			SLsmg_erase_eol ();
			row++;
			SLsmg_gotorc (row, 0);
   			SLsmg_set_color (color);
			full = 0;
		}
		if (row <= row_max) {
			SLsmg_write_nchars(prev, p - prev);
		}
	}

	Last_Display_Row = row;
	Last_i = i;

	return 1;
}

int get_line_info_above (int j, Line *line, int to_cur, int lno)
{
   register unsigned char *p, *prev, *pmax;
   int tab, w, i, m, row;
	int after_cur = 0;

	if (DWrap == 0) {
		return 1;
	}

	if (j > 0 || j < n_before - 1) {
		char buf[100];
		sprintf(buf, "get_line_info bug j: %d, n_before: %d", j, n_before);
		exit_error(buf, 0);
	}
	if (j >= n_before) {
		return LineInfo[j].n;
	}

	i = 0;

   p = line->data;
   pmax = p + (line->len - 1);
   if (*pmax != '\n') pmax++;
   
	if (p == pmax) {
		if (p == CursorPos.p) {
			CursorPos.m = 0;
		}
		LineInfo[j].n = 1;
		LineInfo[j].line = line;
		LineInfo[j].lno = lno;
		if (j <= 0)
			n_before = j;
		return 1;
	}

   tab = Buffer_Local.tab;
	prev = p;
	m = 0;

   while(p < pmax)
     {
	if (p == CursorPos.p) {
		after_cur = 1;
		CursorPos.m = m;
		CursorPos.dcol = i;
		ROWINFO(m).p = prev;
	}

	if(iskanji(*p))
	  {
	     if(p == (pmax - 1)) break;
	     else
	       {
		  w = Char_Width[*p];
		  i += w + (w == 1 ? 1 : Char_Width[*(p+1)]);
		  p++;
			if (i > DWrap) {
				p -= 2;
			}
	       }
	  }
	else if ((*p == '\t') && tab)
	  {
	     i = tab * (i / tab + 1);
	  }
	else
	  {
	     w = Char_Width[*p];
	     i +=  w;
/*	     if (w == 0) p++; */
	  }
	
	p++;
	if (i >= DWrap) {
		if (j == 0 && p > CursorPos.p) {
			if (p < pmax)
				More_Row = 1;
			if (to_cur)
				break;
		}

		if (!after_cur)
{
			ROWINFO(m).p = prev;
}
		m++;
		prev = p;
		i = 0;
	}

/*
	if (j == 0 && to_cur & p > CursorPos.p) {
		break;
	}
*/
     }
	if (p == CursorPos.p) {
		CursorPos.m = m;
		CursorPos.dcol = i;
	}

	if (p - prev > 0) {
		if (!after_cur)
{
			ROWINFO(m).p = prev;
}
		m++;
	}

	LineInfo[j].n = m;
	LineInfo[j].line = line;
	LineInfo[j].lno = lno;
	if (j <= 0)
		n_before = j;

	return m;
}

int get_line_info_below (int j, Line *line, int lno)
{
   register unsigned char *p, *prev, *pmax;
   int tab, w, i, m, row;

	if (DWrap == 0) {
		return 1;
	}

   p = line->data;
   pmax = p + (line->len - 1);
   if (*pmax != '\n') pmax++;
   
	if (j == 0) {
		i = CursorPos.dcol;
		m = CursorPos.m;
		p = CursorPos.p;
	} else {
		i = 0;
		m = 0;
	}

	if (p == pmax) {
		LineInfo[j].n = m + 1;
		LineInfo[j].line = line;
		LineInfo[j].lno = lno;
		return 1;
	}

   tab = Buffer_Local.tab;
	prev = p;

   while(p < pmax)
     {
	if(iskanji(*p))
	  {
	     if(p == (pmax - 1)) break;
	     else
	       {
		  w = Char_Width[*p];
		  i += w + (w == 1 ? 1 : Char_Width[*(p+1)]);
		  p++;
			if (i > DWrap) {
				p -= 2;
			}
	       }
	  }
	else if ((*p == '\t') && tab)
	  {
	     i = tab * (i / tab + 1);
	  }
	else
	  {
	     w = Char_Width[*p];
	     i +=  w;
/*	     if (w == 0) p++; */
	  }
	
	p++;
	if (i >= DWrap) {
		m++;
		prev = p;
		i = 0;
	}
     }
	if (p - prev > 0) {
		m++;
	}

	LineInfo[j].n = m;
	LineInfo[j].line = line;
	LineInfo[j].lno = lno;

	return m;
}

static void display_line (Line *line, int row)
{
   unsigned int len;
   int hscroll_col;
   int is_mini;
   Screen_Type *s;
#if JED_HAS_LINE_MARKS
   Mark *line_marks;
#endif
   
   SLsmg_Tab_Width = Buffer_Local.tab;
   is_mini = (row == Jed_Num_Screen_Rows);
   
   row--;
   
   SLsmg_gotorc (row, 0);
   SLsmg_set_color (0);
   
   s = JScreen + row;
   
   s->line = line;
   s->is_modified = 0;
   s->m = (row == JWindow->top - 1) ? BeginLine.m: 0;
   
   if (line == NULL)
     {
	SLsmg_erase_eol ();
	return;
     }
   
   hscroll_col = JWindow->column - 1;
   
   if ((line == HScroll_Line)
       && Wants_HScroll && HScroll)
     hscroll_col += HScroll;
   
   if (hscroll_col)
     {
	int tmp = hscroll_col;
	SLsmg_set_screen_start (NULL, &tmp);
     }
   
   len = line->len;
   
	jed_write_start(row, 0);

   if (is_mini)
     {
	SLsmg_write_string ((char *)Mini_Info.prompt);
	SLsmg_Newline_Behavior = SLSMG_NEWLINE_PRINTABLE;
     }
   else 
     {
	SLsmg_Newline_Behavior = 0;
	if (len && (line->data[len - 1] == '\n'))
	  len--;
     }
   
#if JED_HAS_LINE_MARKS
   line_marks = CBuf->user_marks;
   while (line_marks != NULL)
     {
	if ((line_marks->line == line)
	    && (line_marks->flags & JED_LINE_MARK))
	  {
	     SLsmg_set_color (line_marks->flags & MARK_COLOR_MASK);
	     break;
	  }
	line_marks = line_marks->next;
     }
#endif
   
   if (len)
     {
	if ((line_marks == NULL)
	    && Mode_Has_Syntax_Highlight
	    && (line != &Eob_Line)
#if !defined(IBMPC_SYSTEM)
	    && (*tt_Use_Ansi_Colors && Term_Supports_Color)
#endif
	    && Wants_Syntax_Highlight)
	  write_syntax_highlight (line, len);
	else
	  jed_write_nchars ((char *)line->data, len, 0);
     }
#if JED_HAS_LINE_ATTRIBUTES
   if ((line->next != NULL)
       && (line->next->flags & JED_LINE_HIDDEN))
     {
	SLsmg_set_color (JDOTS_COLOR);
	jed_write_nchars ("...", 3, JDOTS_COLOR);
	SLsmg_set_color (0);
     }
#endif
   SLsmg_erase_eol ();
   
	wrap_num = jed_write_end();

	{ int i;
	for (i = 1; i < wrap_num; i++) {
		JScreen[row+i].line = line;
		JScreen[row+i].is_modified = 1;
		JScreen[row+i].m = JScreen[row+i-1].m;
	}
	}

   if (DWrap == 0 && Jed_Dollar)
     {
	char dollar = (char) Jed_Dollar;
	
	if (hscroll_col + Jed_Num_Screen_Cols <= SLsmg_get_column ())
	  {
	     unsigned char *p;

	     p = jed_compute_effective_point(line->data, hscroll_col + Jed_Num_Screen_Cols - 1);
	     if (iskanji2nd(line->data, p - line->data))
	       {
		  SLsmg_gotorc (row, hscroll_col + Jed_Num_Screen_Cols - 2);
		  SLsmg_set_color (JDOLLAR_COLOR);
		  SLsmg_write_nchars (&dollar, 1);
	       }
	     SLsmg_gotorc (row, hscroll_col + Jed_Num_Screen_Cols - 1);
	     SLsmg_set_color (JDOLLAR_COLOR);
	     SLsmg_write_nchars (&dollar, 1);
	  }
	if (hscroll_col)
	  {
	     unsigned char *p;

	     SLsmg_gotorc (row, hscroll_col);
	     SLsmg_set_color (JDOLLAR_COLOR);
	     SLsmg_write_nchars (&dollar, 1);
	     p = jed_compute_effective_point(line->data, hscroll_col + 1);
	     if (iskanji2nd(line->data, p - line->data))
	       SLsmg_write_nchars (&dollar, 1);
	  }
     }

   if ((s->hi0 != NULL) && Wants_Attributes)
     {
	int c;
	len = (int) (s->hi1 - s->hi0);
	
	if (len && (s->hi0[len - 1] == '\n'))
	  len--;
	
	if (len)
	  {
	     c = jed_compute_effective_length (line->data, s->hi0);
	     if (is_mini)
	       c += Mini_Info.effective_prompt_len;
	     SLsmg_gotorc (row, c);
/*
	     SLsmg_set_color (JREGION_COLOR);
	     SLsmg_write_nchars ((char *)s->hi0, len);
*/
		if (DWrap) {
			jed_write_start(row, 0);
			jed_write_start2(row, c/DWrap, c%DWrap);
		}
	     SLsmg_set_color (JREGION_COLOR);
	     jed_write_nchars ((char *)s->hi0, len, JREGION_COLOR);
	  }
     }
   
   if (hscroll_col)
     SLsmg_set_screen_start (NULL, NULL);
   
   SLsmg_set_color (0);
}

#if JED_HAS_LINE_ATTRIBUTES
static int Non_Hidden_Point;
Line *jed_find_non_hidden_line (Line *l)
{
   int dir;
   Line *cline;
   
   Non_Hidden_Point = 0;
   if (0 == (l->flags & JED_LINE_HIDDEN))
     {
	return l;
     }
   
   cline = l;
   
   dir = 1;
   while ((cline != NULL)
	  && (cline->flags & JED_LINE_HIDDEN))
++Lno,
     cline = cline->prev;
   
   if (cline == NULL)
     {
	cline = l;
	dir = -1;
	while ((cline != NULL)
	       && (cline->flags & JED_LINE_HIDDEN))
++Lno,
	  cline = cline->next;
	
	if (cline == NULL)
	  return NULL;
     }
   
   if (dir == 1)
     {
	Non_Hidden_Point = cline->len;
     }
   
   return cline;
}

#endif

static Line *find_top_to_recenter (Line *cline)
{
   int n, i;
   Line *prev, *last_prev;
   int last_Lno;
  
   n = JWindow->rows / 2 - CursorPos.m;
   
   last_prev = prev = cline;
   last_Lno = Lno;
   i = -1;
   
   while ((n > 0) && (prev != NULL))
     {
/*
	n--;
*/
	last_prev = prev;
	last_Lno = Lno;
#if JED_HAS_LINE_ATTRIBUTES
	do
	  {
	     prev = prev->prev;
	     --Lno;
	  }
	while ((prev != NULL)
	       && (prev->flags & JED_LINE_HIDDEN));
#else
	prev = prev->prev;
	--Lno;
#endif
	if (prev) {
       		n -= get_line_info_above(i--, prev, Row_Scroll, Lno);
	}
     }
   Begin_no = i + 1;
   n_before = Begin_no + 1;
   get_line_info_above(Begin_no, LineInfo[Begin_no].line, Row_Scroll, Lno);
   BeginLine.m = n < 0 ? -n: 0;
   BeginLine.mp = ROWINFO(BeginLine.m).p;
   
   if (prev != NULL) return prev;
   Lno = last_Lno;
   return last_prev;
}

Line *find_top (void)
{
   int nrows, i, point;
   Line *cline, *prev, *next;
   Line *top_window_line;
   Line *last_prev;
   int bm;
   unsigned char *bmp;
   Line *save_prev;
   
   cline = CLine;
   point = Point;
   Lno = LineNum;
   CursorPos.p = NULL;
   
#if JED_HAS_LINE_ATTRIBUTES
   if (cline->flags & JED_LINE_HIDDEN)
     cline = jed_find_non_hidden_line (cline);
   if (cline == NULL)
     return NULL;
   if (cline != CLine) point = Non_Hidden_Point;   /* updated by jed_find_non_hidden_line */
#endif
   
   nrows = JWindow->rows;
   
   if (nrows <= 1)
     return cline;
   
   /* Note: top_window_line might be a bogus pointer.  This means that I cannot
    * access it unless it really corresponds to a pointer in the buffer.
    */
   top_window_line = JScreen [JWindow->top - 1].line;
   
if (DWrap) {
   CursorPos.p = cline->data + point;
/*
   if (iskanji2nd(cline->data, point))
	CursorPos.p++;
*/
   	get_line_info_above(0, cline, (Row_Scroll || cline == top_window_line), Lno);
	if (Row_Scroll) {
		bm = CursorPos.m;
	} else {
		bm = CursorPos.m - nrows + 1;
		if (bm < 0)
			bm = 0;
	}
   	bmp = ROWINFO(bm).p;
}

   if (top_window_line == NULL)
     return find_top_to_recenter (cline);
   
   /* Chances are that the current line is visible in the window.  This means
    * that the top window line should be above it.
    */
   prev = cline;

/* ============================================== */
if (DWrap) {
	int n, n1, j, last_Lno;

   n = nrows;
   i = 0;

   while (prev != NULL)
     {
	if (prev == top_window_line)
	  {
       		n -= get_line_info_above(i--, prev, Row_Scroll, Lno);
		BeginLine.m = -n;
		if (BeginLine.m < JScreen [JWindow->top - 1].m) {
			BeginLine.m = JScreen [JWindow->top - 1].m;
		}
		if (prev == cline && CursorPos.m < BeginLine.m) {
			BeginLine.m = CursorPos.m;
		}
   		BeginLine.mp = ROWINFO(BeginLine.m).p;
   		Begin_no = i + 1;

		return top_window_line;
	  }

       	n -= get_line_info_above(i--, prev, Row_Scroll, Lno);
        if (n <= 0)
	  {
	     BeginLine.m = -n;
		if (prev == cline && CursorPos.m < BeginLine.m) {
			BeginLine.m = CursorPos.m;
		}
   		BeginLine.mp = ROWINFO(BeginLine.m).p;
   		Begin_no = i + 1;
	     save_prev = prev;
	     break;
	  }

	last_prev = prev;
	last_Lno = Lno;

#if JED_HAS_LINE_ATTRIBUTES
	do
	  {
	     prev = prev->prev;
	     --Lno;
	  }
	while ((prev != NULL)
	       && (prev->flags & JED_LINE_HIDDEN));
#else
	prev = prev->prev;
	--Lno;
#endif
   }

} else {
/* ============================================== */
   
   i = 0;
   while ((i < nrows) && (prev != NULL))
     {
	if (prev == top_window_line)
	  {
		Begin_no = -i;
	     return top_window_line;
	  }
	
#if JED_HAS_LINE_ATTRIBUTES
	do
	  {
	     prev = prev->prev;
--Lno;
	  }
	while ((prev != NULL)
	       && (prev->flags & JED_LINE_HIDDEN));
#else
	prev = prev->prev;
--Lno;
#endif
	i++;
     }
} /* DWrap == 0 */
   
   Lno = LineNum;

   /* Now check the borders of the window.  Perhaps the current line lies
    * outsider the border by a line.  Only do this if terminal can scroll.
    */
   
   if ((*tt_Term_Cannot_Scroll)
       && (*tt_Term_Cannot_Scroll != -1))
     return find_top_to_recenter (cline);
   
   next = cline->next;
#if JED_HAS_LINE_ATTRIBUTES
   while ((next != NULL) && (next->flags & JED_LINE_HIDDEN))
     next = next->next;
#endif
   
   if ((next != NULL)
       && (next == top_window_line))
{
   	BeginLine.m = bm;
   	BeginLine.mp = bmp;
   	Begin_no = 0;
     return cline;
}
   
   prev = cline->prev;
   --Lno;
#if JED_HAS_LINE_ATTRIBUTES
   while ((prev != NULL) && (prev->flags & JED_LINE_HIDDEN))
   --Lno,
     prev = prev->prev;
#endif
   
   top_window_line = JScreen [nrows + JWindow->top - 2].line;
   
/*
   if ((prev == NULL)
       || (prev != top_window_line))
     return find_top_to_recenter (cline);
*/
   if ((prev == NULL)
       || ((prev != top_window_line) && (cline != top_window_line)))
     return find_top_to_recenter (cline);
   
   /* It looks like cline is below window by one line.  See what line should
    * be at top to scroll it into view.
    */
   
/* ============================================== */
if (DWrap) {
	prev = save_prev;
} else {
/* ============================================== */

   i = 2;
   while ((i < nrows) && (prev != NULL))
     {
#if JED_HAS_LINE_ATTRIBUTES
	do
	  {
	     prev = prev->prev;
	     --Lno;
	  }
	while ((prev != NULL)
	       && (prev->flags & JED_LINE_HIDDEN));
#else
	prev = prev->prev;
	--Lno;
#endif
	i++;
     }
	Begin_no = -i + 1;
} /* DWrap == 0 */
   
   if (prev != NULL)
     return prev;
   
   return find_top_to_recenter (cline);
}

void get_info_below()
{
	Line *cline, *next;
	int i, n, point;

   cline = CLine;
   point = Point;
   Lno = LineNum;
#if JED_HAS_LINE_ATTRIBUTES
   cline = jed_find_non_hidden_line (CLine);
   if (cline != CLine) point = Non_Hidden_Point;   /* updated by jed_find_non_hidden_line */
#endif
	CursorPos.p = cline->data + point;
/*
   	if (iskanji2nd(cline->data, point))
		CursorPos.p++;
*/
	get_line_info_below(0, cline, Lno);

	LineInfo[Begin_no].n -= BeginLine.m;

	i = Begin_no;
	n = JWindow->rows;
	for (i = Begin_no; i <= 0; i++) {
		n -= LineInfo[i].n;
	}

	next = cline;
	while (n > 0) {
   next = next->next;
   ++Lno;
#if JED_HAS_LINE_ATTRIBUTES
   while ((next != NULL) && (next->flags & JED_LINE_HIDDEN))
     ++Lno,
     next = next->next;
#endif
		if (next == NULL)
			break;
		n -= get_line_info_below(i++, next, Lno);
	}
	End_no = i;
}

/* This function has to be consistent with SLsmg. */
static void fix_char_width(void)
{
   int i;
   register int n;
   
   if (SLsmg_Display_Eight_Bit < 128)
     SLsmg_Display_Eight_Bit = 128;
   
   /* Control Characters */
   n = (kSLcode ? 0 : 4);	/* for kanji */
   for (i = 0; i < 32; i++)
     {
	Char_Width[i] = 2;
	Char_Width[i + 128] = n;
     }
   
   for (i = 32; i < 127; i++)
     {
	Char_Width[i] = 1;
	Char_Width[i + 128] = 4;
     }
   Char_Width[127] = 2;
   
   for (i = SLsmg_Display_Eight_Bit; i < 256; i++)
     Char_Width[i] = 1;
   
#if KANJI_NEW_STUFF_BETA__NOT_USE	/* To do fix by k-yosino. */
   if(kSLcode == EUC)
     {
	for (i = 0x80 ; i < 0xa0 ; i++)
	  {
	     Char_Width[i] = 0;
	  }
	if(SKanaToDKana)
	  {
	     Char_Width[0x8e] = 1;
	  }
	
     }
   else		/* SJIS */
     {
	if(SKanaToDKana)
	  {
	     for (i = 0xa0 ; i <= 0xdf ; i++)
	       {
		  Char_Width[i] = 2;
	       }
	     if(SKanaToDKana < 0)
	       {
		  Char_Width[0xde] = 0;	/* NIGORI character */
		  Char_Width[0xdf] = 0;	/* MARU character */
	       }
	  }
     }
#endif /* KANJI_NEW_STUFF_BETA */
   Display_Eight_Bit = SLsmg_Display_Eight_Bit;
   Kana_Changed = SKanaToDKana;
}

void point_column (int n)
{
   register unsigned char *p, *pmax;
   register int i;
   int tab, w;
   
   FIX_CHAR_WIDTH;
   
   if (IS_MINIBUFFER) n -= Mini_Info.effective_prompt_len;
   
   if (CLine->len == 0)
     {
	Point = 0;
	return;
     }
   p = CLine->data;
   pmax = p + (CLine->len - 1);
   if (*pmax != '\n') pmax++;
   
   tab = Buffer_Local.tab;
   i = 0;
   n--;   /* start at 0 */
   while(p < pmax)
     {
	if(iskanji(*p))
	  {
	     if(p == (pmax - 1)) break;
	     else
	       {
		  w = Char_Width[*p];
		  i += w + (w == 1 ? 1 : Char_Width[*(p+1)]);
		  p++;
	       }
	  }
	else if ((*p == '\t') && tab)
	  {
	     i = tab * (i / tab + 1);
	  }
	else
	  {
	     w = Char_Width[*p];
	     i +=  w;
/*	     if (w == 0) p++; */
	  }
	
	if (i > n) break;
	p++;
     }
   Point = (int) (p - CLine->data);

   if(iskanji2nd(CLine->data, Point))
     Point--;
}

void point_next_row ()
{
	if (DWrap) {
		Goal_Column += DWrap;
	}
	point_column (Goal_Column);
}

/* given a position in a line, return apparant distance from bol
 *   expanding tabs, etc... up to pos.
 */
int jed_compute_effective_length (unsigned char *pos, unsigned char *pmax)
{
   int i;
   int tab, w;
   register unsigned char *cw = Char_Width;
   register unsigned char ch;
   
   FIX_CHAR_WIDTH;
   
   i = 0;
   /*
   i = LNW;
   */

   tab = Buffer_Local.tab;
   
   while (pos < pmax)
     {
	ch = *pos++;
	if ((ch == '\t') && tab)
	  {
		if (DWrap) {
			int ii = i / DWrap * DWrap;
	     		i -= ii;
	     		i = tab * (i/tab + 1);
			if (i > DWrap) {
				i = DWrap;
			}
	     		i += ii;
		} else {
	     i = tab * (i/tab + 1);  /* tab column tabs */
		}
	  }
	/* Must use for KANJI_NEW_STUFF_BETA. */
	else if (iskanji(ch))
	  {
		int ii = i, ww;

	     w = cw[ch];
	     if(pos != pmax)
	       i += w + (w == 1 ? 1 : cw[*pos]);
	     pos++;
	     /* i += 2; pos++; */
		if (DWrap) {
			ww = i - ii;
			if ((i-1) % DWrap + 1 < ww) {
				i = (ii/DWrap + 1) * DWrap + ww;
				continue;
			}
		}
	  }
	else
	  {
	     w = cw[ch];
	     i += w;
/*	     if (w == 0) pos++; */
	  }
     }
/*   if(cw[*pos] == 0) i--; */
   return i;
}

unsigned char *jed_compute_effective_point (unsigned char *pos, int point)
{
   int i;
   int tab, w;
   register unsigned char *cw = Char_Width;
   register unsigned char ch;

   FIX_CHAR_WIDTH;

   i = 0;
   tab = Buffer_Local.tab;

   while (i < point)
     {
	ch = *pos++;
	if ((ch == '\t') && tab)
	  {
	     i = tab * (i/tab + 1);  /* tab column tabs */
	  }
	else
	  {
	     w = cw[ch];
	     i += w;
	  }
     }
   if (i > point) pos--;
/*   if(cw[*pos] == 0) return (char*)NULL; */
   return pos;
}

int calculate_column (void)
{
   int c;
   
   c = 1 + jed_compute_effective_length (CLine->data, CLine->data + Point);
   Absolute_Column = c;
   if(iskanji2nd(CLine->data, Point))
     c--;
   
   if (DWrap) {
	if (iskanji(CLine->data[Point]) && c % DWrap == 0) {
		c++;
	}
   }

   if (IS_MINIBUFFER) c += Mini_Info.effective_prompt_len;
   Screen_Col = c;
   return (c);
}

void point_cursor (int c)
{
   int r, row;
   Line *tthis, *cline;
   int c0;
   
   if (JWindow->trashed) return;
   
   cline = CLine;
#if JED_HAS_LINE_ATTRIBUTES
   if (cline->flags & JED_LINE_HIDDEN)
     {
	cline = jed_find_non_hidden_line (cline);
	if (cline != NULL)
	  c = Non_Hidden_Point;
     }
#endif
   
   r = JWindow->top;
   Point_Cursor_Flag = 0;
   for (row = r; row < r + JWindow->rows; row++)
     {
	tthis = JScreen[row-1].line;
	if (tthis == NULL) break;
	if ((tthis == cline) || (tthis == &Eob_Line))
	  {
	     r = row;
	     break;
	  }
     }
   
   if (Point >= CLine->len)
     {
	Point = CLine->len - 1;
	
	if (Point < 0) Point = 0;
	else if ((*(CLine->data + Point) != '\n')
		 || (CBuf == MiniBuffer)) Point++;
     }
   
   if (c == 0)
     c = calculate_column ();
   c -= (JWindow->column - 1);
   
if (DWrap == 0) {
   if (cline == HScroll_Line) c -= HScroll;
   if (c < 1) c = 1; else if (c > JWindow->width) c = JWindow->width;
} else {
	if (c < 1) c = 1;
	c0 = c;
	if (CBuf != MiniBuffer) {
		c--;
		if (c%DWrap == 0 && c > 0 && Point == CLine->len - 1
			&& *(CLine->data + Point) == '\n') {
			r += c/DWrap - 1;
			c = DWrap + 1;
		} else {
			r += c/DWrap;
			c = c%DWrap + 1;
		}
		if (JScreen[JWindow->top - 1].line == cline) {
			r -= JScreen[JWindow->top - 1].m;
		}
	}
}
   
   SLsmg_gotorc (r - 1, c - 1);
   Screen_Row = r;
   Screen_Col = c;
   
   SLsmg_refresh ();
   
/*
   if (!Cursor_Motion) Goal_Column = c;
*/
   if (!Cursor_Motion) Goal_Column = c0;
}

static unsigned long Status_Last_Time;
static unsigned long Status_This_Time;

static char *status_get_time(void)
{
   static char status_time[10];
   register char *t, ch, *t1;
   char am;
   int n;
   
   if (Display_Time == 0) return (NULL);
   if (Status_This_Time == 0) Status_This_Time = sys_time();
   if (Status_This_Time - Status_Last_Time >= 30)
     {
	Status_Last_Time = Status_This_Time;
	am = 'a';
	t = SLcurrent_time_string ();
	/* returns a string like:  "Tue Nov 2 13:18:19 1993" */
	t1 = status_time;
	while (ch = *t, (ch <= '0') || (ch > '9')) t++;
	/* on date number, skip it */
	while (*t++ != ' ');
	if (*t == '0') t++;
	if (Display_Time > 0)
	  {
	     n = 0;
	     while ((ch = *t++) != ':')
	       {
		  n = 10 * n + (int) (ch - '0');
	       }
	     if (n >= 12) am = 'p';
	     n = n % 12;
	     if (n == 0) n = 12;
	     if (n >= 10)
	       {
		  n -= 10;
		  *t1++ = '1';
	       }
	     *t1++ = '0' + n;
	     *t1++ = ':';
	     while ((*t1++ = *t++) != ':');
	     *(t1 - 1) = am; *t1++ = 'm'; *t1 = 0;
	  }
	else
	  {
	     *t1++ = '[';
	     while ((*t1++ = *t++) != ':');
	     while ((*t1++ = *t++) != ':');
	     *--t1 = ']'; *++t1 = 0;
	  }
     }
   return (status_time);
}

static void finish_status(int col_flag)
{
   char *v, ch;
   Line *l;
   int top, rows, narrows;
   char buf [128];
   char *str;
   
   v = CBuf->status_line;
   if (*v == 0) v = Default_Status_Line;
   
   while (1)
     {
	while (1)
	  {
	     int i=0;
	     i=0;
	     buf[i++] = ch = *v++;
	     if (ch == 0) return;
	     if (ch == '%')
	       break;
	     while ((ch = *v) != 0 && ch != '%')
	       {
		  buf[i] = ch;
		  i++; v++;
	       }
	     buf[i] = (char)0;
	     SLsmg_write_nchars (buf, i);
	  }
	
	ch = *v++;
	
	switch (ch)
	  {
	   case 'a':
	     if (CBuf->flags & ABBREV_MODE) str = " abbrev"; else str = NULL;
	     break;
	   case 'f': str = CBuf->file; break;
	   case 'n':
	     narrows = jed_count_narrows ();
	     if (narrows)
	       {
		  sprintf (buf, " Narrow[%d]", narrows);
		  str = buf;
	       }
	     else str = NULL;
	     break;
	     
	   case 'o':
	     if (CBuf->flags & OVERWRITE_MODE) str = " Ovwrt"; else str = NULL;
	     break;
	   case 'b': str = CBuf->name; break;
	     
	   case 'p':
	     str = buf;
	     if (0 == User_Prefers_Line_Numbers)
	       {
		  top = JWindow->top - 1;	
		  rows = JWindow->rows - 1;
		  l = JScreen[top + rows].line;
		  if (l == CBuf->end) l = NULL;
		  if (JScreen[top].line == CBuf->beg)
		    {		    
		       if (l == NULL) str = "All";
		       else str = "Top";
		    }
		  else if (l == NULL) str = "Bot";
		  else
		    {
		       sprintf(buf, "%d%%",
			       (int) ((LineNum * 100L) / (long) Max_LineNum));
		    }
	       }
	     else 
	       {
		  if (User_Prefers_Line_Numbers == 1)
		    sprintf(buf, "%d/%d", LineNum, Max_LineNum);
		  else
		    {
		       if (col_flag) (void) calculate_column ();
		       sprintf(buf, "%d/%d,%d", LineNum, Max_LineNum, Absolute_Column);
		    }
	       }
	     break;
	     
	   case 'v': str = JED_VERSION; break;
	   case 'm': str = CBuf->mode_string; break;
	   case 't': str = status_get_time(); break;
	   case 'c':
	     if (col_flag) (void) calculate_column ();
	     sprintf(buf, "%d",  Absolute_Column);
	     str = buf;
	     break;
	     
	   case '%': str = "%"; break;
	   case 0:
	     return;
	     
	   default:
	     str = NULL;
	  }
	if (str != NULL)
	  SLsmg_write_string (str);
     }
}

void set_status_format(char *f, int *local)
{
   char *s;
   if (*local) s = Default_Status_Line; else s = CBuf->status_line;
   strncpy(s, f, 79);
   s[79] = 0;
}

static int update_status_line (int col_flag)
{
   unsigned char star0, star1;
   char buf[32], *b;
   int num;
   
   if (JWindow->top == Jed_Num_Screen_Rows) return 0;   /* minibuffer ? */
   
   SLsmg_gotorc (JWindow->rows + JWindow->top - 1, 0);
   SLsmg_set_color (JSTATUS_COLOR);
   
   b = buf;
   if (JWindow->column != 1) *b++ = '<'; else *b++ = '-';
   
   if (CBuf->flags & BUFFER_MODIFIED) star0 = star1 = '*';
   else star0 = star1 = '-';
   
   if ((CBuf->flags & READ_ONLY)
#if JED_HAS_LINE_ATTRIBUTES
       || (CLine->flags & JED_LINE_IS_READONLY)
#endif
       )
     star0 = star1 = '%';
   
#if JED_HAS_SUBPROCESSES
   if (CBuf->subprocess) star1 = 'S';
#endif
   *b++ = star0;
   *b++ = star1;
   
   if (CBuf->marks != NULL) *b++ = 'm'; else *b++ = '-';
   if (CBuf->flags & FILE_MODIFIED) *b++ = 'd'; else *b++ = '-';
   if (CBuf->spots != NULL) *b++ = 's'; else *b++ = '-';
   
   if (CBuf->flags & BINARY_FILE) *b++ = 'B';
#ifdef IBMPC_SYSTEM
   else if ((CBuf->flags & ADD_CR_ON_WRITE_FLAG) == 0) *b++ = 'L';
#else
# ifdef __unix__
   else if (CBuf->flags & ADD_CR_ON_WRITE_FLAG) *b++ = 'C';
# endif
#endif
#if SUPPORT_CRONLY
   else if (CBuf->flags & CR_ONLY_ON_WRITE_FLAG) *b++ = 'M';
#endif
   else *b++ = '-';
   if (CBuf->flags & UNDO_ENABLED) *b++ = '+'; else *b++ = '-';
/* Kanji Code Flags */
   if(is_kanji_jedcode())
     {
      *b++ = *(kcode_to_str(CBuf->kfcode));	/* file kanji code */
      *b++ = *(kcode_to_str(kSLinput_code));	/* input kanji code */
      *b++ = *(kcode_to_str(kSLdisplay_code));	/* display kanji code */
     }
   else	/* not kanji mode */
     {
      *b++ = '-';
      *b++ = '-';
      *b++ = '-';
     }
/* Kanji end */
   SLsmg_write_nchars (buf, (unsigned int) (b - buf));
   
   finish_status (col_flag);
   
   if (Defining_Keyboard_Macro) SLsmg_write_string (" [Macro]");
   
   num = Jed_Num_Screen_Cols - SLsmg_get_column ();
   star1 = '-';
   while (num > 0) 
     {
	SLsmg_write_nchars ((char *) &star1, 1);
	num--;
     }
   SLsmg_set_color (0);
   Point_Cursor_Flag = 1;
   return 1;
}

static int screen_input_pending (int tsec)
{
   if (Input_Buffer_Len
#ifdef HAS_RESIZE_PENDING
       || Jed_Resize_Pending
#endif
       )
     return 1;
   
   return input_pending (&tsec);
}

/* This routine is called before the window is updated.  This means that the
 * line structures attached to the screen array cannot be trusted.  However,
 * the current line (or nearest visible one) can be assumed to lie in
 * the window.
 */
static void mark_window_attributes (int wa)
{
   register Screen_Type *s = &JScreen[JWindow->top - 1],
     *smin = s,
     *smax = s + JWindow->rows, *s1, *s2;
   Mark *m;
   register Line *l = JWindow->beg.line, *ml, *cline;
   unsigned char *hi0, *hi1;
   int mn, pn, dn, point, mpoint;
   int i1, i2;
   
   s1 = s;
	i1 = Begin_no;

   
#if JED_HAS_LINE_ATTRIBUTES
   cline = jed_find_non_hidden_line (CLine);
   if (cline != CLine) point = Non_Hidden_Point;   /* updated by jed_find_non_hidden_line */
   else point = Point;
#else
   cline = CLine;
   point = Point;
#endif
   
   if ((CBuf->vis_marks == 0) || (wa == 0) || (Wants_Attributes == 0)
#if JED_HAS_LINE_ATTRIBUTES
       || (l->flags & JED_LINE_HIDDEN)
#endif
       )
     {
	s2 = s;
	i2 = Begin_no;
	goto done;		       /* I hate gotos but they are convenient */
     }
   
   m = CBuf->marks;
   
   while ((m->flags & VISIBLE_MARK) == 0) m = m->next;
   ml = m->line;
   mn = m->n;			       /* already in canonical form */
   pn = LineNum + CBuf->nup;	       /* not in canonical form */
   dn = pn - mn;
   
#if JED_HAS_LINE_ATTRIBUTES
   ml = jed_find_non_hidden_line (ml);
   if (ml == cline) dn = 0;
   if (ml != m->line)
     {
	mpoint = Non_Hidden_Point;     /* updated by jed_find_non_hidden_line */
     }
   else mpoint = m->point;
#else
   mpoint = m->point;
#endif
   
   /* find Screen Pos of point in window.  It has to be there */
   while (l != cline)
     {
	l = l->next;
#if JED_HAS_LINE_ATTRIBUTES
	if (l->flags & JED_LINE_HIDDEN) continue;
#endif
/*
	s1++;
*/
	s1 += LineInfo[i1++].n;
     }
   
   /* s1 now points at current line */
   /* The whole point of all of this is to preserve the screen flags without
    * touching the screen.
    */
   
   if (dn > 0)			       /* mark on prev lines */
     {
/*
	s2 = s1 + 1;
*/
	s2 = s1 + LineInfo[i1].n; i2 = i1 + 1;
	hi0 = l->data;
	hi1 = l->data + point;
	if ((s1->hi0 != hi0) || (s1->hi1 != hi1))
	  {
	     s1->hi0 = hi0; s1->hi1 = hi1;
	     s1->is_modified = 1;
	  }
	
	l = l->prev;
/*
	s1--;
*/
	s1 -= LineInfo[--i1].n;
	while ((s1 >= s) && (l != ml) && (l != NULL))
	  {
#if JED_HAS_LINE_ATTRIBUTES
	     if (l->flags & JED_LINE_HIDDEN)
	       {
		  l = l->prev;
		  continue;
	       }
#endif
	     hi0 = l->data;
	     hi1 = l->data + l->len;
	     if ((s1->hi0 != hi0) || (s1->hi1 != hi1))
	       {
		  s1->hi0 = hi0; s1->hi1 = hi1;
		  s1->is_modified = 1;
	       }
/*
	     s1--;
*/
		s1 -= LineInfo[--i1].n;
	     l = l->prev;
	  }
	
	if (s1 >= s)
	  {
	     hi0 = ml->data + mpoint;
	     hi1 = ml->data + ml->len;
	     if ((s1->hi0 != hi0) || (s1->hi1 != hi1))
	       {
		  s1->hi0 = hi0; s1->hi1 = hi1;
		  s1->is_modified = 1;
	       }
/*
	     s1--;
*/
		s1 -= LineInfo[--i1].n;
	  }
     }
   else if (dn < 0)		       /* mark ahead of point */
     {
/*
	s2 = s1;
	s1--;
*/
	s2 = s1; i2 = i1;
	s1 -= LineInfo[--i1].n;
	hi0 = l->data + point;
	hi1 = l->data + l->len;
	if ((s2->hi0 != hi0) || (s2->hi1 != hi1))
	  {
	     s2->hi0 = hi0; s2->hi1 = hi1;
	     s2->is_modified = 1;
	  }
	
	l = l->next;
/*
	s2++;
*/
	s2 += LineInfo[i2++].n;
	while ((s2 < smax) && (l != ml) && (l != NULL))
	  {
#if JED_HAS_LINE_ATTRIBUTES
	     if (l->flags & JED_LINE_HIDDEN)
	       {
		  l = l->next;
		  continue;
	       }
#endif
	     hi0 = l->data;
	     hi1 = l->data + l->len;
	     if ((s2->hi0 != hi0) || (s2->hi1 != hi1))
	       {
		  s2->hi0 = hi0; s2->hi1 = hi1;
		  s2->is_modified = 1;
	       }
	     l = l->next;
/*
	     s2++;
*/
		s2 += LineInfo[i2++].n;
	  }
	
	if (s2 < smax)
	  {
	     hi0 = ml->data;
	     hi1 = ml->data + mpoint;
	     if ((s2->hi0 != hi0) || (s2->hi1 != hi1))
	       {
		  s2->hi0 = hi0; s2->hi1 = hi1;
		  s2->is_modified = 1;
	       }
/*
	     s2++;
*/
		s2 += LineInfo[i2++].n;
	  }
     }
   else				       /* same line */
     {
	if (point < mpoint)
	  {
	     s1->hi0 = l->data + point;
	     s1->hi1 = l->data + mpoint;
	  }
	else
	  {
	     s1->hi1 = l->data + point;
	     s1->hi0 = l->data + mpoint;
	  }
	s1->is_modified = 1;
/*
	s2 = s1 + 1;
	s1--;
*/
	s2 = s1 + LineInfo[i1].n; i2 = i1 + 1;
	s1 -= LineInfo[--i1].n;
     }
   
   done:			       /* reached if there is no mark */
   
   /* now do area outside the region */
   while (s1 >= s)
     {
	if (s1->hi0 != NULL)
	  {
	     s2->hi1 = s1->hi0 = NULL;
	     s1->is_modified = 1;
	  }
/*
	s1--;
*/
	s1 -= LineInfo[--i1].n;
     }
   
   while (s2 < smax)
     {
	if (s2->hi0 != NULL)
	  {
	     s2->hi1 = s2->hi0 = NULL;
	     s2->is_modified = 1;
	  }
/*
	s2++;
*/
	s2 += LineInfo[i2++].n;
     }
}


/* if force then do update otherwise return 1 if update or 0 if not */
static int update_1(Line *top, int force)
{
   int i;
   Window_Type *w;
   int did_eob = 0, time_has_expired = 0;
   
   if (Batch ||
       (!force
	&& (Executing_Keyboard_Macro || (Repeat_Factor != NULL)
	    || screen_input_pending (0)
	    || (Read_This_Character != NULL)))
       || (CBuf != JWindow->buffer))
     
     {
	return(0);
     }
   
   if (Suspend_Screen_Update != 0)
     {
	Suspend_Screen_Update = 0;
	touch_screen ();
     }
   
   JWindow->mark.line = CLine;
   JWindow->mark.point = Point;
   JWindow->mark.n = LineNum + CBuf->nup;
   CBuf->linenum = LineNum;
   CBuf->max_linenum = Max_LineNum;

   if ((DWrap = Display_Wrap_Width) < 0)
     {
	DWrap = Jed_Num_Screen_Cols;
     }
   if (DWrap > 0 && DWrap < 2)
     {
	DWrap = 2;
     }
   
   if (Wants_Attributes && CBuf->vis_marks)
     {
	JWindow->trashed = 1;
     }
   
   /* Do not bother setting this unless it is really needed */
   if (Display_Time)
     {
	Status_This_Time = sys_time();
	time_has_expired = (Status_This_Time > Status_Last_Time + 45);
     }
   
   /* if cursor moves just left right, do not update status line */
   if (!force && !JWindow->trashed && !DWrap &&
       ((JWindow == JWindow->next) || (User_Prefers_Line_Numbers && Cursor_Motion))
       /* if % wanted, assume user is like me and gets annoyed with
	* screen updates
	*/
       && (User_Prefers_Line_Numbers
	   || time_has_expired))
     {
	update_status_line(0);
	return(1);
     }
   
   if (!JWindow->trashed && Cursor_Motion && !DWrap)
     {
#if JED_HAS_LINE_ATTRIBUTES
	if (CLine->flags & JED_LINE_IS_READONLY)
	  update_status_line (0);
#endif
	return 1;
     }
   
   w = JWindow;
   do
     {
	int imax;
	int is_modified = 0;
#if JED_HAS_LINE_ATTRIBUTES
	if (CBuf->min_unparsed_line_num)
	  jed_syntax_parse_buffer (0);
#endif
	if (Wants_Syntax_Highlight) init_syntax_highlight ();
	
#if JED_HAS_LINE_ATTRIBUTES
	if (top != NULL) top = jed_find_non_hidden_line (top);
#endif
	
	n_before = 1;
	BeginLine.m = 0;
	More_Row = 0;

	if (top == NULL)
	  {
	     top = find_top();
	     if (top == NULL) top = CLine;
	  }
	
	get_info_below();

	if (BeginLine.m != JScreen[JWindow->top - 1].m) {
		JScreen[JWindow->top - 1].is_modified = 1;
	}

	if (!DWrap) {
		for (i = 0; i < JWindow->rows; i++) {
			LineInfo[Begin_no + i].n = 1;
		}
	}
	LineInfo[Begin_no - 1].n = 1;

	JWindow->beg.line = top;

#if JED_HAS_LINE_ATTRIBUTES
	if (top->flags & JED_LINE_HIDDEN)
	  top = NULL;
	else
#endif
	  mark_window_attributes ((w == JWindow) || (w->buffer != CBuf));
	
	
	did_eob = 0;
	

	i = JWindow->top - 1;
	imax = i + JWindow->rows;
	row_max = imax - 1;


	while (i < imax)
	  {
#if JED_HAS_LINE_ATTRIBUTES
	     if ((top != NULL) && (top->flags & JED_LINE_HIDDEN))
	       {
		  top = top->next;
		  ++Lno;
		  continue;
	       }
#endif
	     
	     /* the next line is really optional */
#if 0
	     if (!force && (Exit_From_MiniBuffer ||
			    screen_input_pending (0))) break;
#endif
	     
		wrap_num = 1;

	     if ((JScreen[i].line != top)
		 || JScreen[i].is_modified
		 || is_modified
/*
		 || (i == JWindow->top - 1 && JScreen[i].m != BeginLine.m)
*/
		 || (Want_Eob
		     && !did_eob
		     && (i != Jed_Num_Screen_Rows - 1)
		     && (top == NULL)))
	       {
			if (JScreen[i].is_modified) {
				is_modified = 1;
			}

		  if (((top == NULL) || (top->len == 0))
		      && (Want_Eob && !did_eob && !(CBuf->flags & READ_ONLY)))
		    {
		       display_line(&Eob_Line, i + 1);
		       
		       /* JScreen[i].line = top; */
		       did_eob = 1;
		    }
		  else display_line(top, i + 1);

			i += wrap_num;
	       }
	     else
	       {
			i++;
			while (i < imax) {
	     			if (JScreen[i-1].line != JScreen[i].line) {
					break;
				}
				i++;
			}
	       }
	     
	     if (top != NULL)
	       {
		  top = top->next;
		  Lno++;
	       }
	  }
	
	HScroll_Line = NULL;
	Mode_Has_Syntax_Highlight = 0;
	if (!force && screen_input_pending (0))
	  {
	     while(JWindow != w) other_window();
	     JWindow->trashed = 1;  /* since cursor not pointed */
	     return(0);
	  }
	else update_status_line(w != JWindow);
	
	JWindow->trashed = 0;
	
	other_window();
	top = NULL;
	/* if (!JWindow->trashed) top = JWindow->beg.line; else  top = NULL; */
	
     }
   while (JWindow != w);
   return 1;
}

int Mini_Ghost = 0;

static void update_minibuffer(void)
{
   Window_Type *w;
   
   if (Executing_Keyboard_Macro) return;
   
   if (MiniBuffer != NULL)
     {
	w = JWindow;
	while (!IS_MINIBUFFER) other_window();
#if 0
	if ((*Message_Buffer) && JScreen[Jed_Num_Screen_Rows - 1].n)
	  (void) screen_input_pending (10);
#endif
	JWindow->beg.line = CLine;
	BeginLine.m = 0;
	display_line(CLine, Jed_Num_Screen_Rows);
	while (w != JWindow) other_window();
	Mini_Ghost = 1;
     }
   else if (Mini_Ghost && !*Error_Buffer && !*Message_Buffer)
     {
	/* if < 0, it is a result of flush message so let it pass this round */
	if (Mini_Ghost < 0) Mini_Ghost = 1;
	else Mini_Ghost = 0;
     }
   else Mini_Ghost = ((*Message_Buffer) || (*Error_Buffer));
   
   if (Mini_Ghost == 0)
     display_line(NULL, Jed_Num_Screen_Rows);
}

void do_dialog(char *b)
{
   char *quit = "Quit!";
   
   if (Batch) return;
   FIX_CHAR_WIDTH;
   if (! *b)
     {
	if(!SLKeyBoard_Quit) return;
	b = quit;
     }
   
   if ((b == Error_Buffer) || (b == quit))
     {
	SLsmg_set_color (JERROR_COLOR);
	touch_screen();
     }
   else
     SLsmg_set_color (JMESSAGE_COLOR);
   
   SLsmg_Newline_Behavior = SLSMG_NEWLINE_PRINTABLE;
   SLsmg_gotorc (Jed_Num_Screen_Rows - 1, 0);
   SLsmg_write_string (b);
   SLsmg_set_color (0);
   SLsmg_erase_eol ();
   SLsmg_Newline_Behavior = 0;
   
   if ((b == Error_Buffer) || (SLKeyBoard_Quit))
     {
	jed_beep();
	flush_input();
     }
   
   if (*b)
     {
	if (MiniBuffer != NULL)
	  {
	     SLsmg_refresh ();
	     (void) input_pending(&Number_Ten);
	  }
	Mini_Ghost = -1;
     }
   else Mini_Ghost = 0;
}

static void set_hscroll(int col)
{
   int hdiff, whs = abs(Wants_HScroll), wc = JWindow->column - 1,
     sw = Jed_Num_Screen_Cols - 1;
   static Line *last;
   Line *tmp;
   
   /* take care of last effect of horizontal scroll */
   if (last != NULL)
     {
	tmp = CLine;
	CLine = last;
	register_change(0);
	CLine = tmp;
	if (last != CLine)
	  {
#if 0
	     /* I need to think about this more */
	     if (Wants_HScroll < 0)
	       {
		  if (wc != 0)
		    {
		       JWindow->column = 1;
		       wc = 0;
		       touch_window ();
		    }
	       }
#endif
	     HScroll = 0;
	  }
	
	last = NULL;
     }
   
   col--;			       /* use 0 origin */
   hdiff = col - wc;
   if ((HScroll >= hdiff)
       || (HScroll <= hdiff - sw + (iskanji(what_char()) ? 1 : 0)))
     {
	if ((hdiff + (iskanji(what_char()) ? 1 : 0)) >= sw)
	  {
	     HScroll = hdiff - sw + whs;
	  }
	else if ((hdiff == 0) && (wc == 0)) HScroll = 0;
	else if (hdiff <= 1)
	  {
	     HScroll = hdiff - whs - 1;
	  }
	else HScroll = 0;
     }
   
   if (HScroll)
     {
	if (wc + HScroll < 0) HScroll = -wc;
	
	if (Wants_HScroll < 0)
	  {
	     JWindow->column += HScroll;
	     touch_window();
	     HScroll = 0;
	  }
	else
	  {
	     register_change(0);
	     last = HScroll_Line = CLine;
	  }
     }
}

static char Top_Screen_Line_Buffer[132] = "If you see this, you have an installation problem.";

void define_top_screen_line (char *neew)
{
   SLang_push_string (Top_Screen_Line_Buffer);
   strncpy (Top_Screen_Line_Buffer, neew, sizeof (Top_Screen_Line_Buffer) - 1);
   Top_Screen_Line_Buffer[sizeof(Top_Screen_Line_Buffer) - 1] = 0;
   if (JScreen != NULL)
     JScreen[0].is_modified = 1;
}

static void update_top_screen_line (void)
{
#if JED_HAS_TTY_MENUS
   if (Jed_Menus_Active
       || (Top_Window_Row != 1))
     {
	jed_redraw_menus ();
	return;
     }
#else
   if (Top_Window_Row == 1)
     return;
   
   SLsmg_gotorc (0,0);
   SLsmg_set_color (JMENU_COLOR);
   SLsmg_write_string (Top_Screen_Line_Buffer);
   SLsmg_erase_eol ();
   JScreen[0].is_modified = 0;
#endif
}

/* if flag is non-zero, do not touch the message/error buffers */
void update(Line *line, int force, int flag, int run_update_hook)
{
   int pc_flag = 1;
   int col;
   static unsigned long last_time;
   Line *hscroll_line_save;
   
#if JED_HAS_SUBPROCESSES
   if (Child_Status_Changed_Flag)
     {
	jed_get_child_status ();
	force = 1;
     }
#endif
   
   if (Batch) return;
   
   if (!force && !SLang_Error && !SLKeyBoard_Quit && (!*Error_Buffer))
     {
	if (screen_input_pending (0))
	  {
	     JWindow->trashed = 1;
	     return;
	  }
     }
   
   if (last_time + 30 < Status_This_Time)
     {
	if (last_time == 0) last_time = Status_This_Time;
	else
	  {
	     last_time = Status_This_Time;
	     if (SLang_run_hooks ("update_timer_hook", 0))
	       flag = 0;
	  }
     }
   
   if ((SLang_Error == 0) && (CBuf->update_hook != NULL) && run_update_hook)
     {
	Suspend_Screen_Update = 1;
	SLexecute_function (CBuf->update_hook);
	if (SLang_Error) CBuf->update_hook = NULL;
     }
   
   if (Suspend_Screen_Update != 0)
     {
	Suspend_Screen_Update = 0;
	touch_screen ();
     }
   
   if (X_Update_Open_Hook != NULL) (*X_Update_Open_Hook) ();
   FIX_CHAR_WIDTH;
   
   col = calculate_column ();
   HScroll_Line = NULL;
if (DWrap == 0) {
   if (Wants_HScroll) set_hscroll(col); else HScroll = 0;
} else {
   HScroll = 0;
}
   hscroll_line_save = HScroll_Line;
   
   if (SLang_Error) flag = 0;	       /* update hook invalidates flag */
   
   if (SLang_Error && !(*Error_Buffer || SLKeyBoard_Quit)) SLang_doerror(NULL);
   
   if (!flag && (*Error_Buffer || SLKeyBoard_Quit))
     {
	do_dialog(Error_Buffer);
	SLKeyBoard_Quit = 0;
	SLang_restart(0);
	SLang_Error = 0;
	Mini_Ghost = 1;
	(void) update_1(line, 1);
	update_minibuffer();
     }
   else if ((flag == 0) && *Message_Buffer)
     {
	if (!update_1(line, force))
	  goto done;
	
	do_dialog(Message_Buffer);
	Mini_Ghost = 1;
	update_minibuffer();
     }
   else
     {
	pc_flag = JWindow->trashed || (JWindow != JWindow->next) || Cursor_Motion;
	if (!flag) update_minibuffer();
	if (!update_1(line, force)) goto done;
     }
   if (!flag) *Error_Buffer = *Message_Buffer = 0;
   
#if JED_HAS_TTY_MENUS
   update_top_screen_line ();
#else
   if ((Top_Window_Row != 1) && JScreen[0].is_modified)
     {
	update_top_screen_line ();
     }
#endif
   done:
   
   HScroll_Line = hscroll_line_save;
   
   if (MiniBuf_Get_Response_String != NULL)
     {
	do_dialog (MiniBuf_Get_Response_String);
	Mini_Ghost = 1;
     }
   else if (Point_Cursor_Flag || pc_flag)
     point_cursor(col);
   
   if (X_Update_Close_Hook != NULL) (*X_Update_Close_Hook) ();
   if(iskanji2nd(CLine->data, Point)) Point--;
   
   SLsmg_refresh ();
}

/* search for the CLine in the SCreen and flag it as changed */
/* n = 0 means line was changed, n = 1 means it was destroyed */
void register_change(int n)
{
   Window_Type *w;
   register Screen_Type *s, *smax;
   register Line *cl = CLine;
   
   if (JScreen == NULL)
     return;
   
   JWindow->trashed = 1;
   if (Suspend_Screen_Update) return;
   if (No_Screen_Update)
     {
	No_Screen_Update = 0;
	if (((n == CINSERT) || (n == CDELETE)) && (JWindow->next == JWindow))
	  {
	     /* Since no screen update, we are probably safe to do: */
	     /* JScreen[Screen_Row - 1].flags = 1; */
	     return;
	  }
	w = JWindow->next;  /* skip this window */
     }
   else w = JWindow;
   
   do
     {
	s = &JScreen[w->top - 1];
	smax = s + w->rows;
	
	while (s < smax)
	  {
	     if (s->line == cl)
	       {
		  s->is_modified = 1;
		  if ((n == NLDELETE) || (n == LDELETE)) s->line = NULL;
		  w->trashed = 1;
	       }
	     s++;
	  }
	w = w->next;
     }
   while(w != JWindow);
}

int jed_get_screen_size (int *r, int *c)
{
   if (tt_get_screen_size == NULL)
     {
	*r = 24;
	*c = 80;
	return 0;
     }
   
   (void) (*tt_get_screen_size) (r, c);
   if (*r <= 0) *r = 24;
   if (*c <= 0) *c = 80;
   
   if (*r > 1024) *r = 24;
   if (*c > 1024) *c = 80;
   
   /* Let's not be ridiculous */
   if (*r <= 5) *r = 5;
   if (*c <= 5) *c = 5;
   
   return 0;
}

static int Display_Initialized;

static void dealloc_display (void)
{
   if (JScreen != NULL)
     {
	SLfree ((char *) JScreen);
	JScreen = NULL;
	SLfree ((char *) JScreen2);
	JScreen2 = NULL;
	SLfree ((char *) RowInfo);
	RowInfo = NULL;
	SLfree ((char *) LineInfo0);
	LineInfo0 = NULL;
     }
}

void jed_reset_display (void)
{
   if (Display_Initialized == 0) return;
   if (Batch) return;
   
   Display_Initialized = 0;
   
#if defined(VMS) || defined(REAL_UNIX_SYSTEM)
   (void) SLang_run_hooks ("reset_display_hook", 0);
#endif
   
   dealloc_display ();
   SLsmg_reset_smg ();
}


static void alloc_display (void)
{
   int r, c, i;
   
   jed_get_screen_size (&r, &c);
   jed_update_window_sizes (r, c);
   
   if (NULL == (JScreen = (Screen_Type *) SLmalloc (sizeof (Screen_Type) * r)))
     exit_error ("Out of memory", 0);
   if (NULL == (JScreen2 = (Screen_Type *) SLmalloc (sizeof (Screen_Type) * r)))
     exit_error ("Out of memory", 0);
   MaxRowInfo = r;
   if (NULL == (RowInfo = (RowInfo_Type *) SLmalloc (sizeof (RowInfo_Type) * MaxRowInfo)))
     exit_error ("Out of memory", 0);
   if (NULL == (LineInfo0 = (LineInfo_Type *) SLmalloc (sizeof (LineInfo_Type) * 2 * r)))
     exit_error ("Out of memory", 0);

   LineInfo = LineInfo0 + r;
   
   memset ((char *) JScreen, 0, r * sizeof(Screen_Type));
   for (i = 0; i < r; i++)
     JScreen[i].is_modified = 1;
   
   Jed_Num_Screen_Cols = c;
   Jed_Num_Screen_Rows = r;
}

void jed_init_display (void)
{
   if (Batch) return;
   
   jed_reset_display ();
   alloc_display ();
   
   if (-1 == SLsmg_init_smg ())
     exit_error ("init_display: error initializing display", 0);
   
   
#if defined(VMS) || defined(REAL_UNIX_SYSTEM)
   (void) SLang_run_hooks ("init_display_hook", 0);
#endif
   Display_Initialized = 1;
}

void jed_resize_display (void)
{
   Jed_Resize_Pending = 0;
   
   if (Display_Initialized == 0)
     return;
   
   dealloc_display ();
   alloc_display ();
   SLsmg_reinit_smg ();
   jed_redraw_screen (0);
}

void jed_redraw_screen (int force)
{
   int row, center;
   Window_Type *w;
   Line *l;
   
   if (Batch) return;
   SLsmg_set_color (0);
   SLsmg_cls ();
   
   if (JScreen == NULL)
     return;
   
   for (row = 0; row < Jed_Num_Screen_Rows; row++)
     {
	JScreen[row].line = NULL;
	JScreen[row].is_modified = 1;
     }
   
   if (NULL == (w = JWindow))
     return;
   
   center = JWindow->trashed;
   do
     {
	w->trashed = 1;
	w = w->next;
     }
   while(w != JWindow);
   
   if (center)
     {
	for (row = 0; row < JWindow->rows; row++)
	  {
	     JScreen[row + JWindow->top - 1].line = NULL;
	  }
	l = NULL;
     }
   else l = JWindow->beg.line;
   
   update(l, force, 0, 1);
}

void recenter(int *np)
{
   Line *l = CLine;
   int i, n = *np;

   if (Batch)
     return;

   JWindow->trashed = 1;
   if (n == 0)
     {
	n = JWindow->rows / 2;
	i = 0;
	while (i < n)
	  {
	     if (l->prev == NULL) break;
	     l = l->prev;
#if JED_HAS_LINE_ATTRIBUTES
	     if (l->flags & JED_LINE_HIDDEN) continue;
#endif
	     i++;
	  }
	JWindow->beg.line = l;
	JWindow->beg.n -= i;
	JWindow->beg.point = 0;
	jed_redraw_screen (0);
	return;
     }
   
   if (CBuf != JWindow->buffer) return;
   
   if ((n <= 0) || (n > JWindow->rows)) n = JWindow->rows / 2;
   
   while (n > 1)
     {
	l = l->prev;
	if (l == NULL)
	  {
	     l = CBuf->beg;
	     break;
	  }
#if JED_HAS_LINE_ATTRIBUTES
	if (l->flags & JED_LINE_HIDDEN) continue;
#endif
	n--;
     }
   
   /* update(l, 1, 0, 1); */
   JScreen [JWindow->top - 1].line = l;
   JScreen [JWindow->top - 1].is_modified = 1;
   JScreen [JWindow->top - 1].m = 0;
}

int window_line (void)
{
   Line *cline;
   Line *top;
   int n;
   
   if (CBuf != JWindow->buffer) return 0;
   
   top = find_top ();
   
#if JED_HAS_LINE_ATTRIBUTES
   cline = jed_find_non_hidden_line (CLine);
#else
   cline = CLine;
#endif
   
   n = 1;
   
   while ((top != NULL) && (top != cline))
     {
	top = top->next;
#if JED_HAS_LINE_ATTRIBUTES
	while ((top != NULL) && (top->flags & JED_LINE_HIDDEN))
	  top = top->next;
#endif
	n++;
     }
   return n;
}

void get_point (int x, int y, int *linep, int *colp)
{
   Line *cline;
   Line *top;
   int n, i, point;
   
   if (CBuf != JWindow->buffer) return;
   
   n_before = 1;
   BeginLine.m = 0;
   More_Row = 0;

   top = find_top ();
   
   get_info_below();

	y -= JWindow->top;
	if (y < 0)
		y = 0;
	else if (y >= JWindow->rows)
		y = JWindow->rows - 1;
	for (i = Begin_no; y >= 0; i++) {
		y -= LineInfo[i].n;
	}
	y += LineInfo[--i].n;

	*linep = LineInfo[i].lno;
	if (i == Begin_no) {
		y += BeginLine.m;
	}
	if (x > DWrap) x = DWrap;
	*colp = y * DWrap + x;
}

void touch_window (void)
{
   Screen_Type *s, *smax;
   
   if (Suspend_Screen_Update) return;
   
   if (JScreen == NULL)
     return;
   
   s = JScreen + (JWindow->top - 1);
   if (JWindow->rows > Jed_Num_Screen_Rows)
     smax = s + Jed_Num_Screen_Rows;
   else
     smax = s + JWindow->rows;
   
   while (s < smax)
     {
	s->is_modified = 1;
	s++;
     }
   JWindow->trashed = 1;
}

void touch_screen (void)
{
   Window_Type *w;
   
   No_Screen_Update = 0;
   
   if (Suspend_Screen_Update) return;
   if (JScreen == NULL) return;
   
   w = JWindow;
   do
     {
	touch_window();
	JWindow = JWindow->next;
     }
   while(w != JWindow);
   if (Top_Window_Row != 1) JScreen[0].is_modified = 1;
}

void exit_error(char *str, int severity)
{
   static int already_here;
   
   if (already_here)
     return;
   
   SLang_Error = SLKeyBoard_Quit = 0;
   auto_save_all();
   jed_reset_display();
   reset_tty();
   fprintf(stderr,"\
\007\rJED (%s): Fatal Error: %s\nS-Lang Version: %u\n",
	   JED_VERSION, str, SLANG_VERSION);
   
   if (*Error_Buffer) fprintf (stderr, "%s\n", Error_Buffer);
   if (CBuf != NULL)
     {
	if (Batch == 0)
	  {
	     fprintf(stderr, "CBuf: %p, CLine: %p, Point %d\n", CBuf, CLine, Point);
	     if (CLine != NULL) fprintf(stderr, "CLine: data: %p, len = %d, next: %p, prev %p\n",
					CLine->data, CLine->len, CLine->next, CLine->prev);
	     fprintf(stderr, "Max_LineNum: %d, LineNum: %d\n", Max_LineNum, LineNum);
	     if (JWindow != NULL) fprintf(stderr, "JWindow: %p, top: %d, rows: %d, buffer: %p\n",
					  JWindow, JWindow->top, JWindow->rows, JWindow->buffer);
	  }
     }
   if (severity)
     {
#ifdef __unix__
	fprintf(stderr, "Dumping Core.");
	abort ();
#endif
     }
   exit (1);
}

void touch_window_hard(Window_Type *w, int all)
{
   Window_Type *wsave = w;
   
   if (JScreen == NULL)
     return;
   
   do
     {
	Screen_Type *s, *smax;
	
	s = JScreen + (w->top - 1);
	if (w->rows > Jed_Num_Screen_Rows)
	  smax = s + Jed_Num_Screen_Rows;
	else
	  smax = s + w->rows;
	
	while (s < smax)
	  {
	     s->is_modified = 1;
	     s->line = NULL;
	     s++;
	  }
	w->trashed = 1;
	w = w->next;
     }
   while (all && (w != wsave));
}

int jed_find_line_on_screen (Line *l)
{
   int i, imax;
   
   if (JScreen == NULL)
     return -1;
   
   imax = Jed_Num_Screen_Rows - 2;
   for (i = 0; i < imax; i++)
     {
	if (JScreen[i].line == l)
	  return i;
     }
   return -1;
}


void update_cmd (int *force)
{
   if (Batch) return;
   JWindow->trashed = 1;
   update((Line *) NULL, *force, 0, 1);
}

void update_sans_update_hook_cmd (int *force)
{
   if (Batch) return;
   JWindow->trashed = 1;
   update((Line *) NULL, *force, 0, 0);
}

