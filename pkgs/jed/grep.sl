variable Grep_Buffer = "*grep*";
variable Grep_Targets = Null_String;
variable Grep_Current_Directory = Null_String;
variable Grep_Reg_Exp = Null_String;

!if (keymap_p ($1)) make_keymap ("grep");
definekey ("grep_open",	"\r",	"grep");

define grep_format ()
{
   variable pat, not_found = 1;
   variable ch, len;

   bob ();
   do
     {
	bol();
	push_mark();
	ffind (":");
	go_right (bfind ("/"));
	del_region();
     }
   while (down (1));

   bob ();
   insert ("== ");
   insert (sprintf ("grep %s %s", Grep_Reg_Exp, Grep_Targets));
   newline ();
}  

define grep ()
{
   variable prompt, file, flags;

   get_token();

   Grep_Reg_Exp = read_mini("Grep:", Null_String, Null_String);
   !if (strlen (Grep_Reg_Exp)) return;

   prompt = strcat (strcat ("Grep: '", Grep_Reg_Exp), "' ");
   Grep_Targets = read_file_from_mini (prompt);
   
   pop2buf (Grep_Buffer);
   (file,,,flags) = getbuf_info ();
   setbuf_info (file, Grep_Targets, Grep_Buffer, flags);
   set_status_line (" GREP: %b   (%m%n)  (%p)", 0);
   set_readonly (0);
   erase_buffer ();
   use_keymap ("grep");
   set_mode("grep",0);
   shell_cmd (sprintf ("grep -n %s %s 2>/dev/null",
		       Grep_Reg_Exp, Grep_Targets));

   (Grep_Current_Directory,) = parse_filename (Grep_Targets);
   
   grep_format();
   set_buffer_modified_flag (0); set_readonly(1);
   flush (Null_String);
}

define grep_open ()
{
   variable name, lineno;

   bol();
   push_mark();
   go_right (ffind (":"));
   go_left_1();
   name = dircat(Grep_Current_Directory, bufsubstr ());

   go_right_1 ();
   push_mark();
   go_right (ffind (":"));
   go_left_1();
   lineno = bufsubstr ();

   !if ( read_file (name) ) error ("Unable to read file.");
   
   pop2buf (whatbuf ());
   goto_line (integer(lineno));
}
 
