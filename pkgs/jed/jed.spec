%global momorel 41
%global ver1 0.99
%global ver2 13
%global jver jp0
%global slangver 1.4.5

# USE_WRAP option is switch enable/desable wrap mode support.
# rpm -ba --define "USE_WRAP=yes" (wrap mode support on)
%global use_wrap 0
%{?USE_WRAP:%global use_wrap 1}

Summary: A fast, compact editor based on the slang screen library
Name: jed
Version: %{ver1}.%{ver2}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source0: ftp://space.mit.edu/pub/davis/jed/v0.99/jed-B%{ver1}-%{ver2}.tar.bz2
Source3: ruby.sl
Source6: tcp.c
Source7: jed.sh
Source8: sample.jedrc-kondara
Source9: sample.skkrc-kondara
Source10: xjed.desktop

# slang-ja
Source500: ftp://space.mit.edu/pub/davis/slang/v1.4/slang-%{slangver}.tar.bz2

Patch0: jed-B0.99-13jp0-1.patch.gz
Patch1: jed-B0.99-13jp0-make.patch
Patch2: jed-B0.99-8__J067-xjed.patch
Patch3: jed-B0.99-13jp0-jedrc.patch
# Patch4: jed-B0.99-8__J067-mouse.patch
Patch10: jed-B0.99-13jp0-wrap.patch.gz
Patch100: jed-B0.99-13jp0-sl.patch
Patch104: jed-info.patch
Patch105: jed-B0.99-13jp0-backtothefuture.patch
Patch106: jed-C-h.patch
Patch107: skk.diff
Patch108: jed-format-jp.patch
Patch109: skk.paste.diff
Patch110: skk_sjis.paste.diff
Patch111: jed-B0.99-13jp0-quoted_insert.patch
Patch112: jed-B0.99-13jp0-changelog.patch

Patch200: jed-0.99.13-slang-1.4.5-gcc4.patch

# slang-ja
Patch500: slang-1.4.5jp0.patch

Requires: jed-common = %{version}
BuildRequires: gpm-devel
URL: http://space.mit.edu/~davis/jedsoft/jed/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Jed is a fast, compact editor based on the slang screen library.  Jed
features include emulation of the Emacs, EDT, WordStar and Brief editors;
support for extensive customization with slang macros, colors,
keybindings, etc.; and a variety of programming modes with syntax
highlighting.

You should install jed if you've used it before and you like it, or if you
haven't used any text editors before and you're still deciding what you'd
like to use.  You'll also need to have slang installed.

%package common
Summary: Files needed by any Jed editor
Group: Applications/Editors
Requires(post): info
Requires(preun): info

%description common
The jed-common package contains files (such as .sl files) that are
needed by any jed binary in order to run.

%package -n xjed
Requires: jed-common = %{version}
Summary: The X Window System version of the Jed text editor
Group: Applications/Editors

%description -n xjed
Xjed is a version of the Jed text editor that will work with the X Window
System.
  
You should install xjed if you like Jed and you'd like to use it with X.
You'll also need to have the X Window System installed.

%package -n rgrep
Summary: A grep utility which can recursively descend through directories
Group: Applications/Text

%description -n rgrep
The rgrep utility can recursively descend through directories as
it greps for the specified pattern.  Note that this ability does
take a toll on rgrep's performance, which is somewhat slow.  Rgrep
will also highlight the matching expression.

Install the rgrep package if you need a recursive grep which can
highlight the matching expression.

%prep
#%setup -q -a500 -n jed-B%{ver1}-%{ver2}%{jver}
%setup -q -a500 -n jed-B%{ver1}-%{ver2}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
# %patch4 -p1
%if %{use_wrap}
%patch10 -p1
%endif
%patch100 -p1
%patch104 -p1
%patch105 -p1
%patch106 -p1
%patch107 -p1
%patch108 -p1
%patch109 -p1
%patch110 -p1
%patch111 -p1
%patch112 -p1

cp $RPM_SOURCE_DIR/ruby.sl lib/ruby.sl

# slang-ja
pushd slang-%{slangver}
%patch500 -p1
%patch200 -p2 -b .gcc4
popd

%build
# build libslang.a
pushd slang-%{slangver}
perl -p -i -e 's|(ELF_CFLAGS=\"[^\"]*)-O2([^\"]*\".*)|$1 %{optflags} $2|gs' configure
cp /usr/share/libtool/config/config.{sub,guess} autoconf

%configure  --includedir=%{_includedir}/slang

# Parallel build shall fail!
# please use make -j1
%undefine _smp_mflags
make %{?_smp_mflags} all

popd

chmod +x configure
CFLAGS="%{optflags}" JED_ROOT=%{_libdir}/jed ./configure --host=i386-momonga-linux --prefix=/usr
perl -p -i -e 's|-lslang|'"$RPM_BUILD_DIR/%{name}-B%{ver1}-%{ver2}/slang-%{slangver}/src/objs/libslang.a"'|gs' src/Makefile 
make %{?_smp_mflags} \
	SLANG_INC="-I$RPM_BUILD_DIR/%{name}-B%{ver1}-%{ver2}/slang-%{slangver}/src" \
	SLANG_LIB=$RPM_BUILD_DIR/%{name}-B%{ver1}-%{ver2}/slang-%{slangver}/src/objs/libslang.a \
	all
make %{?_smp_mflags} \
	SLANG_INC="-I$RPM_BUILD_DIR/%{name}-B%{ver1}-%{ver2}/slang-%{slangver}/src" \
	SLANG_LIB=$RPM_BUILD_DIR/%{name}-B%{ver1}-%{ver2}/slang-%{slangver}/src/objs/libslang.a \
	xjed

cp $RPM_SOURCE_DIR/tcp.c .
cc -O -o skktcp tcp.c

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}/etc/X11/applnk/Utilities
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/jed
mkdir -p %{buildroot}%{_mandir}/man1

cp -r lib %{buildroot}/%{_libdir}/jed
mkdir -p %{buildroot}%{_infodir}
cp -r info/jed* %{buildroot}%{_infodir}

install -m 0755 skktcp %{buildroot}/usr/bin/skktcp
cd src/objs
install -m 0755 -s jed %{buildroot}%{_bindir}
install -m 0755 -s xjed %{buildroot}%{_bindir}
install -m 0755 -s rgrep %{buildroot}%{_bindir}
JED_ROOT=%{buildroot}%{_libdir}/jed %{buildroot}/usr/bin/jed -batch -n -l preparse.sl

cd ../../doc/manual
install -m 644 jed.1 %{buildroot}%{_mandir}/man1
install -m 644 rgrep.1 %{buildroot}%{_mandir}/man1

mkdir -p %{buildroot}/etc/profile.d
install -m 755 %{SOURCE7} %{buildroot}/etc/profile.d

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/applications/

install -d %{buildroot}%{_datadir}/config-sample/jed
install -m644 %{SOURCE8} %{buildroot}%{_datadir}/config-sample/jed/dot-jedrc
install -m644 %{SOURCE9} %{buildroot}%{_datadir}/config-sample/jed/dot-skkrc

rm -f %{buildroot}/etc/profile.d/jed.sh

%clean
rm -rf --preserve-root %{buildroot}

%post common
/sbin/install-info %{_infodir}/jed.info %{_infodir}/dir

%preun common
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/jed.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%{_bindir}/jed
%{_bindir}/skktcp

%files common
%defattr(-,root,root)
%doc COPYING COPYRIGHT doc INSTALL INSTALL.unx README changes.txt jed-j.doc
%{_mandir}/man1/jed.1*
%{_infodir}/jed.*
%{_libdir}/jed
%{_datadir}/config-sample/jed

%files -n xjed
%defattr(-,root,root)
%{_bindir}/xjed
%{_datadir}/applications/xjed.desktop

%files -n rgrep
%defattr(-,root,root)
%{_bindir}/rgrep
%{_mandir}/man1/rgrep.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.13-41m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.13-40m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.13-39m)
- full rebuild for mo7 release

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.13-38m)
- Requires(post,preun): info

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.13-37m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.99.13-36m)
- rebuild against libtool-2.2.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.13-35m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.13-34m)
- update Patch3 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.13-33m)
- rebuild against gcc43

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.99.13-32m)
- revised installdir

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.13-31m)
- add gcc4 patch
- Patch200: jed-0.99.13-slang-1.4.5-gcc4.patch

* Tue Jul 19 2005 TOru Hoshina <t@momonga-linux.org>
- (0.99.13-30m)
- quoted_insert is unbinded from '`'.
- add "ChangeLog" func as 'c-x 4 a',  need to evalfile("chglog") at ~/.jedrc.

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.13-29m)
- move xjed.desktop to %%{_datadir}/applications/
- add Source10: xjed.desktop

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (0.99.13-28m)
- enable x86_64.

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.99.13-27m)
- revised spec for enabling rpm 4.2.

* Thu May 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.99.13-26m)
- revise %%post, %%preun

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.99.13-25m)
- change URL
- use %%{?_smp_mflags}
- use own slang
   a steppingstone for replacing slang with slang-utf8

* Mon Apr 14 2003 Kenta MURATA <muraken2@nifty.com>
- (0.99.13-24m)
- add BuildPreReq: slang-devel.

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.13-23m)
- add URL tag

* Fri Feb 15 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.99.13-22k)
- Source0 not found in Official Site.
  it was included.

* Tue Dec  4 2001 Toru Hoshina <t@kondara.org>
- (0.99.13-20k)
- add sample.skkrc
- applied some patch.

* Tue Dec 04 2001 Motonobu Ichimura <famao@kondara.org>
- (0.99.13-18k)
- config-sample
- modify sample.jedrc-kondara

* Wed Sep 19 2001 Toru Hoshina <t@kondara.org>
- (0.99.13-14k)
- wrap patch is turned off, still it could be enabled by option.

* Thu Jun  7 2001 Toru Hoshina <toru@df-usa.com>
- (0.99.13-14k)
- partial fixed skk.sl and skk_sjis.sl.

* Wed Jun  6 2001 Toru Hoshina <toru@df-usa.com>
- (0.99.13-12k)
- applied ruby.sl.diff, thanx > Hideki Ikemoto <gh8h-ikmt@asahi-net.or.jp>

* Wed May 30 2001 Toru Hoshina <toru@df-usa.com>
- gpmmouse disabled again :-P

* Fri May 25 2001 Toru Hoshina <toru@df-usa.com>
- No comment, please...

* Thu Nov 30 2000 Toru Hoshina <toru@df-usa.com>
- renamed to jed-ja, but...

* Sun Oct 29 2000 Toru Hoshina <toru@df-usa.com>
- applied [s-lang:01031] [s-lang:01041]

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed May 17 2000 MATSUDA, Daiki <dyky@df-usa.com>
- remake because of slang-ja is modified

* Sat Mar 11 2000 Kikutani Makoto <g@kondara.org>
- new upstream version

* Sun Dec 19 1999 Tenkou N. Hattori <tnh@kondara.org>
- Package name is changed to slang-ja.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Nov 5 1999 Daisuke Sato <d@kondara.org>
- add xterm.c for 'cut and paste'

* Sat Oct 30 1999 Daisuke Sato <d@kondara.org>
- fixed /etc/profile.d/jed.sh

* Thu Oct 28 1999 Daisuke Sato <d@kondara.org>
- fixed jed.rc
- if not exist ~/.jedrc, copy /usr/lib/jed/lib/jed.rc.
- package name changed, jed-xjed -> xjed

* Wed Sep 1 1999 Toru Hoshina <hoshina@best.com>
- added canna patch.

* Sun Aug 1 1999 Toru Hoshina <hoshina@best.com>
- version up. 0.99.8.

* Tue Jun 22 1999 Toru Hoshina <hoshina@best.com>
- fixed missing skk.sl issue, sorry about that.

* Wed Jun 16 1999 Toru Hoshina <hoshina@best.com>
- fixed paste issue.
- fixed skkeuc.sl in order to correct hirakana, katakana and zenkaku convertion.

* Wed May 19 1999 Toru Hoshina <hoshina@best.com>
- version up. 0.99.4.

* Tue Apr 20 1999 Toru Hoshina <hoshina@best.com>
- added skk-mouse patch.

* Sat Apr 10 1999 Toru Hoshina <hoshina@best.com>
- rebuild against rawhide 1.3.3
- fixed ruby.sl.

* Tue Mar 16 1999 Toru Hoshina <hoshina@best.com>
- rebuild against rawhide 1.2.9
- modified spec file in order to be based on rawhide's one.

* Thu Oct 29 1998 Bill Nottingham <notting@redhat.com>
- update to 0.98.7 for Raw Hide
- split off lib stuff into jed-common

* Mon Oct  5 1998 Jeff Johnson <jbj@redhat.com>
- change rgep group tag, same as grep.

* Sat Aug 15 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 15 1998 Erik Troan <ewt@redhat.com>
- built against new ncurses

* Mon Nov  3 1997 Michael Fulbright <msf@redhat.com>
- added wmconfig entry for xjed

* Tue Oct 21 1997 Michael Fulbright <msf@redhat.com>
- updated to 0.98.4
- included man pages in file lists

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
