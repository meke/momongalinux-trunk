%global momorel 1

Summary: "Nanika/Uakagaka" compatible desktop mascot
Name: ninix-aya
Version: 4.2.1
Release: %{momorel}m%{?dist}
License: GPLv2
URL:   http://ninix-aya.sourceforge.jp/
Group: Amusements/Graphics
Source0: http://dl.sourceforge.jp/%{name}/52799/%{name}-%{version}.tgz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python
Requires: pygtk2
Requires: gettext
Requires: unzip
Requires: numpy
Requires: python-httplib2
BuildRequires: python
BuildRequires: gettext

%description
ninix-aya is the extended version of ninix which is "Ale igai no Nanika" or 
"Ukagaka" compatible desktop mascot.

This package doesn't contain any ghosts and balloons. 
Please install one using ninix-install.

%prep
%setup -q

%build

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}
make DESTDIR=%{buildroot} prefix=i%{buildroot}%{_prefix} \
     bindir=%{buildroot}%{_bindir} \
     localedir=%{buildroot}%{_datadir}/locale \
     docdir=%{buildroot}%{_datadir}/doc/%{name}-%{version} \
     exec_libdir=%{_libdir}/ninix install

%files
%defattr(-,root,root)
%docdir %{_datadir}/doc/%{name}-%{version}
%{_datadir}/doc/%{name}-%{version}
%{_bindir}/ninix
%{_bindir}/ninix-install
%{_libdir}/ninix
%{_datadir}/locale/*/LC_MESSAGES/ninix.mo

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Sat Aug  6 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.4-3m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (4.1.4-2m)
- Add Requires: numpy

* Mon Feb 28 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (4.1.4-1m)
- Initial Build for Momonga Linux
