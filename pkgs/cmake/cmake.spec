%global momorel 1
%global majorver 2.8

# Bootstrap sequence is
# 1) rpmbuild cmake with bootstrap=1 and install
# 2) build xmlrpc-c and install
# 3) rebuild cmake with bootstrap=0 and install
%global bootstrap 0

Name:           cmake
Summary:        Cross-platform make system
Version:        2.8.12.2
Release:        %{momorel}m%{?dist}
Group:          Development/Tools
License:        Modified BSD
URL:            http://www.cmake.org/
Source0:        http://www.cmake.org/files/v%{majorver}/%{name}-%{version}.tar.gz
Source1:        macros.cmake
Source2:        macros.kde4
NoSource:       0
# Patch to find DCMTK in Fedora (bug #720140)
Patch0:         cmake-dcmtk.patch
# Patch to fix RindRuby vendor settings
# http://public.kitware.com/Bug/view.php?id=12965
# https://bugzilla.redhat.com/show_bug.cgi?id=822796
# Patch to use ninja-build instead of ninja (renamed in Fedora)
# https://bugzilla.redhat.com/show_bug.cgi?id=886184
Patch1:         cmake-ninja.patch
Patch2:         cmake-findruby.patch
# Patch to fix FindPostgreSQL
# https://bugzilla.redhat.com/show_bug.cgi?id=828467
# http://public.kitware.com/Bug/view.php?id=13378
Patch3:         cmake-FindPostgreSQL.patch
# Fix issue with finding consistent python versions
# http://public.kitware.com/Bug/view.php?id=13794
# https://bugzilla.redhat.com/show_bug.cgi?id=876118
Patch4:         cmake-FindPythonLibs.patch
# Add FindLua52.cmake
Patch5:         cmake-2.8.11-rc4-lua-5.2.patch
# Add -fno-strict-aliasing when compiling cm_sha2.c
# http://www.cmake.org/Bug/view.php?id=14314
Patch6:         cmake-strict_aliasing.patch
# Patch away .png extension in icon name in desktop file.
# http://www.cmake.org/Bug/view.php?id=14315
Patch7:         cmake-desktop_icon.patch
# Remove automatic Qt module dep adding
Patch8:         cmake-qtdeps.patch
# Fix FindFreetype for 2.5.1+
# http://public.kitware.com/Bug/view.php?id=14601
Patch9:         cmake-FindFreetype.patch
# Upstream patch to find Boost MPI library
# http://www.cmake.org/Bug/view.php?id=14739
# https://bugzilla.redhat.com/show_bug.cgi?id=756141
Patch10:        cmake-boostmpi.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  curl-devel >= 7.19.7-3m
BuildRequires:  desktop-file-utils
BuildRequires:  expat-devel
BuildRequires:  gcc-gfortran
BuildRequires:  libX11-devel
BuildRequires:  ncurses-devel 
BuildRequires:  libarchive-devel >= 3.0.4
BuildRequires:  openldap-devel >= 2.4.8
BuildRequires:  qt-devel >= 4.7.2
BuildRequires:  vtk-devel >= 5.8.0-3m
BuildRequires:  zlib-devel
%if !%{bootstrap}
BuildRequires:  xmlrpc-c-devel
%endif

%description
CMake is used to control the software compilation process using simple 
platform and compiler independent configuration files. CMake generates 
native makefiles and workspaces that can be used in the compiler 
environment of your choice. CMake is quite sophisticated: it is possible 
to support complex environments requiring system configuration, pre-processor 
generation, code generation, and template instantiation.

%package        gui
Summary:        Qt GUI for %{name}
Group:          Development/Tools
Requires:       %{name} = %{version}-%{release}

%description    gui
The %{name}-gui package contains the Qt based GUI for CMake.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1

find -name \*.h -o -name \*.cxx -print0 | xargs -0 chmod -x

%build
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
%{__mkdir} build
pushd build
../bootstrap --prefix=%{_prefix} \
             --datadir=/share/%{name} \
             --docdir=/share/doc/%{name}-%{version} \
             --mandir=/share/man \
             --qt-gui \
             --qt-qmake=%{_bindir}/qmake-qt4 \
             --parallel=`/usr/bin/getconf _NPROCESSORS_ONLN` \
%if !%{bootstrap}
             --system-libs
%else
             --no-system-libs
%endif

make VERBOSE=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
pushd build
make install DESTDIR=%{buildroot}
find %{buildroot}%{_datadir}/%{name}/Modules -type f | xargs chmod -x
popd
mkdir -p %{buildroot}%{_datadir}/emacs/site-lisp
cp -a Example %{buildroot}%{_datadir}/doc/%{name}-%{version}/
install -m 0644 Docs/cmake-mode.el %{buildroot}%{_datadir}/emacs/site-lisp/
# RPM macros
mkdir -p %{buildroot}%{_sysconfdir}/rpm
install -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/rpm/
install -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/rpm/
sed -i -e "s|@@CMAKE_VERSION@@|%{version}|" %{buildroot}%{_sysconfdir}/rpm/macros.cmake
touch -r %{SOURCE2} %{buildroot}%{_sysconfdir}/rpm/macros.cmake
mkdir -p %{buildroot}%{_libdir}/%{name}

# Desktop file
desktop-file-install \
  --delete-original \
  --vendor="" \
  --dir=%{buildroot}%{_datadir}/applications \
  %{buildroot}/%{_datadir}/applications/CMake.desktop

%check
unset DISPLAY
unset CVSROOT
pushd build
#ModuleNotices fails for some unknown reason, and we don't care
#CMake.HTML currently requires internet access
#CTestTestUpload requires internet access
bin/ctest -V -E ModuleNotices -E CMake.HTML -E CTestTestUpload %{?_smp_mflags} || :
popd

%clean
rm -rf %{buildroot}

%post gui
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun gui
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%files
%defattr(-,root,root)
%doc ChangeLog.txt Copyright.txt CMakeLogo.gif
%doc DartConfig.cmake
%doc Example
%config(noreplace) %{_sysconfdir}/rpm/macros.cmake
%config(noreplace) %{_sysconfdir}/rpm/macros.kde4
%{_datadir}/%{name}
%{_datadir}/aclocal/%{name}.m4
%{_datadir}/emacs/site-lisp/*
%{_bindir}/ccmake
%{_bindir}/%{name}
%{_bindir}/cpack
%{_bindir}/ctest
%{_libdir}/%{name}
%{_mandir}/man1/*

%files gui
%defattr(-,root,root,-)
%{_bindir}/%{name}-gui
%{_datadir}/applications/CMake.desktop
%{_datadir}/mime/packages/cmakecache.xml
%{_datadir}/pixmaps/CMakeSetup32.png

%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.12.2-1m)
- update to 2.8.12.2
- add patches from Fedora

* Fri Aug 30 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.8.11.2-1m)
- update to 2.8.11.2

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.11.1-1m)
- update to 2.8.11.1

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.11-1m)
- update to 2.8.11

* Thu Jan 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.10.2-1m)
- update to 2.8.10.2

* Thu Jan 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.10-1m)
- update to 2.8.10

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.9-1m)
- update to 2.8.9

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.8-1m)
- update to 2.8.8

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7-4m)
- rebuild against libarchive-3.0.4

* Sat Jan 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7-3m)
- fix failures of tests

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7-2m)
- revive subversion test (BTS #398)

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7-1m)
- update to 2.8.7

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.6-1m)
- update to 2.8.6
- skip subversion tests for a while

* Sun Jul 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.5-2m)
- update macros.kde4

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.5-1m)
- update to 2.8.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.4-2m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.4-1m)
- update to 2.8.4

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.3-3m)
- add BR libarchive-devel 

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.3-2m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-1m)
- update macros.cmake and macros.kde4
- own %%{_libdir}/cmake
- update to 2.8.2

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-2m)
- rebuild against qt-4.6.3-1m

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-2m)
- unset CVSROOT in %%check for convenience

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-3m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-2m)
- drop Patch1, already merged upstream
- License: Modified BSD

* Mon Oct 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2
-- this version includes the following bug fix
-- http://www.vtk.org/Bug/view.php?id=7447

* Fri Aug 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Fri Aug 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-3m)
- back to 2.6.0, due to build problem on x86_64

* Thu Aug  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Sun Jun  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-2m)
- adjust BuildRequires: section

* Sat Jun  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0
- add cmake-gui subpackage (sync with Fedora devel)

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-6m)
- rebuild against openssl-0.9.8h-1m

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-5m)
- update macros.kde4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.8-4m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.8-3m)
- rebuild against openldap-2.4.8

* Mon Feb 18 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.8-2m)
- add bootstrap option

* Mon Feb 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-1m)
- update to 2.4.8
- no %%NoSource

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-4m)
- add macros.kde4 (import from Fedora devel, kde-filesystem)

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-3m)
- sync with Fedora devel

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-2m)
- import 2 patches from FC-devel

* Sat Jul 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6

* Sat Dec 09 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Wed Oct 18 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.3-1m)
- first import to Momonga

* Sat Oct 07 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (2.4.3-0.0.1m)
- update to latest 2.4.3

* Fri Apr 21 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (2.4.0-0.0.1m)
- update to latest 2.4.0

* Sat Sep 18 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (2.0.3-0.0.1m)
- version 2.0.3

* Sun Aug 25 2002 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.4.3-0.0.1kl)
- build for Asumi

* Thu Aug 22 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to release 1.4 patch 3.

* Mon Aug 05 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Thu Jul 25 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Fri Jul 19 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Wed Jul 17 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Mon Jul 15 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources (version 1.5). 
- removed all the manual installation script in favour of the standard 
  installation procedure.

* Sat Jun 01 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Sun May 05 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Wed Feb 06 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Mon Jan 01 2002 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Sat Dec 22 2001 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Fri Dec 21 2001 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Sat Dec 15 2001 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- update to latest CVS sources.

* Fri Nov 30 2001 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- initial release
