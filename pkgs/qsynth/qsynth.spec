%global         momorel 1
%global         qtver 4.8.5
%global         kdever 4.12.0
%global         kdelibsrel 1m
%global         qtdir %{_libdir}/qt4
%global         kdedir /usr

Summary:	Qt based Fluidsynth GUI front end
Name:		qsynth
Version:	0.3.8
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/Multimedia
URL:		http://qsynth.sourceforge.net/qsynth-index.html
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:         %{name}-0.3.3-gcc44.patch
Patch1:         %{name}-0.3.6-linking.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	fluidsynth >= 1.1.5
Requires:	hicolor-icon-theme
BuildRequires:	qt-devel >= %{qtver}
BuildRequires:	fluidsynth-devel >= 1.1.5
BuildRequires:	desktop-file-utils
BuildRequires:	cmake

%description
A simple KDE web browser based on KParts.

%prep
%setup -q

%patch0 -p1 -b .gcc44~
%patch1 -p1 -b .linking

%build
%configure --with-qt=%{qtdir}
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README* TODO TRANSLATORS
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/locale/%{name}*
%{_datadir}/icons/*/*/*/%{name}.png

%changelog
* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-4m)
- rebuild against fluidsynth-1.1.5

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-3m)
- rebuild against fluidsynth-1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.6-1m)
- update to 0.3.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-6m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.5-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-3m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-2m)
- explicitly link libX11

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-1m)
- update to 0.3.5

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.4-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-1m)
- update to 0.3.4
- remove %%{?_smp_mflags}

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-2m)
- rebuild against rpm-4.6

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-1m)
- initial build for Momonga Linux 5
