%global momorel 8

Summary: Streaming Media Server
Name: icecast
Version: 2.3.2
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.icecast.org/
Group: System Environment/Daemons
Source0: http://downloads.xiph.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: krb5-devel, e2fsprogs-devel
BuildRequires: libidn-devel
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libxml2-devel, libxslt-devel, curl-devel >= 7.16.0
BuildRequires: libkate-devel >= 0.3.7-4m
BuildRequires: libogg-devel, libvorbis-devel, libtheora-devel
BuildRequires: speex-devel >= 1.2
BuildRequires: zlib-devel

%description
Icecast is an Internet based broadcasting system based on the Mpeg Layer III
streaming technology.  It was originally inspired by Nullsoft's Shoutcast
and also mp3serv by Scott Manley.  The icecast project was started for several
reasons: a) all broadcasting systems were pretty much closed source,
non-free software implementations, b) Shoutcast doesn't allow you to run your
own directory servers, or support them, and c) we thought it would be a
lot of fun.

%prep
%setup -q

%build
autoreconf -fiv
KATE_CFLAGS=`pkg-config --cflags %{_libdir}/pkgconfig/kate.pc`
export CFLAGS="$CFLAGS $KATE_CFLAGS"
%configure --bindir=%{_sbindir}
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir config-sample
cp conf/*.dist config-sample/
mkdir -p %{buildroot}%{_localstatedir}/log/icecast/

perl -p -i -e 's/nogroup/nobody/g' %{buildroot}%{_sysconfdir}/icecast.xml

rm -r %{buildroot}%{_docdir}/icecast
rm -r %{buildroot}%{_datadir}/icecast/doc

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog HACKING COPYING README TODO
%doc doc/*.html
%doc doc/*.jpg
%doc doc/*.css
%doc config-sample/
%{_sbindir}/icecast
%{_datadir}/icecast/
%config(noreplace) /etc/icecast.xml
%attr(-, nobody, nobody) %dir %{_localstatedir}/log/icecast/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-6m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-5m)
- use pkg-config for libkate

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-4m)
- enable build with libkate

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (2.3.1-10m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-9m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-8m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-7m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-6m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-5m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-4m)
- rebuild against libvorbis-1.2.0-1m

* Fri Feb 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-3m)
- BuildRequires: speex-devel

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3.1-2m)
- rebuild against curl-7.16.0
- - import icecast-curl.patch from Fedora Core

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.3.1-1m)
- update to 2.3.1

* Sat Jan  7 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.0-2m)
- rebuild against libtheora

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.0-1m)
- up to 2.2.0

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.12-8m)
- revised spec for enabling rpm 4.2.

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.12-7m)
- add errno patch

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.12-6m)
- change Source0 URI

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.12-5m)
- fix spec file
- use rpm macros

* Fri Feb 28 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.12-4m)
- enable stack protector again

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.12-3m)
  disable stack protector.

* Wed May 15 2002 Toru Hoshina <t@kondara.org>
- (1.3.12-2k)
- version up. security fix. See jitterbug/#1039.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.3.11-4k)
- nigittenu

* Fri Nov 9 2001 TABUCHI Takaaki <tab@kondara.org>
- (1.3.11-2k)
- update to 1.3.11

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<

* Thu Aug 31 2000 AYUHANA Tomonori <l@kondara.org>
- (1.3.8.beta2-1k)
- update to 1.3.8.beta2
- temporary comment outed --with-crypt for ices

* Mon Jul 24 2000 AYUHANA Tomonori <l@kondara.org>
- (1.3.7-1k)
- ported from MandrakeCooker
- Be a NoSrc :-P
- (1.3.7-2k)
- add --with-crypt at configure
- change --enable-libwrap to --with-libwrap

* Fri Jul 07 2000 Geoffrey Lee <snailtalk@linux-mandrake.com> 1.3.7-1mdk
- mandrake-ize package

* Tue Mar 21 2000 Jeremy Katz <katzj@icecast.org>
- clean up the spec file a little

* Thu Dec 9 1999 Jeremy Katz <katzj@icecast.org>
- First official rpm build, using 2.0.0-beta
