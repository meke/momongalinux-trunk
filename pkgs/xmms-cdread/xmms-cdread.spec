%global momorel 20
%global version_suffix a
%global plugindir %{_libdir}/xmms/Input

Name: xmms-cdread
Summary: Input plugin that reads audio data from CDs
Version: 0.14
Release: 0.1.%{momorel}m%{?dist}
Epoch: 1
License: GPLv2+
Group: Applications/Multimedia
Source: ftp://mud.stack.nl/pub/OuterSpace/willem/%{name}-%{version}%{version_suffix}.tar.gz
Patch0: %{name}-whyfini.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xmms >= 1.2.10-13m
BuildRequires: xmms-devel >= 1.2.10-13m
BuildRequires: coreutils
BuildRequires: libtool

%description
This is an alternative to the xmms audio CD plugin with advanced features.

%prep
%setup -q -n %{name}-%{version}%{version_suffix}

%patch0 -p1

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make libdir=%{buildroot}%{plugindir} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS INSTALL TODO ChangeLog NEWS README cddb.howto
%{plugindir}/libcdread.*

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.14-0.1.20m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.14-0.1.19m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.14-0.1.18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.14-0.1.17m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.14-0.1.16m)
- add epoch to %%changelog

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.14-0.1.15m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.14-0.1.14m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.14-0.1.13m)
- add Epoch: 1 to enable upgrading from STABLE_5
- DO NOT REMOVE Epoch FOREVER

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14-0.1.12m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-0.1.11m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-0.1.10m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-0.1.9m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14-0.1.8m)
- delete libtool library

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.14-0.1.7m)
- change installdir (/usr/X11R6 -> /usr)

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.14-0.1.6m)
- enable x86_64.

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.14-0.1.5m)
- rebuild against xmms-1.2.8
- revise versioning.

* Wed Apr 10 2002 Toru Hoshina <t@kondara.org>
- (0.14a-4k)
- Why _fini?

* Sun Dec 02 2001 Toshiro HIKITA <toshi@sodan.org>
- (0.14a-2k)
- Imported from 0.11c-1mdk of Mandrake
- updated to 0.14a
- Kondarize

* Tue Sep 11 2001 Guillaume Cottenceau <gc@mandrakesoft.com> 0.11c-1mdk
- rebuild

* Tue May 22 2001 Guillaume Cottenceau <gc@mandrakesoft.com> 0.11c-1mdk
- bump to 0.11c to comply with xmms-1.2.5 interface

* Fri May 18 2001 Chmouel Boudjnah <chmouel@mandrakesoft.com> 0.9a-3mdk
- Rebuild.

* Wed Mar 14 2001 Guillaume Cottenceau <gc@mandrakesoft.com> 0.9a-2mdk
- build for release

* Tue Nov 07 2000 Lenny Cartier <lenny@mandrakesoft.com> 0.9a-1mdk
- updated to 0.9a
- patch cdread.c to avoid function conflict

* Tue Oct  3 2000 Goetz Waschk <waschk@linux-mandrake.com> 0.8a-1mdk
- initial Mandrake build
