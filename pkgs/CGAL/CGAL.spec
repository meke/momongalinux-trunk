%global momorel 1
# Min dependencies
%global boost_version 1.55.0
%global qt_version 4.8
%global cmake_version 2.6.2

# Various variables that defines the release
%global soname 10
%global soversion 10.0.2
%global alphatag %{nil}
%global alphaname %{nil}
#global alphaname -%{alphatag}
%global gforgedlnumber 32995

Name:           CGAL
Version:        4.3
Release:        %{momorel}m%{?dist}
Summary:        Computational Geometry Algorithms Library

Group:          System Environment/Libraries
License:        "LGPLv3+ and GPLv3+ and Boost"
URL:            http://www.cgal.org/

Source0:        https://gforge.inria.fr/frs/download.php/%{gforgedlnumber}/%{name}-%{version}%{alphaname}.tar.xz
NoSource:       0
Source10:       CGAL-README.Fedora

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Required devel packages.
BuildRequires: cmake >= %{cmake_version}
BuildRequires: gmp-devel
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: libQGLViewer-devel
BuildRequires: qt3-devel
BuildRequires: qt-devel >= %{qt_version}
BuildRequires: zlib-devel
BuildRequires: blas-devel lapack-devel
BuildRequires: mpfr-devel

%description
Libraries for CGAL applications.
CGAL is a collaborative effort of several sites in Europe and
Israel. The goal is to make the most important of the solutions and
methods developed in computational geometry available to users in
industry and academia in a C++ library. The goal is to provide easy
access to useful, reliable geometric algorithms.


%package devel
Group:          Development/Libraries
Summary:        Development files and tools for CGAL applications
Requires:       cmake
Requires:       %{name} = %{version}-%{release}
Requires:       boost-devel%{?_isa} >= %{boost_version}
Requires:       qt-devel%{?_isa} >= %{qt_version}
Requires:       blas-devel%{?_isa} lapack-devel%{?_isa} qt3-devel%{?_isa} zlib-devel%{?_isa} gmp-devel%{?_isa}
Requires:       mpfr-devel%{?_isa}
%description devel
The %{name}-devel package provides the headers files and tools you may need to 
develop applications using CGAL.


%package demos-source
Group:          Documentation
Summary:        Examples and demos of CGAL algorithms
Requires:       %{name}-devel%{?_isa} = %{version}-%{release}
%description demos-source
The %{name}-demos-source package provides the sources of examples and demos of
CGAL algorithms.


%prep
%setup -q -n %{name}-%{version}%{alphaname}

# Fix some file permissions
chmod a-x include/CGAL/export/ImageIO.h
chmod a-x include/CGAL/export/CORE.h
chmod a-x include/CGAL/internal/Static_filters/Equal_3.h
chmod a-x include/CGAL/export/Qt4.h
chmod a-x include/CGAL/export/CGAL.h

# Install README.Fedora here, to include it in %%doc
install -p -m 644 %{SOURCE10} ./README.Fedora

%build

mkdir build
pushd build
%cmake -DCGAL_INSTALL_LIB_DIR=%{_lib} ${CHANGE_SOVERSION} ..
make VERBOSE=1 %{?_smp_mflags}
popd


%install
rm -rf %{buildroot}

pushd build

make install DESTDIR=%{buildroot}

popd

# Install demos and examples
mkdir -p %{buildroot}%{_datadir}/CGAL
touch -r demo %{buildroot}%{_datadir}/CGAL/
cp -a demo %{buildroot}%{_datadir}/CGAL/demo
cp -a examples %{buildroot}%{_datadir}/CGAL/examples

%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE LICENSE.FREE_USE LICENSE.LGPL LICENSE.GPL CHANGES README.Fedora
%{_libdir}/libCGAL*.so.%{soname}
%{_libdir}/libCGAL*.so.%{soversion}


%files devel
%defattr(-,root,root,-)
%{_includedir}/CGAL
%{_libdir}/libCGAL*.so
%{_libdir}/CGAL
%dir %{_datadir}/CGAL
%{_bindir}/*
%exclude %{_bindir}/cgal_make_macosx_app
%{_mandir}/man1/cgal_create_cmake_script.1.*


%files demos-source
%defattr(-,root,root,-)
%dir %{_datadir}/CGAL
%{_datadir}/CGAL/demo
%{_datadir}/CGAL/examples
%exclude %{_datadir}/CGAL/*/*/skip_vcproj_auto_generation

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3-1m)
- update to 4.3
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-1m)
- update to 4.1
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.2-2m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.2-1m)
- new upstream release

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-1m)
- new upstream release
-- now soname is 9

* Sun Jan  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9-1m)
- new upstream release
-- now soname is 8

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8-4m)
- rebuild for boost-1.48.0

* Wed Dec 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8-3m)
- rebuild for lapack-3.4.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (3.8-2m)
- rebuild for boost

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8-1m)
- update 

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7-5m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7-4m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7-3m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7-1m)
- update 3.7

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-4m)
- rebuild against boost-1.44.0

* Tue Sep 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.1-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6-2m)
- rebuild against qt-4.6.3-1m

* Sat Jun 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6-1m)
- update to 3.6

* Sat Dec 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-1m)
- update to 3.5.1 (bug fix release)

* Sun Nov 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5-4m)
- rebuild against boost-1.41.0

* Tue Nov 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-3m)
- import a fix from rawhide. see redhat's bugzilla #532431

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-1m)
- update to 3.5 release version

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-0.2.2m)
- rebuild for boost-1.40.0

* Sat Oct 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-0.2.1m)
- fix Release

* Sat Aug 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-0.1m)
- update to CGAL-3.5-beta1

* Fri Jun 19 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-2m)
- fix typo 
- enable libQGLViewer support

* Sun Apr  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-1m)
- update 4.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-2m)
- rebuild against rpm-4.6

* Wed Oct  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-1m)
- inital import from fedora (CGAL-3_3_1-11_fc9)
