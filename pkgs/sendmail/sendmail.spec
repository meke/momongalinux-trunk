%global momorel 1

# package options
%global with_tls	yes
%global with_sasl2	yes
%global with_milter	yes
%global with_ldap	yes
%global enable_pie	yes

%global sendmailcf %{_datadir}/sendmail-cf
%global stdir %{_localstatedir}/log/mail
%global smshell /sbin/nologin
%global spooldir %{_localstatedir}/spool
%global maildir %{_sysconfdir}/mail

%global sysv2systemdnvr 8.14.5-3

# hardened build if not overrided
%{!?_hardened_build:%global _hardened_build 1}

%if %{?_hardened_build:%{_hardened_build}}%{!?_hardened_build:0}
%global relro -Xlinker -z -Xlinker relro -Xlinker -z -Xlinker now
%endif

Summary: A widely used Mail Transport Agent (MTA)
Name: sendmail
Version: 8.14.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Daemons
URL: http://www.sendmail.org/
Source0: ftp://ftp.sendmail.org/pub/sendmail/sendmail.%{version}.tar.gz
NoSource: 0
# Systemd Service file
Source1: sendmail.service
# NetworkManager dispatch script
Source2: sendmail.nm-dispatcher
# script to generate db and cf files
Source3: sendmail.etc-mail-make
# default sysconfig file
Source4: sendmail.sysconfig
# default /etc/mail/Makefile
Source5: sendmail.etc-mail-Makefile
# default sendmail.mc
Source6: sendmail-redhat.mc
# Systemd Service file
Source7: sm-client.service
# pam config
Source8: sendmail.pam
# SysV initscript
Source9: sendmail.init
# sasl2 config
Source11: Sendmail-sasl2.conf
# default /etc/mail/access
Source12: sendmail-etc-mail-access
# default /etc/mail/domaintable
Source13: sendmail-etc-mail-domaintable
# default /etc/mail/local-host-names
Source14: sendmail-etc-mail-local-host-names
# default /etc/mail/mailertable
Source15: sendmail-etc-mail-mailertable
# default /etc/mail/trusted-users
Source16: sendmail-etc-mail-trusted-users
# default /etc/mail/virtusertable
Source17: sendmail-etc-mail-virtusertable
# fix man path and makemap man page
Patch3: sendmail-8.14.4-makemapman.patch
# fix smrsh paths
Patch4: sendmail-8.14.3-smrsh_paths.patch
# fix sm-client.pid path
Patch7: sendmail-8.13.7-pid.patch
# do not reject all numeric login names if hesiod support is compiled in, #80060
Patch9: sendmail-8.12.7-hesiod.patch
# fix sendmail man page
Patch10: sendmail-8.12.7-manpage.patch
# compile with -fpie
Patch11: sendmail-8.14.4-dynamic.patch
# fix cyrus path
Patch12: sendmail-8.13.0-cyrus.patch
# fix aliases.db path
Patch13: sendmail-8.14.4-aliases_dir.patch
# fix vacation Makefile
Patch14: sendmail-8.13.7-vacation.patch
# remove version information from sendmail helpfile
Patch15: sendmail-8.14.1-noversion.patch
# do not accept localhost.localdomain as valid address from SMTP
Patch16: sendmail-8.13.1-localdomain.patch
# build libmilter as DSO
Patch17: sendmail-8.14.3-sharedmilter.patch
# skip colon separator when parsing service name in ServiceSwitchFile
Patch18: sendmail-8.14.4-switchfile.patch
# fix milter file descriptors leaks, #485426
Patch20: sendmail-8.14.3-milterfdleaks.patch
# handle IPv6:::1 in block_bad_helo.m4 like 127.0.0.1, #549217
Patch21: sendmail-8.14.3-ipv6-bad-helo.patch
# fix compilation with libdb5
Patch22: sendmail-8.14.4-libdb5.patch
# silence warning about missing sasl2 config in /usr/lib*, now in /etc/sasl2
Patch23: sendmail-8.14.4-sasl2-in-etc.patch
# add QoS support, patch from Philip Prindeville <philipp@fedoraproject.org>
# upstream reserved option ID 0xe7 for testing of this new feature, #576643
Patch25: sendmail-8.14.7-qos.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: tcp_wrappers-devel
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: hesiod-devel
BuildRequires: groff
BuildRequires: ghostscript
BuildRequires: m4
BuildRequires: systemd-units
Provides: MTA smtpdaemon server(smtp)
Requires(post): systemd-units systemd-sysv coreutils %{_sbindir}/alternatives
Requires(preun): systemd-units %{_sbindir}/alternatives
Requires(postun): systemd-units coreutils %{_sbindir}/alternatives
Requires(pre): shadow-utils
Requires: initscripts
Requires: procmail
Requires: bash >= 2.0
Requires: setup >= 2.8.15-1m
BuildRequires: setup >= 2.8.15-1m

%if "%{with_tls}" == "yes"
BuildRequires: openssl-devel >= 1.0.0
%endif
%if "%{with_sasl2}" == "yes"
BuildRequires: cyrus-sasl-devel openssl-devel >= 1.0.0
Requires: cyrus-sasl
%endif
%if "%{with_ldap}" == "yes"
BuildRequires: openldap-devel openssl-devel >= 1.0.0
%endif


%description
The Sendmail program is a very widely used Mail Transport Agent (MTA).
MTAs send mail from one machine to another. Sendmail is not a client
program, which you use to read your email. Sendmail is a
behind-the-scenes program which actually moves your email over
networks or the Internet to where you want it to go.

If you ever need to reconfigure Sendmail, you will also need to have
the sendmail-cf package installed. If you need documentation on
Sendmail, you can install the sendmail-doc package.

%package sysvinit
Summary: SysV initscript for sendmail
Group: System Environment/Daemons
BuildArch: noarch
Requires: %{name} = %{version}-%{release}
Requires(preun): chkconfig
Requires(post): chkconfig

%description sysvinit
This package contains the SysV initscript.

%package doc
Summary: Documentation about the Sendmail Mail Transport Agent program
Group: Documentation
BuildArch: noarch
Requires: sendmail = %{version}-%{release}

%description doc
This package contains the Sendmail Installation and Operation Guide (PDF),
text files containing configuration documentation, plus a number of
contributed scripts and tools for use with Sendmail.

%package devel
Summary: Extra development include files and development files
Group: Development/Libraries
Requires: sendmail = %{version}-%{release}
Requires: sendmail-milter = %{version}-%{release}

%description devel
Include files and devel libraries for e.g. the milter add-ons as part
of sendmail.

%package cf
Summary: The files needed to reconfigure Sendmail
Group: System Environment/Daemons
Requires: sendmail = %{version}-%{release}
BuildArch: noarch
Requires: m4

%description cf
This package includes the configuration files you need to generate the
sendmail.cf file distributed with the sendmail package. You will need
the sendmail-cf package if you ever need to reconfigure and rebuild
your sendmail.cf file.

%package milter
Summary: The sendmail milter library
Group: System Environment/Libraries

%description milter
The sendmail Mail Filter API (Milter) is designed to allow third-party
programs access to mail messages as they are being processed in order to
filter meta-information and content.

This package includes the milter shared library.

%prep
%setup -q

%patch3 -p1 -b .makemapman
%patch4 -p1 -b .smrsh_paths
%patch7 -p1 -b .pid
%patch9 -p1 -b .hesiod
%patch10 -p1 -b .manpage
%patch11 -p1 -b .dynamic
%patch12 -p1 -b .cyrus
%patch13 -p1 -b .aliases_dir
%patch14 -p1 -b .vacation
%patch15 -p1 -b .noversion
%patch16 -p1 -b .localdomain

cp devtools/M4/UNIX/{,shared}library.m4
%patch17 -p1 -b .sharedmilter

%patch18 -p1 -b .switchfile
%patch20 -p1 -b .milterfdleaks
%patch21 -p1 -b .ipv6-bad-helo
%patch22 -p1 -b .libdb5
%patch23 -p1 -b .sasl2-in-etc
%patch25 -p1 -b .qos

for f in RELEASE_NOTES contrib/etrn.0; do
	iconv -f iso8859-1 -t utf8 -o ${f}{_,} &&
		touch -r ${f}{,_} && mv -f ${f}{_,}
done

sed -i 's|/usr/local/bin/perl|%{_bindir}/perl|' contrib/*.pl

%build
# generate redhat config file
cat > redhat.config.m4 << EOF
define(\`confMAPDEF', \`-DNEWDB -DNIS -DHESIOD -DMAP_REGEX -DSOCKETMAP -DNAMED_BIND=1')
define(\`confOPTIMIZE', \`\`\`\`${RPM_OPT_FLAGS}'''')
define(\`confENVDEF', \`-I%{_includedir}/libdb -I/usr/kerberos/include -Wall -DXDEBUG=0 -DTCPWRAPPERS -DNETINET6 -DHES_GETMAILHOST -DUSE_VENDOR_CF_PATH=1 -D_FFR_TLS_1 -D_FFR_LINUX_MHNL -D_FFR_QOS')
define(\`confLIBDIRS', \`-L/usr/kerberos/%{_lib}')
define(\`confLIBS', \`-lnsl -lwrap -lhesiod -lcrypt -ldb -lresolv %{?relro:%{relro}}')
define(\`confMANOWN', \`root')
define(\`confMANGRP', \`root')
define(\`confMANMODE', \`644')
define(\`confMAN1SRC', \`1')
define(\`confMAN5SRC', \`5')
define(\`confMAN8SRC', \`8')
define(\`confSTDIR', \`%{stdir}')
define(\`STATUS_FILE', \`%{stdir}/statistics')
define(\`confLIBSEARCH', \`db resolv 44bsd')
EOF
#'

cat >> redhat.config.m4 << EOF
%ifarch ppc ppc64 s390x
APPENDDEF(\`confOPTIMIZE', \`-DSM_CONF_SHM=0')
%else
APPENDDEF(\`confOPTIMIZE', \`')
%endif
EOF

%if "%{enable_pie}" == "yes"
%ifarch s390 s390x sparc sparcv9 sparc64
%global _fpie -fPIE
%else
%global _fpie -fpie
%endif
cat >> redhat.config.m4 << EOF
APPENDDEF(\`confOPTIMIZE', \`%{_fpie}')
APPENDDEF(\`confLIBS', \`-pie')
EOF
%endif

%if "%{with_tls}" == "yes"
cat >> redhat.config.m4 << EOF
APPENDDEF(\`conf_sendmail_ENVDEF', \`-DSTARTTLS')dnl
APPENDDEF(\`conf_sendmail_LIBS', \`-lssl -lcrypto')dnl
EOF
%endif

%if "%{with_sasl2}" == "yes"
cat >> redhat.config.m4 << EOF
APPENDDEF(\`confENVDEF', \`-DSASL=2')dnl
APPENDDEF(\`confLIBS', \`-lsasl2 -lcrypto')dnl
EOF
%endif

%if "%{with_milter}" == "yes"
cat >> redhat.config.m4 << EOF
APPENDDEF(\`conf_sendmail_ENVDEF', \`-DMILTER')dnl
EOF
%endif

%if "%{with_ldap}" == "yes"
cat >> redhat.config.m4 << EOF
APPENDDEF(\`confMAPDEF', \`-DLDAPMAP -DLDAP_DEPRECATED')dnl
APPENDDEF(\`confENVDEF', \`-DSM_CONF_LDAP_MEMFREE=1')dnl
APPENDDEF(\`confLIBS', \`-lldap -llber -lssl -lcrypto')dnl
EOF
%endif

DIRS="libsmutil sendmail mailstats rmail praliases smrsh makemap"

%if "%{with_milter}" == "yes"
DIRS="libmilter $DIRS"
%endif

for i in $DIRS; do
	pushd $i
	sh Build -f ../redhat.config.m4
	popd
done

make -C doc/op op.pdf

%install
rm -rf %{buildroot}

# create directories
for d in %{_bindir} %{_sbindir} %{_includedir}/libmilter \
	%{_libdir} %{_mandir}/man{1,5,8} %{maildir} %{stdir} %{spooldir} \
	%{_docdir}/sendmail-%{version} %{sendmailcf} %{_sysconfdir}/smrsh\
	%{spooldir}/clientmqueue %{_sysconfdir}/sysconfig %{_initscriptdir} \
	%{_sysconfdir}/pam.d %{_docdir}/sendmail-%{version}/contrib \
	%{_sysconfdir}/NetworkManager/dispatcher.d
do
	install -m 755 -d %{buildroot}$d
done
install -m 700 -d %{buildroot}%{spooldir}/mqueue

# create /usr/lib for 64 bit architectures
%if "%{_libdir}" != "/usr/lib"
install -m 755 -d %{buildroot}/usr/lib
%endif

nameuser=`id -nu`
namegroup=`id -ng`

Make() {
	make $@ \
		DESTDIR=%{buildroot} \
		LIBDIR=%{_libdir} \
		MANROOT=%{_mandir}/man \
		LIBMODE=0755 INCMODE=0644 \
		SBINOWN=${nameuser} SBINGRP=${namegroup} \
		UBINOWN=${nameuser} UBINGRP=${namegroup} \
		MANOWN=${nameuser} MANGRP=${namegroup} \
		INCOWN=${nameuser} INCGRP=${namegroup} \
		LIBOWN=${nameuser} LIBGRP=${namegroup} \
		GBINOWN=${nameuser} GBINGRP=${namegroup} \
		CFOWN=${nameuser} CFGRP=${namegroup} \
		CFMODE=0644 MSPQOWN=${nameuser}
}

OBJDIR=obj.$(uname -s).$(uname -r).$(uname -m)

Make install -C $OBJDIR/libmilter
Make install -C $OBJDIR/sendmail
Make install -C $OBJDIR/mailstats
Make force-install -C $OBJDIR/rmail
Make install -C $OBJDIR/praliases
Make install -C $OBJDIR/smrsh
Make install -C $OBJDIR/makemap

# replace absolute with relative symlinks
ln -sf ../sbin/makemap %{buildroot}%{_bindir}/makemap
for f in hoststat mailq newaliases purgestat ; do
	ln -sf ../sbin/sendmail.sendmail %{buildroot}%{_bindir}/${f}
done

# use /usr/lib, even for 64 bit architectures
ln -sf ../sbin/sendmail.sendmail %{buildroot}/usr/lib/sendmail.sendmail

# install docs for sendmail
install -p -m 644 FAQ %{buildroot}%{_docdir}/sendmail-%{version}
install -p -m 644 KNOWNBUGS %{buildroot}%{_docdir}/sendmail-%{version}
install -p -m 644 LICENSE %{buildroot}%{_docdir}/sendmail-%{version}
install -p -m 644 README %{buildroot}%{_docdir}/sendmail-%{version}
install -p -m 644 RELEASE_NOTES %{buildroot}%{_docdir}/sendmail-%{version}
gzip -9 %{buildroot}%{_docdir}/sendmail-%{version}/RELEASE_NOTES

# install docs for sendmail-doc
install -m 644 doc/op/op.pdf %{buildroot}%{_docdir}/sendmail-%{version}
install -p -m 644 sendmail/README %{buildroot}%{_docdir}/sendmail-%{version}/README.sendmail
install -p -m 644 sendmail/SECURITY %{buildroot}%{_docdir}/sendmail-%{version}
install -p -m 644 smrsh/README %{buildroot}%{_docdir}/sendmail-%{version}/README.smrsh
install -p -m 644 libmilter/README %{buildroot}%{_docdir}/sendmail-%{version}/README.libmilter
install -p -m 644 cf/README %{buildroot}%{_docdir}/sendmail-%{version}/README.cf
install -p -m 644 contrib/* %{buildroot}%{_docdir}/sendmail-%{version}/contrib

# install the cf files for the sendmail-cf package.
cp -ar cf/* %{buildroot}%{sendmailcf}
# remove patch backup files
rm -rf %{buildroot}%{sendmailcf}/cf/Build.*
rm -rf %{buildroot}%{sendmailcf}/*/*.mc.*
rm -rf %{buildroot}%{sendmailcf}/*/*.m4.*
# remove cf/README file because it is useless for end users
rm -f %{buildroot}%{sendmailcf}/cf/README

# install sendmail.mc with proper paths
install -m 644 %{SOURCE6} %{buildroot}%{maildir}/sendmail.mc
sed -i -e 's|@@PATH@@|%{sendmailcf}|' %{buildroot}%{maildir}/sendmail.mc
touch -r %{SOURCE6} %{buildroot}%{maildir}/sendmail.mc

# create sendmail.cf
cp %{buildroot}%{maildir}/sendmail.mc cf/cf/redhat.mc
sed -i -e 's|%{sendmailcf}|\.\.|' cf/cf/redhat.mc
%if "%{stdir}" != "%{maildir}"
sed -i -e 's:%{maildir}/statistics:%{stdir}/statistics:' cf/cf/redhat.mc
%endif
(cd cf/cf && m4 redhat.mc > redhat.cf)
install -m 644 cf/cf/redhat.cf %{buildroot}%{maildir}/sendmail.cf
install -p -m 644 cf/cf/submit.mc %{buildroot}%{maildir}/submit.mc

# remove our build info as it causes multiarch conflicts
sed -i '/##### built by.*on/,+3d' %{buildroot}%{maildir}/{submit,sendmail}.cf \
	%{buildroot}%{sendmailcf}/cf/submit.cf

install -p -m 644 %{SOURCE12} %{buildroot}%{maildir}/access
install -p -m 644 %{SOURCE13} %{buildroot}%{maildir}/domaintable
install -p -m 644 %{SOURCE14} %{buildroot}%{maildir}/local-host-names
install -p -m 644 %{SOURCE15} %{buildroot}%{maildir}/mailertable
install -p -m 644 %{SOURCE16} %{buildroot}%{maildir}/trusted-users
install -p -m 644 %{SOURCE17} %{buildroot}%{maildir}/virtusertable

# create db ghosts
for map in virtusertable access domaintable mailertable ; do
	touch %{buildroot}%{maildir}/${map}.db
	chmod 0644 %{buildroot}%{maildir}/${map}.db
done

touch %{buildroot}%{maildir}/aliasesdb-stamp

install -p -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/sendmail
install -p -m 755 %{SOURCE9} %{buildroot}%{_initscriptdir}/sendmail
install -p -m 755 %{SOURCE2} %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d/10-sendmail
install -p -m 755 %{SOURCE3} %{buildroot}%{maildir}/make
install -p -m 644 %{SOURCE5} %{buildroot}%{maildir}/Makefile

chmod 644 %{buildroot}%{maildir}/helpfile

# Systemd
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{SOURCE1} %{buildroot}%{_unitdir}
install -m644 %{SOURCE7} %{buildroot}%{_unitdir}

# fix permissions to allow debuginfo extraction and stripping
chmod 755 %{buildroot}%{_sbindir}/{mailstats,makemap,praliases,sendmail,smrsh}
chmod 755 %{buildroot}%{_bindir}/rmail

%if "%{with_sasl2}" == "yes"
install -m 755 -d %{buildroot}%{_sysconfdir}/sasl2
install -m 644 %{SOURCE11} %{buildroot}%{_sysconfdir}/sasl2/Sendmail.conf
%endif
install -m 644 %{SOURCE8} %{buildroot}%{_sysconfdir}/pam.d/smtp.sendmail

# fix path for statistics file in man pages
%if "%{stdir}" != "%{maildir}"
sed -i -e 's:%{maildir}/statistics:%{stdir}/statistics:' %{buildroot}%{_mandir}/man*/*
%endif

# rename files for alternative usage
mv %{buildroot}%{_sbindir}/sendmail %{buildroot}%{_sbindir}/sendmail.sendmail
touch %{buildroot}%{_sbindir}/sendmail
for i in mailq newaliases rmail; do
	mv %{buildroot}%{_bindir}/$i %{buildroot}%{_bindir}/$i.sendmail
	touch %{buildroot}%{_bindir}/$i
done
mv %{buildroot}%{_mandir}/man1/mailq.1 %{buildroot}%{_mandir}/man1/mailq.sendmail.1
touch %{buildroot}%{_mandir}/man1/mailq.1
mv %{buildroot}%{_mandir}/man1/newaliases.1 %{buildroot}%{_mandir}/man1/newaliases.sendmail.1
touch %{buildroot}%{_mandir}/man1/newaliases.1
mv %{buildroot}%{_mandir}/man5/aliases.5 %{buildroot}%{_mandir}/man5/aliases.sendmail.5
touch %{buildroot}%{_mandir}/man5/aliases.5
mv %{buildroot}%{_mandir}/man8/sendmail.8 %{buildroot}%{_mandir}/man8/sendmail.sendmail.8
touch %{buildroot}%{_mandir}/man8/sendmail.8
mv %{buildroot}%{_mandir}/man8/rmail.8 %{buildroot}%{_mandir}/man8/rmail.sendmail.8
touch %{buildroot}%{_mandir}/man8/rmail.8
touch %{buildroot}/usr/lib/sendmail
touch %{buildroot}%{_sysconfdir}/pam.d/smtp

# create stub man pages
for m in man8/hoststat.8 man8/purgestat.8; do
	[ -f %{buildroot}%{_mandir}/$m ] || 
		echo ".so man8/sendmail.8" > %{buildroot}%{_mandir}/$m
done

%clean
rm -rf %{buildroot}

%pre
getent group mailnull >/dev/null || \
  %{_sbindir}/groupadd -g 47 -r mailnull >/dev/null 2>&1
getent passwd mailnull >/dev/null || \
  %{_sbindir}/useradd -u 47 -g mailnull -d %{spooldir}/mqueue -r \
  -s %{smshell} mailnull >/dev/null 2>&1
getent group smmsp >/dev/null || \
  %{_sbindir}/groupadd -g 51 -r smmsp >/dev/null 2>&1
getent passwd smmsp >/dev/null || \
  %{_sbindir}/useradd -u 51 -g smmsp -d %{spooldir}/mqueue -r \
  -s %{smshell} smmsp >/dev/null 2>&1
exit 0

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    /bin/systemctl try-restart sendmail.service >/dev/null 2>&1 || :
    /bin/systemctl try-restart sm-client.service >/dev/null 2>&1 || :
	mta=`readlink %{_sysconfdir}/alternatives/mta`
	if [ "$mta" == "%{_sbindir}/sendmail.sendmail" ]; then
		%{_sbindir}/alternatives --set mta %{_sbindir}/sendmail.sendmail
	fi
fi
exit 0

%post
if [ $1 -eq 1 ] ; then
# Initial installation
	/bin/systemctl enable sendmail.service >/dev/null 2>&1 || :
	/bin/systemctl enable sm-client.service >/dev/null 2>&1 || :
	/bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

# Set up the alternatives files for MTAs.
%{_sbindir}/alternatives --install %{_sbindir}/sendmail mta %{_sbindir}/sendmail.sendmail 90 \
	--slave %{_bindir}/mailq mta-mailq %{_bindir}/mailq.sendmail \
	--slave %{_bindir}/newaliases mta-newaliases %{_bindir}/newaliases.sendmail \
	--slave %{_bindir}/rmail mta-rmail %{_bindir}/rmail.sendmail \
	--slave /usr/lib/sendmail mta-sendmail /usr/lib/sendmail.sendmail \
	--slave %{_sysconfdir}/pam.d/smtp mta-pam %{_sysconfdir}/pam.d/smtp.sendmail \
	--slave %{_mandir}/man8/sendmail.8.bz2 mta-sendmailman %{_mandir}/man8/sendmail.sendmail.8.bz2 \
	--slave %{_mandir}/man1/mailq.1.bz2 mta-mailqman %{_mandir}/man1/mailq.sendmail.1.bz2 \
	--slave %{_mandir}/man1/newaliases.1.bz2 mta-newaliasesman %{_mandir}/man1/newaliases.sendmail.1.bz2 \
	--slave %{_mandir}/man5/aliases.5.bz2 mta-aliasesman %{_mandir}/man5/aliases.sendmail.5.bz2 \
	--slave %{_mandir}/man8/rmail.8.bz2 mta-rmailman %{_mandir}/man8/rmail.sendmail.8.bz2 \
	--initscript sendmail > /dev/null 2>&1

# Rebuild maps.
{
	chown root %{_sysconfdir}/aliases.db %{maildir}/access.db \
		%{maildir}/mailertable.db %{maildir}/domaintable.db \
		%{maildir}/virtusertable.db
	SM_FORCE_DBREBUILD=1 %{maildir}/make
	SM_FORCE_DBREBUILD=1 %{maildir}/make aliases
} > /dev/null 2>&1

# Move existing SASL2 config to new location.
%if "%{with_sasl2}" == "yes"
[ -f %{_libdir}/sasl2/Sendmail.conf ] && touch -r %{_sysconfdir}/sasl2/Sendmail.conf \
  %{_libdir}/sasl2/Sendmail.conf ] && mv -f %{_libdir}/sasl2/Sendmail.conf \
  %{_sysconfdir}/sasl2 2>/dev/null || :
%endif
exit 0

%preun
if [ $1 = 0 ]; then
	/bin/systemctl --no-reload disable sendmail.service > /dev/null 2>&1 || :
	/bin/systemctl stop sendmail.service > /dev/null 2>&1 || :
	/bin/systemctl --no-reload disable sm-client.service > /dev/null 2>&1 || :
	/bin/systemctl stop sm-client.service > /dev/null 2>&1 || :
	%{_sbindir}/alternatives --remove mta %{_sbindir}/sendmail.sendmail
fi
exit 0

%post milter -p /sbin/ldconfig

%postun milter -p /sbin/ldconfig

%post sysvinit
/sbin/chkconfig --add sendmail >/dev/null 2>&1 ||:

%preun sysvinit
if [ "$1" = 0 ]; then
	%{_initscriptdir}/sendmail stop >/dev/null 2>&1 ||:
	/sbin/chkconfig --del sendmail >/dev/null 2>&1 ||:
fi

%postun sysvinit
[ "$1" -ge "1" ] && %{_initscriptdir}/sendmail condrestart >/dev/null 2>&1 ||:

%triggerun -- sendmail < %{sysv2systemdnvr}
%{_bindir}/systemd-sysv-convert --save sendmail >/dev/null 2>&1 ||:
/bin/systemctl enable sendmail.service >/dev/null 2>&1
/bin/systemctl enable sm-client.service >/dev/null 2>&1
/sbin/chkconfig --del sendmail >/dev/null 2>&1 || :
/bin/systemctl try-restart sendmail.service >/dev/null 2>&1 || :
/bin/systemctl try-restart sm-client.service >/dev/null 2>&1 || :
# workaround for systemd rhbz#738022
/bin/systemctl is-active sendmail.service >/dev/null 2>&1 && \
	! /bin/systemctl is-active sm-client.service >/dev/null 2>&1 && \
	/bin/systemctl start sm-client.service >/dev/null 2>&1 || :

%triggerpostun -n sendmail-sysvinit -- sendmail < %{sysv2systemdnvr}
/sbin/chkconfig --add sendmail >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%dir %{_docdir}/sendmail-%{version}
%doc %{_docdir}/sendmail-%{version}/FAQ
%doc %{_docdir}/sendmail-%{version}/KNOWNBUGS
%doc %{_docdir}/sendmail-%{version}/LICENSE
%doc %{_docdir}/sendmail-%{version}/README
%doc %{_docdir}/sendmail-%{version}/RELEASE_NOTES.gz
%{_bindir}/hoststat
%{_bindir}/makemap
%{_bindir}/purgestat
%{_sbindir}/mailstats
%{_sbindir}/makemap
%{_sbindir}/praliases
%attr(2755,root,smmsp) %{_sbindir}/sendmail.sendmail
%{_bindir}/rmail.sendmail
%{_bindir}/newaliases.sendmail
%{_bindir}/mailq.sendmail
%{_sbindir}/smrsh
/usr/lib/sendmail.sendmail

%{_mandir}/man8/rmail.sendmail.8.*
%{_mandir}/man8/praliases.8.*
%{_mandir}/man8/mailstats.8.*
%{_mandir}/man8/makemap.8.*
%{_mandir}/man8/sendmail.sendmail.8.*
%{_mandir}/man8/smrsh.8.*
%{_mandir}/man8/hoststat.8.*
%{_mandir}/man8/purgestat.8.*
%{_mandir}/man5/aliases.sendmail.5.*
%{_mandir}/man1/newaliases.sendmail.1.*
%{_mandir}/man1/mailq.sendmail.1.*

# dummy attributes for rpmlint
%ghost %attr(0755,-,-) %{_sbindir}/sendmail
%ghost %attr(0755,-,-) %{_bindir}/mailq
%ghost %attr(0755,-,-) %{_bindir}/newaliases
%ghost %attr(0755,-,-) %{_bindir}/rmail
%ghost %attr(0755,-,-) /usr/lib/sendmail

%ghost %{_sysconfdir}/pam.d/smtp
%ghost %{_mandir}/man8/sendmail.8.bz2
%ghost %{_mandir}/man1/mailq.1.bz2
%ghost %{_mandir}/man1/newaliases.1.bz2
%ghost %{_mandir}/man5/aliases.5.bz2
%ghost %{_mandir}/man8/rmail.8.bz2

%dir %{stdir}
%dir %{_sysconfdir}/smrsh
%dir %{maildir}
%attr(0770,smmsp,smmsp) %dir %{spooldir}/clientmqueue
%attr(0700,root,mail) %dir %{spooldir}/mqueue

%config(noreplace) %verify(not size mtime md5) %{stdir}/statistics
%config(noreplace) %{maildir}/Makefile
%config(noreplace) %{maildir}/make
%config(noreplace) %{maildir}/sendmail.cf
%config(noreplace) %{maildir}/submit.cf
%config(noreplace) %{maildir}/helpfile
%config(noreplace) %{maildir}/sendmail.mc
%config(noreplace) %{maildir}/submit.mc
%config(noreplace) %{maildir}/access
%config(noreplace) %{maildir}/domaintable
%config(noreplace) %{maildir}/local-host-names
%config(noreplace) %{maildir}/mailertable
%config(noreplace) %{maildir}/trusted-users
%config(noreplace) %{maildir}/virtusertable

%ghost %{maildir}/aliasesdb-stamp
%ghost %{maildir}/virtusertable.db
%ghost %{maildir}/access.db
%ghost %{maildir}/domaintable.db
%ghost %{maildir}/mailertable.db

%{_unitdir}/sendmail.service
%{_unitdir}/sm-client.service
%config(noreplace) %{_sysconfdir}/sysconfig/sendmail
%config(noreplace) %{_sysconfdir}/pam.d/smtp.sendmail
%{_sysconfdir}/NetworkManager/dispatcher.d/10-sendmail

%if "%{with_sasl2}" == "yes"
%config(noreplace) %{_sysconfdir}/sasl2/Sendmail.conf
%endif

%files cf
%defattr(-,root,root,-)
%doc %{sendmailcf}/README
%dir %{sendmailcf}
%{sendmailcf}/cf
%{sendmailcf}/domain
%{sendmailcf}/feature
%{sendmailcf}/hack
%{sendmailcf}/m4
%{sendmailcf}/mailer
%{sendmailcf}/ostype
%{sendmailcf}/sendmail.schema
%{sendmailcf}/sh
%{sendmailcf}/siteconfig

%files devel
%defattr(-,root,root,-)
%doc libmilter/docs/*
%dir %{_includedir}/libmilter
%{_includedir}/libmilter/*.h
%{_libdir}/libmilter.so

%files milter
%defattr(-,root,root,-)
%doc LICENSE
%doc %{_docdir}/sendmail-%{version}/README.libmilter
%{_libdir}/libmilter.so.[0-9].[0-9]
%{_libdir}/libmilter.so.[0-9].[0-9].[0-9]

%files doc
%defattr(-,root,root,-)
%{_docdir}/sendmail-%{version}/README.cf
%{_docdir}/sendmail-%{version}/README.sendmail
%{_docdir}/sendmail-%{version}/README.smrsh
%{_docdir}/sendmail-%{version}/SECURITY
%{_docdir}/sendmail-%{version}/op.pdf
%dir %{_docdir}/sendmail-%{version}/contrib
%attr(0644,root,root) %{_docdir}/sendmail-%{version}/contrib/*

%files sysvinit
%defattr(-,root,root,-)
%{_initscriptdir}/sendmail

%changelog
* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8.14.7-1m)
- update 8.14.7

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.14.5-2m)
- rebuild against libdb-5.3.15

* Sun Oct 16 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.14.5-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.14.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.14.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.14.4-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.4-3m)
- rebuild against openssl-1.0.0

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.4-2m)
- more provides
- setup >= 2.8.15-1m

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.4-1m)
- [SECURITY] CVE-2009-4565
- sync with Fedora 13
- the priority of alternatives is still 50
-- 
-- * Mon Mar 29 2010 Jaroslav Skarvada <jskarvad@redhat.com> - 8.14.4-4
-- - rebuilt with compatdb-4.7.25 (#556148)
-- 
-- * Mon Feb 15 2010 Jaroslav Skarvada <jskarvad@redhat.com> - 8.14.4-3
-- - fixed libresolv implicit DSO linking (#564647)
-- - fixed initscript LSB compliance (#561040)
-- 
-- * Thu Feb 04 2010 Jaroslav Skarvada <jskarvad@redhat.com> - 8.14.4-2
-- - fixed typo in spec file
-- - fixed aliases_dir patch
-- 
-- * Tue Feb 02 2010 Jaroslav Skarvada <jskarvad@redhat.com> - 8.14.4-1
-- - new version 8.14.4 (#552078)
-- - RPM attributes S, 5, T not recorded for statistics file
-- - adapted patches: makemapman, dynamic, switchfile (#552078)
-- - movefiles patch incorporated into aliases_dir patch
-- - drop exitpanic patch (fixed upstream)
-- 
-- * Sun Jan 03 2010 Robert Scheck <robert@fedoraproject.org> 8.14.3-10
-- - handle IPv6:::1 in block_bad_helo.m4 like 127.0.0.1 (#549217)
-- 
-- * Tue Dec 15 2009 Miroslav Lichvar <mlichvar@redhat.com> 8.14.3-9
-- - fix milter file descriptors leaks (#485426)
-- - skip colon separator when parsing service name in ServiceSwitchFile
-- - return with non-zero exit code when free space is below MinFreeBlocks
-- - fix service stop/restart when only smclient is running
-- - fix submit.cf and helpfile permissions
-- - more merge review fixes (#226407)
-- 
-- * Wed Sep 16 2009 Tomas Mraz <tmraz@redhat.com> - 8.14.3-8
-- - Use password-auth common PAM configuration instead of system-auth
-- 
-- * Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 8.14.3-7
-- - rebuilt with new openssl
-- 
-- * Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 8.14.3-6
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 8.14.3-5
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Tue Jan 20 2009 Miroslav Lichvar <mlichvar@redhat.com> 8.14.3-4
-- - build shared libmilter (#309281)
-- - drop static libraries
-- - convert RELEASE_NOTES to UTF-8

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.3-6m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.14.3-4m)
- stop daemon

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.3-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.3-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.14.3-1m)
- sync with Rawhide (8.14.3-3)

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.14.2-6m)
- rebuild against db4-4.7.25-1m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.14.2-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.14.2-4m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.14.2-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.14.2-2m)
- %%NoSource -> NoSource

* Fri Nov  2 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.14.2-1m)
- up to 8.14.2

* Sun Oct 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.14.1-2m)
- rebuild against db4-4.6.21

* Mon Jun 18 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.14.1-1m)
- up to 8.14.1
- be synchronized with Fedora
- change AliasFile

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.13.8-2m)
- rebuild against db4-4.5.20

* Thu Aug 17 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.13.8-1m)
- up to 8.13.8

* Sat Aug  5 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.13.7-2m)
- fix alternatives

* Sun Jun 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (8.13.7-1m)
- update to 8.13.7
- [SECURITY] CVE-2006-1173
- modify patch7 (rename sendmail-8.13.7-pid.patch)

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.13.6-1m)
- [SECURITY] CVE-2009-1490
- version up

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (8.13.1-2m)
- rebuild against for MySQL-4.1.14

* Wed Dec 08 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.13.1-1m)
- update to 8.13.1
- delete harmful ifdef old_setup for /etc/aliases problem
- add aliases from setup
- add Patch13: sendmail-8.13.1-errata_cataddr.patch

* Tue Jul 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (8.13.0-4m)
- specify PreReq by package name instead of path

* Wed Jul 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (8.13.0-3m)
- nigirisugi

* Sun Jul 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (8.13.0-2m)
- modify spec

* Fri Jul  9 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (8.13.0-1m)
- reimport from Fedora Core development(8.13.0-1.1)
- change alternatives cost 50

* Wed Jan 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.12.11-1m)
- update to 8.2.11
- update Patch4: sendmail-8.12.11-smrsh-paths.patch

* Mon Nov 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (8.12.10-2m)
- rebuild against gdbm-1.8.0-19m

* Thu Sep 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.12.10-1m)
- update to 8.2.10 for a critical security problem
  http://www.sendmail.org/
  http://lwn.net/Articles/49630/
- update Patch4: sendmail-8.12.10-smrsh-paths.patch
- update Patch5: sendmail-8.12.10-movefiles.patch
- update Patch7: sendmail-8.12.10-pid.patch

* Mon Jun 23 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.9-2m)
- rebuild against cyrus-sasl-2.1.13
- enable milter when hesiod_support is 0

* Mon Mar 31 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.9-1m)

* Sat Mar 22 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.8-3m)
- move Name: and Summary: ([Momonga-devel.ja:01504])

* Tue Mar  4 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.8-2m)
- add URL: preamble

* Tue Mar  4 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.8-1m)
- remove Patch8: sendmail-8.12.7-etrn.patch (already applied)
- remove Patch11: sendmail-8.12.7-proto.patch (already applied)

* Sat Feb 22 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.7-2m)
- turn BuildRequires: db4-devel, instead of db3-devel

* Thu Feb  6 2003 HOSONO Hidetomo <h12o@h12o.org>
- (8.12.7-1m)
- import from sendmail-8.12.7-6 for rawhide and momonga-ize
- use specopt
- define _use_alternative macro (but do not use)
- define _use_standard_docdir macro (but do not use)
- remove http://www.informatik.uni-kiel.de/%7Eca/email/rules/check.tar
  because it seems to be no longer used
- remove sendmail-8.9.3-etc-mail-Makefile instead of
  sendmail-etc-mail-Makefile
- remove sendmail8.11.6+3.4W.patch.gz (WIDE patch for 8.11.6)
- remove other unnecessary patches

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (8.11.6-6k)

* Tue Oct 16 2001 Toshiro HIKITA <toshi@sodan.org>
- (8.11.6-4k)
- add WIDE Patch for 8.11.6

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (8.11.6-2k)
- http://www.securityfocus.com/bid/3163
- WIDE Patch for 8.11.6 is not released yet, so not applied this time.

* Tue May  5 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (8.11.4-2k)
- update to sendmail 8.11.4-2k

* Mon Apr 17 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (8.11.3-4k)
- backport to "Mary"

* Mon Apr 17 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (8.11.3-5k)
- fixed sendmail.init /etc/rc.d/init.d -> /etc/init.d

* Mon Apr  2 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (8.11.3-3k)
- update to sendmail 8.11.3
- update patch sendmail8.11.3+3.4W.patch.gz
- rework patches sendmail-8.11.2-smrsh-paths.patch to sendmail-8.11.3...

* Sun Apr  1 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (8.11.2-3k)
- update to sendmail 8.11.2
- update patch sendmail8.11.0+3.4Wpre to sendmail8.11.2+3.4W.patch.gz
- rework patches sendmail-8.10.0-smrsh-paths.patch to sendmail-8.11.2...
- rework patches sendmail-8.10.0-movefiles.patch to sendmail-8.11.2...

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (8.11.0-9k)
- rebuild againt rpm-3.0.5-39k

* Wed Nov 29 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}.

* Mon Aug 21 2000 Toru Hoshina <t@kondara.org>
- add patch for WIDE p3.4pre.

* Tue Jul 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- disable the LDAP support until we can remove the sendmail->OpenLDAP->perl dep
- fix prereq

* Tue Jul 25 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to sendmail 8.11.0
- add LDAP support

* Thu Jul 20 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jul  9 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- require procmail
- add further aliases

* Sat Jul  8 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- prereq init.d
- fix typo

* Tue Jul  4 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- ignore error from useradd

* Fri Jun 30 2000 Than Ngo <than@redhat.de>
- FHS fixes
- /etc/rc.d/init.d -> /etc/init.d
- fix initscript

* Fri Jun 23 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- change to /usr/share/man

* Wed Jun 21 2000 Preston Brown <pbrown@redhat.com>
- turn off daemon behaviour by default

* Mon Jun 18 2000 Bill Nottingham <notting@redhat.com>
- rebuild, fix dependencies

* Sat Jun 10 2000 Bill Nottingham <notting@redhat.com>
- prereq /usr/sbin/useradd

* Fri May 19 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- enable MAP_REGEX
- enable tcp_wrapper support

* Thu May 18 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- fix /etc/mail/aliases -> /etc/aliases in sendmail-redhat.mc

* Wed May  3 2000 Bill Nottingham <notting@redhat.com>
- update to 8.10.1
- fix build without sendmail installed
- add 'mailnull' user

* Wed Mar 15 2000 Bill Nottingham <notting@redhat.com>
- update to 8.10.0
- remove compatiblity chkconfig links
- add a mailnull user for sendmail to use

* Thu Feb 17 2000 Cristian Gafton <gafton@redhat.com>
- break the hard link for makemap and create it as a symlnk (#8223)

* Thu Feb 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix location of mailertable (Bug #6035)

* Sat Feb  5 2000 Bill Nottingham <notting@redhat.com>
- fixes for non-root builds (#8178)

* Wed Feb  2 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- change perms on /etc/sysconfig/sendmail from 0755 to 0644
- allow compressed man-pages

* Thu Dec 02 1999 Cristian Gafton <gafton@redhat.com>
- add patch to prevent the DoS when rebuilding aliases

* Wed Sep  1 1999 Jeff Johnson <jbj@redhat.com>
- install man pages, not groff output (#3746).
- use dnl not '#' in m4 comment (#3749).
- add FEATURE(mailtertable) to the config -- example file needs this (#4649).
- use db2 not db1.

* Tue Aug 31 1999 Jeff Johnson <jbj@redhat.com>
- add 127.0.0.1 to /etc/mail/access to avoid IDENT: relay problem (#3178).

* Tue Aug 31 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in preun, not postun (#3982)

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Fri Jul 02 1999 Cristian Gafton <gafton@redhat.com>
- fixed typo bug in comment in the default .mc file (#2812)

* Mon Apr 19 1999 Cristian Gafton <gafton@redhat.com>
- fox the awk scripts in the postinstall
- enable FEATURE(accept_unresolvable_domains) by default to make laptop
  users happy.

* Sun Apr 18 1999 Cristian Gafton <gafton@redhat.com>
- make the redhat.mc be a separate source files. Sanitize patches that used
  to touch it.
- install redhat.mc as /etc/sendmail.mc so that people can easily modify
  their sendmail.cf configurations.

* Mon Apr 05 1999 Cristian Gafton <gafton@redhat.com>
- fixed virtusertable patch
- make smrsh look into /etc/smrsh

* Mon Mar 29 1999 Jeff Johnson <jbj@redhat.com>
- remove noreplace attr from sednmail.cf.

* Thu Mar 25 1999 Cristian Gafton <gafton@redhat.com>
- provide a more sane /etc/mail/access default config file
- use makemap to initializa the empty databases, not touch
- added a small, but helpful /etc/mail/Makefile

* Mon Mar 22 1999 Jeff Johnson <jbj@redhat.com>
- correxct dangling symlinks.
- check for map file existence in %post.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- improved 8.9.3 config from Mike McHenry <mmchen@minn.net>

* Tue Mar 16 1999 Cristian Gafton <gafton@redhat.com>
- version 8.9.3

* Tue Dec 29 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0
- use the libdb1 stuff correctly

* Mon Sep 21 1998 Michael K. Johnson <johnsonm@redhat.com>
- Allow empty QUEUE in /etc/sysconfig/sendmail for those who
  want to run sendmail in daemon mode without processing the
  queue regularly.

* Thu Sep 17 1998 Michael K. Johnson <johnsonm@redhat.com>
- /etc/sysconfig/sendmail

* Fri Aug 28 1998 Jeff Johnson <jbj@redhat.com>
- recompile statically linked binary for 5.2/sparc

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- enhanced initscripts

* Fri May 01 1998 Cristian Gafton <gafton@redhat.com>
- added a rmail patch

* Wed Oct 29 1997 Donnie Barnes <djb@redhat.com>
- argh!  Fixed some of the db1 handling that had to be added for glibc 2.1

* Fri Oct 24 1997 Donnie Barnes <djb@redhat.com>
- added support for db1 on SPARC

* Thu Oct 16 1997 Donnie Barnes <djb@redhat.com>
- added chkconfig support
- various spec file cleanups
- changed group to Networking/Daemons (from Daemons).  Sure, it runs on
  non networked systems, but who really *needs* it then?

* Wed Oct 08 1997 Donnie Barnes <djb@redhat.com>
- made /etc/mail/deny.db a ghost
- removed preun that used to remove deny.db (ghost handles that now)
- NOTE: upgrading from the sendmail packages in 4.8, 4.8.1, and possibly
  4.9 (all Red Hat betas between 4.2 and 5.0) could cause problems.  You
  may need to do a makemap in /etc/mail and a newaliases after upgrading
  from those packages.  Upgrading from 4.2 or prior should be fine.

* Mon Oct 06 1997 Erik Troan <ewt@redhat.com>
- made aliases.db a ghost

* Tue Sep 23 1997 Donnie Barnes <djb@redhat.com>
- fixed preuninstall script to handle aliases.db on upgrades properly

* Mon Sep 15 1997 Donnie Barnes <djb@redhat.com>
- fixed post-install output and changed /var/spool/mqueue to 755

* Thu Sep 11 1997 Donnie Barnes <djb@redhat.com>
- fixed /usr/lib/sendmail-cf paths

* Tue Sep 09 1997 Donnie Barnes <djb@redhat.com>
- updated to 8.8.7
- added some spam filtration
- combined some makefile patches
- added BuildRoot support

* Wed Sep 03 1997 Erik Troan <ewt@redhat.com>
- marked initscript symlinks as missingok
- run newalises after creating /var/spool/mqueue

* Thu Jun 12 1997 Erik Troan <ewt@redhat.com>
- built against glibc, udated release to -6 (skipped -5!)

* Tue Apr 01 1997 Erik Troan <ewt@redhat.com>
- Added -nsl on the Alpha (for glibc to provide NIS functions).

* Mon Mar 03 1997 Erik Troan <ewt@redhat.com>
- Added nis support.
