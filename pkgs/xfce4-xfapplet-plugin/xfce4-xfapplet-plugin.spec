%global momorel 11
%global xfce4ver 4.6.2

Name:           xfce4-xfapplet-plugin
Version:        0.1.0
Release:	%{momorel}m%{?dist}
Summary:        A plugin to use gnome-panel based applets inside the Xfce4 panel
Group:          User Interface/Desktops
License:        GPL
URL:            http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:        http://goodies.xfce.org/releases/%{name}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxml2-devel, gettext, perl-XML-Parser
BuildRequires:  gnome-panel-devel >= 2.25.91
Requires:       xfce4-panel >= %{xfce4ver}
Requires:       gnome-applets

%description
The XfApplet Plugin is a plugin for the Xfce 4 Panel which allows one to use 
applets designed for the Gnome Panel inside the Xfce Panel. You can think of 
XfApplet as a tiny Gnome Panel that lives inside the Xfce Panel and allows 
you to show the same applets that the Gnome Panel is capable of showing.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/pixmaps/xfapplet*.svg
%{_datadir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-9m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-8m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-6m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-4m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-1m)
- import to Momonga from fc

* Mon Jan 22 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-2
- Rebuild for Xfce 4.4.

* Sat Sep 23 2006 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-1
- Initial Fedora Extras version.
