%global         momorel 1

%global         major 0.5

Summary:        A lightweight GTK+ web browser 
Name:           midori
Version:        0.5.2
Release:        %{momorel}m%{?dist}
Group:          Applications/Internet
License:        LGPLv2+
URL:            http://twotoasts.de/index.php/midori/
Source0:        http://archive.xfce.org/src/apps/midori/%{major}/midori-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  gtksourceview2-devel
BuildRequires:  intltool
BuildRequires:  libidn-devel
BuildRequires:  libnotify-devel >= 0.7.2
BuildRequires:  librsvg2
BuildRequires:  libsexy-devel
BuildRequires:  libsoup-devel
BuildRequires:  libtool
BuildRequires:  libxml2-devel
BuildRequires:  perl-XML-Parser
BuildRequires:  python-docutils
BuildRequires:  sqlite-devel
BuildRequires:  unique-devel
## XXX: Using the system waf makes it fail to build (python error).
# BuildRequires:	waf >= 1.5
BuildRequires:  webkitgtk-devel

%description
Midori is a lightweight web browser, and has many features expected of a
modern browser, including:
* Full integration with GTK+2.
* Fast rendering with WebKit.
* Tabs, windows and session management.
* Bookmarks are stored with XBEL.
* Searchbox based on OpenSearch.
* Custom context menu actions.
* User scripts and user styles support.
* Extensible via Lua scripts.

The project is currently in an early alpha state. The features are still being
implemented, and some are still quite incomplete.

%prep
%setup -q

## Use the system-provided waf, instead of the in-tarball copy.
#rm -rf waf

%build
export CFLAGS="%{optflags}"
./waf -j1 --prefix=%{_usr}                       \
          --docdir=%{_docdir}/%{name}-%{version} \
          --libdir=%{_libdir}                    \
          --enable-apidocs                       \
          configure
./waf -j1 build


%install
rm -rf %{buildroot}
./waf -j1 --destdir=%{buildroot} install
%find_lang %{name}
desktop-file-install                                     \
        --vendor=""                                     \
        --delete-original                               \
        --dir %{buildroot}%{_datadir}/applications       \
        %{buildroot}%{_datadir}/applications/%{name}.desktop


%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null ||:


%postun
if [ $1 -eq 0 ] ; then
        touch --no-create %{_datadir}/icons/hicolor &>/dev/null 
        if [ -x %{_bindir}/gtk-update-icon-cache ]; then
	        %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor &>/dev/null ||:
        fi
fi


%posttrans
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor &>/dev/null ||:
fi


## Need to separaqte devel package ?
%files -f %{name}.lang
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}/
%{_includedir}/%{name}-%{major}
%{_bindir}/midori
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/%{name}-private.desktop
%{_datadir}/icons/*/*/apps/%{name}.*
%{_datadir}/icons/*/*/categories/extension.*
%{_datadir}/icons/*/*/status/internet-news-reader.*
%{_datadir}/%{name}/
%{_datadir}/vala/vapi/*
%{_libdir}/%{name}/
%{_sysconfdir}/xdg/%{name}/


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Sun Apr 14 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0
- remove Patch0: midori-0.4.7-desktop.patch

* Tue Jan  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.7-1m)
- update to 0.4.7

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.6-1m)
- update
- build against xfce4-4.10.0

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-2m)
- use webkitgtk-devel instead of webkit-devel

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Sun Dec 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Jun 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-3m)
- add -j1 to avoid a possible dead lock with python-2.7.2

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.6-2m)
- rebuild against xfce4-4.8

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-4m)
- rebuild with webkit

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-3m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-2m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-1m)
- update to 0.2.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8-2m)
- rebuild against webkitgtk-1.3.4

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.8-1m)
- update to 0.2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.6-3m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-2m)
- rebuild against xfce4 4.6.2

* Sat May 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.6-1m)
- update to 0.2.6

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Mon Aug  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-1m)
- update to 0.1.9

* Mon Jul 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7
- use internal waf

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-2m)
- rebuild against WebKit-1.1.1

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.3-1m)
- import from Fedora to Momonga

* Sun Feb 22 2009 Peter Gordon <peter@thecodergeek.com> - 0.1.3-1
- Update to new upstream release (0.1.3): support for bookmark folders,
  full image zoom, and "Find as you type" (among other enhancements).
- Add patch to remove git build-time dependency:
  + no-git.patch

* Sat Jan 31 2009 Peter Gordon <peter@thecodergeek.com> - 0.1.2-1
- Update to new upstream release (0.1.2): support for bookmarklets
  ("javascript:foo" URLs and bookmarks), better persistent cookie support,
  preference changes saved dynamically. Lots of startup fixes for speed
  issues, too. :)

* Sat Dec 20 2008 Peter Gordon <peter@thecodergeek.com> - 0.1.1-1
- Update to new upstream release (0.1.1): contains many enhancements and
  bugfixes - including error pages, basic documentation, panel history
  support, icon caching, libsoup integration, support for WebKit's Inspector
  functionality, and the beginnings of support for runtime extensions (in C).

* Tue Sep 09 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.21-1
- Update to new upstream release (0.0.21): contains updated translations,
  fixes for GVFS-->GIO regressions, and various aesthetic enhancements.
  (See the included ChangeLog for full details.)
- Update Source0 URL.

* Sun Sep 07 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.20-2
- Add scriplets for GTK+ icon cache.

* Sun Sep 07 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.20-1
- Update to new upstream release (0.0.20): adds support for single instances,
  some userscripts and Greasemonkey scripting, zooming and printing, as well as
  enhanced news feed detection and session-saving (among other improvements).
- Switch to WAF build system.

* Fri Aug  8 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.0.18-2
- fix license tag

* Sat May 24 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.18-1
- Update to new upstream release (0.0.18), adds some translations and
  a lot of bug-fixes.
- Alphabetize dependency list (aesthetic-only change).

* Sat Apr 12 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.17-3
- Rebuild for updated WebKit library so-name and include directory.

* Mon Mar 03 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.17-2
- Cleanups from review (bug 435661):
  (1) Fix consistency of tabs/spaces usage.
  (2) Fix source permissions.
  (3) Add desktop-file-utils build dependency.

* Sun Mar 02 2008 Peter Gordon <peter@thecodergeek.com> - 0.0.17-1
- Initial packaging for Fedora.
