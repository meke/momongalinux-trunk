%global momorel 18

Summary: Advanced Power Management (APM) BIOS utilities for laptops.
Name: apmd
Version: 3.2.2
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.worldvisions.ca/~apenwarr/apmd/
Group: System Environment/Daemons
Source0: ftp://ftp.debian.org/debian/pool/main/a/apmd/%{name}_%{version}.orig.tar.gz
Source1: apmd.init
Source2: apmscript
Source3: apmd.conf
Source4: laptopmode
Source5: apmd.modules
Source6: apmd.service
Patch0: apmd-3.2-build.patch
Patch1: apmd-3.2-umask.patch
Patch2: apmd-3.2-error.patch
Patch4: apmd-3.2-x.patch
Patch5: apmd-3.2.2-lib64.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pm-utils
BuildRequires: libtool
ExclusiveArch: %{ix86} x86_64 ppc ia64
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
APMD is a set of programs for controlling the Advanced Power 
Management daemon and utilities found in most modern laptop 
computers. APMD can watch your notebook's battery and warn 
users when the battery is low. APMD is also capable of shutting 
down the PCMCIA sockets before a suspend.

Install the apmd package if you need to control the APM system 
on your laptop.

%package sysvinit
Group: System Environment/Daemons
Summary: SysV initscript for apmd daemon
Requires: %{name} = %{version}-%{release}
Requires(preun): initscripts
Requires(postun): initscripts

%description sysvinit
The apmd-sysvinit contains SysV initscritps support.

%prep
%setup -q -n apmd-%{version}.orig
%patch0 -p1 -b .build
%patch1 -p1 -b .umask
%patch2 -p1 -b .error
%patch4 -p1 -b .x
%if %{_lib} == "lib64"
%patch5 -p1 -b .lib64~
%endif
iconv -f iso-8859-1 -t utf-8 < apmsleep.fr.1 > apmsleep.fr.1_
mv apmsleep.fr.1_ apmsleep.fr.1

%build
make CFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/{module-load.d,sysconfig/apm-scripts,init.d}
mkdir -p $RPM_BUILD_ROOT%{_unitdir}

%makeinstall APMD_PROXY_DIR=%{buildroot}%{_sysconfdir}

mkdir -p %{buildroot}%{_mandir}/man{1,8}
mkdir -p %{buildroot}%{_mandir}/fr/man1
for manpage in apm apmsleep ; do
  install -m 644 $manpage.1 %{buildroot}/%{_mandir}/man1/
done
install -m 644 apmd.8 %{buildroot}%{_mandir}/man8/
install -m 644 apmsleep.fr.1 %{buildroot}%{_mandir}/fr/man1/

install -m 755 %{SOURCE1} %{buildroot}%{_sysconfdir}/init.d/apmd
install -m 755 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/apm-scripts/
install -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/apmd
install -m 755 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/apm-scripts/
install -m 755 %{SOURCE5} %{buildroot}%{_sysconfdir}/module-load.d/apmd.conf
install -m 755 %{SOURCE6} %{buildroot}%{_unitdir}

# conflicts with pm-utils
rm -rf %{buildroot}%{_bindir}/on_ac_power
rm -rf %{buildroot}%{_sysconfdir}/apmd_proxy
rm -rf %{buildroot}%{_bindir}/on_ac_power
rm -rf %{buildroot}%{_libdir} %{buildroot}%{_includedir}

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%preun
if [ $1 = 0 ]; then
  /bin/systemctl disable apmd.service > /dev/null 2>&1 || :
  /bin/systemctl stop apmd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

if [ "$1" -ge "1" ]; then
  /bin/systemctl try-restart apmd.service >/dev/null 2>&1 || :
fi

%triggerun --  %{name} < 3.2.2-17m
  /sbin/chkconfig --del apmd >/dev/null 2>&1 || :
  /bin/systemctl try-restart apmd.service >/dev/null 2>&1 || :

%triggerpostun -n %{name}-sysvinit -- %{name} <= 1:3.2.2-13
  /sbin/chkconfig --add apmd >/dev/null 2>&1 || :


%files
%defattr(-,root,root)
%doc ChangeLog COPYING apmlib.COPYING README AUTHORS LSM
%config(noreplace) %{_sysconfdir}/sysconfig/apmd
%config(noreplace) %{_sysconfdir}/sysconfig/apm-scripts/
%config(noreplace) %{_sysconfdir}/module-load.d/apmd.conf
%dir %{_sysconfdir}/sysconfig/apm-scripts
%config /%{_sysconfdir}/sysconfig/apm-scripts/apmscript
%{_bindir}/apm
%{_bindir}/apmsleep
%{_sbindir}/apmd
%{_mandir}/man?/apm.1*
%{_mandir}/man?/apmd.8*
%{_mandir}/man?/apmsleep.1*
%{_mandir}/fr/man?/apmsleep.fr.1*
%{_unitdir}/apmd.service

%files sysvinit
%attr(0755,root,root) %{_sysconfdir}/init.d/apmd

%changelog
* Sun Sep 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-18m)
- modify Requires

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-17m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.2-14m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-13m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jan 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-11m)
- update lib64.patch to enable build with rpm-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-10m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-9m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-8m)
- rebuild against gcc43

* Wed Jun 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-7m)
- disable service at initial start up

* Sat Feb 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-6m)
- revise apmscript

* Sat Feb 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-5m)
- revive apm.h, battfink needs it

* Fri Feb 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-4m)
- adjust package for pm-utils
- sync with Fedora

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-3m)
- delete libtool library

* Fri Feb 24 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.2.2-2m)
- add patch5 for x86_64

* Thu Feb 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-1m)
- version 3.2.2
- No NoSource

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.2-10m)
- revised docdir permission

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.0.2-9m)
- enable x86_64.

* Tue Oct 19 2004 TABUCHI Takaki <tab@momonga-linux.org>
- (3.0.2-8m)
- add Patch7: apmd-3.0.2-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.2-7m)
- revised spec for enabling rpm 4.2.

* Sat Jul 12 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (3.0.2-6m)
- add a patch for LOCK_X in /etc/sysconfig/apm-script/apmscript
- use %%{momorel}

* Fri Dec 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.2-5m)
- import patches from redhat
  apmd-3.0.2-kernel2418.patch
  apmd-3.0-security.patch

* Fri May 17 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (3.0.2-4k)
- remove %triggerpostun jitterbug #1029

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.2-2k)
- update to 3.0.2

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (3.0-32k)
- nigittenu

* Sun May 18 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0-30k)
- correct apmd.acpi_module.patch

* Sun Apr 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0-28k)
- add apmd.acpi_module.patch for ACPI and APM module

* Thu Apr 12 2001 Shingo Akagaki <dora@kondara.org>
- build patch for Kondara 2.0

* Mon Mar 26 2001 Motonobu Ichimura <famao@kondara.org>
- (3.0-24k)
- fixed bug (#860)

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0-22k)
- rebuild against glibc-2.2.2 and add apmd.glibc222.time.patch

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0-20k)
- modified the bug that failed if %{_initscriptdir} is /etc/rc.d/init.d
- added apmd-3.0-Makefile.patch for compatiblity

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Mon Jul 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Use cardctl to suspend/resume pcmcia cards instead of restarting
  the entire subsystem (Bug #13836)
* Mon Jul 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Don't use obsolete parameters (Bug #13710)
- Add the possibility to lock X displays on suspend, based on patch from
  Hannu Martikka <Hannu.Martikka@nokia.com>
- some other fixes to the apmd scripts

* Sat Jul 15 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 27 2000 Preston Brown <pbrown@redhat.com>
- don't prereq, just require initscripts

* Mon Jun 26 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix init script, move to /etc/init.d

* Sun Jun 25 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix resume for users of non-bash shells (Bug #11580)
- Support restoring sound with the commercial OSS drivers (Bug #11580)

* Mon Jun 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- FHSify

* Sat May 27 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Some more changes to apm-scripts:
  - Fix up HDPARM_AT_RESUME
  - Add ANACRON_ON_BATTERY, and default to turning it off

* Mon May  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Various fixes to the apm-scripts:
  - use modprobe instead of insmod for restoring sound
  - don't try to restore the X display if X isn't running
  - /usr/sbin/anacron, not /usr/bin/anacron
  - misc. cleanups

* Fri Feb  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild to compress man pages

* Mon Jan 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Update to 3.0final

* Mon Jan 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fixes for UTC clocks (Bug #7939)

* Thu Jan  6 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- If anacron is installed, run it at resume time.

* Sun Nov 21 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up the broken harddisk fix (needs to be done earlier during suspend,
  also we need to manually wake the drive at resume.)

* Sun Nov 21 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Updates to the apm-scripts and sysconfig/apmd:
  - Make hwclock --hctosys call optional (CLOCK_SYNC variable)
  - Add possibility to modify hdparm settings on suspend/resume.
    Some broken harddisks (Gericom 3xC) require this for suspend
    to disk to work.

* Wed Nov 10 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Put in new apm scripts to handle PCMCIA suspend/resume, and give the
  possibility to refresh displays and reload sound modules for some
  broken chipsets
- permit builds on i486, i586 and i686 archs

* Mon Sep 20 1999 Michael K. Johnson <johnsonm@redhat.com>
- accept both "UTC=yes" and "UTC=true"

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- uh-oh, do I own this now?
- update to 3.0beta9

* Thu Jul  1 1999 Bill Nottingham <notting@redhat.com>
- start after, die before network fs...

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- add check for /proc/apm (not on SMP) (#3403)

* Mon May 31 1999 Jeff Johnson <jbj@redhat.com>
- shell script tweak (#3176).

* Fri May  7 1999 Bill Nottingham <notting@redhat.com>
- set -u flag for utc

* Sat Apr 17 1999 Matt Wilson <msw@redhat.com>
- prereqs chkconfig

* Fri Apr 16 1999 Cristian Gafton <gafton@redhat.com>
- exlusive arch i3786, as sparcs and alphas have no apm support...

* Wed Apr 14 1999 <ewt@redhat.com>
- removed X bits; gnome has a much better X interface for apm anyway

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Fri Mar 19 1999 Preston Brown <pbrown@redhat.com>
- quoted APMD_OPTIONS variable in the init script

* Tue Mar 09 1999 Preston Brown <pbrown@redhat.com>
- whoops, making /etc/rc.d/init.d/apmd a directory was a bad idea. fixed.

* Mon Mar 08 1999 Preston Brown <pbrown@redhat.com>
- now owned by Avery Pennarun <apenwarr@debian.org>, upgraded to his latest.

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Fri Nov 06 1998 Preston Brown <pbrown@redhat.com>
- updated to latest patchlevel from web page.

* Wed Apr 22 1998 Michael K. Johnson <johnsonm@redhat.com>
- enhanced init script

* Thu Apr 1 1998 Erik Troan <ewt@redhat.com>
- moved init script into a separate source file
- added restart and status options to initscript
- made it use a build root
- don't start apm when the package is installed
- don't stop apm when the package is removed

* Mon Dec  8 1997 Jan "Yenya" Kasprzak <kas@fi.muni.cz>
- Compiled on RH5.0 against libc6.
- Renamed /etc/rc.d/init.d/apmd.init to /etc/rc.d/init.d/apmd
- Make /etc/rc.d/init.d/apmd to be chkconfig-compliant.

* Thu Oct  2 1997 Jan "Yenya" Kasprzak <kas@fi.muni.cz>
- Fixed buggy /etc/sysconfig/apmd file generation in the spec file.
- Added a patch for apm.c's option handling.
- Both fixes were submitted by Richard D. McRobers <rdm@csn.net>
