%global momorel 6

%define lang de
%define langrelease 1
%define aspellversion 6
Summary: German dictionaries for Aspell
Name: aspell-%{lang}
Version: 20030222
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell%{aspellversion}-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: German, Swiss German

%prep
%setup -q -n aspell%{aspellversion}-%{lang}-%{version}-%{langrelease}

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc COPYING
%defattr(-,root,root,-)

%{_libdir}/aspell-0.60/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030222-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030222-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20030222-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030222-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030222-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (20030222-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:20030222-4
- Autorebuild for GCC 4.3

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:20030222-3
- add documentation

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:20030222-1
- update to 20030222
- use configure script to create Makefile
- update default build root
- some minor spec changes

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-11.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-11.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-11.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Jul 18 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-11
- build with aspell-0.60.3

* Mon Apr 11 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-10
- rebuilt

* Wed Sep 29 2004 Adrian Havill <havill@redhat.com> 50:0.50-9
- remove debuginfo

* Wed Aug 11 2004 Adrian Havill <havill@redhat.com> 50:0.50-8
- sync epoch with other aspell dicts

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun 23 2003 Adrian Havill <havill@redhat.com> 0.50-5
- data files are arch dependent

* Fri Jun 20 2003 Adrian Havill <havill@redhat.com> 0.50-4
- first build for new aspell (0.50)
