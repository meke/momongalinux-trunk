%global momorel 4

Summary: A file compression utility.
Name: unrar
Version: 3.9.10
Release: %{momorel}m%{?dist}
License: "See license.txt"
Group: Applications/File
URL: http://www.rarlab.com/
Source0: http://www.rarlab.com/rar/%{name}src-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%description
The unRAR utility is a freeware program, distributed with source
code and developed for extracting, testing and viewing the
contents of archives created with the RAR archiver, version 1.50
and above.  For the usage and distribution license please read the
file LICENSE.TXT.

%prep
%setup -q -n %{name}

%build
make -f makefile.unix

%install
rm -rf --preserve-root %{buildroot}
install -p -m755 -D %{name} %{buildroot}%{_bindir}/%{name}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc readme.txt license.txt
%{_bindir}/unrar

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.10-2m)
- full rebuild for mo7 release

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.10-1m)
- update to 3.9.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.8-2m)
- %%NoSource -> NoSource

* Mon Oct  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7.8-1m)
- update to 3.7.8

* Sat Jul 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7.6-2m)
- to Main

* Tue Jun 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.6-1m)
- update to 3.7.6
- delete unused patch
- change source URI

* Mon Oct  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.71-1m)
- initial build
