%global momorel 5

Name:           phoronix-test-suite
Version:        1.8.1
Release:        %{momorel}m%{?dist}
License:        GPLv3+
URL:            http://phoronix-test-suite.com/
Source0:        http://www.phoronix-test-suite.com/releases/%{name}-%{version}.tar.gz
NoSource:       0
Group:          Applications/System
Summary:        Phoronix Test Suite
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       php, php-gd
BuildArch:      noarch

# norootforbuild

%description 
The Phoronix Test Suite is the most comprehensive testing and
benchmarking platform available for Linux and is designed to carry out
qualitative and quantitative benchmarks in a clean, reproducible, and
easy-to-use manner.

%prep
%setup -q -n %{name}

%build
%install
rm -rf %{buildroot}
./install-sh %{buildroot}%{_prefix}

sed -i 's|%{buildroot}||g' %{buildroot}%{_bindir}/phoronix-test-suite

chmod 755 %{buildroot}%{_datadir}/%{name}/pts/distro-scripts/install-pclinuxos-packages.sh

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc %{_datadir}/doc/%{name}
%{_datadir}/%{name}/*
%{_datadir}/icons/%{name}.png
%{_bindir}/phoronix-test-suite
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- import from opensuse to Momonga
