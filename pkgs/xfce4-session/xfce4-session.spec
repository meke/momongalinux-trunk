%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0

Name: 		xfce4-session
Version: 	4.11.0
Release:	%{momorel}m%{?dist}
Summary: 	Xfce Session manager

Group: 		User Interface/Desktops
License:	GPL
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires: libxfcegui4-devel >= %{xfce4ver}
BuildRequires: libxfce4ui-devel >= %{xfce4ver}
BuildRequires: xfce4-dev-tools
BuildRequires: xfconf-devel
BuildRequires: xfce4-settings >= %{xfce4ver}
BuildRequires: startup-notification-devel
BuildRequires: gettext intltool
BuildRequires: imake
BuildRequires: libXt-devel
BuildRequires: xorg-x11-server-utils
BuildRequires: dbus-devel GConf2-devel
Requires: dbus-x11
Requires: fortune-mod
#Requires: libxfcegui4 >= %{xfce4ver}
Requires: xfconf
Requires: xfce4-settings
Requires: xorg-x11-server-utils

Obsoletes: xfce-utils

%description
xfce4-session is the session manager for the Xfce desktop environment.

%package devel
Summary:	Development files for xfce4-session
Group:		Development/Libraries
Requires:	xfce4-session >= %{xfce4ver}

%description devel
Header files for the Xfce Session Manager.

%package engines
Summary:	Additional engines for xfce4-session
Group:		User Interface/Desktops
Requires:	xfce4-session >= %{xfce4ver}

%description engines
Additional splash screen engines for the Xfce Session Manager.

%prep
%setup -q

%build
%configure --enable-gnome --enable-final LIBS=-lgmodule-2.0
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "*.a" -delete

%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
#%%{_sysconfdir}/xdg/xfce4-session/*
#%%{_sysconfdir}/xdg/autostart/xfce4-tips-autostart.desktop
%{_sysconfdir}/xdg/autostart/xscreensaver.desktop
%dir %{_sysconfdir}/xdg/xfce4
%{_sysconfdir}/xdg/xfce4/*
%dir %{_sysconfdir}/xdg/xfce4/xfconf
%dir %{_sysconfdir}/xdg/xfce4/xfconf/xfce-perchannel-xml
%{_sysconfdir}/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml
%{_bindir}/*
%{_datadir}/applications/*
%{_datadir}/icons/*/*/*/*png
%{_datadir}/icons/*/*/*/*svg
#%%{_datadir}/xfce4/*/*/*/*png
#%%dir %{_datadir}/xfce4/tips
#%%{_datadir}/xfce4/tips/tips
#%%dir %{_datadir}/xfce4/panel-plugins/
#%%{_datadir}/xfce4/panel-plugins/xfsm-logout-plugin.desktop
%{_datadir}/xsessions/xfce.desktop
%{_datadir}/locale/*/*/*
#%%dir %{_datadir}/doc/%{name}
#%%dir %{_datadir}/doc/%{name}/html
#%%{_datadir}/doc/%{name}/html/*
#%%{_libdir}/xfce4/splash/engines/libmice.*
#%%dir %{_libdir}/xfce4/session
%{_libdir}/lib*.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%{_includedir}/xfce4/xfce4-session-*
#%%{_libdir}/xfce4/panel/plugins/libxfsm-logout-plugin.so
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc

%files engines
%defattr(-,root,root)
%dir %{_datadir}/themes/Default/balou
%{_datadir}/themes/Default/balou/*
%dir %{_libdir}/xfce4/session/splash-engines
%{_libdir}/xfce4/session/splash-engines/*
%{_libdir}/xfce4/session/xfsm-shutdown-helper
%{_libdir}/xfce4/session/balou-*

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to 4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-3m)
- change Source0 URI

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8.0-2m)
- build fix

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update
- rebuild against xfce4-4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.2-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- revise BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-2m)
- fix %%files

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Thu Jul  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-2m)
- add Requires: dbus-x11 for dbus-launch

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-3m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m libxfce4util-4.2.3-2m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.2.3-2m)
- rebuild against expat-2.0.0-1m

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90
