%global         momorel 5

Name:           perl-Catalyst-Plugin-Authentication
Version:        0.10023
Release:        %{momorel}m%{?dist}
Summary:        Infrastructure plugin for the Catalyst authentication framework
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Plugin-Authentication/
Source0:        http://www.cpan.org/authors/id/B/BO/BOBTFISH/Catalyst-Plugin-Authentication-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Catalyst-Plugin-Session >= 0.10
BuildRequires:  perl-Catalyst-Runtime
BuildRequires:  perl-Class-Inspector
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 2.0001
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Catalyst-Plugin-Session >= 0.10
Requires:       perl-Catalyst-Runtime
Requires:       perl-Class-Inspector
Requires:       perl-Moose >= 2.0001
Requires:       perl-MRO-Compat
Requires:       perl-Test-Exception
Requires:       perl-Test-Simple >= 0.88
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The authentication plugin provides generic user support for Catalyst apps.
It is the basis for both authentication (checking the user is who they
claim to be), and authorization (allowing the user to do what the system
authorises them to do).

%prep
%setup -q -n Catalyst-Plugin-Authentication-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Authentication
%{perl_vendorlib}/Catalyst/Plugin/Authentication
%{perl_vendorlib}/Catalyst/Plugin/Authentication.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10023-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10023-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10023-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10023-2m)
- rebuild against perl-5.18.0

* Fri Apr 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10023-1m)
- update to 0.10023

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10022-2m)
- rebuild against perl-5.16.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10022-1m)
- update to 0.10022

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10021-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10021-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10021-1m)
- update to 0.10021
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10018-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10018-1m)
- update to 0.10018

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10017-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10017-4m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10017-3m)
- rebuild against perl-Moose-2.0001

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10017-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10017-1m)
- update to 0.10017

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10016-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10016-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10016-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10016-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10016-2m)
- rebuild against perl-5.12.0

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10016-1m)
- update to 0.10016

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10015-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10015-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10015-1m)
- update to 0.10015

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10014-1m)
- update to 0.10014

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10013-2m)
- rebuild against perl-5.10.1

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10013-1m)
- update to 0.10013

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10012-1m)
- update to 0.10012

* Tue Mar 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10011-1m)
- update to 0.10011

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10010-1m)
- update to 0.10010

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100092-1m)
- update to 0.100092

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10009-2m)
- rebuild against rpm-4.6

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10009-1m)
- update to 0.10009

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10008-1m)
- update to 0.10008

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10007-1m)
- update to 0.10007

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10006-2m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00006-1m)
- update to 1.00006

* Sat Jan 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00005-1m)
- update to 1.00005

* Thu Dec  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10004-1m)
- update to 0.10004

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10003-1m)
- update to 0.10003

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10002-1m)
- update to 0.10002

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10000-1m)
- update to 0.10000
- change source URI
- remove Password.pm.patch from %%files section
- add Patch0 to provide Catalyst::Plugin::Authentication::Store::Minimal::Backend

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.09-3m)
- use vendor

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09-2m)
- delete dupclicate directory

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.09-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
