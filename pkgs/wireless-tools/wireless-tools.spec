%global momorel 9
%global srcname wireless_tools
%global ver 29

Summary: Wireless ethernet configuration tools
Group: System Environment/Base
License: GPL+
Name: wireless-tools
Version: %{ver}
Release: %{momorel}m%{?dist}
URL: http://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/Tools.html
Source0: http://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/%{srcname}.%{version}.tar.gz
# Source0: http://pcmcia-cs.sourceforge.net/ftp/contrib/wireless_tools.%{version}.tar.gz
NoSource: 0
Patch1: %{name}-29-makefile.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExcludeArch: s390 s390x

# This is artificial, for packaging purposes.  The presumption is that
# wireless users will install this package, so they will want crda too.
# This avoids adding a Requires to the kernel package that would affect
# non-wireless users as well.
Requires: crda

%description
This package contain the Wireless tools, used to manipulate
the Wireless Extensions. The Wireless Extension is an interface
allowing you to set Wireless LAN specific parameters and get the
specific stats for wireless networking equipment. 

%package devel
Summary: Development headers for the wireless-tools package
Group: Development/Libraries
Requires: wireless-tools = %{version}-%{release}

%description devel
Development headers for the wireless-tools package.

%prep
%setup -q -n %{srcname}.%{version}
%patch1 -p1 -b .makefile

%build
make clean
%make CFLAGS="-I. %{optflags}" BUILD_SHARED=1 FORCE_WEXT_VERSION=16

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}{/sbin,/%{_lib},%{_mandir}/man8,%{_includedir},%{_libdir}}

make install INSTALL_DIR=%{buildroot}/sbin \
        INSTALL_LIB=%{buildroot}/%{_lib} \
	INSTALL_INC=%{buildroot}%{_includedir} \
     INSTALL_MAN=%{buildroot}%{_mandir}
rm -f %{buildroot}/%{_lib}/libiw.{a,so}
ln -sf ../../%{_lib}/libiw.so.%{version} \
       %{buildroot}%{_libdir}/libiw.so

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc INSTALL README DISTRIBUTIONS.txt
/%{_lib}/*.so.*
/sbin/*
%{_mandir}/man*/*

%files devel
%defattr(-, root, root)
%{_includedir}/*
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (29-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (29-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (29-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (29-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (29-5m)
- Requires: crda for new kernel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (29-4m)
- rebuild against rpm-4.6

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (29-3m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (29-2m)
- rebuild against gcc43

* Mon Dec 17 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (29-1m)
- update Ver.29

* Mon Mar  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (28-1m)
- update 28-release

* Tue Nov  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (28-0.1.1m)
- modify version and release for OmoiKondara

* Sun Oct 30 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (28pre10-1m)
- version 28pre10
- build to synch with NetworkManager-0.5.1

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (27-2m)
- enable x86_64.

* Tue Nov 30 2004 Shingo Akagaki <dora@unya.org>
- (27-1m)
- update to 27

* Fri Sep  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (26-2m)
- rebuild against kernel-2.6

* Fri Jul 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (26-1m)
- update to 26

* Fri May  9 2003 Junichiro Kita <kita@momonga-linux.org>
- (26-0.8.1m)
- update to 26-pre8

* Thu Feb 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (26-0.7.1m)
- update to 26-pre7

* Thu Dec 12 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (26-0.5.1m)
- update to 26-pre5

* Sun Sep 15 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (25-1m)
  update to 25

* Tue Jul 23 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (24-1m)
  update to 24

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (23-2k)
- update to 23

* Thu Aug 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
-(21-2k)
 update to 21

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (20-4k)
- rebuild against glibc-2.2.2 and add wireless-tools.glibc222.patch

* Sat Nov 25 2000 Kenichi Matsubara <m@kondara.org>
- add %clean section.

* Sat Sep 23 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Mar 10 2000 Bill Nottingham <notting@redhat.com>
- initial build
