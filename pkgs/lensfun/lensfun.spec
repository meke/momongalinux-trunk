%global momorel 1
%global docver 0.2.8.0

Summary: A photographic lens database and a library for accessing it
Name: lensfun
Version: 0.2.8
Release: %{momorel}m%{?dist}
License: LGPLv3
Group: System Environment/Libraries
URL: http://lensfun.berlios.de/
Source0: http://download.berlios.de/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: glib2-devel
BuildRequires: libpng-devel
BuildRequires: python
BuildRequires: zlib-devel

%description
The goal of the lensfun library is to provide a open source database
of photographic lenses and their characteristics. In the past there
was a effort in this direction (see http://www.epaperpress.com/ptlens/),
but then author decided to take the commercial route and the database
froze at the last public stage. This database was used as the basement
on which lensfun database grew, thanks to PTLens author which gave his
permission for this, while the code was totally rewritten from scratch
(and the database was converted to a totally new, XML-based format).

The lensfun library not only provides a way to read the database and
search for specific things in it, but also provides a set of algorithms
for correcting images based on detailed knowledge of lens properties
and calibration data. Right now lensfun is designed to correct distortion,
transversal (also known as lateral) chromatic aberrations, vignetting
and colour contribution index of the lens.

%package devel
Summary: lensfun development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Header and library definition files for developing applications
that use the lensfun library/database.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
  -DBUILD_DOC:BOOL=ON \
  -DBUILD_TESTS:BOOL=OFF \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1
make doc -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README
%doc docs/adobe-lens-profile.txt
%doc docs/cc-by-sa-3.0.txt
%doc docs/gpl-3.0.txt
%doc docs/lgpl-3.0.txt
%doc docs/manual-main.txt
%doc docs/mounts.txt
%doc %{_docdir}/%{name}-%{docver}
%{_libdir}/lib%{name}.so.*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%doc %{_target_platform}/doc_doxygen/*.css
%doc %{_target_platform}/doc_doxygen/*.html
%doc %{_target_platform}/doc_doxygen/*.png
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.so

%changelog
* Fri May 16 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8-1m)
- version 0.2.8

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-7m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-6m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-5m)
- import cpuid.patch from Fedora to avoid crashing of ufraw-0.17 or higher
- https://bugzilla.redhat.com/show_bug.cgi?id=631674

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-3m)
- full rebuild for mo7 release

* Sun Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-2m)
- touch up spec file

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-1m)
- version 0.2.5
- clean up spec file

* Sun Nov 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-1m)
- [CRASH FIX] version 0.2.4, ufraw is working fully on trunk now
- remove merged trunk_db.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.2.3-2m)
- append "NoSource: 0"

* Tue Dec 30 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.2.3-1m)
- Initial specfile for Momonga.
