%global         momorel 1
%global         qtver 4.8.4
%global         kdever 4.9.97
%global         kdelibsrel 1m
%global         qtdir %{_libdir}/qt4
%global         kdedir /usr
%global         ktorrent_ver 4.3.1
#global         rcver rc1
#global         prever 1

Summary:        library for KTorrent
Name:           libktorrent
Version:        1.3.1
Release:        %{?rcver:0.%{prever}.}%{momorel}m%{?dist}
License:        GPLv2
Group:          Applications/Internet
URL:            http://ktorrent.org/
Source0:        http://ktorrent.org/downloads/%{ktorrent_ver}%{?rcver:%{rcver}}/%{name}-%{version}%{?rcver:%{rcver}}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
Requires:       qt >= %{qtver}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  cmake
BuildRequires:  avahi-devel
BuildRequires:  strigi-devel >= 0.7.7
BuildRequires:  qca2-devel

%description
Libktorrent contains all the torrent downloading code, and ktorrent contains all application code and plugins.
The goal is to make libktorrent an independent library (though still closely related to ktorrent), which can be used by other applications.

%package devel
Summary: Headers and libraries for building apps that use ktorrent
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs-devel
Requires: qt-devel

%description devel
The headers and libraries used for developing applications on top of libktorrent.

%prep
%setup -q -n %{name}-%{version}%{?rcver:%{rcver}}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog
%{_kde4_libdir}/%{name}.so.*
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/%{name}.so
%{_kde4_appsdir}/cmake/modules/FindKTorrent.cmake

%changelog
* Sat Jan 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-0.1.1m)
- update to 1.3rc1

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.1.1m)
- update to 1.2rc1

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-0.20111230.1m)
- update to 1.2.0 git snapshot

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux,org>
- (1.1.3-1m)
- update to 1.1.3

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- add BuildRequires

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-2m)
- rebuild against gmp-5.0.1

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-2m)
- rebuild against qt-4.6.3-1m

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue May  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.1.1m)
- initial release for Momonga Linux
