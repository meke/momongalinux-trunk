%global momorel 9
%global only_ruby18 0

Summary:    Ruby reference manual
Name:       ruby-refm
Version:    1.9.0
Release:    %{momorel}m%{?dist}
URL:        http://redmine.ruby-lang.org/wiki/rurema
License:    see "license.rd"
Group:      Documentation
Source0: http://www.ruby-lang.org/ja/man/archive/%{name}-%{version}-dynamic.tar.bz2 
NoSource: 0
Source1:    http://i.loveruby.net/svn/rubydoc/doctree/tags/release1_9_0/refm/license.rd
Source2:    refe2
Source3:    refe2-19
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
BuildRequires: perl
Requires:   ruby

%description
Ruby reference manual, what includes command line lookup util and web server

%define top_dir %{_datadir}/%{name}

%prep
%setup -q -n %{name}-%{version}-dynamic
cp %{SOURCE1} .
cp %{SOURCE2} .
cp %{SOURCE3} .

%build
perl -p -i -e 's|TOPDIR|%{top_dir}|g' refe2
perl -p -i -e 's|TOPDIR|%{top_dir}|g' refe2-19

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{top_dir}

install -m 755 refe2 %{buildroot}%{_bindir}/refe2
install -m 755 server.rb %{buildroot}%{top_dir}/server.rb
cp -pr bitclust %{buildroot}%{top_dir}
cp -pr db-1_8_6 %{buildroot}%{top_dir}

%if ! %{only_ruby18}
	install -m 755 refe2-19 %{buildroot}%{_bindir}/refe2-19
	cp -pr db-1_9_0 %{buildroot}%{top_dir}
%endif

%clean
rm --preserve-root -rf %{buildroot}

%files
%defattr(-,root,root)
%doc readme.html license.rd
%{_bindir}/*
%{top_dir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-7m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-6m)
- change URL

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-2m)
- %%NoSource -> NoSource

* Tue Jan  2 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.0-1m)
- initial commit
