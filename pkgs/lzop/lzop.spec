%global momorel 2

Summary: LZO fast file compressor
Name: lzop

Version: 1.03
Release: %{momorel}m%{?dist}
Source0: http://www.lzop.org/download/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://www.lzop.org
License: GPL
Group: Applications/Archiving
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: lzo-devel >= 2.01

%description
lzop is a file compressor similar to gzip. Its main advantages over gzip
are much higher compression and decompression speed at the cost
of compression ratio.

lzop was designed with the following goals in mind:
  1) speed (both compression and decompression)
  2) reasonable drop-in compatibility to gzip
  3) portability

%prep
%setup -q

%build
%configure \
    --prefix=/usr \
    --program-transform-name=""
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README COPYING AUTHORS NEWS THANKS doc/lzop.ps doc/lzop.html
%{_bindir}/lzop
%{_mandir}/man1/lzop.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-2m)
- rebuild for new GCC 4.6

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-0.1.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.02-0.1.7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-0.1.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-0.1.5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-0.1.4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.02-0.1.3m)
- %%NoSource -> NoSource

* Thu Oct 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.02-0.1.2m)
- good-bye autoconf

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.02-0.1.1m)
- update to 1.02rc1

* Thu Jul 21 2005 Toru Hoshina <t@momonga-linux.org>
- (1.01-3m)
- rebuild against lzo-2.01

* Sat Jun 11 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.01-2m)
  rebuild against lzo-2.00

* Fri Nov 14 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.01-1m)
- First Relese
