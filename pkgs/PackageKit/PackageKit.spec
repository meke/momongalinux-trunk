%global momorel 2

%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}


Summary:   Package management service
Name:      PackageKit
Version:   0.9.2
Release: %{momorel}m%{?dist}
License:   GPLv2+ and LGPLv2+
Group:     System Environment/Libraries
URL:       http://www.packagekit.org
Source0:   http://www.freedesktop.org/software/%{name}/releases/%{name}-%{version}.tar.xz
NoSource: 0

# Fedora-specific: set Vendor.conf up for Fedora.
Patch0:    PackageKit-0.3.8-Fedora-Vendor.conf.patch

# Fedora specific: the yum backend doesn't do time estimation correctly
Patch1:    PackageKit-0.4.4-Fedora-turn-off-time.conf.patch

# Upstreamable?  allow use of xulrunner2 for browser-plugin support
Patch4: PackageKit-0.7.4-xulrunner2.patch

Requires: %{name}-glib%{?_isa} = %{version}-%{release}
Requires: PackageKit-backend
Requires: shared-mime-info
Requires: comps-extras
Requires: systemd

# required by patch4
BuildRequires: automake gtk-doc libtool
BuildRequires: glib2-devel >= 2.40.0
BuildRequires: dbus-devel  >= 1.1.1
BuildRequires: dbus-glib-devel >= 0.74
BuildRequires: pam-devel
BuildRequires: libX11-devel
BuildRequires: xmlto
BuildRequires: sqlite-devel
BuildRequires: NetworkManager-devel
BuildRequires: polkit-devel >= 0.92
BuildRequires: libtool
BuildRequires: gtk2-devel
BuildRequires: gtk3-devel
BuildRequires: docbook-utils
BuildRequires: gnome-doc-utils
BuildRequires: python-devel
BuildRequires: perl(XML::Parser)
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: libgudev1-devel
BuildRequires: xulrunner-devel
BuildRequires: libarchive-devel
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: cppunit-devel
BuildRequires: pango-devel
BuildRequires: pm-utils-devel
BuildRequires: fontconfig-devel
BuildRequires: systemd-devel
BuildRequires: gobject-introspection-devel

# functionality moved to udev itself
Obsoletes: PackageKit-udev-helper < %{version}-%{release}
Obsoletes: udev-packagekit < %{version}-%{release}

# No more GTK+-2 plugin
Obsoletes: PackageKit-gtk-module < %{version}-%{release}

Obsoletes: PackageKit-filesystem-devel

# No more zif or device-rebind
Obsoletes: PackageKit-zif
Obsoletes: PackageKit-device-rebind

%description
PackageKit is a D-Bus abstraction layer that allows the session user
to manage packages in a secure way using a cross-distro,
cross-architecture API.

%package yum
Summary: PackageKit YUM backend
Group: System Environment/Libraries
Requires: yum >= 3.2.19
# python(gio)
Requires: pygobject2
Requires: %{name}%{?_isa} = %{version}-%{release}
Provides: PackageKit-backend

%description yum
A backend for PackageKit to enable yum functionality.

%package smart
Summary: PackageKit SMART backend
Group: System Environment/Libraries
Requires: smart
Requires: %{name}%{?_isa} = %{version}-%{release}

%description smart
A backend for PackageKit to enable SMART functionality.

%package docs
Summary: Documentation for PackageKit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description docs
API docs for PackageKit.

%package yum-plugin
Summary: Tell PackageKit to check for updates when yum exits
Group: System Environment/Base
Requires: yum >= 3.0
Requires: PackageKit
Requires: dbus-python
Obsoletes: yum-packagekit < %{version}-%{release}

%description yum-plugin
PackageKit-yum-plugin tells PackageKit to check for updates when yum exits.
This way, if you run 'yum update' and install all available updates, puplet
will almost instantly update itself to reflect this.

%package glib
Summary: GLib libraries for accessing PackageKit
Group: Development/Libraries
Requires: dbus >= 1.1.1
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: gobject-introspection
Obsoletes: PackageKit-libs < %{version}-%{release}
Provides: PackageKit-libs = %{version}-%{release}

%description glib
GLib libraries for accessing PackageKit.

%package cron
Summary: Cron job and related utilities for PackageKit
Group: System Environment/Base
Requires: cronie
Requires: %{name}%{?_isa} = %{version}-%{release}

%description cron
Crontab and utilities for running PackageKit as a cron job.

%package debug-install
Summary: Facility to install debugging packages using PackageKit
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
Obsoletes: PackageKit-debuginfo-install <= 0.5.2-0.1.20090902git.fc12

%description debug-install
Provides facility to install debugging packages using PackageKit.

%package glib-devel
Summary: GLib Libraries and headers for PackageKit
Group: Development/Libraries
Requires: %{name}-glib%{?_isa} = %{version}-%{release}
Requires: dbus-devel >= 1.1.1
Requires: sqlite-devel
Obsoletes: PackageKit-devel < %{version}-%{release}
Provides: PackageKit-devel = %{version}-%{release}

%description glib-devel
GLib headers and libraries for PackageKit.

%package backend-devel
Summary: Headers to compile out of tree PackageKit backends
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description backend-devel
Headers to compile out of tree PackageKit backends.

%package browser-plugin
Summary: Browser Plugin for PackageKit
Group: Development/Libraries
Requires: gtk2
Requires: %{name}-glib%{?_isa} = %{version}-%{release}
Requires: mozilla-filesystem

%description browser-plugin
The PackageKit browser plugin allows web sites to offer the ability to
users to install and update packages from configured repositories
using PackageKit.

%package gstreamer-plugin
Summary: Install GStreamer codecs using PackageKit
Group: Development/Libraries
Requires: gstreamer
Requires: %{name}-glib%{?_isa} = %{version}-%{release}
Obsoletes: codeina < 0.10.1-10
Provides: codeina = 0.10.1-10

%description gstreamer-plugin
The PackageKit GStreamer plugin allows any Gstreamer application to install
codecs from configured repositories using PackageKit.

%package gtk3-module
Summary: Install fonts automatically using PackageKit
Group: Development/Libraries
Requires: pango
Requires: %{name}-glib%{?_isa} = %{version}-%{release}

%description gtk3-module
The PackageKit GTK3+ module allows any Pango application to install
fonts from configured repositories using PackageKit.

%package command-not-found
Summary: Ask the user to install command line programs automatically
Group: Development/Libraries
Requires: bash
Requires: %{name}-glib%{?_isa} = %{version}-%{release}

%description command-not-found
A simple helper that offers to install new packages on the command line
using PackageKit.

###%package device-rebind
###Summary: Device rebind functionality for PackageKit
###Group: Development/Libraries
###Requires: %{name}-glib%{?_isa} = %{version}-%{release}

###%description device-rebind
###The device rebind functionality offer the ability to re-initialize devices
###after firmware has been installed by PackageKit. This removes the need for the
###user to restart the computer or remove and re-insert the device.

%prep
%setup -q
#%patch0 -p1 -b .fedora
#%patch1 -p1 -b .no-time
%patch4 -p1 -b .xulrunner2
NOCONFIGURE=1 ./autogen.sh

%build
%configure \
        --disable-static \
        --enable-yum \
	--disable-connman \
        --enable-smart \
        --enable-introspection \
        --with-default-backend=yum \
        --disable-local \
        --disable-strict \
        --disable-tests

%make  V=1

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/libpackagekit*.la
rm -f %{buildroot}%{_libdir}/packagekit-backend/*.la
rm -f %{buildroot}%{_libdir}/packagekit-plugins*/*.la
rm -f %{buildroot}%{_libdir}/mozilla/plugins/packagekit-plugin.la
rm -f %{buildroot}%{_libdir}/gtk-2.0/modules/*.la
rm -f %{buildroot}%{_libdir}/gtk-3.0/modules/*.la
rm -f %{buildroot}%{_libdir}/polkit-1/extensions/libpackagekit-action-lookup.la

touch %{buildroot}%{_localstatedir}/cache/PackageKit/groups.sqlite

# create a link that GStreamer will recognise
pushd ${RPM_BUILD_ROOT}%{_libexecdir} > /dev/null
ln -s pk-gstreamer-install gst-install-plugins-helper
popd > /dev/null

# create a link that from the comps icons to PK, as PackageKit frontends
# cannot add /usr/share/pixmaps/comps to the icon search path as some distros
# do not use comps. Patching this in the frontend is not a good idea, as there
# are multiple frontends in multiple programming languages.
pushd ${RPM_BUILD_ROOT}%{_datadir}/PackageKit > /dev/null
ln -s ../pixmaps/comps icons
popd > /dev/null

%find_lang %name

%post
update-mime-database %{_datadir}/mime &> /dev/null || :
systemctl enable packagekit-offline-update.service &> /dev/null || :
%systemd_post packagekit.service 

%postun
update-mime-database %{_datadir}/mime &> /dev/null || :
%systemd_preun packagekit.service

%post glib -p /sbin/ldconfig

%postun glib -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README AUTHORS NEWS COPYING
%dir %{_datadir}/PackageKit
%dir %{_datadir}/PackageKit/helpers
%dir %{_sysconfdir}/PackageKit
%dir %{_sysconfdir}/PackageKit/events
%dir %{_sysconfdir}/PackageKit/events/post-transaction.d
%dir %{_sysconfdir}/PackageKit/events/pre-transaction.d
%{_sysconfdir}/PackageKit/events/*.d/README
%dir %{_localstatedir}/lib/PackageKit
%dir %{python_sitelib}/packagekit
%dir %{_localstatedir}/cache/PackageKit
%ghost %verify(not md5 size mtime) %{_localstatedir}/cache/PackageKit/groups.sqlite
%dir %{_localstatedir}/cache/PackageKit/downloads
%{python_sitelib}/packagekit/*py*
%dir %{_libdir}/packagekit-backend
%config(noreplace) %{_sysconfdir}/PackageKit/PackageKit.conf
%config(noreplace) %{_sysconfdir}/PackageKit/Vendor.conf
%config %{_sysconfdir}/dbus-1/system.d/*
%dir %{_datadir}/PackageKit/helpers/test_spawn
%dir %{_datadir}/PackageKit/icons
%{_datadir}/bash-completion/completions/pkcon
%{_datadir}/PackageKit/helpers/test_spawn/*
%{_datadir}/man/man1/pkcon.1.*
%{_datadir}/man/man1/pkmon.1.*
##%{_datadir}/man/man1/pkgenpack.1.*
%{_datadir}/polkit-1/rules.d/*.rules
%{_datadir}/polkit-1/actions/*.policy
##%{_datadir}/mime/packages/packagekit-*.xml
%{_datadir}/PackageKit/pk-upgrade-distro.sh
%{_libexecdir}/packagekitd
%{_libexecdir}/pk-trigger-offline-update
%{_bindir}/pkmon
%{_bindir}/pkcon
##%{_bindir}/pkgenpack
##%{_bindir}/packagekit-bugreport.sh
%exclude %{_libdir}/libpackagekit*.so.*
%{_libdir}/packagekit-backend/libpk_backend_dummy.so
%{_libdir}/packagekit-backend/libpk_backend_test_*.so
%ghost %verify(not md5 size mtime) %{_localstatedir}/lib/PackageKit/transactions.db
%{_datadir}/dbus-1/system-services/*.service
##%{_libdir}/pm-utils/sleep.d/95packagekit
%{_libdir}/packagekit-plugins-2/*.so
%{_libdir}/girepository-1.0/PackageKitPlugin-1.0.typelib
%{_datadir}/dbus-1/interfaces/*.xml
%{_unitdir}/packagekit.service
%{_unitdir}/packagekit-offline-update.service
%{_libexecdir}/pk-*offline-update
%{_libdir}/gtk-2.0/modules/libpk-gtk-module.so

%files docs
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/PackageKit

%if 0%{?rhel} == 0
%files smart
%defattr(-,root,root,-)
%{_libdir}/packagekit-backend/libpk_backend_smart.so
%dir %{_datadir}/PackageKit/helpers/smart
%{_datadir}/PackageKit/helpers/smart/*
%endif

%files yum
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/PackageKit/Yum.conf
%{_libdir}/packagekit-backend/libpk_backend_yum.so
%dir %{_datadir}/PackageKit/helpers/yum
%{_datadir}/PackageKit/helpers/yum/*

%files yum-plugin
%defattr(-, root, root)
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/refresh-packagekit.conf
/usr/lib/yum-plugins/refresh-packagekit.*

%files glib
%defattr(-,root,root,-)
%{_libdir}/*packagekit-glib2.so.*
%{_libdir}/girepository-1.0/PackageKitGlib-1.0.typelib

%files cron
%defattr(-,root,root,-)
%config %{_sysconfdir}/cron.daily/packagekit-background.cron
%config(noreplace) %{_sysconfdir}/sysconfig/packagekit-background

%files debug-install
%defattr(-,root,root,-)
%{_bindir}/pk-debuginfo-install
%{_datadir}/man/man1/pk-debuginfo-install.1.*

%files browser-plugin
%defattr(-,root,root,-)
%{_libdir}/mozilla/plugins/packagekit-plugin.so

%files gstreamer-plugin
%defattr(-,root,root,-)
%{_libexecdir}/pk-gstreamer-install
%{_libexecdir}/gst-install-plugins-helper

%files gtk3-module
%defattr(-,root,root,-)
%{_libdir}/gtk-3.0/modules/*.so
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/*.desktop

%files command-not-found
%defattr(-,root,root,-)
%{_sysconfdir}/profile.d/*
%{_libexecdir}/pk-command-not-found
%config(noreplace) %{_sysconfdir}/PackageKit/CommandNotFound.conf

##%files device-rebind
##%defattr(-,root,root,-)
##%{_sbindir}/pk-device-rebind
##%{_datadir}/man/man1/pk-device-rebind.1.*

%files glib-devel
%defattr(-,root,root,-)
%{_libdir}/libpackagekit-glib2.so
%{_libdir}/pkgconfig/packagekit-glib2.pc
%dir %{_includedir}/PackageKit
%dir %{_includedir}/PackageKit/packagekit-glib2
%{_includedir}/PackageKit/packagekit-glib*/*.h
%{_datadir}/gir-1.0/PackageKitGlib-1.0.gir
%{_datadir}/gir-1.0/PackageKitPlugin-1.0.gir

%files backend-devel
%defattr(-,root,root,-)
%dir %{_includedir}/PackageKit
%{_includedir}/PackageKit/plugin
%{_libdir}/pkgconfig/packagekit-plugin.pc

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- remove require preupgrade

* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2
- remove subpackage zif (removed at version 0.8.14)
- remove subpackage device-rbind

* Thu Jul 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9-1m)
- update to 0.8.9

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6
- PackageKit-qt is separated to another package

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-4m)
- disable connman support

* Sun Jul 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-3m)
- fix BTS #456

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- add Requires

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-1m)
- reimport from fedora

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.21-3m)
- fix build failure with glib 2.33

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.21-2m)
- rebuild against libarchive-3.0.4

* Sat Feb  4 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.21-1m)
- version down to 0.6.21

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-3m)
- delete contributed file

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-2m)
- enable to build with xulrunner-8.0 or later

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update to 0.7.1
- drop qt1 support

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0
- comment out smart gtk-module

* Thu Sep  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.18-1m)
- update to 0.6.18

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.17-1m)
- update to 0.6.17

* Sat Jul 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.16-1m)
- update to 0.6.16

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.14-2m)
- fix build with new glib2

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.14-1m)
- update to 0.6.14

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.12-3m)
- enable-introspection

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.13-2m)
- rebuild against python-2.7

* Sun Apr 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.13-1m)
- update to 0.6.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.12-3m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.12-2m)
- add gtk3-module

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.12-1m)
- update to 0.6.12

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.11-1m)
- update to 0.6.11

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.10-3m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.10-2m)
- BR zif-devel

* Sun Nov  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.10-1m)
- update to 0.6.10

* Sat Oct 16 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (0.6.8-2m)
- disable-introspection

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (0.6.8-1m)
- update to 0.6.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-6m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-5m)
- update momorel patch

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-4m)
- apply momorel patch which enable to show differences in spec files
  of Momonga.

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-3m)
- revise preupgrade

* Thu Aug 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-2m)
- remove Requires: preupgrade
-- i think preupgrade is fedora specific, so momonga should not have
   it.

* Mon Jul  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update to 0.6.6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-2m)
- rebuild against qt-4.6.3-1m

* Mon Jun 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.5-1m)
- update to 0.6.5

* Wed Jun  2 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat Mar 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- rebuild against xulrunner-1.9.2

* Sun Jan 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-2m)
- add BuildRequires

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-3m)
- Requre preupgrade package

* Sat Aug 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-2m)
- fix "System is not recognised" messages

* Sun Aug  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-1m)
- update 0.4.9

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-3m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-2m)
- rebuild against xulrunner-1.9.1

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.8-1m)
- update 0.4.8

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-2m)
- add a package filesystem-devel for smart handling of a directory

* Sun Apr 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-1m)
- update 0.4.6

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-3m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Sat Mar  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-2m)
- release %%dir %%{_sysconfdir}/bash_completion.d

* Wed Feb 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-6m)
- build with -fno-strict-aliasing when gcc44

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7-5m)
- rebuild against libarchive-2.6.0-3m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7-3m)
- rebuild against python-2.6.1

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7-2m)
- revise %%files to avoid conflicting
- package browser-plugin Requires: mozilla-filesystem

* Sun Oct 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.7-1m)
- update 0.3.7

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.5-1m)
- update 0.2.5

* Wed Jul  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-1m)
- update 0.2.3

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.12-1m)
- import from Fedora 0.1.12-9.20080502

* Thu May 02 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-9.20080502
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes rh#444612, which is a release blocker.

* Thu Apr 30 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-8.20080425
- Bodge in some of the GPG import code from master in an attempt to be able to
  install signatures for F9.
- Fixes rh#443445, which is a release blocker.

* Thu Apr 25 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-7.20080425
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes rh#443972.

* Thu Apr 24 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-6.20080424
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes rh#443913.

* Wed Apr 23 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-5.20080423
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes rh#443342, rh#443341, rh#443235, rh#443235 and rh#442166.

* Wed Apr 16 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-4.20080416git
- Urgh, actually upload the correct tarball.

* Wed Apr 16 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-3.20080416git
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes rh#439735.

* Tue Apr 15 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-2.20080415git
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes rh#442286, rh#442286 and quite a few upstream bugs.

* Sat Apr 12 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-1.20080412git
- Pull in the new snapshot from the stable PACKAGEKIT_0_1_X branch.
- Fixes that were cherry picked into this branch since 0.1.11 was released can be viewed at:
  http://gitweb.freedesktop.org/?p=packagekit.git;a=log;h=PACKAGEKIT_0_1_X

* Sat Apr  5 2008 Matthias Clasen <mclasen@redhat.com> - 0.1.11-1
- Update to 0.1.11

* Fri Mar 28 2008 Bill Nottingham <notting@redhat.com> - 0.1.10-1
- update to 0.1.10
- fix glib buildreq

* Fri Mar 28 2008 Matthias Clasen <mclasen@redhat.com> - 0.1.9-3
- Fix a directory ownership oversight

* Mon Mar 17 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.9-2
- Make PackageKit require yum-packagekit
- Resolves: rhbz#437539

* Wed Mar  5 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.9-1
- Update to latest upstream version: 0.1.9
- Enable yum2 backend, but leave old yum backend the default for now

* Thu Feb 21 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.8-1
- Update to latest upstream version: 0.1.8

* Mon Feb 18 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.7-2
- Fix the yum backend.

* Thu Feb 14 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.7-1
- Update to latest upstream version: 0.1.7

* Sat Jan 19 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.6-1
- Update to latest upstream version: 0.1.6

* Fri Dec 21 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.5-1
- Update to latest upstream version: 0.1.5
- Remove polkit.patch for PolicyKit 0.7, no longer needed

* Mon Dec 17 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.4-3
- fix rpm -V issues by ghosting data files
- Resolves: rhbz#408401

* Sun Dec  9 2007 Matthias Clasen <mclasen@redhat.com> - 0.1.4-2
- Make it build against PolicyKit 0.7

* Tue Nov 27 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.4-1
- Update to latest upstream version: 0.1.4
- Include spec file changes from hughsie to add yum-packagekit subpackage

* Sat Nov 10 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.3-1
- Update to latest upstream version: 0.1.3

* Thu Nov 01 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.2-1
- Update to latest upstream version: 0.1.2

* Fri Oct 26 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.1-5
- More issues from package review:
- Need to own all created directories
- PackageKit-devel doesn't really require sqlite-devel
- Include docs in PackageKit-libs

* Fri Oct 26 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.1-4
- use with-default-backend instead of with-backend

* Thu Oct 25 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.1-3
- Add BR: python-devel

* Wed Oct 24 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.1-2
- doc cleanups from package review

* Tue Oct 23 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.1-1
- Update to latest upstream version

* Wed Oct 17 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.0-3
- Add BR for docbook-utils

* Tue Oct 16 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.0-2
- Apply recommended fixes from package review

* Mon Oct 15 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.0-1
- Initial build (based upon spec file from Richard Hughes)
