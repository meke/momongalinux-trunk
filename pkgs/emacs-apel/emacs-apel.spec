%global		momorel 9
%define		pkg		apel
%define		pkgname		APEL

Name:		emacs-%{pkg}
Version:	10.8
Release:	%{momorel}m%{?dist}
Summary:	A Portable Emacs Library

Group:		Applications/Editors
License:	GPLv2+
URL:		http://git.chise.org/elisp/apel/
Source0:        http://git.chise.org/elisp/dist/%{pkg}/%{pkg}-%{version}.tar.gz
NoSource:	0

BuildArch:	noarch
BuildRequires:	emacs
Requires:	emacs(bin) >= %{_emacs_version}
Provides:	apel = %{version}-%{release}
Obsoletes:	apel < 10.8-1
Provides:	elisp-apel = %{version}-%{release}
Obsoletes:	elisp-apel
Obsoletes:      apel-emacs
Obsoletes:      apel-xemacs

Patch0:		APEL-CFG.patch
Patch1:		apel-10.4-missing-el.patch

%description
%{pkgname} (A Portable Emacs Library) is a library to support
to write portable Emacs Lisp programs.

%package	-n %{name}-el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Editors
Requires:	%{name} = %{version}-%{release}
Obsoletes:	apel < 10.8-1

%description	-n %{name}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q -n %{pkg}-%{version}
%patch0 -p1 -b .cfg
%patch1 -p1 -b .missing

%build


%install
rm -rf $RPM_BUILD_ROOT

make PREFIX=$RPM_BUILD_ROOT%{_prefix} \
	LISPDIR=$RPM_BUILD_ROOT%{_emacs_sitelispdir} \
	INSTALL="install -p"  install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%doc README.en ChangeLog
%lang(ja) %doc README.ja
%{_emacs_sitelispdir}/%{pkg}/*.elc
%dir %{_emacs_sitelispdir}/%{pkg}

%files -n %{name}-el
%defattr(-, root, root, -)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.8-9m)
- change primary site and download URIs

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.8-8m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.8-7m)
- rename the package name
- re-import fedora's emacs-apel

* Sat May  7 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (10.8-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.8-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.8-4m)
- rebuild against emacs-23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10.8-2m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.8-1m)
- update to 10.8

* Sat May  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-35m)
- rebuild against emacs-23.2
-- merge from T4R

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-26m)
- merge apel-emacs to elisp-apel
- kill apel-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-25m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-24m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-23m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-22m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-21m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-20m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-19m)
- rebuild against emacs-23.0.93

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-18m)
- merge the following T4R changes
- 
-- * Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (10.7-17m)
-- - rebuild against emacs-23.0.92
-- 
-- * Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (10.7-16m)
-- - rebuild against emacs-23.0.91
-- 
-- * Tue Feb  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (10.7-15m)
-- - rebuild against emacs-23.0.90
-- - merge recent trunk changes
-- 
-- * Sun Sep  2 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (10.7-8m)
-- - set emacsver to 23.0.50

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-14m)
- rebuild against rpm-4.6

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-13m)
- update to cvs snapshot (bug fix)

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-12m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-11m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-10m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-9m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (10.7-8m)
- %%NoSource -> NoSource

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.7-7m)
- fix %%changelog section

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-6m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-5m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-4m)
- rebuild against emacs-22.0.96

* Sun Mar 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-3m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.7-2m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.7-1m)
- update to 10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6-10m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6-9m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6-8m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6-7m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (10.6-6m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6-5m)
- rebuild against emacs 22.0.50

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (10.6-4m)
- use %%{sitepdir}.

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (10.6-3m)
- rebuild against emacs-21.3.50

* Tue Jul  8 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (10.6-2m)
- reyurn Source0 URL to ftp.m17n.org

* Sat Jul  5 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (10.6-1m)
- version up to 10.6
- change Source0 URL temporary.

* Fri Jun  6 2003 Kazuhiko <kazuhiko@fdiary.net>
- (10.5-1m)

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.4-2m)
- rebuild against emacs-21.3
- use %%_prefix and %%_datadir

* Tue Dec 31 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (10.4-1m)
- Version update to 10.4

* Fri Aug  9 2002 Hideo TASHIRO <tashiron@jasmiso.org>
- (10.3-21m)
- First build for momonga Linux
- Update CVS patch.  Use snapshot
  (ftp://ftp.jpl.org/pub/elisp/apel/snapshots/apel-200207221304.tar.gz)
- Add xemacs-sumo to BuildPreReq

* Tue Mar 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (10.3-20k)
- rebuild against emacs-21.2

* Thu Oct 25 2001 Hidetomo Machi <mcHT@kondara.org>
- (10.3-18k)
- use %emacsver again

* Tue Oct 23 2001 Kazuhiko <kazuhiko@kondara.org>
- (10.3-16k)
- rebuild against emacs-21.1 and xemacs-21.4

* Mon Oct 22 2001 Toshiro HIKITA <toshi@sodan.org>
- (10.3-14k)
- install emu modules in site-lisp dir
  avoid to depend %emacsver

* Mon Oct 22 2001 Hidetomo Machi <mcHT@kondara.org>
- (10.3-12k)
- modify spec

* Sat Oct 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (10.3-10k)
- requires common package

* Sat Oct 20 2001 Hidetomo Machi <mcHT@kondara.org>
- (10.3-8k)
- move working section to %build from %%install

* Fri Oct 19 2001 Hidetomo Machi <mcHT@kondara.org>
- (elisp-apel-10.3-6k)
- merge apel-{emacs, xemacs} into elisp-apel

* Thu Sep 13 2001 Hidetomo Machi <mcHT@kondara.org>
- (10.3-4k)
- sync with latest cvs

* Sat May 12 2001 Hidetomo Machi <mcHT@kondara.org>
- (10.3-2k)
- import to Mary
- synch with latest cvs

* Tue Apr 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (10.3-3k)
- change Source0 URI

* Sun Dec  3 2000 KIM Hyeong Cheol <kim@kondara.org>
- (10.2-11k)
- use the same name for cvs patch as before 10.2-7k

* Wed Nov 15 2000 Toshiro HIKITA <toshi@sodan.org>
- (10.2-9k)
- update cvs patch (name changed)

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (10.2-7k)
- update cvs patch (but its name is not be changed)
- modify specfile (License)

* Fri Oct 06 2000 Toshiro HIKITA <toshi@sodan.org>
- (10.2-5k)
- Fixed spec for apel-emacs

* Sat Jul  8 2000 AYUHANA Tomonori <l@kondara.org>
- (10.2-2k)
- change emacsver 20.6 to 20.7.
- add -b at %patch

* Sat Jun 10 2000 Hidetomo Machi <mcHT@kondara.org>
- modified "Requires"

* Sun Mar 26 2000 Takaaki Tabuchi <tab@kondara.org>
- change emacsver 20.5 to 20.6.
- add -q at %setup.

* Fri Mar 10 2000 Hidetomo Machi <mcHT@kondara.org>
- version 10.2

* Wed Feb 23 2000 Hidetomo Machi <mcHT@kondara.org>
- include cvs patch

* Wed Dec  8 1999 Hidetomo Hosono <h@kondara.org>
- release for emacs-20.5

* Sun Dec  5 1999 Hidetomo Hosono <h@kondara.org>
- re-format.
- Change category name.
- using filelist.

* Sat Dec  4 1999 Hidetomo Hosono <h@kondara.org>
- release for emacs, based on Hidetomo Hosono <h@kondara.org> 's one
  for mule.

* Tue Nov 23 1999 Hidetomo Hosono <h@kondara.org>
- release for mule, based on Toru Hoshina <t@kondara.org> 's one for xemacs.
  (the format is depended on my favorite style ;^) )
