%global momorel 1
%global srcver 4.7.1

%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global sourcedir stable/phonon/%{name}/%{version}

Summary: Gstreamer phonon backend 
Name: phonon-backend-gstreamer
Version: %{srcver}
Release: %{momorel}m%{?dist}
Epoch: 2
Group: System Environment/Libraries
License: LGPLv2+
URL: http://phonon.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: automoc4
BuildRequires: cmake
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: phonon-devel >= %{version}
Requires: gstreamer-plugins-good
# not *strictly* required, but strongly recommended by upstream when built
# with USE_INSTALL_PLUGIN
Requires: PackageKit-gstreamer-plugin
Requires: phonon >= %{version}
Requires: qt >= %{qtver}

%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
  -DUSE_INSTALL_PLUGIN:BOOL=ON \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null ||:
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null ||:

%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/kde4/plugins/phonon_backend/phonon_gstreamer.so
%{_datadir}/kde4/services/phononbackends/gstreamer.desktop
%{_datadir}/icons/hicolor/*/apps/phonon-gstreamer.*

%changelog
* Sat Dec  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.7.1-1m)
- update to 4.7.1

* Tue Nov  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.7.0-1m)
- update to 4.7.0

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.6.3-1m)
- update to 4.6.3

* Mon Aug 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.6.2-1m)
- update to 4.6.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.6.0-1m)
- update to 4.6.0

* Wed Dec 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.5.90-0.2011228.1m)
- update to 4.5.90 (git 20111228 snapshot)

* Wed Aug 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.5.1-1m)
- update to 4.5.1

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2:4.5.0-1m)
- update to 4.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2:4.4.4-3m)
- rebuild for new GCC 4.6

* Sun Jan 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2:4.4.4-2m)
- keep Epoch to enable upgrading

* Sat Jan 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- import Fedora devel

* Fri Jan 21 2011 Rex Dieter <rdieter@fedoraproject.org> 4.4.4-1
- phonon-backend-gstreamer-4.4.4

* Fri Jan 07 2011 Rex Dieter <rdieter@fedoraproject.org> - 2:4.4.4-0.4.20110104
- Requires: PackageKit-gstreamer-plugin , avoids potential crashers elsewhere 
  when built with USE_INSTALL_PLUGIN (kde#262308)

* Wed Jan 05 2011 Rex Dieter <rdieter@fedoraproject.org> - 2:4.4.4-0.3.20110104
- %%doc COPYING.LIB
- add comment on pnonon-gstreamer_snapshot.sh usage

* Wed Jan 05 2011 Rex Dieter <rdieter@fedoraproject.org> - 2:4.4.4-0.2.20110104
- phonon-backend-gstreamer

* Tue Jan 04 2011 Rex Dieter <rdieter@fedoraproject.org> - 4.4.4-0.1.20110104
- phonon-gstreamer-4.4.4-20110104 snapshot

