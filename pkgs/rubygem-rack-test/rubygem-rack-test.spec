%global momorel 1

# Generated from rack-test-0.6.1.gem by gem2rpm -*- rpm-spec -*-
%global gemname rack-test

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Simple testing API built on Rack
Name: rubygem-%{gemname}
Version: 0.6.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/brynary/rack-test
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(rack) >= 1.0
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Rack::Test is a small, simple testing API for Rack apps. It can be used on its
own or as a reusable starting point for Web frameworks and testing libraries
to build on. Most of its initial functionality is an extraction of Merb 1.0's
request helpers feature.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/
rm -rf %{buildroot}%{geminstdir}/.document
rm -rf %{buildroot}%{geminstdir}/.gitignore

%files
%dir %{geminstdir}
%{geminstdir}/Gemfile
%{geminstdir}/Gemfile.lock
%{geminstdir}/Rakefile
%{geminstdir}/Thorfile
%{geminstdir}/rack-test.gemspec
%{geminstdir}/lib
%{geminstdir}/spec
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/MIT-LICENSE.txt


%changelog
* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update 0.6.2

* Thu Sep  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m) 
- update 0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.6-1m)
- update 0.5.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.4-1m)
- Initial package for Momonga Linux
