%global momorel 1

Name: soundconverter
Version: 2.0.4
Release: %{momorel}m%{?dist}
Summary: sound converter

Group: Applications/Multimedia
License: GPLv3
URL: http://soundconverter.org/
Source0: https://launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: python
BuildRequires: pygtk2-devel
BuildRequires: gnome-python2-devel
BuildRequires: gnome-python2-gnome

%description
A simple sound converter application for the GNOME environment. It reads 
anything the GStreamer library can read, and writes WAV, FLAC, MP3, and 
Ogg Vorbis files.

%prep
%setup -q

%build
%configure --disable-dependency-tracking --libdir=%{_prefix}/lib
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog NEWS TODO
%{_bindir}/%{name}
%{_prefix}/lib/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/locale/*/*/*
%{_mandir}/man1/%{name}.1.*

%changelog
* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Wed Aug 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.4-3m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.4-2m)
- rebuild for new GCC 4.6

* Sun Apr 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-5m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4
- delete ja.po

* Fri Apr 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3
- update ja.po

* Wed Jan 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- initial build
