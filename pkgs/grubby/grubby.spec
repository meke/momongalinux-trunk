%global momorel 1

Name: grubby
Version: 8.35
Release: %{momorel}m%{?dist}
Summary: Command line tool for updating bootloader configs
Group: System Environment/Base
License: GPLv2+
URL: http://git.fedorahosted.org/git/grubby.git
# we only pull git snaps at the moment
# git clone git://git.fedorahosted.org/git/grubby.git
# git archive --format=tar --prefix=grubby-%{version}/ HEAD |bzip2 > grubby-%{version}.tar.bz2
Source0: %{name}-%{version}.tar.bz2
Patch0:  grubby-8.25-no_lang.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig glib2-devel popt-devel 
BuildRequires: libblkid-devel
# for make test / getopt:
BuildRequires: util-linux-ng
%ifarch s390 s390x
Requires: s390utils-base
%endif

%description
grubby  is  a command line tool for updating and displaying information about 
the configuration files for the grub, lilo, elilo (ia64),  yaboot (powerpc)  
and zipl (s390) boot loaders. It is primarily designed to be used from scripts
which install new kernels and need to find information about the current boot 
environment.

%prep
%setup -q

%patch0 -p1 -b .no_lang

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT mandir=%{_mandir}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING
/sbin/installkernel
/sbin/new-kernel-pkg
/sbin/grubby
%{_mandir}/man8/*.8*


%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8.35-1m)
- update 8.35

* Wed May  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.25-1m)
- update 8.25

* Thu Aug  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.15-1m)
- update 8.15

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.8-3m)
- rebuild for glib 2.33.2

* Sun Feb 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.8-2m)
- remove language option

* Mon Dec 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.8-1m)
- update 8.8

* Mon Dec 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.3-2m)
- import fedora patch

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.3-1m)
- update to 8.3

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2-1m)
- update to 8.2

* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (8.1-1m)
- version up 8.1

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.0.16-1m)
- update 7.0.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.15-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.15-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.15-4m)
- full rebuild for mo7 release

* Sat Aug 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.0.15-3m)
- add grubby-7.0.15-no_LANG.patch

* Wed Aug 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.0.15-2m)
- add grubby-7.0.15-getuuid.patch

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.0.15-1m)
- update 7.0.15

* Fri Apr  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.13-2m)
- fix "./grubby: is a directory" error

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.13-1m)
- Initial commit Momonga Linux. import from Fedora

* Thu Feb 11 2010 Peter Jones <pjones@redhat.com> - 7.0.13-1
- Strip boot partition prefix from initrd path if present during --update.
  Related: rhbz#557922
- add host only support for local kernel compiles (airlied)

* Mon Feb 08 2010 Peter Jones <pjones@redhat.com> - 7.0.12-1
- compare rootdev using uuid instead of stat, for better btrfs support (josef)
  Resolves: rhbz#530108

* Mon Feb 08 2010 Peter Jones <pjones@redhat.com> - 7.0.11-1
- Make it possible to update the initrd without any other change.
  Related: rhbz#557922

* Fri Feb 05 2010 Peter Jones <pjones@redhat.com> - 7.0.10-1
- Make --update able to add an initramfs.
  Related: rhbz#557922

* Mon Nov 30 2009 Peter Jones <pjones@redhat.com> - 7.0.9-3
- Use s390utils-base as the s390 dep, not s390utils
  Related: rhbz#540565

* Tue Nov 24 2009 Peter Jones <pjones@redhat.com> - 7.0.9-2
- Add s390utils dep when on s390, since new-kernel-package needs it.
  Resolves: rhbz#540565

* Fri Oct 30 2009 Peter Jones <pjones@redhat.com> - 7.0.9-1
- Add support for dracut to installkernel (notting)

* Thu Oct  1 2009 Hans de Goede <hdegoede@redhat.com> - 7.0.8-1
- Stop using nash

* Fri Sep 11 2009 Hans de Goede <hdegoede@redhat.com> - 7.0.7-1
- Remove writing rd_plytheme=$theme to kernel args in dracut mode (hansg)
- Add a couple of test cases for extra initrds (rstrode)
- Allow tmplLine to be NULL in getInitrdVal (rstrode)

* Fri Sep 11 2009 Peter Jones <pjones@redhat.com> - 7.0.6-1
- Fix test case breakage from 7.0.5 (rstrode)

* Fri Sep 11 2009 Peter Jones <pjones@redhat.com> - 7.0.5-1
- Add support for plymouth as a second initrd. (rstrode)
  Resolves: rhbz#520515

* Wed Sep 09 2009 Hans de Goede <hdegoede@redhat.com> - 7.0.4-1
- Add --dracut cmdline argument for %post generation of dracut initrd

* Wed Aug 26 2009 Hans de Goede <hdegoede@redhat.com> - 7.0.3-1
- Silence error when no /etc/sysconfig/keyboard (#517187)

* Fri Aug  7 2009 Hans de Goede <hdegoede@redhat.com> - 7.0.2-1
- Add --add-dracut-args new-kernel-pkg cmdline option

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 7.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 17 2009 Jeremy Katz <katzj@redhat.com> - 7.0.1-1
- Fix blkid usage (#124246)

* Wed Jun 24 2009 Jeremy Katz <katzj@redhat.com> - 7.0-1
- BR libblkid-devel now instead of e2fsprogs-devel
- Add bits to switch to using dracut for new-kernel-pkg

* Wed Jun  3 2009 Jeremy Katz <katzj@redhat.com> - 6.0.86-2
- add instructions for checking out from git

* Tue Jun  2 2009 Jeremy Katz <katzj@redhat.com> - 6.0.86-1
- initial build after splitting out from mkinitrd

