%global momorel 1

Summary: BitTorrent client
Name: transmission
Version: 2.82
Release: %{momorel}m%{?dist}
License: see "COPYING"
Group: Applications/Internet
URL: http://www.transmissionbt.com/
Source0: http://download.m0k.org/%{name}/files/%{name}-%{version}.tar.xz
NoSource: 0
# This is intended to be merged by upstream post 1.80 release
Source1:        transmission-qt.desktop
# init script. Upstream doesn't want it because it is distro specific
Source2:        transmission-daemon-init
# Example conf file
Source3:        transmission-daemon-sysconfig
Patch0: 	%{name}-%{version}-momonga.patch
Patch1:         transmission-2.82-changeset_14150.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel >= 0.9.4
BuildRequires:  glib2-devel >= 2.32.0
BuildRequires:  gtk3-devel >= 3.2.0
BuildRequires:  libnotify-devel >= 0.4.3
BuildRequires:  libcanberra-devel
BuildRequires:  libcurl-devel >= 7.16.3
BuildRequires:  dbus-glib-devel >= 0.70
BuildRequires:  libevent-devel >= 2.0.10
BuildRequires:  desktop-file-utils
BuildRequires:  gettext intltool
BuildRequires:  qt4-devel
BuildRequires:  systemd-devel
Requires: transmission-cli
Requires: transmission-gtk

Provides: Transmission = %{version}
Obsoletes: Transmission < %{version}

%description 
Transmission is a free, lightweight BitTorrent client. It features a
simple, intuitive interface on top on an efficient, cross-platform
back-end.

Transmission is open source (MIT license) and runs on Mac OS X (Cocoa
interface), Linux/NetBSD/FreeBSD/OpenBSD (GTK+ interface) and BeOS
(native interface).

%package common
Summary:       Transmission common files
Group:         Applications/Internet
Conflicts:     transmission < %{version}

%description common
Common files for Transmission BitTorrent client sub-packages. It includes 
the web user interface, icons and transmission-remote, transmission-create, 
transmission-edit, transmission-show utilities.

%package cli
Summary:       Transmission command line implementation
Group:         Applications/Internet
Requires:      transmission-common
Provides:      transmission = %{version}-%{release}

%description cli
Command line version of Transmission BitTorrent client.

%package daemon
Summary:       Transmission daemon
Group:         Applications/Internet
Requires:      transmission-common
Requires(pre): shadow-utils
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
BuildRequires: systemd
Provides:      transmission = %{version}-%{release}

%description daemon
Transmission BitTorrent client daemon.

%package gtk
Summary:       Transmission GTK interface
Group:         Applications/Internet
Requires:      transmission-common
Obsoletes:     transmission < %{version}
Provides:      transmission = %{version}-%{release}

%description gtk
GTK graphical interface of Transmission BitTorrent client.

%package qt
Summary:       Transmission Qt interface
Group:         Applications/Internet
Requires:      transmission-common
Requires:      qt >= 4.7.2

%description qt
Qt graphical interface of Transmission BitTorrent client.

%pre daemon
getent group transmission >/dev/null || groupadd -r transmission
getent passwd transmission >/dev/null || \
useradd -r -g transmission -d /var/lib/transmission -s /sbin/nologin \
        -c "transmission daemon account" transmission
exit 0

%prep
%setup -q
#%%patch0 -p1 -b .momonga
%patch1 -p2 -R 

%build
%configure \
    --disable-static \
    --with-gtk \
    --without-wx \
    --enable-libcanberra \
    --enable-libnotify \
    --enable-daemon \
    --with-systemd-daemon

make %{?_smp_mflags}

pushd qt
qmake-qt4 qtr.pro
make %{?_smp_mflags}
popd

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_unitdir}
install -m0644 daemon/transmission-daemon.service  %{buildroot}%{_unitdir}/
mkdir -p %{buildroot}%{_sharedstatedir}/transmission
%make_install
make install INSTALL_ROOT=%{buildroot}%{_prefix} -C qt

%find_lang %{name}-gtk

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}-gtk.desktop
desktop-file-install \
                --dir=%{buildroot}%{_datadir}/applications/  \
                  qt/%{name}-qt.desktop
        

%post common
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%post gtk
/usr/bin/update-desktop-database &> /dev/null || :

%post qt
/usr/bin/update-desktop-database &> /dev/null || :

%post daemon
%systemd_post transmission-daemon.service

%preun daemon
%systemd_preun transmission-daemon.service

%postun common
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%postun daemon
%systemd_postun_with_restart transmission-daemon.service

%postun gtk
/usr/bin/update-desktop-database &> /dev/null || :

%postun qt
/usr/bin/update-desktop-database &> /dev/null || :

%posttrans common
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files

%files common 
%defattr(-, root, root, -)
%doc AUTHORS COPYING NEWS README
%{_bindir}/transmission-remote
%{_bindir}/transmission-create
%{_bindir}/transmission-edit
%{_bindir}/transmission-show
%{_datadir}/transmission/
%{_datadir}/pixmaps/*
%{_datadir}/icons/hicolor/*/apps/transmission.*
%doc %{_mandir}/man1/transmission-remote*
%doc %{_mandir}/man1/transmission-create*
%doc %{_mandir}/man1/transmission-edit*
%doc %{_mandir}/man1/transmission-show*

%files cli 
%defattr(-, root, root, -)
%{_bindir}/transmission-cli
%doc %{_mandir}/man1/transmission-cli*

%files daemon
%defattr(-, root, root, -)
%{_bindir}/transmission-daemon
%{_unitdir}/transmission-daemon.service
%attr(-,transmission, transmission)%{_sharedstatedir}/transmission/
%doc %{_mandir}/man1/transmission-daemon*

%files gtk -f %{name}-gtk.lang
%defattr(-, root, root, -)
%{_bindir}/transmission-gtk
%{_datadir}/applications/transmission-gtk.desktop
%doc %{_mandir}/man1/transmission-gtk.*

%files qt
%defattr(-, root, root, -)
%{_bindir}/transmission-qt
%{_datadir}/applications/transmission-qt.desktop
%doc %{_mandir}/man1/transmission-qt.*

%changelog
* Fri Oct 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.82-1m)
- update 2.82
- support systemd

* Fri Aug 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.61-1m)
- [SECURITY] CVE-2012-4037
- update 2.61

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.42-2m)
- rebuild for glib 2.33.2

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.42-1m)
- update 2.42

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.33-1m)
- update 2.33

* Wed Jul 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23-1m)
- update 2.23

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22-3m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-1m)
- update to 2.22 and separate subpackages
- rebuild against libevent-2.0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.04-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.92-2m)
- rebuild against openssl-1.0.0

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.92-1m)
- [SECURITY] CVE-2010-0748 CVE-2010-0749 CVE-2010-1853
- update to 1.92

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.91-1m)
- update to 1.91

* Thu Jan  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.77-1m)
- [SECURITY] CVE-2010-0012
- update to 1.77

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.75-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.75-1m)
- update to 1.75

* Sat Aug 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-2m)
- enable to build on Sinji

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-1m)
- update to 1.73

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.72-1m)
- update to 1.72

* Mon Jun  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.71-1m)
- update to 1.71

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.61-1m)
- [SECURITY] CVE-2009-1757
- update to 1.61

* Thu Apr 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-2m)
- rebuild against openssl-0.9.8k

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.50-1m)
- update to 1.50
-- enable wxGTK

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-2m)
- rebuild against rpm-4.6

* Sat Dec 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42-1m)
- update to 1.42

* Tue Nov 11 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (1.40-2m)
- updated checksum

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-2m)
- rebuild against openssl-0.9.8h-1m

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Mon May 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Mon Apr  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-2m)
- rebuild against gcc43

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Thu Feb 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sat Feb  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Sun Feb  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Fri Feb  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Jan 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Thu Jan  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon Nov 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Mon Oct 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Fri Oct 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-2m)
- rebuild against libevent-1.3e

* Fri Sep 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82
- rebuild against libevent-1.3d
- do not use %%NoSource macro

* Thu Aug 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-2m)
- rebuild against libevent-1.3c
- add Requires: libevent

* Wed Aug  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Tue Jul 10 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.1-3m)
- fix Transmission.desktop permission

* Mon Sep 18 2006 Nishio Futosho <futoshi@momonga-linux.org>
- (0.6.1-2m)
- modify desktop file
- add patch0 (bad gtk version check script) 
- to main

* Sat Jul 22 2006 Nishio Futosho <futoshi@momonga-linux.org>
- (0.6.1-1m)
- initial build
