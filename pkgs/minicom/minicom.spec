%global momorel 4

Summary: A text-based modem control and terminal emulation program
Name: minicom
Version: 2.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Communications
URL: http://alioth.debian.org/projects/minicom/
Source0: http://alioth.debian.org/frs/download.php/3195/minicom-%{version}.tar.gz
NoSource: 0
Patch1: minicom-2.4-umask.patch
Patch2: minicom-2.2-spaces.patch
Patch3: minicom-2.3-gotodir.patch
Patch4: minicom-2.4-rh.patch
Patch5: minicom-2.4-esc.patch
Patch6: minicom-2.4-staticbuf.patch
Patch7: minicom-2.4-config.patch
Patch10: minicom-2.4-ncurses.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: lockdev-devel ncurses-devel
Requires: lockdev lrzsz

%description
Minicom is a simple text-based modem control and terminal emulation
program somewhat similar to MSDOS Telix. Minicom includes a dialing
directory, full ANSI and VT100 emulation, an (external) scripting
language, and other features.

%prep
%setup -q
%patch1 -p1 -b .umask
%patch2 -p1 -b .spaces
%patch3 -p1 -b .gotodir
%patch4 -p1 -b .rh
%patch5 -p1 -b .esc
%patch6 -p1 -b .staticbuf
%patch7 -p1 -b .config

%patch10 -p1 -b .ncurses

cp -pr doc doc_
rm -f doc_/Makefile*

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root)
%doc ChangeLog AUTHORS NEWS TODO doc_/*
%{_bindir}/minicom
%{_bindir}/runscript
%{_bindir}/xminicom
%{_bindir}/ascii-xfr
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-1m)
- update to 2.4 based on Fedora 13 (2.4-1)
-- Patch10 is in momonga only

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3-6m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-4m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2m)
- update Patch7,9,10 for fuzz=0
- License: GPLv2+

* Sun Jul 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-1m)
- update 2.3
- import some patches from fc-devel (minicom-2_3-2_fc9)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-2m)
- %%NoSource -> NoSource

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2 (sync Fedora)

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-5m)
- add gcc4 patch
- Patch3: minicom-2.1-gcc4.patch

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.1-4m)
- rebuild against libtermcap and ncurses

* Sat Aug 14 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.1-3m)
- remove minicom.desktop

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1-2m)
- rebuild against ncurses 5.3.

* Tue Jun  3 2003 zunda <zunda at freeshell.org>
- (2.1-1m)
- source update
- minicom-2.00.0-build.patch replaced with minicom-2.1-build.patch
- minicom-notsjis.patch no longer needed (really?)
- minicom-2.00.0-autogen.sh.patch no longer needed because we don't have
  to run autogen.sh

* Mon Dec 20 2002 Kenta MURATA <muraken@momonga-linux.org>
- (2.00.0-9m)
- use autoconf-2.53 and autoheader-2.53

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (2.00.0-8k)
- not need ja_JP.SJIS.po

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (2.00.0-6k)
- japanese releted stuff

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- (2.00.0-4k)
- nigirisugi

* Mon Oct 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.00.0-2k)
- update 2.00.0, so unable to translate ko.po

* Sun Oct 21 2001 Toru Hoshina <t@kondara.org>
- (1.83.1-4k)
- rebuild against gettext 0.10.40.

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Sun Jun 17 2001 Toru Hoshina <toru@df-usa.com>
- (1.83.1-2k)
- security fix.

* Thu May  3 2001 Mike A. Harris <mharris@redhat.com> 1.83.1-8
- Changed minicom to disable SGID/SUID operation completely as it was
  never designed to be secure, and likely never will be. (#35613)
- Updated the format string patch I made to fix more format string abuses.
- Added Czeck cs_CZ locale translations.

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.83-8k)
- rebuild against glibc-2.2.2 and add minicom.glibc222.time.patch

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.83-6k)
- errased minicom-1.83.0-manfix.patch and modified spec file for compatibility

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- wmconfig -> desktop

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Fri Dec 17 1999 Bill Nottingham <notting@redhat.com>
- update to 1.83.0

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- update to 1.82.1
- s/sunsite/metalab

* Wed May 19 1999 Jeff Johnson <jbj@redhat.com>
- restore setgid uucp to permit minicom to lock in /var/lock (#2922).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Tue Jan 24 1999 Michael Maher <mike@redhat.com>
- fixed bug, changed groups.

* Thu Oct 01 1998 Cristian Gafton <gafton@redhat.com>
- updated to 1.82 to include i18n fixes

* Wed Sep 02 1998 Michael Maher <mike@redhat.com>
- Built package for 5.2.

* Sun May 10 1998 Cristian Gafton <gafton@redhat.com>
- security fixes (alan cox, but he forgot about the changelog)

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu May 07 1998 Cristian Gafton <gafton@redhat.com>
- BuildRoot; updated .make patch to cope with the buildroot
- fixed the spec file

* Tue May 06 1998 Michael Maher <mike@redhat.com>
- update of package (1.81)

* Wed Oct 29 1997 Otto Hammersmith <otto@redhat.com>
- added wmconfig entries

* Tue Oct 21 1997 Otto Hammersmith <otto@redhat.com>
- fixed source url

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
