%global momorel 1

Name: libatasmart
Version: 0.19
Release: %{momorel}m%{?dist}
Summary: ATA S.M.A.R.T. Disk Health Monitoring Library
Group: System Environment/Libraries
License: LGPLv2+
Url: http://git.0pointer.de/?p=libatasmart.git;a=summary
Source0: http://0pointer.de/public/libatasmart-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: systemd-devel >= 187

%description
A small and lightweight parser library for ATA S.M.A.R.T. hard disk
health monitoring.

%package devel
Summary: Development Files for libatasmart Client Development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: vala

%description devel
Development Files for libatasmart Client Development

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%prep
%setup -q

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
find $RPM_BUILD_ROOT \( -name *.a -o -name *.la \) -exec rm {} \;
rm $RPM_BUILD_ROOT%{_docdir}/libatasmart/README

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README LGPL
%{_libdir}/libatasmart.so.*
%{_sbindir}/skdump
%{_sbindir}/sktest

%files devel
%defattr(-,root,root)
%{_includedir}/atasmart.h
%{_libdir}/libatasmart.so
%{_libdir}/pkgconfig/libatasmart.pc
%{_datadir}/vala/vapi/atasmart.vapi
%doc blob-examples/SAMSUNG* blob-examples/ST* blob-examples/Maxtor* blob-examples/WDC* blob-examples/FUJITSU* blob-examples/INTEL* blob-examples/TOSHIBA*

%changelog
* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19-1m)
- update 0.19

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@mmonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0..15-1m)
- sync Fedora
 +* Fri Sep 18 2009 Lennart Poettering <lpoetter@redhat.com> 0.15-1
 +- New upstream release
 +- Fixes #515881

* Tue Jun 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12-1m)
- Initial Commit Momonga Linux

* Wed May 6 2009 Lennart Poettering <lpoetter@redhat.com> 0.12-3
- Update handling of one ST drive, add another quirk for a weird FUJITSU drive (rhbz 498284)

* Wed Apr 22 2009 Lennart Poettering <lpoetter@redhat.com> 0.12-2
- Properly handle some ST and FUJITSU drive attributes. (rhbz 496087, 497107)

* Wed Apr 15 2009 Lennart Poettering <lpoetter@redhat.com> 0.12-1
- New upstream release

* Tue Apr 14 2009 Lennart Poettering <lpoetter@redhat.com> 0.11-1
- New upstream release

* Mon Apr 13 2009 Lennart Poettering <lpoetter@redhat.com> 0.10-1
- New upstream release

* Sun Apr 12 2009 Lennart Poettering <lpoetter@redhat.com> 0.9-1
- New upstream release

* Fri Apr 10 2009 Lennart Poettering <lpoetter@redhat.com> 0.8-1
- New upstream release

* Tue Apr 7 2009 Lennart Poettering <lpoetter@redhat.com> 0.7-1
- New upstream release

* Sat Apr 5 2009 Lennart Poettering <lpoetter@redhat.com> 0.6-1
- New upstream release

* Fri Apr 3 2009 Lennart Poettering <lpoetter@redhat.com> 0.5-1
- New upstream release

* Thu Apr 2 2009 Lennart Poettering <lpoetter@redhat.com> 0.4-1
- New upstream release

* Tue Mar 24 2009 Lennart Poettering <lpoetter@redhat.com> 0.3-1
- New upstream release

* Thu Mar 19 2009 Lennart Poettering <lpoetter@redhat.com> 0.2-1
- New upstream release

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jul 25 2008 Lennart Poettering <lpoetter@redhat.com> 0.1-1
- Initial version
