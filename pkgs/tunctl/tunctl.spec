%global momorel 5

Name:           tunctl
Version:        1.5
Release:        %{momorel}m%{?dist}
Summary:        Create and remove virtual network interfaces

Group:          Applications/System
License:        GPL+
URL:            http://tunctl.sourceforge.net/
Source0:        http://downloads.sourceforge.net/tunctl/tunctl-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  docbook-utils

%description
tunctl is a tool to set up and maintain persistent TUN/TAP network
interfaces, enabling user applications access to the wire side of a
virtual nework interface. Such interfaces is useful for connecting VPN
software, virtualization, emulation and a number of other similar
applications to the network stack.

tunctl originates from the User Mode Linux project.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_mandir}/man8/tunctl.8*
%{_sbindir}/tunctl
%doc ChangeLog

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5-1m)
- import from fc devel

* Thu Jul 18 2008 Henrik Nordstrom <henrik@henriknordstrom.net> 1.5-2
- Corrected package description formatting

* Wed Jul 16 2008 Henrik Nordstrom <henrik@henriknordstrom.net> 1.5-1
- Update to version 1.5 based on separate upstream release

* Tue Mar 25 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.4-2
- Move to sbin (Marek Mahut, #434583)

* Fri Feb 22 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.4-1
- Initial packaging
