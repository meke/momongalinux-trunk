%global momorel 6

Name:           gkrellm-wifi
Version:        0.9.12
Release:        %{momorel}m%{?dist}
Summary:        Wireless monitor plugin for the GNU Krell Monitors
Group:          Applications/System
License:        GPLv2+
URL:            http://www.gkrellm.net/
# upsteam is dead, no URL
Source0:        %{name}-%{version}.tar.gz
Patch0:         %{name}-%{version}.patch
Patch1:         gkrellm-wifi-0.9.12-asm_h.patch
Patch2:         gkrellm-wifi-0.9.12-kernel-2.6.26.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gkrellm-devel >= 2.2
BuildRequires:  gkrellm
Requires:       gkrellm >= 2.2, gkrellm < 3
# Unfortunate, but nescesarry this plugin used to be (wrongly) packaged in the
# same specfile as gkrellm itself, with the wrong namae gkrellm-wireless and
# causing it to have version 2.2.9 :(
Obsoletes:      gkrellm-wireless <= 2.2.9-3
Provides:       gkrellm-wireless = 2.2.9-4
ExcludeArch:    s390 s390x

%description
Plug-in for gkrellm (a system monitor) which monitors the wireless LAN cards in
your PC and displays a graph of the link quality percentage for each card.


%prep
%setup -q
%patch0 -p1
%patch1 -p1 -z .asm_h
%patch2 -p1


%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -fPIC \
  `pkg-config gkrellm --cflags` -DG_LOG_DOMAIN=\\\"gkrellm-wifi\\\""


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gkrellm2/plugins
install -m 755 %{name}.so $RPM_BUILD_ROOT%{_libdir}/gkrellm2/plugins


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README THEMING TODO
%{_libdir}/gkrellm2/plugins/%{name}.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.12-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.12-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.12-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.12-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.12-2m)
- rebuild against rpm-4.6

* Mon Jun 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.12-1m)
- import from Fedora to Momonga

* Sat May 31 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-8
- Fix compiling with mix of latest glibc + kernel headers (avoid header
  conflict)

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.12-7
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-6
- Fix Source0 URL

* Tue Aug  7 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-5
- Update License tag for new Licensing Guidelines compliance

* Mon Feb 19 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-4
- Add #include <asm/types.h> to fix compile with recent kernel-headers

* Mon Aug 28 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-3
- FE6 Rebuild

* Mon Jul 17 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-2
- Use pristine upstream source and put changes found in the Core package
  tarbal in a patch
- Require gkrellm >= 2.2, gkrellm < 3
- Add ExcludeArch: s390 s390x

* Fri Jul  7 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 0.9.12-1
- Initial Fedora Extras Package
