%global momorel 4

Name:           telepathy-filesystem
Version:        0.0.2
Release:        %{momorel}m%{?dist}
Summary:        Telepathy filesystem layout

Group:          System Environment/Base
License:        Public Domain
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
Requires:       filesystem


%description
This package provides some directories which are required by other
packages which comprise the Telepathy release.  

%prep


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_datadir}/telepathy/managers
mkdir -p $RPM_BUILD_ROOT%{_datadir}/telepathy/clients
mkdir -p $RPM_BUILD_ROOT%{_includedir}/telepathy-1.0


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%dir %{_datadir}/telepathy
%dir %{_datadir}/telepathy/managers
%dir %{_datadir}/telepathy/clients
%dir %{_includedir}/telepathy-1.0


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.2-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.2-1m)
- update to 0.0.2
- sync with Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-2m)
- rebuild against rpm-4.6

* Tue May 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.1-1m)
- import from Fedora

* Tue Jun  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.0.1-2
- Add telepathy-1.0 dir.

* Thu Sep 21 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.0.1-1
- Initial FE spec.

