%global momorel 1

%define long_hash  04fd09cfa88718838e02f4419befc1a0dd4b5a0e
%define short_hash 04fd09cfa

Name:		fprintd
Version:	0.5.1
Release:	%{momorel}m%{?dist}
Summary:	D-Bus service for Fingerprint reader access

Group:		System Environment/Daemons
License:	GPLv2+

Url:		http://www.reactivated.net/fprint/wiki/Fprintd
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:	http://people.freedesktop.org/~hadess/fprintd-%{version}.tar.bz2
NoSource:	0
Patch0:         fprintd-0.5.0-pod2man.patch
BuildRequires:	dbus-glib-devel
BuildRequires:	pam-devel
BuildRequires:	libfprint-devel >= 0.5.0
BuildRequires:	PolicyKit-devel
BuildRequires:	gtk-doc
BuildRequires:	perl(XML::Parser) intltool
BuildRequires:  systemd-units

%description
D-Bus service to access fingerprint readers.

%package pam
Summary:	PAM module for fingerprint authentication
Requires:	%{name} = %{version}-%{release}
# Note that we obsolete pam_fprint, but as the configuration
# is different, it will be mentioned in the release notes
Provides:	pam_fprint = %{version}-%{release}
Obsoletes:	pam_fprint < 0.2-3

Group:		System Environment/Base
License:	GPLv2+

%description pam
PAM module that uses the fprintd D-Bus service for fingerprint
authentication.

%package devel
Summary:	Development files for %{name}
Requires:	%{name} = %{version}-%{release}
Requires:	gtk-doc
Group:		Development/Libraries
License:	GPLv2+

%description devel
Development documentation for fprintd, the D-Bus service for
fingerprint readers access.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1

%build
%configure --libdir=/%{_lib}/ --disable-gtk-doc --enable-pam 

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_localstatedir}/lib/fprint

rm -f  $RPM_BUILD_ROOT/%{_lib}/security/*.{a,la}

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :

%preun
if [ $1 = 0 ]; then
  /bin/systemctl disable fprintd.service > /dev/null 2>&1 || :
  /bin/systemctl stop fprintd.service > /dev/null 2>&1 || :
fi

%postun
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README COPYING AUTHORS TODO
%{_unitdir}/fprintd.service
%{_bindir}/fprintd-*
%{_libexecdir}/fprintd
# FIXME This file should be marked as config when it does something useful
%{_sysconfdir}/fprintd.conf
%{_sysconfdir}/dbus-1/system.d/net.reactivated.Fprint.conf
%{_datadir}/dbus-1/system-services/net.reactivated.Fprint.service
%{_datadir}/polkit-1/actions/net.reactivated.fprint.device.policy
%{_localstatedir}/lib/fprint
%{_mandir}/man1/fprintd.1.*

%files pam
%defattr(-,root,root,-)
%doc pam/README
/%{_lib}/security/pam_fprintd.so

%files devel
%defattr(-,root,root,-)
#%%{_datadir}/gtk-doc/html/fprintd
%{_datadir}/dbus-1/interfaces/net.reactivated.Fprint.Device.xml
%{_datadir}/dbus-1/interfaces/net.reactivated.Fprint.Manager.xml

%changelog
* Fri Dec 13 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1

* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0
- support systemd

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-1m)
- update 0.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.1.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.1.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-0.1.6m)
- full rebuild for mo7 release

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-0.1.5m)
- rebuild against libfprint-0.1.0-0.pre2

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1-0.1.4m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.1.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.1.2m)
- change Release

* Sat Aug 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1-0.git04fd09cfa.1m)
- import from Fedora

* Thu May 07 2009 Bastien Nocera <bnocera@redhat.com> 0.1-9.git04fd09cfa
- Add /var/lib/fprint to the RPM to avoid SELinux errors (#499513)

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-8.git04fd09cfa
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 27 2009 - Bastien Nocera <bnocera@redhat.com> - 0.1-7.git04fd09cfa
- Add a patch to handle device disconnects

* Mon Jan 26 2009 - Bastien Nocera <bnocera@redhat.com> - 0.1-6.git04fd09cfa
- Update to latest git, fixes some run-time warnings

* Wed Dec 17 2008 - Bastien Nocera <bnocera@redhat.com> - 0.1-5.git43fe72a2aa
- Add patch to stop leaking a D-Bus connection on failure

* Tue Dec 09 2008 - Bastien Nocera <bnocera@redhat.com> - 0.1-4.git43fe72a2aa
- Update D-Bus config file for recent D-Bus changes

* Thu Dec 04 2008 - Bastien Nocera <bnocera@redhat.com> - 0.1-3.git43fe72a2aa
- Update following comments in the review

* Sun Nov 23 2008 - Bastien Nocera <bnocera@redhat.com> - 0.1-2.gitaf42ec70f3
- Update to current git master, and add documentation

* Tue Nov 04 2008 - Bastien Nocera <bnocera@redhat.com> - 0.1-1
- First package

