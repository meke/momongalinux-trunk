%global         momorel 9
%global		dictname wn
Summary:	WordNet lexical reference system formatted as dictionary for dictd
Name:		dict-wn
Version:	2.0
Release:        %{momorel}m%{?dist}
License:	MIT/X
Group:		Applications/Text
Source0:	ftp://ftp.dict.org/pub/dict/pre/%{name}-%{version}-pre.tar.gz
# original is	ftp://ftp.cogsci.princeton.edu/pub/wordnet/%{version}/WordNet-%{version}.tar.gz
Source1:	http://www.cogsci.princeton.edu/%{version}/README
Source2:	http://www.cogsci.princeton.edu/%{version}/LICENSE
Source3:	http://www.cogsci.princeton.edu/%{version}/CHANGES
URL:		http://www.cogsci.princeton.edu/~wn/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#BuildRequires:	dictzip
BuildArch:	noarch
Requires:	dictd

%description
This package contains WordNet (r) Lexical Database formatted for
use by the dictionary server in the dictd package.

%prep
%setup -q -c
cp %{SOURCE1} .
cp %{SOURCE2} .
cp %{SOURCE3} .

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_sysconfdir}/dictd
install -d %{buildroot}%{_datadir}/dictd

cp wn.dict.dz wn.index %{buildroot}%{_datadir}/dictd/

dictprefix=%{_datadir}/dictd/wn
echo "# WordNet Lexical Database dictionary
database %{dictname} {
    data  \"$dictprefix.dict.dz\"
    index \"$dictprefix.index\"
}" > %{buildroot}%{_sysconfdir}/dictd/%{dictname}.dictconf

%clean
rm -rf %{buildroot}

%postun
if [ -f /var/lock/subsys/dictd ]; then
	/sbin/service dictd restart 1>&2 || true
fi

%post
if [ -f /var/lock/subsys/dictd ]; then
	/sbin/service dictd restart 1>&2
fi

%files
%defattr(644,root,root,755)
%attr(640,root,root) %config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/dictd/%{dictname}.dictconf
%{_datadir}/dictd/%{dictname}*
%doc README LICENSE CHANGES

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-9m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-3m)
- rebuild against gcc43

* Wed Jan 19 2005 TAKAHASHI Tamotsu <tamo>
- (2.0-2m)
- add doc files
- correct URL
- License: MIT/X ("The name of Princeton University
 or Princeton may not be used...")

* Fri Dec 12 2003 TAKAHASHI Tamotsu <tamo>
- (2.0-1m)
- update
- License: Modified BSD
 ( http://www.cogsci.princeton.edu/~wn/license.shtml
   Is this MIT/X? Anyway, commercial use is permitted.
   Note that authors want you to mail to wordnet@princeton.edu )

* Sun Apr 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.7a-1m)
- use /sbin/service instead of /etc/init.d/dictd

* Thu Oct 17 2002 Kikutani Makoto <poo@momonga-linux.org>
- (1.7a-1m)
- New upstream version

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-5m)
- remove Requires: %{_sysconfdir}/dictd

* Tue Jul 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-4m)
- BuildArch: noarch (please remove dict-wn-1.5-3m.i586.rpm file manually)
- spec file cleanup

* Mon Jul 15 2002 Kikutani Makoto <poo@momonga-linux.org>
- (1.5-3m)
- 1st Momonga version
- I made this based on PLD Linux's spec
