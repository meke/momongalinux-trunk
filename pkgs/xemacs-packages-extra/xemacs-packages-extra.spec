%global momorel 1
%global pkgdir  %{_datadir}/xemacs
%global xemver  %(pkg-config --modversion xemacs 2>/dev/null || echo 0)

Name:           xemacs-packages-extra
Version:        20110502
Release:        %{momorel}m%{?dist}
Summary:        Collection of XEmacs lisp packages

Group:          Applications/Editors
License:        GPLv2+ and GPLv3+
URL:            http://www.xemacs.org/Documentation/packageGuide.html
# Tarball created with Source99
Source0:        %{name}-%{version}.tar.xz
Source10:       Emacs.ad.ja_JP.eucJP
Source11:       Emacs.ad.ko_KR.eucKR
Source12:       Emacs.ad.zh_CN.GB2312
Source13:       Emacs.ad.zh_TW.Big5
Source14:       Emacs.ad.ja_JP.UTF-8
Source15:       Emacs.ad.ko_KR.UTF-8
Source16:       Emacs.ad.zh_CN.UTF-8
Source17:       Emacs.ad.zh_TW.UTF-8
Source99:       %{name}-checkout.sh

# adapt ispell.el to aspell >= 0.60's encoding behaviour, #190151
Patch0:         %{name}-20060510-aspellenc-190151.patch
# use TrAX by default in xslt-process
Patch1:         %{name}-20060510-trax.patch
# catch harmless errors in mouse-avoidance-too-close-p (avoid.el)
Patch2:         %{name}-20060510-avoid-catch-error-65346.patch
# make egg-wnn use unix domain sockets by default
Patch3:         %{name}-20060510-egg-wnn-host-unix-79826.patch
# fix keywords inadvertently expanded by XEmacs CVS, failing auctex build
Patch4:         %{name}-20090217-auctex-cvs-keywords.patch
# use ptex rather than jtex by default for Japanese
Patch5:         %{name}-20090217-auctex-texjp-platex.patch
# update browsers in psgml-html
Patch6:         %{name}-20090217-browsers.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  xemacs >= 21.5.29-11
BuildRequires:  texinfo
# For building auctex docs
BuildRequires:  texlive-texmf-latex
BuildRequires:  info
BuildRequires:  %{__python}
BuildRequires:  java-devel
# xz for unpacking Source0
BuildRequires:  xz
Requires:       xemacs-packages-base = %{version}
Requires:       xemacs(bin) >= %{xemver}
# Fake release here for historical reasons
Provides:       apel-xemacs = 10.7-1

%description
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

The XEmacs packages collection contains a large collection of useful
lisp packages for XEmacs including mail readers, programming modes and
utilities, and packages related to using XEmacs in multi-lingual
environments.

%package        el
Summary:        Emacs lisp source files for XEmacs packages collection
Group:          Development/Libraries
Requires:       xemacs-packages-extra = %{version}-%{release}
Requires:       xemacs-packages-base-el = %{version}

%description    el
This package is not needed to run XEmacs; it contains the lisp source
files for the XEmacs packages collection, mainly of interest when
developing or debugging the packages.

%package        info
Summary:        XEmacs packages documentation in GNU texinfo format
Group:          Documentation
Requires:       xemacs-packages-extra = %{version}-%{release}

%description    info
This package contains optional documentation for the XEmacs packages
collection in GNU texinfo format


%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1

chmod -c -x \
    xemacs-packages/auctex/style/babel.el \
    xemacs-packages/ede/ede-proj-comp.el \
    xemacs-packages/vm/lisp/vm-version.el

cat << EOF > make.sh
#!/bin/sh
make \\
    XEMACS_BINARY=%{_bindir}/xemacs \\
    XEMACS_INSTALLED_PACKAGES_ROOT=\$RPM_BUILD_ROOT%{pkgdir} \\
    XEMACS_21_5=t \\
    "\$@"
EOF
chmod +x make.sh

sed -i -e 's|/usr/local/bin/perl5\?|/usr/bin/perl|g' \
  xemacs-packages/bbdb/utils/*.pl xemacs-packages/hyperbole/file-newer


%build
cd xemacs-packages/xslt-process/java
javac xslt/TrAX.java && jar cvf xslt.jar xslt/*.class && rm xslt/*.class
cd -
./make.sh autoloads
./make.sh


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{pkgdir}
./make.sh install

# The X-Symbol font files are only needed on Windows
rm -fr $RPM_BUILD_ROOT%{pkgdir}/xemacs-packages/etc/x-symbol/fonts
rm -fr $RPM_BUILD_ROOT%{pkgdir}/xemacs-packages/etc/x-symbol/origfonts
rm -fr $RPM_BUILD_ROOT%{pkgdir}/xemacs-packages/etc/x-symbol/pcf

cd $RPM_BUILD_ROOT%{pkgdir}/mule-packages/etc/app-defaults
mkdir {de_DE,fr_FR,ro_RO,ja_JP,ko_KR,sv_SE,zh_CN,zh_TW}.UTF-8 \
  ja_JP.eucJP ko_KR.eucKR zh_CN.GB2312 zh_TW.Big5
iconv -f ISO-8859-1  -t UTF-8 de/Emacs > de_DE.UTF-8/Emacs
iconv -f ISO-8859-1  -t UTF-8 fr/Emacs > fr_FR.UTF-8/Emacs
iconv -f ISO-8859-16 -t UTF-8 ro/Emacs > ro_RO.UTF-8/Emacs
iconv -f ISO-8859-1  -t UTF-8 sv/Emacs > sv_SE.UTF-8/Emacs
mv de de_DE
mv fr fr_FR
mv ro ro_RO
mv sv sv_SE
install -pm 644 %{SOURCE10} ja_JP.eucJP/Emacs
install -pm 644 %{SOURCE11} ko_KR.eucKR/Emacs
install -pm 644 %{SOURCE12} zh_CN.GB2312/Emacs
install -pm 644 %{SOURCE13} zh_TW.Big5/Emacs
install -pm 644 %{SOURCE14} ja_JP.UTF-8/Emacs
install -pm 644 %{SOURCE15} ko_KR.UTF-8/Emacs
install -pm 644 %{SOURCE16} zh_CN.UTF-8/Emacs
install -pm 644 %{SOURCE17} zh_TW.UTF-8/Emacs
# these don't seem to appear in manifest
#ln -s ja_JP.eucJP ja_JP
#ln -s ko_KR.eucKR ko_KR
#ln -s zh_CN.GB2312 zh_CN
#ln -s zh_TW.Big5 zh_TW
cd -

remove_package() {
    pdir=$RPM_BUILD_ROOT%{pkgdir}/$2
    %{_bindir}/xemacs -batch -vanilla -l package-admin -eval \
        "(package-admin-delete-binary-package '$1 \"$pdir\")"
    rm -rf $pdir/{etc,lisp,man}/$1
}

# efs, xemacs-base: included in xemacs-packages-base
# mew: provided by mew-xemacs
# jde: not buildable from sources, all deps not met, see 2006-03-03 changelog
for pkg in efs jde mew xemacs-base ; do
    remove_package $pkg xemacs-packages
done

# mule-base: included in xemacs-packages-base
# skk: provided by ddskk-xemacs
for pkg in mule-base skk ; do
    remove_package $pkg mule-packages
done

# mule-ucs: not needed (and unusable) with >= 21.5
remove_package mule-ucs mule-packages

# info docs: pre-generate "dir"s and compress files
for file in $RPM_BUILD_ROOT%{pkgdir}/*-packages/info/*.info ; do
  /sbin/install-info $file `dirname $file`/dir
done
find $RPM_BUILD_ROOT%{pkgdir} -type f -name '*.info*' | xargs gzip -9

# separate files
rm -f *.files
echo "%%defattr(-,root,root,-)" > base-files
echo "%%defattr(-,root,root,-)" > el-files
echo "%%defattr(-,root,root,-)" > info-files

find $RPM_BUILD_ROOT%{pkgdir}/* \
  \( -type f -name '*.el.orig' -exec rm '{}' ';' \) -o \
  \( -type f -not -name '*.el' -fprint base-non-el.files \) -o \
  \( -type d -not -name info -fprintf dir.files "%%%%dir %%p\n" \) -o \
  \( -name '*.el' \( -exec test -e '{}'c \; -fprint el-bytecomped.files -o \
     -fprint base-el-not-bytecomped.files \) \)

sed -i -e "s|$RPM_BUILD_ROOT||" *.files
cat base-*.files dir.files | grep -v /info/ >> base-files
cat el-*.files                              >> el-files
cat base-non-el.files | grep /info/         >> info-files

sed -i -e 's/^\(.*\(\.ja\|-ja\.texi\)\)$/%lang(ja) \1/' base-files
sed -i -e 's/^\(.*[_-]ja\.info.*\)$/%lang(ja) \1/' info-files

# in case redhat-rpm-config is not installed or doesn't do this for us:
%{__python} -c "import compileall; compileall.compile_dir('$RPM_BUILD_ROOT%{pkgdir}/xemacs-packages/etc/python-modes', ddir='/', force=1)"
%{__python} -O -c "import compileall; compileall.compile_dir('$RPM_BUILD_ROOT%{pkgdir}/xemacs-packages/etc/python-modes', ddir='/', force=1)"


%clean
rm -rf $RPM_BUILD_ROOT


%files -f base-files
%{pkgdir}/xemacs-packages/etc/python-modes/*.py[co]

%files el -f el-files

%files info -f info-files
%dir %{pkgdir}/*-packages/info/


%changelog
* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110502-1m)
- import from fedora