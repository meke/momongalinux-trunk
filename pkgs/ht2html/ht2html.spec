%global momorel 7

Name:		ht2html
Version:	2.0
Release:	%{momorel}m%{?dist}
URL:		http://ht2html.sourceforge.net
Source0:	http://dl.sf.net/%{name}/%{name}-%{version}.tar.gz
Source1:	%{name}
Source2:	%{name}-LICENSE
License:	"Python"
Group:		System Environment/Libraries
Summary:	The www.python.org Web site generator
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
Requires:	python

%description
The www.python.org Web site generator.

%prep
%setup -q
cp %{SOURCE1} .
cp %{SOURCE2} ./LICENSE

%build

%install
rm -rf $RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 755 calcroot.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 755 ht2html.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 BAWGenerator.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 Banner.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 HTParser.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 IPC8Generator.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 JPyGenerator.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 JPyLocalGenerator.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 LinkFixer.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 PDOGenerator.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 SelfGenerator.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 Sidebar.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 Skeleton.py $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 StandardGenerator.py $RPM_BUILD_ROOT%{_datadir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README LICENSE doc/*.{html,png}
%{_datadir}/%{name}/*.py*
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-5m)
- full rebuild for mo7 release

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-4m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-2m)
- rebuild against rpm-4.6

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-1m)
- import from Fedora to Momonga for jython
- tmp comment out .pyc and .pyo

* Wed Sep 06 2006 Igor Foox <ifoox@redhat.com> 2.0-5
- Remove ghosting of pyo files as per new Fedora Guidelines.

* Tue Jul 25 2006 Igor Foox <ifoox@redhat.com> 2.0-4
- Fix Requires from Python to python.

* Mon Jul 24 2006 Igor Foox <ifoox@redhat.com> 2.0-3
- Remove comment to reference license and include a copy as another source file.
- Add Requires for Python.
- Add dist tag.

* Mon Jul 24 2006 Igor Foox <ifoox@redhat.com> 2.0-2
- Include comment to reference ML thread about licensing.

* Mon Jun 26 2006 Igor Foox <ifoox@redhat.com> 2.0-1
- Changed release number to numeric (1)
- Fixed license to 'Python Software Foundation License'

* Fri Jun 23 2006 Igor Foox <ifoox@redhat.com> 2.0-1jpp_2fc
- Removed BuildRequires of python-devel
- Removed Vendor and Distribution tags
- Changed Source0 to be a URL, and also changed to the file provided by 
upstream
- Changed license to Python License
- Split %%files section into seperate entries for .pyo .pyc and .py files,
%%ghosting the .pyo files

* Thu Jun 1 2006 Igor Foox <ifoox@rehdat.com> 2.0-1jpp_1fc
- Changed buildroot to what Extras expects

* Mon Nov 22 2004 Fernando Nasser <fnasser@redhat.com> 2.0-1jpp
- Import of 2.0-4mdk spec file from
  Guillaume Rousse <g.rousse@linux-mandrake.com>
  David Walluck <walluck@linux-mandrake.com>
