%global	skindir	%{_datadir}/mplayer/skins
%global mplayer_url 	http://www.mplayerhq.hu/MPlayer/skins/
%global momorel 22

### Abyss
%global src00_name	Abyss
%global src00_ver	1.7

### AlienMind
%global src01_name	AlienMind
%global src01_ver	1.2

### Blue-small
%global src02_name	Blue-small
%global src02_ver	1.5

### BlueHeart
%global src03_name	BlueHeart
%global src03_ver	1.5

### Canary
%global src04_name	Canary
%global src04_ver	1.2

### Corelian
%global src05_name	Corelian
%global src05_ver	1.1

### CornerMP
%global src06_name	CornerMP
%global src06_ver	1.2

### CornerMP-aqua
%global src07_name	CornerMP-aqua
%global src07_ver	1.4

### CubicPlayer
%global src08_name	CubicPlayer
%global src08_ver	1.1

### Cyrus
%global src09_name	Cyrus
%global src09_ver	1.2

### DVDPlayer
%global src10_name	DVDPlayer
%global	src10_ver	1.1

### Dushku
%global src11_name	Dushku
%global src11_ver	1.2

### Industrial
%global src12_name	Industrial
%global src12_ver	1.0

### JiMPlayer
%global src13_name	JiMPlayer
%global src13_ver	1.4

### KDE
%global src14_name	KDE
%global src14_ver	0.3

### MidnightLove
%global src15_name	MidnightLove
%global src15_ver	1.6

### OSX-Brushed
%global src16_name	OSX-Brushed
%global src16_ver	2.3

### OSX-Mod
%global src17_name	OSX-Mod
%global src17_ver	1.1

### Orange
%global src18_name	Orange
%global src18_ver	1.3

### PowerPlayer
%global src19_name	PowerPlayer
%global	src19_ver	1.1

### QPlayer
%global src20_name	QPlayer
%global src20_ver	1.2

### QuickSilver
%global src21_name	QuickSilver
%global src21_ver	1.0

### Terminator3
%global src22_name	Terminator3
%global src22_ver	1.1

### WMP6
%global src23_name	WMP6
%global src23_ver	2.2

### XFce4
%global src24_name	XFce4
%global src24_ver	1.0

### avifile
%global src25_name	avifile
%global src25_ver	1.6

### bluecurve
%global src26_name	bluecurve
%global src26_ver	1.3

### disappearer
%global src27_name	disappearer
%global src27_ver	1.1

### divxplayer
%global src28_name	divxplayer
%global src28_ver	1.3

### gnome
%global src29_name	gnome
%global src29_ver	1.1

### handheld
%global src30_name	handheld
%global src30_ver	1.0

### hayraphon
%global src31_name	hayraphon
%global src31_ver	1.0

### hwswskin
%global src32_name	hwswskin
%global src32_ver	1.3

### iTunes
%global src33_name	iTunes
%global src33_ver	1.1

### iTunes-mini
%global src34_name	iTunes-mini
%global src34_ver	1.1

### krystal
%global src35_name	krystal
%global src35_ver	1.1

### mentalic
%global src36_name	mentalic
%global src36_ver	1.2

### mini
%global src37_name	mini
%global src37_ver	0.1

### moonphase
%global src38_name	moonphase
%global src38_ver	1.0

### mplayer_red
%global src39_name	mplayer_red
%global src39_ver	1.0

### netscape4
%global src40_name	netscape4
%global src40_ver	1.0

### neutron
%global src41_name	neutron
%global src41_ver	1.5

### new-age
%global src42_name	new-age
%global src42_ver	1.0

### phony
%global src43_name	phony
%global src43_ver	1.1

### plastic
%global src44_name	plastic
%global src44_ver	1.2

### proton
%global src45_name	proton
%global src45_ver	1.2

### sessene
%global src46_name	sessene
%global src46_ver	1.0

### slim
%global src47_name	slim
%global src47_ver	1.2

### softgrip
%global src48_name	softgrip
%global src48_ver	1.1

### standard
%global src49_name	standard
%global src49_ver	1.9

### trium
%global src50_name	trium
%global src50_ver	1.3

### tvisor
%global src51_name	tvisor
%global src51_ver	1.1

### ultrafina
%global src52_name	ultrafina
%global src52_ver	1.1

### xanim
%global src53_name	xanim
%global src53_ver	1.6

### xine-lcd
%global src54_name	xine-lcd
%global src54_ver	1.2

### xmmplayer
%global src55_name	xmmplayer
%global src55_ver	1.5

Summary: A collection of skins for MPlayer.
Name: mplayer-skins
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Source0:  %{mplayer_url}%{src00_name}-%{src00_ver}.tar.bz2 
NoSource:  0
Source1:  %{mplayer_url}%{src01_name}-%{src01_ver}.tar.bz2 
NoSource:  1
Source2:  %{mplayer_url}%{src02_name}-%{src02_ver}.tar.bz2 
NoSource:  2
Source3:  %{mplayer_url}%{src03_name}-%{src03_ver}.tar.bz2 
NoSource:  3
Source4:  %{mplayer_url}%{src04_name}-%{src04_ver}.tar.bz2 
NoSource:  4
Source5:  %{mplayer_url}%{src05_name}-%{src05_ver}.tar.bz2 
NoSource:  5
Source6:  %{mplayer_url}%{src06_name}-%{src06_ver}.tar.bz2 
NoSource:  6
Source7:  %{mplayer_url}%{src07_name}-%{src07_ver}.tar.bz2 
NoSource:  7
Source8:  %{mplayer_url}%{src08_name}-%{src08_ver}.tar.bz2 
NoSource:  8
Source9:  %{mplayer_url}%{src09_name}-%{src09_ver}.tar.bz2 
NoSource:  9
Source10: %{mplayer_url}%{src10_name}-%{src10_ver}.tar.bz2 
NoSource: 10
Source11: %{mplayer_url}%{src11_name}-%{src11_ver}.tar.bz2 
NoSource: 11
Source12: %{mplayer_url}%{src12_name}-%{src12_ver}.tar.bz2 
NoSource: 12
Source13: %{mplayer_url}%{src13_name}-%{src13_ver}.tar.bz2 
NoSource: 13
Source14: %{mplayer_url}%{src14_name}-%{src14_ver}.tar.bz2 
NoSource: 14
Source15: %{mplayer_url}%{src15_name}-%{src15_ver}.tar.bz2 
NoSource: 15
Source16: %{mplayer_url}%{src16_name}-%{src16_ver}.tar.bz2 
NoSource: 16
Source17: %{mplayer_url}%{src17_name}-%{src17_ver}.tar.bz2 
NoSource: 17
Source18: %{mplayer_url}%{src18_name}-%{src18_ver}.tar.bz2 
NoSource: 18
Source19: %{mplayer_url}%{src19_name}-%{src19_ver}.tar.bz2 
NoSource: 19
Source20: %{mplayer_url}%{src20_name}-%{src20_ver}.tar.bz2 
NoSource: 20
Source21: %{mplayer_url}%{src21_name}-%{src21_ver}.tar.bz2 
NoSource: 21
Source22: %{mplayer_url}%{src22_name}-%{src22_ver}.tar.bz2 
NoSource: 22
Source23: %{mplayer_url}%{src23_name}-%{src23_ver}.tar.bz2 
NoSource: 23
Source24: %{mplayer_url}%{src24_name}-%{src24_ver}.tar.bz2 
NoSource: 24
Source25: %{mplayer_url}%{src25_name}-%{src25_ver}.tar.bz2 
NoSource: 25
Source26: %{mplayer_url}%{src26_name}-%{src26_ver}.tar.bz2 
NoSource: 26
Source27: %{mplayer_url}%{src27_name}-%{src27_ver}.tar.bz2 
NoSource: 27
Source28: %{mplayer_url}%{src28_name}-%{src28_ver}.tar.bz2 
NoSource: 28
Source29: %{mplayer_url}%{src29_name}-%{src29_ver}.tar.bz2 
NoSource: 29
Source30: %{mplayer_url}%{src30_name}-%{src30_ver}.tar.bz2 
NoSource: 30
Source31: %{mplayer_url}%{src31_name}-%{src31_ver}.tar.bz2 
NoSource: 31
Source32: %{mplayer_url}%{src32_name}-%{src32_ver}.tar.bz2 
NoSource: 32
Source33: %{mplayer_url}%{src33_name}-%{src33_ver}.tar.bz2 
NoSource: 33
Source34: %{mplayer_url}%{src34_name}-%{src34_ver}.tar.bz2 
NoSource: 34
Source35: %{mplayer_url}%{src35_name}-%{src35_ver}.tar.bz2 
NoSource: 35
Source36: %{mplayer_url}%{src36_name}-%{src36_ver}.tar.bz2 
NoSource: 36
Source37: %{mplayer_url}%{src37_name}-%{src37_ver}.tar.bz2 
NoSource: 37
Source38: %{mplayer_url}%{src38_name}-%{src38_ver}.tar.bz2 
NoSource: 38
Source39: %{mplayer_url}%{src39_name}-%{src39_ver}.tar.bz2 
NoSource: 39
Source40: %{mplayer_url}%{src40_name}-%{src40_ver}.tar.bz2 
NoSource: 40
Source41: %{mplayer_url}%{src41_name}-%{src41_ver}.tar.bz2 
NoSource: 41
Source42: %{mplayer_url}%{src42_name}-%{src42_ver}.tar.bz2 
NoSource: 42
Source43: %{mplayer_url}%{src43_name}-%{src43_ver}.tar.bz2 
NoSource: 43
Source44: %{mplayer_url}%{src44_name}-%{src44_ver}.tar.bz2 
NoSource: 44
Source45: %{mplayer_url}%{src45_name}-%{src45_ver}.tar.bz2 
NoSource: 45
Source46: %{mplayer_url}%{src46_name}-%{src46_ver}.tar.bz2 
NoSource: 46
Source47: %{mplayer_url}%{src47_name}-%{src47_ver}.tar.bz2 
NoSource: 47
Source48: %{mplayer_url}%{src48_name}-%{src48_ver}.tar.bz2 
NoSource: 48
Source49: %{mplayer_url}%{src49_name}-%{src49_ver}.tar.bz2 
NoSource: 49
Source50: %{mplayer_url}%{src50_name}-%{src50_ver}.tar.bz2 
NoSource: 50
Source51: %{mplayer_url}%{src51_name}-%{src51_ver}.tar.bz2 
NoSource: 51
Source52: %{mplayer_url}%{src52_name}-%{src52_ver}.tar.bz2 
NoSource: 52
Source53: %{mplayer_url}%{src53_name}-%{src53_ver}.tar.bz2 
NoSource: 53
Source54: %{mplayer_url}%{src54_name}-%{src54_ver}.tar.bz2 
NoSource: 54
Source55: %{mplayer_url}%{src55_name}-%{src55_ver}.tar.bz2 
NoSource: 55

URL: http://www.mplayerhq.hu/homepage/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: mplayer >= 0.90
BuildArch: noarch

%description
A collection of additional skins for the GUI version of MPlayer, the movie player for Linux. 

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{skindir}
# install skins
skins="%{SOURCE0} %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} \
%{SOURCE5} %{SOURCE6} %{SOURCE7} %{SOURCE8} %{SOURCE9} \
%{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} \
%{SOURCE15} %{SOURCE16} %{SOURCE17} %{SOURCE18} %{SOURCE19} \
%{SOURCE20} %{SOURCE21} %{SOURCE22} %{SOURCE23} %{SOURCE24} \
%{SOURCE25} %{SOURCE26} %{SOURCE27} %{SOURCE28} %{SOURCE29} \
%{SOURCE30} %{SOURCE31} %{SOURCE32} %{SOURCE33} %{SOURCE34} \
%{SOURCE35} %{SOURCE36} %{SOURCE37} %{SOURCE38} %{SOURCE39} \
%{SOURCE40} %{SOURCE41} %{SOURCE42} %{SOURCE43} %{SOURCE44} \
%{SOURCE45} %{SOURCE46} %{SOURCE47} %{SOURCE48} %{SOURCE49} \
%{SOURCE50} %{SOURCE51} %{SOURCE52} %{SOURCE53} %{SOURCE54} \
%{SOURCE55}"
pushd %{buildroot}%{skindir}
for skin in $skins; do
    tar -x -j -f $skin
done
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, 755)
%{skindir}/*

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-22m)
- update Blue-small 1.5

* Sat Jan 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-21m)
- update hwswskin 1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-18m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-16m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-15m)
- revised skindir

* Sat May 10 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-14m)
- update Abyss 1.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-13m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-12m)
- change Source URL

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-11m)
- %%NoSource -> NoSource

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-10m)
- upadte Blue-small to 1.4

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-9m)
- update Abyss to 1.6
- update Blue-small to 1.3
- use %%NoSource

* Fri Feb 10 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.0-8m)
- update to bluecurve-1.3

* Fri Dec 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-7m)
- add CubicPlayer-1.1, QuickSilver-1.0 and netscape4-1.0
- update to xmmplayer-1.5

* Sun Aug 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-6m)
- add standard (old default) skin
- remove WindowsMediaPlayer6 skin

* Mon Aug  9 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0-5m)
- update some skins.

* Thu Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-4m)
- chagne source
- add some skins

* Tue May  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-3m)
- MidnightLove 1.6.

* Tue May  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-2m)
- update skins and make them 'NoSource'

* Tue Jul 8 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-1m)
- initial version
