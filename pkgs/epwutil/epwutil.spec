%global momorel 20

Summary: Utilities for EB/EPWING
Name: epwutil
Version: 1.1
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Applications/Productivity
Source0: http://openlab.ring.gr.jp/edict/epwutil/epwutil-%{version}.tar.gz 
Patch0: epwutil-1.1-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
EPWUTIL is utilities for using EB/EPWING dictionaries on Hard disk.

%prep
%setup -q
%patch0 -p1 -b .glibc210

%build
make -f makefile.unx

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
cp catdump bookinfo squeeze %{buildroot}/usr/bin

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README epwutil.doc *.man
%{_bindir}/catdump
%{_bindir}/bookinfo
%{_bindir}/squeeze

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-20m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-19m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-17m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-15m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-14m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-13m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-12m)
- %%NoSource -> NoSource

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1-11m)
- rebuild against new environment.

* Sat Jul 07 2001 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.1-10k)
- fpwcatdump -> catdump again.
- add -q to setup

* Tue May 29 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-8k)
- uhe. catdump with epwutil is totally different /(T_T)\
- returned as the fpwcatdump.

* Sun May 20 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-6k)
- avoid conflicting.

* Mon May  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-4k)
- rebuild against new environment.
- move to main.

* Tue Apr 18 2000 Keizi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- version 1.1

* Wed Mar 22 2000 Uechi Yasumasa <uechi@ryucom.ne.jp>
- fix spec file for Kondara MNU/Linux

* Tue Aug 24 1999 Uechi Yasumasa <uechi@ryukyu.ne.jp>
- 1st release
