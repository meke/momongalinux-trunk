%global momorel 1

# Notes
# agent - read local files (syslog, snort, etc) and forward
# server -  above + notifications + remote agents
# local - do everything server does, but not recieve messages


%define asl 1
%define _default_patch_fuzz 2

%define prg  ossec
%define cvs  100326

Summary:     An Open Source Host-based Intrusion Detection System
Name:        ossec-hids
Version:     2.5.1
Release:     %{momorel}m%{?dist}
License:     GPLv3
Group:       Applications/System
Source0:     http://www.ossec.net/files/%{name}-%{version}.tar.gz
NoSource:    0
Source1:     %{name}-find-requires
Source2:     %{name}.init
Source3:     asl_rules.xml
Source4:     authpsa_rules.xml
Source5:     asl-shun.pl
Source6:     ossec-hids.logrotate
Source7:     zabbix-alert.sh
Source8:     ossec-configure
Patch2:      decoder-asl.patch
Patch3:      syslog_rules.patch
Patch4:      ossec-client-conf.patch
Patch5:      firewall-drop-update.patch
Patch6:      disable-psql.patch
Patch9:      ossec-client-init.patch
Patch10:     smtp_auth-decoder.patch
Patch11:     courier-imap-rules.patch
Patch12:     denyhosts-decoder.patch
Patch13:     ossec-hids-server-reload.patch
URL:         http://www.%{prg}.net/
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils glibc-devel httpd-devel openssl-devel
BuildRequires: mysql-devel inotify-tools-devel
Provides: ossec = %{version}-%{release}
Requires: inotify-tools

ExclusiveOS: linux


%description
OSSEC HIDS is an Open Source Host-based Intrusion Detection
System. It performs log analysis, integrity checking, root kit
detection, time-based alerting and active response.


%package client
Summary:     The OSSEC HIDS Client
Group:       Applications/System
Provides:    ossec-client = %{version}-%{release}
Requires:    %{name} = %{version}-%{release} 
Conflicts:   %{name}-server
%if %{asl}
Requires:    perl-DBD-SQLite
%endif


%package server
Summary:     The OSSEC HIDS Server
Group:       Applications/System
Provides:    ossec-server = %{version}-%{release}
Requires:    %{name} = %{version}-%{release} 
Conflicts:   %{name}-client
%if %{asl}
Requires:    perl-DBD-SQLite
%endif


%description client
The %{name}-client package contains the client part of the
OSSEC HIDS. Install this package on every client to be
monitored.

%description server
The %{name}-server package contains the server part of the
OSSEC HIDS. Install this package on a central machine for
log collection and alerting.


%prep
%setup -q 
%if %{asl}
%patch2 -p0
%patch3 -p0
%patch4 -p0
%patch5 -p0
%patch6 -p0
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%endif
%patch9 -p0

# Prepare for docs
rm -rf contrib/specs
find doc -type f -exec chmod 644 {} \;
find contrib -type f -exec chmod 644 {} \;
chmod 644 CONFIG INSTALL README BUGS
OLDPWD=`pwd`
cd doc/br
for i in `ls`
do
   iconv -f iso8859-1 -t utf-8 $i > $i.conv && mv -f $i.conv $i
done
cd $OLDPWD


%build
pushd src
# Build the agent version first
echo "CEXTRA=-DCLIENT" >> ./Config.OS
make all
make build
mv addagent/manage_agents ../bin/manage_client
mv logcollector/ossec-logcollector  ../bin/client-logcollector
mv syscheckd/ossec-syscheckd  ../bin/client-syscheckd
# Rebuild for server
make clean
make setdb
make all
make build
popd


# Generate the ossec-init.conf template
echo "DIRECTORY=\"%{_localstatedir}/%{prg}\"" >  %{prg}-init.conf
echo "VERSION=\"%{version}\""                 >> %{prg}-init.conf
echo "DATE=\"`date`\""                        >> %{prg}-init.conf


# Exclude from requires
%define _use_internal_dependency_generator 0
%define __find_requires %{SOURCE1} 
chmod 755 %{SOURCE1}


%install
[ -n "${RPM_BUILD_ROOT}" -a "${RPM_BUILD_ROOT}" != "/" ] && rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/init.d/
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/{bin,stats,rules,tmp}
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/rules/translated/pure_ftpd
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/logs/{archives,alerts,firewall}
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/queue/{alerts,%{prg},fts,syscheck,rootcheck,agent-info,rids}
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/var/run
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/shared
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/templates
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/mysql
mkdir -p ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/active-response/bin
strip -s -v bin/*

install -m 0644 %{prg}-init.conf ${RPM_BUILD_ROOT}%{_sysconfdir}
install -m 0644 etc/%{prg}.conf ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/%{prg}.conf.sample
install -m 0644 etc/%{prg}-{agent,server}.conf ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc
install -m 0644 etc/*.xml ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc
install -m 0644 etc/internal_options* ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc
install -m 0644 etc/rules/*xml ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/rules
install -m 0644 etc/rules/translated/pure_ftpd/* ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/rules/translated/pure_ftpd
install -m 0644 etc/templates/config/* ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/templates/
install -m 0755 bin/* ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/bin
install -m 0755 active-response/*.sh ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/active-response/bin
install -m 0644 src/rootcheck/db/*.txt ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/shared
install -m 0644 src/os_dbd/mysql.schema ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/mysql/mysql.schema
install -m 0755 src/init/%{prg}-{client,server}.sh ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/bin
install -m 0755 %{SOURCE2} ${RPM_BUILD_ROOT}%{_sysconfdir}/init.d/%{name}

touch ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/etc/%{prg}.conf

%if %{asl}
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
install -m 0644 %{SOURCE3} ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/rules
install -m 0644 %{SOURCE4} ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/rules
install -m 0755 %{SOURCE5} ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/active-response/bin/asl-shun.pl
install -m 0644 %{SOURCE6} ${RPM_BUILD_ROOT}/etc/logrotate.d/ossec-hids
install -m 0755 %{SOURCE7} ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/active-response/bin/zabbix-alert.sh
install -m 0755 %{SOURCE8} ${RPM_BUILD_ROOT}%{_localstatedir}/%{prg}/bin/ossec-configure
%endif

%pre
if ! id -g %{prg} > /dev/null 2>&1; then
  groupadd -r %{prg}
fi
if ! id -u %{prg} > /dev/null 2>&1; then
  useradd -g %{prg} -G %{prg}       \
        -d %{_localstatedir}/%{prg} \
        -r -s /sbin/nologin %{prg}
fi

%pre server
if ! id -u %{prg}m > /dev/null 2>&1; then
  useradd -g %{prg} -G %{prg}       \
        -d %{_localstatedir}/%{prg} \
        -r -s /sbin/nologin %{prg}m
fi
if ! id -u %{prg}e > /dev/null 2>&1; then
  useradd -g %{prg} -G %{prg}       \
        -d %{_localstatedir}/%{prg} \
        -r -s /sbin/nologin %{prg}e
fi
if ! id -u %{prg}r > /dev/null 2>&1; then
  useradd -g %{prg} -G %{prg}       \
        -d %{_localstatedir}/%{prg} \
        -r -s /sbin/nologin %{prg}r
fi


%post client
if [ $1 = 1 ]; then
  chkconfig --add %{name}
  chkconfig %{name} off
fi

echo "TYPE=\"agent\"" >> %{_sysconfdir}/%{prg}-init.conf
chmod 600 %{_sysconfdir}/%{prg}-init.conf

if [ ! -f  %{_localstatedir}/%{prg}/etc/%{prg}.conf ]; then
  ln -sf %{prg}-agent.conf %{_localstatedir}/%{prg}/etc/%{prg}.conf
fi

ln -sf %{prg}-client.sh %{_localstatedir}/%{prg}/bin/%{prg}-control

# daemon trickery
ln -sf /var/ossec/bin/ossec-client-logcollector /var/ossec/bin/ossec-logcollector
ln -sf /var/ossec/bin/ossec-client-syscheckd    /var/ossec/bin/ossec-syscheckd
ln -sf %{_localstatedir}/%{prg}/bin/client-logcollector  %{_localstatedir}/%{prg}/bin/%{prg}-logcollector 
ln -sf %{_localstatedir}/%{prg}/bin/client-syscheckd  %{_localstatedir}/%{prg}/bin/%{prg}-syscheckd 
chmod -R 550 %{_localstatedir}/%{prg}/bin/


touch %{_localstatedir}/%{prg}/logs/ossec.log
chown %{prg}:%{prg} %{_localstatedir}/%{prg}/logs/ossec.log
chmod 0664 %{_localstatedir}/%{prg}/logs/ossec.log

if [ -f %{_localstatedir}/lock/subsys/%{name} ]; then
  %{_sysconfdir}/init.d/%{name} restart
fi

%post server
if [ $1 = 1 ]; then
  chkconfig --add %{name}
  chkconfig %{name} off
fi

echo "TYPE=\"server\"" >> %{_sysconfdir}/%{prg}-init.conf
chmod 600 %{_sysconfdir}/%{prg}-init.conf

if [ ! -f %{_localstatedir}/%{prg}/etc/%{prg}.conf ]; then
  ln -sf %{prg}-server.conf %{_localstatedir}/%{prg}/etc/%{prg}.conf
fi

ln -sf %{prg}-server.sh %{_localstatedir}/%{prg}/bin/%{prg}-control
chmod -R 550 %{_localstatedir}/%{prg}/bin/

touch %{_localstatedir}/%{prg}/logs/ossec.log
chown %{prg}:%{prg} %{_localstatedir}/%{prg}/logs/ossec.log
chmod 0664 %{_localstatedir}/%{prg}/logs/ossec.log

if [ -f %{_localstatedir}/lock/subsys/%{name} ]; then
  %{_sysconfdir}/init.d/%{name} restart
fi

chmod 550 /var/ossec/rules /var/ossec/tmp
chmod 700 /var/ossec/queue/rootcheck /var/ossec/queue/fts
chmod 750 /var/ossec/logs/alerts /var/ossec/logs/archives /var/ossec/stats /var/ossec/logs/firewall


%preun client
if [ $1 = 0 ]; then
  chkconfig %{name} off
  chkconfig --del %{name}

  if [ -f %{_localstatedir}/lock/subsys/%{name} ]; then
    %{_sysconfdir}/init.d/%{name} stop
  fi

  rm -f %{_localstatedir}/%{prg}/etc/localtime
  rm -f %{_localstatedir}/%{prg}/etc/%{prg}.conf
  rm -f %{_localstatedir}/%{prg}/bin/%{prg}-control
  rm -f %{_localstatedir}/%{prg}/bin/%{prg}-logcollector 
  rm -f %{_localstatedir}/%{prg}/bin/%{prg}-syscheckd 
fi

%preun server
if [ $1 = 0 ]; then
  chkconfig %{name} off
  chkconfig --del %{name}

  if [ -f %{_localstatedir}/lock/subsys/%{name} ]; then
    %{_sysconfdir}/init.d/%{name} stop
  fi

  rm -f %{_localstatedir}/%{prg}/etc/localtime
  rm -f %{_localstatedir}/%{prg}/etc/%{prg}.conf
  rm -f %{_localstatedir}/%{prg}/bin/%{prg}-control
fi


%triggerin -- glibc
[ -r %{_sysconfdir}/localtime ] && cp -fpL %{_sysconfdir}/localtime %{_localstatedir}/%{prg}/etc


%clean
[ -n "${RPM_BUILD_ROOT}" -a "${RPM_BUILD_ROOT}" != "/" ] && rm -rf ${RPM_BUILD_ROOT}
chmod 644 %{SOURCE1}



%files
%defattr(-,root,root)
%doc BUGS CONFIG INSTALL* README 
%doc %dir contrib doc
%attr(550,root,%{prg}) %dir %{_localstatedir}/%{prg}
%attr(550,root,%{prg}) %dir %{_localstatedir}/%{prg}/active-response
%attr(550,root,%{prg}) %dir %{_localstatedir}/%{prg}/active-response/bin
%attr(755,root,%{prg}) %dir %{_localstatedir}/%{prg}/bin
%attr(550,root,%{prg}) %dir %{_localstatedir}/%{prg}/etc
%attr(770,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/etc/shared
%attr(750,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/etc/templates
%attr(640,%{prg},%{prg}) %{_localstatedir}/%{prg}/etc/templates/*
%attr(750,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/logs
%attr(550,root,%{prg}) %dir %{_localstatedir}/%{prg}/queue
%attr(770,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/queue/alerts
%attr(770,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/queue/%{prg}
%attr(750,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/queue/syscheck
%attr(550,root,%{prg}) %dir %{_localstatedir}/%{prg}/var
%attr(770,root,%{prg}) %dir %{_localstatedir}/%{prg}/var/run
%if %{asl}
%config(noreplace) /etc/logrotate.d/ossec-hids
%{_localstatedir}/%{prg}/bin/%{prg}-configure
%endif


%files client
%defattr(-,root,root)
%doc BUGS CONFIG INSTALL* README
%doc %dir contrib doc
%attr(644,root,root) %config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/%{prg}-init.conf
%{_sysconfdir}/init.d/*
%config(noreplace) %{_localstatedir}/%{prg}/etc/%{prg}-agent.conf
%config(noreplace) %{_localstatedir}/%{prg}/etc/internal_options*
%config(noreplace) %{_localstatedir}/%{prg}/etc/shared/*
%{_localstatedir}/%{prg}/etc/*.sample
%{_localstatedir}/%{prg}/active-response/bin/*
%{_localstatedir}/%{prg}/bin/%{prg}-client.sh
%{_localstatedir}/%{prg}/bin/%{prg}-agentd
%{_localstatedir}/%{prg}/bin/client-logcollector
%{_localstatedir}/%{prg}/bin/client-syscheckd
%{_localstatedir}/%{prg}/bin/%{prg}-execd
%{_localstatedir}/%{prg}/bin/manage_client
%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/queue/rids

%files server
%defattr(-,root,root)
%doc BUGS CONFIG INSTALL* README
%doc %dir contrib doc
%attr(644,root,root) %config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/%{prg}-init.conf
%{_sysconfdir}/init.d/*
%ghost %config(missingok,noreplace) %{_localstatedir}/%{prg}/etc/ossec.conf
%config(noreplace) %{_localstatedir}/%{prg}/etc/%{prg}-server.conf
%config(noreplace) %{_localstatedir}/%{prg}/etc/internal_options*
%config %{_localstatedir}/%{prg}/etc/*.xml
%config(noreplace) %{_localstatedir}/%{prg}/etc/shared/*
%{_localstatedir}/%{prg}/etc/mysql/mysql.schema
%{_localstatedir}/%{prg}/etc/*.sample
%{_localstatedir}/%{prg}/active-response/bin/*
%{_localstatedir}/%{prg}/bin/%{prg}-server.sh
%{_localstatedir}/%{prg}/bin/%{prg}-agentd
%{_localstatedir}/%{prg}/bin/%{prg}-analysisd
%{_localstatedir}/%{prg}/bin/%{prg}-execd
%{_localstatedir}/%{prg}/bin/%{prg}-logcollector
%{_localstatedir}/%{prg}/bin/%{prg}-maild
%{_localstatedir}/%{prg}/bin/%{prg}-monitord
%{_localstatedir}/%{prg}/bin/%{prg}-remoted
%{_localstatedir}/%{prg}/bin/%{prg}-syscheckd
%{_localstatedir}/%{prg}/bin/%{prg}-dbd
%{_localstatedir}/%{prg}/bin/%{prg}-reportd
%{_localstatedir}/%{prg}/bin/%{prg}-agentlessd
%{_localstatedir}/%{prg}/bin/ossec-csyslogd
%{_localstatedir}/%{prg}/bin/list_agents
%{_localstatedir}/%{prg}/bin/manage_agents
%{_localstatedir}/%{prg}/bin/syscheck_update
%{_localstatedir}/%{prg}/bin/clear_stats
%{_localstatedir}/%{prg}/bin/agent_control
%{_localstatedir}/%{prg}/bin/rootcheck_control
%{_localstatedir}/%{prg}/bin/syscheck_control
%{_localstatedir}/%{prg}/bin/ossec-logtest
%{_localstatedir}/%{prg}/bin/verify-agent-conf
%{_localstatedir}/%{prg}/bin/ossec-makelists
%{_localstatedir}/%{prg}/bin/ossec-regex

%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/logs/archives
%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/logs/alerts
%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/logs/firewall
%attr(755,%{prg}r,%{prg}) %dir %{_localstatedir}/%{prg}/queue/agent-info
%attr(755,%{prg}r,%{prg}) %dir %{_localstatedir}/%{prg}/queue/rids
%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/queue/fts
%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/queue/rootcheck
%attr(755,root,%{prg}) %dir %{_localstatedir}/%{prg}/rules
%config(noreplace) %{_localstatedir}/%{prg}/rules/*
%attr(755,%{prg},%{prg}) %dir %{_localstatedir}/%{prg}/stats
%attr(755,root,%{prg}) %dir %{_localstatedir}/%{prg}/tmp


%changelog
* Thu Apr 28 2011 Matthew Hall <yohgaki@ohgaki.net> 2.5.1-1m
- Import form www.firehat.org/SRPM/

* Wed Jan 19 2011 Matthew Hall <matt@ecsc.co.uk> 2.5.1-1%{?dist}
- Build for FireHat 2.4

* Sun Jan 09 2011 Udo Seidel <udoseidel@gmx.de> - 2.4.1-4.fcXX
- Reviewed and updated agai to match Fedora package policies

* Sat May 22 2010 Udo Seidel <udoseidel@gmx.de> - 2.4.1-4.fcXX
- Reviewed and updated to match Fedora package policies

* Thu Apr 29 2010 Support <scott@atomicorp.com> - 2.4.1-4
- Updated init and ossec-server scripts to support the new reload feature.

* Tue Apr 20 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.4.1-1
- Update to 2.4.1

* Fri Apr 9 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.4-5
- Added zabbix reporting active response

* Thu Apr 1 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.4-4
- Update to 2.4 final
- Lowered courier rule 3910 (failures) from 6 over 240 to 10 over 10
- Lowered courier rule 3911 (success) from 10 over 60 to 30 over 20

* Tue Mar 23 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.4-1
- Rebuilt for atomic repo

* Thu Mar 22 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.4-0.2
- Update to CVS 100317

* Thu Mar 11 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.4-0.1
- Update to CVS 100311
- Add decoder for denyhosts
- Update asl_rules.xml to include denyhosts rules

* Tue Mar 9 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.3-8
- Update to CVS 100309

* Fri Mar 5 2010 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.3-7
- Added new decoder for smtp_auth
- Added rules to detect smtp_auth brute force attempts
- Added rules to detect imap/pop brute force attempts

* Mon Dec 7 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.3-6
- Updated ossec-server.conf to be in parity with the ASL config
- Added templates dir for generating configs

* Mon Dec 7 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.3-1
- Update to 2.3 release

* Mon Nov 9 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.2-5
- Update to snapshot 091109

* Tue Sep 29 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.2-4
- Update to snapshot 091008

* Tue Sep 29 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.2-3
- Update to snapshot 090925
- Added timestamp field to the mysql schema
- Bugfix #XXX, for the ossec-client.init script to call the correct (renamed) ossec syscheckd/logcollector daemons
- Appologies for not updating the previous changelogs. Missed a few updates!

* Mon Aug 31 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.2.0.beta2.1
- Update to snapshot 090827
- Feature Request #225, Added logrotate event to active-response log
- Updated system_audit_rcl.txt to look for the correct php.ini file

* Mon Aug 24 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.2.0.beta1.1
- Update to 090824, beta 1 release

* Wed Aug 12 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.1.1-5
- Update to 090812 snapshot

* Thu Jul 28 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.1.1-3
- Rebuild agent daemons with -DCLIENT, added symlink trickery

* Thu Jul 2 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.1.1-1
- update to 2.1.1

* Wed Jun 30 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.1-3
- update to 090630 snapshot, this has fixes for CentOS/RHEL 4 64-bit environments

* Wed Jun 12 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.1-1
- update to 2.1 final

* Wed Jun 12 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-11
- update to snapshot 090612

* Wed Jun 10 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-10
- update to snapshot 090610

* Wed Jun 3 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-9
- update to snapshot 090603

* Mon Apr 27 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-8
- Disable postgresql support, to get around an undesirable dependency on EL4

* Mon Apr 17 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-7
- Update to snapshot 090417

* Mon Apr 13 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-6
- Update to snapshot 090413 (this adds in inotify support)

* Wed Apr 10 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-5
- Update to snapshot 090410 (this adds in inotify support)

* Wed Apr 8 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-4
- Update to snapshot 090408

* Thu Mar 5 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-2
- Added authpsa rules back in, this is used to detect brute force attacks
- Added conditional building support for ASL modifications

* Fri Feb 27 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0-1
- Update to 2.0 official release

* Thu Feb 26 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0.0-0.090225.1
- update to snapshot 090225

* Sun Feb 20 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0.0-0.090220.1
- update to snapshot 090220

* Fri Feb 6 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0.0-0.090206.1
- update to snapshot 090206

* Mon Feb 5 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 2.0.0-0.090205.1
- update to snapshot 090205

* Fri Jan 30 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.99-2
- update to CVS code 090129, this is not an offical release. Its for testing only

* Tue Jan 27 2009 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.99-1
- update to CVS code 090126, this is not an offical release. Its for testing only

* Thu Oct 9 2008 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.6.1-1
- update to 1.6.1

* Wed Sep 3 2008 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.6-1
- update to 1.6

* Thu Jun 26 2008 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.5.1-1
- update to 1.5.1

* Mon Jun 9 2008 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.5-3
- added mysql support

* Tue May 20 2008 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.5-2
- Added Stanislaw Polak's excellent ban-hackers script to manage shunning more intelligently.

* Tue May 13 2008 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.5-1
- update to 1.5

* Mon Nov 26 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.4-2
- fix on active-response locking bug that prevented some rules from expiring.

* Mon Nov 19 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.4-1
- update to ossec 1.4

* Mon Oct 15 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.3-4
- update snapshot to ossec-hids-071011.tar.gz
- relinked C4, FC4, FC5 against mysql4

* Tue Oct 9 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.3-3
- update to snapshot ossec-hids-071006.tar.gz

* Wed Sep 5 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.3-2
- update to shun blocklist tracking used by ASL
- added authpsa rules + decoder

* Tue Aug 14 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.3-1
- update to 1.3

* Wed Aug 8 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.2-8
- minor adjustment in post, to check for config file before overwriting it

* Fri Aug 3 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.2-7
- v6 was first version of the patch.
- added in logging in active-response for better ASL support
- Disabled conf event in post, to keep from overwriting config files. 

* Mon Jun 25 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.2-5
- changed permissions on queue/syscheck so it can be read by the ossec group (tweak for web gui)

* Fri Jun 15 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.2-4
- removed the noreplace settings from decoder and the rules
- patch for a more ASL friendly client config

* Thu Jun 14 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.2-3
- release -2 had a bug. 
- added ASL rules (asl_rules.xml)
- added decoder for the asl style modsecurity logging
- adjusted syslog_rules for qmail-scanner issue (BUG #ASL-18)
- Added http index in asl_rules.xml (BUG #ASL-7)

* Tue May 15 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.2-1
- update to 1.2

* Tue Apr 24 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.1-1
- update to 1.1

* Tue Mar 6 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.0-2
- configuration change for ASL

* Wed Jan 17 2007 Scott R. Shinn <scott@atomicrocketturtle.com> - 1.0
- updated to 1.0

* Fri Dec 8 2006 Scott R. Shinn <scott@atomicrocketturtle.com>
- import into ART
- changed their naming conventions a bit, 0.9-3 to 0.9.3. Please dont be cross with me.

* Thu Nov 02 2006 peter.pramberger@member.fsf.org
- new version (0.9-3)

* Fri Sep 29 2006 peter.pramberger@member.fsf.org
- new version (0.9-2)

* Thu Sep 07 2006 peter.pramberger@member.fsf.org
- new version (0.9-1a)

* Thu Aug 24 2006 peter.pramberger@member.fsf.org
- new version (0.9-1)

* Wed Jul 26 2006 peter.pramberger@member.fsf.org
- new version (0.9)

* Fri Jul 14 2006 peter.pramberger@member.fsf.org
- some bugfixes

* Fri Jul 07 2006 peter.pramberger@member.fsf.org
- created

