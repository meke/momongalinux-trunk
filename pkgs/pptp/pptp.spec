%global momorel 1

Name:		pptp
Version:	1.8.0
Release:	%{momorel}m%{?dist}
Summary:	Point-to-Point Tunneling Protocol (PPTP) Client
Group:		Applications/Internet
License:	GPLv2+
URL:		http://pptpclient.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/pptpclient/pptp-%{version}.tar.gz
NoSource:	0
Source1:        pptp-tmpfs.conf
Patch0:		pptp-1.7.2-pptpsetup-mppe.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	ppp >= 2.4.2, iproute
Requires:	systemd-units

%description
Client for the proprietary Microsoft Point-to-Point Tunneling
Protocol, PPTP. Allows connection to a PPTP based VPN as used
by employers and some cable and ADSL service providers.

%package setup
Summary:        PPTP Tunnel Configuration Script
Group:          Applications/Internet
Requires:       %{name} = %{version}-%{release} 

%description setup
This package provides a simple configuration script for setting up PPTP
tunnels.

%prep
%setup -q

# Don't check for MPPE capability in kernel and pppd at all because current
# Fedora releases and EL ~I� 5 include MPPE support out of the box (#502967)
%patch0 -p1 -b .mppe

# Pacify rpmlint
perl -pi -e 's/install -o root -m 555 pptp/install -m 755 pptp/;' Makefile

%build
OUR_CFLAGS="-Wall %{optflags} -Wextra -Wstrict-aliasing=2 -Wnested-externs -Wstrict-prototypes"
make %{?_smp_mflags} CFLAGS="$OUR_CFLAGS" IP=/sbin/ip

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
install -d -m 750 %{buildroot}%{_localstatedir}/run/pptp

# Make sure /var/run/pptp exists at boot time for systems
# with /var/run on tmpfs (#656672)
%if 0%{?fedora} > 14
install -d -m 755 %{buildroot}%{_prefix}/lib/tmpfiles.d
install -p -m 644 %{SOURCE1} %{buildroot}%{_prefix}/lib/tmpfiles.d/pptp.conf
%endif


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING DEVELOPERS NEWS README TODO USING
%doc ChangeLog Documentation/DESIGN.PPTP PROTOCOL-SECURITY
%if 0%{?fedora} > 14
%{_prefix}/lib/tmpfiles.d/pptp.conf
%endif
%{_sbindir}/pptp
%{_mandir}/man8/pptp.8*
%dir %attr(750,root,root) %{_localstatedir}/run/pptp/
%config(noreplace) %{_sysconfdir}/ppp/options.pptp

%files setup
%defattr(-,root,root,-)
%{_sbindir}/pptpsetup
%{_mandir}/man8/pptpsetup.8*

%changelog
* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-1m)
- update 1.8.0

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-7m)
- add tmpfiles.d conf

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.2-2m)
- modify Requires

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.7.2-1m)
- import from Fedora

* Wed Mar 25 2009 Paul Howarth <paul@city-fan.org> 1.7.2-5
- Retain permissions on /etc/ppp/chap-secrets when using pptpsetup (#492090)
- Use upstream versions of patches
- Re-enable parallel build; Makefile dependencies now fixed
- Use perl rather than sed to edit Makefile, for spec compatibility with
  ancient distro releases

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon May 19 2008 Paul Howarth <paul@city-fan.org> 1.7.2-3
- Add dependency on /sbin/ip
- Disable parallel make - object files are missing dependency on config.h

* Mon May 19 2008 Paul Howarth <paul@city-fan.org> 1.7.2-2
- Use /sbin/ip, not /bin/ip for routing

* Wed May 14 2008 Paul Howarth <paul@city-fan.org> 1.7.2-1
- Update to 1.7.2
- New script and manpage: pptpsetup
- Add patch to remove reference to stropts.h, not shipped in F9 onwards

* Wed Feb 13 2008 Paul Howarth <paul@city-fan.org> 1.7.1-4
- Rebuild with gcc 4.3.0 for Fedora 9

* Fri Aug 24 2007 Paul Howarth <paul@city-fan.org> 1.7.1-3
- Change download URL from df.sf.net to downloads.sf.net
- Expand tabs in spec
- Clarify license as GPL version 2 or later

* Wed Aug 30 2006 Paul Howarth <paul@city-fan.org> 1.7.1-2
- FE6 mass rebuild

* Mon Feb 13 2006 Paul Howarth <paul@city-fan.org> 1.7.1-1
- new upstream version 1.7.1 (fixes #166394)
- include new document PROTOCOL-SECURITY
- cosmetic change: replace variables with macros

* Wed Aug 10 2005 Paul Howarth <paul@city-fan.org> 1.7.0-2
- own directory %%{_localstatedir}/run/pptp

* Thu Jul 28 2005 Paul Howarth <paul@city-fan.org> 1.7.0-1
- new upstream version 1.7.0
- remove patch, included upstream
- edit Makefile to prevent attempted chown in %%install
- remove redundant %%attr tag in %%files
- honour $RPM_OPT_FLAGS
- ensure directories have correct permissions

* Fri May 27 2005 Paul Howarth <paul@city-fan.org> 1.6.0-5
- bump and rebuild

* Tue May 17 2005 Paul Howarth <paul@city-fan.org> 1.6.0-4
- rebuild with dist tags

* Tue May 10 2005 Paul Howarth <paul@city-fan.org> 1.6.0-3
- fix URL for SOURCE0 not to point to a specific sf.net mirror

* Tue May 10 2005 Paul Howarth <paul@city-fan.org> 1.6.0-2
- Weed out documentation useful only to developers
- Add dist tag
- Use full URL for SOURCE0
- Fix permissions on %%{_sbindir}/pptp

* Fri May  6 2005 Paul Howarth <paul@city-fan.org> 1.6.0-1
- First build for Fedora Extras, based on upstream spec file
