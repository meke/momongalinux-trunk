%global momorel 1

Summary: The client for the Trivial File Transfer Protocol (TFTP)
Name: tftp

### include local configuration
%{?include_specopt}

Version: 5.1
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
Source0: http://www.kernel.org/pub/software/network/tftp/tftp-hpa-%{version}.tar.bz2
#NoSource: 0

Patch0: tftp-0.40-remap.patch
Patch2: tftp-hpa-0.39-tzfix.patch
Patch3: tftp-0.42-tftpboot.patch
Patch4: tftp-0.49-chk_retcodes.patch
Patch5: tftp-hpa-0.49-fortify-strcpy-crash.patch
Patch6: tftp-0.49-cmd_arg.patch
Patch7: tftp-hpa-0.49-stats.patch

BuildRequires: tcp_wrappers
BuildRequires: readline-devel, ncurses-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Trivial File Transfer Protocol (TFTP) is normally used only for
booting diskless workstations.  The tftp package provides the user
interface for TFTP, which allows users to transfer files to and from a
remote machine.  This program and TFTP provide very little security,
and should not be enabled unless it is expressly needed.

%package server
Group: System Environment/Daemons
Summary: The server for the Trivial File Transfer Protocol (TFTP).
Requires: xinetd

%description server
The Trivial File Transfer Protocol (TFTP) is normally used only for
booting diskless workstations.  The tftp-server package provides the
server for TFTP, which allows users to transfer files to and from a
remote machine. TFTP provides very little security, and should not be
enabled unless it is expressly needed.  The TFTP server is run from
/etc/xinetd.d/tftp, and is disabled by default on Red Hat Linux systems.

%prep
%setup -q -n tftp-hpa-%{version}

%patch0 -p1 -b .zero
%patch2 -p1 -b .tzfix
%patch3 -p1 -b .tftpboot
%patch4 -p1 -b .chk_retcodes
%patch5 -p1 -b .fortify-strcpy-crash
%patch6 -p1 -b .cmd_arg
%patch7 -p1 -b .stats

%build

%configure
%make

%install
%__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__mkdir_p %{buildroot}%{_mandir}/man{1,8}
%__mkdir_p %{buildroot}%{_sbindir}

%define _makeinstall_args %nil
%makeinstall INSTALLROOT=%{buildroot} \
    SBINDIR=%{_sbindir} MANDIR=%{_mandir}
%__install -m755 -d %{buildroot}%{_sysconfdir}/xinetd.d/ %{buildroot}/tftpboot
%__install -m644 tftp-xinetd %{buildroot}%{_sysconfdir}/xinetd.d/tftp

%post server
/sbin/service xinetd reload > /dev/null 2>&1 || :

%postun server
if [ $1 = 0 ]; then
    /sbin/service xinetd reload > /dev/null 2>&1 || :
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES README README.security
%{_bindir}/tftp
%{_mandir}/man1/*

%files server
%defattr(-,root,root)
%config(missingok,noreplace) %{_sysconfdir}/xinetd.d/tftp
%dir /tftpboot
%{_sbindir}/in.tftpd
%{_mandir}/man8/*

%changelog
* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.1-1m)
- update 5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0-5m)
- use BuildRequires

* Tue Dec 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0-4m)
- revise for new %%configure macro

* Wed Dec 09 2009 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (5.0-3m)
- imported tftp-hpa-0.49-fortify-strcpy-crash.patch from Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.48-4m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.48-3m)
- remove BPR libtermcap-devel

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.48-2m)
- %%NoSource -> NoSource

* Tue Feb  6 2007 YONEKAWA Susumu <yonekawat@mmg.roka.jp>
- (0.48-1m)
- update to 0.48

* Sun Jan 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Wed Dec 13 2006 YONEKAWA Susumu <yonekawat@mmg.roka.jp>
- (0.45-1m)
- update to 0.45

* Tue Nov 21 2006 YONEKAWA Susumu <yonekawat@mmg.roka.jp>
- (0.43-1m)
- update to 0.43

* Fri Mar 10 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (0.42-1m)
- update to 0.42

* Wed Nov 16 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.41-1m)
- update to 0.41

* Sun Oct 24 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.40-1m)
- update to 0.40

* Thu Sep 23 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.39-1m)
- update to 0.39
- remove %%SOURCE2 and use tftp-xinetd in original archive.
- revised BuildPreReq: libtermcap-devel

* Tue Sep  7 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.38-1m)
- update to 0.38

* Wed Jan 21 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.36-1m)
- update to 0.36
- remove readline support default configuration
  conflict between BSD license and readline's

* Fri Jan  9 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.35-1m)
- update to 0.35
- use %%NoSource, %%global
- remove INSTALL, INSTALL.tftp from %%doc

* Fri Aug  1 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.34-1m)
- update to 0.34
- add BuildPreReq: tcp_wrappers, readline-devel, libtermcap-devel
- use %%{momorel} macro
- use %%make, %%makeinstall, %__rm, %__mkdir_p, %__install
- add /tftpboot directory
- add %%doc

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (0.29-2k)
- version up.

* Sun May 05 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.17-16k)
- fixed xinetd.d/tftp for chroot
- fixed spec.
- fixed default to create file (-c option)

* Fri Nov  7 2001 Toru Hoshina <t@kondara.org>
- (0.17-14k)
- tftp doesn't need xinetd, ummm.

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (0.17-12k)
- merge from Jirai.

* Sun Dec  3 2000 Daiki Matsuda <dyky@df-usa.com>
- (0.16-10k)
- errased tftp.fhs.patch and modifiles spec file for compatibility

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91 with xinetd.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.16-5).
- change c compiler egcs to gcc.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Fri Feb 11 2000 Bill Nottingham <notting@redhat.com>
- fix description

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages (again).

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- fix description and summary

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Thu Dec 2 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-netkit-tftp-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Aug 28 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.15.

* Wed Apr  7 1999 Jeff Johnson <jbj@redhat.com>
- tftpd should truncate file when overwriting (#412)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Fri Aug  7 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Sep 22 1997 Erik Troan <ewt@redhat.com>
- added check for getpwnam() failure

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
