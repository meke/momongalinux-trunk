%global momorel 1

Name: opensm
Version: 3.3.17
Release: %{momorel}m%{?dist}
Summary: OpenIB InfiniBand Subnet Manager and management utilities
Group: System Environment/Daemons
License: GPLv2 or BSD
Url: http://www.openfabrics.org/
Source0: http://www.openfabrics.org/downloads/management/%{name}-%{version}.tar.gz
NoSource: 0
Source2: opensm.logrotate
Source4: opensm.sysconfig
Source5: opensm.service
Source6: opensm.launch
Source7: opensm.rwtab
Patch0:  opensm-3.3.17-prefix.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libibmad-devel >= 1.3.8, libtool, bison, flex, byacc
Requires: %{name}-libs = %{version}-%{release}, logrotate, rdma
ExcludeArch: s390 s390x

%description
OpenSM is the OpenIB project's Subnet Manager for Infiniband networks.
The subnet manager is run as a system daemon on one of the machines in
the infiniband fabric to manage the fabric's routing state.  This package
also contains various tools for diagnosing and testing Infiniband networks
that can be used from any machine and do not need to be run on a machine
running the opensm daemon.

%package libs
Summary: Libraries used by opensm and included utilities
Group: System Environment/Libraries

%description libs
Shared libraries for Infiniband user space access

%package devel
Summary: Development files for the opensm-libs libraries
Group: Development/System
Requires: %{name}-libs = %{version}-%{release}

%description devel
Development environment for the opensm libraries

%package static
Summary: Static version of the opensm libraries
Group: Development/System
Requires: %{name}-devel = %{version}-%{release}

%description static
Static version of opensm libraries

%prep
%setup -q
%patch0 -p1 -b .prefix

%build
%configure --with-opensm-conf-sub-dir=rdma
make %{?_smp_mflags}
cd opensm
./opensm -c ../opensm-%{version}.conf

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# remove unpackaged files from the buildroot
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -fr $RPM_BUILD_ROOT%{_sysconfdir}/init.d
install -D -m644 opensm-%{version}.conf %{buildroot}%{_sysconfdir}/rdma/opensm.conf
install -D -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/opensm
install -D -m644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/opensm
install -D -m644 %{SOURCE5} %{buildroot}%{_unitdir}/opensm.service
install -D -m755 %{SOURCE6} %{buildroot}%{_libexecdir}/opensm-launch
install -D -m644 %{SOURCE7} %{buildroot}%{_sysconfdir}/rwtab.d/opensm
mkdir -p ${RPM_BUILD_ROOT}/var/cache/opensm

%clean
rm -rf $RPM_BUILD_ROOT

%post
%systemd_post opensm.service

%preun
# Don't use the macro because we need to remove our cache directory as well
# and the macro doesn't have the ability to do that.  But, here the macro
# is for future reference:
# %systemd_preun opensm.service
if [ $1 = 0 ]; then
	/bin/systemctl --no-reload disable opensm.service >/dev/null 2>&1 || :
	/bin/systemctl stop opensm.service >/dev/null 2>&1 || :
	rm -fr /var/cache/opensm
fi

%postun
%systemd_postun_with_restart opensm.service

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%dir /var/cache/opensm
%{_sbindir}/*
%{_mandir}/*
%{_unitdir}/*
%{_libexecdir}/*
%config(noreplace) %{_sysconfdir}/logrotate.d/opensm
%config(noreplace) %{_sysconfdir}/rdma/opensm.conf
%config(noreplace) %{_sysconfdir}/sysconfig/opensm
%{_sysconfdir}/rwtab.d/opensm
%doc AUTHORS COPYING ChangeLog INSTALL README NEWS

%files libs
%defattr(-,root,root,-)
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/lib*.so
%{_includedir}/infiniband/*

%files static
%defattr(-,root,root,-)
%{_libdir}/lib*.a

%changelog
* Fri Mar 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.17-1m)
- update 3.3.17

* Wed Oct 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.11-2m)
- rebuild against libibmad-1.3.8

* Wed Oct 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.11-1m)
- update 3.3.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.5-4m)
- full rebuild for mo7 release

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.5-3m)
- remove duplicate directory

* Thu Mar 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-2m)
- use %%{_initscriptdir} instead of %%{_initddir}

* Thu Mar 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.5-1m)
- import from Fedora 13

* Mon Mar 08 2010 Doug Ledford <dledford@redhat.com> - 3.3.5-1
- Update to latest upstream release.  We need various defines in ib_types.h
  for the latest ibutils package to build properly, and the latest ibutils
  package is needed because we found licensing problems in the older
  tarballs during review.

* Mon Jan 11 2010 Doug Ledford <dledford@redhat.com> - 3.3.3-2
- ExcludeArch s390(x) as there's no hardware support there

* Thu Dec 03 2009 Doug Ledford <dledford@redhat.com> - 3.3.3-1
- Update to latest upstream release
- Minor tweaks to init script for LSB compliance

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 20 2009 Doug Ledford <dledford@redhat.com> - 3.3.2-1
- Update to latest upstream version

* Wed Apr 22 2009 Doug Ledford <dledford@redhat.com> - 3.3.1-1
- Update to latest upstream version

* Fri Mar 06 2009 Caolan McNamara <caolanm@redhat.com> - 3.2.1-3
- fix bare elifs to rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jun 08 2008 Doug Ledford <dledford@redhat.com> - 3.2.1-1
- Initial package for Fedora review process

