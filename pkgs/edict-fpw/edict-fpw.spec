%global momorel 25

Summary: JIS X 4081 version of edict
Name: edict-fpw
Version: 1.2.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
Source0: http://openlab.ring.gr.jp/edict/fpw/dist/edict/edict-fpw1.2.1-src.tar.gz
Source1: ftp://ftp.cc.monash.edu.au/pub/nihongo/edict_doc.txt
URL: http://openlab.ring.gr.jp/edict/fpw/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gzip, epwutil, eb, perl-Jcode >= 0.82-2m
BuildRequires: edict >= 1.0-0.20030819.1m
BuildRequires: freepwing >= 1.4.2
BuildArch: noarch
Requires: filesystem >= 2.0.7
Docdir: %{_docdir}

%description
JIS X 4081 version of edict

%define dictdir /usr/share/freepwing-dict
%define dictname edict

%prep
rm -rf %{buildroot}

%setup -q -n %{name}%{version}
mkdir source
ln -s  /usr/share/dict/edict ./source/
cp %{SOURCE1} ./source/

%build
fpwmake
fpwmake catalogs
fpwmake package-tar-gz

%install
mkdir -p %{buildroot}%{dictdir}
tar zxvf %{name}%{version}.tar.gz -C %{buildroot}%{dictdir}
cd %{buildroot}%{dictdir}/%{dictname} ; ebzip

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.PKG
%dictdir/edict

%changelog
* Fri Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-25m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-24m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-23m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-22m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-21m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-19m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-18m)
- rebuild against gcc43

* Sun Oct 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.1-17m)
- rebuild against edict-1.0-0.20030819.1m

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (kossori)
- change BuildPreReq edict >= 1.0-0.20020426.3m

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-16m)
- adapt the License: preamble for the Momonga Linux license expression unification policy (draft)

* Sun Jan 12 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.1-15m)
- BuildPrereq perl-Jcode >= 0.82-2m

* Fri Apr 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-14k)
- rebuild against freepwing-1.4.2
- follow an update of edict

* Fri Jan 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-12k)
- follow an update of edict

* Mon Nov 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-10k)
- rebuild against updated edict

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1-8k)
- modified spec file with macros about docdir

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1-7k)
- modified spec file with macros about docdir

* Thu Aug 17 2000 AYUHANA Tomonori <l@kondara.org>
- (1.2.1-3k)
- add perl-Jcode at BuildPreReq
- obey FHS
- add Requires: filesystem >= 2.0.7
- add Docdir: /usr/share/doc

* Tue Jul 25 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2.1-2k)
- change freepwing > freepwing-dict

* Sun Jul 23 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2.1-1k)
- up tp 1.2 

* Sat Jul 22 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.2-1k)
- up tp 1.2 

* Wed Jul 19 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.1.1-1k)
- first release
