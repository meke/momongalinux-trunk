%global momorel 6

Summary: Streamripper records shoutcast-compatible streams
Name:    streamripper
Version: 1.64.6
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPLv2+
URL:     http://streamripper.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/streamripper-%{version}.tar.gz
NoSource: 0
BuildRequires: faad2-devel >= 2.7
BuildRequires: glib2-devel
BuildRequires: libmad
BuildRequires: libogg-devel
BuildRequires: libvorbis-devel
BuildRequires: ncurses-devel
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Streamripper was started sometime back in early 2000. Streamripper started as a way to separate tracks via Shoutcast's title-streaming feature. This has now been expanded into a much more generic feature, where part of the program only tries to "hint" at where one track starts and another ends, thus allowing a mp3 decoding engine to scan for a silent mark, which is used to find an exact track separation.

%prep
#'
%setup -q

%build
%configure \
    --without-included-libmad
%make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES README THANKS INSTALL COPYING
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.64.6-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.64.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.64.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.64.6-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.64.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.64.6-1m)
- update to 1.64.6

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.64.0-3m)
- rebuild against faad2-2.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.64.0-2m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.64.0-1m)
- [SECURITY] CVE-2008-4829
- update to 1.64.0

* Tue Apr 22 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.62.2-1m)
- import to Momonga

* Mon Mar 31 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.62.2-1m)
- update to 1.62.2

* Tue Jul 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.60.10-0.0.1m)
- build for Momonga
