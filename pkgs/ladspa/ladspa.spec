%global momorel 7
%global srcname ladspa_sdk

Summary: LADSPA SDK example plugins
Name: ladspa
Version: 1.13
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://www.ladspa.org/
Group: Applications/Multimedia
Source0: http://www.ladspa.org/download/%{srcname}_%{version}.tgz 
NoSource: 0
Source1: %{name}.sh
Source2: %{name}.csh
Patch0: %{name}-buildid.patch
BuildRequires: binutils >= 2.18
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
There is a large number of synthesis packages in use or development on
the Linux platform at this time. The Linux Audio Developer's Simple
Plugin API (LADSPA) attempts to give programmers the ability to write
simple `plugin' audio processors in C/C++ and link them dynamically
against a range of host applications.

%package devel
Summary: Linux Audio Developer's Simple Plugin API
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
There is a large number of synthesis packages in use or development on
the Linux platform at this time. The Linux Audio Developer's Simple
Plugin API (LADSPA) attempts to give programmers the ability to write
simple `plugin' audio processors in C/C++ and link them dynamically
against a range of host applications.

Definitive technical documentation on LADSPA plugins for both the host
and plugin is contained within copious comments within the ladspa.h
header file.

%prep
%setup -q -n %{srcname}

%patch0 -p1 -b .buildid~

# fix links to the header file in the docs
cd doc
perl -pi -e "s!HREF=\"ladspa.h.txt\"!href=\"file:///usr/include/ladspa.h\"!" *.html

%build
cd src
%make targets

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

cd src
make install \
INSTALL_BINARY_DIR=%{buildroot}%{_bindir} \
INSTALL_INCLUDE_DIR=%{buildroot}%{_includedir} \
INSTALL_PLUGINS_DIR=%{buildroot}%{_libdir}/ladspa

mkdir -p %{buildroot}%{_sysconfdir}/profile.d
install -m 755 %{SOURCE1} %{buildroot}%{_sysconfdir}/profile.d/ladspa.sh
install -m 755 %{SOURCE2} %{buildroot}%{_sysconfdir}/profile.d/ladspa.csh

# gw replace lib by lib64 if needed
perl -pi -e "s!/usr/lib!%{_libdir}!" %{buildroot}%{_sysconfdir}/profile.d/ladspa.*sh

# for pulgins
mkdir -p %{buildroot}%{_datadir}/ladspa/rdf

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README doc/COPYING
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/profile.d/ladspa*sh
%{_bindir}/analyseplugin
%{_bindir}/applyplugin
%{_bindir}/listplugins
%{_libdir}/ladspa

%files devel
%defattr(-,root,root)
%doc doc/*.html
%{_includedir}/ladspa.h

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-7m)
- rebuild for new GCC 4.6

* Thu Feb 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-6m)
- update patch0 for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13-2m)
- rebuild against rpm-4.6

* Wed May  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.13-1m)
- version 1.13
- remove merged compile-fix.diff

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-3m)
- %%NoSource -> NoSource

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-3m)
- add ladspa-buildid.patch
- add BuildRequires: binutils >= 2.18

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12-2m)
- import ladspa_sdk-compile-fix.diff from opensuse
 +* Thu Oct 06 2005 - tiwai@suse.de
 +- fix some compile problems with the recent gcc.

* Wed Feb 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12-1m)
- import from cooker

* Fri Jun  4 2004 Gotz Waschk <waschk@linux-mandrake.com> 1.12-7mdk
- add source URL
- drop prefix
- new g++

* Tue Jan 13 2004 Gotz Waschk <waschk@linux-mandrake.com> 1.12-6mdk
- add scripts for setting the LADSPA_PATH variable (Daniel Lyddy)

* Mon Apr 28 2003 Gotz Waschk <waschk@linux-mandrake.com> 1.12-5mdk
- fix distriblint warning

* Tue Apr  8 2003 Gotz Waschk <waschk@linux-mandrake.com> 1.12-4mdk
- fix buildrequires

* Fri Dec 27 2002 Gotz Waschk <waschk@linux-mandrake.com> 1.12-3mdk
- use versioned tarball
- new version

* Thu Aug 15 2002 Laurent Culioli <laurent@pschit.net> 1.12-2mdk
- Rebuild with gcc3.2

* Mon Aug 12 2002 Gotz Waschk <waschk@linux-mandrake.com> 1.12-1mdk
- new version

* Mon Jul 29 2002 Gotz Waschk <waschk@linux-mandrake.com> 1.11-3mdk
- gcc 3.2 build

* Tue May 28 2002 Gotz Waschk <waschk@linux-mandrake.com> 1.11-2mdk
- gcc 3.1

* Thu Mar 21 2002 Gotz Waschk <waschk@linux-mandrake.com> 1.11-1mdk
- initial package
