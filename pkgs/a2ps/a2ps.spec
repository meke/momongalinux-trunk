%global momorel 17

Summary: Converts text and other types of files to PostScript(TM)
Name: a2ps
Version: 4.14
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Publishing
Source: ftp://ftp.gnu.org/gnu/a2ps/%{name}-%{version}.tar.gz
NoSource: 0
Source1: ftp://ftp.enst.fr/pub/unix/a2ps/i18n-fonts-0.1.tar.gz
Patch0: a2ps-4.14-conf.patch
Patch1: a2ps-4.13-etc.patch
Patch3: a2ps-4.13-security.patch
Patch4: a2ps-4.13-glibcpaper.patch
Patch5: a2ps-texi-comments.patch
Patch7: a2ps-sort.patch
Patch8: a2ps-iso5-minus.patch
Patch9: a2ps-perl.patch
# EUC-JP support
Patch10: a2ps-4.13-eucjp.patch
Patch11: a2ps-4.13-autoenc.patch
Patch12: a2ps-4.13b-attr.patch
Patch13: a2ps-4.13b-numeric.patch
Patch14: a2ps-4.13b-encoding.patch
Patch15: a2ps-4.13b-tilde.patch
Patch17: a2ps-4.13-euckr.patch
Patch18: a2ps-4.13-gnusource.patch
Patch20: a2ps-4.13-hebrew.patch
Patch22: a2ps-4.14-shell.patch
Patch26: a2ps-make-fonts-map.patch
Patch28: a2ps-wdiff.patch
Patch29: a2ps-U.patch
Patch31: a2ps-mb.patch
Patch34: a2ps-external-libtool.patch
Patch35: a2ps-4.14-texinfo-nodes.patch
Requires: fileutils sh-utils info
BuildRequires: gperf
BuildRequires: emacs >= %{_emacs_version}, flex, libtool, texinfo, groff
BuildRequires: ImageMagick
BuildRequires: groff-perl
BuildRequires: cups
BuildRequires: gettext, bison
BuildRequires: psutils
Url: http://www.gnu.org/software/a2ps/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: psutils, ImageMagick, texinfo-tex, gzip, bzip2, groff-perl
Requires: file
#Requires: fonts-hebrew
#Requires: culmus-fonts
#Requires: fonts-hebrew-fancy
Requires(post): info coreutils
Requires(preun): info coreutils
Obsoletes: a2ps-i18n

%description
The a2ps filter converts text and other types of files to PostScript(TM).
A2ps has pretty-printing capabilities and includes support for a wide
number of programming languages, encodings (ISO Latins, Cyrillic, etc.),
and medias.

%package -n emacs-a2ps
Summary: a2ps style sheets mode for Emacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
BuildArch: noarch
Obsoletes: elisp-a2ps
Provides: elisp-a2ps

%description -n emacs-a2ps
A major-mode to edit a2ps style sheet files.

%prep
%setup -q -a 1
%patch0 -p1 -b .conf
%patch1 -p1 -b .etc
%patch3 -p1 -b .security
%patch4 -p1 -b .glibcpaper
%patch5 -p1 -b .texi-comments
%patch7 -p1 -b .sort
%patch8 -p1 -b .iso5-minus
%patch9 -p1 -b .perl

%patch10 -p1 -b .euc
%patch11 -p1 -b .ae
%patch12 -p1 -b .attr

# Use C locale's decimal point style (bug #53715).
%patch13 -p1 -b .numeric

# Use locale to determine a sensible default encoding (bug #64584).
%patch14 -p1 -b .encoding

# Fix koi8 tilde (bug #66393).
%patch15 -p1 -b .tilde

# Add Korean resource file (bug #81421).
%patch17 -p1 -b .euckr

# Prevent strsignal segfaulting (bug #104970).
%patch18 -p1 -b .gnusource

# Hebrew support (bug #113191).
%patch20 -p1 -b .hebrew

# Use environment variable to pass filenames to shell (bug #128647).
%patch22 -p1 -b .shell

# Use external libtool (bug #225235).
%patch34 -p1 -b .external-libtool

# Fix problems in make_fonts_map script (bug #142299).  Patch from
# Michal Jaegermann.
%patch26 -p1 -b .make-fonts-map

# Make pdiff default to not requiring wdiff (bug #68537).
%patch28 -p1 -b .wdiff

# Make pdiff use diff(1) properly (bug #156916).
%patch29 -p1 -b .U

# Fixed multibyte handling (bug #212154).
%patch31 -p1 -b .mb

# Remove dots in node names, patch from Vitezslav Crhonek (Bug #445971)
%patch35 -p1 -b .nodes

for file in AUTHORS ChangeLog; do
  iconv -f latin1 -t UTF-8 < $file > $file.utf8
  touch -c -r $file $file.utf8
  mv $file.utf8 $file
done

mv doc/encoding.texi doc/encoding.texi.utf8
iconv -f KOI-8 -t UTF-8 doc/encoding.texi.utf8 -o doc/encoding.texi

# Fix reference to a2ps binary (bug #112930).
perl -pi -e "s,/usr/local/bin,%{_bindir}," contrib/emacs/a2ps.el

chmod -x lib/basename.c lib/xmalloc.c

# restore timestamps of patched files
touch -c -r configure.in.conf configure.in
touch -c -r config.h.in.euc config.h.in
touch -c -r configure.conf configure
touch -c -r src/Makefile.am.euc src/Makefile.am
touch -c -r etc/Makefile.am.etc etc/Makefile.am
#touch -c -r fonts/Makefile.in src/Makefile.in lib/Makefile.in
touch -c -r etc/Makefile.in.etc etc/Makefile.in

chmod 644 encoding/iso8.edf.hebrew
chmod 644 encoding/euc-kr.edf.euckr

%build
# preset the date in README.in to avoid the timestamp of the build time
sed -e "s!@date@!`date -r NEWS`!" etc/README.in > etc/README.in.tmp
touch -c -r etc/README.in etc/README.in.tmp
mv etc/README.in.tmp etc/README.in

%ifarch ppc64
cp %{_datadir}/libtool/config.* auxdir
%endif
EMACS=emacs %configure \
  --with-medium=_glibc \
  --enable-kanji

# Remove prebuilt info files to force regeneration at build time
find . -name "*.info*" -exec rm -f {} \;
# force rebuilding scanners by flex - patched or not
find src lib -name '*.l' -exec touch {} \;

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install INSTALL='install -p'

# reset the timestamp for the generated etc/README file
touch -r etc/README.in %{buildroot}%{_datadir}/a2ps/README

mkdir -p %{buildroot}%{_sysconfdir}/a2ps

mkdir -p %{buildroot}%{_datadir}/a2ps/{afm,fonts}
pushd i18n-fonts-0.1/afm
install -p -m 0644 *.afm %{buildroot}%{_datadir}/a2ps/afm
pushd ../fonts
install -p -m 0644 *.pfb %{buildroot}%{_datadir}/a2ps/fonts
popd
popd

# remove unwanting file
rm -f %{buildroot}%{_infodir}/dir

%find_lang %name

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/a2ps.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/ogonkify.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/regex.info %{_infodir}/dir || :
(cd %{_datadir}/a2ps/afm;
    	./make_fonts_map.sh > /dev/null 2>&1 || /bin/true
	if [ -f fonts.map.new ]; then
	    mv fonts.map.new fonts.map
	fi
)

%preun
if [ $1 = 0 ]; then
   /sbin/install-info --delete %{_infodir}/a2ps.info %{_infodir}/dir || :
   /sbin/install-info --delete %{_infodir}/ogonkify.info %{_infodir}/dir || :
   /sbin/install-info --delete %{_infodir}/regex.info %{_infodir}/dir || :
fi

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%dir %{_sysconfdir}/a2ps
%config %{_sysconfdir}/a2ps.cfg
%config(noreplace) %{_sysconfdir}/a2ps-site.cfg
%doc ANNOUNCE AUTHORS COPYING ChangeLog FAQ HACKING NEWS README README.eucJP THANKS TODO
%{_bindir}/*
%{_libdir}/lib*
%{_includedir}/*
%{_infodir}/a2ps.info*
%{_infodir}/ogonkify.info*
%{_infodir}/regex.info*
%{_mandir}/*/*
# automatically regenerated at install and update time
%verify(not size mtime md5) %{_datadir}/a2ps/afm/fonts.map
%{_datadir}/a2ps/afm/*.afm
%{_datadir}/a2ps/afm/make_fonts_map.sh
%{_datadir}/a2ps/README
%{_datadir}/a2ps/encoding
%{_datadir}/a2ps/fonts
%{_datadir}/a2ps/ppd
%{_datadir}/a2ps/ps
%{_datadir}/a2ps/sheets
%{_datadir}/ogonkify/
%dir %{_datadir}/a2ps/afm
%dir %{_datadir}/a2ps

%files -n emacs-a2ps
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/a2ps.el*
%{_emacs_sitelispdir}/a2ps-print.el*

%changelog
* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14-17m)
- fix texi build failure

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.14-16m)
- add i18n-fonts-0.1.tar.gz

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.14-15m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.14-15m)
- rename elisp- to emacs-

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.14-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.14-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.14-12m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-11m)
- rebuild against emacs-23.2

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-10m)
- split out elisp-a2ps

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.14-9m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.14-7m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.14-6m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-5m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-4m)
- rebuild against emacs-23.0.94

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-2m)
- rebuild against rpm-4.6

* Thu Jan  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.14-1m)
- update to 4.14 based on Rawhide (4.14-6)
-- import Patch7,8,9,26,28,29,31,34,35 from Rawhide (4.14-6)
-- update Patch0 for fuzz=0 and correct afm font paths
-- update Patch1,10,17,20 from Rawhide (4.14-6)
-- update Patch22
-- drop Patch5,16,19,21,30, merged upstream
-- drop Patch2, not needed
-- License: GPLv3+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.13b-29m)
- rebuild against gcc43

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.13b-28m)
- revised spec for debuginfo

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.13b-27m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.13b-26m)
- enable ppc64

* Sun Feb  6 2005 Toru Hoshina <t@momonga-linux.org>
- (4.13b-25m)
- enable x86_64. applied some new patches.

* Wed Jan 26 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.13b-24m)
  touch *.am are first, *.in are next

* Mon Oct 18 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.13b-23m)
- add gcc34 patch

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (4.13b-22m)
- revised spec for enabling rpm 4.2.

* Wed Nov  5 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (4.13b-21m)
- use %%{momorel}

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (4.13b-20k)
- applied some new patches.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (4.13b-18k)
- omit /usr/bin/emacs in BuildRequires
- append emacs in BuildPreReq.

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (4.13b-16k)
- rebuild against gettext 0.10.40.

* Mon Apr 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (4.13b-14k)
- modified %post and %preun about install-info --dir-file=

* Tue Mar 27 2001 Toru Hoshina <toru@df-usa.com>
- kondarized.

* Thu Feb 28 2001 SATO Satoru <ssato@redhat.com>
- bunzip2-ed all patches except eucjp

* Thu Feb 22 2001 SATO Satoru <ssato@redhat.com>
- support Japanese
- bzip2-ed all patches
- replace macros (%%makeinstall, %%configure) with traditional 
  commands to avoid some troubles those macros caused.

* Tue Feb 20 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Using letter is as weird as oz, fl. oz, Fahrenheit, lb etc. 
  Add a patch for using the glibc media type for giving US
  letter for en_US (only locale with this paper), and A4
  for everyone else.

* Tue Feb 20 2001 Tim Powers <timp@redhat.com>
- changed default medium back to letter (bug 27794)

* Mon Feb 19 2001 Trond Eivind Glomsrod <teg@redhat.com>
- langify
- use %%{_tmppath}

* Mon Feb 12 2001 Tim Waugh <twaugh@redhat.com>
- Fix tmpfile security patch so that it actually _works_ (bug #27155).

* Sun Jan 21 2001 Tim Waugh <twaugh@redhat.com>
- New-style prereq line.
- %%post script requires fileutils (mv) and sh-utils (true).  This
  should fix bug #24251).

* Mon Jan 08 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add /usr/bin/emacs to BuildRequires
- A4
- specify use of GNU Emacs for building

* Fri Jan 05 2001 Preston Brown <pbrown@redhat.com>
- security patch for tmpfile creation from Olaf Kirch <okir@lst.de>

* Mon Dec 11 2000 Preston Brown <pbrown@redhat.com>
- obsoleted old a2ps-i18n package (it was tiny) and included those fonts
  directly here.

* Thu Dec  7 2000 Tim Powers <timp@redhat.com>
- built for dist-7.1

* Mon Aug 07 2000 Tim Powers <timp@redhat.com>
- update to 4.13b to fix some bugs, thanks to czar@acm.org for giving me a
  heads up on this (bug #15679)

* Mon Jul 24 2000 Prospector <prospector@redhat.com>
- rebuilt

* Mon Jul 10 2000 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Jun 23 2000 Tim Powers <timp@redhat.com>
- info pages weren't getting gzipped.
- stdout & stderror redirected to /dev/null in post section

* Mon Jun 19 2000 Tim Powers <timp@redhat.com>
- fixed bug 12451 which was a stupid mistake by me.
- quiet the post section
- added patches from michal@ellpspace.math.ualberta.ca and did some spec file
  magic he suggested as well.

* Fri Jun 2 2000 Tim Powers <timp@redhat.com>
- fixed bug 5876, was not setting the paper size to Letter again :(
- man pages and info pages to /usr/share, FHS compliant.
- used macros wherever possible

* Wed May 31 2000 Tim Powers <timp@rehat.com>
- fixed bug #11078, now requires psutils

* Wed Apr 26 2000 Tim Powers <timp@redhat.com>
- updated to 4.13
- compress man pages

* Thu Feb 10 2000 Tim Powers <timp@redhat.com>
- gzip man pages
- strip binaries

* Mon Jan 24 2000 Tim Powers <timp@redhat.com>
- had to be more specific since the i18n stuff was removed from the package.
	There is a new a2ps-i18n package which treats the
	/usr/share/a2ps/afm/fonts.map file as a config file
	
* Wed Oct 27 1999 Tim Powers <timp@redhat.com>
- added the --with-medium=Letter option to the configure process

* Thu Aug 5 1999 Tim Powers <timp@redhat.com>
- fixed problems with missing dirs as reported in bug 3822
- built for powertools

* Tue Jul 6 1999 Tim Powers <timp@redhat.com>
- rebuilt for powertools 6.1

* Wed May 12 1999 Bill Nottingham <notting@redhat.com>
- add a2ps-site.cfg

* Mon Apr 26 1999 Preston Brown <pbrown@redhat.com>
- update to 4.12 for Powertools 6.0

* Sat Oct 24 1998 Jeff Johnson <jbj@redhat.com>
- narrower range of %files splats.
- install info correctly.
- new description/summary text.

* Tue Oct 06 1998 Michael Maher <mike@redhat.com>
- updated source

* Sat Jul 04 1998 Michael Maher <mike@redhat.com>
- built package
