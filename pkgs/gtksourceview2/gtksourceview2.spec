%global momorel 6
%global tarname gtksourceview

Summary: GtkSourceView is a text widget that extends the GtkTextView.
Name: gtksourceview2
Version: 2.10.5
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{tarname}/2.10/%{tarname}-%{version}.tar.bz2 
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.16.1
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: gnome-vfs2-devel >= 2.24.1
BuildRequires: glib2-devel >= 2.20.1
BuildRequires: gnome-common
BuildRequires: intltool
BuildRequires: gtk-doc
BuildRequires: glade-devel

%description
GtkSourceView is a text widget that extends the standard gtk+ 2.x
text widget GtkTextView.
                                                                               
It improves GtkTextView by implementing syntax highlighting and other
features typical of a source editor.

%package devel
Summary: gtksourceview-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: libgnome-devel
Requires: libxml2-devel

%description devel
gtksourceview-devel

%prep
%setup -q -n %{tarname}-%{version}

%build
%configure \
    --enable-silent-rules \
    --enable-glade-catalog \
    --disable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README ChangeLog AUTHORS MAINTAINERS NEWS HACKING
%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la
%{_datadir}/locale/*/*/*
%{_datadir}/gtksourceview-2.0

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.so
%{_includedir}/gtksourceview-2.0
%doc %{_datadir}/gtk-doc/html/gtksourceview-2.0

%changelog
* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.5-6m)
- rebuild for glade-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.5-5m)
- rebuild for glib 2.33.2

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.5-4m)
- delete glade file

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.5-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.5-1m)
- update to 2.10.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.4-2m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4

* Sun May 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.3-2m)
- add BuildRequires

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Tue May  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-3m)
- enable-gtkdoc enable-glade-catalog

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-2m)
- disable-gtkdoc (cannot build with gcc-4.4.4)

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.7-1m)
- update to 2.29.7

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.5-1m)
- update to 2.9.5

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Mon May 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.2

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.6-1m)
- update to 2.5.6

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Nov  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Mon Jun 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-2m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- rename gtksourceview2

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Mar 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.1-1m)
- version 1.1.1
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.1-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Thu Apr 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.7.0-2m)
- revised spec for enabling rpm 4.2.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.0-1m)
- version 0.7.0

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.6.0-1m)
- version 0.6.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-1m)
- version 0.5.0

* Tue Jul 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.0-1m)
- version 0.4.0

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-1m)
- version 0.3.0

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.1-1m)
- version 0.2.1

* Fri May  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.0-1m)
- version 0.2.0
