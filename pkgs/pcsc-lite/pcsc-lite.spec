%global momorel 1

Name:           pcsc-lite
Version:        1.8.11
Release:        %{momorel}m%{?dist}
Summary:        PC/SC Lite smart card framework and applications

Group:          System Environment/Daemons
License:        BSD
URL:            http://pcsclite.alioth.debian.org/
Source0:        http://alioth.debian.org/download.php/3598/%{name}-%{version}.tar.bz2
#NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libusb-devel >= 0.1.7
BuildRequires:  doxygen
BuildRequires:  perl
BuildRequires:  systemd-devel

Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires:       pcsc-ifd-handler
Requires:       %{name}-libs = %{version}-%{release}
# 390 does not have libusb or smartCards
ExcludeArch: s390 s390x

%description
The purpose of PC/SC Lite is to provide a Windows(R) SCard interface
in a very small form factor for communicating to smartcards and
readers.  PC/SC Lite uses the same winscard API as used under
Windows(R).  This package includes the PC/SC Lite daemon, a resource
manager that coordinates communications with smart card readers and
smart cards that are connected to the system, as well as other command
line tools.

%package        libs
Summary:        PC/SC Lite libraries
Group:          System Environment/Libraries
Provides:       libpcsc-lite = %{version}-%{release}

%description    libs
PC/SC Lite libraries.

%package        devel
Summary:        PC/SC Lite development files
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}
Requires:       pkgconfig
Provides:       libpcsc-lite-devel = %{version}-%{release}

%description    devel
PC/SC Lite development files.

%package        doc
Summary:        PC/SC Lite developer documentation
Group:          Documentation

%description    doc
%{summary}.


%prep
%setup -q

autoreconf -f

# Convert to utf-8
for file in ChangeLog; do
    iconv -f ISO-8859-1 -t UTF-8 -o $file.new $file && \
    touch -r $file $file.new && \
    mv $file.new $file
done

%build
%configure \
  --disable-static \
  --disable-autostart \
  --enable-ipcdir=%{_localstatedir}/run \
  --enable-usbdropdir=%{_libdir}/pcsc/drivers
make %{?_smp_mflags}
doxygen doc/doxygen.conf ; rm -f doc/api/*.{map,md5}

%install
rm -rf %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

# Create empty directories
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/reader.conf.d
mkdir -p $RPM_BUILD_ROOT%{_libdir}/pcsc/drivers
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/pcscd

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

# Remove documentation installed in a wrong directory
rm $RPM_BUILD_ROOT%{_docdir}/pcsc-lite/README.DAEMON


%post
%systemd_post pcscd.socket pcscd.service

%preun
%systemd_preun pcscd.socket pcscd.service

%postun
%systemd_postun_with_restart pcscd.socket pcscd.service

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%triggerun -- pcsc-lite < 1.7
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply pcscd
# to migrate them to systemd targets
%{_bindir}/systemd-sysv-convert --save pcscd >/dev/null 2>&1 ||:

# Enable pcscd socket activation
/bin/systemctl enable pcscd.socket >/dev/null 2>&1

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del pcscd >/dev/null 2>&1 || :

# Restart the service if it's already running
if /bin/systemctl is-active pcscd.service >/dev/null 2>&1 ; then
    /bin/systemctl stop pcscd.service >/dev/null 2>&1 ||:
    /bin/systemctl start pcscd.socket pcscd.service >/dev/null 2>&1 ||:
fi 

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog* COPYING DRIVERS HELP README SECURITY TODO
%dir %{_sysconfdir}/reader.conf.d/
%{_unitdir}/pcscd.service
%{_unitdir}/pcscd.socket
%{_sbindir}/pcscd
%{_libdir}/pcsc/
%{_mandir}/man5/reader.conf.5*
%{_mandir}/man8/pcscd.8*

%files libs
%defattr(-,root,root,-)
%{_libdir}/libpcsclite.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/PCSC/
%{_bindir}/pcsc-spy
%{_libdir}/libpcscspy.so*
%{_libdir}/libpcsclite.so
%{_libdir}/pkgconfig/libpcsclite.pc
%{_mandir}/man1/pcsc-spy.*

%files doc
%defattr(-,root,root,-)
%doc doc/api/ doc/example/pcsc_demo.c
%{_datadir}/doc/pcsc-lite/README.polkit


%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.11-1m)
- update 1.8.11

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.10-2m)
- rebuild against graphviz-2.36.0-1m

* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.10-1m)
- update 1.8.10

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-2m)
- rebuild against systemd-187

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-1m)
- update 1.7.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update 1.6.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-4m)
- delete __libtoolize hack

* Tue Nov 24 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.5.5-3m)
- modified boot order because pcscd should start after haldaemon

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.5-1m)
- [SECURITY] CVE-2009-4902
- update 1.5.5

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- [SECURITY] CVE-2010-0407 CVE-2009-4901
- update 1.5.4

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.2-3m)
- define __libtoolize :

* Sun Apr 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-2m)
- change Source0. tar.gz to tar.bz2

* Mon Feb 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.102-1m)
- update 1.4.102

* Mon Oct  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.4-2m)
- stop auto-startup 

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4 (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-6m)
- rebuild against gcc43

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-5m)
- fix %%install

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-4m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Wed Jun 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.3.3-3m)
- stop auto start

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-3m)
- No NoSource

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-2m)
- fix %%prep and %%files

* Wed Mar 14 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.3-1m)
- import from fc

* Tue Feb 06 2007 Bob Relyea <rrelyea@redhat.com> - 1.3.3-1
- Pick up 1.3.3

* Thu Nov 02 2006 Bob Relyea <rrelyea@redhat.com> - 1.3.2-1
- Pick up 1.3.2

* Thu Sep 14 2006  Bob Relyea <rrelyea@redhat.com> - 1.3.1-7
- Incorporate patch from Ludovic to stop the pcsc daemon from
  unnecessarily waking up.

* Mon Jul 31 2006 Ray Strode <rstrode@redhat.com> - 1.3.1-6
- follow packaging guidelines for setting up init service
  (bug 200778)

* Sun Jul 24 2006 Bob Relyea <rrelyea@redhat.com> - 1.3.1-5
- start pcscd when pcsc-lite is installed

* Sun Jul 16 2006 Florian La Roche <laroche@redhat.com> - 1.3.1-4
- fix excludearch line

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.3.1-3.1
- rebuild

* Mon Jul 10 2006 Bob Relyea <rrelyea@redhat.com> - 1.3.1-3
- remove s390 from the build

* Mon Jun 5 2006 Bob Relyea <rrelyea@redhat.com> - 1.3.1-2
- Move to Fedora Core. 
- Remove dependency on graphviz. 
- Removed %%{_dist}

* Sat Apr 22 2006 Ville Skytta <ville.skytta at iki.fi> - 1.3.1-1
- 1.3.1.

* Sun Mar  5 2006 Ville Skytta <ville.skytta at iki.fi> - 1.3.0-1
- 1.3.0, init script and reader.conf updater included upstream.
- Split developer docs into a -doc subpackage, include API docs.
- libmusclecard no longer included, split into separate package upstream.

* Mon Feb 13 2006 Ville Skytta <ville.skytta at iki.fi> - 1.2.0-14
- Avoid standard rpaths on multilib archs.
- Fine tune dependencies.

* Fri Nov 11 2005 Ville Skytta <ville.skytta at iki.fi> - 1.2.0-13
- Don't ship static libraries.
- Don't mark the init script as a config file.
- Use rm instead of %%exclude.
- Specfile cleanups.

* Thu May 19 2005 Ville Skytta <ville.skytta at iki.fi> - 1.2.0-12
- Rebuild.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 1.2.0-11
- rebuilt

* Tue Aug 17 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-10
- Disable dependency tracking to speed up the build.
- Drop reader.conf patch, it's not needed any more.
- Rename update-reader-conf to update-reader.conf for consistency with Debian,
  and improve it a bit.

* Sat Jul 31 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.9
- Add update-reader-conf, thanks to Fritz Elfert.

* Thu Jul  1 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.8
- Own the %%{_libdir}/pcsc hierarchy.

* Thu May 13 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.7
- Make main package require pcsc-ifd-handler (idea from Debian).

* Wed May 12 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.6
- Improve package summary.
- Improvements based on suggestions from Ludovic Rousseau:
  - Don't install pcsc_demo but do include its source in -devel.
  - Sync reader.conf with current upstream CVS HEAD (better docs, less
    intrusive in USB-only setups where it's not needed).

* Fri Apr 16 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.5
- Move PDF API docs to -devel.
- Improve main package and init script descriptions.

* Thu Jan 29 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.4
- Init script fine tuning.

* Fri Jan  9 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.3
- BuildRequires libusb-devel 0.1.6 or newer.

* Thu Oct 30 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.2
- s/pkgconfi/pkgconfig/ in -devel requirements.

* Tue Oct 28 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.1
- Update to 1.2.0.
- Add libpcsc-lite and libmusclecard provides to -libs and -devel.

* Thu Oct 16 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.0.2.rc3
- Update to 1.2.0-rc3.
- Trivial init script improvements.
- Enable %%{_smp_mflags}.
- Don't bother trying to enable SCF.

* Sun Sep 14 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.0.2.rc2
- Specfile cleanups.

* Fri Sep  5 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.0.1.rc2
- Update to 1.2.0-rc2.

* Wed Aug 27 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.2.0-0.fdr.0.1.rc1
- Update to 1.2.0-rc1.

* Sun Jun  1 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.1.2-0.fdr.0.1.beta5
- Update to 1.1.2beta5.

* Sat May 24 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.1.2-0.fdr.0.1.beta4
- First build, based on PLD's 1.1.1-2.
