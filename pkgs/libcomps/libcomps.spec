%global momorel 1
%global commit 475cba38b309cada09b0119f31bc0dc2a6fbd676

Name:           libcomps
Version:        0.1.6
Release:        %{momorel}m%{?dist}
Summary:        Comps XML file manipulation library

Group:          Development/Libraries
License:        GPLv2+
URL:            https://github.com/midnightercz/libcomps/
Source0:        https://github.com/midnightercz/libcomps/archive/%{commit}/libcomps-%{commit}.tar.gz
NoSource:       0
BuildRequires:  libxml2-devel
BuildRequires:  check-devel
BuildRequires:  expat-devel
BuildRequires:  cmake

%description
Libcomps is library for structure-like manipulation with content of
comps XML files. Supports read/write XML file, structure(s) modification.

%package doc
Summary:        Documentation files for libcomps library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch
BuildRequires:  doxygen

%description doc
Documentation files for libcomps library

%package devel
Summary:        Development files for libcomps library
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development files for libcomps library

%package -n python-libcomps
Summary:        Python2 bindings for libcomps library
Group:          Development/Libraries
BuildRequires:  python-devel
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description -n python-libcomps
Python2 bindings for libcomps library

%package -n python3-libcomps
Summary:        Python3 bindings for libcomps library
Group:          Development/Libraries
BuildRequires:  python3-devel
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description -n python3-libcomps
Python3 bindings for libcomps library


%prep
%setup -qn %{name}-%{commit}

rm -rf py3
mkdir ../py3
cp -a . ../py3/
mv ../py3 ./

%build
%cmake -DPYTHON_DESIRED:STRING=2 libcomps/
make %{?_smp_mflags}
make %{?_smp_mflags} docs

pushd py3
%cmake -DPYTHON_DESIRED:STRING=3 libcomps/ -DPYTHON_INCLUDE_DIR=/usr/include/python3.4m -DPYTHON_LIBRARY=/%{_libdir}/libpython3.4m.so
make %{?_smp_mflags}
make %{?_smp_mflags} docs
popd


%check devel
    make test

%install
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}%{_libdir}/python2*/libcomps/__pycache__
pushd py3
rm -rf %{buildroot}%{_libdir}/python3*/libcomps/__pycache__
make install DESTDIR=%{buildroot}
popd

%clean
rm -rf $buildroot

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%{_libdir}/libcomps.so.*
%doc README.md COPYING

%files devel
%{_libdir}/libcomps.so
%{_includedir}/*

%files doc
%doc docs/libcomps-doc/html

%files -n python-libcomps
%{_libdir}/python2*

%files -n python3-libcomps
%{_libdir}/python3*


%changelog
* Mon Mar 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-1m)
- update 0.1.6

* Wed Jan 08 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.5-1m)
- Initial commit Momonga Linux

* Tue Dec 03 2013 Jindrich Luza <jluza@redhat.com> 0.1.5.gite77f33d
- dict.get() exception error fixed

* Wed Oct 23 2013 Jindrich Luza <jluza@redhat.com> 0.1.4-4
- group.uservisible is true by default now.

* Tue Oct 08 2013 Jindrich Luza <jluza@redhat.com> 0.1.5
- version bump

* Tue Oct 01 2013 Jindrich Luza <jluza@redhat.com> 0.1.4
- added missing files
- added missing files
- version bumped. Python bindings is now easier.
- architectural redesign finished
- fixed #1003986 by Gustavo Luiz Duarte guidelines (but not tested on ppc)
- fixed bug #1000449
- fixed bug #1000442
- added GroupId.default test
- some minor unreported bugs discovered during testing fixed

* Tue Aug 20 2013 Jindrich Luza <jluza@redhat.com> 0.1.3
- finished default attribute support in groupid object
- Comps.get_last_parse_errors and Comps.get_last_parse_log has been renamed
-   as Comps.get_last_errors and Comps.get_last_log
- finished default attribute support in groupid object
- Comps.get_last_parse_errors and Comps.get_last_parse_log has been renamed
-   as Comps.get_last_errors and Comps.get_last_log

* Thu Jul 18 2013 Jindrich Luza <jluza@redhat.com> 0.1.2
- automatic changelog system
- fixed issue #14
- libcomps.Dict is now behave more like python dict. Implemented iter(libcomps.Dict)
- libcomps.iteritems() and libcomps.itervalues()
- added <packagereq requires=...> support
- remaked error reporting system.
-     libcomps.Comps.fromxml_f and libcomps.Comps.fromxml_str now return
-     -1, 0 or 1. 0 means parse procedure completed without any problem,
-     1 means there's some errors or warnings but not fatal. -1 indicates
-     fatal error problem (some results maybe given, but probably incomplete
-     and invalid)
- errors catched during parsing can be obtained by calling
-     libcomps.Comps.get_last_parse_errors
- all log is given by
-     libcomps.Comps.get_last_parse_log
- improved integrated tests
- prop system complete
- fixed issue 1
- fixed issue 3
- new prop system in progress....
- separated doc package
- some minor fixes in CMakeFiles

* Tue Jun 25 2013 Jindrich Luza <jluza@redhat.com> 0.1.1-1
- Automatic commit of package [libcomps] release [0.1.1-1].


