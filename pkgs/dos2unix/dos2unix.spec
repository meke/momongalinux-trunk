%global         momorel 1

Summary:        Text file format converter
Name:           dos2unix
Version:        6.0.3
Release:        %{momorel}m%{?dist}
Group:          Applications/Text
License:        BSD
URL:            http://www.xs4all.nl/~waterlan/dos2unix.html
Source0:        http://www.xs4all.nl/~waterlan/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gettext
Obsoletes:      unix2dos < %{version}
Provides:       unix2dos = %{version}

%description
Dos2unix converts DOS or MAC text files to UNIX format.
Unix2dos converts UNIX text files to DOS or MAC format.
Since versin 5.0, Dos2unix and Unix2dos have been bundled in a single package.

%prep
%setup -q

find . -name '*.txt' -exec chmod ugo-x {} \;

%build
make CFLAGS="$RPM_OPT_FLAGS -D_LARGEFILE_SOURCE $(getconf LFS_CFLAGS)" \
  %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

# remove duplicate doc dir
rm -rf %{buildroot}%{_docdir}/%{name}-%{version}-beta%{betaver}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,0755)
%doc COPYING.txt ChangeLog.txt INSTALL.txt
%doc NEWS.txt README.txt TODO.txt
%{_bindir}/dos2unix
%{_bindir}/mac2unix
%{_bindir}/unix2dos
%{_bindir}/unix2mac
%{_mandir}/man1/*.1*
%{_mandir}/es/man1/*.1*
%{_mandir}/nl/man1/*.1*

%changelog
* Mon Jan 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.3-1m)
- update to 6.0.3

* Thu Aug 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.1-1m)
- update to 5.3.1

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3-1m)
- update to 5.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.1-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.2-0.2.1m)
- update to 5.1.2-beta2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.1-3m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.1.1-2m)
- modify %%files

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.1-1m)
- update to 5.1.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1-1m)
- update to 5.1
- switch License to BSD

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- rebuild against gcc43

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1-1m)
- import from FC

* Tue Feb  6 2007 Tim Waugh <twaugh@redhat.com> 3.1-28
- Fixed build root (bug #225706).
- Build with SMP flags (bug #225706).
- Use dist in release tag (bug #225706).
- Fixed macros in changelog (bug #225706).
- Preserve timestamps when using install (bug #225706).

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 3.1-27.1
- rebuild

* Mon Jul 10 2006 Tim Waugh <twaugh@redhat.com> 3.1-27
- Re-encoded spec file in UTF-8 (bug #197817).

* Mon Jun  5 2006 Tim Waugh <twaugh@redhat.com> 3.1-26
- Rebuilt.

* Thu Jun  1 2006 Tim Waugh <twaugh@redhat.com> 3.1-25
- Build with large file support.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 3.1-24.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3.1-24.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Apr 13 2005 Tim Waugh <twaugh@redhat.com> 3.1-24
- Fixed tmppath patch (bug #150277).

* Thu Mar  3 2005 Mike A. Harris <mharris@redhat.com> 3.1-23
- Bump and rebuild for FC4, using gcc 4.

* Tue Feb  8 2005 Mike A. Harris <mharris@redhat.com> 3.1-22
- Bump and rebuild for FC4

* Wed Oct 20 2004 Miloslav Trmac <mitr@redhat.com> - 3.1-21
- Don't just delete the original file when destination and current directory
  are on different filesystems (#65548, #123069, patch by James Antill)
- Fix return type of StripDelimiter in dos2unix-3.1-safeconv.patch (#136148)

* Wed Oct  6 2004 Mike A. Harris <mharris@redhat.com> 3.1-20
- Added dos2unix-3.1-manpage-update-57507.patch to fix manpage (#57507)
- Added dos2unix-3.1-preserve-file-modes.patch to properly preserve file
  permissions (#91331,55183,112710,132145)

* Sun Sep 26 2004 Rik van Riel <riel@redhat.com> 3.1-19
- safer conversion w/ mac2unix (fix from bz #57508)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Feb 05 2003 Elliot Lee <sopwith@redhat.com> 3.1-15
- Remove build dependency on perl, since perl BuildRequires: dos2unix,
  and there's no good reason not to just use sed here.

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Mon Oct  7 2002 Mike A. Harris <mharris@redhat.com> 3.1-13
- All-arch rebuild
- Added BuildRequires: perl

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Feb 28 2002 Trond Eivind Glomsrod <teg@redhat.com> 3.1-10
- Build in new environment

* Thu Jan 17 2002 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix bug #57700 (segfault)
- Add the mac2unix symlink recommended in README
- Fix compiler warnings

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Wed Jan 10 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- shut up rpmlint

* Fri Nov 17 2000 Tim Powers <timp@redhat.com>
- use mkstemp instead of mktemp. Not much needed to change.

* Thu Nov 16 2000 Tim Powers <timp@redhat.com>
- cleaned up specfile a bit
- built for 7.1

* Tue Jul 07 1999 Peter Soos <sp@osb.hu> 
- Added Hungarian "Summary:" and "%%description" 
- Corrected the file and directory attributes to rebuild the package 
  under RedHat Linux 6.0
