%global momorel 6

Name:           rzip
Version:        2.1
Release:        %{momorel}m%{?dist}
Summary:        A large-file compression program
Group:          Applications/File
License:        GPL
URL:            http://rzip.samba.org
Source0:        http://rzip.samba.org/ftp/rzip/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  bzip2-devel

%description
rzip is a compression program, similar in functionality to gzip or
bzip2, but able to take advantage of long distance redundancies in
files, which can sometimes allow rzip to produce much better
compression ratios than other programs.

%prep
%setup -q

%build
export CFLAGS="${RPM_OPT_FLAGS} -fPIE -pie"
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install INSTALL_BIN=$RPM_BUILD_ROOT%{_bindir} INSTALL_MAN=$RPM_BUILD_ROOT%{_mandir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/rzip
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-1m)
- import from Fedora to Momonga

* Sun Feb 24 2008  Paul P Komkoff Jr <i@stingr.net> - 2.1-3
- make rzip a PIE

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.1-2
- Autorebuild for GCC 4.3

* Sat Dec 30 2006 Paul P Komkoff Jr <i@stingr.net> - 2.1-1
- Added -L compression level option
- minor portability fixes
- fixed a bug that could cause some files to not be able to be uncompressed

* Sun Sep 10 2006 Paul P Komkoff Jr <i@stingr.net> - 2.0-3
- rebuild

* Sun Feb 19 2006 Paul P Komkoff Jr <i@stingr.net> - 2.0-2
- rebuild

* Sun Apr 24 2005 Paul P Komkoff Jr <i@stingr.net> - 2.0-1
- initial import.
