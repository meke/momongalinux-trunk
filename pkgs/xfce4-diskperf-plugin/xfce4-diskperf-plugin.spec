%global momorel 1

%global xfce4ver 4.10.0
%global major 2.5

Summary: 	XFce4 disk performance monitor plugin
Name: 		xfce4-diskperf-plugin
Version: 	2.5.4
Release:	%{momorel}m%{?dist}

Group: 		User Interface/Desktops
License:	BSD 
URL:		http://goodies.xfce.org/panel-plugins/{%name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk2-devel
BuildRequires:  libxml2-devel
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxfce4util-devel >= %{xfce4ver}
BuildRequires:  pkgconfig
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}

Requires:	xfce4-panel >= %{xfce4ver}

%description 
The DiskPerf plugin displays disk/partition performance (bytes transfered per
second) based on data provided by the kernel.

%prep
%setup -q

%build
%configure --disable-static LIBS="-lm"
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/xfce4/panel/plugins/libdiskperf.so
%{_datadir}/xfce4/*
%{_datadir}/locale/*/*/*

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.4-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.0-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-9m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-6m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-5m)
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-4m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-4m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-7m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-6m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-5m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-4m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-3m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-2m)
- rebuild against xfce4 4.1.90

* Sun Sep  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-1m)
- version up

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.1-4m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.1-3m)
- rebuild against for libxml2-2.6.8

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-2m)
- rebuild against xfce4 4.0.3

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1
- rebuild against xfce4 4.0.1

* Wed Oct 29 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.1-1m)
- version 1.1

* Wed Oct 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-2m)
- revise License

* Tue Oct 14 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-1m)
- first import to Momonga

