%global momorel 1

# configure options
%global   with_snmp       1
%global   with_vtysh      1
%global   with_ospf_te    1
%global   with_nssa       1
%global   with_opaque_lsa 1
%global   with_tcp_zebra  0
%global   with_pam        0
%global   with_ipv6       1
%global   with_ospfclient 1
%global   with_ospfapi    1
%global   with_rtadv      1
%global   with_multipath  64
%global   quagga_uid      92
%global   quagga_gid      92
%global   quagga_user     quagga
%global   vty_group       quaggavt
%global   vty_gid         85

# path globals
%global   _sysconfdir /etc/quagga
%global   zeb_src     %{_builddir}/%{name}-%{version}
%global   zeb_rh_src  %{zeb_src}/redhat
%global   zeb_docs    %{zeb_src}/doc

# globals for configure
%global   _libexecdir %{_exec_prefix}/libexec/quagga
%global   _includedir %{_prefix}/include
%global   _libdir     %{_exec_prefix}/%{_lib}/quagga
%global   _localstatedir /var/run/quagga

Summary: Routing daemon
Name: quagga
Version: 0.99.22.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://www.quagga.net/
Source0: http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Source1: quagga-filter-perl-requires.sh
Source2: quagga-tmpfs.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Initscripts > 5.60 is required for IPv6 support
Requires(pre): shadow-utils
Requires(post): coreutils
Requires(post): info
Requires(post): systemd-units
Requires(postun): coreutils
Requires(postun): info
Requires(postun): systemd-units
Requires(preun): coreutils
Requires(preun): info
Requires(preun): systemd-units
Requires: initscripts >= 5.60
Requires: ncurses
Requires: pam
%if %with_snmp
Requires: net-snmp
BuildRequires: net-snmp-devel >= 5.4
%endif
%if %with_vtysh
BuildRequires: readline-devel >= 5.0
BuildRequires: ncurses-devel >= 5.6-10
%endif
BuildRequires: autoconf
BuildRequires: libcap-devel
BuildRequires: pam-devel
BuildRequires: patch
BuildRequires: tetex
BuildRequires: texi2html
BuildRequires: texinfo
Provides: routingdaemon
Obsoletes: bird
Obsoletes: gated
Obsoletes: mrt
Obsoletes: zebra

%define __perl_requires %{SOURCE1}

%description
Quagga is a free software that manages TCP/IP based routing
protocol. It takes multi-server and multi-thread approach to resolve
the current complexity of the Internet.

Quagga supports BGP4, BGP4+, OSPFv2, OSPFv3, RIPv1, RIPv2, and RIPng.

Quagga is intended to be used as a Route Server and a Route Reflector. It is
not a toolkit, it provides full routing power under a new architecture.
Quagga by design has a process for each protocol.

Quagga is a fork of GNU Zebra.

%package sysvinit
Group: System Environment/Daemons
Summary: SysV initscript for quagga routing daemons
Requires: %{name} = %{version}-%{release}
Requires(preun): initscripts
Requires(postun): initscripts

%description sysvinit
The quagga-sysvinit contains SysV initscritps support.

%package contrib
Summary: contrib tools for quagga
Group: System Environment/Daemons

%description contrib
Contributed/3rd party tools which may be of use with quagga.

%package devel
Summary: Header and object files for quagga development
Group: System Environment/Daemons

%description devel
The quagga-devel package contains the header and object files neccessary for
developing OSPF-API and quagga applications.

%prep
%setup  -q

%build
export RPM_OPT_FLAGS=${RPM_OPT_FLAGS//-fstack-protector/-fstack-protector-all}
#./autogen.sh
export CFLAGS="$RPM_OPT_FLAGS $CPPFLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS $CPPFLAGS"

%configure \
%if %with_ipv6
	--enable-ipv6=yes \
%endif
%if %with_snmp
	--enable-snmp=yes \
%endif
%if %with_multipath
	--enable-multipath=%with_multipath \
%endif
%if %with_tcp_zebra
	--enable-tcp-zebra \
%endif
%if %with_nssa
	--enable-nssa \
%endif
%if %with_opaque_lsa
	--enable-opaque-lsa \
%endif
%if %with_ospf_te
	--enable-ospf-te \
%endif
%if %with_vtysh
	--enable-vtysh=yes \
%endif
%if %with_ospfclient 
	--enable-ospfclient=yes \
%else
	--enable-ospfclient=no\
%endif
%if %with_ospfapi
	--enable-ospfapi=yes \
%else
	--enable-ospfapi=no \
%endif
%if %with_pam
	--with-libpam \
%endif
%if %quagga_user
	--enable-user=%quagga_user \
	--enable-group=%quagga_user \
%endif
%if %vty_group
	--enable-vty-group=%vty_group \
%endif
%if %with_rtadv
	--enable-rtadv \
%endif
--enable-netlink

for X in Makefile */Makefile ; do perl -pe 's/^COMPILE \= \$\(CC\) /COMPILE = \$(CC) -fPIE /;s/^LDFLAGS = $/LDFLAGS = -pie/' < $X > $X.tmp ; mv $X.tmp $X ; done

make %{?_smp_mflags} MAKEINFO="makeinfo --no-split"

pushd doc
texi2html quagga.texi
popd

%install
rm -rf --preserve-root %{buildroot}

install -d %{buildroot}/etc/{init.d,sysconfig,logrotate.d,pam.d} \
	%{buildroot}/var/log/quagga %{buildroot}%{_infodir}
mkdir -p %{buildroot}%{_prefix}/lib/systemd/system

make install transform='s,x,x,' DESTDIR=%{buildroot}

# Remove this file, as it is uninstalled and causes errors when building on RH9
rm -rf %{buildroot}/usr/share/info/dir

install -m 644 %{zeb_rh_src}/zebra.service %{buildroot}%{_prefix}/lib/systemd/system
install -m 644 %{zeb_rh_src}/isisd.service %{buildroot}%{_prefix}/lib/systemd/system
install -m 644 %{zeb_rh_src}/ripd.service %{buildroot}%{_prefix}/lib/systemd/system
install -m 644 %{zeb_rh_src}/ospfd.service %{buildroot}%{_prefix}/lib/systemd/system
install -m 644 %{zeb_rh_src}/bgpd.service %{buildroot}%{_prefix}/lib/systemd/system
install -m 644 %{zeb_rh_src}/babeld.service %{buildroot}%{_prefix}/lib/systemd/system
%if %with_ipv6
install -m 644 %{zeb_rh_src}/ospf6d.service %{buildroot}%{_prefix}/lib/systemd/system
install -m 644 %{zeb_rh_src}/ripngd.service %{buildroot}%{_prefix}/lib/systemd/system
%endif

install %{zeb_rh_src}/zebra.init %{buildroot}/etc/init.d/zebra
install %{zeb_rh_src}/bgpd.init %{buildroot}/etc/init.d/bgpd
%if %with_ipv6
install %{zeb_rh_src}/ospf6d.init %{buildroot}/etc/init.d/ospf6d
install %{zeb_rh_src}/ripngd.init %{buildroot}/etc/init.d/ripngd
%endif
install %{zeb_rh_src}/ospfd.init %{buildroot}/etc/init.d/ospfd
install %{zeb_rh_src}/ripd.init %{buildroot}/etc/init.d/ripd
install -m644 %{zeb_rh_src}/quagga.sysconfig %{buildroot}/etc/sysconfig/quagga
install -m644 %{zeb_rh_src}/quagga.pam %{buildroot}/etc/pam.d/quagga
install -m644 %{zeb_rh_src}/quagga.logrotate %{buildroot}/etc/logrotate.d/quagga
install -d -m770  %{buildroot}/var/run/quagga

install -d -m 755 %{buildroot}/etc/tmpfiles.d
install -p -m 644 %{SOURCE2} %{buildroot}/etc/tmpfiles.d/quagga.conf

find %{buildroot} -name "*.la" -delete

%pre
# add vty_group
%if %vty_group
groupadd -g %vty_gid -r %vty_group 2> /dev/null || :
%endif
# add quagga user and group
%if %quagga_user
# Ensure that quagga_gid gets correctly allocated
if getent group %quagga_user >/dev/null 2>&1 ; then : ; else \
 /usr/sbin/groupadd -g %quagga_gid %quagga_user > /dev/null 2>&1 || exit 1 ; fi
if getent passwd %quagga_user >/dev/null 2>&1 ; then : ; else \
 /usr/sbin/useradd -u %quagga_uid -g %quagga_gid -M -r -s /sbin/nologin \
 -c "Quagga routing suite" -d %_localstatedir %quagga_user 2> /dev/null \
 || exit 1 ; fi
%endif

%post
%{_bindir}/systemctl daemon-reload >/dev/null 2>&1 || :

if [ -f %{_infodir}/%{name}.inf* ]; then
        %{_sbindir}/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

# Create dummy files if they don't exist so basic functions can be used.
if [ ! -e %{_sysconfdir}/zebra.conf ]; then
        echo "hostname `hostname`" > %{_sysconfdir}/zebra.conf
%if %quagga_user
        chown %quagga_user:%quagga_user %{_sysconfdir}/zebra.conf
%endif
        chmod 640 %{_sysconfdir}/zebra.conf
fi
if [ ! -e %{_sysconfdir}/vtysh.conf ]; then
        touch %{_sysconfdir}/vtysh.conf
        chmod 640 %{_sysconfdir}/vtysh.conf
        chown %{quagga_user}:%{vty_group} %{_sysconfdir}/vtysh.conf
fi

%postun
%{_bindir}/systemctl daemon-reload >/dev/null 2>&1 || :
if [ -f %{_infodir}/%{name}.inf* ]; then
        %{_sbindir}/install-info --delete %{_infodir}/quagga.info %{_infodir}/dir || :
fi


%preun
if [ "$1" = "0" ]; then
        %{_bindir}/systemctl disable ripd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop ripd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl disable bgpd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop bgpd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl disable zebra.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop zebra.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl disable isisd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop isisd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl disable ospfd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop ospfd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl disable watchquagga.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop watchquagga.service > /dev/null 2>&1 || :
%if %with_ipv6
        %{_bindir}/systemctl disable ospf6d.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop ospf6d.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl disable ripngd.service > /dev/null 2>&1 || :
        %{_bindir}/systemctl stop ripngd.service > /dev/null 2>&1 || :
%endif
fi

%triggerun --  %{name} < 0.99.18-1m
        %{_sbindir}/chkconfig --del zebra >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --del isisd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --del ripd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --del bgpd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --del ospfd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --del watchquagga >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart zebra.service >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart isisd.service >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart ripd.service >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart bgpd.service >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart ospfd.service >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart watchquagga.service >/dev/null 2>&1 || :
%if %with_ipv6
        %{_sbindir}/chkconfig --del ospf6d >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --del ripngd >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart ospf6d.service >/dev/null 2>&1 || :
        %{_bindir}/systemctl try-restart ripngd.service >/dev/null 2>&1 || :
%endif

%triggerpostun -n %{name}-sysvinit -- %{name} < 0.99.18-1m
        %{_sbindir}/chkconfig --add zebra >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --add isisd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --add ripd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --add bgpd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --add ospfd >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --add watchquagga >/dev/null 2>&1 || :
%if %with_ipv6
        %{_sbindir}/chkconfig --add ospf6d >/dev/null 2>&1 || :
        %{_sbindir}/chkconfig --add ripngd >/dev/null 2>&1 || :
%endif

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc */*.sample* AUTHORS COPYING
%doc doc/quagga.html
%doc doc/mpls
%doc ChangeLog INSTALL NEWS README REPORTING-BUGS SERVICES TODO
%if %quagga_user
%dir %attr(751,%quagga_user,%quagga_user) %{_sysconfdir}
%dir %attr(770,%quagga_user,%quagga_user) /var/log/quagga 
%dir %attr(771,%quagga_user,%quagga_user) /var/run/quagga
%else
%dir %attr(750,root,root) %{_sysconfdir}
%dir %attr(750,root,root) /var/log/quagga
%dir %attr(755,root,root) /usr/share/info
%dir %attr(750,root,root) /var/run/quagga
%endif
%if %vty_group
%attr(644,%quagga_user,%vty_group) %{_sysconfdir}/vtysh.conf.sample
%endif
%{_infodir}/*info*
%{_mandir}/man*/*
%{_sbindir}/*
%if %with_vtysh
%{_bindir}/*
%endif
%dir %{_libdir}
%{_libdir}/*.so*
%config /etc/quagga/[!v]*
%config(noreplace) /etc/pam.d/quagga
%config(noreplace) %attr(640,root,root) /etc/logrotate.d/quagga
%config(noreplace) /etc/sysconfig/quagga                  
%config(noreplace) /etc/tmpfiles.d/quagga.conf
%{_unitdir}/*.service

%files sysvinit
%attr(755,root,root) /etc/init.d/*

%files contrib
%defattr(-,root,root)
%doc tools

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%dir %{_includedir}/quagga
%{_includedir}/quagga/*.h
%dir %{_includedir}/quagga/ospfd
%{_includedir}/quagga/ospfd/*.h
%if %with_ospfapi
%dir %{_includedir}/quagga/ospfapi
%{_includedir}/quagga/ospfapi/*.h
%endif

%changelog
* Sat Mar  8 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.22.4-1m)
- version 0.99.22.4

* Sat Mar  8 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.22-2m)
- adapt new directory rules

* Tue Mar 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.22-1m)
- update to 0.99.22

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.21-1m)
- [SECURITY] CVE-2012-1820
- update to 0.99.21

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.20.1-1m)
- [SECURITY] CVE-2012-0249 CVE-2012-0250 CVE-2012-0255
- update to 0.99.20.1

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.20-2m)
- rebuild against net-snmp-5.7.1

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.20-1m)
- update 0.99.20
- [SECURITY] CVE-2011-3323 CVE-2011-3324 CVE-2011-3325
- [SECURITY] CVE-2011-3326 CVE-2011-3327

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.18-1m)
- update 0.99.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.15-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.15-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.15-5m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.15-4m)
- rebuild against readline6

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.15-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.15-1m)
- update to 0.99.15

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.13-1m)
- [SECURITY] CVE-2009-1572
- update to 0.99.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.11-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.11-1m)
- update to 0.99.11
-- update Patch1 for fuzz=0

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.10-2m)
- fix install-info

* Wed Sep 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.10-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.9-4m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.9-3m)
- no use libtermcap

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.99.9-2m)
- rebuild against perl-5.10.0-1m

* Sat Sep 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.9-1m)
- [SECURITY] SA26744, Quagga Multiple Denial of Service Vulnerabilities
- update to 0.99.9
- do not use %%NoSouce macro

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.7-2m)
- rebuild against net-snmp

* Thu Jun  7 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.99.7-1m)
- [SECURITY] CVE-2007-1995
- update to 0.99.7
- remove Patch0 quagga-0.96.2-lib64.patch
- remove Patch5 quagga-0.98.5-pam.patch
- remove Patch6 quagga-0.98.5-pie.patch

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.98.6-4m)
- delete libtool library

* Mon Aug  7 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.98.6-3m)
- add transform at makeinstall

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.98.6-2m)
- rebuild against readline-5.0

* Mon May 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.98.6-1m)
- update 0.98.6
- [SECURITY] http://www.quagga.net/news2.php?y=2006&m=5&d=8
- CVE-2006-2223 CVE-2006-2224 CVE-2006-2276

* Fri Mar  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.98.5-2m)
- modify BuildRequires

* Tue Feb 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.98.5-1m)
- import from fc-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0:0.98.5-3.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0:0.98.5-3.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Oct 19 2005 Jay Fenlason <fenlason@redhat.com> 0.98.5-3
- add the -pie patch, to make -fPIE compiling actually work on all platforms.
- Include -pam patch to close
  bz#170256 ? pam_stack is deprecated
- Change ucd-snmp to net-snmp to close
  bz#164333 ? quagga 0.98.4-2 requires now obsolete package ucd-snmp-devel
- also fix duplicate line mentioned in bz#164333

* Mon Aug 29 2005 Jay Fenlason <fenlason@redhat.com> 0.98.5-2
- New upstream version.

* Mon Jun 27 2005 Jay Fenlason <fenlason@redhat.com> 0.98.4-2
- New upstream version.

* Mon Apr 4 2005 Jay Fenlason <fenlason@redhat.com> 0.98.3-2
- new upstream verison.
- remove the -bug157 patch.

* Tue Mar 8 2005 Jay Fenlason <fenlason@redhat.com> 0.98.2-2
- New upstream version, inclues my -gcc4 patch, and a patch from
  Vladimir Ivanov <wawa@yandex-team.ru>

* Sun Jan 16 2005 Jay Fenlason <fenlason@redhat.com> 0.98.0-2
- New upstream version.  This fixes bz#143796
- --with-rtadv needs to be --enable-rtadv according to configure.
  (but it was enabled by default, so this is a cosmetic change only)
  Change /etc/logrotate.d/* to /etc/logrotate.d/quagga in this spec file.
- Modify this spec file to move the .so files to the base package
  and close bz#140894
- Modify this spec file to separate out %{_includedir}/quagga/ospfd
  from .../*.h and %{_includedir}/quagga/ospfapi from .../*.h
  Rpmbuild probably shouldn't allow %dir of a plain file.

* Wed Jan 12 2005 Tim Waugh <twaugh@redhat.com> 0.97.3-3
- Rebuilt for new readline.
- Don't explicitly require readline.  The correct dependency is automatically
  picked up by linking against it.

* Fri Nov 26 2004 Florian La Roche <laroche@redhat.com>
- remove target buildroot again to not waste disk space

* Wed Nov 10 2004 Jay Fenlason <fenlason@redhat.com> 0.97.3-1
- New upstream version.

* Tue Oct 12 2004 Jay Fenlason <fenlason@redhat.com> 0.97.2-1
- New upstream version.

* Fri Oct 8 2004 Jay Fenlason <fenlason@redhat.com> 0.97.0-1
- New upstream version.  This obsoletes the -lib64 patch.

* Wed Oct 6 2004 Jay Fenlason <fenlason@redhat.com>
- Change the permissions of /var/run/quagga to 771 and /var/log/quagga to 770
  This closes #134793

* Thu Sep  9 2004 Bill Nottingham <notting@redhat.com> 0.96.5-2
- don't run by default

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May 4 2004 Jay Fenlason <fenlason@redhat.com> 0.96.5-0
- New upstream version
- Change includedir
- Change the pre scriptlet to fail if the useradd command fails.
- Remove obsolete patches from this .spec file and renumber the two
  remaining ones.

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Jay Fenlason <fenlason@redhat.com>
- Add --enable-rtadv, to turn ipv6 router advertisement back on.  Closes
  bugzilla #114691

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Nov 3 2003 Jay Fenlason <fenlason@redhat.com> 0.96.4-0
- New upstream version
- include .h files in the -devel package.

* Wed Oct 15 2003 Jay Fenlason <fenlason@redhat.com> 0.96.3-1
- Patch a remote DoS attack (#107140)
- New upstream version
- Removed the .libcap patch, which was applied upstream
- Renamed the vty group from quaggavty to quaggavt because quaggavty is
  too long for NIS.
- Patch the spec file to fix #106857: /etc/quagga/zebra.conf is created with
  the wrong owner.
- Removed the "med" part of the 0.96.1-warnings patch for . . .
- Add a new warnings patch with a fix for #106315.  This also updates
  the "med" part of the previous warnings patch.

* Mon Sep 22 2003 Jay Fenlason <fenlason@redhat.com> 0.96.2-5
- merge sysconfig patch from 3E branch.  This patch is from Kaj J. Niemi
  (kajtzu@basen.net), and puts Quagga configuration options in
  /etc/sysconfig/quagga, and defaults Quagga to listening on 127.0.0.1 only.
  This closes #104376
- Use /sbin/nologin for the shell of the quagga user.  This closes #103320
- Update the quagga-0.96.1-warnings.patch patch to kill a couple more
  warnings.

* Mon Sep 22 2003 Nalin Dahyabhai <nalin@redhat.com>
- Remove the directory part of paths for PAM modules in quagga.pam, allowing
  libpam to search its default directory (needed if a 64-bit libpam needs to
  look in /lib64/security instead of /lib/security).

* Tue Aug 26 2003 Jay Fenlason <fenlason@redhat.com> 0.96.2-1
- New upstream version

* Tue Aug 19 2003 Jay Fenlason <fenlason@redhat.com> 0.96.1-3
- Merge from quagga-3E-branch, with a fix for #102673 and a couple
  more compiler warnings quashed.

* Wed Aug 13 2003 Jay Fenlason <fenlason@redhat.com> 0.96-1
- added a patch (libwrap) to allow the generated Makefiles for zebra/
  and lib/ to work on AMD64 systems.  For some reason make there does
  not like a dependency on -lcap
- added a patch (warnings) to shut up warnings about an undefined
  structure type in some function prototypes and quiet down all the
  warnings about assert(ptr) on x86_64.
- Modified the upstream quagga-0.96/readhat/quagga.spec to work as an
  official Red Hat package (remove user and group creation and changes
  to services)
- Trimmed changelog.  See the upstream .spec file for previous
  changelog entries.
