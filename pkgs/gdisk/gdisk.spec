%global momorel 1
Summary:       A fdisk-like partitioning tool for GPT disks
Name:          gdisk
Version:       0.8.6
Release: %{momorel}m%{?dist}
License:       GPLv2
URL:           http://www.rodsbooks.com/gdisk/
Group:         System Environment/Base
Source0:       http://downloads.sourceforge.net/gptfdisk/gptfdisk-%{version}.tar.gz
NoSource:	0
Patch0:        gptfdisk-0.8.1-gcc47.patch
BuildRequires: popt-devel
BuildRequires: libicu-devel >= 52
BuildRequires: libuuid-devel
BuildRequires: ncurses-devel
BuildRoot:     %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
%description
A fdisk-like partitioning tool for GPT disks. GPT fdisk features a
command-line interface, fairly direct manipulation of partition table
structures, recovery tools to help you deal with corrupt partition
tables, and the ability to convert MBR disks to GPT format.

%prep
%setup -q -n gptfdisk-%{version}
%patch0 -p1
chmod 0644 gdisk_test.sh

%build
%{__make} CXXFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64"

%install
%{__rm} -rf %{buildroot}
for f in gdisk sgdisk cgdisk fixparts ; do 
    %{__install} -D -p -m 0755 $f %{buildroot}%{_sbindir}/$f
    %{__install} -D -p -m 0644 $f.8 %{buildroot}%{_mandir}/man8/$f.8
done

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc COPYING README gdisk_test.sh
%{_sbindir}/gdisk
%{_sbindir}/cgdisk
%{_sbindir}/sgdisk
%{_sbindir}/fixparts
%{_mandir}/man8/gdisk.8*
%{_mandir}/man8/cgdisk.8*
%{_mandir}/man8/sgdisk.8*
%{_mandir}/man8/fixparts.8*

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-1m)
- update 0.8.6

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-1m)
- reimport from fedora

* Tue Dec  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Sun Sep 04 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.7.2-2m)
- correct spec file errors

* Fri Aug 19 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.7.2-1m) 
- initial momonga release

* Mon Aug 23 2010 Dag Wieers <dag@wieers.com> - 0.6.10-1
- Updated to release 0.6.10.

* Mon Aug 16 2010 Dag Wieers <dag@wieers.com> - 0.6.8-1
- Initial package. (using DAR)
