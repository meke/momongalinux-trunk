%global momorel 1
%global ocamlver 3.12.1

%global debug_package %{nil}

Name:           ocaml-pxp
Version:        1.2.2
Release:        %{momorel}m%{?dist}
Summary:        Validating XML parser

Group:          Development/Libraries
License:        MIT
URL:            http://projects.camlcity.org/projects/pxp.html
Source0:        http://download.camlcity.org/download/pxp-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamlnet-devel >= 3.4.1-1m
BuildRequires:  ocaml-ulex-devel >= 1.1-9m
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m, pcre-devel
BuildRequires:  ocaml-camlp4-devel

%global __ocaml_requires_opts -i Asttypes -i Outcometree -i Parsetree

%description
PXP is a validating XML parser for O'Caml. It represents the parsed
document either as tree or as stream of events. In tree mode, it is
possible to validate the XML document against a DTD.

The acronym PXP means Polymorphic XML Parser. This name reflects the
ability to create XML trees with polymorphic type parameters.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n pxp-%{version}
./configure \
  -without-wlex \
  -without-wlex-compat \
  -lexlist all


%build
make all
make opt


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/pxp-engine/*.cma
%{_libdir}/ocaml/pxp-engine/*.cmi
%{_libdir}/ocaml/pxp-engine/*.cmo
%{_libdir}/ocaml/pxp-lex-*/*.cma
%{_libdir}/ocaml/pxp-lex-*/*.cmi
%{_libdir}/ocaml/pxp-lex-*/*.cmo
%{_libdir}/ocaml/pxp-ulex-utf8/*.cma
%{_libdir}/ocaml/pxp-ulex-utf8/*.cmi
%{_libdir}/ocaml/pxp-ulex-utf8/*.cmo
%{_libdir}/ocaml/pxp-pp/pxp_pp.cma


%files devel
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/pxp-engine/META
%{_libdir}/ocaml/pxp-engine/*.a
%{_libdir}/ocaml/pxp-engine/*.cmxa
%{_libdir}/ocaml/pxp-engine/*.mli
%{_libdir}/ocaml/pxp-lex-*/META
%{_libdir}/ocaml/pxp-lex-*/*.a
%{_libdir}/ocaml/pxp-lex-*/*.cmxa
%{_libdir}/ocaml/pxp-lex-*/*.cmx
%{_libdir}/ocaml/pxp-lex-*/*.o
%{_libdir}/ocaml/pxp-ulex-utf8/META
%{_libdir}/ocaml/pxp-ulex-utf8/*.a
%{_libdir}/ocaml/pxp-ulex-utf8/*.cmxa
%{_libdir}/ocaml/pxp-ulex-utf8/*.cmx
%{_libdir}/ocaml/pxp-ulex-utf8/*.o
%{_libdir}/ocaml/pxp-pp/META
%{_libdir}/ocaml/pxp/META


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-5m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-4m)
- rebuild against the following packages
- ocaml-3.11.2
- ocaml-ocamlnet-2.2.9-9m
- ocaml-pcre-6.1.0-1m
- ocaml-ulex-1.1-5m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against ocaml-pcre-6.0.0 and ocaml-ocamlnet-2.2.9-6m

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0test2-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0test2-2m)
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0test2-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test2-3
- ExcludeArch ppc64 (bz #443899).

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test2-2
- Rebuild for OCaml 3.10.2

* Mon Apr  2 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test2-1
- New upstream version 1.2.0test2.
- New upstream URL.
- Re-enabled camlp4 extension.

* Sun Mar  2 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test1-6
- Rebuild for ppc64.

* Fri Feb 15 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test1-5
- Added BR ocaml-camlp4-devel

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test1-4
- Added BR ocaml-pcre-devel, pcre-devel

* Thu Sep 13 2007 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test1-3
- ExcludeArch ppc64

* Thu Sep 13 2007 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test1-2
- Build on OCaml 3.10
- Disable building the preprocessor (requires old camlp4 or camlp5).
- License is BSD.
- Ignore Parsetree.

* Sat May 26 2007 Richard W.M. Jones <rjones@redhat.com> - 1.2.0test1-1
- Initial RPM release.
