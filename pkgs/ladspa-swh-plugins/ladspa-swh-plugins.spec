%global momorel 7

Summary: A collection of LADSPA plugins written by Steve Harris.
Name: ladspa-swh-plugins
Version: 0.4.15
Release: %{momorel}m%{?dist}
License: GPL
URL: http://plugin.org.uk/
Group: Applications/Multimedia
Source0:  http://plugin.org.uk/releases/%{version}/swh-plugins-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
Requires: fftw
BuildRequires: ladspa-devel
BuildRequires: fftw-devel

%description
A collection of LADSPA plugins written by Steve Harris.

%prep
%setup -q -n swh-plugins-%{version}

%build
autoreconf -ivf

%configure
%make

%install
mkdir -p %{buildroot}
make DESTDIR=%{buildroot} install plugindir=%{_libdir}/ladspa

%files
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog 
%doc INSTALL NEWS README TODO
%{_libdir}/ladspa/*.so
%{_datadir}/ladspa/rdf/*.rdf
%{_datadir}/locale/*/LC_MESSAGES/swh-plugins.mo

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.15-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.15-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.15-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.15-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.15-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.15-2m)
- fix build on x86_64

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.4.15-1m)
- Initial Build for Momonga Linux
