%global momorel 8

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define	section	devel

Name:		jrefactory
Version:	2.8.9
Release:	6jpp.%{momorel}m%{?dist}
Epoch:		0
Summary:	JRefactory and Pretty Print
License:	Apache
Group:		Development/Libraries
Source0:	%{name}-%{version}-full-RHCLEAN.zip
Patch0:	jrefactory-2.8.9-fixcrlf.patch
Patch1: jrefactory-savejpg.patch
Url:		http://jrefactory.sourceforge.net/
BuildRequires:	ant
BuildRequires:	jpackage-utils >= 0:1.5
%if ! %{gcj_support}
BuildArch:	noarch
%endif
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
JRefactory provides a variety of refactoring and pretty printing tools

%prep
%setup -q -c -n %{name}
mv settings/.Refactory settings/sample
%patch0 -p0 -b .fixcrlf
%patch1 -p1

rm -f src/org/acm/seguin/pmd/swingui/PMDLookAndFeel.java

# remove classes that don't build without said jarfiles
find -name '*.java' | \
    xargs grep -l '^import \(edu\|org\.\(jaxen\|saxpath\)\)\.' | \
        xargs rm

%build
perl -p -i -e 's|^Class-Path:.*||' \
	src/meta-inf/refactory.mf
ant -Dant.build.javac.source=1.4 jar

%install
# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 ant.build/lib/%{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar

(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} ${jar/-%{version}/}; done)

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0664,root,root,0755)
%doc docs/{*.html,*.jpg,*.gif,*.txt} settings/sample
%{_javadir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.9-6jpp.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.9-6jpp.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.9-6jpp.6m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9-6jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9-6jpp.4m)
- build with -Dant.build.javac.source=1.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9-6jpp.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.9-6jpp.2m)
- rebuild against gcc43

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.9-6jpp.1m)
- import from Fedora

* Thu Aug 10 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.8.9-6jpp.3
- Changed release to match new spec.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:2.8.9-6jpp_2fc
- Rebuilt

* Thu Jul 20 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.8.9-6jpp_1fc
- Added conditional native compilation.
- From gbenson@redhat:
-    Remove classes that don't build without said jarfiles.
-    Avoid Sun-specific classes.

* Fri Apr 28 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.8.9-5jpp
- First JPP 1.7 build

* Tue Apr 19 2005 Ralph Apel <r.apel at r-apel.de> - 0:2.8.9-4jpp
- Patch to fix CRLF problem; THX to Richard Bullington-McGuire

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:2.8.9-3jpp
- Rebuild with ant-1.6.2

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 0:2.8.9-2jpp
- Upgrade to Ant 1.6.X

* Wed Jan 21 2004 David Walluck <david@anti-microsoft.org> 0:2.8.9-1jpp
- 2.8.9
- remove Class-Path from manifest

* Mon Dec 15 2003 Paul Nasrat <pauln at truemesh.com> 0:2.6.40-1jpp
- Initial Release
