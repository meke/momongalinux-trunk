%global momorel 5

%global ver0 2
%global ver1 30
%global ver2 0
%global ver3 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: An open source microblogging client for Linux
Name: gwibber
Version: %{ver0}.%{ver1}.%{ver2}.%{ver3}
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2+
URL: http://gwibber.com/
Source0: http://launchpad.net/gwibber/%{ver0}.%{ver1}/%{ver0}.%{ver1}.%{ver2}/+download/gwibber-%{version}.tar.gz
NoSource: 0
BuildArch: noarch
BuildRequires: python-devel
BuildRequires: python-distutils-extra
Requires: dbus-python
Requires: gnome-python2-gconf
Requires: mx
Requires: notify-python
Requires: pygtk2
Requires: python >= 2.7
Requires: python-feedparser
Requires: python-imaging
Requires: python-mako
Requires: python-pycurl
Requires: python-simplejson
Requires: pywebkitgtk
Requires: pyxdg

%description
Gwibber is an open source microblogging client for Linux. It brings
the most popular social networking web services to your desktop and
gives you the ability to control how you communicate.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --prefix %{_prefix} --root %{buildroot} -O1 --skip-build

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/gwibber
%{_bindir}/gwibber-accounts
%{_bindir}/gwibber-error
%{_bindir}/gwibber-poster
%{_bindir}/gwibber-preferences
%{_bindir}/gwibber-service
%{python_sitelib}/%{name}
%{python_sitelib}/%{name}-*.egg-info
%{_datadir}/dbus-1/services/com.Gwibber.Service.service
%{_datadir}/dbus-1/services/com.GwibberClient.service
%{_datadir}/%{name}
%{_datadir}/indicators/messages/applications/%{name}
%{_datadir}/pixmaps/%{name}.svg

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.0.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0.1-2m)
- full rebuild for mo7 release

* Sat Apr 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.30.0.1-1m)
- update to 2.30.0.1

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29.93-1m)
- initial packaging (revno: 682)
