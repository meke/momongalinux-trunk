%global momorel 1

Name:		numactl
Summary:	Library for tuning for Non Uniform Memory Access machines
Version:	2.0.9
Release:	%{momorel}m%{?dist}
License:	GPLv2 and LGPLv2
Group: 		System Environment/Base
URL:		ftp://oss.sgi.com/www/projects/libnuma/download
Source0: 	ftp://oss.sgi.com/www/projects/libnuma/download/numactl-%{version}.tar.gz
Patch0: 	%{name}-%{version}-cpuid-pic.patch
NoSource:	0
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExcludeArch: s390 s390x

%description
Simple NUMA policy support. It consists of a numactl program to run
other programs with a specific NUMA policy and a libnuma to do
allocations with NUMA policy in applications.

%package devel
Summary: Development package for building Applications that use numa
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Provides development headers for numa library calls

%prep
%setup -q 

%patch0 -p1 -b .cpuid

%build
make CFLAGS="$RPM_OPT_FLAGS -I. -fPIC"

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_libdir}
mkdir -p $RPM_BUILD_ROOT%{_includedir}
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man3
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man8

make prefix=$RPM_BUILD_ROOT/usr libdir=$RPM_BUILD_ROOT/%{_libdir} install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libnuma.so.1
%{_bindir}/numactl
%{_bindir}/numademo
%{_bindir}/numastat
%{_bindir}/memhog
%{_bindir}/migspeed
%{_bindir}/migratepages
%{_mandir}/man8/*.8*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libnuma.so
%{_libdir}/libnuma.a
%{_includedir}/numa.h
%{_includedir}/numaif.h
%{_includedir}/numacompat1.h
%{_mandir}/man3/*.3*

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.9-1m)
- update 2.0.9

* Mon Jul 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-2m)
- import cpuid-pic.patch from gentoo to fix build on i686
- https://bugs.gentoo.org/show_bug.cgi?id=456238

* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.8-1m)
- UPDATE 2.0.8

* Mon Jul 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.7-1m)
- UPDATE 2.0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA KIoichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2
- import Patch0,1 from Fedora 11 (2.0.2-4)
- License: GPLv2 and LGPLv2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8-2m)
- rebuild against gcc43

* Wed Mar 14 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.8-1m)
- import from fc

* Fri Jan 12 2007 Neil Horman <nhorman@redhat.com> - 0.9.8-2%{dist}
- Properly fixed bz 221982
- Updated revision string to include %{dist}

* Thu Jan 11 2007 Neil Horman <nhorman@redhat.com> - 0.9.8-1.38
- Fixed -devel to depend on base package so libnuma.so resolves

* Thu Sep 21 2006 Neil Horman <nhorman@redhat.com> - 0.9.8-1.36
- adding nodebind patch for bz 207404

* Fri Aug 25 2006 Neil Horman <nhorman@redhat.com> - 0.9.8-1.35
- moving over libnuma.so to -devel package as well

* Fri Aug 25 2006 Neil Horman <nhorman@redhat.com> - 0.9.8-1.34
- split out headers/devel man pages to a devel subpackage

* Tue Aug 15 2006 Neil Horman <nhorman@redhat.com> - 0.9.8-1.32
- add patch for broken cpu/nodebind output (bz 201906)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.9.8-1.31
- rebuild

* Tue Jun 13 2006 Neil Horman <nhorman@redhat.com>
- Rebased numactl to version 0.9.8 for FC6/RHEL5

* Wed Apr 26 2006 Neil Horman <nhorman@redhat.com>
- Added patches for 64 bit overflows and cpu mask problem

* Fri Mar 10 2006 Bill Nottingham <notting@redhat.com>
- rebuild for ppc TLS issue (#184446)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.6.4-1.25.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Jul  7 2005 Dave Jones <davej@redhat.com>
- numactl doesn't own the manpage dirs. (#161547)

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Tue Feb  8 2005 Dave Jones <davej@redhat.com>
- rebuild with -D_FORTIFY_SOURCE=2

* Wed Nov 10 2004 David Woodhouse <dwmw2@redhat.com>
- Fix build on x86_64

* Thu Oct 21 2004 David Woodhouse <dwmw2@redhat.com>
- Add PPC support

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sat Jun 05 2004 Warren Togami <wtogami@redhat.com> 
- spec cleanup

* Sat Jun 05 2004 Arjan van de Ven <arjanv@redhat.com>
- initial packaging

