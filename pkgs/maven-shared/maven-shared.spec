%global momorel 4

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define shared_components_version 8
%define file_management_version 1.2
%define ant_version 1.0
%define app_configuration_model_version 1.1
%define app_configuration_web_version 1.1
%define archiver_version 2.3
%define common_artifact_filters_version 1.0
%define dependency_analyzer_version 1.0
%define dependency_tree_version 1.1
%define downloader_version 1.2
%define invoker_version 2.0.7
%define model_converter_version 2.3
%define plugin_testing_harness_version 1.2
%define plugin_testing_tools_version 1.0
%define plugin_tools_version 2.2
%define plugin_tools_ant_version 2.2
%define plugin_tools_api_version 2.2
%define plugin_tools_beanshell_version 2.2
%define plugin_tools_java_version 2.2
%define plugin_tools_model_version 2.2
%define reporting_impl_version 2.1
%define repository_builder_version 1.0
%define io_version 1.1
%define jar_version 1.1
%define monitor_version 1.0
%define osgi_version 0.2.0
%define script_ant_version 2.1
%define script_beanshell_version 2.1
%define test_tools_version 1.0
#%define toolchain_version 1.0
%define verifier_version 1.2
#%define web_ui_tests_version 1.0
                                                                                
Summary:        Maven Shared Components
URL:            http://maven.apache.org/shared/
Name:           maven-shared
Version:        8
Release:        %{momorel}m%{?dist}
License:        "ASL 2.0"
Group:          Development/Libraries

# svn export \
#  http://svn.apache.org/repos/asf/maven/shared/tags/maven-shared-components-8/
# tar czf maven-shared-components-8.tar.gz maven-shared-components-8
Source0:        maven-shared-components-8.tar.gz
Source1:        %{name}-8-jpp-depmap.xml

Patch0:        %{name}-pom.patch
Patch1:        %{name}-ant-pom.patch
Patch2:        %{name}-file-management-pom.patch
Patch3:        %{name}-io-ArtifactLocatorStrategyTest.patch
Patch4:        %{name}-plugin-tools-pom.patch
Patch5:        %{name}-disable-cobertura.patch
Patch6:        %{name}-modelv3.patch

BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
#BuildRequires:  maven2-plugin-release
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-source
BuildRequires:  maven2-plugin-surefire
BuildRequires:  maven2-plugin-surefire-report
BuildRequires:  maven-doxia-sitetools
BuildRequires:  plexus-maven-plugin
BuildRequires:  junit
BuildRequires:  saxon
BuildRequires:  saxon-scripts
BuildRequires:  plexus-utils
BuildRequires:  plexus-registry
BuildRequires:  modello-maven-plugin
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  tomcat5
BuildRequires:  easymock

Requires:       maven2 >= 2.0.8-2m
Requires:       plexus-utils

BuildArch:      noarch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%description
Maven Shared Components

%package file-management
Summary:        Maven Shared File Management API
Group:          Development/Libraries
Version:        %{file_management_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-io >= 0:%{io_version}
Requires:  maven2
Requires:  plexus-container-default
Requires:  plexus-utils

%description file-management
API to collect files from a given directory using 
several include/exclude rules.

%package osgi
Summary:        Maven OSGi
Group:          Development/Libraries
Version:        %{osgi_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  aqute-bndlib
Requires:  maven2 >= 0:2.0.7

%description osgi
Library for Maven-OSGi integration

%package ant
Summary:        Maven Ant
Group:          Development/Libraries
Version:        %{ant_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  ant
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-containers-container-default

%description ant
Runs ant scripts embedded in the POM.

%package app-configuration-model
Summary:        Maven Applications Shared Configuration Model
Group:          Development/Libraries
Version:        %{app_configuration_model_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  plexus-containers
Requires:  plexus-registry

%description app-configuration-model
%{summary}.

%package app-configuration-web
Summary:        Maven Applications Shared Configuration Web
Group:          Development/Libraries
Version:        %{app_configuration_web_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-app-configuration-model = 0:%{app_configuration_model_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  maven-wagon
Requires:  plexus-containers-container-default

%description app-configuration-web
%{summary}.

%package archiver
Summary:        Maven Archiver
Group:          Development/Libraries
Version:        %{archiver_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-archiver

%description archiver
%{summary}.

%package common-artifact-filters
Summary:        Maven Common Artifact Filters
Group:          Development/Libraries
Version:        %{common_artifact_filters_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-test-tools >= 0:%{test_tools_version}-%{release}
Requires:  junit
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default

%description common-artifact-filters
%{summary}.

%package dependency-tree
Summary:        Maven Dependency Tree
Group:          Development/Libraries
Version:        %{dependency_tree_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-plugin-testing-harness >= 0:%{plugin_testing_harness_version}-%{release}
Requires:  maven2 >= 0:2.0.7

%description dependency-tree
%{summary}.

%package downloader
Summary:        Maven Downloader
Group:          Development/Libraries
Version:        %{downloader_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7

%description downloader
Provide a super simple interface for downloading a 
single artifact.

%package dependency-analyzer
Summary:        Maven Dependency Analyzer
Group:          Development/Libraries
Version:        %{dependency_analyzer_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  objectweb-asm

%description dependency-analyzer
%{summary}.

%package invoker
Summary:        Maven Process Invoker
Group:          Development/Libraries
Version:        %{invoker_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-monitor >= 0:%{monitor_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-utils

%description invoker
%{summary}.

%package model-converter
Summary:        Maven Model Converter
Group:          Development/Libraries
Version:        %{model_converter_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  dom4j
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default
Requires:  plexus-utils

%description model-converter
Converts between version 3.0.0 and version 4.0.0 models.

%package plugin-testing-harness
Summary:        Maven Plugin Testing Mechanism
Group:          Development/Libraries
Version:        %{plugin_testing_harness_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  junit
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-utils

%description plugin-testing-harness
%{summary}.

%package plugin-testing-tools
Summary:        Maven Plugin Testing Tools
Group:          Development/Libraries
Version:        %{plugin_testing_tools_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-invoker = 0:%{invoker_version}-%{release}
Requires:  %{name}-repository-builder = 0:%{repository_builder_version}-%{release}
Requires:  %{name}-test-tools = 0:%{test_tools_version}-%{release}
Requires:  junit
Requires:  maven2 >= 0:2.0.7

%description plugin-testing-tools
%{summary}.

%package plugin-tools-ant
Summary:        Maven Ant Plugin Tools
Group:          Development/Libraries
Version:        %{plugin_tools_ant_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-plugin-tools-api = 0:%{plugin_tools_api_version}-%{release}
Requires:  %{name}-plugin-tools-model = 0:%{plugin_tools_model_version}-%{release}
Requires:  ant
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default
Requires:  plexus-utils

%description plugin-tools-ant
%{summary}.

%package plugin-tools-api
Summary:        Maven Plugin Tools APIs
Group:          Development/Libraries
Version:        %{plugin_tools_api_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-utils

%description plugin-tools-api
%{summary}.

%package plugin-tools-beanshell
Summary:        Maven Plugin Tools for Beanshell
Group:          Development/Libraries
Version:        %{plugin_tools_beanshell_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-plugin-tools-api = 0:%{plugin_tools_api_version}-%{release}
Requires:  bsh
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default
Requires:  plexus-utils

%description plugin-tools-beanshell
%{summary}.

%package plugin-tools-java
Summary:        Maven Plugin Tools for Java
Group:          Development/Libraries
Version:        %{plugin_tools_java_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-plugin-tools-api = 0:%{plugin_tools_api_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default
Requires:  plexus-utils
Requires:  qdox

%description plugin-tools-java
%{summary}.

%package plugin-tools-model
Summary:        Maven Plugin Metadata Model
Group:          Development/Libraries
Version:        %{plugin_tools_model_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default
Requires:  plexus-utils

%description plugin-tools-model
%{summary}.

%package reporting-impl
Summary:        Maven Reporting Implementation
Group:          Development/Libraries
Version:        %{reporting_impl_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  jakarta-commons-validator
Requires:  jakarta-oro
Requires:  maven2 >= 0:2.0.7
Requires:  maven-doxia

%description reporting-impl
%{summary}.

%package repository-builder
Summary:        Maven Repository Builder
Group:          Development/Libraries
Version:        %{repository_builder_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  %{name}-common-artifact-filters = 0:%{common_artifact_filters_version}-%{release}
Requires:  maven2 >= 0:2.0.7

%description repository-builder
%{summary}.

%package io
Summary:        Maven Shared I/O API
Group:          Development/Libraries
Version:        %{io_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  maven-wagon
Requires:  plexus-utils
Requires:  plexus-container-default

%description io
%{summary}.

%package jar
Summary:        Maven Shared Jar
Group:          Development/Libraries
Version:        %{jar_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7

%description jar
Utilities that help identify the contents of a JAR, 
including Java class analysis and Maven metadata 
analysis.

%package monitor
Summary:        Maven Shared Monitor API
Group:          Development/Libraries
Version:        %{monitor_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  maven2 >= 0:2.0.7
Requires:  plexus-container-default

%description monitor
%{summary}.

%package test-tools
Summary:        Maven Testing Tools
Group:          Development/Libraries
Version:        %{test_tools_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  easymock
Requires:  junit
Requires:  plexus-utils

%description test-tools
%{summary}.

#%package toolchain
#Summary:        Maven Toolchain
#Group:          Development/Libraries
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  junit
#
#%description toolchain
#%{summary}.
#
#%package user-acegi
#Summary:        Maven User Management Acegi Bridge
#Group:          Development/Libraries
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  %{name}-user-model >= 0:%{user_model_version}-%{release}
#Requires:  acegi-security
#Requires:  jakarta-commons-dbcp
#
#%description user-acegi
#Bridge between Maven User Management and Acegi.
#
#%package user-controller
#Summary:        Maven User Management Controller
#Group:          Development/Libraries
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  %{name}-user-model >= 0:%{user_model_version}-%{release}
#Requires:  plexus-xwork-integration
#Requires:  servlet_2_4_api
#Requires:  spring-core
#
#%description user-controller
#User Management Controller Layer.
#
#%package user-example
#Summary:        Maven User Management Example Webapp
#Group:          Development/Libraries
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  %{name}-user-acegi = 0:%{user_acegi_version}-%{release}
#Requires:  %{name}-user-controller = 0:%{user_controller_version}-%{release}
#Requires:  %{name}-user-webapp = 0:%{user_webapp_version}-%{release}
#Requires:  plexus-log4j-logging
#
#%description user-example
#User Management Example Webapp.
#
#%package user-model
#Summary:        Maven User Management Model
#Group:          Development/Libraries
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  jpox-core
#Requires:  plexus-container-default
#Requires:  plexus-jdo2
#Requires:  plexus-utils
#
#%description user-model
#User Management Model Classes.
#
#%package user-webapp
#Summary:        Maven User Management Webapp
#Group:          Development/Libraries
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  %{name}-user-acegi = 0:%{user_acegi_version}-%{release}
#Requires:  %{name}-user-controller = 0:%{user_controller_version}-%{release}
#Requires:  jakarta-taglibs-standard
#Requires:  jpox-core
#Requires:  plexus-container-default
#Requires:  plexus-log4j-logging
#Requires:  servlet_2_4_api
#Requires:  sitemesh
#
#%description user-webapp
#User Management Webapp Layer.

%package verifier
Summary:        Maven Verifier Component
Group:          Development/Libraries
Version:        %{verifier_version}
Requires:  %{name} = 0:%{shared_components_version}-%{release}
Requires:  junit

%description verifier
%{summary}.

#%package web-ui-tests
#Summary:        Maven Shared Web UI Tests
#Group:          Development/Libraries
#Version:        %{web_ui_tests_version}
#Requires:  %{name} = 0:%{shared_components_version}-%{release}
#Requires:  junit
#Requires:  openqa-selenium-rc-java-client-driver
#Requires:  openqa-selenium-rc-server
#Requires:  plexus-utils

#%description web-ui-tests
#%{summary}.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Provides:       %{name}-file-management-javadoc = %{epoch}:%{file_management_version}-%{release}
Obsoletes:      %{name}-file-management-javadoc < %{epoch}:%{file_management_version}-%{release}
Provides:       %{name}-plugin-testing-harness-javadoc = %{epoch}:%{plugin_testing_harness_version}-%{release}
Obsoletes:      %{name}-plugin-testing-harness-javadoc < %{epoch}:%{plugin_testing_harness_version}-%{release}

%description javadoc
%{summary}.

%prep
%setup -q -n %{name}-components-%{shared_components_version}
#gzip -dc %{SOURCE4} | tar xf -
chmod -R go=u-w *
%patch0 -b .sav0
%patch1 -b .sav1
%patch2 -b .sav2
%patch3 -b .sav3
%patch4 -b .sav4
%patch5 -b .sav5
%patch6 -b .sav6

# Remove test that needs junit-addons until that makes it into Fedora
rm -f maven-reporting-impl/src/test/java/org/apache/maven/reporting/AbstractMavenReportRendererTest.java

# Remove tests that need easymock until it is available in Fedora
rm -f maven-shared-io/src/test/java/org/apache/maven/shared/io/MockManager.java
rm -f maven-shared-io/src/test/java/org/apache/maven/shared/io/location/ArtifactLocatorStrategyTest.java
rm -f maven-shared-io/src/test/java/org/apache/maven/shared/io/location/ArtifactLocatorStrategyTest.java.sav3
rm -f maven-shared-io/src/test/java/org/apache/maven/shared/io/location/LocatorTest.java
rm -f maven-shared-io/src/test/java/org/apache/maven/shared/io/download/DefaultDownloadManagerTest.java

rm -f maven-dependency-analyzer/src/test/java/org/apache/maven/shared/dependency/analyzer/DefaultClassAnalyzerTest.java

rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/AbstractDependencyNodeTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/DependencyNodeTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/DependencyTreeResolutionListenerTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/filter/AbstractDependencyNodeFilterTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/filter/AncestorOrSelfDependencyNodeFilterTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/filter/ArtifactDependencyNodeFilterTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/traversal/CollectingDependencyNodeVisitorTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/traversal/FilteringDependencyNodeVisitorTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/traversal/SerializingDependencyNodeVisitorTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/filter/AndDependencyNodeFilterTest.java
rm -f maven-dependency-tree/src/test/java/org/apache/maven/shared/dependency/tree/traversal/BuildingDependencyNodeVisitorTest.java

# FIXME: These tests freeze.. no idea why :(
rm -rf maven-plugin-testing-tools/src/test

# Remove tests that need jmock (for now)
rm -f maven-dependency-analyzer/src/test/java/org/apache/maven/shared/dependency/analyzer/InputStreamConstraint.java
rm -f maven-dependency-analyzer/src/test/java/org/apache/maven/shared/dependency/analyzer/ClassFileVisitorUtilsTest.java
rm -f maven-dependency-analyzer/src/test/java/org/apache/maven/shared/dependency/analyzer/AbstractFileTest.java

%build
export MAVEN_REPO_LOCAL=$(pwd)/m2_repo/repository

export MAVEN_OPTS="-XX:MaxPermSize=256m"
mvn-jpp \
        -e \
        -Dmaven2.jpp.depmap.file=%{SOURCE1} \
        -Dmaven.test.failure.ignore=true \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install

# Manual iteration should not be needed, but there is a bug in the javadoc 
# plugin which makes this necessary. See: 
# http://jira.codehaus.org/browse/MJAVADOC-157
for pom in `find -name pom.xml | grep -v /test | grep -v tests | grep -v maven-toolchain | sort`; do
    pushd `dirname $pom`
        mvn-jpp \
          -e \
          -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
          -Dmaven2.jpp.depmap.file=%{SOURCE1} \
          javadoc:javadoc
    popd
done

%install
# main package infrastructure
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/maven-shared
install -d -m 755 $RPM_BUILD_ROOT/%{_datadir}/maven2/poms

# poms and jars
install -pm 644 pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-components-parent.pom
%add_to_maven_depmap org.apache.maven.shared maven-shared-components %{shared_components_version} JPP/maven-shared components-parent

install -pm 644 maven-downloader/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-downloader.pom
%add_to_maven_depmap org.apache.maven.shared maven-downloader %{downloader_version} JPP/maven-shared downloader
install -p -m 0644 maven-downloader/target/maven-downloader-%{downloader_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/downloader-%{downloader_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf downloader-%{downloader_version}.jar downloader.jar
popd

install -pm 644 maven-dependency-analyzer/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-dependency-analyzer.pom
%add_to_maven_depmap org.apache.maven.shared maven-dependency-analyzer %{dependency_analyzer_version} JPP/maven-shared dependency-analyzer
install -p -m 0644 maven-dependency-analyzer/target/maven-dependency-analyzer-%{dependency_analyzer_version}-alpha-3-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/dependency-analyzer-%{dependency_analyzer_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf dependency-analyzer-%{dependency_analyzer_version}.jar dependency-analyzer.jar
popd

install -pm 644 maven-dependency-tree/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-dependency-tree.pom
%add_to_maven_depmap org.apache.maven.shared maven-dependency-tree %{dependency_tree_version} JPP/maven-shared dependency-tree
install -p -m 0644 maven-dependency-tree/target/maven-dependency-tree-%{dependency_tree_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/dependency-tree-%{dependency_tree_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf dependency-tree-%{dependency_tree_version}.jar dependency-tree.jar
popd

#install -pm 644 maven-web-ui-tests/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-web-ui-tests.pom
#%%add_to_maven_depmap org.apache.maven.shared maven-web-ui-tests %{web_ui_tests_version} JPP/maven-shared web-ui-tests
#install -p -m 0644 maven-web-ui-tests/target/maven-web-ui-tests-%{web_ui_tests_version}-SNAPSHOT.jar \
#        $RPM_BUILD_ROOT%{_javadir}/maven-shared/web-ui-tests-%{web_ui_tests_version}.jar
#pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
#  ln -sf web-ui-tests-%{web_ui_tests_version}.jar web-ui-tests.jar
#popd

install -pm 644 maven-verifier/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-verifier.pom
%add_to_maven_depmap org.apache.maven.shared maven-verifier %{verifier_version} JPP/maven-shared verifier
install -p -m 0644 maven-verifier/target/maven-verifier-%{verifier_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/verifier-%{verifier_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf verifier-%{verifier_version}.jar verifier.jar
popd

install -pm 644 maven-test-tools/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-test-tools.pom
%add_to_maven_depmap org.apache.maven.shared maven-test-tools %{test_tools_version} JPP/maven-shared test-tools
install -p -m 0644 maven-test-tools/target/maven-test-tools-%{test_tools_version}-alpha-2-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/test-tools-%{test_tools_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf test-tools-%{test_tools_version}.jar test-tools.jar
popd

install -pm 644 maven-shared-monitor/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-monitor.pom
%add_to_maven_depmap org.apache.maven.shared maven-shared-monitor %{monitor_version} JPP/maven-shared monitor
install -p -m 0644 maven-shared-monitor/target/maven-shared-monitor-%{monitor_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/monitor-%{monitor_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf monitor-%{monitor_version}.jar monitor.jar
popd

install -pm 644 maven-shared-io/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-io.pom
%add_to_maven_depmap org.apache.maven.shared maven-shared-io %{io_version} JPP/maven-shared io
install -p -m 0644 maven-shared-io/target/maven-shared-io-%{io_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/io-%{io_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf io-%{io_version}.jar io.jar
popd

install -pm 644 maven-shared-jar/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-jar.pom
%add_to_maven_depmap org.apache.maven.shared maven-shared-jar %{jar_version} JPP/maven-shared jar
install -p -m 0644 maven-shared-jar/target/maven-shared-jar-%{jar_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/jar-%{jar_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf jar-%{jar_version}.jar jar.jar
popd

install -pm 644 maven-repository-builder/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-repository-builder.pom
%add_to_maven_depmap org.apache.maven.shared maven-repository-builder %{repository_builder_version} JPP/maven-shared repository-builder
install -p -m 0644 maven-repository-builder/target/maven-repository-builder-%{repository_builder_version}-alpha-2-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/repository-builder-%{repository_builder_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf repository-builder-%{repository_builder_version}.jar repository-builder.jar
popd

install -pm 644 maven-reporting-impl/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-reporting-impl.pom
%add_to_maven_depmap org.apache.maven.reporting maven-reporting-impl %{reporting_impl_version} JPP/maven-shared reporting-impl
install -p -m 0644 maven-reporting-impl/target/maven-reporting-impl-%{reporting_impl_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/reporting-impl-%{reporting_impl_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf reporting-impl-%{reporting_impl_version}.jar reporting-impl.jar
popd

install -pm 644 maven-plugin-tools/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools.pom
%add_to_maven_depmap org.apache.maven maven-plugin-tools %{plugin_tools_version} JPP/maven-shared plugin-tools

install -pm 644 maven-plugin-tools/maven-plugin-tools-model/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-model.pom
%add_to_maven_depmap org.apache.maven maven-plugin-tools-model %{plugin_tools_model_version} JPP/maven-shared plugin-tools-model
install -p -m 0644 maven-plugin-tools/maven-plugin-tools-model/target/maven-plugin-tools-model-%{plugin_tools_model_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-tools-model-%{plugin_tools_model_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-tools-model-%{plugin_tools_model_version}.jar plugin-tools-model.jar
popd

install -pm 644 maven-plugin-tools/maven-plugin-tools-java/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-java.pom
%add_to_maven_depmap org.apache.maven maven-plugin-tools-java %{plugin_tools_java_version} JPP/maven-shared plugin-tools-java
install -p -m 0644 maven-plugin-tools/maven-plugin-tools-java/target/maven-plugin-tools-java-%{plugin_tools_java_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-tools-java-%{plugin_tools_java_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-tools-java-%{plugin_tools_java_version}.jar plugin-tools-java.jar
popd

install -pm 644 maven-plugin-tools/maven-plugin-tools-beanshell/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-beanshell.pom
%add_to_maven_depmap org.apache.maven maven-plugin-tools-beanshell %{plugin_tools_beanshell_version} JPP/maven-shared plugin-tools-beanshell
install -p -m 0644 maven-plugin-tools/maven-plugin-tools-beanshell/target/maven-plugin-tools-beanshell-%{plugin_tools_beanshell_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-tools-beanshell-%{plugin_tools_beanshell_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-tools-beanshell-%{plugin_tools_beanshell_version}.jar plugin-tools-beanshell.jar
popd

install -pm 644 maven-plugin-tools/maven-plugin-tools-api/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-api.pom
%add_to_maven_depmap org.apache.maven maven-plugin-tools-api %{plugin_tools_api_version} JPP/maven-shared plugin-tools-api
install -p -m 0644 maven-plugin-tools/maven-plugin-tools-api/target/maven-plugin-tools-api-%{plugin_tools_api_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-tools-api-%{plugin_tools_api_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-tools-api-%{plugin_tools_api_version}.jar plugin-tools-api.jar
popd

install -pm 644 maven-plugin-tools/maven-plugin-tools-ant/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-ant.pom
%add_to_maven_depmap org.apache.maven maven-plugin-tools-ant %{plugin_tools_ant_version} JPP/maven-shared plugin-tools-ant
install -p -m 0644 maven-plugin-tools/maven-plugin-tools-ant/target/maven-plugin-tools-ant-%{plugin_tools_ant_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-tools-ant-%{plugin_tools_ant_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-tools-ant-%{plugin_tools_ant_version}.jar plugin-tools-ant.jar
popd

install -pm 644 maven-plugin-testing-tools/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-testing-tools.pom
%add_to_maven_depmap org.apache.maven.shared maven-plugin-testing-tools %{plugin_testing_tools_version} JPP/maven-shared plugin-testing-tools
install -p -m 0644 maven-plugin-testing-tools/target/maven-plugin-testing-tools-%{plugin_testing_tools_version}-alpha-3-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-testing-tools-%{plugin_testing_tools_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-testing-tools-%{plugin_testing_tools_version}.jar plugin-testing-tools.jar
popd

install -pm 644 maven-plugin-testing-harness/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-plugin-testing-harness.pom
%add_to_maven_depmap org.apache.maven.shared maven-plugin-testing-harness %{plugin_testing_harness_version} JPP/maven-shared plugin-testing-harness
install -p -m 0644 maven-plugin-testing-harness/target/maven-plugin-testing-harness-%{plugin_testing_harness_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/plugin-testing-harness-%{plugin_testing_harness_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf plugin-testing-harness-%{plugin_testing_harness_version}.jar plugin-testing-harness.jar
popd

install -pm 644 maven-model-converter/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-model-converter.pom
%add_to_maven_depmap org.apache.maven.shared maven-model-converter %{model_converter_version} JPP/maven-shared model-converter
install -p -m 0644 maven-model-converter/target/maven-model-converter-%{model_converter_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/model-converter-%{model_converter_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf model-converter-%{model_converter_version}.jar model-converter.jar
popd

install -pm 644 maven-invoker/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-invoker.pom
%add_to_maven_depmap org.apache.maven.shared maven-invoker %{invoker_version} JPP/maven-shared invoker
install -p -m 0644 maven-invoker/target/maven-invoker-%{invoker_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/invoker-%{invoker_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf invoker-%{invoker_version}.jar invoker.jar
popd

install -pm 644 maven-common-artifact-filters/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-common-artifact-filters.pom
%add_to_maven_depmap org.apache.maven.shared maven-common-artifact-filters %{common_artifact_filters_version} JPP/maven-shared common-artifact-filters
install -p -m 0644 maven-common-artifact-filters/target/maven-common-artifact-filters-%{common_artifact_filters_version}-alpha-2-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/common-artifact-filters-%{common_artifact_filters_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf common-artifact-filters-%{common_artifact_filters_version}.jar common-artifact-filters.jar
popd

install -pm 644 maven-archiver/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-archiver.pom
%add_to_maven_depmap org.apache.maven maven-archiver %{archiver_version} JPP/maven-shared archiver
install -p -m 0644 maven-archiver/target/maven-archiver-%{archiver_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/archiver-%{archiver_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf archiver-%{archiver_version}.jar archiver.jar
popd

install -pm 644 maven-app-configuration/web/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-app-configuration-web.pom
%add_to_maven_depmap org.apache.maven.shared maven-app-configuration-web %{app_configuration_web_version} JPP/maven-shared app-configuration-web
install -p -m 0644 maven-app-configuration/web/target/maven-app-configuration-web-%{app_configuration_web_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/app-configuration-web-%{app_configuration_web_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf app-configuration-web-%{app_configuration_web_version}.jar app-configuration-web.jar
popd

install -pm 644 maven-app-configuration/model/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-app-configuration-model.pom
%add_to_maven_depmap org.apache.maven.shared maven-app-configuration-model %{app_configuration_model_version} JPP/maven-shared app-configuration-model
install -p -m 0644 maven-app-configuration/model/target/maven-app-configuration-model-%{app_configuration_model_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/app-configuration-model-%{app_configuration_model_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf app-configuration-model-%{app_configuration_model_version}.jar app-configuration-model.jar
popd

install -pm 644 maven-ant/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-ant.pom
%add_to_maven_depmap org.apache.maven.shared maven-ant %{ant_version} JPP/maven-shared ant
install -p -m 0644 maven-ant/target/maven-ant-%{ant_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/ant-%{ant_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf ant-%{ant_version}.jar ant.jar
popd

install -pm 644 maven-osgi/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-osgi.pom
%add_to_maven_depmap org.apache.maven.shared maven-osgi %{osgi_version} JPP/maven-shared osgi
install -p -m 0644 maven-osgi/target/maven-osgi-%{osgi_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/osgi-%{osgi_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf osgi-%{osgi_version}.jar osgi.jar
popd

install -pm 644 file-management/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-shared-file-management.pom
%add_to_maven_depmap org.apache.maven.shared file-management %{file_management_version} JPP/maven-shared file-management
install -p -m 0644 file-management/target/file-management-%{file_management_version}-SNAPSHOT.jar \
        $RPM_BUILD_ROOT%{_javadir}/maven-shared/file-management-%{file_management_version}.jar
pushd $RPM_BUILD_ROOT%{_javadir}/maven-shared
  ln -sf file-management-%{file_management_version}.jar file-management.jar
popd

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/downloader
cp -pr maven-downloader/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/downloader
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/dependency-tree
cp -pr maven-dependency-tree/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/dependency-tree
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/dependency-analyzer
cp -pr maven-dependency-analyzer/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/dependency-analyzer
#install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/web-ui-tests
#cp -pr maven-web-ui-tests/target/site/apidocs/* \
#         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/web-ui-tests
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/verifier
cp -pr maven-verifier/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/verifier
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/test-tools
cp -pr maven-test-tools/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/test-tools
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/monitor
cp -pr maven-shared-monitor/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/monitor
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/io
cp -pr maven-shared-io/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/io
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/jar
cp -pr maven-shared-jar/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/repository-builder
cp -pr maven-repository-builder/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/repository-builder
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/reporting-impl
cp -pr maven-reporting-impl/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/reporting-impl
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-model
cp -pr maven-plugin-tools/maven-plugin-tools-model/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-model
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-java
cp -pr maven-plugin-tools/maven-plugin-tools-java/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-java
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-beanshell
cp -pr maven-plugin-tools/maven-plugin-tools-beanshell/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-beanshell
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-api
cp -pr maven-plugin-tools/maven-plugin-tools-api/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-api
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-ant
cp -pr maven-plugin-tools/maven-plugin-tools-ant/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-tools-ant
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-testing-tools
cp -pr maven-plugin-testing-tools/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-testing-tools
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-testing-harness
cp -pr maven-plugin-testing-harness/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/plugin-testing-harness
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/model-converter
cp -pr maven-model-converter/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/model-converter
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/invoker
cp -pr maven-invoker/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/invoker
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/common-artifact-filters
cp -pr maven-common-artifact-filters/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/common-artifact-filters
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/archiver
cp -pr maven-archiver/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/archiver
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/app-configuration-web
cp -pr maven-app-configuration/web/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/app-configuration-web
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/app-configuration-model
cp -pr maven-app-configuration/model/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/app-configuration-model
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/ant
cp -pr maven-ant/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/ant
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/osgi
cp -pr maven-osgi/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/osgi
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/file-management
cp -pr file-management/target/site/apidocs/* \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{shared_components_version}/file-management

ln -s %{name}-%{shared_components_version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}


%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%dir %{_javadir}/maven-shared
%{_datadir}/maven2/poms/JPP.maven-shared-components-parent.pom
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools.pom
%{_mavendepmapfragdir}/*

%files file-management
%defattr(-,root,root,-)
%{_javadir}/maven-shared/file-management*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-file-management.pom

%files osgi
%defattr(-,root,root,-)
%{_javadir}/maven-shared/osgi*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-osgi.pom

%files ant
%defattr(-,root,root,-)
%{_javadir}/maven-shared/ant*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-ant.pom

%files app-configuration-model
%defattr(-,root,root,-)
%{_javadir}/maven-shared/app-configuration-model*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-app-configuration-model.pom

%files app-configuration-web
%defattr(-,root,root,-)
%{_javadir}/maven-shared/app-configuration-web*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-app-configuration-web.pom

%files archiver
%defattr(-,root,root,-)
%{_javadir}/maven-shared/archiver*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-archiver.pom

%files common-artifact-filters
%defattr(-,root,root,-)
%{_javadir}/maven-shared/common-artifact-filters*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-common-artifact-filters.pom

%files dependency-analyzer
%defattr(-,root,root,-)
%{_javadir}/maven-shared/dependency-analyzer*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-dependency-analyzer.pom

%files dependency-tree
%defattr(-,root,root,-)
%{_javadir}/maven-shared/dependency-tree*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-dependency-tree.pom

%files downloader
%defattr(-,root,root,-)
%{_javadir}/maven-shared/downloader*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-downloader.pom

%files invoker
%defattr(-,root,root,-)
%{_javadir}/maven-shared/invoker*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-invoker.pom

%files model-converter
%defattr(-,root,root,-)
%{_javadir}/maven-shared/model-converter*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-model-converter.pom

%files plugin-testing-harness
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-testing-harness*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-testing-harness.pom

%files plugin-testing-tools
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-testing-tools*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-testing-tools.pom

%files plugin-tools-ant
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-tools-ant*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-ant.pom

%files plugin-tools-api
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-tools-api*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-api.pom

%files plugin-tools-beanshell
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-tools-beanshell*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-beanshell.pom

%files plugin-tools-java
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-tools-java*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-java.pom

%files plugin-tools-model
%defattr(-,root,root,-)
%{_javadir}/maven-shared/plugin-tools-model*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-plugin-tools-model.pom

%files reporting-impl
%defattr(-,root,root,-)
%{_javadir}/maven-shared/reporting-impl*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-reporting-impl.pom

%files repository-builder
%defattr(-,root,root,-)
%{_javadir}/maven-shared/repository-builder*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-repository-builder.pom

%files io
%defattr(-,root,root,-)
%{_javadir}/maven-shared/io*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-io.pom

%files jar
%defattr(-,root,root,-)
%{_javadir}/maven-shared/jar*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-jar.pom

%files monitor
%defattr(-,root,root,-)
%{_javadir}/maven-shared/monitor*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-monitor.pom

%files test-tools
%defattr(-,root,root,-)
%{_javadir}/maven-shared/test-tools*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-test-tools.pom

#%files toolchain
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/toolchain*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-toolchain.pom
#
#%files user-acegi
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/user-acegi*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-user-acegi.pom
#
#%files user-controller
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/user-controller*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-user-controller.pom
#
#%files user-example
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/user-example*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-user-example.pom
#
#%files user-model
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/user-model*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-user-model.pom
#
#%files user-webapp
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/user-webapp*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-user-webapp.pom

%files verifier
%defattr(-,root,root,-)
%{_javadir}/maven-shared/verifier*.jar
%{_datadir}/maven2/poms/JPP.maven-shared-verifier.pom

#%files web-ui-tests
#%defattr(-,root,root,-)
#%{_javadir}/maven-shared/web-ui-tests*.jar
#%{_datadir}/maven2/poms/JPP.maven-shared-web-ui-tests.pom
#%if %{gcj_support}
#%attr(-,root,root) %dir %{_libdir}/gcj/%{name}
#%attr(-,root,root) %{_libdir}/gcj/%{name}/web-ui-tests*-%{web_ui_tests_version}.jar.*
#%endif

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{shared_components_version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8-2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8-1m)
- sync with Rawhide (8-4) for maven2-2.0.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4jpp.2m)
- rebuild against rpm-4.6

* Wed May 21 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-4jpp.1m)
- import from Fedora to Momonga

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.0-4jpp.4
- Rebuild

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-4jpp.3
- Rebuild with ppc64 excludearch'd
- Removed 'jpp' from a BR version

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-4jpp.2
- Fixed BRs and Reqa

* Tue Feb 27 2007 Tania Bento <tbento@redhat.com> 0:1.0-4jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Removed %%Vendor.
- Removed %%Distribution.
- Removed %%post and %%postun sections for file-management-javadoc.
- Removed %%post and %%postun sections for plugin-testing-harness-javadoc.
- Defined _with_gcj_support and gcj_support.
- Fixed %%License.
- Fixed %%Group.
- Marked config file with %%config(noreplace) in %%files section.
- Fixed instructions on how to generate source drop.

* Fri Oct 27 2006 Deepak Bhole <dbhole@redhat.com> 1.0-4jpp
- Update for maven 9jpp

* Fri Sep 15 2006 Deepak Bhole <dbhole@redhat.com> 1.0-3jpp
- Removed the file-management-pom.patch (no longer required)
- Install poms

* Wed Sep 13 2006 Ralph Apel <r.apel@r-apel.de> 0:1.0-2jpp
- Add plugin-testing-harness subpackage

* Mon Sep 11 2006 Ralph Apel <r.apel@r-apel.de> 0:1.0-1jpp
- First release
- Add gcj_support option
- Add post/postun Requires for javadoc

