%global         momorel 4

Name:           perl-Graph
Version:        0.96
Release:        %{momorel}m%{?dist}
Summary:        Graph data structures and algorithms
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Graph/
Source0:        http://www.cpan.org/authors/id/J/JH/JHI/Graph-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(List::Util)
BuildRequires:  perl(Math::Complex)
BuildRequires:  perl(Safe)
BuildRequires:  perl(Storable) >= 2.05
BuildRequires:  perl(Test::Simple)
Requires:       perl(List::Util)
Requires:       perl(Math::Complex)
Requires:       perl(Safe)
Requires:       perl(Storable) >= 2.05
Requires:       perl(Test::Simple)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Non-Description

%prep
%setup -q -n Graph-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README RELEASE DESIGN Changes TODO util
%{perl_vendorlib}/Graph*
%{perl_vendorlib}/Heap071*
%{perl_vendorlib}/auto/Heap071*
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-2m)
- rebuild against perl-5.18.1

* Sun May 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.94-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.94-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.94-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.91-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.91-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-3m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.91-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.91-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.91-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb  2 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.91-1
- Update to upstream 0.91

* Wed Jun  4 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.84-3
- Remove old check construct that prevents build in F-10+ (#449571)

* Fri Feb 08 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.84-2
- rebuild for new perl

* Wed Sep 05 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.84-1
- Update to latest upstream.

* Thu Aug 23 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.83-3
- License tag to GPL+ or Artistic as per new guidelines.

* Sat Aug 18 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.83-2
- Add missing BR: perl(Test::More)

* Sat Aug 18 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.83-1
- Update to latest upstream

* Wed Mar 23 2007 Alex Lancaster <alexl@users.sourceforge.net> 0.81-1
- Update to 0.81

* Wed Apr 06 2005 Hunter Matthews <thm@duke.edu> 0.59-2
- Review suggestions from Jose Pedro Oliveira

* Fri Mar 18 2005 Hunter Matthews <thm@duke.edu> 0.59-1
- Initial Packageing.


