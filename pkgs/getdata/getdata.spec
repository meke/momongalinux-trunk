%global momorel 1
%global             python_ver %(%{__python} -c "import sys ; print sys.version[:3]")
%global             python_sitelib  %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global             python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name:               getdata
Version:            0.8.5
Release:            %{momorel}m%{?dist}
Summary:            Library for reading and writing dirfile data
Group:              Development/Libraries
License:            GPLv2+
URL:                http://getdata.sourceforge.net/
Source0:            http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource:           0
BuildRoot:          %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:      gcc-gfortran python-devel numpy
BuildRequires:      zlib-devel
BuildRequires:      libtool-ltdl-devel
BuildRequires:      bzip2-devel
BuildRequires:      xz-devel >= 5.0.0
BuildRequires:      python-devel >= 2.7
Requires(post):     /sbin/ldconfig
Requires(postun):   /sbin/ldconfig

%description
The GetData Project is the reference implementation of the Dirfile Standards, a
filesystem-based database format for time-ordered binary data. The Dirfile
database format is designed to provide a fast, simple format for storing and
reading data. 

%package devel
Group:              Development/Libraries
Summary:            Headers required when building programs against getdata
Requires:           %{name} = %{version}-%{release}
Requires:           pkgconfig
Requires:           gcc-gfortran

%description devel
Headers required when building a program against the GetData library.
Includes C++ and FORTRAN (77 & 95) bindings. 

%package gzip
Group:              Development/Libraries
Summary:            Enables getdata read ability of gzip compressed dirfiles
Requires:           %{name} = %{version}-%{release}

%description gzip
Enables getdata to read dirfiles that are encoded (compressed) with gzip.
Fields must be fully compressed with gzip, not actively being written to.
Does not yet allow writing of gzip encoded dirfiles.  

%package bzip2
Group:              Development/Libraries
Summary:            Enables getdata read ability of bzip2 compressed dirfiles
Requires:           %{name} = %{version}-%{release}

%description bzip2
Enables getdata to read dirfiles that are encoded (compressed) with bzip2.
Fields must be fully compressed with bzip2, not actively being written to.
Does not yet allow writing of bzip2 encoded dirfiles.

%package lzma
Group:              Development/Libraries
Summary:            Enables getdata read ability of lzma compressed dirfiles
Requires:           %{name} = %{version}-%{release}
Requires:           xz-libs >= 5.0.0

%description lzma
Enables getdata to read dirfiles that are encoded (compressed) with lzma.
Fields must be fully compressed with lzma, not actively being written to.
Does not yet allow writing of lzma encoded dirfiles.

%package python
Group:              Development/Libraries
Summary:            getdata python bindings
Requires:           %{name} = %{version}-%{release}
Requires:           numpy

%description python
Bindings to the getdata library for the python lanuguage.
Uses (and requires) the numpy python library for large numeric arrays.

%prep
%setup -q

%build
# FIXME: FFLAGS/FCFLAGS are not being honored; looking into it with upstream.
%configure --disable-static --enable-modules --disable-perl --without-libzzip

# removing rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%check
LD_LIBRARY_PATH="%{buildroot}/%{_libdir}:%{buildroot}/%{_libdir}/getdata" make check

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
make DESTDIR=%{buildroot} SUID_ROOT="" install
# Remove .la files.  
rm -f %{buildroot}/%{_libdir}/lib*.la
rm -f %{buildroot}/%{_libdir}/getdata/lib*.la
rm -f %{buildroot}/%{python_sitearch}/*.la
# Remove simple docs, as we install them ourselves (along with others)
rm -f %{buildroot}/%{_datadir}/doc/%{name}/*
# Place fortran module in the correct location
mkdir -p %{buildroot}/%{_fmoddir}
mv %{buildroot}/%{_includedir}/getdata.mod  %{buildroot}/%{_fmoddir}/
# remove conflicting files with CUnit
rm -f %{buildroot}%{_mandir}/man3/get_error*

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README NEWS COPYING AUTHORS TODO ChangeLog
%{_bindir}/checkdirfile
%{_bindir}/dirfile2ascii
%{_libdir}/lib*getdata*.so.*
%dir %{_libdir}/getdata
%{_mandir}/man1/*
%{_mandir}/man5/*

%files devel
%defattr(-,root,root,-)
%doc doc/README.cxx doc/README.f77 doc/README.f95 doc/unclean_database_recovery.txt doc/README.python
%{_libdir}/libgetdata.so
%{_libdir}/libf*getdata.so
%{_libdir}/libgetdata++.so
%{_includedir}/*
%{_mandir}/man3/*
%{_libdir}/pkgconfig/getdata.pc
%{_fmoddir}/getdata.mod

%files gzip
%defattr(-,root,root,-)
%{_libdir}/getdata/libgetdatagzip*.so

%files bzip2
%defattr(-,root,root,-)
%{_libdir}/getdata/libgetdatabzip2*.so

%files lzma
%defattr(-,root,root,-)
%{_libdir}/getdata/libgetdatalzma*.so

%files python
%defattr(-,root,root,-)
%{python_sitearch}/pygetdata.*

%changelog
* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.5-1m)
- update to 0.8.5

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.3-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-4m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-3m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3
 
* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- apply glibc212 patch

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-2m)
- remove conflicting man pages with CUnit

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- import from Fedora devel
- update to 0.6.0

* Mon Sep 21 2009 Matthew Truch <matt at truch.net> - 0.5.0-5
- Include bugfix from upstream.
- Put fortran module in correct place. BZ 523539

* Mon Jul 27 2009 Matthew Truch <matt at truch.net> - 0.5.0-4
- Disable verbose debugging output.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Feb 23 2009 Matthew Truch <matt at truch.net> - 0.5.0-2
- Bump for mass rebuild.

* Fri Jan 16 2009 Matthew Truch <matt at truch.net> - 0.5.0-1
- Upstream 0.5.0
-   Includes bugfixes.
-   New gzip and bzip2 encoded dirfile read ability.
-   Uses ltdl dynamic module loading for gzip and bzip2 modules.

* Tue Nov 18 2008 Matthew Truch <matt at truch.net> - 0.4.2-1
- Upstream 0.4.2.
-   Includes several bugfixes, especially to the legacy interface.

* Sat Nov 1 2008 Matthew Truch <matt at truch.net> - 0.4.0-1
- Upstream 0.4.0.

* Thu Oct 16 2008 Matthew Truch <matt at truch.net> - 0.3.1-2
- Remove mention of static libs in description
- Include TODO in doc.  
- Cleanup man-pages file glob.
- Include signature.

* Wed Sep 24 2008 Matthew Truch <matt at truch.net> - 0.3.1-1
- Upstream 0.3.1.
-   Includes former c++ compile fix patch
-   Includes bug fixes to legacy API.

* Fri Sep 19 2008 Matthew Truch <matt at truch.net> - 0.3.0-1
- Initial Fedora build.

