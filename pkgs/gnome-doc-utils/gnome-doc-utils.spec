%global momorel 1
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}

### Abstract ###

Name: gnome-doc-utils
Version: 0.20.10
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+ and GFDL
Group: Development/Tools
Summary: Documentation utilities for GNOME
URL: http://www.gnome.org/
#VCS: git:git://git.gnome.org/gnome-doc-utils
Source: http://download.gnome.org/sources/gnome-doc-utils/0.20/%{name}-%{version}.tar.xz
NoSource: 0
BuildArch: noarch

### Patches ###

# RH bug #438638 / GNOME bug #524207
Patch1: gnome-doc-utils-0.14.0-package.patch

### Dependencies ###

Requires: libxml2 >= 2.6.12
Requires: libxslt >= 1.1.8
Requires: libxml2-python
# for /usr/share/aclocal
Requires: automake
# for /usr/share/gnome/help
#Requires: yelp
# Currently creates a chicken/egg problem; gnome-doc-utils is needed in
# the build-chain for yelp, thus making it nearly impossible to ever
# update yelp for say newer Firefox.
Requires: gnome-doc-utils-stylesheets = %{version}-%{release}

# momonga linux requires -devel package
Provides: gnome-doc-utils-devel
Obsoletes: gnome-doc-utils-devel <= 0.20.6

### Build Dependencies ###

BuildRequires: libxml2-devel >= 2.6.12
BuildRequires: libxslt-devel >= 1.1.8
BuildRequires: libxml2-python

BuildRequires: intltool
BuildRequires: gettext
BuildRequires: scrollkeeper

%description
gnome-doc-utils is a collection of documentation utilities for the GNOME
project. Notably, it contains utilities for building documentation and
all auxiliary files in your source tree.

# note that this is an "inverse dependency" subpackage
%package stylesheets
Summary: XSL stylesheets used by gnome-doc-utils
License: LGPLv2+
Group: Development/Tools
# for the validation with xsltproc to use local dtds
Requires: docbook-dtds
# for /usr/share/pkgconfig
Requires: pkgconfig
# for /usr/share/xml
Requires: xml-common

%description stylesheets
The gnome-doc-utils-stylesheets package contains XSL stylesheets which
are used by the tools in gnome-doc-utils and by yelp.

%prep
%setup -q
%patch1 -p1 -b .package

%build
%configure --disable-scrollkeeper --enable-build-utils
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

sed -i -e '/^Requires:/d' $RPM_BUILD_ROOT%{_datadir}/pkgconfig/xml2po.pc

%find_lang %{name}

%files -f %{name}.lang
%doc AUTHORS README NEWS COPYING COPYING.GPL COPYING.LGPL
%{_bindir}/*
%{_datadir}/aclocal/gnome-doc-utils.m4
%{_datadir}/gnome/help/gnome-doc-make
%{_datadir}/gnome/help/gnome-doc-xslt
%{_datadir}/gnome-doc-utils
%doc %{_mandir}/man1/xml2po.1.*
%{python_sitelib}/xml2po/
%{_datadir}/pkgconfig/gnome-doc-utils.pc
%{_datadir}/pkgconfig/xml2po.pc

%files stylesheets
%{_datadir}/xml/gnome
%{_datadir}/xml/mallard

%changelog
* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.10-1m)
- update to 0.20.10

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20.6-2m)
- rebuild for python-2.7

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.6-1m)
- update to 0.20.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.5-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.5-1m)
- update to 0.20.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.2-1m)
- update to 0.20.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20.1-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.1-1m)
- update to 0.20.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.0-1m)
- update to 0.20.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19.4-1m)
- update to 0.19.4

* Mon Nov 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.1-1m)
- update to 0.18.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.0-1m)
- update to 0.18.0

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17.4-1m)
- update to 0.17.4

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.1-2m)
- add devel package

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.1-1m)
- update to 0.16.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.2-1m)
- update to 0.15.2

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.1-1m)
- update to 0.15.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.2-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.1-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Dec 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Mon Sep 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.2-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-1m)
- update to 0.12.2

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Sat Apr 21 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.10.3-2m)
- use ./configure instead of %%configure
- sorry for my careless mistake of 0.10.3-1m

* Sat Apr 21 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3
- define _target_cpu macro for configure script's error

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-3m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2 (unstable)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Mon Feb 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-2m)
- fix conflict directories

* Sun Dec 4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4
- GNOME 2.12.2 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-4m)
- comment out make check

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-3m)
- comment out autoreconf

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-2m)
- autoreconf with force option

* Tue Nov 15 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- start
- GNOME 2.12.1 Desktop
