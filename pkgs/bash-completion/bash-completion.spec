%global momorel 3

# Expected failures in mock, hangs in koji
%bcond_with tests

Name:           bash-completion
Version:        2.1
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Programmable completion for Bash

Group:          System Environment/Shells
License:        GPLv2+
URL:            http://bash-completion.alioth.debian.org/
Source0:        http://bash-completion.alioth.debian.org/files/%{name}-%{version}.tar.bz2
NoSource:       0
Source2:        CHANGES.package.old
# https://bugzilla.redhat.com/677446, see also noblacklist patch
Source3:        %{name}-2.0-redefine_filedir.bash
# https://bugzilla.redhat.com/677446, see also redefine_filedir source
Patch0:         %{name}-1.99-noblacklist.patch
# Commands included in util-linux >= 2.23-rc2
Patch1:         %{name}-2.1-util-linux-223.patch


Patch100:       %{name}-2.1-tar.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
%if %{with tests}
BuildRequires:  dejagnu
BuildRequires:  screen
BuildRequires:  tcllib
%endif
Requires:       bash >= 4
Requires:       coreutils

%description
bash-completion is a collection of shell functions that take advantage
of the programmable completion feature of bash.


%prep
%setup -q
%patch0 -p1
%patch1 -p1
install -pm 644 %{SOURCE2} .

%patch100 -p1 -b .mo_tar

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT %{name}-files.list

make install DESTDIR=$RPM_BUILD_ROOT

# Updated completion shipped in cowsay package:
rm $RPM_BUILD_ROOT%{_datadir}/bash-completion/completions/{cowsay,cowthink}

# Updated completion shipped in NetworkManager package:
rm $RPM_BUILD_ROOT%{_datadir}/bash-completion/completions/nmcli

install -Dpm 644 %{SOURCE3} \
    $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d/redefine_filedir

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc AUTHORS CHANGES CHANGES.package.old COPYING README
# Temporarily not noreplace for < 1.90 to 1.90+ updates (changed location)
%config %{_sysconfdir}/profile.d/bash_completion.sh
%{_sysconfdir}/bash_completion.d/
%{_datadir}/bash-completion/
%{_datadir}/pkgconfig/bash-completion.pc


%changelog
* Sun Jul  7 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:2.1-3m)
- remove nmcli to avoid conflicting with new NetworkManager

* Fri May 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.1-2m)
- some commands merged into util-linux-2.23

* Wed May  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:2.1-1m)
- update 2.1

* Fri Dec 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:2.0-1m)
- update 2.0

* Wed Oct 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.3-3m)
- rebuild for java-1.7.0-openjdk

* Wed Nov 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.3-2m)
- re-enable use tar zxvf .bz2|.xz|.lzma (Patch100)

* Tue Sep  8 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1:1.3-1m)
- version up 1.3
- sync Fedora 16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.0-5m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0-4m)
- add epoch to %%changelog

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.0-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.0-1m)
- [SECURITY] https://bugzilla.redhat.com/show_bug.cgi?id=492589
- sync with Fedora 11 (1:1.0-2)
- add Epoch
-- 
-- * Tue Apr  7 2009 Ville Skytta <ville.skytta at iki.fi> - 1:1.0-2
-- - Apply upstream patch to fix quoting issues with bash 4.x (#490322).
-- 
-- * Mon Apr  6 2009 Ville Skytta <ville.skytta at iki.fi> - 1:1.0-1
-- - 1.0.
-- 
-- * Mon Mar 23 2009 Ville Skytta <ville.skytta at iki.fi> - 20080705-4.20090314gitf4f0984
-- - Add dependency on coreutils for triggers (#490768).
-- - Update and improve mock completion.
-- 
-- * Sun Mar 15 2009 Ville Skytta <ville.skytta at iki.fi> - 20080705-3.20090314gitf4f0984
-- - git snapshot f4f0984, fixes #484578 (another issue), #486998.
-- 
-- * Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080705-3.20090211git47d0c5b
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Wed Feb 11 2009 Ville Skytta <ville.skytta at iki.fi> - 20080705-2.20090211git47d0c5b
-- - git snapshot 47d0c5b, fixes #484578.
-- - lzop and repomanage completions included upstream.
-- 
-- * Sun Jan 18 2009 Ville Skytta <ville.skytta at iki.fi> - 20080705-2.20090115bzr1252
-- - r1252 snapshot; all patches applied upstream.
-- - Do not install mercurial completion, an updated version is shipped with it.
-- - Improve lzop and repomanage completion.
-- 
-- * Tue Jan  6 2009 Ville Skytta <ville.skytta at iki.fi> - 20080705-1
-- - 20080705; new upstream at http://bash-completion.alioth.debian.org/
-- - Perl, Debian, and scp patches applied upstream.
-- - Patch to improve man completion: more sections, better filename handling.
-- - Patch to speed up yum install/deplist completion (#478784).
-- - Patch to fix and speed up rpm installed packages completion.
-- - Update mock completion.
-- 
-- * Thu Sep 25 2008 Ville Skytta <ville.skytta at iki.fi>
-- - More Matroska associations (#463829, based on patch from Yanko Kaneti).
-- 
-- * Thu Sep 11 2008 Ville Skytta <ville.skytta at iki.fi> - 20060301-13
-- - Borrow/improve/adapt to Fedora some patches from Mandriva: improved support
--   for getent and rpm --eval, better rpm backup file avoidance, lzma support.
-- - Patch/unpatch to fix gzip and bzip2 options completion.
-- - Patch to add --rsyncable to gzip options completion.
-- - Add and trigger-install support for lzop.
-- - Associate *.sqlite with sqlite3.
-- 
-- * Wed Jul 23 2008 Ville Skytta <ville.skytta at iki.fi> - 20060301-12
-- - Fix plague-client completion install (#456355, Ricky Zhou).
-- - Trigger-install support for sitecopy.
-- 
-- * Tue Apr 29 2008 Ville Skytta <ville.skytta at iki.fi> - 20060301-11
-- - Media player association improvements (#444467).
-- 
-- * Sat Feb 23 2008 Ville Skytta <ville.skytta at iki.fi> - 20060301-10
-- - Patch to fix filename completion with svn (#430059).
-- - Trigger-install support for dsniff.
-- - Drop disttag.
-- 
-- * Mon Dec 31 2007 Ville Skytta <ville.skytta at iki.fi> - 20060301-8
-- - Associate VDR recording files with media players.
-- - Update mock completion.
-- 
-- * Fri Nov 16 2007 Ville Skytta <ville.skytta at iki.fi> - 20060301-7
-- - Add JPEG2000 extensions for display(1) (#304771).
-- - Update mock completion.
-- 
-- * Sat Sep 22 2007 Ville Skytta <ville.skytta at iki.fi> - 20060301-6
-- - Patch to improve perl completion (#299571, Jim Radford,
--   http://use.perl.org/~Alias/journal/33508).
-- 
-- * Mon Aug 13 2007 Ville Skytta <ville.skytta at iki.fi> - 20060301-5
-- - License: GPLv2+
-- 
-- * Sun Jun 24 2007 Jeff Sheltren <sheltren@cs.ucsb.edu> - 20060301-4
-- - Update triggers to work with older versions of RPM
-- 
-- * Wed Feb 28 2007 Ville Skytta <ville.skytta at iki.fi> - 20060301-3
-- - Fix scp with single quotes (#217178).
-- - Borrow fix for bzip2 w/spaces, and apropos and whatis support from Debian.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060301-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20060301-6m)
- rebuild against gcc43

* Wed Feb 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20060301-5m)
- add %%do_triggerin and %%do_triggerun from fc 20060301-8
- add Patch2:         %{name}-20060301-perl-299571.patch
- add Patch3:         %{name}-20060301-jpeg2000-304771.patch
- add Patch4:         %{name}-20060301-vdrfiles.patch
- apply patch0

* Mon Aug 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20060301-4m)
- PreReq: coreutils

* Sun Mar 11 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (20060301-3m)
- enable use tar zxvf *bz2 (Patch100)
- import from fc Patch0:         %{name}-20060301-scp-apos-217178.patch
- import from fc Patch1:         %{name}-20060301-debian.patch

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20060301-2m)
- import new lilypond.bash-completion from cooker

* Sun Feb 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (20060301-1m)
- import to Momonga from fc-extras

* Thu Aug 31 2006 Ville Skytta <ville.skytta at iki.fi> - 20060301-2
- Trigger-install support for gcl, lilypond, mercurial and svk.
- Improve mock completion a bit.

* Thu Mar  2 2006 Ville Skytta <ville.skytta at iki.fi> - 20060301-1
- 20060301, patches and profile.d scriptlet applied/included upstream.
- Convert docs to UTF-8.

* Wed Feb  8 2006 Ville Skytta <ville.skytta at iki.fi> - 20050721-4
- Don't source ourselves in non-interactive shells (#180419, Behdad Esfahbod).
- Trigger-install snippets for clisp, gnatmake, isql, ri, sbcl, and snownews.

* Sat Feb  4 2006 Ville Skytta <ville.skytta at iki.fi>
- Add mtr(8) completion using known hosts (#179918, Yanko Kaneti).

* Sun Jan  8 2006 Ville Skytta <ville.skytta at iki.fi> - 20050721-3
- Patch to hopefully fix quoting problems with bash 3.1 (#177056).

* Mon Nov 28 2005 Ville Skytta <ville.skytta at iki.fi> - 20050721-2
- Work around potential login problem in profile.d snippet (#174355).

* Sat Nov 26 2005 Ville Skytta <ville.skytta at iki.fi>
- Don't mark the main source file as %%config.
- Make profile.d snippet non-executable (#35714) and noreplace.
- Add mock, plague-client and repomanage completion.
- Allow "cvs stat" completion.
- Macroize trigger creation.

* Fri Jul 22 2005 Ville Skytta <ville.skytta at iki.fi> - 20050721-1
- 20050721.

* Wed Jul 20 2005 Ville Skytta <ville.skytta at iki.fi> - 20050720-1
- 20050720, all patches applied upstream.

* Mon Jul 18 2005 Ville Skytta <ville.skytta at iki.fi> - 20050712-1
- 20050712.
- Add more OO.o2 extensions, and *.pdf for evince (#163520, Horst von Brand).
- Add/fix support for some multimedia formats and players.
- Fix tarball completion.

* Sat Jan 22 2005 Ville Skytta <ville.skytta at iki.fi> - 0:20050121-2
- Update to 20050121.

* Thu Jan 13 2005 Ville Skytta <ville.skytta at iki.fi> - 0:20050112-1
- Update to 20050112, openssl patch applied upstream.

* Wed Jan  5 2005 Ville Skytta <ville.skytta at iki.fi> - 0:20050103-1
- Update to 20050103.

* Sat Nov 27 2004 Ville Skytta <ville.skytta at iki.fi> - 0:20041017-5
- Change version scheme, bump release to provide Extras upgrade path.

* Sat Nov  6 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.4.20041017
- Do the right thing with bash >= 3 too in profile.d snippet (bug 2228, thanks
  to Thorsten Leemhuis).

* Mon Oct 18 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20041017
- Update to 20041017, adds dhclient, lvm, and bittorrent completion.

* Mon Jul 12 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20040711
- Update to 20040711, patches applied upstream.

* Sun Jul  4 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20040704
- Update to 20040704.
- Change to symlinked /etc/bash_completion.d snippets, add patch to read them.

* Wed May 26 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20040526
- Update to 20040526.

* Thu Apr  1 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20040331
- Add command-specific contrib snippet copying triggers.

* Thu Apr  1 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20040331
- Update to 20040331.

* Sun Feb 15 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20040214
- Update to 20040214.

* Wed Feb 11 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20040210
- Update to 20040210.

* Fri Jan  2 2004 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20040101
- Update to 20040101.
- Update %%description.

* Sat Dec 27 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20031225
- Update to 20031225.

* Sat Dec 20 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20031215
- Don't pull in *.rpm* from %%{_sysconfdir}/bash_completion.d.

* Mon Dec 15 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20031215
- Update to 20031215.

* Sun Nov 30 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20031125
- Update to 20031125.

* Thu Nov 13 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20031112
- Update to 20031112.

* Wed Oct 22 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20031022
- Update to 20031022.

* Tue Oct  7 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20031007
- Update to 20031007.

* Tue Sep 30 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20030929
- Update to 20030929.

* Fri Sep 12 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20030911
- Update to 20030911.

* Thu Aug 21 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20030821
- Update to 20030821.
- Drop .nosrc.rpm patch, already applied upstream.

* Sat Aug 16 2003 Ville Skytta <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20030811
- Update to 20030811.
- Patch to make rpm --rebuild work with .nosrc.rpm's.

* Sun Aug  3 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030803
- Update to 20030803.

* Wed Jul 23 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030721
- Update to 20030721.

* Sun Jul 13 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030713
- Update to 20030713.

* Mon Jun 30 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030630
- Update to 20030630.

* Sun Jun  8 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030607
- Update to 20030607.

* Tue May 27 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030527
- Update to 20030527.

* Sat May 24 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.0-0.fdr.1.20030505
- First build.
