%global momorel 25

%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global apelver 10.8-6m

%global diredver 0.1.0
%global dndver 0.0.0
%global gfver 1.5.3
%global gobyver 0.0.0
%global howmver 0.1.3
%global serverver 0.2.0
%global w3mver 0.2.2
%global wlver 0.8.0
%global speedbarver 0.0.0
%global colorthemever 0.0.0

Summary: Screen Manager for Emacs
Name: emacs-elscreen
Version: 1.4.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Editors
URL: http://www.morishima.net/~naoto/software/elscreen/
Source0: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-%{version}.tar.gz
Source1: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-dired-%{diredver}.tar.gz
Source2: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-dnd-%{dndver}.tar.gz
Source3: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-gf-%{gfver}.tar.gz
Source4: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-goby-%{gobyver}.tar.gz
Source5: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-howm-%{howmver}.tar.gz
Source6: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-server-%{serverver}.tar.gz
Source7: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-w3m-%{w3mver}.tar.gz
Source8: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-wl-%{wlver}.tar.gz
Source9: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-speedbar-%{speedbarver}.tar.gz
Source10: ftp://ftp.morishima.net/pub/morishima.net/naoto/ElScreen/elscreen-color-theme-%{colorthemever}.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: emacs
Requires: emacs-apel >= %{apelver}
Obsoletes: elscreen-emacs
Obsoletes: elscreen-xemacs

Obsoletes: elisp-elscreen
Provides: elisp-elscreen

%description
ElScreen provides the ease-to-use environment to save or restore
several window-configurations.

Key features of ElScreen are:
* Key bindings are similar to GNU Screen.
* Screens are tabbed which makes it easier and faster to switch among applications.
* ElScreen menu is also added to menu-bar to create, select or delete screens.
* Each frame keeps up to 10 window-configurations.
* List of screens can be shown in mini-buffer.
* You can assign your own name to each window.

%prep
%setup -q -a 1 -a 2 -a 3 -a 4 -a 5 -a 6 -a 7 -a 8 -a 9 -a 10 -n elscreen-%{version}

%build

%install
rm -rf %{buildroot}
# for the time being, do not byte compile
%{__mkdir_p} %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-dired-%{diredver}/elscreen-dired.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-dnd-%{dndver}/elscreen-dnd.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-gf-%{gfver}/elscreen-gf.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-goby-%{gobyver}/elscreen-goby.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-howm-%{howmver}/elscreen-howm.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-server-%{serverver}/elscreen-server.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-w3m-%{w3mver}/elscreen-w3m.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-wl-%{wlver}/elscreen-wl.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-speedbar-%{speedbarver}/elscreen-speedbar.el %{buildroot}%{e_sitedir}/elscreen
%{__install} -m 644 elscreen-color-theme-%{colorthemever}/elscreen-color-theme.el %{buildroot}%{e_sitedir}/elscreen

rm -f elscreen-gf-%{gfver}/elscreen-gf.el

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README elscreen-gf-%{gfver}
%{e_sitedir}/elscreen

%changelog
* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-25m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.6-24m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-23m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-22m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-20m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-19m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-18m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-17m)
- merge elscreen-emacs to elisp-elscreen
- kill elscreen-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-15m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-14m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-13m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-12m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-11m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-10m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-9m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-8m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-7m)
- update elscreen-howm to 0.1.3

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-6m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-5m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-2m)
- rebuild against gcc43

* Mon Dec 31 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Sat Nov 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-7m)
- update elscreen-server to 0.2.0

* Thu Nov 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-6m)
- add elscreen-color-theme-0.0.0
- License: GPLv2

* Mon Nov 19 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-5m)
- add elscreen-speedbar-0.0.0

* Sat Nov 10 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-4m)
- update elscreen-gf to 1.5.3

* Mon Nov  5 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-3m)
- update elscreen-wl to 0.8.0

* Tue Oct 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-2m)
- update elscreen-server to 0.1.0

* Wed Aug 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-1m)
- initial packaging
