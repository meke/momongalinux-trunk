%global momorel 7

Summary: A multi-threaded FTP client for the X Window System
Name: gftp
Version: 2.0.19
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Url: http://gftp.seul.org/
Source0: http://www.gftp.org/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-stropts.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2.2.0
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: readline-devel >= 5.0

%description
gFTP is a multi-threaded FTP client for the X Window System. gFTP
supports simultaneous downloads, resumption of interrupted file
transfers, file transfer queues to allow downloading of multiple
files, support for downloading entire directories/subdirectories, a
bookmarks menu to allow quick connection to FTP sites, caching of
remote directory listings, local and remote chmod, drag and drop, a
connection manager and much more.

Install gftp if you need an FTP client.

%prep
%setup -q

%patch0 -p1 -b .stropts

%build
%ifarch alpha alphaev5
MIEEE_FLAG="-mieee"
%endif
CFLAGS="%{optflags} $MIEEE_FLAG" %configure
make %{?_smp_mflags} || make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog* INSTALL README* THANKS TODO
%doc docs/USERS-GUIDE
%{_bindir}/gftp
%{_bindir}/gftp-gtk
%{_bindir}/gftp-text
%{_datadir}/applications/gftp.desktop
%{_datadir}/gftp
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_mandir}/man*/gftp.1*
%{_datadir}/pixmaps/gftp.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.19-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.19-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.19-5m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.19-4m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.19-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.19-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.19-1m)
- update to 2.0.19
- remove unused patches

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.18-12m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.18-11m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-10m)
- rebuild against openssl-0.9.8h-1m

* Thu May 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.18-9m)
- remove gentoo's bug fixes (Patch0 - Patch2) to avoid crashing

* Fri Apr  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.18-8m)
- import stropts.patch from Fedora
 +* Wed Apr  2 2008 Christopher Aillon <caillon@redhat.com> - 2:2.0.18-3
 +- stropts.h was removed from glibc; don't #include it anymore

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.18-6m)
- %%NoSource -> NoSource

* Wed Nov  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.18-5m)
- [SECURITY] CVE-2007-3961 CVE-2007-3962 GLSA 200711-01
- import gftp-2.0.18-188252.patch from gentoo
 +- 21 Sep 2007; Daniel Gryniewicz <dang@gentoo.org>
 +- +files/gftp-2.0.18-188252.patch, +gftp-2.0.18-r6.ebuild:
 +- Bump to 2.0.18-r6
 +- Fix for issues in bug #188252
- http://www.gentoo.org/security/en/glsa/glsa-200711-01.xml
- http://bugs.gentoo.org/show_bug.cgi?id=188252
- merge gentoo's bug fixes (Patch0 - Patch2)

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.18-4m)
- Change Source URL 

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.18-3m)
- rebuild against readline-5.0

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.18-2m)
- rebuild against openssl-0.9.8a

* Wed Feb  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.18-1m)
- version 2.0.18
- remove desktop-file-utils from BuildRequires

* Sat Oct 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.17-2m)
- move desktop file to %%{_datadir}/applications
- add some files to %%doc
- BuildRequires: readline-devel, openssl-devel, desktop-file-utils

* Thu Apr 29 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.17-1m)
- version 2.0.17

* Thu Nov 13 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.16-1m)
- version 2.0.16

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.15-1m)
- version 2.0.15

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.14-2m)
- rebuild against for XFree86-4.3.0

* Mon Dec 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.14-1m)
- version 2.0.14

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.14-0.1.1m)
- version 2.0.14rc1

* Sat Oct  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.13-5m)
- update gtk2 patch

* Thu Sep 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.13-4m)
- update gtk2 patch

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.13-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sat Jul 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.13-2m)
- modify ja.po

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.13-1k)
- version 2.0.13

* Tue Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.12-2k)
- version 2.0.12

* Tue Jan 15 2002 Shingo Akagaki <dora@kondara.org
- (2.0.11-2k)
- version 2.0.11

* Tue Dec 04 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.8-12k)
- correct charset

* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.8-10k)
- po locale fix

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (2.0.8-8k)
- rebuild against gettext 0.10.40.

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (2.0.8-6k)
- add alphaev5 support.

* Sun May 13 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.8-4k)
- modified spec file correctly

* Thu May 10 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.8-2k)
- import to Mary

* Wed May 02 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.8-3k)
- import from rawhide.
- too old to modify original one ;-<.
- so this is the TEST RELEASE.

* Thu Apr 19 2001 Havoc Pennington <hp@redhat.com>
- Upgrade to 2.0.8, fixes a security exploit and a bunch of other
  things, adds a console frontend 

* Tue Feb 27 2001 Trond Eivind Glomsrod <teg@redhat.com>
- langify
- don't try to include two non-existing files in %%doc
- move changelog to end of file

* Thu Aug 10 2000 Havoc Pennington <hp@redhat.com>
- Set Epoch, since upstream versions are not ascending

* Mon Aug 07 2000 Havoc Pennington <hp@redhat.com>
- 2.0.7b, should fix outstanding bugs

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Jun 19 2000 Preston Brown <pbrown@redhat.com>
- FHS paths

* Fri May 19 2000 Havoc Pennington <hp@redhat.com>
- Update to 2.0.7pre3, and build for Winston

* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.0.6a-3).

* Fri Feb 11 2000 Elliot Lee <sopwith@redhat.com>
- Add -mieee on alpha to solve bug #9317

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- gzip man page

* Tue Jan 04 2000 Elliot Lee <sopwith@redhat.com>
- Update to 2.0.6a

* Sun Dec 26 1999 Toru Hoshina <t@kondara.org>
- merge Vine's patch. Thanx! > FUrukawa-san.

* Mon Dec  20 1999 Yasuyuki Furukawa <yasu@on.cs.keio.ac.jp> (Vine)
- modified default bookmarks
- added config_proxy patch to fix some bugs about proxy.
- added dnd patch to fix dnd loop bug
- added lang_c patch to avoid a trouble about NLS of ls command.

* Sun Dec 19 1999 Shingo Akagaki <dora@kondara.org>
- Upgrade to 2.0.6a release.

* Mon Dec 13 1999 Shingo Akagaki <dora@kondara.org>
- Upgrade to the full 2.0.6 release.

* Thu Dec  9 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file
- fix po/Makefile

* Tue Nov 30 1999 Shingo Akagaki <dora@kondara.org>
- fall bak 2.0.5a

* Sun Nov 28 1999 Shingo Akagaki <dora@kondara.org>
- Try 2.0.6pre1

* Fri Sep 3 1999 Elliot Lee <sopwith@redhat.com>
- Upgrade to the full 2.0.4 release. (Yes, pre1 fixed that bug.)

* Wed Sep 1 1999 Elliot Lee <sopwith@redhat.com>
- Try 2.0.4pre1 to see if it fixes bug #4700.

* Thu Jul  8 1999 Michael Fulbright <drmike@redhat.com>
- version 2.0.3

* Wed Jul  7 1999 Michael Fulbright <drmike@redhat.com>
- bumped to version 2.0.2

* Wed Mar 31 1999 Michael Fulbright <drmike@redhat.com>
- version 1.13

* Tue Mar 30 1999 Michael Fulbright <drmike@redhat.com>
- patch to fix a segfault reported by Chris Evans <chris@ferret.lmh.ox.ac.uk>

* Tue Feb 16 1999 Michael Fulbright <drmike@redhat.com>
- version 1.12

* Wed Feb 10 1999 Michael Fulbright <drmike@redhat.com>
- first attempt at spec file
