%global         momorel 5

Name:           perl-Text-Hunspell
Version:        2.08
Release:        %{momorel}m%{?dist}
Summary:        Perl interface to the GNU Hunspell library
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Text-Hunspell/
Source0:        http://www.cpan.org/authors/id/C/CO/COSIMO/Text-Hunspell-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  hunspell-devel
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides a Perl interface to the OO Hunspell library. This
module is to meet the need of looking up many words, one at a time, in a
single session, such as spell-checking a document in memory.

%prep
%setup -q -n Text-Hunspell-%{version}

%build
HUNSPELL_LIB_NAME=$(pkg-config --libs hunspell | sed 's/-l//; s/[[:space:]]*$//')
perl -pi -e "s/'hunspell'/'${HUNSPELL_LIB_NAME}'/" Makefile.PL t/00-prereq.t

%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" LIBS="-l${HUNSPELL_LIB_NAME}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes perlobject.map README
%{perl_vendorarch}/auto/Text/Hunspell
%{perl_vendorarch}/Text/Hunspell.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-2m)
- rebuild against perl-5.18.0

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-1m)
- update to 2.06

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-2m)
- rebuild against perl-5.16.2

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-1m)
- update to 2.05

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-2m)
- rebuild against perl-5.14.2

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-5m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02-4m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
