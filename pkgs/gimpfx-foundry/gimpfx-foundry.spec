%global momorel 4
%global srcver 2.6-1

Summary:          Additional GIMP plugins
Name:             gimpfx-foundry
Version:          2.6.1
Release:          %{momorel}m%{?dist}
License:          GPLv2+ and GPLv3+ and Public Domain
URL:              http://gimpfx-foundry.sourceforge.net/
Group:            Applications/Multimedia
Source0:          http://dl.sourceforge.net/project/%{name}/%{name}-scriptpack/%{name}-%{srcver}/%{name}-%{srcver}.tar.gz
NoSource:         0
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:         gimp >= 2.6.0
BuildArch:        noarch

%description
These scripts allow GIMP graphics to be endowed with special effects, such as 
blurring or distorting them in certain ways. This package has 117+ new 
scripts for GIMP that are not part of the graphic software's standard 
installation.

Among them are the Roy Lichtenstein effect script to render graphics in the 
pop artist's style, the Planet Render script to create a planet of your 
choosing and desired size and dimension. and the Old Photo script to give 
existing photos that antiquated touch.

%prep
%setup -q -c %{name}-%{version}

%build

%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}%{_datadir}/gimp/2.0/scripts/
%{__install} -p --mode=0664 -t %{buildroot}%{_datadir}/gimp/2.0/scripts/ *.scm

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc release-notes.txt
%{_datadir}/gimp/2.0/scripts/*.scm

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-1m)
- import from Fedora 13

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.6.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jun 04 2009 Rick L Vinyard Jr <rvinyard@cs.nmsu.edu> - 2.6.1-2
- Added GPLv2+ and Public Domain licenses to License tag

* Wed Jun 03 2009 Rick L Vinyard Jr <rvinyard@cs.nmsu.edu> - 2.6.1-1
- Initial version

