%global momorel 6
%global qtver 4.8.5
%global kdever 4.12.97
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global gitdate 20140413

Summary: A GraphViz dot graph viewer for KDE
Name: kgraphviewer
Version: 2.1.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Engineering
URL: http://extragear.kde.org/apps/kgraphviewer/
Source0: %{name}-%{gitdate}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: graphviz
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: graphviz-devel >= 2.36.0
BuildRequires: boost-devel >= 1.50.0

%description
KGraphViewer is user-friendly KDE application for viewing dot graphs 
created for processing with GraphViz. Notable features provided include:

- loading of several graphs in tabs
- bird's-eye graph view
- zooming view
- simple printing
- context menu and toolbar for selecting layout algorithms
- session management

%prep
%setup -q -n %{name}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# revise desktop files
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-category Application \
  %{buildroot}%{_kde4_datadir}/applications/kde4/kgrapheditor.desktop

desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-category Application \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

## Should we remove devel stuff files ?

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q &> /dev/null
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog INSTALL README TODO
%{_kde4_includedir}/%{name}
%{_kde4_bindir}/kgrapheditor
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/lib*.so*
%{_kde4_libdir}/kde4/%{name}part.so
%{_kde4_datadir}/applications/kde4/*.desktop
%{_kde4_appsdir}/kgrapheditor
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/%{name}part
%{_kde4_datadir}/config.kcfg/*.kcfg
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_kde4_datadir}/kde4/services/*.desktop

%changelog
* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-6m)
- rebuild against graphviz-2.36.0
- use source from git (20140413)

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-5m)
- add source

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-4m)
- rebuild against graphviz-2.28.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Tue Nov  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-1m)
- update to 2.1

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-25m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-24m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-23m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-22m)
- rebuild against qt-4.6.3-1m

* Wed May 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-21m)
- add BuildRequires

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-20m)
- touch up spec file

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-19m)
- new source for KDE 4.4.0

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-18m)
- new source for KDE 4.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-16m)
- NoSource again

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-15m)
- no NoSource for STABLE_6

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-14m)
- new source for KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-13m)
- new source for KDE 4.3.0

* Fri Jul 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-12m)
- [BUILD FIX] apply upstream fix to enable build with KDE 4.2.96

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-11m)
- new source for KDE 4.2.4

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-10m)
- new source for KDE 4.2.3

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-9m)
- new source for KDE 4.2.2

* Sun Feb  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-8m)
- new source for KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-7m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-6m)
- new source for KDE 4.2 RC

* Mon Dec  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-5m)
- update desktop.patch for lost icon
- revise desktop-file-install section
- clean up %%install section

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-4m)
- rebuild from new source

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-3m)
- new source for KDE 4.1

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-2m)
- new source for KDE 4.1 RC 1

* Sun Jun  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2 (KDE 4.1 beta 1)

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-7m)
- rebuild against gcc43

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-6m)
- update Source0 (use stable/4.0.2/src/extragear)
- set ftpdirver 4.0.2

* Tue Feb 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-5m)
- update Source0 (use stable/4.0.1/src/extragear)

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-4m)
- revise kgrapheditor.desktop again

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-3m)
- revise and clean up desktop files

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-2m)
- re-add desktop.patch

* Mon Jan 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-1m)
- update to 2.0 for KDE4

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-2m)
- change "Categories" of kgraphviewer.desktop from Math to Graphics

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-1m)
- version 1.0.4
- update desktop.patch

* Thu Apr 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-2m)
- apply some fixes from svn.kde.org

* Wed Apr 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-1m)
- initial package for Momonga Linux
