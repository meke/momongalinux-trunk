%global momorel 4

Name:		fpaste
Version:	0.3.4
Release:	%{momorel}m%{?dist}
Summary:	A simple tool for pasting info onto fpaste.org
BuildArch:	noarch
Group:		Applications/Internet
License:	GPLv3+
URL:		https://fedorahosted.org/fpaste/
Source0:	https://fedorahosted.org/released/fpaste/fpaste-%{version}.tar.bz2
NoSource:       0
Requires:	xsel

%description
It is often useful to be able to easily paste text to the Fedora
Pastebin at http://fpaste.org and this simple script will do that
and return the resulting URL so that people may examine the
output. This can hopefully help folks who are for some reason 
stuck without X, working remotely, or any other reason they may
be unable to paste something into the pastebin

%prep
%setup -q 

%build
#nothing required

%install
mkdir -p %{buildroot}%{_bindir}
make install BINDIR=%{buildroot}%{_bindir} MANDIR=%{buildroot}%{_mandir}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%doc CHANGELOG COPYING README TODO
%{_mandir}/man1/fpaste.1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.4-1m)
- import from Fedora

* Mon Aug 24 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.4-1
- New release
- Validate paste size and content as non-binary before sending; user is prompted Y/N to override for huge pastes or binary content.
- Added options --printonly and --confirm to show or ask user before sending
- --sysinfo updated: Added blkid, top CPU hogs, top memory hogs, X errors,h/w virtualization, 64-bit capable, and last few reboot/runlevel changes
- Workaround to read user input from raw_input following sys.stdin EOF
- Guess language syntax from common file extension(s)
- --help usage compacted and grouped
- Check that 'xsel' is installed for clipboard input support; silent fail on output
- Use 'fpaste/x.x.x' User-agent header
- bug fixes
* Fri Aug 21 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.3-2
- Added xsel in requires
* Fri Aug 21 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.3-1
- new package release
- Proper urllib2 error handling
- Catches Ctrl-C while waiting for stdin to show a usage reminder rather than a traceback
- Typos fixed, and more TODO
- Added --sysinfo option to gather and pastebin basic system information
- Added options to read text from (xsel) clipboard and write resultant URL to clipboard
* Thu Aug 13 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.2-2
- Corrected source0 field
* Thu Aug 13 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.2-1
- New tar 
- Man page included in tar
* Wed Aug 12 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.1-2
- Review request begins : #516698
- Removed buildroot declaration and removal from install section in accordance with new guidelines
- Aligned description properly
* Tue Aug 11 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 0.3.1-1
- Initial rpm build
