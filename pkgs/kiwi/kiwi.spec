%global momorel 9
%global pythonver 2.7
%global python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A Framework for developing graphical applications in Python
Name: kiwi
Version: 1.9.25
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
URL: http://www.async.com.br/projects/kiwi/index.html
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/1.9/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: ja.po
Requires: rarian
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel
BuildRequires: pygtk2-devel
BuildRequires: python >= %{pythonver}
BuildRequires: libglade2-devel

%description
Kiwi is a framework composed of a set of modules, which eases Python
development using PyGTK. Kiwi makes graphical applications *much* faster
to code, with good architecture and more Python-like bindings; it also
offers extended widgets based upon the original PyGTK widgets.

%prep
%setup -q
cp %{SOURCE1} po

%build
python setup.py build

%install
rm -rf --preserve-root %{buildroot}
python setup.py install --skip-build --root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}-i18n
%{_bindir}/%{name}-ui-test
%{python_sitelib}/%{name}-*.egg-info
%{python_sitelib}/%{name}
%{_libdir}/python*/site-packages/gazpacho/widgets/kiwiwidgets.py* 
%{_datadir}/kiwi/
%{_datadir}/gazpacho/*/*
%{_datadir}/locale/*/*/*
%{_datadir}/doc/%{name}

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.25-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.25-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.25-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.25-6m)
- full rebuild for mo7 release

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.25-5m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.25-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Apr 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.25-1m)
- update to 1.9.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.22-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.9.22-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Jun  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.22-1m)
- update to 1.9.22

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.21-4m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.21-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.21-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.21-1m)
- update to 1.9.21

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.19-3m)
- %%NoSource -> NoSource

* Mon Nov 19 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.19-2m)
- source file was changed

* Sat Nov 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.19-1m)
- update to 1.9.19

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.18-1m)
- update to 1.9.18

* Fri Aug 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.17-1m)
- update to 1.9.17

* Tue Jul 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.16-1m)
- update to 1.9.16

* Thu May 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.15-1m)
- update to 1.9.15

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.9.12-4m)
- rebuild against python-2.5-9m

* Mon Feb  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.12-3m)
- add ja.po

* Sun Feb  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.12-2m)
- fix %%files for lib64

* Sun Feb  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.12-1m)
- initila build
