diff -urN disktype-9/disktype.1 disktype-9-ewf/disktype.1
--- disktype-9/disktype.1	2005-02-21 20:46:02.000000000 +0000
+++ disktype-9-ewf/disktype.1	2008-05-01 21:24:37.000000000 +0100
@@ -46,7 +46,8 @@
 Linux LVM2 physical volumes, Solaris x86 disklabel (vtoc),
 Solaris SPARC disklabel.
 .It Other structures:
-Debian split floppy header, Linux swap.
+Debian split floppy header, Linux swap, Expert Witness Compression Format
+(EWF).
 .It Disk images:
 Raw CD image (.bin), Virtual PC hard disk image,
 Apple UDIF disk image (limited).
diff -urN disktype-9/ewf.c disktype-9-ewf/ewf.c
--- disktype-9/ewf.c	1970-01-01 01:00:00.000000000 +0100
+++ disktype-9-ewf/ewf.c	2008-05-01 21:11:48.000000000 +0100
@@ -0,0 +1,127 @@
+/*
+ * ewf.c
+ * Layered data source for EWF images via libewf.
+ * For use with disktype, Copyright (c) 2003 Christoph Pfisterer
+ *
+ * Copyright (c) 2007 David Loveall
+ *
+ * Permission is hereby granted, free of charge, to any person
+ * obtaining a copy of this software and associated documentation
+ * files (the "Software"), to deal in the Software without
+ * restriction, including without limitation the rights to use, copy,
+ * modify, merge, publish, distribute, sublicense, and/or sell copies
+ * of the Software, and to permit persons to whom the Software is
+ * furnished to do so, subject to the following conditions:
+ *
+ * The above copyright notice and this permission notice shall be
+ * included in all copies or substantial portions of the Software.
+ *
+ * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
+ * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
+ * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
+ * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
+ * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
+ * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
+ * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
+ * SOFTWARE. 
+ */
+
+#include "global.h"
+
+#ifdef USE_LIBEWF
+#include <libewf.h>
+
+/*
+ * types
+ */
+
+typedef struct ewf_source {
+  SOURCE c;
+  LIBEWF_HANDLE *e;
+} EWF_SOURCE;
+
+/*
+ * helper functions
+ */
+
+static SOURCE *init_ewf_source(char * const filenames[], uint16_t file_amount);
+static int read_block_ewf(SOURCE *s, u8 pos, void *buf);
+static void close_ewf(SOURCE *s);
+
+/*
+ * ewf analyzer
+ */
+
+void analyze_ewf(char * const filenames[], uint16_t file_amount)
+{
+  SOURCE *s;
+
+  /* create and analyze wrapped source */
+  s = init_ewf_source(filenames, file_amount);
+  analyze_source(s, 1);
+  close_source(s);
+}
+
+/*
+ * initialize the ewf source
+ */
+
+static SOURCE *init_ewf_source(char * const filenames[], uint16_t file_amount)
+{
+  EWF_SOURCE *src;
+
+  src = (EWF_SOURCE *)malloc(sizeof(EWF_SOURCE));
+  if (src == NULL)
+    bailout("Out of memory");
+  memset(src, 0, sizeof(EWF_SOURCE));
+
+  src->e = libewf_open(filenames, file_amount, LIBEWF_OPEN_READ);
+  if (src->e == NULL)
+    bailout("Can't open EWF file");
+
+  src->c.size_known = 1;
+
+#if defined( LIBEWF_STRING_DIGEST_HASH_LENGTH_MD5 )
+  src->c.size = libewf_get_media_size(src->e);
+  src->c.blocksize = libewf_get_bytes_per_sector(src->e);
+#else
+  if( libewf_get_media_size( src->e, &( src->c.size ) ) != 1 )
+    bailout("Unable to get media size of EWF file");
+  if( libewf_get_bytes_per_sector( src->e, (uint32_t *) &( src->c.blocksize ) ) != 1 )
+    bailout("Unable to get sector size of EWF file");
+#endif
+  src->c.read_block = read_block_ewf;
+  src->c.close = close_ewf;
+
+  return (SOURCE *)src;
+}
+
+/*
+ * raw read
+ */
+
+static int read_block_ewf(SOURCE *s, u8 pos, void *buf)
+{
+  EWF_SOURCE *es = (EWF_SOURCE *)s;
+
+  /* read from lower layer */
+  if(libewf_read_random(es->e, buf, es->c.blocksize, pos) == -1)  {
+    return 0;
+  }
+
+  return 1;
+}
+
+/*
+ * cleanup
+ */
+
+static void close_ewf(SOURCE *s)
+{
+  EWF_SOURCE *es = (EWF_SOURCE *)s;
+
+  libewf_close(es->e);
+}
+
+#endif
+/* EOF */
diff -urN disktype-9/main.c disktype-9-ewf/main.c
--- disktype-9/main.c	2003-05-24 18:35:44.000000000 +0100
+++ disktype-9-ewf/main.c	2008-05-01 21:11:48.000000000 +0100
@@ -27,6 +27,10 @@
 
 #include "global.h"
 
+#ifdef USE_LIBEWF
+#include <libewf.h>
+#endif
+
 #ifdef USE_MACOS_TYPE
 #include <CoreServices/CoreServices.h>
 #endif
@@ -38,6 +42,10 @@
 static void analyze_file(const char *filename);
 static void print_kind(int filekind, u8 size, int size_known);
 
+#ifdef USE_LIBEWF
+void analyze_ewf(char * const filenames[], uint16_t file_amount);
+#endif
+
 #ifdef USE_MACOS_TYPE
 static void show_macos_type(const char *filename);
 #endif
@@ -59,7 +67,19 @@
   /* loop over filenames */
   print_line(0, "");
   for (i = 1; i < argc; i++) {
+#ifndef USE_LIBEWF
     analyze_file(argv[i]);
+#endif
+#ifdef USE_LIBEWF
+    if (libewf_check_file_signature(argv[i]) == 1) {
+      print_line(0, "EWF image");
+      analyze_ewf(&argv[i], argc-i);
+      return 0;
+    }
+    else {
+      analyze_file(argv[i]);
+    }
+#endif
     print_line(0, "");
   }
 
diff -urN disktype-9/Makefile disktype-9-ewf/Makefile
--- disktype-9/Makefile	2006-01-12 17:55:15.000000000 +0000
+++ disktype-9-ewf/Makefile	2008-05-01 21:12:04.000000000 +0100
@@ -9,7 +9,7 @@
          buffer.o file.o cdaccess.o cdimage.o vpc.o compressed.o \
          detect.o apple.o amiga.o atari.o dos.o cdrom.o \
          linux.o unix.o beos.o archives.o \
-         udf.o blank.o cloop.o
+         udf.o blank.o cloop.o ewf.o
 
 TARGET = disktype
 
@@ -43,6 +43,11 @@
   endif
 endif
 
+ifneq ($(LIBEWF),)
+  CPPFLAGS += -DUSE_LIBEWF
+  LIBS     += -lewf
+endif
+
 # real making
 
 all: $(TARGET)
diff -urN disktype-9/Seedfile disktype-9-ewf/Seedfile
--- disktype-9/Seedfile	2006-01-12 17:55:15.000000000 +0000
+++ disktype-9-ewf/Seedfile	2008-05-01 21:11:48.000000000 +0100
@@ -7,7 +7,7 @@
          buffer.c file.c cdaccess.c cdimage.c vpc.c compressed.c
          detect.c apple.c amiga.c atari.c dos.c cdrom.c
          linux.c unix.c beos.c archives.c
-         udf.c blank.c cloop.c;
+         udf.c blank.c cloop.c ewf.c;
 
   cflags "-D_LARGEFILE_SOURCE" "-D_FILE_OFFSET_BITS=64";
 }
