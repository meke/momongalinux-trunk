%global momorel 5

Name:           disktype
Version:        9
Release:        %{momorel}m%{?dist}
Summary:        Detect the content format of a disk or disk image

Group:          Applications/File
License:        MIT
URL:            http://disktype.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/disktype/disktype-9.tar.gz
NoSource:       0
Patch0:         makefile.patch
Patch1:         ewf.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libewf-devel

%description
The purpose of disktype is to detect the content format of a disk or disk
image. It knows about common file systems, partition tables, and boot codes.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
make %{?_smp_mflags} LIBEWF=1

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}{%{_bindir},%{_mandir}/man1}
install -m 755 disktype %{buildroot}%{_bindir}
install -p -m 644 disktype.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc HISTORY LICENSE TODO
%{_bindir}/*
%{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9-1m)
- import from Fedora 11

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 9-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu May  1 2008 Richard Fearn <richardfearn@gmail.com> - 9-3
- update EWF patch so that it doesn't modify CFLAGS/LDFLAGS
- mention EWF in man page

* Fri Apr 25 2008 Richard Fearn <richardfearn@gmail.com> - 9-2
- build using $(RPM_OPT_FLAGS)
- install man page with -p to preserve timestamp
- add patch to support EWF images

* Sat Mar  8 2008 Richard Fearn <richardfearn@gmail.com> - 9-1
- initial package for Fedora

