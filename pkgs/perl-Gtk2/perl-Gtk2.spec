%global         momorel 2

# Find a free display (resources generation requires X) and sets XDISPLAY
%define init_xdisplay XDISPLAY=2; while [ $XDISPLAY -lt 13 ]; do if [ ! -f /tmp/.X$XDISPLAY-lock ]; then sleep 2s; ( /usr/bin/Xvfb -ac :$XDISPLAY >& /dev/null & ); sleep 15s; if [ -f /tmp/.X$XDISPLAY-lock ]; then export DISPLAY=:$XDISPLAY; break ; fi; fi; XDISPLAY=$(($XDISPLAY+1)); done; if [ $XDISPLAY -ge 13 ]; then echo No free display found; exit 1; fi
# The virtual X server PID
%define kill_xdisplay kill $(cat /tmp/.X$XDISPLAY-lock)

Name:           perl-Gtk2
Version:        1.2491
Release:        %{momorel}m%{?dist}
Summary:        Perl interface to the 2.x series of the Gimp Toolkit library
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Gtk2/
Source0:        http://www.cpan.org/authors/id/X/XA/XAOC/Gtk2-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Cairo >= 1.104
BuildRequires:  perl-ExtUtils-Depends >= 0.306
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-ExtUtils-PkgConfig >= 1.15
BuildRequires:  perl-Glib >= 1.302
BuildRequires:  perl-Pango >= 1.224
Requires:       perl-Cairo >= 1.104
Requires:       perl-ExtUtils-Depends >= 0.306
Requires:       perl-ExtUtils-PkgConfig >= 1.15
Requires:       perl-Glib >= 1.302
Requires:       perl-Pango >= 1.224
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Gtk2 module allows a Perl developer to use the Gtk+ graphical user
interface library. Find out more about Gtk+ at http://www.gtk.org.

%prep
%setup -q -n Gtk2-%{version}

# Provides: exclude perl(main)
cat <<__EOF__ > %{name}-perl.prov
#!/bin/sh
/usr/lib/rpm/perl.prov \$* | grep -v '^perl(main)$'
__EOF__
%define __perl_provides %{_builddir}/Gtk2-%{version}/%{name}-perl.prov
chmod +x %{__perl_provides}

# Requires: exclude unversioned perl(Glib)
cat <<__EOF__ > %{name}-perl.req
#!/bin/sh
/usr/lib/rpm/perl.req \$* | grep -v '^perl(Glib)$'
__EOF__
%define __perl_requires %{_builddir}/Gtk2-%{version}/%{name}-perl.req
chmod +x %{__perl_requires}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

# Mo files fix
mkdir -p %{buildroot}%{perl_vendorlib}/Gtk2/
mkdir -p %{buildroot}%{perl_vendorlib}/Gtk2/Ex/

%check
%if %{do_test}
%{init_xdisplay}
export DISPLAY=":$XDISPLAY"
make test ||:
%{kill_xdisplay}
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog.pre-git constants-2.0 doctypes gdk.typemap Gtk2.exports gtk.typemap 
%doc LICENSE 
%doc maps-2.0 maps-2.10 maps-2.12 maps-2.14 maps-2.16 maps-2.2 maps-2.4 maps-2.6 maps-2.8 
%doc maps_pango-1.0 maps_pango-1.10 maps_pango-1.16 maps_pango-1.18 maps_pango-1.4 maps_pango-1.6 maps_pango-1.8 
%doc NEWS README TODO 
%doc xs_files-2.0 xs_files-2.10 xs_files-2.12 xs_files-2.14 xs_files-2.16 xs_files-2.2 xs_files-2.4 xs_files-2.6 xs_files-2.8
%dir %{perl_vendorarch}/auto/Gtk2/
%{perl_vendorarch}/auto/Gtk2/*
%dir %{perl_vendorarch}/Gtk2/
%{perl_vendorarch}/Gtk2.pm
%{perl_vendorarch}/Gtk2/*
%dir %{perl_vendorlib}/Gtk2/
%dir %{perl_vendorlib}/Gtk2/Ex/
%{_mandir}/man3/*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2491-2m)
- rebuild against perl-5.20.0

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2491-1m)
- update to 1.2491

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.249-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.249-1m)
- update to 1.249

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.248-1m)
- update to 1.248

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.247-5m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.247-4m)
- enable tests again

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.247-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.247-2m)
- rebuild against perl-5.16.3

* Tue Feb  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.247-1m)
- update to 1.247

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.246-1m)
- update to 1.246

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.245-4m)
- rebuild against perl-5.16.2

* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.245-3m)
- disable tests for a while...

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.245-2m)
- rebuild against perl-5.16.1

* Tue Aug  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.245-1m)
- update to 1.245

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.244-1m)
- update to 1.244
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.242-1m)
- update to 1.242

* Sun Nov 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.241-1m)
- update to 1.241
- some of tests fail, but ignore now...

* Fri Oct 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.240-1m)
- update to 1.240

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-1m)
- update to 1.224

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.223-2m)
- rebuild for new GCC 4.6

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-1m)
- update to  1.223

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.222-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.222-2m)
- full rebuild for mo7 release

* Mon May 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-1m)
- update to 1.222

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.221-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-1m)
- update to 1.221

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.220-1m)
- update to 1.220

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.203-1m)
- update to 1.203

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.202-2m)
- rebuild against rpm-4.6

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.202-1m)
- update to 1.202
- fix typo...

* Mon Oct 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.201-1m)
- update to 1.201

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.200-1m)
- update to 1.200

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.183-1m)
- update to 1.183

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.182-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.182-1m)
- update to 1.182

* Sat Mar 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.181-1m)
- update to 1.181

* Wed Mar 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.180-1m)
- update to 1.180

* Thu Jan 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.164-1m)
- update to 1.164

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.163-1m)
- update to 1.163

* Sun Nov 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.162-1m)
- update to 1.162

* Mon Oct 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.161-1m)
- update to 1.161

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.160-1m)
- update to 1.160
- do not use %%NoSource macro

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.146-1m)
- update to 1.146

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.145-1m)
- update to 1.145

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.144-1m)
- update to 1.144

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.143-4m)
- add dirs for avoid dup dirs

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.143-3m)
- use vendor

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.143-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.143-1m)
- import to Momonga from Fedora Development


* Mon Feb 26 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.143-1
- Update to 1.143.

* Sun Jan 21 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.142-1
- Update to 1.142.

* Wed Nov 22 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.141-1
- Update to 1.141.

* Wed Sep  6 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.140-1
- Update to 1.140.

* Mon May 29 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.122-1
- Update to 1.122.

* Mon May  1 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.121-2
- Requires perl(Cairo)  (distro >= FC-5).

* Tue Apr 11 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.121-1
- Update to 1.121.

* Tue Mar 14 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.120-1
- Update to 1.120.

* Mon Feb 20 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.104-1
- Update to 1.104.
- Requires perl(Glib) >= 1.105 (1.104 had problems with perl 5.8.8).

* Thu Jan 19 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.103-1
- Update to 1.103.
- Converted the Gtk2::Helper man page to utf8 (#177802).
- Provides list: filtered out perl(main) (#177802).

* Wed Nov 30 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.102-1
- Update to 1.102.

* Thu Oct  6 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.101-1
- Update to 1.101.

* Thu Sep  8 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.100-1
- Update to 1.100.

* Fri Jul 29 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.083-1
- Update to 1.083.

* Mon Jun 27 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.082-1
- Update to 1.082.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Thu Mar 10 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.080-2
- Use perl-Glib for versioning control (patch by Ville Skytta).

* Tue Mar  8 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.080-1
- Update to 1.080.

* Tue Feb 15 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.062-1
- Update to 1.062.

* Mon Oct 25 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.061-0.fdr.2
- Removed irrelevant or duplicated documentation files.
- Description simplified (as suggested by Ville Skytta).

* Tue Oct  5 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.061-0.fdr.1
- Update to 1.061.

* Tue Oct  5 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.043-0.fdr.2
- make test commented: needs X.

* Sun Jul 18 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.043-0.fdr.1
- First build.
