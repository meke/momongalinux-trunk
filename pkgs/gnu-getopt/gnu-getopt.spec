%global momorel 4

# Copyright (c) 2000-2009, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define section free

Name:           gnu-getopt
Version:        1.0.13
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Java getopt implementation
License:        LGPLv2+
URL:            http://www.urbanophile.com/arenn/hacking/download.html
Source0:        http://www.urbanophile.com/arenn/hacking/getopt/java-getopt-%{version}.tar.gz
NoSource:       0
Source2:        gnu-getopt-%{version}.pom
Group:          Development/Libraries
Provides:       gnu.getopt = %{epoch}:%{version}-%{release}
Obsoletes:      gnu.getopt < %{epoch}:%{version}-%{release}
Requires(post): jpackage-utils
Requires(postun): jpackage-utils
BuildRequires:  ant
BuildRequires:  jpackage-utils
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root

%description
The GNU Java getopt classes support short and long argument parsing in
a manner 100% compatible with the version of GNU getopt in glibc 2.0.6
with a mostly compatible programmer's interface as well. Note that this
is a port, not a new implementation. I am currently unaware of any bugs
in this software, but there certainly could be some lying about. I would
appreciate bug reports as well as hearing about positive experiences.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Provides:       gnu.getopt-javadoc = %{epoch}:%{version}-%{release}
Obsoletes:      gnu.getopt-javadoc < %{epoch}:%{version}-%{release}

%description javadoc
%{summary}.

%prep
%setup -q -c
%{__mv} gnu/getopt/buildx.xml build.xml

%build
export CLASSPATH=
export OPT_JAR_LIST=:
%{ant} jar javadoc

%install
%{__rm} -rf %{buildroot}

# jars
%{__mkdir_p} %{buildroot}%{_javadir}
%{__cp} -p build/lib/gnu.getopt.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
%{__ln_s} %{name}-%{version}.jar %{buildroot}%{_javadir}/gnu.getopt-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}*; do %{__ln_s} ${jar} ${jar/-%{version}/}; done)

# javadoc
%{__mkdir_p} %{buildroot}%{_javadocdir}/%{name}-%{version}
%{__cp} -pr build/api/* %{buildroot}%{_javadocdir}/%{name}-%{version}
%{__ln_s} %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}
%{__ln_s} %{name}-%{version} %{buildroot}%{_javadocdir}/gnu.getopt-%{version}
%{__ln_s} gnu.getopt-%{version} %{buildroot}%{_javadocdir}/gnu.getopt

# poms
%add_to_maven_depmap gnu-getopt getopt %{version} JPP %{name}
%add_to_maven_depmap urbanophile java-getopt %{version} JPP %{name}
%{__install} -D -p -m 0644 %{SOURCE2} %{buildroot}%{_datadir}/maven2/poms/JPP-%{name}.pom

%clean
%{__rm} -rf %{buildroot}

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(0644,root,root,0755)
%doc gnu/getopt/COPYING.LIB gnu/getopt/README
%{_javadir}/%{name}-%{version}.jar
%{_javadir}/%{name}.jar
%{_javadir}/gnu.getopt-%{version}.jar
%{_javadir}/gnu.getopt.jar
%{_datadir}/maven2/poms/JPP-%{name}.pom
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}
%{_javadocdir}/gnu.getopt-%{version}
%{_javadocdir}/gnu.getopt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.13-2m)
- full rebuild for mo7 release

* Sat Mar 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.13-1m)
- sync with Fedora 13 (0:1.0.13-3.1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.12-4jpp.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.12-4jpp.3m)
- rebuild against rpm-4.6

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.12-4jpp.2m)
- modify Requires of javadoc

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.12-4jpp.1m)
- import from Fedora to Momonga

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0:1.0.12-4jpp.1
- Autorebuild for GCC 4.3

* Fri Aug 18 2006 Fernando Nasser <fnasser@redhat.com> 0:1.0.12-3jpp.1
- Merge with upstream

* Fri Aug 18 2006 Fernando Nasser <fnasser@redhat.com> 0:1.0.12-3jpp
- Add requires for post and postun javadoc

* Fri Jul 21 2006 Fernando Nasser <fnasser@redhat.com> 0:1.0.12-2jpp_1fc
- Merge with upstream

* Fri Jul 21 2006 Fernando Nasser <fnasser@redhat.com> 0:1.0.12-2jpp
- Add versions to backward compatibility Obsoletes/Provides
- Add AOT bits

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:1.0.9-4jpp_5fc
- rebuild

* Thu May 04 2006 Ralph Apel <r.apel at r-apel.de> 0:1.0.12-1jpp
- 1.0.12
- Change name to gnu-getopt, Provide/Obsolete gnu.getopt
- Still provide gnu.getopt.jar as symlink

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:1.0.9-4jpp_4fc
- stop scriptlet spew

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> 0:1.0.9-4jpp_3fc
- rebuilt again

* Tue Dec 13 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jun 21 2005 Gary Benson <gbenson@redhat.com> 0:1.0.9-4jpp_2fc
- Remove classes from the tarball.

* Fri Jan 21 2005 Gary Benson <gbenson@redhat.com> 0:1.0.9-4jpp_1fc
- Build into Fedora.

* Tue Dec 07 2004 David Walluck <david@jpackage.org> 0:1.0.10-1jpp
- 1.0.10

* Mon Aug 23 2004 Ralph Apel <r.apel at r-apel.de> 0:1.0.9-5jpp
- Build with ant-1.6.2

* Fri Mar  5 2004 Frank Ch. Eigler <fche@redhat.com> 0:1.0.9-4jpp_1rh
- RH vacuuming
- GetoptDemo package fix

* Fri May 09 2003 David Walluck <david@anti-microsoft.org> 0:1.0.9-4jpp
- fix groups

* Fri May 09 2003 David Walluck <david@anti-microsoft.org> 0:1.0.9-3jpp
- update for JPackage 1.5

* Wed Mar 26 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> 1.0.9-2jpp
- For jpackage-utils 1.5

* Sat Feb 16 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.9-1jpp 
- 1.0.9
- build script merged upstream

* Sat Jan 19 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.8-2jpp 
- versioned dir for javadoc
- no dependencies for javadoc package
- additional sources in individual archives
- section macro

* Sat Dec 8 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.8-1jpp
- first JPackage release
