%global momorel 8

Summary: benchmark software
Name: super_pi
Version: 2.0
Release: %{momorel}m%{?dist}
License: "unknown"
Group: Applications/System
Source0: ftp://pi.super-computing.org/Linux_jp/super_pi-jp.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-script.patch
URL: ftp://pi.super-computing.org/Linux_jp/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Super PI
ex: execxute %{_bindir}/super_pi_20

%prep
tar zxvf %{S:0}
%patch0 -p0
%{__mv} pi s_pi

%install
mkdir -p %{buildroot}%{_bindir}/
install -m 755 s_pi %{buildroot}%{_bindir}/
install -m 755 super_pi %{buildroot}%{_bindir}/
cat <<EOF >>%{buildroot}%{_bindir}/super_pi_20
%{_bindir}/s_pi 20
EOF
chmod 755 %{buildroot}%{_bindir}/super_pi_20

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root)
%doc Readme_jp.txt
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-4m)
- rename from pi to s_pi, binary name was conflict with cln

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-2m)
- rebuild against gcc43

* Fri Sep 01 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-1m)
- initial import to Momonga Linux
