%global momorel 70

%global emacsver %{_emacs_version}
%global apelver 10.8-6m
%global flimver 1.14.9-27m
%global e_sitedir %{_emacs_sitelispdir}
%global cvs_date 201006212322

Summary: Message Utilities for Emacs
Name: emacs-mu
Version: 8.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: ftp://ftp.jpl.org/pub/elisp/mu/snapshots/mu-cite-%{cvs_date}.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
BuildRequires: emacs-flim >= %{flimver}
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Requires: emacs-flim >= %{flimver}
Obsoletes: mu-emacs
Obsoletes: mu-xemacs

Obsoletes: elisp-mu
Provides: elisp-mu

%description
MU stands for "Message Utilities".  It consists of following
modules:

	mu-cite: a citation utility
	  mu-cite.el     --- main module of mu-cite
	  mu-bbdb.el     --- mu-cite submodule for BBDB
	  mu-register.el --- mu-cite submodule for registration

	latex-math-symbol.el --- translate mathematical symbols of
			     	 LaTeX into MULE characters

It requires two packages APEL 10.7 or later and FLIM 1.12.2 or later.
Please install them.

%prep
%setup -q -n mu-cite-%{cvs_date}

%build
make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir}

%install
rm -rf %{buildroot}

make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog NEWS README.en
%{e_sitedir}/mu

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1-70m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1-69m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.1-68m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1-67m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-66m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1-65m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.1-64m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-63m)
- update to cvs snapshot (2010-06-21)

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-62m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-61m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-60m)
- merge apel-emacs to elisp-apel
- kill apel-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-59m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-58m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-57m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-56m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-55m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-54m)
- update to cvs snapshot

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-53m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-52m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-51m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-50m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-49m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-48m)
- rebuild against apelver 10.7-11m
- rebuild against flimver 1.14.9-5m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-47m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-46m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-45m)
- rebuild against gcc43

* Wed Nov 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-44m)
- rebuild against flim-1.14.9

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-43m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-42m)
- update to cvs snapshot

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-41m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-40m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-39m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-38m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-37m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-36m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-35m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-34m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-33m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (8.1-32m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (8.1-31m)
- use %%{e_sitedir}, %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-30m)
- rebuild against emacs 22.0.50

* Tue Feb  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.1-29m)
- user flim insted of limit

* Thu Nov 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-28m)
- update apelver to 10.6-3m

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.1-27m)
- rebuild against emacs-21.3.50

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-26m)
- rebuild against emacs-21.3
- update version apel and limit
- use macro emacsver, xemacsver, apelver, limitver
- use %%{_prefix} macro

* Mon Jan 13 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (8.1-25m)
- Rebuild against elisp-apel-10.4-1m and elisp-limit-1.14.7-21m

* Mon Dec  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.1-24m)
- Requires: emacsen

* Thu Aug 15 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (8.1-23m)
- Rebuild against elisp-apel-10.3-21m and elisp-limit-1.14.7-19m
- sync with latest CVS (mu-20020225-cvs.patch)
- Remove Obsolete: tag.
- Requires: and BuildPreReq: emacs-21.1 -> 21.2

* Sat Dec 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (8.1-22k)
- sync with latest CVS (mu-20011221-cvs.patch)

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (8.1-20k)
- modify Requires tag

* Wed Oct 24 2001 Hidetomo Machi <mcHT@kondara.org>
- (8.1-18k)
- rebuild against emacs-21.1 and xemacs-21.4
- remove %emacsver

* Mon Oct 22 2001 Kazuhiko <kazuhiko@kondara.org>
- (8.1-16k)
- add Provides: tag

* Mon Oct 22 2001 Hidetomo Machi <machi@uranus.ailab.is.tsukuba.ac.jp>
- (eilsp-mu-8.1-12k)
- sync with latest CVS (mu-20010829-cvs-patch)
- change package's name

* Thu May 31 2001 Hidetomo Machi <mcHT@kondara.org>
- (8.1-10k)
- sync with latest CVS (mu-cite-20010523-cvs.patch)
- change Source0 URL

* Fri Nov 10 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- change BuildPreReq & Requires TAG

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (8.1-7k)
- modify specfile (License)
- dir problem

* Mon Feb 28 2000 SAKA Toshihide <saka@yugen.org>
- Released for GNU Emacs 20.6.

* Sun Jan 7 2000 SAKA Toshihide <saka@yugen.org>
- First release.
