%global momorel 1

Summary: GPLed free VHDL
Name: freehdl
Version: 0.0.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Engineering
Source0: http://dl.sourceforge.net/project/qucs/qucs/0.0.16/freehdl-%{version}.tar.gz
NoSource: 0
URL: http://www.freehdl.seul.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info

%description
A project to develop a free, open source, GPL'ed VHDL simulator for Linux!

%prep
%setup -q

# %patch1 -p1 -b .gcc43~

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

# clean up for x86_64
rm -f %{buildroot}%{_infodir}/dir

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/fire.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/fire.info %{_infodir}/dir || :
fi

%postun
if [ "$1" = 0 ]; then
    /sbin/ldconfig
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README*
%{_infodir}/fire*
%{_bindir}/*
%{_datadir}/freehdl
%{_libdir}/freehdl
%{_libdir}/lib*
%{_libdir}/pkgconfig/freehdl.pc
%{_includedir}/freehdl
%{_mandir}/man1/*
%{_mandir}/man5/*

%changelog
* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.8-1m)
- update 0.0.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.7-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.7-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.7-1m)
- update 0.0.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-4m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-3m)
- run install-info

* Wed Apr 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.6-2m)
- fix build on x86_64

* Wed Apr 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.6-1m)
- update 0.0.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4-4m)
- rebuild against gcc43

* Mon Jan 21 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-3m)
- add patch for gcc43, generated by gen43patch(v1)

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-2m)
- revised spec for debuginfo

* Sun May 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4-2m)
- NoSource -> Source

* Tue May 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.4-1m)
- update 0.0.4

* Mon Apr  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.3-1m)
- update 0.0.3

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.1-4m)
- retrived libtool library

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.1-3m)
- delete libtool library

* Mon Aug  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.1-2m)
- use %%makeinstall transform='s,x,x,'

* Tue Mar 14 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.0.1-1m)
- update to 0.0.1
- enable x86_64.

* Tue Nov  1 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (20040113-3m)
- fixed build error gcc3.4, gcc4.
- gcc3.4-patch import from gentoo
- Patch0:freehdl-20040113-gcc3.4.patch
- Patch1:freehdl-20040113-gcc4.patch

* Tue Jan 18 2005 Toru Hoshina <t@momonga-linux.org>
- (20040113-2m)
- use gcc_3_2 instead of gcc_2_95_3, so that gcc_2_95_3 will be retired soon...

* Wed Sep  8 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (20040113-1m)
- update to 20040113
- use g++_2_95_3 because it cannot be built with gcc-3.4

* Thu Aug 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (20030731-3m)
- cancel NoSource

* Thu Aug 28 2003 Toru Hoshina <t@momonga-linux.org>
- (20030731-2m)
- build it as it is.

* Wed Aug 27 2003 Kikutani Makoto <poo@momonga-linux.org>
- (20030731-1m)
- new upstream version

* Tue Nov  5 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (20020129-3m)
- use gcc_2_96

* Sun Aug 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (20020129-2m)
- %clean
- cleanup spec file

* Tue Aug 6 2002 Kikutani Makoto <poo@momonga-linux.org>
- (20020129-1m)
- 1st Momonga version
