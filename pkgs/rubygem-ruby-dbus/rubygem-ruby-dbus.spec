# Generated from ruby-dbus-0.7.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname ruby-dbus

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Ruby module for interaction with D-Bus
Name: rubygem-%{gemname}
Version: 0.7.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: LGPLv2
URL: https://trac.luon.net/ruby-dbus
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby >= 1.8.7
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby >= 1.8.7
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description



%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/COPYING
%doc %{geminstdir}/README
%doc %{geminstdir}/NEWS
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- udpate 0.7.2

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-2m)
- rename to rubygem-ruby-dbus
- add Obso ruby-dbus rubygem-dbus
- add provide rubygem-dbus

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update 0.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-1m)
- update 0.3.1

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-1m)
- update 0.3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-1m)
- Initial commit Momonga Linux
