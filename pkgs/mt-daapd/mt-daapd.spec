%global momorel 10

Summary: A multi-threaded implementation of Apple's DAAP server
Name: mt-daapd
Version: 0.2.4.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://www.fireflymediaserver.org/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: mt-daapd.service
Source2: mt-daapd.conf
Source3: mt-daapd.playlist
Patch0: http://ouchi.nahi.to/~kaidempa/mt-daapd/mt-daapd-0.2.1.1-cp932-3.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gdbm
Requires: libid3tag
Requires(pre): shadow-utils
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
BuildRequires: systemd-units
BuildRequires: avahi-devel
BuildRequires: gdbm-devel
BuildRequires: libid3tag-devel
BuildRequires: libogg-devel
BuildRequires: zlib-devel

%description
A multi-threaded implementation of Apple's DAAP server, mt-daapd
allows a Linux machine to advertise MP3 files to to used by
Windows or Mac iTunes clients.  This version uses Apple's ASPL Rendezvous
daemon.

%prep
%setup -q
%patch0 -p1

%build
%configure --enable-avahi --enable-oggvorbis
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall

%{__mkdir_p} %{buildroot}%{_unitdir}
%{__mkdir_p} %{buildroot}%{_sysconfdir}
%{__mkdir_p} %{buildroot}/var/cache/mt-daapd

install -m 0644  %{SOURCE1} %{buildroot}%{_unitdir}/
install -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}

%pre
/usr/sbin/userdel %{name} > /dev/null 2>&1 || :
/usr/sbin/groupadd -r %{name} > /dev/null 2>&1 || :
/usr/sbin/useradd -M -g %{name} -d %{_datadir}/%{name} -r -s /bin/false %{name}

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable mt-daapd.service > /dev/null 2>&1 || :
    /bin/systemctl stop mt-daapd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart mt-daapd.service >/dev/null 2>&1 || :
fi

%triggerun -- mt-daapd < 0.2.4.2-10m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply mt-daapd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save mt-daapd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del mt-daapd >/dev/null 2>&1 || :
/bin/systemctl try-restart mt-daapd.service >/dev/null 2>&1 || :

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING CREDITS ChangeLog NEWS README TODO
%config(noreplace) %{_sysconfdir}/mt-daapd.conf
%config(noreplace) %{_sysconfdir}/mt-daapd.playlist
%{_unitdir}/mt-daapd.service
%{_sbindir}/mt-daapd
%{_datadir}/mt-daapd
%attr(0700,mt-daapd,root) %{_var}/cache/mt-daapd

%changelog
* Sun Mar 30 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4.2-10m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4.2-7m)
- full rebuild for mo7 release

* Mon Nov 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4.2-6m)
- enable avahi and oggvorbis
- revise init script
- run as proper user

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4.2-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4.2-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4.2-2m)
- change BR from libid3tag to libid3tag-devel
- own %%{_datadir}/mt-daapd

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.4.2-1m)
- [SECURITY] CVE-2007-5824 CVE-2007-5825 CVE-2008-1771
- update to 0.2.4.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4-2m)
- %%NoSource -> NoSource

* Tue Mar 14 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.2.4-1m)
- update to 0.2.4

* Sun Jan 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.3-1m)
- update to 0.2.3

* Tue Sep  8 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.2-1m)
- for iTunes 5.0

* Wed Aug 31 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.1.1-1m)
- first import

* Tue Jan 18 2005 ron <ron@pedde.com>
- Update to 0.2.1, add oggvorbis

* Tue Jun 01 2004 ron <ron@pedde.com>
- Update to 0.2.0

* Mon Apr 06 2004 ron <ron@pedde.com>
- Update to 0.2.0-pre1
- Add /var/cache/mt-daapd

* Thu Jan 29 2004 ron <ron@pedde.com>
- Update to 0.1.1

* Fri Nov 14 2003 root <root@hafnium.corbey.com>
- Initial build.


