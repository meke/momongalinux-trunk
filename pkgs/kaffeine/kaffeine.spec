%global momorel 2
%global kaffeine_mozver 0.2
%global moz_plugindir %{_libdir}/mozilla/plugins
%global xinever 1.2.4
%global xinerel 1m
%global gstreamerver 0.10.30
%global gstreamer_pluginsver 0.10.30
%global phononver 4.5.0
%global phononrel 1m
%global qtver 4.7.5
%global kdever 4.12.1
%global kdelibsrel 1m
%global kdebaseworkspacerel 1m

Summary: A xine-based Media Player for KDE
Name: kaffeine
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://kaffeine.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/current/%{name}-%{version}.tar.gz
NoSource: 0
Source1: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-mozilla-%{kaffeine_mozver}.tar.bz2
NoSource: 1
Patch0: %{name}-%{version}-set-initialpreference.patch
Patch1: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: kdebase-workspace >= %{kdever}-%{kdebaseworkspacerel}
Requires: gstreamer-plugins-base >= %{gstreamer_pluginsver}
Requires: oxygen-icon-theme
Requires: phonon >= %{phononver}-%{phononrel}
Requires: xine-lib >= %{xinever}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: gettext
BuildRequires: gstreamer-devel >= %{gstreamerver}
BuildRequires: gstreamer-plugins-base-devel >= %{gstreamer_pluginsver}
BuildRequires: lame-devel >= 3.97-2m
BuildRequires: libogg-devel >= 1.1.3-2m
BuildRequires: libxml2
BuildRequires: libvorbis-devel >= 1.1.2-2m
BuildRequires: libxcb-devel
BuildRequires: phonon-devel >= %{phononver}-%{phononrel}
BuildRequires: xine-lib >= %{xinever}-%{xinerel}
BuildRequires: zlib-devel

%description
Kaffeine is a xine-based Media Player for KDE.

%package plugin
Summary: Browsers plugin using kaffeine for audio/video playback.
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}
Provides: %{name}-firefox-plugin
Provides: %{name}-mozilla-plugin
Obsoletes: %{name}-firefox-plugin
Obsoletes: %{name}-mozilla-plugin

%description plugin
kaffeine-plugin provides multimedia capabilities to Mozilla
or compatible browsers using kaffeine.

%prep
%setup -q -a 1

%patch0 -p1 -b .initialpreference
%patch1 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

# build plugin
pushd %{name}-mozilla-%{kaffeine_mozver}
CFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix}

make %{?_smp_mflags}
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install plugin
pushd %{name}-mozilla-%{kaffeine_mozver}
make install DESTDIR=%{buildroot}
popd

mkdir -p %{buildroot}%{_libdir}/%{name}plugin
mv %{buildroot}%{_prefix}/plugins/* %{buildroot}%{_libdir}/%{name}plugin/
rm -rf %{buildroot}%{_prefix}/plugins

# link plugin
mkdir -p %{buildroot}%{moz_plugindir}
ln -sf ../../%{name}plugin/%{name}plugin.so %{buildroot}%{moz_plugindir}/%{name}plugin.so

# link icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/oxygen/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
    update-desktop-database %{_datadir}/applications &> /dev/null
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
    gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
fi

%posttrans
update-desktop-database %{_datadir}/applications &> /dev/null
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
   
%files
%defattr(-,root,root)
%doc COPYING README
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/%{name}-xbu
%{_libdir}/%{name}plugin
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/profiles/%{name}.profile.xml
%{_kde4_appsdir}/solid/actions/%{name}_play_*.desktop
%{_kde4_iconsdir}/hicolor/16x16/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/22x22/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/32x32/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/48x48/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/64x64/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/128x128/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/scalable/apps/%{name}.svgz
%{_kde4_iconsdir}/oxygen/*/actions/*-encrypted.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%files plugin
%defattr(-,root,root)
%doc %{name}-mozilla-%{kaffeine_mozver}/AUTHORS
%doc %{name}-mozilla-%{kaffeine_mozver}/COPYING
%doc %{name}-mozilla-%{kaffeine_mozver}/ChangeLog
%doc %{name}-mozilla-%{kaffeine_mozver}/README
%{moz_plugindir}/%{name}plugin.so

%changelog
* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-2m)
- rebuild against xine-lib-1.2.4

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- version 1.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-3m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-2m)
- add %%post and %%postun

* Wed Sep 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- version 1.1
- update set-initialpreference.patch

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-5m)
- rebuild against qt-4.7.0-0.2.1m

* Sat Sep  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4m)
- [BUG FIX] install lost icons

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- rebuild against qt-4.6.3-1m

* Sun Jun 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0
- add desktop.patch
- remove merged gcc4.patch
- remove a switch usesvn
- add %%doc to package plugin
- modify spec file for smart build

* Wed May 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20100512.1m)
- update to 20100512 svn snapshot (revision 1125836)
- add a switch usesvn

* Fri Mar 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.3.1m)
- update to 1.0-pre3
- update set-initialpreference.patch
- remove fix-desktop.patch
- add gcc4.patch by pulsar

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.2.2m)
- change BR from xine-lib to xine-lib-devel (sync with trunk)

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-3m)
- change BR from xine-lib to xine-lib-devel

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.2.1m)
- update to 1.0-pre2
- update set-initialpreference.patch

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-1m)
- version 0.8.8, hello again KDE3

* Mon Apr 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.1m)
- update to version 1.0-pre1
- merge T4R/KDE4/kaffeine4
- update set-initialpreference.patch
- update fix-desktop.patch
- remove kernel2629.patch
- good-bye KDE3

* Thu Mar 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-6m)
- %%global with_dvb 1

* Wed Mar 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-5m)
- build without dvb for the moment
  can not build dvb support with kernel-2.6.29

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-3m)
- update Patch1 for fuzz=0
- License: GPLv2+

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.1.1m)
- initial build for Momong Linux 5
- KDE4 porting kaffeine media player

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-2m)
- change Requires: gst-plugins-base to gstreamer-plugins-base
- change BuildRequires: gst-plugins-base to gstreamer-plugins-base

* Mon Jul  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-1m)
- version 0.8.7

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-5m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-4m)
- move headers to %%{_includedir}/kde

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-1m)
- version 0.8.6
- update fix-desktop.patch
- License: GPLv2

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.5-2m)
- rebuild against libvorbis-1.2.0-1m

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- version 0.8.5
- fix kaffeine.desktop
- remove merged url.patch

* Fri Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-4m)
- update kaffeine.desktop (set InitialPreference=8) 

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-3m)
- import kaffeine-0.8.4-url.patch
- http://qa.mandriva.com/show_bug.cgi?id=30469

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-2m)
- enable xcb support with xine-lib-1.1.6

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- version 0.8.4
- remove merged gstreamer-0-10.patch

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-7m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-6m)
- clean up spec file

* Tue Feb 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-5m)
- replace gstreamer-0-10.patch (use opensuse's original patch)
- use "PACKAGE=%%{name}" to revise name of *.mo
- remove %%{_datadir}/locale/xx

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-4m)
- rebuild against kdelibs etc.

* Thu Feb  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-3m)
- import and modify kaffeine-gstreamer-0-10.patch from opensuse
 +* Mon Apr 10 2006 - jpr@suse.de
 +- Improve gstreamer 0.10 by getting visualization to work
 +* Sun Apr 09 2006 - jpr@suse.de
 +- Move gstreamer part to 0.10
- BuildPreReq: gstreamer-devel -> gstreamer010-devel
- BuildPreReq: gst-plugins-devel -> gst-plugins-base-devel
- Requires: gst-plugins-additional -> gst-plugins-base

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-2m)
- merge kaffeine-firefox-plugin and kaffeine-mozilla-plugin to kaffeine-plugin
- kaffeine-plugin Provides: kaffeine-firefox-plugin and kaffeine-mozilla-plugin
- kaffeine-plugin Obsoletes: kaffeine-firefox-plugin and kaffeine-mozilla-plugin

* Tue Nov 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-1m)
- version 0.8.3

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-2m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m

* Sat Sep  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-1m)
- version 0.8.2
- remove merged 64bit-fix.patch

* Thu Aug  3 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-0.1.4m)
- add BuildPreReq: lame-devel >= 3.96.1-4m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-0.1.3m)
- remove x-mplayer2.desktop for KDE 3.5.4

* Thu May 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.2-0.1.2m)
- add patch0: kaffeine-0.8.2-beta1-64bit-fix.patch

* Sun Apr 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-0.1.1m)
- update to version 0.8.2-beta1

* Mon Apr  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- version 0.8.1

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- version 0.8

* Tue Feb 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-5m)
- add a package kaffeine-firefox-plugin

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-4m)
- modify %%prep (clean up kaffeine-mozilla-plugin)
- add --enable-new-ldflags to configure

* Mon Nov 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-3m)
- rebuild against KDE 3.5 RC1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-2m)
- add --disable-rpath to configure

* Thu Sep  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1
- remove fix-copy-desktop-file.patch

* Mon Aug  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-1m)
- version 0.7

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-4m)
- add --with-qt-libraries=%%{qtdir}/lib to configure

* Fri Mar 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-3m)
- mozilla-plugin Requires: mozilla = %%{mozver} -> mozilla

* Thu Mar 24 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6-2m)
- rebuild against mozilla-1.7.6
- install to {_libdir}/mozilla/plugins.

* Mon Mar 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- version 0.6

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5-3m)
- enable x86_64.

* Sat Dec 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- rebuild against mozilla-1.7.5

* Fri Dec 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-1m)
- version 0.5
- remove kaffeine-0.5-first-run-wizard.patch
- add kaffeine-0.5-fix-copy-desktop-file.patch
- BuildPreReq: XFree86-devel -> xorg-x11-devel

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-0.2.1m)
- update to 0.5-rc2
- update first-run-wizard.patch

* Tue Oct  5 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-0.1.1m)
- update to version 0.5-rc1
- update first-run-wizard.patch
- remove kaffeine-0.4.3b-m3u.patch.bz2

* Fri Oct  1 2004 Hiroyuki koga <kuma@momonga-linux.org>
- (0.4.3b-5m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Oct  1 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-4m)
- remove createDesktopIcon from first-run wizard

* Mon Sep 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-3m)
- rebuild against KDE 3.3.0
- move desktop file from %%{_datadir}/applnk to %%{_datadir}/applications/kde
- BuildPreReq: desktop-file-utils
- add %%{_datadir}/pixmaps/kaffeine.png for GNOME

* Thu Sep 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-2m)
- rebuild against mozilla-1.7.3

* Sun Aug 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-1m)
- update to 0.4.3b
- import kaffeine-0.4.3b-m3u.patch.bz2 from cooker
- add man files
- add %%post and %%postun sections
- BuildPreReq: XFree86-devel, zlib-devel
- add a package kaffeine-mozilla-plugin
- change URL
- s/%%define/%%global/g

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.1-2m)
- revised spec for enabling rpm 4.2.

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
- rebuild against KDE 3.2.0

* Sat Jan 10 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.4.0-1m)
- ver up to 0.4.0

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.3.2-4m)
- rebuild against for qt-3.2.2

* Thu Sep 25 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-3m)
- clean up specfile
- use ./configure
- add --disable-debug flag

* Mon Sep  1 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-2m)
- revise %%files section

* Sun Aug 31 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- import from Suzuka

* Wed Aug 13 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- added ja.po.

* Tue Aug 12 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- First build.
