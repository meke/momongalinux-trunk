%global momorel 2

Summary: Eterm -- an Enlightened terminal emulator for the X Window System
Name: Eterm
Version: 0.9.6
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: User Interface/X
URL: http://www.eterm.org/
Source0: http://www.eterm.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: Eterm-0.9.6-momonga.patch
Patch1: Eterm-0.9.2-termname.patch
Patch2: Eterm-0.9.6-lib64.patch
Patch3: Eterm-0.9.6-nottf.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: imlib2-devel >= 1.2.0-1m
BuildRequires: libICE-devel, libSM-devel, libX11-devel
BuildRequires: libXres-devel, libXext-devel, libXmu-devel
BuildRequires: freetype-devel
BuildRequires: libtiff-devel
BuildRequires: libungif-devel
BuildRequires: libast-devel >= 0.7.1-0.6.20080502cvs.3m
BuildRequires: utempter
BuildRequires: pcre-devel >= 8.31
BuildRequires: libast
Obsoletes: Eterm-backgrounds
Provides: Eterm-backgrounds

%description
Eterm is a color vt102 terminal emulator with enhanced graphical
capabilities.  Eterm is intended to be a replacement for xterm for
Enlightenment window manager users, but it can also be used as a
replacement for xterm by users without Enlightenment.  Eterm supports
various themes and is very configurable, in keeping with the
philosophy of Enlightenment. If you install Eterm, you'll also need to
have the Imlib2 library installed.

%prep 
%setup -q
%patch0 -p1 -b .momonga
%patch1 -p1 -b .termname
%if %{_lib} == "lib64"
%patch2 -p1 -b .lib64~
%endif
%patch3 -p1 -b .nottf

%build
libtoolize -c -f
aclocal-1.9 -I .
autoconf
autoheader
automake-1.9 -ac
%configure --with-backspace=bs --with-delete=execute --enable-xim \
			--enable-multi-charset --enable-auto-encoding \
			--enable-escreen --disable-mmx
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall
for i in %{buildroot}%{_datadir}/Eterm/themes/*/theme.cfg; do
sed s/login_shell\ true/login_shell\ false/ $i > tmp
%__mv tmp $i
done

make -C bg all
make -C bg datadir=%{buildroot}%{_datadir} install

pushd %{buildroot}%{_bindir}
%__ln_s Eterm Escreen
popd

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc doc/Eterm_reference.html doc/Eterm.1.html
%doc README ChangeLog
%{_bindir}/*
%{_libdir}/lib*
%{_mandir}/man1/*
%{_datadir}/Eterm

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-2m)
- rebuild against pcre-8.31

* Mon Aug 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-3m)
- rebuild against rpm-4.6

* Thu Jan  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-2m)
- update Patch0 for fuzz=0
- drop Patch10, not needed

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1m)
- [SECURITY] CVE-2008-1692
- update to 0.9.5

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-3m)
- [BUILD FIX] BR: libast-devel >= 0.7.1-0.6.20080502cvs.3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-2m)
- rebuild against gcc43

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-0.20060130.6m)
- revised spec for debuginfo

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-20060130-5m)
- BuildRequires: freetype2-devel -> freetype-devel

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-20060130-4m)
- delete libtool library

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-20060130-3m)
- specify automake version 1.9

* Tue Apr 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-20060130-2m)
- add BuildRequires: libast for link libast

* Tue Jan 31 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.4-20060130-1m)
- update to 0.9.4 (cvs version 20060130)

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-7m)
- add gcc4 patch.
- Patch100: Eterm-0.9.2-gcc4.patch

* Fri Feb 25 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.3-6m)
- Patch10: Eterm-libast-exit.patch

* Thu Feb 24 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.2-5m)
- role-back to 0.9.2 (Japanese font problem)

* Mon Feb 14 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.3-4m)
- disable ttf

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.3-3m)
- enable x86_64.

* Wed Jan 12 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.3-2m)
- rebuid against imlib2-1.2.0
- BuildRequires: pcre-devel

* Wed Dec 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.3-1m)
  update to 0.9.3

* Wed May  5 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.2-4m)
- rebuild against imlib2-1.1.0
- disable-mmx
- revise specfile

* Tue Nov 19 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.2-3m)
- modify spec file

* Tue Nov 12 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.9.2-2m)
- apply termname patch
- add Escreen

* Tue Nov 12 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (0.9.2-1m)
- up to 0.9.2
- re-create patch (Eterm-0.9.2-momonga.patch)

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.9.1-6k)
- remove unnecessary requirements

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-4k)
- add termname patch

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-2k)
- version 0.9.1

* Mon Nov 05 2001 Motonobu Ichimura <famao@kondara.org>
- (0.9-14k)
- add patch for XIM
- change default themes to work various XIM
- modify BuildRequires,Requires Tag
- use gcc, not egcs

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (0.9-12k)
- rebuild against libpng 1.2.0.

* Sun Oct 14 2001 Masaru Sato <masachan@kondara.org>
- Modify BuildRoot
- Modify %ChangeLog to %changelog

* Wed Jan 31 2001 Shigeru Yamazaki <s@kondara.org>
- (0.9-11k)
- fixed backgrounds to show

* Fri Jan 19 2001 Toru Hoshina <toru@df-usa.com>
- add *.png again, sorry.

* Wed Oct 25 2000 Toru Hoshina <toru@df-usa.com>
- No more script error :-P

* Thu Oct  5 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix source URL

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7 

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sat Jun 24 2000 Toru Hoshina <t@kondara.org>
- version up.
- applied compount text patch from http://www.on.ics.keio.ac.jp/~yasu

* Mon Feb 28 2000 Tadashi Matsumoto <error@uranus.interq.or.jp>
- update Japanese patch
- fix BadMatch

* Wed Jan 19 2000 Shingo Akagaki <dora@kondara.org>
- default shell to be not "login shell" 

* Sat Dec 25 1999 Tadashi Matsumoto <error@uranus.interq.or.jp>
- fixed Japanese patch

