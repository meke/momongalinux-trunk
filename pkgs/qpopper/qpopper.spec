%global momorel 7

Summary: POP3 daemon from Qualcomm
Name: qpopper
Version: 4.0.19
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Daemons
URL: http://www.eudora.com/products/unsupported/qpopper/
Source0: ftp://ftp.qualcomm.com/eudora/servers/unix/popper/%{name}%{version}.tar.gz
NoSource: 0
Source1: qpopper.pamd
Patch1:  qpopper-4.0.19-syserr.patch
Requires: pam >= 0.59
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: postfix

%description
POP3 server from QUALCOMM, with the following features: lower memory
requirements, thus faster UIDL assists POP clients which "leave mail
on server" in determining which messages are new. Implements some
other extended POP3 commands. 

%prep
%setup -q -n %{name}%{version}
%patch1 -p1 -b .syserr~

%build
CFLAGS="%{optflags}" ./configure \
    --prefix=/usr \
    --enable-bulletins=/var/spool/mail/bulletins \
    --enable-specialauth \
    --with-pam=qpopper \
    --with-popuid=pop \
    --enable-apop=/etc/pop.auth
make

%clean
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/sbin
mkdir -p %{buildroot}%{_mandir}/man8
install -d -m 755 %{buildroot}/etc/pam.d/
install -m 755 -s popper/popper %{buildroot}/usr/sbin/in.qpopper
sed -e 's#/usr/local/lib/popper#/usr/sbin/in.qpopper#g' < man/popper.8 > %{buildroot}%{_mandir}/man8/in.qpopper.8
chmod 644 %{buildroot}%{_mandir}/man8/in.qpopper.8
install -m 755 -s popper/popauth %{buildroot}/usr/sbin/popauth
install -m 644 man/popauth.8 %{buildroot}%{_mandir}/man8/popauth.8
install -m 755 -d %{buildroot}/var/spool/mail/bulletins
install -m 644 %{SOURCE1} %{buildroot}/etc/pam.d/qpopper

%files
%defattr(-,root,root)
%doc License.txt README doc
%attr(-,root,mail) %dir /var/spool/mail/bulletins
/usr/sbin/in.qpopper
/usr/sbin/popauth
%{_mandir}/man8/in.qpopper.8*
%{_mandir}/man8/popauth.8*
%config /etc/pam.d/qpopper

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.19-7m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.19-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.19-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.19-4m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.19-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.19-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.19-1m)
- update to 4.0.19

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.18-2m)
- apply glibc210 patch

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.18-1m)
- update to 4.0.18
-- drop Patch1, merged upstream

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.16-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.16-1m)
- update to 4.0.16
- License: Modified BSD

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.15-1m)
- update to 4.0.15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.8-3m)
- rebuild against gcc43

* Mon Jun 19 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.8-2m)
- rebuilt against pam-0.99.7.1-1m
- revisez qpopper.pamd

* Mon Jul 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.0.8-1m)
- [SECURITY] CAN-2005-1151, CAN-2005-1152

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.5-2m)
- rebuild against gdbm

* Fri Mar 14 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.5-1m)
- upgrade to 4.0.5

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.0.4-7m)
- rebuild against for gdbm

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.4-6m)
- change BuildPreReq to postfix(default MTA) for error of OmoiKondara
- qpopper is checking sendmail when configure

* Fri Dec 27 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.4-5m)
- add BuildPreReq: smtpdaemon

* Tue May 14 2002 Toru Hoshina <t@kondara.org>
- (4.0.4-4k)
- security fix. See incoming/1052. Thanks to masaki-c@is.aist-nara.ac.jp.

* Wed Apr 17 2002 TABUCHI Takaaki <tab@kondara.org>
- (4.0.4-2k)
- update to 4.0.4

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.0.3-2k)
- update to 4.0.3

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Mon Aug  3 2001 Toru Hoshina <toru@df-usa.com>
- (3.1.2-4k)
- fixed system-auth issue.

* Wed Nov 22 2000 Kencichi Matsubara <m@kondara.org>
- update to 3.1.2.

* Thu Nov  2 2000 Toru Hoshina <toru@df-usa.com>
- back to 3.1, because qpopper3.1b6.tar.gz is no longer downloadable
  from primary site.

* Thu Jul 27 2000 AYUHANA Tomonori <l@kondara.org>
- (3.1b6-1k)
- update to 3.1b6
- remove %patch0 for BOF (fixed)

* Fri Jul 07 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu May 25 2000 AYUHANA Tomonori <l@kondara.org>
- Security Patch by Kazutoshi Morioka <aab36830@pop07.odn.ne.jp> jitter#553
- SPEC fixed ( Source, BuildRoot, %defattr )
- add -q at %setup

* Sat May 18 2000 Shin Fukui <shinta@april.co.jp>
- Upgrade to qpopper-3.0.2

* Sat May 6 2000 Kazutoshi Morioka <aab36830@pop07.odn.ne.jp>
- Upgrade to qpopper-3.0.1

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Aug 1 1999 Norihito Ohmori <ohmori@flatout.org>
- rebuild for glibc-2.1.x

* Fri Jul 9 1999 Norihito Ohmori <ohmori@flatout.org>
- fix APOP bug
