%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.9.90
%global         kdelibsrel 1m
%global         qtver 4.8.4
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 3.0
%global         sourcedir %{release_dir}/active/%{ftpdirver}/src

Name:           startactive
Summary:        An alternative Plasma session start script
Version:        0.3
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          User Interface/Desktops
URL:            http://community.kde.org/Plasma/Active/Contour
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
Source1:        plasma-active.desktop
# Fix path to libexec binaries
Patch1: startactive-0.3-libexec_path.patch 

BuildRequires:  kdelibs-devel >= %{kdever}

# pretty sure this is a minimal requirement, probably more -- rex
Requires:       plasma-mobile

%description
An alternative Plasma session start script, with 
limited scope to fit Plasma Active session needs.

Startactive is part of Plasma Active project.

%prep
%setup -q
%patch1 -p1 -b .libexec_path

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

install -p -m644 -D %{SOURCE1} \
  %{buildroot}%{_kde4_datadir}/xsessions/plasma-active.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%doc readme.txt
%{_kde4_bindir}/setup-kde-skel
%{_kde4_bindir}/startactive
%{_kde4_bindir}/startactive.bin
%{_kde4_appsdir}/ksplash/Themes/qmldefault/*
%{_kde4_appsdir}/startactive
%{_kde4_datadir}/xsessions/plasma-active.desktop

%changelog
* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Sat Oct 27 2012 Rex Dieter <rdieter@fedoraproject.org> 0.3-1
- 0.3

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Feb 27 2012 Radek Novacek <rnovacek@redhat.com> 0.2-3
- Create xsession desktop file

* Mon Feb 27 2012 Radek Novacek <rnovacek@redhat.com> 0.2-2
- Fix path to libexec binaries
- Disable setup-contour-intro module

* Sun Feb 19 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.2-1
- initial try

