%global momorel 4
%global srcname silc-toolkit

Summary: SILC Client Library
Name: libsilc
Version: 1.1.10
Release: %{momorel}m%{?dist}
License: GPLv2 or BSD
Group: System Environment/Libraries
URL: http://www.silcnet.org/
Source0: http://www.silcnet.org/download/toolkit/sources/%{srcname}-%{version}.tar.bz2
NoSource: 0
Patch0: %{srcname}-1.1-wordsize.patch
Patch1: %{srcname}-1.1.5-docinst.patch
Patch2: %{srcname}-%{version}-libs.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libidn-devel
BuildRequires: libtool
Epoch: 0
# doc subpackage was removed because they are too big and not useful
Obsoletes: libsilc-doc <= 0.9.12

%description
SILC Client Library libraries for SILC clients.

%package devel
Summary: Headers and shared libraries for %{name}
Group: Development/Libraries
Requires: libsilc = %{epoch}:%{version}-%{release}
Requires: pkgconfig
%description devel
The SILC Toolkit development libraries and headers. Required for building SILC clients.

%package doc
Summary: Development documentation for %{name}
Group:   Documentation

%description doc
The SILC Toolkit documentation in HTML format. Useful for writing new SILC
applications.

%prep
%setup -q -n silc-toolkit-%{version}
%patch0 -p1 -b .wordsize
%patch1 -p1 -b .docinst
%patch2 -p1 -b .libs

# filter out libsilc module SONAME Provides (#245323)
cat << \EOF > %{name}-prov
#!/bin/sh
sed -e '\,/silc/modules/,d' |\
%{__find_provides} $*
EOF

%define _use_internal_dependency_generator 0
%define __find_provides %{_builddir}/silc-toolkit-%{version}/%{name}-prov
chmod +x %{__find_provides}

%build
%configure --libdir=%{_libdir} --enable-shared --without-libtoolfix \
           --includedir=%{_includedir}/silc --with-simdir=%{_libdir}/silc/modules \
           --docdir=%{_docdir}/%{name}-%{version} CFLAGS="%{optflags}" \
           --without-libtoolfix

# WARNING! smp flags cause bad binaries!
make

%install
# clear the buildroot
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# make install
make DESTDIR=%{buildroot} install
chmod 0755 %{buildroot}%{_libdir}/lib* %{buildroot}%{_libdir}/silc/modules/*.so

# move doc files that would be deleted by rpm
mkdir docinst
mv %{buildroot}%{_docdir}/%{name}-%{version}/{toolkit,tutorial} docinst/
# fix up permisiion for Patch3
chmod 644 docinst/toolkit/*
# fix encoding of zlib.html
mv docinst/toolkit/zlib.html docinst/toolkit/zlib.html.orig
iconv -f iso-8859-15 -t utf8 -o docinst/toolkit/zlib.html docinst/toolkit/zlib.html.orig
rm -f docinst/toolkit/zlib.html.orig

# remove files we don't want into the package, but are being installed to buildroot
rm -rf %{buildroot}%{_sysconfdir}/silcalgs.conf %{buildroot}%{_sysconfdir}/silcd.conf

# remove .a and .la
rm -f %{buildroot}%{_libdir}/libsilc.a
rm -f %{buildroot}%{_libdir}/libsilc.la
rm -f %{buildroot}%{_libdir}/libsilcclient.a
rm -f %{buildroot}%{_libdir}/libsilcclient.la

# Fix encoding of CREDITS
mv CREDITS CREDITS.orig
iconv -f iso-8859-15 -t utf8 -o CREDITS CREDITS.orig

%check
# If this fails, the filter-provides script needs an update.
[ -d %{buildroot}%{_libdir}/silc/modules ]

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# the main package libsilc
%files
%defattr(0755, root, root, 0755)
%{_libdir}/libsilc-1.1.so.*
%{_libdir}/libsilcclient-1.1.so.*
%dir %{_libdir}/silc
%dir %{_libdir}/silc/modules
%{_libdir}/silc/modules/*.so
%defattr(0644, root, root, 0755)
%doc COPYING doc/FAQ CREDITS

# sub-package libsilc-devel
%files devel
%defattr(0644, root, root, 0755)
%{_libdir}/libsilc.so
%{_libdir}/libsilcclient.so
%{_libdir}/pkgconfig/silc.pc
%{_libdir}/pkgconfig/silcclient.pc
%dir %{_includedir}/silc
%{_includedir}/silc/*.h

%files doc
%defattr(0644, root, root, 0755)
%doc docinst/toolkit
%doc docinst/tutorial

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.10-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-1m)
- update to 1.1.10
- sync with Fedora devel (1.1.10-4)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-2m)
- [SECURITY] CVE-2009-3051 CVE-2009-3163 CVE-2008-7160
- import security patches from Fedora 11
-- 
-- * Fri Sep 04 2009 Stu Tomlinson <stu@nosnilmot.com> 1.1.8-7
-- - Backport patch to fix stack corruption (CVE-2008-7160) (#521256)
-- 
-- * Fri Sep 04 2009 Stu Tomlinson <stu@nosnilmot.com> 1.1.8-6
-- - Backport patch to fix additional string format vulnerabilities (#515648)
-- 
-- * Wed Aug 05 2009 Stu Tomlinson <stu@nosnilmot.com> 1.1.8-5
-- - Backport patch to fix string format vulnerability (#515648)

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.8-1m)
- version 1.1.8
- add --without-libtoolfix to configure for the moment
- import libtool.patch from Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-3m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-2m)
- fix build on i686

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7, sync with Fedora
- separate doc sub package

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc43

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- import from fc to Momonga

* Wed Oct 04 2006 Warren Togami <wtogami@redhat.com> 1.0.2-2
- fix multilib file conflicts in -devel

* Wed Jun 28 2006 Warren Togami <wtogami@redhat.com> 1.0.2-1
- remove .a and .la files

* Wed Dec 21 2005 Stu Tomlinson <stu@nosnilmot.com> 1.0.2-0
- Update to 1.0.2

* Sat Apr 9  2005 Stu Tomlinson <stu@nosnilmot.com>  0.9.12-11
- use RPM_OPT_FLAGS (#153261)

* Fri Apr 1  2005 Warren Togami <wtogami@redhat.com> 0.9.12-10
- remove huge doc subpackage to save space, not useful

* Wed Mar 16 2005 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Feb 28 2005 Warren Togami <wtogami@redhat.com> 0.9.12-8
- gcc4 rebuild

* Wed Sep 1 2004 Warren Togami <wtogami@redhat.com> 0.9.12-7
- rawhide import
- minor spec changes

* Tue Sep 1 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.5 - Had to remove smp_mflags because build fails with them (Michael Schwendt)
* Tue Aug 31 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.5 - corrections to lib and include path (from Michael Schwendt)
* Tue Aug 31 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.4 - post/postun /sbin/ldconfig
  (Patch 823 from Stu Tomlinson)
* Tue Aug 31 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.3 - Move libs to %{_libdir} and add a silc.pc 
  (Patch 815 from Stu Tomlinson)
* Tue Aug 17 2004 Toni Willberg <toniw@iki.fi>
- fix so permissions and hardcoded paths (patch from Michael Schwendt)
* Mon Jul 5 2004 Toni Willberg <toniw@iki.fi>
- Fixed various errors
* Sun Jul 4 2004 Toni Willberg <toniw@iki.fi>
- Initial version for Fedora
