%global momorel 2

Summary: Open Phone Abstraction Library
Name: opal
Version: 3.10.10
Release: %{momorel}m%{?dist}
URL: http://www.opalvoip.org/wiki/
License: MPL
Source0: http://ftp.gnome.org/pub/gnome/sources/%{name}/3.10/%{name}-%{version}.tar.xz
NoSource: 0
# patch1 was imported from arch linux
Patch1: opal-3.10.10-ffmpeg.patch

Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ptlib >= 2.6.0
Requires: openldap
BuildRequires: openssl-devel >= 0.9.8b
BuildRequires: SDL-devel
BuildRequires: openldap >= 2.4.0
BuildRequires: ptlib-devel >= 2.10.10
BuildRequires: x264-devel >= 0.0.2377
BuildRequires: ffmpeg-devel >= 0.6.1-0.20110511
BuildRequires: unixODBC-devel >= 2.2.14
Obsoletes: openh323 openh323gk

%description
Open Phone Abstraction Library, implementation of the ITU H.323
teleconferencing protocol, and successor of the openh323 library.

%package devel
Summary: Development package for opal
Group: Development/Libraries
Requires: opal = %{version}
Requires: ptlib-devel
Obsoletes: openh323-devel

%description devel
Static libraries and header files for development with opal.

%prep
%setup -q
%patch1 -p1 -b .ffmpeg~

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_datadir}/opal/opal_inc.mak

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc  mpl-1.0.htm
%{_libdir}/*.so.*
%{_libdir}/%{name}-%{version}

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/libopal_s.a
%{_libdir}/pkgconfig/opal.pc

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.10-2m)
- import a patch to fix build failure with ffmpeg-2.2.1+

* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.10-1m)
- update to 3.10.10

* Wed Oct 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.7-1m)
- update to 3.10.7
- rebuild against ptlib-2.10.7

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.2-4m)
- rebuild against x264-2120-0.20120520

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.2-3m)
- rebuild against x264-2120-0.20111215

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.2-2m)
- rebuild against x264-2106-0.20111024

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10.2-1m)
- update to 3.10.2

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.3-4m)
- rebuild against x263 and ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.3-3m)
- rebuild for new GCC 4.6

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.3-2m)
- rebuild against x264-0.0.1913

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.3-1m)
- rebuild against x264-0.0.1867

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.2-5m)
- rebuild against x264-0.0.1820

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.2-4m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.2-3m)
- rebuild against x264-0.0.1745

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.2-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.2-1m)
- update 3.8.2

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.1-2m)
- rebuild against x264-1649-20100616

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.1-1m)
- update 3.8.1

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.7-1m)
- rebuild against x264-1523

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.7-1m)
- update 3.6.7

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.6-6m)
- rebuild against x264

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.6-5m)
- rebuild against x264

* Wed Nov 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.6-4m)
- rebuild against x264

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.6-2m)
- rebuild against x264

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.6-1m)
- update to 3.6.6

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.4-4m)
- rebuild against x264

* Mon Aug 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.4-3m)
- rebuild against x264

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.4-2m)
- rebuild against x264

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.4-1m)
- update to 3.6.4

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2
- comment out patch1 (for gcc44)

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.1-3m)
- rebuild against unixODBC-2.2.14

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.1-2m)
- rebuild against openssl-0.9.8k

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-3m)
- update x264

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.0-2m)
- fix BPR

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0
- delete patch0

* Wed Feb 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.5.2-3m)
- rebuild against x264

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.2-2m)
- apply gcc44 patch

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.2-1m)
- update to 3.2.5
- delete patch0 (for gcc44) merged
- add patch0 (for ffmpeg) build fix

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.4-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.4-3m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.4-2m)
- rebuild against ptlib-2.4.4

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.4.4-1m)
- update to 3.4.4

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- rebuild against x264-0.0.721-0.20081025.1m

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.4.2-1m)
- update to 3.4.2

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.11-7m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.11-6m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.11-5m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.11-4m)
- %%NoSource -> NoSource

* Wed Oct 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.11-3m)
- update to 2.2.11
- [SECURITY] CVE-2007-4924
- merge changelog only

* Fri Sep 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.11-2m)
- make devel package

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.8-1m)
- update to 2.2.8

* Mon Mar 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.6-1m)
- update to 2.2.6

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.5-1m)
- update to 2.2.5

* Mon Jan 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.4-1m)
- update to 2.2.4

* Fri Oct 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Fri Mar  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.3-1m)
- version up

* Wed Jan 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-1m)
- Sart
