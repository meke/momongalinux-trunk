%global momorel 1

# Spec file for rhdb-utils.
# Authors: Liam Stewart <liams@redhat.com>, Andrew Overholt
# <overholt@redhat.com>, Tom Lane <tgl@redhat.com>
# Copyright (C) 2002-2010 Red Hat, Inc.

Summary: Miscellaneous utilities for PostgreSQL - Red Hat Edition
Name: rhdb-utils
Version: 9.1.0
Release: %{momorel}m%{?dist}
URL: http://sources.redhat.com/rhdb/
# pg_crc.c is copied from postgresql, the rest is GPL
License: GPLv2+ and "PostgreSQL"
Group: Applications/Databases
BuildRequires: postgresql-devel

Source0: http://sources.redhat.com/rhdb/tools/pg_filedump-%{version}.tar.gz
# We keep a copy of pg_crc.c in this SRPM so we don't need to have the
# full PostgreSQL sources to build.  This should be refreshed periodically
# from the PostgreSQL sources.
Source1: pg_crc.c
Patch1: pg_filedump-make.patch

%description
This package contains miscellaneous, non-graphical tools developed for
PostgreSQL - Red Hat Edition.

%prep
%setup -q -n pg_filedump-%{version}

cp -p %{SOURCE1} pg_crc.c

%patch1 -p1

%build
#Following flags needed as per rh##596204 
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -fwrapv"
make %{?_smp_mflags} 

%install
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
install -p -m 755 pg_filedump ${RPM_BUILD_ROOT}%{_bindir}

%files
%defattr(-,root,root,-)
%{_bindir}/pg_filedump
%doc README.pg_filedump

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.0-1m)
- sync with Fedora (9.1.0-3)

* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (9.0.0-1m)
- sync Fedora 

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.4.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.4.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.4.0-2m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.4.0-1m)
- update to 8.4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (8.3.0-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 8.3.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb  8 2008 Tom Lane <tgl@redhat.com> 8.3.0-1
- Update pg_filedump to version 8.3.0, to support PostgreSQL 8.3.

* Thu Aug  2 2007 Tom Lane <tgl@redhat.com> 8.2.0-2
- Update License tag to match code.

* Wed Feb 14 2007 Tom Lane <tgl@redhat.com> 8.2.0-1
- Update pg_filedump to version 8.2.0, to support PostgreSQL 8.2.
Resolves: #224175

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 8.1.1-1.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 8.1.1-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 8.1.1-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 21 2005 Tom Lane <tgl@redhat.com> 8.1.1-1
- Update pg_filedump to version 8.1.1, to fix a couple of oversights.
- Simplify specfile a bit.

* Mon Nov 21 2005 Tom Lane <tgl@redhat.com> 8.1-1
- Update pg_filedump to version 8.1, to support PostgreSQL 8.1.
- Change version numbering so that major version matches corresponding
  PostgreSQL version.

* Wed Mar  2 2005 Tom Lane <tgl@redhat.com> 4.0-3
- Rebuild for gcc4 update.

* Fri Feb 11 2005 Tom Lane <tgl@redhat.com> 4.0-2
- Adjust build to honor $RPM_OPT_FLAGS.

* Thu Feb 10 2005 Tom Lane <tgl@redhat.com> 4.0-1
- Update pg_filedump to version 4.0, to support PostgreSQL 8.0.
- Keep pg_crc.c on hand as a plain source file instead of a patch.
  Easier to compare to main PG sources that way.

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Apr  5 2004 Tom Lane <tgl@redhat.com>
- Update outdated URL.

* Fri Feb 20 2004 Tom Lane <tgl@redhat.com>
- Remove beta label from pg_filedump.

* Fri Feb 20 2004 Tom Lane <tgl@redhat.com>
- Update to version 3.0.
- Increment Buildrequires to >= PostgreSQL 7.4
- Rebuild for Fedora Core 2.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Nov 21 2003 David Jee <djee@redhat.com> 2.0-2
- Remove Distribution tag.
- Rebuild for Fedora Core 1.

* Thu May 29 2003 Andrew Overholt <overholt@redhat.com>
- Bump to version 2.0.
- Modify distribution.
- Increment Buildrequires to >= PostgreSQL 7.3

* Wed Sep 11 2002 Andrew Overholt <overholt@redhat.com>
- Changed revision to 1.

* Fri Jul  8 2002 Liam Stewart <liams@redhat.com>
- Updated summary and description.

* Thu Jul  4 2002 Liam Stewart <liams@redhat.com>
- Updated Source0 entry.

* Wed Jun 26 2002 Liam Stewart <liams@redhat.com>
- Group fix.

* Mon Jun 24 2002 Liam Stewart <liams@redhat.com>
- Initial build.


