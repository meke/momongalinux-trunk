%global momorel 5

Name:           lxde-icon-theme
Version:        0.0.1
Release:        %{momorel}m%{?dist}
Summary:        Default icon theme for LXDE

Group:          User Interface/Desktops
License:        LGPLv3+
URL:            http://lxde.org/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/lxde-icon-theme-%{version}.tar.bz2
NoSource:       0
Source1:        shiretoko128x128.png
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ImageMagick
Requires:       filesystem
Requires(post): gtk2
Requires(postun): gtk2
BuildArch:      noarch

%description
nuoveXT2 is a very complete set of icons for several operating systems. It is 
also the default icon-theme of LXDE, the Lightweight X11 Desktop Environment.

%prep
%setup -q

%build
%configure
# replace firefox icons
for size in 16x16 22x22 24x24 32x32 36x36 48x48 64x64 72x72 96x96
do
    rm -f nuoveXT2/${size}/apps/firefox.png
    convert -resize ${size} %{SOURCE1} nuoveXT2/${size}/apps/firefox.png
done
rm -f nuoveXT2/128x128/apps/firefox.png
cp %{SOURCE1} nuoveXT2/128x128/apps/firefox.png

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

%clean
rm -rf %{buildroot}

%post
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache -q -f -t %{_datadir}/icons/nuoveXT2 || :
fi

%postun
if [ "$1" -eq "0" ]; then
    if [ -x %{_bindir}/gtk-update-icon-cache ]; then
        %{_bindir}/gtk-update-icon-cache -q -f -t %{_datadir}/icons/nuoveXT2 || :
    fi
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%dir %{_datadir}/icons/nuoveXT2/
%{_datadir}/icons/nuoveXT2/*/*
%{_datadir}/icons/nuoveXT2/index.theme

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-3m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-2m)
- Requires(post): gtk2
- Requires(postun): gtk2

* Thu Dec 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-1m)
- initial packaging
