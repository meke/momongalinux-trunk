%global momorel 1

Summary: A GNU utility for data encryption and digital signatures
Name: gnupg
Version: 1.4.16
Release: %{momorel}m%{?dist}
License: "GPLv3+ with exceptions"
Group: Applications/File
Source0: ftp://ftp.gnupg.org/gcrypt/gnupg/%{name}-%{version}.tar.bz2
NoSource: 0
Source3: gpg.conf
URL: http://www.gnupg.org/
Provides: gpg openpgp
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openldap-devel >= 2.3.19 , rpm-build >= 4.0.4-33m
BuildRequires: readline-devel >= 6.1
BuildRequires: curl-devel >= 7.16.0
Requires(post): info
Requires(preun): info

%{?include_specopt}
%{?!WITH_SELINUX: %global WITH_SELINUX 0}
%if %{WITH_SELINUX}
Requires: libselinux
BuildRequires: libselinux-devel
%endif

%description
The GNU Privacy Guard (GnuPG) is a GNU tool for secure communication
and data storage. It is a complete and free replacement for PGP and
can be used to encrypt data and to create digital signatures. It
includes an advanced key management facilitiy and is compliant with
the proposed OpenPGP Internet standard (and its optional features)
as described in RFC2440. There are a lot of useful extra features
like anonymous message recipients and integrated keyserver support.
The program does not use any patented algorithms, and can be used as a
filter program. Can handle all OpenPGP messages and messages generated
by PGP 5.0 and newer unless they use the IDEA algorithm.

%prep
%setup -q

%build
%configure CC=gcc --program-prefix="" \
	--with-static-rnd=auto \
	DOCBOOK_TO_MAN=%{_bindir}/docbook2man \
	DOCBOOK_TO_TEXI=%{_bindir}/docbook2texi \
	--enable-maintainer-mode \
%if %{WITH_SELINUX}
	--enable-selinux \
%endif
	--enable-mailto \
%ifarch ppc64
	--disable-asm \
%endif
	--with-mailprog=/usr/sbin/sendmail

%make
make check

%clean
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
install -m755 ./keyserver/gpgkeys_hkp %{buildroot}%{_libexecdir}/gnupg/gpgkeys_hkp

# lspgpot (script to extract the ownertrust values)
sed 's^\.\./g[0-9\.]*/^^g' tools/lspgpot > lspgpot
install -m755 lspgpot %{buildroot}%{_bindir}/lspgpot

# mail-signed-keys (to mail someone that his key has been signed)
install -m755 tools/mail-signed-keys %{buildroot}%{_bindir}/mail-signed-keys

# ring-a-party  (print a keyring suitable for a key signing party)
install -m755 tools/ring-a-party %{buildroot}%{_bindir}/ring-a-party

# config-sample
mkdir -p %{buildroot}%{_datadir}/config-sample/gnupg/
install -D -m644 %{SOURCE3} %{buildroot}%{_datadir}/config-sample/gnupg/gpg.conf

# remove
rm -f %{buildroot}%{_infodir}/dir

%post
/sbin/install-info %{_infodir}/gnupg1.info --entry="* gpg: (gnupg1).            OpenPGP encryption and signing tool (v1)." %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gnupg1.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING ChangeLog INSTALL NEWS
%doc PROJECTS README THANKS TODO VERSION
%doc doc/DETAILS doc/HACKING doc/OpenPGP
%doc doc/samplekeys.asc
%doc doc/highlights-1.4.txt

%attr(4755,root,root) %{_bindir}/gpg
%{_bindir}/gpgsplit
%{_bindir}/gpgv
%{_bindir}/lspgpot
%{_bindir}/mail-signed-keys
%{_bindir}/ring-a-party
%{_bindir}/gpg-zip
#
%dir %{_libexecdir}/gnupg
%{_libexecdir}/gnupg/gpgkeys_ldap
%{_libexecdir}/gnupg/gpgkeys_mailto
%{_libexecdir}/gnupg/gpgkeys_hkp
%{_libexecdir}/gnupg/gpgkeys_finger
#%%{_libexecdir}/gnupg/gpgkeys_http
%{_libexecdir}/gnupg/gpgkeys_curl
#
%dir %{_datadir}/gnupg
%{_datadir}/gnupg/options*
%doc %{_datadir}/gnupg/FAQ
%{_datadir}/locale/*/*/*
#
%{_datadir}/config-sample/gnupg
#
%{_mandir}/man1/gpg.*
%{_mandir}/man1/gpgv.*
%{_mandir}/man1/gpg-zip.*
%{_mandir}/man7/gnupg.*
#
%{_infodir}/gnupg1.info.*

%changelog
* Thu Dec 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.16-1m)
- [SECURITY] CVE-2013-4576
- update to 1.4.16

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.14-1m)
- [SECURITY] CVE-2013-4242
- update to 1.4.14

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-1m)
- update to 1.4.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.11-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.11-2m)
- rebuild for new GCC 4.5

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.11-1m)
- update to 1.4.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.10-5m)
- full rebuild for mo7 release

* Tue May  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.10-4m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.10-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.10-1m)
- update to 1.4.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-7m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-6m)
- drop Patch1 for fuzz=0, not needed
- License: "GPLv3+ with exceptions"

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-5m)
- run install-info

* Sun Jul 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.9-4m)
- add a patch to fix compilation error

* Sat Jun  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-3m)
- do not use force bz2 setting in info

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.9-2m)
- rebuild against openssl-0.9.8h-1m

* Fri Apr 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-1m)
- update to 1.4.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.8-4m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.8-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.8-2m)
- %%NoSource -> NoSource

* Fri Dec 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8
- License: GPLv3

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-1m)
- [SECURITY] CVE-2007-1263
- update to 1.4.7 (include security fixes)

* Wed Feb 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-3m)
- gnupg was no info...

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.6-2m)
- enable ppc64, sparc64

* Sun Dec 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6
- [SECURITY] CVE-2006-6235
- http://lists.gnupg.org/pipermail/gnupg-announce/2006q4/000246.html
- [SECURITY] CVE-2006-6169
- http://lists.gnupg.org/pipermail/gnupg-announce/2006q4/000241.html

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.5-2m)
- rebuild against curl-7.16.0

* Sun Aug  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.5-1m)
- [SECURITY] CVE-2006-3746
- http://lists.immunitysec.com/pipermail/dailydave/2006-July/003354.html

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.3-3m)
- rebuild against readline-5.0

* Sat Jun 24 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.3-2m)
- [SECURITY] CVE-2006-3082
- http://seclists.org/lists/fulldisclosure/2006/May/0774.html
- add Patch3: gnupg-1.4.3-CVE-2006-3082.patch

* Tue May 16 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.3-1m)
- update to 1.4.3
- import new patches from FC
- remove configure option --enable-m-guard
- do make check
- remove gpgkeys_http

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.2.2-2m)
- rebuild against openldap-2.3.19

* Fri Mar 10 2006 TAKAHASHI Tamotsu <tamo>
- (1.4.2.2-1m)
- [SECURITY] CVE-2006-0049 "GnuPG does not detect injection of unsigned data"
 (All versions of gnupg prior to 1.4.2.2 are affected.)
 http://lists.gnupg.org/pipermail/gnupg-announce/2006q1/000216.html
 http://it.slashdot.org/article.pl?sid=06/03/09/233227

* Fri Feb 17 2006 TAKAHASHI Tamotsu <tamo>
- (1.4.2.1-1m)
- [SECURITY] False positive signature verification in GnuPG
 http://marc.theaimsgroup.com/?l=gnupg-devel&m=113999098729114&w=2

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.2-3m)
- rebuild against openldap-2.3.11

* Sat Aug 13 2005 TAKAHASHI Tamotsu <tamo>
- (1.4.2-1m)

* Sat Mar 19 2005 TAKAHASHI Tamotsu <tamo>
- (1.4.1-1m)
- [SECURITY] From NEWS:
    Added countermeasures against the Mister/Zuccherato CFB attack
    <http://eprint.iacr.org/2005/033>.
  (But this should not affect most of Momonga users at all.)

* Sat Dec 18 2004 TAKAHASHI Tamotsu <tamo>
- (1.4.0-1m)
- update: see doc/highlights-1.4.txt
- disable selinux by default (I'm not sure this is stable enough)

* Mon Aug 30 2004 TAKAHASHI Tamotsu <tamo>
- (1.2.6-1m)
- From NEWS:
 * Fixed a race condition possibly leading to deleted keys.
 * Some performance improvements with large keyrings.  See the
   build time option --enable-key-cache=SIZE in the README file for
   details.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.4-2m)
- revised spec for enabling rpm 4.2.

* Tue Jan 20 2004 TAKAHASHI Tamotsu <tamo>
- (1.2.4-1m)
- global momorel
- correct install-info

* Sat Nov 29 2003 TAKAHASHI Tamotsu <tamo>
- (1.2.3-5m)
- ElGamel -> ElGamal (hazukashii...)

* Sat Nov 29 2003 TAKAHASHI Tamotsu <tamo>
- (1.2.3-4m)
- disable ElGamel sign+encrypt keys (patch0,1,2)
 patch0: disable the ability to create signatures using the
         ElGamal sign+encrypt (type 20) keys as well as to
	 remove the option to create such keys.
 patch1: entirely stifles the use of Elgamal for signing.
 patch2: if(sk->pubkey_algo==PUBKEY_ALGO_ELGAMAL)
         rc=G10ERR_UNU_SECKEY;

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-3m)
- rebuild against openldap

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-2m)
- kill %%define name

* Sun Aug 24 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.2.3-1m)
- version up
- use %%NoSource, %%momorel
- clean up

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.2-2m)
  rebuild against cyrus-sasl2

* Sun May  4 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.2.2-1m)
- version up
- change my mail address on the previous changelog entry,
  "h@h12o.org" to "h12o@h12o.org" :D

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.1-4m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-3m)
- rebuild against for gdbm

* Mon Nov 11 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.2.1-2m)
- make gpgkeys_hkp plugin, which is not built by default,
 but useful for other programs (e.g. gpa)

* Sat Oct 26 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.2.1-1m)
- bugfixes

* Mon Oct 21 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-6m)
- remove BuildPreReq ownself
- remove IDEA patch against PATENTED! license

* Sat Oct 05 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.2.0-5m)
- specopt priority order: fixed (ref. kernel.spec)
- you may not want to require gpg to build gpg itself.
  you may not want to use unverified sources.
  you can set your preferences in specopt file. (%skip_gpg)
  the default is to use gpg.
  NOTE: IF YOU SKIP VERIFYING, NLS-UPDATE IS ALSO SKIPPED

* Wed Sep 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-4m)
- add '--with-mailprog=/usr/sbin/sendmail'

* Tue Sep 24 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.2.0-3m)
- "%if" in description corrupts the format : default behavior has priority
  (if IDEA is enabled, a few null lines are appeared in description)
- "j" is to untar bz2 files (though unsure)

* Tue Sep 24 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.2.0-2m)
- add IDEA support (DON'T DISTRIBUTE IDEA IN JAPAN!)
  this will be enabled when defined in specopt
- update messages (Source4)
- BuildPreReq: gnupg

* Sun Sep 22 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.2.0-1m)
- use bz2 source instead of gz
- check sig if gpg is installed
- make DESTDIR=buildroot install
- description is modified to correct IDEA patent description
  (copied from gnupg.txt and the release-announce)
- configure --with-static-rnd=auto
- unlist _libdir because rnd's are static
- configure --enable-m-guard
- gpgkeys_* : %{_bindir} -> %{_libexecdir}/gnupg/ (nigiru)
- add mail-signed-keys and ring-a-party scripts
- remove FAQ and faq.html from doc (they are in datadir)
- add info's
- add gnupg.7 man
- add config-sample

* Fri Jun 21 2002 HOSONO Hidetomo <h12o@h12o.org>
- (1.0.7-8k)
- add "BuildPreReq: openldap-devel"

* Wed May 01 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.0.7-6k)
- add fixed ja.po from http://japo.sourceforge.jp/trans/ja/gnupg-20020419.ja.po

* Wed May 01 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.0.7-4k)
- added doc/samplekeys.asc
- revised summary (from scripts/gnupg.spec)
- revised source uri (from gnupg-devel mailinglist)
- decided not to use included-gettext..... now it seems safe

* Wed May 01 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.0.7-2k)
- update to 1.0.7
- obsolete g*/OPTIONS and g*/pubring.asc
- separate dir + file at files section
- add  CC=gcc and --program-prefix="" at configure
  for avoid binary prefix i586-redhat-linux-gpg

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.0.6-8k)
- rebuild against gettext 0.10.40.

* Fri Oct 12 2001 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.0.6-6k)
- use "--with-included-gettext" for configuration (the same reason for 4k)

* Wed Oct 10 2001 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.0.6-4k)
- delete LINGUAS because messages were shown as "???" with ja locale.

* Mon Jun  4 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.6-2k)
- version up

* Tue Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.4-9k)
- security fix from RH

* Fri Oct 27 2000 Toyokazu WATABE <toy2@kondra.org> 1.0.4-2k
- update for 1.0.4 including security fixes.

* Fri Jul 21 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (1.0.2-1k)
- up to 1.0.2
- add LINGUAS (do not make japanease message catalog)
- use configure not %configure (if %configure used, bindir etc was hard coded)
- remove gpg.1(ncludes man is newer)

* Wed May 31 2000 Hidetomo Hosono <h@kondara.org>
- setuid-ed /usr/bin/gpg (because of the "insecure memory" warning)

* Fri Apr 21 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot, Summary, Distribution, description )

* Sun Apr 02 2000 TABUCHI Takaaki <tab@kondara.org>
- be a NoSrc :-P

* Fri Feb 18 2000 Bill Nottingham <notting@redhat.com>
- build of 1.0.1

* Fri Sep 10 1999 Cristian Gafton <gafton@redhat.com>
- version 1.0.0 build for 6.1us
