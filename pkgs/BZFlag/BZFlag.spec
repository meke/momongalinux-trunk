%global momorel 5
%global srcname bzflag

Summary: BZFlag is a multiplayer 3D tank battle game
Name: BZFlag
Version: 2.4.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Amusements/Games
URL: http://www.bzflag.org/
Source0: http://dl.sourceforge.net/sourceforge/%{srcname}/%{srcname}-%{version}.tar.bz2
NoSource: 0
Source1: %{srcname}.desktop
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libstdc++
Requires: mesa-libGL
Requires: mesa-libGLU
Requires: mesa-libGLw
BuildRequires: SDL-devel >= 1.2.5
BuildRequires: coreutils
BuildRequires: curl-devel >= 7.16.0
BuildRequires: desktop-file-utils
BuildRequires: glew-devel >= 1.10.0
BuildRequires: groff
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXi-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libbind-devel >= 6.0
BuildRequires: libidn-devel
BuildRequires: libstdc++-devel
BuildRequires: ncurses-devel >= 5.7-14m
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: mesa-libGLw-devel
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: zlib-devel

%description
BZFlag is a multiplayer 3D tank battle game. The name stands for Battle Zone 
capture Flag. It runs on Irix, Linux, *BSD, Windows, Mac OS X and other 
platforms. It's one of the most popular games ever on Silicon Graphics systems.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure --libdir=%{_libdir}/%{srcname}
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'

# delete *.la
find %{buildroot} -name "*.la" -delete

# install desktop file
mkdir -p %{buildroot}/%{_datadir}/applications
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Game \
  --add-category ArcadeGame \
    %{SOURCE1}

# install icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 package/rpm/%{srcname}-*.xpm %{buildroot}%{_datadir}/pixmaps/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# note -- bzflag must be setuid root to use 3Dfx drivers without /dev/3dfx
%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS BUGS DEVINFO PORTING TODO
%doc ChangeLog DEVINFO INSTALL README*
%{_bindir}/bzadmin
%{_bindir}/%{srcname}
%{_bindir}/bzfs
%dir %{_libdir}/%{srcname}
%{_libdir}/%{srcname}/HoldTheFlag.so
%{_libdir}/%{srcname}/Phoenix.so
%{_libdir}/%{srcname}/RogueGenocide.so
%{_libdir}/%{srcname}/SAMPLE_PLUGIN.so
%{_libdir}/%{srcname}/TimeLimit.so
%{_libdir}/%{srcname}/airspawn.so
%{_libdir}/%{srcname}/chathistory.so
%{_libdir}/%{srcname}/customflagsample.so
%{_libdir}/%{srcname}/flagStay.so
%{_libdir}/%{srcname}/hiddenAdmin.so
%{_libdir}/%{srcname}/keepaway.so
%{_libdir}/%{srcname}/killall.so
%{_libdir}/%{srcname}/koth.so
%{_libdir}/%{srcname}/logDetail.so
%{_libdir}/%{srcname}/nagware.so
%{_libdir}/%{srcname}/playHistoryTracker.so
%{_libdir}/%{srcname}/pushstats.so
%{_libdir}/%{srcname}/rabbitTimer.so
%{_libdir}/%{srcname}/rabidRabbit.so
%{_libdir}/%{srcname}/recordmatch.so
%{_libdir}/%{srcname}/regFlag.so
%{_libdir}/%{srcname}/serverControl.so
%{_libdir}/%{srcname}/shockwaveDeath.so
%{_libdir}/%{srcname}/teamflagreset.so
%{_libdir}/%{srcname}/timedctf.so
%{_libdir}/%{srcname}/wwzones.so
%{_datadir}/applications/%{srcname}.desktop
%{_datadir}/%{srcname}
%{_mandir}/man5/bzw.5*
%{_mandir}/man6/bzadmin.6*
%{_mandir}/man6/%{srcname}.6*
%{_mandir}/man6/bzfs.6*
%{_datadir}/pixmaps/%{srcname}-?.xpm

%changelog
* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-5m)
- rebuild against glew-1.10.0

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-4m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-3m)
- rebuild for glew-1.9.0

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-2m)
- rebuild against glew-1.7.0

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.16-3m)
- full rebuild for mo7 release

* Sat May  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.16-2m)
- set BR: ncurses-devel >= 5.7-14m
- touch up spec file

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.12-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.12-3m)
- rebuild against libbind-6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.12-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.12-1m)
- update to 2.0.12
- License: LGPLv2+

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.10-2m)
- rebuild against openssl-0.9.8h-1m

* Fri May 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.10-1m)
- 2.0.10 has been released

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-7m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-6m)
- rebuild against openldap-2.4.8

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-5m)
- %%NoSource -> NoSource

* Thu Jan 17 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-4m)
- add patch for gcc43, generated by gen43patch(v1)

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.8-3m)
- rebuild against curl-7.16.0

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.8-2m)
- remove category Application

* Fri May 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Mon Mar 27 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.0.2-4m)
- change Mesa -> mesa-*

* Tue Feb 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-3m)
- update bzflag.desktop

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-2m)
- add gcc4 patch.
- Patch0: bzflag-2.0.2-gcc4.patch

* Tue Jul 12 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.2-1m)
- BZFlag 2 has been released
- BZFlag seems to link zlib dynamically now.

* Wed Nov 10 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.10.8-1m)
- update to 1.10.8
- install desktop file using desktop-file-utils

* Fri May 28 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.10.6-1m)
- update to 1.10.6

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.10.4-1m)
- update to 1.10.4
- minor bugfixes

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.10.0-1m)
- update to 1.10.0
- This version breaks compatibility with all previous BZFlag releases

* Thu Jul 10 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.7g2-1m)
- update to 1.7g2
- install KDE menu and icon

* Sun Jan 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.7g0-1m)
- update to 1.7g0
  [Momonga-devel.ja:01293], thanks to Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- add BuildRequires: groff

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (1.7e4-2k)
- rebuild against XFree86-4.2.0 with internal Mesa.

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.7e1-16k)
- add BuildRequires,Requires

* Sun Oct 14 2001 Masaru Sato <masachan@kondara.org>
- modify BuildRoot

* Tue Apr 10 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.7e1-12k)
- errased Glide3 Glide3-devel from tag

* Wed Mar 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.7e1-2k)
- update to 1.7e1

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.7d.9-11k)
- added bzflag.as for afterstep

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (1.7d.9-9k)
- rebuild against gcc 2.96.

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.7d.9-7k)
- added bzflag.desktop

* Thu Jan  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.7d.9-5k)
- rebuild against Glide3

* Mon Oct 30 2000 Toru Hoshina <toru@df-usa.com>
- 1st release for Kondara.
