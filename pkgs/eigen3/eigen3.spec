%global		momorel 2

Name:           eigen3
Version:        3.1.3
Release:        %{momorel}m%{?dist}
Summary:        A lightweight C++ template library for vector and matrix math

Group:          Development/Libraries
License:        "MPLv2.0 and LGPLv2+ and BSD"
URL:            http://eigen.tuxfamily.org/index.php?title=Main_Page
Source0:	http://bitbucket.org/eigen/eigen/get/%{version}.tar.bz2
NoSource:	0
Patch0:         eigen3_unused-typedefs.patch
# Fix for Upstream bug 554: http://eigen.tuxfamily.org/bz/show_bug.cgi?id=554 
# Derived from hg changeset https://bitbucket.org/eigen/eigen/commits/21273ebd6b4d/
# Should fix rhbz 978971
Patch1:         eigen3-3.1.3-memalign.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  atlas-devel
BuildRequires:  fftw-devel
BuildRequires:  glew-devel
BuildRequires:  gmp-devel
BuildRequires:  gsl-devel
BuildRequires:  mpfr-devel
BuildRequires:  sparsehash-devel
BuildRequires:  suitesparse-devel

BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  graphviz
BuildRequires:  texlive-pdftex

%description
%{summary}

%package devel
Summary: A lightweight C++ template library for vector and matrix math
Group:   Development/Libraries
# -devel subpkg only atm, compat with other distros
Provides: %{name} = %{version}-%{release}
# not *strictly* a -static pkg, but the results are the same
Provides: %{name}-static = %{version}-%{release}
%description devel
%{summary}

%prep
%setup -q -n eigen-eigen-2249f9c22fe8
%patch0 -p1
%patch1 -p1

%build
mkdir %{_target_platform}
pushd %{_target_platform}
%cmake .. -DBLAS_LIBRARIES="cblas" -DBLAS_LIBRARIES_DIR=%{_libdir}/atlas
popd
make -C %{_target_platform} %{?_smp_mflags}
make doc -C %{_target_platform} %{?_smp_mflags}

rm -f %{_target_platform}/doc/html/installdox
rm -f %{_target_platform}/doc/html/unsupported/installdox

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files devel
%defattr(-,root,root,-)
%doc COPYING.README COPYING.BSD COPYING.MPL2 COPYING.LGPL
%doc %{_target_platform}/doc/html
%{_includedir}/eigen3
%{_datadir}/pkgconfig/*

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.3-2m)
- rebuild against graphviz-2.36.0-1m

* Mon Jul  8 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.3-1m)
- import from fedora
