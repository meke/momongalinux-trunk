%global momorel 4

# Copyright (c) 2000-2009, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# If you don't want to build with maven, and use straight ant instead,
# give rpmbuild option '--without maven'

%define with_maven 1

%define section free

Summary:        Extract class/interface/method definitions from sources
Name:           qdox
Version:        1.10.1
Release:        %{momorel}m%{?dist}
Epoch:          0
License:        ASL 2.0
URL:            http://qdox.codehaus.org/
Group:          Development/Libraries
Source0:        %{name}-%{version}.tar.bz2
# svn export http://svn.codehaus.org/qdox/tags/qdox-1.10.1/
# tar cjf qdox-1.10.1.tar.bz2 qdox-1.10.1/

Source1:        build.xml.tar.gz
Source2:        qdox-settings.xml
Patch0:         %{name}-disable-xsite.patch

BuildRequires:  jpackage-utils >= 0:1.7.4
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  ant >= 0:1.6
BuildRequires:  ant-junit >= 0:1.6
BuildRequires:  junit >= 0:3.8.1
BuildRequires:  byaccj
BuildRequires:  jflex
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.7
BuildRequires:  maven2-plugin-ant
BuildRequires:  maven2-plugin-antrun
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-changes
BuildRequires:  maven2-plugin-clean
BuildRequires:  maven-plugin-cobertura
BuildRequires:  maven2-plugin-dependency
BuildRequires:  maven2-plugin-deploy
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-site
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven-surefire-maven-plugin
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  maven-jflex-plugin
%endif

Requires:          java >= 1.6.0
Requires(post):    jpackage-utils >= 0:1.7.4
Requires(postun):  jpackage-utils >= 0:1.7.4

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot

Obsoletes:      qdox-manual <= 0:1.9.2


%description
QDox is a high speed, small footprint parser
for extracting class/interface/method definitions
from source files complete with JavaDoc @tags.
It is designed to be used by active code
generators or documentation tools.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
API docs for %{name}.


%prep
%setup -q 
%patch0 -b .sav
for j in $(find . -name "*.jar"); do
    mv $j $j.no
done
rm bootstrap/yacc.linux
ln -s /usr/bin/byaccj bootstrap/yacc.linux
ln -s $(build-classpath jflex) bootstrap
#ln -s $(build-classpath java-cup) bootstrap
mkdir -p .m2/repository/JPP/maven2/default_poms
tar xzf %{SOURCE1}

cp %{SOURCE2} settings.xml
sed -i -e "s|<url>__JPP_URL_PLACEHOLDER__</url>|<url>file://`pwd`/.m2/repository</url>|g" settings.xml
sed -i -e "s|<url>__JAVADIR_PLACEHOLDER__</url>|<url>file://`pwd`/external_repo</url>|g" settings.xml
sed -i -e "s|<url>__MAVENREPO_DIR_PLACEHOLDER__</url>|<url>file://`pwd`/.m2/repository</url>|g" settings.xml

%build
%if %{with_maven}
mkdir external_repo
ln -s %{_javadir} external_repo/JPP

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
        -e \
        -s settings.xml \
        -Dmaven.test.skip=true \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        ant:ant install javadoc:javadoc

%else
mkdir -p src/java/com/thoughtworks/qdox/parser/impl
export CLASSPATH=$(build-classpath jmock jflex):target/classes:target/test-classes
java JFlex.Main \
    -d src/java/com/thoughtworks/qdox/parser/impl \
    src/grammar/lexer.flex
pushd src
byaccj \
    -Jnorun \
    -Jnoconstruct \
    -Jclass=Parser \
    -Jsemantic=Value \
    -Jpackage=com.thoughtworks.qdox.parser.impl \
    grammar/parser.y
popd
mv src/Parser.java src/java/com/thoughtworks/qdox/parser/impl
#TODO reenable test when jmock is imported
ant -Dbuild.sysclasspath=only -Dmaven.test.skip=true -Dmaven.mode.offline=true jar javadoc
%endif

%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p target/%{name}-%{version}.jar \
      $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

%add_to_maven_depmap %{name} %{name} %{version} JPP %{name}
%add_to_maven_depmap  com.thoughtworks.qdox qdox %{version} JPP %{name}


# poms
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -m 644 pom.xml \
    $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}.pom

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr target/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(0644,root,root,0755)
%doc LICENSE.txt README.txt
%{_javadir}/%{name}.jar
%{_javadir}/%{name}-%{version}.jar
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.1-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.1-1m)
- sync with Fedora 13 (0:1.10.1-2)

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-1m)
- sync with Fedora 12 (0:1.9.2-2)
- add java_cup dependency

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-2m)
- enable upgrading of javadoc

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-1m)
- sync with Fedoar 11 (0:1.6.1-6.2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2jpp.2m)
- rebuild against rpm-4.6

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-2jpp.1m)
- import from Fedora to Momonga

* Fri Feb 22 2008 Matt Wringe <mwringe@redhat.com> - 0:1.5-2jpp.2
- add a patch (qdox-flex.patch) so that qdox will build with
  the new jflex.

* Thu Feb 15 2007 Permaine Cheung <pcheung@redhat.com> - 0:1.5-2jpp.1
- Use ant for building, and fixes as per fedora guidelines.

* Mon Feb 20 2006 Ralph Apel <r.apel at r-apel.de> - 0:1.5-2jpp
- Rebuild for JPP-1.7, adapting to maven-1.1

* Wed Nov 16 2005 Ralph Apel <r.apel at r-apel.de> - 0:1.5-1jpp
- Upgrade to 1.5
- Build is now done with maven and requires jflex and byaccj

* Wed Aug 25 2004 Fernando Nasser <fnasser@redhat.com> - 0:1.4-3jpp
- Rebuild with Ant 1.6.2

* Fri Aug 06 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.4-2jpp
- Upgrade to ant-1.6.X

* Mon Jun 07 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.4-1jpp
- Upgrade to 1.4
- Drop Requires: mockobjects (Build/Test only)

* Tue Feb 24 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.3-1jpp
- First JPackage release
