%global momorel 1

%global rcver 2

Name: log4cplus
Version: 1.1.0
#Release: %{momorel}m%{?dist}
Release: 0.%{rcver}.%{momorel}m%{?dist}

Summary: log4cplus, C++ logging library
License: Apache
Group: Development/Libraries
URL: http://log4cplus.sourceforge.net/

%global srcname %{name}-%{version}-rc%{rcver}
#Source0: http://dl.sourceforge.net/projects/%{name}/%{name}-%{version}.tar.bz2
#Source0: http://dl.sourceforge.net/projects/%{name}/%{srcname}.tar.bz2
Source0: http://dl.sourceforge.net/projects/%{name}/%{srcname}.tar.xz
#NoSource: 0 

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
log4cplus is a simple to use C++ logging API providing thread-safe,
flexible, and arbitrarily granular control over log management and
configuration. It is modeled after the Java log4j API.

%package devel
Summary: log4cplus headers, static libraries
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
log4cplus is a simple to use C++ logging API providing thread-safe,
flexible, and arbitrarily granular control over log management and
configuration. It is modeled after the Java log4j API.

%prep
rm -rf %{buildroot}
%setup -q -n %{srcname}

%build
%configure
%make

%install
rm -rf %{buildroot}

%makeinstall

mkdir -p %{buildroot}%{prefix}/include/
cp -rp include/log4cplus %{buildroot}%{prefix}/include/

find %{buildroot}%{_libdir} -name '*.la' -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,755)
%attr(755,root,root) %{_libdir}/lib*.so.*
#%%attr(644,root,root) %{_libdir}/*.la

%files devel
%defattr(-,root,root,755)

%dir %{_includedir}/log4cplus/
%{_includedir}/log4cplus/*
%attr(644,root,root) %{_libdir}/*.a
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Sat Mar 31 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-0.2.1m)
- update to 1.1.0-rc2
- No NoSource: 0

* Sat Mar 31 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-1m)
- initial spec file for bind10
