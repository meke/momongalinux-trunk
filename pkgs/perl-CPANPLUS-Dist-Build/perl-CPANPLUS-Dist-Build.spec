%global         momorel 1

Name:           perl-CPANPLUS-Dist-Build
Version:        0.78
Release:        %{momorel}m%{?dist}
Epoch:          20
Summary:        CPANPLUS plugin to install packages that use Build.PL
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CPANPLUS-Dist-Build/
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/CPANPLUS-Dist-Build-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.0
BuildRequires:  perl-CPANPLUS >= 0.84
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-Install >= 1.42
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-if
BuildRequires:  perl-IPC-Cmd >= 0.42
BuildRequires:  perl-Locale-Maketext-Simple
BuildRequires:  perl-Module-Build >= 0.32
BuildRequires:  perl-Module-Load >= 0.16
BuildRequires:  perl-Module-Load-Conditional >= 0.30
BuildRequires:  perl-Params-Check >= 0.26
BuildRequires:  perl-Test-Harness >= 3.16
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-CPANPLUS >= 0.84
Requires:       perl-Cwd
Requires:       perl-ExtUtils-Install >= 1.42
Requires:       perl-if
Requires:       perl-IPC-Cmd >= 0.42
Requires:       perl-Locale-Maketext-Simple
Requires:       perl-Module-Build >= 0.32
Requires:       perl-Module-Load >= 0.16
Requires:       perl-Module-Load-Conditional >= 0.30
Requires:       perl-Params-Check >= 0.26
Requires:       perl-Test-Harness >= 3.16
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
CPANPLUS::Dist::Build is a distribution class for Module::Build related
modules. Using this package, you can create, install and uninstall perl
modules. It inherits from CPANPLUS::Dist.

%prep
%setup -q -n CPANPLUS-Dist-Build-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/CPANPLUS/Dist/Build.pm
%{perl_vendorlib}/CPANPLUS/Dist/Build
%{_mandir}/man3/*

%changelog
* Sun Jun 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:0.78-1m)
- perl-CPANPLUS-Dist-Build was removed from perl core libraries
