%global momorel 2

Summary:        IRQ balancing daemon.
Name:           irqbalance
Version:        1.0.7
Release:        %{momorel}m%{?dist}
Epoch:          1
Group:          System Environment/Base
License:        "GPL/OSL"
Source0:        http://irqbalance.googlecode.com/files/irqbalance-%{version}.tar.bz2
NoSource:       0
Source1:        irqbalance.sysconfig
Patch1: irqbalance-1.0.4-env-file-path.patch
Patch2: irqbalance-1.0.7-nopcidevs_memleak.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  glib2-devel pkgconfig imake libcap-ng-devel numactl
Requires:       numactl
Requires(post): systemd-units
Requires(postun):systemd-units
Requires(preun):systemd-units
ExclusiveArch:	%{ix86} x86_64 ia64
Obsoletes:      kernel-utils

%description
irqbalance is a daemon that evenly distributes IRQ load across
multiple CPUs for enhanced performance.

%prep
%setup -q
%patch1 -p1
%patch2 -p1

%build
%configure 
CFLAGS="%{optflags}" make %{?_smp_mflags}

%install
install -D -p -m 0755 %{name} %{buildroot}%{_sbindir}/%{name}
install -D -p -m 0644 ./misc/irqbalance.service %{buildroot}/%{_unitdir}/irqbalance.service
install -D -p -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

install -d %{buildroot}%{_mandir}/man1/
install -p -m 0644 ./irqbalance.1 %{buildroot}%{_mandir}/man1/

%files
%doc COPYING AUTHORS
%{_sbindir}/irqbalance
%{_unitdir}/irqbalance.service
%{_mandir}/man1/*
%config(noreplace) %{_sysconfdir}/sysconfig/irqbalance
    
%post
%systemd_post irqbalance.service

%preun
%systemd_preun irqbalance.service

%postun 
%systemd_postun_with_restart irqbalance.service

%triggerun -- irqbalance < 1:0.56
if /sbin/chkconfig --level 3 irqbalance ; then
    /bin/systemctl enable irqbalance.service >/dev/null 2>&1 || :
fi
/sbin/chkconfig --del irqbalance >/dev/null 2>&1 || :

%files
%doc COPYING AUTHORS
%{_sbindir}/irqbalance
%{_unitdir}/irqbalance.service
%{_mandir}/man1/*
%config(noreplace) %{_sysconfdir}/sysconfig/irqbalance

%changelog
* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.0.7-2m)
- import fedora patches

* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.0.7-1m)
- update 1.0.7

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0.3-2m)
- rebuild for glib 2.33.2

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.0.3-1m)
- update 1.0.3

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.0.2-1m)
- update 1.0.2

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.0-2m)
- add patch. from fedora

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:1.0-1m)
- update 1.0

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:0.56-1m)
- update 0.56

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.55-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.55-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.55-10m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.55-9m)
- add epoch to %%changelog

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:0.55-8m)
- use Requires

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.55-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.55-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:0.55-5m)
- rebuild against gcc43

* Thu Mar  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.55-4m)
- import fedora patches

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.55-3m)
- %%NoSource -> NoSource

* Thu May  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.55-2m)
- add Epoch: 1 for upgrading from STABLE_3

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.55-1m)
- update 0.55

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12-1m)
- import from fc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild with gcc4

* Tue Feb  8 2005 Dave Jones <davej@redhat.com>
- Build as pie, also -D_FORTIFY_SOURCE=2

* Tue Jan 11 2005 Dave Jones <davej@redhat.com>
- Add missing Obsoletes: kernel-utils.

* Mon Jan 10 2005 Dave Jones <davej@redhat.com>
- Start irqbalance in runlevel 2 too. (#102064)

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based on kernel-utils.

