%global momorel 13
%global srcrel 58240
%global qtver 4.8.2

# brp-strip-static-archive does not work...
%global __os_install_post /usr/lib/rpm/momonga/brp-compress; /usr/lib/rpm/momonga/brp-strip; /usr/lib/rpm/momonga/modify-init.d; /usr/lib/rpm/momonga/modify-la

Summary: A free internet radio player on demand
Name: last.fm
Version: 1.4.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.last.fm/
Source0: http://cdn.last.fm/client/src/%{name}-%{version}.%{srcrel}.src.tar.bz2
Source1: %{name}.desktop
Source2: as48.png
Patch0: %{name}-%{version}.%{srcrel}-gcc43.patch
Patch1: %{name}-%{version}.%{srcrel}-x86_64.patch
Patch2: %{name}-%{version}.%{srcrel}-linking.patch
Patch3: %{name}-%{version}.%{srcrel}-glib2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick
BuildRequires: alsa-lib-devel
BuildRequires: coreutils
BuildRequires: fftw-devel
BuildRequires: glib2-devel >= 2.33.10
BuildRequires: libgpod-devel >= 0.7.0
BuildRequires: libmad
BuildRequires: libsamplerate-devel
BuildRequires: qt-devel >= %{qtver}

%description
With Last.fm on your computer you can scrobble your tracks,
share your music taste, listen to personalised radio streams,
and discover new music and people.
Last.fm is open source software and contains no spyware / adware.
(We hate that stuff as much as you do.)

%prep
%setup -q -n %{name}-%{version}.%{srcrel}

%patch0 -p1 -b .gcc43

# 64bit fix
%ifarch x86_64
%patch1 -p1 -b .fix-build
%endif

%patch2 -p1 -b .lz-lX11
%patch3 -p1 -b .glib2

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing...
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/%{name}
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps

# install last.fm
cat <<END > %{buildroot}%{_bindir}/%{name}
#!/bin/bash
cd %{_libdir}/%{name}/
exec ./%{name}.sh $@
END

# fix up permission
chmod 755 %{buildroot}%{_bindir}/%{name}

# install last.fm
cp -a bin/* %{buildroot}%{_libdir}/%{name}/

# install desktop file
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icons
convert -scale 16x16 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/as.png
convert -scale 32x32 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/as.png
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/as.png

# link icon
ln -s ../icons/hicolor/48x48/apps/as.png %{buildroot}%{_datadir}/pixmaps/as.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/as.png
%{_datadir}/pixmaps/as.png

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-13m)
- add source
- enable to build with glib2-2.33.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-11m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-10m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-9m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-8m)
- rebuild against qt-4.6.3-1m

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-7m)
- fix build with new gcc

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-6m)
- modify __os_install_post for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-4m)
- rebuild against libgpod-0.7.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-3m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-2m)
- fix build on x86_64

* Wed Dec 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- initial package for music freaks using Momonga Linux
