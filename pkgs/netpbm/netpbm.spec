%global         momorel 1

Summary:        A library for handling different graphics file formats
Name:           netpbm
Version:        10.47.37
Release:        %{momorel}m%{?dist}
License:        see "copyright_summary"
Group:          System Environment/Libraries
URL:            http://netpbm.sourceforge.net/
# Source0 is prepared by
# svn checkout https://netpbm.svn.sourceforge.net/svnroot/netpbm/stable netpbm-%{version}
# svn checkout https://netpbm.svn.sourceforge.net/svnroot/netpbm/userguide netpbm-%{version}/userguide
# and removing the .svn directories ( find -name "\.svn" -type d -print0 | xargs -0 rm -rf )
# and removing the ppmtompeg code, due to patents ( rm -rf netpbm-%{version}/converter/ppm/ppmtompeg/ )
Source0:        netpbm-%{version}.tar.xz
Patch1:         netpbm-time.patch
Patch2:         netpbm-message.patch
Patch3:         netpbm-security-scripts.patch
Patch4:         netpbm-security-code.patch
Patch5:         netpbm-nodoc.patch
Patch6:         netpbm-gcc4.patch
Patch7:         netpbm-bmptopnm.patch
Patch8:         netpbm-CAN-2005-2471.patch
Patch9:         netpbm-xwdfix.patch
Patch11:        netpbm-multilib.patch
Patch12:        netpbm-pamscale.patch
Patch13:        netpbm-glibc.patch
Patch14:        netpbm-svgtopam.patch
Patch15:        netpbm-docfix.patch
Patch16:        netpbm-ppmfadeusage.patch
Patch17:        netpbm-fiasco-overflow.patch
Patch18:        netpbm-lz.patch
Patch19:        netpbm-pnmmontagefix.patch
Patch20:        netpbm-noppmtompeg.patch
Patch21:        netpbm-cmuwtopbm.patch
Patch22:        netpbm-pamtojpeg2k.patch
Patch23:        netpbm-manfix.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  libpng-devel >= 1.2.2
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  flex
BuildRequires:  libX11-devel
BuildRequires:  python
BuildRequires:  jasper-devel
BuildRequires:  zlib-devel >= 1.1.4-5m
Obsoletes:      libgr
Provides:       libgr

%description
The netpbm package contains a library of functions which support
programs for handling various graphics file formats, including .pbm
(portable bitmaps), .pgm (portable graymaps), .pnm (portable anymaps),
.ppm (portable pixmaps) and others.

%package devel
Summary:        Development tools for programs which will use the netpbm libraries
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Obsoletes:      libgr-devel
Provides:       libgr-devel

%description devel
The netpbm-devel package contains the header files and static libraries,
etc., for developing programs which can handle the various graphics file
formats supported by the netpbm libraries.

Install netpbm-devel if you want to develop programs for handling the
graphics file formats supported by the netpbm libraries.  You'll also need
to have the netpbm package installed.

%package progs
Summary:        Tools for manipulating graphics files in netpbm supported formats
Group:          Applications/Multimedia
Requires:       %{name} = %{version}-%{release}
Obsoletes:      libgr-progs
Provides:       libgr-progs

%description progs
The netpbm-progs package contains a group of scripts for manipulating the
graphics files in formats which are supported by the netpbm libraries.  For
example, netpbm-progs includes the rasttopnm script, which will convert a
Sun rasterfile into a portable anymap.  Netpbm-progs contains many other
scripts for converting from one graphics file format to another.

If you need to use these conversion scripts, you should install
netpbm-progs.  You'll also need to install the netpbm package.

%package doc
Summary:        Documentation for tools manipulating graphics files in netpbm supported formats
Group:          Applications/Multimedia
Requires:       %{name}-progs = %{version}-%{release}

%description doc
The netpbm-doc package contains a documentation in HTML format for utilities
present in netpbm-progs package.

If you need to look into the HTML documentation, you should install
netpbm-doc.  You'll also need to install the netpbm-progs package.

%prep
%setup -q
%patch1 -p1 -b .time
%patch2 -p1 -b .message
%patch3 -p1 -b .security-scripts
%patch4 -p1 -b .security-code
%patch5 -p1 -b .nodoc
%patch6 -p1 -b .gcc4
%patch7 -p1 -b .bmptopnm
%patch8 -p1 -b .CAN-2005-2471
%patch9 -p1 -b .xwdfix
%patch11 -p1 -b .multilib
%patch12 -p1 -b .pamscale
%patch13 -p1 -b .glibc
%patch14 -p1 -b .svgtopam
%patch15 -p1
%patch16 -p1 -b .ppmfadeusage
%patch17 -p1 -b .fiasco-overflow
%patch18 -p1 -b .lz
%patch19 -p1 -b .pnmmmontagefix
%patch20 -p1 -b .noppmtompeg
%patch21 -p1 -b .cmuwtopbmfix
%patch22 -p1 -b .pamtojpeg2kfix
%patch23 -p1 -b .manfix

%build
# don't edit below, it's options for configure...
./configure <<EOF



















EOF

TOP=`pwd`
make \
        CC="%{__cc}" \
        LDFLAGS="-L$TOP/pbm -L$TOP/pgm -L$TOP/pnm -L$TOP/ppm" \
        CFLAGS="$RPM_OPT_FLAGS -fPIC -flax-vector-conversions -fno-strict-aliasing" \
        LADD="-lm" \
        JPEGINC_DIR=%{_includedir} \
        PNGINC_DIR=%{_includedir} \
        TIFFINC_DIR=%{_includedir} \
        JPEGLIB_DIR=%{_libdir} \
        PNGLIB_DIR=%{_libdir} \
        TIFFLIB_DIR=%{_libdir} \
        LINUXSVGALIB="NONE" \
        X11LIB=%{_libdir}/libX11.so \
        XML2LIBS="NONE" \
        JASPERLIB="" \
        JASPERDEPLIBS="-ljasper" \
        JASPERHDR_DIR="/usr/include/jasper"

# prepare man files
cd userguide
for i in *.html ; do
  ../buildtools/makeman ${i}
done
for i in 1 3 5 ; do
  mkdir -p man/man${i}
  mv *.${i} man/man${i}
done

%install
rm -rf --preserve-root %{buildroot}

mkdir -p %{buildroot}
make package pkgdir=%{buildroot}/usr LINUXSVGALIB="NONE" XML2LIBS="NONE"

# Ugly hack to have libs in correct dir on 64bit archs.
mkdir -p %{buildroot}%{_libdir}
if [ "%{_libdir}" != "/usr/lib" ]; then
  mv %{buildroot}/usr/lib/lib* %{buildroot}%{_libdir}
fi

cp -af lib/libnetpbm.a %{buildroot}%{_libdir}/libnetpbm.a
ln -sf libnetpbm.so.10 %{buildroot}%{_libdir}/libnetpbm.so

mkdir -p %{buildroot}%{_datadir}
mv userguide/man %{buildroot}%{_mandir}

# Get rid of the useless non-ascii character in pgmminkowski.1
sed -i 's/\xa0//' %{buildroot}%{_mandir}/man1/pgmminkowski.1

# Don't ship man pages for non-existent binaries and bogus ones
for i in hpcdtoppm pcdovtoppm pnmtojbig \
	 ppmsvgalib vidtoppm picttoppm jbigtopnm \
	 directory error extendedopacity \
	 pam pbm pgm pnm ppm index libnetpbm_dir \
	 liberror pambackground pamfixtrunc \
	 pamtogif pamtooctaveimg pamundice ppmtotga; do
	rm -f %{buildroot}%{_mandir}/man1/${i}.1
done
rm -f %{buildroot}%{_mandir}/man5/extendedopacity.5

mkdir -p %{buildroot}%{_datadir}/netpbm
mv %{buildroot}/usr/misc/*.map %{buildroot}%{_datadir}/netpbm/
mv %{buildroot}/usr/misc/rgb.txt %{buildroot}%{_datadir}/netpbm/
rm -rf %{buildroot}/usr/README
rm -rf %{buildroot}/usr/VERSION
rm -rf %{buildroot}/usr/link
rm -rf %{buildroot}/usr/misc
rm -rf %{buildroot}/usr/man
rm -rf %{buildroot}/usr/pkginfo
rm -rf %{buildroot}/usr/config_template

# Don't ship the static library
rm -f %{buildroot}%{_libdir}/lib*.a

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc doc/copyright_summary doc/COPYRIGHT.PATENT doc/GPL_LICENSE.txt doc/HISTORY README
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%{_libdir}/lib*.so
%{_mandir}/man3/*

%files progs
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_datadir}/%{name}

%files doc
%defattr(-,root,root)
%doc userguide

%changelog
* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.47.37-1m)
- update to 10.47.37
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.47.24-2m)
- rebuild for new GCC 4.6

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.47.24-1m)
- sync with Fedora devel (10.47.24-1)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.47.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10.47.17-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (10.47.17-1m)
- update to 10.47.17
- sync with Fedora devel

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.67-5m)
- explicitly link libz

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.67-4m)
- rebuild against libjpeg-8a

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.67-3m)
- [SECURITY] CVE-2009-4274
- apply the upstream patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.67-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.35.67-1m)
- sync with Fedora (10.35.67-1)
- rebuild against libjpeg-7

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.64-1m)
- sync with Fedora 11 (10.35.64-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.57-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.35.57-1m)
- [SECURITY] CVE-2008-4799 (fixed in 10.35.48)
- update to 10.35.57
-- import Patch4,6,20,21,22 from Rawhide (10.35.57-3)
-- drop Patch5, not needed
-- License: see "copyright_summary"

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (10.35.45-1m)
- update to 10.35.45

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.35.38-3m)
- rebuild against gcc43

* Thu Feb 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (10.35.38-2m)
- fix gcc-4.1.x env

* Thu Feb 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (10.35.38-1m)
- update 10.35.38

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (10.35-3m)
- rebuild against perl-5.10.0-1m

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.35-2m)
- fix %%changelog section

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.35-1m)
- sync FC7
- update 10.35

* Wed May  3 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.33-1m)
- [SECURITY] CVE-2005-2471 SA16184
- http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=319757

* Sat Mar 12 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (10.26.5-1m)
- version up
- it may contain security fix.

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (10.18.6-4m)
- enable x86_64.

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (10.18.6-3m)
- revised spec. g3topbm.

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (10.18.6-2m)
- revised spec for rpm 4.2.

* Mon Jan 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (10.18.6-1m)
- update to 1.18.6

* Thu Dec 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.11.15-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.11.15-1m)
- update to 10.11.15
- fix configure
- change License: "Artistic License and GPL and MIT/X"
- add BuildPreReq: perl
- add "make package"
- add %%{_libdir}/lib*.so into %%files devel section
- add %%{_libdir}/*.map and %%{_libdir}/rgb.txt into %%files progs section 

* Sun Oct 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.11.14-1m)
- update to 10.11.14
- update Patch10: to netpbm-10.11.14-burngif.patch
- chage file path in exec mv

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.25-6m)
- rebuild against zlib 1.1.4-5m
- move Name: tag to top of spec file for specopt

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (9.25-5m)
- add BuildPrereq: flex

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (9.25-4k)
- rebuild against libpng 1.2.2.

* Sat Mar 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (9.25-2k)

* Wed Feb  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (9.24-2k)

* Tue Jan  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (9.23-2k)

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (9.20-2k)
- version up.
- maintained for printconf environment, it's mandatory :-P

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (9.10-6k)
- rebuild against libpng 1.2.0.

* Mon Jun 18 2001 Toru Hoshina <toru@df-usa.com>
- (9.10-4k)
- fixed sshhopt.h issue.

* Sat Feb 10 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (9.10-3k)
- update to 9.10
- add URL
- add BuildPreReq: zlib-devel
- Burn All GIFs!
- disable jbig because of patent issues
- disable hpcd because it may not be sold or used for 
  profit-making activities

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<

* Tue Sep 12 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- added Provides TAG

* Mon Sep  4 2000 Kazuhiko <kazuhiko@kondara.org>
- - version 9.8

* Tue Aug 30 2000 Kazuhiko <kazuhiko@kondara.org>
- version 9.7

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- merge from rawhide.
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Jul  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 9.5

* Tue Jun 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 9.4

* Sat Jun  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- switch back to the netpbm tree, which is maintained again

* Mon Feb 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- make sure all man pages are included (#9328)
- fix pstopnm bomb when xres == yres (#9329)
- add libjpeg and libz because libtiff now needs them

* Wed Feb 02 2000 Nalin Dahyabhai <nalin@redhat.com>
- added/updated TIFF compression patch from jik@kamens.brookline.ma.us (#8826)

* Mon Dec 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- added TIFF resolution patch from jik@kamens.brookline.ma.us (#7589)

* Mon Sep 20 1999 Michael K. Johnson <johnsonm@redhat.com>
- added section 5 man pages

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- fix tiff-to-pnm.fpi (#4267)

* Thu Jul 29 1999 Bill Nottingham <notting@redhat.com>
- add a pile of foo-to-bar.fpi filters (#4251)

* Mon Mar 23 1999 Michael Johnson <johnsonm@redhat.com>
- removed old png.h header file that was causing png utils to die
- build png in build instead of install section...

* Mon Mar 22 1999 Bill Nottingham <notting@redhat.com>
- patch for 24-bit .BMP files (from sam@campbellsci.co.uk)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 15)

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- clean up the spec file
- build for glibc 2.1
- patch to fix pktopbm

* Wed Jun 10 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Wed Jun 10 1998 Jeff Johnson <jbj@redhat.com>
- glibc2 defines random in <stdlib.h> (pbm/pbmplus.h problem #693)

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu May 07 1998 Cristian Gafton <gafton@redhat.com>
- cleaned up the spec file a little bit
- validated mike's changes :-)

* Wed May 6 1998 Michael Maher <mike@redhat.com>
- added pnm-to-ps.fpi that was missing from previous packages

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- altered %%install so that the package installs now even if a previous
  version was not installed on the system 

* Thu Apr 16 1998 Erik Troan <ewt@redhat.com>
- built against libpng 1.0

* Thu Nov 06 1997 Donnie Barnes <djb@redhat.com>
- changed copyright from "distributable" to "freeware"
- added some missing scripts that existed in netpbm
- added some binaries that weren't getting built
- added patch to build tiff manipulation progs (requires libtiff)

* Wed Oct 15 1997 Donnie Barnes <djb@redhat.com>
- obsoletes netpbm now

* Tue Oct 14 1997 Erik Troan <ewt@redhat.com>
- mucked config.guess and Make.Rules to build on Alpha/Linux

* Tue Oct 07 1997 Donnie Barnes <djb@redhat.com>
- updated to 2.0.13
- dropped libjpeg and libtiff (those should come from home sources)
- removed glibc patch (new version appears to have it!)
- added i686 as a valid arch type to config.guess

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc

