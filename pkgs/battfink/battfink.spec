%global momorel 14

Name:		battfink
Summary:	Energy saving and battery status utility.
Version:	0.6.2
Release:	%{momorel}m%{?dist}
License: 	GPLv2
Group:		User Interface/Desktops
URL: 		http://www.gnome.org/
Source0:	ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.6/%{name}-%{version}.tar.bz2
NoSource:	0
Patch1:		battfink-0.6.2-xorg75.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post):	GConf2 rarian
Requires(postun): rarian
BuildRequires:	gtk2-devel >= 2.0.0
BuildRequires:	libglade-devel >= 2.0.0
BuildRequires:	GConf >= 2.0.0
BuildRequires:	libgnomeui-devel
BuildRequires:  xorg-x11-proto-devel >= 7.5
# for /usr/include/apm.h
BuildRequires:  apmd

%description
An Energy Saving Preference dialog with an associating
battery notification icon. 

%prep
%setup -q
%patch1 -p1 -b .xorg75~

%build
%configure LIBS="-lX11 -lXext"
make %{?_smp_mflags} || make

%install
rm -rf %{buildroot}
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%makeinstall

# remove... dasa...
rm -rf %{buildroot}/var/scrollkeeper

%clean
rm -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/battfink.schemas > /dev/null
scrollkeeper-update

%postun
scrollkeeper-update

%files
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/battfink.schemas
%{_bindir}/battfink
%{_libdir}/bonobo/servers/*.server
%{_datadir}/omf/battfink
%{_datadir}/control-center-2.0/capplets/battfink-preferences.desktop
%{_datadir}/locale/*/*/*.mo
%{_datadir}/pixmaps/battfink.png
%{_datadir}/battfink

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-14m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.2-11m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-10m)
- fix build

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-9m)
- fix up Requires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-8m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-6m)
- fix build failure with xorg-x11-proto-devel-7.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-5m)
- rebuild against rpm-4.6

* Thu Apr 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-4m)
- add BuildPrereq

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-3m)
- rebuild against gcc43

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.2-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.2-1m)
- version 0.6.2

* Wed Jul 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-1m)
- version 0.6.1

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6-1m)
- version 0.6

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5-1m)
- version 0.5
