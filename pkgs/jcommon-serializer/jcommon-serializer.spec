%global momorel 5

# Use rpmbuild --without gcj to disable native bits
%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name: jcommon-serializer
Version: 0.3.0
Release: %{momorel}m%{?dist}
Summary: JFree Java General Serialization Framework
License: LGPLv2+
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/jfreereport/jcommon-serializer-%{version}.tar.gz
NoSource: 0
Source1: http://dl.sourceforge.net/sourceforge/jfreereport/libserializer-1.0.0-OOo31.zip
NoSource: 1
URL: http://www.jfree.org/jfreereport/jcommon-serializer
BuildRequires: ant, java-devel, jpackage-utils, libbase >= 1.0.0
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: java, jpackage-utils, libbase >= 1.0.0
%if %{with_gcj}
BuildRequires: java-gcj-compat-devel >= 1.0.31
Requires(post): java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch: noarch
%endif
Patch1: jcommon-serializer-0.3.0-depends.patch

%description
Jcommon-serializer is a general serialization framework used by JFreeChart,
JFreeReport and other projects.

%package javadoc
Summary: Javadoc for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
Requires: jpackage-utils

%description javadoc
Javadoc for %{name}.

%prep
%setup -q
%patch1 -p1 -b .depends
find . -name "*.jar" -exec rm -f {} \;
build-jar-repository -s -p lib libbase commons-logging-api
unzip -qq %{SOURCE1} -d serializer
cd serializer
mkdir -p lib
build-jar-repository -s -p lib libbase commons-logging-api

%build
ant compile javadoc
cd serializer
ant jar

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
cp -p serializer/build/lib/libserializer.jar $RPM_BUILD_ROOT%{_javadir}/libserializer.jar

mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -rp javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}
%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc ChangeLog.txt licence-LGPL.txt README.txt
%{_javadir}/%{name}.jar
%{_javadir}/libserializer.jar
%if %{with_gcj}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-1m)
- sync with Fedora 11 (0.3.0-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-2m)
- rebuild against rpm-4.6

* Thu Oct 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-1m)
- import from Fedora to Momonga for pentaho-reporting-flow-engine -> OOo-3

* Wed May 07 2008 Caolan McNamara <caolanm@redhat.com> 0.2.0-3
- reshuffle

* Tue May 06 2008 Caolan McNamara <caolanm@redhat.com> 0.2.0-2
- take review notes

* Sat May 03 2008 Caolan McNamara <caolanm@redhat.com> 0.2.0-1
- initial fedora import
