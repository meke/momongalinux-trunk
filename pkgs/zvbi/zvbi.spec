%global momorel 14

%global fontdir %{_datadir}/fonts/%{name}
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: Raw VBI, Teletext and Closed Caption decoding library
Name: zvbi
Version: 0.2.33
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2
Group: System Environment/Libraries
URL: http://zapping.sourceforge.net/ZVBI/
Source0: http://dl.sourceforge.net/sourceforge/zapping/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-0.2.24-tvfonts.patch
Patch1: %{name}-0.2.25-openfix.patch
Patch2: %{name}-0.2.33-stat.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: fontpackages-filesystem
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

BuildRequires: systemd-units
BuildRequires: doxygen
BuildRequires: fontconfig-devel
BuildRequires: gettext
BuildRequires: libpng-devel
BuildRequires: xorg-x11-font-utils

%description
ZVBI provides functions to capture and decode VBI data. The vertical blanking
interval (VBI) is an interval in a television signal that temporarily suspends
transmission of the signal for the electron gun to move back up to the first
line of the television screen to trace the next screen field. The vertical
blanking interval can be used to carry data, since anything sent during the VBI
would naturally not be displayed; various test signals, closed captioning, and
other digital data can be sent during this time period.

%package devel
Summary: Header files and static libraries from zvdi
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on zvdi.

%prep
%setup -q

%patch0 -p1
%patch1 -p1
%patch2 -p1

# systemd service file 
cat >zvbid.service <<EOF
[Unit]
Description=Proxy Sharing V4L VBI Device Between Applications
After=syslog.target

[Service]
Type=forking
ExecStart=%{_sbindir}/zvbid

[Install]
WantedBy=multi-user.target

EOF

%build
%configure \
	--enable-v4l \
	--enable-dvb \
	--enable-proxy \
	--disable-rpath
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# generate and install fonts
mkdir -p %{buildroot}%{fontdir}
pushd contrib
./x11font
bdftopcf teletext.bdf | gzip -9 -c > %{buildroot}%{fontdir}/teletext.pcf.gz
bdftopcf teletexti.bdf | gzip -9 -c > %{buildroot}%{fontdir}/teletexti.pcf.gz
bdftopcf caption.bdf | gzip -9 -c > %{buildroot}%{fontdir}/caption.pcf.gz
bdftopcf captioni.bdf | gzip -9 -c > %{buildroot}%{fontdir}/captioni.pcf.gz
mkfontdir -x .bdf %{buildroot}%{fontdir}/
cat >%{buildroot}%{fontdir}/fonts.alias <<EOF
teletext   -ets-teletext-medium-r-normal--*-200-75-75-c-120-iso10646-1
EOF
touch %{buildroot}%{fontdir}/fonts.cache-1
popd

# link fonts
mkdir -p %{buildroot}%{catalogue}
ln -sf %{fontdir} %{buildroot}%{catalogue}/%{name}

mkdir -p %{buildroot}%{_unitdir}
install -m644 zvbid.service %{buildroot}%{_unitdir}
rm -rf %{buildroot}%{_initscriptdir}

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

if [ $1 = 1 ]; then
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi


%postun
/sbin/ldconfig

/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 != 0 ]; then
  /bin/systemctl try-restart zvbid.service >/dev/null 2>&1 || :
fi


%preun

if [ $1 = 0 ]; then
  /bin/systemctl --no-reload disable zvbid.service >/dev/null 2>&1 || :
  /bin/systemctl stop zvbid.service >/dev/null 2>&1 || :
fi


%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING* ChangeLog INSTALL NEWS README TODO
%{catalogue}/%{name}
%{_unitdir}/zvbid.service
%{_bindir}/%{name}-atsc-cc
%{_bindir}/%{name}-chains
%{_bindir}/%{name}-ntsc-cc
%{_libdir}/lib%{name}-chains.so.*
%{_libdir}/lib%{name}.so.*
%{_sbindir}/%{name}d
%dir %{fontdir}
%{fontdir}/*.pcf.gz
%{fontdir}/fonts.alias
%ghost %{fontdir}/fonts.cache-1
%{fontdir}/fonts.dir
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}-atsc-cc.1*
%{_mandir}/man1/%{name}-chains.1*
%{_mandir}/man1/%{name}-ntsc-cc.1*
%{_mandir}/man1/%{name}d.1*

%files devel
%defattr(-,root,root)
%{_includedir}/lib%{name}.h
%{_libdir}/pkgconfig/%{name}-0.2.pc
%{_libdir}/lib%{name}-chains.a
%{_libdir}/lib%{name}-chains.so
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so

%changelog
* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.33-14m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.33-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.33-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.33-11m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.33-10m)
- touch up spec file

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.33-9m)
- use Requires

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.33-8m)
- apply glibc212 patch

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.33-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.33-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.33-5m)
- fix up fonts

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.33-4m)
- define __libtoolize

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.33-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.33-2m)
- rebuild against rpm-4.6

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.33-1m)
- version 0.2.33

* Fri Aug 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.32-1m)
- version 0.2.32

* Sat Jul 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.31-1m)
- version 0.2.31

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.30-2m)
- rebuild against gcc43

* Thu Mar  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.30-1m)
- version 0.2.30

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.29-1m)
- version 0.2.29

* Fri Feb 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.27-1m)
- version 0.2.27

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.26-2m)
- %%NoSource -> NoSource

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.26-1m)
- version 0.2.26

* Mon Mar 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.25-1m)
- initial package for kdetv-0.8.9
