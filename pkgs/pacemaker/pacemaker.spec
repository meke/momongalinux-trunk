%global momorel 4

%global gname haclient
%global uname hacluster
%global pcmk_docdir %{_docdir}/%{name}

# Supported cluster stacks, must support at least one
%bcond_without ais

# ESMTP is not available in RHEL, only in EPEL. Allow people to build
# the RPM without ESMTP in case they choose not to use EPEL packages
%bcond_without esmtp

# SNMP trap support only works with Net-SNMP 5.4 and above
# And links against libperl.so which is borked on fedora
%bcond_with snmp

# Support additional trace logging
%bcond_with tracedata

# We generate some docs using Publican, but its not available everywhere
%bcond_with publican

%global specversion 3
%global upstream_version 1.1.10
%global upstream_prefix pacemaker-Pacemaker

# Keep around for when/if required
#global alphatag %{upstream_version}.hg

%global pcmk_release %{?alphatag:0.}%{specversion}%{?alphatag:.%{alphatag}}%{?dist}

# Compatibility macros for distros that don't provide Python macros by default.
# Do this instead of trying to conditionally include
#   {_rpmconfigdir}/macros.python which doesn't always exist
%{!?py_ver:    %{expand: %%global py_ver      %%(echo `python -c "import sys; print sys.version[:3]"`)}}
%{!?py_prefix: %{expand: %%global py_prefix   %%(echo `python -c "import sys; print sys.prefix"`)}}
%{!?py_libdir:	%{expand: %%global py_libdir   %%{expand:%%%%{py_prefix}/%%%%{_lib}/python%%%%{py_ver}}}}
%{!?py_sitedir: %{expand: %%global py_sitedir %%{expand:%%%%{py_libdir}/site-packages}}}

Name:		pacemaker
Summary:	Scalable High-Availability cluster resource manager
Version:	1.1.10
Release:	%{momorel}m%{?dist}
License:	GPLv2+ and LGPLv2+
Url:		http://www.clusterlabs.org
Group:		System Environment/Daemons
Source0:	        %{upstream_prefix}-%{upstream_version}.zip
Patch0:         pacemaker-1.1.8-cast-align.patch

BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
AutoReqProv:	on
Requires:	resource-agents
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

# Required for core functionality
BuildRequires:	automake autoconf libtool pkgconfig libtool-ltdl-devel python-devel
BuildRequires:	glib2-devel cluster-glue-libs-devel >= 1.0.6-4m libxml2-devel libxslt-devel 
BuildRequires:	pkgconfig python-devel gcc-c++ bzip2-devel gnutls-devel >= 3.2.0 pam-devel

# Enables optional functionality
BuildRequires:	help2man ncurses-devel openssl-devel libselinux-devel docbook-style-xsl resource-agents

# We need reasonably recent versions of libqb
BuildRequires: libqb-devel >= 0.16.0
Requires:      libqb >= 0.16.0

%if %{with esmtp}
BuildRequires:	libesmtp-devel
%endif

%if %{with snmp}
BuildRequires:	net-snmp-devel >= 5.4
Requires:	net-snmp >= 5.4
%endif

%if %{with ais}
BuildRequires:	corosynclib-devel >= 2.3.1
%endif

%if %{with publican}
%ifarch %{ix86} x86_64
BuildRequires:	publican
%endif
%endif

Obsoletes: %{name}-cloud

%description
Pacemaker is an advanced, scalable High-Availability cluster resource
manager for Linux-HA (Heartbeat) and/or OpenAIS.

It supports "n-node" clusters with significant capabilities for
managing resources and dependencies.

It will run scripts at initialization, when machines go up or down,
when related resources fail and can be configured to periodically check
resource health.

%package cli
License:      GPLv2+ and LGPLv2+
Summary:      Command line tools for controlling Pacemaker clusters
Group:        System Environment/Daemons
Requires:     %{name}-libs = %{version}-%{release}
Requires:     perl-TimeDate

%description cli
Pacemaker is an advanced, scalable High-Availability cluster resource
manager for Corosync, CMAN and/or Linux-HA.

The pacemaker-cli package contains command line tools that can be used
to query and control the cluster from machines that may, or may not,
be part of the cluster.

%package -n pacemaker-libs
License:	GPLv2+ and LGPLv2+
Summary:	Libraries used by the Pacemaker cluster resource manager and its clients
Group:		System Environment/Daemons
Requires:	%{name} = %{version}-%{release}

%description -n pacemaker-libs
Pacemaker is an advanced, scalable High-Availability cluster resource
manager for Linux-HA (Heartbeat) and/or OpenAIS.

It supports "n-node" clusters with significant capabilities for
managing resources and dependencies.

It will run scripts at initialization, when machines go up or down,
when related resources fail and can be configured to periodically check
resource health.

%package -n pacemaker-cluster-libs
License:      GPLv2+ and LGPLv2+
Summary:      Cluster Libraries used by Pacemaker
Group:        System Environment/Daemons
Requires:     %{name}-libs = %{version}-%{release}

%description -n pacemaker-cluster-libs
Pacemaker is an advanced, scalable High-Availability cluster resource
manager for Corosync, CMAN and/or Linux-HA.

The pacemaker-cluster-libs package contains cluster-aware shared
libraries needed for nodes that will form part of the cluster nodes.

%package remote
License:      GPLv2+ and LGPLv2+
Summary:      Pacemaker remote daemon for non-cluster nodes
Group:        System Environment/Daemons
Requires:     %{name}-libs = %{version}-%{release}
Requires:      %{name}-cli = %{version}-%{release}
Requires:      resource-agents
%if %{defined systemd_requires}
%systemd_requires
%endif

%description remote
Pacemaker is an advanced, scalable High-Availability cluster resource
manager for Corosync, CMAN and/or Linux-HA.

The %{name}-remote package contains the Pacemaker Remote daemon
which is capable of extending pacemaker functionality to remote
nodes not running the full corosync/cluster stack.

%package -n pacemaker-libs-devel 
License:	GPLv2+ and LGPLv2+
Summary:	Pacemaker development package
Group:		Development/Libraries
Requires:	%{name}-libs = %{version}-%{release}
Requires:	cluster-glue-libs-devel libtool-ltdl-devel
%if %{with ais}
Requires:	corosynclib-devel >= 2.3.1
%endif

%description -n pacemaker-libs-devel
Headers and shared libraries for developing tools for Pacemaker.

Pacemaker is an advanced, scalable High-Availability cluster resource
manager for Linux-HA (Heartbeat) and/or OpenAIS.

It supports "n-node" clusters with significant capabilities for
managing resources and dependencies.

It will run scripts at initialization, when machines go up or down,
when related resources fail and can be configured to periodically check
resource health.

%package	cts
License:	GPLv2+ and LGPLv2+
Summary:	Test framework for cluster-related technologies like Pacemaker
Group:		System Environment/Daemons
Requires:	python

%description	cts
Test framework for cluster-related technologies like Pacemaker

%package	doc
License:	GPLv2+ and LGPLv2+
Summary:	Documentation for Pacemaker
Group:		Documentation

%description	doc
Documentation for Pacemaker.

Pacemaker is an advanced, scalable High-Availability cluster resource
manager for OpenAIS/Corosync.

It supports "n-node" clusters with significant capabilities for
managing resources and dependencies.

It will run scripts at initialization, when machines go up or down,
when related resources fail and can be configured to periodically check
resource health.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%patch0 -p1 -R

%build
./autogen.sh

%{configure}					\
	  %{?with_profiling:   --with-profiling}	\
	  --with-initdir=%{_initscriptdir}	\
	  --localstatedir=%{_var}		\
	  --with-version=%{version}-%{release}	\
	  --docdir=%{pcmk_docdir}		\
    CPPFLAGS=-DGLIB_COMPILATION

make %{_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 mcp/pacemaker.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/pacemaker

# Scripts that should be executable
chmod a+x %{buildroot}/%{_datadir}/pacemaker/tests/cts/CTSlab.py

# These are not actually scripts
find %{buildroot} -name '*.xml' -type f -print0 | xargs -0 chmod a-x
find %{buildroot} -name '*.xsl' -type f -print0 | xargs -0 chmod a-x
find %{buildroot} -name '*.rng' -type f -print0 | xargs -0 chmod a-x
find %{buildroot} -name '*.dtd' -type f -print0 | xargs -0 chmod a-x
 
# Dont package static libs or compiled python
find %{buildroot} -name '*.a' -type f -print0 | xargs -0 rm -f
find %{buildroot} -name '*.la' -type f -print0 | xargs -0 rm -f

# Don't package these either
rm -f %{buildroot}/%{_libdir}/service_crm.so

# Don't ship init scripts for systemd based platforms
rm -f %{buildroot}/%{_initscriptdir}/pacemaker
rm -f %{buildroot}/%{_initscriptdir}/pacemaker_remote

%if %{with profiling}
GCOV_BASE=%{buildroot}/%{_var}/lib/pacemaker/gcov
mkdir -p $GCOV_BASE
find . -name '*.gcno' -type f | while read F ; do
        D=`dirname $F`
        mkdir -p ${GCOV_BASE}/$D
        cp $F ${GCOV_BASE}/$D
done
%endif

%clean
rm -rf %{buildroot}

%post
%systemd_post pacemaker.service

%preun
%systemd_preun pacemaker.service

%postun
%systemd_postun_with_restart pacemaker.service 

%post remote
%systemd_post pacemaker_remote.service

%preun remote
%systemd_preun pacemaker_remote.service

%postun remote
%systemd_postun_with_restart pacemaker_remote.service 

%pre -n pacemaker-libs

getent group %{gname} >/dev/null || groupadd -r %{gname} -g 189
getent passwd %{uname} >/dev/null || useradd -r -g %{gname} -u 189 -s /sbin/nologin -c "cluster user" %{uname}
exit 0

%post -n pacemaker-libs -p /sbin/ldconfig

%postun -n pacemaker-libs -p /sbin/ldconfig

%post -n pacemaker-cluster-libs -p /sbin/ldconfig

%postun -n pacemaker-cluster-libs -p /sbin/ldconfig

%files
###########################################################
%defattr(-,root,root)

%exclude %{_datadir}/pacemaker/tests

%config(noreplace) %{_sysconfdir}/sysconfig/pacemaker
%{_sbindir}/pacemakerd

%{_unitdir}/pacemaker.service

%{_datadir}/pacemaker
%{_datadir}/snmp/mibs/PCMK-MIB.txt
%exclude %{_libexecdir}/pacemaker/lrmd_test
%exclude %{_sbindir}/pacemaker_remoted
%{_libexecdir}/pacemaker/*

%{_sbindir}/crm_attribute
%{_sbindir}/crm_master
%{_sbindir}/crm_node
%{_sbindir}/attrd_updater
%{_sbindir}/fence_legacy
%{_sbindir}/fence_pcmk
%{_sbindir}/stonith_admin

%doc %{_mandir}/man7/*
%doc %{_mandir}/man8/attrd_updater.*
%doc %{_mandir}/man8/crm_attribute.*
%doc %{_mandir}/man8/crm_node.*
%doc %{_mandir}/man8/crm_master.*
%doc %{_mandir}/man8/fence_pcmk.*
%doc %{_mandir}/man8/pacemakerd.*
%doc %{_mandir}/man8/stonith_admin.*

%doc COPYING
%doc AUTHORS
%doc ChangeLog

%dir %attr (750, %{uname}, %{gname}) %{_var}/lib/pacemaker
%dir %attr (750, %{uname}, %{gname}) %{_var}/lib/pacemaker/cib
%dir %attr (750, %{uname}, %{gname}) %{_var}/lib/pacemaker/pengine
%dir %attr (750, %{uname}, %{gname}) %{_var}/lib/pacemaker/blackbox
%ghost %dir %attr (750, %{uname}, %{gname}) %{_var}/run/crm
%dir /usr/lib/ocf
%dir /usr/lib/ocf/resource.d
/usr/lib/ocf/resource.d/pacemaker

%files cli
%defattr(-,root,root)
%{_sbindir}/cibadmin
%{_sbindir}/crm_diff
%{_sbindir}/crm_error
%{_sbindir}/crm_failcount
%{_sbindir}/crm_mon
%{_sbindir}/crm_resource
%{_sbindir}/crm_standby
%{_sbindir}/crm_verify
%{_sbindir}/crmadmin
%{_sbindir}/iso8601
%{_sbindir}/crm_shadow
%{_sbindir}/crm_simulate
%{_sbindir}/crm_report
%{_sbindir}/crm_ticket
%doc %{_mandir}/man8/*
%exclude %{_mandir}/man8/attrd_updater.*
%exclude %{_mandir}/man8/crm_attribute.*
%exclude %{_mandir}/man8/crm_node.*
%exclude %{_mandir}/man8/crm_master.*
%exclude %{_mandir}/man8/fence_pcmk.*
%exclude %{_mandir}/man8/pacemakerd.*
%exclude %{_mandir}/man8/pacemaker_remoted.*
%exclude %{_mandir}/man8/stonith_admin.*

%doc COPYING
%doc AUTHORS
%doc ChangeLog

%files -n pacemaker-libs
%defattr(-,root,root)
%{_libdir}/libcib.so.*
%{_libdir}/liblrmd.so.*
%{_libdir}/libcrmservice.so.*
%{_libdir}/libcrmcommon.so.*
%{_libdir}/libpe_status.so.*
%{_libdir}/libpe_rules.so.*
%{_libdir}/libpengine.so.*
%{_libdir}/libtransitioner.so.*
%{_libdir}/libstonithd.so.*
%doc COPYING.LIB
%doc AUTHORS

%files -n %{name}-cluster-libs
%defattr(-,root,root)
%{_libdir}/libcrmcluster.so.*
%doc COPYING.LIB
%doc AUTHORS

%files remote
%defattr(-,root,root)

%{_unitdir}/pacemaker_remote.service

%{_sbindir}/pacemaker_remoted
%{_mandir}/man8/pacemaker_remoted.*
%doc COPYING.LIB
%doc AUTHORS

%files doc
%defattr(-,root,root)
%doc %{pcmk_docdir}

%files cts
%defattr(-,root,root)
%{py_sitedir}/cts
%{_datadir}/pacemaker/tests/cts
%doc COPYING.LIB
%doc AUTHORS

%files -n pacemaker-libs-devel
%defattr(-,root,root)
%exclude %{_datadir}/pacemaker/tests/cts
%{_datadir}/pacemaker/tests
%{_includedir}/pacemaker
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%doc COPYING.LIB
%doc AUTHORS

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-3m)
- rebuild against perl-5.18.2

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-2m)
- resolve conflicting files

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-1m)
- update to 1.1.10
- rebuild against perl-5.18.1

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-15m)
- rebuild against gnutls-3.2.0

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-14m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-13m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-12m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-11m)
- rebuild against perl-5.16.1

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-10m)
- rebuild with cluster-glue-1.0.6-4m

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-9m)
- rebuild against perl-5.16.0

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-8m)
- rebuild for libesmtp-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-7m)
- rebuild for glib 2.33.2

* Sun Mar 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-6m)
- build fix

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.5-5m)
- add exclude %%{_libdir}/heartbeat/plugins/RAExec

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.5-4m)
- rebuild against net-snmp-5.7.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-3m)
- rebuild against perl-5.14.2

* Fri Sep 23 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.5-2m)
- modify spec

* Sat Sep 10 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.5-1m)
- sync Fedora

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-11m)
- rebuild against net-snmp-5.7

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-9m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-7m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-6m)
- rebuild against net-snmp-5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-3m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-2m)
- correct dependency
- remove %%doc from package libs for multilib
- move %%{_libdir}/heartbeat from pacemaker to pacemaker-libs
- modify %%files

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-10m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-9m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-8m)
- rebuild against openssl-1.0.0

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-7m)
- fix build failure; add *.pyo and *.pyc

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-6m)
- rebuild against net-snmp-5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-4m)
- remove duplicate directories

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-3m)
- change Release

* Thu Oct  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-2.1-2m)
- fix %%files

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-2.1-1m)
- import from Fedora

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 1.0.5-2.1
- rebuilt with new openssl

* Wed Aug 19 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.5-2
- Add versioned perl dependancy as specified by
    https://fedoraproject.org/wiki/Packaging/Perl#Packages_that_link_to_libperl
- No longer remove RPATH data, it prevents us finding libperl.so and no other
  libraries were being hardcoded
- Compile in support for heartbeat
- Conditionally add heartbeat-devel and corosynclib-devel to the -devel requirements 
  depending on which stacks are supported

* Mon Aug 17 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.5-1
- Add dependancy on resource-agents
- Use the version of the configure macro that supplies --prefix, --libdir, etc
- Update the tarball from upstream to version 462f1569a437 (Pacemaker 1.0.5 final)
  + High: Tools: crm_resource - Advertise --move instead of --migrate
  + Medium: Extra: New node connectivity RA that uses system ping and attrd_updater
  + Medium: crmd: Note that dc-deadtime can be used to mask the brokeness of some switches

* Tue Aug 11 2009 Ville Skytta <ville.skytta@iki.fi> - 1.0.5-0.7.c9120a53a6ae.hg
- Use bzipped upstream tarball.

* Wed Jul  29 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.5-0.6.c9120a53a6ae.hg
- Add back missing build auto* dependancies
- Minor cleanups to the install directive

* Tue Jul  28 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.5-0.5.c9120a53a6ae.hg
- Add a leading zero to the revision when alphatag is used

* Tue Jul  28 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.5-0.4.c9120a53a6ae.hg
- Incorporate the feedback from the cluster-glue review
- Realistically, the version is a 1.0.5 pre-release
- Use the global directive instead of define for variables
- Use the haclient/hacluster group/user instead of daemon
- Use the _configure macro
- Fix install dependancies

* Fri Jul  24 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.4-3
- Include an AUTHORS and license file in each package
- Change the library package name to pacemaker-libs to be more 
  Fedora compliant
- Remove execute permissions from xml related files
- Reference the new cluster-glue devel package name
- Update the tarball from upstream to version c9120a53a6ae
  + High: PE: Only prevent migration if the clone dependancy is stopping/starting on the target node
  + High: PE: Bug 2160 - Dont shuffle clones due to colocation
  + High: PE: New implementation of the resource migration (not stop/start) logic
  + Medium: Tools: crm_resource - Prevent use-of-NULL by requiring a resource name for the -A and -a options
  + Medium: PE: Prevent use-of-NULL in find_first_action()
  + Low: Build: Include licensing files

* Tue Jul 14 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.4-2
- Reference authors from the project AUTHORS file instead of listing in description
- Change Source0 to reference the project's Mercurial repo
- Cleaned up the summaries and descriptions
- Incorporate the results of Fedora package self-review

* Tue Jul 14 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0.4-1
- Initial checkin
