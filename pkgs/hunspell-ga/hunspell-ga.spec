%global momorel 5

Name: hunspell-ga
Summary: Irish hunspell dictionaries
Version: 4.4
Release: %{momorel}m%{?dist}
Source0: http://borel.slu.edu/ispell/ispell-gaeilge-%{version}.tar.gz
Source1: myspell-header
Source2: hunspell-header
Group: Applications/Text
URL: http://borel.slu.edu/ispell/index.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch
Patch1: ispell-gaeilge-4.2-buildhunspell.patch

Requires: hunspell

%description
Irish hunspell dictionaries.

%prep
%setup -q -n ispell-gaeilge-%{version}
%patch1 -p1 -b .buildhunspell.patch

%build
make
cat %{SOURCE1} %{SOURCE2} > header
export LANG=en_IE.UTF-8
ispellaff2myspell gaeilge.aff --myheader header | sed -e "s/\"\"/0/g" | sed -e "s/\"//g" > ga_IE.aff

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p ga_IE.dic ga_IE.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README COPYING ChangeLog
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4-1m)
- update to 4.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3-1m)
- import from Fedora to Momonga

* Tue Mar 04 2008 Caolan McNamara <caolanm@redhat.com> - 4.3-2
- update to latest .aff

* Mon Nov 05 2007 Caolan McNamara <caolanm@redhat.com> - 4.3-1
- latest version

* Mon Aug 20 2007 Caolan McNamara <caolanm@redhat.com> - 4.2-1
- bump to latest upstream

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 0.20060731-2
- clarify license version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20060731-1
- initial version
