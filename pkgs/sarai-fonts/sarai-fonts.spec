%global momorel 10

%global fontname sarai
%global fontconf 68-%{fontname}.conf

Name:        %{fontname}-fonts
Version:     1.0
Release:     %{momorel}m%{?dist}
Summary:     Free Sarai Hindi Truetype Font

Group:       User Interface/X
License:     GPLv2
URL:         http://www.sarai.net/practices/indic-localization
Source0:     http://www.sarai.net/practices/indic-localization/downloads-1/Sarai_07.ttf
Source1:     COPYING
Source2:     %{fontconf}
BuildArch:   noarch
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fontpackages-devel
Requires: fontpackages-filesystem

%description 
This package provides a free Sarai Hindi TrueType font. 
It is suitable for print and publishing needs.

%prep
%setup -q -T -c 
cp -p %{SOURCE1} .

%build
echo "Nothing to do in Build."

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.ttf
%doc COPYING



%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-8m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- sync with Fedora 13 (1.0-9)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-4m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Sat Feb 24 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 1.0-4
  Fix build issue with missing setup macro

* Sat Feb 16 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 1.0-3
- Fix more review issues

* Fri Feb 15 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 1.0-2
- Fix issues as per review from Rahul Bhalerao

* Thu Feb 14 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 1.0-1
- Initial build
