%global momorel 14

%global xfce4ver 4.8.0
%global exover 0.6.0

%global with_dbus 0
# build without dbus support
    
Summary:        Xfmedia - lightweight media player based on the xine engine
Name:           xfmedia
Version:        0.9.2
Release:        %{momorel}m%{?dist}

Group:          Applications/Multimedia
License:        GPLv2
URL:            http://spuriousinterrupt.org/projects/xfmedia/index.php
Source0:        http://spuriousinterrupt.org/projects/xfmedia/files/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:         %{name}-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf >= 2.50
BuildRequires:  automake >= 1.8
%{?with_dbus:BuildRequires:     dbus-glib-devel >= 0.31}
BuildRequires:  gtk2-devel
BuildRequires:  exo-devel >= %{exover}
BuildRequires:  libtool
BuildRequires:  libxfce4util-devel >= %{xfce4ver}
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  startup-notification-devel
BuildRequires:  taglib-devel
BuildRequires:  xfce4-dev-tools >= 4.6.0
BuildRequires:  xine-lib-devel
Requires:       exo >= %{exover}

%description
Xfmedia is a lightweight media player based on the xine engine. The
GTK+ GUI focuses on playing and managing audio files, but, being based
on xine, supports video as well.

%package plugins
Summary:        Xfmedia plugins
Group:          Applications/Multimedia
Requires:       %{name} = %{version}-%{release}

%description plugins
Xfmedia plugins:
- infopipe

%package devel
Summary:        Header files for Xfmedia
Group:          Applications/Multimedia
Requires:       %{name} = %{version}-%{release}

%description devel
Xfmedia - header files.

%prep
%setup -q
%patch0 -p0

%build
%configure \
        --disable-static \
        --%{?with_dbus:en}%{!?with_dbus:dis}able-dbus \
        --enable-startup-notification \
        --with-taglib \
        --x-includes=%{_includedir}/xorg \
        --x-libraries=%{_prefix}/lib/X11 \
        LIBS="-lXext -lm"
%{__make}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_libdir}/xfmedia/plugins/xfmedia-infopipe.la
%find_lang %{name}

desktop-file-install --dir %{buildroot}%{_datadir}/applications \
  --remove-category=X-XFCE \
  --add-only-show-in=XFCE \
  %{buildroot}%{_datadir}/applications/xfmedia.desktop

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(644,root,root,755)
%doc AUTHORS ChangeLog NEWS README* TODO
%dir %{_sysconfdir}/xdg/%{name}
%config(noreplace) %verify(not md5 mtime size) %{_sysconfdir}/xdg/%{name}/*
%attr(755,root,root) %{_bindir}/*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%{_datadir}/icons/hicolor/48x48/apps/*
%{_datadir}/icons/hicolor/22x22/actions/*
%{_desktopdir}/*.desktop
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/doc
%{_datadir}/%{name}/doc/xfce.css
%dir %{_datadir}/%{name}/doc/C/
%{_datadir}/%{name}/doc/C/*

%files plugins
%defattr(644,root,root,755)
%attr(755,root,root) %{_libdir}/%{name}/plugins/*.so

%files devel
%defattr(644,root,root,755)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/*.pc

%changelog
* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-14m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-11m)
- add "-lm" to LIBS

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-10m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-9m)
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-8m)
- fix build

* Mon Jul 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-7m)
- add only show on XFCE

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-6m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-4m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- initial Momonga package based on PLD
