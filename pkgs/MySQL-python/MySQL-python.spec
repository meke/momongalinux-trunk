%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: An interface to MySQL
Name: MySQL-python
Version: 1.2.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Libraries
URL: http://sourceforge.net/projects/mysql-python/
Source0: http://dl.sourceforge.net/sourceforge/mysql-python/MySQL-python-%{version}.tar.gz
NoSource: 0
# 1.2.2 has adopted python-setuptools in preference to distutils, but since
# we don't (yet?) ship that in Fedora or RHEL, we keep using the setup.py
# script from 1.2.1_p2 for now.
Source1: setup.py
Patch1: sitecfg.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python mx mysql
Requires: python
BuildRequires: python-devel >= 2.7
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: python Distutils gcc zlib-devel
BuildRequires: python-setuptools-devel >= 0.6.15
BuildRequires: openssl-devel >= 1.0.0

%description
Python interface to MySQL

MySQLdb is an interface to the popular MySQL database server for Python.
The design goals are:

-     Compliance with Python database API version 2.0 
-     Thread-safety 
-     Thread-friendliness (threads will not block each other) 
-     Compatibility with MySQL 3.23 and up

This module should be mostly compatible with an older interface
written by Joe Skinner and others. However, the older version is
a) not thread-friendly, b) written for MySQL 3.21, c) apparently
not actively maintained. No code from that version is used in MySQLdb.

%prep
%setup -q

cp -f %{SOURCE1} setup.py

%patch1 -p1

%build
rm -f doc/*~
export libdirname=%{_lib}
CFLAGS="$RPM_OPT_FLAGS" python -c 'import setuptools; execfile("setup.py")' build

%install
rm -rf $RPM_BUILD_ROOT

%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc HISTORY PKG-INFO README doc/*
##%%{python_sitelib}/MySQL_python-*.egg-info
%{_libdir}/python*/site-packages/MySQL_python-*.egg-info
##%%{python_sitelib}/MySQLdb
%{_libdir}/python*/site-packages/MySQLdb
##%%{python_sitelib}/_mysql*
%{_libdir}/python*/site-packages/_mysql*

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-6m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-5m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-4m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-9m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-7m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-6m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.2-4m)
- rebuild agaisst python-2.6.1-1m

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- rebuild against python-setuptools-0.6c9

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.2-1m)
- import from Fedora
- enable egg-info

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.2.2-6
- Autorebuild for GCC 4.3

* Wed Dec  5 2007 Tom Lane <tgl@redhat.com> 1.2.2-5
- Rebuild for new openssl

* Thu Aug  2 2007 Tom Lane <tgl@redhat.com> 1.2.2-4
- Update License tag to match code.

* Tue Jul  3 2007 Tom Lane <tgl@redhat.com> 1.2.2-3
- Ooops, previous fix for quoting bug was wrong, because it converted the
  version_info tuple to a string in Python's eyes
Resolves: #246366

* Tue Jun 12 2007 Tom Lane <tgl@redhat.com> 1.2.2-2
- Fix quoting bug in use of older setup.py: need to quote version_info now
Resolves: #243877

* Fri Apr 20 2007 Tom Lane <tgl@redhat.com> 1.2.2-1
- Update to 1.2.2, but not 1.2.2 setup.py (since we don't ship setuptools yet)

* Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 1.2.1_p2-2
- rebuild for python 2.5

* Wed Dec  6 2006 Tom Lane <tgl@redhat.com> 1.2.1_p2-1
- Update to 1.2.1_p2

* Fri Jul 21 2006 Tom Lane <tgl@redhat.com> 1.2.1-1
- Update to 1.2.1
- Remove hardwired python version number in favor of asking Python

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.2.0-3.2.2.1
- rebuild

* Mon Feb 13 2006 Jesse Keating <jkeating@redhat.com> - 1.2.0-3.2.2
- rebump for build order issues during double-long bump

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.2.0-3.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.2.0-3.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Nov  9 2005 Tom Lane <tgl@redhat.com> 1.2.0-3
- Rebuild due to mysql 5.0 update and openssl library update.

* Wed Aug 03 2005 Karsten Hopp <karsten@redhat.de> 1.2.0-2
- package all python files. INSTALLED_FILES doesn't contain files created
  by the brp-python-bytecompile script

* Thu Apr 21 2005 Tom Lane <tgl@redhat.com> 1.2.0-1
- Update to 1.2.0, per bug #155341
- Link against mysql 4.x not 3.x, per bug #150828

* Sun Mar  6 2005 Tom Lane <tgl@redhat.com> 1.0.0-3
- Rebuild with gcc4.

* Thu Nov 11 2004 Tom Lane <tgl@redhat.com> 1.0.0-2
- bring us to python 2.4

* Thu Nov 11 2004 Tom Lane <tgl@redhat.com> 1.0.0-1
- update to 1.0.0; rebuild against mysqlclient10

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 20 2004 Tom Lane <tgl@redhat.com>
- reinstate (and update) patch for /usr/lib64 compatibility
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Nov 25 2003 Patrick Macdonald <patrickm@redhat.com> 0.9.2-1
- update to 0.9.2
- remove patches (no longer applicable)

* Sat Nov 15 2003 Tom "spot" Callaway <tcallawa@redhat.com> 0.9.1-10
- bring us to python 2.3

* Thu Jul 03 2003 Patrick Macdonald <patrickm@redhat.com> 0.9.1-9
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com> 0.9.1-8
- rebuilt

* Tue Mar 04 2003 Patrick Macdonald <patrickm@redhat.com> 0.9.1-7
- explicitly define the constants directory in case a more
  restrictive umask is encountered (#74019)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 0.9.1-5
- lib64'ize

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon May 13 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.1-2
- Build for newer python

* Wed Mar 13 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.1-1
- 0.9.1

* Tue Feb 26 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.0-6
- Rebuild

* Thu Jan 31 2002 Elliot Lee <sopwith@redhat.com> 0.9.0-5
- Change python conflicts to requires
- Use pybasever/pynextver macros.

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri Sep 14 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.9.0-3
- Build for Python 2.2

* Mon Jul 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add zlib-devel to buildrequires (#49788)

* Tue Jun 19 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Initial build
