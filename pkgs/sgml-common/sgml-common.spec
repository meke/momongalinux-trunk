%global momorel 30
%global xmlxsdver 2009/01

Name: sgml-common
Version: 0.6.3
Release: %{momorel}m%{?dist}
Group: Applications/Text

Summary: Common SGML catalog and DTD files

License: GPL

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: ftp://sources.redhat.com/pub/docbook-tools/new-trials/SOURCES/%{name}-%{version}.tgz
NoSource: 0
Source1: xml.dcl
Source2: xml.soc
Source3: html.dcl
Source4: html.soc
Source5: http://www.w3.org/%{xmlxsdver}/xml.xsd
Patch0: sgml-common-umask.patch
Patch1: sgml-common-xmldir.patch
Patch2: sgml-common-quotes.patch

Requires: sh-utils fileutils textutils grep
BuildRequires: libxml2 >= 2.4.8-2
BuildRequires: automake
BuildRequires: autoconf

%description
The sgml-common package contains a collection of entities and DTDs
that are useful for processing SGML, but that don't need to be
included in multiple packages.  Sgml-common also includes an
up-to-date Open Catalog file.

%package -n xml-common
Group: Applications/Text
Summary: Common XML catalog and DTD files.
License: GPL
URL: http://www.iso.ch/cate/3524030.html
Requires: sh-utils fileutils textutils grep

%description -n xml-common
The xml-common package contains a collection of entities and DTDs
that are useful for processing XML, but that don't need to be
included in multiple packages.


%prep
%setup -q
%patch0 -p1 -b .umask
%patch1 -p1 -b .xmldir
%patch2 -p1 -b .quotes

# replace bogus links with files
for file in COPYING INSTALL install-sh missing mkinstalldirs; do 
   rm $file
   cp -p /usr/share/automake-1.4/$file .
done


%build
%configure 

%install
DESTDIR=$RPM_BUILD_ROOT
rm -rf $DESTDIR
make install DESTDIR="$RPM_BUILD_ROOT" htmldir='%{_datadir}/doc' INSTALL='install -p'
cp %{SOURCE1} CHANGES
mkdir $RPM_BUILD_ROOT/etc/xml
mkdir $RPM_BUILD_ROOT/usr/share/sgml/docbook
# Create an empty XML catalog.
XMLCATALOG=$RPM_BUILD_ROOT/etc/xml/catalog
/usr/bin/xmlcatalog --noout --create $XMLCATALOG
# ...and add xml.xsd in it
for type in system uri ; do
        for path in 2001 %{xmlxsdver} ; do
                %{_bindir}/xmlcatalog --noout --add $type \
                        "http://www.w3.org/$path/xml.xsd" \
                        "file://%{_datadir}/xml/xml.xsd" $XMLCATALOG
        done
done
# Now put the common DocBook entries in it
/usr/bin/xmlcatalog --noout --add "delegatePublic" \
	"-//OASIS//ENTITIES DocBook XML" \
	"file:///usr/share/sgml/docbook/xmlcatalog" $XMLCATALOG
/usr/bin/xmlcatalog --noout --add "delegatePublic" \
	"-//OASIS//DTD DocBook XML" \
	"file:///usr/share/sgml/docbook/xmlcatalog" $XMLCATALOG
/usr/bin/xmlcatalog --noout --add "delegatePublic" \
	"ISO 8879:1986" \
	"file:///usr/share/sgml/docbook/xmlcatalog" $XMLCATALOG
/usr/bin/xmlcatalog --noout --add "delegateSystem" \
	"http://www.oasis-open.org/docbook/" \
	"file:///usr/share/sgml/docbook/xmlcatalog" $XMLCATALOG
/usr/bin/xmlcatalog --noout --add "delegateURI" \
	"http://www.oasis-open.org/docbook/" \
	"file:///usr/share/sgml/docbook/xmlcatalog" $XMLCATALOG
# Also create the common DocBook catalog
/usr/bin/xmlcatalog --noout --create \
	$RPM_BUILD_ROOT/usr/share/sgml/docbook/xmlcatalog

rm -f $RPM_BUILD_ROOT%{_datadir}/sgml/xml.dcl
install -p -m0644 %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} \
        $RPM_BUILD_ROOT%{_datadir}/sgml
rm -rf $RPM_BUILD_ROOT%{_datadir}/xml/*
install -p -m0644 %{SOURCE5} $RPM_BUILD_ROOT%{_datadir}/xml

# remove installed doc file and prepare installation with %%doc
rm $RPM_BUILD_ROOT%{_datadir}/doc/*.html
rm -rf __dist_doc/html/
mkdir -p __dist_doc/html/
cp -p doc/HTML/*.html __dist_doc/html/

%clean
DESTDIR=$RPM_BUILD_ROOT
rm -rf $DESTDIR

%pre -n xml-common
if [ $1 -gt 1 ] && [ -e %{_sysconfdir}/xml/catalog ]; then
        for type in system uri ; do
                for path in 2001 %{xmlxsdver} ; do
                        %{_bindir}/xmlcatalog --noout --add $type \
                                "http://www.w3.org/$path/xml.xsd" \
                                "file://%{_datadir}/xml/xml.xsd" \
                                %{_sysconfdir}/xml/catalog
                done
        done
fi

%files
%defattr (-,root,root)
%doc __dist_doc/html/ AUTHORS NEWS ChangeLog README
%dir %{_sysconfdir}/sgml
%config(noreplace) %{_sysconfdir}/sgml/sgml.conf
%dir %{_datadir}/sgml
%dir %{_datadir}/sgml/sgml-iso-entities-8879.1986
%{_datadir}/sgml/sgml-iso-entities-8879.1986/*
%{_datadir}/sgml/xml.dcl
%{_datadir}/sgml/xml.soc
%{_datadir}/sgml/html.dcl
%{_datadir}/sgml/html.soc
%{_bindir}/sgmlwhich
%{_bindir}/install-catalog
%{_mandir}/man8/install-catalog.8*

%files -n xml-common
%defattr (-,root,root)
%dir %{_sysconfdir}/xml
%config(noreplace) %{_sysconfdir}/xml/catalog
#%%dir %{_datadir}/sgml
%dir %{_datadir}/sgml/docbook
%config(noreplace) %{_datadir}/sgml/docbook/xmlcatalog
%dir %{_datadir}/xml
%{_datadir}/xml/xml.xsd

%changelog
* Mon Sep 16 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-30m)
- add any files

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-27m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-25m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-24m)
- update Patch0 for fuzz=0

* Sun Aug 31 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-23m)
- fix %%files

* Sat Apr  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-22m)
- fix xmlcatalog corruption

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.3-21m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-21m)
- import fedora patch

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-20m)
- modify %%files

* Mon May 21 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.3-19m)
- sync Fedora

* Wed Apr 18 2007 zunda <zunda at freeshell.org>
- (kossori)
- Modified to run autoconf in prep. Otherwise, @mkdir_p@ is not converted.

* Fri Oct 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-18m)
- specify automake version 1.9

* Sat Jul  1 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.3-17m)
- revise %%files to avoid conflicting with  openjade

* Wed Nov 17 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.6.3-16m)
- sync with Fedora (0.6.3-17)

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.3-15m)
- revised spec for rpm 4.2.

* Sat Mar 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.6.3-14k)
- fix %post: create /etc/xml/catalog if not exist
- fix %preun: add catalog filename

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (0.6.3-12k)
- remove docbook, xhtml catalog

* Tue Jan 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.6.3-10K)
- add xhtml support

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.6.3-8k)
- nigittenu

* Fri Oct  5 2001 Tim Waugh <twaugh@redhat.com> 0.6.3-2
- Move XML things into /usr/share/xml, and split them out into separate
  xml-common package.

* Mon Oct  1 2001 Tim Waugh <twaugh@redhat.com> 0.6.3-1
- 0.6.3.  Incorporates oldsyntax and quiet patches.
- Make /etc/sgml/sgml.conf noreplace.
- Own /etc/sgml, various other directories (bug #47485, bug #54180).

* Wed May 23 2001 Tim Waugh <twaugh@redhat.com> 0.5-7
- Remove execute bit from data files.

* Mon May 21 2001 Tim Waugh <twaugh@redhat.com> 0.5-6
- install-catalog needs to make sure that it creates world-readable files
  (bug #41552).

* Wed Jan 24 2001 Tim Waugh <twaugh@redhat.com>
- Make install-catalog quieter during normal operation.

* Tue Jan 23 2001 Tim Waugh <twaugh@redhat.com>
- Require textutils, fileutils, grep (bug #24719).

* Wed Jan 17 2001 Tim Waugh <twaugh@redhat.com>
- Require sh-utils.

* Mon Jan 15 2001 Tim Waugh <twaugh@redhat.com>
- Don't play so many macro games.
- Fix typo in install-catalog patch.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Change group.
- Install by hand (man/en/...).  Use %%{_mandir}.
- Use %%{_tmppath}.
- Make install-catalog fail silently if given the old syntax.
- Add CHANGES file.
- Change Copyright: to License:.
- Remove Packager: line.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Based on Eric Bischoff's new-trials packages.
