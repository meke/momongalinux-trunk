%global momorel 13

%global xfce4ver 4.8.0
%global major 0.0

Summary:	Xfce panel plugin for open or close a CD-ROM drive tray
Name:		xfce4-cddrive-plugin
Version:	0.0.1
Release:	%{momorel}m%{?dist}

Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
#Source1:	%{name}.desktop
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext
BuildRequires:  libcdio-devel >= 0.82
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
xfce4-cddrive-plugin puts a button in the XFCE panel which allow to open
or close a CD-ROM drive tray.
Additionally, the button icon reports the content of the drive.
The icon indicates if there is a disc in the drive, if it is mounted,
and the type of the disc (regular CD-ROM, audio or DVD).
You can also mount or unmount the disc via the plugin's menu (the action appears
when a disc is in the drive).

%prep
%setup -q

%build
%configure --program-prefix=""
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS TODO
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/locale/*/*/*

%changelog
* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-13m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-12m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-9m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-8m)
- rebuild against xfce4-4.6.2

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.1-7m)
- rebuild against libcdio-0.82

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-5m)
- rebuild against libcdio-0.81

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-4m)
- rebuild against xfce4-4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-1m)
- initial Momonga  package
