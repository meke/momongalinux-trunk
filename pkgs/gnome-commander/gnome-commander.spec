%global momorel 10

Summary:   powerful twin-panel file manager
Name:      gnome-commander
Version:   1.2.8.14
Release:   %{momorel}m%{?dist}
License:   GPL
Group:     Applications/Internet
Source0:   http://ftp.gnome.org/pub/GNOME/sources/%{name}/1.2/%{name}-%{version}.tar.bz2
NoSource:  0
Patch0:    %{name}-%{version}-poppler-0.24.3.patch

URL:       http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: libgnome-devel
BuildRequires: libgnomeui-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: exiv2-devel >= 0.23
BuildRequires: taglib-devel
BuildRequires: libgsf-devel
BuildRequires: poppler-devel >= 0.24.3

%description
GNOME Commander is a fast and powerful twin-panel file manager for the GNOME desktop.

%prep
%setup -q

%patch0 -p1 -b .poppler020

%build
%configure \
	--disable-static \
	--enable-python
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS BUGS COPYING ChangeLog* NEWS README README.commits TODO
%{_bindir}/%{name}
%{_bindir}/gcmd-block
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/gnome/help/%{name}
%{_datadir}/omf/%{name}
%{_mandir}/man1/%{name}.1.*

%changelog
* Fri Nov  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.14-10m)
- rebuild against poppler-0.24.3

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.14-9m)
- rebuild against poppler-0.22.3

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.8.14-8m)
- rebuild against poppler-0.22.0

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.14-7m)
- rebuild against poppler-0.20.5

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.14-6m)
- rebuild against exiv2-0.23

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.8.14-5m)
- rebuild against poppler-0.20.1, use a patch from ubuntu

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.8.14-4m)
- rebuild for glib 2.33.2

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.14-3m)
- rebuild against poppler-0.18.2

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.14-2m)
- rebuild against exiv2-0.22

* Thu Oct  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.8.14-1m)
- update to 1.2.8.14

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.8.13-2m)
- rebuild against poppler-0.18.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.8.13-1m)
- update to 1.2.8.13

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.8.12-1m)
- update to 1.2.8.12

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.8.11-1m)
- initial build

