%global momorel 7

Name:      geany
Version:   0.19
Release:   %{momorel}m%{?dist}
Summary:   A fast and lightweight IDE using GTK2

Group:     Development/Tools
License:   GPLv2+
URL:       http://geany.uvena.de/
Source0:   http://download.geany.org/%{name}-%{version}.tar.bz2
NoSource:  0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: desktop-file-utils, gettext, gtk2-devel, glib2-devel, pango-devel, intltool
BuildRequires: perl-XML-Parser
Requires: vte3

%define geany_docdir $RPM_BUILD_ROOT%{_datadir}/doc/%{name}-%{version}

%description
Geany is a small and fast editor with basic features of an 
integrated development environment.

Some features:
- syntax highlighting
- code completion
- code folding
- call tips
- folding
- many supported filetypes like C, Java, PHP, HTML, Python, Perl, Pascal
- symbol lists

%package devel
Summary:   Header files for building Geany plug-ins
Group:     Development/Tools
Requires:  geany = %{version}-%{release}

%description devel
This package contains the header files and pkg-config file needed for building
Geany plug-ins. You do not need to install this package to use Geany.

%prep
%setup -q

%build
autoreconf -fiv
%configure --docdir=%{_docdir}/%{name}-%{version} LIBS=-lgmodule-2.0
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT DOCDIR=%{geany_docdir}
mkdir $RPM_BUILD_ROOT%{_datadir}/pixmaps
ln -s %{_datadir}/icons/hicolor/48x48/apps/%{name}.png $RPM_BUILD_ROOT%{_datadir}/pixmaps/%{name}.png
desktop-file-install --delete-original --vendor=""              \
        --dir=${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --mode 0644                                             \
        $RPM_BUILD_ROOT/%{_datadir}/applications/%{name}.desktop
sed -i 's/\r//' %{geany_docdir}/ScintillaLicense.txt
%find_lang %{name}

# Remove static library *.la files
rm -rf $RPM_BUILD_ROOT%{_libdir}/geany/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-, root, root, -)
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/*.*
%{_datadir}/doc/%{name}-%{version}
%{_mandir}/man1/geany.1.*

%files devel
%defattr(-, root, root, -)
%{_includedir}/geany
%{_libdir}/pkgconfig/geany.pc

%changelog
* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-7m)
- change Requires from vte to vte3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-6m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-8m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.19-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.17-2m)
- add autoreconf (build fix)

* Sun May  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-1m)
- update

* Mon Feb 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15-1m)
- import from Fedora-devel

* Sun Oct 26 2008 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.15-1
- Update to 0.15
- Update URL
- Add intltool to BuildRequires

* Sun May 11 2008 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.14-1
- Update to 0.14
- New -devel sub-package for header files
- Corectly remove the .la libtool files
- Remove hack relating to finding the system installed html files
- No longer correct the desktop file

* Mon Mar 24 2008 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.13-2
- Fix docdir/doc_dir so geany correctly finds the system installed html docs (BZ
  438534) 

* Sun Feb 24 2008 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.13-1
- Update to version 0.13

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.12-5
- Autorebuild for GCC 4.3

* Thu Oct 18 2007 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.12-4
- Fix license tag
- Package new library files
- Remove static library .la files
- Package new icons

* Thu Oct 18 2007 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.12-3
- Fix Version entry in .desktop file again

* Thu Oct 18 2007 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.12-2
- Add a BuildRequires for perl(XML::Parser)

* Thu Oct 18 2007 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.12-1
- Update to version 0.12

* Sun Sep  9 2007 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.11-2
- Fix Version entry in .desktop file

* Sun Sep  9 2007 Jonathan G. Underwood <jonathan.underwood@gmail.com> - 0.11-1
- Update to version 0.11

* Fri Feb 23 2007 Josef Whiter <josef@toxicpanda.com> 0.10.1-1
- updating to 0.10.1 of geany

* Thu Jan 25 2007 Josef Whiter <josef@toxicpanda.com> 0.10-5
- removed autoconf/automake/vte-devel from BR as they are not needed
- removed patch to dynamically link in libvte
- adding patch to find appropriate libvte library from the vte package
- added vte as a Requires

* Wed Jan 24 2007 Josef Whiter <josef@toxicpanda.com> 0.10-4
- added autoconf and automake as a BR

* Wed Jan 24 2007 Josef Whiter <josef@toxicpanda.com> 0.10-3
- adding patch to dynamically link in libvte instead of using g_module_open

* Tue Jan 04 2007 Josef Whiter <josef@toxicpanda.com> 0.10-2
- Fixed mixed spaces/tabs problem
- added sed command to install to fix the ScintillaLicense.txt eol encoding
- fixed the docs so they are installed into the right place
- added an rm pixmaps/geany.ico, its only for windows installations

* Thu Dec 28 2006 Josef Whiter <josef@toxicpanda.com> 0.10-1
- Initial Release
