%global momorel 1
%global qt_module qtscript

Summary: Qt5 - QtScript component
Name:    qt5-%{qt_module}
Version: 5.2.1
Release: %{momorel}m%{?dist}

# See LGPL_EXCEPTIONS.txt, LICENSE.GPL3, respectively, for exception details
License: "LGPLv2 with exceptions or GPLv3 with exceptions"
Group: System Environment/Libraries
Url: http://qt-project.org/
Source0: http://download.qt-project.org/official_releases/qt/5.2/%{version}/submodules/%{qt_module}-opensource-src-%{version}.tar.xz
NoSource: 0
# add s390(x0 support to Platform.h (taken from webkit)
Patch0: qtscript-opensource-src-5.2.0-s390.patch
## upstream patches
# https://codereview.qt-project.org/#change,74927
Patch101: qtscript-opensource-src-5.2.0-aarch64.patch 

# http://bugzilla.redhat.com/1005482
ExcludeArch: ppc64 ppc

BuildRequires: qt5-qtbase-devel >= %{version}

%{?_qt5_version:Requires: qt5-qtbase%{?_isa} >= %{_qt5_version}}

%description
%{summary}.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: qt5-qtbase-devel%{?_isa}

%description devel
%{summary}.

%package doc
Summary: API documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
%{summary}.

%package examples
Summary: Programming examples for %{name}
Group: Documentation
Requires: %{name}%{?_isa} = %{version}-%{release}

%description examples
%{summary}.

%prep
%setup -q -n %{qt_module}-opensource-src-%{version}%{?pre:-%{pre}}
%patch0 -p1 -b .s390
%patch101 -p1 -b .aarch64

%build
%{_qt5_qmake}

make %{?_smp_mflags}

make %{?_smp_mflags} docs

%install
rm -rf --preserve-root %{buildroot}
make install INSTALL_ROOT=%{buildroot}

make install_docs INSTALL_ROOT=%{buildroot}

## .prl file love (maybe consider just deleting these -- rex
# nuke dangling reference(s) to %%buildroot, excessive (.la-like) libs
sed -i \
  -e "/^QMAKE_PRL_BUILD_DIR/d" \
  -e "/^QMAKE_PRL_LIBS/d" \
  %{buildroot}%{_qt5_libdir}/*.prl

## unpackaged files
# .la files, die, die, die.
rm -fv %{buildroot}%{_qt5_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LGPL_EXCEPTION.txt LICENSE.GPL  LICENSE.LGPL
%{_qt5_libdir}/libQt5Script.so.5*
%{_qt5_libdir}/libQt5ScriptTools.so.5*

%files devel
%defattr(-,root,root,-)
%{_qt5_headerdir}/Qt*
%{_qt5_libdir}/libQt5*.so
%{_qt5_libdir}/libQt5*.prl
%{_qt5_libdir}/cmake/Qt5*
%{_qt5_libdir}/pkgconfig/Qt5*.pc
%{_qt5_archdatadir}/mkspecs/modules/*.pri

%files doc
%defattr(-,root,root,-)
%{_qt5_docdir}/qtscript.qch
%{_qt5_docdir}/qtscript/
%{_qt5_docdir}/qtscripttools.qch
%{_qt5_docdir}/qtscripttools/

%if 0%{?_qt5_examplesdir:1}
%files examples
%defattr(-,root,root,-)
%{_qt5_examplesdir}/
%endif

%changelog
* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1

* Wed Sep 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.1-1m)
- import from Fedora

* Wed Aug 28 2013 Rex Dieter <rdieter@fedoraproject.org> 5.1.1-1
- 5.1.1

* Wed Aug 28 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-2
- update Source URL
- %%doc LGPL_EXCEPTION.txt LICENSE.GPL LICENSE.LGPL

* Thu Apr 11 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-1
- 5.0.2

* Sat Feb 23 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.1-1
- first try

