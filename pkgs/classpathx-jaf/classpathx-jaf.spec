%global momorel 13
%global with_javadoc %{?java_bootstrap1:0}%{!?java_bootstrap1:1}

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _gcj_support 0

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define section free
%define jafver  1.0.2

Name:           classpathx-jaf
Version:        1.0
Release:        9jpp.%{momorel}m%{?dist}
Epoch:          0
Summary:        GNU JavaBeans(tm) Activation Framework


Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.gnu.org/software/classpathx/
Source0:        http://ftp.gnu.org/gnu/classpathx/activation-1.0.tar.gz
Source1:        http://ftp.gnu.org/gnu/classpathx/activation-1.0.tar.gz.sig
Patch0:		classpathx-jaf-MimeType.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if ! %{gcj_support}
BuildArch:      noarch
%endif
Requires(preun):  chkconfig
Requires(post):  chkconfig
BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  make
BuildRequires:  ant
Provides:       jaf = 0:%{jafver}
Obsoletes:      gnujaf <= 0:1.0-0.rc1.1jpp

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
JAF provides a means to type data and locate components suitable for
performing various kinds of action on it. It extends the UNIX standard
mime.types and mailcap mechanisms for Java.

%if %{with_javadoc}
%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Provides:       jaf-javadoc = 0:%{jafver}
Obsoletes:      gnujaf-javadoc <= 0:1.0-0.rc1.1jpp
BuildRequires:  java-javadoc
Requires(post):		coreutils
Requires(postun):	coreutils

%description    javadoc
%{summary}.
%endif


%prep
%setup -q -n activation-%{version}

%patch0 -p0

%build
%configure
%ant 
%if %{with_javadoc}
make javadoc JAVADOCFLAGS="-link %{_javadocdir}/java"
%endif

%install
rm -rf $RPM_BUILD_ROOT

%{__make} install DESTDIR=$RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
cd $RPM_BUILD_ROOT%{_javadir}
mv activation.jar %{name}-%{version}.jar
ln -s %{name}-%{version}.jar %{name}.jar
ln -s %{name}-%{version}.jar jaf-%{jafver}.jar
ln -s %{name}-%{version}.jar activation.jar
#touch jaf.jar
cd -
%if %{with_javadoc}
install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/jaf-%{jafver}
cp -pR docs/* $RPM_BUILD_ROOT%{_javadocdir}/jaf-%{jafver}
ln -s jaf-%{jafver} $RPM_BUILD_ROOT%{_javadocdir}/jaf
%endif

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%triggerpostun -- classpathx-jaf <= 0:1.0-7jpp_5fc
# Remove file from old non-free packages
rm -f %{_javadir}/jaf.jar
# Recreate the link as update-alternatives could not do it
ln -s %{_sysconfdir}/alternatives/jaf %{_javadir}/jaf.jar

%post
/usr/sbin/update-alternatives --install %{_javadir}/jaf.jar jaf %{_javadir}/%{name}.jar 10002

%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%preun
if [ "$1" = "0" ]; then
    /usr/sbin/update-alternatives --remove jaf %{_javadir}/%{name}.jar
fi


%if %{with_javadoc}
%post javadoc
rm -f %{_javadocdir}/jaf
ln -s jaf-%{jafver} %{_javadocdir}/jaf

%postun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/jaf
fi
%endif

%files
%defattr(644,root,root,755)
%doc AUTHORS ChangeLog COPYING
%{_javadir}/activation.jar
%{_javadir}/jaf-%{jafver}.jar
#%ghost %{_javadir}/jaf*.jar
%{_javadir}/%{name}*.jar

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/classpathx-jaf-1.0.jar.*
%dir %{_libdir}/gcj/%{name}
%endif

%if %{with_javadoc}
%files javadoc
%defattr(644,root,root,755)
%doc %{_javadocdir}/jaf-%{jafver}
%ghost %doc %{_javadocdir}/jaf
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9jpp.13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9jpp.12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-9jpp.11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9jpp.10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.0-9jpp.9m)
- build with OpenJDK

* Sun May 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9jpp.8m)
- make javadoc

* Sun May 10 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9jpp.7m)
- switch to use %%ant

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9jpp.6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9jpp.5m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-9jpp.4m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-9jpp.3m)
- modify Requires

* Wed Jun  6 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9jpp.2m)
- Added with_javadoc option
- Revised spec for bootstrapping.

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-9jpp.1m)
- import from Fedora

* Fri Aug 18 2006 Vivek Lakshmanan <vivekl@redhat.com> - 0:1.0-9jpp.1
- Resync with latest from JPP.

* Fri Aug 11 2006 Vivek Lakshmanan <vivekl@redhat.com> - 0:1.0-8jpp.1
- Replace explicit Requires(post) on coreutils with 
  Requires-post/postun on rm and ln since more portable with JPP.
- Revert explicit install of versionless jaf.jar install since
  alternatives handles this.
- Extend triggerpostun to trigger on <= 0:1.0-7jpp_5fc so jaf.jar
  exists when upgrading from -7jpp_5fc.

* Thu Aug 10 2006 Karsten Hopp <karsten@redhat.de> 1.0-7jpp_5fc
- Requires(post):     coreutils

* Sun Jul 23 2006 Thomas Fitzsimmons <fitzsim@redhat.com> - 0:1.0-7jpp_4fc
- Install versionless jaf.jar symlink.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> 0:1.0-7jpp_3fc
- Rebuilt

* Thu Jul 20 2006 Vivek Lakshmanan <vivekl@redhat.com> 0:1.0-7jpp_2fc
- Rebuild.

* Tue Jul 18 2006 Vivek Lakshmanan <vivekl@redhat.com> 0:1.0-7jpp_1fc
- Add conditional native compilation with GCJ.
- Merge with latest from JPP.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - sh: line 0: fg: no job control
- rebuild

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:1.0-2jpp_5fc
- stop scriptlet spew

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> 0:1.0-2jpp_4fc
- rebuilt

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jan 11 2005 Gary Benson <gbenson@redhat.com> 0:1.0-2jpp_3fc
- Sync with RHAPS.

* Mon Nov  1 2004 Gary Benson <gbenson@redhat.com> 0:1.0-2jpp_2fc
- Build into Fedora.

* Thu Oct 28 2004 Fernando Nasser <fnasser@redhat.com> 0:1.0-2jpp_1rh
- First Red Hat build

* Fri Aug 20 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0-2jpp
- Pro forma rebuild with ant-1.6.2 present

* Tue Jun 15 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-1jpp
- Update to 1.0.

* Fri Jun 11 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.rc1.2jpp
- Bump release to work around bug in rpm <https://bugzilla.redhat.com/116299>.

* Thu Jun 10 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.rc1.1jpp
- Rename gnujaf to classpathx-jaf.

* Fri May 28 2004 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.rc1.1jpp
- First build.
