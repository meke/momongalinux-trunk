%global         momorel 1
%global         qtver 4.7.3
%global         qtsoapver 2.7

Name:           herqq
Version:        1.0.0
Release:        %{momorel}m%{?dist}
Summary:        A software library for building UPnP devices and control points
Group:          System Environment/Libraries
# test application is GPLv3 but we do not ship it
License:        LGPLv3+
URL:            http://herqq.org/
Source0:        http://downloads.sourceforge.net/project/hupnp/hupnp/%{name}-%{version}.zip
NoSource:       0
Patch2:         herqq-1.0.0-qtsoap-library.patch
BuildRequires:  desktop-file-utils
BuildRequires:  qt-devel
BuildRequires:  qtsoap-devel

%description
Herqq UPnP (HUPnP) is a software library for building UPnP 
devices and control points conforming to the UPnP Device 
Architecture version 1.1. 

%package devel
Summary:  Development files for %{name}
Group:    Development/Languages
Requires: %{name} = %{version}-%{release}
%description devel
Header files for developing applications using %{name}.

%prep
%setup -q

# lQtSolutions to lqtsoap
%patch2 -p1 -b .qtsoap-library

%build
# we have to disable bundled QtSOAP library
qmake-qt4 PREFIX=%{_prefix} -config DISABLE_QTSOAP \
  -config DISABLE_TESTAPP -config USE_QT_INSTALL_LOC
make %{?_smp_mflags}

%install
make INSTALL_ROOT=%{buildroot} install


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%doc hupnp/ChangeLog hupnp/LICENSE_LGPLv3.txt
%{_qt4_libdir}/libHUpnp.so.1*

%files devel
%{_qt4_libdir}/libHUpnp.so
%{_qt4_headerdir}/HUpnpCore/

%changelog
* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- import from Fedora

* Wed Jul 20 2011 Jaroslav Reznik <jreznik@redhat.com> - 1.0.0-1
- post-review update to 1.0.0

* Wed Jul 20 2011 Jaroslav Reznik <jreznik@redhat.com> - 0.9.1-3
- fix license to LGPLv3+
- qt4 header dir for consistency
- shlib soname tracked in %files
- -devel should not duplicate COPYING

* Tue Jul 19 2011 Jaroslav Reznik <jreznik@redhat.com> - 0.9.1-2
- qtsoap library
- cleanup SPEC file

* Tue Feb 22 2011 Jaroslav Reznik <jreznik@redhat.com> - 0.9.1-1
- Initial spec file 
