%global momorel 1

Name:           apache-ivy
Version:        2.2.0
Release:        %{momorel}m%{?dist}
Summary:        Java-based dependency manager

Group:          Development/Tools
License:        ASL 2.0
URL:            http://ant.apache.org/ivy/
Source0:        http://www.apache.org/dist/ant/ivy/%{version}/%{name}-%{version}-src.tar.gz
NoSource:       0
BuildArch:      noarch

Provides:       ivy = %{version}-%{release}

BuildRequires:  ant
BuildRequires:  jakarta-commons-httpclient
BuildRequires:  jsch
BuildRequires:  jakarta-oro
BuildRequires:  java-devel >= 1.5
BuildRequires:  jpackage-utils
Requires:       jpackage-utils
Requires:       jakarta-oro
Requires:       jsch
Requires:       ant
Requires:       jakarta-commons-httpclient

%description
Apache Ivy is a tool for managing (recording, tracking, resolving and
reporting) project dependencies.  It is designed as process agnostic and is
not tied to any methodology or structure. while available as a standalone
tool, Apache Ivy works particularly well with Apache Ant providing a number
of powerful Ant tasks ranging from dependency resolution to dependency
reporting and publication.

%package javadoc
Summary:        API Documentation for ivy
Group:          Development/Tools
Requires:       %{name} = %{version}-%{release}
Requires:       jpackage-utils

%description javadoc
JavaDoc documentation for %{name}

%prep
%setup -q

# Fix messed-up encodings
for F in RELEASE_NOTES README LICENSE NOTICE CHANGES.txt
do
        sed 's/\r//' $F |iconv -f iso8859-1 -t utf8 >$F.utf8
        touch -r $F $F.utf8
        mv $F.utf8 $F
done
rm -fr src/java/org/apache/ivy/plugins/signer/bouncycastle

%build
# Remove prebuilt documentation
rm -rf doc build/doc

# How to properly disable a plugin?
# we disable vfs plugin since commons-vfs is not available
rm -rf src/java/org/apache/ivy/plugins/repository/vfs \
        src/java/org/apache/ivy/plugins/resolver/VfsResolver.java
sed '/vfs.*=.*org.apache.ivy.plugins.resolver.VfsResolver/d' -i \
        src/java/org/apache/ivy/core/settings/typedef.properties

# Craft class path
mkdir -p lib
build-jar-repository lib ant jakarta-commons-httpclient jakarta-oro jsch 

# Build
ant /localivy /offline jar javadoc


%install
# Code
install -d $RPM_BUILD_ROOT%{_javadir}
install -p -m644 build/artifact/jars/ivy.jar $RPM_BUILD_ROOT%{_javadir}/ivy.jar

# API Documentation
install -d $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -rp build/doc/reports/api/. $RPM_BUILD_ROOT%{_javadocdir}/%{name}

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/ant.d
echo "ivy" > $RPM_BUILD_ROOT%{_sysconfdir}/ant.d/%{name}

%files
%{_javadir}/*
%{_sysconfdir}/ant.d/%{name}
%doc RELEASE_NOTES CHANGES.txt LICENSE NOTICE README

%files javadoc
%{_javadocdir}/*
%doc LICENSE

%changelog
* Sun Jan  8 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-1m)
- import from Fedora

* Wed Jul 6 2011 Alexander Kurtakov <akurtako@redhat.com> 2.2.0-2
- Fix ant integration.

* Fri Feb 25 2011 Alexander Kurtakov <akurtako@redhat.com> 2.2.0-1
- Update to 2.2.0.

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Nov 09 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.1.0-1
- Initial Fedora packaging
