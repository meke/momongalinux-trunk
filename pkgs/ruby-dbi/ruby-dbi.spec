%global momorel 4

Summary: a database independent interface for accessing databases
Name: ruby-dbi
Version: 0.4.3
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
Source0: http://rubyforge.org/frs/download.php/63601/dbi-%{version}.tar.gz
NoSource: 0
URL: http://ruby-dbi.rubyforge.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby-devel >= 1.9.2

%description
Ruby/DBI - a database independent interface for accessing databases -
similar to Perl's DBI

Note that you will need ruby-mysql, ruby-postgres or something for your
favorite database driver.

%package sqlite
Summary: SQLite interface for ruby-dbi
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description sqlite
SQLite interface for ruby-dbi

%prep

%setup -q -n dbi-%{version}

%build
ruby setup.rb config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_sitelibdir} \
    --so-dir=%{buildroot}%{ruby_sitearchdir}
ruby setup.rb setup

%install
rm -rf %{buildroot}
ruby setup.rb install

%clean
rm -rf %{buildroot}

%files 
%defattr(- ,root,root)
%doc ChangeLog LICENSE README examples test
%{_bindir}/*
%{ruby_sitelibdir}/dbi
%{ruby_sitelibdir}/dbi.rb

%files sqlite
%defattr(- ,root,root)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.3-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-1m)
- update 0.4.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.23-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.23-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.23-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.23-5m)
- rebuild against gcc43

* Thu Oct 25 2007 zunda <zunda at freeshell.org>
- (kossori)
- modified BuildPreReq from sqlite-devel to sqlite2-devel.
  extconf.rb requires libsqlite instead of libsqlite3.

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.23-4m)
- rebuild against ruby-1.8.6-4m

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.23-3m)
- rebuild against ruby-1.8.2

* Wed May 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.23-1m)
- version up

* Sun Dec 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.21-2m)
- add sqlite sub package

* Fri Dec 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.21-1m)
- initial import to Momonga
