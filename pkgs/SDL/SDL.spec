%global momorel 1

Summary: Simple DirectMedia Layer
Name: SDL
Version: 1.2.15
Release: %{momorel}m%{?dist}
Source0: http://www.libsdl.org/release/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: SDL_config.h
# Temporary hack: Use pulseaudio via esd so SDL apps don't lock up when they
# attempt to use ALSA via pulseaudio.  This will be removed when...
# 1) SDL's pulseaudio support is working natively without problems
# 2) it conditionally switches to pulseaudio or ALSA automatically
Source2: SDL_pulseaudio_hack.csh
Source3: SDL_pulseaudio_hack.sh
Patch1: SDL-1.2.12-multilib.patch
Patch4: SDL-1.2.14-fix-_XGetRequest-error.patch
URL: http://www.libsdl.org/
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool
%ifarch %{ix86}
BuildRequires: nasm
%endif
BuildRequires: esound-devel >= 0.2.29-2m, alsa-lib-devel >= 1.0.13-2m
BuildRequires: arts-devel
BuildRequires: libX11-devel, libXext-devel
BuildRequires: audiofile-devel >= 0.2.1
BuildRequires: pkgconfig
Obsoletes: aalib

%description
This is the Simple DirectMedia Layer, a generic API that provides low
level access to audio, keyboard, mouse, and display framebuffer across
multiple platforms.

%package devel
Summary: Libraries, includes and more to develop SDL applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This is the Simple DirectMedia Layer, a generic API that provides low
level access to audio, keyboard, mouse, and display framebuffer across
multiple platforms.

This is the libraries, include files and other resources you can use
to develop SDL applications.

%package static
Summary: Files needed to develop static Simple DirectMedia Layer applications
Group: Development/Libraries
Requires: SDL-devel = %{version}-%{release}

%description static
Simple DirectMedia Layer (SDL) is a cross-platform multimedia library
designed to provide fast access to the graphics frame buffer and audio
device. This package provides the static libraries needed for developing
static SDL applications.

%prep
%setup -q
%patch1 -p1 -b .multilib
%patch4 -p1 -b .fix-_XGetRequest-error~

%build
CFLAGS="$RPM_OPT_FLAGS -O3" CXXFLAGS="$RPM_OPT_FLAGS -O3" \
%configure \
   --disable-video-svga --disable-video-ggi --disable-video-aalib \
   --disable-debug \
   --enable-sdl-dlopen \
   --enable-dlopen \
   --enable-arts-shared \
   --enable-esd-shared \
   --enable-pulseaudio-shared \
   --enable-alsa \
   --disable-rpath

%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
export tagname=CC
%makeinstall transform='s,x,x,'

# Rename SDL_config.h to SDL_config-<arch>.h to avoid file conflicts on
# multilib systems and install SDL_config.h wrapper
basearch=%{_arch}
# always use i386 for iX86
%ifarch %{ix86}
basearch=i386
%endif
# always use arm for arm*
%ifarch %{arm}
basearch=arm
%endif
# Rename SDL_config.h
mv %{buildroot}/%{_includedir}/SDL/SDL_config.h %{buildroot}/%{_includedir}/SDL/SDL_config-${basearch}.h
install -m644 %{SOURCE1} %{buildroot}/%{_includedir}/SDL/SDL_config.h

find %{buildroot} -name "*.la" -delete

# Temporary SDL pulseaudio hack
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc README-SDL.txt COPYING CREDITS BUGS
%{_libdir}/lib*.so.*
# Temporary SDL pulseaudio hack
%{_sysconfdir}/profile.d/*

%files devel
%defattr(-,root,root)
%doc README README-SDL.txt COPYING CREDITS BUGS WhatsNew docs.html
%doc docs/index.html docs/html
%{_bindir}/*-config
%{_libdir}/pkgconfig/sdl.pc
%{_libdir}/lib*.so
%{_includedir}/SDL
%{_mandir}/man3/*
%{_datadir}/aclocal/*

%files static
%defattr(-,root,root)
%{_libdir}/lib*.a

%changelog
* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15-1m)
- update to 1.2.15

* Tue Jan 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.14-6m)
- add "-lX11" for static linking

* Thu Jan 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.14-5m)
- add "-lX11" into sdl.pc and sdl-config

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.14-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.14-2m)
- full rebuild for mo7 release

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.14-1m)
- update to 1.2.14
- remove %%global _libtoolize :
- sync with Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.13-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.13-3m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.13-2m)
- remove %%{_libdir}/lib*.a from devel package
  (to avoid conflicting with static package)

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.13-1m)
- update to 1.2.13
- sync with Fedora
 
* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-4m)
- %%NoSource -> NoSource

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.11-2m)
- delete libtool library

* Wed Dec  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.11-1m)
- update to 1.2.11 for xine-lib-1.1.3, xine-lib needs sdl.pc
- import SDL-1.2.11-no-yasm.patch from cooker
 +* Fri Jul 21 2006 Karlsen <pkarlsen@mandriva.com> 1.2.11-1mdv2007.0
 +- use nasm, not yasm as it will fail to build (P53 from debian)

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.10-1m)
- update to 1.2.10
- sync with Fedora Core (update patches)
- - * Mon May 22 2006 Thomas Woerner <twoerner@redhat.com> 1.2.10-1
- - - new version 1.2.10
- - - dropped the following patches because they are not needed anymore:
- -   ppc_modes, gcc4, yuv_mmx_gcc4 and no_exec_stack
- - - new pagesize patch (drop PAGE_SIZE, use sysconf(_SC_PAGESIZE) instead)
- PLEASE CHECK!!: Patch17(SDL-1.2.10-libdir.patch) seems to be for lib64, is that correct?

* Tue May  9 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.2.9-2m)
- revised lib64 patch

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.9-1m)
- update to 1.2.9
- import patch18 and patch19 and patch20 from FC
- delete patch4 and patch5 and patch14 and patch16

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.8-3m)
- add gcc4 patch.
- Patch200: SDL-1.2.8-gcc4.patch

* Mon Aug  8 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.8-2m)
- revised for x86_64 (disable patch8)

* Wed Jul 27 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8
- sync with FC (SDL-1.2.8-3.2.src.rpm) and import following patches
- - Patch0: SDL-1.1.7-byteorder.patch
- - Patch4: SDL-1.2.3-prefersounddaemons.patch
- - Patch5: SDL-1.2.3-c++.patch
- - Patch8: SDL-1.2.6-no_rpath.patch
- - Patch13: SDL-1.2.7-ppc_modes.patch
- - Patch14: SDL-1.2.8-autofoo.patch
- - Patch15: SDL-1.2.8-gcc4.patch
- - Patch16: SDL-1.2.8-x86_64.patch
- - Patch17: SDL-1.2.8-libdir.patch
- removed unused patches
- changelog in FC is as follows,
- - * Thu May 26 2005 Bill Nottingham <notting@redhat.com> 1.2.8-3.2
- - - fix configure script for libdir so library deps are identical on all
- -   arches (#158346)
- - 
- - * Thu Apr 14 2005 Thomas Woerner <twoerner@redhat.com> 1.2.8-3.1
- - - new version of the gcc4 fix
- - 
- - * Tue Apr 12 2005 Thomas Woerner <twoerner@redhat.com> 1.2.8-3
- - - fixed gcc4 compile problems
- - - fixed x86_64 endian problem
- - 
- - * Wed Feb  9 2005 Thomas Woerner <twoerner@redhat.com> 1.2.8-2
- - - rebuild
- - 
- - * Fri Dec 17 2004 Thomas Woerner <twoerner@redhat.com> 1.2.8-1
- - - new version 1.2.8

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.7-11m)
- disable aalib

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.7-10m)
- diable DirectFB

* Sun Feb  6 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.7-8m)
- enable x86_64. revised lib64 patch.

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.7-7m)
- enable x86_64.

* Tue Oct 19 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.7-6m)
- add SDL-1.2.7-add-DirectFB-libs.patch
- reported by docky [Momonga-devel.ja:02767] [momongaja:00071]

* Mon Oct  4 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.7-5m)
- add gcc34 patches

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.7-4m)
- build against DirectFB-0.9.21-0.20040828.1m

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.7-3m)
- revised spec for enabling rpm 4.2.

* Sat Mar  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.7-2m)
- kossori.

* Mon Feb 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.7-1m)
- update to 1.2.7

* Sat Dec 20 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.2.6-2m)
- add patch for ALSA-1.0.0

* Tue Sep  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.6-1m)
- revise SDL-directfb-0.9.16.patch for SDL-1.2.6
- remove SDL-1.2.5-multiline-string.patch which is merged into original source

* Fri Jul 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.5-4m)
- fix multiline string

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.5-3m)
  rebuild against DirectFB 0.9.18

* Sat Jan 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.5-2m)
- add patch for DirectFB-0.9.16

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.5-1m)

* Mon Sep  2 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (1.2.4-6m)
- fix typo?

* Sat Aug 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.4-5m)
- remove Requires: tag

* Fri Aug 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.4-4m)
- transform='s,x,x,' in %makeinstall

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.4-3m)
- SDL-1.2-20020825 snapshot
- rebuild against arts-1.0.3-2m, esound-0.2.29-2m, DirectFB-0.9.13-1m

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (1.2.4-2k)
- version up.

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.2-2k)
- version up.

* Sat Apr 21 2001 Kenichi Matsubara <m@kondara.org>
- (1.2.0-4k)
- add SDL-1.2.0.so.

* Sat Apr 14 2001 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (1.1.7-5k)
- rebuild against audiofile-0.2.1.

* Tue Mar 27 2001 Toru Hoshina <toru@df-usa.com>
- (1.1.7-3k)

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (1.1.6-5k)
- rebuild against audiofile-0.2.0.

* Wed Oct 25 2000 Davide Inglima <cat@kondara.org>
- (1.1.6-1k)
- update to 1.1.6

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.5-3k)
- changed spec file with %configure and %makeinstall

* Thu Oct 19 2000 Davide Inglima <cat@kondara.org>
- Added necessary BuildPrereqs.

* Mon Oct 16 2000 Davide Inglima <cat@kondara.org>
- Modified specfile to install manpages in /usr/share/man, and tested doc 
  against /usr/share/doc.

* Sun Oct 15 2000 Unknown Kondaraz <webmaster@kondara.org>
- Modified SDL for Kondara and uploaded spec

* Wed Jan 19 2000 Sam Lantinga <slouken@libsdl.org>
- Re-integrated spec file into SDL distribution
- 'name' and 'version' come from configure 
- Some of the documentation is devel specific
- Removed SMP support from %build - it doesn't work with libtool anyway

* Tue Jan 18 2000 Hakan Tandogan <hakan@iconsult.com>
- Hacked Mandrake sdl spec to build 1.1

* Sun Dec 19 1999 John Buswell <johnb@mandrakesoft.com>
- Build Release

* Sat Dec 18 1999 John Buswell <johnb@mandrakesoft.com>
- Add symlink for libSDL-1.0.so.0 required by sdlbomber
- Added docs

* Thu Dec 09 1999 Lenny Cartier <lenny@mandrakesoft.com>
- v 1.0.0

* Mon Nov  1 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- First spec file for Mandrake distribution.
