%global momorel 9

Summary: Network booting/install configuration utility (GUI)
Name: system-config-netboot
Version: 0.1.45.4
Release: %{momorel}m%{?dist}
URL: http://fedorahosted.org/system-config-netboot/
Source0: %{name}-%{version}.tar.gz
Source1: pxelinux.0
Patch0: system-config-netboot-0.1.45.4-remove-alchemist.patch
License: GPLv2+
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gnome-python2, pygtk2, pygtk2-libglade, gnome-python2-canvas, alchemist
Requires: usermode usermode-gtk
Requires: %{name}-cmd = %{version}-%{release}
Requires(post): hicolor-icon-theme
BuildRequires: autoconf, automake, gettext, intltool, python, perl, perl-XML-Parser
BuildRequires: desktop-file-utils
BuildArch: noarch
Obsoletes: redhat-config-netboot

%description
system-config-netboot is a utility which allows you to configure 
diskless environments and network installations.

%package cmd
Summary: Network booting/install configuration utility
Group: Applications/System
Requires: tftp-server >= 0.29

%description cmd
system-config-netboot is a utility which allows you to configure 
diskless environments and network installations.

This package contains only command line utilities.

%prep
%setup -q

%patch0 -p1 -b .remove-alchemist

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%makeinstall
mkdir -p %{buildroot}/tftpboot/linux-install/pxelinux.cfg
cp -arfp %{buildroot}/%{_datadir}/system-config-netboot/msgs %{buildroot}/tftpboot/linux-install/
cp -fp %{SOURCE1} %{buildroot}/tftpboot/linux-install
mkdir -p %{buildroot}/%{_datadir}/icons/hicolor/48x48/apps
cp -afp pixmaps/preferences-network-boot.png  %{buildroot}/%{_datadir}/icons/hicolor/48x48/apps/preferences-network-boot.png
rm -f docs/Makefile*
%find_lang %name

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
%doc docs/*
%{_sbindir}/system-config-netboot
%{_bindir}/system-config-netboot
%{_datadir}/applications/system-config-netboot.desktop
%dir %{_datadir}/system-config-netboot
%{_datadir}/system-config-netboot/diskless.gif
%{_datadir}/system-config-netboot/install.gif
%{_datadir}/system-config-netboot/netboot_gtk.py*
%{_datadir}/system-config-netboot/pxeosdialog.py*
%{_datadir}/system-config-netboot/system-config-netboot.glade
%{_datadir}/system-config-netboot/system-config-netboot.py*
%{_datadir}/pixmaps/preferences-network-boot.xpm
%{_datadir}/icons/hicolor/48x48/apps/preferences-network-boot.png
%{_mandir}/*/system-config-netboot.8*

%files -f %{name}.lang cmd
%defattr(-,root,root)
%doc COPYING
%{_sbindir}/pxe*
%{_datadir}/system-config-netboot/diskless
%{_datadir}/system-config-netboot/msgs
%{_datadir}/system-config-netboot/pxelinux.cfg
%{_datadir}/system-config-netboot/diskless.py*
%{_datadir}/system-config-netboot/firsttime.py*
%{_datadir}/system-config-netboot/netboot_util.py*
%{_datadir}/system-config-netboot/pxeboot.py*
%{_datadir}/system-config-netboot/pxeos.py*
%{_mandir}/*/pxe*.8*
%dir /tftpboot/linux-install
/tftpboot/linux-install/pxelinux.cfg
/tftpboot/linux-install/msgs
/tftpboot/linux-install/pxelinux.0
%config %{_sysconfdir}/sysconfig/system-config-netboot
%config %{_sysconfdir}/pam.d/system-config-netboot
%config %{_sysconfdir}/security/console.apps/system-config-netboot

%changelog
* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.45.4-9m)
- add COPYING

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.45.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.45.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.45.4-6m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.45.4-5m)
- add system-config-netboot-0.1.45.4-remove-alchemist.patch

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.45.4-4m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.45.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.45.4-2m)
- remove Makefile* from doc

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.45.4-1m)
- update to 0.1.45.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.45.2-3m)
- rebuild against rpm-4.6

* Sun Jun 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.45.2-2m)
- remove %%{_datadir}/locale/*/LC_MESSAGES/system-config-netboot.mo from package cmd
- and fix %%files to avoid conflicting

* Sun Jun 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.45.2-1m)
- update to 0.1.45.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.40-3m)
- rebuild against gcc43

* Sat Dec 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.40-2m)
- use automake --add-missing

* Sun Oct 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.40-1m)
- import from fc5

* Thu Apr 19 2006 Jason Vas Dias <jvdias@redhat.com> - 0.1.40-1
- fix further issues reported by brian@chpc.utah.edu:
  o should be a way of configuring remote logging from the GUI
  o provide a means of disabling default snapshot directory generation

* Tue Feb 14 2006 Jason Vas Dias <jvdias@redhat.com> - 0.1.38-1
- further fix for f1@micromemory.com problem (now bug 181365)
  deal with required binaries that have been replaced by scripts
- fix bug 178395: allow setting of NISDOMAIN 
- fix bug 174629: fix initscript patches (prevent application!)
- fix bug 182869: should Requires(post): hicolor-icon-theme
- fix issues reported by brian@chpc.utah.edu (bugs to be raised):
  o mkdiskless must not wipe out client root customizations on re-run
  o give serial console checkbox a speed drop down list
    - add "Use tty0 console also" checkbox option
  o allow editing of extra boot options in the config file
  o do not create snapshot directory for network boot targets
  o remove /etc/ssh keys and make /etc/ssh a snapshot file
  o mount /var/cache, /var/lib/misc, /var/lib/mrtg on tmpfs / snapshot
  o fix network syslog : '*' -> '*.*' 

* Thu Feb 09 2006 Jason Vas Dias <jvdias@redhat.com> - 0.1.37-1
- fix problem reported by f1@micromemory.com: 
  detect and deal with missing libraries in the client root correctly
- fix bug 178392: disklessrc's "^$MODULE" should be "^$MODULE "
- ship updated .po files

* Wed Dec 21 2005 Jason Vas Dias <jvdias@redhat.com> - 0.1.36-1
- fix bug 170634: don't use pam_stack in pam configuration file
- fix bug 171820: add desktop.in file to POTFILES.in
- use 'LC_ALL=C' for updateDiskless' 'echo y | /sbin/mke2fs ...',
  as pointed out by email from Kiko Albiol Colomer <kiko@sertecnet.com>

* Mon Dec 19 2005 Jason Vas Dias <jvdias@redhat.com> - 0.1.34-1 
- fix bug 174942: make diskless clients free initrd memory
- fix bug 174941: fix mkdiskless syslog.conf typo: '*' -> '*.*'
- fix bug 176144: ship updated translations

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Sep 21 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.33-1
- fix bug 169011: fstab should use /media, not /mnt for cdrom, floppy
- fix bug 168782: /var/lib/xkb needs to be in snapshot files list
- fix bug 168415: duplicate old boot args not written to pxelinux.cfg file
- fix bug 167757: clients now log to netboot server by default
- fix bug 167762: extra kernel boot arguments preserved in pxelinux.cfg file
- fix bug 167543: disklessrc should look for pci devices of class 0x680 also

* Wed Aug 31 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.32-1
- fix bug 167145: help functionality disabled owing to wrong VERSION string
  This turned out to be because build was not using automake + autoconf + configure -
  it now does - + added AM_INIT_AUTOMAKE(system-config-netboot, ${VERSION}) to 
  configure.in and VERSION variable set in .spec file.

* Fri Aug 19 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.30-1
- fix bug 166018 : disklessrc's findhardware was grep-ping for PCI vendor:device
  IDs in the pcitable; it now uses /lib/modules/$kernel/modules.pcimap instead.

* Thu Aug 18 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.28-1
- fix bug 166217 : the fixed-size initrd was not big enough for RHEL-4 smp kernel.
  updateDiskless now creates the initrd first in a directory; its size is then
  determined and the initrd.img is created with sufficient size.
  pxeos.py now determines the uncompressed size and pxeboot.py now writes the correct 
  ramdisk_size to the syslinux.cfg file. 

* Thu Aug 11 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.26-1
- fix bug 165772 : read-only root filesystem reported as mounted with "rw" option
  It appears that the initscripts package of RHEL-4 requires a "READONLY=yes"
  option in /etc/sysconfig/init in the client root for rc.sysinit not to attempt
  to mount it read-write.
- fix bug 165735 : diskless clients cannot cope with no DNS PTR record for their IP address
  It was possible for multiple clients who were unable to determine their
  host name to mount the same snapshot directory - this is now impossible;
  If clients are unable to determine their host name and have empty
  SNAPSHOT settings, the IP address is used for the snapshot directory.
  It is also now possible to set the hostname and domainname without DNS
  by the DHCP server supplying the host-name option.
- fix bug 165730: prevent kernel*-2.6.9-12.2.EL getting an Oops when 'host' is run

* Fri Aug 05 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.24-1
- fix bug 164776: don't write empty 'ks=' string in pxeboot.py
  Fix network install parameters:
  Instead of just writing unused 'ks.cfg' file, specify 
  loader 'method=' and 'ip=' arguments if no kickstart file given.

* Fri Jul 22 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.22-1
- fix bugs 164011, 164012:
  updateDiskless now resolves missing module dependencies in the
  initrd, and returns an error for missing module files and executables.
- fix bug 161904: fix tooltips in pxeosdialog and NFS server label

* Wed Jun 15 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.20-1
- fix addendum to bugs 149000/135411: updateDiskless:
  Do not create SELinux xattr labels in the initrd filesystem

* Mon Jun 13 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.18-1
- fix bugs 159490, 159996, 160143

* Wed Jun 08 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.17-1
- fix bugs 159490, 159390, 159064, 156274

* Thu May 26 2005 Jason Vas Dias <jvdias@redhat.com> 0.1.16-1
- fix bugs 144240, 148022, 149000, 153047, 154982 

* Thu Apr 21 2005 Dan Walsh <dwalsh@redhat.com> 0.1.14-1
- Apply Ronny Buchmann patch
	system-config-netboot.glade has a wrong id for the kickstart file input.
	pxeosdialog.py wants osKickstartEntry and produces a traceback later, 
	because it didn't get a node for it.	

* Fri Apr 1 2005 Dan Walsh <dwalsh@redhat.com> 0.1.13-1
- Fix python gtk.False -> False

* Mon Dec 20 2004 Dan Walsh <dwalsh@redhat.com> 0.1.12-1
- Remove references to /bin/ash

* Mon Nov 15 2004 Dan Walsh <dwalsh@redhat.com> 0.1.11-1
- Remove Alchemist import

* Thu Nov 11 2004 Dan Walsh <dwalsh@redhat.com> 0.1.10-1
- Apply Mustafa Mahudhawala for disklessrc

* Mon Nov 1 2004 Dan Walsh <dwalsh@redhat.com> 0.1.9-1
- Fix pxeos to grab correct files, eliminate extra /
- Bug #115652  

* Wed Oct 6 2004 Dan Walsh <dwalsh@redhat.com> 0.1.8-1
- Update lang

* Fri Oct 1 2004 Dan Walsh <dwalsh@redhat.com> 0.1.7-1
- Update languages

* Thu Sep 30 2004 Dan Walsh <dwalsh@redhat.com> 0.1.6-1
- Fix location of mkinstalldirs

* Tue Sep 28 2004 Dan Walsh <dwalsh@redhat.com> 0.1.5-1
- Cleanup rsync messages and replace RHEL3 with RHEL

* Mon Sep 27 2004 Dan Walsh <dwalsh@redhat.com> 0.1.4-1
- Split netboot_util into netboot_gtk, so command line will work without X-Windows.

* Thu Jun 10 2004 Dan Walsh <dwalsh@redhat.com> 0.1.3-5
- Fix internationalization calls in pxeos and pxeboot

* Tue Mar 24 2004 Dan Walsh <dwalsh@redhat.com> 0.1.3-4
- Fix warning messages

* Tue Mar 24 2004 Dan Walsh <dwalsh@redhat.com> 0.1.3-3
- Add additional languages

* Tue Jan 6 2004 Dan Walsh <dwalsh@redhat.com> 0.1.3-2
- Remove python2.2 requirment

* Thu Dec 11 2003 Dan Walsh <dwalsh@redhat.com> 0.1.3-1
- Rename to system-config-netboot

* Tue Nov 11 2003 Dan Walsh <dwalsh@redhat.com> 0.1.2-2
- Remove quotes used to handle spaces in dir names.  Spaces will not be allowed.

* Wed Nov 5 2003 Dan Walsh <dwalsh@redhat.com> 0.1.2-1
- Cleanup pxe handling add kickstart more error checking

* Wed Nov 5 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-23
- Fix kickstart handling

* Wed Oct 22 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-22
- Bump

* Wed Oct 22 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-21
- Update with fixes from Oracle.

* Tue Oct 7 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-20
- bump

* Tue Sep 30 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-19
- Fix documentation
- Start with default initrd ram_size=10000

* Wed Sep 24 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-18
- Add documentation

* Tue Sep 16 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-17
- Fix nfs directory selection

* Fri Sep 12 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-16
- Make several changes suggested by Tammy.
- Fix man pages and pxeos bugs.

* Tue Sep 02 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-15
- Change to use snapshot name for diskless mount point instead of ip address.

* Tue Aug 19 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-14
- Add /etc/cups/certs to files for diskless

* Wed Jul 30 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-13
- Fix man page

* Tue Jul 22 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-12
- Improve boot up performance

* Tue Jul 19 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-11
- Update pxeboot

* Tue Jul 7 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-10
- Fix app title

* Tue Jul 7 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-9
- remove busybox-anaconda as a required package
- Change Red Hat Linux to Red Hat Enterprise Linux

* Mon Jul 6 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-7
- Fix relative paths in mkdiskless

* Tue Jul 1 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-6
- Add busybox-anaconda as a required package

* Mon Jun 16 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-5
- Fix /usr/sbin/system-config-netboot link.

* Tue Apr 29 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-4
- Add syncfiles to speed up firstboot in diskless

* Tue Apr 29 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-3
- Fix gui problems

* Fri Apr 18 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-2
- Add ExclusiveArch: i386 x86_64

* Wed Apr 16 2003 Dan Walsh <dwalsh@redhat.com> 0.1.1-1
- Move setup stuff into command line pxeos class

* Fri Apr 4 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-23
- Fix OS propagation problems

* Tue Apr 1 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-22
- Add Man pages for pxeos and pxeboot

* Tue Apr 1 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-21
- setup tftpboot directory

* Tue Apr 1 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-20
- Cleanup first time wizard, to be able to define network install or diskless

* Fri Mar 28 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-19
- remove erroneous exit from disklessrc

* Thu Mar 27 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-18
- Add command line support for pxeos and pxeboot

* Fri Mar 21 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-17
- Add syncfiles to speed up diskless

* Tue Mar 18 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-16
- Add diskless support to GUI

* Wed Mar 12 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-15
- Fix GUI and remove swap

* Wed Mar 12 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-14
- add missing diskless files

* Fri Mar 07 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-13
- correct diskless paths

* Wed Jan 29 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-12
- eliminate /dev/root

* Mon Jan 27 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-11
- Add additional dialogs and fix pxe file writes

* Fri Jan 24 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-10
- Update to support diskless environment

* Fri Jan 24 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-9
- Fix Typos

* Tue Jan 7 2003 Dan Walsh <dwalsh@redhat.com> 0.1.0-8
- Fix errors

* Thu Dec 19 2002 Dan Walsh <dwalsh@redhat.com> 0.1.0-7
- Fix GUI to create config files in new format

* Thu Nov 07 2002 Dan Walsh <dwalsh@redhat.com> 0.1.0-6
- Check services and start them if they are not running

* Fri Oct 25 2002 Dan Walsh <dwalsh@redhat.com> 0.1.0-5
- Add mftp

* Thu Oct 24 2002 Dan Walsh <dwalsh@redhat.com>
- Add diskless support

* Thu Oct 17 2002 Dan Walsh <dwalsh@redhat.com>
- cleanup

* Wed Sep 25 2002 Dan Walsh <dwalsh@redhat.com>
- Initial build.


