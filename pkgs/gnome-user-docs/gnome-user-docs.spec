%global momorel 1
Summary:        GNOME User Documentation
Name:           gnome-user-docs
Version:        3.6.2
Release: %{momorel}m%{?dist}
License:        GFDL
#VCS: git:git://git.gnome.org/gnome-user-docs
Source:         http://download.gnome.org/sources/gnome-user-docs/3.6/gnome-user-docs-%{version}.tar.xz
NoSource: 0
Group:          Documentation
BuildArch:      noarch

BuildRequires: scrollkeeper
BuildRequires: gnome-doc-utils
BuildRequires: pkgconfig
BuildRequires: gettext
BuildRequires: itstool
BuildRequires: yelp-tools
BuildRequires: autoconf automake

%description
This package contains end user documentation for the GNOME desktop
environment.

%prep
%setup -q -n gnome-user-docs-%{version}

%build
%configure --disable-scrollkeeper
%make 

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"

# FIXME: %%find-lang can't deal with /usr/share/help (yet)
%files
%doc COPYING AUTHORS NEWS README
%doc %{_datadir}/help/*/*

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-2m)
- reimport from fedora

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0.1-1m)
- update to 3.2.0.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.1-2m)
- add BR

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.2-1m)
- update to 2.29.2

* Wed Dec 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Mon Nov 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.2-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.1-1m)
- update to 2.25.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Dec 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sat Oct 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update 2.16.0

* Tue Apr 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1
- GNOME 2.8 Desktop

* Fri Apr 16 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.6.0.1-1m)
- update to 2.6.0.1
- GNOME 2.6 Desktop

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.1-2m)
- revised spec for enabling rpm 4.2.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-14k)
- add omfenc patch

* Thu Dec 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-12k)
- markup check

* Thu Dec 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-10k)
- validation check

* Wed Dec 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-8k)
- updatet japanese transration
- update nautilus-user-manual

* Tue Dec 11 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-6k)
- updatet japanese transration (still working...

* Tue Dec 11 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-4k)
- add japanese transration (still working...

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.4.1.1-2k)
- version 1.4.1.1

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- arangemen spec file

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.1

* Thu Mar 29 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0
- remove de ja transration
- K2K

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.72-5k)
- fixed for FHS

* Thu Apr 13 2000 kulara <ouka@fx.sakura.ne.jp>
- added japanese translation

* Tue Jan 4 2000 Elliot Lee <sopwith@redhat.com>
- version 1.0.72

* Fri Sep 17 1999 Elliot Lee <sopwith@redhat.com>
- version 1.0.7

* Thu Sep 09 1999 Elliot Lee <sopwith@redhat.com>
- version 1.0.6

* Fri Apr 02 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.5
- added german translation

* Mon Mar 22 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.4

* Mon Mar 15 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.3

* Fri Mar 12 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.2

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.5rh - special version for 5.2/GNOME beta CD

* Mon Feb 08 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.5

* Thu Feb 04 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.3

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.2

* Thu Dec 17 1998 Michael Fulbright <drmike@redhat.com>
- first pass at a spec file
