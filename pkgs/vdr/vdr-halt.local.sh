#!/bin/bash

# Modify /sbin/halt.local to call this script.  For more information,
# see /usr/share/doc/vdr*/README.package
#
# If /sbin/halt.local does not exist, something like the following shell
# script should work.  Remember to make it executable.
#
# #!/bin/sh
# /usr/lib/vdr/bin/vdr-halt.local.sh

wakeupfile=/var/lib/vdr/data/next-wakeup
rtcdir=/sys/class/rtc/rtc0
procalarm=/proc/acpi/alarm

trap "rm -f $wakeupfile" EXIT

if [ -s $wakeupfile ] ; then
    secs=$(cat $wakeupfile)
    datestr="1970-01-01 UTC $secs sec"
    echo -n "Setting wakeup for next VDR timer: " ; date -d "$datestr"
    if [ -w $rtcdir/wakealarm ] ; then
        offset=0
        [ -r $rtcdir/since_epoch ] && \
            offset=$(( $(cat $rtcdir/since_epoch) - $(date +%s) ))
        echo 0                      > $rtcdir/wakealarm
        echo $(( $secs + $offset )) > $rtcdir/wakealarm
    elif [ -w $procalarm ] ; then
        grep -qsx UTC /etc/adjtime && utc=--utc || utc=
        date $utc -d "$datestr" +"%F %T" > $procalarm
    else
        echo "Warning: no supported methods found, wakeup not set."
        sleep 1
    fi
fi
