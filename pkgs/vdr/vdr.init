#!/bin/bash
#
# vdr          Video Disk Recorder
#
# chkconfig:   - 30 09
# description: Video Disk Recorder (VDR) implements a complete digital \
#              set-top-box and video recorder.  It can work with signals \
#              received from satellites (DVB-S) as well as cable (DVB-C) \
#              and terrestrial (DVB-T) signals.  At least one DVB card \
#              is required to run VDR.

### BEGIN INIT INFO
# Provides: vdr
# Should-Start: $syslog $network lirc
# Should-Stop: $syslog $network lirc
# Short-Description: Video Disk Recorder
# Description: Video Disk Recorder (VDR) implements a complete digital
#              set-top-box and video recorder.  It can work with signals
#              received from satellites (DVB-S) as well as cable (DVB-C)
#              and terrestrial (DVB-T) signals.  At least one DVB card
#              is required to run VDR.
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

prog=vdr
runner=/usr/sbin/runvdr

log()
{
    [ -x /usr/bin/logger ] && \
        /usr/bin/logger -s -pdaemon.error -t"$prog" "$1" || echo "$1"
}

checkconf()
{
    cfg=/etc/vdr/channels.conf
    if [ ! -s "$cfg" ] ; then
        failure
        echo
        log $"Error: no valid $cfg found."
        log $"Use \"scandvb -o vdr\" from the dvb-apps package to create one."
        return 6
    else
        chown VDR_USER:VDR_GROUP "$cfg" && chmod 644 "$cfg"
    fi
}

lockfile=/var/lock/subsys/$prog

start() {
    echo -n $"Starting Video Disk Recorder ($prog): "
    checkconf || return $?
    status $prog &>/dev/null || VDR_INIT=1 daemon "$runner >/dev/null 2>&1 &"
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping Video Disk Recorder ($prog): "

    # Kill runvdr so it won't cause a restart in case of problems at shutdown.
    killproc $runner >/dev/null

    # Shutdown can be a bit slow sometimes with some plugins or lots of them,
    # so let's give it some time and a chance to go down gracefully.
    # TODO: make this configurable
    killproc -d 7 $prog

    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    if status $prog >/dev/null ; then
        action $"Restarting Video Disk Recorder ($prog): " killproc $prog -HUP
    else
        stop
        start
    fi
}

case "$1" in
    start|stop|restart)
        $1
        ;;
    force-reload)
        restart
        ;;
    reload)
        action $"Service $prog does not support the reload action: " /bin/false
        exit 3
        ;;
    status)
        status $prog
        ;;
    condrestart|try-restart)
  	[ ! -f $lockfile ] || restart
	;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|try-restart|force-reload}"
        exit 2
esac
