%global momorel 15

# TODO, maybe some day:
# - livebuffer patch, http://www.vdr-portal.de/board/thread.php?threadid=37309
# - channelfilter patch, http://www.u32.de/vdr.html#patches
# - rofa's parentalrating, cutter-marks patches

%bcond_with       docs
%bcond_with       plugins

%define varbase   %{_var}/lib/vdr
%define videodir  %{varbase}/video
%define vardir    %{varbase}/data
%define plugindir %{_libdir}/vdr
%define configdir %{_sysconfdir}/vdr
%define datadir   %{_datadir}/vdr
%define cachedir  %{_var}/cache/vdr
%define rundir    %{_var}/run/vdr
%define vdr_user  vdr
%define vdr_group video
# From APIVERSION in config.h
%define apiver    1.6.0

Name:           vdr
Version:        1.6.0
Release:        %{momorel}m%{?dist}
Summary:        Video Disk Recorder

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://www.cadsoft.de/vdr/
Source0:        ftp://ftp.cadsoft.de/vdr/%{name}-%{version}.tar.bz2
Source1:        %{name}.init
Source2:        %{name}.sysconfig
# TODO
Source4:        %{name}-udev.rules
Source5:        %{name}-reccmds.conf
Source6:        %{name}-commands.conf
Source7:        %{name}-runvdr.sh
# TODO
Source8:        %{name}.consoleperms
Source9:        %{name}-config.sh
# TODO
Source10:       %{name}-README.package
Source11:       %{name}-skincurses.conf
Source12:       %{name}-sky.conf
Source13:       %{name}-timercmds.conf
Source14:       %{name}-shutdown.sh
Source15:       %{name}-moveto.sh
Source16:       %{name}-CHANGES.package.old
Source17:       %{name}-halt.local.sh
# TODO
Patch0:         %{name}-channel+epg.patch
# TODO
Patch1:         http://zap.tartarus.org/~ds/debian/dists/stable/main/source/vdr_1.4.5-2.ds.diff.gz
Patch2:         http://www.saunalahti.fi/~rahrenbe/vdr/patches/vdr-1.6.0-liemikuutio-1.21.diff.gz
Patch3:         %{name}-1.6.0-scriptnames.patch
Patch4:         %{name}-1.6.0-paths.patch
Patch5:         %{name}-1.5.18-use-pkgconfig.patch
# http://article.gmane.org/gmane.linux.vdr/36097
Patch6:         %{name}-1.5.18-syncearly.patch
Patch7:         http://www.saunalahti.fi/~rahrenbe/vdr/patches/vdr-1.6.0-ttxtsubs-0.0.5.diff.gz
Patch8:         %{name}-1.6.0-man-section.patch
# Patch9: http://www.udo-richter.de/vdr/files/vdr-1.5.13-hlcutter-0.2.0.diff
# edited so that it applies on top of the liemikuutio patch (menu.c)
Patch9:         %{name}-1.5.18-hlcutter-0.2.0.diff
# TODO
Patch10:        %{name}-1.4.7-hlcutter-0.2.0-finnish.patch
# mainmenuhooks, timercmd and progressbar-support from
# http://winni.vdr-developer.org/epgsearch/downloads/vdr-epgsearch-0.9.24.tgz
Patch11:        MainMenuHooks-v1_0.patch
Patch12:        timercmd-0.1_1.6.0.diff
Patch13:        %{name}-1.5.17-progressbar-support-0.0.1.diff
Patch14:        %{name}-1.6.0-includes+pkgconfig.patch
Patch15:        %{name}-1.6.0-fedora-pkgconfig.patch
Patch16:        %{name}-1.6.0-dxr3-subtitles.patch
Patch17:        http://toms-cafe.de/vdr/download/vdr-timer-info-0.5-1.5.15.diff
Patch18:        ftp://ftp.cadsoft.de/vdr/Developer/vdr-1.6.0-1.diff
Patch19:        ftp://ftp.cadsoft.de/vdr/Developer/vdr-1.6.0-2.diff
Patch20:        %{name}-1.6.0-remove-dvb-abi-check.patch
Patch21:        vdr-1.6.0-conflicting-declaration.patch
Patch22:        vdr-1.6.0-glibc210.patch
Patch23:	vdr-1.6.0-use-libv4l1-videodev.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  libcap-devel
BuildRequires:  pkgconfig
BuildRequires:  perl(File::Spec)
BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
BuildRequires:  gettext
%if %{with docs}
BuildRequires:  doxygen
BuildRequires:  graphviz
%endif # docs
# patch23 requires this
BuildRequires:  libv4l-devel

Requires:       udev
Requires(pre):  shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig
Provides:       vdr(abi) = %{apiver}
Obsoletes:      vdr-subtitles <= 0.5.0

%description
VDR implements a complete digital set-top-box and video recorder.
It can work with signals received from satellites (DVB-S) as well as
cable (DVB-C) and terrestrial (DVB-T) signals.  At least one DVB card
is required to run VDR.

%package        devel
Summary:        Development files for VDR
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Provides:       vdr-devel(api) = %{apiver}

%description    devel
%{summary}.

%package        docs
Summary:        Developer documentation for VDR
Group:          Documentation

%description    docs
%{summary}.

%package        skincurses
Summary:        Shell window skin plugin for VDR
Group:          Applications/Multimedia
%if %{with plugins}
BuildRequires:  ncurses-devel
%endif # plugins
Requires:       vdr(abi) = %{apiver}

%description    skincurses
The skincurses plugin implements a VDR skin that works in a shell
window, using only plain text output.

%package        sky
Summary:        Sky Digibox plugin for VDR
Group:          Applications/Multimedia
Requires:       vdr(abi) = %{apiver}
Requires:       wget
Requires:       /usr/bin/logger

%description    sky
The sky plugin implements a new device for VDR, which is based on the
MPEG2 encoder card described at linuxtv.org/mpeg2/kfir.xml.  It allows
you to connect the analog a/v output of your Sky Digibox to VDR, so
that you can enjoy the full recording flexibility of VDR with your Sky
subscription.  You will need a Sky Digibox and a valid subscription in
order to use this plugin.


%prep
%setup -q
# TODO: does not apply
#patch0 -p1
%patch1 -p1
# TODO: does not apply
#patch -F 0 -i debian/patches/02_plugin_missing.dpatch
# sort_options would be nice, but it conflicts with channel+epg which is nicer
#patch -F 0 -i debian/patches/02_sort_options.dpatch
patch -F 0 -i debian/patches/06_recording_scan_speedup.dpatch
patch -F 2 -i debian/patches/07_blockify_define.dpatch
patch -F 0 -i debian/patches/10_livelock.dpatch
patch -F 0 -i debian/patches/11_atsc.dpatch
echo "DEFINES += -DHAVE_ATSC" >> Makefile
patch -F 0 -i debian/patches/19_debian_osdbase_maxitems.dpatch
# TODO: does not apply, check upstream for a 1.6.x version:
#       http://www.ktverkko.fi/~msmakela/software/vdr/
#patch -F 0 -i debian/patches/opt-20_suspend.dpatch
%patch2 -p1
%patch3 -p1
sed \
  -e 's|__CACHEDIR__|%{cachedir}|'   \
  -e 's|__CONFIGDIR__|%{configdir}|' \
  -e 's|__PLUGINDIR__|%{plugindir}|' \
  -e 's|__VARDIR__|%{vardir}|'       \
  -e 's|__VIDEODIR__|%{videodir}|'   \
  %{PATCH4} | patch -p1 -F 1
%patch5 -p1
%patch6 -p0
%patch7 -p1
%patch8 -p1
patch -p0 -F 1 -i %{PATCH9}
# TODO: does not apply
#patch10 -p0
patch -p1 -F 2 -i %{PATCH11}
# timer-info patch needs to come before timercmd patch to avoid conflicts
%patch17 -p1
patch -p1 -F 2 -i %{PATCH12}
%patch13 -p1
patch -p0 -F 2 -i %{PATCH14}
%patch15 -p0
%patch16 -p0
%patch18 -p1
%patch19 -p1
%patch20 -p0
%patch21 -p1 -b .conflicting-declaration~
%patch22 -p1 -b .glibc210
%patch23 -p1 -b .use-libv4l1-videodev~

for f in CONTRIBUTORS HISTORY* UPDATE-1.4.0 README.timer-info ; do
  iconv -f iso-8859-1 -t utf-8 -o $f.utf8 $f && mv $f.utf8 $f
done

cp -p %{SOURCE5} reccmds.conf
cp -p %{SOURCE13} timercmds.conf
cp -p %{SOURCE6} commands.conf
# Unfortunately these can't have comments in them, so ship 'em empty.
cat /dev/null > channels.conf
cat /dev/null > remote.conf
cat /dev/null > setup.conf
cat /dev/null > timers.conf

install -pm 644 %{SOURCE10} README.package
install -pm 644 %{SOURCE16} CHANGES.package.old

# Would like to do "files {channels,setup,timers}.conf" from config dir
# only, but rename() in cSafeFile barks "device or resource busy", cf.
# http://lists.suse.com/archive/suse-programming-e/2003-Mar/0051.html
cat << EOF > %{name}.rwtab
dirs    %{cachedir}
files   %{configdir}
files   %{vardir}
EOF

# Disable some graphs that end up too big to be useful.
for g in COLLABORATION INCLUDE INCLUDED_BY ; do
    sed -i -e 's/^\(\s*'$g'_GRAPH\s*=\s*\).*/\1NO/' Doxyfile
done


%build

cat << EOF > Make.config
CC           = %{__cc}
CXX          = %{__cxx}

ifeq (\$(RPM_OPT_FLAGS),)
  CFLAGS     = $RPM_OPT_FLAGS
  CXXFLAGS   = $RPM_OPT_FLAGS -Wall -Woverloaded-virtual -Wno-parentheses
else
  CFLAGS     = \$(RPM_OPT_FLAGS)
  CXXFLAGS   = \$(RPM_OPT_FLAGS) -Wall -Woverloaded-virtual -Wno-parentheses
endif
ifdef PLUGIN
  CFLAGS    += -fPIC
  CXXFLAGS  += -fPIC
endif

MANDIR       = %{_mandir}
BINDIR       = %{_sbindir}

LOCDIR       = \$(shell pkg-config vdr --variable=localedir)
PLUGINLIBDIR = \$(shell pkg-config vdr --variable=plugindir)
VIDEODIR     = \$(shell pkg-config vdr --variable=videodir)
CONFDIR      = \$(shell pkg-config vdr --variable=configdir)
INCLUDEDIR   = \$(shell pkg-config vdr --variable=includedir)
LIBDIR       = \$(PLUGINLIBDIR)

VDR_USER     = %{vdr_user}
EOF

make %{?_smp_mflags} vdr vdr.pc include-dir \
    BINDIR=%{_sbindir} INCLUDEDIR=%{_includedir} CONFDIR=%{configdir} \
    VIDEODIR=%{videodir} PLUGINLIBDIR=%{plugindir} LOCDIR=%{_datadir}/locale \
    DATADIR=%{datadir} CACHEDIR=%{cachedir} \
    RUNDIR=%{rundir} VARDIR=%{vardir} VDR_GROUP=%{vdr_group}

make %{?_smp_mflags} i18n LOCALEDIR=./locale

%if %{with docs}
make %{?_smp_mflags} srcdoc
find srcdoc -type f -size 0 -delete
%endif # docs

%if %{with plugins}
make %{?_smp_mflags} -C PLUGINS/src/skincurses LIBDIR=. all
make %{?_smp_mflags} -C PLUGINS/src/sky        LIBDIR=. all
%endif # plugins


%install
rm -rf $RPM_BUILD_ROOT

abs2rel() { perl -MFile::Spec -e 'print File::Spec->abs2rel(@ARGV)' "$@" ; }

install -Dpm 755 vdr $RPM_BUILD_ROOT%{_sbindir}/vdr

install -dm 755 $RPM_BUILD_ROOT%{_bindir}
install -pm 755 svdrpsend.pl $RPM_BUILD_ROOT%{_bindir}/svdrpsend
install -pm 755 epg2html.pl $RPM_BUILD_ROOT%{_bindir}/epg2html

export PKG_CONFIG_PATH=.
make install-i18n install-includes install-pc install-doc \
    PCDIR=%{_libdir}/pkgconfig DESTDIR=$RPM_BUILD_ROOT

install -dm 755 $RPM_BUILD_ROOT%{configdir}/plugins
install -pm 644 *.conf $RPM_BUILD_ROOT%{configdir}

install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vdr-plugins.d

install -dm 755 $RPM_BUILD_ROOT%{vardir}/themes
touch $RPM_BUILD_ROOT%{vardir}/themes/{classic,sttng}-default.theme

install -Dpm 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initscriptdir}/vdr
sed -i \
  -e 's|/usr/sbin/|%{_sbindir}/|'  \
  -e 's|/etc/vdr/|%{configdir}/|g' \
  -e 's|VDR_USER|%{vdr_user}|'     \
  -e 's|VDR_GROUP|%{vdr_group}|'   \
  $RPM_BUILD_ROOT%{_initscriptdir}/vdr

install -pm 755 %{SOURCE7} $RPM_BUILD_ROOT%{_sbindir}/runvdr
sed -i \
  -e 's|/usr/sbin/|%{_sbindir}/|'                    \
  -e 's|/etc/sysconfig/|%{_sysconfdir}/sysconfig/|g' \
  -e 's|/usr/lib/vdr\b|%{plugindir}|'                \
  -e 's|VDR_PLUGIN_VERSION|%{apiver}|'               \
  $RPM_BUILD_ROOT%{_sbindir}/runvdr

install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
sed -e 's|/usr/lib/vdr/|%{plugindir}/|' < %{SOURCE2} \
  > $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vdr
chmod 644 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vdr

install -dm 755 $RPM_BUILD_ROOT%{videodir}

touch $RPM_BUILD_ROOT%{videodir}/.update

install -dm 755 $RPM_BUILD_ROOT%{plugindir}/bin
sed -e 's|/var/lib/vdr/data/|%{vardir}/|' < %{SOURCE14} \
  > $RPM_BUILD_ROOT%{plugindir}/bin/%{name}-shutdown.sh
chmod 755 $RPM_BUILD_ROOT%{plugindir}/bin/%{name}-shutdown.sh
sed -e 's|/var/lib/vdr/data/|%{vardir}/|' \
    -e 's|/usr/lib/vdr/bin/|%{plugindir}/bin/|' < %{SOURCE17} \
  > $RPM_BUILD_ROOT%{plugindir}/bin/%{name}-halt.local.sh
chmod 755 $RPM_BUILD_ROOT%{plugindir}/bin/%{name}-halt.local.sh
sed -e 's|/var/lib/vdr/video|%{videodir}|' -e 's|/etc/vdr/|%{configdir}/|' \
  < %{SOURCE15} > $RPM_BUILD_ROOT%{plugindir}/bin/%{name}-moveto.sh
chmod 755 $RPM_BUILD_ROOT%{plugindir}/bin/%{name}-moveto.sh

install -dm 755 $RPM_BUILD_ROOT%{cachedir}
touch $RPM_BUILD_ROOT%{cachedir}/epg.data
install -dm 755 $RPM_BUILD_ROOT%{datadir}/logos
install -dm 755 $RPM_BUILD_ROOT%{rundir}
install -dm 755 $RPM_BUILD_ROOT%{vardir}
touch $RPM_BUILD_ROOT%{vardir}/next-wakeup

install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d
sed -e 's/VDR_GROUP/%{vdr_group}/' < %{SOURCE4} \
  > $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d/51-%{name}.rules
chmod 644 $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d/*-%{name}.rules

install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/security/console.perms.d
sed -e 's/VDR_GROUP/%{vdr_group}/' < %{SOURCE8} \
  > $RPM_BUILD_ROOT%{_sysconfdir}/security/console.perms.d/95-%{name}.perms
chmod 644 $RPM_BUILD_ROOT%{_sysconfdir}/security/console.perms.d/*%{name}.perms

install -Dpm 644 %{name}.rwtab $RPM_BUILD_ROOT%{_sysconfdir}/rwtab.d/%{name}

# devel
install -pm 755 %{SOURCE9} $RPM_BUILD_ROOT%{_bindir}/vdr-config
install -pm 755 i18n-to-gettext.pl \
  $RPM_BUILD_ROOT%{_bindir}/vdr-i18n-to-gettext
install -pm 755 newplugin $RPM_BUILD_ROOT%{_bindir}/vdr-newplugin
install -pm 644 Make.config $RPM_BUILD_ROOT%{_libdir}/vdr
ln -s $(abs2rel %{_includedir}/vdr/config.h %{_libdir}/vdr) \
  $RPM_BUILD_ROOT%{_libdir}/vdr

# i18n
%find_lang %{name}
sed -i -e '1i%%defattr(-,root,root,-)' %{name}.lang

# plugins
%if %{with plugins}
install -pm 755 PLUGINS/src/skincurses/libvdr-skincurses.so.%{apiver} \
  $RPM_BUILD_ROOT%{plugindir}
install -pm 644 %{SOURCE11} \
  $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vdr-plugins.d/skincurses.conf
%find_lang %{name}-skincurses
sed -i -e '1i%%defattr(-,root,root,-)' %{name}-skincurses.lang

install -pm 755 PLUGINS/src/sky/libvdr-sky.so.%{apiver} \
  $RPM_BUILD_ROOT%{plugindir}
install -pm 644 %{SOURCE12} \
  $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vdr-plugins.d/sky.conf
install -pm 755 PLUGINS/src/sky/getskyepg.pl \
  $RPM_BUILD_ROOT%{_bindir}/getskyepg
install -Dpm 644 PLUGINS/src/sky/channels.conf.sky \
  $RPM_BUILD_ROOT%{configdir}/plugins/sky/channels.conf.sky
%endif # plugins


%check
export PKG_CONFIG_PATH=$RPM_BUILD_ROOT%{_libdir}/pkgconfig
if [ "$(pkg-config vdr --variable=apiversion)" != "%{apiver}" ] ; then
    echo "ERROR: API version mismatch in vdr.pc / package / config.h" ; exit 1
fi


%clean
rm -rf $RPM_BUILD_ROOT


%pre
getent group %{vdr_group} >/dev/null || groupadd -r %{vdr_group}
getent passwd %{vdr_user} >/dev/null || \
useradd -r -g %{vdr_group} -d %{vardir} -s /sbin/nologin -M -n \
    -c "Video Disk Recorder" %{vdr_user}
:

%post
# Migrate stuff in vardir, videodir when upgrading from pre 1.6.0-15 (#443706)
# TODO: to be removed in F-13
if [ $1 -gt 1 -a -d /srv/vdr ] ; then

    # Continue to use /srv/vdr as the video dir for old setups
    f=%{_sysconfdir}/sysconfig/vdr
    if ! grep -q '^\s*VDR_OPTIONS.* -v' $f ; then
        sed -i -e 's|^\(\s*VDR_OPTIONS.*(\)\(.*\)|\1-v /srv/vdr \2|' $f
    fi

    # Move stuff previously in /var/lib/vdr to /var/lib/vdr/data
    for f in $(find %{varbase} -mindepth 1 -maxdepth 1 -not -name data -not -name "video*") ; do
        mv $f %{vardir}
    done

    # Set new home dir
    usermod -d %{vardir} %{vdr_user}
fi
/sbin/chkconfig --add vdr || :

%preun
if [ $1 -eq 0 ] ; then
    %{_initscriptdir}/vdr stop >/dev/null 2>&1
    /sbin/chkconfig --del vdr
fi
:

%postun
[ $1 -gt 0 ] && %{_initscriptdir}/vdr try-restart >/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc CONTRIBUTORS COPYING HISTORY* INSTALL MANUAL README* UPDATE-1.?.0
%doc CHANGES.package.old
%config(noreplace) %{_sysconfdir}/sysconfig/vdr
%config(noreplace) %{_sysconfdir}/udev/rules.d/*-%{name}.rules
%config(noreplace) %{_sysconfdir}/security/console.perms.d/*-%{name}.perms
%config(noreplace) %{_sysconfdir}/rwtab.d/%{name}
%config %dir %{_sysconfdir}/sysconfig/vdr-plugins.d/
%{_initscriptdir}/vdr
%{_bindir}/epg2html
%{_bindir}/svdrpsend
%{_sbindir}/runvdr
%{_sbindir}/vdr
%dir %{plugindir}/
%dir %{plugindir}/bin/
%{plugindir}/bin/%{name}-halt.local.sh
%{plugindir}/bin/%{name}-moveto.sh
%{plugindir}/bin/%{name}-shutdown.sh
%{datadir}/
%{_mandir}/man[58]/vdr.[58]*
%dir %{varbase}/
%defattr(-,%{vdr_user},%{vdr_group},-)
# TODO: tighten ownerships to root:root for some files in %{configdir}
%config(noreplace) %{configdir}/*.conf
%dir %{videodir}/
%ghost %{videodir}/.update
%ghost %{vardir}/next-wakeup
%ghost %{vardir}/themes/*.theme
%ghost %{cachedir}/epg.data
%defattr(-,%{vdr_user},root,-)
%dir %{configdir}/
%dir %{configdir}/plugins/
%dir %{rundir}/
%dir %{vardir}/
%dir %{vardir}/themes/
%dir %{cachedir}/

%files devel
%defattr(-,root,root,-)
%doc COPYING
%if ! %{with docs}
%doc PLUGINS.html
%endif # docs
%{_bindir}/vdr-config
%{_bindir}/vdr-i18n-to-gettext
%{_bindir}/vdr-newplugin
%{_includedir}/libsi/
%{_includedir}/vdr/
%{_libdir}/pkgconfig/vdr.pc
%{_libdir}/vdr/Make.config
%{_libdir}/vdr/config.h

%if %{with docs}
%files docs
%defattr(-,root,root,-)
%doc PLUGINS.html srcdoc/html/
%endif

%if %{with plugins}
%files skincurses -f %{name}-skincurses.lang
%defattr(-,root,root,-)
%doc PLUGINS/src/skincurses/COPYING PLUGINS/src/skincurses/HISTORY
%doc PLUGINS/src/skincurses/README
%config(noreplace) %{_sysconfdir}/sysconfig/vdr-plugins.d/skincurses.conf
%{plugindir}/libvdr-skincurses.so.%{apiver}

%files sky
%defattr(-,root,root,-)
%doc PLUGINS/src/sky/COPYING PLUGINS/src/sky/HISTORY
%doc PLUGINS/src/sky/README PLUGINS/src/sky/lircd.conf.sky
%{_bindir}/getskyepg
%config(noreplace) %{configdir}/plugins/sky/channels.conf.sky
%config(noreplace) %{_sysconfdir}/sysconfig/vdr-plugins.d/sky.conf
%{plugindir}/libvdr-sky.so.%{apiver}
%endif # plugins

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.0-15m)
- rebuild against graphviz-2.36.0-1m

* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-14m)
- fix v4l issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-11m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-10m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-8m)
- apply glibc210 patch

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-7m)
- rebuild against libjpeg-7

* Fri Apr  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-6m)
- fix compilation (Patch21)
-- http://www.vdr-portal.de/board/thread.php?threadid=84625
-- http://www.vdr-portal.de/board/thread.php?postid=803495

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-5m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-4m)
- modify %%files for smart handling of a directory
- and package devel Requires: %%{name} = %%{version}-%%{release}

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-3m)
- sync with Rawhide (1.6.0-16)

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- modify %%files for smart handling of a directory

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-1m)
- import from Fedora to Momonga
- use %%{_initscriptdir} instead of %%{_initrddir}

* Sat May 10 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-3
- Update liemikuutio patch to 1.21.
- Change timercmd patch to the one shipped with epgsearch 0.9.24.
- Include vdr-i18n-to-gettext in -devel.
- Own (%%ghost) videodir/.update.

* Sun Apr 13 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-2
- Apply upstream 1.6.0-1 maintenance patch.
- Update timer info patch to 0.5 (fixes the "+" sign).

* Mon Apr  7 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-1
- Apply "unofficial" timercmd patch from HoochVDR.
- Apply Luca Olivetti's DXR3 subtitle compatibility patch.
- Decrease default wakeup delay before a timed recording to 10 minutes.
- Don't reload DVB drivers by default on unexpected exits in runvdr.
- Use "kill -HUP" in init script's restart action only on a running service.
- Update and apply timer-info patch.

* Sun Apr  6 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-0.4
- Obsolete vdr-subtitles.

* Fri Apr  4 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-0.3
- Update liemikuutio patch to 1.20.
- Move various (partially) upstreamable changes to patches instead of
  inlining them in the specfile.
- Drop unused logdir.
- Move pre-2008 changelog entries to README.package.old.

* Sat Mar 29 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-0.2
- Add --with docs build option for building a -docs subpackage.
- Refresh liemikuutio and ttxtsubs patches.

* Sun Mar 23 2008 Ville Skytta <ville.skytta at iki.fi> - 1.6.0-0.1
- 1.6.0 final; still quite a bit to do with the package.
- Adapt sync early and epgsearch (partially) patches.
- Fix dependencies of the sky plugin.
- Move bunch of inline sed'ing to separate patches.
- Use kill -HUP in init script's restart action.
- Don't include bundled plugin sysconfig snippets in main package.

* Tue Mar 18 2008 Ville Skytta <ville.skytta at iki.fi> - 1.5.18-0.1
- First cut at packaging 1.5.18.
- Plugin licenses clarified to be GPLv2+.

* Sun Mar 16 2008 Ville Skytta <ville.skytta at iki.fi> - 1.4.7-11
- Kill runvdr before vdr in init script's "stop" action in order to prevent
  it from restarting vdr in case something goes wrong when shutting down.
- Direct debug messages from sync early patch to to syslog instead of stderr.
- Make time to wake up before a timer configurable.
- Enable shutdown script by default.

* Wed Feb 20 2008 Ville Skytta <ville.skytta at iki.fi> - 1.4.7-10
- Rebuild.

* Sun Feb 17 2008 Ville Skytta <ville.skytta at iki.fi> - 1.4.7-9
- Don't run "restart" for init script's "reload" action per the LSB spec.
- runvdr cleanups, handle PLUGIN_ENABLED values case insensitively.

* Thu Feb 14 2008 Ville Skytta <ville.skytta at iki.fi> - 1.4.7-8
- Patch to fix build with GCC 4.3's cleaned up C++ headers.

* Sat Jan 12 2008 Ville Skytta <ville.skytta at iki.fi> - 1.4.7-7
- Include Udo Richter's hard link cutter patch v0.2.0 (see README-HLCUTTER).
- Add some plugins to the default plugin order list in sysconfig.
- Minor runvdr cleanups.
