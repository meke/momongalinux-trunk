%global momorel 5

Name: hunspell-gl
Summary: Galician hunspell dictionaries
%define upstreamid 20080515
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://openoffice.mancomun.org/libreeengalego/Corrector/gl_ES-pack.zip
Group: Applications/Text
URL: http://wiki.mancomun.org/index.php/Corrector_ortogr%C3%A1fico_para_OpenOffice.org#Descrici.C3.B3n
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2
BuildArch: noarch

Requires: hunspell

%description
Galician hunspell dictionaries.

%prep
%setup -q -c -n hunspell-gl

%build
unzip gl_ES.zip
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_gl_ES.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20080515-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20080515-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20080515-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080515-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080515-1m)
- update to 20080515

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20071107-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20071107-1m)
- import from Fedora to Momonga

* Fri Nov 09 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071107-1
- latest dictionaries

* Tue Aug 21 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070802-1
- latest dictionaries
- clarify license

* Sun May 05 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070504-1
- latest dictionaries

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20061002-1
- initial version
