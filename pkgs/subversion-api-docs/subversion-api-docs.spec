%global         momorel 1

Name:           subversion-api-docs
Version:        1.8.8
Release:        %{momorel}m%{?dist}
Summary:        Subversion API documentation

Group:          Development/Tools
License:        BSD
URL:            http://subversion.tigris.org/
Source0:        http://svn.collab.net/repos/svn/tags/%{version}/doc/doxygen.conf
Source1:        http://svn.collab.net/repos/svn/tags/%{version}/COPYING
BuildRequires:  subversion-devel = %{version}, doxygen
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

%description
Subversion is a concurrent version control system which enables one or more
users to collaborate in developing and maintaining a hierarchy of files and
directories while keeping a history of all changes. This package provides
Subversion API documentation for developers.

%prep
rm -rf %{name}-%{version}
mkdir -p %{name}-%{version}
cd %{name}-%{version}
cp %{SOURCE0} doxygen.conf

%build
cd %{name}-%{version}
sed -e 's,\(INPUT *= *\).*$,\1%{_includedir}/subversion-1', \
    -e 's,\(OUTPUT_DIRECTORY *= *\).*$,\1docs,' \
    -e 's,\(GENERATE_TAGFILE *= *\).*$,\1docs/svn.tag,' \
doxygen.conf | doxygen -

%install
cd %{name}-%{version}
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_docdir}/subversion-%{version}/api-docs 
install -m 644 docs/svn.tag %{buildroot}%{_docdir}/subversion-%{version}/api-docs
install -m 644 docs/html/* %{buildroot}%{_docdir}/subversion-%{version}/api-docs
install -m 644 %{SOURCE1} %{buildroot}%{_docdir}/subversion-%{version}/api-docs

%clean
rm -rf %{name}-%{version}
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc %dir %{_docdir}/subversion-%{version}/api-docs
%doc %{_docdir}/subversion-%{version}/api-docs/*

%changelog
* Fri Mar 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.8-1m)
- update 1.8.8

* Mon Dec  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Wed Oct 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Sun Sep 01 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Wed Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update to 1.8.1

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update to 1.8.0

* Thu Jun  6 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.10-1m)
- update to 1.7.10

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.8-1m)
- update to 1.7.8

* Mon Aug 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6-1m)
- update to 1.7.6

* Fri May 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5-1m)
- update to 1.7.5

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-1m)
- update to 1.7.4

* Tue Feb 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.3-1m)
- update to 1.7.3

* Tue Dec  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update to 1.7.2

* Wed Oct 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.1-1m)
- update to 1.7.1

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update to 1.7.0

* Sun Sep 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-0.2m)
- update to 1.7.0-rc3

* Wed Aug 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-0.1m)
- update to 1.7.0-rc2

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.17-1m)
- update to 1.6.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.16-2m)
- rebuild for new GCC 4.6

* Fri Mar  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.16-1m)
- update 1.6.16

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.15-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.15-1m)
- update 1.6.15

* Wed Oct  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.13-1m)
- update 1.6.13

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.12-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.12-1m)
- update 1.6.12

* Mon Apr 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.11-1m)
- update 1.6.11

* Tue Jan 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.9-1m)
- update 1.6.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.6-1m)
- update 1.6.6

* Sun Aug 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.5-1m)
- update 1.6.5

* Fri Aug  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update 1.6.4

* Tue Jun 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- update 1.6.3

* Fri May 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-1m)
- update 1.6.2

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update 1.6.1

* Wed Apr  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Sun Mar  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.6-1m)
- update 1.5.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-2m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-1m)
- update 1.5.5

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- update 1.5.4

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.3-1m)
- update 1.5.3

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1

* Sun Jun 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-1m)
- update 1.5.0

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.6-2m)
- release %%{_docdir}/subversion-%%{version}, it's provided by subversion

* Fri May 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-1m)
- import from Fedora to Momonga

* Thu Feb 14 2008 Bojan Smojver <bojan@rexursive.com> 1.4.6-1
- bump up to 1.4.6

* Fri Aug 24 2007 Bojan Smojver <bojan@rexursive.com> 1.4.4-2
- bump up to 1.4.4

* Sat Mar 31 2007 Bojan Smojver <bojan@rexursive.com> 1.4.3-1
- bump up to 1.4.3

* Wed Nov 01 2006 Bojan Smojver <bojan@rexursive.com> 1.4.2-2
- re-tag for rebuild

* Wed Nov 01 2006 Bojan Smojver <bojan@rexursive.com> 1.4.2-1
- bump up to 1.4.2

* Sat Sep 16 2006 Bojan Smojver <bojan@rexursive.com> 1.3.2-2.1
- mass rebuild

* Tue Jul 25 2006 Bojan Smojver <bojan@rexursive.com> 1.3.2-2
- remove subversion-devel dependency (hopefully make mock happy)

* Sat Jun 10 2006 Bojan Smojver <bojan@rexursive.com> 1.3.2-1
- bump up to 1.3.2

* Sun Apr 09 2006 Bojan Smojver <bojan@rexursive.com> 1.3.1-1
- bump up to 1.3.1

* Wed Jan 11 2006 Bojan Smojver <bojan@rexursive.com> 1.3.0-1
- initial package
