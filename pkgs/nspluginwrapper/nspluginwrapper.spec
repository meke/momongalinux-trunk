%global momorel 11

# Compile as a debug package
%global make_debug_package 	0

# What gecko we use
%global gecko_flavour 		"mozilla"

%global geckover 		1.9.2

%global plugin_config_version 1.9
%global plugin_config_name plugin-config-%{plugin_config_version}
%global plugin_config_binary plugin-config

# Excluded plugins (separated by ':')
%define exclude_list 	"libtotem*:libjavaplugin*:gecko-mediaplayer*:mplayerplug-in*:librhythmbox*:packagekit*"

# Target defines
%if "%{_target_cpu}" == "i386"
%global target_bits	32
%endif

%if "%{_target_cpu}" == "i486"
%global target_bits	32
%endif

%if "%{_target_cpu}" == "i586"
%global target_bits	32
%endif

%if "%{_target_cpu}" == "i686"
%global target_bits	32
%endif

%if "%{_target_cpu}" == "ppc"
%global target_bits	32
%endif

%if "%{_target_cpu}" == "x86_64"
%global target_bits	64
%endif

%if "%{_target_cpu}" == "ppc64"
%global target_bits	64
%endif

# Define libraries for 32/64 arches
%global lib32			lib
%global lib64			lib64
%global libdir32		/usr/lib
%global libdir64		/usr/lib64

# define nspluginswrapper libdir (invariant, including libdir)
%global pkgdir32		%{libdir32}/%{name}
%global pkgdir64		%{libdir64}/%{name}

# define mozilla plugin dir and back up dir for 32-bit browsers
%global pluginsourcedir32	%{libdir32}/mozilla/plugins
%global plugindir32 		%{libdir32}/mozilla/plugins-wrapped

# define mozilla plugin dir and back up dir for 64-bit browsers
%global pluginsourcedir64	%{libdir64}/mozilla/plugins
%global plugindir64 		%{libdir64}/mozilla/plugins-wrapped

%global build_dir 		objs-%{target_bits}

%if "%{target_bits}" == "32"
%global lib	  	%{lib32}
%global libdir  	%{libdir32}
%global pkgdir  	%{pkgdir32}
%global plugindir	%{plugindir32}
%global pluginsourcedir	%{pluginsourcedir32}
%else
%global lib	  	%{lib64}
%global libdir  	%{libdir64}
%global pkgdir  	%{pkgdir64}
%global plugindir	%{plugindir64}
%global pluginsourcedir	%{pluginsourcedir64}
%endif

Summary:	A compatibility layer for Netscape 4 plugins
Name:		nspluginwrapper
Version:	1.3.0
Release:        %{momorel}m%{?dist}
Source0:        %{name}-%{version}%{?svndate:-%{svndate}}.tar.bz2
Source1:	%{plugin_config_name}.tar.gz
Source2:	plugin-config.sh.in
Source3:	%{name}.sh.in
Patch1:		nspluginwrapper-1.3.0-make.patch
Patch2:		nspluginwrapper-1.3.0-configure.patch
Patch3:		nspluginwrapper-1.3.0-directory.patch
Patch4:		nspluginwrapper-20090625-fix-npident-array-sending.patch
Patch5:		nspluginwrapper-1.3.0-inst.patch
Patch6:		nspluginwrapper-1.3.0-compiz.patch
Patch100:	plugin-config-setuid.patch
Patch101:	plugin-config-umask.patch
Patch102:	plugin-config-print.patch
Patch103:	plugin-config-native.patch
License:	GPLv2+
Group:		Applications/Internet
Url:		http://gwenole.beauchesne.info/projects/nspluginwrapper/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:	%{name} = %{version}-%{release}
Requires:	mozilla-filesystem
BuildRequires:	pkgconfig gtk2-devel glib2-devel nspr-devel
BuildRequires:	libX11-devel libXt-devel cairo-devel pango-devel libcurl-devel
BuildRequires:	gecko-devel >= %{geckover}
BuildRequires:  libstdc++-static
ExclusiveArch:	%{ix86} x86_64 ppc

%description
nspluginwrapper makes it possible to use Netscape 4 compatible plugins
compiled for %{_arch} into Mozilla for another architecture, e.g. x86_64.

This package consists in:
  * npviewer: the plugin viewer
  * npwrapper.so: the browser-side plugin
  * nspluginplayer: stand-alone NPAPI plugin player
  * mozilla-plugin-config: a tool to manage plugins installation and update

%prep
%setup  -q -a 1

# Installation & build patches
%patch1 -p1 -b .make
%patch2 -p1 -b .conf
%patch3 -p1 -b .dir
%patch4 -p0 -b .array
%patch5 -p1 -b .inst
%patch6 -p1 -b .compiz

# Plugin-config patches
pushd %plugin_config_name
%patch100 -p2
%patch101 -p2 -b .umask
%patch102 -p2 -b .print
%patch103 -p2 -b .native
popd

%build
# Build wrapper

# set the propper built options
%if %{make_debug_package}
    %if "%{target_bits}" == "64"
	export CFLAGS="-g -m64 -DDEBUG -DGLIB_COMPILATION"
    %else
	export CFLAGS="-g -m32 -DDEBUG -DGLIB_COMPILATION"
    %endif
%else
    export CFLAGS="$RPM_OPT_FLAGS -DGLIB_COMPILATION"
%endif

# set the propper built options
%if "%{target_bits}" == "64"
    export LDFLAGS="-m64 -L%{libdir64} -lgmodule-2.0"
%else
    export LDFLAGS="-m32 -L%{libdir32} -lgmodule-2.0"
%endif

mkdir %{build_dir}
pushd %{build_dir}
../configure 					\
	    --prefix=%{_prefix} 		\
	    --target-cpu=%{_target_cpu}		\
	    --pkgdir=%{name}			\
	    --pkglibdir=%{pkgdir}	        \
	    --with-lib32=%{lib32}		\
	    --with-lib64=%{lib64}		\
	    --with-base-lib=%{lib}		\
	    --with-base-libdir=%{libdir}	\
	    --viewer-paths=%{pkgdir}		\
	    --with-x11-prefix=/usr		\
	    --with-gecko=%{gecko_flavour}	\
	    --enable-viewer			\
	    --viewer-paths="%{pkgdir32}:%{pkgdir64}"\
	    --disable-biarch

make
popd

#Build plugin configuration utility
pushd %{plugin_config_name}
./configure --prefix=%{_prefix} --libdir=%{_libdir} CFLAGS="$RPM_OPT_FLAGS -DGLIB_COMPILATION"
make
popd

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{plugindir}
mkdir -p $RPM_BUILD_ROOT%{pluginsourcedir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig

make -C %{build_dir} install DESTDIR=$RPM_BUILD_ROOT

ln -s %{pkgdir}/npwrapper.so $RPM_BUILD_ROOT/%{plugindir}/npwrapper.so

# Install plugin-config utility
pushd %{plugin_config_name}
DESTDIR=$RPM_BUILD_ROOT make install
popd

cd $RPM_BUILD_ROOT/usr/bin
mv %{plugin_config_binary} $RPM_BUILD_ROOT/%{pkgdir}
cd -

rm -rf $RPM_BUILD_ROOT/usr/doc/plugin-config

cat %{SOURCE2} > $RPM_BUILD_ROOT%{_bindir}/mozilla-plugin-config
chmod 755 $RPM_BUILD_ROOT%{_bindir}/mozilla-plugin-config

cat %{SOURCE3} | %{__sed} -e "s|EXCLUDE_LIST|%{exclude_list}|g" \
    > $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}
chmod 644 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}

# set up nsplugin player starting script
%{__cat} > $RPM_BUILD_ROOT%{pkgdir}/nspluginplayer << EOF
export MOZ_PLUGIN_PATH=%{pluginsourcedir}
%{pkgdir}/npplayer "$@"
EOF
chmod 755 $RPM_BUILD_ROOT%{pkgdir}/nspluginplayer

# Remove conflicting files
rm -rf $RPM_BUILD_ROOT%{_bindir}/nspluginplayer
rm -rf $RPM_BUILD_ROOT%{_bindir}/nspluginwrapper

%clean
rm -rf $RPM_BUILD_ROOT

%post
/usr/bin/mozilla-plugin-config -i -f > /dev/null 2>&1 || :

%preun
if [ "$1" == "0" ]; then
    /usr/bin/mozilla-plugin-config -r > /dev/null 2>&1 || :
fi;

%files
%defattr(-,root,root)
%doc README COPYING NEWS
%dir %{pkgdir}
%dir %{plugindir}

%{pkgdir}/%{plugin_config_binary}
%{pkgdir}/npconfig
%{pkgdir}/npwrapper.so
%{pkgdir}/npviewer.bin
%{pkgdir}/npviewer.sh
%{pkgdir}/npviewer
%{pkgdir}/npplayer
%{pkgdir}/libxpcom.so
%{pkgdir}/libnoxshm.so
%{pkgdir}/nspluginplayer
%{plugindir}/npwrapper.so
%{_bindir}/mozilla-plugin-config
%config %{_sysconfdir}/sysconfig/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-11m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-10m)
- build fix

* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-9m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-6m)
- full rebuild for mo7 release

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-5m)
- sync with Fedora 13 (1.3.0-11)

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-4m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against xulrunner-1.9.1

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-1m)
- sync with Fedora 11 (1.3.0-5)

* Sat Mar 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-6m)
- revise BuildReq.

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-5m)
- rebuild against xulrunner-1.9.0.6-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-4m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-3m)
- rebuild against xulrunner-1.9.0.5-1m

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-2m)
- rebuild against xulrunner-1.9.0.4-1m

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-1m)
- sync with Fedora devel (1.1.2-4)

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-2m)
- rebuild against xulrunner-1.9.0.3-1m

* Sat Oct 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-1m)
- sync with Fedora 9 updates (1.1.0-5)

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91.5-6m)
- rebuild against xulrunner-1.9.0.2-1m

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91.5-5m)
- rebuild against xulrunner-1.9.0.1-1m

* Sun May  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91.5-4m)
- merge fedora 0.9.91.5-27
-
- * Wed Apr 30 2008 Christopher Aillon <caillon@redhat.com> 0.9.91.5-27
- - mozilla-filesystem now owns the plugin source dir

* Thu May  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.91.5-3m)
- release %%{pluginsourcedir}, it's provided by xulrunner

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91.5-2m)
- re-merge fedora package
- change Patch9

* Tue Mar 11 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-26
- /etc/sysconfig/nspluginwrapper marked as config file
- exclude some player plugins

* Mon Mar 10 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-25
- updated the sleep patch

* Thu Mar 06 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-24
- added experimental patch for #426968 - nspluginwrapper wakes up too much

* Tue Feb 26 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-23
- merged exclude patch with main tarball
- fixed #431095 - Typo in mozilla-plugin-config verbose output

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.91.5-22
- Autorebuild for GCC 4.3

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91.5-1m)
- import to Momonga from fedora-devel

* Mon Jan 21 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-21
- fixed #426618 - gcjwebplugin error: Failed to run
  (added to ignored plugins)

* Mon Jan 14 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-20
- fixed #426176 - Orphaned npviewer.bin processes

* Thu Jan 10 2008 Martin Stransky <stransky@redhat.com> 0.9.91.5-19
- xulrunner rebuild
- fixed build script, added gthread-2.0

* Mon Dec 24 2007 Warren Togami <wtogami@redhat.com> 0.9.91.5-18
- Make nsviewer.bin initialized for multithreading, fixes #360891

* Tue Dec 20 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-17
- disabled xpcom support - it causes more troubles than advantages

* Tue Dec 13 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-16
- spec fixes
- fixed xulrunner support

* Mon Dec 10 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-15
- updated configure script - gecko selection

* Thu Dec 06 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-14
- enabled xpcom support
- added fix for #393541 - scripts will never fail

* Fri Nov 23 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-13
- rebuilt against xulrunner

* Tue Nov 6 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-12
- more fixes from review by security standards team

* Wed Oct 31 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-11
- added fixes from review by security standards team

* Fri Oct 26 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-10
- mozilla-plugin-config can be run by normal user now

* Wed Oct 24 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-9
- Updated config utility - removes dangling symlinks and
  wrapped plugins
  
* Tue Oct 23 2007 Jeremy Katz <katzj@redhat.com> 0.9.91.5-8
- Rebuild against new firefox

* Mon Oct 15 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-7
- added a fix for #281061 - gnash fails when wrapped, works when native

* Wed Oct 10 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-6
- removed possibble deadlock during plugin restart

* Tue Oct 9 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-5
- fixed browser crashes (#290901)

* Mon Oct 1 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-4
- quit the plugin when browser crashes (#290901)

* Fri Sep 21 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-3
- added original plugin dir to the package

* Mon Sep 10 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-2
- added upstream patches - RPC error handling and plugin restart

* Mon Aug 27 2007 Martin Stransky <stransky@redhat.com> 0.9.91.5-1
- update to the latest upstream

* Mon Aug 27 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-19
- converted rpc error handling code to a thread-safe variant
- added a time limit to plugin restart

* Tue Aug 14 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-18
- implemented plugin restart (#251530)

* Tue Aug 14 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-17
- fixed an installation script (#251698)

* Mon Aug 13 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-16
- fixed plugins check
- minor spec fixes

* Mon Aug 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.91.4-3m)
- change spec file style to Fedora style

* Mon Aug 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.91.4-2m)
- import nspluginwrapper-0.9.91.4-rh.patch from Fedora

* Fri Aug 10 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-15
- removed mozembeded dependency
- excluded totem plugins from wrapping
- xpcom support is optional now

* Thu Aug 9 2007 Christopher Aillon <caillon@redhat.com> 0.9.91.4-14
- Rebuild against newer gecko

* Wed Aug 8 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-13
- removed unsafe plugins probe
- added agruments to mozilla-plugin-config

* Tue Aug 7 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-12
- removed fake libxpcom

* Mon Aug 6 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-11
- added gecko dependency
- added plugin configuration utility

* Fri Aug 3 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-10
- fixed totem-complex plugin wrapping

* Mon Jul 30 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-9
- added plugin dirs

* Fri Jul 27 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-8
- added switch for creating debug packages

* Thu Jul 19 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-7
- integrated with firefox / seamonkey

* Tue Jul 11 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-6
- added new options to the configuration utility
- modified along new plug-ins concept

* Thu Jun 19 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-5
- updated nspluginsetup script
- added support for x86_64 plug-ins

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.91.4-1m)
- initial package for Momonga Linux

* Thu Jun 14 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-4
- added ppc arch
- silenced installation scripts
- moved configuration to /etc/sysconfig

* Thu Jun 12 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-3
- updated nspluginsetup script and package install/uninstall scripts
- added cross-compilation support
- removed binaries stripping

* Fri Jun 8 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-2
- added BuildRequires - pkgconfig, gtk2-devel, glib, libXt-devel

* Fri Jun 8 2007 Martin Stransky <stransky@redhat.com> 0.9.91.4-1
- initial build
