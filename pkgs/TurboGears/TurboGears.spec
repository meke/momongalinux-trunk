%global momorel 2

Name:           TurboGears
Version:        1.1.3
Release:        %{momorel}m%{?dist}
Summary:        Back-to-front web development in Python

Group:          Development/Languages
License:        MIT
URL:            http://www.turbogears.org
Source0:        http://pypi.python.org/packages/source/T/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
Source1:        porting-1.0-1.1.txt
Patch0:         %{name}-1.0.8-cherrypyreq.patch
# Reported upstream http://trac.turbogears.org/ticket/2419
Patch1: turbogears-sqlcreate.patch
Patch2: turbogears-feed.patch
# Patch to allow turbogears to work with old turbokid until/unless RHEL6
# updates
Patch100: TurboGears-old-turbokid.patch
# Patch so TurboGears detects the correct version of dependencies installed on the system
Patch101: turbogears-pkg-resources-fix.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: python-configobj
BuildRequires: python-formencode
BuildRequires: python-genshi
BuildRequires: python-kid
BuildRequires: python-paste-script
BuildRequires: python-peak-rules
BuildRequires: python-simplejson
BuildRequires: python-tgmochikit
BuildRequires: python-turbocheetah
BuildRequires: python-sqlobject
BuildRequires: python-sqlalchemy
BuildRequires: python-elixir
BuildRequires: python-toscawidgets
BuildRequires: python-tw-forms
BuildRequires: python-nose
BuildRequires: python-webtest
BuildRequires: python-dateutil
BuildRequires: python-markdown

Requires:       python-cherrypy2
BuildRequires:  python-cherrypy2
BuildRequires: python-turbojson >= 1.3
Requires: python-turbojson >= 1.3
BuildRequires: python-turbokid >= 1.0.5
Requires:       python-turbokid >= 1.0.5

Requires:       python-sqlobject >= 0.10.1
Requires:       python-formencode >= 1.2.1
Requires:       python-setuptools >= 0.6c11
Requires:       python-turbocheetah >= 1.0
Requires:       python-simplejson >= 1.9.1
Requires:       python-paste-script >= 1.7
Requires:       python-configobj >= 4.3.2
Requires:       python-nose >= 0.9.3
Requires:       python-psycopg2
Requires:       python-elixir >= 0.6.1
Requires:       python-genshi >= 0.4.4
Requires:       python-kid
Requires:       python-peak-rules
Requires:       python-tgmochikit
Requires:       python-toscawidgets >= 0.9.6
Requires:       python-tw-forms >= 0.9.6
Requires:       python-webtest
Requires:       python-dateutil

%description
TurboGears brings together four major pieces to create an easy to install, easy
to use web megaframework. It covers everything from front end (MochiKit
JavaScript for the browser, Genshi for templates in Python) to the controllers
(CherryPy) to the back end (SQLAlchemy).

The TurboGears project is focused on providing documentation and integration
with these tools without losing touch with the communities that already exist
around those tools.

TurboGears is easy to use for a wide range of web applications.


%prep
%setup -q
%patch0 -b .cherrypyreq
%patch1 -p1 -b .sqlcreate
%patch2 -p1 -b .feed
%patch101 -p1 -b .pkgresources

cp -p %{SOURCE1} .

%build
sed -i 's/.*TurboJson.*//' TurboGears.egg-info/requires.txt
%{__python} setup.py egg_info
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%check
cp %{_bindir}/nosetests .
sed -i 's/__requires__ =\(.*\)/__requires__ = [\1, "TurboGears"]/' nosetests 

PYTHONWARNINGS=default PYTHONPATH=$(pwd) ./nosetests -q

# fails {
#PYTHONWARNINGS=default PYTHONPATH=$(pwd) python setup.py test
# }

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%doc CHANGELOG.txt LICENSE.txt porting-1.0-1.1.txt
%attr(0755,root,root) %{_bindir}/tg-admin
%{python_sitelib}/%{name}-%{version}-py*.egg-info/
%{python_sitelib}/turbogears/

%changelog
* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-2m)
- add BuildRequires

* Fri Dec 23 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.3-1m)
- version up 1.1.3

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.9-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-1m)
- back to 1.0.9
- import patches from Fedora 13 (1.0.9-3)

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8
- import Patch1 from Rawhide (1.0.7-2)

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.4.4-1m)
- version up 1.0.4.4
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2.2-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2.2-1m)
- import from Fedora

* Thu May 11 2007 Luke Macken <lmacken@redhat.com> 1.0.2.2-2
- Update etree patch to work with Python2.4
- Update setuptools patch

* Thu May 3 2007 Luke Macken <lmacken@redhat.com> 1.0.2.2-1
- 1.0.2.2
- TurboGears-1.0.2.2-etree.patch to use xml.etree instead of elementtree module

* Fri Jan 26 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 1.0.1-2
- Don't flub the patch this time.

* Tue Jan 23 2007 Toshio Kuratomi <toshio@tiki-lounge.com> 1.0.1-1
- Upgrade to upstream 1.0.1.
- Update the setuptools patch.
- Conditionalize python-elementtree as python-2.5 provides it.
- Include rather than ghosting *.pyo.
- Require python-psycopg2 instead of psycopg, TurboGears + psycopg2 supports
  unicode whereas psycopg does not.
- Make all files except tg-admin non-executable.

* Fri Jan 19 2007 Luke Macken <lmacken@redhat.com> 1.0b2-6
- Add python-elementtree to BuildRequires

* Tue Dec 12 2006 Luke Macken <lmacken@redhat.com> 1.0b2-5
- Rebuild for new elementtree

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 1.0b2-4
- Add python-devel to BuildRequires

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 1.0b2-3
- Rebuild for python 2.5

* Sat Dec  2 2006 Luke Macken <lmacken@redhat.com> 1.0b2-2
- Update the setuptools patch

* Sat Dec  2 2006 Luke Macken <lmacken@redhat.com> 1.0b2-1
- 1.0b2

* Fri Nov 21 2006 Luke Macken <lmacken@redhat.com> 1.0b1-3
- Add python-TestGears back to Requires

* Fri Nov 21 2006 Luke Macken <lmacken@redhat.com> 1.0b1-2
- Add python-psycopg to Requires

* Mon Sep 11 2006 Luke Macken <lmacken@redhat.com> 1.0b1-1
- 1.0b1

* Sun Sep  3 2006 Luke Macken <lmacken@redhat.com> 0.8.9-4
- Rebuild for FC6

* Sun Jul  9 2006 Luke Macken <lmacken@redhat.com> 0.8.9-3
- Require python-TestGears (bug #195370)

* Thu Jul 6 2006 Luke Macken <lmacken@redhat.com> 0.8.9-2
- Add TurboGears-0.8.9-setuptools.patch

* Thu Jul 6 2006 Luke Macken <lmacken@redhat.com> 0.8.9-1
- Bump to 0.8.9
- Remove TurboGears-0.8a5-optim.patch and TurboGears-0.8a5-setuptools.patch

* Sat Feb 18 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8a5-1
- Initial RPM release
