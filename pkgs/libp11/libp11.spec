%global momorel 2

Name:           libp11
Version:        0.2.8
Release:        %{momorel}m%{?dist}
Summary:        Library for using PKCS#11 modules

Group:          Development/Libraries
License:        LGPLv2+
URL:            http://www.opensc-project.org/libp11
Source0:        http://www.opensc-project.org/files/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  doxygen graphviz
BuildRequires:  openssl-devel >= 1.0.0 pkgconfig libtool-ltdl-devel

%description
Libp11 is a library implementing a small layer on top of PKCS#11 API to
make using PKCS#11 implementations easier.

%package devel
Summary:        Files for developing with %{name}
Group:          Development/Libraries
Requires:       pkgconfig libp11 = %{version}-%{release}
License:        LGPLv2+

%description devel
Files for developing with %{name}

%prep
%setup -q

%build
%configure --disable-static --enable-doc --enable-api-doc
make %{?_smp_mflags}

# Remove CRLF line endings
sed -i "s|\r||g" doc/nonpersistent/wiki.out/trac.css

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

# Use %%doc to install documentation in a standard location
mkdir __docdir
mv $RPM_BUILD_ROOT%{_datadir}/doc/%{name}/{api,wiki}/ __docdir/
mv $RPM_BUILD_ROOT%{_datadir}/doc/%{name}/README __docdir/wiki
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/%{name}/

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING NEWS __docdir/wiki
%{_libdir}/libp11.so.*

%files devel
%defattr(-,root,root,-)
%doc examples __docdir/api
%exclude %{_libdir}/*.la
%{_libdir}/libp11.so
%{_libdir}/pkgconfig/*
%{_includedir}/*


%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.8-2m)
- rebuild against graphviz-2.36.0-1m

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.8-1m)
- update 0.2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.7-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.7-2m)
- rebuild against openssl-1.0.0

* Thu Mar 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.7-1m)
- import from Fedora to Momonga for engine_pkcs11 -> bind-9.7.0

* Wed Nov 25 2009 Kalev Lember <kalev@smartlink.ee> - 0.2.7-1
- Update to 0.2.7

* Sat Aug 22 2009 Kalev Lember <kalev@smartlink.ee> - 0.2.6-1
- Update to 0.2.6
- Enable building documentation with doxygen
- Use INSTALL="install -p" to preserve timestamps

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 0.2.3-8
- rebuilt with new openssl

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.3-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> - 0.2.3-5
- rebuild with new openssl

* Fri Nov 28 2008 Adam Tkac <atkac redhat com> - 0.2.3-4
- rebuild against new libltdl

* Thu Aug  7 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.2.3-3
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.2.3-2
- Autorebuild for GCC 4.3

* Thu Dec 06 2007 Matt Anderson <mra@hp.com> - 0.2.3-1
- Update to latest upstream sources

* Wed Dec 05 2007 Release Engineering <rel-eng at fedoraproject dot org> - 0.2.2-7
- Rebuild for deps

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.2.2-6
- Rebuild for selinux ppc32 issue.

* Thu Jun 28 2007 Matt Anderson <mra@hp.com> - 0.2.2-5
- Additional suggestions by tibbs@math.uh.edu, Source0 URL and license in docs

* Wed Jun 27 2007 Matt Anderson <mra@hp.com> - 0.2.2-4
- Merged in changes suggested by michael@gmx.net

* Thu Jun 21 2007 Matt Anderson <mra@hp.com> - 0.2.2-3
- Rebuilt .spec file based on rpmdev-newspec template
- Added --disable-static to comply with Fedora Packaging/Guidelines

* Thu Jun 21 2007 Matt Anderson <mra@hp.com> - 0.2.2-2
- Merged in changes suggested by tibbs@math.uh.edu

* Wed Jun 20 2007 Matt Anderson <mra@hp.com> - 0.2.2-1
- Initial RPM packaging
