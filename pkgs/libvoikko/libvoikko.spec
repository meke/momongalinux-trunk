%global momorel 4

Name:           libvoikko
Version:        3.0
Release:        %{momorel}m%{?dist}
Summary:        Voikko is a library for spellcheckers and hyphenators

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://voikko.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/voikko/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  malaga-devel >= 7.8 python
Requires:       malaga-suomi-voikko >= 1.3

%description
This is libvoikko, library for spellcheckers and hyphenators using Malaga
natural language grammar development tool. The library is written in C.

Currently only Finnish is supported, but the API of the library has been
designed to allow adding support for other languages later. Note however that
Malaga is rather low level tool that requires implementing the whole morphology
of a language as a left associative grammar. Therefore languages that have
simple or even moderately complex morphologies and do not require morphological
analysis in their hyphenators should be implemented using other tools such as
Hunspell.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release} pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package -n     voikko-tools
Summary:        Test tools for %{name}
Group:          Applications/Text
Requires:       %{name} = %{version}-%{release}

%description -n voikko-tools
This package contains voikkospell and voikkohyphenate, small command line
tools for testing libvoikko. These tools may also be useful for shell
scripts.

%prep
%setup -q


%build
# The dictionary path must be the same where malaga-suomi-voikko is installed
%configure --with-dictionary-path=%{_libdir}/voikko
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install INSTALL="install -p" DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
# Remove static archive
find $RPM_BUILD_ROOT -name '*.a' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING README
%{_libdir}/*.so.*

%files -n voikko-tools
%defattr(-,root,root,-)
%{_bindir}/voikkospell
%{_bindir}/voikkohyphenate
%{_bindir}/voikkogc
%{_mandir}/man1/voikkohyphenate.1*
%{_mandir}/man1/voikkospell.1*
%{_mandir}/man1/voikkogc.1*

%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/libvoikko.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linun.org>
- (3.0-1m)
- update to 3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-1m)
- update to 2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6-1m)
- import from Fedora

* Sun Mar 02 2008 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-3
- Put voikkospell and voikkohyphenate into a separate voikko-tools
  subpackage to decrease the size of the binary libvoikko package

* Sat Feb 16 2008 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-2
- Rebuild for GCC 4.3

* Tue Dec 04 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-1
- libvoikko 1.6
- Add versioned BuildRequires and Requires as per the Voikko release notes
  at http://voikko.sourceforge.net/releases.html

* Mon Dec 03 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-0.4.rc4
- Upstream released a new release candidate

* Wed Nov 28 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-0.3.rc3
- Upstream released a new release candidate

* Wed Nov 28 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-0.2.rc2
- Upstream released a new release candidate

* Tue Nov 27 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.6-0.1.rc1
- Upstream released a new release candidate

* Thu Nov 08 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.5-1
- Bump Release for the first Fedora build

* Wed Nov 07 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.5-0.3
- libvoikko-devel: remove unneeded Requires: malaga-devel
- install with -p so that timestamps are preserved

* Wed Nov 07 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.5-0.2
- Requires only malaga-suomi-voikko, BR malaga-devel and malaga-suomi-voikko
- Remove static archive

* Wed Oct 24 2007 - Ville-Pekka Vainio <vpivaini AT cs.helsinki.fi> 1.5-0.1
- Initial package
