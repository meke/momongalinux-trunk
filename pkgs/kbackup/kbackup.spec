%global momorel 1
%global srcrel 44998
%global qtver 4.8.2
%global kdever 4.8.97
%global kdelibsrel 1m

Summary: Back up your data in a simple, user friendly way
Name: kbackup
Version: 0.8
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Archiving
URL: http://members.aon.at/m.koller/
Source0: http://members.aon.at/m.koller/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.6.4-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: shared-mime-info
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils

%description
KBackup is a program that lets you back up any directories or files,
whereby it uses an easy to use directory tree to select the things to back up.
The program was designed to be very simple in its use
so that it can be used by non-computer experts.
The storage format is the well known TAR format, whereby the data
is still stored in compressed format (bzip2 or gzip).

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --remove-category X-SuSE-Backup \
  --remove-category X-MandrivaLinux-System-Archiving-Backup \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/*/mimetypes/text-x-kbp.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Mon Apr 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-3m)
- update sources, use --rmsrc option

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.6

* Sun Mar  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-7m)
- source was changed, remove srpm and build with --rmsrc option
- correct version string

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-6m)
- rebuild for new GCC 4.5

* Mon Oct 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-5m)
- fix version in about dialog

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-2m)
- rebuild against qt-4.6.3-1m

* Sun Jun 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Wed Jun  9 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.4-2m)
- source check sum

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-3m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Mon Jul 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-1m)
- version 0.6.1

* Mon May 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- version 0.6
- update desktop.patch
- good-bye KDE3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-8m)
- rebuild against rpm-4.6

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-7m)
- build doc/de again

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-6m)
- revise %%{_docdir}/HTML/*/kbackup/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-4m)
- rebuild against gcc43

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-3m)
- modify BPR for KDE4
- remove docs of de, this is temporal build fix

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-1m)
- version 0.5.4
- update desktop.patch
- License: GPLv2

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- version 0.5.3

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-4m)
- re-disable parallel build for i686

* Mon Aug 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-3m)
- update source tar ball, including Spanish translations

* Wed Aug 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-2m)
- re-enable parallel build

* Wed Aug 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-1m)
- version 0.5.2
- remove fr paches

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-2m)
- use make, make %%{?_smp_mflags} doesn't work with make-3.81-2m

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- initial package for Momonga Linux
- fr pathces are imported from Fedora
