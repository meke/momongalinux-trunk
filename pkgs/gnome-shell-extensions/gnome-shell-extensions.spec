%global momorel 1
%global pkg_prefix gnome-shell-extension
%global major_version %%(cut -d "." -f 1-2 <<<%{version})
%global min_gs_version %{major_version}.0
# Commit corresponding to the current release
%global commit 651f454


Name:           gnome-shell-extensions
Version:        3.6.2
Release: %{momorel}m%{?dist}
Summary:        Modify and extend GNOME Shell functionality and behavior

Group:          User Interface/Desktops
License:        GPLv2+ and BSD
URL:            http://live.gnome.org/GnomeShell/Extensions
# Using git archive since upstream doesn't publish tarballs on ftp.gnome.org
# anymore
# $ git clone git://git.gnome.org/gnome-shell-extensions/
# $ cd gnome-shell-extensions/
# $ git archive --format=tar --prefix=%{name}-%{version}/ %{commit} | xz > ../%{name}-%{version}.tar.xz
Source:		%{name}-%{version}.tar.xz
#Source: 	http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
#NoSource:	0

BuildRequires:  gnome-common
BuildRequires:  intltool
BuildRequires:  pkgconfig(gnome-desktop-3.0)
BuildRequires:  pkgconfig(libgtop-2.0)
Requires:       gnome-shell >= %{min_gs_version}
BuildArch:      noarch

%description
GNOME Shell Extensions is a collection of extensions providing additional 
and optional functionality to GNOME Shell.

Enabled extensions:
  * alternate-tab
  * alternative-status-menu
  * apps-menu
  * auto-move-windows
  * dock
  * drive-menu
  * native-window-placement
  * places-menu
  * systemMonitor
  * user-theme
  * windowsNavigator
  * workspace-indicator
  * xrandr-indicator
# TODO: uncomment when enabling gajim
#  * gajim


%package -n %{pkg_prefix}-common
Summary:        Files common to GNOME Shell Extensions
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       gnome-shell >= %{min_gs_version}

%description -n %{pkg_prefix}-common
GNOME Shell Extensions is a collection of extensions providing additional 
and optional functionality to GNOME Shell. Common files and directories 
needed by extensions are provided here.


%package -n %{pkg_prefix}-alternate-tab
Summary:        Classic Alt+Tab behavior. Window based instead of app based
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-alternate-tab
Lets you use classic Alt+Tab (window-based instead of app-based) in GNOME Shell.  
GNOME Shell groups multiple instances of the same application together. 
This extension disables grouping.  


%package -n %{pkg_prefix}-alternative-status-menu
Summary:        For those who want a power off item visible at all the time
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-alternative-status-menu
For those who want a power off item visible at all the time, replaces 
GNOME Shell status menu with one featuring separate Suspend and Power Off. 
Adds the ability to hibernate as well.


%package -n %{pkg_prefix}-apps-menu
Summary:        Application menu for GNOME Shell
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description  -n %{pkg_prefix}-apps-menu
Add a GNOME 2.x style menu for applications.


%package -n %{pkg_prefix}-auto-move-windows
Summary:        Assign specific workspaces to applications
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-auto-move-windows
Lets you manage your workspaces more easily, assigning a specific workspace to
each application as soon as it creates a window, in a manner configurable with a
GSettings key.


%package -n %{pkg_prefix}-dock
Summary:        Shows a dock-style task switcher permanently
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-dock
Shows a dock-style task switcher on the right side of the screen permanently.


%package -n %{pkg_prefix}-drive-menu
Summary:        Disk device manager in the system status area
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-drive-menu
Adds a menu in the system status area that tracks removable disk devices
attached and offers to browse them and eject/unmount them.


# TODO: uncomment when enabling gajim
# %package -n %{pkg_prefix}-gajim
# Summary:        Gajim IM integration
# Group:          User Interface/Desktops
# License:        GPLv2+
# Requires:       %{pkg_prefix}-common = %{version}-%{release}
# Requires:       gajim

# %description  -n %{pkg_prefix}-gajim
# Display Gajim incoming chats as notifications in the Shell message tray.


%package -n %{pkg_prefix}-native-window-placement
Summary:        Arrange windows in overview in a more native way
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description  -n %{pkg_prefix}-native-window-placement
This extension employs an algorithm (taken from KDE) for layouting the
thumbnails in the overview that more closely reflects the positions and relative
sizes of the actual windows, instead of using a fixed grid.


%package -n %{pkg_prefix}-places-menu
Summary:        Places menu indicator in the system status area
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-places-menu
Adds a menu in the system status area that resembles the Places menu from
GNOME 2.x


%package -n %{pkg_prefix}-systemMonitor
Summary:        Monitor your system status
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}
# should be pulled in by control-center, but in case someone tries for
# a minimalist gnome-shell installation
Requires:       libgtop2

%description -n %{pkg_prefix}-systemMonitor
Monitor your system status


%package -n %{pkg_prefix}-user-theme
Summary:        Lets the user select a custom theme for the shell
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-user-theme
Lets the user select a custom theme for the Gnome shell. It will allow you to 
apply a style from /.themes/[themeName]/gnome-shell/gnome-shell.css


%package -n %{pkg_prefix}-windowsNavigator
Summary:        Keyboard selection of windows and work-spaces in overlay mode
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-windowsNavigator
Allow keyboard selection of windows and work-spaces in overlay mode in 
GNOME Shell.  Switch to overview mode (press the windows or alt+f1 key) and 
press the alt key to show numbers over windows.  Press any number to switch
to the corresponding window.


%package -n %{pkg_prefix}-workspace-indicator
Summary:        Workspace Indicator
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description -n %{pkg_prefix}-workspace-indicator
Put an indicator on the panel signaling in which workspace you are, and give you
the possibility of switching to another one.


%package -n %{pkg_prefix}-xrandr-indicator
Summary:        Monitor status indicator
Group:          User Interface/Desktops
License:        GPLv2+
Requires:       %{pkg_prefix}-common = %{version}-%{release}

%description  -n %{pkg_prefix}-xrandr-indicator
This extension adds a systems status menu for rotating monitors
(overrides what is currently provided by gnome-settings-daemon.


%prep
%setup -q


%build
# since we build from a git checkout
[ -x autogen.sh ] && NOCONFIGURE=1 ./autogen.sh 

# TODO: add gajim to the list when enabling it
%configure  --enable-extensions="alternate-tab alternative-status-menu apps-menu auto-move-windows dock drive-menu native-window-placement places-menu systemMonitor user-theme windowsNavigator workspace-indicator xrandr-indicator"
%make 


%install
make install DESTDIR=%{buildroot}

%find_lang %{name}


%files -n %{pkg_prefix}-common -f %{name}.lang
%doc COPYING NEWS README
%dir %{_datadir}/gnome-shell/extensions/


%files -n %{pkg_prefix}-alternate-tab
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.alternate-tab.gschema.xml
%{_datadir}/gnome-shell/extensions/alternate-tab*


%files -n %{pkg_prefix}-alternative-status-menu
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.alternative-status-menu.gschema.xml
%{_datadir}/gnome-shell/extensions/alternative-status-menu*


%files -n %{pkg_prefix}-apps-menu
%{_datadir}/gnome-shell/extensions/apps-menu*


%files -n %{pkg_prefix}-auto-move-windows
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.auto-move-windows.gschema.xml
%{_datadir}/gnome-shell/extensions/auto-move-windows*


%files -n %{pkg_prefix}-dock
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.dock.gschema.xml
%{_datadir}/gnome-shell/extensions/dock*


%files -n %{pkg_prefix}-drive-menu
%{_datadir}/gnome-shell/extensions/drive-menu*


# TODO: uncomment when enabling gajim
# %files -n %{pkg_prefix}-gajim
# %{_datadir}/gnome-shell/extensions/gajim*


%files -n %{pkg_prefix}-native-window-placement
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.native-window-placement.gschema.xml
%{_datadir}/gnome-shell/extensions/native-window-placement*


%files -n %{pkg_prefix}-places-menu
%{_datadir}/gnome-shell/extensions/places-menu*


%files -n %{pkg_prefix}-systemMonitor
%{_datadir}/gnome-shell/extensions/systemMonitor*


%files -n %{pkg_prefix}-user-theme
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.user-theme.gschema.xml
%{_datadir}/gnome-shell/extensions/user-theme*


%files -n %{pkg_prefix}-windowsNavigator
%{_datadir}/gnome-shell/extensions/windowsNavigator*


%files -n %{pkg_prefix}-workspace-indicator
%{_datadir}/gnome-shell/extensions/workspace-indicator*


%files -n %{pkg_prefix}-xrandr-indicator
%{_datadir}/gnome-shell/extensions/xrandr-indicator*


%postun -n %{pkg_prefix}-alternate-tab
if [ $1 -eq 0 ]; then
  /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :
fi

%posttrans -n %{pkg_prefix}-alternate-tab
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :


%postun -n %{pkg_prefix}-alternative-status-menu
if [ $1 -eq 0 ]; then
  /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :
fi

%posttrans -n %{pkg_prefix}-alternative-status-menu
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :


%postun -n %{pkg_prefix}-auto-move-windows
if [ $1 -eq 0 ]; then
  /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :
fi

%posttrans -n %{pkg_prefix}-auto-move-windows
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :


%postun -n %{pkg_prefix}-dock
if [ $1 -eq 0 ]; then
  /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :
fi

%posttrans -n %{pkg_prefix}-dock
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :


%postun -n %{pkg_prefix}-native-window-placement
if [ $1 -eq 0 ]; then
  /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :
fi

%posttrans -n %{pkg_prefix}-native-window-placement
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :


%postun -n %{pkg_prefix}-user-theme
if [ $1 -eq 0 ]; then
  /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :
fi

%posttrans -n %{pkg_prefix}-user-theme
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas/ &>/dev/null || :


%changelog
* Tue Dec 18 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2

* Fri Oct 19 2012 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.6.0-2m)
- rebuild against gnome-shell 3.6.0

* Wed Oct  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sun Jul 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora
- update to 3.5.4

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-3m)
- rebuild for glib 2.33.2

* Mon Jan 16 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.3-2m)
- add patch2 - disable autohide

* Thu Dec 22 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3
- Fix alternative-status-menu extension crash when login
- Remove workaround to fix alternative-status-menu extension crash when login
- Re-enable all references to localedir in metadata.json files (reverted from commit c4b4092)
- Re-enable GSettings settings removed since 3.2.3 release

* Fri Oct 21 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.0-4m)
- make gnome-shell-extensions work with gnome-shell-3.2.1

* Fri Oct 14 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.0-3m)
- add patch0:dock: make dock extension work with gnome-3.2
- add patch1:dock: Fix popup menus

* Tue Oct 11 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.0-2m)
- correct require package name

* Tue Oct  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Sep 19 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91-1m)
- Update to 3.1.91
- Rename subpackages in line with new guidelines (# 715367)
- Enable systemMonitor extension
- Enable apps-menu and places-menu extension
- Enable drive-menu extension
- Enable  xrandr-indicator and workspace-indicator extension

* Thu Aug 11 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.0.2-1m)
- initial build
