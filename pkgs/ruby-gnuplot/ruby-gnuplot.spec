%global momorel 12

%global rbname gnuplot
Summary: A pipe-based interface to the ever popular gnuplot package for Ruby.
Name: ruby-%{rbname}

Version: 0.5
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://rgnuplot.sourceforge.net/

Source0: http://osdn.dl.sourceforge.net/sourceforge/rgnuplot/rgnuplot-%{version}.tar.gz 
Source1: http://osdn.dl.sourceforge.net/sourceforge/rgnuplot/manual.pdf 

Patch0: ruby-gnuplot-0.5-ruby18.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
BuildRequires: ruby

Requires: ruby, gnuplot

%description
Ruby Gnuplot is a pipe-based interface to the ever popular gnuplot)
package. Through this interface every capability of gnuplot is useable
from within Ruby. The design of this software was very much inspired
by the Python Gnuplot effort that is currently being worked on by
Michael Haggerty.

The goals of this package are as follows (in order of my priorities):

1. Provide a general object oriented interface for scientific plotting
   that completely hides the details of the underlying
   implementation. ie. The fact that this uses gnuplot should be
   hidden from the user.

2. Provide sufficient documentation so that this software is useable
   and extendable by other individuals in the scientific community.

usage: require 'Gnuplot'

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n gplot

%patch0 -p1

%__cp %{S:1} .

%build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{ruby_libdir}
%__install -m 0644 Gnuplot.rb %{buildroot}%{ruby_libdir}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc MANIFEST README *.txt manual.pdf *_demo.rb
%{ruby_libdir}/Gnuplot.rb

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-12m)
- add source(s)

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5-11m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-8m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5-7m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-2m)
- %%NoSource -> NoSource

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5-1m)
- version 0.5.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3-2m)
- rebuild against ruby-1.8.0.

* Thu Oct 24 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.3-1m)
- new version.

* Fri Apr 19 2002 Kenta MURATA <muraken@kondara.org>
- (0.2-2k)
- first release.
