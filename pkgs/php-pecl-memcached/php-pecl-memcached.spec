%global momorel 1

%global php_apiver  %((echo 0; php -i 2>/dev/null | sed -n 's/^PHP API => //p') | tail -1)
%{!?__pecl:     %{expand: %%global __pecl     %{_bindir}/pecl}}
%{!?php_extdir: %{expand: %%global php_extdir %(php-config --extension-dir)}}

%global pkg_name memcached

Summary: memcached module for PHP
Name:    php-pecl-%{pkg_name}
Version: 2.0.1
Release: %{momorel}m%{?dist}
License: "PHP"
Group:   Development/Languages
URL:         http://pecl.php.net/package/memcached
Source0:     http://pecl.php.net/get/%{pkg_name}-%{version}.tgz
NoSource:    0 

BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#Requires:    php-api = %{php_apiver}
Requires:    php-api
#Requires:    memcached
BuildRequires: php-devel >= 5.4.1
BuildRequires: zlib-devel
BuildRequires: libmemcached-devel >= 0.48-1m
Requires: libmemcached

%description
This extension uses libmemcached library to provide API for communicating with memcached servers.

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}

%build
%{_bindir}/phpize
%configure --enable-intl
# Build main module
%make

%install
rm -rf %{buildroot}
%makeinstall INSTALL_ROOT=%{buildroot}

# install config file
install -d $RPM_BUILD_ROOT%{_sysconfdir}/php.d
cat > $RPM_BUILD_ROOT%{_sysconfdir}/php.d/%{pkg_name}.ini << 'EOF'
; Enable extension module
zend_apd=%{php_extdir}/%{pkg_name}.so
EOF

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CREDITS ChangeLog LICENSE README.markdown memcached-api.php
%config(noreplace) %{_sysconfdir}/php.d/%{pkg_name}.ini
%{php_extdir}/%{pkg_name}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1
- rebuild against php-5.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-5m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-4m)
- rebuild against libmemcached-0.48

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-4m)
- rebuild against php-5.3.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 5 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1.1-1m)
- initial version
