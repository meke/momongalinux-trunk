%global momorel 5

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: Yum plugin to order downloaded packages
Name: yum-plugin-download-order
Version: 0.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source: http://rakesh.fedorapeople.org/yum/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://rakesh.fedorapeople.org/yum/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python-setuptools, python-devel >= 2.7
Requires: yum >= 3.2.24

%description
Yum plugin to order downloads. Right now it just orders based on size.


%prep
%setup -q

%build
#Empty build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%doc README COPYING
%{python_sitelib}/*
/usr/lib/yum-plugins/download-order.py*
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/download-order.conf

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2-1m)
- import from Fedora

* Mon Sep 14 2009 Rakesh Pandit <rakesh@fedoraproject.org> 0.2-1
- Updated to 0.2 in sync with 3.2.24

* Wed Aug 26 2009 Rakesh Pandit <rakesh@fedoraproject.org> 0.1-5
- Added python-devel as BR and renamed to yum-plugin-download-order

* Sun Aug 23 2009 Rakesh Pandit <rakesh@fedoraproject.org> 0.1-4
- fixed buildrequire and definition of python_sitelib

* Tue Aug 04 2009 Rakesh Pandit <rakesh@fedoraproject.org> 0.1-3
- fixed tarball, fixed import bug

* Tue Jun 23 2009 Rakesh Pandit <rakesh@fedoraproject.org> 0.1-2
- Updated with setup file
- Included egg files

* Mon Jun 22 2009 Rakesh Pandit <rakesh@fedoraproject.org> 0.1-1
- Initial package
