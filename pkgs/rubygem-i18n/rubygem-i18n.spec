%global momorel 2
%global abi_ver 1.9.1

# Generated from i18n-0.4.1.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname i18n
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: New wave Internationalization support for Ruby
Name: rubygem-%{gemname}
Version: 0.6.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/svenfuchs/i18n
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
Requires: rubygem(activesupport) => 3.0.0
Requires: rubygem(sqlite3-ruby)
Requires: rubygem(mocha)
Requires: rubygem(test_declarative)
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
New wave Internationalization support for Ruby.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-2m)
- use RbConfig::Config

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-1m)
- update 0.6.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-1m)
- update 0.4.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-1m)
- Initial package for Momonga Linux
