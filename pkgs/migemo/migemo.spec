%global momorel 29
%global apelver 10.8-1m
%global pkg migemo
%global pkgname migemo


%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: A tool for Japanese incremental search
Name: migemo
Version: 0.40
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://migemo.namazu.org/
Source0: http://migemo.namazu.org/stable/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: migemo-0.40-ruby18.patch
Group: Applications/Text
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18 ruby18-bsearch ruby18-romkan
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
Requires: ruby18 ruby18-bsearch ruby18-romkan

%description
Migemo is a tool for Japanese incremental search.

%package -n emacs-migemo
Summary: Emacs front-end of Migemo
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Obsoletes: migemo-emacs
Obsoletes: migemo-xemacs
Obsoletes: elisp-migemo
Provides:  elisp-migemo

%description -n emacs-%{pkgname}
Emacs front-end of Migemo.

%package -n emacs-%{pkgname}-el
Summary:	Elisp source files for emacs-%{pkgname} under GNU Emacs
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}

%description -n emacs-%{pkgname}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q
%patch0 -p1 -b .ruby18

%build
autoreconf -fi
RUBY=ruby18 %{configure} --with-rubydir=%{ruby18_sitelibdir}

# create migemo.el
make migemo.el
# and bytecompile it
%{_emacs_bytecompile} migemo.el

cat > %{pkgname}-init.el <<"EOF"
(load "migemo")
EOF

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/%{pkg}
mv %{buildroot}%{_emacs_sitelispdir}/%{pkg}.el* %{buildroot}%{_emacs_sitelispdir}/%{pkg}
%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m 644 %{name}-init.el %{buildroot}%{_emacs_sitestartdir}/%{pkg}-init.el

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README tests
%{_bindir}/migemo*
%{_datadir}/migemo
%{ruby18_sitelibdir}/migemo*

%files -n emacs-%{pkgname}
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n emacs-%{pkgname}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-29m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-28m)
- switch to use new macros in /etc/rpm/macros.emacs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-27m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-26m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.40-25m)
- full rebuild for mo7 release

* Tue Aug 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-24m)
- update ruby18 patch

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-23m)
- build with ruby18

* Wed Aug  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.40-22m)
- rebuild against ruby-1.9.2

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-21m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-20m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-19m)
- rename migemo-emacs to elisp-migemo
- kill migemo-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.40-17m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.40-16m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-15m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-14m)
- rebuild against emacs-23.0.94

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-13m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-12m)
- rebuild against xemacs-21.5.29

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-11m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-10m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-9m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.40-8m)
- rebuild against gcc43

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-7m)
- add dependencies on apel

* Tue Jun 28 2005 Toru Hoshina <t@momonga-linux.org>
- (0.40-6m)
- /usr/lib/ruby

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.40-5m)
- enable x86_64.
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Thu Nov 25 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.40-4m)
- rebuild against emacs-21.3.50 (nen no tame...)

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (0.40-3m)
- revised spec for enabling rpm 4.2.

* Tue Aug  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.40-2m)
- rebuild against ruby-1.8

* Fri May 30 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.40-1m)
- update 0.40

* Sat Sep  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.32-2k)
- Obsoletes: jrsearch-emacs
- divide into migemo, migemo-emacs, and migemo-xemacs packages
