%global momorel 4
%global alphaver 16
%global patchver 15
%global prever 16

Summary: An X Window System based tool for creating CDs.
Name: xcdroast
Version: 0.98
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPL
URL: http://www.xcdroast.org/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}alpha%{alphaver}.tar.gz 
NoSource: 0
Source1: %{name}.desktop
Patch1: %{name}-%{version}alpha%{patchver}-linebuffer.patch
Patch2: %{name}-%{version}alpha%{patchver}-nowarn.patch
Patch3: %{name}-%{version}alpha%{patchver}-scan.patch
Patch16: %{name}-%{version}alpha%{patchver}-06_man.patch
Patch17: %{name}-%{version}alpha%{patchver}-07_case_cmp.patch
Patch18: %{name}-%{version}alpha%{patchver}-08_desktop.patch
Patch19: %{name}-%{version}alpha%{patchver}-09_share_dir.patch
Patch20: %{name}-%{version}alpha%{patchver}-10_cddbtool.patch
Patch23: %{name}-%{version}alpha%{patchver}-13_cdrecord_to_wodim.patch
Patch24: %{name}-%{version}alpha%{patchver}-14_atapi_to_oldatapi.patch
Patch25: %{name}-%{version}alpha%{patchver}-15_no_readcd_version.patch
Patch30: %{name}-%{version}alpha%{patchver}-prodvd.patch
Patch32: %{name}-%{version}alpha%{patchver}-nogtk1.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: genisoimage
Requires: wodim
Requires: icedax
BuildRequires: gdk-pixbuf-devel >= 0.8.0
BuildRequires: gtk2-devel
BuildRequires: pcre-devel
BuildRequires: pkgconfig

%description
X-CD-Roast provides a GUI interface for commands like cdrecord and
mkisofs. X-CD-Roast includes a self-explanatory X11 user interface,
automatic SCSI and IDE hardware setup, support for mastering of new
ISO9660 data CDs, support for production of new audio CDs, fast
copying of CDs without hard disk buffering, and a logfile option.

%prep
%setup -q -n %{name}-%{version}alpha%{alphaver}

%patch1 -p1 -b .linebuffer
%patch2 -p1 -b .nowarn
%patch3 -p1 -b ,scan
%patch16 -p1 -b .p16
%patch17 -p1 -b .p17
%patch18 -p1 -b .p18
## DO NOT APPLY Patch19 breaks GUI (ichiro@xcdroast-0.98-0.15.12m)
#%%patch19 -p1 -b .p19
%patch20 -p1 -b .p20
%patch23 -p1 -b .p23
%patch24 -p0 -b .p24
%patch25 -p1 -b .p25
%patch30 -p1 -b .cdrkit
%patch32 -p1 -b .nogtk1

%build
CFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--sysconfdir=%{_sysconfdir} \
	--mandir=%{_mandir} \
	--enable-gtk2 \
	--with-xcdroast-libdir-prefix=%{_libdir}/xcdroast-%{version}

make %{?_smp_mflags} || make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../../..%{_libdir}/%{name}-%{version}/icons/xcdricon.png %{buildroot}%{_datadir}/pixmaps/xcdricon.png

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
cp -f %{SOURCE1} %{buildroot}%{_datadir}/applications/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL README
%doc doc/*
%{_bindir}/xcdroast
%{_libdir}/xcdroast-%{version}
%{_datadir}/applications/xcdroast.desktop
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/pixmaps/xcdricon.png
%{_mandir}/man1/xcdroast.1*
 
%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-0.16.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-0.16.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-0.16.2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-0.16-1m)
- update to 0.98alpha16
- delete unused patches

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98-0.15.14m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98-0.15.13m)
- rebuild against rpm-4.6

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.12m)
- replace all patches to Fedora 9's patches for new cdrkit
- fix up desktop file

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98-0.15.11m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-0.15.10m)
- %%NoSource -> NoSource

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.9m)
- good-bye cdrtools and welcome cdrkit
- merge gentoo patches (to use cdrkit)

* Wed Apr 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.8m)
- fix %%build section (enable CFLAGS again)

* Sun Apr  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.7m)
- good-bye, consolehelper

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.6m)
- import new xcdroast.desktop from VineSeedPlus
- move xcdroast.desktop to %%{_datadir}/applications/
- add %%{_datadir}/pixmaps/xcdricon.png for x86_64

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.98-0.15.5m)
- enable x86_64.

* Sat Nov 20 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.96-0.15.4m)
- fix missing bindir/*

* Tue Aug 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.3m)
- update Source1: xcdroast.desktop
- correct BuildRequires

* Tue Nov 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98-0.15.2m)
- fix files: reported by Ichiro Nakai [Momonga-devel.ja:02338]

* Sat Nov 15 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98-0.15.1m)
- enable gtk2
- remove xcdroast-momonga.patch

* Wed Jan 29 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (0.98-0.13.2m)
- fix configure option.
- add man file.
- (Thanks to Ichiro Nakai [Momonga-devel.ja:01307],[Momonga-devel.ja:01308])
- fix /usr -> %{_prefix}

* Sat Jan 25 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (0.98-0.13.1m)
- 0.98alpha13
- add xcdroast-momonga.patch (Patch3 replace xcdroast-kondara.patch)

* Tue Jul 30 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.98-0.10.1m)
  change Release tag numbering rule.
  requires tag changed

* Wed Apr 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.98-0.0010002k)
- 0.98alpha10
- remove %{name}-%{version}alpha9-ver.patch

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.98-0.0009014k)
- BuildPreReq: imlib-devel >= 1.9.14-4k

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.98-0.0009012k)
- rebuild against libpng 1.2.2.

* Wed Feb 27 2002 Shingo Akagaki <dora@kondra.org>
- (0.98-0.0009010k)
- rebuild against for cdrtools-1.11-0.0012002k

* Tue Dec 25 2001 Toru Hoshina <t@kondara.org>
- (0.98-0.0009008k)
- broken package...

* Tue Dec 11 2001 Toru Hoshina <t@kondara.org>
- (0.98-0.0009006k)
- rebuild cdrtools 1.11-0.0012002k.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.98-0.0009004k)
- rebuild against libpng 1.2.0.

* Fri Oct 12 2001 Toru Hoshina <t@kondara.org>
- (0.98-0.0009002k)
- version up.

* Sat Jun 23 2001 Toru Hoshina <toru@df-usa.com>
- (0.98-0.0008003k)
- applied kondara.patch, thanks.
- see http://moomoo.gaiax.com/www/moomoo/i/n/i_nakai/diary.html

* Tue May 22 2001 Karsten Hopp <karsten@redhat.de>
- fix #41802, needs some testing if cdrecord 1.10
  is compatible with 1.9

* Tue Feb 27 2001 Karsten Hopp <karsten@redhat.de>
- symlink should be relative
- fix strip

* Mon Feb 12 2001 Karsten Hopp <karsten@redhat.de>
- update to alpha8 (security bug in alpha7)
- use consolehelper
- fix bugzilla #26870

* Fri Dec 01 2000 Karsten Hopp <karsten@redhat.de>
- rebuild to fix /usr/share/doc permissions 

* Mon Nov 13 2000 Karsten Hopp <karsten@redhat.de>
- update to 0.98alpha7, which has a new menu to configure non-root mode
- added README.nonroot, README.atapi to documentation

* Thu Aug 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix interoperability with mkisofs 1.13

* Wed Aug 23 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Update to current, this fixes the "crashes on startup" bug (#16055)
  (and the maintainer confirms the "alpha" is more stable than the lastest
  "release").

* Mon Aug  7 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- remove another insistence on Tcl 8.0

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jun 15 2000 Bill Nottingham <notting@redhat.com>
- fix dependencies

* Sun Jun 11 2000 Bill Nottingham <notting@redhat.com>
- build on ia64

* Fri Jun  9 2000 Jakub Jelinek <jakub@redhat.com>
- Remove patch for gcc 2.96 breakages, there were none
- Add patch to make et2c work with recent Tcl/Tk headers

* Thu Jun  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- get rid of the ExclusiveArch: line; it compiles on sparc and it might
  even work there...
- Exclude ia64 for now; cdrecord isn't available there.

* Thu Jun  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild for main distribution
- fix build with TCL/Tk 8.3
- remove cdrecord and mkisofs from the package, add
  dependencies on the cdrecord and mkisofs packages instead
- fix up the code to find cdrecord where it belongs.
- put man pages in FHS locations
- fix build with kernel 2.4 headers
- work around gcc 2.96 breakages

* Sun May 28 2000 Ngo Than <than@redhat.de>
- rebuild for 7.0
- put man page in correct place
- fix xcdroast to build with tcltk8.2
- add cdrecord-1.8 in xcdroast

* Tue Apr 04 2000 Ngo Than <than@redhat.de>
- include needed devices in the package (Bug #9997)

* Fri Feb 11 2000 Ngo Than <than@redhat.de>
- fix Bug 8564 and 8563

* Mon Jul 26 1999 Tim Powers <timp@redhat.com>
- rebuilt for 6.1

* Wed Apr 15 1999 Michael Maher <mike@redhat.com>
- built package for 6.0
- updated source

* Tue Oct 13 1998 Michael Maher <mike@redhat.com>
- rushed by wanger.. not updated

* Wed May 20 1998 Michael Maher <mike@redhat.com>
- built and updated package
- added changelog
- edited paths  
