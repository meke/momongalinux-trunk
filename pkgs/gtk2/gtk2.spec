%global momorel 1
# Note that this is NOT a relocatable package

%define glib2_base_version 2.27.3
%define glib2_version %{glib2_base_version}-1
%define pango_base_version 1.20.0
%define pango_version %{pango_base_version}-1
%define atk_base_version 1.29.4
%define atk_version %{atk_base_version}-2
%define cairo_base_version 1.6.0
%define cairo_version %{cairo_base_version}-1
%define libpng_version 1.2.2-16
%define xrandr_version 1.2.99.4-2
%define gobject_introspection_version 0.9.3
%define gir_repository_version 0.6.5-5

%define bin_version 2.10.0

Summary: The GIMP ToolKit (GTK+), a library for creating GUIs for X
Name: gtk2
Version: 2.24.19
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.gtk.org
#VCS: git:git://git.gnome.org/gtk+#gtk-2-24
Source: http://download.gnome.org/sources/gtk+/2.24/gtk+-%{version}.tar.xz
NoSource: 0
Source2: update-gtk-immodules
#Source3: im-cedilla.conf

# Biarch changes
Patch0: gtk-lib64.patch
Patch1: system-python.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=583273
Patch2: icon-padding.patch
#Patch4: fresh-tooltips.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=599618
#Patch8: tooltip-positioning.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=592582
#Patch14: gtk2-landscape-pdf-print.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=611313
Patch15: window-dragging.patch

# fix dso.
Patch17: gtk2-fixdso.patch

BuildRequires: atk-devel >= %{atk_version}
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: cairo-devel
BuildRequires: gdk-pixbuf2-devel
BuildRequires: pango-devel >= %{pango_version}
BuildRequires: libtiff-devel
BuildRequires: libjpeg-devel
BuildRequires: jasper-devel
BuildRequires: libXi-devel
BuildRequires: libpng-devel >= %{libpng_version}
BuildRequires: gettext
BuildRequires: cups-devel
BuildRequires: cairo-devel >= %{cairo_version}
BuildRequires: libXrandr-devel >= %{xrandr_version}
BuildRequires: libXrender-devel
BuildRequires: libXcursor-devel
BuildRequires: libXfixes-devel
BuildRequires: libXinerama-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXdamage-devel
BuildRequires: gobject-introspection-devel >= %{gobject_introspection_version}
# Bootstrap requirements
BuildRequires: gtk-doc
BuildRequires: automake autoconf libtool pkgconfig

# Conflicts with packages containing theme engines
# built against the 2.4.0 ABI
Conflicts: gtk2-engines < 2.7.4-7
Conflicts: libgnomeui < 2.15.1cvs20060505-2
Conflicts: redhat-artwork < 0.243-1

Provides: gail = %{version}-%{release}
Obsoletes: gail < 2.13.0-1

# required for icon theme apis to work
Requires: hicolor-icon-theme

# We need to prereq these so we can run gtk-query-immodules-2.0
Requires(post): glib2 >= %{glib2_version}
Requires(post): atk >= %{atk_version}
Requires(post): pango >= %{pango_version}
# and these for gdk-pixbuf-query-loaders
Requires(post): libtiff >= 3.6.1
Requires: libXrandr >= %{xrandr_version}

%description
GTK+ is a multi-platform toolkit for creating graphical user
interfaces. Offering a complete set of widgets, GTK+ is suitable for
projects ranging from small one-off tools to complete application
suites.

%package immodules
Summary: Input methods for GTK+
Group: System Environment/Libraries
Requires: gtk2 = %{version}-%{release}
# for /etc/X11/xinit/xinput.d and im-cedilla.conf
Requires: imsettings

%description immodules
The gtk2-immodules package contains standalone input methods that are shipped
as part of GTK+.

%package immodule-xim
Summary: XIM support for GTK+
Group: System Environment/Libraries
Requires: gtk2 = %{version}-%{release}

%description immodule-xim
The gtk2-immodule-xim package contains XIM support for GTK+.

%package devel
Summary: Development files for GTK+
Group: Development/Libraries
Requires: gtk2 = %{version}-%{release}
Requires: pango-devel >= %{pango_version}
Requires: atk-devel >= %{atk_version}
Requires: glib2-devel >= %{glib2_version}
Requires: gdk-pixbuf2-devel
Requires: cairo-devel >= %{cairo_version}
Requires: libX11-devel, libXcursor-devel, libXinerama-devel
Requires: libXext-devel, libXi-devel, libXrandr-devel
Requires: libXfixes-devel, libXcomposite-devel
Requires: libpng-devel
Requires: pkgconfig
# for /usr/share/aclocal
Requires: automake

Provides: gail-devel = %{version}-%{release}
Obsoletes: gail-devel < 2.13.0-1

%description devel
This package contains the libraries amd header files that are needed
for writing applications with the GTK+ widget toolkit. If you plan
to develop applications with GTK+, consider installing the gtk2-devel-docs
package.

%package devel-docs
Summary: Developer documentation for GTK+
Group: Development/Libraries
Requires: gtk2 = %{version}-%{release}
#BuildArch: noarch

%description devel-docs
This package contains developer documentation for the GTK+ widget toolkit.

%prep
%setup -q -n gtk+-%{version}

%patch0 -p1 -b .lib64
%patch1 -p1 -b .system-python
%patch2 -p1 -b .icon-padding
#%patch4 -p1 -b .fresh-tooltips
#%patch8 -p1 -b .tooltip-positioning
#%patch14 -p1 -b .landscape-pdf-print
%patch15 -p1 -b .window-dragging
%patch17 -p1 -b .fixdso

libtoolize
autoreconf

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; CONFIGFLAGS=--enable-gtk-doc; fi;
 %configure $CONFIGFLAGS \
	--with-xinput=xfree 		\
	--enable-debug		\
)

# fight unused direct deps
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

make %{?_smp_mflags}

# truncate NEWS
awk '/^Overview of Changes/ { seen+=1 }
{ if (seen < 2) print }
{ if (seen == 2) { print "For older news, see http://git.gnome.org/cgit/gtk+/plain/NEWS"; exit } }' NEWS > tmp; mv tmp NEWS

%install
# Deriving /etc/gtk-2.0/$host location
# NOTE: Duplicated below
#
# autoconf changes linux to linux-gnu
case "%{_host}" in
  *linux) host="%{_host}-gnu"
  ;;
  *) host="%{_host}"
  ;;
esac

# autoconf uses powerpc not ppc
host=`echo $host | sed "s/^ppc/powerpc/"`
# autoconf uses ibm-linux not redhat-linux (s390x)
host=`echo $host | sed "s/^s390\(x\)*-redhat/s390\1-ibm/"`

# Make sure that the host value that is passed to the compile
# is the same as the host that we're using in the spec file
#
compile_host=`grep 'host_triplet =' gtk/Makefile | sed "s/.* = //"`

if test "x$compile_host" != "x$host" ; then
  echo 1>&2 "Host mismatch: compile='$compile_host', spec file='$host'" && exit 1
fi

make install DESTDIR=$RPM_BUILD_ROOT        \
             RUN_QUERY_IMMODULES_TEST=false

%find_lang gtk20
%find_lang gtk20-properties

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/gtk-2.0
#
# Make cleaned-up versions of tutorials, examples, and faq for installation
#
mkdir -p tmpdocs
cp -aR docs/tutorial/html tmpdocs/tutorial
cp -aR docs/faq/html tmpdocs/faq

for dir in examples/* ; do
  if [ -d $dir ] ; then
     mkdir -p tmpdocs/$dir
     for file in $dir/* ; do
       install -m 0644 $file tmpdocs/$dir
     done
  fi
done

# We need to have separate 32-bit and 64-bit binaries
# for places where we have two copies of the GTK+ package installed.
# (we might have x86_64 and i686 packages on the same system, for example.)
case "$host" in
  alpha*|ia64*|powerpc64*|s390x*|x86_64*)
   mv $RPM_BUILD_ROOT%{_bindir}/gtk-query-immodules-2.0 $RPM_BUILD_ROOT%{_bindir}/gtk-query-immodules-2.0-64
   ;;
  *)
   mv $RPM_BUILD_ROOT%{_bindir}/gtk-query-immodules-2.0 $RPM_BUILD_ROOT%{_bindir}/gtk-query-immodules-2.0-32
   ;;
esac

# Install wrappers for the binaries
cp %{SOURCE2} $RPM_BUILD_ROOT%{_bindir}/update-gtk-immodules

# now im-cedilla.conf is provided by imsettings package
# Input method frameworks want this
#mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/xinput.d
#cp %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/xinput.d

# Remove unpackaged files
rm $RPM_BUILD_ROOT%{_libdir}/*.la
rm $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/*/*.la
rm $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/%{bin_version}/*/*.la

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/gtk-2.0/$host
touch $RPM_BUILD_ROOT%{_sysconfdir}/gtk-2.0/$host/gtk.immodules

mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/modules
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/immodules
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/%{bin_version}/filesystems

#
# We need the substitution of $host so we use an external
# file list
#
echo %dir %{_sysconfdir}/gtk-2.0/$host >> gtk20.lang
echo %ghost %{_sysconfdir}/gtk-2.0/$host/gtk.immodules >> gtk20.lang

%post
/sbin/ldconfig
/usr/bin/update-gtk-immodules %{_host}

%post immodules
/usr/bin/update-gtk-immodules %{_host}

%post immodule-xim
/usr/bin/update-gtk-immodules %{_host}

%postun
/sbin/ldconfig
if [ $1 -gt 0 ]; then
  /usr/bin/update-gtk-immodules %{_host}
fi

%postun immodules
/usr/bin/update-gtk-immodules %{_host}

%postun immodule-xim
/usr/bin/update-gtk-immodules %{_host}

%files -f gtk20.lang
%doc AUTHORS COPYING NEWS README
%{_bindir}/gtk-query-immodules-2.0*
%{_bindir}/update-gtk-immodules
%{_bindir}/gtk-update-icon-cache
%{_libdir}/libgtk-x11-2.0.so.*
%{_libdir}/libgdk-x11-2.0.so.*
%{_libdir}/libgailutil.so.*
%dir %{_libdir}/gtk-2.0
%dir %{_libdir}/gtk-2.0/%{bin_version}
%{_libdir}/gtk-2.0/%{bin_version}/engines
%{_libdir}/gtk-2.0/%{bin_version}/filesystems
%dir %{_libdir}/gtk-2.0/%{bin_version}/immodules
%{_libdir}/gtk-2.0/%{bin_version}/printbackends
%{_libdir}/gtk-2.0/modules
%{_libdir}/gtk-2.0/immodules
%{_datadir}/themes/Default
%{_datadir}/themes/Emacs
%{_datadir}/themes/Raleigh
%dir %{_sysconfdir}/gtk-2.0
%{_libdir}/girepository-1.0

%files immodules
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-am-et.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-cedilla.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-cyrillic-translit.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-inuktitut.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-ipa.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-multipress.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-thai.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-ti-er.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-ti-et.so
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-viqr.so
#%{_sysconfdir}/X11/xinit/xinput.d/im-cedilla.conf
%config(noreplace) %{_sysconfdir}/gtk-2.0/im-multipress.conf

%files immodule-xim
%{_libdir}/gtk-2.0/%{bin_version}/immodules/im-xim.so

%files devel -f gtk20-properties.lang
%{_libdir}/lib*.so
%{_libdir}/gtk-2.0/include
%{_includedir}/*
%{_datadir}/aclocal/*
%{_bindir}/gtk-builder-convert
%{_libdir}/pkgconfig/*
%{_bindir}/gtk-demo
%{_datadir}/gtk-2.0
%{_datadir}/gir-1.0

%files devel-docs
%{_datadir}/gtk-doc
# oops, man pages went missing
# %{_mandir}/man1/*
%doc tmpdocs/tutorial
%doc tmpdocs/faq
%doc tmpdocs/examples

%changelog
* Thu Jul  4 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.19-1m)
- update to 2.24.19

* Sun May 26 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.18-1m)
- update to 2.24.18

* Sat May  4 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.17-1m)
- update to 2.24.17

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.16-1m)
- update to 2.24.16

* Fri Jan  4 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.14-1m)
- update to 2.24.14

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.13-1m)
- update to 2.24.13

* Thu Sep 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.12-2m)
- fix https://bugzilla.gnome.org/show_bug.cgi?id=675365

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.12-1m)
- update to 2.24.12

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.11-1m)
- update to 2.24.11

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.10-4m)
- remove im-cedilla.conf, which is now provided by imsettings

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.10-3m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.10-2m)
- rebuild for glib 2.33.2

* Mon Feb 20 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.10-1m)
- update to 2.24.10
-- comment out patch8 (iBus on gnome-panel crasher)

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.8-1m)
- update to 2.24.8

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.7-1m)
- update to 2.24.7
-- disable-debug (g_value_get_schar has not implement glib-2.30.1 yet)

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.6-1m)
- update to 2.24.6

* Mon Aug  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.5-1m)
- update to 2.24.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.4-2m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4-1m)
- update to 2.24.4

* Thu Mar 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3 (bug fix release)

* Sun Mar  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1
- --disable-gtk-doc

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.1-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.0-2m)
- modify SPEC file

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Thu Sep 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-7m)
- build fix libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-6m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-5m)
- change patch gtk-lib64.patch

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-4m)
- revival Provides: gtk+-devel Obsolete: gtk+-devel

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-3m)
- split package immodules immodule-xim
- remove package static and remve file *.la
- add update-gdk-pixbuf-loaders update-gtk-immodules im-cedilla.conf
- add patches

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.1-2m)
- split off devel-docs subpackage

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.0-2m)
- rebuild against libjpeg-8a

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Feb 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.6-1m)
- update to 2.19.6

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.5-1m)
- update to 2.19.5

* Thu Jan 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.4-1m)
- update to 2.19.4

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.3-1m)
- update to 2.19.3

* Wed Jan 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.6-1m)
- update to 2.18.6

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.5-1m)
- [SECURITY] https://bugzilla.gnome.org/show_bug.cgi?id=598476
- update to 2.18.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2
-- add static package

* Thu Oct  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0
-- two files were deleted ?
-- %%{_sysconfdir}/gtk-2.0/gdk-pixbuf.loaders
-- %%{_sysconfdir}/gtk-2.0/gtk.immodules

* Tue Sep 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.6-3m)
- add patch5 for jpeg7

* Mon Aug 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.6-2m)
- rebuild against libjpeg-7

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.6-1m)
- update to 2.16.6
- delete patch30

* Sun Aug  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-3m)
- add Patch30 (no error patch, so remove please)
- add BPR: automake17 (http://pc11.2ch.net/test/read.cgi/linux/1188293074/609)

* Sat Aug  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-2m)
- add BPR:jasper-devel

* Sun Jul 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-1m)
- update to 2.16.5

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Mon Jun 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3
- delete patch0 (merged)

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.2-2m)
- add workaround for anaconda
-- disable gio mime

* Sun May 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2
- comment out patch20

* Sun Apr 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sat Mar 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0
- delete patch30 (merged)

* Thu Mar 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-2m)
- fix search button (Patch30)
-- http://bugzilla.gnome.org/show_bug.cgi?id=574059

* Tue Mar  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15.5-1m)
- update tp 2.15.5

* Fri Feb 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15.4-1m)
- update tp 2.15.4

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.3-1m)
- update tp 2.15.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.7-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.7-1m)
- update to 2.14.7

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.6-2m)
- update Patch0,1 for fuzz=0
- License: LGPLv2+

* Tue Dec 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-1m)
- update to 2.14.6
-- add configure option --with-included-loaders=png 

* Sat Nov 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-2m)
- back to 2.14.4
- /etc/gtk-2.0/gdk-pixbuf.loaders was broken

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5

* Sat Oct 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.4

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Mon Sep 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Mon Sep 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.12-1m)
- update to 2.12.12

* Mon Sep 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.11-6m)
- rollback to 2.12.11

* Sun Sep  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.1-1m)
- update 2.14.1
- Obso & Provide gail, gail-devel

* Thu Sep  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-5m)
- mv libgdk-x11-2.0.so from devel to main (for last-exit)

* Wed Aug 20  2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-4m)
- good-bye gtk2-common

* Wed Aug 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.11-3m)
- revise %%files to avoid conflicting
- gtk2-common should be merged into gtk2 to resolve dependencies of many packages

* Tue Aug 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-2m)
- mv libgdk-x11-2.0.so.* and libgtk-x11-2.0.so.* from gtk2 to gtk2-common

* Wed Jul  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-1m)
- update to 2.12.11

* Wed Jun  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.10-1m)
- update to 2.12.10

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.9-3m)
- rebuild against gcc43

* Mon Mar 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.9-2m)
- gtk2-devel need glib2-devel

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.9-1m)
- update to 2.12.9

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.8-1m)
- update to 2.12.8

* Wed Jan 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.7-1m)
- update to 2.12.7

* Tue Jan 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-1m)
- update to 2.12.6

* Wed Jan  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Wed Dec  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Thu Nov  8 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.1-2m)
- remove gtk+2.12.0-autotest.patch (avoid autoreconf)
- add Patch3: system-log-crash.patch
-     Patch4: firefox-print-preview.patch

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Fri Sep 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0
- add patch40 (do not build tests dir), i want to remove this patch.

* Tue Sep 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.14-5m)
- fix konqueror lockup issue (with flash)
- http://bugzilla.gnome.org/show_bug.cgi?id=463773

* Mon Sep 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.14-4m)
- Fix BuildRequires

* Sun Sep  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.14-3m)
- add any Requires

* Mon Aug 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.14-2m)
- import patch13 from Fedora (cups)

* Sun Jul 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.14-1m)
- version 2.10.14 (bug-fix release)

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.13-2m)
- unhold directory /usr/share/theme

* Thu Jun 14 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.13-1m)
- version 2.10.13 (bug-fix release)

* Tue Jun 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.12-2m)
- add patch11 (icon-theme.cache)
-- from http://svn.gnome.org/viewcvs/gtk%2B/trunk/gtk/gtkiconcachevalidator.c?r1=17767&r2=17982&view=patch

* Thu May  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.12-1m)
- version 2.10.12 (bug-fix release)

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.11-3m)
- set --enable-gtk-doc again

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.11-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3
- --disable-gtk-doc for the moment

* Thu Mar 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.11-1m)
- version 2.10.11 (bug-fix release)

* Tue Feb  6 2007 zunda  <zunda at freeshell.org>
- (kossori)
- added BUildPrereq: gamin-devel for fam.h

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.9-2m)
- rename gtk+ -> gtk2

* Tue Jan 23 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.9-1m)
- version 2.10.9 (bug-fix release)

* Sat Jan 20 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.8-2m)
- add patches from FC

* Fri Jan 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.8-1m)
- version 2.10.8 (bug-fix release)

* Mon Jan  1 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.10.6-2m)
- enable ppc64.

* Thu Oct  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.6-1m)
- version 2.10.6 (bug-fix release)

* Sun Sep 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4
-- add patch2 from FC

* Thu Sep 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-2m)
- add patch 9,10 from FC

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.8.20-3m)
- rebuild against expat-2.0.0-1m

* Wed Jul 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.20-2m)
- /usr/lib/libgdk-x11-2.0.so to gtk+
-- youtranslate requires this file.

* Mon Jul  3 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.20-1m)
- version 2.8.20 (bug-fix release)

* Thu Jun 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.19-1m)
- version 2.8.19 (bug-fix release)

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.18-2m)
- add %%exclude %%dir %%{_libdir}/gtk-2.0/include at %%files comon

* Sat May 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.18-1m)
- version 2.8.18 (bug-fix release)

* Sun Apr  9 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.17-1m)
- version 2.8.17 (bug-fix release)

* Thu Mar 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.16-1m)
- version 2.8.16 (bug-fix release)

* Wed Mar 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.15-1m)
- version 2.8.15 (bug-fix release)

* Sun Feb 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.13-1m)
- version 2.8.13 (bug-fix release)

* Sun Jan 29 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.11-1m)
- version 2.8.11 (bug-fix release)

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.9-1m)
- version 2.8.9 (bug-fix release)

* Mon Dec 5 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.8-2m)
- required Patch0: gtk+-2.8.3-gdk-2.0_pc-pango.patch by firefox-1.5

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.8-1m)
- version up 2.8.8
- GNOME 2.12.2 Platform

* Wed Nov 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.7-1m)
- version 2.8.7 (bug-fix release)

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.6-5m)
- comment out unnessesaly autoreconf and make check

* Fri Nov 18 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.6-4m)
- enable fbmanager gtk-doc man

* Tue Nov 15 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.6-3m)
- rebuild against cairo-1.0.2

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.6-2m)
- add autoreconf
- GNOME 2.12.1 Desktop

* Thu Oct  6 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.6-1m)
- version 2.8.6 (bug-fix release)
- revised spec file

* Wed Oct  5 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.4-1m)
- version 2.8.4 (bug-fix release)

* Thu Sep 22 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.3-2m)
- add Patch0 gtk+-2.8.3-gdk-2.0_pc-pango.patch

* Sat Sep 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.3-1m)
- update 2.8.3

* Tue Aug 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.10-1m)
- version 2.6.10 (bug-fix release)

* Thu Aug  4 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.9-1m)
- version 2.6.9 (bug-fix release)

* Fri Jul  1 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.8-1m)
- version 2.6.8 (bug-fix release)

* Wed May  4 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.7-2m)
- Cancel deletion of the conflict file(gtk-engines)
- reported by jkuchi [Momonga-devel.ja:03095]

* Thu Apr 14 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.7-1m)
- reverts two problematic changes which were made in GTK+ 2.6.5.

* Mon Apr 11 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.6-1m)
- bugfix release to fix problems with stock images in GTK+ 2.6.5.

* Sun Apr 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.5-1m)
- version 2.6.5 (bugfix release)

* Sat Mar 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.4-1m)
- version 2.6.4 (bugfix release)

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.1-3m)
- remove 'Requires: DirectFB-devel' in gtk+-devel sub package

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.1-2m)
- enable x86_64.

* Mon Jan 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Mon Oct 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.13-1m)
- version 2.4.13

* Sun Aug 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.7-2m)
- add noreplace at %%{_sysconfdir}/gtk-2.0/gtk.immodules

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.7-1m)
- version 2.4.7

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.6-1m)
- version 2.4.6

* Thu Jun 17 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.3-1m)
- version 2.4.3

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.0-3m)
- adjustment BuildPreReq

* Tue Apr 13 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.4.0-2m)
- corrected BuildPreReq

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0
- GNOME 2.6 Desktop
- remove patch0
- use aclocal-1.7, automake-1.7

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.4-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.2.4-1m)
- version 2.2.4
- obsolete patch1

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-2m)
- apply imcontextxim patch for Infinite loop (baatari-).

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-1m)
- version 2.2.2

* Wed Apr  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-5m)
- change handle key_press order.

* Sun Apr 06 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-4m)
- rebuild against zlib 1.1.4-5m

* Fri Mar  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-3m)
- rebuild against for XFree86-4.3.0

* Wed Mar 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-2m)
- fix URL
- delete doc TODO

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Wed Dec  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Sat Oct 19 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-4m)
- fix filelist

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-3m)
- add gtkimcontextxim.patch (see bugzilla.gnome.org#87482)

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-2m)
- fixup file list

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-2m)
- use old gtkimcontextxim

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0
- wait wait for DirectFB..

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-3m)
- update gdk-directfb to 2002-08-26

* Mon Aug 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-2m)
- support DirectFB

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-4m)
- uuppddaattee xxiimm mmoodduullee ppaattcchh

* Mon Jul 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-3m)
- add gtk+-2.0.5-xim_keycode.patch ("<")

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.5-2k)
- version 2.0.5

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.4-2k)
- version 2.0.4

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.3-2k)
- version 2.0.3

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.2-4k)
- rebuild against libpng-1.2.2.

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.2-2k)
- version 2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0.rc1-4k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0.rc1-2k)
- version 2.0.0.rc1
- change name to gtk+

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.15-4k)
- modify dependency list

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.15-2k)
- version 1.3.15

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-2k)
- version 1.3.14

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-10k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-8k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-6k)
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-2k)
- version 1.3.13
- rebuild against for pango-0.24
- rebuild against for glib-1.3.13
- rebuild against for atk-0.10

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.3.12-2k)
- version 1.3.12

* Mon Jul 16 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.3.6

* Thu Apr 26 2001 Shingo Akagaki <dora@kondara.org>
- fork from gtk+ spec
