%global momorel 8
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Faster access to applications, bookmarks, and other items
Name: katapult
Version: 0.3.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: http://katapult.kde.org/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.3.2.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils

%description
Katapult is an application for KDE, designed to allow faster access to
applications, bookmarks, and other items. It is plugin-based, so it can
launch anything that is has a plugin for. Its display is driven by
plugins as well, so its appearance is completely customizable. It was
inspired by Quicksilver for OS X.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath \
	LIBS="-lDCOP"

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category X-KDE-Utilities-Desktop \
  %{buildroot}%{_datadir}/applications/kde/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL %{name}/COPYING-DOCS
%{_bindir}/%{name}
%{_libdir}/kde3/%{name}_*.la
%{_libdir}/kde3/%{name}_*.so
%{_libdir}/lib%{name}.la
%{_libdir}/lib%{name}.so*
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/doc/HTML/en/%{name}
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/icons/*/*/*/*.svgz
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/services/%{name}_*.desktop
%{_datadir}/servicetypes/%{name}*.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2.2-6m)
- full rebuild for mo7 release

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2.2-5m)
- fix build with new gcc

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2.2-4m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2.2-2m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2.2-1m)
- version 0.3.2.2

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2.1-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2.1-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2.1-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2.1-2m)
- %%NoSource -> NoSource

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2.1-1m)
- version 0.3.2.1
- update desktop.patch

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1.4-1m)
- initial package for Momonga Linux
