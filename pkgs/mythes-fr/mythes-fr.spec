%global momorel 5

Name: mythes-fr
Summary: French thesaurus
Version: 2.2
Release: %{momorel}m%{?dist}
Source: http://www.dicollecte.org/download/fr/thesaurus_2-2.zip
Group: Applications/Text
URL: http://www.dicollecte.org/home.php?prj=fr
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+
BuildArch: noarch

%description
French thesaurus.

%prep
%setup -q -c

%build
for i in README_thes_fr.txt; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-1 -t UTF-8 $i > $i.new
    touch -r $i $i.new
    mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mythes
cp -p thes_fr.dat $RPM_BUILD_ROOT/%{_datadir}/mythes/th_fr_FR_v2.dat
cp -p thes_fr.idx $RPM_BUILD_ROOT/%{_datadir}/mythes/th_fr_FR_v2.idx

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_thes_fr.txt
%{_datadir}/mythes/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-3m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- remove dups

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-1m)
- import from Fedora 13

* Thu Oct 22 2009 Caolan McNamara <caolanm@redhat.com> - 2.2-1
- latest version

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 13 2009 Caolan McNamara <caolanm@redhat.com> - 2.1-4
- tidy spec

* Sat May 23 2009 Caolan McNamara <caolanm@redhat.com> - 2.1-3
- update to new location

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 15 2009 Caolan McNamara <caolanm@redhat.com> - 2.1-1
- initial version
