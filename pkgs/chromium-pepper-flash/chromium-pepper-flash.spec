%global momorel 1
%global chromeversion stable-35.0.1916.153-1

%ifarch %{ix86}
%define _arch i386
%endif
%ifarch x86_64
%define _arch x86_64
%endif

Name:           chromium-pepper-flash
URL:            http://www.google.com/chrome
Summary:        Pepperflash (FlashPlayer) for Google's opens source browser Chromium
Version:        14.0.0.125
Release:        %{momorel}m%{?dist}
License:        see "http://www.google.co.jp/intl/en/chrome/browser/privacy/eula_text.html"
Group:          Applications/Internet
Source0:        http://dl.google.com/linux/chrome/rpm/stable/%{_arch}/google-chrome-%{chromeversion}.%{_arch}.rpm 
NoSource:       0
Requires:       chromium
BuildRequires:  rpm cpio
ExclusiveArch:  %{ix86} x86_64

%description
Pepperflash plugin for Chromium

%package -n chromium-pdf-plugin
Summary:        The pdf plugin for Google's opens source browser Chromium
License:        see "http://www.google.co.jp/intl/en/chrome/browser/privacy/eula_text.html"
Group:          Applications/Internet
Requires:       chromium

%description -n chromium-pdf-plugin
Official pdf plugin for Chromium

%prep
%setup -c -T

%build
rpm2cpio %{SOURCE0} | cpio -idmv

%install
mkdir -p %{buildroot}%{_libdir}/chromium-browser/PepperFlash/
install -m644 opt/google/chrome/PepperFlash/* %{buildroot}%{_libdir}/chromium-browser/PepperFlash/ 
install -m755 opt/google/chrome/libpdf.so %{buildroot}%{_libdir}/chromium-browser/

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_libdir}/chromium-browser/PepperFlash
%{_libdir}/chromium-browser/PepperFlash/

%files -n chromium-pdf-plugin
%defattr(-,root,root,-)
%{_libdir}/chromium-browser/libpdf.so

%changelog
* Sun Jun 15 2014 Hajime Yoshimori <lugia@momonga-linux.org>
- (14.0.0.125-1m)
- update chrome-35.0.1916.153 (Flash 14.0.0.125)

* Wed Apr 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (13.0.0.182-1m)
- update chrome-34.0.1847.116 (Flash 13.0.0.182)

* Wed Mar 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (12.0.0.77-1m)
- update chrome-33.0.1750.149

* Fri Feb 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (12.0.0.70-1m)
- update chrome-33.0.1750.117

* Thu Feb 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (12.0.0.44-2m)
- fix install path

* Wed Feb 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (12.0.0.44-1m)
- Initial Commit Momonga Linux

