%global momorel 8

Name: xhtml1-dtds
Version: 1.0
Release: %{momorel}m%{?dist}
Group: Applications/Text

Summary: XML document type definition for XHTML 1.0

License: "W3C IPR"
URL: http://www.w3.org/TR/xhtml1/

Requires: coreutils
Requires(post): libxml2 >= 2.4.8
Requires(preun): libxml2 >= 2.4.8
# xml-common for /etc/xml
Requires: xml-common
BuildRequires: libxml2 >= 2.4.8

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
Source0: http://www.w3.org/TR/xhtml1/xhtml1.tgz
BuildRequires: tar

%description
This provides the DTDs of the Second Edition of XHTML 1.0, a reformulation
of HTML 4 as an XML 1.0 application, and three DTDs corresponding to the
ones defined by HTML 4. The semantics of the elements and their attributes
are defined in the W3C Recommendation for HTML 4. These semantics provide
the foundation for future extensibility of XHTML.

%prep
%setup -c -T
tar xvzf %{SOURCE0}
if [ `id -u` -eq 0 ]; then
  chown -R root:root .
  chmod -R a+rX,g-w,o-w .
fi


%build


%install
rm -rf $RPM_BUILD_ROOT

# XHTML 1.0 2nd edition
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/xhtml1/xhtml1-20020801/DTD
mkdir -p $DESTDIR
install xhtml1-20020801/DTD/*.dtd $DESTDIR
install xhtml1-20020801/DTD/*.ent $DESTDIR

##
## Create the XHTML1 XML catalog
##

CATALOG=$RPM_BUILD_ROOT/usr/share/sgml/xhtml1/xmlcatalog

/usr/bin/xmlcatalog --noout --create $CATALOG
/usr/bin/xmlcatalog --noout --add "public" \
    "-//W3C//DTD XHTML 1.0 Strict//EN" \
    "xhtml1-20020801/DTD/xhtml1-strict.dtd" $CATALOG
/usr/bin/xmlcatalog --noout --add "public" \
    "-//W3C//DTD XHTML 1.0 Transitional//EN" \
    "xhtml1-20020801/DTD/xhtml1-transitional.dtd" $CATALOG
/usr/bin/xmlcatalog --noout --add "public" \
    "-//W3C//DTD XHTML 1.0 Frameset//EN" \
    "xhtml1-20020801/DTD/xhtml1-frameset.dtd" $CATALOG
/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
    "http://www.w3.org/TR/xhtml1/DTD" \
    "xhtml1-20020801/DTD" $CATALOG
/usr/bin/xmlcatalog --noout --add "rewriteURI" \
    "http://www.w3.org/TR/xhtml1/DTD" \
    "xhtml1-20020801/DTD" $CATALOG

%clean
rm -rf $RPM_BUILD_ROOT

%post
CATALOG=/usr/share/sgml/xhtml1/xmlcatalog
#
# Register it in the super catalog with the appropriate delegates
#
ROOTCATALOG=/etc/xml/catalog

if [ ! -r $ROOTCATALOG ]
then
    /usr/bin/xmlcatalog --noout --create $ROOTCATALOG
fi

if [ -w $ROOTCATALOG ]
then
	/usr/bin/xmlcatalog --noout --add "delegatePublic" \
		"-//W3C//DTD XHTML 1.0" \
		"file://$CATALOG" $ROOTCATALOG
	/usr/bin/xmlcatalog --noout --add "delegateSystem" \
		"http://www.w3.org/TR/xhtml1/DTD" \
		"file://$CATALOG" $ROOTCATALOG
	/usr/bin/xmlcatalog --noout --add "delegateURI" \
		"http://www.w3.org/TR/xhtml1/DTD" \
		"file://$CATALOG" $ROOTCATALOG
fi

%postun
#
# On removal, unregister the xmlcatalog from the supercatalog
#
if [ "$1" = 0 ]; then
    CATALOG=/usr/share/sgml/xhtml1/xmlcatalog
    ROOTCATALOG=/etc/xml/catalog

    if [ -w $ROOTCATALOG ]
    then
	    /usr/bin/xmlcatalog --noout --del \
		    "-//W3C//DTD XHTML 1.0" $ROOTCATALOG
	    /usr/bin/xmlcatalog --noout --del \
		    "http://www.w3.org/TR/xhtml1/DTD" $ROOTCATALOG
	    /usr/bin/xmlcatalog --noout --del \
		    "http://www.w3.org/TR/xhtml1/DTD" $ROOTCATALOG
    fi
fi

%files
%defattr (-,root,root)
/usr/share/sgml/xhtml1

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-6m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.0-7.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Jun  2 2004 Daniel Veillard <veillard@redhat.com> 1.0-7
- add BuildRequires: libxml2, fixes 125030

* Mon Feb 23 2004 Tim Waugh <twaugh@redhat.com>
- Use ':' instead of '.' as separator for chown.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Oct 21 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- add %%clean specfile target

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Dec 13 2002 Daniel Veillard <veillard@redhat.com> 1.0-4
- Prepare for inclusion, Prereq xml-common, fix the uninstall
  for upgrades of the package

* Thu Dec 12 2002 Daniel Veillard <veillard@redhat.com> 1.0-1
- Creation, based on Tim Waugh docbook-dtd package
