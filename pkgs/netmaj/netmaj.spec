%global momorel 32

Summary: netmaj 2.0.7
Name: netmaj
Version: 2.0.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Amusements/Games
URL: http://www.sfc.wide.ad.jp/~kusune/netmaj/
Source0: http://www.sfc.wide.ad.jp/~kusune/netmaj/files/netmaj-2.0.7.tar.gz
Source1: http://www.sfc.wide.ad.jp/~kusune/netmaj/files/netmaj-xui-2.0.7.tar.gz
Source2: http://www.geocities.co.jp/SiliconValley-PaloAlto/4984/knetmaj/kannetmaj1.0.6.tar.gz
Source3: http://www.geocities.co.jp/SiliconValley-PaloAlto/4984/knetmaj/small-pai.tar.gz
Source4: netmaj.desktop
Source5: maj.desktop
Source6: netmaj.as
Source7: maj.as
Source8: maj-128.png
Source9: netmaj-128.png
Patch0: netmaj-2.0.7.patch1
Patch1: netmaj-2.0.7.jg.patch
Patch2: netmaj-momonga.patch.bz2
Patch3: netmaj-vararg.patch
Patch4: netmaj-2.0.7-lib64.patch
Patch5: netmaj-2.0.7-gcc41.patch
Patch6: netmaj-2.0.7-glibc210.patch
NoSource: 0
NoSource: 1
NoSource: 2
NoSource: 3
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
BuildRequires: ImageMagick
BuildRequires: coreutils

%description
netmaj 2.0.7/netmaj-xui 2.0.7/knetmaj 1.0.6(Small Pai).

%prep
%setup -q -a 1 -a 2 -n netmaj
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1 -b .vararg
%if %{_lib} == "lib64"
%patch4 -p1 -b .lib64~
%endif
%patch5 -p1 -b .gcc41~
%patch6 -p1 -b .glibc210

%build
make
cd kui
tar zxvf $RPM_SOURCE_DIR/small-pai.tar.gz
make
cd ..

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}/usr/games
mkdir -p %{buildroot}/usr/games/lib/netmaj
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/X11/ja/app-defaults
mkdir -p %{buildroot}%{_docdir}/netmaj-2.0.7
mkdir -p %{buildroot}%{_docdir}/netmaj-2.0.7/html
mkdir -p %{buildroot}%{_docdir}/netmaj-2.0.7/xui
mkdir -p %{buildroot}%{_docdir}/netmaj-2.0.7/kui
make install DESTDIR=%{buildroot} XAPPLOADDIR=%{_datadir}/X11/ja/app-defaults
make install-pf DESTDIR=%{buildroot} XAPPLOADDIR=%{_datadir}/X11/ja/app-defaults
pushd %{buildroot}%{_bindir}
for i in netmaj knetmaj1 knetmajcl; do
  ln -s "../../usr/games/$i" $i
done
#  ln -s "../../usr/games/xnetmaj1" xnetmaj
popd

# rename duplicated file
cp -f xui/FILES xui/FILES.xui
install -c -m 0444 *.j FILES %{buildroot}%{_docdir}/netmaj-2.0.7
install -c -m 0444 html/* FILES %{buildroot}%{_docdir}/netmaj-2.0.7/html
install -c -m 0444 xui/FILES.xui xui/README.* %{buildroot}%{_docdir}/netmaj-2.0.7/xui
install -c -m 0444 xui/doc/* %{buildroot}%{_docdir}/netmaj-2.0.7/xui
install -c -m 0444 kui/COPYING %{buildroot}%{_docdir}/netmaj-2.0.7/kui
install -c -m 0444 kui/CUSTOMIZE %{buildroot}%{_docdir}/netmaj-2.0.7/kui
install -c -m 0444 kui/HISTORY %{buildroot}%{_docdir}/netmaj-2.0.7/kui
install -c -m 0444 kui/INSTALL %{buildroot}%{_docdir}/netmaj-2.0.7/kui
install -c -m 0444 kui/README %{buildroot}%{_docdir}/netmaj-2.0.7/kui
install -c -m 0444 kui/TODO %{buildroot}%{_docdir}/netmaj-2.0.7/kui
cd kui
make install DESTDIR=%{buildroot} XAPPLOADDIR=%{_datadir}/X11/ja/app-defaults

# install desktop files
mkdir -p %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE4} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE5} %{buildroot}%{_datadir}/applications/

mkdir -p %{buildroot}%{_datadir}/afterstep/start/Games/
install -m 644 %{SOURCE6} %{buildroot}%{_datadir}/afterstep/start/Games/
install -m 644 %{SOURCE7} %{buildroot}%{_datadir}/afterstep/start/Games/

# install and convert icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64,128x128}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
convert -scale 16x16 %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/maj.png
convert -scale 32x32 %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/maj.png
convert -scale 48x48 %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/maj.png
convert -scale 64x64 %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/maj.png
install -m 644 %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/maj.png
ln -s ../icons/hicolor/48x48/apps/maj.png %{buildroot}%{_datadir}/pixmaps/maj.png
convert -scale 16x16 %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
install -m 644 %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# clean up
rm -rf %{buildroot}%{_docdir}

%clean
rm -rf --preserve-root %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-,root,root)
%doc *.j FILES html xui/FILES.xui xui/README.* xui/doc
%doc kui/COPYING kui/CUSTOMIZE kui/HISTORY kui/INSTALL kui/README kui/TODO
%{_bindir}/*
%{_prefix}/games/knetmaj1
%{_prefix}/games/knetmajcl
%{_prefix}/games/netmaj
%{_prefix}/games/netmaj1
%{_prefix}/games/plview
%{_prefix}/games/xnetmaj1
%{_prefix}/games/xplview
%{_prefix}/games/lib/netmaj/server
%{_prefix}/games/lib/netmaj/client
%{_prefix}/games/lib/netmaj/auto
%{_prefix}/games/lib/netmaj/server_kill
%{_prefix}/games/lib/netmaj/player_info
%{_prefix}/games/lib/netmaj/proxyserver
%{_prefix}/games/lib/netmaj/inputserver
%{_prefix}/games/lib/netmaj/proxyclient
%{_prefix}/games/lib/netmaj/netmaj.hlp
%{_prefix}/games/lib/netmaj/auto-1.pf
%{_prefix}/games/lib/netmaj/auto-2.pf
%{_prefix}/games/lib/netmaj/auto-3.pf
%{_prefix}/games/lib/netmaj/auto-4.pf
%{_prefix}/games/lib/netmaj/auto.pf
%{_prefix}/games/lib/netmaj/server.pf
%{_prefix}/games/lib/netmaj/xclient
%{_prefix}/games/lib/netmaj/.fcc.pf
%{_prefix}/games/lib/netmaj/.momokuri.pf
%{_prefix}/games/lib/netmaj/.monkey.pf
%{_prefix}/games/lib/netmaj/.naki.pf
%{_prefix}/games/lib/netmaj/.nomi.pf
%{_prefix}/games/lib/netmaj/.puyo.pf
%{_prefix}/games/lib/netmaj/.puyo2.pf
%{_prefix}/games/lib/netmaj/.yakuman.pf
%{_datadir}/X11/ja/app-defaults/Knetmaj
%{_datadir}/applications/maj.desktop
%{_datadir}/applications/netmaj.desktop
%{_datadir}/afterstep/start/Games/maj.as
%{_datadir}/afterstep/start/Games/netmaj.as
%{_datadir}/icons/hicolor/*/apps/maj.png
%{_datadir}/icons/hicolor/*/apps/netmaj.png
%{_datadir}/pixmaps/maj.png
%{_datadir}/pixmaps/netmaj.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-32m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-31m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-30m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-29m)
- fix up desktop files again

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-28m)
- add icons for menu and fix up desktop files
- add %%post and %%postun

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-27m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-26m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-25m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-24m)
- rebuild against gcc43

* Mon Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-23m)
- no use libtermcap. 
-- update netmaj-2.0.7.jg.patch
-- remake netmaj-momonga.patch

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.7-22m)
- revised installdir

* Wed Jan 11 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.0.7-21m)
  gcc-4.1 build fix.

* Wed Mar 23 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.7-19m)
- revised desktop files

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.0.7-18m)
- rebuild against libtermcap and ncurses

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.0.7-17m)
- enable x86_64.... uumu..

* Fri Jan 21 2005 mutecat <mutecat@momonga-linux.org>
- (2.0.7.16m)
- arrange ppc remove ifnarch of Knetmaj 

* Tue Nov 23 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (2.0.7-15m)
- vararg.h is changed into stdarg.h

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.7-14m)
- revised spec for rpm 4.2.

* Tue Dec 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.7-13m)
- fix source location

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.7-9k)
- added netmaj.as and maj.as for afterstep

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.7-7k)
- imported netmaj.desktop and maj.desktop
- modified lang directory fromt ja_JP.ujis to ja

* Tue Oct 31 2000 Akira Higuchi <a@kondara.org>
- add Kondara patch
* Fri Apr 28 2000 Hironobu ABE <hiro-a@mars.dti.ne.jp>
- Changed for Vine Linux 2.0
* Wed Apr 26 2000 Akira Higuchi <a@kondara.org>
- First release


