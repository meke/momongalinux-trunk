%global momorel 1

Summary: A general purpose sound file conversion tool
Name: sox
Version: 14.4.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
Url: http://sox.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
#Patch0: sox-14.3.2-new_ffmpeg.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: flac-devel
BuildRequires: gsm-devel
BuildRequires: libid3tag-devel
BuildRequires: libvorbis-devel >= 1.0-2m
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: libao-devel >= 1.0.0

%description
SoX (Sound eXchange) is a sound file format converter for Linux, UNIX
and DOS PCs. The 'Swiss Army knife of sound tools,' SoX can convert
between many different digitized sound formats and perform simple
sound manipulation functions, including sound effects.

Install the sox package if you'd like to convert sound file formats or
manipulate some sounds.
#'

%package devel
Summary: The SoX sound file format converter libraries.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel 
This package contains the library needed for compiling applications
which will use the SoX sound file format converter.

Install sox-devel if you want to develop applications which will use
SoX.

%prep
%setup -q

#%%patch0 -p1 -b .new_ffmpeg

%build
export CFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64"
%configure --enable-flac --with-gsm --includedir=%{_includedir}/sox
%make 

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'

# get rid of la file
rm -f %{buildroot}%{_libdir}/libs*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/play
%{_bindir}/rec
%{_bindir}/sox
%{_bindir}/soxi
%{_libdir}/libsox.so.*
%{_mandir}/man1/play.1*
%{_mandir}/man1/rec.1*
%{_mandir}/man1/sox.1*
%{_mandir}/man1/soxi.1*
%{_mandir}/man7/soxeffect.7*
%{_mandir}/man7/soxformat.7*

%files devel
%defattr(-,root,root)
%{_includedir}/sox
%{_libdir}/libsox.a
%{_libdir}/libsox.so
%{_libdir}/pkgconfig/sox.pc
%{_mandir}/man3/libsox.3*

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (14.4.1-1m)
- update 14.4.1

* Wed Oct 24 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (14.4.0-1m)
- version update

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (14.3.2-3m)
- rebuild for ffmpeg-0.6.1-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (14.3.2-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (14.3.2-1m)
- update to 14.3.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (14.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (14.3.1-3m)
- full rebuild for mo7 release

* Wed May 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.3.1-2m)
- BuildRequires: libao-devel >= 1.0.0

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (14.3.1-1m)
- update to 14.3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.3.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.3.0-2m)
- re-update to 14.3.0

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (14.2.0-2m)
- back to 14.2.0

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (14.3.0-1m)
- version 14.3.0

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (14.2.0-1m)
- rebuild against ffmpeg-0.5.1-0.20090622

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (14.2.0-1m)
- version 14.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.1.0-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.1.0-2m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (14.1.0-1m)
- version 14.1.0

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (14.0.1-2m)
- add BR: libid3tag-devel

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (14.0.1-1m)
- version 14.0.1
- enable flac support
- use external gsm

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (13.0.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (13.0.0-3m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (13.0.0-2m)
- rebuild against libvorbis-1.2.0-1m

* Sun Jun 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (13.0.0-1m)
- version 13.0.0
- move libst-config from sox to sox-devel
- add %%post and %%postun
- enable parallel build
- clean up spec file

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (12.18.2-1m)
- update to 12.18.2

* Wed May 31 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (12.18.1-1m)
- update to 12.18.1
- do not apply patch0
- change configure option

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (12.17.5-1m)
- version up
- sync with FC3

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (12.17.3-5m)
- enable x86_64.

* Tue Aug 03 2004 TAKAHASHI Tamotsu <tamo>
- (12.17.3-4m)
- [SECURITY] CAN-2004-0557

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (12.17.3-3m)
- revised spec for rpm 4.2.

* Thu Oct 30 2003 zunda <zunda at freeshell.org>
- (12.17.3-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Sep 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (12.17.3-1m)
- minor bugfixes
- TEMPORARY remove '--with-alsa-dsp'

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (12.17.2-7m)
- added BuildPreReq: libvorbis >= 1.0-2m

* Fri Jun 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (12.17.2-6k)
- devel package 'Requires: %{name} = %{version}-%{release}'

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (sox-12.17-8k)
- add alphaev5 support.

* Fri Feb  2 2001 HOSONO Hidetomo <h@kondara.org>
- (sox-12.17-6k)
- deleted non-existing files: "TIPS" and "CHEAT*" in the document dir

* Thu Nov 30 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- added sox-alpha.patch

* Wed Nov 29 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 12.17
- fix Source0 URL

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat6.2 (12.16-7).

* Thu Feb 03 2000 Bill Nottingham <notting@redhat.com>
- fix manpage link the Right Way(tm)

* Thu Feb 03 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix rec manpage link - now that man pages are compressed, it should point to
  play.1.gz, not play.1

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Sep 28 1999 Bill Nottingham <notting@redhat.com>
- Grrr. Arrrrgh. Fix link.

* Fri Sep 24 1999 Bill Nottingham <notting@redhat.com>
- add some more files to devel

* Fri Sep 17 1999 Bill Nottingham <notting@redhat.com>
- fix link

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- update to 12.16

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Wed Jan 20 1999 Bill Nottingham <notting@redhat.com>
- allow spaces in filenames for play/rec

* Wed Dec  9 1998 Bill Nottingham <notting@redhat.com>
- fix docs

* Mon Nov 23 1998 Bill Nottingham <notting@redhat.com>
- update to 12.15

* Sat Oct 10 1998 Michael Maher <mike@redhat.com>
- fixed broken spec file

* Mon Jul 13 1998 Michael Maher <mike@redhat.com>
- updated source from Chris Bagwell.

* Wed Jun 23 1998 Michael Maher <mike@redhat.com>
- made patch to fix the '-e' option. BUG 580
- added buildroot

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Nov 06 1997 Erik Troan <ewt@redhat.com>
- built against glibc
