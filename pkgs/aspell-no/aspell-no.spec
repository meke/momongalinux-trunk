%global momorel 7

%define lang nb
%define langrelease 0
Summary: Norwegian dictionaries for Aspell
Name: aspell-no
Version: 0.50.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Patch0: aspell-nb-0.50.1-0.utf-filename.patch
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: Norwegian

%prep
%setup -q -n aspell-%{lang}-%{version}-%{langrelease}
%patch0 -p1 -b .utf-filename
cp bokmal.alias bokmål.alias

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT 
make install  DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.50.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.50.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.50.1-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.50.1-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.50.1-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:0.50.1-13
- Autorebuild for GCC 4.3

* Mon Sep 17 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50.1-12
- remove useles souce file

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50.1-11
- add documentation
- use configure script to create Makefile
- update default buildroot
- some minor spec changes

* Wed Jan 24 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50.1-10
- spec file cleanup
- fix 224147 - rawhide rebuild fails

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50.1-9.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50.1-9.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50.1-9.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 19 2005 Ivana Varekova <varekova@redhat.com> 50:0.50.1-9
- build with aspell-0.60.3

* Mon Apr 11 2005 Ivana Varekova <varekova@redhat.com> 50:0.50.1-8
- rebuilt

* Tue Sep 28 2004 Adrian Havill <havill@redhat.com> 50:0.50.1-7
- bump n-v-r, remove debuginfo, use "nb" lang, utf alias filename,
  add nb locale (Norsk Bokmål) support (#126690; Håvard Wigtil)

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun 23 2003 Adrian Havill <havill@redhat.com> 0.50-3
- first build for new aspell (0.50)
