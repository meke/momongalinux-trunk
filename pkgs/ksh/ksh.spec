%global momorel 1

%define       releasedate   2012-08-01

Name:         ksh
Summary:      The Original ATT Korn Shell
URL:          http://www.kornshell.com/
Group:        System Environment/Shells
License:      CPL
Version:      20120801
Release:      %{momorel}m%{?dist}
Source0:      http://www.research.att.com/~gsf/download/tgz/ast-ksh.%{releasedate}.tgz
Source1:      http://www.research.att.com/~gsf/download/tgz/INIT.%{releasedate}.tgz
Source2:      kshcomp.conf
Source3:      kshrc.rhs
Source4:      dotkshrc
#expected results of test suite
Source5:      expectedresults.log

#don't use not wanted/needed builtins
Patch1:       ksh-20070328-builtins.patch

#fix regression test suite to be usable during packagebuild - Fedora/RHEL specific
Patch2: ksh-20100826-fixregr.patch
Patch3: rmdirfix.patch
Patch4: ksh-20120801-cdfix.patch
Patch5: ksh-20120801-tabfix.patch
Patch6: ksh-20120801-cdfix2.patch
Patch7: ksh-20130214-fixkill.patch
Patch8: ksh-20120801-kshmfix.patch
Patch9: ksh-20120801-memlik.patch
Patch10: ksh-20120801-mtty.patch
Patch11: ksh-20120801-argvfix.patch
Patch12: ksh-20130628-longer.patch

# for ksh <= 2013-04-09, rhbz#960371
Patch13: ksh-20120801-lexfix.patch


BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Conflicts:    pdksh
Requires: coreutils, glibc-common, diffutils
BuildRequires: bison
Requires(post): grep, coreutils
Requires(preun): grep, coreutils
Provides: /bin/ksh

%description
KSH-93 is the most recent version of the KornShell by David Korn of
AT&T Bell Laboratories.
KornShell is a shell programming language, which is upward compatible
with "sh" (the Bourne Shell).

%prep
%setup -q -c
%setup -q -T -D -a 1
%patch1 -p1 -b .builtins
%patch2 -p1 -b .fixregr
%patch3 -p1 -b .rmdirfix
%patch4 -p1 -b .cdfix
%patch5 -p1 -b .tabfix
%patch6 -p1 -b .cdfix2
%patch7 -p1 -b .fixkill
%patch8 -p1 -b .kshmfix
%patch9 -p1 -b .memlik
%patch10 -p1 -b .mtty
%patch11 -p1 -b .argvfix
%patch12 -p1 -b .longer
%patch13 -p1 -b .lexfix

#/dev/fd test does not work because of mock
sed -i 's|ls /dev/fd|ls /proc/self/fd|' src/cmd/ksh93/features/options

%build
./bin/package "read" ||:
export CCFLAGS="$RPM_OPT_FLAGS"
export CC=gcc
./bin/package "make"

#missing in 2010-06-21, chech later if added back
#cp lib/package/LICENSES/ast LICENSE

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT{%{_bindir},%{_mandir}/man1}
install -c -m 755 arch/*/bin/ksh $RPM_BUILD_ROOT/%{_bindir}/ksh
install -c -m 755 arch/*/bin/shcomp $RPM_BUILD_ROOT%{_bindir}/shcomp
install -c -m 644 arch/*/man/man1/sh.1 $RPM_BUILD_ROOT%{_mandir}/man1/ksh.1
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/skel
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/skel/.kshrc
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/kshrc
install -D -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/binfmt.d/kshcomp.conf

%check
[ -f ./skipcheck -o -f ./../skipcheck ] && exit 0 ||:

export SHELL=$(ls $(pwd)/arch/*/bin/ksh)
cd src/cmd/ksh93/tests/
ulimit -c unlimited
if [ ! -e /dev/fd ]
then
  echo "ERROR: /dev/fd does not exist, regression tests skipped"
  exit 0
fi
$SHELL ./shtests 2>&1 | tee testresults.log
exit 0
sed -e '/begins at/d' -e '/ 0 error/d' -e 's/at [^\[]*\[/\[/' testresults.log -e '/tests skipped/d' >filteredresults.log
if ! cmp filteredresults.log %{SOURCE5} >/dev/null || ls core.*
then
  echo "Regression tests failed"
  diff -Naurp %{SOURCE5} filteredresults.log
  exit -1
fi

%post
if [ ! -f /etc/shells ]; then
        echo "/usr/bin/ksh" > /etc/shells
else
        if ! grep -q '^/usr/bin/ksh$' /etc/shells ; then
                echo "/usr/bin/ksh" >> /etc/shells
        fi
fi

/bin/systemctl try-restart systemd-binfmt.service >/dev/null 2>&1 || :

%postun
if [ ! -f /bin/ksh ]; then
        sed -i '/^\/usr\/bin\/ksh$/ d' /etc/shells
fi

%verifyscript
echo -n "Looking for ksh in /etc/shells... "
if ! grep '^/usr/bin/ksh$' /etc/shells > /dev/null; then
    echo "missing"
    echo "ksh missing from /etc/shells" >&2
else
    echo "found"
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root,-)
#%doc README LICENSE
%doc README
%{_bindir}/ksh
%{_bindir}/shcomp
%{_mandir}/man1/*
%config(noreplace) %{_sysconfdir}/skel/.kshrc
%config(noreplace) %{_sysconfdir}/kshrc
%config(noreplace) %{_sysconfdir}/binfmt.d/kshcomp.conf

%changelog
* Wed Mar 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (20120801-1m)
- update 20120801

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100621-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100621-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100621-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100621-1m)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090505-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090505-2m)
- Provides: /bin/ksh

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090505-1m)
- sync with Fedora 11 (20090505-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080725-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080725-1m)
- sync with Rawhide (20080725-4)

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20080202-1m)
- update
- merge fedora 20080202-1
- changelog is below
-
- * Mon Feb 11 2008 Tomas Smetana <tsmetana@redhat.com> 20080202-1
- - new upstream version
- 
- * Wed Jan 30 2008 Tomas Smetana <tsmetana@redhat.com> 20071105-3
- - fix #430602 - ksh segfaults after unsetting OPTIND
- 
- * Mon Jan 07 2008 Tomas Smetana <tsmetana@redhat.com> 20071105-2
- - fix #405381 - ksh will not handle $(xxx) when typeset -r IFS
- - fix #386501 - bad group in spec file
- 
- * Wed Nov 07 2007 Tomas Smetana <tsmetana@redhat.com> 20071105-1
- - new upstream version
- 
- * Wed Aug 22 2007 Tomas Smetana <tsmetana@redhat.com> 20070628-1.1
- - rebuild
- 
- * Thu Jul 12 2007 Tomas Smetana <tsmetana@redhat.com> 20070628-1
- - new upstream version
- - fix unaligned access messages (Related: #219420)
- 
- * Tue May 22 2007 Tomas Smetana <tsmetana@redhat.com> 20070328-2
- - fix wrong exit status of spawned process after SIGSTOP
- - fix building of debuginfo package, add %%{?dist} to release
- - fix handling of SIGTTOU in non-interactive shell
- - remove useless builtins
- 
- * Thu Apr 19 2007 Tomas Smetana <tsmetana@redhat.com> 20070328-1
- - new upstream source
- - fix login shell invocation (#182397)
- - fix memory leak

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20070111-2m)
- rebuild against gcc43

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (20070111-1m)
- import to Momonga from fc-devel

* Wed Feb 21 2007 Karsten Hopp <karsten@redhat.com> 20070111-1
- new upstream version
- fix invalid write in uname function

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 20060214-1.1
- rebuild

* Thu Jun 01 2006 Karsten Hopp <karsten@redhat.de> 20060214-1
- new upstream source

* Mon Feb 27 2006 Karsten Hopp <karsten@redhat.de> 20060124-3
- PreReq grep, coreutils (#182835)

* Tue Feb 14 2006 Karsten Hopp <karsten@redhat.de> 20060124-2
- make it build in chroots (#180561)

* Mon Feb 13 2006 Karsten Hopp <karsten@redhat.de> 20060124-1
- version 20060124

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 20050202-5.1
- bump again for double-long bug on ppc(64)

* Fri Feb 10 2006 Karsten Hopp <karsten@redhat.de> 20050202-5
- rebuild

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 20050202-4.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Feb 02 2006 Karsten Hopp <karsten@redhat.de> 20050202-4
- fix uname -i output
- fix loop (*-path.patch)
- conflict pdksh instead of obsoleting it

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com> 20050202-3.1
- rebuilt for new gcj

* Tue May 10 2005 Karsten Hopp <karsten@redhat.de> 20050202-3
- enable debuginfo

* Tue Mar 15 2005 Karsten Hopp <karsten@redhat.de> 20050202-2
- add /usr/bin/ksh link for compatibility with pdksh scripts (#151134)

* Wed Mar 02 2005 Karsten Hopp <karsten@redhat.de> 20050202-1 
- update and rebuild with gcc-4

* Tue Mar 01 2005 Karsten Hopp <karsten@redhat.de> 20041225-2 
- fix gcc4 build 

* Fri Jan 21 2005 Karsten Hopp <karsten@redhat.de> 20041225-1
- rebuild with new ksh tarball (license change)

* Tue Nov 02 2004 Karsten Hopp <karsten@redhat.de> 20040229-11
- disable ia64 for now

* Fri Oct 15 2004 Karsten Hopp <karsten@redhat.de> 20040229-9 
- rebuild

* Thu Sep 02 2004 Nalin Dahyabhai <nalin@redhat.com> 20040229-8
- remove '&' from summary

* Thu Sep 02 2004 Bill Nottingham <notting@redhat.com> 20040229-7
- obsolete pdksh (#131303)

* Mon Aug 02 2004 Karsten Hopp <karsten@redhat.de> 20040229-6
- obsolete ksh93, provide ksh93

* Mon Jul 05 2004 Karsten Hopp <karsten@redhat.de> 20040229-3 
- add /bin/ksh to /etc/shells

* Wed Jun 16 2004 Karsten Hopp <karsten@redhat.de> 20040229-2 
- add ppc64 patch to avoid ppc64 dot symbol problem

* Fri May 28 2004 Karsten Hopp <karsten@redhat.de> 20040229-1 
- initial version

