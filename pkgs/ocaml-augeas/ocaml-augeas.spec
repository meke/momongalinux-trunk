%global momorel 10
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-augeas
Version:        0.4
Release:        %{momorel}m%{?dist}
Summary:        OCaml bindings for Augeas configuration API

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://et.redhat.com/~rjones/augeas/files/
Source0:        http://et.redhat.com/~rjones/augeas/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  augeas-devel >= 0.10.0
BuildRequires:  chrpath


%description
Augeas is a unified system for editing arbitrary configuration
files. This provides complete OCaml bindings for Augeas.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q


%build
export CFLAGS="$RPM_OPT_FLAGS -I/usr/include/libxml2"
%configure
make
make doc


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs

# The upstream 'make install' rule is missing '*.so' and distributes
# '*.cmi' instead of just the augeas.cmi file.  Temporary fix:
#make install
ocamlfind install augeas META *.mli *.cmx *.cma *.cmxa *.a augeas.cmi *.so

strip $OCAMLFIND_DESTDIR/stublibs/dll*.so
chrpath --delete $OCAMLFIND_DESTDIR/stublibs/dll*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/ocaml/augeas
%if %opt
%exclude %{_libdir}/ocaml/augeas/*.a
%exclude %{_libdir}/ocaml/augeas/*.cmxa
%exclude %{_libdir}/ocaml/augeas/*.cmx
%endif
%exclude %{_libdir}/ocaml/augeas/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc html
%if %opt
%{_libdir}/ocaml/augeas/*.a
%{_libdir}/ocaml/augeas/*.cmxa
%{_libdir}/ocaml/augeas/*.cmx
%endif
%{_libdir}/ocaml/augeas/*.mli


%changelog
* Sat Jan  7 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4-10m)
- rebuild against augeas-0.10.0

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-9m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-1m)
- import from Fedora

* Wed May  7 2008 Richard W.M. Jones <rjones@redhat.com> - 0.4-1
- Initial RPM release.
