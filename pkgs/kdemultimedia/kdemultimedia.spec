%global momorel 1
%global kdever 4.13.1

Summary: Multimedia applications for the K Desktop Environment (KDE)
Name: kdemultimedia
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.kde.org/areas/multimedia/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: libkcddb = %{version}
Requires: libkcompactdisc = %{version}
Requires: audiocd-kio = %{version}
Requires: dragon = %{version}
Requires: ffmpegthumbs = %{version}
Requires: juk = %{version}
Requires: kmix = %{version}
Requires: kscd = %{version}
Requires: mplayerthumbs = %{version}

%description
kdemultimedia metapackage.

%package libs
Summary: Runtime libraries for %{name}
Group:   System Environment/Libraries
Requires: audiocd-kio-libs = %{version}

%description libs
%{summary}.

%package devel
Group:     Development/Libraries
Summary:   Developer files for %{name}
Requires: libkcddb-devel = %{version}
Requires: libkcompactdisc-devel = %{version}
Requires: audiocd-kio-devel = %{version}

%description devel
%{summary}.

%prep

%build

%install

%clean

%files

%files libs

%files devel

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Tue Apr 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- fix BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-5m)
- rebuild against qt-4.6.3-1m

* Thu Jun 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-4m)
- update kmix-pulse.patch from F-13

* Sat Jun  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-3m)
- replace kmix-pulse.patch to Fedora's kdemultimedia-4.4.4-kmix-pulse.patch

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-2m)
- re-import, update and apply kmix-pulse.patch from cooker, we need it

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-2m)
- update patch1 (sync with Fedora devel)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-3m)
- import patch from Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuld with ne source

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-2m)
- Obsoletes: kmediaplayer

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.62

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0,84-1m)
- minor version up for KDE 4,2 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- release %%{_kde4_appsdir}/konqsidebartng, it's provided by kdebase
- release %%{_kde4_datadir}/kde4/services/ServiceMenus, it's provided by kdelibs

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-2m)
- import new patch from Fedora 9

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- import Patch0 from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8
- enable audio file preview in konqueror

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-3m)
- rebuild against libvorbis-1.2.0-1m

* Sun Jul 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-2m)
- delete "xineVideoPlayObject.mcopclass" to avoid freeze konqueror when preview video/audio files
- modify some desktop files to be disabled to preview video files in konqueror
- these are temporaly

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- use kernel-headers, instead of glibc-kernheaders (which was OBSOLETED)

* Thu Feb 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-4m)
- rebuild with flac again
- import kdemultimedia-3.5.5+flac-1.1.3.patch from gentoo-x86-portage
 +- 21 Oct 2006; Diego Petten <flameeyes@gentoo.org>
 +- +files/kdemultimedia-3.5.5+flac-1.1.3.patch, kdemultimedia-3.5.5.ebuild:
 +- Add patch from Josh Coalson to be able to build with flac 1.1.3.

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-3m)
- rebuild without flac-1.1.4 for the moment

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against libvorbis alsa-lib libjpeg libmng xine-lib libakode lame etc.

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-3m)
- rebuild without libtunepimp
  kdemultimedia doesn't support libtunepimp >= 0.5

* Mon Nov 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- import upstream patches to fix following problems
  #119116, konqueror crashes in displaying file tooltip in icon view
  #116136, enable setting year to zero

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- rebuild against xine-lib-1.1.2-4m

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-3m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against kdelibs-3.5.4-2m
- rebuild against kdebase-3.5.4-6m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- remove merged upstream patch

* Tue Jul 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-5m)
- modify options of configure

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- fix setuid

* Wed Jun 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- rebuild against cdparanoia-alpha9.8-8m

* Fri Jun 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- Requires: libtunepimp = 0.4.1
- BuildPreReq: libtunepimp-devel = 0.4.1

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- remove merged audiocd-in-sata.patch

* Mon May 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-6m)
- modify and install kscdrc (set ledColor and BackColor)

* Sun May 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-5m)
- rebuild against libtunepimp-0.4.2

* Mon May  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-4m)
- import kdemultimedia-3.5.2-audiocd-in-sata.patch from SVN (kde.org)

* Tue Apr  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- Requires: gst-plugins-good

* Tue Apr  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.2-2m)
- rebuild against gstreamer010
- comment out Requires: gst-plugins-additional

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Sat Mar 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- build with libakode (fix short ogg issue)

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1
- BuildPreReq: libtunepimp-devel

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- BuildPreReq: gstreamer-devel
- Requres: gst-plugins-additional
- disable hidden visibility

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- update x86_64.patch
- add %%post and %%postun

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.3-2m)
- enable ia64.

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure
- BuildPreReq: glib-devel

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- remove nokdelibsuff.patch

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-2m)
- rebuild against flac-1.1.2

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- modify %%files section

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.
- enable x86_64.

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- BuildPreReq: alsa-lib-devel, libjpeg-devel, libmng-devel,
- BuildPreReq: kdebase, cdparanoia-devel

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-3m)
- rebuild with lame.

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Dec 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.1-3m)
- comment out Requires: TiMidity++

* Sat Nov 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-2m)
- rebuild against flac 1.1.1

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- Notice: In kernel-2.4.{25,26,27} environment, mpeglibs/lib/input fails to build. Build this package in kernel-2.6.x environment now.

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- following patches are removed (not appliable)
- - Patch2: kdemultimedia-2.1-glibc-2.2.2.patch
- - Patch6: kdemultimedia-2.1-wav2pat-build.patch
- use xine_artsplugin in tarball (maybe this is new enough)
- add BuildPreReq: flac-devel and taglib-devel
- modified %%files (many new files)

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Sat Dec 26 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-4m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- rebuild against for qt-3.2.3

* Sat Oct  4 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- refine %%files

* Sat Oct  4 2003 Toru Hoshina <t@momonga-linux.org>
- (3.1.4-1m)
- kaboodle cannot build.

* Wed Aug 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     Ogg Vorbis Info List view column titles were incorrect.
     kmix: Vertical label widget to sliders in replacement of large horizontal labels
     kmix: Fixed alsa 0.9x to restore current volumes
     kmix: Fixed multiple alsa 0.9x cards detection
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m

* Sun May 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-4m)
- export _POSIX2_VERSION=199209

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-3m)
- remove requires: qt-Xt

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    KsCD:
      Stopped KsCD from pausing after tracks in random mode
      Correctly associate extra CDDB information with tracks
      Support non-Latin encodings properly in CDDB entries and elsewhere
      Proper systemtray behaviour
      Updated key accel code to avoid depricated calls
    Movie previews have been removed due to severe unresolved stability problems

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-7m)
- add URL

* Fri Feb 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1-6m)
- --without-lame

* Wed Feb 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1-5m)
- undef __STRICT_ANSI__

* Fri Feb 21 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-4m)
- avoid conflict reported by Ichiro Nakai([Momonga-devel.ja:01420])

* Tue Feb 18 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-3m)
- add xine_artsplugin -- Thank you, exp (Shingo TAKEDA).

* Sun Feb 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-2m)
- del BuildPreReq: lame (lame is Nonfree)
- add BuildPreReq: xine-lib(sorry... not yet compiled.)

* Sun Feb  2 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-2m)
- rebuild against kdelibs-3.0.5a-2m.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Sun Dec  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Wed Aug 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.3-2m)
- rebuild against kdelibs-3.0.3-2m

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Sun Jul 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.2-4m)
- please rebuild with REMOVE.PLEASE

* Sat Jul 20 2002 kourin <kourin@momonga-linux.org>
- (3.0.2-3m)
- add libvorbis-devel >= 1.0-2m

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- use autoconf from autoconf-old.

* Sun Jun 09 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.0.0-8k)
- cancel gcc-3.1 autoconf-2.53

* Sat May 25 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-6k)
  applied gcc 3.1 patch

* Sun Apr 28 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-4k)
- enable build koncd.

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Mon Feb 24 2002 Shingo Akagaki <dora@kondara.org>
- (2.2.2-4k)
- just rebuild for .la fix

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- revised spec file.

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alpha2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-5k)
- rebuild against audiofile-0.2.1.

* Fri Mar 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.1.1-3k)

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.1-2k)

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-20k)
- rebuild against QT-2.3.0.

* Tue Jan 16 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-18k]
- backport 2.0.1-19k(Jirai).

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-16k]
- backport 2.0.1-17k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-14k]
- backport 2.0.1-15k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-15k]
- modifined Obsoletes tag.(remove TiMidity++).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-12k]
- backport 2.0.1-13k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-13k]
- rebuild against.

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-11k]
- rebuild against audiofile-0.2.0.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- rebuild against qt-2.2.3.

* Tue Dec 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- add kdemultimudia-2.0.1-kaiman-kondara.patch.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.4k]
- rebuild against new environment glibc-2.2 egcs++

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-0.3k]
- rebuild against qt 2.2.2.

* Fri Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- modifile Requires,BuildPrereq.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.
- add kdemultimedia-1.94-kscd-20000924.diff

* Tue Sep 26 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Tue Jun 20 2000 Kenichi Matsubara <m@kondara.org>
- initial release Kondara MNU/Linux.
