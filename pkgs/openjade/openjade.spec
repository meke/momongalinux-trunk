%global momorel 20

Summary: A DSSSL implementation
Name: openjade
Version: 1.3.2
Release: %{momorel}m%{?dist}

Source0: http://dl.sourceforge.net/sourceforge/openjade/openjade-%{version}.tar.gz
NoSource: 0
Patch0: openjade-ppc64.patch
Patch1: openjade-1.3.1-nsl.patch
Patch2: openjade-deplibs.patch
Patch3: openjade-nola.patch
Patch4: openjade-1.3.2-gcc46.patch
Patch5: openjade-getoptperl.patch
URL: http://openjade.sourceforge.net/
License: see "COPYING"
Group: Applications/Text
BuildRequires: perl
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: gettext-devel >= 0.15-9m
Requires(post): sgml-common >= 0.5
Requires(preun): sgml-common >= 0.5
Obsoletes: jade
Provides: jade
BuildRequires: opensp-devel >= 1.5.2-2m
Obsoletes: openjade-devel
Obsoletes: mathml-dtds
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
OpenJade is an implementation of the ISO/IEC 10179:1996 standard DSSSL
(Document Style Semantics and Specification Language). OpenJade is
based on James Clark's Jade implementation of DSSSL. OpenJade is a
command-line application and a set of components. The DSSSL engine
inputs an SGML or XML document and can output a variety of formats:
XML, RTF, TeX, MIF (FrameMaker), SGML, or XML.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b .deplibs
%patch3 -p1 -b .nola
%patch4 -p1 -b .gcc46
%patch5 -p1 -b .getopt

%build
# openjade-1.3.2 requires this when using gcc 4.6
export CXXFLAGS="%{optflags} -fpermissive"

%configure --disable-static --datadir=%{_datadir}/sgml/%{name}-%{version} \
	--enable-splibdir=%{_libdir}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install install-man DESTDIR=$RPM_BUILD_ROOT

# Fix up libtool libraries
find $RPM_BUILD_ROOT -name '*.la' | \
  xargs perl -p -i -e "s|-L$RPM_BUILD_DIR[\w/.-]*||g"

# oMy, othis ois osilly.
ln -s openjade $RPM_BUILD_ROOT/%{_bindir}/jade
echo ".so man1/openjade.1" > $RPM_BUILD_ROOT/%{_mandir}/man1/jade.1

# install jade/jade $RPM_BUILD_ROOT/%{prefix}/bin/jade 
cp dsssl/catalog $RPM_BUILD_ROOT/%{_datadir}/sgml/%{name}-%{version}/
cp dsssl/{dsssl,style-sheet,fot}.dtd $RPM_BUILD_ROOT/%{_datadir}/sgml/%{name}-%{version}/

# add unversioned/versioned catalog and symlink
mkdir -p $RPM_BUILD_ROOT/etc/sgml
cd $RPM_BUILD_ROOT/etc/sgml
touch %{name}-%{version}-%{release}.soc
ln -s %{name}-%{version}-%{release}.soc %{name}.soc
cd -

rm -f $RPM_BUILD_ROOT%{_libdir}/*.so $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
# remove broken entries in /etc/sgml/catalog
touch /etc/sgml/catalog
sed --in-place=.rpmsave -e '/^CATALOG.*\/etc\/sgml\/openjade.*/d' /etc/sgml/catalog

/usr/bin/xmlcatalog --sgml --noout --add \
    /etc/sgml/%{name}-%{version}-%{release}.soc \
    %{_datadir}/sgml/%{name}-%{version}/catalog >/dev/null || :
 
%preun
/usr/bin/xmlcatalog --sgml --noout --del \
    /etc/sgml/%{name}-%{version}-%{release}.soc \
    %{_datadir}/sgml/%{name}-%{version}/catalog >/dev/null || :

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc jadedoc/* dsssl/README.jadetex
%doc README COPYING VERSION
%ghost /etc/sgml/%{name}-%{version}-%{release}.soc
/etc/sgml/%{name}.soc
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/*/*
%{_datadir}/sgml/%{name}-%{version}

%changelog
* Mon Sep 16 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-20m)
- add any patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-19m)
- rebuild for new GCC 4.6

* Sun Feb  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-18m)
- add "-fpermissive" for GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-16m)
- full rebuild for mo7 release

* Wed Jun  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-15m)
- improve %%post: remove broken entries in /etc/sgml/catalog
-- this will help the user who upgrades mo6 to mo7

* Tue May  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-14m)
- fix bug in /etc/sgml/catalog handling

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-13m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.2-11m)
- Obsoletes mathml-dtds

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-9m)
- rebuild against gcc43
 
* Sun Apr 22 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-8m)
- rebuild against opensp (resolved libosp.la dependency)

* Sat Apr 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-7m)
- delete opensp
- add Patch2: openjade-deplibs.patch

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-6m)
- delete libtool library

* Mon Aug  7 2006 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPrereq: gettext-devel

* Sat Jul  1 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.2-5m)
- revise %%files to avoid conflicting with  sgml-common, xml-common

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-4m)
- add gcc-4.1 patch.
--  Patch5: opensp-1.5.1-gcc41.patch

* Fri Jun 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.3.2-3m)
- up to OpenSP-1.5.1
- change source URI

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-c++-3.4.1

* Mon Apr 12 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.2-1m)
- version 1.3.2
- sync with Fedora

* Wed Nov  5 2003 zunda <zunda at freeshell.org>
- (1.3.1-8m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Feb 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-7m)
- add BuildPrereq: gcc-c++

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-6m)
- modified Prereq: sgml-common BuildPrereq: perl

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-5m)
- Prereq: => BuildPrereq

* Fri May 24 2002 kourin <kourin@kondara.org>
- (1.3.1-4k)
- Prereq: perl-base -> perl 

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.1-2k)
- version 1.3.1

* Mon Aug 27 2001 Motonobu Ichimura <famao@kondara.org>
- (1.3-15k)
- added patch for gcc 3.0

* Fri Feb  2 2001 Daiki Matsuda <dyky@df-usa.com>
- (1.3-13k)
- build by egcs and egcs++ on Alpha

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gcc 2.96.

* Sat Aug 19 2000 Toru Hoshina <t@kondara.org>
- merge from Pinstripe.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul  4 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Wed May 31 2000 Matt Wilson <msw@redhat.com>
- fix several C++ build problems (declarations)
- build against new libstdc++

* Wed May 17 2000 Matt Wilson <msw@redhat.com>
- build with -O0 on alpha
- fix -j testing

* Thu May  5 2000 Bill Nottingham <notting@redhat.com>
- openjade is maintained, and actually builds. Let's try that.

* Thu Mar  9 2000 Bill Nottingham <notting@redhat.com>
- this package is way too huge. strip *everything*

* Mon Feb 21 2000 Matt Wilson <msw@redhat.com>
- build with CXXFLAGS="-O2 -ggdb" to work around segfault on alpha

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- strip binaries

* Wed Jan  5 2000 Bill Nottingham <notting@redhat.com>
- sanitize spec file some

* Tue Aug 17 1999 Tim Powers <timp@redhat.com>
- fixed conflict problem with sgml-tools

* Sat Jul 17 1999 Tim Powers <timp@redhat.com>
- changed buildroot path to %{_tmppath}/%{name}-%{version}-root
- rebuilt for 6.1

* Fri Apr 23 1999 Michael K. Johnson <johnsonm@redhat.com>
- quiet scripts

* Thu Apr 23 1999 Owen Taylor <otaylor@redhat.com>
- Made requires for sgml-common into prereq
