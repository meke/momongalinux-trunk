%global momorel 2

Summary:        A pitch correction LADSPA plugin
Name:           ladspa-autotalent-plugins
Version:        0.2
Release:        %{momorel}m%{?dist}

Group:          Applications/Multimedia
# The files mayer_fft.h and mayer_hht.c are from Pure Data, which is under a
# standard improved BSD license.  autotalent.c is under GPLv2+.
License:        GPLv2+ and BSD and "CC-BY-ND"
URL:            http://tombaran.info/autotalent.html
Source0:        http://tombaran.info/autotalent-%{version}.tar.gz
NoSource:       0
# Useful documentation is not included in the upstream source distribution.
Source1:        http://tombaran.info/autotalent-%{version}_refcard.pdf
NoSource:       1

BuildRequires:  ladspa-devel
Requires:       ladspa

%description
Autotalent is a real-time pitch correction plugin. You specify the notes that
a singer is allowed to hit, and Autotalent makes sure that they do. You can
also use Autotalent for more exotic effects, making your voice sound like a
chiptune, adding artificial vibrato, or messing with your formants.
Autotalent can also be used as a harmonizer that knows how to sing in the
scale with you. Or, you can use Autotalent to change the scale of a melody
between major and minor or to change the musical mode. 

%prep
%setup -q -n autotalent-%{version}
cp %{SOURCE1} .

# Use the correct Momonga optimization flags
sed -i 's|-O3|%{optflags}|' Makefile

# Use the system ladspa.h
rm ladspa.h
sed -i 's|"ladspa.h"|<ladspa.h>|' autotalent.c
sed -i 's|ladspa.h||' Makefile


%build
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install INSTALL_PLUGINS_DIR="%{buildroot}%{_libdir}/ladspa"


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README COPYING COPYING-mayer_fft autotalent-%{version}_refcard.pdf
%{_libdir}/ladspa/*



%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-2m)
- change primary site and download URIs

* Sat Aug  6 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.2-1m)
- Import from Fedora

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri May 28 2010 David Cornette <rpm@davidcornette.com> 0.2-3
- Updated to upstream package which includes the license file for the Pure Data
  source files

* Sun May 23 2010 David Cornette <rpm@davidcornette.com> 0.2-2
- Changed License: field to reflect license of the reference card pdf

* Fri May 14 2010 David Cornette <rpm@davidcornette.com> 0.2-1
- Initial build

