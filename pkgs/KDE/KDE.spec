%global momorel 1

## this parameter is same as kdeedu.spec and marble.spec
#global python_marble 1
#global touch 1

Summary: Meta package for KDE
Name: KDE
Version: 4.13.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: User Interface/Desktops
# same as kdelibs
URL: http://www.kde.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is meta package for KDE.

#####################################################################

%package core
Summary: Meta package for KDE core components
Group: User Interface/Desktops
Provides: %{name}
Obsoletes: %{name}

## these are necessary and should be install in this order
Requires: qt
Requires: phonon
Requires: kdelibs
Requires: kactivities
Requires: kdepimlibs
Requires: kde-runtime
Requires: libkworkspace
Requires: oxygen-icons
Requires: oxygen-icons-scalable

## Qt plugin modules
Requires: qt-assistant
Requires: qt-config
Requires: qt-designer-plugin-webkit

Requires: qt-ibase
Requires: qt-mysql
Requires: qt-odbc
Requires: qt-postgresql
Requires: qt-qvfb
Requires: qt-tds
Requires: qt-demos
Requires: qt-examples
Requires: qt-assistant-adp

## minimal functionality
Requires: kate
Requires: kate-libs
Requires: kate-part
Requires: pate
Requires: kwrite
Requires: kdelibs-common
Requires: kde-workspace
Requires: kde-workspace-libs
Requires: kwin-gles
Requires: kwin-gles-libs
Requires: kde-wallpapers
Requires: kde-base-artwork
Requires: kde-baseapps
Requires: kde-baseapps-common
Requires: kde-baseapps-libs
Requires: dolphin
Requires: dolphin-libs
Requires: kdialog
Requires: kdepasswd
Requires: keditbookmarks
Requires: keditbookmarks-libs
Requires: kfind
Requires: libkonq
Requires: konqueror
Requires: konqueror-libs
Requires: kde-plasma-folderview
Requires: kdeclassic-icon-theme
Requires: kdm
Requires: konsole
Requires: konsole-part
Requires: ksysguard
Requires: ksysguard-libs
Requires: ksysguardd
Requires: mono-icon-theme
Requires: kfilemetadata
Requires: baloo
Requires: baloo-libs
Requires: baloo-widgets
Requires: nepomuk-core
Requires: nepomuk-core-libs
Requires: nepomuk-widgets
Requires: nuvola-icon-theme
Requires: oxygen-cursor-themes
Requires: polkit-kde
Requires: polkit-qt

## maybe necessary for Japanese Users
Requires: kde-l10n-Japanese

## basic functionality and applications
Requires: gwenview
Requires: gwenview-libs
Requires: kamera
Requires: kcolorchooser
Requires: kde-style-phase
Requires: kdegraphics
Requires: kdegraphics-libs
Requires: kdegraphics-strigi-analyzer
Requires: kdegraphics-thumbnailers
Requires: kdeartwork
Requires: kdeartwork-screensavers
Requires: kdeartwork-kxs
Requires: kdeartwork-wallpapers
Requires: kde-runtime-flags
Requires: kde-runtime-libs
Requires: kdenetwork
Requires: kdenetwork-filesharing
Requires: kdenetwork-strigi-analyzers
Requires: zeroconf-ioslave
Requires: kget
Requires: kopete
Requires: kppp
Requires: krfb
Requires: krdc
Requires: kget-libs
Requires: kopete-libs
Requires: krfb-libs
Requires: krdc-libs
Requires: kdemultimedia
Requires: kdemultimedia-libs
Requires: libkcddb
Requires: libkcompactdisc
Requires: audiocd-kio
Requires: audiocd-kio-libs
Requires: dragon
Requires: ffmpegthumbs
Requires: juk
Requires: kmix
Requires: kscd
Requires: mplayerthumbs
Requires: kdepimlibs-akonadi
Requires: kdeadmin
Requires: kcron
Requires: ksystemlog
Requires: kuser
Requires: kdeutils
Requires: kdeutils-libs
Requires: filelight
Requires: kcharselect
Requires: kfloppy
Requires: superkaramba
Requires: superkaramba-libs
Requires: sweeper
Requires: ark
Requires: ark-libs
Requires: kcalc
Requires: kdf
Requires: kgpg
Requires: kremotecontrol
Requires: kremotecontrol-libs
Requires: ktimer
Requires: kwalletmanager
Requires: kgamma
Requires: kio_sysinfo
Requires: kolourpaint
Requires: kolourpaint-libs
Requires: kruler
Requires: ksaneplugin
Requires: ksnapshot
Requires: okular
Requires: okular-active
Requires: okular-libs
Requires: libkdcraw
Requires: libkexiv2
Requires: libkipi
Requires: libksane
Requires: soprano
Requires: svgpart

## additional functionality and applications
Requires: qt-x11
Requires: kdeedu
Requires: libkdeedu
Requires: analitza
Requires: artikulate
Requires: blinken
Requires: kalzium
Requires: kalzium-libs
Requires: kanagram
Requires: kanagram-libs
Requires: kgeography
Requires: khangman
Requires: khangman-libs
Requires: kiten
Requires: kiten-libs
Requires: klettres
Requires: kqtquickcharts
Requires: kstars
Requires: ktouch
Requires: kturtle
Requires: kwordquiz
Requires: marble
Requires: marble-common
Requires: marble-libs
Requires: marble-mobile
Requires: marble-qt
%if 0%{?touch}
Requires: marble-touch
%endif
%if 0%{?python_marble}
Requires: python-marble
%endif
Requires: kdegraphics-mobipocket
Requires: qmobipocket
Requires: parley
Requires: step
Requires: kdeedu-math
Requires: cantor
Requires: cantor-R
Requires: cantor-libs
Requires: kalgebra
Requires: kbruch
Requires: kig
Requires: kmplot
Requires: rocs
Requires: rocs-libs
Requires: kdegames
Requires: kdegames-libs
Requires: libkdegames
Requires: libkmahjongg
Requires: bomber
Requires: bovo
Requires: granatier
Requires: kajongg
Requires: kapman
Requires: katomic
Requires: kblackbox
Requires: kblocks
Requires: kbounce
Requires: kbreakout
Requires: kdiamond
Requires: kfourinline
Requires: kgoldrunner
Requires: kigo
Requires: killbots
Requires: kiriki
Requires: kjumpingcube
Requires: klickety
Requires: klines
Requires: kmahjongg
Requires: kmines
Requires: knavalbattle
Requires: knetwalk
Requires: kolf
Requires: kolf-libs
Requires: kollision
Requires: konquest
Requires: kpat
Requires: kreversi
Requires: kshisen
Requires: ksirk
Requires: ksirk-libs
Requires: ksnakeduel
Requires: kspaceduel
Requires: ksquares
Requires: ksudoku
Requires: ktuberling
Requires: kubrick
Requires: lskat
Requires: palapeli
Requires: palapeli-libs
Requires: picmi
Requires: kio_msits
Requires: print-manager
Requires: kdetoys
Requires: amor
Requires: kteatime
Requires: ktux
Requires: kdepim
Requires: kdepim-libs
Requires: kdepim-runtime
Requires: kdepim-runtime-libs
Requires: kdeaccessibility
Requires: jovie
Requires: kttsd
Requires: kaccessible
Requires: kmag
Requires: kmousetool
Requires: kmouth
Requires: kdeplasma-addons
Requires: kdeplasma-addons-libs
Requires: pairs
Requires: plasma-wallpaper-marble
Requires: plasma-applet-kimpanel
Requires: plasma-scriptengine-python
Requires: plasma-scriptengine-ruby
Requires: phonon-backend-gstreamer
# Requires: phonon-backend-xine

Requires: contour
Requires: kde-artwork-active
Requires: plasma-mobile
Requires: plasma-mobile-libs
Requires: plasma-mobile-mouse-cursor-themes
Requires: plasma-mobile-wallpapers
Requires: share-like-connect
Requires: startactive

Requires: shared-desktop-ontologies
Requires: webkitkde
Requires: webkitpart

## packages for developement
Requires: kdebindings
Requires: pykde4
Requires: pykde4-akonadi
Requires: PyQt4
Requires: qtruby
Requires: kdbg
Requires: kdiff3
Requires: kdesdk
Requires: cervisia
Requires: dolphin-plugins
Requires: kapptemplate
Requires: kcachegrind
Requires: kde-dev-scripts
Requires: kde-dev-utils
Requires: kde-dev-utils-kmtrace
Requires: kde-dev-utils-kpartloader
Requires: kde-dev-utils-kstartperf
Requires: kde-dev-utils-kuiviewer
Requires: kdesdk-kioslaves
Requires: kdesdk-strigi-analyzers
Requires: kdesdk-thumbnailers
Requires: libkomparediff2
Requires: kompare
Requires: lokalize
Requires: okteta
Requires: poxml
Requires: umbrello
Requires: kde-dev-utils-kmtrace-libs
Requires: kompare-libs
Requires: okteta-libs
Requires: kdevplatform
Requires: kdevplatform-libs
Requires: kdevelop
Requires: kdevelop-libs
Requires: kdevelop-pg-qt
Requires: kdevelop-php
Requires: kdevelop-php-docs
Requires: kdev-python
Requires: kdewebdev
Requires: kdewebdev-libs
Requires: kimono
Requires: kimono-akonadi
Requires: korundum
Requires: korundum-akonadi
Requires: kross-ruby
Requires: kross-python
Requires: qyoto
Requires: smokegen
Requires: smokekde
Requires: smokekde-akonadi
Requires: smokeqt
Requires: perlkde
Requires: perlqt
Requires: sip
Requires: akonadi
## we need KDE3 environment
Requires: qt3
Requires: qt3-MySQL
Requires: qt3-ODBC
Requires: qt3-PostgreSQL
Requires: qt3-designer
Requires: qt3-sqlite
Requires: qt3-styles
Requires: arts
Requires: arts-artsc
Requires: arts-gmcop
Requires: kdelibs3
Requires: kdebase3
Requires: kde3-i18n-Japanese
Requires: kdewebdev3

## minimum hardware support
Requires: NetworkManager-gnome
Requires: bluedevil
Requires: knemo
Requires: kde-plasma-nm
%ifnarch ia64
Requires: ksynaptics
%endif
Requires: libbluedevil
Requires: mykrandr

## for package management
Requires: kpackagekit

## independent packages for developement
Requires: kdesvn
Requires: kdesvn-svnqt

## for phonon(kdebase), amarok, kaffeine and kmplayer
Requires: xine-lib
Requires: xine-lib-alsa
Requires: xine-lib-arts
Requires: xine-lib-esd
Requires: xine-lib-flac
Requires: xine-lib-jack
Requires: xine-lib-musepack
Requires: xine-lib-oggvorbis
Requires: xine-lib-oss
Requires: xine-lib-pulseaudio
Requires: xine-lib-sdl
%ifarch %{ix86}
Requires: xine-lib-w32dll
%endif
Requires: xine-lib-xcb
Requires: xine-lib-xv

## calligra
Requires: calligra
Requires: calligra-core
Requires: calligra-libs
Requires: calligra-kdchart
Requires: calligra-author
Requires: calligra-braindump
Requires: calligra-braindump-libs
Requires: calligra-reports-map-element
Requires: calligra-words
Requires: calligra-words-libs
Requires: calligra-sheets
Requires: calligra-sheets-libs
Requires: calligra-stage
Requires: calligra-stage-libs
Requires: calligra-flow
Requires: calligra-flow-libs
Requires: calligra-karbon
Requires: calligra-karbon-libs
Requires: calligra-krita
Requires: calligra-krita-libs
Requires: calligra-kexi
Requires: calligra-kexi-libs
Requires: calligra-kexi-driver-mysql
Requires: calligra-kexi-driver-postgresql
Requires: calligra-kexi-driver-sybase
Requires: calligra-kexi-map-form-widget
Requires: calligra-kexi-spreadsheet-import
Requires: calligra-plan
Requires: calligra-plan-libs
Requires: calligra-okular-odpgenerator
Requires: calligra-semanticitems
## calligra-2.5.92 does not include Japanese translations
#Requires: calligra-l10n-Japanese

## for input method
Requires: honoka-plugin-anthy
Requires: honoka-plugin-ascii
Requires: honoka-plugin-kanainput
Requires: honoka-plugin-prime
Requires: honoka-plugin-romkan
Requires: honoka-plugin-simpleprediction
Requires: honoka-plugin-skkdic
Requires: ibus-anthy
Requires: ibus-chewing
Requires: ibus-hangul
Requires: ibus-m17n
Requires: ibus-mozc
Requires: ibus-pinyin
Requires: ibus-pinyin-open-phrase
Requires: ibus-qt
Requires: ibus-skk
Requires: ibus-tegaki
Requires: scim-anthy
Requires: scim-bridge
Requires: scim-canna
Requires: scim-chewing
Requires: scim-fcitx
Requires: scim-hangul
Requires: scim-input-pad
Requires: scim-m17n
Requires: scim-pinyin
Requires: scim-prime
Requires: scim-qtimm
Requires: scim-qtimm-qt4
Requires: scim-sinhala
Requires: scim-skk
Requires: scim-tables
Requires: scim-tomoe
Requires: uim-anthy
Requires: uim-anthy-utf8
Requires: uim-canna
Requires: uim-m17nlib
Requires: uim-prime
Requires: uim-qt
Requires: uim-qt4
Requires: uim-qtimmodule
Requires: uim-qt4immodule
Requires: uim-skk
Requires: uim-tomoe-gtk

## for printing
Requires: cups
Requires: foomatic
Requires: ghostscript
Requires: gutenprint
Requires: gutenprint-cups

## for system start up
Requires: gdm

## for gtk+ themes
Requires: gtk2-engines

## kmenu needs icons of fedora-icon-theme required by fedora-gnome-theme
## gtkrc is provided by fedora-gnome-theme
Requires: fedora-gnome-theme

Requires: xorg-x11-xinit-base

## wallpapers
Requires: homura-backgrounds-kde

## systemsettings modules
Requires: kcm-grub2
Requires: kcm-gtk
Requires: kcm_colors
Requires: kcm_devinfo
Requires: kcm_tablet
Requires: kcm_touchpad
Requires: kcmsystemd
Requires: ktp-accounts-kcm

%description core
This is meta package for KDE core components.

#####################################################################

%package extragears
Summary: Meta package for KDE official main applications
Group: User Interface/Desktops
URL: http://extragear.kde.org/
Requires: %{name}-core = %{version}-%{release}

Requires: amarok
# basket is hosted by kde.org
Requires: basket
Requires: digikam
Requires: digikam-doc
Requires: k3b
Requires: kaffeine
Requires: kaffeine-plugin
Requires: kaffeine3
Requires: katapult
Requires: kaudiocreator
Requires: kcoloredit
Requires: kcpuload
Requires: kde-plasma-ktorrent
Requires: kdetv
Requires: keurocalc
Requires: kfax
Requires: kftpgrabber
Requires: kgrab
Requires: kgraphviewer
Requires: kiconedit
Requires: kile
Requires: kio-gopher
Requires: kiosktool
Requires: kipi-plugins
Requires: kmid
Requires: kmplayer
Requires: knetstats
Requires: konq-plugins
Requires: konversation
Requires: kopete-cryptography
Requires: kphotoalbum
Requires: krecipes
Requires: ksig
Requires: kst
Requires: ktorrent
Requires: ktorrent-libs
Requires: rsibreak
Requires: skanlite
Requires: skrooge
Requires: yakuake

%description extragears
This is meta package for KDE official main applications.

#####################################################################

%package apps
Summary: Meta package for KDE applications
Group: User Interface/Desktops
URL: http://www.kde-apps.org/
Requires: %{name}-core = %{version}-%{release}

## independent
Requires: audiokonverter
Requires: choqok
Requires: falf
Requires: fotowall
Requires: gammu
Requires: grip-kde
Requires: kannadic
Requires: kanyremote
Requires: kasablanca
Requires: kbackup
Requires: kbibtex
Requires: kbilliards
Requires: kchmviewer
Requires: kcometen4
Requires: kde-plasma-discburner
Requires: kde-plasma-lastmoid
Requires: kde-plasma-lyrics
# Requires: kde-plasma-nm-vpnc
# Requires: kde-plasma-nm-openswan
# Requires: kde-plasma-nm-strongswan
# Requires: kde-plasma-nm-openconnect
# Requires: kde-plasma-nm-pptp
# Requires: kde-plasma-nm-openvpn
# Requires: kde-plasma-nm-l2tp
# Requires: kde-plasma-nm-mobile
Requires: kde-plasma-yawp
Requires: kdenlive
Requires: kdirstat
Requires: kdissert
Requires: kdocker
Requires: kflickr
Requires: kgtk
Requires: kguitar
Requires: kid3
Requires: kid3-qt
Requires: kid3-libs
Requires: kita
# Requires: kita2
Requires: klamav
Requires: kleansweep
Requires: kmediafactory
Requires: kmetronome
Requires: kmp
Requires: kmymoney
Requires: kndiswrapper
Requires: komparator
Requires: kompose
Requires: kover
Requires: koverartist
Requires: kradio
Requires: kreetingkard
Requires: krename
Requires: krusader
Requires: ksensors
Requires: kshutdown
Requires: ksshaskpass
Requires: ktechlab
Requires: ktrack
Requires: kwakeonlan
Requires: kwave
Requires: kwlan
Requires: kyum
# Requires: libopensync-plugin-kdepim
Requires: lmms
Requires: luminance
Requires: minitunes
Requires: moodbar_generator
Requires: nmapsi4
Requires: partitionmanager
Requires: playlist2cd
Requires: polyester
Requires: q4wine
Requires: qalculate-kde
Requires: qbittorrent
Requires: qdvdauthor
Requires: qjackctl
Requires: qsynth
Requires: qt-recordmydesktop
Requires: qtcurve-gtk2
Requires: qtcurve-kde4
Requires: qtpfsgui
Requires: qtractor
Requires: qtrans
Requires: qtscrob
Requires: qwit
Requires: rekonq
Requires: rosegarden
Requires: scribus
Requires: smb4k
Requires: solarsystem-kde
Requires: soundkonverter
Requires: waheela
Requires: webkam
Requires: x3f-tools-kde

## wallpapers
Requires: momonga-backgrounds-extras-kde

%description apps
This is meta package for KDE applications.

#####################################################################

%package devel
Summary: Meta package for KDE development
Group: Development/Libraries
Requires: %{name}-core = %{version}-%{release}

Requires: kdebindings-devel
Requires: pykde4-devel
Requires: PyQt4-devel
Requires: qtruby-devel
Requires: qt-devel
Requires: qt-devel-private
Requires: qt-assistant-adp-devel
Requires: qt3-devel
Requires: akonadi-devel
Requires: arts-devel
Requires: arts-artsc-devel
Requires: automoc
Requires: dbus-qt-devel
Requires: dbus-qt3-devel
Requires: gmm-devel
Requires: kfilemetadata-devel
Requires: kate-devel
Requires: kde-baseapps-devel
Requires: kde-runtime-devel
Requires: kactivities-devel
Requires: kde-workspace-devel
Requires: kdeedu-devel
Requires: libkdeedu-devel
Requires: libkdeedu-static
Requires: analitza-devel
Requires: cantor-devel
Requires: kanagram-devel
Requires: kalzium-devel
Requires: kiten-devel
Requires: khangman-devel
Requires: marble-devel
Requires: rocs-devel
Requires: kdegames-devel
Requires: libkdegames-devel
Requires: libkmahjongg-devel
Requires: palapeli-devel
Requires: kdegraphics-devel
Requires: kdelibs-devel
Requires: kdelibs3-devel
Requires: kdemultimedia-devel
Requires: libkcddb-devel
Requires: libkcompactdisc-devel
Requires: audiocd-kio-devel
Requires: kdenetwork-devel
Requires: kopete-devel
Requires: krdc-devel
Requires: kdepim-devel
Requires: kdepimlibs-devel
Requires: kdeplasma-addons-devel
Requires: kdesdk-devel
Requires: kde-dev-utils-kmtrace-devel
Requires: kde-dev-utils-devel
Requires: libkomparediff2-devel
Requires: kompare-devel
Requires: okteta-devel
Requires: kdevelop-devel
Requires: kdevelop-pg-qt-devel
Requires: kdewebdev-devel
Requires: calligra-devel
Requires: calligra-kdchart-devel
Requires: korundum-devel
Requires: libkdcraw-devel
Requires: libkexif-devel
Requires: libkexiv2-devel
Requires: libkipi-devel
Requires: libksane-devel
Requires: okular-devel
Requires: qmobipocket-devel
Requires: perlqt-devel
Requires: phonon-devel
Requires: polkit-qt-devel
Requires: qyoto-devel
Requires: sip-devel
Requires: smokegen-devel
Requires: smokekde-devel
Requires: smokeqt-devel
Requires: soprano-devel
Requires: webkitpart-devel
Requires: nepomuk-core-devel
Requires: nepomuk-widgets-devel
Requires: baloo-devel
Requires: baloo-widgets-devel

Requires: plasma-mobile-devel
Requires: share-like-connect-devel

## extragers
Requires: digikam-devel
Requires: k3b-devel
Requires: kipi-plugins-devel
Requires: kst-devel

## independent
Requires: bluedevil-devel
Requires: choqok-devel
Requires: gammu-devel
Requires: kdesvn-svnqt-devel
Requires: kftpgrabber-devel
Requires: libbluedevil-devel
Requires: scribus-devel
Requires: shared-desktop-ontologies-devel
Requires: smb4k-devel

## apidocs
Requires: kdelibs-apidocs
Requires: kdepimlibs-apidocs
Requires: soprano-apidocs

Requires: qt-doc

%description devel
This is meta package for KDE development.

#####################################################################

%package suite
Summary: Meta package for KDE suite and independent packages
Group: User Interface/Desktops
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-extragears = %{version}-%{release}
Requires: %{name}-apps = %{version}-%{release}

Requires: games-suite
Requires: graphics-suite
Requires: internet-suite
Requires: multimedia-suite
Requires: office-suite

%description suite
This is meta package for KDE suite and independent packages.

#####################################################################

%package pulseaudio
Summary: Meta package for pulseaudio support on KDE
Group: User Interface/Desktops
Requires: alsa-plugins-pulseaudio
Requires: pulseaudio
Requires: pulseaudio-module-x11

%description pulseaudio
This is meta package for pulseaudio support on KDE.

#####################################################################

%files core

%files extragears

%files apps

%files devel

%files suite

%files pulseaudio

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-2m)
- remove Requires: kdelibs3-apidocs

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC
- so many changes...

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3
- remove Requires: kwallet
- add Requires: kwalletmanager

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Sun Feb  2 2014 NARITA Kichi <pulsar@momonga-linux.org>
- (4.12.1-5m)
- remove Requires: k9copy

* Sun Feb  2 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.12.1-4m)
- remove Requires: phonon-backend-xine

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.12.1-3m)
- remove Requires: libakode libakode-devel 

* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-2m)
- add Requires: calligra-semanticitems

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0
- add Requires: qmobipocket and qmobipocket-devel
- add Requires: libkomparediff2 and libkomparediff2-devel
- remove Requires: kdeartwork-sounds

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.11.3-4m)
- move Requires: qt-doc and soprano-apidocs from core to devel

* Fri Nov 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.11.3-3m)
- add Requires: kcm-grub2
- add Requires: kcmsystemd
- add Requires: ktp-accounts-kcm
- move Requires: kdelibs-apidocs and kdepimlibs-apidocs from core to devel
- add Requires: kdelibs3-apidocs

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-2m)
- add Requires: kde-plasma-nm

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Thu Oct 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-2m)
- add Requires: kid3-qt
- add Requires: kid3-libs

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)
- many changes for KDE 4.11

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2
- add Requires: pate

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-2m)
- remove Requires: PyKDE
- remove Requires: PyKDE-devel
- remove Requires: PyKDE-examples
- remove Requires: PyQt
- remove Requires: PyQt-devel
- remove Requires: PyQt-examples

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Tue Jan 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-4m)
- revive Requires: kgraphviewer

* Tue Jan 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-3m)
- remove Requires: kgraphviewer

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-2m)
- add missing qt subpackages

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)
- add Requires: python-marble

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)
- add Requires: kgoldrunner

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-4m)
- remove Requires: xine-lib-smb

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-3m)
- revive kmymoney

* Mon Dec 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.90-2m)
- remove kmymoney for the moment

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)
- add Requires: nepomuk-widgets
- add Requires: okular-active
- add Requires: libkdegames
- add Requires: libkmahjongg
- add Requires: bomber
- add Requires: bovo
- add Requires: granatier
- add Requires: kajongg
- add Requires: kapman
- add Requires: katomic
- add Requires: kblackbox
- add Requires: kblocks
- add Requires: kbounce
- add Requires: kbreakout
- add Requires: kdiamond
- add Requires: kfourinline
- add Requires: kigo
- add Requires: killbots
- add Requires: kiriki
- add Requires: kjumpingcube
- add Requires: klickety
- add Requires: klines
- add Requires: kmahjongg
- add Requires: kmines
- add Requires: knavalbattle
- add Requires: knetwalk
- add Requires: kolf
- add Requires: kolf-libs
- add Requires: kollision
- add Requires: konquest
- add Requires: kpat
- add Requires: kreversi
- add Requires: kshisen
- add Requires: ksirk
- add Requires: ksirk-libs
- add Requires: ksnakeduel
- add Requires: kspaceduel
- add Requires: ksquares
- add Requires: ksudoku
- add Requires: ktuberling
- add Requires: kubrick
- add Requires: lskat
- add Requires: palapeli
- add Requires: palapeli-libs
- add Requires: picmi
- add Requires: print-manager
- add Requires: calligra-kdchart
- add Requires: contour
- add Requires: kde-artwork-active
- add Requires: plasma-mobile
- add Requires: plasma-mobile-libs
- add Requires: plasma-mobile-mouse-cursor-themes
- add Requires: plasma-mobile-wallpapers
- add Requires: share-like-connect
- add Requires: startactive
- add Requires: libkdegames-devel
- add Requires: libkmahjongg-devel
- add Requires: palapeli-devel
- add Requires: nepomuk-widgets-develop
- add Requires: plasma-mobile-devel
- add Requires: share-like-connect-devel
- remove Requires: kde-printer-applet
- remove Requires: system-config-printer-kde

* Mon Nov 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-2m)
- add Requires: kdev-python

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-3m)
- remove Requires: compiz-kde

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-2m)
- remove Requires: ktorrent-devel

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2
- many changes

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-2m)
- replace koffice to calligra

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-3m)
- add Reqiures: smokekde-akonadi
- add Requires: kimono-akonadi
- add Requires: korundum-akonadi

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- remove Requires: kde-plasma-ihatethecashew

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1
- remove Requires: kscope
- remove Requires: kscope-devel

* Sun Feb 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- change Requires: from natsuki-backgrounds-kde to homura-backgrounds-kde

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- rename from kdebase-runtime* to kde-runtime*
- rename ftom kdebase-workspace* to kde-workspace*
- rename from kdebase* to kde-baseapps*
- rename from mobipocket to kdegraphics-mobipocket
- rename from PyKDE4* to pykde4*
- rename from QtRuby* to qtruby*
- rename from printer-applet to kde-printer-applet
- add Requires: kcm_colors

* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-7m)
- remove Requires: ksmarttray

* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-6m)
- remove Requires: skim
- remove Requires: skim-honoka
- remove Requires: skim-scim-anthy
- remove Requires: skim-scim-canna
- remove Requires: skim-scim-hangul
- remove Requires: skim-scim-pinyin
- remove Requires: skim-scim-prime
- remove Requires: skim-scim-skk
- remove Requires: skim-scim-tables
- remove Requires: skim-devel

* Mon Jan 23 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (4.7.97-5m)
- remove Requires: kgrubeditor

* Sat Jan 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-4m)
- revive Requires: kdevelop-php
- revive Requires: kdevelop-php-docs

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-3m)
- remove Requires: kdevelop-php
- remove Requires: kdevelop-php-docs

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-2m)
- replace Requires: from kio_gopher to kio-gopher

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)
- remove Requires: kdepim-runtime-devel

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)
- so many changes...

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-3m)
- add Requires: mobipocket

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-2m)
- add Requires: libkactivities
- add Requires: libkactivities-devel

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3
- replace Requires: from kdebase-workspace-wallpapers to kde-wallpapers
- add Requires: libkworkspace
- add Requires: ksysguard
- add Requires: ksysguard-libs

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-3m)
- kdeedu metapackege

* Thu Sep 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.1-2m)
- remove Requires: kdegraphics3

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.0-2m)
- remove Requires: gutenprint-utils

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0
- so many many changes...

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-2m)
- remove Requires: qtwitter

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- remove Requires: cardinal

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Wed Feb 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.0-2m)
- remove Requires: koffice-kformula and koffice-kformula-libs from package core

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)
- remove Requires: kdebase-workspace-googlegadgets
- remove Requires: kdebase-workspace-python-applet
- add Requires: plasma-scriptengine-googlegadgets
- add Requires: plasma-scriptengine-python
- add Requires: plasma-scriptengine-ruby

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-3m)
- remove Requires: kbluetooth
- add Requires: bluedevil
- add Requires: libbluedevil
- add Requires: bluedevil-devel
- add Requires: libbluedevil-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-2m)
- add Requires: kde-plasma-discburner
- add Requires: kde-plasma-ihatethecashew
- add Requires: kde-plasma-lastmoid
- add Requires: kde-plasma-lyrics
- add Requires: kde-plasma-yawp
- add Requires: rekonq

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Tue Nov  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.2-3m)
- add Requires: choqok

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- good bye kerry

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-2m)
- modify for koffice-2.2.81

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-6m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-5m)
- add Requires: grip-kde to package apps

* Wed Aug 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-4m)
- add Requires: natsuki-backgrounds-kde to package core
- add Requires: momonga-backgrounds-extras-kde to package apps

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-3m)
- remove Requires: knetworkmanager, it may be broken

* Thu Aug 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- remove kita2 for the moment

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Aug  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- remove packages of koffice2 for the moment

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-11m)
- change Requires from knetworkmanager4 to knetworkmanager

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-10m)
- remove Requires: eureka
- remove Requires: foxkit

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-9m)
- add Requires: kcm-gtk
- remove Requires: gtk-qt-engine

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-8m)
- add Requires: qtcurve-gtk2
- add Requires: qtcurve-kde4
- add Requires: kio_gopher
- add Requires: konq-plugins
- add Requires: kopete-cryptography
- add Requires: skanlite

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-7m)
- add Requires: kmetronome

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-6m)
- add Requires: cardinal
- add Requires: kdevelop-pg-qt
- add Requires: kdevelop-pg-qt-devel
- add Requires: kdevelop-php
- add Requires: kdevelop-php-docs

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-5m)
- add Requires: minitunes

* Mon Jul 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-4m)
- remove Requires: kdebluetooth

* Mon Jul 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-3m)
- remove Requires: qtparted

* Sun Jul 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-2m)
- Requires: solarsystem-kde

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-4m)
- rebuild against qt-4.6.3-1m

* Mon Jun 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-3m)
- add Requires: kaffeine3

* Sun Jun  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-2m)
- add Requires: playlist2cd

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Sat May 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-4m)
- add Requires: honoka-plugin-anthy
- add Requires: honoka-plugin-ascii
- add Requires: honoka-plugin-kanainput
- add Requires: honoka-plugin-prime
- add Requires: honoka-plugin-romkan
- add Requires: honoka-plugin-simpleprediction
- add Requires: honoka-plugin-skkdic
- add Requires: ibus-anthy
- add Requires: ibus-chewing
- add Requires: ibus-hangul
- add Requires: ibus-m17n
- add Requires: ibus-mozc
- add Requires: ibus-pinyin
- add Requires: ibus-pinyin-open-phrase
- add Requires: ibus-qt
- add Requires: ibus-skk
- add Requires: ibus-tegaki
- add Requires: scim-anthy
- add Requires: scim-canna
- add Requires: scim-skk
- add Requires: scim-tables
- add Requires: scim-chewing
- add Requires: scim-fcitx
- add Requires: scim-hangul
- add Requires: scim-m17n
- add Requires: scim-pinyin
- add Requires: scim-prime
- add Requires: scim-sinhala
- add Requires: uim-anthy-utf8
- add Requires: uim-m17nlib
- move skim* from core to apps
- add a package pulseaudio

* Fri May 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-3m)
- add Requires: moodbar_generator

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-2m)
- add Requires: koffice-suite
- add Requires: koffice-libs
- add Requires: koffice-kchart
- add Requires: koffice-kchart-libs
- add Requires: koffice-kdchart
- add Requires: koffice-kdchart-devel
- add Requires: koffice-kexi
- add Requires: koffice-kexi-libs
- add Requires: koffice-karbon-libs
- add Requires: koffice-kformula-libs
- add Requires: koffice-kplato-libs
- add Requires: koffice-kpresenter-libs
- add Requires: koffice-krita-libs
- add Requires: koffice-kword-libs

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-4m)
- add Requires: kdevplatform-libs
- add Requires: kdevelop-libs

* Mon Apr 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-3m)
- add Requires: kmp

* Fri Apr  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-2m)
- good-bye kpowersave

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2
- add Requires: q4wine

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.1-4m)
- remove Requires: kmid-devel and kmid2

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.1-3m)
- add Requires: k3b-devel

* Fri Mar 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.1-2m)
- add Requires: qjackctl

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1
- add Requires: kdeedu-math-cantor-R
- add Requires: kdeedu-math-libs

* Mon Feb 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-3m)
- add Requires: qtscrob

* Sat Feb 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-2m)
- add Requires: waheela

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Tue Jan 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)
- add Requires: luminance

* Sat Jan 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.90-2m)
- add Requires: mykrandr

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)
- add Requires: polkit-kde
- add Requires: kde-style-phase
- add Requires: kdeedu-kstars-libs
- remove Requires: kdebase-workspace-akonadi
- remove Requires: PolicyKit-kde
- remove Requires: PolicyKit-kde-devel
- remove Requires: kdeadmin-kpackage
- remove Requires: kdeutils-devel

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)
- add Requires: kdebase-workspace-akonadi
- add Requires: webkitkde
- add Requires: webkitpart
- add Requires: webkitpart-devel
- add Requires: shared-desktop-ontologies

* Sun Dec 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- add Requires: kde-plasma-ktorrent
- add Requires: ktorrent-libs
- add Requires: ktorrent-devel

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- add Requires: kdeaccessibility-libs
- add Requires: kdeartwork-wallpapers
- add Requires: kdeedu-marble-libs
- add Requires: kdeutils-libs
- add Requires: kdeutils-devel
- add Requires: shared-desktop-ontologies-devel
- remove Requires: kdelibs-experimental
- remove Requires: kdelibs-experimental-devel
- remove Requires: kdegraphics in devel section

* Wed Dec  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.4-3m)
- change Requires: from devinfo to kcm_devinfo

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-2m)
- add Requires: phonon-backend-xine

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4
- add Requires: qtpfsgui

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-6m)
- add Requires: kmid2

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.3-5m)
- add Requires: uim-qt4

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.3-4m)
- add Requires: qtwitter

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.3-2m)
- add Requires: kcm_tablet

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Sun Nov  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-9m)
- add Requires: kcm_touchpad

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-8m)
- revive Requires: koffice-kformula

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-7m)
- move devinfo from core to apps

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-6m)
- add Requires: devinfo

* Sun Oct 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-5m)
- remove Requires: koffice-kchart, kexi, kformula, kivio and kugar for the moment

* Thu Oct 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-4m)
- add Requires: x3f-tools-kde

* Tue Oct 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-3m)
- move kdebluetooth from core to extragears
- add Requires: kpackagekit

* Sun Oct 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-2m)
- add Requires: skim-devel

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2
- add Requires: kdeplasma-addons-libs
- add Requires: kdeplasma-addons-devel

* Sun Sep 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.1-2m)
- add Requires: kbluetooth

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sun Jul 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.96-4m)
- remove Requires: knetworkmanager and Requires: NetworkManager-gnome

* Mon Jul 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.96-3m)
- remove Requires: openoffice.org-kde

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.96-2m)
- remove Requires: kdebluetooth4

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Sun Jul  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-2m)
- add Requires: dbus-qt3-devel
- add Requires: kdevelop-devel

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)
- add Requires: kdepim-runtime
- add Requires: kdepim-runtime-libs
- add Requires: kdepim-runtime-devel

* Tue Jun 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.91-4m)
- add Requires: kaudiocreator

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.91-3m)
- remove Requires: ghostscript-resource

* Sun Jun 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.91-2m)
- add Requires: qtractor

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)
- add Requires: kio_sysinfo
- add Requires: qt-demos
- add Requires: qt-examples
- add Requires: soprano
- add Requires: soprano-apidocs
- add Requires: soprano-devel
- add Requires: polkit-qt
- add Requires: polkit-qt-devel
- remove Requires: qt-sqlite

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89
- add Requires: kdelibs-eperimental
- add Requires: kdelibs-eperimental-devel
- add Requires: nuvola-icon-theme
- add Requires: kdevplatform
- add Requires: PolicyKit-kde
- add Requires: PolicyKit-kde-devel

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-2m)
- add Requires: PyKDE4-akonadi

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.89-1m)
- - update to 4.2.89
- - add Requires: PolicyKit-kde
- - add Requires: PolicyKit-kde-devel

- * Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88
- - add Requires: kdelibs-eperimental
- - add Requires: kdelibs-eperimental-devel
- - add Requires: nuvola-icon-theme
- - add Requires: kdevplatform

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4
- add Requires: kdelibs-apidocs
- add Requires: kdepimlibs-pidocs
- add Requires: kdepimlibs-akonadi
- add Requires: kdebase-runtime-libs
- add Requires: kdebase-workspace-googlegadgets
- add Requires: kdebase-workspace-python-applet
- add Requires: kdm
- add Requires: kdeclassic-icon-theme
- add Requires: system-config-printer-kde

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88
- - add Requires: kdevplatform

- * Fri May 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Fri May 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.3-3m)
- move kdebluetooth from core to extragears
- move kdebluetooth4 from experimental hardware support to minimum hardware support
- move knetworkmanager from core to extragears
- move kpowersave from core to apps
- move kwlan from core to apps

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.3-2m)
- add Requires: kscope-devel

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - add Requires: kdelibs-apidocs
- - add Requires: kdelibs-eperimental
- - add Requires: kdelibs-eperimental-devel
- - add Requires: kdepimlibs-pidocs
- - add Requires: kdepimlibs-akonadi
- - add Requires: kdebase-runtime-libs
- - add Requires: kdebase-workspace-googlegadgets
- - add Requires: kdebase-workspace-python-applet
- - add Requires: kdm
- - add Requires: kdeclassic-icon-theme
- - add Requires: nuvola-icon-theme
- - add Requires: system-config-printer-kde

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3
- add Requires: kio_msits
- add Requires: kdeutils-printer-applet

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-3m)
- move skrooge from apps to extragears
- add Requires: kmediafactory

- * Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-2m)
- - add Requires: PolicyKit-kde
- - add Requires: polkit-qt
- - add Requires: polkit-qt-devel

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- revive Requires: oxygen-icon-theme
- revive Requires: oxygen-icon-theme-scalable

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- remove Requires: kdepimlibs-apidoc
- remove Requires: oxygen-icon-theme
- remove Requires: oxygen-icon-theme-scalable
- remove Requires: kdeartwork-extras
- remove Requires: kdeartwork-icons
- add Requires: kdeclassic-icon-theme
- add Requires: kdeartwork-screensavers
- add Requires: kdeartwork-sounds
- add Requires: kdeedu-marble
- add Requires: kimono
- add Requires: qyoto
- add Requires: qyoto-devel
- add Requires: php-qt
- add Requires: php-qt-devel

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.1-3m)
- add Requires: fotowall

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.1-2m)
- add Requires: kwakeonlan

- * Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.67-1m)
- - update to 4.2.67
- - remove Requires: oxygen-icon-theme
- - remove Requires: oxygen-icon-theme-scalable
- - add Requires: oxygen-icons

- * Thu Mar 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.66-1m)
- - update to 4.2.66

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- add Requires: kross-falcon

* Thu Mar  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-8m)
- add Requires: qbittorrent

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-7m)
- add Requires: skrooge

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-6m)
- add Requires: qwit

* Mon Mar  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-5m)
- add Requires: kradio

* Fri Feb 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-4m)
- add Requires: knetworkmanager4

* Sat Feb 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-3m)
- add Requires: qtrans

* Fri Feb  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-2m)
- remove Requires: kdebindings3

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- add Requires: korundum
- add Requires: QtRuby
- add Requires: dum-devel
- add Requires: QtRuby-devel
- add Requires: kross-ruby
- add Requires: kross-python

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Wed Jan 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.87-2m)
- add Requires: partitionmanager

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-3m)
- modify for new digikam
- add Requires: digikam-devel
- add Requires: kipi-plugins-devel
- remove Requires: kipi-plugins4
- remove Requires: kipi-plugins4-devel
- remove Requires: libkdcraw-devel
- remove Requires: libkexiv2-devel
- remove Requires: libkipi-devel

* Sun Dec 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-2m)
- remove Require: kmediaplayer, kmediaplayer was obsoleted

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.80-2m)
- remove Requires: anp

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80
- remove Requires: kdelibs-apidocs
- add Requires: kdelibs-common
- add Requires: kdeedu-math
- add Requires: kdebase-workspace-wallpapers

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Tue Nov  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.71-2m)
- update for Amarok 2

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69
- revive Requires: kftpgrabber-devel

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-9m)
- change Requires: from phonon-backend-gst to phonon-backend-gstreamer

- * Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.69-1m)
- - update to 4.1.69

- * Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.68-1m)
- - update to 4.1.68

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-8m)
- add Requires: kdegraphics3

- * Mon Sep 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.67-2m)
- - remove Requires: kftpgrabber-devel (obsoleted)

- * Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.67-1m)
- - update to 4.1.67

- * Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.66-1m)
- - update to 4.1.66

* Sun Sep 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-7m)
- enable Requires: kdebluetooth4

* Tue Sep  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-6m)
- add Requires: kndiswrapper

- * Fri Sep  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.65-1m)
- - update to 4.1.65

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-5m)
- switch from kcometen3 to kcometen4

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Fri Aug 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62
- - change Requires: from phonon-backend-gst to phonon-backend-gstreamer

- * Fri Aug 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

* Thu Aug 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-4m)
- add a comment # Requires: kdebluetooth4

* Fri Aug  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-3m)
- move Requires: kdelibs-apidocs from KDE-core to KDE-devel
- move Requires: kdepimlibs-apidocs from KDE-core to KDE-devel

* Sun Aug  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- add Requires: kdebindings3
- add Requires: kdewebdev3

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-2m)
- add Requires: phonon-backend-gst

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99
- remove Requires: kdeplasmoids
- add Requires: kdeplasma-addons
- add Requires: PyKDE4
- add Requires: PyKDE4-devel

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-4m)
- add Requires: qsynth

* Thu Jul 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-3m)
- add Requires: eureka

* Wed Jul  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.85-2m)
- add Requires: qalculate-kde

* Sun Jul  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85
- add Requires: gmm-devel to devel package

* Fri Jul  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.84-1m)
- add Requires: kipi-plugins4

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2
- remove Requires: extragear-plasma
- add Requires: kdeplasmoids
- add Requires: phonon
- add Requires: akonadi
- add Requires: phonon-devel
- add Requires: automoc
- add Requires: akonadi-devel

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-7m)
- remove Requires: crystalsvg-icon-theme

* Sat Jun 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-6m)
- add Requires: foxkit

* Sun Jun  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-5m)
- add Requires: qt-recordmydesktop

* Thu Jun  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-4m)
- add Requires: kita2 again

* Tue Jun  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-3m)
- add Requires: kanyremote

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- remove Requires: libopensync-plugin-kdepim for the moment

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1
- add Requires: kdebase
- add Requires: kdebase-libs
- add Requires: kdelibs-apidocs
- add Requires: kdepimlibs-apidocs
- add Requires: crystalsvg-icon-theme
- add Requires: kdeedu-kstars
- add Requires: kdepim-libs
- add Requires: kdewebdev-libs
- add Requires: kdepim-devel
- add Requires: kdewebdev-devel
- delete Requires: kdeartwork-icons-crystalsvg
- delete Requires: dragonplayer
- delete Requires: kaider
- delete Requires: kmobiletools

* Fri May 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-9m)
- add Requires: ksmarttray

* Fri May 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-8m)
- add Requires: knetworkmanager again

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-7m)
- add Requires: kgrab
- add Requires: nmapsi4

* Fri May 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-6m)
- change Requires from bluecurve-icon-theme to fedora-gnome-theme

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.4-5m)
- add Requires: kiconedit
- add ifarch ix86 and x86_64 to kgrubeditor

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-4m)
- change Requires from redhat-artwork to bluecurve-icon-theme

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-3m)
- add Requires: kflickr
- add Requires: kst

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-2m)
- add Requires: kmid
- add Requires: ksig
- add Requires: kscope

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- rebuild against qt3 and qt-4.4.0-1m

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-5m)
- revive Requires: amarok-libvisual

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-4m)
- revive Requires: koffice-kugar
- revive Requires: koffice-l10n-Japanese
- revive Requires: amarok-yauap
- revive Requires: amarokFS
- revive Requires: anp

* Tue Apr 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-3m)
- add Requires: scim-bridge

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-2m)
- re-split packages

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- revive kdebindings and kdebindings-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-5m)
- rebuild against gcc43

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-4m)
- add Requires: scim-qtimm-qt4

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-3m)
- remove Requires: ksmoothdock
- add Requires: webkam

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-2m)
- remove Requires: qtruby for the moment

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to KDE 4.0.2

* Mon Mar  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-5m)
- remove Requires: ghostscript-cups

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-4m)
- add Requires: koffice-kspread again

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-3m)
- remove Requires: amarokFS

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- remove Requires: kickpim

* Sat Feb 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to KDE 4.0.1

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-6m)
- add Requires: kita2
- add Requires: tastymenu

* Sun Feb 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-5m)
- add Requires: falf

* Wed Feb  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-4m)
- add printing support for Momonga Linux 4plus
- add Requires: cups
- add Requires: foomatic
- add Requires: ghostscript
- add Requires: ghostscript-cups
- add Requires: ghostscript-resource
- add Requires: gimp-print
- add Requires: gimp-print-cups
- add Requires: gimp-print-utils

* Tue Jan 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-3m)
- add Requires: anp
- add Requires: kdenlive

* Sat Oct 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-2m)
- remove Requires: fusion-icon-qt3

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-13m)
- add Requires: komparator

* Mon Aug 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-12m)
- add Requires: ksplash-engine-moodin

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-11m)
- add Requires: kicker-compiz
- add Requires: koverartist

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-10m)
- add Requires: kgraphviewer

* Sat Jul  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-9m)
- change Requires from fusion-icon to fusion-icon-qt3

* Sat Jul  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-8m)
- add Requires: fusion-icon

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-7m)
- add Requires: compiz-kde
- remove Requires: pykompiz

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-6m)
- add Requires: gammu
- add Requires: gammu-devel

* Wed Jun 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-5m)
- add Requires: gtk2-engines

* Thu Jun 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-4m)
- add Requires: digikam-doc

* Thu Jun 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- remove Requires: digikamimageplugins, it's merged into digikam

* Sun Jun  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- add Requires: libopensync-plugin-kdepim

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7

* Mon May 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-41m)
- remove Requires: beryl-aquamarine

* Fri May 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-40m)
- add Requires: libkdcraw-devel

* Thu May 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-39m)
- add Requires: kwave

* Thu May 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-38m)
- add Requires: kmplayer

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-37m)
- add Requires: gdm

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-36m)
- add Requires: kdebluetooth

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-35m)
- add Requires: kwlan

* Wed Apr 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-34m)
- add Requires: openoffice.org-kde

* Tue Apr 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-33m)
- add a package KDE-suite
- add Requires: kaffeine-plugin

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-32m)
- add Requires: uim-anthy
- add Requires: uim-canna
- add Requires: uim-prime
- add Requires: uim-skk

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-31m)
- add Requires: xine-lib-xcb again
- add Requires: xine-lib-musepack

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-30m)
- remove Requires: xine-lib-xcb for the moment

* Fri Apr 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-29m)
- add Requires: kiosktool

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-28m)
- add Requires: lmms
- add Requires: qdvdauthor
- add Requires: soundkonverter

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-27m)
- add Requires: scim-input-pad

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-26m)
- add Requires: skim-scim-pinyin
- add Requires: skim-scim-tables

* Wed Apr 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-25m)
- add Requires: audiokonveter
- add Requires: kima
- add Requires: ksmoothdock

* Wed Apr 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-24m)
- add Requires: skim-scim-hangul

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-23m)
- add Requires: xine-lib-pulseaudio
- add Requires: xine-lib-sdl
- add Requires: xine-lib-smb
- add Requires: xine-lib-xcb

* Tue Apr  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-22m)
- remove Requires: k9copy-devel

* Tue Apr  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-21m)
- add Requires: beryl-aquamarine

* Sun Apr  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-20m)
- add Requires: rsibreak

* Sun Apr  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-19m)
- add Requires: gtk-qt-engine
- add Requires: polyester

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-18m)
- add Requires: basket
- add Requires: kalgebra
- add Requires: kbibtex
- add Requires: kchmviewer
- add Requires: kile

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-17m)
- add Requires: ksshaskpass
- add Requires: ktechlab
- add Requires: ktrack

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-16m)
- aad Requires: kcometen3
- add Requires: kdirstat
- add Requires: kdocker
- add Requires: keurocalc
- add Requires: kgtk
- add Requires: kickpim
- add Requires: kid3
- add Requires: knetstats
- add Requires: kover
- add Requires: kpowersave
- add Requires: kshutdown

* Fri Mar 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-15m)
- add Requires: k9copy-devel
- add Requires: scim-tomoe
- add Requires: uim-tomoe-gtk

* Thu Mar 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-14m)
- add Requires: k9copy

* Wed Mar 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-13m)
- add Requires: ksudoku

* Wed Mar 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-12m)
- add Requires: ksynaptics

* Mon Mar 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-11m)
- add Requires: amarokFS
- add Requires: kbilliards
- add Requires: kdetv
- add Requires: kdiff3
- add Requires: kdissert

* Mon Mar 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-10m)
- add Requires: katapult
- add Requires: krusader

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-9m)
- add Requires: kbackup
- add Requires: kleansweep
- add Requires: kmobiletools
- add Requires: kmymoney
- add Requires: knemo
- add Requires: kphotoalbum
- add Requires: qt-MySQL
- add Requires: qt-ODBC
- add Requires: qt-PostgreSQL
- add Requires: qt-sqlite
- add Requires: dbus-qt-devel

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-8m)
- add Requires: knetworkmanager
- add Requires: krecipes
- add Requires: ksensors
- add Requires: smb4k
- add Requires: smb4k-devel

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-7m)
- add Requires: klamav

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-6m)
- add Requires: PyKDE-examples

* Wed Mar  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-5m)
- add Requires: kyum

* Tue Mar  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-4m)
- add Requires: kdesvn-kiosvn
- add Requires: kguitar

* Sun Feb 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-3m)
- add Requires: libkexiv2-devel

* Mon Feb  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-2m)
- remove Requires: amarok-xmms

* Sat Feb  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-6m)
- add Requires: amarok-yauap

* Sun Dec 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-5m)
- add Requires: xine-lib-jack

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- add Requires: PyKDE
- add Requires: PyKDE-devel
- add Requires: PyQt-devel
- add Requires: PyQt-examples
- add Requires: sip-devel

* Wed Nov 15 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-3m)
- add Requires: ktorrent

* Sat Oct 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- replace Requires: kfish with Requires: kaquarium

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Sun Oct  1 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.4-3m)
- add Requires: kftpgrabber
- add Requires: kftpgrabber-devel

* Mon Sep 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-2m)
- add Requires: kdesvn
- add Requires: kdesvn-svnqt
- add Requires: kdesvn-svnqt-devel
- add Requires: konversation

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Sun Jul 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-7m)
- add Requires: kasablanca

* Mon Jul  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-6m)
- remove Requires: amarok-gstreamer

* Fri Jun 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-5m)
- add Requires: pykompiz

* Tue Jun 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- add Requires: yakuake

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- add Requires: amarok-gstreamer again

* Thu Jun  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- revise Requires for ia64 and x86_64
- remove BuildArchitectures: noarch

* Thu Jun  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- version update to 3.5.3
- add Requires: kerry
- add Requires: scribus-devel

* Thu May 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-5m)
- remove Requires: amarok-gstreamer (not yet stable)

* Wed May 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-4m)
- add Requires: qtparted
- add Requires: scribus

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.2-3m)
- change xinitrc -> xorg-x11-xinit

* Fri Apr 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-2m)
- add Requires: sylpheed
 - sylpheed is required by kickerrc of kdebase
 - gtk+ is required even if it does not demand sylpheed

* Tue Apr  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- version update to 3.5.2
- add amarok-gstreamer again (gstreamer010, welcome)

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-5m)
- add Requires: libakode, libakode-devel

* Fri Mar 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-4m)
- modify for koffice-1.5-beta2

* Mon Mar  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-3m)
- remove Requires: amarok-gstreamer (wait for GStreamer-0.10)

* Wed Mar  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- add Requires: krename

* Sun Feb  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- version update to 3.5.1
- add Requires: skim-honoka
- add Requires: skim-scim-anthy
- add Requires: skim-scim-canna
- add Requires: skim-scim-prime
- add Requires: skim-scim-skk

* Fri Oct  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add a package KDE-devel

* Fri Sep 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- version update to 3.4.2
- add Requires: amarok-helix
- add Requires: digikam
- add Requires: digikamimageplugins
- add Requires: kipi-plugins
- add Requires: redhat-artwork
- Requires: koffice-i18n-Japanese -> koffice-l10n-Japanese
- Requires: uim-kdehelper -> uim-qt
- Requires: uim-qt -> uim-qtimmodule

* Wed Mar  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-2m)
- add Requires: amarok-libvisual
- add Requires: kannadic
- add Requires: kompose
- add Requires: kreetingkard
- add Requires: rosegarden
- add Requires: xine-lib
- add Requires: xine-lib-alsa
- add Requires: xine-lib-arts
- add Requires: xine-lib-esd
- add Requires: xine-lib-flac
- add Requires: xine-lib-oggvorbis
- add Requires: xine-lib-oss
- add Requires: xine-lib-xv
- add Requires: xine-lib-w32dll
- add Requires: skim
- add Requires: scim-qtimm
- add Requires: uim-qt
- add Requires: uim-kdehelper

* Sun Jan  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-1m)
- version update to 3.3.2
- add Requires: PyQt
- add Requires: amarok
- add Requires: k3b
- add Requires: kita
- add Requires: qtruby
- add Requires: sip
- Requires: quanta -> kdewebdev

* Sat May 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.2-2m)
- revise for only require packages from kde.org and extragear.kde.org (kaffeine is also included)

* Tue May 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-1m)
- version update to 3.2.2
