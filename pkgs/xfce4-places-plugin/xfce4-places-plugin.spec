%global momorel 1

%global xfce4ver 4.10.0
%global thunarver 1.4.0
%global major 1.6

Name:		xfce4-places-plugin
Version:	1.6.0
Release:	%{momorel}m%{?dist}
Summary:	Places menu for the Xfce panel

Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource: 	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	libxfce4ui-devel >= %{xfce4ver}
BuildRequires:	exo-devel >= 0.5.0
BuildRequires:	libxml2-devel, gettext
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
A menu with quick access to folders, documents, and removable media. The 
Places plugin brings much of the functionality of GNOME's Places menu to 
Xfce. It puts a simple button on the panel. Clicking on this button opens up 
a menu with 4 sections:
1) System-defined directories (home folder, trash, desktop, file system)
2) Removable media (using thunar-vfs)
3) User-defined bookmarks (reads ~/.gtk-bookmarks)
4) Recent documents submenu (requires GTK v2.10 or greater) 

%prep
%setup -q

%build
%configure --disable-static LIBS="-lX11"
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

# remove la file
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# make sure debuginfo is generated properly
chmod -c +x %{buildroot}%{_libdir}/xfce4/panel/plugins/*.so

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/xfce4-popup-places
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop


%changelog
* Sun Jan  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0
- remove Patch0: xfce4-places-plugin-1.4.0-icon-naming.patch

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.0-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-5m)
- update
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- update
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-5m)
- fix build

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-4m)
- good-bye autoreconf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- add autoreconf
-- need libtool-2.2.x

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- update and fix %%files
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- import to Momonga from fedora

* Wed Nov 07 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.10-1
- Update to 0.0.10

* Sat Aug 25 2007 Christoph Wickert <fedora christoph-wickert de> - 0.9.992-1
- Update to 0.9.992

* Sat Aug 25 2007 Christoph Wickert <fedora christoph-wickert de> - 0.9.991-1
- Update to 0.9.991
- Update license tag

* Thu May 17 2007 Christoph Wickert <fedora christoph-wickert de> - 0.3.0-1
- Update to 0.3.0

* Sun Apr 29 2007 Christoph Wickert <fedora christoph-wickert de> - 0.2.0-1
- Update to 0.2.0

* Sun Apr 08 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-1
- Initial package.
