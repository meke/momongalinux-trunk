%global momorel 21

Name:           perl-WWW-YouTube
Version:        2008.0728
Release:        %{momorel}m%{?dist}
Summary:        YouTube Development Interface (YTDI)
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/WWW-YouTube/
Source0:        http://www.cpan.org/modules/by-module/WWW/WWW-YouTube-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.1
BuildRequires:	perl-AppConfig-Std
BuildRequires:  perl-DBI
BuildRequires:  perl-Frontier-RPC
BuildRequires:  perl-HTML-Tree
BuildRequires:  perl-IO-Zlib
BuildRequires:  perl-Module-Build
BuildRequires:  perl-SQL-Statement
BuildRequires:  perl-String-Approx
BuildRequires:  perl-Test-Memory-Cycle
BuildRequires:  perl-Test-Pod
BuildRequires:  perl-Test-Taint
BuildRequires:  perl-Test-Warn
BuildRequires:  perl-Time-Date
BuildRequires:  perl-XML-Dumper
BuildRequires:  perl-XML-Parser
BuildRequires:	perl-XML-TreeBuilder
BuildRequires:  perl-libwww-perl
Requires:	perl-AppConfig-Std
Requires:       perl-DBI
Requires:       perl-Frontier-RPC
Requires:       perl-HTML-Tree
Requires:       perl-IO-Zlib
Requires:       perl-Module-Build
Requires:       perl-SQL-Statement
Requires:       perl-String-Approx
Requires:       perl-Test-Memory-Cycle
Requires:       perl-Test-Pod
Requires:       perl-Test-Taint
Requires:       perl-Test-Warn
Requires:       perl-Time-Date
Requires:       perl-XML-Dumper
Requires:       perl-XML-Parser
Requires:	perl-XML-TreeBuilder
Requires:       perl-libwww-perl
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
YouTube Development Interface (YTDI)

%prep
%setup -q -n WWW-YouTube-%{version}

## adjust require
for file in `grep -r "require WWW::YouTube::GData" * | cut -d : -f 1`; do
    mv $file $file.org
    sed s/"require WWW::YouTube::GData"/"##require WWW::YouTube::GData"/g $file.org > $file
    rm -f $file.org
done

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
./Build test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING README
%{perl_vendorlib}/WWW/YouTube*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2008.0728-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2008.0728-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2008.0728-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2008.0728-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2008.0728-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0728-1m)
- update to 2008.0728

* Tue Jun 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0623-1m)
- update to 2008.0623
- add BuildRequires: and Requires: perl-AppConfig-Std

* Wed Jun 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0610-1m)
- update to 2008.0610

* Sat Jun  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2008.0606-1m)
- update to 2008.0606
- add BuildRequires: perl-XML-TreeBuilder and Requires: perl-XML-TreeBuilder

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2006.0626-2m)
- rebuild against gcc43

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2006.0626-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
