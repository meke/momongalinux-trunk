%global momorel 21

Summary: Japanese Console for Linux Frame Buffer Device
Name: jfbterm
Version: 0.4.7
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/System
URL: http://jfbterm.sourceforge.jp/
Source: http://dl.sourceforge.jp/%{name}/13501/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.4.6-conf.patch
Patch1: %{name}-0.4.7-remove-sticky.patch
Patch2: %{name}-0.4.6-x86_64.patch
Patch3: %{name}-%{version}-infinite_loop.patch
Patch5: %{name}-%{version}-userspace.patch
Patch10: %{name}-%{version}-remove-warning.patch
Patch11: %{name}-%{version}-mmap-newkernel.patch
Patch12: %{name}-%{version}-hang-onexit.patch
Patch13: %{name}-%{version}-pagemask_userspace.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: coreutils
Requires: grep
Requires: ncurses
Requires: pam
Requires: fontpackages-filesystem
BuildRequires: efont-unicode-bdf >= 0.4.2-5m
BuildRequires: gzip
BuildRequires: japanese-bitmap-fonts
BuildRequires: jisksp16-1990-fonts
BuildRequires: ncurses
BuildRequires: xorg-x11-fonts-misc >= 7.5-3m
BuildRequires: xorg-x11-server-Xvfb

%description
JFBTERM/ME takes advantages of framebuffer device that is 
supported since linux kernel 2.2.x (at least on ix86 architecture) 
and make it enable to display multilingual text on console. 
It is developed on ix86 architecture, and it will works on 
other architectures such as linux/ppc.

Features:
   * It works with framebuffer device instead of VGA.
   * It supports pcf format font
   * It is not so fast because it doesn't take any advantages 
     of accelaration.
   * It also support coding systems other than ISO-2022, 
     such as SHIFT-JIS by using iconv(3).
   * It is userland program.
#'

%prep
%setup -q

%patch0 -p1 -b .conf
%patch1 -p1 -b .remove_sticky
%patch5 -p1 -b .userspace
%patch3 -p1 -b .infinite_loop
%patch10 -p1 -b .remove_warn
%patch11 -p1 -b .nmap_newkernel
%patch12 -p1 -b .hang_onexit
%patch13 -p1 -b .pagemask

touch Makefile.in aclocal.m4 config.h.in configure stamp-h.in

%build
%ifarch ia64
%configure --enable-direct-color --enable-vga16fb=no
%else
%configure --enable-direct-color
%endif

touch stamp-h

%make

tic -C terminfo.jfbterm > jfbterm.termcap

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_datadir}/fonts/jfbterm

%makeinstall

mv %{buildroot}%{_sysconfdir}/jfbterm.conf.sample %{buildroot}%{_sysconfdir}/jfbterm.conf
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/man5
install -m 644 jfbterm.1 %{buildroot}%{_mandir}/man1
install -m 644 jfbterm.conf.5 %{buildroot}%{_mandir}/man5

cp -p %{_datadir}/fonts/efont-unicode/b16.pcf.gz %{buildroot}%{_datadir}/fonts/jfbterm/

for font in \
   shnm8x16r.pcf.gz shnmk16.pcf.gz jisksp16-1990.pcf.gz \
   8x16.pcf.gz gb16fs.pcf.gz hanglg16.pcf.gz \
   ; do
   status=1
   for path in \
      %{_datadir}/fonts/jisksp16-1990 \
      %{_datadir}/fonts/japanese-bitmap \
      %{_datadir}/X11/fonts/misc \
       ; do
      if [ -f $path/$font -a $status = 1 ] ; then
         cp -p $path/$font %{buildroot}%{_datadir}/fonts/jfbterm/
         status=0
         break
      fi
   done
   if [ $status = 1 ] ; then exit 1 ; fi
done

status=1
for num in `seq 1 15` ; do
   font=8x13-ISO8859-${num}.pcf.gz
   path=%{_datadir}/X11/fonts/misc
   if [ -f $path/$font ] ; then
    cp -p $path/$font %{buildroot}%{_datadir}/fonts/jfbterm/
    status=0
   fi
done
if [ $status = 1 ] ; then exit 1 ; fi

cat > 60-jfbterm.perms <<EOF
# permission definitions
<console> 0660 /dev/tty0    0660 root
<console> 0600 /dev/console 0600 root
EOF

mkdir -p -m 755 %{buildroot}%{_sysconfdir}/security/console.perms.d
install -m 644 60-jfbterm.perms %{buildroot}%{_sysconfdir}/security/console.perms.d/

# Change documents' fonts to UTF-8
sed -i -e 's|\r||' AUTHORS

for f in AUTHORS ChangeLog ; do
   mv ${f} ${f}.orig
   iconv -f ISO-2022-JP -t UTF8 ${f}.orig > ${f} && \
   rm -f ${f}.orig || mv ${f}.orig ${f}
done
mv README.ja README.ja.orig
iconv -f EUCJP -t UTF8 README.ja.orig > README.ja && \
   rm -f README.ja.orig || mv README.ja.orig README.ja

# fix up permission
chmod 755 %{buildroot}%{_bindir}/jfbterm

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README*
%doc jfbterm.termcap
%config(noreplace) %{_sysconfdir}/jfbterm.conf
%config(noreplace) %{_sysconfdir}/security/console.perms.d/60-jfbterm.perms
%{_bindir}/jfbterm
%{_datadir}/fonts/jfbterm
%{_mandir}/man1/jfbterm.1*
%{_mandir}/man5/jfbterm.conf.5*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.7-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.7-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.7-19m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-18m)
- use BuildRequires and Requires

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.7-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.7-16m)
- BuildPreReq: efont-unicode-bdf >= 0.4.2-5m
- BuildPreReq: xorg-x11-fonts-misc >= 7.5-3m
- revise japanese-bitmap-fonts
- remove BuildPreReq: xorg-x11-fonts-base
- remove BuildPreReq: fonts-japanese

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.7-15m)
- build fix (efont-unicode path)

* Tue May  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-14m)
- fix spec file for new fonts

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-13m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.7-12m)
- rebuild against rpm-4.6

* Sat Apr 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-11m)
- merge Fedora's changes
- welcome home, jfbterm
 +* Tue Apr  1 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.7-16
 +- Remove asm/page.h include, replaced by using sysconf
 +
 +* Sat Feb  9 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp>
 +- Rebuild against gcc43
 +
 +* Mon Dec 17 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.7-15
 +- Supress gcc warning on 64 bits
 +
 +* Sun Dec 16 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.7-14
 +- Remove previous workaround patch for glibc >= 2.7.90
 +- Remove unneeded autoconf call
 +
 +* Mon Dec  3 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.7-13
 +- Add BR: jisksp16-1990-fonts due to fonts-japanese split
 +- Workarround for bug 408731
 +
 +* Wed Aug 22 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.7-12
 +- Use sysconf instead of kernel-private PAGE_SIZE macro
 +
 +* Wed Aug 22 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.7-10.dist.2
 +- Mass rebuild (buildID or binutils issue)
 +
 +* Sun Dec 24 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-10
 +- Properly own directories to remove ncurses dependency
 +- Remove terminfo on FC7+ (bug 220193)
 +
 +* Mon Aug 28 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-9
 +- Really copy font files, not use symlink to get rid of X requirement.
 +  (This package is aimed for CUI use, so X requirement is
 +   unwilling)
 +
 +* Mon Aug 28 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-8.1
 +- Rebuild for mass rebuild and kernel-headers
 +  (glibc-kernheaders removed).
 +
 +* Sun Aug 20 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-7
 +- Fix compilation problem on ppc.
 +
 +* Sun Aug 20 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-6
 +- Really require fonts as they are symlinks.
 +
 +* Sun Aug 20 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-5
 +- Install fonts required by relative symlinks.
 +
 +* Thu Aug 15 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-4
 +- Change the font search path.
 +
 +* Thu Aug 10 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-3
 +- Another attempt to remove sticky bit.
 +- Move the entry where we copy fonts needed.
 +
 +* Thu Aug 10 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-2
 +- Fix man page entry.
 +
 +* Thu Aug 10 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-1
 +- Strict dist to fc5 and above.
 +- Split efont-unicode-bdf to another rpm.
 +
 +* Tue Aug  1 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-0.15
 +- Clean up spec file and make some cosmetic change.
 +- Specify the correct licence.
 +
 +* Tue Jul 25 2006 MACHIDA Hideki <h-machida@jc-c.co.jp> 0.4.7-0.9.1
 +- FIX: fc1 - fc3 font pathes.
 +- add console.perms file for not use sticky bit (fc4 or later).
 +
 +* Tue Jul 25 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-0.9
 +- Remove gcc compilation warning.
 +- Suppress mmap warning for linux >= 2.6.12 (this code is dead, perhaps?)
 +- Workarround for occasional hang on exit.
 +- Change Japanese documents coding to UTF-8.
 +
 +* Tue Jul 25 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 0.4.7-0.1
 +- Initial package for fc6 and fc5, based on srpm by Hideki Machida, initially
 +  by momonga linux project.
 +
 +* Fri Mar 24 2006 MACHIDA Hideki <h@matchy.net> 0.4.7-matchy4
 +- for FedoraCore-5
 +
 +* Tue Jun 14 2005 MACHIDA Hideki <h@matchy.net> 0.4.7-matchy3
 +- for FedoraCore-4
 +
 +* Wed May 18 2005 MACHIDA Hideki <h@matchy.net> 0.4.7-matchy2
 +- add jfbterm-0.4.6-x86_64.patch and jfbterm-0.4.7-infinite_loop.patch
 +- from 0.4.7-1m (momonga-linux).
 +
 +* Thu Feb 25 2005 MACHIDA Hideki <h@matchy.net> 0.4.7-matchy1
 +- update to 0.4.7.
 +
 +* Thu Jan 27 2005 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy6
 +- for release.
 +
 +* Wed Jan 19 2005 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy5.2
 +- add BuildPreReq: automake14, autoconf.
 +
 +* Wed Jan 19 2005 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy5
 +- use %%dist macro.
 +
 +* Wed Jan 19 2005 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy4
 +- add BuildPreReqs jisksp16-1990.
 +
 +* Fri Dec 31 2004 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy3
 +- fix debug package (Makefile patch).
 +
 +* Wed Dec 29 2004 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy2
 +- fix Packager and ChangeLog (^-^;)
 +
 +* Tue Dec 28 2004 MACHIDA Hideki <h@matchy.net> 0.4.6-matchy1
 +- for FedoraCore-3 (from momonga-linux)
 +- add efont-unicode
 +- use tic.
- thanks, Mamoru Tasaka-san and MACHIDA Hideki-san

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-10m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.7-9m)
- rebuild against ncurses-5.6-10m(Obso termcap)

* Sat Jun 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.7-8m)
- revised spec for fonts-japanese

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.7-7m)
- enable ppc, ppc64

* Tue Aug  1 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.7-6m)
- disable jfbterm-0.4.6-x86_64.patch

* Mon Jul 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-5m)
- add jfbterm-0.4.7-use-sys-io.patch for new glibc-kernheaders

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-4m)
- change xorg-x11 -> xorg-x11-server-Xvfb

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.7-3m)
- revise for xorg-7.0
- change install dir

* Wed Oct 19 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.7-2m)
- enable ia64

* Sat Mar 26 2005 TAKAHASHI Tamotsu <tamo>
- (0.4.7-1m)
- [SECURITY] a malicious config file could execute arbitrary code
- apply debian's jfbterm_0.4.7-2.diff to fix a screen lock problem

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.6-2m)
- enable x86_64.
  add jfbterm-0.4.6-x86_64.patch

* Wed Dec 15 2004 TAKAHASHI Tamotsu <tamo>
- (0.4.6-1m)

* Tue Apr  6 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.3-2m)
- not enumerate all of font file name.

* Tue Sep 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.3-1m)
- bugfixes

* Mon Sep 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.2-1m)
- include font files

* Thu May 29 2003 Shingo Akagaki <droa@kitty.dnsalias.org>
- (0.3.12-1m)
- version 0.3.12

* Fri Feb 15 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.3.10-20k)
- update Source0 URL

* Tue May 22 2001 Toru Hoshina <toru@df-usa.com>
- (0.3.10-18k)

* Tue May  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.3.10-16k)
- add termcap to PreReq tag

* Sun May  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.3.10-14k)
- add PreReq tag for %post section

* Tue Apr 17 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- applied gcc296 patch

* Sun Apr 15 2001 Toru Hoshina <toru@df-usa.com>
- revised spec file.
- add ppc support.
* Fri Oct 20 2000 Toru Hoshina <toru@df-usa.com>
- *Req*:tag never use ABS path

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Apr 25 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- up to 0.3.10

* Wed Dec 01 1999 Motonobu Ichimura <famao@kondara.org>
- up to 0.3.7 

* Wed Nov 17 1999 Motonobu Ichimura <famao@kondara.org>
- up to 0.2.3

* Fri Nov 12 1999 Motonobu Ichimura <famao@kondara.org>
- up to 0.2.2

* Mon Nov 08 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Oct 23 1999 Motonobu Ichimura <famao@kondara.org>
- removed termcap and some changes added.
- not use terminfo.kon but terminfo.jfbterm

* Sat Oct 02 1999 Motonobu Ichimura <g95j0116@mn.waseda.ac.jp>
- first release
