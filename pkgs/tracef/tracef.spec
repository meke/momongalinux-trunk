%global momorel 30
%global boost_version 1.55.0

Summary: function call tracer
Name: tracef
Version: 0.16
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Development/Debuggers
URL: http://binary.nahi.to/hogetrace/
Source0: %{name}-%{version}.tar.gz
Patch0:  tracef-0.16-build-fix.patch
Patch1: tracef-0.16-gcc44.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ >= 3.4.0
BuildRequires: libstdc++-devel binutils elfutils-libelf-devel
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: libdwarf-devel >= 0.20100629
BuildRequires: binutils-devel
Requires: libstdc++ elfutils-libelf boost

%description

%prep
%setup -q -n %{name}-%{version}

%patch0 -p1 -b .build
%patch1 -p1 -b .gcc44~

%build
#CXXFLAGS="-fstack-protector --param=ssp-buffer-size=4 -Wp,-D_FORTIFY_SOURCE=2" \
#LDFLAGS="-Wl,-z,relro -Wl,-z,now"  
LDFLAGS="-lz -ldl" \
CXXFLAGS="$RPM_OPT_FLAGS -I/usr/include" ./configure
cd src
%make
cd ..

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
cd src
cp tracef %{buildroot}/usr/bin
cd ..

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
/usr/bin/tracef


%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-30m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-29m)
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-28m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.16-27m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-26m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-25m)
- rebuild for boost

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-24m)
- delete "BuildRequires: binutils-static"

* Mon Jul 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-23m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-22m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-21m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-20m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-19m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-18m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-17m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16-16m)
- rebuild against libdwarf-0.20100808

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-15m)
- rebuild against libdwarf-0.20100629

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-14m)
- rebuild against boost-1.43.0

* Mon May  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-13m)
- add "BuildRequires: binutils-static"

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-12m)
- use BuildRequires

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-11m)
- rebuild against boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-9m)
- rebuild against boost-1.40.0

* Sun Jul 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.16-8m)
- rebuild against libdwarf

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-7m)
- rebuilt for boost-1.39.0

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-5m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-4m)
- rebuild against boost-1.37.0

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv3+

* Wed Oct 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-2m)
- add "LDFLAGS=-lz" for binutils-2.18.50.0.9
- revise spec
 
* Thu Apr 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- Initial commit Momonga Linux

* Mon Sep 17 2007 SATO Yusuke <ads01002 at nifty.com>
- Initial build

