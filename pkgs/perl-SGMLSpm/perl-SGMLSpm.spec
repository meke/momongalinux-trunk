%global         momorel 10

Name:           perl-SGMLSpm
Version:        1.1
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Perl library for parsing the output of nsgmls.
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SGMLS/
Source0:        http://www.cpan.org/authors/id/R/RA/RAAB/SGMLSpm-%{version}.tar.gz
NoSource:       0
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl-Test-Simple
Requires:       openjade
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Perl programs can use the SGMLSpm module to help convert SGML, HTML or XML
documents into new formats.

%prep
%setup -q -n SGMLSpm-%{version}
find . -type f | xargs chmod 644
find . -type d | xargs chmod 755

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

# see momonga's BTS #402
mv %{buildroot}/%{_bindir}/sgmlspl.pl %{buildroot}/%{_bindir}/sgmlspl

%check
%if %{do_test}
make test
%endif

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc BUGS COPYING ChangeLog DOC/ README TODO elisp
%{_bindir}/sgmlspl
%{perl_vendorlib}/SGMLS.pm
%{perl_vendorlib}/SGMLS
%{perl_vendorlib}/sgmlspl-specs
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-8m)
- reuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-3m)
- rebuild against perl-5.16.0

* Thu Nov  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.1-2m)
- fix momonga BTS #402 issue
-- rename sgmlspl.pl as sgmlspl for backward compatibility

* Sat Oct 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.1-1m)
- update to 1.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-31m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-30m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-29m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03ii-28m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03ii-27m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-26m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03ii-25m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-24m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-23m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03ii-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03ii-21m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03ii-20m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03ii-19m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.03ii-18m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.03ii-17m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.03ii-16m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.03ii-15m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.03ii-14m)
- remove Epoch from BuildRequires

* Sun Apr 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.03ii-13m)
- revise specfile and permissions of docdir

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03ii-12m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.03ii-11m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.03ii-10m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.03ii-9m)
- rebuild against perl-5.8.0

* Wed Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.03ii-8m)
- source url change

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.03ii-6k)
- rebuild against for perl-5.6.1

* Sun Jan 14 2001 Tim Waugh <twaugh@redhat.com>
- Add defattr to files section.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Change group.
- rm before install.
- Change Copyright: to License:.
- Remove Packager: line.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Based on Eric Bischoff's new-trials packages.
