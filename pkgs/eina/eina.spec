%global momorel 1
#%%global snapdate 2010-06-27

Summary: The EFL Data Type Library
Name: eina
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.enlightenment.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig, doxygen
Obsoletes: edb edb-ed edb-devel
Obsoletes: engrave-devel engrave
Obsoletes: epeg epeg-devel

%description
Eina is a data type library.

%package devel
Summary: Eina headers, static libraries, documentation and test programs
Group: System Environment/Libraries
Requires: %{name} = %{version}

%description devel
Headers, static libraries, test programs and documentation for Eina

%prep
%setup -q

%build
%configure --disable-static 
# Remove Rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool 
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall 
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';' 

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%dir %{_includedir}/eina-1
%{_includedir}/eina-1/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49539-1m)
- update to new svn snap

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.2.062-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2.062-1m)
- update to new svn snap

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.0.2.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.0.2.060-1m)
- import to momonga

