%global momorel 4
%global abi_ver 1.9.1

# Generated from tokyocabinet-1.26.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname tokyocabinet
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Tokyo Cabinet: a modern implementation of DBM
Name: rubygem-%{gemname}
Version: 1.31
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://fallabs.com/tokyocabinet/
# 1. get http://1978th.net/tokyocabinet/rubypkg/tokyocabinet-ruby-xxx.tar.gz
# 2. tar zxvf tokyocabinet-ruby-xxx.tar.gz
# 3. gem build tokyocabinet.gemspec
Source0: %{gemname}-%{version}.gem
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
BuildRequires: tokyocabinet >= 1.4.45
Provides: rubygem(%{gemname}) = %{version}

%description
Tokyo Cabinet is a library of routines for managing a database.  The database
is a simple data file containing records, each is a pair of a key and a value.
Every key and value is serial bytes with variable length.  Both binary data
and character string can be used as a key and a value.  There is neither
concept of data tables nor data types.  Records are organized in hash table,
B+ tree, or fixed-length array.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Aug 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.31-4m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.31-1m)
- update 1.31

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.30-2m)
- rebuild against ruby-1.9.2

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.30-1m)
- update 1.30

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.29-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.29-1m)
- update 1.29

* Wed Jul 15 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-2m)
- add BuildRequires

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.26-1m)
- Initial package for Momonga Linux
