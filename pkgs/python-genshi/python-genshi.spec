%global momorel 1

%{!?python_sitelib: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-genshi
Version:        0.6
Release:        %{momorel}m%{?dist}
Summary:        Toolkit for stream-based generation of output for the web

Group:          Development/Languages
License:        BSD
URL:            http://genshi.edgewall.org/

Source0:        http://ftp.edgewall.com/pub/genshi/Genshi-%{version}.tar.gz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools >= 0.6c9-2m

%description
Genshi is a Python library that provides an integrated set of
components for parsing, generating, and processing HTML, XML or other
textual content for output generation on the web. The major feature is
a template language, which is heavily inspired by Kid.

%prep
%setup -q -n Genshi-%{version}

find examples -type f | xargs chmod a-x

%build
%{__python} setup.py --with-speedups build

%install
rm -rf %{buildroot}
%{__python} setup.py --with-speedups install -O1 --skip-build --root %{buildroot}

## with python-2.7, tests will fail
## http://genshi.edgewall.org/ticket/500
## http://genshi.edgewall.org/ticket/501
#check
#{__python} setup.py test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING doc examples README.txt 
%{python_sitearch}/*

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update to 0.6
- with python-2.7, disable tests

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against python-2.6.2
-- add workaround to avoid FAIL: Doctest: genshi.template.eval.Undefined
-- this failure occurs on python-2.6.2 which has certain specification change

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1
-- import Patch1 from Fedora 11 (0.5.1-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-3m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.4.4-1m)
- initial import

* Tue Aug 28 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.4-2
- BR python-setuptools-devel

* Mon Aug 27 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.4-1
- Update to 0.4.4

* Mon Jul  9 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.2-2
- BR python-setuptools so that egg-info files get installed.  Fixes #247430.

* Thu Jun 21 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.2-1
- Update to 0.4.2

* Sat Jun  9 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.1-1
- Update to 0.4.1

* Wed Apr 18 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.0-1
- Update to 0.4.0

* Thu Apr 12 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.3.6-1
- First version for Fedora Extras

