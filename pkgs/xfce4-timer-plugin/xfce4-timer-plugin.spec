%global momorel 1

%global xfce4ver 4.11.0
%global major 1.6

Name:		xfce4-timer-plugin
Version:	1.6.0
Release:	%{momorel}m%{?dist}
Summary:	Timer for the Xfce panel

Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel, gettext, perl-XML-Parser
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
A timer for the Xfce panel. It supports countdown periods and alarms at 
certain times.


%prep
%setup -q


%build
%configure --disable-static
%make


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_libexecdir}/xfce4/panel-plugins/xfce4-timer
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/icons/hicolor/48x48/apps/xfce4-timer.png

%changelog
* Thu Jun  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0
- build against xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-9m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-8m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-5m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-4m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.1-2m)
- define __libtoolize (build fix)

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-4m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-1m)
- import to Momonga from fc

* Sat Jan 27 2007 Christoph Wickert <fedora christoph-wickert de> - 0.5.1-1
- Update to 0.5.1 on Xfce 4.4.

* Sat Sep 23 2006 Christoph Wickert <fedora christoph-wickert de> - 0.5-1
- Initial Fedora Extras version.
