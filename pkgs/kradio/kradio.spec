%global momorel 3
%global srcname kradio4
%global qtver 4.8.5
%global kdever 4.12.1
%global kdelibsrel 2m

Summary: Comfortable radio application for KDE
Name: kradio
Version: 4.0.7
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.kde-apps.org/content/show.php/KRadio4+for+KDE4.x?content=28097
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{srcname}-%{version}.tar.bz2
NoSource: 0
Source1: README.momonga
Patch0: %{srcname}-4.0.4-desktop.patch
Patch1: %{srcname}-4.0.0-rc2-dbus-link.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: vorbis-tools
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: alsa-lib-devel
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: dbus-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: lame-devel
BuildRequires: libogg-devel
BuildRequires: libsndfile-devel
BuildRequires: libvorbis-devel
BuildRequires: lirc-devel

%description
Comfortable V4L/V4L2-Radio Application for KDE

KRadio is a comfortable radio application for KDE with support for 
V4L and V4L2 radio cards drivers.

KRadio currently provides:

* V4L/V4L2 Radio support
* RDS support
* PVR support
* Remote Control support (LIRC)
* Alarms, Sleep Countdown
* Several GUI Controls (Docking Menu, Station Quickbar, Radio Display)
* Timeshifter Capability
* Recording Capabilities (mp3, ogg/vorbis, wav, ...)
* Extendable Plugin Architecture

This Package also includes a growing collection of station preset
files for many cities around the world contributed by KRadio Users.

As KRadio is based on an extendable plugin architecture, contributions
of new plugins (e.g. Internet Radio Streams, new cool GUIs) are welcome.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .link-dbus

install -m 644 %{SOURCE1} .

%build
# workaround for ffmpeg
%global optflags %{optflags} `pkg-config libavcodec --cflags`

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# clean up
rm -rf %{buildroot}%{_docdir}/%{srcname}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog FAQ INSTALL README* REQUIREMENTS TODO*
%{_kde4_bindir}/%{name}4
%{_kde4_bindir}/%{name}4-convert-presets
%{_kde4_libdir}/%{name}4
%{_kde4_datadir}/applications/kde4/%{name}4.desktop
%{_kde4_appsdir}/%{name}4
%{_kde4_iconsdir}/*color/*/*/%{name}*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}4*.mo
%{_datadir}/pixmaps/%{name}4.png

%changelog
* Sun Jan 26 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.12.1-3m)
- [BUILD FIX] set kdelibsrel to 2m

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.7-2m)
- rebuild against ffmpeg

* Sun Jan 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.7-1m)
- update to 4.0.7

* Fri Aug 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.6-1m)
- update to 4.0.6

* Mon Aug  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.5-1m)
- update to 4.0.5

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-3m)
- rebuild with new ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.2-2m)
- rebuild for new GCC 4.6

* Sat Jan  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.1-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Mon Sep 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-0.2.1m)
- version 4.0.1-rc2

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-0.1.1m)
- version 4.0.1-rc1

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-5m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-2m)
- version 4.0.0
- update desktop.patch

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1.5.2m)
- move Source0 to sf.net

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1.5.1m)
- update to version 4.0.0-rc4

* Sun Apr 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1.4.1m)
- update to version 4.0.0-rc3
- remove merged gcc44.patch

* Sat Apr 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1.3.1m)
- update to version 4.0.0-rc2
- add dbus-link.patch and ffmpeg-headers.patch

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1.2.1m)
- update to version 4.0.0-rc1

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1.1.1m)
- update to version 4.0.0-beta2

* Tue Mar 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-0.20090308.1m)
- update to 20090308 snapshot

* Wed Mar  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-0.20090301.2m)
- apply gcc44 patch

* Mon Mar  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-0.20090301.1m)
- initial package for radio freaks using Momonga Linux
