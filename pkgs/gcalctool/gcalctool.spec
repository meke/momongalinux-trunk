%global momorel 1

Summary: the calculator application
Name: gcalctool
Version: 6.6.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.gnome.org/
Group: Applications/System
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/6.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: atk-devel >= 1.26.0
BuildRequires: gtk3-devel
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: rarian

Requires(pre): GConf
Requires(pre): rarian

%description
This is the calculator application that was in the
OpenWindows Deskset of the Solaris 8 operating system. Sun Microsystems Inc.
have kindly given me permission to release it.

It incorporates a multiple precision arithmetic packages based on the work
of Professor Richard Brent, who has also kindly given me permission to make
it available.

%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    --disable-scrollkeeper \
    --disable-static
    
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files 
%defattr(-, root, root)
%{_bindir}/gcalctool
%{_bindir}/gcalccmd
%{_bindir}/gnome-calculator
%{_datadir}/%{name}
%doc %{_datadir}/help/*/*
%{_datadir}/locale/*/*/*
%{_datadir}/applications/*.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gcalctool.gschema.xml
%{_mandir}/man1/gcalctool.1.*
%{_mandir}/man1/gcalccmd.1.*

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.6.1-1m)
- update to 6.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6.0-1m)
- update to 6.6.0

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5.92-1m)
- update to 6.5.92

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.2.0-2m)
- rebuild for glib 2.33.2

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.2.0-1m)
- update to 6.2.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.1.5-1m)
- update to 6.1.5

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.0.2-1m)
- update to 6.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.0.1-1m)
- update to 6.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.32.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.32.2-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.32.2-1m)
- update to 5.32.2

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.32.1-1m)
- update to 5.32.1

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.32.0-2m)
- add glib-compile-schemas script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.32.0-1m)
- update to 5.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.30.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.30.2-1m)
- update to 5.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.30.1-1m)
- update to 5.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.30.0-1m)
- update to 5.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.29.91-1m)
- update to 5.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.29.90-1m)
- update to 5.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.28.2-2m)
- delete __libtoolize hack

* Wed Dec 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.28.2-1m)
- update to 5.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.28.1-1m)
- update to 5.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.28.0-1m)
- update to 5.28.0
- delete patch0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.26.3-1m)
- update to 5.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.26.2-1m)
- update to 5.26.2

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.26.1-1m)
- update to 5.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.26.0-1m)
- update to 5.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.25.92-1m)
- update to 5.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.25.91-1m)
- update to 5.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.25.90-1m)
- update to 5.25.90

* Wed Jan 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.24.3.1-1m)
- update to 5.24.3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.24.3-1m)
- update to 5.24.3

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.24.2-1m)
- update to 5.24.2

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.24.1-1m)
- update to 5.24.1

* Wed Oct  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.24.0-1m)
- update to 5.24.0

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.22.3-2m)
- change %%preun script

* Sun Jun 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.22.3-1m)
- update to 5.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.22.2-1m)
- update to 5.22.2

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.22.1-2m)
- fix desktop

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.22.1-1m)
- update to 5.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.22.0-1m)
- update to 5.22.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.20.2-2m)
- %%NoSource -> NoSource

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.20.2-1m)
- update to 5.20.2

* Tue Oct  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.20.1-1m)
- update to 5.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.20.0-1m)
- update to 5.20.0

* Wed Apr 18 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (5.9.14-1m)
- update to 5.9.14

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.13-1m)
- update to 2.9.13

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.12-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.12-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.12-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.12-1m)
- update to 5.9.12 (unstable)

* Fri Nov  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.8.25-1m)
- update to 5.8.25

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.8.24-1m)
- update to 5.8.24

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.8.19-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.8.19-1m)
- update to 5.8.19

* Mon Jun 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.7.32-4m)
- rewind 5.7.32 (5.8.13 for GNOME 2.15.X, sorry)

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.8.13-1m)
- update to 5.8.13

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.7.32-3m)
- revise %%files for rpm-4.4.2

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.7.32-2m)
- rebuild against openssl-0.9.8a

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.7.32-1m)
- update to 5.7.32

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.6.31-1m)
- version up
- GNOME 2.12.1 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.4.20-1m)
- version 4.4.20
- GNOME 2.8 Desktop

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (4.3.51-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.3.51-1m)
- version 4.3.51
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (4.3.38-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.3.38-1m)
- version 4.3.38

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.3.16-1m)
- version 4.3.16

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.3.15-1m)
- version 4.3.15

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (4.3.3-1m)
- version 4.3.3

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.3.0-1m)
- version 4.3.0

* Sat Aug 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.103-1m)
- version 4.2.103

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.99-1m)
- version 4.2.99

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.86-1m)
- version 4.2.86

* Thu May 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.83-1m)
- version 4.2.83

* Wed Mar 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.77-1m)
- version 4.2.77

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.1.9-2m)
- rebuild against for XFree86-4.3.0

* Sat Aug 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.1.9-1m)
- version 4.1.9

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.1.0-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.1.0-2m)
- add URL tag

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.1.0-1m)
- create
