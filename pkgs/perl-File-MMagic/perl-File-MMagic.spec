%global         momorel 4

Name:           perl-File-MMagic
Version:        1.30
Release:        %{momorel}m%{?dist}
Summary:        Guess file type
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-MMagic/
Source0:        http://www.cpan.org/authors/id/K/KN/KNOK/File-MMagic-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
checktype_filename(), checktype_filehandle() and checktype_contents returns
string contains file type with MIME mediatype format.

%prep
%setup -q -n File-MMagic-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README*
%{perl_vendorlib}/File/MMagic.pm
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-2m)
- rebuild against perl-5.18.1

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-2m)
- rebuild against perl-5.16.1

* Thu Jul 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.27-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.27-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.27-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.27-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.27-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.27-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.27-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.27-2m)
- use vendor

* Fri Jun  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.27-1m)
- update to 1.27

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.26-1m)
- update to 1.26

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.25-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.25-1m)
- update to 1.25

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.22-4m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.22-3m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.22-2m)
- remove Epoch from BuildRequires

* Mon Mar 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.22-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.20-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.20-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Sep  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.20-1m)

* Tue Mar  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.17-1m)

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.16-2m)
- rebuild against perl-5.8.0

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.16-1m)

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.13-4k)
- rebuild against for perl-5.6.1

* Mon Jul  2 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.13-2k)
- add documents

* Tue Nov 28 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- remove BuildArchitectures TAG. This package included
  architecture dependent directory.

* Thu Oct 26 2000 Kazuhiko <kazuhiko@kondara.org>
- (1.09-1k)
- BuildArchitectures: noarch

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.600.

* Thu Apr 06 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 1.06.

* Tue Mar 28 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 1.02.

* Sat Mar 11 2000 Motonobu Ichimura <famao@kondara.org>
- fix .packlist problem

* Wed Mar 01 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 1.01.

* Thu Dec 30 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Kondara Adaptation.

* Sat Dec 11 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Rebuild for RHL-6.1.

* Thu Dec 02 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Specified Obsoletes: File-MMagic.

* Wed Dec 01 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Fixed package name (Added prefix `perl-').

* Thu Sep 09 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- First build.
