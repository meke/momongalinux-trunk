%global momorel 1
%global upstream_version 20110511

Summary: Mobile broadband provider database
Name: mobile-broadband-provider-info
Version: 1.%{upstream_version}
Release: %{momorel}m%{?dist}
Source: http://ftp.gnome.org/pub/GNOME/sources/mobile-broadband-provider-info/%{upstream_version}/mobile-broadband-provider-info-%{upstream_version}.tar.bz2
NoSource: 0
License: Public Domain
Group: System Environment/Base

BuildArch: noarch
URL: http://live.gnome.org/NetworkManager/MobileBroadband/ServiceProviders
BuildRoot: BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libxml2

%description
The mobile-broadband-provider-info package contains listings of mobile
broadband (3G) providers and associated network and plan information.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains files necessary for
developing developing applications that use %{name}.

%prep
%setup -q -n %{name}-%{upstream_version}

%build
%configure
make %{?_smp_mflags}

%check
make check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(0644, root, root, 0755)
%doc COPYING README
%attr(0644,root,root) %{_datadir}/%{name}/*
	
%files devel
%defattr(0644, root, root, 0755)
%{_datadir}/pkgconfig/%{name}.pc

%changelog
* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20110511-1m)
- update 20110511

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20110218-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20110218-1m)
- update 20110218

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20100510-4m)
- rebuild for new GCC 4.5

* Thu Nov  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20100510-3m)
- support b-mobile

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20100510-2m)
- full rebuild for mo7 release

* Thu Jun  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20100122-1m)
- Initial commit Momonga Linux

