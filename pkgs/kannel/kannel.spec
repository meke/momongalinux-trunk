%global momorel 5

Summary: WAP and SMS gateway
Name: kannel
Version: 1.4.3
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Daemons
URL: http://www.kannel.org/
Source0: http://www.kannel.org/download/%{version}/gateway-%{version}.tar.bz2
NoSource: 0
Source1: kannel.logrotate
Source2: kannel.init
Source3: kannel.conf
Source4: gw-config
Patch0: gateway-1.4.3-ssldetect.patch
Patch1: gateway-1.4.1-typesh.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(postun): chkconfig
BuildRequires: bison, byacc, flex, ghostscript
BuildRequires: libxml2-devel, openssl-devel >= 1.0.0, zlib-devel, pcre-devel >= 8.31
BuildRequires: libbind-devel >= 6.0
BuildRequires: sqlite-devel
%{?_with_mysql:Buildrequires: mysql-devel}
%{?_with_pgsql:Buildrequires: postgresql-devel}
# For the docs...
BuildRequires: jadetex, tetex-dvips, docbook-dtds, docbook-style-dsssl
BuildRequires: ImageMagick, transfig

%description
The Kannel Open Source WAP and SMS gateway works as both an SMS gateway, for
implementing keyword based services via GSM text messages, and a WAP gateway,
via UDP. The SMS part is fairly mature, the WAP part is early in its
development. In this release, the GET request for WML pages and WMLScript
files via HTTP works, including compilation for WML and WMLScript to binary
forms. Only the data call bearer (UDP) is supported, not SMS.


%package devel
Summary: Development files for the kannel WAP and SMS gateway
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The Kannel Open Source WAP and SMS gateway works as both an SMS gateway, for
implementing keyword based services via GSM text messages, and a WAP gateway,
via UDP. The SMS part is fairly mature, the WAP part is early in its
development. In this release, the GET request for WML pages and WMLScript
files via HTTP works, including compilation for WML and WMLScript to binary
forms. Only the data call bearer (UDP) is supported, not SMS.

Install this package if you need to develop or recompile applications that
use the kannel WAP and SMS gateway.

%package docs
Summary: Documentation for the Kannel WAP and SMS gateway
Group: Documentation
BuildArch: noarch

%description docs
The Kannel Open Source WAP and SMS gateway works as both an SMS gateway, for
implementing keyword based services via GSM text messages, and a WAP gateway,
via UDP. The SMS part is fairly mature, the WAP part is early in its
development. In this release, the GET request for WML pages and WMLScript
files via HTTP works, including compilation for WML and WMLScript to binary
forms. Only the data call bearer (UDP) is supported, not SMS.

This package contains documentation for the Kannel WAP and SMS gateway.


%prep
%setup -q -n gateway-%{version}
%patch0 -p1 -b .ssldetect
%patch1 -p1 -b .typesh
%{__chmod} -c -x gwlib/html-entities.def # for -debuginfo, as of 1.4.3


%build
%configure \
    --enable-pcre \
    --enable-docs \
    --enable-start-stop-daemon \
    --with-sqlite3 \
    %{?_with_mysql} \
    %{?_with_pgsql}
# Remove %{?_smp_mflags} since the docs fail to build with it
%{__make}


%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
# Install fakesmsc and fakewap, useful for monitoring
%{__install} -m 0755 test/{fakesmsc,fakewap} %{buildroot}%{_bindir}/
# Logrotate entry
%{__install} -D -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/kannel
# Init script
%{__install} -D -m 0755 %{SOURCE2} %{buildroot}%{_initscriptdir}/kannel
# Default configuration file
%{__install} -D -m 0640 %{SOURCE3} %{buildroot}%{_sysconfdir}/kannel.conf
# Empty log directory
%{__mkdir_p} %{buildroot}%{_var}/log/kannel/
# Rename start-stop-daemon to start-stop-kannel
%{__mv} %{buildroot}%{_sbindir}/start-stop-daemon \
        %{buildroot}%{_sbindir}/start-stop-kannel
# Remove executable bit from man pages (as of 1.4.1)
%{__chmod} -x %{buildroot}%{_mandir}/man?/*
# Multilib fix : Replace the gw-config with our wrapper script
%{__mv} %{buildroot}%{_bindir}/gw-config \
        %{buildroot}%{_bindir}/gw-config.%{_arch}
%{__install} -p -m 0755 %{SOURCE4} %{buildroot}%{_bindir}/gw-config


%clean
%{__rm} -rf %{buildroot}


%pre
# Create system account
/usr/sbin/useradd -c "Kannel WAP and SMS gateway" -r -M -s '' \
    -d %{_var}/lib/kannel kannel &>/dev/null || :

%post
/sbin/chkconfig --add kannel

%preun
if [ $1 -eq 0 ]; then
    # Last removal, stop service and remove it
    /sbin/service kannel stop &>/dev/null || :
    /sbin/chkconfig --del kannel
fi

%postun
if [ $1 -ge 1 ]; then
    /sbin/service kannel condrestart &>/dev/null || :
fi


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README STATUS
%attr(0640,kannel,kannel) %config(noreplace) %{_sysconfdir}/kannel.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/kannel
%{_initscriptdir}/kannel
%{_bindir}/fakesmsc
%{_bindir}/fakewap
%{_bindir}/mtbatch
%{_bindir}/seewbmp
%{_bindir}/wmlsc
%{_bindir}/wmlsdasm
%{_sbindir}/*
%{_mandir}/man?/*
%attr(0750,kannel,kannel) %dir %{_var}/log/kannel/

%files devel
%defattr(-,root,root,-)
%{_bindir}/gw-config*
%{_includedir}/kannel/
%dir %{_libdir}/kannel/
%{_libdir}/kannel/*.a

%files docs
%defattr(-,root,root,-)
%doc doc/*/*.pdf

%changelog
* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-5m)
- rebuild against pcre-8.31

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3
- sync with Fedora devel

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-6m)
- rebuild against openssl-1.0.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-4m)
- rebuild against libbind-6.0

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-3m)
- rebuild against openssl-0.9.8k

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.enmail.ne.jp>
- (1.4.1-2m)
- use %%{_initscriptdir} instead of %%{_initrddir}
- PreReq: chkconfig

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-1m)
- import from Fedora to Momonga

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> 1.4.1-10
- rebuild with new openssl

* Mon Dec 22 2008 Matthias Saou <http://freshrpms.net/> 1.4.1-9
- Include patch to fix openssl detection on 64bit.
- Include gw-config wrapper script to fix multilib conflicts (#341701).
- Include patch to remove definitions of types which never happen on Linux.
- Added "INIT INFO" block to the init script (#246964).

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org>
- Autorebuild for GCC 4.3

* Thu Dec 06 2007 Release Engineering <rel-eng at fedoraproject dot org>
- Rebuild for deps

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 1.4.1-5
- Rebuild for new BuildID feature.

* Mon Aug  6 2007 Matthias Saou <http://freshrpms.net/> 1.4.1-4
- Update License field.
- Switch to using the DESTDIR install method.
- No longer mark init script as config file.
- Remove executable bit from man pages.
- Move gw-config to devel sub-package.

* Thu Dec 14 2006 Matthias Saou <http://freshrpms.net/> 1.4.1-3
- Add ghostscript build requirement, needed (at least on) RHEL4.

* Mon Oct 16 2006 Matthias Saou <http://freshrpms.net/> 1.4.1-2
- Make sure we keep sqlite2 support for FC5 and below.

* Mon Oct  2 2006 Matthias Saou <http://freshrpms.net/> 1.4.1-1
- Update to 1.4.1.
- Remove obsolete depend patch.
- Add 3s sleep sfter bearerbox start to let it settle before connections.
- Switch sqlite backend from sqlite2 to new sqlite3.
- Add conditional --with mysql and pgsql rebuild support.
- Re-enable building the doc since transfig is now available and include PDFs.

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 1.4.0-9
- FC6 rebuild.

* Mon Mar  6 2006 Matthias Saou <http://freshrpms.net/> 1.4.0-8
- FC5 rebuild.

* Thu Feb  9 2006 Matthias Saou <http://freshrpms.net/> 1.4.0-7
- Rebuild for new gcc/glibc.

* Thu Nov 17 2005 Matthias Saou <http://freshrpms.net/> 1.4.0-6
- Rebuild on FC-5 devel for new openssl library.
- Enable pcre.
- Include at last my pending improvements (detail below).
- Include (at last!) user creation, logrotate entry and init script.
- Include default configuration file (do nothing, access only from 127.0.0.1).
- Include empty log directory.
- Include fakesmsc and fakewap programs, useful for monitoring purposes.

* Wed May 25 2005 Jeremy Katz <katzj@redhat.com> - 1.4.0-5
- rebuild with change to depend on sqlite2 (#156229)

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Jan 17 2005 Matthias Saou <http://freshrpms.net/> 1.4.0-3
- Added Stefan Radman's patch for kannel bug #173 to fix .depend problem.

* Fri Dec 10 2004 Matthias Saou <http://freshrpms.net/> 1.4.0-1
- Update to 1.4.0.
- Remove the obsolete OpenSSL workaround.

* Thu Nov  4 2004 Matthias Saou <http://freshrpms.net/> 1.3.2-4
- Added pcre support, doc building (almost) and sqlite backend...
  it still fails with a corrupt first line of .depend on FC3, though.

* Tue Aug 24 2004 Matthias Saou <http://freshrpms.net/> 1.3.2-2
- Really comment out all scriplets, they're not yet used.

* Thu Jul 29 2004 Matthias Saou <http://freshrpms.net/> 1.3.2-1
- Don't fix the openssl detection for RHL 7.x.

* Thu Jul 22 2004 Matthias Saou <http://freshrpms.net/> 1.3.2-0
- Update to 1.3.2 development version.
- Added -devel sub-package since there are now headers and a static lib.

* Wed Jul 14 2004 Matthias Saou <http://freshrpms.net/> 1.2.1-0
- Initial RPM release, still need to add an init script I think.

