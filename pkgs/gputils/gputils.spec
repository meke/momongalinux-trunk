%global momorel 5

Summary: Development utilities for Microchip (TM) PIC (TM) microcontrollers
Name: gputils
Version: 0.13.7
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://gputils.sourceforge.net/
Group: Development/Languages
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a collection of development tools for Microchip (TM) PIC (TM)
microcontrollers.

This is ALPHA software: there may be serious bugs in it, and it's
nowhere near complete.  gputils currently only implements a subset of
the features available with Microchip's tools.  See the documentation for
an up-to-date list of what gputils can do.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%doc doc/gputils.lyx doc/gputils.pdf doc/gputils.ps
%{_bindir}/gpasm
%{_bindir}/gpdasm
%{_bindir}/gplib
%{_bindir}/gplink
%{_bindir}/gpstrip
%{_bindir}/gpvc
%{_bindir}/gpvo
%{_datadir}/gputils
%{_mandir}/fr/man1/gpasm.1*
%{_mandir}/fr/man1/gpdasm.1*
%{_mandir}/fr/man1/gplib.1*
%{_mandir}/fr/man1/gplink.1*
%{_mandir}/fr/man1/gpstrip.1*
%{_mandir}/fr/man1/gputils.1*
%{_mandir}/fr/man1/gpvc.1*
%{_mandir}/fr/man1/gpvo.1*
%{_mandir}/man1/gpasm.1*
%{_mandir}/man1/gpdasm.1*
%{_mandir}/man1/gplib.1*
%{_mandir}/man1/gplink.1*
%{_mandir}/man1/gpstrip.1*
%{_mandir}/man1/gputils.1*
%{_mandir}/man1/gpvc.1*
%{_mandir}/man1/gpvo.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.7-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.7-1m)
- version 0.13.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.6-2m)
- rebuild against rpm-4.6

* Sun Jun  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.6-1m)
- version 0.13.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13.5-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.5-2m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.5-1m)
- version 0.13.5
- License: GPLv2

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.4-1m)
- initial package for ktechlab-0.3
