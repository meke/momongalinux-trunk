%global momorel 3

Summary: Shared MIME information database
Name: shared-mime-info
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://freedesktop.org/Software/shared-mime-info
Source0: http://freedesktop.org/~hadess/%{name}-%{version}.tar.xz
NoSource: 0
Source1: defaults.list
# Generated with:
# for i in `cat /home/hadess/Projects/jhbuild/totem/data/mime-type-list.txt | grep -v real | grep -v ^#` ; do if grep MimeType /home/hadess/Projects/jhbuild/rhythmbox/data/rhythmbox.desktop.in.in | grep -q "$i;" ; then echo "$i=rhythmbox.desktop;totem.desktop;" >> totem-defaults.list ; else echo "$i=totem.desktop;" >> totem-defaults.list ; fi ; done ; for i in `cat /home/hadess/Projects/jhbuild/totem/data/uri-schemes-list.txt | grep -v ^#` ; do echo "x-scheme-handler/$i=totem.desktop;" >> totem-defaults.list ; done
Source2: totem-defaults.list
# Generated with:
# for i in `grep MimeType= /usr/share/applications/gnome-file-roller.desktop | sed 's/MimeType=//' | sed 's/;/ /g'` ; do if ! `grep -q $i defaults.list` ; then echo $i=gnome-file-roller.desktop\; >> file-roller-defaults.list ; fi ; done
Source3: file-roller-defaults.list
# Generated with:
# for i in `grep MimeType= /usr/share/applications/shotwell-viewer.desktop | sed 's/MimeType=//' | sed 's/;/ /g'` ; do echo $i=shotwell-viewer.desktop\; >> shotwell-viewer-defaults.list ; done
Source4: shotwell-viewer-defaults.list

Patch0: shared-mime-info-0.90-media_type.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  pkgconfig intltool
BuildRequires:  libxml2-devel >= 2.6.27
BuildRequires:  glib2-devel >= 2.12.12
# For intltool:
BuildRequires: perl-XML-Parser >= 2.34-9m
Requires: libxml2 glib2

%description
This is the freedesktop.org shared MIME info database.

Many programs and desktops use the MIME system to represent the types of
files. Frequently, it is necessary to work out the correct MIME type for
a file. This is generally done by examining the file's name or contents,
and looking up the correct MIME type in a database.
#'

%prep
%setup -q
%patch0 -p1 -b .media_type

%build
%configure --disable-update-mimedb
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}


mkdir -p %{buildroot}%{_datadir}/applications
sed -i -e 's/gnome-abiword.desktop/abiword.desktop/g' \
  -e 's/gnome-nautilus-folder-handler.desktop/nautilus-folder-handler.desktop/g' \
  -e 's/gnome-eog.desktop/eog.desktop/g' \
  -e 's/gnome-file-roller.desktop/file-roller.desktop/g' \
  -e 's/gnome-gedit.desktop/gedit.desktop/g' \
  -e 's/gnome-glade-2.desktop/glade-2.desktop/g' \
  -e 's/gnome-gnucash.desktop/gnucash.desktop/g' \
  -e 's/gnome-gnumeric.desktop/gnumeric.desktop/g' \
  %SOURCE1
install -m 644 %SOURCE1 %{buildroot}%{_datadir}/applications/defaults.list
cat %SOURCE2 >> %{buildroot}/%{_datadir}/applications/defaults.list
cat %SOURCE3 >> %{buildroot}/%{_datadir}/applications/defaults.list
cat %SOURCE4 >> %{buildroot}/%{_datadir}/applications/defaults.list

%post 
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README NEWS shared-mime-info-spec.xml
%{_bindir}/update-mime-database
%dir %{_datadir}/mime/
%{_datadir}/mime/packages
%{_datadir}/applications/defaults.list
%{_datadir}/pkgconfig/shared-mime-info.pc
%{_mandir}/man1/update-mime-database.1.*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for glib 2.33.2

* Fri Jun  1 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.0-2m)
- update default.list
- add totem-defaults.list, file-roller-defaults.list, shotwell-viewer-defaults.list

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.90-2m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.71-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.71-4m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.71-3m)
- modify desktop filename
- add sed command processing

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.71-2m)
- add chemical and uri to media_types
- add patch0

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Sat Aug 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.23-5m)
- revise defaults.list

* Sat Aug 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.23-4m)
- update defaults.list (sync fedora)

* Tue Aug 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.23-3m)
- back to 0.23

* Thu Jul 10 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.30-2m)
- roll back to 0.30

* Sun Jun 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Tue Jun  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-2m)
- rebuild against gcc43

* Sun Jan  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Thu Aug  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Wed May 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sun Mar 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20-2m)
- delete patch0 (pkgconfig search /usr/share/pkgconfig)

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Thu Sep 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19-1m)
- update to 0.17

* Thu Apr  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Tue Nov 15 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.16-1m)
- update to 0.16
- remove Patch0(shared-mime-info-0.15-xul.patch)
- remove Patch1(shared-mime-info-0.15-endian.patch)

* Thu Apr 14 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.15-3m)
- update defaults.list (added xml entry)

* Sun Apr 10 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.15-2m)
- Handle XUL files. #134122
- Fix for mime sniffing on big-endian
- add defaults.list

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Apr 23 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.14-1m)
- import from FC1

* Mon Mar 22 2004 Alex Larsson <alexl@redhat.com> 0.14-1
- update to 0.14

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jan 26 2004 Alexander Larsson <alexl@redhat.com> 0.13-1
- 0.13

* Fri Jan 16 2004 Alexander Larsson <alexl@redhat.com> mime-info
- Initial build.
