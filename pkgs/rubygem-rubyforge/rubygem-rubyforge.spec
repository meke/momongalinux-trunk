%global momorel 5

# Generated from rubyforge-0.4.4.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname rubyforge
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: A script which automates a limited set of rubyforge operations
Name: rubygem-%{gemname}
Version: 2.0.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyforge.org/projects/codeforpeople
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
A script which automates a limited set of rubyforge operations.  * Run
'rubyforge help' for complete usage. * Setup: For first time users AND
upgrades to 0.4.0: * rubyforge setup (deletes your username and password, so
run sparingly!) * edit ~/.rubyforge/user-config.yml * rubyforge config * For
all rubyforge upgrades, run 'rubyforge config' to ensure you have latest. *
Don't forget to login!  logging in will store a cookie in your .rubyforge
directory which expires after a time.  always run the login command before any
operation that requires authentication, such as uploading a package.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/rubyforge
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Nov 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.4-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update to 2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.3-1m)
- update to 2.0.3

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-1m)
- update to 2.0.2

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- update to 1.0.4

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.4-1m)
- Initial package for Momonga Linux
