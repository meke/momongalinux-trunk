%global momorel 1
# first two digits of version
%global release_version %%(echo %{version} | awk -F. '{print $1"."$2}')

Name:           libsecret
Version:        0.11
Release: %{momorel}m%{?dist}
Summary:        Library for storing and retrieving passwords and other secrets
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            https://live.gnome.org/Libsecret
Source0:        http://download.gnome.org/sources/libsecret/%{release_version}/libsecret-%{version}.tar.xz
NoSource: 0

BuildRequires:  glib2-devel
BuildRequires:  gobject-introspection-devel
BuildRequires:  intltool
BuildRequires:  libgcrypt-devel
BuildRequires:  vala-devel

Provides:       bundled(egglib)

%description
libsecret is a library for storing and retrieving passwords and other secrets.
It communicates with the "Secret Service" using DBus. gnome-keyring and
KSecretService are both implementations of a Secret Service.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
%make 


%install
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang libsecret


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f libsecret.lang
%doc AUTHORS COPYING NEWS README
%{_bindir}/secret-tool
%{_libdir}/libsecret-1.so.*
%{_libdir}/girepository-1.0/Secret-1.typelib
%{_libdir}/girepository-1.0/SecretUnstable-0.typelib
%{_datadir}/vala/vapi
%doc %{_datadir}/man/man1/secret-tool.1.*

%files devel
%{_includedir}/libsecret-1/
%{_libdir}/libsecret-1.so
%{_libdir}/pkgconfig/libsecret-1.pc
%{_libdir}/pkgconfig/libsecret-unstable.pc
%{_datadir}/gir-1.0/Secret-1.gir
%{_datadir}/gir-1.0/SecretUnstable-0.gir
%doc %{_datadir}/gtk-doc/


%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11-1m)
- update to 0.11

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-2m)
- rebuild for vala

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-1m)
- import from fedora

