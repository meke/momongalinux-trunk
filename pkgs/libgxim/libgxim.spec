%global		momorel	1
Name:		libgxim
Version:	0.3.3
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
URL:		http://code.google.com/p/libgxim/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	intltool gettext ruby
BuildRequires:	dbus-devel > 0.23, dbus-glib-devel >= 0.74, glib2-devel >= 2.16, gtk2-devel
Source0:	http://libgxim.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:		libgxim-fix-fontset.patch

Summary:	GObject-based XIM protocol library
Group:		System Environment/Libraries

%description
libgxim is a X Input Method protocol library that is implemented by GObject.
this library helps you to implement XIM servers or client applications to
communicate through XIM protocol without using Xlib API directly, particularly
if your application uses GObject-based main loop.

This package contains the shared library.

%package	devel
Summary:	Development files for libgxim
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig
Requires:	glib2-devel >= 2.16.0
Requires:	gtk2-devel

%description	devel
libgxim is a X Input Method protocol library that is implemented by GObject.
this library helps you to implement XIM servers or client applications to
communicate through XIM protocol without using Xlib API directly, particularly
if your application uses GObject-based main loop.

This package contains the development files to make any applications with
libgxim.

%prep
%setup -q
%patch0 -p0 -b 0-fontset


%build
%configure --disable-static --disable-rebuilds

%make


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

# clean up the unnecessary files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/libgxim.so.*

%files	devel
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/libgxim.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/libgxim
%{_datadir}/gtk-doc/html/libgxim

%changelog
* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-1m)
- import from fedora