%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global src_ver %{version}+nmu2

Summary:	Alternatives Configurator
Name:		galternatives
Version:	0.13.5
Release:	%{momorel}m%{?dist}
License:	GPL+
Group:		Applications/System
URL:		http://packages.qa.debian.org/g/galternatives.html
Source0:	http://ftp.debian.org/debian/pool/main/g/%{name}/%{name}_%{src_ver}.tar.gz
NoSource:	0
Patch0:		galternatives-desktop.patch
Patch1:		galternatives-fedora.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	pygtk2-devel
BuildRequires:	python-devel
BuildRequires:	desktop-file-utils gettext intltool

Requires:	usermode
Requires:	pygtk2
Requires:	chkconfig

BuildArch:	noarch

%description
Graphical setup tool for the alternatives system. A GUI to help the system
administrator to choose what program should provide a given service

%prep
%setup -q
%patch0 -p0 -b .desktop 
%patch1 -p0 -b .fedora
 
# To silence rpmlint
sed -i '/^#!\/usr\/bin\/python/ d' galternatives/*.py

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

desktop-file-install  --delete-original			\
	--vendor=     					\
	--dir %{buildroot}%{_datadir}/applications	\
	--mode 0644					\
	galternatives.desktop

mkdir -p %{buildroot}%{_sbindir}
mv %{buildroot}%{_bindir}/galternatives %{buildroot}%{_sbindir}
ln -s consolehelper %{buildroot}%{_bindir}/galternatives

mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps
cat << EOF > %{buildroot}%{_sysconfdir}/security/console.apps/galternatives
USER=root
PROGRAM=%{_sbindir}/galternatives
SESSION=true
EOF

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
cat << EOF > %{buildroot}%{_sysconfdir}/pam.d/galternatives
#%PAM-1.0
auth	sufficient	pam_rootok.so
auth	sufficient	pam_timestamp.so
auth	include		system-auth
session	required	pam_permit.so
session	optional	pam_xauth.so
session	optional	pam_timestamp.so
account	required	pam_permit.so
EOF

pushd translations
LANGS=$(find . -name \*.po | cut -d '.' -f 2 | tr -d '/')
for lang in ${LANGS};	do
	echo ${lang}:
	mkdir -p %{buildroot}%{_datadir}/locale/${lang}/LC_MESSAGES
	cp ${lang}.mo  %{buildroot}%{_datadir}/locale/${lang}/LC_MESSAGES/galternatives.mo
done
popd

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%doc TODO debian/copyright debian/changelog
%defattr(-,root,root,-)
%{_bindir}/galternatives
%{_sbindir}/galternatives
%{python_sitelib}/galternatives/
%{python_sitelib}/galternatives*.egg-info
%{_datadir}/applications/galternatives.desktop
%{_datadir}/galternatives/
%{_datadir}/pixmaps/galternatives.png
%config(noreplace) %{_sysconfdir}/pam.d/galternatives
%config(noreplace) %{_sysconfdir}/security/console.apps/galternatives

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.4-3m)
- remove fedora from --vendor

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.4-2m)
- modify Requires

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.13.4-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.13.4-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.13.4-6
- Rebuild for Python 2.6

* Thu Jan 10 2008 Deji Akingunola <dakingun@gmail.com> - 0.13.4-5
- Package the python egg file
- Update the License tag 

* Sat Dec 30 2006 Deji Akingunola <dakingun@gmail.com> 0.13.4-4
- Package the debian copyright and changelog files
- Add a Require on pygtk2

* Thu Dec 28 2006 Deji Akingunola <dakingun@gmail.com> 0.13.4-3
- Fix the source to handle the RH specific --initscript option 

* Thu Dec 28 2006 Deji Akingunola <dakingun@gmail.com> 0.13.4-2
- Don't ghost *pyo files 

* Sat Sep 02 2006 Deji Akingunola <dakingun@gmail.com> 0.13.4-1
- Update to version 0.13.4
- Add a quirk to workaround pygtk needing a 'DISPLAY', to allow mock build

* Tue Nov 08 2005 Deji Akingunola <dakingun@gmail.com> 0.12-1
- Initial build
