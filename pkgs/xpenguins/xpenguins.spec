%define name xpenguins
%define version 2.2
%define release 1
%global momorel 8

Summary: Cute little penguins that walk along the tops of your windows
Name: %{name}
Version: %{version}
Release: %{momorel}m%{?dist}
License: GPL
Source0: http://xpenguins.seul.org/%{name}-%{version}.tar.gz 
NoSource: 0
Group: Amusements/Graphics
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#Packager: Robin Hogan <R.J.Hogan@reading.ac.uk>
URL: http://xpenguins.seul.org/

%description
XPenguins animates a friendly family of penguins in your root window.
They drop in from the top of the screen, walk along the tops of your
windows, up the side of your windows, levitate, skateboard, and do
other similarly exciting things. XPenguins is now themeable so if
you're bored of penguins, try something else. The themes that come
with this package are "Penguins", "Classic Penguins", "Big Penguins",
"Turtles" and "Bill".
#'

%prep
%setup -q

%build
%configure
%make

%install
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog lay-out-frames.scm resize-frames.scm
%attr(755,root,root) %{_bindir}/xpenguins
%{_mandir}/man1/*
%{_datadir}/%{name}/themes/Penguins/*
%{_datadir}/%{name}/themes/Big_Penguins/*
%{_datadir}/%{name}/themes/Classic_Penguins/*
%{_datadir}/%{name}/themes/Turtles/*
%{_datadir}/%{name}/themes/Bill/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-2m)
- %%NoSource -> NoSource

* Sat Mar 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- initila build

* Mon Oct  1 2001 Robin Hogan <R.J.Hogan@reading.ac.uk> 2.1.5-1
- Removed Lemmings (now in xpenguins_themes), added Bill, Big Penguins
- Added resize-frames.scm to docs
* Wed Aug 22 2001 Robin Hogan <R.J.Hogan@reading.ac.uk> 2.1.3-1
- Added Lemmings theme
* Sat May  5 2001 Robin Hogan <R.J.Hogan@reading.ac.uk> 1.9.1-1
- First spec file used with autoconf
* Tue May 23 2000 Robin Hogan <R.J.Hogan@reading.ac.uk> 1.2-1
- Use BuildRoot.

# end of file
