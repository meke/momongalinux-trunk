Summary: The BSD 4.4 version of make.
Name: pmake

%global momorel 17
%global deb_rel 11
Version: 1.45
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Tools

Source0: http://ftp.debian.org/debian/pool/main/p/%{name}/%{name}_%{version}-%{deb_rel}.tar.gz 

Patch0: pmake-1.45-mktemp.patch
Patch1: pmake-1.45-groffpath.patch
Patch2: pmake-1.45-gcc4.patch
Patch3: pmake-1.45-readdir.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: mktemp
Requires: groff
Obsoletes: pmake-customs

%description
Make is a GNU tool which allows users to build and install programs
without any significant knowledge of the build process. Details about
how the program should be built are included in the program's
Makefile. Pmake is a particular version (BSD 4.4) of make. Pmake
supports some additional syntax which is not in the standard make
program. Some Berkeley programs have Makefiles written for pmake.
#'

%prep
%setup -q
%patch0 -p1 -b .mktemp~
%patch2 -p1 -b .gcc4
%patch3 -p0 -b .readdir~

%build
%__make -f Makefile.boot CFLAGS="%{optflags} -D__COPYRIGHT\(x\)= -D__RCSID\(x\)= \
    -I. -DMACHINE=\\\"momonga\\\" -DMACHINE_ARCH=\\\"`arch`\\\"" CC=gcc
touch build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%__mkdir -p %{buildroot}
%__mkdir -p %{buildroot}/{%{_bindir},%{_mandir}/man1}
%__install bmake %{buildroot}/%{_bindir}/pmake
%__install -m 755 mkdep %{buildroot}/%{_bindir}
%__install make.1 %{buildroot}/%{_mandir}/man1/pmake.1
%__install mkdep.1 %{buildroot}/%{_mandir}/man1

%__mkdir -p %{buildroot}/%{_datadir}/mk
%__rm -f mk/*~
for file in mk/*; do
  %__install -m 644 $file %{buildroot}/%{_datadir}/mk
done

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc PSD.doc/tutorial.ms
%{_bindir}/*
%{_datadir}/mk
%{_mandir}/man1/*

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-17m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-14m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-12m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.45-11m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-10m)
- %%NoSource -> NoSource

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-9m)
- add gcc4 patch. import FC.
-   Patch2: pmake-1.45-gcc4.patch
- remove pmake-1.45-suppress-warnings.patch

* Wed Feb 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.45-8m)
- remove groff version depends.

* Sun Dec 19 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.45-7m)
- add pmake-1.45-readdir.patch (Do not assume two subsequent 
  calls of readdir() just after opendir() return "." and "..".)

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.45-6m)
- remove Epoch

* Mon Feb 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.45-5m)
- add patch to suppress warnings on build

* Sun Feb 22 2004 Kenta MURATA <muraken2@nifty.com>
- (1.45-4m)
- version 1.45-11.
- MACHINE=momonga.

* Sun Oct 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.45-3m)
- fix groff path

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (1.45-2k)
- Epoched and update. ummmm.

* Tue Aug 21 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- bad. Hardcoded path including a version number for groff.

* Fri May 11 2001 Preston Brown <pbrown@redhat.com>
- use mktemp in mkdep script

* Mon Jan 08 2001 Preston Brown <pbrown@redhat.com>
- report newer pmake from Debian who got it from 4.4BSD

