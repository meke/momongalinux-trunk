%global momorel 1
%global ver 5.1

Summary: Tools needed to create Texinfo format documentation files
Name: texinfo
Version: %{ver}
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Publishing
URL: http://www.gnu.org/software/texinfo/
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Source1: info-dir
Source10: gnome-info.png

Patch0: texinfo-4.12-zlib.patch
Patch1: texinfo-4.13a-powerpc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: rpm >= 3.0.5-17k
BuildRequires: ncurses-devel
# texi2pdf requires help2man 
BuildRequires: help2man
Requires: help2man

Requires(post): info
Requires(preun): info
%{?include_specopt}
%{?!do_test: %global do_test 0}
%if %{do_test}
# !!FIXME!!
BuildRequires: tetex
%endif

%description
Texinfo is a documentation system that can produce both online
information and printed output from a single source file.  The GNU
Project uses the Texinfo file format for most of its documentation.

Install texinfo if you want a documentation system for producing both
online and print documentation from the same source file and/or if you
are going to write documentation for the GNU Project.


%package -n info
Summary: A stand-alone TTY-based reader for GNU texinfo documentation
Group: System Environment/Base
# do not include gzip to Requires because an installtion error occurs
# in anaconda. A loop (info <-> gzip) exists.
Requires: bash bzip2 lzma
Provides: /sbin/install-info

%description -n info
The GNU project uses the texinfo file format for much of its
documentation. The info package provides a standalone TTY-based
browser program for viewing texinfo files.

You should install info, because GNU's texinfo documentation is a
valuable source of information about the software on your system.

%package tex
Summary: Tools for formatting Texinfo documentation files using TeX
Group: Applications/Publishing
Requires: texinfo = %{version}-%{release}
#Requires: tetex
Requires:  texlive-pdftex
Requires:  texlive-texmf-plain
Requires:  texlive-texmf-texinfo
Requires:  texlive-texmf-pdftex

%description tex
Texinfo is a documentation system that can produce both online
information and printed output from a single source file. The GNU
Project uses the Texinfo file format for most of its documentation.

The texinfo-tex package provides tools to format Texinfo documents
for printing using TeX.

%prep
%setup -q -n %{name}-%{ver}

%patch0 -p1 -b .zlib
%patch1 -p1 -b .powerpc

%build
%configure \
    --with-external-Text-Unidecode \
     --with-external-libintl-perl \
%if %{do_test}
    --enable-maintainer-mode
%endif
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_sysconfdir}

%makeinstall

install -p -m644 %{SOURCE1} %{buildroot}%{_infodir}/dir
mv %{buildroot}%{_bindir}/install-info %{buildroot}/sbin

# install desktop file
pushd %{buildroot}
mkdir -p .%{_datadir}/applications
cat > .%{_datadir}/applications/info.desktop <<EOF
[Desktop Entry]
Name=Info Viewer
Comment=GNU Info Page Reader
Exec=info
Icon=gnome-info
Terminal=true
Type=Application
Categories=Utility;
EOF
popd

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/pixmaps/

rm -f %{buildroot}%{_datadir}/texinfo/texinfo.{xsl,dtd}

# Convert ChangeLog to UTF-8
/usr/bin/iconv -f iso-8859-2 -t utf-8 < ChangeLog > ChangeLog_utf8
touch -r ChangeLog ChangeLog_utf8
mv ChangeLog_utf8 ChangeLog

%if %{do_test}
%check
make check
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/texinfo %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/texinfo %{_infodir}/dir || :
fi

%post -n info
/sbin/install-info %{_infodir}/info-stnd.info %{_infodir}/dir || :

%preun -n info
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/info-stnd.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README TODO COPYING
%doc --parents info/README
%{_bindir}/makeinfo
%{_bindir}/texi2any
%{_bindir}/pod2texi
%dir %{_datadir}/texinfo
%{_datadir}/texinfo/*
%{_infodir}/texinfo*
%{_datadir}/locale/*/*/*
%{_mandir}/man1/makeinfo.1*
%{_mandir}/man1/texi2any.1*
%{_mandir}/man1/pod2texi.1*
%{_mandir}/man5/texinfo.5*

%files -n info
%defattr(-,root,root)
%config(noreplace) %verify(not md5 size mtime) %{_infodir}/dir
%{_bindir}/info
%{_bindir}/infokey
%{_datadir}/applications/info.desktop
%{_infodir}/info.info*
%{_infodir}/info-stnd.info*
/sbin/install-info
%{_mandir}/man1/info.1*
%{_mandir}/man1/infokey.1*
%{_mandir}/man1/install-info.1*
%{_mandir}/man5/info.5*
%{_datadir}/pixmaps/gnome-info.png

%files tex
%defattr(-,root,root)
%{_bindir}/texindex
%{_bindir}/texi2dvi
%{_bindir}/texi2pdf
%{_bindir}/pdftexi2dvi
%{_mandir}/man1/pdftexi2dvi.*
%{_mandir}/man1/texindex.1*
%{_mandir}/man1/texi2dvi.1*
%{_mandir}/man1/texi2pdf.1*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1-1m)
- update to 5.1

* Tue Jan 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.13a-15m)
- add BuildRequires: and Requires:

* Mon Sep 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.13a-14m)
- update texi2dvi

* Mon Aug  1 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.13a-13m)
- update texi2dvi
- add %%check

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.13a-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.13a-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.13a-10m)
- full rebuild for mo7 release

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-9m)
- info package does not require gzip to break dependency loop

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-8m)
- fix Source3: brp-compress

* Wed Aug 18 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (4.13a-7m)
- Change Requires: for texlive

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-6m)
- modify __os_install_post for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-4m)
- support xz from upstream (bfae00d02b5fb3a2ce34c09d2dbf0ca2f96b154f)
- completely remove bgzlib patch

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-3m)
- temporarily disable bgzlib patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13a-1m)
- update to 4.13a
-- update bgzlib patch (includes lzma support)
-- drop Source2,Patch10,11, merged upstream
-- revise __os_install_post not to bzip2 dir
-- License: GPLv3+

* Sun Oct 19 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (4.11-3m)
- added BuildPreReq: zlib-static

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.11-2m)
- rebuild against gcc43

* Tue Jan  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.11-1m)
- update to version 4.11 for new lilypond
- add a package texinfo-tex
- update bgzlib.patch and bgzlib-lib64.patch
- remove texindex.patch
- import res_win_segfault.patch from Fedora
 +* Tue Nov 13 2007 Vitezslav Crhonek <vcrhonek@redhat.com> - 4.11-3
 +- Fix info crashes when resizing window
 +  Resolves: #243971
- import direntry.patch from Fedora
 +* Mon Dec 10 2007 Vitezslav Crhonek <vcrhonek@redhat.com> - 4.11-4
 +- Don't insert description ("This is...") into the direntry section
 +  of some generated files
 +  Resolves: #394191
- modify %%post and %%preun

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8-9m)
- revise spec

* Tue Jul 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8-8m)
- import gnome-info.png from xfce4-icon-theme-4.4.1

* Sun Jul 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8-7m)
- change provide /sbin/install-info from texinfo to info

* Fri Nov 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8-6m)
- [SECURITY] CVE-2005-3011 CVE-2006-4810
-- import patch3 from FC

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.8-5m)
- rebuild against zlib-1.2.3 (CAN-2005-1849)

* Tue Jul 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.8-4m)
- rebuild against zlib-devel 1.2.2-2m, which fixes CAN-2005-2096.

* Wed Feb 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.8-3m)
- support multilib.

* Tue Feb 22 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8-2m)
- readd bgzlib patch
- delete patch1 from fc

* Sun Feb 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8-1m)
- update to 4.8
- delete Patch0
- add Patch1: texinfo-4.3-zlib.patch
- add Patch2: texinfo-4.7.test.patch.bz2
- add Patch107: texinfo-4.7-vikeys-segfault-fix.patch.bz2

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6-5m)
- move info.desktop to %%{_datadir}/applications/

* Tue Sep 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.6-4m)
- used %{_libdir}/libz.a and %{_libdir}/libbz2.a instead of ones in /usr/lib

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (4.6-3m)
- revised spec for rpm 4.2.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (4.6-2m)
- revise texinfo-4.3-install-info-bgzlib.patch for alpha

* Thu Jun 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.6-1m)
  update to 4.6

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5-4m)
- rebuild against zlib 1.1.4-5m

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5-3m)
- add URL tag
- use macros

* Tue Feb 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.5-2m)
- rebuild against zlib-1.1.4-4m

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.5-1m)
  update to 4.5

* Mon Nov 25 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (4.3-1m)
- update to 4.3 ([Momonga-devel.ja:00837] thanks, simm)
- revise zlib and bzlib support patch
- add man pages

* Mon Jul 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (4.2-9m)
- BuildPreReq: ncurses-devel, bzip2-devel

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (4.2-8k)
- Prereq: /sbin/install-info -> info

* Mon Apr 29 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (4.2-6k)
- Provides: /sbin/install-info

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (4.2-4k)
- add texinfo-fileextension.patch

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (4.2-2k)
- update to 4.2
- add -b .bzip2 at patch100
- adapt texinfo-4.0-zlib.patch to version 4.2 
- merge texinfo-3.12h-fix.patch and texinfo-4.0-zlib.patch
  install-info.c part
- separate zlib patch by file Makefile.in and install-info.c
- adapt version 4.2 texinfo.bzip2.dyky.patch
- change name to texinfo-4.2-bzip2-dyky.patch
  This file must patch after patched zlib-patch
  because from -lz to -lz -lbz2
- change spec file encoding system to EUC...
  Oops Mr bero's name was changed, revised

* Fri Mar 15 2002 Kazuhiko <kazuhiko@kondara.org>
- (4.0-20k)
- rebuild against zlib-1.1.4

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (4.0-18k)
- changed to user bzip2

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (4.0-15k)
- enable bzip2 on install-info

* Thu Sep  7 2000 AYUHANA Tomonori <l@kondara.org>
- (4.0-13k)
- remove Requires: filesystem >= 2.0.7-1
- remove Docdir: /usr/share/doc
- use %{_infodir}
- (4.0-12k)
- for stable

* Wed Aug 16 2000 AYUHANA Tomonori <l@kondara.org>
- (4.0-10k)
- obey FHS
- add Requires: filesystem >= 2.0.7-1
- add Docdir: /usr/share/doc
- change %files, %post, %preun against FHS

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Jun 28 2000 Bill Nottingham <notting@redhat.com>
- fix build wackiness with info page compressing

* Fri Jun 16 2000 Bill Nottingham <notting@redhat.com>
- fix info-dir symlink

* Thu May 18 2000 Preston Brown <pbrown@redhat.com>
- use FHS paths for info.

* Fri Mar 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild with current ncurses

* Wed Feb 09 2000 Preston Brown <pbrown@redhat.com>
- wmconfig -> desktop

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions

* Wed Jan 26 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- move info-stnd.info* to the info package, /sbin/install-info it
  in %post (Bug #6632)

* Thu Jan 13 2000 Jeff Johnson <jbj@redhat.com>
- recompile to eliminate ncurses foul-up.

* Tue Nov  9 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 4.0
- handle RPM_OPT_FLAGS

* Tue Sep 07 1999 Cristian Gafton <gafton@redhat.com>
- import version 3.12h into 6.1 tree from HJLu

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Wed Mar 17 1999 Erik Troan <ewt@redhat.com>
- hacked to use zlib to get rid of the requirement on gzip

* Wed Mar 17 1999 Matt Wilson <msw@redhat.com>
- install-info prerequires gzip

* Thu Mar 11 1999 Cristian Gafton <gafton@redhat.com>
- version 3.12f
- make /usr/info/dir to be a %config(noreplace)
* Wed Nov 25 1998 Jeff Johnson <jbj@redhat.com>
- rebuild to fix docdir perms.

* Thu Sep 24 1998 Cristian Gafton <gafton@redhat.com>
- fix allocation problems in install-info

* Wed Sep 23 1998 Jeff Johnson <jbj@redhat.com>
- /sbin/install-info should not depend on /usr/lib/libz.so.1 -- statically
  link with /usr/lib/libz.a.

* Fri Aug 07 1998 Erik Troan <ewt@redhat.com>
- added a prereq of bash to the info package -- see the comment for a
  description of why that was done

* Tue Jun 09 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Tue Jun  9 1998 Jeff Johnson <jbj@redhat.com>
- add %attr to permit non-root build.

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sun Apr 12 1998 Cristian Gafton <gafton@redhat.com>
- added %clean
- manhattan build

* Wed Mar 04 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to version 3.12
- added buildroot

* Sun Nov 09 1997 Donnie Barnes <djb@redhat.com>
- moved /usr/info/dir to /etc/info-dir and made /usr/info/dir a
  symlink to /etc/info-dir.

* Wed Oct 29 1997 Donnie Barnes <djb@redhat.com>
- added wmconfig entry for info

* Wed Oct 01 1997 Donnie Barnes <djb@redhat.com>
- stripped /sbin/install-info

* Mon Sep 22 1997 Erik Troan <ewt@redhat.com>
- added info-dir to filelist

* Sun Sep 14 1997 Erik Troan <ewt@redhat.com>
- added patch from sopwith to let install-info understand gzip'ed info files
- use skeletal dir file from texinfo tarball (w/ bash entry to reduce
  dependency chain) instead (and install-info command everywhere else)
- patches install-info to handle .gz names correctly

* Tue Jun 03 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Feb 25 1997 Erik Troan <ewt@redhat.com>
- patched install-info.c for glibc.
- added /usr/bin/install-info to the filelist

* Tue Feb 18 1997 Michael Fulbright <msf@redhat.com>
- upgraded to version 3.9.
