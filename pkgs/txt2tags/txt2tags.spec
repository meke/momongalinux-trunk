%global momorel 6

Summary: txt2tags text converter
Name:	 txt2tags
Version: 2.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group:	 Applications/Text
Source0: http://txt2tags.googlecode.com/files/txt2tags-%{version}.tgz
NoSource: 0
URL:	 http://txt2tags.sourceforge.net/
Requires: python
BuildRequires: gettext
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Txt2tags converts a text file with minimal markup to HTML, XHTML, SGML, LaTeX, Lout, UNIX man page, MoinMoin, MagicPoint (mgp) and PageMaker. Features: simple, fast, automatic TOC, macros, filters, include, tools, GUI/CLI/Web interfaces, extensive docs.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/%{name}

cp -a txt2tags %{buildroot}%{_bindir}
cp -a extras %{buildroot}%{_datadir}/%{name}

# message files
(cd po
mkdir -p %{buildroot}%{_datadir}/locale
for pofile in `find . -type f -name "*.po" | grep -v ._`
do
    mkdir -p %{buildroot}%{_datadir}/locale/${pofile%%.po}/LC_MESSAGES
    msgfmt -o %{buildroot}%{_datadir}/locale/${pofile%%.po}/LC_MESSAGES/%{name}.mo ${pofile}
done
)

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING  ChangeLog  README  TODO doc samples test
%{_bindir}/*
%{_datadir}/%{name}/extras

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-2m)
- revise for bash-4.0

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-1m)
- update to 2.5
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- %%NoSource -> NoSource

* Tue Apr 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4-1m)
- import to Momonga
