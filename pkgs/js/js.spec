%global momorel 3
%global _target_platform %{_build}

Summary: JavaScript interpreter and libraries
Name: js
Version: 1.8.5
Release: %{momorel}m%{?dist}
# The sources are triple licensed, but when we link against readline which is
# GPL, the result can only be GPL.
%if 0%{?_without_readline:1}
License: GPLv2+ or LGPLv2+ or MPLv1.1
%else
License: GPLv2+
%endif
Group: Development/Languages
URL: http://www.mozilla.org/js/
#Source: http://ftp.mozilla.org/pub/mozilla.org/js/js-%{version}.tar.gz
Source: http://ftp.mozilla.org/pub/mozilla.org/js/js185-1.0.0.tar.gz
NoSource: 0
Patch0:		js-1.8.5-64bit-big-endian.patch
Patch1:		js-1.8.5-secondary-jit.patch
Patch2:		js185-destdir.patch
Patch3:		js-1.8.5-537701.patch
Patch4:		js185-arm-nosoftfp.patch
Patch5:		js185-libedit.patch
Patch6:         0001-Make-js-config.h-multiarch-compatible.patch

Provides:libjs = %{version}-%{release}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Buildrequires: nspr-devel
Buildrequires: readline-devel, ncurses-devel
Provides: libjs = %{version}-%{release}
Obsoletes: SpiderMonkey

%description
JavaScript is the Netscape-developed object scripting language used in millions
of web pages and server applications worldwide. Netscape's JavaScript is a
superset of the ECMA-262 Edition 3 (ECMAScript) standard scripting language,
with only mild differences from the published standard.


%package devel
Summary: Header files, libraries and development documentation for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: ncurses-devel readline-devel
Provides: libjs-devel = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.


%prep
%setup -q -n %{name}-%{version}
%patch0 -p2 -b .64bit-big-endian
%patch1 -p2 -b .secondary-jit
%patch2 -p0 -b .destdir
%patch3 -p1 -b .537701
%patch4 -p1 -b .armhfp
%patch5 -p1 -b .libedit
%patch6 -p1 -b .multiarch
cd js

# Rm parts with spurios licenses, binaries
# Some parts under BSD (but different suppliers): src/assembler
#rm -rf src/assembler src/yarr/yarr src/yarr/pcre src/yarr/wtf src/v8-dtoa
rm -rf src/ctypes/libffi src/t src/tests/src/jstests.jar src/tracevis src/v8

pushd src
autoconf-2.13
%configure \
    --enable-static \
    --with-system-nspr \
    --enable-threadsafe \
    --enable-readline 
popd

# Create pkgconfig file
%{__cat} > libjs.pc << 'EOF'
prefix=%{_prefix}
exec_prefix=%{_prefix}
libdir=%{_libdir}
includedir=%{_includedir}

Name: libjs
Description: JS library
Requires: nspr >= 4.7
Version: %{version}
Libs: -L${libdir} -ljs
Cflags: -DXP_UNIX=1 -DJS_THREADSAFE=1 -I${includedir}/js
EOF


%build
cd js
%{__make} %{?_smp_mflags} -C src

%install
%{__rm} -rf %{buildroot}
cd js
make -C src install DESTDIR=%{buildroot}
# We don't want this 
%{__rm} -f %{buildroot}%{_bindir}/js-config
%{__install} -m 0755 src/jscpucfg src/shell/js %{buildroot}%{_bindir}/
%{__rm} -rf %{buildroot}%{_libdir}/*.a
%{__rm} -rf %{buildroot}%{_libdir}/*.la
    
# For compatibility
# XXX do we really need libjs?!?!?!
pushd %{buildroot}%{_libdir}
%{__ln_s} libmozjs185.so.1.0 libmozjs.so.1
%{__ln_s} libmozjs185.so.1.0 libjs.so.1
%{__ln_s} libmozjs185.so libmozjs.so
%{__ln_s} libmozjs185.so libjs.so
popd

%{__install} -m 0644 libjs.pc %{buildroot}%{_libdir}/pkgconfig/


%clean
%{__rm} -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc js/src/README.html
%{_bindir}/js
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/jscpucfg
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so
%{_includedir}/js

%changelog
* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-3m)
- import fedora patches

* Fri Mar 23 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.5-2m)
- import Patch4

* Thu Dec 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.5-1m)
- update 1.8.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0-6m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-5m)
- remove static library
- modify libjs.pc headers

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.0-4m)
- rebuild against readline6

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-2m)
- remove real_version
- Obsoletes: SpiderMonkey

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- initial commit Momonga Linux

* Thu Apr  9 2009 Matthias Saou <http://freshrpms.net/> 1.70-5
- Update description (#487903).

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org>
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jun  4 2008 Jon McCann <jmccann@redhat.com> - 1.70-3
- Add two missing files (#449715)

* Wed Feb 27 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.70-2
- Rebuild for perl 5.10 (again)

* Sun Feb  3 2008 Matthias Saou <http://freshrpms.net/> 1.70-1
- Update to 1.7.0, as 1.70 to avoid introducing an epoch for now...
- Remove no longer provided perlconnect parts.

* Thu Jan 24 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.60-6
- BR: perl(ExtUtils::Embed)

* Sun Jan 20 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.60-5
- rebuild for new perl

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 1.60-4
- Rebuild for new BuildID feature.

* Mon Aug  6 2007 Matthias Saou <http://freshrpms.net/> 1.60-3
- Update License field.
- Add perl(ExtUtils::MakeMaker) build requirement to pull in perl-devel.

* Fri Feb  2 2007 Matthias Saou <http://freshrpms.net/> 1.60-2
- Include jsopcode.tbl and js.msg in devel (#235481).
- Install static lib mode 644 instead of 755.

* Fri Feb  2 2007 Matthias Saou <http://freshrpms.net/> 1.60-1
- Update to 1.60.
- Rebuild in order to link against ncurses instead of termcap (#226773).
- Add ncurses-devel build requirement and patch s/termcap/ncurses/ in.
- Change mode of perl library from 555 to 755 (#224603).

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 1.5-6
- Fix pkgconfig file (#204232 & dupe #204236).

* Mon Jul 24 2006 Matthias Saou <http://freshrpms.net/> 1.5-5
- FC6 rebuild.
- Enable JS_THREADSAFE in the build (#199696), add patch and nspr build req.

* Mon Mar  6 2006 Matthias Saou <http://freshrpms.net/> 1.5-4
- FC5 rebuild.

* Thu Feb  9 2006 Matthias Saou <http://freshrpms.net/> 1.5-3
- Rebuild for new gcc/glibc.

* Mon Jan 30 2006 Matthias Saou <http://freshrpms.net/> 1.5-2
- Fix .pc file.

* Thu Jan 26 2006 Matthias Saou <http://freshrpms.net/> 1.5-1
- Update to 1.5.0 final.
- Spec file cleanups.
- Move docs from devel to main, since we need the license there.
- Remove no longer needed js-perlconnect.patch.
- Update js-1.5-va_copy.patch.
- Include a pkgconfig file (#178993).

* Tue Apr 19 2005 Ville Skytta <ville.skytta at iki.fi> - 1.5-0.rc6a.6
- Link shared lib with libperl.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Feb 14 2005 David Woodhouse <dwmw2@infradead.org> - 1.5-0.rc6a.4
- Take js-va_copy.patch out of %%ifarch x86_64 so it fixes the PPC build too

* Sun Feb 13 2005 Thorsten Leemhuis <fedora at leemhuis dot info> - 1.5-0.rc6a.3
- Add js-va_copy.patch to fix x86_64; Patch was found in a Mandrake srpm

* Sat Dec 11 2004 Ville Skytta <ville.skytta at iki.fi> - 1.5-0.rc6a.2
- Include perlconnect.
- Include readline support, rebuild using "--without readline" to disable.
- Add libjs* provides for upstream compatibility.
- Install header files in %%{_includedir} instead of %%{_includedir}/js.

* Tue Jun 15 2004 Matthias Saou <http://freshrpms.net> 1.5-0.rc6a
- Update to 1.5rc6a.

* Tue Mar 02 2004 Dag Wieers <dag@wieers.com> - 1.5-0.rc6
- Initial package. (using DAR)

