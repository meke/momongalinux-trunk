%global momorel 2

Summary: Powerful image loading and rendering library
Name: imlib2
Version: 1.4.5
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/2008-01-25/%{name}-%{version}.tar.gz
Source0: http://dl.sourceforge.net/sourceforge/enlightenment/%{name}-%{version}.tar.bz2
NoSource: 0
# Fedora specific multilib hack, upstream should switch to pkgconfig one day
Patch1: imlib2-1.3.0-multilib.patch
Patch10: %{name}-1.4.0-pc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bzip2-devel
BuildRequires: freetype-devel >= 2.1.9-4
BuildRequires: giflib-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXext-devel
BuildRequires: libid3tag-devel >= 0.15.1-5m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel >= 1.2.7
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libtool
BuildRequires: libungif-devel
BuildRequires: pkgconfig
BuildRequires: zlib-devel

%description
Imlib 2 is a library that does image file loading and saving as well
as rendering, manipulation, arbitrary polygon support, etc.  It does
ALL of these operations FAST. Imlib2 also tries to be highly
intelligent about doing them, so writing naive programs can be done
easily, without sacrificing speed.  This is a complete rewrite over
the Imlib 1.x series. The architecture is more modular, simple, and
flexible.

%package devel
Summary: Imlib2 headers, static libraries and documentation
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libjpeg-devel
Requires: libtiff-devel
Requires: libungif-devel
Requires: libpng-devel >= 1.2.7
Requires: freetype-devel >= 2.3.1-2m
Requires: zlib-devel
Requires: bzip2-devel

%description devel
This package contains development files for %{name}.

Imlib 2 is a library that does image file loading and saving as well
as rendering, manipulation, arbitrary polygon support, etc.  It does
ALL of these operations FAST. Imlib2 also tries to be highly
intelligent about doing them, so writing naive programs can be done
easily, without sacrificing speed.  This is a complete rewrite over
the Imlib 1.x series. The architecture is more modular, simple, and
flexible.

%package id3tag-loader
Summary: Imlib2 id3tag-loader
License: GPLv2+
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description id3tag-loader
This package contains a plugin which makes imlib2 capable of parsing id3 tags
of mp3 files. This plugin is packaged separately because it links with
libid3tag which is GPLv2+, thus making imlib2 and apps using it subject to the
conditions of the GPL version 2 (or at your option) any later version.

%prep
%setup -q
%patch1 -p1 -b .multilib~
%patch10 -p1 -b .freetype~

%build
asmopts="--disable-mmx --disable-amd64"
%ifarch %{ix86}
asmopts="--enable-mmx --disable-amd64"
%endif

# stop -L/usr/lib[64] getting added to imlib2-config
export x_libs=" "
%configure --disable-static --with-pic $asmopts
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
%{__rm} -rf --preserve-root %{buildroot}
%makeinstall transform='s,x,x,'

%{__rm} -f %{buildroot}/%{_bindir}/imlib2_test

# DO NOT REMOVE la files
# ship .la files due to a bug in kdelibs (bugzilla.fedora.us #2284):
#  $RPM_BUILD_ROOT%{_libdir}/libImlib2.la \
# find %{buildroot} -name "*.la" -delete

%clean
%{__rm} -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS README ChangeLog TODO
%{_bindir}/imlib2_*
%{_libdir}/libImlib2.so.*
%dir %{_datadir}/imlib2
%{_datadir}/imlib2/data/
%dir %{_libdir}/imlib2/
%dir %{_libdir}/imlib2/filters/
%{_libdir}/imlib2/filters/*.so
%{_libdir}/imlib2/filters/*.la
%dir %{_libdir}/imlib2/loaders/
%{_libdir}/imlib2/loaders/*.so
%{_libdir}/imlib2/loaders/*.la
%exclude %{_libdir}/imlib2/loaders/id3.*
 

%files devel
%defattr(-,root,root,-)
%doc doc/*.gif doc/*.html
%{_bindir}/imlib2-config
%{_includedir}/Imlib2.h
 %{_libdir}/libImlib2.la
 %{_libdir}/libImlib2.so
%{_libdir}/pkgconfig/imlib2.pc

%files id3tag-loader
%{_libdir}/imlib2/loaders/id3.*

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-2m)
- rebuild against libtiff-4.0.1

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.5-1m)
- update 1.4.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-2m)
- full rebuild for mo7 release

* Sat May 15 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4
- remove Patch2. merged upstream
- revised spec

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-9m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-4m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-3m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-2m)
- [SECURITY] CVE-2008-5187
- import a security patch from Debian unstable (1.4.0-1.1)
  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=505714

* Sun Oct 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-1m)
- [SECURITY] CVE-2008-6079
- update to 1.4.2
-- drop Patch0 and Patch2, merged upstream

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1.000-1m)
- merge T4R
-
- changelog is below
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (1.4.1.000-1m)
- - update to 1.4.1.000

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- [SECURITY] CVE-2008-2426
- update to 1.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-4m)
- %%NoSource -> NoSource

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-2m)
- revive la files and move {filters,loaders}/*.la to main package
- http://bugs.kde.org/93359
- https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=142244
- move {filters,loaders}/*.so from devel to main package

* Wed May 30 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.0-1m)
- [SECURITY] CVE-2006-4806 CVE-2006-4807 CVE-2006-4808 CVE-2006-4809
- update to 1.4.0

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-4m)
- imlib2-devel Requires: freetype2-devel -> freetype-devel

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-3m)
- delete libtool library

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-2m)
- change autoreconf to each autotools

* Sun May 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Fri Feb 11 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-4m)
- modify dependency freetype -> freetype2

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-3m)
- @requirements@ was left in imlib2.pc, it's annoying so much...

* Thu Jan 20 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-2m)
- enable x86_64. mmx support doesn't match for x86_64 at this moment.

* Sun Jan  9 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0

* Mon Nov 29 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.2-2m)
- add transform='s,x,x,' at makeinstall section

* Sat Nov 27 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.2-1m)
- [SECURITY] fixed buffer overflow in bmp handling, CAN-2004-0802
- update to 1.1.2

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-3m)
- add Patch0: imlib2-1.1.0-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Wed May 5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.0-2m)
- pkgconfig is missed.

* Wed May 5 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0
- revise specfile

* Sat Jul 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.6-3m)
- fix niwatama reported by sanuki([devel.ja:01975])

* Fri Apr 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.6-2k)

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.5-8k)
- rebuild against libpng-1.2.2

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.5-6k)
- include .la 

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.5-4k)
- disable mmx

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.5-2k)
- create
