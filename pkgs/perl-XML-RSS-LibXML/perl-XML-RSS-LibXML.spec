%global         momorel 2

Name:           perl-XML-RSS-LibXML
Version:        0.3105
Release:        %{momorel}m%{?dist}
Summary:        XML::RSS with XML::LibXML
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-RSS-LibXML/
Source0:        http://www.cpan.org/authors/id/D/DM/DMAKI/XML-RSS-LibXML-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-DateTime-Format-Mail
BuildRequires:  perl-DateTime-Format-W3CDTF
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-UNIVERSAL-require
BuildRequires:  perl-XML-LibXML >= 1.66
Requires:       perl-Class-Accessor
Requires:       perl-DateTime-Format-Mail
Requires:       perl-DateTime-Format-W3CDTF
Requires:       perl-Encode
Requires:       perl-UNIVERSAL-require
Requires:       perl-XML-LibXML >= 1.66
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
XML::RSS::LibXML uses XML::LibXML (libxml2) for parsing RSS instead of
XML::RSS' XML::Parser (expat), while trying to keep interface compatibility
with XML::RSS.

%prep
%setup -q -n XML-RSS-LibXML-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/XML/RSS/LibXML
%{perl_vendorlib}/XML/RSS/LibXML.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3105-2m)
- rebuild against perl-5.20.0

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3105-1m)
- update to 0.3105

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3104-1m)
- update to 0.3104

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-2m)
- rebuild against perl-5.14.2

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3102-1m)
- update to 0.3102

* Wed Jul  6 2011 NARITA Koichi <pulsar@momonfa-linux.org>
- (0.3101-1m)
- update to 0.3101

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3100-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3100-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3100-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3100-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3100-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3100-2m)
- full rebuild for mo7 release

* Tue Jun 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3100-1m)
- update to 0.3100

* Thu Jun 03 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3005-1m)
- update to 0.3005
- Specfile re-generated by cpanspec 1.78.

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3004-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3004-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3004-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3004-2m)
- rebuild against perl-5.10.1

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3004-1m)
- update to 0.3004

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3003-2m)
- rebuild against rpm-4.6

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3003-1m)
- update to 0.3003

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3002-2m)
- rebuild against gcc43

* Mon Oct  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3002-1m)
- update to 0.3002

* Mon Sep 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3001-2m)
- change the version number of required perl-XML-LibXML module

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3001-1m)
- upadate to 0.3001

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.23-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-1m)
- spec file was autogenerated
