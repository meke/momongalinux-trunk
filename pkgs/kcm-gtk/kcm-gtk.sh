#!/bin/sh

# Make sure our customised gtkrc file is loaded.
GTK2RC_KDE4=${HOME}/.gtkrc-2.0-kde4
if [ -z "${GTK2_RC_FILES}" ]; then
  GTK2_RC_FILES=${GTK2RC_KDE4}
elif ! echo ${GTK2_RC_FILES} | /bin/grep -q ${GTK2RC_KDE4} ; then
  GTK2_RC_FILES=${GTK2_RC_FILES}:${GTK2RC_KDE4}
fi
unset GTK2RC_KDE4

export GTK2_RC_FILES
