%global momorel 7
%global         major 0.5
%global         minor 3
%global         env_script 1

Summary:        Configure the appearance of GTK apps in KDE 
Name:           kcm-gtk 
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          User Interface/Desktops
URL:            https://launchpad.net/kcm-gtk 
Source0:        http://launchpad.net/%{name}/%{major}.x/%{version}/+download/%{name}_%{version}.orig.tar.gz 
NoSource:       0
# set GTK2_RC_FILES=~/.gtkrc-2.0-kde4
Source1:        %{name}.sh
Patch0:         %{name}-%{version}-desktop.patch
## upstreamable patches
# ensures GTK2_RC_FILES gets used/updated on first use, avoids 
# possible need for logout/login, code borrowed from kdebase-workspace's krdb.cpp
Patch51:        %{name}-%{version}-gtkrc_setenv.patch
# fix missing umlauts and sharp s in the German translation
# The translations need a lot more fixing than that, but this looks very broken!
Patch52:        %{name}-%{version}-fix-de.patch
# https://bugs.launchpad.net/kcm-gtk/+bug/505988, already upstream
# Modified so that it applies over the setenv patch, and removed index.theme
# existence check: https://bugs.launchpad.net/kcm-gtk/+bug/505988/comments/4
Patch53:        %{name}-%{version}-cursortheme.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 
BuildRequires:  gettext
BuildRequires:  kdelibs-devel
Obsoletes:      gtk-qt-engine <= 1.1

%description
This is a System Settings configuration module for configuring the
appearance of GTK apps in KDE.

%prep
%setup -q 

%patch0 -p1 -b .desktop-ja
%patch51 -p1 -b .gtkrc_setenv
%patch52 -p1 -b .fix-de
%patch53 -p1 -b .cursortheme

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot} 
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang kcm_gtk

%{?env_script:install -p -m755 -D %{SOURCE1} %{buildroot}%{_sysconfdir}/kde/env/kcm-gtk.sh}

%clean
rm -rf %{buildroot} 

%files -f kcm_gtk.lang
%defattr(-,root,root,-)
%doc COPYING Changelog
%{?env_script:%{_sysconfdir}/kde/env/kcm-gtk.sh}
%{_kde4_libdir}/kde4/kcm_gtk.so
%{_kde4_iconsdir}/kcm_gtk.png
%{_kde4_datadir}/kde4/services/kcmgtk.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-6m)
- rebuild for new GCC 4.5

* Mon Sep  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-5m)
- [BUG FIX] set executable bit to %%{_sysconfdir}/kde/env/kcm-gtk.sh

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-4m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-3m)
- adapt kcmgtk.desktop to KDE 4.5 specification

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-2m)
- add desktop.patch

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- import from Fedora devel

* Wed Jul  7 2010 Ville Skytta <ville.skytta@iki.fi> - 0.5.3-4
- Apply modified upstream patch to add cursor theme support (#600976).

* Fri Dec 25 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.3-3
- GTK2_RC_FILES handling moved to kde-settings (#547700)

* Sun Dec 20 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.5.3-2
- fix missing umlauts and sharp s in the German translation

* Fri Oct 30 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.3-1
- kcm-gtk-0.5.3
- .gtkrc-2.0-kde4 doesn't get used (#531788)

* Thu Oct 22 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.1-2
- Requires: kde4-macros(api)...

* Thu Oct 22 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.1-1
- kcm-gtk-0.5.1 (first try)

