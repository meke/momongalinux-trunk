# This is 'set'-file for tcsh
#
 #
 # set 
 #
unset autologout
 # ダルイのでauto-logoutは解除する。
 # 初心者用にはsetした方が良いだろう。default=60minutes.
 # ex.
 # set autologout=90
 # set autologout #
 #
#unset ignoreeof
set ignoreeof
 # C-dによる補完は使わないので、setする。
 #
set history=1000
set savehist=1000
 #
 #set cdpath=(/work)
 # set notify 
 # setするとすぐ報告するようになる。
 # background-jobはpromptが帰って来てから結果を報告させる
 #
set rmstar
 # 多分無いと思うけど、rm *を防ぐ。
 #
set noclobber
 # リダイレクトの失敗を防ぐ。
 #
set editmode=emacs
 # 
 #
set histdup=erase
 # all   : ヒストリリストに無いコマンドなら登録する
 # erase : 直前と同じコマンドだったら登録しない
 # prev  : ヒストリリストにある場合は古い方を消して登録する。
 #
set filec #ファイル名補完
set fignore=(.o .aux .log \~) #補完の例外
set autolist  #C-dを不要にする。逆はrecexact
set showdots
set nostat=(/afs/ /dev/)
 #
 # statシステムコールを呼ばないようにするディレクトリ
 #
set autoexpand # history complete
 #補完の作業でヒストリも展開する
 #
set symlinks=expand
 # chase  :シンボリックリンクは追いかけない
 # ignore :シンボリックリンクを追いかける(内部コマンドとファイル名補完のみ)
 # expand :シンボリックリンクを追いかける(全部)追いたくない時は""で囲む)
 #
set dunique
 # ディレクトリスタックにある時はそれを消して新しく積む
 #
 # set dextract
 # pushd +n で0とnの置換えでなくて、nが0、0は1になる。
 # set pushdtohome
 # pushd が pushd ~の動作になる
 # pushd - で積み変える
 #
 # set savedirs
 # logout時に ~/.cshdirsにスタックを保存する。
 #
set nobeep # ベルは鳴らさない。
set matchbeep=never
 # nomatch	:候補無し時のみ鳴らす
 # ambiguous	:候補無しもしくは複数ある時鳴らす
 # notunique	:補完したものと同じ名前の拡張子付きがある時など鳴らす
 # never	:全く鳴らさない
 #
 #set correct
 # スペルチェック
 # all
 # cmd
 # complete :入力されている間違ったコマンド名を
 #コマンドのプレフィックスと見倣して、残りを補完して実行する。
#
set addsuffix
# 補完する時にdirectoryに/を付ける.
# from Personal UNIX vol.1 p.155
#
set visiblebell
# 警告音を鳴らさずに画面のフラッシュにする.
# emacsでは無駄か.
#
# EOF
