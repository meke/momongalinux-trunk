%global momorel 2

Summary: An enhanced version of csh, the C shell
Name: tcsh
Version: 6.18.01
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Shells
Source0: ftp://ftp.astron.com/pub/tcsh/%{name}-%{version}.tar.gz
NoSource: 0
Source101: tab.dot.tcshrc
Source102: tab.aliases.tcsh
Source103: tab.complete.tcsh
Source104: tab.hosts.tcsh
Source105: tab.set.tcsh
Source106: toms.cshrc
#
Patch1: tcsh-6.15.00-closem.patch
Patch2: tcsh-6.18.01-tinfo.patch
Patch3: tcsh-6.14.00-unprintable.patch
Patch4: tcsh-6.15.00-hist-sub.patch
Patch8: tcsh-6.14.00-syntax.patch
Patch9: tcsh-6.18.01-memoryuse.patch
Patch11: tcsh-6.14.00-order.patch
Patch13: tcsh-6.18.01-mh-color.patch

# The idea is good, but the patch must be rewritten to be accepted by upstream
# (see tcsh mailing list for more information):
Patch14: tcsh-6.18.01-glob-automount.patch

# Patch by upstream (tcsh-6.17.01b http://mx.gw.com/pipermail/tcsh-bugs/2010-May/000673.html):
Patch22: tcsh-6.18.01-dont-print-history-on-verbose.patch
# Proposed upstream - http://github.com/tcsh-org/tcsh/pull/1
Patch28: tcsh-6.17.00-manpage-spelling.patch


#Mo
Patch101: tcsh-6.18.01-termios.patch
Patch111: tcsh.glibc222.time.patch

Provides: csh = %{version}
Provides: /bin/csh
Requires(post): grep
Requires(postun): coreutils, grep
URL: http://www.tcsh.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, automake, ncurses-devel, gettext-devel

%description
Tcsh is an enhanced but completely compatible version of csh, the C
shell.  Tcsh is a command language interpreter which can be used both
as an interactive login shell and as a shell script command processor.
Tcsh includes a command line editor, programmable word completion,
spelling correction, a history mechanism, job control and a C language
like syntax.

%prep
%setup -q
%patch101 -p1 -b .termios
%patch111 -p1

%patch1 -p1 -b .closem
%patch2 -p1 -b .tinfo
%patch3 -p1 -b .unprintable
%patch4 -p1 -b .hist-sub
%patch8 -p1 -b .syntax
%patch9 -p1 -b .memoryuse
%patch11 -p1 -b .order
%patch13 -p1 -b .mh-color
%patch14 -p1 -b .glob-automount
%patch22 -p1 -b .dont-print-history-on-verbose
%patch28 -p1 -b .manpage-spelling

for i in Fixes WishList; do
 iconv -f iso-8859-1 -t utf-8 "$i" > "${i}_" && \
 touch -r "$i" "${i}_" && \
 mv "${i}_" "$i"
done

%build
# For tcsh-6.14.00-tinfo.patch
autoreconf
%configure --without-hesiod
make %{?_smp_mflags} all
make %{?_smp_mflags} -C nls catalogs

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man1 ${RPM_BUILD_ROOT}%{_bindir}
install -m 755 tcsh ${RPM_BUILD_ROOT}%{_bindir}/tcsh
install -m 644 tcsh.man ${RPM_BUILD_ROOT}%{_mandir}/man1/tcsh.1
ln -sf tcsh ${RPM_BUILD_ROOT}%{_bindir}/csh
ln -sf tcsh.1 ${RPM_BUILD_ROOT}%{_mandir}/man1/csh.1

while read lang language ; do
	dest=${RPM_BUILD_ROOT}%{_datadir}/locale/$lang/LC_MESSAGES
	if test -f tcsh.$language.cat ; then
		mkdir -p $dest
		install -p -m 644 tcsh.$language.cat $dest/tcsh
		echo "%lang($lang) %{_datadir}/locale/$lang/LC_MESSAGES/tcsh"
	fi
done > tcsh.lang << _EOF
de german
el greek
en C
es spanish
et et
fi finnish
fr french
it italian
ja ja
pl pl
ru russian
uk ukrainian
_EOF

mkdir -p %{buildroot}%{_datadir}/tcsh

mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE101} %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE102} %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE103} %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE104} %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE105} %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE106} %{buildroot}%{_datadir}/config-sample/%{name}

%clean
rm -rf %{buildroot}

%post
if [ ! -f /etc/shells ]; then
	echo "%{_bindir}/tcsh" >> /etc/shells
	echo "%{_bindir}/csh"  >> /etc/shells
else
	grep -q '^%{_bindir}/tcsh$' /etc/shells || \
	echo "%{_bindir}/tcsh" >> /etc/shells
	grep -q '^%{_bindir}/csh$'  /etc/shells || \
	echo "%{_bindir}/csh"  >> /etc/shells
fi

%postun
if [ ! -x %{_bindir}/tcsh ]; then
	grep -v '%{_bindir}/tcsh$'  /etc/shells | \
	grep -v '%{_bindir}/csh$' > /etc/shells.rpm
	cat /etc/shells.rpm > /etc/shells && rm /etc/shells.rpm
fi

%files -f tcsh.lang
%defattr(-,root,root,-)
%doc BUGS FAQ Fixes NewThings WishList complete.tcsh README
%{_bindir}/tcsh
%{_bindir}/csh
%{_mandir}/man1/*.1*
%{_datadir}/tcsh
%dir %{_datadir}/config-sample/%{name}
%{_datadir}/config-sample/%{name}/*

%changelog
* Tue Mar 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.18.01-2m)
- support UserMove env

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.18.01-1m)
- update to 6.18.01

* Mon Feb  6 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (6.17.00-5m)
- import patch from Fedora 16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.17.00-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.17.00-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.17.00-2m)
- full rebuild for mo7 release

* Sat Dec 12 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.17.00-1m)
- update to 6.17.00
- patches from fc devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.15.00-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.15.00-4m)
- import Patch6-Patch13 from Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.15.00-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.15.00-2m)
- update Patch2,101 for fuzz=0

* Mon Jul 21 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.15.00-1m)
- update to 6.15.00

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.14.00-5m)
- rebuild against gcc43

* Sun Sep  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.14.00-4m)
- BUG ID=192 (Change install dir /usr/bin/csh -> /bin/csh)

* Mon Mar 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.14.00-3m)
- comment out Patch116: tcsh-6.13.00-tgetent.patch
- update Patch112: nlsfile.LANG.patch

* Mon Mar 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.14.00-3m)
- merge fc-devel 6.14-15
- delete --bindir=/bin at configure
- - update Patch9: tcsh-6.14.00-wide-seeks.patch

* Sat Mar 10 2007 TABUCHI Takaaki <tab@momonga-linuxx.org>
- (6.14.00-2m)
- import fedora patch

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (6.14.00-1m)
- up to 6.14.00

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.13.00-3m)
- enable x86_64.

* Sat Jun 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (6.13.00-2m)
- remove a sample .tcshrc

* Sat May 21 2004 TABUCHI Takaaki <tab@momonga-linuxx.org>
- (6.13.00-1m)
- version up to 6.13.00
- comment out included Patch2: tcsh-6.08.02-kanji.patch
- update kludge Patch3
- update Patch12
- comment out Patch13, the patch was obsoleted by Patch12
- tmp comment out Patch14
- comment out Patch15: tcsh-6.12.01-enhanced.patch

* Wed Oct 29 2003 zunda <zunda at freeshell.org>
- (6.12.01-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft).
- please note that pl_PL nls catalog, which is currently not included in
  the package seems to be licensed under GPL

* Sat May  3 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (6.12.01-2m)
- tcsh works as if "complete" shell variable being set to "enhanced"
  even if it is not set. 
  To fix this bug, tcsh-6.12.01-enhanced.patch was added.

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linuxx.org>
- (6.12.01-1m)
- update to 6.12.01
- delete unused define
- use rpm macros
- update patch10

* Thu Jul 25 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (6.12.00-1m)
  update to 6.12.00

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (6.11.02-2k)
- update to 6.11.02.

* Tue Apr 30 2002 Toru Hoshina <t@kondara.org>
- (6.11.01-4k)
- provide /bin/csh explicitly.

* Tue Apr 23 2002 TABUCHI Takaaki <tab@kondara.org>
- (6.11.01-2k)
- update verion 6.11.01
- change Source0 URI, Could not connet to ftp.astron.com
- changte (6.11.00-16k) changelog typo
- patch5: tcsh-6.09.04-strlangeuc.patch is now included

* Tue Dec 11 2001 Tsutomu Yasuda <tom@kondara.org>
- (6.11.00-16k)
  include my .cshrc

* Mon Dec 10 2001 TABUCHI Takaaki <tab@kondara.org>
- (6.11.00-14k)
- add tab.dot.tcshrc and etc. (sourc101..105) as sample

* Wed Nov 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.11.00-12k)
- add tcsh-6.11.00-EUC-JP.patch for locale ja_JP.EUC-JP

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (6.11.00-10k)
- nigittenu

* Mon Nov 12 2001 Motonobu Ichimura <famao@kondara.org>
- (6.11.00-6k)
- add %dir %{_datadir}/config-sample/%{name}

* Mon Oct 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.11.00-4k)
- add dyky.tcshrc as sample

* Mon Sep 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (6.11.00-2k)
  update to 6.11

* Fri Aug 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.10.02-2k)
- update to 6.10.02
- add Polish catalogue for new version

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (6.10.01-6k)
- no more ifarch alpha.

* Mon May 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.10.01-4k)
- build catalog file with LANG environment
- modified typo in finnish catalog

* Sat May 12 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.10.01-2k)
- update to 6.10.01

* Tue May  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.10.00-6k)
- include *.m files

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.10.00-4k)
- rebuild against glibc-2.2.2 and add tcsh.glibc222.time.patch

* Wed Nov 22 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (6.10.00-1k)
- update to 6.10

* Sun Nov 19 2000 AYUHANA Tomonori <l@kondara.org>
- (6.09.04-1k)
- version 6.09.04
- use %configure
- add tcsh-6.09.04-strlangeuc.patch

* Thu Jul 20 2000 AYUHANA Tomonori <l@kondara.org>
- (6.09.03-1k)
- version up 6.09.01 to 6.09.03
- catalogue version up 6.09.01 to 6.09.03
- change %patch0 to %patch10 (for offset)
- add %patch4 (for ja.lamhirh in Makefile)
- rebuild against glibc-2.1.91

* Wed Jul 06 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Mar  9 2000 Shingo Akagaki <dora@kondara.org>
- BuildPreReq: perl

* Wed Mar  8 2000 MATSUDA, Daiki <dyky@df-usa.com>
- update to 6.09.01
- include some original catalogs

* Thu Mar 02 2000 Shingo Akagaki <dora@kondara.org>
- use static link

* Sat Dec 11 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Sat Nov 27 1999 Toru Hoshina <t@kondara.org>
- use ja instead of ja_JP.ujis.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Sep 28 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- updated to 6.09

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- updated to 6.07; added BuildRoot
- cleaned up the spec file; fixed source url

* Wed Sep 03 1997 Erik Troan <ewt@redhat.com>
- added termios hacks for new glibc
- added /bin/csh to file list

* Fri Jun 13 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Fri Feb 07 1997 Erik Troan <ewt@redhat.com>
 - Provides csh, adds and removes /bin/csh from /etc/shells if csh package
isn't installed.
