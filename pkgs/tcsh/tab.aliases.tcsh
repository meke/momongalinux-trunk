# This is aliases file for tcsh(rc).
# 
	alias a 'alias'
	alias j 'jobs'
	alias ls 'ls -gF'
	alias sl '\ls -glF'
	alias das 'du -a \!* | sort -nr | head -20'
	alias st 'source ~/.tcshrc'
	alias rm 'rm -i'
	alias mew 'emacs -nw -f mew'
	alias gnus 'emacs -nw -f gnus'
#	alias zcat 'gzip -cd'
	alias wgetm 'wget -m --no-parent'
	alias kernel 'finger @www.kernel.org'

	alias i 'imget'
	alias f 'from'
	alias h 'history 20'
	alias rf '\rm -rvf'

	alias	ncftplsx 'ncftpls -x"-ltr"'
	alias	fd 'df'

	alias ff	'from'
	alias fff	'from'
#EOF
