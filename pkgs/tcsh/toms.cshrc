set path=(~/tools ~/bin /usr/local/{bin,sbin} /usr/X11R6/bin /usr/bin /bin /usr/sbin \
	/sbin /usr/games)
set history=1000
set savehist=$history
set autocorrect
set autooexpand
set autolist
set correct=cmd
unset autologout
# for pushd
set pushdtohome
set pushdsilent
set savedirs
set dunique
# set symlinks=expand
set nobeep

#  new style
set dspmbyte=euc

alias ls 'ls --color=auto -F'
alias hd 'od -t x1 -v'
alias s 'sudo'
alias dvips 'dvips \!^ -o `basename \!^ .dvi`.ps'

set noglob
complete {cd,pushd}	p/1/d/
complete {zcat,gunzip}	n/*/f:*.{{gz,Z},tgz}/
complete bunzip2        n/*/f:*.{bz2}/
complete dd		c/if=/f/ c/of=/f/
complete tar		p/2/f:*.{tar,tar.{gz,Z,bz2},taz,taZ,tgz}/\
			c/[ctx]vf*/"(z y O p B)"/\
			p/1/"(cvf tvf xvf czvf tzvf xzvf cyvf tyvf xyvf)"/ n/*/f/
unset noglob
set prompt="%B%n@%m:%~%b%# %L"

bindkey -e
bindkey -k down history-search-forward
bindkey -k up   history-search-backward
bindkey -b C-?  delete-char-or-list-or-eof

setenv PERL_BADLANG 0
setenv CVSROOT :ext:tom@cvs.momonga-linux.org:/home/cvs
setenv CVSEDITOR jed
setenv CVS_RSH ssh
setenv EDITOR jed
setenv PAGER w3m
setenv LESS '-r -f -i'

if ($?EMACS) then
    stty nl
    alias ls 'ls --color=no -F'
endif

if ($?REMOTEHOST) then
    setenv DISPLAY ${REMOTEHOST}:0.0
endif
