%global momorel 7

Summary: TyopoytaORvelo System MOnitor
Name: torsmo
Version: 0.18
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/System
URL: http://torsmo.sourceforge.net/
Source0: http://dl.sf.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
Source1: %{name}.desktop
# convert from http://torsmo.sourceforge.net/shot3.jpg
Source2: %{name}-128.png
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXft-devel
BuildRequires: libXrender-devel

%description
Torsmo (TyopoytaORvelo System MOnitor) is a system monitor that sits in the
corner of your desktop. Torsmo can show various information about your system
and its peripherals. Torsmo is very light and customizable, and it renders
text on the root window.

For a sample configuration take a look torsmorc.sample.

%prep
%setup -q

%build
%configure \
	--enable-xft \
	--enable-proc-uptime \
	--enable-seti \
	--x-includes=%{_includedir} \
	--x-libraries=%{_libdir}

make %{?_smp_mflags}

%install
[ -d %{buildroot} ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor "" \
        --dir %{buildroot}%{_datadir}/applications \
        %{SOURCE1}

# install and convert icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64,128x128}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
convert -scale 16x16 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ %{buildroot} != / ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files 
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README readme.html %{name}rc.sample
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-5m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18-4m)
- add icons for menu and fix up desktop file
- add %%post and %%postun
- remove mixed tab and space from spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-2m)
- rebuild against rpm-4.6

* Sun Jun  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18-1m)
- initial Momonga package
