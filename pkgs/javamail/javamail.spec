%global momorel 4

%global with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name:		javamail
Version:	1.4.3
Release:	%{momorel}m%{?dist}
Summary:	Java Mail API

Group:		Development/Libraries
License:	"CDDL" or "GPLv2 with exceptions"
URL:		http://java.sun.com/products/javamail/

# Parent POM
Source0:	http://download.java.net/maven/2/com/sun/mail/all/%{version}/all-%{version}.pom

# POMs and source files for things that get built
Source1:	http://download.java.net/maven/2/javax/mail/mail/%{version}/mail-%{version}-sources.jar
Source2:	http://download.java.net/maven/2/javax/mail/mail/%{version}/mail-%{version}.pom
Source3:	http://download.java.net/maven/2/com/sun/mail/dsn/%{version}/dsn-%{version}-sources.jar
Source4:	http://download.java.net/maven/2/com/sun/mail/dsn/%{version}/dsn-%{version}.pom

# Additional POMs for things that are provided by the monolithic mail.jar
Source5:	http://download.java.net/maven/2/javax/mail/mailapi/%{version}/mailapi-%{version}.pom
Source6:	http://download.java.net/maven/2/com/sun/mail/imap/%{version}/imap-%{version}.pom
Source7:	http://download.java.net/maven/2/com/sun/mail/pop3/%{version}/pop3-%{version}.pom
Source8:	http://download.java.net/maven/2/com/sun/mail/smtp/%{version}/smtp-%{version}.pom

# Parent POM for many of the above
# http://kenai.com/projects/javamail/sources/mercurial/content/parent-distrib/pom.xml?raw=true
Source9:	%{name}-parent-distrib.pom

# Remove Maven plugins we don't have yet
# Remove unavailable-on-Fedora dependencies from pom.xml
Patch0:		%{name}-cleanup-poms.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	jpackage-utils
BuildRequires:	maven2
BuildRequires:	maven2-plugin-assembly
BuildRequires:	maven2-plugin-compiler 
BuildRequires:	maven2-plugin-dependency
BuildRequires:	maven2-plugin-install
BuildRequires:	maven2-plugin-jar
BuildRequires:	maven2-plugin-javadoc
BuildRequires:	maven2-plugin-resources
BuildRequires:	maven2-plugin-site
BuildRequires:	maven-plugin-bundle
BuildRequires:	maven-surefire-maven-plugin
BuildRequires:	tomcat5
BuildRequires:	tomcat5-jsp-2.0-api

BuildRequires:	java-devel >= 1.6.0

Requires:	jpackage-utils
Requires(post):	jpackage-utils
Requires(postun): jpackage-utils

# Requirements from POMs
Requires:	tomcat5-jsp-2.0-api

# Adapted from the classpathx-mail (and JPackage glassfish-javamail) Provides
Provides:	javamail-monolithic = 0:%{version}

%if %{with_gcj}
BuildRequires:	java-gcj-compat-devel >= 1.0.31
Requires(post):	java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch:	noarch
%endif


%description
The JavaMail API provides a platform-independent and protocol-independent
framework to build mail and messaging applications. 


%package javadoc
Summary:	Javadoc for %{name}
Group:		Documentation
Requires:	jpackage-utils >= 0:1.7.5
BuildArch:	noarch

%description javadoc
%{summary}.


%prep
%setup -c -T
mkdir -p mail dsn

(cd mail && jar xvf %SOURCE1 && cp %SOURCE2 ./pom.xml)
(cd dsn && jar xvf %SOURCE3 && cp %SOURCE4 ./pom.xml)

for sub in *; do
	pushd $sub
	mkdir -p src/main/java src/main/resources
	mv META-INF src/main/resources
	[ -e com ] && mv com src/main/java
	[ -e javax ] && mv javax src/main/java
	popd
done

cp %SOURCE0 ./pom.xml
mkdir poms
cp %SOURCE5 %SOURCE6 %SOURCE7 %SOURCE8 %SOURCE9 poms

%patch0 -p1

# Convert license file to UTF-8
for file in mail/src/main/resources/META-INF/*.txt; do
	iconv -f ISO-8859-1 -t UTF-8 -o $file.new $file && \
	touch -r $file $file.new && \
	mv $file.new $file
done


%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
	-P deploy \
	-Dmaven.repo.local=$MAVEN_REPO_LOCAL \
	package javadoc:javadoc


%install
rm -rf $RPM_BUILD_ROOT

install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -d -m 755 p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
(cd $RPM_BUILD_ROOT%{_javadocdir} && ln -sf %{name}-%{version} %{name})

install -pm 644 pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}-all.pom
%add_to_maven_depmap com.sun.mail all %{version} JPP %{name}-all

# Install everything
for sub in mail dsn; do
	install -m 644 $sub/target/$sub.jar \
		$RPM_BUILD_ROOT%{_javadir}/%{name}/$sub-%{version}.jar
	install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/$sub/
	cp -pr $sub/target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/$sub/
done
install -m 644 mail/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}.pom
install -m 644 dsn/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}-dsn.pom

# Install the remaining POMs
for sub in mailapi imap pop3 smtp; do
	install -m 644 poms/$sub-%{version}.pom \
		$RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}-$sub.pom
done

# Add maven dependency information
%add_to_maven_depmap javax.mail mail %{version} JPP/%{name} mail
%add_to_maven_depmap com.sun.mail dsn %{version} JPP/%{name} dsn
%add_to_maven_depmap javax.mail mailapi %{version} JPP/%{name} mail
%add_to_maven_depmap com.sun.mail imap %{version} JPP/%{name} mail
%add_to_maven_depmap com.sun.mail pop3 %{version} JPP/%{name} mail
%add_to_maven_depmap com.sun.mail smtp %{version} JPP/%{name} mail

install -m 644 poms/%{name}-parent-distrib.pom \
	$RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-%{name}-parent-distrib.pom
%add_to_maven_depmap com.sun.mail parent-distrib %{version} JPP %{name}

(cd $RPM_BUILD_ROOT%{_javadir}/%{name} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif


%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap
%if %{with_gcj}
  if [ -x %{_bindir}/rebuild-gcj-db ] 
  then
    %{_bindir}/rebuild-gcj-db
  fi
%endif

%postun
%update_maven_depmap
%if %{with_gcj}
  if [ -x %{_bindir}/rebuild-gcj-db ] 
  then
    %{_bindir}/rebuild-gcj-db
  fi
%endif


%files
%defattr(-,root,root,-)
%doc mail/src/main/resources/META-INF/LICENSE.txt mail/overview.html
%{_javadir}/%{name}
%config(noreplace) %{_mavendepmapfragdir}/*
%{_datadir}/maven2/poms/*.pom
%if %{with_gcj}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-1m)
- import from Fedora 13

* Fri Jan  8 2010 Mary Ellen Foster <mefoster at gmail.com> 1.4.3-2
- Remove unnecessary (build)requirement tomcat5-servlet-2.4-api
- Move jar files into subdirectory

* Wed Dec  2 2009 Mary Ellen Foster <mefoster at gmail.com> 1.4.3-1
- Initial package
