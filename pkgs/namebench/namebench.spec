%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
#%%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

Summary:        Open-source DNS Benchmark Utility
Name:           namebench
Version:        0.9.6
Release:        %{momorel}m%{?dist}
License:        Apache
Group:          Applications/System
URL:            http://code.google.com/p/namebench/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        http://namebench.googlecode.com/files//%{name}-%{version}.tgz
NoSource:       0
Patch0:          namebench-096_svn20091204.patch.bz2
BuildRequires:  python >= 2.7
Requires:       python tkinter

%description
Speed up your internet experience by finding the best DNS servers available!
namebench can utilize your web browser history, tcpdump output,
or standardized datasets in order to provide an individualized recommendation.
namebench is completely free and does not modify your system in any way.
This project began as a 20% project at Google. 
namebench runs on Mac OS X, Windows, and UNIX,
and is available with a graphical user interface as well as a command-line interface.
It was written using Python, Tkinter, and PyObjC. 

%prep
%setup -q
%patch0 -p1

%build
%{__python} setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

# move installed under /usr/namebench files into /usr/share/namebench
%{__mkdir_p} %{buildroot}%{_datadir}
%{__mv} %{buildroot}%{_prefix}/%{name} %{buildroot}%{_datadir}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.txt COPYING
%{_bindir}/*
%{python_sitelib}/*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*

%changelog
* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-3m)
- full rebuild for mo7 release

* Fri Dec  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-2m)
- add Patch0: namebench-096_svn20091204.patch.bz2

* Fri Dec  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-1m)
- initial package for Momonga
