%global momorel 1
# -*- rpm-spec -*-

# A client only build will create a libvirt.so only containing
# the generic RPC driver, and test driver and no libvirtd
# Default to a full server + client build
%define client_only        0

# Now turn off server build in certain cases

# Disable all server side drivers if client only build requested
%if %{client_only}
%define server_drivers     0
%else
%define server_drivers     1
%endif


# Now set the defaults for all the important features, independent
# of any particular OS

# Always build with dlopen'd modules
%define with_driver_modules 1

# First the daemon itself
%define with_libvirtd      0%{!?_without_libvirtd:%{server_drivers}}
%define with_avahi         0%{!?_without_avahi:%{server_drivers}}

# Then the hypervisor drivers that run on local host
%define with_xen           0%{!?_without_xen:%{server_drivers}}
%define with_qemu          0%{!?_without_qemu:%{server_drivers}}
%define with_openvz        0%{!?_without_openvz:%{server_drivers}}
%define with_lxc           0%{!?_without_lxc:%{server_drivers}}
%define with_vbox          0%{!?_without_vbox:%{server_drivers}}
%define with_uml           0%{!?_without_uml:%{server_drivers}}
%define with_uml           0%{!?_without_uml:%{server_drivers}}
%define with_libxl         0%{!?_without_libxl:%{server_drivers}}
%define with_vmware        0%{!?_without_vmware:%{server_drivers}}

# Then the hypervisor drivers that talk a native remote protocol
%define with_phyp          0%{!?_without_phyp:1}
%define with_esx           0%{!?_without_esx:1}
%define with_xenapi        0%{!?_without_xenapi:1}

# Then the secondary host drivers
%define with_network       0%{!?_without_network:%{server_drivers}}
%define with_storage_fs    0%{!?_without_storage_fs:%{server_drivers}}
%define with_storage_lvm   0%{!?_without_storage_lvm:%{server_drivers}}
%define with_storage_iscsi 0%{!?_without_storage_iscsi:%{server_drivers}}
%define with_storage_disk  0%{!?_without_storage_disk:%{server_drivers}}
%define with_storage_mpath 0%{!?_without_storage_mpath:%{server_drivers}}
%define with_numactl       0%{!?_without_numactl:%{server_drivers}}
%define with_selinux       0%{!?_without_selinux:%{server_drivers}}

# A few optional bits off by default, we enable later
%define with_polkit        0%{!?_without_polkit:0}
%define with_capng         0%{!?_without_capng:0}
%define with_netcf         0%{!?_without_netcf:0}
%define with_udev          0%{!?_without_udev:0}
%define with_yajl          0%{!?_without_yajl:0}
%define with_nwfilter      0%{!?_without_nwfilter:0}
%define with_libpcap       0%{!?_without_libpcap:0}
%define with_macvtap       0%{!?_without_macvtap:0}
%define with_libnl         0%{!?_without_libnl:0}
%define with_audit         0%{!?_without_audit:0}
%define with_dtrace        0%{!?_without_dtrace:0}
%define with_cgconfig      0%{!?_without_cgconfig:0}
%define with_sanlock       0%{!?_without_sanlock:0}

# Non-server/HV driver defaults which are always enabled
%define with_python        0%{!?_without_python:1}
%define with_sasl          0%{!?_without_sasl:1}


# Finally set the OS / architecture specific special cases

# Xen is available only on i386 x86_64 ia64
%ifnarch i386 i586 i686 x86_64 ia64
%define with_xen 0
%endif

# Numactl is not available on s390[x]
%ifarch s390 s390x
%define with_numactl 0
%endif

%global with_polkit    0%{!?_without_polkit:1}
%global with_capng     0%{!?_without_capng:1}
%global with_netcf     0%{!?_without_netcf:%{server_drivers}}
%global with_udev     0%{!?_without_udev:%{server_drivers}}
%global with_yajl     0%{!?_without_yajl:%{server_drivers}}
%global with_audit    0%{!?_without_audit:1}
%global with_dtrace 1

# Enable sanlock library for lock management with QEMU
# temporary remove sanlock support in F16 as it seems too old
# checking for sanlock_restrict in -lsanlock... no
%define with_sanlock  0%{!?_without_sanlock:%{server_drivers}}

# Enable libpcap library
%if %{with_qemu}
%define with_nwfilter 0%{!?_without_nwfilter:%{server_drivers}}
%define with_libpcap  0%{!?_without_libpcap:%{server_drivers}}
%define with_macvtap  0%{!?_without_macvtap:%{server_drivers}}
%endif

%if %{with_macvtap}
%define with_libnl 1
%endif

# Pull in cgroups config system
%if %{with_qemu} || %{with_lxc}
%global with_cgconfig 0%{!?_without_cgconfig:1}
%endif

# Force QEMU to run as non-root
%global qemu_user  qemu
%global qemu_group  qemu

# there's no use compiling the network driver without
# the libvirt daemon
%if ! %{with_libvirtd}
%define with_network 0
%endif

Summary: Library providing a simple virtualization API
Name: libvirt
Version: 1.2.5
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Libraries
Source: http://libvirt.org/sources/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
URL: http://libvirt.org/
BuildRequires: python-devel

# The client side, i.e. shared libs and virsh are in a subpackage
Requires: %{name}-client = %{version}-%{release}

# Used by many of the drivers, so turn it on whenever the
# daemon is present
%if %{with_libvirtd}
Requires: bridge-utils
%endif
%if %{with_network}
Requires: dnsmasq >= 2.41
Requires: iptables
%endif
%if %{with_nwfilter}
Requires: ebtables
Requires: iptables
Requires: iptables-ipv6
%endif
%if %{with_udev}
Requires: udev >= 145
%endif
%if %{with_polkit}
Requires: polkit >= 0.93
%endif
%if %{with_storage_fs}
# For mount/umount in FS driver
BuildRequires: util-linux
# For showmount in FS driver (netfs discovery)
BuildRequires: nfs-utils
Requires: nfs-utils
# For glusterfs
Requires: glusterfs-client >= 2.0.1
%endif
%if %{with_qemu}
# From QEMU RPMs
Requires: qemu-img
# For image compression
Requires: gzip
Requires: bzip2
Requires: lzop
Requires: xz
%else
%if %{with_xen}
# From Xen RPMs
Requires: xen-runtime
%endif
%endif
%if %{with_storage_lvm}
# For LVM drivers
Requires: lvm2
%endif
%if %{with_storage_iscsi}
# For ISCSI driver
Requires: iscsi-initiator-utils
%endif
%if %{with_storage_disk}
# For disk driver
Requires: parted >= 3.1
%endif
%if %{with_storage_mpath}
# For multipath support
Requires: device-mapper
%endif
%if %{with_cgconfig}
Requires: libcgroup
%endif
%if %{with_xen}
BuildRequires: xen-devel
%endif
BuildRequires: libxml2-devel
BuildRequires: xhtml1-dtds
BuildRequires: readline-devel
BuildRequires: ncurses-devel
BuildRequires: gettext
BuildRequires: gnutls-devel >= 3.2.0
%if %{with_udev}
BuildRequires: libudev-devel >= 145
BuildRequires: libpciaccess-devel >= 0.10.9
%endif
%if %{with_yajl}
BuildRequires: yajl-devel >= 2.0.0
%endif
%if %{with_libpcap}
BuildRequires: libpcap-devel
%endif
%if %{with_libnl}
BuildRequires: libnl-devel
%endif
%if %{with_avahi}
BuildRequires: avahi-devel
%endif
%if %{with_selinux}
BuildRequires: libselinux-devel
%endif
%if %{with_network}
BuildRequires: dnsmasq >= 2.41
%endif
BuildRequires: bridge-utils
%if %{with_sasl}
BuildRequires: cyrus-sasl-devel
%endif
%if %{with_polkit}
# Only need the binary, not -devel
BuildRequires: polkit >= 0.93
%endif
%if %{with_storage_fs}
# For mount/umount in FS driver
BuildRequires: util-linux
%endif
%if %{with_qemu}
# From QEMU RPMs
BuildRequires: qemu-img
%else
%if %{with_xen}
# From Xen RPMs
BuildRequires: xen-runtime
%endif
%endif
%if %{with_storage_lvm}
# For LVM drivers
BuildRequires: lvm2
%endif
%if %{with_storage_iscsi}
# For ISCSI driver
BuildRequires: iscsi-initiator-utils
%endif
%if %{with_storage_disk}
# For disk driver
BuildRequires: parted-devel >= 3.1
%endif
%if %{with_storage_mpath}
# For Multipath support
BuildRequires: device-mapper-devel
%endif
%if %{with_numactl}
# For QEMU/LXC numa info
BuildRequires: numactl-devel
%endif
%if %{with_capng}
BuildRequires: libcap-ng-devel >= 0.5.0
%endif
%if %{with_phyp}
BuildRequires: libssh2-devel
%endif
%if %{with_netcf}
BuildRequires: netcf-devel >= 0.1.4
%endif
%if %{with_esx}
BuildRequires: libcurl-devel
%endif
%if %{with_audit}
BuildRequires: audit-libs-devel
%endif
%if %{with_dtrace}
# we need /usr/sbin/dtrace
BuildRequires: systemtap-sdt-devel
%endif
%if %{with_sanlock}
BuildRequires: sanlock-devel >= 1.8
%endif

# Fedora build root suckage
BuildRequires: gawk

# for Momonga
BuildRequires: sed

%description
Libvirt is a C toolkit to interact with the virtualization capabilities
of recent versions of Linux (and other OSes). The main package includes
the libvirtd server exporting the virtualization support.

%package client
Summary: Client side library and utilities of the libvirt library
Group: Development/Libraries
Requires: readline
Requires: ncurses
# So remote clients can access libvirt over SSH tunnel
# (client invokes 'nc' against the UNIX socket on the server)
Requires: nc
%if %{with_sasl}
Requires: cyrus-sasl
# Not technically required, but makes 'out-of-box' config
# work correctly & doesn't have onerous dependencies
Requires: cyrus-sasl-md5
%endif

%description client
Shared libraries and client binaries needed to access to the
virtualization capabilities of recent versions of Linux (and other OSes).

%package devel
Summary: Libraries, includes, etc. to compile with the libvirt library
Group: Development/Libraries
Requires: %{name}-client = %{version}-%{release}
Requires: pkgconfig
%if %{with_xen}
Requires: xen-devel
%endif

%description devel
Includes and documentations for the C library providing an API to use
the virtualization capabilities of recent versions of Linux (and other OSes).

%if %{with_sanlock}
%package lock-sanlock
Summary: Sanlock lock manager plugin for QEMU driver
Group: Development/Libraries
Requires: sanlock
Requires: %{name} = %{version}-%{release}

%description lock-sanlock
Includes the Sanlock lock manager plugin for the QEMU
driver
%endif

%prep
%setup -q

%{__sed} -i 's/rc.d\/init.d/init.d/g' src/Makefile.*
%{__sed} -i 's/rc.d\/init.d/init.d/g' daemon/Makefile.*
%{__sed} -i 's/rc.d\/init.d/init.d/g' tools/Makefile.*

%build
%if ! %{with_xen}
%define _without_xen --without-xen
%endif

%if ! %{with_qemu}
%define _without_qemu --without-qemu
%endif

%if ! %{with_openvz}
%define _without_openvz --without-openvz
%endif

%if ! %{with_lxc}
%define _without_lxc --without-lxc
%endif

%if ! %{with_vbox}
%define _without_vbox --without-vbox
%endif

%if ! %{with_xenapi}
%define _without_xenapi --without-xenapi
%endif

%if ! %{with_sasl}
%define _without_sasl --without-sasl
%endif

%if ! %{with_avahi}
%define _without_avahi --without-avahi
%endif

%if ! %{with_phyp}
%define _without_phyp --without-phyp
%endif

%if ! %{with_esx}
%define _without_esx --without-esx
%endif

%if ! %{with_vmware}
%define _without_vmware --without-vmware
%endif

%if ! %{with_polkit}
%define _without_polkit --without-polkit
%endif

%if ! %{with_libvirtd}
%define _without_libvirtd --without-libvirtd
%endif

%if ! %{with_uml}
%define _without_uml --without-uml
%endif

%if ! %{with_network}
%define _without_network --without-network
%endif

%if ! %{with_storage_fs}
%define _without_storage_fs --without-storage-fs
%endif

%if ! %{with_storage_lvm}
%define _without_storage_lvm --without-storage-lvm
%endif

%if ! %{with_storage_iscsi}
%define _without_storage_iscsi --without-storage-iscsi
%endif

%if ! %{with_storage_disk}
%define _without_storage_disk --without-storage-disk
%endif

%if ! %{with_storage_mpath}
%define _without_storage_mpath --without-storage-mpath
%endif

%if ! %{with_numactl}
%define _without_numactl --without-numactl
%endif

%if ! %{with_capng}
%define _without_capng --without-capng
%endif

%if ! %{with_netcf}
%define _without_netcf --without-netcf
%endif

%if ! %{with_selinux}
%define _without_selinux --without-selinux
%endif

%if ! %{with_udev}
%define _without_udev --without-udev
%endif

%if ! %{with_yajl}
%define _without_yajl --without-yajl
%endif

%if ! %{with_libpcap}
%define _without_libpcap --without-libpcap
%endif

%if ! %{with_macvtap}
%define _without_macvtap --without-macvtap
%endif

%if ! %{with_audit}
%define _without_audit --without-audit
%endif

%if ! %{with_dtrace}
%define _without_dtrace --without-dtrace
%endif

%if ! %{with_driver_modules}
%define _without_driver_modules --without-driver-modules
%endif

%define when  %(date +"%%F-%%T")
%define where %(hostname)
%define who   %{?packager}%{!?packager:Unknown}
%define with_packager --with-packager="%{who}, %{when}, %{where}"
%define with_packager_version --with-packager-version="%{release}"

%configure %{?_without_xen} \
           %{?_without_qemu} \
           %{?_without_openvz} \
           %{?_without_lxc} \
           %{?_without_vbox} \
           %{?_without_xenapi} \
           %{?_without_sasl} \
           %{?_without_avahi} \
           %{?_without_polkit} \
           %{?_without_libvirtd} \
           %{?_without_uml} \
           %{?_without_one} \
           %{?_without_phyp} \
           %{?_without_esx} \
           %{?_without_vmware} \
           %{?_without_network} \
           %{?_without_storage_fs} \
           %{?_without_storage_lvm} \
           %{?_without_storage_iscsi} \
           %{?_without_storage_disk} \
           %{?_without_storage_mpath} \
           %{?_without_numactl} \
           %{?_without_capng} \
           %{?_without_netcf} \
           %{?_without_selinux} \
           %{?_without_udev} \
           %{?_without_yajl} \
           %{?_without_libpcap} \
           %{?_without_macvtap} \
           %{?_without_audit} \
           %{?_without_dtrace} \
           %{?_without_driver_modules} \
           %{with_packager} \
           %{with_packager_version} \
           --without-hal \
           --with-qemu-user=%{qemu_user} \
           --with-qemu-group=%{qemu_group} \
           --with-init-script=systemd \
           --with-remote-pid-file=%{_localstatedir}/run/libvirtd.pid
make %{?_smp_mflags}
gzip -9 ChangeLog

%install
rm -rf %{buildroot}

%makeinstall SYSTEMD_UNIT_DIR=%{buildroot}%{_unitdir}
for i in object-events dominfo domsuspend hellolibvirt openauth xml/nwfilter systemtap
do
  (cd examples/$i ; make clean ; rm -rf .deps .libs Makefile Makefile.in)
done
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/python*/site-packages/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/python*/site-packages/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/libvirt/lock-driver/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/libvirt/lock-driver/*.a
%if %{with_driver_modules}
rm -f $RPM_BUILD_ROOT%{_libdir}/libvirt/connection-driver/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/libvirt/connection-driver/*.a
%endif

%if %{with_network}
install -d -m 0755 $RPM_BUILD_ROOT%{_datadir}/lib/libvirt/dnsmasq/
# We don't want to install /etc/libvirt/qemu/networks in the main %files list
# because if the admin wants to delete the default network completely, we don't
# want to end up re-incarnating it on every RPM upgrade.
install -d -m 0755 $RPM_BUILD_ROOT%{_datadir}/libvirt/networks/
cp $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/qemu/networks/default.xml \
   $RPM_BUILD_ROOT%{_datadir}/libvirt/networks/default.xml
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/qemu/networks/default.xml
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/qemu/networks/autostart/default.xml
# Strip auto-generated UUID - we need it generated per-install
sed -i -e "/<uuid>/d" $RPM_BUILD_ROOT%{_datadir}/libvirt/networks/default.xml
%else
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/qemu/networks/default.xml
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/qemu/networks/autostart/default.xml
%endif
%if ! %{with_qemu}
rm -f $RPM_BUILD_ROOT%{_datadir}/augeas/lenses/libvirtd_qemu.aug
rm -f $RPM_BUILD_ROOT%{_datadir}/augeas/lenses/tests/test_libvirtd_qemu.aug
%endif
%find_lang %{name}

%if ! %{with_lxc}
rm -f $RPM_BUILD_ROOT%{_datadir}/augeas/lenses/libvirtd_lxc.aug
rm -f $RPM_BUILD_ROOT%{_datadir}/augeas/lenses/tests/test_libvirtd_lxc.aug
%endif

%if %{client_only}
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/libvirt-%{version}
%endif

%if ! %{with_libvirtd}
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/nwfilter
mv $RPM_BUILD_ROOT%{_datadir}/doc/libvirt-%{version}/html \
   $RPM_BUILD_ROOT%{_datadir}/doc/libvirt-devel-%{version}/
%endif

%if ! %{with_qemu}
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/qemu.conf
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/libvirtd.qemu
%endif
%if ! %{with_lxc}
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/libvirt/lxc.conf
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/libvirtd.lxc
%endif
%if ! %{with_uml}
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/libvirtd.uml
%endif

%clean
rm -fr %{buildroot}

%check
cd tests
make
# These tests don't current work in a mock build root
for i in nodeinfotest seclabeltest
do
  rm -f $i
  printf 'int main(void) { return 0; }' > $i.c
  printf '#!/bin/sh\nexit 0\n' > $i
  chmod +x $i
done
# 1.2.3 one test is segfaulting in mock, need to investigate
#if ! make check VIR_TEST_DEBUG=1
#then
#  cat test-suite.log || true
#  exit 1
#fi

%pre
# Normally 'setup' adds this in /etc/passwd, but this is
# here for case of upgrades from earlier Fedora/RHEL. This
# UID/GID pair is reserved for qemu:qemu
getent group kvm >/dev/null || groupadd -g 36 -r kvm
getent group qemu >/dev/null || groupadd -g 107 -r qemu
getent passwd qemu >/dev/null || \
  useradd -r -u 107 -g qemu -G kvm -d / -s /sbin/nologin \
    -c "qemu user" qemu

%post

%if %{with_libvirtd}
%if %{with_network}
# We want to install the default network for initial RPM installs
# or on the first upgrade from a non-network aware libvirt only.
# We check this by looking to see if the daemon is already installed
if ! /sbin/chkconfig libvirtd && test ! -f %{_sysconfdir}/libvirt/qemu/networks/default.xml
then
    UUID=`/usr/bin/uuidgen`
    sed -e "s,</name>,</name>\n  <uuid>$UUID</uuid>," \
         < %{_datadir}/libvirt/networks/default.xml \
         > %{_sysconfdir}/libvirt/qemu/networks/default.xml
    ln -s ../default.xml %{_sysconfdir}/libvirt/qemu/networks/autostart/default.xml
fi

# All newly defined networks will have a mac address for the bridge
# auto-generated, but networks already existing at the time of upgrade
# will not. We need to go through all the network configs, look for
# those that don't have a mac address, and add one.

network_files=$( (cd %{_localstatedir}/lib/libvirt/network && \
                  grep -L "mac address" *.xml; \
                  cd %{_sysconfdir}/libvirt/qemu/networks && \
                  grep -L "mac address" *.xml) 2>/dev/null \
                | sort -u)

for file in $network_files
do
   # each file exists in either the config or state directory (or both) and
   # does not have a mac address specified in either. We add the same mac
   # address to both files (or just one, if the other isn't there)

   mac4=`printf '%X' $(($RANDOM % 256))`
   mac5=`printf '%X' $(($RANDOM % 256))`
   mac6=`printf '%X' $(($RANDOM % 256))`
   for dir in %{_localstatedir}/lib/libvirt/network \
              %{_sysconfdir}/libvirt/qemu/networks
   do
      if test -f $dir/$file
      then
         sed -i.orig -e \
           "s|\(<bridge.*$\)|\0\n  <mac address='52:54:00:$mac4:$mac5:$mac6'/>|" \
           $dir/$file
         if test $? != 0
         then
             echo "failed to add <mac address='52:54:00:$mac4:$mac5:$mac6'/>" \
                  "to $dir/$file"
             mv -f $dir/$file.orig $dir/$file
         else
             rm -f $dir/$file.orig
         fi
      fi
   done
done
%endif

if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl enable libvirtd.service >/dev/null 2>&1 || :
    /bin/systemctl enable cgconfig.service >/dev/null 2>&1 || :
fi

%endif

%preun
%if %{with_libvirtd}
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable libvirtd.service > /dev/null 2>&1 || :
    /bin/systemctl stop libvirtd.service > /dev/null 2>&1 || :
fi
%endif

%postun
%if %{with_libvirtd}
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart libvirtd.service >/dev/null 2>&1 || :
fi
%endif

%if %{with_libvirtd}
%triggerun -- libvirt < 0.9.9
%{_bindir}/systemd-sysv-convert --save libvirtd >/dev/null 2>&1 ||:

# If the package is allowed to autostart:
/bin/systemctl --no-reload enable libvirtd.service >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del libvirtd >/dev/null 2>&1 || :
/bin/systemctl try-restart libvirtd.service >/dev/null 2>&1 || :
%endif

%post client
/sbin/ldconfig

%postun client -p /sbin/ldconfig

%triggerun client -- libvirt < 0.9.9
%{_bindir}/systemd-sysv-convert --save libvirt-guests >/dev/null 2>&1 ||:

# If the package is allowed to autostart:
/bin/systemctl --no-reload enable libvirt-guests.service >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del libvirt-guests >/dev/null 2>&1 || :
/bin/systemctl try-restart libvirt-guests.service >/dev/null 2>&1 || :

%if %{with_libvirtd}
%files
%defattr(-, root, root)

%doc AUTHORS ChangeLog.gz NEWS README COPYING COPYING.LESSER TODO
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/

%if %{with_network}
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/networks/
%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/qemu/networks/autostart
%endif

%dir %attr(0700, root, root) %{_sysconfdir}/libvirt/nwfilter/
%{_sysconfdir}/libvirt/nwfilter/*.xml

%{_unitdir}/libvirtd.service
%{_unitdir}/virtlockd.service
%{_unitdir}/virtlockd.socket
%config(noreplace) %{_sysconfdir}/sysconfig/libvirtd
%config(noreplace) %{_sysconfdir}/sysconfig/virtlockd
%config(noreplace) %{_sysconfdir}/libvirt/libvirtd.conf
%config(noreplace) %{_sysconfdir}/libvirt/virtlockd.conf
%config(noreplace) %{_prefix}/lib/sysctl.d/libvirtd.conf
%if %{with_dtrace}
%{_datadir}/systemtap/tapset/libvirt_functions.stp
%{_datadir}/systemtap/tapset/libvirt_probes.stp
%{_datadir}/systemtap/tapset/libvirt_qemu_probes.stp
%endif
%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/qemu/
%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/lxc/
%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/uml/

%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd

%if %{with_qemu}
%config(noreplace) %{_sysconfdir}/libvirt/qemu.conf
%config(noreplace) %{_sysconfdir}/libvirt/qemu-lockd.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd.qemu
%endif
%if %{with_lxc}
%config(noreplace) %{_sysconfdir}/libvirt/lxc.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd.lxc
%endif
%if %{with_uml}
%config(noreplace) %{_sysconfdir}/logrotate.d/libvirtd.uml
%endif

%if %{with_network}
%dir %{_datadir}/libvirt/networks/
%{_datadir}/libvirt/networks/default.xml
%endif

%dir %{_localstatedir}/run/libvirt/

%dir %attr(0711, root, root) %{_localstatedir}/lib/libvirt/images/
%dir %attr(0711, root, root) %{_localstatedir}/lib/libvirt/boot/
%dir %attr(0700, root, root) %{_localstatedir}/cache/libvirt/

%if %{with_qemu}
%dir %attr(0700, root, root) %{_localstatedir}/run/libvirt/qemu/
%dir %attr(0750, %{qemu_user}, %{qemu_group}) %{_localstatedir}/lib/libvirt/qemu/
%dir %attr(0750, %{qemu_user}, %{qemu_group}) %{_localstatedir}/cache/libvirt/qemu/
%endif
%if %{with_lxc}
%dir %{_localstatedir}/run/libvirt/lxc/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/lxc/
%endif
%if %{with_uml}
%dir %{_localstatedir}/run/libvirt/uml/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/uml/
%endif
%if %{with_network}
%dir %{_localstatedir}/run/libvirt/network/
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/network/
%dir %attr(0755, root, root) %{_localstatedir}/lib/libvirt/dnsmasq/
%endif

%dir %attr(0755, root, root) %{_libdir}/libvirt/lock-driver
%attr(0755, root, root) %{_libdir}/libvirt/lock-driver/lockd.so

%if %{with_qemu}
%{_datadir}/augeas/lenses/libvirtd_qemu.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd_qemu.aug
%endif

%if %{with_lxc}
%{_datadir}/augeas/lenses/libvirtd_lxc.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd_lxc.aug
%endif

%{_datadir}/augeas/lenses/libvirtd.aug
%{_datadir}/augeas/lenses/tests/test_libvirtd.aug
%{_datadir}/augeas/lenses/virtlockd.aug
%{_datadir}/augeas/lenses/tests/test_virtlockd.aug
%{_datadir}/augeas/lenses/libvirt_lockd.aug
%{_datadir}/augeas/lenses/tests/test_libvirt_lockd.aug

%if %{with_polkit}
%{_datadir}/polkit-1/actions/org.libvirt.api.policy
%{_datadir}/polkit-1/actions/org.libvirt.unix.policy
%endif

%dir %attr(0700, root, root) %{_localstatedir}/log/libvirt/

%if %{with_lxc}
%attr(0755, root, root) %{_libexecdir}/libvirt_lxc
%endif

%attr(0755, root, root) %{_libexecdir}/libvirt_parthelper
%attr(0755, root, root) %{_sbindir}/libvirtd
%attr(0755, root, root) %{_sbindir}/virtlockd

%attr(0755, root, root) %{_libexecdir}/libvirt_iohelper

%{_mandir}/man8/libvirtd.8*
%{_mandir}/man8/virtlockd.8*

%doc docs/*.xml
%endif

%if %{with_sanlock} 
%files lock-sanlock 
%defattr(-, root, root)
%if %{with_qemu}
%config(noreplace) %{_sysconfdir}/libvirt/qemu-sanlock.conf
%endif
%attr(0755, root, root) %{_libdir}/libvirt/lock-driver/sanlock.so
%{_datadir}/augeas/lenses/libvirt_sanlock.aug
%{_datadir}/augeas/lenses/tests/test_libvirt_sanlock.aug
%dir %attr(0700, root, root) %{_localstatedir}/lib/libvirt/sanlock
%{_sbindir}/virt-sanlock-cleanup
%{_mandir}/man8/virt-sanlock-cleanup.8* 
%attr(0755, root, root) %{_libexecdir}/libvirt_sanlock_helper
%endif 

%files client -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS ChangeLog.gz NEWS README COPYING COPYING.LESSER TODO

%config(noreplace) %{_sysconfdir}/libvirt/libvirt.conf
%config(noreplace) %{_sysconfdir}/libvirt/virt-login-shell.conf
%{_mandir}/man1/virsh.1*
%{_mandir}/man1/virt-xml-validate.1*
%{_mandir}/man1/virt-pki-validate.1*
%{_mandir}/man1/virt-host-validate.1*
%{_mandir}/man1/virt-login-shell.1*
%{_bindir}/virsh
%{_bindir}/virt-xml-validate
%{_bindir}/virt-pki-validate
%{_bindir}/virt-host-validate
%attr(4755, root, root) %{_bindir}/virt-login-shell
%{_libdir}/lib*.so.*

%if %{with_driver_modules}
%{_libdir}/%{name}/connection-driver
%endif

%dir %{_datadir}/libvirt/
%dir %{_datadir}/libvirt/schemas/

%{_datadir}/libvirt/schemas/domain.rng
%{_datadir}/libvirt/schemas/domainsnapshot.rng
%{_datadir}/libvirt/schemas/network.rng
%{_datadir}/libvirt/schemas/storagecommon.rng
%{_datadir}/libvirt/schemas/storagepool.rng
%{_datadir}/libvirt/schemas/storagevol.rng
%{_datadir}/libvirt/schemas/nodedev.rng
%{_datadir}/libvirt/schemas/capability.rng
%{_datadir}/libvirt/schemas/interface.rng
%{_datadir}/libvirt/schemas/secret.rng
%{_datadir}/libvirt/schemas/nwfilter.rng
%{_datadir}/libvirt/schemas/basictypes.rng
%{_datadir}/libvirt/schemas/networkcommon.rng
%{_datadir}/libvirt/schemas/domaincommon.rng

%{_datadir}/libvirt/cpu_map.xml
%{_datadir}/libvirt/libvirtLogo.png

%{_unitdir}/libvirt-guests.service
%config(noreplace) %{_sysconfdir}/sysconfig/libvirt-guests
%attr(0755, root, root) %{_libexecdir}/libvirt-guests.sh
%dir %attr(0755, root, root) %{_localstatedir}/lib/libvirt/

%if %{with_sasl}
%config(noreplace) %{_sysconfdir}/sasl2/libvirt.conf
%endif

%files devel
%defattr(-, root, root)

%{_libdir}/lib*.so
%dir %{_includedir}/libvirt
%{_includedir}/libvirt/*.h
%{_libdir}/pkgconfig/libvirt.pc
%dir %{_datadir}/libvirt/api/
%doc %{_datadir}/libvirt/api/*.xml
%dir %{_datadir}/gtk-doc/html/libvirt/
%doc %{_datadir}/gtk-doc/html/libvirt/*.devhelp
%doc %{_datadir}/gtk-doc/html/libvirt/*.html
%doc %{_datadir}/gtk-doc/html/libvirt/*.png
%doc %{_datadir}/gtk-doc/html/libvirt/*.css

%doc docs/*.html docs/html docs/*.gif
%doc docs/libvirt-api.xml
%doc examples/hellolibvirt
%doc examples/object-events
%doc examples/dominfo
%doc examples/domsuspend
%doc examples/openauth
%doc examples/xml
%doc examples/systemtap

%changelog
* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-1m)
- update 1.2.5

* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-1m)
- update 1.2.4

* Tue Apr  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update 1.2.3
- libvirt-python was separated another package

* Thu Nov 14 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-2m)
- remove initscript

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update 1.1.4

* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-1m)
- update 1.1.3

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Thu Aug  1 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Sun Jun  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-1m)
- [SECURITY] CVE-2013-1962
- update 1.0.6

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-2m)
- rebuild against gnutls-3.2.0

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update 1.0.5

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- update 1.0.4

* Sun Mar 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Sat Feb 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- [SECURITY] CVE-2013-0170
- update 1.0.2

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update 1.0.0

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.2-1m)
- update 0.10.2

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.13-3m)
- [SECURITY] CVE-2012-3445

* Fri Aug  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.13-2m)
- reubild against systemd-187

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.13-1m)
- update 0.9.13

* Sat Jun 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.12-1m)
- [SECURITY] CVE-2012-2693
- update 0.9.12

* Wed May  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.11.3-1m)
- update 0.9.11.3 stable 
-- support guest suspend 
-- support openvswitch

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.10-1m)
- update 0.9.10

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-1m)
- update 0.9.9
- support systemd

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-1m)
- update 0.9.8

* Tue Dec  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-2m)
- verbose make check message

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-1m)
- update 0.9.7

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-5m)
- remove BR hal-devel

* Thu Sep  8 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.4-4m)
- rebuild against parted-3.0

* Fri Aug 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-3m)
- add BuildRequires

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- modify %%files

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- update 0.9.4

* Mon Jun 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.8-3m)
- cleanup spec

* Sun Jun 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-2m)
- move startup scripts to correct space (to enable build)

* Sun Jun 12 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.8-1m)
- sync Fedora 15

* Thu May  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-4m)
- rebuild against python-2.7.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.6-3m)
- rebuild for parted-2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-2m)
- rebuild for new GCC 4.6

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.6-1m)
- update 0.8.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.5-1m)
- update 0.8.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.2-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.2-2m)
- [SECURITY] CVE-2010-2237 CVE-2010-2238 CVE-2010-2239 CVE-2010-2242
- import Patch1-12 from Fedora 13 (0.8.2-1)

* Mon Jul  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update to 0.8.2

* Sun Jun 13 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.7-3m)
- rebuild against readline6

* Wed Mar 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.7-2m)
- remove %%dir %%{_datadir}/libvirt from libvirt
- it's provided by libvirt-client and libvirt Requires: libvirt-client
- this change is completely same as 0.7.5-5m and 0.7.5-2m

* Sun Mar 21 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.7-1m)
- update to 0.7.7

* Tue Mar 16 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.5-6m)
- rebuild against parted-2.1

* Mon Feb  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-5m)
- remove %%dir %%{_datadir}/libvirt from libvirt
- it's provided by libvirt-client and libvirt Requires: libvirt-client

* Sun Jan 31 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.5-4m)
- enable yajl

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-3m)
- change BuildRequires: /usr/bin/qemu-img -> qemu-img
- change Requires: and BuildRequires: /usr/sbin/qcow-create -> xen-runtime

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-2m)
- move startup script from %%{_initrddir} to %%{_initscriptdir} again
- stop daemon at initial startup again
- change Requires from /usr/bin/qemu-img to qemu-img
- modify %%files to avoid conflicting

* Sun Jan 31 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5
- sync with Fedora devel

* Sat Jan 30 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.2-7m)
- change src url

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-4m)
-- sync with Fedora 11
-- 
-- * Wed Aug 19 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-17
-- - Fix migration completion with newer versions of qemu (#516187)
-- - Fix dumpxml segfault with newer versions of Xen (#518091)
-- 
-- * Wed Aug 19 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-16.fc11
-- - Allow PCI bus reset to reset other devices (#499678)
-- - Fix stupid PCI reset error message (bug #499678)
-- - Allow PM reset on multi-function PCI devices (bug #515689)
-- - Re-attach PCI host devices after guest shuts down (bug #499561)
-- - Fixes list corruption after disk hot-unplug
-- - Fix minor 'virsh nodedev-list --tree' annoyance
-- 
-- * Thu Aug 13 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.2-15.fc11
-- - Log and ignore NUMA topology problems (rhbz #506590)
-- 
-- * Wed Aug  5 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.2-14.fc11
-- - Fix crash when attaching/detaching non-existant PCI device (rhbz #510907)
-- - Fix QEMU guest name/uuid uniqueness checks (rhbz #507405)
-- - Fix to use correct pci_add/del syntax for QEMU (rhbz #499669)
-- - Relabel disks before hotplugging them to guest (rhbz #496442)
-- - Correctly handle 8-bit high bytes when escaping XML (rhbz #479517)
-- 
-- * Fri Jul  3 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-13.fc11
-- - Fix libvirtd crash with bad capabilities data (bug #505635)
-- - Don't unnecessarily try to change a file context (bug #507555)

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.2-3m)
- stop daemon

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-2m)
-- sync with Fedora 11 (0.6.2-12.fc11)
-- 
-- * Fri Jun  5 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-12.fc11
-- - Use the correct QEMU machine type for ppc (bug #502862)
-- - Fix crash with TLS connections (bug #503066)
-- - Fix broken networking with newer qemu releases (bug #503275)
-- - Remove the qemu BuildRequires
-- 
-- * Mon May 25 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-11.fc11
-- - Bring up the bridge, even if it doesn't have an IP address (bug #501912)
-- 
-- * Fri May 22 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-10.fc11
-- - Don't log monitor output to domain log file (bug #499584)
-- 
-- * Thu May 21 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-9.fc11
-- - Fix qemu argv detection with latest qemu (bug #501923)
-- - Fix XML attribute escaping (bug #499791)
-- - Fix serious event handling issues causing guests to be destroyed (bug #499698)

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-1m)
- re-import from Fedora 11 (0.6.2-8.fc11)
- %%define __libtoolize :
- use %%{_initscriptdir}
-- 
-- * Sun May 10 2009 Cole Robinson <crobinso@redhat.com> - 0.6.2-8.fc11
-- - Don't try to label a disk with no path (e.g. empty cdrom) (bug #499569)
-- 
-- * Thu May  7 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-7.fc11
-- - Enable migration for qemu 0.10 (bug #499704)
-- 
-- * Wed May  6 2009 Cole Robinson <crobinso@redhat.com> - 0.6.2-6.fc11
-- - Refresh qemu caps when getCapabilities is called (bug #460649)
-- 
-- * Wed May  6 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-5.fc11
-- - Fix handling of <hostdev managed='yes'> (bug #499386)
-- 
-- * Tue May  5 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.2-4.fc11
-- - Fix labelling of shared/readonly disks (rhbz #493692)
-- 
-- * Tue Apr 28 2009 Daniel Veillard <veillard@redhat.com> - 0.6.2-3.fc11
-- - Fix missing directories in spec (#496945 and gtk-doc)
-- 
-- * Thu Apr 16 2009 Mark McLoughlin <markmc@redhat.com> - 0.6.2-2.fc11
-- - Fix qemu drive format specification (#496092)
-- 
-- * Fri Apr  3 2009 Daniel Veillard <veillard@redhat.com> - 0.6.2-1.fc11
-- - release of 0.6.2
-- - memory ballooning in QEMU
-- - SCSI HBA storage pool support
-- - support SASL auth for VNC server
-- - PCI passthrough in Xen driver
-- - assorted bug fixes
-- 
-- * Fri Apr  3 2009 Daniel P. Berrange  <berrange@redhat.com> - 0.6.1-6.fc11
-- - Fix typo in previous patch
-- 
-- * Tue Mar 17 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.1-5.fc11
-- - Don't relabel shared/readonly disks
-- - Disable sound cards when running sVirt
-- 
-- * Tue Mar 17 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.1-4.fc11
-- - Fix memory allocation for xend lookup
-- - Avoid crash if storage volume deletion fails
-- - Fix multiple FD leaks
-- - Fix bug in dispatch FD events when a callback is marked deleted
-- - Fix parsing of storage volume owner/group/mode
-- - Fix memory allocation for virDomainGetVcpus RPC handler
-- - Avoid deadlock in setting vCPU count
-- - Use correct driver name in Xen block detach
-- 
-- * Mon Mar  9 2009 Cole Robinson <crobinso@redhat.com> - 0.6.1-3.fc11
-- - Add Requires: libselinux
-- 
-- * Fri Mar  6 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.1-2.fc11
-- - Fix crash after storage vol deletion fails
-- - Add patch to enable VNC SASL authentication
-- 
-- * Wed Mar  4 2009 Daniel Veillard <veillard@redhat.com> - 0.6.1-1.fc11
-- - upstream release 0.6.1
-- - support for node device detach reattach and reset
-- - sVirt mandatory access control support
-- - many bug fixes and small improvements
-- 
-- * Mon Mar  2 2009 Daniel Veillard <veillard@redhat.com> - 0.6.0-6.fc11
-- - make sure Xen is handled in i586 new default 32bits x86 packages
-- 
-- * Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-5.fc11
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Wed Feb 18 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.0-4.fc11
-- - Fix QEMU startup timeout/race (rhbz #484649)
-- - Setup DBus threading. Don't allow dbus to call _exit / change SIGPIPE (rhbz #484553)
-- - Fix timeout when autostarting session daemon
-- 
-- * Wed Feb 11 2009 Richard W.M. Jones <rjones@redhat.com> - 0.6.0-3.fc11
-- - Multiple fixes to remove rpmlint warnings/errors (rhbz #226055)
-- 
-- * Fri Feb  6 2009 Daniel P. Berrange <berrange@redhat.com> - 0.6.0-2.fc11
-- - Fix libvirtd --timeout usage
-- - Fix RPC call problems and QEMU startup handling (rhbz #484414)
-- - Fix unowned directories (rhbz #483442)
-- 
-- * Sat Jan 31 2009 Daniel Veillard <veillard@redhat.com> - 0.6.0-1.fc11
-- - upstream release 0.6.0
-- - thread safety of API
-- - allow QEmu/KVM domains to survive daemon restart
-- - extended logging capabilities
-- - support copy on write storage volumes for QEmu/KVM
-- - support of storage cache control options for QEmu/KVM
-- - a lot of bug fixes
-- 
-- * Wed Dec 17 2008 Daniel Veillard <veillard@redhat.com> - 0.5.1-2.fc11
-- - fix missing read-only access checks, fixes CVE-2008-5086
-- 
-- * Fri Dec  5 2008 Daniel Veillard <veillard@redhat.com> - 0.5.1-1.fc11
-- - upstream release 0.5.1
-- - mostly bugfixes e.g #473071
-- - some driver improvments
-- 
-- * Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.5.0-2
-- - Rebuild for Python 2.6
-- 
-- * Wed Nov 26 2008 Daniel Veillard <veillard@redhat.com> - 0.5.0-1.fc11
-- - upstream release 0.5.0
-- - domain lifecycle event support
-- - node device enumeration
-- - KVM/QEmu migration support
-- - improved LXC support
-- - SDL display configuration
-- - User Mode Linux driver (Daniel Berrange)
-- 
-- * Wed Sep 24 2008 Daniel Veillard <veillard@redhat.com> - 0.4.6-3.fc10
-- - apply the python makefile patch for #463733
-- 
-- * Wed Sep 24 2008 Daniel Veillard <veillard@redhat.com> - 0.4.6-2.fc10
-- - upstream release 0.4.6
-- - fixes some problems with 0.4.5
-- 
-- * Tue Sep  9 2008 Daniel Veillard <veillard@redhat.com> - 0.4.5-2.fc10
-- - fix a crash if a QEmu/KVM domain is defined without an emulator path
-- 
-- * Mon Sep  8 2008 Daniel Veillard <veillard@redhat.com> - 0.4.5-1.fc10
-- - upstream release 0.4.5
-- - a lot of bug fixes
-- - major updates to QEmu/KVM and Linux containers drivers
-- - support for OpenVZ if installed
-- 
-- * Thu Aug  7 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.4.4-3.fc10
-- - fix license tag
-- 
-- * Tue Jul  8 2008 Daniel P. Berrange <berrange@redhat.com> - 0.4.4-2.fc10
-- - Fix booting of CDROM images with KVM (rhbz #452355)
-- 
-- * Wed Jun 25 2008 Daniel Veillard <veillard@redhat.com> - 0.4.4-1.fc10
-- - upstream release 0.4.4
-- - fix a few bugs in previous release

* Sun Mar 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-6m)
- [SECURITY] CVE-2009-0036
- import upstream changes

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-5m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-4m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-3m)
- [SECURITY] CVE-2008-5086
- import a security patch (Patch100) from Ubuntu 8.10 (0.4.4-3ubuntu3.1)
- License: LGPLv2+

* Fri Jan  2 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.4-2m)
- rebuild against python-2.6.1-1m

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4
- rebuild against gnutls-2.4.1

* Mon Apr 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-5m)
- add BPR PolicyKit

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-4m)
- rebuild against gcc43

* Sun Feb 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-3m)
- openvz support was still buggy...

* Thu Jan 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-2m)
- import Fedora Patches

* Thu Dec 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Mon Oct 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3

* Sun Sep  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2
- support OpenVZ.
-- but dangerous...

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1
- import patch1 from FC-devel
- add patch2
- executable file name was changed (libvirt_qemud -> libvirtd)

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.3-3m)
- add Requires: chkconfig

* Wed Jul  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-2m)
- do not start libvirtd daemon again

* Tue Jul  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.3-1m)
- update to 0.2.3
- delete patches

* Wed Jun 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-2m)
- do not start libvirtd daemon

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2 (sync with FC-devel)
- old source and patch were removed
- new patches have imported from FC

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.10-3m)
- delete pyc pyo

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.1.10-2m)
- add BuildRequires: xen-devel

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.10-1m)
- update 0.1.10

* Mon Nov 06 2006 TABUCHI Takaaki <tab@momonga-linux.org> 
- (0.1.8-1m)
- re-import from FC
- delete no need Patch0: chown.patch
-
-* Mon Oct 16 2006 Daniel Veillard <veillard@redhat.com> 0.1.8-1
-- fix missing page size detection code for ia64
-- fix mlock size when getting domain info list from hypervisor
-- vcpu number initialization
-- don't label crashed domains as shut off
-- fix virsh man page
-- blktapdd support for alternate drivers like blktap
-- memory leak fixes (xend interface and XML parsing)
-- compile fix
-- mlock/munlock size fixes
-
-* Fri Sep 22 2006 Daniel Veillard <veillard@redhat.com> 0.1.7-1
-- Fix bug when running against xen-3.0.3 hypercalls
-- Fix memory bug when getting vcpus info from xend
-
-* Fri Sep 22 2006 Daniel Veillard <veillard@redhat.com> 0.1.6-1
-- Support for localization
-- Support for new Xen-3.0.3 cdrom and disk configuration
-- Support for setting VNC port
-- Fix bug when running against xen-3.0.2 hypercalls
-- Fix reconnection problem when talking directly to http xend
-
-* Tue Sep  5 2006 Jeremy Katz <katzj@redhat.com> - 0.1.5-3
-- patch from danpb to support new-format cd devices for HVM guests
-
-* Tue Sep  5 2006 Daniel Veillard <veillard@redhat.com> 0.1.5-2
-- reactivating ia64 support
-
-* Tue Sep  5 2006 Daniel Veillard <veillard@redhat.com> 0.1.5-1
-- new release
-- bug fixes
-- support for new hypervisor calls
-- early code for config files and defined domains
-
-* Mon Sep  4 2006 Daniel Berrange <berrange@redhat.com> - 0.1.4-5
-- add patch to address dom0_ops API breakage in Xen 3.0.3 tree
-
-* Mon Aug 28 2006 Jeremy Katz <katzj@redhat.com> - 0.1.4-4
-- add patch to support paravirt framebuffer in Xen 
-
-* Mon Aug 21 2006 Daniel Veillard <veillard@redhat.com> 0.1.4-3
-- another patch to fix network handling in non-HVM guests
-
-* Thu Aug 17 2006 Daniel Veillard <veillard@redhat.com> 0.1.4-2
-- patch to fix virParseUUID()
-
-* Wed Aug 16 2006 Daniel Veillard <veillard@redhat.com> 0.1.4-1
-- vCPUs and affinity support
-- more complete XML, console and boot options
-- specific features support
-- enforced read-only connections
-- various improvements, bug fixes
-
-* Wed Aug  2 2006 Jeremy Katz <katzj@redhat.com> - 0.1.3-6
-- add patch from pvetere to allow getting uuid from libvirt
-
-* Wed Aug  2 2006 Jeremy Katz <katzj@redhat.com> - 0.1.3-5
-- build on ia64 now
-
-* Thu Jul 27 2006 Jeremy Katz <katzj@redhat.com> - 0.1.3-4
-- don't BR xen, we just need xen-devel
-
-* Thu Jul 27 2006 Daniel Veillard <veillard@redhat.com> 0.1.3-3
-- need rebuild since libxenstore is now versionned
-
-* Mon Jul 24 2006 Mark McLoughlin <markmc@redhat.com> - 0.1.3-2
-- Add BuildRequires: xen-devel
-
-* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.1.3-1.1
-- rebuild
-
-* Tue Jul 11 2006 Daniel Veillard <veillard@redhat.com> 0.1.3-1
-- support for HVM Xen guests
-- various bugfixes

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org> 
- (0.1.2-1m)
- import from fc-devel

* Mon Jul  3 2006 Daniel Veillard <veillard@redhat.com> 0.1.2-1
- added a proxy mechanism for read only access using httpu
- fixed header includes paths

* Wed Jun 21 2006 Daniel Veillard <veillard@redhat.com> 0.1.1-1
- extend and cleanup the driver infrastructure and code
- python examples
- extend uuid support
- bug fixes, buffer handling cleanups
- support for new Xen hypervisor API
- test driver for unit testing
- virsh --conect argument

* Mon Apr 10 2006 Daniel Veillard <veillard@redhat.com> 0.1.0-1
- various fixes
- new APIs: for Node information and Reboot
- virsh improvements and extensions
- documentation updates and man page
- enhancement and fixes of the XML description format

* Tue Feb 28 2006 Daniel Veillard <veillard@redhat.com> 0.0.6-1
- added error handling APIs
- small bug fixes
- improve python bindings
- augment documentation and regression tests

* Thu Feb 23 2006 Daniel Veillard <veillard@redhat.com> 0.0.5-1
- new domain creation API
- new UUID based APIs
- more tests, documentation, devhelp
- bug fixes

* Fri Feb 10 2006 Daniel Veillard <veillard@redhat.com> 0.0.4-1
- fixes some problems in 0.0.3 due to the change of names

* Wed Feb  8 2006 Daniel Veillard <veillard@redhat.com> 0.0.3-1
- changed library name to libvirt from libvir, complete and test the python 
  bindings

* Sun Jan 29 2006 Daniel Veillard <veillard@redhat.com> 0.0.2-1
- upstream release of 0.0.2, use xend, save and restore added, python bindings
  fixed

* Wed Nov  2 2005 Daniel Veillard <veillard@redhat.com> 0.0.1-1
- created
