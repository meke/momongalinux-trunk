%global momorel 20
%global buildgui 0

Summary: jigdo - jigsaw download
Name: jigdo
Version: 0.7.3
Release: %{momorel}m%{?dist}
License: "GPLv2 with exceptions"
Group: Applications/System
URL: http://atterer.org/jigdo/
Source0: http://atterer.net/jigdo/%{name}-%{version}.tar.bz2
Source1: jd4ml
Patch0: jigdo-0.7.3.diff
Patch1: jigdo-0.7.3-gcc43.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: bash, libstdc++, zlib
BuildRequires: libstdc++-devel, zlib-devel
BuildRequires: libdb-devel >= 5.3.15

%if %{buildgui}
%package gtk
Summary: jigdo - jigsaw download for gtk
Group: Applications/System
Requires: jigdo, atk, bash, db4, fontconfig, freetype
Requires: glib, gtk2, libstdc++, pango, w3c-libwww, zlib
Requires: ruby
BuildRequires: atk-devel, fontconfig-devel
BuildRequires: freetype-devel, glib-devel, gtk2-devel
BuildRequires: pango-devel, w3c-libwww-devel
# man be need xorg library 
%endif

%description
Jigsaw Download, or short jigdo, is a tool designed to ease the distribution
of very large files over the internet, for example CD or DVD images.

Its aim is to make downloading the images as easy for users as a click on
a direct download link in a browser, while avoiding all the problems that
server administrators have with hosting such large files.

%if %{buildgui}
%description gtk
jigdo-gtk package provides the GTK+ interface for the jigsaw download.
%endif

%prep
%setup -q

%patch0 -p1
%patch1 -p1 -b .gcc43~

%build
%if %{buildgui}
%configure
%else
%configure --without-gui
%endif
%make

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall
rm -f $RPM_BUILD_ROOT/%{_datadir}/jigdo/debian-mirrors.jigdo
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/jd4ml

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README THANKS doc/*.txt doc/*.html
%{_bindir}/jigdo-file
%{_bindir}/jigdo-lite
%{_bindir}/jigdo-mirror
%{_bindir}/jd4ml
%{_mandir}/man1/jigdo-mirror.1*
%{_mandir}/man1/jigdo-file.1*
%{_mandir}/man1/jigdo-lite.1*
%{_datadir}/locale/de/LC_MESSAGES/jigdo.mo

%if %{buildgui}
%files gtk
%defattr(-,root,root,-)
%{_bindir}/jigdo
%{_mandir}/man1/jigdo.1*
%{_datadir}/jigdo/COPYING
%{_datadir}/jigdo/pixmaps
%endif

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-20m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-19m)
- rebuild against libdb-5.3.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.3-18m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-17m)
- rebuild for new GCC 4.6

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-16m)
- no NoSource, official web page has gone

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-14m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.3-13m)
- use BuildRequires

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-12m)
- rebuild against db-4.8.26

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-10m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-9m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: "GPLv2 with exceptions"

* Mon Oct 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-8m)
- rebuild against db4-4.7.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.3-7m)
- rebuild against gcc43

* Mon Feb 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-6m)
- add gcc43 patch

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-5m)
- %%NoSource -> NoSource

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.3-4m)
- rebuild against db4-4.6.21

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-3m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.3-2m)
- rebuild against db4-4.5.20

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.7.3-1m)
- update to 0.7.3

* Wed Jan 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.2-2m)
- rebuild against db4.3

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-1m)
- update 0.7.2
- add gcc4 patch. import SUSE patch
- Patch0: jigdo-0.7.2.diff

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-2m)
- add gcc4 patch. import from Gentoo.
- Patch0: jigdo-0.7.1-gcc4.patch

* Tue Aug 10 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.7.1-1m)
  Initial build.

