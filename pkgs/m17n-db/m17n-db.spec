%global momorel 1

Summary: The m17n database
Name: m17n-db
Version: 1.6.4
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.nongnu.org/m17n/
Source0: http://ftp.twaren.net/Unix/NonGNU//m17n/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext
BuildRequires: pkgconfig

%description
The m17n database; a sub-part of the m17n library.

%package devel
Summary: Headers of m17n for development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Headers of m17n-db for development.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/m17n

%files devel
%defattr(-,root,root)
%{_datadir}/pkgconfig/%{name}.pc

%changelog
* Tue Jul 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.4-1m)
- version 1.6.4

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- update 1.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-2m)
- full rebuild for mo7 release

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-1m)
- version 1.6.1

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-1m)
- version 1.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.5-1m)
- version 1.5.5

* Wed Jun 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-2m)
- No NoSource

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1-2m)
- rebuild against gcc43

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- version 1.5.1

* Sat Dec 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- version 1.5.0

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- version 1.4.0

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-1m)
- import from cooker, we don't need Fedora style

* Tue Jan 09 2007 Thierry Vignaud <tvignaud@mandriva.com> 1.3.4-1mdv2007.0
+ Revision: 106763
- add buildrequires on gettext-devel for `AM_GNU_GETTEXT
- new release
- latest snapshot (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
- Import m17n-db

* Thu Aug 03 2006 Thierry Vignaud <tvignaud@mandriva.com> 1.3.3-3.20060803.1mdv2007.0
- fix build
- latest snapshot (UTUMI Hirosi <utuhiro78@yahoo.co.jp>): better Thai support
  (http://sourceforge.net/mailarchive/forum.php?thread_id=26116958&forum_id=43684)

* Sun Jun 25 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.3-2.20060625.1mdv2007.0
- latest snapshot

* Sat Feb 25 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.3-1mdk
- new release

* Thu Jan 19 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.1-1mdk
- new release

* Fri Dec 23 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.0-1mdk
- new release

* Wed Nov 16 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-4.20051116.1mdk
- latest snapshot
- remove patches for vi-vni.mim (merged upstream)

* Tue Sep 13 2005 Thierry Vignaud <tvignaud@mandriva.com> 1.2.0-3.20050809.2mdk
- source 1, patch 0: add new vi-vni.mim input method for vietnamese (Yukiko
  Bando)

* Tue Aug 09 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-3.20050809.1mdk
- latest snapshot

* Mon Apr 25 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-2.20050425.1mdk
- latest snapshot
- spec cleanup
- remove find_lang (it doesn't have po files)

* Tue Dec 28 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-1mdk
- new release

* Fri Nov 26 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.1.0-4.cvs20041126.1mdk
- latest snapshot

* Tue Aug 17 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.1.0-1mdk
- new release
- only run bootstrap if needed

* Wed Jul 14 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.0.2-2.cvs20040714.1mdk
- cvs 20040714

* Thu May 27 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.2-1mdk
- initial package (based on UTUMI Hirosi <utuhiro78@yahoo.co.jp> 's work)
