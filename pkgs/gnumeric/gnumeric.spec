%global momorel 2
Name:             gnumeric
Version:          1.12.17
Release: %{momorel}m%{?dist}
Summary:          Spreadsheet program for GNOME
#LGPLv2+:
#plugins/gda/plugin-gda.c
#plugins/fn-financial/sc-fin.c
#plugins/plan-perfect/charset.c
#src/widgets/gnumeric-lazy-list.h
#GPLv3+:
#src/parser.c
License:          GPLv2+ and GPLv3+ and LGPLv2+
Group: 		  User Interface/Desktops
URL:              http://projects.gnome.org/gnumeric/
Source:           ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.12/%{name}-%{version}.tar.xz
NoSource: 0
BuildRequires:    desktop-file-utils
BuildRequires:    goffice-devel >= 0.10.16
BuildRequires:    intltool
BuildRequires:    perl-devel >= 5.16.1
BuildRequires:    perl-ExtUtils-Embed
BuildRequires:    pygobject3-devel
BuildRequires:    pygtk2-devel
BuildRequires:    rarian-compat
BuildRequires:    zlib-devel
Requires:         hicolor-icon-theme
Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description
Gnumeric is a spreadsheet program for the GNOME GUI desktop
environment.


%package devel
Summary:          Files necessary to develop gnumeric-based applications
Requires:         %{name}%{?_isa} = %{version}-%{release}

%description devel
Gnumeric is a spreadsheet program for the GNOME GUI desktop
environment. The gnumeric-devel package includes files necessary to
develop gnumeric-based applications.


%package plugins-extras
Summary:          Files necessary to develop gnumeric-based applications
Requires:         %{name}%{?_isa} = %{version}-%{release}
Requires:         perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description plugins-extras
This package contains the following additional plugins for gnumeric:
* gda and gnomedb plugins:
  Database functions for retrieval of data from a database.
* perl plugin:
  This plugin allows writing of plugins in perl


%prep
%setup -q

chmod -x plugins/excel/rc4.?


%build
%configure --enable-ssindex
# Don't use rpath!
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name} --all-name --with-gnome

mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= --delete-original                  \
  --dir %{buildroot}%{_datadir}/applications                         \
  --add-category Office                                                 \
  --add-category Spreadsheet                                            \
  --remove-category Science                                             \
  %{buildroot}%{_datadir}/applications/*.desktop

#remove unused mime type icons
rm %{buildroot}/%{_datadir}/pixmaps/gnome-application-*.png
rm %{buildroot}/%{_datadir}/pixmaps/%{name}/gnome-application-*.png

#remove spurious .ico thing
rm %{buildroot}/usr/share/pixmaps/win32-%{name}.ico
rm %{buildroot}/usr/share/pixmaps/%{name}/win32-%{name}.ico

#remove .la files
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%post
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
     glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
/usr/bin/update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%doc HACKING AUTHORS ChangeLog NEWS BUGS README COPYING
%{_bindir}/*
%{_libdir}/libspreadsheet-%{version}.so
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/%{version}
%exclude %{_libdir}/%{name}/%{version}/plugins/perl-*
#%exclude %{_libdir}/%{name}/%{version}/plugins/gdaif
#%exclude %{_libdir}/%{name}/%{version}/plugins/gnome-db
%{_datadir}/glib-2.0/schemas/org.gnome.gnumeric.*
%{_datadir}/pixmaps/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.png
#%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/%{version}
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
# The actual omf file is in gnumeric.lang, but find-lang doesn't own the dir!
%dir %{_datadir}/omf/%{name}
%{_datadir}//locale/*/LC_MESSAGES/*.mo
%{_mandir}/man1/*

%files devel
%{_libdir}/libspreadsheet.so
%{_libdir}/pkgconfig/libspreadsheet-1.12.pc
%{_includedir}/libspreadsheet-1.12

%files plugins-extras
%{_libdir}/%{name}/%{version}/plugins/perl-*
%{_libdir}/goffice/*/plugins/gnumeric/gnumeric.so
%{_libdir}/goffice/*/plugins/gnumeric/plugin.xml
#%{_libdir}/%{name}/%{version}/plugins/gdaif
#%{_libdir}/%{name}/%{version}/plugins/gnome-db


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.17-2m)
- rebuild against perl-5.20.0

* Wed Jun 25 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.12.17-1m)
- update to 1.12.17

* Sat May 31 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.12.6-1m)
- update to 1.12.6

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.1-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.1-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.1-2m)
- rebuild against perl-5.18.0

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.1-1m)
- update to 1.12.1
- remove fedora from vendor (desktop-file-install)
- rebuild against goffice-0.10.1

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.90-2m)
- rebuild against perl-5.16.3

* Tue Dec 11 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.11.90-1m)
- update to 1.11.90
- requires goffice 0.9.90

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.6-2m)
- rebuild against perl-5.16.2

* Mon Oct  8 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.11.6-1m)
- update to 1.11.6

* Sat Aug 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.11.5-2m)
- rebuild against perl-5.16.1

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.5-1m)
- reimport from fedora

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.6-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.6-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.6-3m)
- build fix

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.6-2m)
- fix gnumeric.desktop

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.6-1m)
- update to 1.10.6

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.5-1m)
- update to 1.10.5

* Mon May 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.4-4m)
- rebuild against goffice-0.6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.2-6m)
- libtool build fix

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.2-3m)
- rebuild against python-2.6.1-2m

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.8.2-3m)
- change %%preun script

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.2-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Tue Jan  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0
-- add gnumeric-C.omf (missing in tar ball) 

* Wed Sep  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.12-1m)
- update to 1.7.12 (unstable)

* Tue Aug 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.11-1m)
- update to 1.7.11 (unstable)
- add devel package

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6.3-4m)
- enable ppc64, alpha

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.3-3m)
- delete libtool library

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.6.3-2m)
- rebuild against libgsf-1.14.0

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Tue Nov 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Tue May 17 2005  Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3
- import some patchs from FC
- revise setlocale.patch

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.13-4m)
- enable ppc.

* Thu Jan 27 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.13-3m)
- rebuild agianst libgnomedb-1.2.0

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.13-2m)
- build against perl-5.8.5

* Thu Jun 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.13-1m)
- update to 1.2.13

* Wed Jun 23 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.12-1m)
- remove BuildPreReq: gal-devel (seems to be unnecessary)

* Tue May 11 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.12-1m)
- updated to 1.2.12

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.9-1m)
- update to 1.2.9

* Tue Apr  6 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.4-3m)
- BuildPreReq: pygtk -> pygtk-devel

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.4-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 19 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.4-1m)
- update to 1.2.4
- rebuild against gal-2.1.3

* Mon Dec 22 2003 Kenta MURATA <muraken2@nifty.com>
- (1.2.2-1m)
- version 1.2.2.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-1m)
- version 1.2.1

* Sat Sep 20 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.2.0-2m)
- build require libgsf-devel >= 1.8.2

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.1.90-1m)
- version 1.1.90

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.20-1m)
- version 1.1.20

* Sun Aug 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.19-4m)
- add setlocale.patch

* Sun Aug  3 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.19-3m)
- gal

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.19-2m)
- libtool

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.19-1m)
- version 1.1.19

* Tue May 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.17-3m)
- rebuild against for gal

* Fri May 23 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.17-2m)
  build require libgsf-devel >= 1.8.0

* Wed May 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.17-1m)
- version 1.1.17

* Tue Apr 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.16-6m)
- rebuild against for gal-1.99.3

* Thu Apr  3 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (1.1.16-5m)
- rebuild against for gal-1.99.2

* Wed Mar 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.16-4m)
- rebuild against for XFree86-4.3.0

* Wed Mar 12 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (1.1.16-3m)
- rebuild against for libgda-0.11.0

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.16-2m)
  rebuild against openssl 0.9.7a

* Wed Jan 29 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.16-1m)
- version 1.1.16

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.15-1m)
- version 1.1.15

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.14-1m)
- version 1.1.14

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.12-2m)
- add BuildPrereq: libgsf >= 1.5.0 for /usr/lib/libgsf-1.so
  
* Wed Nov 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.12-1m)
- version 1.1.12

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.11-1m)
- version 1.1.11

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.10-2m)
- use gcc instead of gcc_2_95_3

* Mon Oct 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.10-1m)
- version 1.1.10

* Thu Oct 24 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.1.9-3m)
- BuildPreReq gal2-devel >= 0.0.6

* Mon Oct 21 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.9-2m)
- BuildPreReq libgsf-devel >= 1.4.0

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.9-1m)
- version 1.1.9

* Sat Oct  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.8-2m)
- add gnumeric-1.1.8-libgsf14.patch

* Mon Aug 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.8-1m)
- version 1.1.8

* Fri Aug 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-4m)
- update im patch

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-3m)
- update im patch

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-2m)
- 1.1.8 based

* Tue Aug 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-1m)
- version 1.1.7

* Sun Aug 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-14m)
- update im patch

* Sat Aug 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-13m)
- update pango patch
- update print patch

* Sat Aug 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-12m)
- update print patch

* Thu Aug  8 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-11m)
- add print patch

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-10m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Mon Aug  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-9m)
- update im patch

* Fri Aug  2 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-8m)
- update pango patch
- add im patch

* Thu Aug  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-7m)
- update pango patch

* Thu Aug  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-6m)
- excel file read patch

* Thu Aug  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-5m)
- add pango patch

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-4m)
- rebuild against for libgnomeprint-1.116.0
- rebuild against for libgnomeprintui-1.116.0

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-3m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-2m)
- add omf patch

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-1m)
- version 1.1.6

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.5-33m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-32k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-30k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-28k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-26k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-24k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-22k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-20k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-18k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-16k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-14k)
- rebuild against for libgnomeprint-1.115.0
- rebuild against for libgnomeprintui-1.115.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-12k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-10k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-8k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-6k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Thu May 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-2k)
- version 1.1.5

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-14k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-12k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-10k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-8k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-6k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Tue May 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-4k)
- rebuild against for gnome-terminal-1.9.5
- rebuild against for libgda-0.8.190
- rebuild against for libgnomedb-0.8.190
- rebuild against for gnome-db-0.8.190
- rebuild against for libgtkhtml-1.99.7
- rebuild against for yelp-0.7
- rebuild against for metatheme-0.9.7

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-2k)
- version 1.1.3

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-12k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-2k)
- version 1.1.2

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-10k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-8k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Thu Mar 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-6k)
- rebuild against for eog-0.114.0
- rebuild against for gnome-db-0.8.104
- rebuild against for libgda-0.8.104
- rebuild against for libgnomedb-0.8.104

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-4k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.1-2k)
- version 1.1.1

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-24k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-22k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-20k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- change depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-16k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-14k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-12k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-10k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-8k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-6k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-4k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.0-2k)
- version 1.1.0
- gnome2 env

* Tue Feb  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.4-2k)
- version 1.0.4

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.3-2k)
- version 1.0.3

* Wed Jan 09 2002 Motonobu Ichimura <famao@kondara.org>
- (1.0.1-2k)
- up to 1.0.1

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (0.99.0-2k)
- version 0.99.0

* Thu Dec  6 2001 Shingo Akagaki <dora@kondara.org>
- (0.76-4k)
- add http://www.gnome.gr.jp/ml/gnome-devel/200111/msg00013.html patch

* Thu Nov 22 2001 Shingo Akagaki <dora@kondara.org>
- (0.76-2k)
- version 0.76

* Wed Nov 14 2001 Motonobu Ichimura <famao@kondara.org>
- (0.75-8k)
- move zh_CN.GB2312 to zh_CN

* Tue Nov  6 2001 Shingo Akagaki <dora@kondara.org>
- (0.75-2k)
- version 0.75

* Thu Oct 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.72-2k)
- version 0.72

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.69-10k)
- rebuild against gettext 0.10.40.

* Sat Sep  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.69-8k)
- modify required libxml-devel version to over 1.8.14

* Tue Aug 28 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.69-6k)
- fix install path
- rebuild against for gnome-print-0.29

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- version 0.69

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- no more ifarch alpha.

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gal-0.8

* Sat May  5 2001 Toru Hoshina <toru@df-usa.com>
- muki- ppc me-.

* Tue May 2 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gnome-print-0.25

* Tue May  1 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- bug fix l10n patch

* Tue Apr 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- l10n

* Wed Apr 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gal-0.7

* Wed Mar 21 2001 Shingo Akagaki <dora@kondara.org>
- version 0.64

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.62
- K2K

* Wed Jan 17 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (0.61-3k)
- update to 0.61.
- use makeinstall macro.
- build with gb.

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.59a-7k)
- modified spec file with macros about docdir

* Wed Nov 22 2000 Toru Hoshina <toru@df-usa.com>
- revised BuildRequires tag.

* Tue Nov 14 2000 Shingo Akagaki <dora@kondara.org>
- version 0.58

* Mon Oct 16 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against new environment.

* Fri Oct 13 2000 Shingo Akagaki <dora@kondara.org>
- version 0.57

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed Jun 21 2000 Shingo Akagaki <dora@kondara.org>
- version 0.56

* Mon Jun 12 2000 Shingo Akagaki <dora@kondara.org>
- make without bonobo

* Sun May 14 2000 Shingo Akagaki <dora@kondara.org>
- version 0.53

* Mon Feb 29 2000 Jody Goldberg <jgolbderg@home.com>
- Updated version requirements for gnome-print, and gnome-libs.

* Sun Jan 30 2000 Gregory McLean <gregm@comstar.net>
- Added in some auto-detect the language files logic (rpm 3.0.xx only)

* Mon Jan 12 2000 Jody Goldberg <jgolbderg@home.com>
- Add depend on gtk+1 >= 1.2.2 so that we can get gtk_object_get.

* Mon Jan 03 2000 Gregory McLean <gregm@comstar.net>
- Updated to 0.47

* Mon Dec 20 1999 Jody Goldberg <jgolbderg@home.com>
- Updated the libglade dependancy.
- Remove req for guile.  We can build without it.

* Thu Sep 02 1999 Gregory McLean <gregm@comstar.net>
- Added small fix so glade generated dialogs appear.

* Wed Jul 14 1999 Gregory McLean <gregm@comstar.net>
- Added the gnome-print requirement.
- Updated the file list to try and quiet down the updating.
- added the sysconfdir

* Tue Mar 9  1999 Gregory McLean <gregm@comstar.net>
- Updated the spec file.

* Thu Sep 24 1998 Michael Fulbright <msf@redhat.com>
- Version 0.2
