%global momorel 1
%global abi_ver 1.8

# Generated from dnsruby-1.45.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby18 -rrbconfig -e "puts Config::CONFIG['sitelibdir']")
%define gemdir %(ruby18 -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname dnsruby
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Ruby DNS(SEC) implementation
Name: rubygem18-%{gemname}
Version: 1.52
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyforge.org/projects/dnsruby/
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems18
Requires: ruby(abi) = %{abi_ver}
BuildRequires: rubygems18 >= 1.3.7-2m
BuildArch: noarch
Provides: rubygem18(%{gemname}) = %{version}

%description
Ruby DNS(SEC) implementation


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
#mkdir -p ./%{gemdir}
#gem install --local --install-dir ./%{gemdir} \
gem18 install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/DNSSEC
%doc %{geminstdir}/EXAMPLES
%doc %{geminstdir}/README
%doc %{geminstdir}/EVENTMACHINE
#
%dir %{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/gems/%{gemname}-%{version}/*
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.52-1m)
- update 1.52 for opendnssec

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.48-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.48-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.48-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.48-2m)
- copy from rubygem-dnsruby

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.48-1m)
- update 1.48

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.47-1m)
- import to Momonga
- version up

* Mon Mar 22 2010 Ville Mattila <vmattila@csc.fi> 
- Initial package
