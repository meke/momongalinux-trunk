%global         momorel 7

Summary:	Perl binding for MeCab
Name:		perl-mecab
Version:	0.994
Release:	%{momorel}m%{?dist}
License:	LGPL 
Group:		Development/Libraries
URL:		http://mecab.sourceforge.net/
Source0:	http://mecab.googlecode.com/files/mecab-perl-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl
BuildRequires:	mecab-devel >= %{version}
Requires:	perl >= 5.0 
Requires:	mecab >= %{version}
Obsoletes:	perl-Text-MeCab

%description
Perl binding for MeCab

%prep
%setup -q -n mecab-perl-%{version}

%build
CFLAGS="%optflags" perl Makefile.PL INSTALLDIRS=vendor
make

%install
%makeinstall DESTDIR=%{buildroot}
rm %{buildroot}%{perl_vendorarch}/auto/MeCab/.packlist
rm %{buildroot}%{perl_archlib}/*.pod

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README test.pl bindings.html
%{perl_vendorarch}/auto/MeCab/*
%{perl_vendorarch}/MeCab.pm
#%%{perl_vendorarch}/test2.pl

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-2m)
- rebuild against perl-5.16.2

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-1m)
- update to 0.994

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-2m)
- rebuild against perl-5.16.0

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-1m)
- update to 0.993

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99
- rebuild against mecab-0.99

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-11m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98
- perl-Text-MeCab was OBSOLETED

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-5m)
- rebuild against perl-5.10.1

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-4m)
- version down to 0.97 official release

* Tue May 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-0.1.1m)
- update to 0.98pre1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-2m)
- rebuild against gcc43

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96-2m)
- %%NoSource -> NoSource

* Wed Jun 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.95-2m)
- use vendor

* Sun Apr  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95
- change URI from sourceforge.jp to sourceforge.net

* Wed Aug  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.93-1m)
- update to 0.93 (rebuild against mecab-0.93-1m)

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.92-1m)
- update to 0.92 (with mecab-0.92)

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.91-1m)
- update to 0.91 and rebuild against mecab-0.91-1m

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.81-3m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.81-2m)
- rebuilt against perl-5.8.7

* Tue Mar 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-1m)
- version up

* Mon Oct  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.78-3m)
- rebuild against gcc-c++-3.4.2

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.78-2m)
- rebuild against perl-5.8.5

* Wed Aug 18 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.78-1m)
- initial import
