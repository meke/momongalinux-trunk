%global momorel 5
%global libopensyncrel 2m

Summary: XML format Plug-In for OpenSync
Name: libopensync-plugin-xmlformat
Version: 0.39
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.opensync.org/
Group: System Environment/Libraries
Source0: http://opensync.org/download/releases/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake
BuildRequires: glib2-devel
BuildRequires: libopensync-devel >= %{version}-%{libopensyncrel}
BuildRequires: libxml2-devel
BuildRequires: pkgconfig

%description
This plug-in adds support for xml formats like xmlformat-contact,
xmlformat-event, etc. in OpenSync.

Additionally install the libopensync package.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
	-DCMAKE_SKIP_RPATH=YES ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/libopensync1/formats/xmlformat-doc.so
%{_libdir}/libopensync1/formats/xmlformat.so
%{_datadir}/libopensync1/schemas/xmlformat-calendar.xsd
%{_datadir}/libopensync1/schemas/xmlformat-common.xsd
%{_datadir}/libopensync1/schemas/xmlformat-contact.xsd
%{_datadir}/libopensync1/schemas/xmlformat-event.xsd
%{_datadir}/libopensync1/schemas/xmlformat-note.xsd
%{_datadir}/libopensync1/schemas/xmlformat-todo.xsd

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-2m)
- full rebuild for mo7 release

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-1m)
- initial package for new libopensync
