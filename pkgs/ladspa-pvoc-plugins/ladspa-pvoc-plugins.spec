%global momorel 3

Summary: LADSPA pvoc plugins
Name: ladspa-pvoc-plugins
Version: 0.1.12
Release: %{momorel}m%{?dist}
License: GPL
URL: http://quitte.de/dsp/pvoc.html
Group: Applications/Multimedia
Source0: http://quitte.de/dsp/pvoc_%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
Requires: fftw >= 3.0.1
Requires: libsndfile >= 1.0.5
BuildRequires: ladspa-devel
BuildRequires: fftw-devel >= 3.0.1
BuildRequires: libsndfile-devel >= 1.0.5

%description
LADSPA pvoc plugins

%prep
%setup -q -n pvoc-%{version}

%build
%make

%install
%make PLUGDEST=%{buildroot}%{_libdir}/ladspa UTILDEST=%{buildroot}%{_bindir} MAN1DEST=%{buildroot}%{_mandir}/man1 install

%files
%defattr(-, root, root)
%doc COPYING README pvoc.html
%{_bindir}/stretch
%{_mandir}/man1/stretch.1*
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.12-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.12-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.1.12-1m)
- initial build for Momonga Linux

