%global momorel 5

Summary: 1394-based digital camera control library
Name: libdc1394
Version: 2.1.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://sourceforge.net/projects/libdc1394/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: libdc1394-2.1.2-use-libv4l.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: libX11-devel
BuildRequires: libXv-devel
BuildRequires: libusb1-devel
BuildRequires: libraw1394-devel
BuildRequires: pkgconfig
# patch1 requires this
BuildRequires: libv4l-devel

%description
Libdc1394 is a library that is intended to provide a high level programming
interface for application developers who wish to control IEEE 1394 based
cameras that conform to the 1394-based Digital Camera Specification.

%package devel
Summary: Header files and libraries from libdc1394
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libdc1394.

%prep
%setup -q
%patch1 -p1 -b .use-libv4l~

%build
%configure \
	--enable-doxygen-dot \
	--enable-doxygen-html

%make

make doc

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%doc doc/html doc/%{name}.tag
%{_bindir}/dc1394_reset_bus
%{_bindir}/dc1394_vloopback
%{_libdir}/%{name}.so.*
%{_mandir}/man1/dc1394_multiview.1*
%{_mandir}/man1/dc1394_reset_bus.1*
%{_mandir}/man1/dc1394_vloopback.1*
%{_mandir}/man1/grab_color_image.1*
%{_mandir}/man1/grab_gray_image.1*
%{_mandir}/man1/grab_partial_image.1*

%files devel
%defattr(-,root,root)
%{_includedir}/dc1394
%{_libdir}/pkgconfig/%{name}-2.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.2-5m)
- fix v4l issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-2m)
- full rebuild for mo7 release

* Mon Mar  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.2-1m)
- initial package for opencv-2.0.0
