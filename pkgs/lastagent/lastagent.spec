%global momorel 7
%global srcrel 4
%global pylastver 0.2.18

Summary: A Last.fm music tracker for Linux
Name: lastagent
Version: 0.3.02
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://code.google.com/p/lastagent/
Group: Applications/Multimedia
Source0: http://lastagent.googlecode.com/files/%{name}-%{version}-%{srcrel}.tar.bz2
NoSource: 0
Patch0: %{name}-0.3.01-desktop.patch
Patch1: %{name}-0.2.05-pidof.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dbus-python
Requires: pygtk2
Requires: pylast >= %{pylastver}
Requires: python-sexy
Requires: procps-ng
BuildRequires: coreutils
BuildArch: noarch

%description
A music tracker for Linux that provides some of the missing functionality
of the official client on local music.
It allows you to love, tag or share the currently playing track, or add it
to a Last.fm playlist, and more...
All through a nice, neat and clean interface.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .pidof

# clean up for install
rm -rf gui/images/.svn

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing...
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/{applications,{%{name}/{gui/images,players},pixmaps}}

# install lastagent
install -m 755 %{name} %{buildroot}%{_bindir}/
install -m 644 %{name}.desktop %{buildroot}%{_datadir}/applications/
install -m 755 %{name} %{buildroot}%{_datadir}/%{name}/
install -m 644 *.py %{buildroot}%{_datadir}/%{name}/
install -m 644 %{name}.hidden.desktop %{buildroot}%{_datadir}/%{name}/
install -m 644 gui/*.py %{buildroot}%{_datadir}/%{name}/gui/
install -m 644 gui/images/* %{buildroot}%{_datadir}/%{name}/gui/images/
install -m 644 players/*.py %{buildroot}%{_datadir}/%{name}/players/

# link icon
ln -s ../%{name}/gui/images/app_blue.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING INSTALL LICENSE
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Jan 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.02-7m)
- remove Require: sysvinit-tools

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.02-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.02-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.02-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.02-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.02-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.02-1m)
- version 0.3.02

* Sun Nov  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.01-1m)
- version 0.3.01

* Fri Oct 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.05-1m)
- version 0.2.05
- do not add Epoch, because this package still stays trunk
- split pylast and Requires: pylast
- update pidof.patch
- set BuildArch: noarch

* Wed Oct  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.3-1m)
- version 0.2.3

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-3m)
- explicitly Requires: dbus-python, python-sexy

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-2m)
- apply pidof.patch

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-1m)
- initial package for music freaks using Momonga Linux
