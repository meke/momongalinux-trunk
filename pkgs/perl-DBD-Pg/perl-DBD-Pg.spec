%global         momorel 1

Name:           perl-DBD-Pg
Version:        3.3.0
Release:        %{momorel}m%{?dist}
Summary:        PostgreSQL database driver for the DBI module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBD-Pg/
Source0:        http://www.cpan.org/authors/id/T/TU/TURNSTEP/DBD-Pg-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.006001
BuildRequires:  perl-DBI >= 1.52
BuildRequires:  perl-Data-Peek
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Comments
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Module-Signature >= 0.50
BuildRequires:  perl-PathTools
BuildRequires:  perl-Perl-Critic
BuildRequires:  perl-Pod-Spell
BuildRequires:  perl-Test-Simple >= 0.61
BuildRequires:  perl-Test-Pod >= 0.95
BuildRequires:  perl-Test-Pod-Coverage
BuildRequires:  perl-Test-Warn >= 0.08
BuildRequires:  perl-Test-YAML-Meta >= 0.03
BuildRequires:  perl-Text-SpellChecker
BuildRequires:  perl-Time-HiRes
BuildRequires:  perl-version
Requires:       perl-DBI >= 1.52
Requires:       perl-Data-Peek
Requires:       perl-Encode
Requires:       perl-File-Comments
Requires:       perl-File-Temp
Requires:       perl-Module-Signature >= 0.50
Requires:       perl-PathTools
Requires:       perl-Perl-Critic
Requires:       perl-Pod-Spell
Requires:       perl-Test-Pod >= 0.95
Requires:       perl-Test-Pod-Coverage
Requires:       perl-Test-Warn >= 0.08
Requires:       perl-Test-YAML-Meta >= 0.03
Requires:       perl-Text-SpellChecker
Requires:       perl-Time-HiRes
Requires:       perl-version
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DBD::Pg is a Perl module that works with the DBI module to provide access
to PostgreSQL databases.

%prep
%setup -q -n DBD-Pg-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README Changes TODO
%{_mandir}/man?/*
%{perl_vendorarch}/DBD/*
%{perl_vendorarch}/Bundle/DBD/*
%{perl_vendorarch}/auto/DBD/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-1m)
- rebuild against perl-5.20.0
- update to 3.3.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.3-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.3-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.3-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.3-2m)
- rebuild against perl-5.16.2

* Wed Aug 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.3-1m)
- update to 2.19.3

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.2-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.2-2m)
- rebuild against perl-5.16.0

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.2-1m)
- update to 2.19.2

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.1-1m)
- update to 2.19.1

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19.0-1m)
- [SECURITY] CVE-2012-1151
- update to 2.19.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.1-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.1-2m)
- rebuild against perl-5.14.1

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.0-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.0-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.17.2-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17.2-1m)
- update to 2.17.2

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17.1-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.1-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17.1-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17.1-2m)
- rebuild against perl-5.12.0

* Fri Apr  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17.1-1m)
- update to 2.17.1

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17.0-1m)
- update to 2.17.0

* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.1-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.1-1m)
- update to 2.15.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.1-1m)
- update to 2.13.1

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.8-2m)
- rebuild against rpm-4.6

* Mon Dec 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.8-1m)
- update to 2.11.8

* Wed Dec 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.7-1m)
- update to 2.11.7

* Tue Dec  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.6-1m)
- update to 2.11.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.5-1m)
- update to 2.11.5

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.4-1m)
- update to 2.11.4

* Tue Nov  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.3-1m)
- update to 2.11.3

* Thu Oct 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.2-1m)
- update to 2.11.2

* Tue Oct 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.1-1m)
- update to 2.11.1

* Tue Sep 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.7-1m)
- update to 2.10.7

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-1m)
- update to 2.10.6

* Wed Sep 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.5-1m)
- update to 2.10.5

* Wed Sep  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Mon Sep  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Wed Aug 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.2-1m)
- update to 2.9.2

* Thu Jul 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7-1m)
- update to 2.8.7

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.5-1m)
- update to 2.8.5

* Fri Jul 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.4-1m)
- update to 2.8.4

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Mon Jun 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2

* Thu Jun 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Thu May 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Sat May  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Fri May  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Fri May  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Fri Apr 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.0-2m)
- rebuild against gcc43

* Tue Mar 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Sun Mar 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Tue Mar  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Sun Mar  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Fri Feb 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Wed Feb 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Mon Feb 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Wed Jan 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.9.1m)
- update to 2.0.0_9

* Tue Jan 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.8.1m)
- update to 2.0.0_8

* Sun Jan 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.7.1m)
- [SECURITY] CVE-2009-0663 CVE-2009-1341
- update to 2.0.0_7

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.49-3m)
- use vendor

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.49-2m)
- rebuild against postgresql-8.2.3

* Fri Jun  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.49-1m)
- update to 1.49

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.48-1m)
- update to 1.48

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.43-3m)
- rebuild against perl-5.8.8

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.43-2m)
- rebuild against postgresql-8.1.0

* Sun Jul 31 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.43-1m)
- update to 1.43

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.42-2m)
- rebuilt against perl-5.8.7

* Sat Jun 04 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.42-1m)
- update to 1.42

* Mon May 16 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.41-1m)
- update to 1.41

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (1.32-4m)
- rebuild against postgresql-8.0.2.

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.32-3m)
- rebuild against perl-5.8.5
- rebuild against perl-DBI 1.38-4m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.32-2m)
- remove Epoch from BuildRequires

* Wed Mar 10 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.32-1m)
- update to 1.32
- add TODO to %%doc

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.22-4m)
- rebuild against postgresql-7.4.1

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.22-3m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.22-2m)
- rebuild against perl-5.8.1

* Fri Oct 31 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.22-1m)
- update to 1.22
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- use %%{momorel}, %%NoSource
- change URL: preamble
- revise %%files

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.01-14m)
  rebuild against openssl 0.9.7a

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.01-13m)
- rebuild against for postgresql

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.01-12m)
- rebuild against perl-5.8.0

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.01-11m)
- remove BuildRequires: gcc2.95.3

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.01-10k)
- rebuild against for perl-5.6.1

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (1.01-8k)
- rebuild against postgresql 7.1.3-4k.
- add BuildRequires.

* Sat Oct 27 2001 Toru Hoshina <t@kondara.org>
- (1.01-6k)
- rebuild against postgresql 7.1.3.

* Mon Sep 17 2001 Toru Hoshina <toru@df-usa.com>
- (1.01-4k)
- no more fixpack...

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (1.01-2k)
- merge from rawhide. based on 1.01-1.
