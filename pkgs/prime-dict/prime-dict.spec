%global momorel 15
%define ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')
%global dic_date 20090507

Summary: Dictionary for PRIME (a Japanese PRedictive Input Method Editor)
Name: prime-dict
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://taiyaki.org/prime/
Source0: http://prime.sourceforge.jp/src/%{name}-%{version}.tar.gz
Source1: http://dl.sourceforge.net/sourceforge/mdk-ut/prime-dict-%{version}-%{dic_date}ut.tar.bz2
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: ruby18, prime >= 1.0.0.1-10m
BuildRequires: ruby18

%description
PRIME predicts user's input words using the knowledge of natural
language and the history of user's operations, and reduces the
cost of typing by the user.  For example, if a user wants to input
"application" and types "ap" as the beginning characters of the word,
PRIME might predict some candidate words like "apple", "application",
"appointment", etc...  And then the user can input "application"
easily by selecting the word from the candidate words by PRIME.

%prep
rm -rf %{buildroot}

%setup -q -a 1
cp prime-dict-%{version}-%{dic_date}ut/prime-dict* ./dict/

%build
%configure --with-rubydir=%{ruby18_sitelibdir}
%make

%install
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog
%{_datadir}/prime/*
%{ruby18_sitelibdir}/prime/prime-dict-config.rb

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-15m)
- add source(s)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-12m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-11m)
- build with ruby18

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-10m)
- rebuild against ruby19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-8m)
- update prime-dict-ut-20090507

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-7m)
- rebuild against rpm-4.6

* Sat Jan  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-6m)
- update prime-dict-ut-20081211

* Mon Sep  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-5m)
- add Source1: http://dl.sourceforge.net/sourceforge/mdk-ut/prime-dict-ut-%{dic_date}.tar.bz2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-4m)
- rebuild against gcc43

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-3m)
- revise %%files to avoid conflicting
- /usr/lib/ruby/1.8/prime is already provided by prime

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-2m)
- /usr/lib/ruby

* Wed Mar 30 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.0-1m)
- up to 1.0.0

* Tue Mar  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-1m)
- version 0.8.7

* Wed Dec 15 2004 TAKAHASHI Tamotsu <tamo>
- (0.8.5-1m)

* Wed Sep 29 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.8.4-1m)
- version up

* Tue Jun 22 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.8.3-1m)
- version up

* Wed Mar 3 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.6.8.1-1m)
- version up

* Wed Dec 31 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.6.5-1m)
- version up

* Mon Nov  3 2003 Masaki Yatsu <yatsu@digital-genes.com>
- (0.6.0-1)
- initial version
