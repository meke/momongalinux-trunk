%global momorel 6

#
# Specfile for rsnapshot
#
# https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=191603
#

Name:           rsnapshot
Version:        1.3.1
Release:        %{momorel}m%{?dist}
Summary:        Local and remote filesystem snapshot utility
Group:          Applications/System
License:        GPLv2+
URL:            http://www.rsnapshot.org/
Source0:        http://www.rsnapshot.org/downloads/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
# This patch enables a few defaults in the config file that make sense to have
# enabled in linux.
Patch0:         rsnapshot.conf.patch
Patch1:         rsnapshot-1.3.1-pod2man.patch
BuildRequires:  rsync
BuildRequires:  openssh-clients
Requires:       openssh-clients
Requires:       rsync

%description
This is a remote backup program that uses rsync to take backup snapshots of
filesystems.  It uses hard links to save space on disk.

%prep
%setup -q
%patch0 -p0
%patch1 -p1

# Disable the config-file testing during configure because it freaks out if
# you're building as non-root on a system that actually has rsnapshot installed.
sed -ie 's|if test -e "\$RSNAPSHOT_SYSCONFDIR/rsnapshot.conf"|if false|' configure

%build
%configure                              \
    --with-perl="%{__perl}"             \
    --with-rsync="%{_bindir}/rsync"     \
    --with-ssh="%{_bindir}/ssh"         \
    --with-logger="%{_bindir}/logger"   \
    --with-du="%{_bindir}/du"

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# Rename the installed .default config file to a usable name
mv $RPM_BUILD_ROOT/etc/rsnapshot.conf.default $RPM_BUILD_ROOT/etc/rsnapshot.conf

# Change the perms on the utils/ files so rpm doesn't pick up their dependencies
find utils/ -type f -print0 | xargs -r0 chmod 644

%post
%define logmsg logger -t %{name}/rpm

# Get the version of the current rsnapshot config file so we can see if it
# needs to be upgraded.
    CONF_VERSION="$(%{_bindir}/rsnapshot check-config-version 2>/dev/null)"
    if [ $? != 0 ]; then
        %logmsg "Error upgrading %{_sysconfdir}/rsnapshot.conf."
    fi

# Old version in need of an upgrade
    if [ "$CONF_VERSION" == "unknown" ]; then
        %{_bindir}/rsnapshot upgrade-config-file
        exit $?
    fi

# Latest version is 1.2, so anything else is not good.
    if [ "$CONF_VERSION" != "1.2" ]; then
        %logmsg "Error upgrading %{_sysconfdir}/rsnapshot.conf. Config format unknown!"
        exit 1
    fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING INSTALL README TODO
%doc rsnapshot.conf.default
%doc utils/
%config(noreplace) %{_sysconfdir}/rsnapshot.conf
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-6m)
- fix pod2man

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-1m)
- import from Rawhide

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.3.1-1
- fix license tag
- update to 1.3.1
- remove comment lines (more trouble than they're worth)

* Mon May 28 2007 Chris Petersen <rpm@forevermore.net>                  1.3.0-1
- Upgrade to 1.3.0

* Fri Sep  8 2006 Chris Petersen <rpm@forevermore.net>                  1.2.9-5
- Rebuild for FC6 glibc update

* Sun Jul 23 2006 Chris Petersen <rpm@forevermore.net>                  1.2.9-4
- Bump release number to correspond with fc4.

* Thu May 25 2006 Chris Petersen <rpm@forevermore.net>                  1.2.9-2
- Update configfile patch to work with 1.2.9

* Sat May 21 2006 Chris Petersen <rpm@forevermore.net>                  1.2.9-1
- Preliminary build for 1.2.9

* Wed May 17 2006 Chris Petersen <rpm@forevermore.net>                  1.2.3-2
- Add rsync and openssh-clients build requirements because the configure script checks for them.
- Remove perl requirement because it's detected by rpm.

* Wed May 17 2006 Chris Petersen <rpm@forevermore.net>                  1.2.3-1
- Update to version 1.2.3 which is in the downloads directory but not linked on the site.
- Change openssh requirement to openssh-clients
- Update %%post script with better info from the rpmforge spec and nifty logger reports
- Add utils/ to the %%doc files

* Sat May 13 2006 Chris Petersen <rpm@forevermore.net>                  1.2.1-1
- Update specfile to be compatible with fedora guidelines

* Sun Jan 29 2005 Nathan Rosenquist <nathan@rsnapshot.org>
- Added upgrade script

* Sat Jan 22 2005 Nathan Rosenquist <nathan@rsnapshot.org>
- Added --with-du option

* Thu Jan 15 2004 Nathan Rosenquist <nathan@rsnapshot.org>
- Added "AutoReqProv: no" for SuSE compatibility

* Fri Dec 26 2003 Nathan Rosenquist <nathan@rsnapshot.org>
- Added util-linux dependency, and --with-logger= option

* Fri Dec 19 2003 Nathan Rosenquist <nathan@rsnapshot.org>
- now fully support autoconf

* Tue Dec 16 2003 Nathan Rosenquist <nathan@rsnapshot.org>
- changed rsnapshot.conf to rsnapshot.conf.default from the source tree

* Wed Nov 05 2003 Nathan Rosenquist <nathan@rsnapshot.org>
- Removed fileutils dependency, added verification info

* Tue Nov 04 2003 Nathan Rosenquist <nathan@rsnapshot.org>
- fixed anonymous rsync error

* Thu Oct 30 2003 Nathan Rosenquist <nathan@rsnapshot.org>
- update to 1.0.3

* Tue Oct 28 2003 Carl Wilhelm Soderstrom <chrome@real-time.com>
- created spec file
