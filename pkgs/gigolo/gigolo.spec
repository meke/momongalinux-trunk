%global momorel 3

%global major 0.4

Summary:        GIO/GVFS management application
Name:           gigolo
Version:        0.4.1
Release:        %{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        GPLv2
URL:            http://www.uvena.de/gigolo/index.html
#Source0:        http://files.uvena.de/gigolo/gigolo-%{version}.tar.bz2
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: gtk2-devel
BuildRequires: desktop-file-utils

#Requires: %{_bindir}/gvfs-open
Requires: gvfs
#Requires: %{_bindir}/fusermount
Requires: fuse

Obsoletes: sion < 0.1.0-3

%description
A frontend to easily manage connections to remote filesystems using GIO/GVFS. 
It allows you to quickly connect/mount a remote filesystem and manage
bookmarks of such. 

%prep
%setup -q

%build
export CFLAGS="%{optflags}"
./waf -j1 configure --prefix=%{_prefix} \
              --exec-prefix=%{_exec_prefix} \
              --bindir=%{_bindir} \
              --sbindir=%{_sbindir} \
              --sysconfdir=%{_sysconfdir} \
              --datadir=%{_datadir} \
              --includedir=%{_includedir} \
              --libdir=%{_libdir} \
              --libexecdir=%{_libexecdir} \
              --localstatedir=%{_localstatedir} \
              --sharedstatedir=%{_sharedstatedir} \
              --mandir=%{_mandir} --infodir=%{_infodir} --enable-debug

./waf -j1 build -v

%install
rm -rf $RPM_BUILD_ROOT
DESTDIR=$RPM_BUILD_ROOT ./waf -j1 install 

# remove docs that waf installs in the wrong place
rm -rf $RPM_BUILD_ROOT/%{_datadir}/doc/gigolo

rm -f $RPM_BUILD_ROOT/%{_datadir}/applications/gigolo.desktop

desktop-file-install                                           \
      --dir=%{buildroot}%{_datadir}/applications               \
      --vendor=""                                              \
      --add-only-show-in XFCE                                  \
      _build_/default/%{name}.desktop

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README README.I18N TODO THANKS
%{_bindir}/gigolo
%{_datadir}/applications/gigolo.desktop
%{_mandir}/man1/gigolo.1.*

%changelog
* Wed Aug 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-3m)
- change Source0 URL

* Tue Jun 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-2m)
- add -j1 to avoid a possible dead lock with python-2.7.2

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-3m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against xfce4 4.6.2

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-2m)
- revise desktop file, add OnlyShowIn=XFCE; to gigolo.desktop
  because gigolo.desktop has no icon, KDE needs icon

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.1-1m)
- import from Fedora to Momonga

* Sun Feb 22 2009 Kevin Fenzi <kevin@tummy.com> - 0.2.1-1
- Update to 0.2.1
- Add THANKS
- Fix waf configure line and use local waf.

* Sun Feb 15 2009 Kevin Fenzi <kevin@tummy.com> - 0.2.0-2
- Fix CFLAGS
- Fix build to be verbose
- Use Fedora waf
- Remove vendor

* Sun Feb 15 2009 Kevin Fenzi <kevin@tummy.com> - 0.2.0-1
- Change name to gigolo
- Update to 0.2.0

* Sun Jan 04 2009 Kevin Fenzi <kevin@tummy.com> - 0.1.0-2
- Fix License tag
- Add Requires for needed binaries

* Fri Jan 02 2009 Kevin Fenzi <kevin@tummy.com> - 0.1.0-1
- Initial version for Fedora
