%global         momorel 2

Name:           perl-XML-LibXSLT
Version:        1.92
Release:        %{momorel}m%{?dist}
Summary:        Interface to the gnome libxslt library
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-LibXSLT/
Source0:        http://www.cpan.org/authors/id/S/SH/SHLOMIF/XML-LibXSLT-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-XML-LibXML >= 1:2.0014-2m
BuildRequires:  libdb-devel >=  5.3.15
BuildRequires:  libxml2-devel >= 2.9.0
BuildRequires:  libxslt-devel >= 1.1.28, gdbm-devel
BuildRequires:  db4-devel >= 4.8.26-1m
Requires:       perl-XML-LibXML >= 1:2.0014-2m
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is an interface to the gnome project's libxslt. This is an
extremely good XSLT engine, highly compliant and also very fast. I have
tests showing this to be more than twice as fast as Sablotron.

%prep
%setup -q -n XML-LibXSLT-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorarch}/auto/XML/LibXSLT
%{perl_vendorarch}/XML/LibXSLT.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-2m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-1m)
- update to 1.92

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.89-1m)
- update to 1.89

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.87-1m)
- update to 1.87
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.84-1m)
- update to 1.84

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.82-1m)
- update to 1.82

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.81-2m)
- rebuild against perl-5.18.1

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.81-1m)
- update to 1.81

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-2m)
- rebuild against perl-5.16.3

* Thu Jan 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-1m)
- update to 1.80

* Tue Nov 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.79-1m)
- update to 1.79

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-2m)
- rebuild against perl-5.16.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-1m)
- update to 1.78

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-3m)
- rebuild against perl-5.16.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-2m)
- rebuild against libdb-5.3.15

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-1m)
- update to 1.77

* Sat Oct 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.75-1m)
- update to 1.75

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.74-1m)
- update to 1.74

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-1m)
- update to 1.73

* Fri Oct  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-1m)
- update to 1.72

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-2m)
- rebuild against perl-5.14.2

* Sun Sep 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- update to 1.71

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.70-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.70-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.70-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-6m)
- rebuild against perl-5.12.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.70-5m)
- rebuild against db-4.8.26

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.70-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.70-2m)
- rebuild against libxml2-2.7.6 and perl-XML-LibXML-1:1.70-3m

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-1m)
- update to 1.70

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.68-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.68-3m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.68-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.68-1m)
- import from Fedora

* Wed Mar 18 2009 Stepan Kasal <skasal@redhat.com> - 1.68-3
- patch to fix a refcounting bug leading to segfaults (#490781)

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.68-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Dec 20 2008 Paul Howarth <paul@city-fan.org> - 1.68-1
- update to 1.68
- relax hard version requirement on XML::LibXML, which is at 1.69 upstream
  but 1.67 or above will suffice (care will still have to be taken to keep
  the packages in sync, particularly when XML::LibXML is updated)
- specify $RPM_OPT_FLAGS once rather than twice
- drop historical perl version requirement, which is met even by EL-3
- explicitly buildreq ExtUtils::MakeMaker rather than just perl-devel

* Mon Nov  3 2008 Stepan Kasal <skasal@redhat.com> - 1.66-2
- require XML::LibXML of the same version

* Fri Aug  8 2008 Zing <zing@fastmail.fm> - 1.66-1
- update to 1.66

* Sat May 31 2008 Zing <zing@fastmail.fm> - 1.63-6
- rpm check stage barfs on || :

* Mon Mar  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.63-5
- rebuild for new perl (again)

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.63-4
- Autorebuild for GCC 4.3

* Fri Feb  8 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.63-3
- rebuild for new perl

* Sat Jan 19 2008 Zing <zing@fastmail.fm> - 1.63-2
- build requires gdbm-devel

* Fri Jan 18 2008 Zing <zing@fastmail.fm> - 1.63-1
- update to 1.63

* Sat Aug 11 2007 Zing <zing@fastmail.fm> - 1.62-2
- require perl-devel

* Tue Aug  7 2007 Zing <zing@fastmail.fm> - 1.62-1
- update to 1.62
- Conform to Fedora Licensing Guideline

* Fri Sep  8 2006 Zing <zing@fastmail.fm> - 1.58-3
- rebuild for FE6

* Tue Feb 14 2006 Zing <shishz@hotpop.com> - 1.58-2
- rebuild for FE5

* Wed Aug 17 2005 Zing <shishz@hotpop.com> - 1.58-1
- new upstream
- use dist macro

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Mar  5 2005 Ville Skytta <ville.skytta at iki.fi> - 1.57-3
- Drop pre-FC2 LD_RUN_PATH hack.
- Install benchmark.pl only as %%doc.

* Fri Feb 26 2005 Zing <shishz@hotpop.com> - 1.57-2
- QA from Ville Skytta
-	BuildRequires XML::LibXML >= 1.57
-	BuildRequires libxslt-devel
-	put benchmark.pl in %%doc

* Fri Feb 25 2005 Zing <shishz@hotpop.com> - 1.57-1
- First build.

