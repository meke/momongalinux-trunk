%global momorel 1

%define ebminor 4

Name:		ebtables
Version:	2.0.10
Release:	%{momorel}m%{?dist}
Summary:	Ethernet Bridge frame table administration tool
License:	GPL
Group:		System Environment/Base
URL:		http://ebtables.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}-v%{version}-%{ebminor}.tar.gz
NoSource:   0
Source1:	ebtables-save
Source2:	ebtables.systemd
Source3:	ebtables.service
Patch0:		ebtables-2.0.10-norootinst.patch
Patch3:		ebtables-2.0.9-lsb.patch
Patch4:		ebtables-2.0.10-linkfix.patch
Patch5:		ebtables-2.0.0-audit.patch
# Upstream commit 5e126db0f
Patch6:			0001-add-RARP-and-update-iana-url.patch
BuildRequires:	binutils >= 2.18
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:		systemd-units
Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

%description
Ethernet bridge tables is a firewalling tool to transparently filter network
traffic passing a bridge. The filtering possibilities are limited to link
layer filtering and some basic filtering on higher network layers.

This tool is the userspace control for the bridge and ebtables kernel
components (built by default in Fedora Core kernels).

The ebtables tool can be used together with the other Linux filtering tools,
like iptables. There are no known incompatibility issues.

%prep
%setup -q -n ebtables-v%{version}-%{ebminor}
%patch0 -p1 -b .norootinst
%patch3 -p1 -b .lsb
# extension modules need to link to libebtc.so for ebt_errormsg
%patch4 -p1 -b .linkfix
%patch5 -p1 -b .AUDIT
%patch6 -p1 -b .RARP

# Convert to UTF-8
f=THANKS; iconv -f iso-8859-1 -t utf-8 $f -o $f.utf8 ; mv $f.utf8 $f

%build
MY_CFLAGS=`echo $RPM_OPT_FLAGS -fPIC | sed -e 's/-fstack-protector-strong//g' | sed -e 's/-fstack-protector//g'`
make %{?_smp_mflags} CFLAGS="$MY_CFLAGS" LIBDIR="/%{_lib}/ebtables" BINDIR="/sbin" MANDIR="%{_mandir}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_unitdir}
install -p %{SOURCE3} %{buildroot}%{_unitdir}/
chmod -x %{buildroot}%{_unitdir}/*.service
mkdir -p %{buildroot}%{_libexecdir}
install -m0755 %{SOURCE2} %{buildroot}%{_libexecdir}/ebtables
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
make DESTDIR="%{buildroot}" LIBDIR="/%{_libdir}/ebtables" BINDIR="/sbin" MANDIR="%{_mandir}" install
touch %{buildroot}%{_sysconfdir}/sysconfig/ebtables.filter
touch %{buildroot}%{_sysconfdir}/sysconfig/ebtables.nat
touch %{buildroot}%{_sysconfdir}/sysconfig/ebtables.broute

# Do not need the sysvinit
rm -rf %{buildroot}%{_initscriptdir}
rm -rf %{buildroot}/etc/rc.d

# install ebtables-save bash script
rm -f %{buildroot}/sbin/ebtables-save
install %{SOURCE1} %{buildroot}/sbin/ebtables-save

# move libebtc.so into the ldpath
mv %{buildroot}/%{_libdir}/ebtables/libebtc.so %{buildroot}/%{_libdir}/

%clean
rm -rf %{buildroot}

%post
%systemd_post ebtables.service
/sbin/ldconfig

%preun
%systemd_preun ebtables.service

%postun
%systemd_postun_with_restart ebtables.service
/sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc ChangeLog COPYING THANKS
%doc %{_mandir}/man8/ebtables.8*
%config(noreplace) %{_sysconfdir}/ethertypes
%config(noreplace) %{_sysconfdir}/sysconfig/ebtables-config
%{_unitdir}/ebtables.service
%{_libexecdir}/ebtables
/%{_libdir}/libebtc.so
/%{_libdir}/ebtables/
/sbin/ebtables*
%ghost %{_sysconfdir}/sysconfig/ebtables.filter
%ghost %{_sysconfdir}/sysconfig/ebtables.nat
%ghost %{_sysconfdir}/sysconfig/ebtables.broute

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.10-1m)
- update 2.0.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.9-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-1m)
- update to 2.0.9-2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-0.2.7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.8-0.2.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.8-0.2.5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-0.2.4m)
- rebuild against gcc43

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-0.2.3m)
- add ebtables-2.0.8-buildid.patch
-- add BuildRequires: binutils >= 2.18
- remove unused patches

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-0.2.2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.8-0.2.1m)
- version up 2.0.8-rc2

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.6-3m)
- add gcc4 patch
- Patch2: ebtables-2.0.6-gcc4.patch

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.6-2m)
- add Patch1: ebtables-v2.0.6-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Tue Dec 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.6-1m)
- update to 2.0.6

* Tue Dec 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-2m)
- modify Makefile.

* Wed Sep 10 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.5-1m)
- write spec for momonga
- ebtables is 
