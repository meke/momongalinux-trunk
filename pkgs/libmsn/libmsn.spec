%global         momorel 1

Name:		libmsn
Version:	4.2.1
Release:	%{momorel}m%{?dist}
Group:		System Environment/Libraries
License:	GPLv2
URL:		http://sourceforge.net/projects/libmsn/
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:		%{name}-4.0beta2-rmhardcoded.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	cmake
BuildRequires:	openssl-devel >= 1.0.0
#All other headers seem to be provided by glibc or stdlibc++

Summary:	Library for connecting to the MSN(tm) Messenger service

%description
Libmsn is a reusable, open-source, fully documented library for connecting to
the MSN(tm) Messenger service.

%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p1

%build
mkdir build
cd build
%{cmake} ..
make

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README COPYING THANKS
%{_bindir}/msntest
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/libmsn.pc
%{_includedir}/*

%changelog
* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-1m)
- update to 4.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-3m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-2m)
- apply openssl100 patch

* Mon Feb 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1-1m)
- update to 4.1

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-1m)
- update to 4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-0.8.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.8.1m)
- update to 4.0 beta8

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.7.1m)
- update to 4.0 beta7

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.6.1m)
- update to 4.0 beta6

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.5.1m)
- update to 4.0 beta5

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-0.4.2m)
- rebuild against openssl-0.9.8k

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.4.1m)
- update to 4.0 beta4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-0.2.2m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.2.1m)
- update to 4.0 beta2

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.1.1m)
- import from Fedora devel

* Thu Dec 11 2008 John5342 <john5342 at, fedoraproject.org> 4.0-0.3.beta1
- Left docs out of devel package
- Added (tm) to MSN

* Wed Dec 10 2008 John5342 <john5342 at, fedoraproject.org> 4.0-0.2.beta1
- Removed questionable trademarks in description and summary

* Wed Dec 10 2008 John5342 <john5342 at, fedoraproject.org> 4.0-0.1.beta1
- Initial package
