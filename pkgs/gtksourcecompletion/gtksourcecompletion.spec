%global momorel 10

Summary: Adds Completion support to GtkSourceView
Name:gtksourcecompletion
Version: 0.7.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Editors
Source0: http://cloud.github.com/downloads/chuchiperriman/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

Source1: ja.po
Patch0: gtksourcecompletion-0.7.0-ja.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://gtksourcecomple.sourceforge.net/

BuildRequires: intltool
BuildRequires: pkgconfig

BuildRequires: gtksourceview2-devel
BuildRequires: glib2-devel
BuildRequires: libglade2-devel

Requires: gtksourceview2

%description
gtksourcecompletion - Adds Completion support to GtkSourceView

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q
cp %{SOURCE1} po
%patch0 -p1 -b .ja

%build
gtkdocize --copy
autoreconf -vi
%configure --enable-static=no \
    --disable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/completion-custom-widgets
%{_bindir}/completion-focus
%{_bindir}/completion-simple
%{_bindir}/gio-test

%{_libdir}/libgtksourcecompletion-2.0.so.*
%exclude %{_libdir}/libgtksourcecompletion-2.0.la
%{_datadir}/locale/*/*/*
%{_datadir}/gtksourcecompletion/pixmaps/document-words-icon.png

%files devel
%defattr(-, root, root)
%{_includedir}/gtksourcecompletion-2.0
%{_libdir}/libgtksourcecompletion-2.0.so
%{_libdir}/pkgconfig/gtksourcecompletion-2.0.pc
%{_datadir}/gtk-doc/html/gtksourcecompletion-2.0

%changelog
* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-10m)
- disable gtk-doc

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-9m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.0-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.2-1m)
- initial build
