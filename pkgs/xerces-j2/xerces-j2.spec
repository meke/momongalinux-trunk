%global momorel 5

%global cvs_version 2_9_0

Name:          xerces-j2
Version:       2.9.0
Release:       %{momorel}m%{?dist}
Summary:       Java XML parser
Group:         Development/Libraries
License:       "ASL 2.0"
URL:           http://xerces.apache.org/xerces2-j/

Source0:       http://archive.apache.org/dist/xml/xerces-j/source/Xerces-J-src.%{version}.tar.gz
Source1:       %{name}-version.sh
Source2:       %{name}-constants.sh

# Custom javac ant task used by the build
Source3:       https://svn.apache.org/repos/asf/xerces/java/tags/Xerces-J_%{cvs_version}/tools/src/XJavac.java

# Upstream's build doesn't generate an OSGi manifest
Source4:       %{name}-MANIFEST.MF

# Patch the build so that it doesn't try to use bundled xml-commons source
Patch0:        %{name}-build.patch

BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch

BuildRequires: java-devel >= 1.6.0
BuildRequires: jpackage-utils
BuildRequires: xml-commons-apis >= 1.3
BuildRequires: xml-commons-resolver >= 1.1
BuildRequires: ant
BuildRequires: xalan-j2
BuildRequires: xml-stylebook
BuildRequires: jaxp_parser_impl
BuildRequires: dejavu-sans-fonts
Requires:      java
Requires:      jpackage-utils
Requires:      xml-commons-apis >= 1.3
Requires:      xml-commons-resolver >= 1.1

Provides:      jaxp_parser_impl = 1.3
Requires(post):  chkconfig jaxp_parser_impl
Requires(preun): chkconfig jaxp_parser_impl

# This documentation is provided by xml-commons-apis
Obsoletes:     %{name}-javadoc-apis < %{version}-%{release}

%description
Welcome to the future! Xerces2 is the next generation of high performance,
fully compliant XML parsers in the Apache Xerces family. This new version of
Xerces introduces the Xerces Native Interface (XNI), a complete framework for
building parser components and configurations that is extremely modular and
easy to program.

The Apache Xerces2 parser is the reference implementation of XNI but other
parser components, configurations, and parsers can be written using the Xerces
Native Interface. For complete design and implementation documents, refer to
the XNI Manual.

Xerces2 is a fully conforming XML Schema processor. For more information,
refer to the XML Schema page.

Xerces2 also provides a complete implementation of the Document Object Model
Level 3 Core and Load/Save W3C Recommendations and provides a complete
implementation of the XML Inclusions (XInclude) W3C Recommendation. It also
provides support for OASIS XML Catalogs v1.1.

Xerces2 is able to parse documents written according to the XML 1.1
Recommendation, except that it does not yet provide an option to enable
normalization checking as described in section 2.13 of this specification. It
also handles namespaces according to the XML Namespaces 1.1 Recommendation,
and will correctly serialize XML 1.1 documents if the DOM level 3 load/save
APIs are in use.

%package        javadoc-impl
Summary:        Javadoc for %{name} implementation
Group:          Documentation

%description    javadoc-impl
%{summary}.

%package        javadoc-xs
Summary:        Javadoc for %{name} XML schema API
Group:          Documentation

%description    javadoc-xs
%{summary}.

%package        javadoc-xni
Summary:        Javadoc for %{name} XNI
Group:          Documentation

%description    javadoc-xni
%{summary}.

%package        javadoc-other
Summary:        Javadoc for other %{name} components
Group:          Documentation

%description    javadoc-other
%{summary}.

%package        manual
Summary:        Manual for %{name}
Group:          Documentation
Requires:       xml-commons-apis-javadoc
Requires:       %{name}-javadoc-impl = %{version}-%{release}
Requires:       %{name}-javadoc-xs = %{version}-%{release}
Requires:       %{name}-javadoc-xni = %{version}-%{release}
Requires:       %{name}-javadoc-other = %{version}-%{release}

%description    manual
%{summary}.

%package        demo
Summary:        Demonstrations and samples for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    demo
%{summary}.

%package        scripts
Summary:        Additional utility scripts for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    scripts
%{summary}.

%prep
%setup -q -n xerces-%{cvs_version}
%patch0 -p0 -b .orig

# Copy the custom ant tasks into place
mkdir -p tools/org/apache/xerces/util
cp -a %{SOURCE3} tools/org/apache/xerces/util

# Make sure upstream hasn't sneaked in any jars we don't know about
JARS=""
for j in `find -name "*.jar"`; do
  if [ ! -L $j ]; then
    JARS="$JARS $j"
  fi
done
if [ ! -z "$JARS" ]; then
   echo "These jars should be deleted and symlinked to system jars: $JARS"
   exit 1
fi

%build
# Build custom ant tasks and jar repository needed for main build
pushd tools
javac -classpath $(build-classpath ant) org/apache/xerces/util/XJavac.java
mkdir bin && jar cf bin/xjavac.jar org/apache/xerces/util/XJavac.class
ln -sf $(build-classpath xml-commons-apis) .
ln -sf $(build-classpath xml-commons-resolver) .
ln -sf $(build-classpath xml-stylebook) .
ln -sf $(build-classpath xalan-j2) .
popd

# Build everything
export CLASSPATH=tools/bin/xjavac.jar:build/xercesImpl.jar
export ANT_OPTS="-Xmx256m -Djava.endorsed.dirs=$(pwd)/tools -Djava.awt.headless=true -Dbuild.sysclasspath=first -Ddisconnected=true"
ant \
        -Dbuild.compiler=modern \
        -Djar.apis=xml-commons-apis.jar \
        -Djar.resolver=xml-commons-resolver.jar \
        -Djar.serializer=xalan-j2.jar \
        -Ddoc.generator.package=tools/xml-stylebook.jar \
        clean jars javadocs docs

# Inject OSGi manifest
mkdir -p META-INF
cp -p %{SOURCE4} META-INF/MANIFEST.MF
touch META-INF/MANIFEST.MF
zip -u build/xercesImpl.jar META-INF/MANIFEST.MF

%install
rm -rf %{buildroot}

# jars
install -pD -T build/xercesImpl.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadoc
mkdir -p %{buildroot}%{_javadocdir}/%{name}-impl-%{version}
cp -pr build/docs/javadocs/xerces2/* %{buildroot}%{_javadocdir}/%{name}-impl-%{version}
(cd %{buildroot}%{_javadocdir} && ln -sf %{name}-impl-%{version} %{name}-impl)

mkdir -p %{buildroot}%{_javadocdir}/%{name}-xs-%{version}
cp -pr build/docs/javadocs/api/* %{buildroot}%{_javadocdir}/%{name}-xs-%{version}
(cd %{buildroot}%{_javadocdir} && ln -sf %{name}-xs-%{version} %{name}-xs)

mkdir -p %{buildroot}%{_javadocdir}/%{name}-xni-%{version}
cp -pr build/docs/javadocs/xni/* %{buildroot}%{_javadocdir}/%{name}-xni-%{version}
(cd %{buildroot}%{_javadocdir} && ln -sf %{name}-xni-%{version} %{name}-xni)

mkdir -p %{buildroot}%{_javadocdir}/%{name}-other-%{version}
cp -pr build/docs/javadocs/other/* %{buildroot}%{_javadocdir}/%{name}-other-%{version}
(cd %{buildroot}%{_javadocdir} && ln -sf %{name}-other-%{version} %{name}-other)

rm -rf build/docs/javadocs/*

# manual
install -d %{buildroot}%{_docdir}/%{name}-%{version}/manual
cp -pr build/docs/* %{buildroot}%{_docdir}/%{name}-%{version}/manual
ln -s ../../../../javadoc/xml-commons-apis/ %{buildroot}%{_docdir}/%{name}-%{version}/manual/javadocs/api
ln -s ../../../../javadoc/%{name}-impl/ %{buildroot}%{_docdir}/%{name}-%{version}/manual/javadocs/xerces2
ln -s ../../../../javadoc/%{name}-xs/ %{buildroot}%{_docdir}/%{name}-%{version}/manual/javadocs/xs
ln -s ../../../../javadoc/%{name}-xni/ %{buildroot}%{_docdir}/%{name}-%{version}/manual/javadocs/xni
ln -s ../../../../javadoc/%{name}-other/ %{buildroot}%{_docdir}/%{name}-%{version}/manual/javadocs/other

# other docs
install -p LICENSE README NOTICE %{buildroot}%{_docdir}/%{name}-%{version}

# scripts
install -pD -m755 -T %{SOURCE1} %{buildroot}%{_bindir}/%{name}-version
install -pD -m755 -T %{SOURCE2} %{buildroot}%{_bindir}/%{name}-constants

# demo
install -pD -T build/xercesSamples.jar %{buildroot}%{_datadir}/%{name}/%{name}-samples.jar
cp -pr data %{buildroot}%{_datadir}/%{name}

# jaxp_parser_impl ghost symlink
ln -s %{_sysconfdir}/alternatives \
  %{buildroot}%{_javadir}/jaxp_parser_impl.jar

%clean
rm -rf %{buildroot}

%post
update-alternatives --install %{_javadir}/jaxp_parser_impl.jar \
  jaxp_parser_impl %{_javadir}/%{name}.jar 40

%preun
{
  [ $1 = 0 ] || exit 0
  update-alternatives --remove jaxp_parser_impl %{_javadir}/%{name}.jar
} >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%dir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/LICENSE
%doc %{_docdir}/%{name}-%{version}/NOTICE
%doc %{_docdir}/%{name}-%{version}/README
%{_javadir}/%{name}*
%ghost %{_javadir}/jaxp_parser_impl.jar

%files javadoc-impl
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-impl-%{version}
%{_javadocdir}/%{name}-impl

%files javadoc-xs
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-xs-%{version}
%{_javadocdir}/%{name}-xs

%files javadoc-other
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-other-%{version}
%{_javadocdir}/%{name}-other

%files javadoc-xni
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-xni-%{version}
%{_javadocdir}/%{name}-xni

%files manual
%defattr(-,root,root,-)
#%%dir %{_docdir}/%{name}-%{version}
%{_docdir}/%{name}-%{version}/manual

%files demo
%defattr(-,root,root,-)
%{_datadir}/%{name}

%files scripts
%defattr(-,root,root,-)
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.0-3m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.0-2m)
- fix dups

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.0-1m)
- sync with Fedora 13 (2.9.0-4)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-7jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-7jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.1-7jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-7jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7.1-7jpp.1m)
- import from Fedora

* Sun Aug 13 2006 Warren Togami <wtogami@redhat.com> 0:2.7.1-7jpp.2
- fix typo in preun req

* Sat Aug 12 2006 Matt Wringe <mwringe at redhat.com> 0:2.7.1-7jpp.1
- Merge with upstream version

* Sat Aug 12 2006 Matt Wringe <mwringe at redhat.com> 0:2.7.1-7jpp
- Add conditional native compiling
- Add missing requires for javadocs
- Add missing requires for post and preun
- Update version to 7jpp at Fedora's request

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:2.7.1-6jpp_9fc
- Rebuilt

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:2.7.1-6jpp_8fc
- rebuild

* Thu Mar 30 2006 Fernando Nasser <fnasser@redhat.com> 0:2.7.1-3jpp
- Add missing BR for xml-stylebook

* Wed Mar 22 2006 Ralph Apel <r.apel at r-apel.de> 0:2.7.1-2jpp
- First JPP-1.7 release
- use tools subdir and give it as java.endorsed.dirs (for java-1.4.2-bea e.g.)

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:2.7.1-6jpp_7fc
- stop scriptlet spew

* Wed Feb 22 2006 Rafael Schloming <rafaels@redhat.com> - 0:2.7.1-6jpp_6fc
- Updated to 2.7.1

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0:2.6.2-6jpp_5fc
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0:2.6.2-6jpp_4fc
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Feb  2 2006 Archit Shah <ashah@redhat.com> 0:2.6.2-6jpp_3fc
- build xerces without using native code

* Mon Jan  9 2006 Archit Shah <ashah@redhat.com> 0:2.6.2-6jpp_2fc
- rebuilt for new gcj

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> 0:2.6.2-6jpp_1fc
- rebuilt for new gcj

* Tue Dec 13 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Oct 07 2005 Ralph Apel <r.apel at r-apel.de> 0:2.7.1-1jpp
- Upgrade to 2.7.1

* Thu Jul 21 2005 Ralph Apel <r.apel at r-apel.de> 0:2.6.2-7jpp
- Include target jars-dom3
- Create new subpackage dom3

* Mon Jul 18 2005 Gary Benson <gbenson at redhat.com> 0:2.6.2-5jpp_2fc
- Build on ia64, ppc64, s390 and s390x.
- Switch to aot-compile-rpm (also BC-compiles samples).

* Wed Jul 13 2005 Gary Benson <gbenson at redhat.com> 0:2.6.2-6jpp
- Build with Sun JDK (from <gareth.armstrong at hp.com>).

* Wed Jun 15 2005 Gary Benson <gbenson at redhat.com> 0:2.6.2-5jpp_1fc
- Upgrade to 2.6.2-5jpp.

* Tue Jun 14 2005 Gary Benson <gbenson at redhat.com> 0:2.6.2-5jpp
- Remove the tools tarball, and build xjavac from source.
- Patch xjavac to fix the classpath under libgcj too.

* Fri Jun 10 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_8fc
- Remove the tools tarball, and build xjavac from source.
- Replace classpath workaround to xjavac task and use
  xml-commons classes again (#152255).

* Thu May 26 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_7fc
- Rearrange how BC-compiled stuff is built and installed.

* Mon May 23 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_6fc
- Add alpha to the list of build architectures (#157522).
- Use absolute paths for rebuild-gcj-db.

* Thu May  5 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_5fc
- Add dependencies for %post and %postun scriptlets (#156901).

* Fri Apr 29 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_4fc
- BC-compile.

* Thu Apr 28 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_3fc
- Revert xjavac classpath workaround, and patch to use libgcj's
  classes instead of those in xml-commons (#152255).

* Thu Apr 21 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_2fc
- Add classpath workaround to xjavac task (#152255).

* Wed Jan 12 2005 Gary Benson <gbenson@redhat.com> 0:2.6.2-4jpp_1fc
- Reenable building of classes that require javax.swing (#130006).
- Sync with RHAPS.

* Mon Nov 15 2004 Fernando Nasser <fnasser@redhat.com>  0:2.6.2-4jpp_1rh
- Merge with upstream for 2.6.2 upgrade

* Thu Nov  4 2004 Gary Benson <gbenson@redhat.com> 0:2.6.2-2jpp_5fc
- Build into Fedora.

* Thu Oct 28 2004 Gary Benson <gbenson@redhat.com> 0:2.6.2-2jpp_4fc
- Bootstrap into Fedora.

* Fri Oct 1 2004 Andrew Overholt <overholt@redhat.com> 0:2.6.2-2jpp_4rh
- add coreutils BuildRequires

* Thu Sep 30 2004 Andrew Overholt <overholt@redhat.com> 0:2.6.2-2jpp_3rh
- Remove xml-commons-resolver as a Requires

* Thu Aug 26 2004 Ralph Apel <r.apel at r-apel.de> 0:2.6.2-4jpp
- Build with ant-1.6.2
- Dropped jikes requirement, built for 1.4.2

* Wed Jun 23 2004 Kaj J. Niemi <kajtzu@fi.basen.net> 0:2.6.2-3jpp
- Updated Patch #0 to fix breakage using BEA 1.4.2 SDK, new patch
  from <mwringe@redhat.com> and <vivekl@redhat.com>.

* Mon Jun 21 2004 Vivek Lakshmanan <vivekl@redhat.com> 0:2.6.2-2jpp_2rh
- Added new Source1 URL and added new %setup to expand it under the
  expanded result of Source0.
- Updated Patch0 to fix version discrepancies.
- Added build requirement for xml-commons-apis
 
* Mon Jun 14 2004 Matt Wringe <mwringe@redhat.com> 0:2.6.2-2jpp_1rh
- Update to 2.6.2
- made patch names comformant

* Mon Mar 29 2004 Kaj J. Niemi <kajtzu@fi.basen.net> 0:2.6.2-2jpp
- Rebuilt with jikes 1.18 for java 1.3.1_11

* Fri Mar 26 2004 Frank Ch. Eigler <fche@redhat.com> 0:2.6.1-1jpp_2rh
- add RHUG upgrade cleanup

* Tue Mar 23 2004 Kaj J. Niemi <kajtzu@fi.basen.net> 0:2.6.2-1jpp
- 2.6.2

* Thu Mar 11 2004 Frank Ch. Eigler <fche@redhat.com> 0:2.6.1-1jpp_1rh
- RH vacuuming
- remove jikes dependency
- add nonjikes-cast.patch

* Sun Feb 08 2004 David Walluck <david@anti-microsoft.org> 0:2.6.1-1jpp
- 2.6.1
- update Source0 URL
- now requires xml-commons-resolver

* Fri Jan  9 2004 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:2.6.0-1jpp
- Update to 2.6.0
- Patch #1 (xerces-j2-manifest.patch) is unnecessary (upstream)

* Tue Oct 21 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.5.0-1jpp
- Update to 2.5.0.
- Clean up versionless javadoc dir symlinking, own (ghost) the symlinks.
- Mark javadocs as %%doc.

* Wed Jun  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.4.0-3jpp
- Own (ghost) %%{_javadir}/jaxp_parser_impl.jar.
- Remove alternatives in preun instead of postun.

* Mon May 12 2003 David Walluck <david@anti-microsoft.org> 0:2.4.0-2jpp
- bug #17325 fixed upstream

* Mon May 12 2003 David Walluck <david@anti-microsoft.org> 0:2.4.0-1jpp
- 2.4.0
- BuildRequires: jikes
- update for JPackage 1.5
- re-diff'ed build patch for 2.4.0
- bug #17325 handled by perl now
- scripts: s|find-jar|build-classpath| and don't test for java-functions

* Wed Mar 26 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> - 2.3.0-2jpp
- For jpackage-utils 1.5
- zapped manual, since it doesn't want to build
- as a consequence, removed uneeded dependencies

* Mon Feb 24 2003 Ville Skytta <ville.skytta at iki.fi> - 2.3.0-1jpp
- Update to 2.3.0.
- Add a crude patch to work around invalid XML in doc sources, see
  <http://nagoya.apache.org/bugzilla/show_bug.cgi?id=17325>.
- Built with IBM's 1.3.1 SR3.

* Sat Dec 28 2002 Ville Skytta <ville.skytta at iki.fi> - 2.2.1-2jpp
- Add upstream patch which fixes problems with Tomcat's webapps.
  <http://nagoya.apache.org/bugzilla/show_bug.cgi?id=13282>
  <http://marc.theaimsgroup.com/?l=xerces-cvs&m=103791990130308>
- Separate scripts subpackage.

* Fri Nov 15 2002 Ville Skytta <ville.skytta at iki.fi> - 2.2.1-1jpp
- Update to 2.2.1.
- Change alternative to point to non-versioned jar.
- Don't remove alternative on upgrade.
- Fix Group tag for demo, javadoc and manual subpackages.
- Add version and constants scripts.
- Some spec file cleanup.

* Sun Oct  6 2002 Ville Skytta <ville.skytta at iki.fi> 2.2.0-2jpp
- Fix bad permissions for main jar.

* Sun Sep 29 2002 Ville Skytta <ville.skytta at iki.fi> 2.1.0-1jpp
- Update to 2.2.0.

* Tue Sep 10 2002 Ville Skytta <ville.skytta at iki.fi> 2.1.0-2jpp
- Rebuild with -Dcompiler=modern, not a Jikes bug this time, but sloppy code
  that is tolerated by javac.  See <http://www-124.ibm.com/developerworks/bugs/?func=detailbug&bug_id=3218&group_id=10> for details.

* Tue Sep 10 2002 Ville Skytta <ville.skytta at iki.fi> 2.1.0-1jpp
- 2.1.0.
- Updated description.
- Changed javadoc and manual group to Documentation.
- Spec file cleanups.

* Fri Jul 12 2002 Henri Gomez <hgomez@users.sourceforge.net> 2.0.2-4jpp
- add BuildRequires xerces-j1 and xalan-j2
- removed BuildRequires xml-commons-api since ant require jaxp_parser_impl
  which in turn require xml-commons-api ;)

* Mon Jul 01 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 2.0.2-3jpp 
- vendor, distribution, group tags
- provides jaxp_parser_impl
- dropped api jar
- renamed lone jar to %{name}.jar
- priority bumped to 40
- fixed stylebook build (add xerces-j1 in classpath)

* Wed Jun 26 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 2.0.2-2jpp
- rebuild for missing symlinks
- use sed instead of bash 2.x extension in link area to make spec compatible with distro using bash 1.1x

* Mon Jun 24 2002 Henri Gomez <hgomez@users.sourceforge.net> 2.0.2-1jpp
- 2.0.2

* Sun Mar 10 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 2.0.1-1jpp 
- 2.0.1
- provides jaxp_parser2 virtual resource
- drop wrapper

* Sun Feb 03 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 2.0.0-1jpp 
- first JPackage release
