%global momorel 1
%global date 20140619
%global momongaver 8

Summary: cleanup-packages clean up obsoleted packages for smooth upgrading by yum
Name: cleanup-packages
Version: %{momongaver}
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# 389*
Obsoletes: 389-admin
Obsoletes: 389-admin-console
Obsoletes: 389-admin-console-doc
Obsoletes: 389-adminutil
Obsoletes: 389-adminutil-devel
Obsoletes: 389-console
Obsoletes: 389-ds
Obsoletes: 389-ds-base
Obsoletes: 389-ds-base-libs
Obsoletes: 389-ds-base-devel
Obsoletes: 389-ds-console
Obsoletes: 389-ds-console-doc
Obsoletes: 389-dsgw

# CodeAnalyst-gui
Obsoletes: CodeAnalyst-gui

# HelixPlayer
Obsoletes: HelixPlayer
Obsoletes: HelixPlayer-mozilla-plugin
# OpenGTL
Obsoletes: OpenGTL
Obsoletes: OpenGTL-libs
Obsoletes: OpenGTL-devel
Obsoletes: libQtGTL
Obsoletes: libQtGTL-devel
# abiword
Obsoletes: abiword
Obsoletes: abiword-devel
# abrt
Obsoletes: abrt
Obsoletes: abrt-addon-ccpp
Obsoletes: abrt-addon-kerneloops
Obsoletes: abrt-addon-python
Obsoletes: abrt-cli
Obsoletes: abrt-desktop
Obsoletes: abrt-devel
Obsoletes: abrt-gui
Obsoletes: abrt-libs
Obsoletes: abrt-plugin-bugzilla
Obsoletes: abrt-plugin-catcut
Obsoletes: abrt-plugin-filetransfer
Obsoletes: abrt-plugin-logger
Obsoletes: abrt-plugin-mailx
Obsoletes: abrt-plugin-rhfastcheck
Obsoletes: abrt-plugin-rhticket
Obsoletes: abrt-plugin-runapp
Obsoletes: abrt-plugin-sosreport
Obsoletes: abrt-plugin-ticketuploader
# ant
Obsoletes: ant-manifest-only
# ash
Obsoletes: ash
# avifile
Obsoletes: avifile
# Banshee Backend
Obsoletes: mono-zeroconf mono-zeroconf-devel ipod-sharp ipod-sharp-devel podsleuth podsleuth-devel
# beagle
Obsoletes: beagle
Obsoletes: beagle-doc
# bind
Obsoletes: caching-nameserver
# battfink
Obsoletes: battfink
# bmpx
Obsoletes: bmpx
Obsoletes: bmpx-devel
# boa
Obsoletes: boa
# boo and nant
Obsoletes: boo
Obsoletes: nant
# booty
Obsoletes: booty
# cairo-dock
Obsoletes: cairo-dock
Obsoletes: cairo-dock-devel
Obsoletes: cairo-dock-plug-ins
Obsoletes: cairo-dock-plug-ins-gecko
Obsoletes: cairo-dock-plug-ins-xfce
Obsoletes: cairo-dock-themes
# cardinal
Obsoletes: cardinal
# chkfontpath-momonga
Obsoletes: chkfontpath-momonga

# clustermon
Obsoletes: cluster-cim
Obsoletes: cluster-snmp
Obsoletes: modcluster
# cmucl
Obsoletes: cmucl
Obsoletes: cmucl-extras
Obsoletes: emacs-emaxima
Obsoletes: maxima-exec_cmucl
# cogito
Obsoletes: cogito
# compiz
Obsoletes: ccsm
Obsoletes: compiz
Obsoletes: compiz-bcop
Obsoletes: compiz-core
Obsoletes: compiz-core-devel
Obsoletes: compiz-core-gtk-window-decorator
Obsoletes: compiz-core-kde
Obsoletes: compiz-core-kde-window-decorator
Obsoletes: compiz-fusion-plugins-extra
Obsoletes: compiz-fusion-plugins-extra-devel
Obsoletes: compiz-fusion-plugins-main
Obsoletes: compiz-fusion-plugins-main-devel
Obsoletes: compiz-fusion-plugins-main-kde
Obsoletes: compiz-fusion-plugins-unsupported
Obsoletes: compiz-fusion-plugins-unsupported-devel
Obsoletes: compiz-gnome
Obsoletes: compiz-kde
Obsoletes: compizconfig-backend-gconf
Obsoletes: compizconfig-backend-kconfig4
Obsoletes: compizconfig-python
Obsoletes: compizconfig-python-devel
Obsoletes: emerald
Obsoletes: emerald-devel
Obsoletes: emerald-themes
Obsoletes: fusion-icon
Obsoletes: libcompizconfig
Obsoletes: libcompizconfig-devel
# cpuspeed
Obsoletes: cpuspeed
# ctapi-cyberjack
Obsoletes: ctapi-cyberjack
Obsoletes: ctapi-cyberjack-devel
Obsoletes: ctapi-cyberjack-pcsc
Obsoletes: ctapi-cyberjack-tools
# cxplorer
Obsoletes: libcxp
Obsoletes: libcxp-devel
Obsoletes: cxplorer
# desktop-file-utils-0.15
Obsoletes: desktop-file-utils-0.15
# dhcp
Obsoletes: dhcp-static
Obsoletes: libdhcp4client-static
# distcache
Obsoletes: distcache
Obsoletes: distcache-devel
# eclipse
Obsoletes: eclipse-cvs-client-sdk
Obsoletes: eclipse-jdt-sdk
Obsoletes: eclipse-pde-sdk
Obsoletes: eclipse-platform-sdk
Obsoletes: eclipse-rcp-sdk
# eds-feed
Obsoletes: eds-feed
# elisp-Mule-UCS
Obsoletes: Mule-UCS-xemacs
Obsoletes: elisp-Mule-UCS
# elisp-easypg
Obsoletes: easypg-xemacs
Obsoletes: elisp-easypg
# elisp-mew
Obsoletes: mew-xemacs
# elisp-mixi
Obsoletes: elisp-mixi
Obsoletes: mixi-emacs
Obsoletes: mixi-xemacs
# elisp-morq
Obsoletes: elisp-morq
# esc
Obsoletes: esc
# eureka
Obsoletes: eureka
# eventlog
Obsoletes: eventlog
Obsoletes: eventlog-devel
Obsoletes: eventlog-static
# fastladder
Obsoletes: fastladder
# findutils
Obsoletes: findutils-locate
# firefox
Obsoletes: firefox-devel
# fonts-arabic
Obsoletes: fonts-arabic
# fonts-chinese
Obsoletes: fonts-chinese
# foxkit
Obsoletes: foxkit
# fusion-icon
Obsoletes: fusion-icon-qt3
# galeon
Obsoletes: galeon
# gcl
Obsoletes: gcl
# gecko-mediaplayer
Obsoletes: gecko-mediaplayer
# cluster
Obsoletes: gfs-utils
# gimmie
Obsoletes: gimmie
# gluezilla
Obsoletes: gluezilla gluezilla-devel
# gphpedit
Obsoletes: gphpedit
# gnash
Obsoletes: gnash
Obsoletes: gnash-devel
Obsoletes: gnash-plugin
Obsoletes: gnash-klash
Obsoletes: gnash-cygnal
Obsoletes: python-gnash
# gnome-device-manager
Obsoletes: gnome-device-manager
Obsoletes: gnome-device-manager-devel
Obsoletes: gnome-device-manager-libs
# gnome-mag
Obsoletes: gnome-mag
Obsoletes: gnome-mag-devel
# gnome-system-tools
Obsoletes: gnome-system-tools
# gnopernicus
Obsoletes: gnopernicus
Obsoletes: gnopernicus-devel
# git
Obsoletes: git-p4
# grass-i18n
Obsoletes: grass-i18n
# goffice-0.4
Obsoletes: goffice-0.4
Obsoletes: goffice-0.4-devel
# gonzui
Obsoletes: gonzui
# guake
Obsoletes: guake
# hal
Obsoletes: hal
Obsoletes: hal-devel
Obsoletes: hal-docs
Obsoletes: hal-filesystem
Obsoletes: hal-gnome
Obsoletes: hal-info
Obsoletes: hal-libs
Obsoletes: hal-storage-addon
# hal-cups-utils
Obsoletes: hal-cups-utils
# heap-buddy
Obsoletes: heap-buddy
# heartbeat
Obsoletes: heartbeat
Obsoletes: heartbeat-libs
Obsoletes: heartbeat-devel
# hwbrowser
Obsoletes: hwbrowser
# htmlview
Obsoletes: htmlview
# ikvm
Obsoletes: ikvm
# ipa
Obsoletes: ipa-admintools
Obsoletes: ipa-client
Obsoletes: ipa-python
Obsoletes: ipa-radius-admintools
Obsoletes: ipa-radius-server
Obsoletes: ipa-server
Obsoletes: ipa-server-selinux
# jaxen-bootstrap
Obsoletes: jaxen-bootstrap
# k9copy
Obsoletes: k9copy
# kde-plasma-ihatethecashew
Obsoletes: kde-plasma-ihatethecashew
# kerneloops
Obsoletes: kerneloops
# kerry
Obsoletes: kerry
# kgrubeditor
Obsoletes: kgrubeditor
# krbafs
Obsoletes: krbafs
Obsoletes: krbafs-devel
Obsoletes: krbafs-utils
# kscope
Obsoletes: kscope
Obsoletes: kscope-devel
# kudzu
Obsoletes: kudzu
Obsoletes: kudzu-devel
# libakode
Obsoletes: libakode
Obsoletes: libakode-devel
# libbeagle
Obsoletes: libbeagle
Obsoletes: libbeagle-devel
Obsoletes: libbeagle-doc
Obsoletes: libbeagle-python
# libcm
Obsoletes: libcm
Obsoletes: libcm-devel
# libdhcp
Obsoletes: libdhcp-static
# libopensync
Obsoletes: libopensync-plugin-evolution2-devel
Obsoletes: libopensync-plugin-gpe-devel
Obsoletes: libopensync-plugin-palm-devel
# libtorrent
Obsoletes: libtorrent
# libzvt
Obsoletes: libzvt
Obsoletes: libzvt-devel
# logsentry
Obsoletes: logsentry
# loqui
Obsoletes: loqui
# lucene
Obsoletes: lucene-devel
# man-pages-uk
Obsoletes: man-pages-uk
# matahari
Obsoletes: matahari
Obsoletes: matahari-agent-lib
Obsoletes: matahari-broker
Obsoletes: matahari-consoles
Obsoletes: matahari-core
Obsoletes: matahari-devel
Obsoletes: matahari-host
Obsoletes: matahari-lib
Obsoletes: matahari-network
Obsoletes: matahari-python
Obsoletes: matahari-rpc
Obsoletes: matahari-service
Obsoletes: matahari-shell
Obsoletes: matahari-sysconfig
Obsoletes: matahari-vios-proxy-common
Obsoletes: matahari-vios-proxy-guest
Obsoletes: matahari-vios-proxy-host
# ming-slide
Obsoletes: ming-slide
# mknmz-wwwoffle
Obsoletes: mknmz-wwwoffle
# monster-masher
Obsoletes: monster-masher
# moon
Obsoletes: moon
Obsoletes: moon-mozplugin
# moonshine
Obsoletes: moonshine
Obsoletes: moonshine-mozplugin
# mplayerplug-in
Obsoletes: mplayerplug-in
# nautilus-image-converter
Obsoletes: nautilus-image-converter
# ncpfs
Obsoletes: ncpfs
Obsoletes: ncpfs-devel
Obsoletes: ipxutils
# nessus
Obsoletes: nessus
Obsoletes: nessus-client
Obsoletes: nessus-devel
Obsoletes: nessus-libraries
Obsoletes: nessus-libraries-devel
Obsoletes: nessus-plugins
Obsoletes: libnasl
Obsoletes: libnasl-devel
# ocaml-bitstring-c
Obsoletes: ocaml-bitstring-c
# ocaml-pa-do
Obsoletes: ocaml-pa-do
Obsoletes: ocaml-pa-do-devel
# ossec-hids
Obsoletes: ossec-hids
Obsoletes: ossec-hids-client
Obsoletes: ossec-hids-server
# ossec-wui
Obsoletes: ossec-wui
# pam module(s)
Obsoletes: pam_smb
# perl module(s)
Obsoletes: perl-Net-IPMessenger
# php-eaccelerator
Obsoletes: php-eaccelerator
# php-pecl-apc
Obsoletes: php-pecl-apc
Obsoletes: php-pecl-apc-devel
# php-pecl-apm
Obsoletes: php-pecl-apm
# pino
Obsoletes: pino
# plague
Obsoletes: plague
Obsoletes: plague-builder
Obsoletes: plague-client
Obsoletes: plague-common
Obsoletes: plague-utils
# plain2
Obsoletes: plain2
# plone
Obsoletes: plone
# publican
Obsoletes: publican
Obsoletes: publican-doc
Obsoletes: publican-momonga
# pulseaudio
Obsoletes: pulseaudio-devel
# pump
Obsoletes: pump
Obsoletes: pump-devel
# PyKDE / PyQt
Obsoletes: PyKDE
Obsoletes: PyKDE-devel
Obsoletes: PyKDE-examples
Obsoletes: PyQt
Obsoletes: PyQt-devel
Obsoletes: PyQt-examples
# pyclutter-gtk
Obsoletes: pyclutter-gtk pyclutter-gtk-devel
# pypy
Obsoletes: pypy
Obsoletes: pypy-devel
Obsoletes: pypy-libs
# pyxf86config
Obsoletes: pyxf86config
# qca-tls
Obsoletes: qca-tls
# qtparted
Obsoletes: qtparted
# qtwitter
Obsoletes: qtwitter
# rbbr
Obsoletes: rbbr
# revisor
Obsoletes: revisor
Obsoletes: revisor-cli
Obsoletes: revisor-cobbler
Obsoletes: revisor-comps
Obsoletes: revisor-gui
Obsoletes: revisor-isolinux
Obsoletes: revisor-mock
Obsoletes: revisor-reuseinstaller
Obsoletes: revisor-unity
# ricci
Obsoletes: ccs
Obsoletes: ricci
# rhpl
Obsoletes: rhpl
# rhpxl
Obsoletes: rhpxl
# roundcubemail
Obsoletes: roundcubemail
# ruby-*
Obsoletes: ruby-devel-logger
Obsoletes: ruby-eruby
Obsoletes: ruby-eruby-devel
Obsoletes: ruby-ming
# ruby18-gnome2
Obsoletes: ruby18-gnome2
Obsoletes: ruby18-gnome2-devel
Obsoletes: ruby18-atk
Obsoletes: ruby18-atk-devel
Obsoletes: ruby18-bonobo2
Obsoletes: ruby18-bonobo2-devel
Obsoletes: ruby18-bonoboui2
Obsoletes: ruby18-bonoboui2-devel
Obsoletes: ruby18-gconf2
Obsoletes: ruby18-gconf2-devel
Obsoletes: ruby18-gdkpixbuf2
Obsoletes: ruby18-gdkpixbuf2-devel
Obsoletes: ruby18-glib2
Obsoletes: ruby18-glib2-devel
Obsoletes: ruby18-gnomecanvas2
Obsoletes: ruby18-gnomecanvas2-devel
Obsoletes: ruby18-gnomeprint2
Obsoletes: ruby18-gnomeprint2-devel
Obsoletes: ruby18-gnomeprintui2
Obsoletes: ruby18-gnomeprintui2-devel
Obsoletes: ruby18-gnomevfs
Obsoletes: ruby18-gnomevfs-devel
Obsoletes: ruby18-goocanvas
Obsoletes: ruby18-goocanvas-devel
Obsoletes: ruby18-gstreamer
Obsoletes: ruby18-gstreamer-devel
Obsoletes: ruby18-gtk2
Obsoletes: ruby18-gtk2-devel
Obsoletes: ruby18-gtkglext
Obsoletes: ruby18-gtkglext-devel
Obsoletes: ruby18-gtkhtml2
Obsoletes: ruby18-gtkhtml2-devel
Obsoletes: ruby18-gtkmozembed
Obsoletes: ruby18-gtkmozembed-devel
Obsoletes: ruby18-gtksourceview
Obsoletes: ruby18-gtksourceview-devel
Obsoletes: ruby18-gtksourceview2
Obsoletes: ruby18-gtksourceview2-devel
Obsoletes: ruby18-libart2
Obsoletes: ruby18-libart2-devel
Obsoletes: ruby18-libglade2
Obsoletes: ruby18-libglade2-devel
Obsoletes: ruby18-pango
Obsoletes: ruby18-pango-devel
Obsoletes: ruby18-poppler
Obsoletes: ruby18-poppler-devel
Obsoletes: ruby18-rsvg
Obsoletes: ruby18-rsvg-devel
Obsoletes: ruby18-vte
Obsoletes: ruby18-vte-devel

# smolt
Obsoletes: smolt
Obsoletes: smolt-gui
Obsoletes: smolt-server
# stumpwm
Obsoletes: stumpwm
# system-config-*
Obsoletes: system-config-cluster
Obsoletes: system-config-network
Obsoletes: system-config-soundcard
# sysvinit-*
Obsoletes: sysvinit
Obsoletes: sysvinit-tools
# telepathy-*
Obsoletes: telepathy-stream-engine
# tetex
Obsoletes: tetex-extramacros-jsclasses
Obsoletes: tetex-extramacros-otf
Obsoletes: tetex-extramacros-prosper
Obsoletes: tetex-extramacros-utf
# sblim-cmpi-devel
Obsoletes: sblim-cmpi-devel
# selinux-policy
Obsoletes: selinux-policy-strict
# skencil
Obsoletes: skencil
# snort 
Obsoletes: daq
Obsoletes: daq-devel
Obsoletes: snort
Obsoletes: snort-plain+flexresp
Obsoletes: snort-bloat
Obsoletes: snort-snmp
Obsoletes: snort-snmp+flexresp
Obsoletes: snort-mysql
Obsoletes: snort-postgresql
Obsoletes: snort-postgresql+flexresp
Obsoletes: snort-mysql+flexresp
# spin-kickstarts
Obsoletes: spin-kickstarts
# system-tray-applet
Obsoletes: system-tray-applet
# subversion
Obsoletes: subversion-python
# twitter client
Obsoletes: gtktwitter twitux
# umb-scheme
Obsoletes: umb-scheme
# xgawk
Obsoletes: xgawk
# xorg-x11-drv
Obsoletes: xorg-x11-drv-diamondtouch
Obsoletes: xorg-x11-drv-fpit
Obsoletes: xorg-x11-drv-hyperpen
Obsoletes: xorg-x11-drv-mutouch
# xprint
Obsoletes: xprint
Obsoletes: xprint-common
# youtranslate
Obsoletes: youtranslate
# valadoc
Obsoletes: valadoc
Obsoletes: valadoc-devel
# vtg, DO NOT FORGET REMOVE FOLLOWING LINES
Obsoletes: vtg
Obsoletes: vtg-devel
# vzctl
Obsoletes: vzctl
Obsoletes: vzctl-lib
# vzquota
Obsoletes: vzquota
# wwwoffle
Obsoletes: wwwoffle
# xorg-x11-drv-displaylink
Obsoletes: xorg-x11-drv-displaylink
# xulrunner-python
Obsoletes: xulrunner-python
Obsoletes: xulrunner-python-devel
# zbar
Obsoletes: zbar
Obsoletes: zbar-devel
Obsoletes: zbar-gtk
Obsoletes: zbar-gtk-devel
Obsoletes: zbar-pygtk
Obsoletes: zbar-qt
Obsoletes: zbar-qt-devel
# zphoto
Obsoletes: zphoto

%ifarch %{ix86}
# zfs
Obsoletes: zfs
Obsoletes: zfs-test
Obsoletes: zfs-devel
Obsoletes: zfs-modules
Obsoletes: zfs-modules-devel
Obsoletes: zfs-modules-PAE
Obsoletes: zfs-modules-PAE-devel
%endif

# zope
Obsoletes: zope

# OLPC related packages
Obsoletes: dracut-modules-olpc
Obsoletes: bitfrost

# Gnome shell extensions
Obsoletes: gnome-shell-extension-noim
Obsoletes: gnome-shell-extension-pomodoro
Obsoletes: gnome-shell-extension-remove-accessibility-icon
Obsoletes: gnome-shell-extension-righthotcorner
Obsoletes: gnome-shell-extension-theme-selector
Obsoletes: gnome-shell-extension-advanced-settings-in-usermenu
Obsoletes: gnome-shell-extension-icon-manager
Obsoletes: gnome-shell-extension-mediaplayers
Obsoletes: gnome-shell-extension-noripple
Obsoletes: gnome-shell-extension-pidgin
Obsoletes: gnome-shell-extension-presentation-mode
Obsoletes: gnome-shell-extension-remove-bluetooth-icon
Obsoletes: gnome-shell-extension-remove-volume-icon
Obsoletes: gnome-shell-extension-removeimstatus
Obsoletes: gnome-shell-extension-workspacesmenu

# We removed the following packages at July 2012
# see [Momonga-devel.ja:04807] for details
Obsoletes: evolution-sharp
Obsoletes: evolution-sharp-devel
Obsoletes: gnome-build
Obsoletes: gnome-build-devel
Obsoletes: gdlmm
Obsoletes: gdlmm-devel

Obsoletes: bug-buddy

Obsoletes: monkey-bubble

Obsoletes: libgnomedb  libgnomedb-devel
Obsoletes: libgnomedb3 libgnomedb3-devel
Obsoletes: mergeant

Obsoletes: sdr

Obsoletes: gnome-cups-manager
Obsoletes: gnome-cups-manager-devel
Obsoletes: libsoup-2.2
Obsoletes: libsoup-2.2-devel

%description
cleanup-packages clean up obsoleted packages for smooth upgrading by yum.
This package works at upgrading from STABLE_7 to STABLE_8 only.
And this package should be removed from the system after upgrading.

%files

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140619-1m)
- Obsoletes: htmlview,system-config-network

* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20140429-1m)
- Obsoletes: valadoc*
- Obsoletes: zbar*

* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140324.2m)
- Obsoletes: plague-*

* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140324.1m)
- Obsoletes: nessus*, libnasl*

* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140320.1m)
- Obsoletes: 389*

* Sat Mar  8 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20140308.1m)
- Obsoletes: findutils-locate

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140303.1m)
- Obsoletes: pam_smb

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140302.2m)
- Obsoletes: snort-* and daq*

* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140302.1m)
- Obsoletes: xgawk

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140226.1m)
- Obsoletes: eventlog,eventlog-devel,eventlog-static

* Sun Feb  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20140202.1m)
- Obsoletes: k9copy

* Sat Jan 25 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140125.1m)
- Obsoletes: zphoto

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140124.1m)
- Obsoletes: avifile, libakode, libakode-devel

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140123.1m)
- Obsoletes: OpenGTL, OpenGTL-libs, OpenGTL-devel
-            libQtGTL, libQtGTL-devel

* Mon Jan 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140120.1m)
- Obsoletes: sysvinit sysvinit-tools

* Wed Jan 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140115.2m)
- Obsoletes: ncpfs ncpfs-devel ipxutils

* Wed Jan 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20140115.1m)
- Obsoletes: ash

* Sun Oct 06 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20131006.1m)
- Obsoletes: wwwoffle
- Obsoletes: mknmz-wwwoffle

* Fri Oct 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20131003.1m)
- Obsoletes: boa

* Tue Oct  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20131001.1m)
- Obsoletes: ccs

* Tue Oct 01 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20130930.2m)
- Obsoletes: distcache
- Obsoletes: distcache-devel

* Tue Oct 01 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20130930.1m)
- Obsoletes: ricci

* Sun Sep 15 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20130915.1m)
- Obsoletes: xorg-x11-drv-displaylink

* Tue Aug 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20130820.1m)
- Obsoletes: matahari-vios-proxy-common

* Mon Aug 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20130819-1m)
- Obsoletes matahari and those subpackages
- Obsoletes heartbeat and those subpackages
- Obsoletes clustermon (modcluster, cluster-cim, cluster-snmp)

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20130518-1m)
- Obsoletes ruby-ming due to ming-0.4.4
- Obsoletes ming-slide

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20130316-1m)
- Obsoletes perl-Net-IPMessenger

* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20130217.1m)
- Obdoletes PyKDE ad PyQt related packages
-- PyKDE
-- PyKDE-devel
-- PyKDE-examples
-- PyQt
-- PyQt-devel
-- PyQt-examples

* Tue Jan 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20130115.2m)
- remove Obsoletes: valadoc, valadoc-devel and kgraphviewer

* Tue Jan 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20130115.1m)
- obsoletes following packages for a while
-- valadoc
-- valadoc-devel
-- kgraphviewer

* Fri Dec 21 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (8-0.20121221.1m)
- Obsoletes Gnome shell extensions 
-- gnome-shell-extension-advanced-settings-in-usermenu
-- gnome-shell-extension-icon-manager
-- gnome-shell-extension-mediaplayers
-- gnome-shell-extension-noripple
-- gnome-shell-extension-pidgin
-- gnome-shell-extension-presentation-mode
-- gnome-shell-extension-remove-bluetooth-icon
-- gnome-shell-extension-remove-volume-icon
-- gnome-shell-extension-removeimstatus
-- gnome-shell-extension-workspacesmenu

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20121210.2m)
- remove kmymoney and related packages entry

* Mon Dec 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20121210.1m)
- add kmymoney for the moment

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120927.1m)
- add gnome-cups-manager
- add libsoup-2.2

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120827.3m)
- Obsoletes: compiz
- Obsoletes: compiz-kde

* Mon Aug 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120827.2m)
- add compizconfig-backend-kconfig4

* Mon Aug 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120827.1m)
- Obsolete compiz packages

* Thu Jul 31 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20120731.1m)
- remove gnome-utils and gnome-utils-devel obsoleted by gnome-system-log

* Tue Jul 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120730.1m)
- Obsolete gnome-utils

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120721.1m)
- fix for BTS 447 issue; Obsolete sdr

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120720.1m)
- Obsolete EOLed packages; libgnomedb, libgnomedb3 and mergeant

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20120719.1m)
- Obsoletes: eds-feed
- Obsoletes: goffice-0.4
- Obsoletes: goffice-0.4-devel

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120716.1m)
- Obsoletes: xulrunner-python
- Obsoletes: xulrunner-python-devel

* Sun Jul 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20120715.1m)
- Obsoletes: gnome-mag
- Obsoletes: gnome-mag-devel
- remove OpenGTL

* Tue Jul 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20120710.1m)
- Obsoletes: loqui

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20120708.4m)
- remove fedora-gnome-theme, it's obsoleted and provided by gnome-themes-standard
- remove gnome-python2-bugbuddy, it's obsoleted by gnome-python2-desktop

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120708.3m)
- Obsoletes bug-buddy, gnome-python2-bugbuddy, monkey-bubble

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120708.2m)
- Obsoletes fedora-gnome-theme

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20120708.1m)
- move some Obsoletes to suitable packages

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20120707.1m)
- Obsoletes some packages; see [Momonga-devel.ja:04807]

* Wed Apr 11 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (8-0.20120411.1m)
- Obsoletes Gnome shell extensions 
-- gnome-shell-extension-noim
-- gnome-shell-extension-pomodoro
-- gnome-shell-extension-remove-accessibility-icon
-- gnome-shell-extension-righthotcorner
-- gnome-shell-extension-theme-selector

* Sun Apr  1 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20120401.1m)
- Obsoletes: ruby18-gnome2

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120322.1m)
- Obsoletes: hal-cups-utils

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120316.1m)
- Obsoletes: kde-plasma-ihatethecashew

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120308.1m)
- Obsoletes: kscope
- Obsoletes: kscope-devel
- kscope is no longer being maintained

* Sun Mar  4 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20120204.2m)
- Obsolete Banshee Backend
-- mono-zeroconf mono-zeroconf-devel
-- ipod-sharp ipod-sharp-devel
-- podsleuth podsleuth-devel

* Sun Mar  4 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20120204.1m)
- Obsoletes stumpwm

* Mon Feb 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120220.2m)
- Obsoletes hal-filesystem

* Mon Feb 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120220.1m)
- Obsoletes telepathy-stream-engine

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120219.1m)
- Obsoletes hal hal-devel hal-libs hal-docs
-           hal-info hal-storage-addon   

* Wed Feb  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120201.1m)
- Obsoletes: php-pecl-apm

* Thu Jan 26 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20120126.1m)
- remove monster-masher

* Tue Jan 24 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (8-0.20120124.1m)
- remove Obsoletes: kgrubeditor

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0-20120123.1m)
- remove Obsoletes: cpuspeed
- remove Obsoletes: kerneloops
- remove Obsoletes: pypy
- remove Obsoletes: pypy-devel
- remove Obsoletes: pypy-libs

* Sat Jan 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120121.1m)
- remove Obsoletes: kdevelop-php
- remove Obsoletes: kdevelop-php-docs

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20120110.1m)
- Obsoletes: kdevelop-php
- Obsoletes: kdevelop-php-docs

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.0.20111229.1m)
- Obsoletes: pyxf86config

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20111222.1m)
- Obsoletes: jaxen-bootstrap
- Obsoletes: ossec-hids
- Obsoletes: ossec-hids-client
- Obsoletes: ossec-hids-server
- Obsoletes: php-eaccelerator
- Obsoletes: php-pecl-apc
- Obsoletes: php-pecl-apc-devel

* Tue Dec 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20111220.1m)
- Obsoletes: emacs-emaxima

* Mon Dec 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20111219.2m)
- Obsoletes: cmucl-extras

* Mon Dec 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8-0.20111219.1m)
- Obsoletes: cmucl

* Sat Dec 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20111217.1m)
- Obsoletes: gnash and its subpackages

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8-0.20111129.1m)
- Obsoletes: ocaml-pa-do
- Obsoletes: ocaml-pa-do-devel

* Sun Nov 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20111119.1m)
- Obsoletes: OpenGTL

* Mon Nov 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20111114.1m)
- Orphan: CodeAnalyst-gui battfink esc gonzui

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20110926.1m)
- Obsoletes: nautilus-image-converter

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20110917.1m)
- Obsoletes: gluezilla

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (8-0.20110916.2m)
- Obsoletes: gimmie

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20110916.1m)
- set Obsoletes: gnome-device-manager-libs

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20110915.2m)
- Obsoletes gnome-device-manager

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20110915.1m)
- Obsoletes ctapi-cyberjack

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8-0.20110908.1m)
- version 8
- update %%description

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7-0.20110516.1m)
- Obsoletes: gecko-mediaplayer
- Obsoletes: mplayerplug-in

* Tue Apr 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20110426.2m)
- Obsoletes: guake

* Tue Apr 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20110426.1m)
- Obsoletes: man-pages-uk

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7-0.20110409.2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0.20110409.1m)
- Obsoletes: qtwitter

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0.20110407.1m)
- Obsoletes: pyclutter-gst pyclutter-gtk

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0.20110321.1m)
- Obsoletes: cardinal

* Sat Feb 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20110226.1m)
- Obsoletes: galeon

* Wed Feb 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20110223.1m)
- Obsoletes: pino

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7-0.20101019.2m)
- rebuild for new GCC 4.5

* Sat Oct 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7-0.20101030.1m)
- Obsoletes: gtktwitter twitux

* Tue Oct 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7-0.20101019.1m)
- Obsoletes: bmpx

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20101015.1m)
- Obsoletes: ikvm

* Thu Oct 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20101014.1m)
- Obsoletes: beagle
- Obsoletes: beagle-doc
- Obsoletes: kerry
- Obsoletes: libbeagle
- Obsoletes: libbeagle-devel
- Obsoletes: libbeagle-doc
- Obsoletes: libbeagle-python

* Sun Sep 12 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7-0.20100912.1m)
- Obsoletes: logsentry

* Sat Sep 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100911.1m)
- good-bye zfs on i686

* Sun Sep  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100905.1m)
- Obsoletes: vzctl
- Obsoletes: vzctl-lib
- Obsoletes: vzquota
- please clean up kernel-openvz and kernel-openvz-PAE manually

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7-0.20100829.2m)
- full rebuild for mo7 release

* Sun Aug 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100829.1m)
- abrt* became oboslete

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100813.1m)
- set Obsoletes: following packages to complete upgrading from STABLE_6
- desktop-file-utils-0.15
- moon
- moon-mozplugin
- plain2

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100809.4m)
- Obsoletes: gphpedit

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100809.3m)
- Obsoletes: ipa-admintools
- Obsoletes: ipa-client
- Obsoletes: ipa-python
- Obsoletes: ipa-radius-admintools
- Obsoletes: ipa-radius-server
- Obsoletes: ipa-server
- Obsoletes: ipa-server-selinux
-- ipa conflicts with mod_ssl, so mark TO.Alter

* Mon Aug  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7-0.20100809.2m)
- Obsoletes: abiword

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100809.1m)
- Obsoletes: fastladder
-- fastladder does not work on rails3
- Obsoletes: publican
- Obsoletes: publican-doc
- Obsoletes: publican-momonga
-- publican is needed by release manager only

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100808.2m)
- Obsoletes: rbbr

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100808.1m)
- Obsoletes: ruby-eruby

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.0.20100807-3m)
- Obsoletes: sblim-cmpi-devel

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.0.20100807-2m)
- Obsoletes: ruby-devel-logger

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.0.20100807-1m)
- Obsoletes: spin-kickstarts

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0.20100804-1m)
- Obsoletes: dracut-modules-olpc
- Obsoletes: bitfrost
- above 2 packages are very harmful for non OLPC

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100731.1m)
- set Obsoletes: vtg and Obsoletes: vtg-devel for the moment

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100723.3m)
- Obsoletes: umb-scheme

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7-0.20100723.2m)
- Obsoletes: eureka
- Obsoletes: foxkit

* Fri Jul 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100723.1m)
- Obsoletes: smolt
- Obsoletes: smolt-gui
- Obsoletes: smolt-server

* Mon Jul 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7-0.20100719.1m)
- Obsoletes: hwbrowser
- Obsoletes: kudzu
- Obsoletes: kudzu-devel

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7-0.20100714.1m)
- Obsoletes: gfs-utils

* Mon Jul 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100712.1m)
- Obsoletes: qtparted

* Sun Jul 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7-0.20100711-1m)
- Obsoletes: libcxp
- Obsoletes: libcxp-devel
- Obsoletes: cxplorer

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-0.20100615.2m)
- revive pdftk, xorg-x11-drv-elographics, xorg-x11-drv-penmount

* Tue Jun 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100615.1m)
- Obsoletes: fonts-arabic
- Obsoletes: grass-i18n

* Sun Jun 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100613.1m)
- Obsoletes: elisp-mixi
- Obsoletes: mixi-emacs
- Obsoletes: mixi-xemacs
- Obsoletes: elisp-morq
- remove Obsoletes: ghostscript-cups

* Fri May 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100514.1m)
- Obsoletes: xprint
- Obsoletes: xprint-common

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7-0.20100512.2m)
- Obsoletes: booty
- Obsoletes: system-config-soundcard
- Obsoletes: system-config-cluster

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7-0.20100512.1m)
- Obsoletes: rhpl
- Obsoletes: rhpxl

* Mon May 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100510.1m)
- Obsoletes: moonshine
- Obsoletes: moonshine-mozplugin

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7-0.20100509.1m)
- remove Obsoletes: gnome-scan
- remove Obsoletes: gnome-scan-libs
- remove Obsoletes: gnome-scan-devel

* Mon May  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100503.1m)
- Obsoletes: gnopernicus
- Obsoletes: gnopernicus-devel
- Obsoletes: libcm
- Obsoletes: libcm-devel

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100426.1m)
- Obsoletes: youtranslate
- update %%description

* Mon Apr  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100405.1m)
- Obsoletes: krbafs
- Obsoletes: krbafs-devel
- Obsoletes: krbafs-utils
- Obsoletes: qca-tls

* Thu Mar 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100318.1m)
- Obsoletes: easypg-xemacs
- Obsoletes: elisp-easypg

* Mon Feb  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20100208.1m)
- Obsoletes: HelixPlayer

* Mon Dec 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7-0.20091228.1m)
- Obsoletes: heap-buddy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-0.20091109.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Nov  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20091109.1m)
- Obsoletes: chkfontpath-momonga
- temporarily obsoletes following packages
  DO NOT FORGET REMOVING FOLLOWING PACKAGES FROM THIS SPEC
 - gnome-scan
 - gnome-scan-devel
 - gnome-scan-libs
 - xorg-x11-drv-diamondtouch
 - xorg-x11-drv-elographics
 - xorg-x11-drv-fpit
 - xorg-x11-drv-hyperpen
 - xorg-x11-drv-mutouch
 - xorg-x11-drv-penmount

* Tue Oct 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20091020.1m)
- Obsoletes: pump and pump-devel

* Thu Jul 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20090716.1m)
- Obsoletes: fonts-chinese
- Obsoletes: gnome-system-tools
- Obsoletes: libtorrent
- Obsoletes: ocaml-bitstring-c
- Obsoletes: system-tray-applet

* Thu Jul  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20090702.1m)
- Obsoletes: revisor

* Wed Jul  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20090701.2m)
- Obsoletes: gcl

* Wed Jul  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20090701.1m)
- Obsoletes: libzvt

* Wed Jun 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20090610.1m)
- Obsoletes: cairo-dock
- Obsoletes: roundcubemail

* Wed May 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6-0.20090527.1m)
- Obsoletes: elisp-Mule-UCS

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-0.20081103.2m)
- rebuild against rpm-4.6

* Mon Nov  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (6-0.20081103.1m)
- Obsoletes: skencil

* Sun Jun  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080608.1m)
- Obsoletes: eclipse-cvs-client-sdk
- Obsoletes: eclipse-jdt-sdk
- Obsoletes: eclipse-pde-sdk
- Obsoletes: eclipse-platform-sdk
- Obsoletes: eclipse-rcp-sdk
- Obsoletes: firefox-devel
- Obsoletes: lucene-devel
- Obsoletes: tetex-extramacros-jsclasses
- Obsoletes: tetex-extramacros-otf
- Obsoletes: tetex-extramacros-prosper
- Obsoletes: tetex-extramacros-utf
- Obsoletes: selinux-policy-strict
- Obsoletes: subversion-python

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080425.1m)
- good-bye plone and zope
- hi, amarok-libvisual, amarok-yauap and koffice-kugar

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080414.1m)
- good-bye cogito

* Sun Apr  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080406.1m)
- good-bye pdftk

* Sat Apr  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5-0.20080405.1m)
- welcome abiword gnumeric

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5-0.20080402.2m)
- rebuild against gcc43

* Wed Apr  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5-0.20080402.1m)
- good-bye abiword gnumeric

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080324.1m)
- good-bye boo and nant

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080307.1m)
- add Obsoletes: pulseaudio-devel

* Mon Mar  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5-0.20080303.1m)
- initial package for Momonga Linux 5
- do not set BuildArch: noarch
