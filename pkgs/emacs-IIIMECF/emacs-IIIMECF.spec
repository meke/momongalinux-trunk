%global momorel 19
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Internet/Intranet Input Method Emacs Client Framework
Name: emacs-IIIMECF
Version: 0.75
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://www.meadowy.org/~kawabata/iiimecf/
Source: http://www.meadowy.org/~kawabata/iiimecf/IIIMECF-%{version}.tar.gz
Nosource: 0
#BuildArch: noarch   ## got this out of noarch because of /usr/bin/udclient
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: IIIMECF-emacs
Obsoletes: IIIMECF-xemacs

Obsoletes: elisp-IIIMECF
Provides: elisp-IIIMECF

%description
  IIIMECF conforms IIIM Protocol Specification defined by Sun Microsystems,
and consists of a library to create IIIM awarded applications and a simple
input method library controlled by IIIM server(s).
(This input-method is denoted by "iiimfcf-sc" hereafter.)

  install IIIMECF-emacs if you want to use such protocol on emacs.

  Especially,iiim-sc can work with ATOK/X for linux.

%prep
%setup -q -n iiimecf
rm -rf %{buildroot}

%build
## make udclient
pushd util/
make
mkdir -p %{buildroot}/%{_bindir}
install -m 755 udclient %{buildroot}/%{_bindir}
popd

%install
mkdir -p %{buildroot}%{e_sitedir}/IIIMECF
emacs -q --no-site-file -batch -l iiimcf-comp.el
install -m 644 lisp/*.el* %{buildroot}%{e_sitedir}/IIIMECF/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README README.ja lisp
%{_bindir}/udclient
%{e_sitedir}/IIIMECF

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-19m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-18m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.75-17m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-16m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.75-15m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.75-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.75-13m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-12m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-11m)
- merge IIIMECF-emacs into elisp-IIIMECF

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.75-9m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.75-8m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-7m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-6m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-5m)
- good-bye IIIMECF-xemacs
  Mule-UCS-xemacs does not work on new xemacs

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-4m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-3m)
- rebuild against emacs-23.0.92
- remove dependencies on Mule-UCS-emacs

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-2m)
- rebuild against rpm-4.6

* Wed Nov 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.75-1m)
- update

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-19m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-18m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-17m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-16m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-15m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-14m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-13m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-12m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-11m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-10m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-9m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-8m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-7m)
- rebuild against emacs-22.0.90

* Sun May  8 2005 Shotaro Kamio <skamio@momonga-linux.org>
- (0.6-6m)
- add the patch iiimecf-0.6_0.7.diff for handling unix domain socket.
- got this out of noarch because of /usr/bin/udclient.

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6-5m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-4m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6-3m)
- rebuild against emacs-21.3.50

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-2m)
- rebuild against emacs-21.3
- define muleucsver

* Wed Jul 17 2002 smbd <smbd@momonga-linux.org>
- (0.6-1m)
- up to 0.6

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5-6k)
- rebuild against emacs-21.2

* Fri Nov 02 2001 Toshiro HIKITA <toshi@sodan.org>
- (0.5-4k)
- add iiimecf-cvs.patch from im-sdk

* Thu Nov 01 2001 Toshiro HIKITA <toshi@sodan.org>
- (0.5-2k)
- update to 0.5

* Wed Oct 31 2001 Toshiro HIKITA <toshi@sodan.org>
- (0.4-8k)
- add iiimecf-cvs.patch from im-sdk

* Mon Oct 29 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (elisp-IIIMECF-0.4-6k)
- modified %BuildPrereq (added xemacs-sumo)

* Wed Oct 24 2001 Hidetomo Machi <mcHT@kondara.org>
- (elisp-IIIMECF-0.4-4k)

* Sun Oct 14 2001 Masaru Sato <masachan@kondara.org>
- Modify BuildRoot

* Tue Jul 17 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.84-2k)
- import to 2.0

* Fri Jul 13 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.4-3k)
- version 0.4
- not use %preun and %triggerin

* Mon Dec 18 2000 KIM Hyeong Cheol <kim@kondara.org>
- (0.3-4k)
- deleted serial

* Sun Nov 12 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.3-1k)
- Update to Ver. 0.2
- adopt to stable release

* Fri Nov 10 2000 KIM Hyeong Cheol <kim@kondara.org>
- (0.3pre.4-1k)
- removed patches(minor-mode.patch, terminal.patch)

* Tue Nov 07 2000 KIM Hyeong Cheol <kim@kondara.org>
- (0.3pre.3-5k)
- apply terminal.patch
- add %Serial, %triggerin
- removed BuildPreReq

* Sun Nov 05 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.3pre.3-3k)
- apply minor-mode.patch

* Sat Nov 04 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.3pre.3-1k)
- Update New IIIMECF-current

* Sat Oct 21 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.3pre.2-1k)
- Update New IIIMECF-current

* Fri Oct 20 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.3pre-1k)
- Update to IIIMECF-current

* Mon Sep 18 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.2-1k)
- Update to Ver. 0.2

* Wed Sep 13 2000 Toshiro HIKITA <toshi@sodan.org>
- (0.1-3k)
- Add iiimcf-sc.el ad-hoc.patch

* Sun Sep 10 2000 KIM Hyeong Cheol <hkimu-tky@umin.ac.jp>
- (0.1-1k)
- First Release
