%global momorel 5
%global pyver 2.7

Summary: freedesktop.org standards in python.
Name: pyxdg
Version: 0.19
Release: %{momorel}m%{?dist}

License: GPL
Group: Documentation
URL: http://www.freedesktop.org/wiki/Software/pyxdg
Source0: http://www.freedesktop.org/~lanius/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python
BuildArchitectures: noarch

%description
freedesktop.org standards in python.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf --preserve-root %{buildroot}
python setup.py install \
    --skip-build \
    --root=%{buildroot} \
    --prefix=%{_prefix}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING ChangeLog README TODO
/usr/lib*/python%{pyver}/site-packages/%{name}-%{version}-py*.egg-info
/usr/lib*/python%{pyver}/site-packages/xdg

%changelog
* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.19-2m)
- full rebuild for mo7 release

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.15-3m)
- rebuild against python-2.6.1-1m

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-2m)
- noarch

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-1m)
- initial build
