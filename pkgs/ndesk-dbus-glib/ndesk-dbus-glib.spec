%global momorel 12
%define monoprefix %_prefix/lib
%define dbus_version 0.90
%define dbus_glib_version 0.71

Summary: C# implementation of D-Bus-glib
Name: ndesk-dbus-glib
Version: 0.4.1
Release: %{momorel}m%{?dist}
URL: http://www.ndesk.org/DBusSharp
Source0: http://www.ndesk.org/archive/dbus-sharp/%{name}-%{version}.tar.gz
NoSource: 0
License: BSD
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: mono-core
BuildRequires: dbus-devel
BuildRequires: ndesk-dbus-devel
BuildRequires: glib2-devel

%description
ndesk dbus glib library

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q 

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

# gw wrong on x86_64
%if %_lib != lib
mkdir -p  %buildroot%_prefix/lib/
mv %buildroot%_libdir/mono %buildroot%_prefix/lib/
perl -pi -e "s^%_libdir^%_prefix/lib^" %buildroot%_libdir/pkgconfig/ndesk-dbus-glib-1.0.pc
%endif

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{monoprefix}/mono/gac/NDesk.DBus.GLib
%{monoprefix}/mono/ndesk-dbus-glib-1.0

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/ndesk-dbus-glib-1.0.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-12m)
- rebuild for mono-2.10.9

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-11m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-9m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-8m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-7m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-6m)
- separate devel package

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-4m)
- rebuild against rpm-4.6

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-3m)
- good-bye debug symbol

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-2m)
- rebuild against gcc43

* Sat Dec 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- initila build
