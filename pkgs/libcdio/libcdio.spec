%global momorel 1

# Although we have newer cdparanoia package...
%global with_cdparanoia 1

Summary: GNU Compact Disc Input and Control Library
Name: libcdio
Version: 0.90
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.gnu.org/software/libcdio/projects.html
Source0: http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}-no_date_footer.hml
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libcddb
BuildRequires: doxygen
BuildRequires: libcddb-devel
BuildRequires: ncurses-devel
BuildRequires: pkgconfig

%description
The Compact Disc Input and Control library (libcdio) contains a
library for CD-ROM and CD image access. Applications wishing to be
oblivious of the OS- and device-dependent properties of a CD-ROM or of
the specific details of various CD-image formats may benefit from
using this library.

Some support for on-disk CD-image types like CDRWIN's BIN/CUE format,
cdrdao's TOC format, and Nero's NRG format is available. Therefore,
applications that use this library also have the ability to read
on-disk CD images as though they were CDs.

A library for working with ISO-9660 filesystems (libiso9660) is
included. A generic interface for issuing MMC (multimedia commands) is
also part of the libcdio library.

%package devel
Summary: %{name} header files and development documentation
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires(post): info
Requires(preun): info

%description devel
Header files and development documentation for %{name}

%prep
%setup -q

%build
%configure \
%if ! %{with_cdparanoia}
	--without-cd-paranoia
%endif
%make

pushd doc/doxygen
sed -i -e "s,HTML_FOOTER.*$,HTML_FOOTER = libcdio-no_date_footer.hml,g" Doxyfile
cp %{SOURCE1} .
./run_doxygen
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

%if ! %{with_cdparanoia}
rm -f %{buildroot}%{_libdir}/pkgconfig/libcdio_paranoia.pc
%endif

# convert Japanese manual page from EUC-JP to UTF-8
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

# remove libtool la
rm -f %{buildroot}%{_libdir}/*.la

# remove info/dir
rm -f %{buildroot}%{_infodir}/dir

rm -rf examples
mkdir -p examples/C++
cp -a example/{*.c,README} examples
cp -a example/C++/{*.cpp,README} examples/C++

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post 
/sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir 2>/dev/null || :

%preun devel
if [ "$1" = 0 ] ;then
  /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir 2>/dev/null || :
fi

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README* THANKS TODO
%{_bindir}/*
%{_libdir}/lib*.so.*
%if %{with_cdparanoia}
%{_mandir}/man1/*.1*
%endif

%files devel
%defattr(-, root, root)
%doc doc/doxygen/html examples
%{_includedir}/cdio*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_infodir}/libcdio.info*

%changelog
* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.82-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.82-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.82-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.81-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.81-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.81-2m)
- fix freezing bug, which occurs when there is no /dev/cdrom and so on.

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.80-2m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.80-1m)
- version 0.80
- remove merged gcc43.patch
- build documentations like Fedora
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.78.2-5m)
- rebuild against gcc43

* Wed Jan  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.78.2-4m)
- added patch for gcc43

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.78.2-3m)
- convert ja.man from EUC-JP to UTF-8
- enable parallel build

* Sat Jan 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.78.2-2m)
- fix conflict files (/usr/share/info/dir)

* Wed Dec 06 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.78.2-1m)
- initial packaging for Momonga
