%global momorel 1

Summary: A library for reading DVD-Video images.
Name: libdvdread
Version: 4.2.0
Release: %{momorel}m%{?dist}
URL: http://www.mplayerhq.hu/
License: GPLv2+
Group: System Environment/Libraries
Source0:http://dvdnav.mplayerhq.hu/releases/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig

%description
libdvdread provides a simple foundation for reading DVD-Video images.

%package  devel
Summary: libdvdread development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
livdvdread-devel includes development files for libdvdread

%prep
%setup -q

%build
./configure2 \
	--disable-opts \
	--extra-cflags="%{optflags}" \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--shlibdir=%{_libdir} \
	--enable-shared \
	--disable-strip

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog DEVELOPMENT-POLICY.txt
%doc INSTALL NEWS README TODO 
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/dvdread-config
%{_includedir}/dvdread
%{_libdir}/pkgconfig/dvdread.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.0-1m)
- update 4.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.3-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.3-1m)
- back to the trunk, version 4.1.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- %%NoSource -> NoSource

* Tue Mar 20 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.6-2m)
- delete libtool library

* Fri May 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6
- import libdvdread-0.9.6.automake.patch from Mandriva

* Wed Oct  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- import libdvdread-0.9.4.automake.patch.bz2 from cooker
 +* Tue Nov 30 2004 Guillaume Rousse <guillomovitch@mandrake.org> 0.9.4-5mdk 
 +- patched makefile to export UDF* symbols too
- use %%make

* Tue Feb 25 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.4-1m)
  update to 0.9.4

* Fri Jun 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.3-2k)
  update to 0.9.3

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.9.2-6k)

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (0.9.2-4k)
- mu-, alpha...

* Wed Mar  5 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.9.2-2k)
- import

* Fri Jan 18 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.9.2-3k)
- kondarize

* Thu Oct 11 2001 Martin Norbdck <d95mback@dtek.chalmers.se>
- Updated to version 0.9.2

* Tue Sep 25 2001 Martin Norbdck <d95mback@dtek.chalmers.se>
- Added small patch to fix the ldopen of libdvdcss

* Tue Sep 18 2001 Martin Norbdck <d95mback@dtek.chalmers.se>
- Updated to version 0.9.1

* Fri Sep 14 2001 Martin Norbdck <d95mback@dtek.chalmers.se>
- Split into normal and devel package

* Thu Sep 6 2001 Martin Norbdck <d95mback@dtek.chalmers.se>
- Updated to version 0.9.0

* Tue Jul 03 2001 Martin Norbdck <d95mback@dtek.chalmers.se>
- initial version
