%global momorel 5
%global srcname ctl

Summary: The Color Transformation Language
Name: CTL
Version: 1.4.1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://ampasctl.sourceforge.net/
Source0: http://dl.sourceforge.net/ampasctl/%{srcname}-%{version}.tar.gz
NoSource: 0
Patch0: %{srcname}-%{version}-gcc43.patch
Patch1: %{srcname}-%{version}-fix_pkgconfig.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: pkgconfig
BuildRequires: sed

%description
The Color Transformation Language, or CTL, is a programming language
for digital color management.

%package devel
Summary: Headers for developing programs that will use CTL
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ilmbase-devel >= 1.0.3
Requires: pkgconfig

%description devel
This package contains the static libraries and header files needed for
developing applications with CTL.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .gcc43
%patch1 -p1 -b .pkgconfig

# fix up permissions
chmod 644 IlmCtl/{CtlInterpreter.cpp,CtlParser.cpp}
chmod 644 doc/CtlManual.doc

%build
%configure

# remove rpath from libtool
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# clean unused-direct-shlib-dependencies
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL LICENSE NEWS README*
%doc doc/CtlManual.doc doc/CtlManual.pdf
%{_libdir}/libIlmCtl.so.*
%{_libdir}/libIlmCtlMath.so.*
%{_libdir}/libIlmCtlSimd.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/libIlmCtl.a
%{_libdir}/libIlmCtl.so
%{_libdir}/libIlmCtlMath.a
%{_libdir}/libIlmCtlMath.so
%{_libdir}/libIlmCtlSimd.a
%{_libdir}/libIlmCtlSimd.so

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-5m)
- rebuild against ilmbase-1.0.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- initial package for OpenEXR_CTL
- import 2 patches from Fedora
