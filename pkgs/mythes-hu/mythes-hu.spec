%global momorel 5

Name: mythes-hu
Summary: Hungarian thesaurus
%define upstreamid 20100213
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://extensions.services.openoffice.org/e-files/1283/6/dict-hu.oxt
Group: Applications/Text
URL: http://extensions.services.openoffice.org/project/hu_dicts
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

%description
Hungarian thesaurus.

%prep
%setup -q -c

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mythes
cp -p th_hu_HU_v2.* $RPM_BUILD_ROOT/%{_datadir}/mythes

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_th_hu_HU_v2.txt
%{_datadir}/mythes/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100213-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100213-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100213-3m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20100213-2m)
- remove dups

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20100213-1m)
- import from Fedora 13

* Sun Feb 14 2010 Caolan McNamara <caolanm@redhat.com> - 0.20100213-1
- latest version

* Fri Sep 18 2009 Caolan McNamara <caolanm@redhat.com> - 0.20090918-1
- latest version

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.20090203-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 14 2009 Caolan McNamara <caolanm@redhat.com> - 0.20090203-1
- initial version
