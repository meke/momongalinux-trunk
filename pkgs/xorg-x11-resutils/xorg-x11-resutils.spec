%global momorel 10
%define pkgname resutils

Summary: X.Org X11 X resource utilities
Name: xorg-x11-%{pkgname}
Version: 1.0.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xorg-x11-appres
Requires: xorg-x11-editres
Requires: xorg-x11-listres
Requires: xorg-x11-viewres

%description
A collection of utilities for managing X resources.

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-5m)
- rebuild against gcc43

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-4m)
- separate package

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- delete duplicated files

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Update all resource utils to version 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update all resource utils to version 1.0.0 from X11R7 RC4.
- Move app-defaults files to _datadir
- Change manpage dir from man1x to man1 to match RC4 default.
- Added "BuildRequires: libX11-devel, libXt-devel"

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.1-3
- require newer filesystem package (#172610)

* Sun Nov 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Add "Obsoletes: XFree86, XFree86-tools, xorg-x11, xorg-x11-tools", as 
  appres, editres used to be in the XFree86/xorg-x11 package, whereas
  oddly enough, the listres, viewres utilities were in the *-tools subpackage.
- Rebuild against new libXaw 0.99.2-2, which has fixed DT_SONAME. (#173027)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Initial build, with all apps taken from X11R7 RC2
- Use "make install DESTDIR=$RPM_BUILD_ROOT" as the makeinstall macro fails on
  some packages.
