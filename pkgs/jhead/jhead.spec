%global momorel 4

Summary: Exif Jpeg header and thumbnail manipulator
Name:    jhead
Version: 2.90
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: Public Domain
URL:     http://www.sentex.net/~mwandel/jhead/
Source0: http://www.sentex.net/~mwandel/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRequires: gzip
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Jhead is a command line driven utility for extracting digital camera
settings from the Exif format files used by many digital cameras. It
handles the various confusing ways these can be expressed, and
displays them as F-stop, shutter speed, etc. It is also able to reduce
the size of digital camera JPEGs without loss of information, by
deleting integral thumbnails that digital cameras put into the Exif
header. If you need to add Exif support to a program, this is a simple
program to cut and paste from. Many projects, including PHP, have
reused code from this utility.

%prep
%setup -q

%build
make CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 jhead %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 jhead.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc readme.txt usage.html changes.txt
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.90-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.90-1m)
- update to 2.90

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.86-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Feb 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.86-1m)
- update to 2.86

* Sun Feb  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.85-0.2.1m)
- update to head revision

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.85-0.1.2m)
- rebuild against rpm-4.6

* Sun Dec 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.85-0.1.1m)
- [SECURITY] CVE-2008-4640 CVE-2008-4641
- update to head revision

* Fri Oct 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.84-1m)
- [SECURITY] CVE-2008-4575 CVE-2008-4639
- update to 2.84

* Wed Jul 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.82-1m)
- update to 2.82

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-2m)
- %%NoSource -> NoSource

* Tue Jan 16 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.7-1m)
- first packaging for Momonga

* Sat Dec 02 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6-1m)
- build for Momonga
