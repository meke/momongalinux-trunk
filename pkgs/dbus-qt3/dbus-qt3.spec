%global momorel 9
%global prever 0
%global date 20080623
%global svndate 23.06.08
%global qtdir %{_libdir}/qt3
%global srcname libdbus-1-qt3

Summary: Qt3 DBus Bindings
Name: dbus-qt3
Version: 0.9.0
Release: %{prever}.%{date}.%{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: http://www.freedesktop.org/wiki/Software/DBusBindings
Source0: %{srcname}-%{svndate}.tar.bz2
Patch0: %{srcname}-%{svndate}-automake111.patch
Patch1: %{srcname}-%{svndate}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: dbus-devel >= 0.92
BuildRequires: libtool
BuildRequires: pkgconfig
BuildRequires: qt3-devel

%description
This library provides Qt3-classes for accessing the DBus.

%package devel
Summary: Development files for dbus-qt3
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-devel
Requires: pkgconfig
Requires: qt-devel

%description devel
Development files for dbus-qt3.

%prep
%setup -q -n %{srcname}-%{svndate}

%patch0 -p1 -b .automake111
%patch1 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir}

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--enable-qt3 \
	--enable-new-ldflags \
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of la file
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog INSTALL README TODO
%{_libdir}/%{srcname}.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/dbusxml2qt3
%{_includedir}/dbus-1.0/qt3
%{_libdir}/pkgconfig/dbus-1-qt3.pc
%{_libdir}/%{srcname}.a
%{_libdir}/%{srcname}.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-0.20080623.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-0.20080623.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-0.20080623.7m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.20080623.6m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-0.20080623.5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-0.20080623.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.20080623.3m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-0.20080623.2m)
- rebuild against rpm-4.6

* Mon Jun 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.20080623.1m)
- update to 20080623 svn snapshot for knetworkmanager-0.7
- remove merged makefile.patch

* Fri May 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-4.20080530.1m)
- update to 20080530 svn snapshot for knetworkmanager-0.7

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- initial package for kneworkmanager-0.7
- import fix_ifdef.patch from Fedora
