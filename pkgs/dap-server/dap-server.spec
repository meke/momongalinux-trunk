%global momorel 2

%define dap_cachedir     /var/cache/dap-server
%define dap_webconfdir   %{_sysconfdir}/httpd/conf.d
%define dap_cgidir       %{_datadir}/opendap-cgi
%define dap_cgiconfdir   %{_sysconfdir}/opendap/
%define __perl_provides %{nil}
%define __perl_requires %{nil}

Summary:         Basic request handling for OPeNDAP servers 
Name:            dap-server
Version:         4.1.0
Release:         %{momorel}m%{?dist}
License:         LGPL
Group:           System Environment/Daemons 
Source0:         http://www.opendap.org/pub/source/%{name}-%{version}.tar.gz
NoSource:        0
URL:             http://www.opendap.org/
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:   bes-devel >= 3.9.0
BuildRequires:   curl
BuildRequires:   libdap-devel >= 3.11.0
BuildRequires:   openldap-devel >= 2.4.8
# we use httpd and not webserver because we make use of the apache user. 
# not sure if it is right.
Requires:        bes
Requires:        curl
Requires:        httpd
Requires:        perl 
Requires:        perl-HTML-Parser
Requires:        perl-libwww-perl
Obsoletes:       %{name}-cgi < %{version}-%{release}
Provides:        %{name}-cgi = %{version}-%{release}

%description
This is base software for the OPeNDAP (Open-source Project for a Network 
Data Access Protocol) server. Written using the DAP++ C++ library and Perl, 
this handles processing compressed files and arranging for the correct 
server module to process the file. The base software also provides support 
for the ASCII response and HTML data-request form. Use this in combination 
with one or more of the format-specific handlers.

This package contains all the executable and perl modules. The scripts 
and config files that should be installed in a cgi directory are in the 
documentation directory with -sample appended. If you want a package that 
is usable without manual configuration, install %{name}-cgi.

%prep 
%setup -q

%build
%configure --with-cgidir=%{dap_cgidir} --disable-dependency-tracking \
 --with-cgiconfdir=%{dap_cgiconfdir} --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install INSTALL="%{__install} -p"

find %{buildroot} -name '*.la' -exec rm -f {} \;

%check
make check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING COPYRIGHT_URI NEWS README
%config(noreplace) %{_sysconfdir}/bes/modules/dap-server.conf
%{_datadir}/bes/*
%{_libdir}/bes/libascii_module.so
%{_libdir}/bes/libusage_module.so
%{_libdir}/bes/libwww_module.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.0-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- rebuild against bes-3.9.0, libdap-3.11.0
- update to 4.1.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- rebuild against libdap-3.10.2 and bes-3.8.4
- update to 4.0.0

* Fri Apr 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.8.5-5m)
- add patch0 for new perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.5-3m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.5-2m)
- rebuild against libdap-3.8.2
- rebuild against bes-3.6.2

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.5-1m)
- update to 3.8.5

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.4-7m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.4-6m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7.4-5m)
- rebuild against openldap-2.4.8

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.7.4-4m)
- mv opendap_apache.conf to opendap_apache.conf.dist

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.4-3m)
- rebuild for new libdap

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.7.4-2m)
- rebuild against perl-5.10.0-1m

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.7.4-1m)
- import from Fedora

* Sat May  5 2007  Patrice Dumas <pertusus@free.fr> 3.7.4-3
- update patch, perl(CGI) is not needed

* Mon Apr 30 2007  Patrice Dumas <pertusus@free.fr> 3.7.4-2
- update to 3.7.4
- fix security issue
- remove config files upstreamed patch

* Tue Oct 31 2006 Patrice Dumas <pertusus@free.fr> 3.7.1-4
- rebuild for new libcurl soname (indirect dependency through libdap)

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 3.7.1-3
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Sep 26 2006 Patrice Dumas <pertusus@free.fr> 3.7.1-2
- install cgi files according to the FHS, owned by root

* Wed Sep  6 2006 Patrice Dumas <pertusus@free.fr> 3.7.1-1
- update to 3.7.1
- remove patch not needed anymore to link against libdap >= 3.7.0

* Wed Sep  6 2006 Patrice Dumas <pertusus@free.fr> 3.6.1-4
- rebuild for FC-6

* Tue Jul 25 2006 Patrice Dumas <pertusus@free.fr> 3.6.1-3
- dirty patch to link against libdapclient and libdapserver for libdap 3.7.0

* Sun Jun 18 2006 Patrice Dumas <pertusus@free.fr> 3.6.1-2
- update to 3.6.1
- adjust for newer jgofs handler even though it isn't packaged

* Fri Mar  3 2006 Patrice Dumas <pertusus@free.fr> 3.6.0-2
- new release

* Wed Feb 22 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 3.5.3-1.2
- Small fix for Perl provides

* Thu Feb  2 2006 Patrice Dumas <pertusus@free.fr> 3.5.3-1
- add a cgi subpackage

* Fri Sep  2 2005 Patrice Dumas <pertusus@free.fr> 3.5.1-1
- initial release
