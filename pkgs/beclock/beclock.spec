%global         momorel 1
%global         kdever 4.8.1
%global         qtver 4.8.0
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         content_no 117542
%global         src_name beclock-kwin-fx

Name:           beclock
Version:        0.18
Release:        %{momorel}m%{?dist}
Summary:        A simple clock, implemented as KWin Effect
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://www.kde-look.org/content/show.php?content=117542
Source0:        http://www.kde-apps.org/CONTENT/content-files/%{content_no}-%{src_name}-%{version}.txz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake >= %{cmakever}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}

%description
A simple clock, implemented as KWin Effect

- This is NO plasmoid!
- You need active desktop FX to use this clock
- The idea is somewhat experimental :-)

%prep
%setup -q -n %{src_name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/*.so
%{_kde4_datadir}/kde4/services/kwin/*.desktop

%changelog
* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13-2m)
- rebuild for new GCC 4.6

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
-update to 0.13a

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-5m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against qt-4.6.3-1m

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sun Jun  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Sun Jan 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- update to 0.4

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- update to 0.3

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-1m)
- initial build for Momonga Linux
