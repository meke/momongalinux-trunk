%global momorel 1
%global tarname libgdamm

Summary: C++ wrappers for libgda
Name: libgdamm4
Version: 4.1.3
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Databases
URL: http://www.gnome-db.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{tarname}/4.1/%{tarname}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glibmm-devel >= 2.16.0
BuildRequires: libgda4-devel >= 3.99.13
BuildRequires: perl

%description
C++ wrappers for libgda

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibmm-devel
Requires: libgda3-devel

%description devel
%{name}-devel

%prep
%setup -q -n %{tarname}-%{version}

%ifarch x86_64
find . -name "Makefile.*" | xargs perl -p -i -e "s|/lib/libgda|/lib64/libgda|"
%endif

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.examples ChangeLog NEWS README TODO
%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgdamm-?.0
%{_datadir}/doc/libgdamm-?.0
%{_includedir}/libgdamm-?.0
%{_datadir}/devhelp/books/libgdamm-4.0/libgdamm-4.0.devhelp2

%changelog
* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1.3-1m)
- update 4.1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.99.16-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.99.16-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.99.16-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.99.16-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.99.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.16-1m)
- update to 3.99.16

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.15-1m)
- update to 3.99.15

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.13-1m)
- update to 3.99.13

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.12-1m)
- update to 3.99.12

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.11-1m)
- update to 3.99.11
- delete patch0

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.8-2m)
- rename libgdamm to libgdamm4

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.99.8-1m)
- update to 3.99.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- initial build

