%global momorel 23
%global devel 0
%global develver beta0

Summary: Network Dictionary Transport Protocol daemon
Name: ndtpd
Version: 3.1.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons

%if %{devel}
Source0: ftp://ftp.sra.co.jp/pub/net/ndtp/%{name}/beta/%{name}-%{version}%{develver}.tar.gz
%else
Source0: ftp://ftp.sra.co.jp/pub/net/ndtp/%{name}/%{name}-%{version}.tar.gz
%endif
NoSource: 0
Source1: %{name}-init
Patch0: %{name}-%{version}-eb4.patch
Patch1: ftp://ftp.sra.co.jp/pub/net/ndtp/%{name}/%{name}-%{version}+.diff
Patch2: %{name}-%{version}-canonicalize.patch
URL: http://www.sra.co.jp/people/m-kasahr/ndtpd/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: zlib >= 1.1.3
Requires(post): chkconfig info
Requires(preun): chkconfig info
BuildRequires: eb-devel >= 4.4.3

%description
NDTPD is a server for accessing CD-ROM books with NDTP (Network Dictionary
Transfer Protocol) on TCP. You can substitute NDTPD for DSERVR. 

%prep
rm -rf %{buildroot}

%if %{devel}
%setup -q -n %{name}-%{version}%{develver}
%else
%setup -q
%endif
%patch0 -p1
%patch1 -p0 -b .buffer-overrun~
%patch2 -p1 -b .canonicalize

%build
aclocal-1.7 -I m4
autoconf
automake-1.7
%configure
%make

%install
%makeinstall

install -d %{buildroot}%{_initscriptdir}
install -m744 %{SOURCE1} %{buildroot}%{_initscriptdir}/ndtpd

rm -f %{buildroot}/etc/ndtpd.conf.sample
rm -f %{buildroot}%{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/chkconfig --del ndtpd
    /sbin/install-info --delete %{_infodir}/ndtpd.info %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/ndtpd-ja.info %{_infodir}/dir
fi

%post
/sbin/chkconfig --add ndtpd
[ -f /var/lock/subsys/ndtpd ] && %{_initscriptdir}/ndtpd restart
if [ "$1" = 1 ]; then
    /sbin/install-info %{_infodir}/ndtpd.info %{_infodir}/dir
    /sbin/install-info %{_infodir}/ndtpd-ja.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog* NEWS README* UPGRADE* ndtpd.conf.sample
%dir %attr(0755,nobody,nobody) %{_localstatedir}/ndtpd
%config %{_initscriptdir}/ndtpd
%{_sbindir}/*
%{_libexecdir}/ndtpstat
%{_infodir}/ndtpd*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.5-23m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.5-22m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.5-21m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-20m)
- rebuild against eb-4.4.3

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-19m)
- rebuild against eb-4.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-17m)
- rebuild against eb-4.4.1-3m

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-16m)
- rebuild against eb-4.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-15m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.5-14m)
- import canonicalize.patch from gentoo (to enable build)
 +- 21 Jun 2008; Ryan Hill <dirtyepic@gentoo.org>
 +- +files/ndtpd-3.1.5-canonicalize.patch, ndtpd-3.1.5.ebuild:
 +- Use autotools.eclass instead of calling autoconf directly. Fix conflicting
 +- types build error for canonicalize_file_name. Clean up ebuild.

* Mon Jul 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-13m)
- apply ndtpd-3.1.5+.diff

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.5-12m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-11m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.5-10m)
- rebuild against eb-4.3.1
- License: GPLv2

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-9m)
- rebuild against eb-4.2

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-8m)
- rebuild against eb-4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-7m)
- rebuild against eb-4.1.2

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.5-6m)
- stop daemon

* Sat Jun 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-5m)
- rebuild against eb-4.1

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (3.1.5-4m)
- revised spec for rpm 4.2.

* Thu Jan 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-3m)
- revise for eb-4.0

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-2m)
- rebuild agianst eb-4.0

* Sun May 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.5-1m)

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.4-3m)
- rebuild against eb-3.3.2

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.4-2m)
- rebuild against eb-3.3.1

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.4-1m)
- minor bugfixes

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.3-4m)
- rebuild against eb-3.3

* Mon Jul 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-3m)
- change Require -> PreReq for chkconfig
- add PreReq: info
- Require: eb >= 3.2.2-2k
- BuildPreReq: eb-devel >= 3.2.2-2k

* Tue Jun 11 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.1.3-2k)

* Tue Apr 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.1.2-2k)

* Wed Feb 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.1.1-6k)
- rebuild against eb-3.2

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (3.1.1-4k)
- NoNoSource.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1.1-2k)
- update to 3.1.1

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-0.0000002k)
- 3.1beta0

* Fri Nov 30 2001 Toru Hoshina <t@kondara.org>
- (3.0.4-6k)
- comment out %chkconfig

* Tue Oct 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.4-4k)
- errased the process concerning with /etc/service

* Fri Aug 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0.2-2k)
- use %configure and %makeinstall macro
- Requires eb >= 3.1

* Wed Jul  4 2001 Toru Hoshina <t@kondara.org>
- (3.0.1-2k)

* Wed Jun  6 2001 Hidetomo Machi <mcHT@kondara.org>
- (3.0-2k)
- use macro
- modify files section

* Tue Jun  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-3k)

* Sun May 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-0.0003003k)

* Sat Apr  7 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-0.0002003k)

* Fri Feb  9 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-0.0001003k)

* Fri Jan  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0beta0-3k)
- change localstatedir to /var/lib/ndtpd

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct  9 2000 Kazuhiko <kazuhiko@kondara.org>
- (3.0alpha2-1k)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Jun 28 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (2.3.8-1k)
- version 2.3.8
- patch for compress problem is removed (original package was fixed)

* Wed May 10 2000 Shunji Tanaka <tanaka@kuee.kyoto-u.ac.jp>
- fixed initscript to check existence of /etc/ndtpd.conf when it starts.
- fixed SPEC to install info files correctly.

* Tue Apr 25 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Summary )
- fixed from [Kondara-users.ja:01470]
- fixed from [Kondara-users.ja:01483]
- ( Patch by Shunji TANAKA <tanaka@rotary.kuee.kyoto-u.ac.jp> )

* Mon Apr 17 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- version 2.3.7

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Fri Jan 21 2000 Toru Hoshina <t@kondara.org>
- ver up 2.3.6.
- depends on eb-2.3.6.

* Fri Dec 3 1999 Toru Hoshina <t@kondara.org>
- ver up 2.3.5.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Oct 9 1999 Toru Hoshina <t@kondara.org>
- ver up 2.3.3.

* Wed Jul 28 1999 Toru Hoshina<hoshina@best.com>
- fixed postun bug :-P

* Mon Jul 19 1999 Toru Hoshina<hoshina@best.com>
- ver up 2.3.2.

* Mon May 17 1999 Toru Hoshina<hoshina@best.com>
- ver up 2.2.2.

* Sun Mar 21 1999 Toru Hoshina<hoshina@best.com>
- rebuild against rawhide 1.3.0.

* Tue Feb  9 1999 Toru Hoshina <hoshina@best.com>
- version up to 2.2.

* Sat Dec 26 1998 Toru Hoshina <hoshina@best.com>
- add init program.

* Sat Nov 21 1998 Toru Hoshina <hoshina@best.com>
- re-build against RedHat 5.2 under libwcsmbs.

* Sun Oct 4 1998 Toru Hoshina <hoshina@best.com>
- 1st release.
