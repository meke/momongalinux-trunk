%global momorel 13
%global artsver 1.5.10
%global artsrel 1m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Graphical Directory Statistics for Used Disk Space
Name: kdirstat
Version: 2.5.3
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2
URL: http://kdirstat.sourceforge.net/
Group: Applications/System
Source0: http://%{name}.sourceforge.net/download/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-gcc43.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: gettext
BuildRequires: desktop-file-utils

%description
KDirStat (KDE Directory Statistics) is a utility program that sums up
disk usage for directory trees - very much like the Unix 'du' command.
It can also help you clean up used space.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .gcc43~

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath \
	--disable-largefile

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category System \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* CREDITS ChangeLog INSTALL README TODO
%{_bindir}/%{name}
%{_bindir}/%{name}-cache-writer
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/kconf_update/fix_move_to_trash_bin.pl
%{_datadir}/apps/kconf_update/%{name}.upd
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.3-11m)
- full rebuild for mo7 release

* Mon May 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.3-10m)
- add --disable-largefile

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.3-9m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.3-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.3-7m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.3-5m)
- rebuild against gcc43

* Sun Feb 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.3-4m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.3-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.3-1m)
- initial package for Momonga Linux
