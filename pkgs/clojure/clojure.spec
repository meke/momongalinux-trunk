%global momorel 5

Summary: A dynamic programming language that targets the Java Virtual Machine
Name: clojure
Version: 1.1.0
Release: %{momorel}m%{?dist}
License: "Eclipse Public License 1.0"
Group: Development/Languages
URL: http://clojure.org/
Source0: http://clojure.googlecode.com/files/clojure-%{version}.zip
NoSource: 0
Source1: clojure.sh
Patch1:  clojure-1.1.0-no_classpath.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ant >= 1.6
BuildRequires: jpackage-utils >= 1.5
BuildRequires: objectweb-asm
BuildRequires: java-devel >= 1.6.0
Requires: java >= 1.6.0
Requires: objectweb-asm

%description 
Clojure is a dynamic programming language that targets the Java
Virtual Machine. It is designed to be a general-purpose language,
combining the approachability and interactive development of a
scripting language with an efficient and robust infrastructure for
multithreaded programming. Clojure is a compiled language - it
compiles directly to JVM bytecode, yet remains completely
dynamic. Every feature supported by Clojure is supported at
runtime. Clojure provides easy access to the Java frameworks, with
optional type hints and type inference, to ensure that calls to Java
can avoid reflection.

%prep
%setup -q 
rm -f *.jar
%patch1 -p1

%build
ant jar

%install
rm -rf --preserve-root %{buildroot}

# prefix install
install -p -d -m 755 %{buildroot}%{_datadir}/%{name}
cp -ar src/clj/clojure/*  %{buildroot}%{_datadir}/%{name}/
rm -f %{buildroot}%{_datadir}/%{name}/xml/\#*

# jar - link to prefix'd jar so that java stuff knows where to look
install -d -m 755 %{buildroot}%{_javadir}
cp clojure.jar %{buildroot}%{_javadir}/%{name}.jar

# startup script
install -d -m 755 %{buildroot}%{_bindir}
cp %{SOURCE1} %{buildroot}%{_bindir}/clojure

install -d %{buildroot}%{_datadir}/maven2/poms
%add_to_maven_depmap clojure clojur-lang %{version} JPP/%{name} clojure-lang
install -m 644 pom.xml %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-clojure-lang.pom

%clean
rm -rf --preserve-root %{buildroot}

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc readme.txt epl-v10.html

%attr(0755,root,root) %{_bindir}/clojure
%{_javadir}/%{name}.jar
%{_datadir}/%{name}
%{_datadir}/maven2/poms/*.pom
%{_mavendepmapfragdir}/clojure

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- full rebuild for mo7 release

* Sat Mar 20 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.0-2m)
- avoid duplicate dir

* Sat Mar 20 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.0-1m)
- import from fedora

* Wed Jan 20 2010 Jochen Schmitt <Jochen herr-schmitt de> 1:1.1.0-1
- New upstream release

* Wed Dec  2 2009 Jochen Schmitt <Jochen herr-schmitt de> 1:1.0.0-5
- Installing maven pom file

* Wed Dec  2 2009 Jochen Schmitt <Jochen herr-schmitt de> 1:1.0.0-3
- Add Epoch to get proper EVR path

* Tue Dec  1 2009 Jochen Schmitt <Jochen herr-schmitt de> 1.0.0-2
- Forgot uploading soruces

* Tue Dec  1 2009 Jochen Schmitt <Jochen herr-schmitt de> 1.0.0-1
- New upstream release
- change license tag to EPL

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090320-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Mar 20 2009 Colin Walters <walters@verbum.org> - 20090320
- New upstream

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20081217-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Dec 18 2008 Colin Walters <walters@verbum.org> - 20081217-1
- New upstream

* Fri Oct 24 2008 Colin Walters <walters@verbum.org> - 20080916-2
- BR OpenJDK, we need 1.6

* Tue Sep 30 2008 Colin Walters <walters@verbum.org> - 20080916-1
- initial version

