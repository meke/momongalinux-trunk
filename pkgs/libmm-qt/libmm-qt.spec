%global         momorel 2
%global         src_name modemmanager-qt
%global         unstable 1
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         ftpdirver %{version}
%global         sourcedir %{release_dir}/%{src_name}/%{ftpdirver}/src

Name:           libmm-qt
Version:        1.0.1
Release:        %{momorel}m%{?dist}
Summary:        Qt-only wrapper for ModemManager DBus API

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            https://projects.kde.org/projects/extragear/libs/libmm-qt
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         %{name}-%{version}-return.patch
BuildRequires:  cmake >= 2.6
BuildRequires:  qt-devel
BuildRequires:  ModemManager-devel >= 0.8

Requires:  ModemManager >= 0.6.0

%description
Qt library for ModemManager

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
%description devel
Qt libraries and header files for developing applications that use ModemManager

%prep
%setup -q
# %%patch0 -p1 -b .return

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
rm -rf %{buildroot}

make install/fast  DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/libModemManagerQt.so.0*
%{_libdir}/libModemManagerQt.so.1*

%files devel
%{_libdir}/pkgconfig/ModemManagerQt.pc
%{_includedir}/ModemManagerQt/
%{_libdir}/libModemManagerQt.so

%changelog
* Thu Feb 20 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- change Source0
- remove rejected Patch0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Wed Sep 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-0.20130707.1m)
- import from Fedora (VERSION in CMakeLists.txt is 0.5.0...)

* Thu Jun 13 2013 Jan Grulich <jgrulich@redhat.com> - 0.6.0-2.20130613gitc5920e0
- Update to the current git snapshot

* Fri May 31 2013 Jan Grulich <jgrulich@redhat.com> - 0.6.0-1.20130422git657646b
- Initial package
- Based on git snapshot 657646bdc1eb9913e07a8307afd2b47b6225209b
