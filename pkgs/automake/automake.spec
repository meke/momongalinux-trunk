%global momorel 1

%global api_version 1.13

Summary:	A GNU tool for automatically creating Makefiles.
Name:		automake
%{?include_specopt}
%{?!do_test: %global do_test 0}
Version:	1.13.4
Release:        %{momorel}m%{?dist}
License:	GPL
Group:		Development/Tools
Source:		http://ftp.gnu.org/gnu/automake/automake-%{version}.tar.xz
NoSource:   0
Patch0:     %{name}-1.13.1-disable-tests.patch
URL:		http://sources.redhat.com/automake
Requires:	perl, perl-attributes
Requires:	autoconf >= 2.63
Requires:	filesystem
BuildRequires:	autoconf >= 2.63
BuildRequires:  perl >= 5.16.0
%if %{do_test}
BuildRequires:  dejagnu
BuildRequires:  gcc-gfortran
%endif
Requires(post): info
Requires(preun): info
BuildArchitectures:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Automake is a tool for automatically generating `Makefile.in'
files compliant with the GNU Coding Standards.
#`

You should install Automake if you are developing software and would
like to use its ability to automatically generate GNU standard
Makefiles. If you install Automake, you will also need to install
GNU's Autoconf package.
#'

%prep
%setup -q
%patch0 -p1 -b .disable_tests
autoreconf -ivf

%build
./configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
   --bindir=%{_bindir} --datadir=%{_datadir} --libdir=%{_libdir} \
   --docdir=%{_docdir}/%{name}-%{version} --disable-silent-rules
make V=0 %{?_smp_mflags}
cp m4/acdir/README README.aclocal
cp contrib/multilib/README README.multilib

%if %{do_test}
%check
make -k %{?_smp_mflags} check || ( cat ./test-suite.log && false )
%endif

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

# create this dir empty so we can own it
mkdir -p %{buildroot}%{_datadir}/aclocal
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/automake.info.* %{_infodir}/dir

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/automake.info.* %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc AUTHORS README THANKS NEWS
%{_bindir}/*
%{_infodir}/*.info*
%{_datadir}/automake-%{api_version}
%{_datadir}/aclocal
%{_datadir}/aclocal-%{api_version}
#%%{_datadir}/doc/automake/amhello-1.0.tar.gz
%{_mandir}/man1/aclocal*
%{_mandir}/man1/automake*

%changelog
* Fri Mar 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13.4-1m)
- update 1.13.4

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.2-8m)
- [SECURITY] CVE-2012-3386

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.2-7m)
- rebuild for perl-5.16.0 (thanks futoshi san !)
- patch was based on http://git.savannah.gnu.org/cgit/automake.git/commit/?h=maint&id=6bf58a59a1f3803e57e3f0378aa9344686707b75

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.2-6m)
- add Requires

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.2-5m)
- add BuildRequires

* Fri Dec 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.2-4m)
- modify patch1 (disable test pkglibdir)

* Fri Dec 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.2-3m)
- modify patch1 (allow pkglibdir for PROGRAMS SCRIPTS DATA)

* Fri Dec 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.2-2m)
- add patch1 (allow pkglibdir for PROGRAMS)

* Sat Dec 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.2-1m)
- update to 1.11.2

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.11.1-6m)
- release a directory provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.11.1-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.1-2m)
- use Requires

* Wed Dec  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.1-1m)
- [SECURITY] CVE-2009-4029
- update to 1.11.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11-2m)
- add patch0 for bash-4.0

* Mon May 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.2-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.2-2m)
- run test

* Sun Jan 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.2-1m)
- update to 1.10.2
- now no test ;-P

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.1-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10.1-2m)
- rebuild against perl-5.10.0-1m

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Sat Nov 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10-3m)
- use automake.specopt condition for %%check

* Tue Oct 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10-2m)
- use make check

* Mon Oct 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sun Jan 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.6-2m)
- skip make check ifnarch ix86

* Mon Dec 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.6-1m)
- update 1.9.6
- include NEWS file
- add %%check

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.5-1m)
- seperate to automake14 automake15 automake16 automake17 and automake(1.9)

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.5-25m)
- automake-1.9.4
- new gnupg2 and dirmngr will require automake >= 1.9.3

* Tue Aug 24 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5-24m)
- automake-1.9.1
- thanks to Nishio Futoshi <fut_nis@d3.dion.ne.jp>

* Sun May  9 2004 Toru Hoshina <t@momonga-linux.org>
- (1.5-23m)
- noarch.

* Sun May  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-22m)
- add automake-1.8 (1.8.4)
- cancel noarch temporarily since automake-1.8 can't be configured with
  'noarch-momonga-linux'
#'

* Fri Dec 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-21m)
- automake-1.7.9

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.5-20m)
- automake-1.7.7

* Tue Jun 10 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (1.5-19m)
- automake 1.7.3

* Mon Feb 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-18m)
- automake 1.7.2

* Tue Jan  7 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-17m)
- buildprereq autoconf >= 2.54

* Mon Nov 25 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5-16m)
- add %{_datadir}/aclocal-1.4 to %files

* Sun Nov 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5-15m)
- update automake-1.4 to 1.4-p6
- update automake-1.6 to 1.6.3
- add    automake-1.7(1.7.1)
- remove automake-new

* Mon Nov 18 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5-14m)
- add autohoge-1.4 symlink

* Mon Jul 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-13m)
- define '__libtoolize' as '/bin/true' because libtool requires automake!

* Wed May 22 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5-12k) [kakure 1.6.1]
- added 1.6.1 (as an option)
- /usr/bin/* -> %{_bindir}/explicit_name
- why not simply update: because it seems to break aclocal-old
 compatibility... any idea?

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.5-10k)
- /sbin/install-info -> info in PreReq.

* Fri Aug 31 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5-5k)
- merging automake 1.5 and 1.4-p5

* Thu Aug 30 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5-3k)
- up to 1.5
- remove libtool.patch (no needed)

* Fri Aug 24 2001 Toru Hoshina <toru@df-usa.com>
- (1.4p5-3k)
- merge to Jirai.

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- (1.4p5-2k)

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Sun Jul  2 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.6.0 

* Fri Apr 21 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, Distribution, description )

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.4-6).

* Fri Feb 04 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix bug #8870

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Aug 21 1999 Jeff Johnson <jbj@redhat.com>
- revert to pristine automake-1.4.

* Mon Mar 22 1999 Preston Brown <pbrown@redhat.com>
- arm netwinder patch

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Mon Feb  8 1999 Jeff Johnson <jbj@redhat.com>
- add patches from CVS for 6.0beta1

* Sun Jan 17 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.4.

* Mon Nov 23 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.3b.
- add URL.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 07 1998 Erik Troan <ewt@redhat.com>
- updated to 1.3

* Tue Oct 28 1997 Cristian Gafton <gafton@redhat.com>
- added BuildRoot; added aclocal files

* Fri Oct 24 1997 Erik Troan <ewt@redhat.com>
- made it a noarch package

* Thu Oct 16 1997 Michael Fulbright <msf@redhat.com>
- Fixed some tag lines to conform to 5.0 guidelines.

* Thu Jul 17 1997 Erik Troan <ewt@redhat.com>
- updated to 1.2

* Wed Mar 5 1997 msf@redhat.com <Michael Fulbright>
- first version (1.0)
