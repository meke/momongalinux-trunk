%global momorel 2

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%define _binaries_in_noarch_packages_terminate_build 0

Summary: Boot server configurator
Name: cobbler
Version: 2.2.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://cobbler.github.com/
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{ix86} x86_64 ppc ppc64 s390x
AutoReq: no
BuildRequires: git
BuildRequires: PyYAML
BuildRequires: python-devel
BuildRequires: python-cheetah
BuildRequires: python-setuptools-devel
Requires: python >= 2.7
Requires: python-urlgrabber
%ifarch %{ix86} x86_64
Requires: syslinux
%endif
Requires: httpd
Requires: tftp-server
Requires: mod_python
Requires: python-devel
Requires: createrepo
Requires: python-cheetah
Requires: rsync
Requires: python-netaddr
Requires: PyYAML
Requires: python-simplejson
Requires: libyaml
Requires: genisoimage
BuildRequires: systemd-units
Requires(post): systemd-sysv 
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]" || echo 0)}
Requires: python(abi) = %{pyver}

%description
Cobbler is a network install server.  Cobbler 
supports PXE, virtualized installs, and 
reinstalling existing Linux machines.  The last two 
modes use a helper tool, 'koan', that 
integrates with cobbler.  Cobbler's advanced features 
include importing distributions from DVDs and rsync 
mirrors, kickstart templating, integrated yum 
mirroring, and built-in DHCP/DNS/power Management.  Cobbler has 
a Python and XMLRPC API for integration with other  
applications.  There is also a web interface.

%prep
%setup -q

%build
%{__python} setup.py build

%install
test "x$RPM_BUILD_ROOT" != "x" && rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --optimize=1 --root=$RPM_BUILD_ROOT $PREFIX
mv $RPM_BUILD_ROOT/etc/httpd/conf.d/cobbler.conf $RPM_BUILD_ROOT/etc/httpd/conf.d/cobbler.conf.dist
mv $RPM_BUILD_ROOT/etc/httpd/conf.d/cobbler_web.conf $RPM_BUILD_ROOT/etc/httpd/conf.d/cobbler_web.conf.dist

mkdir -p $RPM_BUILD_ROOT/var/spool/koan

mkdir -p $RPM_BUILD_ROOT/var/lib/tftpboot/images

rm -rf $RPM_BUILD_ROOT/etc/init.d
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
install -m0644 config/cobblerd.service $RPM_BUILD_ROOT%{_unitdir}

%post
if [ $1 -eq 1 ] ; then
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
elif [ "$1" -ge "2" ]; then
  # backup config
  if [ -e /var/lib/cobbler/distros ]; then
    cp /var/lib/cobbler/distros*  /var/lib/cobbler/backup 2>/dev/null
    cp /var/lib/cobbler/profiles* /var/lib/cobbler/backup 2>/dev/null
    cp /var/lib/cobbler/systems*  /var/lib/cobbler/backup 2>/dev/null
    cp /var/lib/cobbler/repos*    /var/lib/cobbler/backup 2>/dev/null
    cp /var/lib/cobbler/networks* /var/lib/cobbler/backup 2>/dev/null
  fi 
  if [ -e /var/lib/cobbler/config ]; then
    cp -a /var/lib/cobbler/config    /var/lib/cobbler/backup 2>/dev/null
  fi
  # upgrade older installs
  # move power and pxe-templates from /etc/cobbler, backup new templates to *.rpmnew
  for n in power pxe; do
    rm -f /etc/cobbler/$n*.rpmnew
    find /etc/cobbler -maxdepth 1 -name "$n*" -type f | while read f; do
      newf=/etc/cobbler/$n/`basename $f`
      [ -e $newf ] &&  mv $newf $newf.rpmnew
      mv $f $newf
    done
  done
  # upgrade older installs
  # copy kickstarts from /etc/cobbler to /var/lib/cobbler/kickstarts
  rm -f /etc/cobbler/*.ks.rpmnew
  find /etc/cobbler -maxdepth 1 -name "*.ks" -type f | while read f; do
    newf=/var/lib/cobbler/kickstarts/`basename $f`
    [ -e $newf ] &&  mv $newf $newf.rpmnew
    cp $f $newf
  done
  /bin/systemctl try-restart cobblerd.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable cobblerd.service > /dev/null 2>&1 || :
  /bin/systemctl stop cobblerd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart cobblerd.service >/dev/null 2>&1 || :
fi

%triggerun -- cobbler < 2.2.1-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply cobblerd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save cobblerd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del cobblerd >/dev/null 2>&1 || :
/bin/systemctl try-restart cobblerd.service >/dev/null 2>&1 || :

%clean
test "x$RPM_BUILD_ROOT" != "x" && rm -rf $RPM_BUILD_ROOT

%files
%doc AUTHORS CHANGELOG README COPYING
%{_bindir}/cobbler
%{_bindir}/cobbler-ext-nodes
%{_bindir}/cobblerd
%{_sbindir}/tftpd.py*
%config(noreplace) %{_sysconfdir}/cobbler
%{_unitdir}/cobblerd.service
%{python_sitelib}/cobbler
%config(noreplace) /var/lib/cobbler
/var/log/cobbler
/var/www/cobbler
%{_mandir}/man1/cobbler.1.*
%config(noreplace) /etc/httpd/conf.d/cobbler.conf.dist
%exclude %{python_sitelib}/cobbler/sub_process.py*
%{python_sitelib}/cobbler*.egg-info
/var/lib/tftpboot/images


%package -n koan

Summary: Helper tool that performs cobbler orders on remote machines.
Group: Applications/System
Requires: python >= 1.5
BuildRequires: python-devel
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}
Requires: python(abi) >= %{pyver}
BuildRequires: python-setuptools-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildArch: noarch
URL: http://fedorahosted.org/cobbler/


%description -n koan

Koan stands for kickstart-over-a-network and allows for both
network installation of new virtualized guests and reinstallation 
of an existing system.  For use with a boot-server configured with Cobbler

%files -n koan
%doc AUTHORS COPYING CHANGELOG README
%dir /var/spool/koan
%dir /var/lib/koan/config
%{_bindir}/koan
%{_bindir}/cobbler-register
%{python_sitelib}/koan
%exclude %{python_sitelib}/koan/sub_process.py*
%exclude %{python_sitelib}/koan/opt_parse.py*
%exclude %{python_sitelib}/koan/text_wrap.py*
%{_mandir}/man1/koan.1.*
%{_mandir}/man1/cobbler-register.1.*
%dir /var/log/koan

%package -n cobbler-web

Summary: Web interface for Cobbler
Group: Applications/System
Requires: cobbler
Requires: Django
BuildRequires: python-devel
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}
Requires: python(abi) >= %{pyver}
BuildRequires: python-setuptools-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildArch: noarch
URL: http://fedorahosted.org/cobbler/

%description -n cobbler-web

Web interface for Cobbler that allows visiting http://server/cobbler_web to configure the install server.

%files -n cobbler-web
%defattr(-,root,root,-)
%doc AUTHORS COPYING CHANGELOG README
%config(noreplace) /etc/httpd/conf.d/cobbler_web.conf.dist
%defattr(-,apache,apache,-)
/usr/share/cobbler/web
%dir /var/lib/cobbler/webui_sessions
/var/www/cobbler_webui_content/

%changelog
* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momoga-linux.org>
- (2.2.3-2m)
- rebuild against syslinux-5.00-1m

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- [SECURITY] CVE-2012-2395
- update 2.2.3

* Fri Oct 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-2m)
- bump version of cobbler-web and koan to 2.2.1

* Wed Oct 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-1m)
- update 2.2.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update 2.0.4

* Tue Apr 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3.1-3m)
- remove BuildRequires: redhat-rpm-config

* Wed Apr  7 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.3.1-2m)
- add logrotate patch

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3.1-1m)
- update 2.0.3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.5-1m)
- sync with Fedora 11 (1.6.5-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.9-2m)
- rebuild against python-2.6.1-1m

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-1m)
- [SECURITY] CVE-2008-6954
- update to 1.2.9

* Tue Oct  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-2m)
- stop service at initial startup

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update 1.2.4

* Mon Sep  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-2m)
- move cobbler_svc.conf to cobbler_svc.conf.dist for httpd start

* Wed Jul 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3
- fix %%{momonga} version

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.2-1m)
- import from Fedora

* Fri Mar 07 2008 Michael DeHaan <mdehaan@redhat.com> - 0.8.2-1
- Upstream changes (see CHANGELOG)

* Wed Feb 20 2008 Michael DeHaan <mdehaan@redhat.com> - 0.8.1-1
- Upstream changes (see CHANGELOG)

* Fri Feb 15 2008 Michael DeHaan <mdehaan@redhat.com> - 0.8.0-2
- Fix egg packaging

* Fri Feb 15 2008 Michael DeHaan <mdehaan@redhat.com> - 0.8.0-1
- Upstream changes (see CHANGELOG)

* Mon Jan 21 2008 Michael DeHaan <mdehaan@redhat.com> - 0.7.2-1
- Upstream changes (see CHANGELOG)
- prune changelog, see git for full

* Mon Jan 07 2008 Michael DeHaan <mdehaan@redhat.com> - 0.7.1-1
- Upstream changes (see CHANGELOG)
- Generalize what files are included in RPM
- Add new python module directory
- Fixes for builds on F9 and later

* Thu Dec 14 2007 Michael DeHaan <mdehaan@redhat.com> - 0.7.0-1
- Upstream changes (see CHANGELOG), testing branch
- Don't require syslinux
- Added requires on rsync
- Disable autoreq to avoid slurping in perl modules

* Wed Nov 14 2007 Michael DeHaan <mdehaan@redhat.com> - 0.6.4-2
- Upstream changes (see CHANGELOG)
- Permissions changes

* Wed Nov 07 2007 Michael DeHaan <mdehaan@redhat.com> - 0.6.3-2
- Upstream changes (see CHANGELOG)
- now packaging javascript file(s) seperately for WUI
- backup state files on upgrade 
- cobbler sync now has pre/post triggers, so package those dirs/files
- WebUI now has .htaccess file
- removed yum-utils as a requirement

* Fri Sep 28 2007 Michael DeHaan <mdehaan@redhat.com> - 0.6.2-2
- Upstream changes (see CHANGELOG)
- removed syslinux as a requirement (cobbler check will detect absense)
- packaged /var/lib/cobbler/settings as a config file
- added BuildRequires of redhat-rpm-config to help src RPM rebuilds on other platforms
- permissions cleanup
- make license field conform to rpmlint
- relocate cgi-bin files to cobbler subdirectory 
- include the WUI!

* Thu Aug 30 2007 Michael DeHaan <mdehaan@redhat.com> - 0.6.1-2
- Upstream changes (see CHANGELOG)

* Thu Aug 09 2007 Michael DeHaan <mdehaan@redhat.com> - 0.6.0-1
- Upstream changes (see CHANGELOG)

* Thu Jul 26 2007 Michael DeHaan <mdehaan@redhat.com> - 0.5.2-1
- Upstream changes (see CHANGELOG)
- Tweaked description

* Fri Jul 20 2007 Michael DeHaan <mdehaan@redhat.com> - 0.5.1-1
- Upstream changes (see CHANGELOG)
- Modified description
- Added logrotate script
- Added findks.cgi

* Wed Jun 27 2007 Michael DeHaan <mdehaan@redhat.com> - 0.5.0-1
- Upstream changes (see CHANGELOG)
- Added dnsmasq.template 

* Fri Apr 27 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.9-1
- Upstream changes (see CHANGELOG)

* Thu Apr 26 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.8-1
- Upstream changes (see CHANGELOG)
- Fix defattr in spec file

* Fri Apr 20 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.7-5
- Upstream changes (see CHANGELOG)
- Added triggers to /var/lib/cobbler/triggers

* Thu Apr 05 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.6-0
- Upstream changes (see CHANGELOG)
- Packaged 'config' directory under ks_mirror

* Fri Mar 23 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.5-3
- Upstream changes (see CHANGELOG)
- Fix sticky bit on /var/www/cobbler files

* Fri Mar 23 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.4-0
- Upstream changes (see CHANGELOG)

* Wed Feb 28 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.3-0
- Upstream changes (see CHANGELOG)
- Description cleanup

* Mon Feb 19 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.2-0
- Upstream changes (see CHANGELOG)

* Mon Feb 19 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.1-0
- Bundles menu.c32 (syslinux) for those distros that don't provide it.
- Unbundles Cheetah since it's available at http://www.python.org/pyvault/centos-4-i386/
- Upstream changes (see CHANGELOG)

* Mon Feb 19 2007 Michael DeHaan <mdehaan@redhat.com> - 0.4.0-1
- Upstream changes (see CHANGELOG)
- Cobbler RPM now owns various directories it uses versus creating them using commands.
- Bundling a copy of Cheetah for older distros
