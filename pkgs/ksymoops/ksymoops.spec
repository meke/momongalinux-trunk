%global momorel 1

Summary: Kernel oops and error message decoder
Name: ksymoops
Version: 2.4.11
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Kernel
Source0: http://www.kernel.org/pub/linux/utils/kernel/%{name}/v2.4/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://freecode.com/projects/ksymoops
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%define buildcc gcc
BuildRequires: gcc
BuildRequires: binutils-devel
BuildRequires: zlib-devel

%description
The Linux kernel produces error messages that contain machine specific
numbers which are meaningless for debugging.  ksymoops reads machine
specific files and the error log and converts the addresses to
meaningful symbols and offsets.

%prep
%setup -q

%build
# binutils requires "-lz -ldl"
LDFLAGS="-lz -ldl" \
CFLAGS="%{optflags}" make all CC=%{buildcc}

%install
rm -rf %{buildroot}
make INSTALL_PREFIX=%{buildroot}/usr \
     INSTALL_MANDIR=%{buildroot}/%{_mandir} \
     install

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root)
%doc COPYING Changelog README*
/usr/bin/ksymoops
%{_mandir}/man8/ksymoops.8*

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11

* Tue Jul 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.9-12m)
- really fix build

* Mon Jul 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-11m)
- fix build failure with new binutils

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.9-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.9-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.9-6m)
- rebuild against rpm-4.6

* Sat Nov  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-5m)
- add LDFLAGS="-lz" for newer binutils
- add BuildRequires

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.9-4m)
- rebuild against gcc43

* Sat Oct 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.4.9-3m)
- remove Obsoletes: kernel-utils

* Tue Jan 18 2005 mutecat <mutecat@momonga-linux.org>
- (2.4.9-2m)
- arrange ppc not use gcc_2_95_3

* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.4.9-1m)
  update to 2.4.9

* Fri Nov  1 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.7-1m)
- formerly known as kernel-utils package
