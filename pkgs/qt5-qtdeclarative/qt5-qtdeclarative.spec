%global momorel 1
%global qt_module qtdeclarative

Summary: Qt5 - QtDeclarative component
Name:    qt5-%{qt_module}
Version: 5.2.1
Release: %{momorel}m%{?dist}

# See LICENSE.GPL LICENSE.LGPL LGPL_EXCEPTION.txt, for details
License: "LGPLv2 with exceptions or GPLv3 with exceptions"
Group: System Environment/Libraries
Url: http://qt-project.org/
Source0: http://download.qt-project.org/official_releases/qt/5.2/%{version}/submodules/%{qt_module}-opensource-src-%{version}.tar.xz
NoSource: 0
# support no_sse2 CONFIG (fedora i686 builds cannot assume -march=pentium4 -msse2 -mfpmath=sse flags, or the JIT that needs them)
# https://codereview.qt-project.org/#change,73710
Patch1: qtdeclarative-opensource-src-5.2.0-no_sse2.patch
# two upstream/upstream fixes from https://code.google.com/p/double-conversion/issues/detail?id=33
# Qt uses old version of double-conversion code
Patch2: 0001-Add-ARM-64-support.patch
Patch3: 0002-Fix-build-on-ARMv8-64bit.patch

# qt5-qtjsbackend supports only ix86, x86_64 and arm , and so do we here
ExclusiveArch: %{ix86} x86_64 %{arm}

# define to build docs, need to undef this for bootstrapping
# where qt5-qttools builds are not yet available
# only primary archs (for now), allow secondary to bootstrap
%ifarch %{arm} %{ix86} x86_64
%{?include_specopt}
%{?!docs: %global docs 1}
%endif

%{?_qt5_version:Requires: qt5-qtbase%{?_isa} >= %{_qt5_version}}

Obsoletes: qt5-qtjsbackend < 5.2.0

%description
%{summary}

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: qt5-qtbase-devel%{?_isa}
Obsoletes: qt5-qtjsbackend-devel < 5.2.0

%description devel
%{summary}.

%package static
Summary: Static library files for %{name}
Group: Development/Libraries
Requires: %{name}-devel%{?_isa} = %{version}-%{release}

%description static
%{summary}.

%if 0%{?docs}
%package doc
Summary: API documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
# for qhelpgenerator
BuildRequires: qt5-qttools-devel
BuildArch: noarch

%description doc
%{summary}.
%endif

%package examples
Summary: Programming examples for %{name}
Group: Documentation
Requires: %{name}%{?_isa} = %{version}-%{release}

%description examples
%{summary}.

%prep
%setup -q -n %{qt_module}-opensource-src-%{version}%{?pre:-%{pre}}
%patch1 -p1 -b .no_sse2
%patch2 -p1 -b .0001
%patch3 -p1 -b .0002

%build
%{_qt5_qmake}

make %{?_smp_mflags}

%if 0%{?docs}
make %{?_smp_mflags} docs
%endif

%install
rm -rf --preserve-root %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%if 0%{?docs}
make install_docs INSTALL_ROOT=%{buildroot}
%endif

# put non-conflicting binaries with -qt5 postfix in %{_bindir}
mkdir %{buildroot}%{_bindir}
pushd %{buildroot}%{_qt5_bindir}
for i in * ; do
  case "${i}" in
    qmlplugindump|qmlprofiler)
      mv $i ../../../bin/${i}-qt5
      ln -s ../../../bin/${i}-qt5 .
      ln -s ../../../bin/${i}-qt5 $i
      ;;
   *)
      mv $i ../../../bin/
      ln -s ../../../bin/$i .
      ;;
  esac
done
popd

## .prl/.la file love
# nuke .prl reference(s) to %%buildroot, excessive (.la-like) libs
pushd %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
  sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
  if [ -f "$(basename ${prl_file} .prl).so" ]; then
    rm -fv "$(basename ${prl_file} .prl).la"
    sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
  fi
done
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE.GPL LICENSE.LGPL LGPL_EXCEPTION.txt
%doc dist/changes*
%{_qt5_libdir}/libQt5Qml.so.5*
%{_qt5_libdir}/libQt5Quick.so.5*
%{_qt5_libdir}/libQt5QuickParticles.so.5*
%{_qt5_libdir}/libQt5QuickTest.so.5*
%{_qt5_plugindir}/accessible/libqtaccessiblequick.so
%{_qt5_plugindir}/qmltooling/
%{_qt5_archdatadir}/qml/

%files devel
%defattr(-,root,root,-)
%{_bindir}/qml*
%{_qt5_bindir}/qml*
%{_qt5_headerdir}/Qt*
%{_qt5_libdir}/libQt5Qml.so
%{_qt5_libdir}/libQt5Qml.prl
%{_qt5_libdir}/libQt5Quick*.so
%{_qt5_libdir}/libQt5Quick*.prl
%{_qt5_libdir}/cmake/Qt5*
%{_qt5_libdir}/pkgconfig/Qt5*.pc
%{_qt5_archdatadir}/mkspecs/modules/*.pri

%files static
%defattr(-,root,root,-)
%{_qt5_libdir}/libQt5QmlDevTools.*a
%{_qt5_libdir}/libQt5QmlDevTools.prl

%if 0%{?docs}
%files doc
%defattr(-,root,root,-)
%{_qt5_docdir}/qtqml.qch
%{_qt5_docdir}/qtqml/
%{_qt5_docdir}/qtquick.qch
%{_qt5_docdir}/qtquick/
%{_qt5_docdir}/qtquickdialogs.qch
%{_qt5_docdir}/qtquickdialogs/
%endif

%if 0%{?_qt5_examplesdir:1}
%files examples
%defattr(-,root,root,-)
%{_qt5_examplesdir}/
%endif

%changelog
* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1
- add doc and examples sub packages

* Wed Sep 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.1-1m)
- import from Fedora

* Wed Aug 28 2013 Rex Dieter <rdieter@fedoraproject.org> 5.1.1-1
- 5.1.1

* Tue Aug 20 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-4
- qt5-qtjsbackend only supports ix86, x86_64 and arm

* Tue May 14 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-3
- fix qmlprofiler conflict with qt-creator

* Fri Apr 12 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-2
- fix qmlplugindump conflict with qt4-devel
- include license files, dist/changes*

* Thu Apr 11 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-1
- 5.0.2

* Sat Feb 23 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.1-1
- first try

