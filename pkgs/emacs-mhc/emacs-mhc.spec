%global momorel 9
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global snapdate 20100807
%global srcname mhc-%{snapdate}

%global ruby_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')
%global ruby_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: Message Harmonized Calendaring System for Emacs
Name: emacs-mhc
Version: 0.25
Release: 0.%{snapdate}.%{momorel}m%{?dist}
License: GPL
Group: Applications/Text
Source0: http://www.quickhack.net/mhc/arc/%{srcname}.tar.gz
Patch1: mhc-set-icondir.patch
URL: http://www.quickhack.net/mhc/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-mew >= 6.3-2m
BuildRequires: emacs-wl >= 2.15.9-0.0.20100303.2m
BuildRequires: ruby18 ruby18-devel pilot-link-devel
Requires: emacs >= %{emacsver}
Requires: emacs-mew >= 6.3-2m
Requires: emacs-wl >= 2.15.9-0.0.20100303.2m
Requires: ruby18 pilot-link
Obsoletes: mhc-emacs
Obsoletes: mhc-xemacs
Obsoletes: elisp-mhc
Provides: elisp-mhc

%description
MHC is designed to help those who receive most appointments via email.
Using MHC, you can easily import schedule articles from emails.

You can get the latest version from:
 http://www.quickhack.net/mhc/

MHC has following features:

+ Simple data structure allows you to manipulate stored data in many ways.
+ Both UNIX and Windows9x support.
+ Appointments can be made to repeat in flexible ways.
+ powerful but simple expression of appointments.
  Each appointment can have following attributes:

    date, subject, start/end time (not mandatory),
    advance time of alarm,
    multiple categories,
    repetition rules,
    duration of the repetition,
    list of exception dates,
    list of extra dates
    description and email which originated the appointment.

+ Multiple User Interface such as commandline/emacs/GUI/Web.
  MHC currently has following interfaces:

    + Elisp package cooperative with  Mew, Wanderlust or Gnus
        (popular MUA in the Emacs world)
        (emacs/mhc.el)
    + GUI (Ruby/Gtk based) desktop calendar application. (gemcal)
    + CGI based Web interface
        (not in this package please see http://mhc.hauN.org/web-mhc/)
    + Command line schedule lister like the scan of MH. (mscan)

MHC stores schedule articles in the same form of MH; you can manipulate
these messages not only by above tools but also by many other MUAs,
editors, UNIX commandline tools or your own scripts.

%prep
%setup -q -n mhc
%patch1 -p1 -b .icondir

%build
ruby18 ./configure.rb \
     --with-ruby=/usr/bin/ruby18 \
     --bindir=%{_bindir} \
     --libdir=%{ruby_sitelibdir} \
     --with-icondir=%{_datadir}/pixmaps/mhc

%install
rm -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_bindir}
%{__mkdir_p} %{buildroot}%{_datadir}/pixmaps/mhc

for f in adb2mhc gemcal mhc-sync mhc2palm palm2mhc today
do
    %{__install} -m 755 $f %{buildroot}%{_bindir}
done

%{__cp} xpm/*.xpm %{buildroot}%{_datadir}/pixmaps/mhc

pushd ruby-ext
%{__make}
%{__make} install DESTDIR=%{buildroot}
popd

pushd emacs
%{__make} elc \
	  MEW_OPTS=--with-mew \
	  WL_OPTS=--with-wl \
	  GNUS_OPTS=--with-gnus \
	  INST_OPTS=--with-lispdir=%{e_sitedir}/mhc
%{__make} install \
	  INST_OPTS=--with-lispdir=%{buildroot}%{e_sitedir}/mhc
%{__mkdir_p} %{buildroot}%{e_sitedir}/etc/mhc
%{__cp} ../icons/*.xpm %{buildroot}%{e_sitedir}/etc/mhc

%{__mv} 00usage.jis 00usage.jis-emacs
%{__mv} CHANGES.ja CHANGES.ja-emacs
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc 00* ChangeLog NEWS* emacs/00usage.jis-emacs emacs/CHANGES.ja-emacs
%{_bindir}/*
%{ruby_sitearchdir}/*
%{ruby_sitelibdir}/*.rb
%{_datadir}/pixmaps/mhc
%{e_sitedir}/mhc
%{e_sitedir}/etc/mhc

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-0.20100807.9m)
- rebuild for emacs-24.1

* Sun Apr  1 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.25-0.20100807.8m)
- delete Require: ruby18-gtk2 (Obsoletes)
- Does not work well maybe

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-0.20100807.7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.25-0.20100807.6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-0.20100807.5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.25-0.20100807.4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-0.20100807.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25-0.20100807.2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.25-0.20100807.1m)
- update to snapshot version
- use ruby18 package

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20091225.3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20091225.2m)
- merge mhc-emacs to elisp-mhc
- kill mhc-xemacs

* Thu Jan  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20091225.1m)
- update to snapshot version

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20090302.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.25-0.20090302.5m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.25-0.20090302.4m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20090302.3m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20090302.2m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20090302.1m)
- update to cvs snapshot
-- drop Patch0, merged upstream

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.7m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.6m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.5m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.4m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.3m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.2m)
- rebuild against xemacs-21.5.28

* Sun Jul 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080720.1m)
- update to cvs snapshot

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.25-0.20080220.5m)
- modify %%files for smart handling of a directory

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080220.4m)
- delete BuildPreReq: mew-xemacs

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080220.3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.25-0.20080220.2m)
- rebuild against gcc43

* Tue Feb 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20080220.1m)
- update to cvs snapshot

* Tue Oct 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20070920.1m)
- update to cvs snapshot

* Mon Aug 20 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20070820.1m)
- update to cvs snapshot

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-0.20070720.1m)
- initial packaging

