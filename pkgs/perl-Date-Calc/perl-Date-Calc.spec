%global momorel 20

Name:           perl-Date-Calc
Version:        6.3
Release:        %{momorel}m%{?dist}
Summary:        Gregorian calendar date calculations
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Date-Calc/
Source0:        http://www.cpan.org/authors/id/S/ST/STBEY/Date-Calc-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Bit-Vector >= 7.1
BuildRequires:  perl-Carp-Clan >= 5.3
#BuildRequires:  perl-Date-Calc-XS >= 6.2
Requires:       perl-Bit-Vector >= 7.1
Requires:       perl-Carp-Clan >= 5.3
#Requires:       perl-Date-Calc-XS >= 6.2
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
use Date::Calc qw( Days_in_Year Days_in_Month ... );

%prep
%setup -q -n Date-Calc-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
make test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES.txt CREDITS.txt README.txt
%{perl_vendorlib}/Date/Calc.*
%{perl_vendorlib}/Date/Calendar.*
%{perl_vendorlib}/Date/Calc/*
%{perl_vendorlib}/Date/Calendar/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-20m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-19m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-18m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-17m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-16m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-15m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-13m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3-1m)
- update to 6.3

* Sat Oct 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2-1m)
- update to 6.2

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- update to 6.0

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7-1m)
- update to 5.7

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6-1m)
- update to 5.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.5.1-3m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.5.1-2m)
- use vendor

* Thu Mar  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.1-1m)
- update to 5.5.1

* Fri Jan 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5-1m)
- update to 5.5

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.4-3m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.4-2m)
- rebuilt against perl-5.8.7
- rebuilt against perl-Bit-Vector-6.4-2m

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.4-1m)
- update to 5.4

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.3-8m)
- rebuild against perl-5.8.5
- rebuild against perl-Bit-Vector 6.3-6m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.3-7m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (5.3-6m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.3-5m)
- rebuild against perl-5.8.2
- rebuild against perl-Bit-Vector 6.3-5m

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.3-4m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (5.3-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Mon Sep 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.3-1m)
- code cleanup

* Sun Sep 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.2-1m)

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.1-2m)
- remove files that conflict with 'perl-Bit-Vector' package

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.1-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.0-3m)
- remove BuildRequires: gcc2.95.3

* Sun May 12 2002 Toru Hoshina <t@Kondara.org>
- (5.0-2k)
- version up.

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (4.3-12k)
- rebuild against for perl-5.6.1

* Mon Sep 17 2001 Toru Hoshina <t@kondara.org>
- (4.3-10k)
- no more fixpack...

* Fri May 04 2001 Toru Hoshina <toru@df-usa.com>
- (4.3-8k)
- rebuild by gcc 2.95.3 :-P

* Wed Nov 29 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- use perl_archlib & perl_sitearch macro

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.600.

* Fri Feb 4 2000 Kyoichi Ozaki <k@afromania.org>
- 1st release
