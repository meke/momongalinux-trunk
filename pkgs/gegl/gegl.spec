%global momorel 4

Summary: Generic Graphics Library
Name: gegl
Version: 0.2.0
Release: %{momorel}m%{?dist}

# Compute some version related macros
# Ugly hack, you need to get your quoting backslashes/percent signs straight
%global major %(ver=%version; echo ${ver%%%%.*})
%global minor %(ver=%version; ver=${ver#%major.}; echo ${ver%%%%.*})
%global micro %(ver=%version; ver=${ver#%major.%minor.}; echo ${ver%%%%.*})
%global apiver %major.%minor

License: GPLv3+ and LGPLv3+
Group: Development/Tools
URL: http://www.gegl.org/
Source0: ftp://ftp.gimp.org/pub/%{name}/%{apiver}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0:  gegl-0.1.6-ruby19.patch
Patch1:  gegl-0.1.2-new_ffmpeg.patch
Patch2:  gegle-0.1.6-docdir.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: enscript
BuildRequires: pkgconfig
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: SDL-devel
BuildRequires: atk-devel
BuildRequires: babl-devel >= 0.1.10
BuildRequires: bzip2-devel
BuildRequires: cairo-devel >= 1.10.2
BuildRequires: dirac-devel
BuildRequires: expat-devel
BuildRequires: faac-devel
BuildRequires: faad2-devel
BuildRequires: ffmpeg-devel >= 0.7.0-0.20110705
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel >= 2.28.0
BuildRequires: glibc-devel
BuildRequires: glitz-devel
BuildRequires: graphviz-devel
BuildRequires: gtk2-devel >= 2.18.0
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: lame-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libogg-devel
BuildRequires: libopenraw-devel
BuildRequires: libpng-devel
BuildRequires: librsvg2-devel >= 2.22.2-3m
BuildRequires: libstdc++-devel
BuildRequires: libtheora-devel
BuildRequires: libvorbis-devel
BuildRequires: libxcb-devel
BuildRequires: pango-devel >= 1.18.2
BuildRequires: pixman-devel
BuildRequires: ruby
BuildRequires: speex-devel
BuildRequires: x264-devel >= 0.0.1995-0.20110514
BuildRequires: xcb-util-devel
BuildRequires: zlib-devel

%description
GEGL (Generic Graphics Library) is a graph based image processing framework.

GEGLs original design was made to scratch GIMPs itches for a new
compositing and processing core. This core is being designed to have
minimal dependencies. and a simple well defined API.

%package devel
Summary: gnomescan devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: babl-devel

%description devel
gegl devel

%prep
%setup -q
%patch0 -p1 -b .ruby19
#%patch1 -p1 -b .new_ffmpeg
#%patch2 -p1 -b .docdir

%build
autoreconf -fi
%configure \
    --with-pic \
    --with-gio \
    --with-gtk \
    --with-cairo \
    --with-pango \
    --with-pangocairo \
    --with-gdk-pixbuf \
    --with-lensfun \
    --with-libjpeg \
    --with-libpng \
    --with-librsvg \
    --with-openexr \
    --with-sdl \
    --with-libopenraw \
    --with-jasper \
    --with-graphviz \
    --with-lua \
    --without-libavformat \
    --with-libv4l \
    --with-libspiro \
    --with-exiv2 \
    --with-umfpack \
    --with-libavforma \
    --without-vala \
    --disable-static --enable-workshop \
    --disable-gtk-doc

# gtk-doc broken...

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete

%find_lang %{name}-%{apiver}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}-%{apiver}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog NEWS README
%{_bindir}/%{name}
%{_libdir}/*.so.*
%{_libdir}/gegl-%{apiver}/

%files devel
%defattr(-,root,root)
%{_includedir}/gegl-%{apiver}/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-4m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- rebuild for glib 2.33.2

* Mon Jun 11 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (0.2.0-1m)
- update to 0.2.0

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.8-1m)
- update to 0.1.8

* Mon Jul 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.2-7m)
- rebuild against ffmpeg-0.7.0-0.20110705

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.2-6m)
- rebuild x264 and ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-3m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.2-2m)
- fix ruby19 document generate error

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-1m)
- update 0.1.2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-4m)
- rebuild against libjpeg-8a

* Wed Mar 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-3m)
- remove BuildRequires: gegl-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-1m)
- update 0.1.0

* Thu Jul  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.22-4m)
- apply replace img_convert() patch from Gentoo
-- http://bugs.gentoo.org/246797
-- http://bugs.gentoo.org/240433

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.22-3m)
- fix license

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.22-2m)
- rebuild against rpm-4.6

* Sat Jan  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.22-1m)
- update to 0.0.22
- License: GPLv3+ or LGPLv3+

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20-1m)
- update 0.0.20

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.18-3m)
- rebuild against graphviz-2.18

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.18-1m)
- update to 0.0.18

* Tue May 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.16-1m)
- update to 0.0.16

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.12-7m)
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.12-6m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.12-5m)
- rebuild against OpenEXR-1.6.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.12-4m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.0.12-3m)
- add libdir patch

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.12-2m)
- install library to lib64 (arch x86_64)

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.12-1m)
- initial build
