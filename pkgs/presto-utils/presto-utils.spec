%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: Tools for creating presto repositories
Name: presto-utils
Version: 0.3.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source: http://www.lesbg.com/jdieter/presto/%{name}-%{version}.tar.bz2
URL: http://www.lesbg.com/jdieter/presto/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python-setuptools-devel
Requires: python >= 2.7, deltarpm >= 3.4-2, createrepo >= 0.4.8

%description
Yum-presto is a plugin for yum that looks for deltarpms rather than rpms
whenever they are available.  This has the potential of saving a lot of
bandwidth when downloading updates.

A Deltarpm is the difference between two rpms.  If you already have foo-1.0
installed and foo-1.1 is available, yum-presto will download the deltarpm
for foo-1.0 => 1.1 rather than the full foo-1.1 rpm, and then build the full 
foo-1.1 package from your installed foo-1.0 and the downloaded deltarpm.

Presto-utils is a series of tools for creating presto repositories that
yum-presto can use.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%doc README 
%doc COPYING
%doc ChangeLog
%doc relaxng
%{_bindir}/createprestorepo
%{_bindir}/createprestorepo-0.2
%{_bindir}/createprestorepo-0.3
%{_bindir}/prunedrpms
%{_bindir}/createdeltarpms
%{python_sitelib}/*

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-6m)
- add source

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.4-1m)
- import from Fedora

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Dec 21 2008 Jonathan Dieter <jdieter@gmail.com> - 0.3.4-1
- Change build system to standard python build system rather than make
- Update documentation (only about 18 months late)

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.3.3-3
- Rebuild for Python 2.6

* Wed Oct 15 2008 Jonathan Dieter <jdieter@gmail.com> - 0.3.3-2
- Bring in python in BR

* Thu Aug 14 2008 Jonathan Dieter <jdieter@gmail.com> - 0.3.3-1
- Include a number of patches to make accessible as a library

* Tue Aug 14 2007 Jonathan Dieter <jdieter@gmail.com> - 0.3.2-1
- Fix small bug that didn't allow certain deltarpms to be generated

* Wed Jul 11 2007 Jonathan Dieter <jdieter@gmail.com> - 0.3.1-1
- Fix typo in XML creation

* Wed Jul 11 2007 Jonathan Dieter <jdieter@gmail.com> - 0.3.0-1
- New XML structure (thanks, Jeremy)
- prunedrpms script added

* Tue Jun 19 2007 Jonathan Dieter <jdieter@gmail.com> - 0.2.0-1
- Initial release 
