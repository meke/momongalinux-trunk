%global momorel 1
%global gemname rabbit
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/rabbit-%{version}

Summary: A presentation tool using RD
Name: rabbit
Version: 2.0.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Productivity
#Source0: http://rabbit-shockers.org/download/rabbit-%{version}.tar.gz 
Source0: http://rubygems.org/gems/rabbit-%{version}.gem
NoSource: 0
URL: http://rabbit-shockers.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby-rdtool, ruby-gtk2, ruby-rttool
Requires: hikidoc
Requires: enscript
Requires: rubygem(gettext)
Requires: rubygem(twitter-stream)
Requires: rubygem(twitter_oauth)
Requires: rubygem(coderay)
BuildRequires: ruby-gettext-package
BuildArch: noarch

%description
This is an application to do presentation with RD document.

%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/


%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc %{gemdir}/doc/%{gemname}-%{version}
%{_bindir}/rabbi*
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sat Jan 19 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.6-1m)
- update 2.0.6

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.8-1m)
- update 1.0.8

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-1m)
- update 1.0.7

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-2m)
- Requre rubygem(gettext)

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Mon Aug 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- update 1.0.4

* Wed Jul 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-2m)
- Requires rubygem-coderay

* Sat Jul 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-1m)
- update 1.0.2

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3-1m)
- update 0.9.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2-1m)
- update 0.9.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-2m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1-1m)
- update 0.9.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.5-1m)
- update 0.6.5

* Sat Feb 27 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-1m)
- update 0.6.4

* Thu Dec 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.3-1m)
- update 0.6.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-2m)
- Require enscript

* Tue Oct  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-1m)
- update 0.6.2

* Tue Jul 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- update 0.6.1

* Tue Feb 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.9-1m)
- update 0.5.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.8-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.8-1m)
- update 0.5.8

* Tue Aug 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.7-1m)
- update 0.5.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-2m)
- %%NoSource -> NoSource

* Mon Nov  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.4.2-1m)
- update to 0.4.2

* Tue Oct 18 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.0-2m)
- requires ruby-gtk2 instead of ruby-gnome2

* Thu Sep  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.0-1m)
- version up

* Sun Jul 17 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.9-1m)
- version up

* Thu Jun  9 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.8-1m)
- version up

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.7-1m)
- version up

* Sun Dec 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.6-1m)
- version up

* Wed Oct  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.3-1m)
- initial import to Momonga
