%global         momorel 3

Name:           perl-SQL-Translator
Version:        0.11018
Release:        %{momorel}m%{?dist}
Summary:        Manipulate structured data definitions (SQL and more)
License:        GPL+
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SQL-Translator/
Source0:        http://www.cpan.org/authors/id/F/FR/FREW/SQL-Translator-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.005
BuildRequires:  perl-Carp-Clan
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-Class-Base
BuildRequires:  perl-Class-Data-Inheritable >= 0.02
BuildRequires:  perl-Class-MakeMethods
BuildRequires:  perl-DBI
BuildRequires:  perl-Digest-SHA1 >= 2
BuildRequires:  perl-ExtUtils-MakeMaker >= 6.11
BuildRequires:  perl-File-ShareDir >= 1
BuildRequires:  perl-GD
BuildRequires:  perl-Graph
BuildRequires:  perl-GraphViz
BuildRequires:  perl-IO
BuildRequires:  perl-IO-stringy >= 2.11
BuildRequires:  perl-Moo >= 0.009007
BuildRequires:  perl-Package-Variant >= 1.001001
BuildRequires:  perl-Parse-RecDescent >= 1.962.002
BuildRequires:  perl-PathTools
BuildRequires:  perl-Pod-Parser
BuildRequires:  perl-Spreadsheet-ParseExcel >= 0.41
BuildRequires:  perl-Template-Toolkit >= 2.2
BuildRequires:  perl-Test-Differences
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.6
BuildRequires:  perl-Text-ParseWords
BuildRequires:  perl-Text-RecordParser >= 0.02
BuildRequires:  perl-XML-LibXML >= 1.69
BuildRequires:  perl-XML-Writer >= 0.5
BuildRequires:  perl-YAML >= 0.66
Requires:       perl-Carp-Clan
Requires:       perl-Class-Accessor
Requires:       perl-Class-Base
Requires:       perl-Class-Data-Inheritable >= 0.02
Requires:       perl-Class-MakeMethods
Requires:       perl-DBI
Requires:       perl-Digest-SHA1 >= 2
Requires:       perl-ExtUtils-MakeMaker >= 6.11
Requires:       perl-File-ShareDir >= 1
Requires:       perl-GD
Requires:       perl-Graph
Requires:       perl-GraphViz
Requires:       perl-IO
Requires:       perl-IO-stringy >= 2.11
Requires:       perl-Moo >= 0.009007
Requires:       perl-Package-Variant >= 1.001001
Requires:       perl-Parse-RecDescent >= 1.962.002
Requires:       perl-PathTools
Requires:       perl-Pod-Parser
Requires:       perl-Spreadsheet-ParseExcel >= 0.41
Requires:       perl-Template-Toolkit >= 2.2
Requires:       perl-Test-Differences
Requires:       perl-Test-Exception
Requires:       perl-Test-Simple >= 0.6
Requires:       perl-Text-ParseWords
Requires:       perl-Text-RecordParser >= 0.02
Requires:       perl-XML-LibXML >= 1.69
Requires:       perl-XML-Writer >= 0.5
Requires:       perl-YAML >= 0.66
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This documentation covers the API for SQL::Translator. For a more general
discussion of how to use the modules and scripts, please see
SQL::Translator::Manual.

%prep
%setup -q -n SQL-Translator-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(:)/d' | \
  sed -e '/^perl(SQL::Translator::Schema::Graph::CompoundEdge/d' | \
  sed -e '/^perl(SQL::Translator::Schema::Graph::Edge/d' | \
  sed -e '/^perl(SQL::Translator::Schema::Graph::HyperEdge/d' | \
  sed -e '/^perl(SQL::Translator::Schema::Graph::Node/d' | \
  sed -e '/^perl(SQL::Translator::Schema::Graph::Port/d'

EOF
%define __perl_requires %{_builddir}/SQL-Translator-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/sqlt
%{_bindir}/sqlt-diagram
%{_bindir}/sqlt-diff
%{_bindir}/sqlt-diff-old
%{_bindir}/sqlt-dumper
%{_bindir}/sqlt-graph
%{perl_vendorlib}/auto/share/dist/SQL-Translator
%{perl_vendorlib}/SQL/Translator
%{perl_vendorlib}/SQL/Translator.pm
%{perl_vendorlib}/Test/SQL/Translator.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11018-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11018-2m)
- rebuild against perl-5.18.2

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11018-1m)
- update to 0.11018

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11016-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11016-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11016-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11016-2m)
- rebuild against perl-5.16.2

* Wed Oct 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11016-1m)
- update to 0.11016

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11015-1m)
- update to 0.11015

* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11013-1m)
- update to 0.11013

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11012-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11012-1m)
- update to 0.11012
- rebuild against perl-5.16.0

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11010-1m)
- update to 0.11010

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11009-2m)
- rebuild against perl-5.14.2

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11009-1m)
- update to 0.11009

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11008-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11008-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11008-1m)
- update to 0.11008

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11007-2m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11007-1m)
- update to 0.11007

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11006-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11006-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11006-2m)
- full rebuild for mo7 release

* Fri Jun 04 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11006-1m)
- update to 0.11006
- Specfile re-generated by cpanspec 1.78.

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11005-4m)
- rebuild against perl-5.12.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11005-3m)
- add BuildRequires:	perl-Digest-SHA1
- add BuildRequires:	perl-DBI

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11005-2m)
- rebuild against perl-5.12.0

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11005-1m)
- update to 0.11005

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11003-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11003-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11003-1m)
- update to 0.11003

* Mon Aug 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11002-1m)
- update to 0.11002

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11001-2m)
- rebuild against perl-5.10.1

* Sat Aug 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11001-1m)
- update to 0.11001

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10000-1m)
- update to 0.10

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09007-1m)
- update to 0.09007

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09006-1m)
- update to 0.09006

* Wed Feb 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09004-1m)
- update to 0.09004

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-1m)
- update to 0.09003

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09001-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09001-1m)
- update to 0.09001

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09000-1m)
- update to 0.09000

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0900-2m)
- rebuild against gcc43

* Tue Feb 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0900-1m)
- update to 0.0900

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-1m)
- update to 0.08001

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.08-2m)
- use vendor

* Wed Dec 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Fri Sep 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.07-1m)
- spec file was autogenerated
