%global git cd5a5dd
%global uuid system-monitor@paradoxxx.zero.gmail.com
%global github paradoxxxzero-gnome-shell-system-monitor-applet
%global git_date 20121117
%global short_name system-monitor-applet
%global momorel 1

Name:           gnome-shell-extension-system-monitor-applet
Version:        0
Release:        0.%{git_date}.%{momorel}m%{?dist}
Summary:        A Gnome shell system monitor extension

Group:          User Interface/Desktops
License:        GPLv3+
URL:            https://github.com/paradoxxxzero/gnome-shell-system-monitor-applet
# https://github.com/paradoxxxzero/gnome-shell-system-monitor-applet/tarball/master/paradoxxxzero-gnome-shell-system-monitor-applet-cd5a5dd.tar.gz

Source0:        %{github}-%{git}.tar.gz
BuildArch:      noarch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       gnome-shell >= 3.6
Requires:       python
Requires:       pygobject >= 3.0.4

Requires(postun): /usr/bin/glib-compile-schemas
Requires(posttrans): /usr/bin/glib-compile-schemas


%description
Display system information in gnome shell status bar, such as memory usage,
CPU usage, and network rate...


%prep
%setup -q -n %{github}-%{git}


%build
#Build translations
cd po
for d in `ls -d */` ; do
  cd $d
  rm -f *.mo
  # FIXME: horrible hack to rename errors pot file to po file
  if test -f *.pot -a ! -f *.po ; then 
    for f in *.pot ; do 
      mv $f `basename $f .pot`.po
    done
  fi
  # Since rpm-build package needs grep package
  if $(ls ./ | grep -q .po) ; then
    for f in *.po ; do
      msgfmt -o `basename $f .po`.mo $f
    done
  fi
  cd ..
done
cd ..

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/gnome-shell/extensions/%{uuid}
mkdir -p %{buildroot}%{_datadir}/glib-2.0/schemas/
install -Dp -m 0644 %{uuid}/{compat.js,convenience.js,extension.js,metadata.json,prefs.js,stylesheet.css} \
  %{buildroot}%{_datadir}/gnome-shell/extensions/%{uuid}/
install -Dp -m 0644 %{uuid}/schemas/org.gnome.shell.extensions.system-monitor.gschema.xml \
  %{buildroot}%{_datadir}/glib-2.0/schemas/

# Translations.
pushd po
for d in `ls -d */` ; do
  pushd $d
  # Since rpm-build package needs grep package
  if $(ls ./ | grep -q .mo) ; then
    for f in *.mo ; do
      install -pD -m 0644 $f %{buildroot}%{_datadir}/locale/$d/LC_MESSAGES/%{short_name}.mo
    done
  fi
  popd
done
popd
%find_lang %{short_name}

%postun
if [ $1 -eq 0 ] ; then
     glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files -f %{short_name}.lang
%defattr(-,root,root,-)
%doc README.md
%{_datadir}/gnome-shell/extensions/%{uuid}/
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.system-monitor.gschema.xml


%changelog
* Fri Dec 21 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0-0.20121117.1m)
- update to 0-0.20121117(commit cd5a5dd)

* Mon Mar 26 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0-0.20120229.3m)
- add patch0

* Mon Mar 26 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0-0.20120229.2m)
- revise desktop file

* Thu Mar 22 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0-0.20120229.1m)
- initial build
