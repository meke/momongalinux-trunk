%global momorel 23

%define pkgname server-utils

Summary: X.Org X11 X server utilities
Name: xorg-x11-%{pkgname}
Version: 1.0.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# FIXME: check if still needed for X11R7
Requires(pre): filesystem >= 2.4.6

Requires: xorg-x11-iceauth
Requires: xorg-x11-rgb
Requires: xorg-x11-sessreg
Requires: xorg-x11-xcmsdb
Requires: xorg-x11-xgamma
Requires: xorg-x11-xhost
Requires: xorg-x11-xmodmap
Requires: xorg-x11-xrandr
Requires: xorg-x11-xrdb
Requires: xorg-x11-xrefresh
Requires: xorg-x11-xset
Requires: xorg-x11-xsetmode
Requires: xorg-x11-xsetpointer
Requires: xorg-x11-xsetroot
Requires: xorg-x11-xstdcmap
Requires: xorg-x11-xtrap
Requires: xorg-x11-xvidtune

%description
A collection of utilities used to tweak and query the runtime configuration
of the X server.

%prep

%build

%install
rm -rf --preserve-root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-23m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-22m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-21m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-19m)
- remove Requires: xorg-x11-lbxproxy

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-18m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-17m)
- rebuild against gcc43

* Fri Sep 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-16m)
- meta package for xorg-x11-iceauth xorg-x11-lbxproxy xorg-x11-rgb
-- xorg-x11-sessreg xorg-x11-xcmsdb xorg-x11-xgamma xorg-x11-xhost
-- xorg-x11-xmodmap xorg-x11-xrandr xorg-x11-xrdb xorg-x11-xrefresh
-- xorg-x11-xset xorg-x11-xsetmode xorg-x11-xsetpointer xorg-x11-xsetroot
-- xorg-x11-xstdcmap xorg-x11-xtrap xorg-x11-xvidtune

* Tue Aug 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-15m)
- update xset-1.0.3

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-14m)
- update xmodmap-1.0.3 xsetroot-1.0.2 xrdb-1.0.4 

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-13m)
- update sessreg-1.0.3

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-12m)
- update xhost-1.0.2, iceauth-1.0.2

* Mon Jul 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-11m)
- update xrandr-1.2.2

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-10m)
- update xrandr-1.2.0

* Wed Jan 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-9m)
- update sassreg-1.0.2

* Wed Jan 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-8m)
- update xrdb-1.0.3

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-7m)
- update xmodmap-1.0.2

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-6m)
- delete duplicated files

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-5m)
- update xhost-1.0.1 xrefresh-1.0.2

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-4m)
- update rgb-1.0.1

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-3m)
- update xmodmap-1.0.1, xrandr-1.0.2, xrdb-1.0.2, xset-1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Thu Mar  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.3m)
- comment out Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga
- add BuildRequires: libXrandr-devel checking for XRANDR

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated all packages to the versions from X11R7.0

* Mon Nov 28 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all packages to version 1.0.0 from X11R7 RC4
- Changed manpage dirs from man1x to man1 to match upstream RC4 default.
- Updated lbxproxy-datadir-AtomControl-fix.patch
- Updated rgb-1.0.0-datadir-rgbpath-fix.patch
- Removed xvidtune-0.99.1-datadir-app-defaults-fix.patch, now unneeded.

* Mon Nov 28 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-6
- Added "Requires: cpp" as xrdb requires it for proper operation (#174302)

* Wed Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-5
- Bump "filesystem" Requires(pre) to 2.3.7-1.

* Fri Nov 18 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-4
- Added lbxproxy-0.99.2-datadir-AtomControl-fix.patch to move architecture
  independent data files from libdir to datadir.
- Added rgb-0.99.2-datadir-rgbpath-fix.patch to move architecture independent
  data files from libdir to datadir.
- Added xvidtune-0.99.1-datadir-app-defaults-fix.patch to move app-defaults
  into datadir.

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.2-3
- require newer filesystem package (#172610)

* Sun Nov 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Added "Obsoletes: XFree86, xorg-x11", as these utilities came from there.
- Rebuild against new libXaw 0.99.2-2, which has fixed DT_SONAME. (#173027)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Initial build, with all apps taken from X11R7 RC2
- Use "make install DESTDIR=$RPM_BUILD_ROOT" as the makeinstall macro fails on
  some packages.
