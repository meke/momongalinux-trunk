%global	momorel 1
Summary: A library for password generation and password quality checking
Name: libpwquality
Version: 1.2.3
Release: %{momorel}m%{?dist}
# The package is BSD licensed with option to relicense as GPL+
# - this option is redundant as the BSD license allows that anyway.
License: BSD or GPL+
Group: System Environment/Base
Source0: http://fedorahosted.org/releases/l/i/libpwquality/libpwquality-%{version}.tar.bz2
NoSource: 0
%global _moduledir %{_libdir}/security
%global _secconfdir %{_sysconfdir}/security

Requires: cracklib-dicts >= 2.8
Requires: pam%{?_isa}
BuildRequires: cracklib-devel
BuildRequires: gettext
BuildRequires: pam-devel
BuildRequires: python2-devel

URL: http://libpwquality.fedorahosted.org/

# we don't want to provide private python extension libs
%define __provides_exclude_from ^%{python_sitearch}/.*\.so$.

%description
This is a library for password quality checks and generation
of random passwords that pass the checks.
This library uses the cracklib and cracklib dictionaries
to perform some of the checks.

%package devel
Group: Development/Libraries
Summary: Files needed for developing PAM-aware applications and modules for PAM
Requires: libpwquality%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description devel
Files needed for development of applications using the libpwquality
library.
See the pwquality.h header file for the API.

%package -n python-pwquality
Group: Development/Libraries
Summary: Python bindings for the libpwquality library
Requires: libpwquality%{?_isa} = %{version}-%{release}

%description -n python-pwquality
This is pwquality Python module that provides Python bindings
for the libpwquality library. These bindings can be used
for easy password quality checking and generation of random
pronounceable passwords from Python applications.

%prep
%setup -q

%build
%configure \
	--with-securedir=/%{_libdir}/security \
	--with-pythonsitedir=%{python_sitearch} \
	--disable-static
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_moduledir}/*.la

%find_lang libpwquality

%check
# Nothing yet

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f libpwquality.lang
%defattr(-,root,root,-)
%doc COPYING README NEWS AUTHORS
%{_bindir}/pwmake
%{_bindir}/pwscore
%{_moduledir}/pam_pwquality.so
%{_libdir}/libpwquality.so.*
%config(noreplace) %{_secconfdir}/pwquality.conf
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/pwquality.h
%{_libdir}/libpwquality.so
%{_libdir}/pkgconfig/*.pc

%files -n python-pwquality
%defattr(-,root,root,-)
%{python_sitearch}/pwquality.so

%changelog
* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-1m)
- update 1.2.3

* Sat Jul 20 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-1m)
- update 1.2.2

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-1m)
- import from fedora
