%global momorel 4
%global svndate 20110515

%define osmo          Osmo4
%define svn           20110515
%define with_amr      0
%define with_mozilla  0
%define with_static   1
%define with_osmo     0
# Mozilla stuff fails. It's completely disabled for now.
%global gecko_ver 1.9
%global gecko_dirnum 1.9pre
%global firefox_ver 3.0b5
%define xuldir        %{_datadir}/idl/xulrunner-sdk-1.9pre
%define xulbindir     %{_libdir}/xulrunner-%{gecko_ver}pre


Summary: Multimedia framework covering MPEG-4, VRML/X3D and SVG.
Name: gpac
Version: 0.4.6
Release: 0.0.%{svndate}.%{momorel}m%{?dist}
License: LGPLv2+
URL: http://gpac.sourceforge.net/
Group: Applications/Multimedia
#Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
#NoSource: 0
Source0: gpac-%{svndate}.tar.xz
Patch0: gpac-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: SDL freetype
Requires: faad2 libmad
# Requires: ffmpeg >= 0.4.9-0.20071219
Requires: wxGTK >= 2.8.6
BuildRequires: ImageMagick
BuildRequires: SDL-devel
BuildRequires: faad2-devel = 2.7
# BuildRequires: ffmpeg-devel(DO NOT SET : Build-Loop)
BuildRequires: freeglut-devel
BuildRequires: freetype-devel
# BuildRequires: gecko-devel = %{gecko_ver}
BuildRequires: libGLU-devel
BuildRequires: libjpeg >= 8a
BuildRequires: libmad
BuildRequires: libogg-devel
BuildRequires: librsvg2-devel
BuildRequires: libtheora-devel
BuildRequires: libvorbis-devel
BuildRequires: libxml2-devel
BuildRequires: openjpeg-devel >= 1.5.0
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: wxGTK-devel >= 2.8.6
%ifarch x86_64 ia64 alpha sparc64 ppc64
Provides: libgpac.so()(64bit)
%else
Provides: libgpac.so
%endif

%description
GPAC is a multimedia framework for MPEG-4, VRML/X3D and SVG/SMIL. GPAC
is built upon an implementation of the MPEG-4 Systems standard
(ISO/IEC 14496-1) developed from scratch in C.

The main development goal is to provide a clean (a.k.a. readable by as
manypeople as possible), small and flexible alternative to the MPEG-4
Systems reference software (known as IM1 and distributed in ISO/IEC
14496-5).

The second development goal is to achieve integration of recent
multimedia standards (SVG/SMIL, VRML, X3D, SWF, etc) into a single
framework.  This stage is still under drafting but GPAC already
supports most of VRML97, some X3D as well as very simple SVG.

GPAC already features 2D and 3D multimedia playback, MPEG-4 Systems
encoders/multiplexers and publishing tools for content distribution.

GPAC is licensed under the GNU Lesser General Public License.

%package devel
Summary: Header files and libraries for a development with %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Header files and libraries for a development with %{name}

%prep
%setup -q -n gpac
%patch0 -p1 -b .linking

# Update doxygen
pushd doc
doxygen -u
popd

# Fix encoding warnings
cp -p Changelog Changelog.origine
iconv -f ISO-8859-1 -t UTF8 Changelog.origine >  Changelog
touch -r Changelog.origine Changelog
rm -rf Changelog.origine

cp -p doc/ipmpx_syntax.bt doc/ipmpx_syntax.bt.origine
iconv -f ISO-8859-1 -t UTF8 doc/ipmpx_syntax.bt.origine >  doc/ipmpx_syntax.bt
touch -r doc/ipmpx_syntax.bt.origine doc/ipmpx_syntax.bt
rm -rf doc/ipmpx_syntax.bt.origine

chmod +x configure

%build
%configure \
  --enable-debug \
  --extra-cflags="$RPM_OPT_FLAGS" \
  --libdir=%{_libdir} \
  --enable-oss-audio \
  --use-ffmpeg=no \
%if %with_mozilla
  --mozdir=%{_libdir}/mozilla/plugins \
%endif
%if %{with_amr}
  --enable-amr \
%endif
  --disable-xvid \
  --disable-static
#  --extra-cflags '-fshort-wchar -I/usr/include/xulrunner-sdk-1.9pre/stable -I/usr/include/nspr4' \
  # ^ pkg-config libxul --cflags

##
## Osmo-zila plugin.
##
%if %{with_mozilla}
#
# Rebuild osmozilla.xpt
pushd applications/osmozilla
%{xulbindir}/xpidl -m header -I%{xuldir}/stable -I%{xuldir}/unstable nsIOsmozilla.idl
%{xulbindir}/xpidl -m typelib -I%{xuldir}/stable -I%{xuldir}/unstable nsIOsmozilla.idl
%{xulbindir}/xpt_link nposmozilla.xpt nsIOsmozilla.xpt
mv nsIOsmozilla.xpt nsIOsmozilla.xpt_linux
popd 

## kwizart - osmozilla parallel make fails
# %{?_smp_mflags}
#make -C applications/osmozilla   \
#  OPTFLAGS="%optflags -fPIC -I%{_includedir}/nspr4/"     \
#  INCLUDES="-I%{_datadir}/idl/firefox-%{mozver}/       \
#    -I%{_includedir}/firefox-%{mozver}/       \
#    -I%{_includedir}/firefox-%{mozver}/xpcom    \
#    -I%{_includedir}/nspr4/ $INCLUDES"       \
#  XPIDL_INCL="-I%{_datadir}/idl/firefox-%{mozver}/     \
#    -I%{_includedir}/firefox-%{mozver}/       \
#    -I%{_includedir}/firefox-%{mozver}/xpcom    \
#    -I%{_includedir}/nspr4/ $INCLUDES"       \
#  install
%endif

# Parallele build will fail
make all OPTFLAGS="$RPM_OPT_FLAGS -fPIC -DPIC"
#{?_smp_mflags}
make sggen OPTFLAGS="$RPM_OPT_FLAGS -fPIC -DPIC" 
#{?_smp_mflags}

## kwizart - build doxygen doc for devel
pushd doc
doxygen
popd

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install install-lib moddir=%{_libdir}/%{name} libdir=%{_lib}

%if %{with_mozilla}
## kwizart - Install osmozilla plugin - make instmoz disabled.
mkdir -p %{buildroot}%{_libdir}/mozilla/{plugins,components}
install -m 755 bin/gcc/nposmozilla.so %{buildroot}%{_libdir}/mozilla/plugins/nposmozilla.so
install -m 755 bin/gcc/nposmozilla.xpt %{buildroot}%{_libdir}/mozilla/components/nposmozilla.xpt
%endif

%if %{with_osmo}
# Desktop menu Osmo4
mkdir -p %{buildroot}%{_datadir}/applications
cat > %{osmo}.desktop <<EOF
[Desktop Entry]
Name=Osmo4 Media Player
GenericName=Media Player
Comment=MPEG-4 Media Player
Exec=%{osmo}
Terminal=false
Icon=%{osmo}.xpm
Type=Application
Encoding=UTF-8
Categories=Application;AudioVideo;Player;
EOF

desktop-file-install --vendor livna \
  --dir %{buildroot}%{_datadir}/applications \
  --mode 644 \
  %{osmo}.desktop

#icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 0644 applications/osmo4_wx/osmo4.xpm %{buildroot}%{_datadir}/pixmaps/%{osmo}.xpm
%else
rm -rf %{buildroot}%{_bindir}/%{osmo}
%endif

## kwizart - rpmlint gpac no-ldconfig-symlink
mv %{buildroot}%{_libdir}/libgpac.so.%{version}-DEV %{buildroot}%{_libdir}/libgpac.so.%{version}
ln -sf libgpac.so.%{version} %{buildroot}%{_libdir}/libgpac.so.0
ln -sf libgpac.so.0 %{buildroot}%{_libdir}/libgpac.so

# Don't provide libgpac.so twice
rm -rf  %{buildroot}%{_libdir}/gpac/libgpac.so

#Install generated sggen binaries
for b in MPEG4 SVG X3D; do
  pushd applications/generators/${b}
    install -pm 0755 ${b}Gen %{buildroot}%{_bindir}
  popd
done

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-, root, root)
%doc AUTHORS BUGS Changelog COPYING README TODO
%{_bindir}/*
%{_libdir}/*.so.*
%{_libdir}/gpac
%{_datadir}/gpac
%{_mandir}/man1/*

%files devel
%defattr(-, root, root)
%{_includedir}/gpac
%{_libdir}/*.so
%{_libdir}/libgpac_static.a

%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-0.0.20110515.4m)
- rebuild for librsvg2 2.36.1

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-0.0.20110515.3m)
- rebuild against openjpeg-1.5.0

* Tue May 17 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-0.0.20110515.2m)
- fix build on x86_64

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-0.0.20110515.1m)
- update 20110515-snapshot

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-0.0.20100729.5m)
- rebuild for new GCC 4.6

* Tue Dec  7 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (0.4.6-0.0.20100729.4m)
- disable xvid, becase xvid is nonfree

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-0.0.20100729.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.6-0.0.20100729.2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.6-0.0.20100729.1m)
- update to svn snapshot

* Sat May 15 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.6-0.0.20100508.1m)
- update 0.4.6-0.0.20100508

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.13m)
- explicitly link libgtk-x11-2.0, libgdk-x11-2.0, libX11 and libstdc++

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.12m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.11m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.10m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.9m)
- apply glibc210 patch

* Wed Sep  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.5-0.0.20080217.8m)
- rebuild against libjpeg-7

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-0.0.20080217.7m)
- rebuild against faad2-2.7

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-0.0.20080217.5m)
- rebuild against rpm-4.6

* Mon Sep 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-0.0.20080217-4m)
- [BUILD FIX] BuildRequires: xvid >= 1.1.3-5m

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.5-0.0.20080217-3m)
- rebuild against openssl-0.9.8h-1m

* Fri May 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.5-0.0.20080217.2m)
- --use-ffmpeg=no since build fails with latest ffmpeg

* Fri May 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.5-0.0.20080217.1m)
- use cvs snapshot 20080217 and disabled mozilla/firefox stuff
- - patches, %%setup, and %%build are taken from http://rpm.livna.org/

* Wed May 07 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.4-11m)
- dont use system gecko files. (build fails with firefox-3)

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-10m)
- rebuild against firefox-3

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.4-9m)
- rebuild against firefox-2.0.0.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-8m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.4.4-7m)
- rebuild against firefox-2.0.0.13

* Sat Mar 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-6m)
- add workaround for ffmpeg-devel

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-5m)
- rebuild against firefox-2.0.0.12
-- replace %%mozver with %%firefox_ver

* Wed Dec 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-4m)
- rebuild against ffmpeg-0.4.9-0.20071219

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.4-3m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.4-2m)
- rebuild against firefox-2.0.0.10

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-1m)
- version 0.4.4 stable release build with wxGTK-2.8.6
- update dont-install-under-home.patch
- build with optflags
- enable parallel build

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.3-0.20070222.10m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.3-0.20070222.9m)
- rebuild against firefox-2.0.0.8

* Wed Sep 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.3-0.20070222.8m)
- rebuild against firefox-2.0.0.7

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-0.20070222.7m)
- rebuild against libvorbis-1.2.0-1m
- rebuild against firefox-2.0.0.6

* Sat Jul 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3-0.20070222.6m)
- rebuild against firefox-2.0.0.5

* Sat Jun  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3-0.20070222.5m)
- rebuild against firefox-2.0.0.4

* Tue Apr 24 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.3-0.20070222.4m)
- remove BuildRequires ffmpeg-devel due to build-loop

* Sun Apr 22 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-0.20070222.3m)
- add patch1: gpac-dont-install-under-home.patch

* Sat Apr 21 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.3-0.20070222.2m)
- use system gecko.
- x86_64 and other arch support.

* Fri Apr 06 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.3-0.20070222.1m)
- initial spec file based on the one in the original source

* Wed Jul 13 2005 Jean Le Feuvre
- Updated for GPAC LGPL release
* Mon Aug 09 2004 Sverker Abrahamsson <sverker@abrahamsson.com>
- Initial RPM release
