%global momorel 2
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-pcre
Version:        6.2.5
Release:        %{momorel}m%{?dist}

Summary:        Perl compatibility regular expressions (PCRE) for OCaml

Group:          Development/Libraries
License:        LGPLv2
URL:            http://www.ocaml.info/home/ocaml_sources.html#pcre-ocaml
Source0:        https://bitbucket.org/mmottl/pcre-ocaml/downloads/pcre-ocaml-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  pcre-devel >= 8.31
BuildRequires:  gawk


%description
Perl compatibility regular expressions (PCRE) for OCaml.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
# This isn't quite right - we need to specify same architecture of pcre-devel
Requires:       pcre-devel


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n pcre-ocaml-%{version}


%build
%if %opt
make all
%else
make -C lib byte-code-library
%endif
strip lib/dll*.so


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
%if %opt
make NATIVE=true install
%else
make LIBINSTALL_FILES="pcre.mli pcre.cmi pcre.cma libpcre_stubs.a dllpcre_stubs.so" install
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE README.txt
%{_libdir}/ocaml/pcre
%if %opt
%exclude %{_libdir}/ocaml/pcre/*.a
%exclude %{_libdir}/ocaml/pcre/*.cmxa
%endif
%exclude %{_libdir}/ocaml/pcre/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc Changelog LICENSE README.txt
%if %opt
%{_libdir}/ocaml/pcre/*.a
%{_libdir}/ocaml/pcre/*.cmxa
%endif
%{_libdir}/ocaml/pcre/*.mli


%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2.5-2m)
- rebuild against pcre-8.31

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2.5-1m)
- update to 6.2.5

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2.3-1m)
- update to 6.2.3
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1.0-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.0-1m)
- update to 6.1.0
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0.1-1m)
- update to 6.0.1

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0.0-1m)
- update to 6.0.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.15.1-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.15.1-1m)
- update to 5.15.1
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.14.0-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 5.14.0-2
- Rebuild for OCaml 3.10.2

* Mon Apr 21 2008 Richard W.M. Jones <rjones@redhat.com> - 5.14.0-1
- New upstream release 5.14.0.
- -devel subpackage should depend on pcre-devel.
- Fixed upstream URL.
- Changed to use .bz2 package.

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 5.13.0-2
- Rebuild for ppc64.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 5.13.0-1
- New upstream version 5.13.0.
- Rebuild for OCaml 3.10.1.

* Tue Sep 18 2007 Richard W.M. Jones <rjones@redhat.com> - 5.12.2-1
- New upstream version 5.12.2.
- Clarified license is LGPLv2.
- Strip .so file.

* Thu Sep  6 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-9
- Force rebuild because of updated requires/provides scripts in OCaml.

* Mon Sep  3 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-8
- Force rebuild because of base OCaml.

* Thu Aug 30 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-7
- Force rebuild because of changed BRs in base OCaml.

* Wed Aug  1 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-6
- ExcludeArch ppc64

* Mon Jun 11 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-5
- Updated to latest packaging guidelines.

* Sat Jun  2 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-4
- Handle bytecode-only architectures.

* Sat May 26 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-3
- Put the stubs in stublibs subdirectory.

* Fri May 25 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-2
- Use ocaml find-requires, find-provides

* Sat May 19 2007 Richard W.M. Jones <rjones@redhat.com> - 5.11.4-1
- Initial RPM release.
