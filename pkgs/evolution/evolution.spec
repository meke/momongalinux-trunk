%global momorel 1

%define glib2_version 2.30.0
%define gnome_desktop_version 2.91.3
%define gnome_doc_utils_version 0.8.0
%define gnome_icon_theme_version 2.30.2.1
%define gtk3_version 3.2.0
%define gtkhtml_version 4.3.1
%define intltool_version 0.35.5
%define libgdata_version 0.10.0
%define libgweather_version 3.5.92
%define clutter_gtk_version 0.10
%define soup_version 2.4.0
%define webkit_version 1.9.92

%define evo_base_version 3.6

%define last_anjal_version 0.3.2-3
%define last_libgal2_version 2:2.5.3-2
%define last_evo_nm_version 3.4.3

# !!FIXME!! we disables temporary
%define inline_audio_support 0
%define ldap_support 1
%define libnotify_support 1
# !!FIXME!! we disables temporary
%define libpst_support 0
%define krb5_support 1

%define evo_plugin_dir %{_libdir}/evolution/%{evo_base_version}/plugins

### Abstract ###

Name: evolution
Version: 3.6.0
Release: %{momorel}m%{?dist}
Group: Applications/Productivity
Summary: Mail and calendar client for GNOME
License: GPLv2+ and GFDL
URL: http://projects.gnome.org/evolution/
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Source: http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Obsoletes: anjal <= %{last_anjal_version}
Obsoletes: libgal2 <= %{last_libgal2_version}
Obsoletes: evolution-NetworkManager <= %{last_evo_nm_version}

### Patches ###

# bad hack
Patch01: evolution-1.4.4-ldap-x86_64-hack.patch

# RH bug #589555
Patch02: evolution-2.30.1-help-contents.patch

#Patch03: evolution-3.5.3-fix-itip-module.patch

## Dependencies ###

Requires: evolution-data-server >= %{version}
Requires: gnome-icon-theme >= %{gnome_icon_theme_version}
Requires: gvfs

### Build Dependencies ###

BuildRequires: atk-devel
BuildRequires: autoconf >= 2.59
BuildRequires: automake >= 1.9
BuildRequires: bison
BuildRequires: cairo-gobject-devel
BuildRequires: clutter-gtk-devel >= %{clutter_gtk_version}
BuildRequires: dbus-glib-devel
BuildRequires: desktop-file-utils
BuildRequires: evolution-data-server-devel >= %{version}
BuildRequires: gettext
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gnome-common
BuildRequires: gnome-desktop-devel >= %{gnome_desktop_version}
BuildRequires: gnome-doc-utils >= %{gnome_doc_utils_version}
BuildRequires: gnome-online-accounts
BuildRequires: gnutls-devel
BuildRequires: gtk-doc
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: gtkhtml3-devel >= %{gtkhtml_version}
BuildRequires: intltool >= %{intltool_version}
BuildRequires: libcanberra-devel
BuildRequires: libgdata-devel >= %{libgdata_version}
BuildRequires: libgweather-devel >= %{libgweather_version}
BuildRequires: libSM-devel
BuildRequires: libsoup-devel >= %{soup_version}
BuildRequires: libtool >= 1.5
BuildRequires: libxml2-devel
BuildRequires: nspr-devel
BuildRequires: nss-devel
BuildRequires: pkgconfig
BuildRequires: rarian-compat
BuildRequires: unique3-devel
BuildRequires: webkitgtk3-devel >= %{webkit_version}

%if %{inline_audio_support}
BuildRequires: gstreamer-devel
%endif

%if %{ldap_support}
BuildRequires: openldap-devel >= 2.0.11 
%endif

%if %{krb5_support} 
BuildRequires: krb5-devel 
# tweak for krb5 1.2 vs 1.3
%define krb5dir /usr/kerberos
#define krb5dir `pwd`/krb5-fakeprefix
%endif

%if %{libnotify_support}
BuildRequires: libnotify-devel
%endif

%if %{libpst_support}
BuildRequires: libpst-devel
%endif
BuildRequires: libytnef-devel

%description
Evolution is the GNOME mailer, calendar, contact manager and
communications tool.  The components which make up Evolution
are tightly integrated with one another and act as a seamless
personal information-management tool.

%package devel
Group: Development/Libraries
Summary: Development files for building against %{name}
Requires: %{name} = %{version}-%{release}
Requires: evolution-data-server-devel >= %{version}
Requires: gtk3-devel >= %{gtk3_version}
Requires: gtkhtml3-devel >= %{gtkhtml_version}
Requires: libgdata-devel >= %{libgdata_version}
Requires: libgweather-devel >= %{libgweather_version}
Requires: libsoup-devel >= %{soup_version}
Requires: libxml2-devel
Obsoletes: libgal2-devel <= %{last_libgal2_version}

%description devel
Development files needed for building things which link against %{name}.

%package help
Group: Applications/Productivity
Summary: Help files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: yelp
BuildArch: noarch

%description help
This package contains user documentation for %{name}. 

%package bogofilter
Group: Applications/Productivity
Summary: Bogofilter plugin for Evolution
Requires: %{name} = %{version}-%{release}
Requires: bogofilter

%description bogofilter
This package contains the plugin to filter junk mail using Bogofilter.

%package spamassassin
Group: Applications/Productivity
Summary: SpamAssassin plugin for Evolution
Requires: %{name} = %{version}-%{release}
Requires: spamassassin

%description spamassassin
This package contains the plugin to filter junk mail using SpamAssassin.

%package perl
Group: Applications/Productivity
Summary: Supplemental utilities that require Perl
Requires: %{name} = %{version}-%{release}

%description perl
This package contains supplemental utilities for %{name} that require Perl.

%if %{libpst_support}
%package pst
Group: Applications/Productivity
Summary: PST importer plugin for Evolution
Requires: %{name} = %{version}-%{release}
Requires: libpst

%description pst
This package contains the plugin to import Microsoft Personal Storage Table
(PST) files used by Microsoft Outlook and Microsoft Exchange.
%endif

%prep
%setup -q -n evolution-%{version}
%patch01 -p1 -b .ldaphack
%patch02 -p1 -b .help-contents
#%patch03 -p1 -b .fix-itip-modules~

mkdir -p krb5-fakeprefix/include
mkdir -p krb5-fakeprefix/lib
mkdir -p krb5-fakeprefix/%{_lib}

# Remove the welcome email from Novell
for inbox in mail/default/*/Inbox; do
  echo -n "" > $inbox
done

%build
# define all of our flags, this is kind of ugly :(
%if %{ldap_support}
%define ldap_flags --with-openldap=yes
%else
%define ldap_flags --without-openldap
%endif

%if %{krb5_support}
%define krb5_flags --with-krb5=%{krb5dir}
%else
%define krb5_flags --without-krb5
%endif

%define ssl_flags --enable-nss=yes --enable-smime=yes

%if %{libpst_support}
%define pst_flags --enable-pst-import
%else
%define pst_flags --disable-pst-import
%endif

if ! pkg-config --exists nss; then 
  echo "Unable to find suitable version of mozilla nss to use!"
  exit 1
fi

CPPFLAGS="-I%{_includedir}/et"; export CPPFLAGS
CFLAGS="$RPM_OPT_FLAGS -fPIC -DLDAP_DEPRECATED -I%{_includedir}/et -Wno-sign-compare"; export CFLAGS

# Regenerate configure to pick up configure.ac changes
USE_GNOME2_MACROS=1 /usr/bin/gnome-autogen.sh || :

%configure \
	--disable-maintainer-mode \
	--disable-image-inline \
	--enable-gtk-doc \
	--with-sub-version=" (%{version}-%{release})" \
	--with-kde-applnk-path=no \
	%ldap_flags %krb5_flags %ssl_flags %pst_flags \
	--disable-weather \
	--enable-plugins=all
export tagname=CC
make %{?_smp_mflags} LIBTOOL=/usr/bin/libtool CFLAGS="$CFLAGS -fno-strict-aliasing"

# Strip unneeded translations from .mo files.
# This reduces the RPM size by several megabytes.
#disabled since 2.31.91 because of a msgmerge floating point exception (see RH bug 628073)
#cd po
#grep -v ".*[.]desktop[.]in[.]in$" POTFILES.in > POTFILES.keep
#mv POTFILES.keep POTFILES.in
#intltool-update --gettext-package=%{name}-%{evo_base_version} --pot
#for p in *.po; do
#	msgmerge $p %{name}-%{evo_base_version}.pot > $p.out
#	msgfmt -o `basename $p .po`.gmo $p.out
#done
#cd -

# Replace identical images in the help by links.
# This reduces the RPM size by several megabytes.
helpdir=$RPM_BUILD_ROOT%{_datadir}/gnome/help/%{name}
for f in $helpdir/C/figures/*.png; do
  b="$(basename $f)"
  for d in $helpdir/*; do
    if [ -d "$d" -a "$d" != "$helpdir/C" ]; then
      g="$d/figures/$b"
      if [ -f "$g" ]; then
        if cmp -s $f $g; then
          rm "$g"; ln -s "../../C/figures/$b" "$g"
        fi
      fi
    fi
  done
done


%install
rm -rf $RPM_BUILD_ROOT
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
export tagname=CC
make LIBTOOL=/usr/bin/libtool DESTDIR=$RPM_BUILD_ROOT install
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

# remove libtool archives for importers and the like
find $RPM_BUILD_ROOT/%{_libdir}/evolution -name '*.la' -exec rm {} \;

# remove statically built libraries:
find $RPM_BUILD_ROOT/%{_libdir}/evolution -name '*.a' -exec rm {} \;

# remove additional things we don't want
%if ! %{inline_audio_support}
%{__rm} -f $RPM_BUILD_ROOT%{_libdir}/evolution/%{evo_base_version}/modules/module-audio-inline.so
%endif

%find_lang evolution-%{evo_base_version} --all-name --with-gnome

grep "/usr/share/locale" evolution-%{evo_base_version}.lang > translations.lang

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf $RPM_BUILD_ROOT

%files -f translations.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README

# GSettings schemas:
%{_datadir}/GConf/gsettings/evolution.convert

%{_datadir}/glib-2.0/schemas/org.gnome.evolution.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.shell.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.addressbook.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.calendar.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.mail.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.importer.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.bogofilter.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.spamassassin.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.attachment-reminder.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.autocontacts.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.email-custom-header.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.external-editor.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.face-picture.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.itip.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.mail-notification.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.prefer-plain.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.publish-calendar.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.plugin.templates.gschema.xml


#%{_sysconfdir}/gconf/schemas/apps-evolution-attachment-reminder.schemas
#%{_sysconfdir}/gconf/schemas/apps-evolution-external-editor.schemas
#%{_sysconfdir}/gconf/schemas/apps-evolution-mail-notification.schemas
#%{_sysconfdir}/gconf/schemas/apps-evolution-mail-prompts-checkdefault.schemas
#%{_sysconfdir}/gconf/schemas/apps-evolution-template-placeholders.schemas
#%{_sysconfdir}/gconf/schemas/apps_evolution_addressbook.schemas
#%{_sysconfdir}/gconf/schemas/apps_evolution_calendar.schemas
#%{_sysconfdir}/gconf/schemas/apps_evolution_email_custom_header.schemas
#%{_sysconfdir}/gconf/schemas/apps_evolution_eplugin_face.schemas
#%{_sysconfdir}/gconf/schemas/apps_evolution_shell.schemas
#%{_sysconfdir}/gconf/schemas/evolution-bogofilter.schemas
#%{_sysconfdir}/gconf/schemas/evolution-mail.schemas
#%{_sysconfdir}/gconf/schemas/evolution-spamassassin.schemas

# The main executable
%{_bindir}/evolution

# Desktop files:
%{_datadir}/applications/evolution.desktop
%{_sysconfdir}/xdg/autostart/evolution-alarm-notify.desktop

# Icons:
%{_datadir}/icons/hicolor/16x16/apps/*
%{_datadir}/icons/hicolor/22x22/apps/*
%{_datadir}/icons/hicolor/24x24/apps/*
%{_datadir}/icons/hicolor/32x32/apps/*
%{_datadir}/icons/hicolor/48x48/apps/*
%{_datadir}/icons/hicolor/256x256/apps/*

# The main data directory
# (have not attempted to split this up into an explicit list)
%dir %{_datadir}/evolution
%{_datadir}/evolution/%{evo_base_version}

# Modules:
%dir %{_libdir}/evolution
%dir %{_libdir}/evolution/%{evo_base_version}
%dir %{_libdir}/evolution/%{evo_base_version}/modules
%{_libdir}/evolution/%{evo_base_version}/modules/module-addressbook.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-backup-restore.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-book-config-google.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-book-config-ldap.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-book-config-local.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-book-config-webdav.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-cal-config-caldav.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-cal-config-contacts.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-cal-config-google.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-cal-config-local.so
#%{_libdir}/evolution/%{evo_base_version}/modules/module-cal-config-weather.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-cal-config-webcal.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-calendar.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-composer-autosave.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-imap-features.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-itip-formatter.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-mail-config.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-mail.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-mailto-handler.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-mdn.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-offline-alert.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-online-accounts.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-prefer-plain.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-plugin-lib.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-plugin-manager.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-startup-wizard.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-text-highlight.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-tnef-attachment.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-vcard-inline.so
%{_libdir}/evolution/%{evo_base_version}/modules/module-web-inspector.so

%if %{inline_audio_support}
%{_libdir}/evolution/%{evo_base_version}/modules/module-audio-inline.so
%endif

# Shared libraries:
%{_libdir}/evolution/%{evo_base_version}/libcomposer.so
%{_libdir}/evolution/%{evo_base_version}/libeabutil.so
%{_libdir}/evolution/%{evo_base_version}/libecontacteditor.so
%{_libdir}/evolution/%{evo_base_version}/libecontactlisteditor.so
%{_libdir}/evolution/%{evo_base_version}/libemail-engine.so
%{_libdir}/evolution/%{evo_base_version}/libemail-utils.so
%{_libdir}/evolution/%{evo_base_version}/libemformat.so
%{_libdir}/evolution/%{evo_base_version}/libemiscwidgets.so
%{_libdir}/evolution/%{evo_base_version}/libeshell.so
%{_libdir}/evolution/%{evo_base_version}/libessmime.so
%{_libdir}/evolution/%{evo_base_version}/libetable.so
%{_libdir}/evolution/%{evo_base_version}/libetext.so
%{_libdir}/evolution/%{evo_base_version}/libetimezonedialog.so
%{_libdir}/evolution/%{evo_base_version}/libeutil.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-a11y.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-addressbook-importers.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-calendar.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-calendar-importers.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-mail-importers.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-mail.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-smime.so
%{_libdir}/evolution/%{evo_base_version}/libevolution-utils.so
%{_libdir}/evolution/%{evo_base_version}/libfilter.so
%{_libdir}/evolution/%{evo_base_version}/libgnomecanvas.so
%{_libdir}/evolution/%{evo_base_version}/libmenus.so

# Various libexec programs:
%dir %{_libexecdir}/evolution
%dir %{_libexecdir}/evolution/%{evo_base_version}
%{_libexecdir}/evolution/%{evo_base_version}/evolution-addressbook-export
%{_libexecdir}/evolution/%{evo_base_version}/evolution-alarm-notify
%{_libexecdir}/evolution/%{evo_base_version}/evolution-backup
%{_libexecdir}/evolution/%{evo_base_version}/killev

# The plugin directory:
%dir %{evo_plugin_dir}

# The various plugins follow; they are all part of the main package:
# (note that there are various resources such as ui and pixmap files that 
# are built as part of specific plugins but which are currently packaged using 
# globs above; the purpose of the separation below is to be more explicit about
# which plugins we ship)
%{evo_plugin_dir}/org-gnome-evolution-attachment-reminder.eplug
%{evo_plugin_dir}/liborg-gnome-evolution-attachment-reminder.so

%{evo_plugin_dir}/org-gnome-email-custom-header.eplug
%{evo_plugin_dir}/liborg-gnome-email-custom-header.so

%{evo_plugin_dir}/org-gnome-evolution-bbdb.eplug
%{evo_plugin_dir}/liborg-gnome-evolution-bbdb.so

%{evo_plugin_dir}/org-gnome-external-editor.eplug
%{evo_plugin_dir}/liborg-gnome-external-editor.so

%{evo_plugin_dir}/org-gnome-face.eplug
%{evo_plugin_dir}/liborg-gnome-face.so

%{evo_plugin_dir}/org-gnome-itip-formatter.eplug
%{evo_plugin_dir}/liborg-gnome-itip-formatter.so

%{evo_plugin_dir}/org-gnome-mailing-list-actions.eplug
%{evo_plugin_dir}/liborg-gnome-mailing-list-actions.so

%{evo_plugin_dir}/org-gnome-mail-notification.eplug
%{evo_plugin_dir}/liborg-gnome-mail-notification.so

%{evo_plugin_dir}/org-gnome-mail-to-task.eplug
%{evo_plugin_dir}/liborg-gnome-mail-to-task.so

%{evo_plugin_dir}/org-gnome-mark-all-read.eplug
%{evo_plugin_dir}/liborg-gnome-mark-all-read.so

%{evo_plugin_dir}/org-gnome-prefer-plain.eplug
%{evo_plugin_dir}/liborg-gnome-prefer-plain.so

%{evo_plugin_dir}/org-gnome-publish-calendar.eplug
%{evo_plugin_dir}/liborg-gnome-publish-calendar.so

%{evo_plugin_dir}/org-gnome-save-calendar.eplug
%{evo_plugin_dir}/liborg-gnome-save-calendar.so

%{evo_plugin_dir}/org-gnome-templates.eplug
%{evo_plugin_dir}/liborg-gnome-templates.so

%{evo_plugin_dir}/org-gnome-dbx-import.eplug
%{evo_plugin_dir}/liborg-gnome-dbx-import.so


#%{evo_plugin_dir}/liborg-gnome-addressbook-file.so
#%{evo_plugin_dir}/liborg-gnome-audio-inline.so
#%{evo_plugin_dir}/liborg-gnome-backup-restore.so
#%{evo_plugin_dir}/liborg-gnome-calendar-file.so
#%{evo_plugin_dir}/liborg-gnome-calendar-http.so
#%{evo_plugin_dir}/liborg-gnome-calendar-weather.so
#%{evo_plugin_dir}/liborg-gnome-default-source.so
#%{evo_plugin_dir}/liborg-gnome-evolution-caldav.so
#%{evo_plugin_dir}/liborg-gnome-evolution-google.so
#%{evo_plugin_dir}/liborg-gnome-evolution-webdav.so
#%{evo_plugin_dir}/liborg-gnome-imap-features.so
#%{evo_plugin_dir}/liborg-gnome-vcard-inline.so
#%{evo_plugin_dir}/org-gnome-addressbook-file.eplug
#%{evo_plugin_dir}/org-gnome-audio-inline.eplug
#%{evo_plugin_dir}/org-gnome-backup-restore.eplug
#%{evo_plugin_dir}/org-gnome-calendar-file.eplug
#%{evo_plugin_dir}/org-gnome-calendar-http.eplug
#%{evo_plugin_dir}/org-gnome-calendar-weather.eplug
#%{evo_plugin_dir}/org-gnome-default-source.eplug
#%{evo_plugin_dir}/org-gnome-evolution-caldav.eplug
#%{evo_plugin_dir}/org-gnome-evolution-google.eplug
#%{evo_plugin_dir}/org-gnome-evolution-webdav.eplug
#%{evo_plugin_dir}/org-gnome-imap-features.eplug
#%{evo_plugin_dir}/org-gnome-vcard-inline.eplug



%files devel
%defattr(-, root, root)
%{_datadir}/gtk-doc/html/eshell
%{_includedir}/evolution-%{evo_base_version}
%{_libdir}/pkgconfig/evolution-calendar-3.0.pc
%{_libdir}/pkgconfig/evolution-mail-3.0.pc
%{_libdir}/pkgconfig/evolution-plugin-3.0.pc
%{_libdir}/pkgconfig/evolution-shell-3.0.pc
%{_libdir}/pkgconfig/libemail-engine.pc
%{_libdir}/pkgconfig/libemail-utils.pc
%{_libdir}/pkgconfig/libevolution-utils.pc

%files help
%defattr(-, root, root)
%{_datadir}/help/*/evolution

%files bogofilter
%defattr(-, root, root)
%{_libdir}/evolution/%{evo_base_version}/modules/module-bogofilter.so

%files spamassassin
%defattr(-, root, root)
%{_libdir}/evolution/%{evo_base_version}/modules/module-spamassassin.so

%files perl
%defattr(-, root, root)
%{_libexecdir}/evolution/%{evo_base_version}/csv2vcard

%if %{libpst_support}
%files pst
%defattr(-, root, root)
%{evo_plugin_dir}/org-gnome-pst-import.eplug
%{evo_plugin_dir}/liborg-gnome-pst-import.so
%endif

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- evolution needs the same version of evolution-data-server

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3.1-2m)
- revise BuildRequires

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3.1-1m)
- update to 3.5.3.1

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3
- reimport from fedora

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-1m)
- update to 3.2.3

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-2m)
- remove BR hal-devel

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.91-2m)
- specify evolution-data-server version

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-3m)
- fix evolution-version

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- fix libgweather version

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.2-6m)
- rebuild against python-2.7

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-5m)
- rebuild against libnotify-0.7.2

* Thu Apr 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.2-4m)
- add a patch to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.2-3m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.32.2-2m)
- add BR libgtkimageview-devel

* Wed Feb  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-1m)
- update to 2.32.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Wed Oct 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- add BuildRequires

* Thu Oct 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- rebuild against mono-2.8

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.2-4m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Tue May 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1.2-3m)
- add BuildRequires

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.2-2m)
- add %%dir

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.2-1m)
- update to 2.30.1.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.29.91-2m)
- add patch for gtk-2.20 by gengtk220patch

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90
-- add --disable-pst-import

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- delete __libtoolize hack

* Mon Dec 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Thu May 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.1.1-2m)
- add autoreconf, need libtool-2.2

* Thu Apr 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1.1-1m)
- update to 2.26.1.1

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- fix evolution-data-server version

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Sat Jan 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4-1m)
- update to 2.24.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24.2-2m)
- rebuild against python-2.6.1

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Fri Nov  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1.1-1m)
- update to 2.24.1.1

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Oct  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.3.1-3m)
- rebuild against pilot-link-0.12.3-1m

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.3.1-2m)
- rebuild against gnutls-2.4.1

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3.1-1m)
- update to 2.22.3.1
- delete patch0 (fixed)

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3
- add patch0 (for gtk-doc)

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Mon May  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-1m)
- update to 2.22.1.1

* Tue Apr 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.3-2m)
- rebuild against openldap-2.4.8

* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Mon Nov 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Sun Aug 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-2m)
- fix evolution-devel requires:

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.19-1m)
- update to 2.9.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.91-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.91-2m)
- revice %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.91-1m)
- update to 2.9.91 (unstable)
- delete patch0 (merged)

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.3-2m)
- rebuild against pilot-link-0.12.1-1m
- add patch0 for new pilot-link

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Fri Feb  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.2.1-3m)
- rebuild without gstreamer-0.8.12

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.8.2.1-2m)
- disable mono for ppc64, alpha, sparc64

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2.1-1m)
- update to 2.8.2.1

* Fri Oct 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1.1-1m)
- update to 2.8.1.1

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.8.0-2m)
- rebuild against gnutls-1.4.4-1m

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-4m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.6.3-3m)
- rebuild against expat-2.0.0-1m

* Sat Aug 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-2m)
- create symbolic link (libeshell.so.0)

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Sun Aug 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-2m)
- rebuild against mono-core

* Fri Jun  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2
-- To main

* Sun May  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1
-- comment out all patches

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2.1-4m)
- rebuild against gnome-vfs

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2.1-3m)
- TO.Alter

* Wed Feb 22 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.2.1-2m)
- build with new nspr/nss

* Sun Jan 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2.1-1m)
- version up

* Tue Jan  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-3m)
- rebuild against dbus-0.60-2m

* Sun Dec 11 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-2m)
- add patch from FC 106 107 108 109 110 803 804
- add autoreconf
- add link evolution-2.4 to evolution
- disable nss (not work)

* Wed Nov 23 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- import from FC
- GNOME 2.12.1 Desktop

* Wed Oct 26 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-7
- Added a patch (110) to hide the component switcher buttons by default on new
  windows (#170799) by patching the GConf schema.
- Made list of installed schemas explicit.
- Own the plugins subdirectory

* Tue Oct 25 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-6
- use 4 separate .desktop files from the redhat-menus package, rather than the
  current single one; bump the redhat-menus requirement accordingly (from 1.13
  to 5.0.4); introduce a macro for this requirement.

* Mon Oct 24 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-5
- fix removal of upstream .desktop file (broke on upgrade to Evolution 2.2, and
  continued to be broken with 2.3/2.4) (#103826, again)

* Tue Oct 18 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-4
- updated patch 804 to declare e_calendar_table_process_completed_tasks

* Tue Oct 18 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-3
- added patch (804: evolution-2.4.1-fix-missing-declarations.patch) to fix
  missing declaration (thanks to Peter Robinson)

* Mon Oct 17 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-2
- bump e-d-s requirement to 1.4.1.1

* Tue Oct  4 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.1-1
- 2.4.1
- regenerate patch 101 to handle conflict in 
  calendar/gui.print.c: print_week_day_event introduced by fix to upstream bug
  244981 (end date added while printing in the week view); bump patch name from
  version 5 to version 6
- removed patch 804 (conduits-multi-day-crash); this is now in upstream tarball

* Wed Sep 14 2005 Jeremy Katz <katzj@redhat.com> - 2.4.0-2
- rebuild for mozilla on ppc64

* Wed Sep  7 2005 David Malcolm <dmalcolm@redhat.com> - 2.4.0-1
- 2.4.0
- Removed patch to fix implicit function declarations (patch 110, added in 
  2.3.8-1) as this is now upstream.

* Thu Sep  1 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.8-4
- Enable exchange support when configuring, so that the exchange-operations
  plugin gets built.

* Fri Aug 26 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.8-3
- Added patch for #157074 (patch 804)

* Fri Aug 26 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.8-2
- Move -Werror-implicit-function-declaration from configuration to the make
  stage, to avoid breaking configuration tests.

* Tue Aug 23 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.8-1
- 2.3.8
- add -Werror-implicit-function-declaration to CFLAGS and a patch to fix the 
  problems arising (patch 110)

* Tue Aug 16 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.7-3
- Introduce macro for gnome-pilot dependency, bumping from 2.0.6 to 2.0.13
- Add obsoletion of libgal2/libgal2-devel (dependency was removed in 2.3.6-1);
  based on the last EVR of the libgal2 package in CVS, 2:2.5.3-2

* Mon Aug 15 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.7-2
- rebuild

* Tue Aug  9 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.7-1
- 2.3.7
- Bump evolution-data-server requirement from 1.3.6 to 1.3.7
- Bump gtkhtml3 requirement from 3.6.2 to 3.7.6

* Mon Aug  8 2005 Tomas Mraz <tmraz@redhat.com> - 2.3.6.1-5
- rebuild with new gnutls

* Tue Aug  2 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.6.1-4
- Added patch to show correct mimetype for OpenOffice.org files when guessing 
  type for attachments with mimetype "application/octet-stream" (#164957)

* Mon Aug  1 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.6.1-3
- Improved version of evolution-2.3.5.1-fix-150458.patch (#150458)

* Sat Jul 30 2005 David Malcolm <dmalcolm@redhat.com> 2.3.6.1-2
- Fixed version numbers in GConf schema files (#164622); added 
  apps-evolution-mail-prompts-checkdefault-2.4.schemas

* Fri Jul 29 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.6.1-1
- 2.3.6.1

* Thu Jul 28 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.6-1
- 2.3.6
- Bump evolution-data-server requirement to 1.3.6 (needed for 
  CAL_STATIC_CAPABILITY_HAS_UNACCEPTED_MEETING)
- Removed libgal2[-devel] dependencies; the code has been moved into the 
  evolution tarball

* Thu Jul 28 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.5.1-2
- added experimental patch to port ETable printing to use Pango (#150458)

* Mon Jul 25 2005 David Malcolm <dmalcolm@redhat.com> - 2.3.5.1-1
- 2.3.5.1
- Update evo_major from 2.2 to 2.4
- Updated evo-calendar-print-with-pango- patch from version 4 to 5
- Removed Patch105: evolution-2.2.2-fix-new-mail-notify.patch as configure.in
  in this branch tests for existance for dbus-glib-1, rather than max-version.
- Removed Patch801: gb-309138-attach-48417-fix-evo-conduit-memleaks.patch as
  this is now in upstream tarball.
- Removed evolution-calendar-importers and evolution-addressbook-importers
  directories.
- Updated evolution-2.2.2-no-gnome-common.patch to include a patch to rename
  mozilla-nspr to nspr

* Tue Jun 28 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-11.fc5
- Remove GNOME_COMPILE_WARNINGS from configure.in (since gnome-common might not be available when we rerun the autotools; patch 803)

* Tue Jun 28 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-10.fc5
- Moved .conduit files to libdir/gnome-pilot/conduits, rather than beneath datadir, to match gnome-pilot (patch 802)

* Mon Jun 27 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-9.fc5
- Replaced patch to port conduits to pilot-link-0.12 with Mark G Adams's version of same (#161817)
- Added Mark G Adams's memory leak fix (patch 801)

* Mon Jun  6 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-8
- Added Ivan Gyurdiev's patch to move autosave files inside the .evolution
  directory
#'

* Thu May 26 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-7
- Added Akira Tagoh's patch for calendar keypress handling (#154360)
#'

* Mon May 23 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-6
- Remove static versions of libraries

* Thu May  5 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-5
- added evolution-2.2.2-fix-new-mail-notify.patch to CVS

* Thu May  5 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-4
- Removed explicit mozilla_build_version; instead use pkg-config to determine 
the path to the NSS/NSPR headers.
- Use a macro to express requirement on pilot-link (was 1:0.11.4, now 0.12; 
patches depend on this)
- Re-enabled the new-mail-notify plugin (my patch to handle differing DBus 
versions is in the upstream tarball; but configure.in disables the plugin for 
dbus versions > 0.23; patched configure.in to allow arbitrary DBus versions, 
and run autoconf at the start of the build) (#156328)

* Sat Apr 30 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-3
- updated mozilla_build_version to 1.7.7

* Sat Apr 30 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-2
- Finished porting conduits to pilot-link-0.12 API; re-enabled pilot support (#152172)

* Mon Apr 11 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.2-1
- 2.2.2
- updated evo-calendar-print-with-pango-4.patch to handle upstream change to print_comp_item
- removed patch for XB73912; now in upstream tarball
- removed patch to new-mail-notify; generalised fix to cope with various DBus API versions is now upstream
- removed patch for XB73844; now in upstream tarball
- Update requirements:
  - gtkhtml3 from 3.6.1 to 3.6.2
  - libgal2 from 2.4.1 to 2.4.2
  - eds from 1.2.1 to 1.2.2

* Wed Mar 23 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.1.1-2
- Add patch for upstream bug XB73844 (should now be able to accept meeting requests)

* Fri Mar 18 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.1.1-1
- 2.1.1.1

* Thu Mar 17 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.1-1
- 2.2.1
- Updated requirements:
  * gtkhtml3 from 3.6.0 to 3.6.1
  * libgal2 from 2.4.0 to 2.4.1
  * eds from 1.2.0 to 1.2.1
- Added rum-time requirement on gnome-vfs2; updated version requirement from 2.0 to 2.4
- The new-mail-notify plugin will not be built for now since the upstream configure test now checks for dbus-glib-1 version <= 0.23.4 (to minimise problems caused by the API change)

* Mon Mar 14 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-10
- disabled pilot-link support for now so that we have an evolution package; more patching is needed to get this to work with pilot-link-0.12

* Mon Mar 14 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-9
- another attempt at porting to pilot-link 0.12

* Mon Mar 14 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-8
- Added patch to deal with changes to pilot-link from 0.11->0.12

* Mon Mar 14 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-7
- use 0.31 rather than 0.31.0 for DBus version

* Mon Mar 14 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-6
- rebuilt against pilot-link-0.12
- added versioning to the requirement on dbus (>=0.31)

* Thu Mar 10 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-5
- Added patch for changes to DBus API in version 0.31 (#150671)
- Removed explicit run-time spec-file requirement on mozilla.
  The Mozilla NSS API/ABI stabilised by version 1.7.3
  The libraries are always located in the libdir
  However, the headers are in /usr/include/mozilla-%{mozilla_build_version}
  and so they move each time the mozilla version changes.
  So we no longer have an explicit mozilla run-time requirement in the specfile; 
  a requirement on the appropriate NSS and NSPR .so files is automagically generated on build.
  We have an explicit, exact build-time version, so that we can find the headers (without
  invoking an RPM query from the spec file; to do so is considered bad practice)
- Introduced mozilla_build_version, to replace mozilla_version

* Wed Mar  9 2005 Christopher Aillon <caillon@redhat.com> - 2.2.0-4
- Depend on mozilla 1.7.6

* Wed Mar  9 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-3
- added patch from upstream for bug XB-73192, fixing missing "Mark as Read/Unread" context menu items

* Tue Mar  8 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-2
- actually add source tarball this time

* Tue Mar  8 2005 David Malcolm <dmalcolm@redhat.com> - 2.2.0-1
- 2.2.0
- Removed patch for GCC 4 fix as this is now in upstream tarball
- Updated requirements:
  * gtkhtml3 from 3.5.7 to 3.6.0
  * libgal2 from 2.3.5 to 2.4.0
  * eds from 1.1.6 to 1.2.0

* Tue Mar  8 2005 David Malcolm <dmalcolm@redhat.com> - 2.1.6-3
- rebuild (to use latest DBus library)

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 2.1.6-2
- added patch to fix build with GCC4

* Tue Mar  1 2005 David Malcolm <dmalcolm@redhat.com> - 2.1.6-1
- Update from upstream unstable 2.1.6 to 2.1.6
- Added patches to fix calendar and addressbook printing for non-Roman scripts (#138075)
- Added explicit requirement on libgnomeprint22 >= 2.8.0
- Added BuildRequires: gtk-doc
- Updated requirements:
  * gtkhtml3 from 3.5.6 to 3.5.7
  * libgal2 from 2.3.4 to 2.3.5
  * eds from 1.1.5 to 1.1.6

* Wed Feb  9 2005 David Malcolm <dmalcolm@redhat.com> - 2.1.5-1
- Update from upstream unstable 2.1.4 to 2.1.5
- Updated requirements:
  * gtkhtml3 from 3.5.4 to 3.5.6
  * libgal2 from 2.3.3 to 2.3.4
  * eds from 1.1.4.1 to 1.1.5
- Removed explicit packaging of weather icons as these are now below DATADIR/evolution/2.2 rather than DATADIR/evolution-2.2

* Wed Jan 26 2005 David Malcolm <dmalcolm@redhat.com> - 2.1.4-1
- Update from upstream stable 2.0.3 to unstable 2.1.4
- Updated evo_major from 2.0 to 2.2
- Removed camel packaging as this has been moved to evolution-data-server for Evolution 2.2
- Added plugins to the packaged files
- Added weather icons to the packaged files
- Updated requirements:
  * gtkhtml3 from 3.3.2 to 3.5.4
  * libgal2 from 2.2.4 to 2.3.3
  * eds from 1.0.3 to 1.1.4.1
  * libsoup from 2.2.0 to 2.2.2
- Added built-time requirement on atk-devel
- Enable all plugins for now
- Added requirement on dbus (for the new-mail-notify plugin)
- Enable gtk-doc
- Updated GConf schema name suffixes from 2.0 to 2.2

* Sun Dec 19 2004 Christopher Aillon <caillon@redhat.com> 2.0.3-2
- Rebuild against mozilla 1.7.5

* Wed Dec 15 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.3-1
- Update from upstream 2.0.2 to 2.0.3 with these bug fixes:
 * Addressbook
   XB67656 - almost the same email address are considrered identical (Siva)
   XB69079 - Data repeated after save with bad date format (Siva)
   XB66854 - Some strings are missed to translation (Rodney)

 * Calendar 	
   XB47529 - Date in reminder window appears in UTF-8 in non-UTF-8 locale (Rodney)
   XB68707 - Events ending at 12:00 AM show as ending at 12:00 pm (JP)
   XB67403 - wrong alarm time displayed (Rodrigo)
   XB68077 - appointment dialog re-size (Rodrigo)
   - leak fixes (Chen)
   - sensitize menu items in list view properly (JP)
   - redraw display when 24hr time setting changes (JP)

 * Mail
   XB69533 - Unable to subscribe to the alt hierarchy (Michael)
   XB69776 - Signed Mail with attachments displays everything with multipart/boundaries stuff (Michael)
   XB69615 - delete certificate after viewing smime message (Michael)
   XB69109 - EHLO or HELO with ip addresses does not conform rfc 821  (Michael)
   XB69982 - During Newsgroup list refresh, it crashes (Michael) 
   XB69446 - Mail shown as attachment if some headers are upper case (S. Caglar Onur) 
   XB68556 - NNTP with SSL won't work, even with stunnel (Michael) 
   XB69145 - toplevel message/rfc822 parts are broken for IMAP (Michael)
   XB69241 - base64 attachement holding PGP block (Jeff)
   XB67895 - nntp support not asking for password (Michael)
   XB67898 - Use of symbolic port-names is not guaranteed to work everywhere (Michael)
   XB69851 - remember password check doesn't stick (Michael)
   XB69623 - Moving a message from an IMAP INBOX to an IMAP folder caused crash (Radek)
   XB69339 - postscript and some other attachments not visable (Michael)
   XB69579 - vFoldersXBUNMATCHED generates errors (Michael)
   XB68958 - current message forgotten in vfolders (Michael)
   XB68974 - Wizard doesn't store smtp auth settings (Michael)
   XB67496 - html email not rendered in preview pane (Michael)
   XB67014 - Checking supported auth types doesn't work with new SSL certificate (Michael)
   XB68006 - Evo crashed after viewing previously-sent email and copying URL from it (Michael)
   XB68787 - Crash when migrating 1.4 data to 2.0.2 (Michael)
   XB67622 - SMTP auth usernames containing % character fail (Jeff)
   - fix pthread_key_delete args (Julio M. Merino Vidal)
- Removed patch for "Unmatched" vfolder properties dialog (#141458) as this is now in upstream tarball (XB69579 above)
- Update dependency on e-d-s from 1.0.2 to 1.0.3
- Update dependency on libgal2 from 2.2.3 to 2.2.4

* Wed Dec  1 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.2-6
- Fix broken properties dialog for "Unmatched" vfolder (#141458)

* Wed Oct 27 2004 Christopher Aillon <caillon@redhat.com> - 2.0.2-4
- Re-enable s390(x)

* Fri Oct 22 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.2-3
- added requirement on gnutls/gnutls-devel

* Fri Oct 22 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.2-2
- Fix for #132050 (no entry for Evolution in the menus): use the new redhat-evolution.desktop file provided by redhat-menus-1.13

* Tue Oct 12 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.2-1
- Update from 2.0.1 to 2.0.2
- Updated dependency on e-d-s from 1.0.1 to 1.0.2
- Updated dependency on libgal2 from 2.2.2 to 2.2.3
- Updated dependency on gtkhtml3 from 3.3.0 to 3.3.2
- ppc's mozilla dependency is now in line with the other architectures at 1.7.3
#'

* Sat Oct  9 2004 David Malcolm <dmalcolm@redhat.com>
- disable s390/s390x for now

* Fri Oct  8 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.1-3
- Fix for #135135, updating the fix for #103826 that removes the evolution.desktop file in "Office"; the file to delete had been renamed to evolution-2.0.desktop
- Added requirement on redhat-menus, since this supplies the target of our .desktop symlink

* Tue Sep 28 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.1-2
- update mozilla dependency from 1.7.2 to 1.7.3, apart from on ppc (and on s390 and s390x, which remain at 1.6, and on ppc64 where it isn't available at all)
#'

* Tue Sep 28 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.1-1
- Update from 2.0.0 to 2.0.1
- Updated dependency on e-d-s from 1.0.0 to 1.0.1
- Updated dependency on libgal2 from 2.2.0 to 2.2.2

* Mon Sep 20 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.0-2
- rebuilt

* Tue Sep 14 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.0-1
- Update from 1.5.94.1 to 2.0.0
- Change source FTP location from 1.5 to 2.0
- Updated dependency on e-d-s from 0.0.99 to 1.0.0
- Documentation has now moved from 1.5 to 2.0

* Tue Aug 31 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.94.1-1
- updated tarball from 1.5.93 to 1.5.94.1
- the BASE_VERSION in the configure.in script has finally been updated from 1.5 to 2.0 (affects OAFIIDs, install dirs, binary names etc); updated evo_major and various other parts of the spec-file to reflect this; however documentation is still 1.5 in upstream tarball
- updated dependency on libgal2 from 2:2.1.14 to 2:2.2.0
- updated dependency on libsoup from 2.1.13 to 2.2.0
- updated dependency on e-d-s from 0.0.98 to 0.0.99

* Tue Aug 17 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.93-2
- updated gnome-icon-theme requirement from 1.2.0 to 1.3.6 to fix problem with missing stock icons (bz #130142)

* Mon Aug 16 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.93-1
- updated tarball from 1.5.92.2 to 1.5.93
- removed filechooser patch - this is now in the upstream tarball, with a test at configuration time; it was autodetected and enabled in my test build; I've explicitly enabled it to be certain.
- updated dependency on libgal2 from 2:2.1.13 to 2:2.1.14
- updated dependency on libsoup from 2.1.12 to 2.1.13
- updated dependency on e-d-s from 0.0.97 to 0.0.98
#'

* Wed Aug 11 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.92.2-2
- Increased mozilla_version from 1.7 to 1.7.2 so that the NSS test looks in the correct place

* Wed Aug 11 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.92.2-1
- updated tarball from 1.5.92.1 to 1.5.92.2

* Wed Aug  4 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.92.1-1
- updated tarball from 1.5.91 to 1.5.92.1
- added a dependency on gnome-icon-theme
- updated dependency on libgal2 from 2:2.1.11 to 2:2.1.13
- updated dependency on gtkhtml3 from 3.1.17 to 3.3.0
- updated dependency on libsoup from 2.1.11 to 2.1.12
- updated dependency on e-d-s from 0.0.95 to 0.0.97

* Mon Jul 26 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.91-1
- 1.5.91

* Thu Jul  8 2004 Jeremy Katz <katzj@redhat.com> - 1.5.90-5
- use mozilla 1.7 on platforms where it's available
- check to make sure the appropriate mozilla headers exist if using
  mozilla nss for ssl or fail the build
#'

* Thu Jul  8 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Wed Jul  7 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Tue Jul  6 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.90-2
- Fixed sources file

* Tue Jul  6 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.90-1
- 1.5.90; updated requirements on gtkhtml3, libgal2, and e-d-s

* Thu Jun 17 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.9.2-1
- 1.5.9.2

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jun  8 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.9.1-3
- Replaced /usr/lib with %%{_libdir} in mozills_nss ssl_flags

* Mon Jun  7 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.9.1-2
- updated filechooser patch again

* Mon Jun  7 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.9.1-1
- 1.5.9.1; updated filechooser patch

* Wed May 26 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.8-3
- added ORBit2 and spamassassin requirements

* Mon May 24 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.8-2
- Fixed up filechooser patch and re-enabled it

* Fri May 21 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.8-1
- 1.5.8; added explicit libbonoboui requirement; disabled filechooser patch for now

* Tue May  4 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.7-3
- Added GtkFileChooser patch based on work by Carlos Garnacho Parro (http://lists.ximian.com/archives/public/evolution-patches/2004-March/004867.html); added requirement for GTK 2.4

* Thu Apr 22 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.7-2
- added emfv signal fix patch and fix for defaults in switch statements on gcc3.4

* Wed Apr 21 2004 David Malcolm <dmalcolm@redhat.com> - 1.5.7-1
- 1.5.7

* Wed Mar 10 2004 Jeremy Katz <katzj@redhat.com> - 1.5.5-1
- 1.5.5

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Feb 18 2004 Jeremy Katz <katzj@redhat.com> - 1.5.4-1
- 1.5.4

* Tue Feb 17 2004 Jeremy Katz <katzj@redhat.com> 
- buildrequire e-d-s-devel instead of e-d-s (#114712)
- enable nntp support (#114802)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jan 27 2004 Jeremy Katz <katzj@redhat.com> 1.5.3-1
- 1.5.3

* Wed Jan 21 2004 Jeremy Katz <katzj@redhat.com> 1.5.2-2
- size_t/int mismatch compile fix for 64bit platforms

* Wed Jan 14 2004 Jeremy Katz <katzj@redhat.com> 1.5.2-0
- 1.5.2
- add patch to fix gconf warning with schema

* Sun Jan  4 2004 Jeremy Katz <katzj@redhat.com> 1.5.1-0
- 1.5.1
- temporarily disable redhatify patch
- use mozilla-nss for SSL
- fix schema names

* Thu Nov  6 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-8
- fall back to HELO for ESMTP (#108753)

* Tue Oct 28 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-7
- fix title on composer save dialog (#108159)

* Mon Oct 27 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-6
- Make imap command length shorter to avoid choking some imap servers 
  (notably cyrus-imap).
- Make wombat session managed so that we don't hit weird bonobo activation
  things.  This adds a dependency on $DISPLAY for wombat.  (#106826)
#'

* Sun Oct 19 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-5
- use AI_ADDRCONFIG to avoid returning IPv6 addresses on hosts without 
  IPv6 support
- add patch from upstream with reply-to-list shortcut (Ctrl-l)

* Wed Oct 15 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-4
- really, really remove duplicate menu entry (#103826)

* Tue Oct 14 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-3
- Pull in some patches from upstream CVS
  * Avoid division by zero with POP (X#41610)
  * Don't mangle headers (X#33545)
  * Prefix IPV6 numeric hosts properly (X#46006, #105028)
  * Use proper function for IPV6 reverse lookups (X#46006)
  * Allow timezone offset to be up to 14 hours (X#49357)
#'

* Mon Oct 13 2003 Jeremy Katz <katzj@redhat.com> 
- add patch from upstream CVS to fix SMTP syntax problems (#106630) 
- really remove duplicate menu entry (#103826)

* Mon Oct  6 2003 Jeremy Katz <katzj@redhat.com> 
- make redhat-email.desktop symlink relative (#104391)

* Wed Sep 24 2003 Jeremy Katz <katzj@redhat.com> 
- add ipv6 support per dwmw2's request
#'

* Tue Sep 23 2003 Jeremy Katz <katzj@redhat.com> 1.4.5-2
- 1.4.5

* Wed Sep 17 2003 Jeremy Katz <katzj@redhat.com> 
- move static libs into -devel (#104399)

* Tue Sep 16 2003 Jeremy Katz <katzj@redhat.com> 1.4.4-7
- filter types are gtypes, not ints (#103934)

* Wed Sep 10 2003 Jeremy Katz <katzj@redhat.com> 1.4.4-6
- fix from upstream (will be in 1.4.5) to fix menu merging in the 
  composer with new libbonobo

* Fri Sep  5 2003 Jeremy Katz <katzj@redhat.com> 
- remove the desktop file in Office (#103826)

* Tue Sep  2 2003 Jeremy Katz <katzj@redhat.com> 1.4.4-5
- patch from upstream to fix display of some mails in 
  different charsets (#102899)
- add requires on newer version of ORBit2 (#103386)
- add patch from upstream (extracted by George Karabin) to use gnome-vfs 
  mime icon lookup where available (#102553)

* Fri Aug 22 2003 Jeremy Katz <katzj@redhat.com> 1.4.4-4
- include static libs (#102834)

* Wed Aug  6 2003 Jeremy Katz <katzj@redhat.com> 1.4.4-3
- add a -devel subpackage (#99376)

* Mon Aug  4 2003 Jeremy Katz <katzj@redhat.com> 1.4.4-1
- 1.4.4

* Wed Jul 30 2003 Jeremy Katz <katzj@redhat.com> 
- buildrequires fixup from Ville Skytta (#101325)

* Thu Jul 24 2003 Jeremy Katz <katzj@redhat.com> 1.4.3-6
- include tagoh's patch for printing cjk contacts (committed upstream, #99374)
#'

* Tue Jul 22 2003 Nalin Dahyabhai <nalin@redhat.com> 1.4.3-5
- rebuild

* Tue Jul 15 2003 Jeremy Katz <katzj@redhat.com> 1.4.3-4
- build on all arches again

* Mon Jul 14 2003 Jeremy Katz <katzj@redhat.com> 1.4.3-3
- rebuild

* Thu Jul 10 2003 Jeremy Katz <katzj@redhat.com> 1.4.3-1
- 1.4.3

* Thu Jun 19 2003 Jeremy Katz <katzj@redhat.com> 
- make gal version dep more explicit

* Fri Jun 13 2003 Jeremy Katz <katzj@redhat.com> 
- fix desktop file (#97162)

* Tue Jun 10 2003 Jeremy Katz <katzj@redhat.com> 1.4.0-2
- rebuild
- excludearch ppc64 for now

* Mon Jun  9 2003 Jeremy Katz <katzj@redhat.com> 1.4.0-1
- 1.4.0

* Wed Jun 5 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun  5 2003 Jeremy Katz <katzj@redhat.com> 1.3.92-2
- rebuild

* Wed Jun  4 2003 Jeremy Katz <katzj@redhat.com> 
- buildrequires gettext (#92276)

* Sun May 25 2003 Jeremy Katz <katzj@redhat.com> 1.3.92-1
- 1.3.92

* Wed May  7 2003 Jeremy Katz <katzj@redhat.com> 1.3.3-2
- fix default for /schemas/apps/evolution/mail/display/mime_types

* Tue May  6 2003 Jeremy Katz <katzj@redhat.com> 1.3.3-1
- 1.3.3

* Sun May  4 2003 Jeremy Katz <katzj@redhat.com> 1.3.2-2
- enable pilot support
- add redhatify patch back

* Tue Apr 22 2003 Jeremy Katz <katzj@redhat.com>
- add a /usr/bin/evolution symlink

* Mon Apr 21 2003 Jeremy Katz <katzj@redhat.com> 
- fix gnome-spell version requirement

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 1.3.2-1
- add trivial fix for evolution-mail schema key (ximian #41419)

* Tue Apr 15 2003 Jeremy Katz <katzj@redhat.com> 
- update to 1.3
- don't build with pilot support for now
- don't redhat-ify the summary prefs for now

* Sun Apr  6 2003 Jeremy Katz <katzj@redhat.com> 1.2.4-2
- fix krb5 libdir for lib64 systems

* Sun Apr  6 2003 Jeremy Katz <katzj@redhat.com> 1.2.4-1
- update to 1.2.4

* Thu Apr  3 2003 Jeremy Katz <katzj@redhat.com> 1.2.2-7
- oops, fix a tyop

* Thu Apr  3 2003 Jeremy Katz <katzj@redhat.com> 1.2.2-6
- add a few cleanups for 64bit cleanliness (#86347)

* Sun Mar 30 2003 Jeremy Katz <katzj@redhat.com> 
- add some buildrequires (#87612)

* Mon Mar 24 2003 Jeremy Katz <katzj@redhat.com> 1.2.3-1
- update to 1.2.3

* Wed Mar 19 2003 Jeremy Katz <katzj@redhat.com> 1.2.2-5
- security patches from upstream
  - sanity check UUEncoding header before decoding (CAN-2003-0128)
  - don't decode doubly UUEncoded content (CAN-2003-0129)
  - don't use a bonobo component to display things without registered 
    handlers (CAN-2003-0130)

* Mon Feb 24 2003 Elliot Lee <sopwith@redhat.com> 1.2.2-4
- debuginfo rebuild

* Thu Feb 20 2003 Jeremy Katz <katzj@redhat.com> 1.2.2-3
- memleak patch had some bits that weren't supposed to be there.  update 
  to newer from upstream.
- fix directory checking in proxy patch
#'

* Thu Feb 20 2003 Jeremy Katz <katzj@redhat.com> 1.2.2-2
- add missing build dep (#84388)
- add patch from upstream for evolution-mail memleak
- add patch from upstream to use the gnome2 proxy settings by default

* Fri Feb  7 2003 Jeremy Katz <katzj@redhat.com> 1.2.2-1
- 1.2.2
- build on x86_64 

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.1-4
- rebuild

* Fri Jan  3 2003 Nalin Dahyabhai <nalin@redhat.com>
- if building with OpenSSL, attempt to get cflags and ldflags from pkgconfig

* Thu Jan  2 2003 Jeremy Katz <katzj@redhat.com> 1.2.1-3
- we don't want to use native POSIX threads for mutexes in db3, override them
#'

* Mon Dec 16 2002 Tim Powers <timp@redhat.com> 1.2.1-2
- rebuild

* Fri Dec 13 2002 Jeremy Katz <katzj@redhat.com> 1.2.1-1
- update to 1.2.1

* Thu Dec 12 2002 Jeremy Katz <katzj@redhat.com> 1.2.0-6
- require a newer soup, the old one Has Bugs (tm)
- excludearch x86_64; getting a R_X86_64_32S relocation in libical 
  although everything appears to be built with -fPIC correctly

* Tue Dec 10 2002 Jeremy Katz <katzj@redhat.com> 1.2.0-5
- patch for multilib krb5

* Mon Dec  2 2002 Jeremy Katz <katzj@redhat.com> 1.2.0-4
- add upstream patch to handle LDAPv3 better
- add upstream patch to fix shell memory leaks
- add upstream patch to fix ldap scope selection
- build with openssl instead of mozilla-nss since it's available on 
  more platforms
- build on all arches
#'

* Fri Nov 22 2002 Jeremy Katz <katzj@redhat.com>
- require bonobo-conf, not -devel (#78398)

* Wed Nov 20 2002 Florian La Roche <Florian.LaRoche@redhat.de> 1.2.0-3
- disable pilot support for mainframe

* Mon Nov 18 2002 Jeremy Katz <katzj@redhat.com> 1.2.0-2
- macro-ify the mozilla version to make it easier to build against 
  newer mozillas with headers in new locations
- buildrequire pilot-link-devel (#78077)
- drop uneeded ldapv3 patch (toshok says 1.2 already handles this)
- drop unneeded patch for ordering of the libdb checks
- add fejj's patch to always subscribe to the inbox from evolution-patches
#'

* Tue Nov 12 2002 Jeremy Katz <katzj@redhat.com> 1.2.0-1
- 1.2.0

* Sat Nov  2 2002 Jeremy Katz <katzj@redhat.com> 1.1.90-2
- reenable pilot support
- redhatify

* Fri Nov  1 2002 Jeremy Katz <katzj@redhat.com> 1.1.90-1
- update to 1.1.90

* Thu Oct 31 2002 Jeremy Katz <katzj@redhat.com>
- include mozilla epochs in requires (#74577)
- add build requires on newer oaf (#76801)

* Thu Oct 24 2002 Jeremy Katz <katzj@redhat.com> 1.1.2-1
- update to 1.1.2
- remove unpackaged files from the buildrooot
- disable pilot support for now

* Tue Sep  3 2002 Jeremy Katz <katzj@redhat.com> 1.0.8-10
- add freetype-devel to build requires (#73319)

* Mon Sep  2 2002 Owen Taylor <otaylor@redhat.com>
- Fix a problem where evolution-mail right click items corrupted the stack
  (#67992)

* Thu Aug 29 2002 Jeremy Katz <katzj@redhat.com> 1.0.8-9
- don't install two desktop files (#72871)
#'

* Wed Aug 28 2002 Preston Brown <pbrown@redhat.com> 1.0.8-8
- absolute symlink .desktop file (#72913)

* Thu Aug 22 2002 han Ngo <than@redhat.com> 1.0.8-7
- rebuild against new pilot-link

* Sat Aug 10 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- bzip2 source

* Tue Aug  6 2002 Than Ngo <than@redhat.com> 1.0.8-5
- rebuild against new pilot-link-0.11.2

* Thu Jul 18 2002 Jeremy Katz <katzj@redhat.com> 1.0.8-4
- rebuild against new gnome-pilot

* Tue Jul  9 2002 Jeremy Katz <katzj@redhat.com> 1.0.8-3
- remove static and libtool archives for importers and camel-providers (#68222)
- do desktop-file-install magic 
- remove dead sites from summary list (#64522)
- support openldap protocol version 3 based off of Nalin's autofs changes 
#'

* Mon Jul  8 2002 Jeremy Katz <katzj@redhat.com> 1.0.8-2
- fix openldap-devel buildrequire

* Mon Jul  1 2002 Jeremy Katz <katzj@redhat.com> 1.0.8-1
- 1.0.8 

* Thu Jun 27 2002 Jeremy Katz <katzj@redhat.com> 1.0.7-2
- include patch to omf files from otaylor@redhat.com to fix 
  scrollkeeper validation errors

* Sun Jun 23 2002 Jeremy Katz <katzj@redhat.com> 1.0.7-1
- update to 1.0.7
- excludearch alpha while mozilla isn't being built there
#'

* Sun May 26 2002 Tim Powers <timp@redhat.com> 1.0.5-2
- automated rebuild

* Mon May 13 2002 Jeremy Katz <katzj@redhat.com> 1.0.5-1
- update to 1.0.5

* Fri May  3 2002 Jeremy Katz <katzj@redhat.com> 1.0.3-6
- add patch to fix spool unread counts (#64198)
- build with the fix for the crasher mail sent to 
  evolution-list (ximian #24140)

* Mon Apr 15 2002 Jeremy Katz <katzj@redhat.com> 1.0.3-4
- include fejj(at)ximian.com's patch to fix the EINPROGRESS error with ssl 
  since it's been committed to the branch and fixes the problem for me
- include patch from tagoh(at)redhat.com to change the default charset 
  for Japanese to ISO-2022-JP (#63214)

* Wed Apr 10 2002 Jeremy Katz <katzj@redhat.com> 1.0.3-3
- minor tweaks to the redhatify patch
- make accepting appointments sent to mailing lists work
- use the RFC specified LDAP attribs for freebusy and calendarURI 
  in addressbook
- fix a crash in the startup wizard

* Sun Mar 31 2002 Jeremy Katz <katzj@redhat.com> 1.0.3-2
- move desktop file to /etc/X11/applnk (#62399)

* Sun Mar 24 2002 Jeremy Katz <katzj@redhat.com> 1.0.3-1
- update to evolution 1.0.3
- change summary view to show a recent errata list by default

* Thu Mar 14 2002 Jeremy Katz <katzj@redhat.com>
- put correct path to nspr includes on configure command line

* Mon Mar 11 2002 Jeremy Katz <katzj@redhat.com> 1.0.2-3
- mozilla 0.9.9 has nspr and nss subpackages, hooray!  rip out the static 
  libnss linkage and just link against what is provided dynamically
- kill the -devel subpackage since it's of questionable use
- explicitly require mozilla-nss and mozilla-nspr packages to make it easier
  to resolve the requirements
#'

* Thu Feb 21 2002 Jeremy Katz <katzj@redhat.com> 1.0.2-2
- rebuild in new environment
- temporarily exclude on ia64 again

* Thu Jan 31 2002 Jeremy Katz <katzj@redhat.com> 1.0.2-1
- update to 1.0.2

* Mon Jan 28 2002 Jeremy Katz <katzj@redhat.com> 1.0.1-4
- build on ia64 now that mozilla exists for ia64

* Sun Jan 27 2002 Jeremy Katz <katzj@redhat.com> 1.0.1-3
- rebuild in new environment
- add pilot support

* Sun Jan 13 2002 Jeremy Katz <katzj@redhat.com> 1.0.1-2
- rebuild without mozilla-psm in the buildroot so libnss is linked 
  statically as intended

* Sat Jan 12 2002 Jeremy Katz <katzj@redhat.com> 1.0.1-1
- update to 1.0.1
- patch for autoconf 2.52 accepted upstream
- include man page
- use --with-sub-version=" (%%{version}-%%{release})"

* Tue Dec 18 2001 Jeremy Katz <katzj@redhat.com> 1.0-2
- really disable news
- add patch from Jens Petersen <juhp@redhat.com> to hopefully get 
  builds working with autoconf 2.52
- conditionalize static libnss stuff so that it can go away when we
  have a mozilla with shared libnss

* Thu Dec  6 2001 Jeremy Katz <katzj@redhat.com> 1.0-1.7.2
- add patches off of branch for:
  * do not show up as Preview Release in version string
  * have next/previous work with multiple selected messages
- build without pilot support

* Mon Dec  3 2001 Jeremy Katz <katzj@redhat.com> 1.0-1
- and bump to 1.0

* Sun Dec  2 2001 Jeremy Katz <katzj@redhat.com>
- let's build with an included copy of libnss now since OpenSSL is support
  is disabled on the 1.0 branch
- build with --enable-dot-locking=no
- excludearch ia64 again now that we need libnspr
#'

* Mon Nov 26 2001 Jeremy Katz <katzj@redhat.com>
- build with gnome-pilot and krb5 support
- conditionalize ldap, pilot and krb5 support
- clean up buildrequires some

* Sat Nov 17 2001 Jeremy Katz <katzj@redhat.com>
- we can build on ia64 since we're using openssl instead of nspr
- disable non-functional nntp support 
- 0.99.2 (rc2) 
#'

* Fri Nov  9 2001 Jeremy Katz <katzj@redhat.com>
- add explicit requires on current bonobo, oaf, and GConf to help people
  help themselves
- s/Copyright/License/

* Thu Nov  8 2001 Jeremy Katz <katzj@redhat.com>
- add a patch to revert changes to camel-tcp-stream-openssl; appears to 
  fix the SSL hangs

* Wed Nov  7 2001 Jeremy Katz <katzj@redhat.com>
- fix filelist to include libical zoneinfo
- add devel subpackage with includes and static libs

* Mon Nov  5 2001 Jeremy Katz <katzj@redhat.com>
- updated to 0.99.0 aka 1.0 RC1

* Tue Oct 23 2001 Havoc Pennington <hp@redhat.com>
- 0.16 snagged from Ximian GNOME

* Fri Oct  5 2001 Havoc Pennington <hp@redhat.com>
- initial build based on David Sainty's specfile
#'

* Thu Oct 04 2001 David Sainty <dsainty@redhat.com>
- Updated to 0.15.99, 20011004 from cvs.

* Wed Sep 05 2001 David Sainty <dsainty@redhat.com>
- Updated to 0.13.99, 20010905 from cvs.

* Mon Sep 03 2001 David Sainty <dsainty@redhat.com>
- Updated to 0.13.99, 20010903 from cvs.
- Fixed Requires + BuildRequires

* Mon Aug 06 2001 David Sainty <dsainty@redhat.com>
- Updated to 0.12.99, 20010806 from cvs.

* Mon Aug 06 2001 David Sainty <dsainty@redhat.com>
- Relocated libical* from /usr/lib due to kdepim, -2

* Mon Aug 06 2001 David Sainty <dsainty@redhat.com>
- First spec file for evolution.
