%global momorel 1
Name:           zathura
Version:        0.2.6
Release: %{momorel}m%{?dist}
Summary:        A lightweight document viewer
Group:          Applications/Publishing
License:        "zlib"
URL:            http://pwmt.org/projects/%{name}/
Source0:        http://pwmt.org/projects/%{name}/download/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires:  cairo-devel
BuildRequires:  desktop-file-utils
BuildRequires:  file-devel
BuildRequires:  gdk-pixbuf2-devel
BuildRequires:  gettext
BuildRequires:  girara-devel
BuildRequires:  glib2-devel
BuildRequires:  gtk2-devel
BuildRequires:  poppler-glib-devel
BuildRequires:  python-docutils

%description
Zathura is a highly customizable and functional document viewer.
It provides a minimalistic and space saving interface as well as
an easy usage that mainly focuses on keyboard interaction.

Zathura requires plugins to support document formats.
For instance:
* zathura-pdf-poppler to open PDF files,
* zathura-ps to open PostScript files,
* zathura-djvu to open DjVu files.
All of these are available as separate packages in Momonga.
A zathura-plugins-all package is available should you want
to install all available plugins.

%package devel
Summary: Development files for the zathura PDF viewer
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description devel
libraries and header files for the zathura PDF viewer.

%package plugins-all
Summary:        Zathura plugins (all plugins)
Group:          Applications/Publishing
Requires:       zathura-djvu
Requires:       zathura-pdf-poppler
Requires:       zathura-ps

%description plugins-all
This package provides all Zathura plugins.

%prep
%setup -q
# don't rebuild at install time
sed -ie "s/install:\ all/install:\ /g" Makefile
# keep using girara/gtk2 for nop
sed -i -e 's/^ZATHURA_GTK_VERSION.*/ZATHURA_GTK_VERSION ?= 2/g' config.mk
# ensure manpages are built from *.rst sources
rm -f zathura.1 zathurarc.5

%build
export CFLAGS="%{optflags}"
export LIBDIR="%{_libdir}"
# avoid stripping
export SFLAGS=
%make

%install
export LIBDIR="%{_libdir}"
make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 zathura.desktop %{buildroot}%{_datadir}/applications/
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop

%find_lang zathura

%post
/sbin/ldconfig
update-desktop-database &> /dev/null || :

%postun
/sbin/ldconfig
update-desktop-database &> /dev/null || :

%files -f zathura.lang
%doc LICENSE README
%{_bindir}/*
%{_mandir}/man*/*
%{_datadir}/applications/*

%files devel
%{_includedir}/zathura
%{_libdir}/pkgconfig/zathura.pc

%files plugins-all

%changelog
* Wed Feb 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-1m)
- import from fedora

