%global momorel 1

Summary: GeoIP module for the Apache HTTP Server
Name: mod_geoip
Version: 1.2.7
Release: %{momorel}m%{?dist}
License: Apache
Group: System Environment/Daemons
URL: http://www.maxmind.com/app/mod_geoip
Source: http://www.maxmind.com/download/geoip/api/mod_geoip2/mod_geoip2_%{version}.tar.gz
NoSource: 0
Patch0: mod_geoip2_1.2.7-httpd-2.4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: GeoIP, httpd  >= 2.4.3, httpd-mmn = %([ -a %{_includedir}/httpd/.mmn ] && cat %{_includedir}/httpd/.mmn || echo missing)
BuildRequires: httpd-devel >= 2.4.3, GeoIP-devel

%description
mod_geoip is an Apache module for finding the country that a web request
originated from.  It uses the GeoIP library and database to perform
the lookup.  It is free software, licensed under the Apache license.

%prep

%setup -q -n mod_geoip2_%{version}
%patch0 -p1 -b .httpd24

%build
%{_httpd_apxs} -Wc,"%{optflags}" -Wl,"-lGeoIP" -c mod_geoip.c

%install
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d/
install -Dp .libs/mod_geoip.so %{buildroot}%{_libdir}/httpd/modules/mod_geoip.so

%{__cat} << EOF > %{buildroot}%{_sysconfdir}/httpd/conf.d/mod_geoip.conf.dist
LoadModule geoip_module modules/mod_geoip.so

<IfModule mod_geoip.c>
  GeoIPEnable On
  GeoIPDBFile /usr/share/GeoIP/GeoIP.dat
</IfModule>

EOF

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc INSTALL README* Changes
%{_libdir}/httpd/modules/mod_geoip.so
%config(noreplace) %{_sysconfdir}/httpd/conf.d/mod_geoip.conf.dist

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7
- rebuild against httpd-2.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- rebuild against rpm-4.6

* Thu Jun 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.8-2m)
- rebuild against gcc43

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.8-1m)
- import from Fedora

* Sun Sep 3 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.1.8-2
- Bump and rebuild

* Mon May 1 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.1.8-1
- New upstream release

* Sat Feb 18 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.1.7-2
- Small cleanups, including a saner Requires: for httpd
- Don't strip the binary

* Sun Feb 5 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.1.7-1
- Initial review package for Extras

