%global momorel 9

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define base_name       launcher
%define short_name      commons-%{base_name}

Name:           jakarta-%{short_name}
Version:        1.1
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        A cross platform Java application launcher
License:        "ASL 2.0"
Group:          Development/Libraries
URL:            http://jakarta.apache.org/commons/launcher/
Source:         http://archive.apache.org/dist/jakarta/commons/launcher/source/%{short_name}-%{version}-src.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRequires:  ant
BuildRequires:  jpackage-utils >= 0:1.6
Requires(pre):  jpackage-utils >= 0:1.6
Requires(postun):  jpackage-utils >= 0:1.6
Requires:       java

%if %{gcj_support}
BuildRequires:          java-gcj-compat-devel
Requires(post):         java-gcj-compat
Requires(postun):       java-gcj-compat
%endif

%description
Commons-launcher eliminates the need for a batch or shell script to launch a 
Java class. Some situations where elimination of a batch or shell script may 
be desirable are:

* You want to avoid having to determining where certain application paths are
e.g. your application's home directory, etc. Determining this dynamically in 
a Windows batch scripts is very tricky on some versions of Windows or when 
softlinks are used on Unix platforms.

* You want to avoid having to handle native file and path separators or native
path quoting issues.

* You need to enforce certain system properties e.g. java.endorsed.dirs when
running with JDK 1.4.

* You want to allow users to pass in custom JVM arguments or system properties
without having to parse and reorder arguments in your script. This can be 
tricky and/or messy in batch and shell scripts.

* You want to bootstrap system properties from a configuration file instead 
hard-coding them in your batch and shell scripts.

* You want to provide localized error messages which is very tricky to do in
batch and shell scripts.


%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
BuildRequires:  java-javadoc
# for /bin/rm and /bin/ln
Requires(post):   coreutils
Requires(postun): coreutils

%description    javadoc
Javadoc for %{name}.


%prep
%setup -q -n %{short_name}
%{__sed} -i 's/\r//' LICENSE.txt
%{__sed} -i 's/\r//' NOTICE.txt
%{__sed} -i 's/\r//' README.txt

%build
mkdir lib
ant \
  -Dbuild.sysclasspath=only \
  -Dfinal.name=%{short_name} \
  -Dj2se.javadoc=%{_javadocdir}/java \
  -Dsrcdir=. \
  jar javadoc


%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p dist/bin/%{short_name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|jakarta-||g"`; done)
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr dist/docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name} # ghost symlink


%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT


%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
   rm -f %{_javadocdir}/%{name}
fi

%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc LICENSE.txt NOTICE.txt README.txt
%{_javadir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/jakarta-commons-launcher-%{version}.jar.*
%endif

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}
%ghost %doc %{_javadocdir}/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-7m)
- full rebuild for mo7 release

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-6m)
- sync with Fedora 13 (0:1.1-4.4)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1jpp.1m)
- import from Fedora

* Fri Feb 09 2007 Matt Wringe <mwringe@redhat.com> - 0:1.1-1jpp.2
- Fix macro naming for post and postun

* Tue Feb 06 2007 Matt Wringe <mwringe@redhat.com> - 0:1.1-1jpp.1
- Merge with upstream jpp spec file
- Add missing jpackage utils requirements on pre and postun
- Add missing requires on java
- Fix some rpmlint issues 

* Fri Nov 17 2006 Jason Corley <jason.corley@gmail.com> - 0:1.1-1jpp
- 1.1

* Thu Aug 10 2006 Matt Wringe <mwringe at redhat.com> - 0:0.9-6jpp.1
- Merge with upstream version
- Add missing javadoc postun
- Add missing javadoc requires

* Thu Aug 10 2006 Karsten Hopp <karsten@redhat.de> 0.9-5jpp_3fc
- Requires(post):     coreutils

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:0.9-5jpp_2fc
- Rebuilt

* Thu Jul 20 2006 Matt Wringe <mwringe at redhat.com> - 0:0.9-5jpp_1fc
- Merge with upstream version
- Natively compile packages

* Thu Jul 20 2006 Matt Wringe <mwringe at redhat.com> - 0:0.9-5jpp
- Add conditional native compiling
- Change section from 'devel' to 'free'

* Thu Apr 27 2006 Fernando Nasser <fnasser@redhat.com> - 0:0.9-4jpp
- First JPP 1.7 build

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:0.9-3jpp
- Rebuild with ant-1.6.2

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 0:0.9-2jpp
- Upgrade to Ant 1.6.X

* Fri Jan  9 2004 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:0.9-1jpp
- First build for JPackage

* Wed Dec 17 2003 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:0.9-0.2
- Fixed description
- Enabled javadocs

* Thu Dec  4 2003 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:0.9-0.1
- Rebuilt w/o javadocs

