%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

Name:           python-paste-deploy
Version:        1.3.3
Release:        %{momorel}m%{?dist}
Summary:        Load, configure, and compose WSGI applications and servers
Group:          System Environment/Libraries
License:        MIT/X
URL:            http://pythonpaste.org/deploy
Source0:        http://cheeseshop.python.org/packages/source/P/PasteDeploy/PasteDeploy-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-setuptools >= 0.6c9-2m python-devel >= 2.7
Requires:       python-paste

%description
This tool provides code to load WSGI applications and servers from
URIs; these URIs can refer to Python Eggs for INI-style configuration
files.  PasteScript provides commands to serve applications based on
this configuration file.

%prep
%setup -q -n PasteDeploy-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --single-version-externally-managed \
                             --skip-build -O1 --root=%{buildroot}

echo '%defattr (0644,root,root,0755)' > pyfiles
find %{buildroot}%{python_sitelib}/paste/deploy -type d | \
        sed 's:%{buildroot}\(.*\):%dir \1:' >> pyfiles
find %{buildroot}%{python_sitelib}/paste/deploy -not -type d | \
        sed 's:%{buildroot}\(.*\):\1:' >> pyfiles

%clean
rm -rf %{buildroot}


%files -f pyfiles
%defattr(-,root,root,-)
%doc docs/*
%{python_sitelib}/PasteDeploy-%{version}-py%{pyver}*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.2-2m)
- rebuild against python-2.6.1

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Sat Mar  3 2007 Luke Macken <lmacken@redhat.com> - 1.1-1
- 1.1

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> - 1.0-2
- Add python-devel to BuildRequires
- 1.0

* Sun Sep 17 2006 Luke Macken <lmacken@redhat.com> - 0.9.6-1
- 0.9.6

* Sun Sep  3 2006 Luke Macken <lmacken@redhat.com> - 0.5-4
- Rebuild for FC6

* Mon Aug 21 2006 Luke Macken <lmacken@redhat.com> - 0.5-3
- Include .pyo files instead of ghosting them.

* Mon Jul 24 2006 Luke Macken <lmacken@redhat.com> - 0.5-2
- Fix docs inclusion
- Rename package to python-paste-deploy
- Fix inconsistent use of buildroots

* Mon Jul 10 2006 Luke Macken <lmacken@redhat.com> - 0.5-1
- Initial package
