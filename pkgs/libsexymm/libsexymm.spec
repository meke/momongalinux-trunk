%global momorel 9

Name: libsexymm
Version: 0.1.9
Release: %{momorel}m%{?dist}
Summary: C++ wrapper for libsexy
Group: System Environment/Libraries
License: LGPL
URL: http://www.chipx86.com/wiki/Libsexy
Source0: http://releases.chipx86.com/libsexy/libsexymm/libsexymm-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtkmm-devel >= 2.4.0
BuildRequires: libsexy-devel >= 0.1.10
BuildRequires: libxml2-devel
BuildRequires: libxcb-devel >= 1.2
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description
libsexymm is a set of C++ bindings around libsexy, compatible with
programs using gtkmm.

%package devel
Summary: Headers for developing programs that will use libsexymm
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the headers that programmers will need to
develop applications which will use libsexymm.

%prep
%setup -q -n libsexymm-%{version}

%build
%configure --disable-static --enable-docs

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
mkdir -p %{buildroot}%{_datadir}/libsexymm/
mv %{buildroot}%{_libdir}/libsexymm/include/libsexymmconfig.h \
   %{buildroot}%{_includedir}/libsexymm/libsexymmconfig.h
mv %{buildroot}%{_libdir}/libsexymm/proc/ \
   %{buildroot}%{_datadir}/libsexymm/

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, -)
%doc COPYING ChangeLog INSTALL NEWS
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root, -)
%{_includedir}/libsexymm/
%dir %{_datadir}/libsexymm
%{_datadir}/libsexymm/proc
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.9-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.9-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.9-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.9-5m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.9-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.9-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-1m)
- import to Momonga from Fedora devel

* Wed Mar 28 2007 Haikel Guemar <karlthered@gmail.com> - 0.1.9-3
- unowned directory

* Sun Jan 21 2007 Haikel Guemar <karlthered@gmail.com> - 0.1.9-2
- rebuild against new cairomm package

* Tue Nov 17 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.9-1
- updated to 0.1.9, license file issue has been fixed upstream

* Tue Sep 12 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.7-4
- rebuild for FC6

* Sun Aug 13 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.7-3
- fixed some rpmlint issues, add a patch to correct the license file

* Tue Jun 13 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.7-2
- some syntax fixes to post and postun section

* Mon May 22 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.7-1
- First Packaging
