%global momorel 3

%global tdocver 20040817
%global tedver  r0

Summary: TAP-plugins is short for Tom's Audio Processing plugins.
#'
Name: ladspa-tap-plugins
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://tap-plugins.sourceforge.net/
Group: Applications/Multimedia
Source0:  http://dl.sourceforge.net/sourceforge/tap-plugins/tap-plugins-%{version}.tar.gz
NoSource: 0
Source1:  http://dl.sourceforge.net/sourceforge/tap-plugins/tap-plugins-doc-%{tdocver}.tar.gz
NoSource: 1
Source2:  http://dl.sourceforge.net/sourceforge/tap-plugins/tap-reverbed-%{tedver}.tar.gz
NoSource: 2
Patch2: tap-reverbed-r0-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa jack libpng
BuildRequires: ladspa-devel
BuildRequires: jack-devel
BuildRequires: libpng-devel

%description
TAP-plugins is short for Tom's Audio Processing plugins. It is a bunch of LADSPA plugins for digital audio processing, intended for use in a professional DAW environment such as Ardour.
#'

%prep
%setup -q -a 0 -a 1 -a 2 -c %{name}-%{version}

%patch2 -p0 -b .linking

%build
cd tap-plugins-%{version}
%make
cd ../

cd tap-reverbed-%{tedver}
%configure
%make
cd ../

%install
install -d %{buildroot}%{_libdir}/ladspa

cd tap-plugins-%{version}
install -m 755 *.so   %{buildroot}%{_libdir}/ladspa
cd ..

cd tap-reverbed-%{tedver}
%makeinstall
cd ..

%files
%defattr(-,root,root)
%doc tap-plugins-%{version}/{COPYING,CREDITS,README}
%doc tap-plugins-doc-%{tdocver}
%doc tap-reverbed-%{tedver}/{AUTHORS,COPYING,ChangeLog,INSTALL,NEWS,README}
%{_bindir}/reverbed
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-5m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-4m)
- explicitly link libm (Patch2)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.7.0-1m)
- Initial Build for Momonga Linux
