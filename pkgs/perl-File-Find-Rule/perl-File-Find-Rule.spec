%global         momorel 10

Name:           perl-File-Find-Rule
Version:        0.33
Release:        %{momorel}m%{?dist}
Summary:        Alternative interface to File::Find
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-Find-Rule/
Source0:        http://www.cpan.org/authors/id/R/RC/RCLAMP/File-Find-Rule-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Number-Compare
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Text-Glob >= 0.07
Requires:       perl-Cwd
Requires:       perl-Number-Compare
Requires:       perl-Test-Simple
Requires:       perl-Text-Glob >= 0.07
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
File::Find::Rule is a friendlier interface to File::Find. It allows you to
build rules which specify the desired files and directories.

%prep
%setup -q -n File-Find-Rule-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes findrule
%{_bindir}/findrule
%{perl_vendorlib}/File/Find/Rule.pm
%{perl_vendorlib}/File/Find/Rule
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-2m)
- rebuild against perl-5.14.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.32-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-3m)
- rebuild against perl-5.12.0

* Mon Jan 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.30-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.30-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.30-2m)
- use vendor

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.30-1m)
- update to 0.30

* Fri Jun  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.29-1m)
- update to 0.29

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.28-3m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.28-2m)
- rebuilt against perl-5.8.7

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.27-3m)
- rebuild against perl-5.8.5
- rebuild against perl-Text-Glob 0.06-5m 
- rebuild against perl-Number-Compare 0.01-6m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.27-2m)
- remove Epoch from BuildRequires

* Mon Mar 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.27-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.24-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.24-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.24-1m)
- version 0.24

* Fri Sep  5 2003 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.11-1m)

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Tue Mar 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Tue Dec 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.08-1m)
- 'BuildArch: noarch'

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.07-1m)
- spec file was semi-autogenerated
