%global momorel 2
%define major 4
%define minor 6
%define patchlevel 3

%define x11_app_defaults_dir %{_datadir}/X11/app-defaults

Summary: A program for plotting mathematical expressions and data
Name: gnuplot
Version: %{major}.%{minor}.%{patchlevel}
Release: %{momorel}m%{?dist}
# Modifications are to be distributed as patches to the released version.
# aglfn.txt has license: MIT
License: "gnuplot and MIT"
Group: Applications/Engineering
URL: http://www.gnuplot.info/
Source: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source2: gnuplot-init.el
Patch1: gnuplot-4.2.0-refers_to.patch
Patch2: gnuplot-4.2.0-fonts.patch
Patch10: gnuplot-4.6.2-texinfo-5.patch
BuildRequires: libpng-devel, texlive-pdftex, zlib-devel, libX11-devel, emacs
BuildRequires: texinfo, readline-devel, libXt-devel, gd-devel, wxGTK-devel >= 2.8.12-2m
BuildRequires: latex2html, librsvg2, giflib-devel, libotf
BuildRequires: lua-devel, pango-devel, cairo-devel
BuildRequires: libtiff-devel >= 4.0.1
Requires: %{name}-common = %{version}-%{release}
Requires: dejavu-sans-fonts
Requires(post): %{_sbindir}/alternatives
Requires(preun): %{_sbindir}/alternatives
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

### include local configuration
%{?include_specopt}
### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpm/specopt/gnuplot.specopt and edit it.
%{?!with_wxGTK: %global with_wxGTK 1}

%description
Gnuplot is a command-line driven, interactive function plotting
program especially suited for scientific data representation.  Gnuplot
can be used to plot functions and data points in both two and three
dimensions and in many different formats.

Install gnuplot if you need a graphics package for scientific data
representation.

%package common
Group: Applications/Engineering
Summary: The common gnuplot parts
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description common
Gnuplot is a command-line driven, interactive function plotting
program especially suited for scientific data representation.  Gnuplot
can be used to plot functions and data points in both two and three
dimensions and in many different formats.

This subpackage contains common parts needed for arbitrary version of gnuplot

%package minimal
Group: Applications/Engineering
Summary: Minimal version of program for plotting mathematical expressions and data
Requires: %{name}-common = %{version}-%{release}
Requires(post): %{_sbindir}/alternatives
Requires(preun): %{_sbindir}/alternatives

%description minimal
Gnuplot is a command-line driven, interactive function plotting
program especially suited for scientific data representation.  Gnuplot
can be used to plot functions and data points in both two and three
dimensions and in many different formats.

Install gnuplot-minimal if you need a minimal version of graphics package 
for scientific data representation.

%package -n emacs-%{name}
Group: Applications/Engineering
Summary: Emacs bindings for the gnuplot main application
Requires: %{name} = %{version}-%{release}
BuildRequires:  emacs emacs-el pkgconfig
Requires: emacs >= %{_emacs_version}
BuildArch: noarch
Provides: gnuplot-emacs = %{version}-%{release}
Obsoletes: gnuplot-emacs < 4.2.2-3

%description -n emacs-%{name}
The gnuplot-emacs package contains the emacs related .elc files so that gnuplot
nicely interacts and integrates into emacs.

%package -n emacs-%{name}-el
Group: Applications/Engineering
Summary: Emacs bindings for the gnuplot main application
Requires: emacs-%{name} = %{version}-%{release}
BuildArch: noarch
Obsoletes: gnuplot-emacs < 4.2.2-3

%description -n emacs-%{name}-el
The gnuplot-emacs package contains the emacs related .el files so that gnuplot
nicely interacts and integrates into emacs.

%package  doc
Group: Applications/Engineering
Summary: Documentation fo bindings for the gnuplot main application
Obsoletes: gnuplot-common < 4.2.4-5
BuildArch: noarch

%description doc
The gnuplot-doc package contains the documentation related to gnuplot 
plotting tool

%package  latex
Group: Applications/Engineering
Summary: Configuration for LaTeX typesetting using gnuplot
Requires: texlive-pdftex
Requires: %{name} = %{version}-%{release}
BuildArch: noarch
Obsoletes: gnuplot-common < 4.2.5-2

%description latex
The gnuplot-latex package contains LaTeX configuration file related to gnuplot 
plotting tool.


%prep
%setup -q
%patch1 -p1 -b .refto
%patch2 -p1 -b .font
%patch10 -p1 -b .texinfo5
sed -i -e 's:"/usr/lib/X11/app-defaults":"%{x11_app_defaults_dir}":' src/gplt_x11.c
iconv -f windows-1252 -t utf-8 ChangeLog > ChangeLog.aux
mv ChangeLog.aux ChangeLog
chmod 644 src/getcolor.h
chmod 644 demo/html/webify.pl
chmod 644 demo/html/webify_svg.pl
chmod 644 demo/html/webify_canvas.pl

%build
# at first create minimal version of gnuplot for server SIG purposes
%configure --with-readline=gnu --with-png --without-linux-vga \
 --enable-history-file --disable-wxwidgets \
 --without-cairo 
make %{?_smp_mflags}
mv src/gnuplot src/gnuplot-minimal

# clean all settings
make clean
# create full version of gnuplot
%configure --with-readline=gnu --with-png --without-linux-vga \
 --enable-history-file \
%if !%{with_wxGTK}
 --disable-wxwidgets
%else
 --enable-wxwidgets
%endif
make %{?_smp_mflags}

cd docs
export TEX4HTENV=/usr/share/texmf-dist/tex4ht/base/unix/tex4ht.env
make html
cd psdoc
export GNUPLOT_PS_DIR=../../term/PostScript
make ps_symbols.ps ps_fontfile_doc.pdf
cd ../..
rm -rf docs/htmldocs/images.idx

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
install -d %{buildroot}/%{_emacs_sitestartdir}/
install -p -m 644 %SOURCE2 %{buildroot}/%{_emacs_sitestartdir}//gnuplot-init.el
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_datadir}/emacs/site-lisp/info-look*.el*
install -d %{buildroot}/%{_emacs_sitelispdir}/%{name}
mv %{buildroot}%{_datadir}/emacs/site-lisp/gnuplot.el{,c} %{buildroot}/%{_emacs_sitelispdir}/%{name}
mv %{buildroot}%{_datadir}/emacs/site-lisp/gnuplot-eldoc.el{,c} %{buildroot}/%{_emacs_sitelispdir}/%{name}
mv %{buildroot}%{_datadir}/emacs/site-lisp/gnuplot-gui.el{,c} %{buildroot}/%{_emacs_sitelispdir}/%{name}

mkdir -p %{buildroot}%{x11_app_defaults_dir}
mv %{buildroot}%{_datadir}/gnuplot/%{major}.%{minor}/app-defaults/Gnuplot %{buildroot}%{x11_app_defaults_dir}/Gnuplot
rm -rf %{buildroot}%{_libdir}/

# rename binary
mv %{buildroot}%{_bindir}/gnuplot %{buildroot}%{_bindir}/gnuplot-wx
# install minimal binary
install -p -m 755 ./src/gnuplot-minimal %{buildroot}%{_bindir}/gnuplot-minimal

%posttrans
%{_sbindir}/alternatives --install %{_bindir}/gnuplot gnuplot %{_bindir}/gnuplot-wx 60

%post common
if [ -f %{_infodir}/gnuplot.info* ]; then
    /sbin/install-info %{_infodir}/gnuplot.info %{_infodir}/dir || :
fi

%posttrans minimal
%{_sbindir}/alternatives --install %{_bindir}/gnuplot gnuplot %{_bindir}/gnuplot-minimal 40

%preun
if [ $1 = 0 ]; then
    %{_sbindir}/alternatives --remove gnuplot %{_bindir}/gnuplot-wx || :
fi

%preun common
if [ $1 = 0 ] ; then # last uninstall
    if [ -f %{_infodir}/gnuplot.info* ]; then
        /sbin/install-info --delete %{_infodir}/gnuplot.info %{_infodir}/dir || :
    fi
fi

%preun minimal
if [ $1 = 0 ]; then
    %{_sbindir}/alternatives --remove gnuplot %{_bindir}/gnuplot-minimal || :
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog Copyright
%{_bindir}/gnuplot-wx

%files doc
%defattr(-,root,root,-)
%doc ChangeLog Copyright
%doc docs/psdoc/ps_guide.ps docs/psdoc/ps_symbols.ps demo docs/psdoc/ps_file.doc
%doc docs/psdoc/ps_fontfile_doc.pdf docs/htmldocs

%files common
%defattr(-,root,root,-)
%doc BUGS ChangeLog Copyright NEWS README
%{_mandir}/man1/gnuplot.1.*
%dir %{_datadir}/gnuplot
%dir %{_datadir}/gnuplot/%{major}.%{minor}
%dir %{_datadir}/gnuplot/%{major}.%{minor}/PostScript
%{_datadir}/gnuplot/%{major}.%{minor}/PostScript/*.ps
%{_datadir}/gnuplot/%{major}.%{minor}/PostScript/aglfn.txt
%dir %{_datadir}/gnuplot/%{major}.%{minor}/js
%{_datadir}/gnuplot/%{major}.%{minor}/js/*
%dir %{_datadir}/gnuplot/%{major}.%{minor}/lua/
%{_datadir}/gnuplot/%{major}.%{minor}/lua/gnuplot-tikz.lua
%{_datadir}/gnuplot/%{major}.%{minor}/gnuplot.gih
%{_datadir}/gnuplot/%{major}.%{minor}/colors_*
%{_datadir}/gnuplot/%{major}.%{minor}/gnuplotrc
%dir %{_libexecdir}/gnuplot
%dir %{_libexecdir}/gnuplot/%{major}.%{minor}
%{_libexecdir}/gnuplot/%{major}.%{minor}/gnuplot_x11
%{x11_app_defaults_dir}/Gnuplot
%{_infodir}/gnuplot.info.*

%files minimal
%defattr(-,root,root,-)
%doc ChangeLog Copyright
%{_bindir}/gnuplot-minimal

%files -n emacs-%{name}
%defattr(-,root,root,-)
%doc ChangeLog Copyright
%dir %{_emacs_sitelispdir}/%{name}
%{_emacs_sitelispdir}/%{name}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n emacs-%{name}-el
%defattr(-,root,root,-)
%doc ChangeLog Copyright
%{_emacs_sitelispdir}/%{name}/*.el

%files latex
%defattr(-,root,root,-)
%doc ChangeLog Copyright
%dir %{_datadir}/texmf/tex/latex/gnuplot
%{_datadir}/texmf/tex/latex/gnuplot/gnuplot.cfg
%{_datadir}/texmf/tex/latex/gnuplot/gnuplot-lua-tikz.sty
%{_datadir}/texmf/tex/latex/gnuplot/gnuplot-lua-tikz-common.tex
%{_datadir}/texmf/tex/latex/gnuplot/gnuplot-lua-tikz.tex
%{_datadir}/texmf/tex/latex/gnuplot/t-gnuplot-lua-tikz.tex

%changelog
* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-2m)
- fix gnuplot.texi error

* Wed May  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.3-1m)
- update to 4.6.3

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-4m)
- fix build failure; use TEX4HTENV so that t4ht uses collect config file

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-3m)
- rebuild for emacs-24.1

* Tue Apr 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.0-2m)
- rebuild against libtiff-4.0.1

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to 4.6.0

* Thu Dec  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- fix configure option when with_wxGTK was defined

* Wed Dec  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.4-1m)
- update 4.4.4
- re-import from fedora

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-1m)
- update 4.4.3
- add "Provides: gnuplot-common"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.1-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.1-1m)
- update 4.4.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.0-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.0-0.0.20090708.4m)
- rebuild against readline6

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.0-0.0.20090708.3m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.0-0.0.20090708.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.0-0.0.20090708-1m)
- update cvs snapshot 20090708

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.0-0.0.20081121.2m)
- ignore term-ja.diff miss patching

* Thu May 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.0-0.0.20081121.1m)
- update to cvs snapshot 20081121

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.0-0.0.20080526.3m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.0-0.0.20080526.2m)
- fix %%post, %%postun

* Sat May 27 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.3.0-0.0.20080526.1m)
- update to cvs snapshot 20080526

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.0-4m)
- %%NoSource -> NoSource

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-3m)
- rebuild against wxGTK-2.8.6

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.0-2m)
- fix %%changelog section

* Tue Mar 27 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.2-2m)
- add -c option to dvipdfmx for making gnuplot.pdf
- add term-ja.patch
- enable "make all" of psdoc block
 
* Tue Mar 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2-1m)
- updated to 4.2

* Sun Feb 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2-0.4.1m)
- updated to 4.2rc4
- added specopt "with_wxGTK" to skip wxGTK support

* Sat Nov 11 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.2-0.1.1m)
- updated to 4.2rc1
- droped "plus patch" since it has not been maintained.
- - use mbfont: or wxt terminal for displaying Japanses on screen
- - use Enhanced Postscript on ps/eps or epslatex terminal to show formula in graphs

* Tue Feb 15 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.0-2m)
- dvipdfm -> dvipdfmx

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.0.0-1m)
- version 4.0.0 has been released
- include some japanese docs
- - html and info files cannot be made now
- - if you want a japanese terminal help, export GNUHELP=/usr/share/gnuplot/gnuplot-jp.gih

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.8j.0-4m)
- patch verup

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (3.8j.0-3m)
- revised spec for enabling rpm 4.2.

* Thu Dec 18 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.8j.0-2m)
- apply "plus patch" for gnuplot-3.8
  see http://f21.aaacafe.ne.jp/~oden/software/gnuplot.html

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (kossori)
- License: see "Copyright" instead of "see Copyright"

* Sun Oct 19 2003 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.8j.0-1m)
- update to 3.8j.0

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.8i.0-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Jul 28 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.8i.0-3m)
- modify BuildPreReq: tetex-dvips -> tetex-dvipsk

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.8i.0-2m)
- rebuild against for gd

* Mon Jan 06 2003 Kenta MURATA <muraken@momonga-linux.org>
- (3.8i.0-1m)
- version up to 3.8i.0.

* Tue Oct 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.7.2-1m)
- change version notation

* Mon May  6 2002 zunda <zunda@kondara.org>
- (3.7.2+1.2.0-0.4k)
- gnuplot3.7.2+1.2.0rc2
- source update to rc2

* Tue Apr 30 2002 zunda <zunda@kondara.org>
- (3.7.1+1.2.0-0.3k)
- Oops, need to distribute the patch along with the binary package!

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.7.1+1.2.0-0.2k)
- /sbin/install-info -> info in PreReq.

* Mon Apr 29 2002 zunda <zunda@kondara.org>
- (3.7.2+1.2.0-0.1k)
- gnuplot3.7.2+1.2.0rc1
- source update
- demo/ added to %doc

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.7.1+1.2.0-8k)
- rebuild against libpng 1.2.2.

* Sat Dec  1 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (3.7.1+1.2.0-6k)
- change URL

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (3.7.1+1.2.0-4k)
- rebuild against libpng 1.2.0.

* Wed Jan 25 2001 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (3.7.1+1.2.0-3k)
- update patch to 1.2.0
- do not use GNU readline !

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Fri Jul 14 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (3.7.1+1.1.9-2k)
- remove gd from Requires:
- touch all files before configure instead of fix in %%install 

* Wed May 24 2000 AYUHANA Tomonori <l@kondara.org>
- fix %%install section (binary contains %{buildroot})
- add -q at %setup

* Sun May 14 2000 SUGAWARA Zenta <sugazen@pop02.odn.ne.jp>
- move %defattr to the head of %files section

* Fri Jan 28 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- Kondara fix
- prefix changed and optimization flags added

* Fri Dec  3 1999 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- apply gnuplot3.7.1+1.1.9.bugfix

* Mon Nov  8 1999 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- 3.7.1+1.1.9

* Fri Sep  3 1999 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- 3.7.0.1+1.1.9

* Tue Jun 11 1999 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- add information about /sbin/install-info

* Tue Jun  1 1999 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- 3.7+1.1.9

* Tue Jan 29 1999 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- first release
