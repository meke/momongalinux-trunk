# Generated from turn-0.9.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname turn

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Test Reporters (New) -- new output formats for Testing
Name: rubygem-%{gemname}
Version: 0.9.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: MIT
URL: http://rubygems.org/gems/turn
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(ansi) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Turn provides a set of alternative runners for MiniTest, both colorful and
informative.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/turn
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE-MIT.txt
%doc %{geminstdir}/Release.txt
%doc %{geminstdir}/LICENSE.txt
%doc %{geminstdir}/Version.txt
%doc %{geminstdir}/LICENSE-RUBY.txt
%doc %{geminstdir}/LICENSE-GPL2.txt
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.md
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- update 0.9.4

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3-1m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3-1m)
- Initial commit Momonga Linux
