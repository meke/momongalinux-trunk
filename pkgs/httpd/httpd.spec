%global momorel 1

Name: httpd

### include local configuration
%{?include_specopt}

%define contentdir %{_datadir}/httpd
%define docroot /var/www
%{?!suexec_caller_user: %global suexec_caller_user apache}
%{?!suexec_caller_group: %global suexec_caller_group apache}
%{?!suexec_uidmin: %global suexec_uidmin 500}
%{?!suexec_gidmin: %global suexec_gidmin 500}
%{?!suexec_docroot: %global suexec_docroot %{contentdir}}
%define mmn 20120211
%define mmnisa %{mmn}-%{__isa_name}-%{__isa_bits}
%define vstring Momonga
%define distro Momonga Linux
%define mpms worker event

Summary: Apache HTTP Server
Version: 2.4.9
Release: %{momorel}m%{?dist}
URL: http://httpd.apache.org/
Source0: http://www.apache.org/dist/httpd/httpd-%{version}.tar.bz2
NoSource: 0
Source1: index.html
Source2: httpd.logrotate
Source3: httpd.sysconf
Source4: httpd-ssl-pass-dialog
Source5: httpd.tmpfiles
Source6: httpd.service
Source7: action-graceful.sh
Source8: action-configtest.sh
Source10: httpd.conf
Source11: 00-base.conf
Source12: 00-mpm.conf
Source13: 00-lua.conf
Source14: 01-cgi.conf
Source15: 00-dav.conf
Source16: 00-proxy.conf
Source17: 00-ssl.conf
Source18: 01-ldap.conf
Source19: 00-proxyhtml.conf
Source20: userdir.conf
Source21: ssl.conf
Source22: welcome.conf
Source23: manual.conf
Source24: 00-systemd.conf
# Documentation
Source30: README.confd
Source40: htcacheclean.service
Source41: htcacheclean.sysconf
# Momonga specific
Source50: powered_by_momonga.png
Source51: error-messages.tar.bz2
# build/scripts patches
Patch1: httpd-2.4.1-apctl.patch
Patch2: httpd-2.4.9-apxs.patch
Patch3: httpd-2.4.1-deplibs.patch
Patch5: httpd-2.4.3-layout.patch
Patch6: httpd-2.4.3-apctl-systemd.patch
# Features/functional changes
Patch23: httpd-2.4.4-export.patch
Patch24: httpd-2.4.1-corelimit.patch
Patch25: httpd-2.4.1-selinux.patch
Patch26: httpd-2.4.4-r1337344+.patch
Patch27: httpd-2.4.2-icons.patch
Patch29: httpd-2.4.3-mod_systemd.patch
Patch30: httpd-2.4.4-cachehardmax.patch
Patch31: httpd-2.4.6-sslmultiproxy.patch
Patch32: httpd-2.4.7-r1537535.patch
# Bug fixes
Patch51: httpd-2.4.9-sslsninotreq.patch
Patch55: httpd-2.4.4-malformed-host.patch
Patch56: httpd-2.4.4-mod_unique_id.patch
# for Momonga layout
Patch60: httpd-2.4.3-momonga-layout.patch

License: ASL 2.0
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, perl, pkgconfig, findutils
BuildRequires: zlib-devel, libselinux-devel
BuildRequires: expat-devel
BuildRequires: xmlto >= 0.0.11
BuildRequires: apr-devel >= 1.2.8-3m, apr-util-devel >= 1.3.4, pcre-devel >= 8.31
Requires: initscripts >= 8.36, /etc/mime.types, system-logos
Requires: gawk, findutils
Obsoletes: httpd-suexec, httpd-apr, mod_auth_ldap
Requires(pre): chkconfig, mktemp, coreutils
Requires(pre): sh-utils, textutils, shadow-utils
Requires(post): chkconfig
Provides: webserver
Provides: mod_dav = %{version}-%{release}, httpd-suexec = %{version}-%{release}
Provides: httpd-mmn = %{mmn}, httpd-mmn = %{mmnisa}
Obsoletes: apache, secureweb, mod_dav, mod_gzip, stronghold-apache
Obsoletes: stronghold-htdocs, mod_put, mod_roaming
## httpd-2.4.3
Obsoletes: mod_auth_mysql, mod_fastcgi, mod_ruby, mod_uploader
Conflicts: pcre < 4.0
Requires: httpd-tools = %{version}-%{release}, apr-util-ldap

%description
The Apache HTTP Server is a powerful, efficient, and extensible
web server.

%package devel
Group: Development/Libraries
Summary: Development interfaces for the Apache HTTP server.
Obsoletes: secureweb-devel, apache-devel, stronghold-apache-devel
Requires: apr-devel, apr-util-devel, pkgconfig
Requires: httpd = %{version}-%{release}

%description devel
The httpd-devel package contains the APXS binary and other files
that you need to build Dynamic Shared Objects (DSOs) for the
Apache HTTP Server.

If you are installing the Apache HTTP server and you want to be
able to compile or develop additional modules for Apache, you need
to install this package.

%package manual
Group: Documentation
Summary: Documentation for the Apache HTTP server
Requires: httpd = %{version}-%{release}
Obsoletes: secureweb-manual, apache-manual
BuildArch: noarch

%description manual
The httpd-manual package contains the complete manual and
reference guide for the Apache HTTP server. The information can
also be found at http://httpd.apache.org/docs/2.2/.

%package tools
Group: System Environment/Daemons
Summary: Tools for use with the Apache HTTP Server

%description tools
The httpd-tools package contains tools which can be used with 
the Apache HTTP Server.

%package -n mod_ssl
Group: System Environment/Daemons
Summary: SSL/TLS module for the Apache HTTP Server
BuildRequires: openssl-devel >= 1.0.0
Requires(post): openssl >= 1.0.0, /bin/cat
Requires: httpd = %{version}-%{release}, httpd-mmn = %{mmn}
Obsoletes: stronghold-mod_ssl

%description -n mod_ssl
The mod_ssl module provides strong cryptography for the Apache Web
server via the Secure Sockets Layer (SSL) and Transport Layer
Security (TLS) protocols.

%package -n mod_proxy_html
Group: System Environment/Daemons
Summary: HTML and XML content filters for the Apache HTTP Server
Requires: httpd = %{version}-%{release}, httpd-mmn = %{mmnisa}
BuildRequires: libxml2-devel

%description -n mod_proxy_html
The mod_proxy_html and mod_xml2enc modules provide filters which can
transform and modify HTML and XML content.

%package -n mod_ldap
Group: System Environment/Daemons
Summary: LDAP authentication modules for the Apache HTTP Server
Requires: httpd = %{version}-%{release}, httpd-mmn = %{mmnisa}
Requires: apr-util-ldap

%description -n mod_ldap
The mod_ldap and mod_authnz_ldap modules add support for LDAP
authentication to the Apache HTTP Server.

%prep
%setup -q
%patch1 -p1 -b .apctl
%patch2 -p1 -b .apxs
%patch3 -p1 -b .deplibs
%patch5 -p1 -b .layout
%patch6 -p1 -b .apctlsystemd

%patch23 -p1 -b .export
%patch24 -p1 -b .corelimit
%patch25 -p1 -b .selinux
%patch26 -p1 -b .r1337344+
%patch27 -p1 -b .icons
%patch29 -p1 -b .systemd
%patch30 -p1 -b .cachehardmax
%patch31 -p1 -b .sslmultiproxy
%patch32 -p1 -b .r1537535

%patch51 -p1 -b .sninotreq
%patch55 -p1 -b .malformedhost
%patch56 -p1 -b .uniqueid

%patch60 -p1 -b .momonga

# Patch in the vendor string
sed -i '/^#define PLATFORM/s/Unix/%{vstring}/' os/unix/os.h

# Prevent use of setcap in "install-suexec-caps" target.
sed -i '/suexec/s,setcap ,echo Skipping setcap for ,' Makefile.in

# Safety check: prevent build if defined MMN does not equal upstream MMN.
vmmn=`echo MODULE_MAGIC_NUMBER_MAJOR | cpp -include include/ap_mmn.h | sed -n '/^2/p'`
if test "x${vmmn}" != "x%{mmn}"; then
   : Error: Upstream MMN is now ${vmmn}, packaged MMN is %{mmn}
   : Update the mmn macro and rebuild.
   exit 1
fi

: Building with MMN %{mmn}, MMN-ISA %{mmnisa} and vendor string '%{vstring}'

%build
# forcibly prevent use of bundled apr, apr-util, pcre
rm -rf srclib/{apr,apr-util,pcre}

# regenerate configure scripts
autoheader && autoconf || exit 1

# Before configure; fix location of build dir in generated apxs
%{__perl} -pi -e "s:\@exp_installbuilddir\@:%{_libdir}/httpd/build:g" \
	support/apxs.in

export CFLAGS=$RPM_OPT_FLAGS
export LDFLAGS="-Wl,-z,relro,-z,now"

# Hard-code path to links to avoid unnecessary builddep
export LYNX_PATH=/usr/bin/links

# Build the daemon
./configure \
 	--prefix=%{_sysconfdir}/httpd \
 	--exec-prefix=%{_prefix} \
 	--bindir=%{_bindir} \
 	--sbindir=%{_sbindir} \
 	--mandir=%{_mandir} \
	--libdir=%{_libdir} \
	--sysconfdir=%{_sysconfdir}/httpd/conf \
	--includedir=%{_includedir}/httpd \
	--libexecdir=%{_libdir}/httpd/modules \
	--datadir=%{contentdir} \
%if %{_lib} == "lib64"
        --enable-layout=Momonga64 \
%else
        --enable-layout=Momonga \
%endif
        --with-installbuilddir=%{_libdir}/httpd/build \
        --enable-mpms-shared=all \
        --with-apr=%{_prefix} --with-apr-util=%{_prefix} \
	--enable-suexec --with-suexec \
        --enable-suexec-capabilities \
	--with-suexec-caller=%{suexec_caller_user} \
	--with-suexec-docroot=%{docroot} \
	--without-suexec-logfile \
        --with-suexec-syslog \
	--with-suexec-bin=%{_sbindir}/suexec \
	--with-suexec-uidmin=500 --with-suexec-gidmin=100 \
        --enable-pie \
        --with-pcre \
        --enable-mods-shared=all \
	--enable-ssl --with-ssl --disable-distcache \
	--enable-proxy \
        --enable-cache \
        --enable-disk-cache \
        --enable-ldap --enable-authnz-ldap \
        --enable-cgid --enable-cgi \
        --enable-authn-anon --enable-authn-alias \
        --disable-imagemap  \
        --disable-session
	$*
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

# Install systemd service files
mkdir -p %{buildroot}%{_unitdir}
for s in httpd htcacheclean; do
  install -p -m 644 $RPM_SOURCE_DIR/${s}.service \
                    %{buildroot}%{_unitdir}/${s}.service
done

# install conf file/directory
mkdir %{buildroot}%{_sysconfdir}/httpd/conf.d \
      %{buildroot}%{_sysconfdir}/httpd/conf.modules.d
install -m 644 $RPM_SOURCE_DIR/README.confd \
    %{buildroot}%{_sysconfdir}/httpd/conf.d/README
for f in 00-base.conf 00-mpm.conf 00-lua.conf 01-cgi.conf 00-dav.conf \
         00-proxy.conf 00-ssl.conf 01-ldap.conf 00-proxyhtml.conf \
         01-ldap.conf 00-systemd.conf; do
  install -m 644 -p $RPM_SOURCE_DIR/$f \
        %{buildroot}%{_sysconfdir}/httpd/conf.modules.d/$f
done

for f in welcome.conf ssl.conf manual.conf userdir.conf; do
  install -m 644 -p $RPM_SOURCE_DIR/$f \
        %{buildroot}%{_sysconfdir}/httpd/conf.d/$f.dist
done

# Split-out extra config shipped as default in conf.d:
for f in autoindex; do
  mv docs/conf/extra/httpd-${f}.conf \
        %{buildroot}%{_sysconfdir}/httpd/conf.d/${f}.conf.dist
done

# Extra config trimmed:
rm -v docs/conf/extra/httpd-{ssl,userdir}.conf

rm %{buildroot}%{_sysconfdir}/httpd/conf/*.conf
install -m 644 -p $RPM_SOURCE_DIR/httpd.conf \
   %{buildroot}%{_sysconfdir}/httpd/conf/httpd.conf

mkdir %{buildroot}%{_sysconfdir}/sysconfig
for s in httpd htcacheclean; do
  install -m 644 -p $RPM_SOURCE_DIR/${s}.sysconf \
                    %{buildroot}%{_sysconfdir}/sysconfig/${s}
done

# tmpfiles.d configuration
mkdir -p %{buildroot}%{_prefix}/lib/tmpfiles.d 
install -m 644 -p $RPM_SOURCE_DIR/httpd.tmpfiles \
   %{buildroot}%{_prefix}/lib/tmpfiles.d/httpd.conf

# Other directories
mkdir -p %{buildroot}%{_localstatedir}/lib/dav \
         %{buildroot}/run/httpd/htcacheclean

# Create cache directory
mkdir -p %{buildroot}%{_localstatedir}/cache/httpd \
         %{buildroot}%{_localstatedir}/cache/httpd/proxy \
         %{buildroot}%{_localstatedir}/cache/httpd/ssl

# Make the MMN accessible to module packages
echo %{mmnisa} > %{buildroot}%{_includedir}/httpd/.mmn
mkdir -p %{buildroot}%{_sysconfdir}/rpm
cat > %{buildroot}%{_sysconfdir}/rpm/macros.httpd <<EOF
%%_httpd_mmn %{mmnisa}
%%_httpd_apxs %%{_bindir}/apxs
%%_httpd_modconfdir %%{_sysconfdir}/httpd/conf.modules.d
%%_httpd_confdir %%{_sysconfdir}/httpd/conf.d
%%_httpd_contentdir %{contentdir}
%%_httpd_moddir %%{_libdir}/httpd/modules
EOF

# Handle contentdir
mkdir %{buildroot}%{contentdir}/noindex
%{__tar} jxf %{SOURCE51} -C  %{buildroot}%{contentdir}
install -m 644 -p $RPM_SOURCE_DIR/index.html \
        %{buildroot}%{contentdir}/noindex/index.html
rm -rf %{contentdir}/htdocs

# remove manual sources
find %{buildroot}%{contentdir}/manual \( \
    -name \*.xml -o -name \*.xml.* -o -name \*.ent -o -name \*.xsl -o -name \*.dtd \
    \) -print0 | xargs -0 rm -f

# Strip the manual down just to English and replace the typemaps with flat files:
set +x
for f in `find %{buildroot}%{contentdir}/manual -name \*.html -type f`; do
   if test -f ${f}.en; then
      cp ${f}.en ${f}
      rm ${f}.*
   fi
done
set -x

# Clean Document Root
rm -v %{buildroot}%{docroot}/html/*.html \
      %{buildroot}%{docroot}/cgi-bin/*

# install icon
install -m 644 %{SOURCE50} \
	%{buildroot}%{contentdir}/icons

# Symlink for the powered-by-$DISTRO image:
ln -s ../../pixmaps/poweredby.png \
        %{buildroot}%{contentdir}/icons/poweredby.png

# symlinks for /etc/httpd
ln -s ../..%{_localstatedir}/log/httpd %{buildroot}/etc/httpd/logs
ln -s /run/httpd %{buildroot}/etc/httpd/run
ln -s ../..%{_libdir}/httpd/modules %{buildroot}/etc/httpd/modules

# install http-ssl-pass-dialog
mkdir -p %{buildroot}%{_libexecdir}
install -m755 $RPM_SOURCE_DIR/httpd-ssl-pass-dialog \
	%{buildroot}%{_libexecdir}/httpd-ssl-pass-dialog

# Install action scripts
mkdir -p %{buildroot}%{_libexecdir}/initscripts/legacy-actions/httpd
for f in graceful configtest; do
    install -p -m 755 $RPM_SOURCE_DIR/action-${f}.sh \
            %{buildroot}%{_libexecdir}/initscripts/legacy-actions/httpd/${f}
done

# Install logrotate config
mkdir -p %{buildroot}/etc/logrotate.d
install -m 644 -p $RPM_SOURCE_DIR/httpd.logrotate \
	%{buildroot}/etc/logrotate.d/httpd

# fix man page paths
sed -e "s|/usr/local/apache2/conf/httpd.conf|/etc/httpd/conf/httpd.conf|" \
    -e "s|/usr/local/apache2/conf/mime.types|/etc/mime.types|" \
    -e "s|/usr/local/apache2/conf/magic|/etc/httpd/conf/magic|" \
    -e "s|/usr/local/apache2/logs/error_log|/var/log/httpd/error_log|" \
    -e "s|/usr/local/apache2/logs/access_log|/var/log/httpd/access_log|" \
    -e "s|/usr/local/apache2/logs/httpd.pid|/run/httpd/httpd.pid|" \
    -e "s|/usr/local/apache2|/etc/httpd|" < docs/man/httpd.8 \
  > %{buildroot}%{_mandir}/man8/httpd.8

# Make ap_config_layout.h libdir-agnostic
sed -i '/.*DEFAULT_..._LIBEXECDIR/d;/DEFAULT_..._INSTALLBUILDDIR/d' \
    %{buildroot}%{_includedir}/httpd/ap_config_layout.h

# Fix path to instdso in special.mk
sed -i '/instdso/s,top_srcdir,top_builddir,' \
    %{buildroot}%{_libdir}/httpd/build/special.mk

# Remove unpackaged files
rm -vf \
      %{buildroot}%{_libdir}/*.exp \
      %{buildroot}/etc/httpd/conf/mime.types \
      %{buildroot}%{_libdir}/httpd/modules/*.exp \
      %{buildroot}%{_libdir}/httpd/build/config.nice \
      %{buildroot}%{_bindir}/ap?-config \
      %{buildroot}%{_sbindir}/{checkgid,dbmmanage,envvars*} \
      %{buildroot}%{contentdir}/htdocs/* \
      %{buildroot}%{_mandir}/man1/dbmmanage.* \
      %{buildroot}%{contentdir}/cgi-bin/*

rm -rf %{buildroot}/etc/httpd/conf/{original,extra}

%pre
# Add the "apache" user
/usr/sbin/useradd -c "Apache" -u 48 \
	-s /sbin/nologin -r -d %{contentdir} apache 2> /dev/null || :

%post
%systemd_post httpd.service htcacheclean.service

%preun
%systemd_preun httpd.service htcacheclean.service

%postun
%systemd_postun

# Trigger for conversion from SysV, per guidelines at:
# https://fedoraproject.org/wiki/Packaging:ScriptletSnippets#Systemd
%triggerun -- httpd < 2.2.21-5
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply httpd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save httpd.service >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del httpd >/dev/null 2>&1 || :

%posttrans
test -f /etc/sysconfig/httpd-disable-posttrans || \
  /bin/systemctl try-restart httpd.service htcacheclean.service >/dev/null 2>&1 || :

%define sslcert %{_sysconfdir}/pki/tls/certs/localhost.crt
%define sslkey %{_sysconfdir}/pki/tls/private/localhost.key

%post -n mod_ssl
umask 077

if [ -f %{sslkey} -o -f %{sslcert} ]; then
   exit 0
fi

%{_bindir}/openssl genrsa -rand /proc/apm:/proc/cpuinfo:/proc/dma:/proc/filesystems:/proc/interrupts:/proc/ioports:/proc/pci:/proc/rtc:/proc/uptime 1024 > %{sslkey} 2> /dev/null

FQDN=`hostname`
if [ "x${FQDN}" = "x" ]; then
   FQDN=localhost.localdomain
fi

cat << EOF | %{_bindir}/openssl req -new -key %{sslkey} \
         -x509 -days 365 -set_serial $RANDOM -extensions v3_req \
         -out %{sslcert} 2>/dev/null
--
SomeState
SomeCity
SomeOrganization
SomeOrganizationalUnit
${FQDN}
root@${FQDN}
EOF

%check
# Check the built modules are all PIC
if readelf -d %{buildroot}%{_libdir}/httpd/modules/*.so | grep TEXTREL; then
   : modules contain non-relocatable code
   exit 1
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)

%doc ABOUT_APACHE README CHANGES LICENSE VERSIONING NOTICE
%doc docs/conf/extra/*.conf

%dir %{_sysconfdir}/httpd
%{_sysconfdir}/httpd/modules
%{_sysconfdir}/httpd/logs
%{_sysconfdir}/httpd/run
%dir %{_sysconfdir}/httpd/conf
%config(noreplace) %{_sysconfdir}/httpd/conf/httpd.conf
%config(noreplace) %{_sysconfdir}/httpd/conf/magic

%config(noreplace) %{_sysconfdir}/logrotate.d/httpd

%dir %{_sysconfdir}/httpd/conf.d
%{_sysconfdir}/httpd/conf.d/README
%config(noreplace) %{_sysconfdir}/httpd/conf.d/*.conf.dist
%exclude %{_sysconfdir}/httpd/conf.d/ssl.conf.dist
%exclude %{_sysconfdir}/httpd/conf.d/manual.conf.dist

%dir %{_sysconfdir}/httpd/conf.modules.d
%config(noreplace) %{_sysconfdir}/httpd/conf.modules.d/*.conf
%exclude %{_sysconfdir}/httpd/conf.modules.d/00-ssl.conf
%exclude %{_sysconfdir}/httpd/conf.modules.d/00-proxyhtml.conf
%exclude %{_sysconfdir}/httpd/conf.modules.d/01-ldap.conf

%config(noreplace) %{_sysconfdir}/sysconfig/ht*
%{_prefix}/lib/tmpfiles.d/httpd.conf

%dir %{_libexecdir}/initscripts/legacy-actions/httpd
%{_libexecdir}/initscripts/legacy-actions/httpd/*

%{_sbindir}/ht*
%{_sbindir}/fcgistarter
%{_sbindir}/apachectl
%{_sbindir}/rotatelogs
%caps(cap_setuid,cap_setgid+pe) %attr(510,root,%{suexec_caller_user}) %{_sbindir}/suexec

%dir %{_libdir}/httpd
%dir %{_libdir}/httpd/modules
%{_libdir}/httpd/modules/mod*.so
%exclude %{_libdir}/httpd/modules/mod_ssl.so
%exclude %{_libdir}/httpd/modules/mod_*ldap.so
%exclude %{_libdir}/httpd/modules/mod_proxy_html.so
%exclude %{_libdir}/httpd/modules/mod_xml2enc.so

%dir %{contentdir}
%dir %{contentdir}/icons
%dir %{contentdir}/error
%dir %{contentdir}/error/include
%dir %{contentdir}/noindex
%{contentdir}/icons/*
%{contentdir}/error/README
%{contentdir}/error/*.var
%{contentdir}/error/include/*.html
%{contentdir}/error/ErrorCode
%{contentdir}/error/README-CSS*
%{contentdir}/error/noindex.html
%dir %{contentdir}/error/styles
%{contentdir}/error/styles/*
%{contentdir}/noindex/index.html

%dir %{docroot}
%dir %{docroot}/cgi-bin
%dir %{docroot}/html

%attr(0710,root,apache) %dir /run/httpd
%attr(0700,apache,apache) %dir /run/httpd/htcacheclean
%attr(0700,root,root) %dir %{_localstatedir}/log/httpd
%attr(0700,apache,apache) %dir %{_localstatedir}/lib/dav
%attr(0700,apache,apache) %dir %{_localstatedir}/cache/httpd
%attr(0700,apache,apache) %dir %{_localstatedir}/cache/httpd/proxy

%{_mandir}/man8/*

%{_unitdir}/*.service

%files tools
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/*
%doc LICENSE NOTICE
%exclude %{_bindir}/apxs
%exclude %{_mandir}/man1/apxs.1*

%files manual
%defattr(-,root,root)
%{contentdir}/manual
%config(noreplace) %{_sysconfdir}/httpd/conf.d/manual.conf.dist

%files -n mod_ssl
%defattr(-,root,root)
%{_libdir}/httpd/modules/mod_ssl.so
%config(noreplace) %{_sysconfdir}/httpd/conf.modules.d/00-ssl.conf
%config(noreplace) %{_sysconfdir}/httpd/conf.d/ssl.conf.dist
%attr(0700,apache,root) %dir %{_localstatedir}/cache/httpd/ssl
%{_libexecdir}/httpd-ssl-pass-dialog

%files -n mod_proxy_html
%defattr(-,root,root)
%{_libdir}/httpd/modules/mod_proxy_html.so
%{_libdir}/httpd/modules/mod_xml2enc.so
%config(noreplace) %{_sysconfdir}/httpd/conf.modules.d/00-proxyhtml.conf

%files -n mod_ldap
%defattr(-,root,root)
%{_libdir}/httpd/modules/mod_*ldap.so
%config(noreplace) %{_sysconfdir}/httpd/conf.modules.d/01-ldap.conf

%files devel
%defattr(-,root,root)
%{_includedir}/httpd
%{_bindir}/apxs
%{_mandir}/man1/apxs.1*
%dir %{_libdir}/httpd/build
%{_libdir}/httpd/build/*.mk
%{_libdir}/httpd/build/*.sh
%{_sysconfdir}/rpm/macros.httpd

%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-1m)
- [SECURITY] CVE-2013-6438 CVE-2014-0098
- update to 2.4.9

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- [SECURITY] CVE-2013-1896 CVE-2013-2249
- update to 2.4.6

* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- [SECURITY] CVE-2012-3499 CVE-2012-4558
- update to 2.4.4

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3
- switch to 2.4.x

* Sat Sep 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.23-1m)
- [SECURITY] CVE-2012-0883 CVE-2012-2687
- update to 2.2.23

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.22-2m)
- rebuild against pcre-8.31

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.22-1m)
- [SECURITY] included following security fixes
- [SECURITY] CVE-2011-3368 CVE-2011-3607 CVE-2011-4317 CVE-2012-0021
- [SECURITY] CVE-2012-0031 CVE-2012-0053
- update to 2.2.22

* Sun Jan 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.21-6m)
- [SECURITY] CVE-2012-0021 CVE-2012-0053
- CVE-2012-0021: http://svn.apache.org/viewvc?view=revision&revision=1227292
- CVE-2012-0053: http://svn.apache.org/viewvc?view=revision&revision=1235454

* Tue Jan 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.21-5m)
- [SECURITY] CVE-2012-0031
- patch from http://svn.apache.org/viewvc?view=revision&revision=1230065

* Mon Nov 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.21-4m)
- [SECURITY] CVE-2011-4317

* Tue Nov 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.21-3m)
- [SECURITY] CVE-2011-3607

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.21-2m)
- [SECURITY] CVE-2011-3368

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.21-1m)
- [SECURITY] CVE-2011-3348
- update to 2.2.21

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.20-2m)
- remove dependency db4-devel

* Wed Aug 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.20-1m)
- update 2.2.20
- [SECURITY] CVE-2011-3192

* Tue Aug 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.19-2m)
- support systemd

* Sun May 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.19-1m)
- [SECURITY] CVE-2011-1928
- update to 2.2.19

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.18-1m)
- [SECURITY] CVE-2011-0419
- update to 2.2.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.17-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.17-2m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.17-1m)
- [SECURITY] CVE-2010-1623 CVE-2010-3560 CVE-2010-3720
- update to 2.2.17

* Mon Sep 13 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.16-4m)
- new powered_by_momonga.png

* Sun Sep 12 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.16-3m)
- install powered_by_momonga.png

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.16-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.16-1m)
- [SECURITY] CVE-2010-1452 CVE-2010-2068
- update to 2.2.16

* Thu Jul 22 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.15-4m)
- sync Fedora
- separate httpd-tools
- add Requires system-logos
- httpd.conf merged mod_proxy.conf mod_cache.conf mod_dav.conf mod_ldap.conf

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.15-3m)
- explicitly link libcrypt

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.15-2m)
- rebuild against openssl-1.0.0

* Fri Mar 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.15-1m)
- [SECURITY] CVE-2009-3555 CVE-2010-0408 CVE-2010-0425 CVE-2010-0434
- update to 2.2.15

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-4m)
- restart httpd after the upgrade

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-3m)
- [SECURITY] CVE-2009-3555
- apply upstream patch (Patch100)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-1m)
- [SECURITY] CVE-2009-3094 CVE-2009-3095
- update to 2.2.14 for mo6plus

* Fri Jul 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-6m)
- [SECURITY] CVE-2009-1191 CVE-2009-1195 CVE-2009-1890 CVE-2009-1891
- import upstream patches (Patch70-73)

* Tue May 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-5m)
- rebuild against apr-util-1.3.4
-- http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=521899

* Sat May  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-4m)
- noreplace %%{_sysconfdir}/httpd/conf/httpd.conf

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11
-- update Patch21,24 for fuzz=0

* Tue Oct 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10
- includes CVE-2008-2939 fixes

* Fri Aug  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.9-2m)
- [SECURITY] CVE-2008-2939

* Sun Jun 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.9-1m)
- update to 2.2.9
- this release includes CVE-2008-2364 and CVE-2007-6420 fixes
- remove unused patches

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.8-6m)
- [SECURITY] CVE-2008-2364, http://svn.apache.org/viewvc/httpd/httpd/trunk/modules/proxy/mod_proxy_http.c?r1=666154&r2=666153&pathrev=666154&view=patch

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.8-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.8-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.8-3m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.8-1m)
- rebuild against perl-5.10.0-1m

* Thu Jan 24 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.8-1m)
- up to 2.2.8
- [SECURITY] 	CVE-2007-5000 CVE-2007-6388 CVE-2007-6421 CVE-2007-6422 CVE-2008-0005
- http://www.mindedsecurity.com/MSA01150108.html 

* Tue Sep 11 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.6-1m)
- up to 2.2.6
- [SECURITY] CVE-2007-3847 CVE-2007-1863 CVE-2007-3304 CVE-2006-5752

* Tue Jun 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.4-3m)
- [SECURITY] CVE-2007-1862

* Sat Jun  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.4-2m)
- rebuild

* Wed Jan 24 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.2.4-1m)
- version up 2.2.4
- modify patch50,60 for 2.2.4

* Wed Nov 22 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-5m)
- rebuild against db4-4.5.20-2m

* Thu Nov  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-4m)
- enable lib64

* Sun Oct 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-3m)
- use config.layout (--enable-layout=Momonga) for autoconf-2.60

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.3-2m)
- rebuild against expat-2.0.0-1m

* Fri Jul 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.3-1m)
- update 2.2.3

* Wed May  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.2-1m)
- update 2.2.2
- based on fc-devel

* Wed Jan 25 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.55-4m)
- [SECURITY] CVE-2005-3357 CVE-2005-3352

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.55-3m)
- rebuild against openldap-2.3.11

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.55-2m)
- rebuild against db4.3

* Wed Oct 19 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.55-1m)
- up to 2.0.55
- fix CAN-2005-2700 CAN-2005-2088 CAN-2005-2728 CAN-2005-1268

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.54-3m)
- revise apr-config.in for large file support.

* Sat Apr 23 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.54-2m)
- modify httpd-devel files include /etc/httpd/build

* Tue Apr 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.54-1m)
- *) Remove formatting characters from ap_log_error() calls.  These
     were escaped as fallout from CAN-2003-0020.

* Thu Apr  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.53-3m)
- remove duplicated files

* Mon Feb 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.53-2m)
- no recreate png

* Tue Feb  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.53-1m)
- up to 2.0.53. large file support.

* Tue Feb  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.52-7m)
- enable specopt for suexec

* Sun Feb 06 2005 mutecat <mutecat@momonga-linux.org>
- (2.0.52-6m)
- link correction pcreposix.h

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (2.0.52-5m)
- enable x86_64.

* Sun Nov 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.52-4m)
- sync STABLE_1
  * Wed Oct 27 2004 YAMAZAKI Makoto <zaki@zakky.org>
  - (2.0.50-7m)
  - indicated the location of db4 explicitly in order to be
    correctly linked with db4.
    
  * Fri Oct  1 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
  - (2.0.50-6m)
  - apply the patches to fix CAN-2004-0747, CAN-2004-0748,
    CAN-2004-0751, CAN-2004-0786, CAN-2004-0809
  - change dist site to archive.apache.org
  - README.confd was listed twice, so remove one.

* Fri Nov 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.52-3m)
- security fix (CAN-2004-0942)

* Mon Oct 18 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.52-2m)
- security fix (CAN-2004-0885)
  reference -> http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0885
  patch92

* Thu Sep 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.52-1m)
- security fix (CAN-2004-0811)
- remove 'httpd-2.0.48-suexeclibs.patch' that was merged

* Mon Sep 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.51-2m)
- apply 'CAN-2004-0811.patch'

* Thu Sep 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.51-1m)
- including security fixes

* Sun Jul 25 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.50-4m)
- seperate mod_proxy.conf/mod_dav.conf/manual.conf

* Wed Jul 21 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0.50-3m)
- modify httpd.conf (gif -> png, AddDefaultCharset UTF-8 -> Off)

* Sun Jul 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.50-2m)
- import patch from FC2

* Tue Jun 29 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.50-1m)
- including security fixes

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.49-8m)
- apply 'CAN-2004-0493.patch'

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.49-7m)
- rebuild against db4.2

* Sat May 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.49-6m)
- adapt specopt suexec_caller_user and suexec_caller_group value
  requested by [Momonga-devel.ja:02526]

* Fri May 21 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.49-5m)
- work around sig HUP problem with PHP
- add LoadModule for PHP

* Fri May  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.49-4m)
- revise ssl.conf
- use /dev/urandom instead of /dev/random

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.49-3m)
- %%{_initscriptdir}

* Fri Apr 16 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.49-2m)
- add --with-berleley-db to configure options

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.49-1m)
- including security fixes

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.48-8m)
- modoshisugi.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.48-7m)
- revised spec for enabling rpm 4.2.

* Tue Mar  2 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.48-6m)
- enable SSL_EXPERIMENTAL

* Sun Feb 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.48-5m)
- make both prefork mpm version and worker mpm version

* Sun Feb 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.48-4m)
- now build with '--with-mpm=worker'

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.48-3m)
- rebuild against gdbm-1.8.0

* Sat Nov  1 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.48-2m)
- revise httpd.conf to prohibit DocumentRoot directory listing
    see http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/bugtraq/2003.10/msg00281.html

* Tue Oct 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.48-1m)
- security fixes

* Mon Oct 27 2003 zunda <zunda at freeshell.org>
- (2.0.47-5m)
- [Momonga-devel.ja:02161] ssl.conf seems to require to be read before
  php.conf or ruby.conf: /etc/httpd/conf.d/ssl.conf is renamed to
  /etc/httpd/conf.d/00ssl.conf. server/config.c takes care of sorting
  filenames alphanumerically in ap_process_resource_config(), right?

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.47-4m)
- change License: to Apache
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Jul 30 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.47-3m)
- add 'mod_limitipconn-0.22/apachesrc.diff'
- http://dominia.org/djao/limitipconn2.html

* Mon Jul 28 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.47-2m)
- modify certs dir

* Wed Jul  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.47-1m)
- security fixes

* Thu Jun 19 2003 Tsuromu Yasuda <tom@tom.homelinux.org>
- (2.0.46-4m)
  fix apr-config (installbuilddir)

* Sun Jun 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.46-3m)
- with installbuilddir

* Mon Jun  9 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.0.46-2m)
- error message's styles directory moved
   from "%{contentdir}/html/styles" to "%{contentdir}/errors/styles".

* Wed May 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.46-1m)
- security fixes

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.45-5m)
- export _POSIX2_VERSION=199209 in %%prep section

* Thu May  1 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.45-4m)
  separated apr

* Sat Apr 26 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.45-3m)
  added apr and apr-util to devel

* Tue Apr 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.45-2m)
  fix requires.
  fix symlink.

* Tue Apr  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.45-1m)
- minor security fixes

* Sun Mar 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.44-8m)
- specoptize

* Sun Mar 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.0.44-7m)
- add japanize error messages and error page's style.

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.44-6m)
  rebuild against openssl 0.9.7a

* Mon Feb 17 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.44-5m)
- keep permission of /usr/sbin/suexec

* Mon Feb 17 2003 smbd <smbd.homelinux.org>
- (2.0.44-4m)
- fix mistake (delete powerdby.png)

* Mon Feb 17 2003 smbd <smbd.homelinux.org>
- (2.0.44-3m)
- make png transport and delete gif icons

* Fri Feb 14 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.44-2m)
- revise %files

* Fri Feb 14 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.44-1m)
- upgrade to 2.0.44
- add configure option --with-ldap --enable-ldap --enable-auth-ldap

* Thu Jan 16 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.43-1m)
- import from rawhide and update to 2.0.43

* Fri Jan  3 2003 Joe Orton <jorton@redhat.com> 2.0.40-15
- fix possible infinite recursion in config dir processing (#77206)
- fix memory leaks in request body processing (#79282)

* Thu Dec 12 2002 Joe Orton <jorton@redhat.com> 2.0.40-14
- remove unstable shmht session cache from mod_ssl
- get SSL libs from pkg-config if available (Nalin Dahyabhai)
- stop "apxs -a -i" from inserting AddModule into httpd.conf (#78676)

* Wed Nov  6 2002 Joe Orton <jorton@redhat.com> 2.0.40-13
- fix location of installbuilddir in apxs when libdir!=/usr/lib

* Wed Nov  6 2002 Joe Orton <jorton@redhat.com> 2.0.40-12
- pass libdir to configure; clean up config_vars.mk
- package instdso.sh, fixing apxs -i (#73428)
- prevent build if upstream MMN differs from mmn macro
- remove installed but unpackaged files

* Wed Oct  9 2002 Joe Orton <jorton@redhat.com> 2.0.40-11
- correct SERVER_NAME encoding in i18n error pages (thanks to Andre Malo)

* Wed Oct  9 2002 Joe Orton <jorton@redhat.com> 2.0.40-10
- fix patch for CAN-2002-0840 to also cover i18n error pages

* Wed Oct  2 2002 Joe Orton <jorton@redhat.com> 2.0.40-9
- security fixes for CAN-2002-0840 and CAN-2002-0843
- fix for possible mod_dav segfault for certain requests

* Tue Sep 24 2002 Gary Benson <gbenson@redhat.com>
- updates to the migration guide

* Wed Sep  4 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.40-8
- link httpd with libssl to avoid library loading/unloading weirdness

* Tue Sep  3 2002 Joe Orton <jorton@redhat.com> 2.0.40-7
- add LoadModule lines for proxy modules in httpd.conf (#73349)
- fix permissions of conf/ssl.*/ directories; add Makefiles for
  certificate management (#73352)

* Mon Sep  2 2002 Joe Orton <jorton@redhat.com> 2.0.40-6
- provide "httpd-mmn" to manage module ABI compatibility

* Sun Sep  1 2002 Joe Orton <jorton@redhat.com> 2.0.40-5
- fix SSL session cache (#69699)
- revert addition of LDAP support to apr-util

* Mon Aug 26 2002 Joe Orton <jorton@redhat.com> 2.0.40-4
- set SIGXFSZ disposition to "ignored" (#69520)
- make dummy connections to the first listener in config (#72692)

* Mon Aug 26 2002 Joe Orton <jorton@redhat.com> 2.0.40-3
- allow "apachectl configtest" on a 1.3 httpd.conf
- add mod_deflate
- enable LDAP support in apr-util
- don't package everything in /var/www/error as config(noreplace)

* Wed Aug 21 2002 Bill Nottingham <notting@redhat.com> 2.0.40-2
- add trigger (#68657)

* Mon Aug 12 2002 Joe Orton <jorton@redhat.com> 2.0.40-1
- update to 2.0.40

* Wed Jul 24 2002 Joe Orton <jorton@redhat.com> 2.0.36-8
- improve comment on use of UserDir in default config (#66886)

* Wed Jul 10 2002 Joe Orton <jorton@redhat.com> 2.0.36-7
- use /sbin/nologin as shell for apache user (#68371)
- add patch from CVS to fix possible infinite loop when processing
  internal redirects

* Wed Jun 26 2002 Gary Benson <gbenson@redhat.com> 2.0.36-6
- modify init script to detect 1.3.x httpd.conf's and direct users
  to the migration guide

* Tue Jun 25 2002 Gary Benson <gbenson@redhat.com> 2.0.36-5
- patch apachectl to detect 1.3.x httpd.conf's and direct users
  to the migration guide
- ship the migration guide

* Fri Jun 21 2002 Joe Orton <jorton@redhat.com>
- move /etc/httpd2 back to /etc/httpd
- add noindex.html page and poweredby logo; tweak default config
  to load noindex.html if no default "/" page is present.
- add patch to prevent mutex errors on graceful restart

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 2.0.36-4
- automated rebuild

* Wed Jun 12 2002 Joe Orton <jorton@redhat.com> 2.0.36-3
- add patch to fix SSL mutex handling

* Wed Jun 12 2002 Joe Orton <jorton@redhat.com> 2.0.36-2
- improved config directory patch

* Mon May 20 2002 Joe Orton <jorton@redhat.com>
- initial build; based heavily on apache.spec and mod_ssl.spec
- fixes: #65214, #58490, #57376, #61265, #65518, #58177, #57245
