%global momorel 4

%define	fontname	samyak
%global fontconf	67-%{fontname}
%define langlist	"devanagari gujarati tamil malayalam oriya"

# Common description
%define common_desc \
The Samyak package contains fonts for the display of \
Scripts Devanagari, Gujarati, Malayalam, Oriya and Tamil

Name:	 %{fontname}-fonts
Version:	1.2.2
Release:	%{momorel}m%{?dist}
Summary:	Free Indian truetype/opentype fonts
Group:	User Interface/X
License:	"GPLv3+ with exceptions"
URL:	http://sarovar.org/projects/samyak/
Source:	samyak-fonts-%{version}.tar.gz
Source1: 67-samyak-devanagari.conf
Source2: 67-samyak-tamil.conf
Source3: 68-samyak-malayalam.conf
Source4: 67-samyak-gujarati.conf
Source5: 67-samyak-oriya.conf
BuildArch:	noarch
BuildRequires:	fontpackages-devel
BuildRequires:	fontforge >= 20080429
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
%common_desc

%package common
Summary:  Common files for smc-fonts
Group:  User Interface/X
Requires: fontpackages-filesystem
Provides: %{fontname}-common-fonts = %{version}-%{release}
Obsoletes: %{fontname}-common-fonts < 1.2.1-4
%description common
%common_desc

%package -n %{fontname}-devanagari-fonts
Summary: Open Type Fonts for Devanagari script
Group: User Interface/X 
Requires: %{name}-common = %{version}-%{release}
License: "GPLv3+ with exceptions"
Provides: %{name}-devanagari = %{version}-%{release}
Obsoletes: %{name}-devanagari < 1.2.1-3
%description -n %{fontname}-devanagari-fonts
This package contains truetype/opentype font for the display of \
Scripts Devanagari.

%_font_pkg -n devanagari -f %{fontconf}-devanagari.conf  %{fontname}-devanagari/Samyak-*.ttf 

%package -n %{fontname}-tamil-fonts
Summary: Open Type Fonts for Tamil script
Group: User Interface/X 
Requires: %{name}-common = %{version}-%{release}
License: "GPLv3+ with exceptions"
Provides: %{name}-tamil = %{version}-%{release}
Obsoletes: %{name}-tamil < 1.2.1-3
%description -n %{fontname}-tamil-fonts
This package contains truetype/opentype font for the display of \
Scripts Tamil.

%_font_pkg -n tamil -f %{fontconf}-tamil.conf %{fontname}-tamil/*.ttf 

%package -n %{fontname}-malayalam-fonts
Summary: Open Type Fonts for Malayalam script
Group: User Interface/X 
Requires: %{name}-common = %{version}-%{release}
License: "GPLv3+ with exceptions"
Provides: %{name}-malayalam = %{version}-%{release}
Obsoletes: %{name}-malayalam < 1.2.1-3
%description -n %{fontname}-malayalam-fonts
This package contains truetype/opentype font for the display of \
Scripts Malayalam.

%_font_pkg -n malayalam -f 68-samyak-malayalam.conf %{fontname}-malayalam/*.ttf 

%package -n %{fontname}-gujarati-fonts
Summary: Open Type Fonts for Gujarati script
Group: User Interface/X 
Requires: %{name}-common = %{version}-%{release}
License: "GPLv3+ with exceptions"
Provides: %{name}-gujarati = %{version}-%{release}
Obsoletes: %{name}-gujarati < 1.2.1-3
%description -n %{fontname}-gujarati-fonts
This package contains truetype/opentype font for the display of \
Scripts Gujarati.

%_font_pkg -n gujarati -f %{fontconf}-gujarati.conf %{fontname}-gujarati/*.ttf 

%package -n %{fontname}-oriya-fonts
Summary: Open Type Fonts for Oriya script
Group: User Interface/X 
Requires: %{name}-common = %{version}-%{release}
License: "GPLv3+ with exceptions"
Provides: %{name}-oriya = %{version}-%{release}
Obsoletes: %{name}-oriya < 1.2.1-3
%description -n %{fontname}-oriya-fonts
This package contains truetype/opentype font for the display of \
Scripts Oriya.

%_font_pkg -n oriya -f %{fontconf}-oriya.conf %{fontname}-oriya/*.ttf 


%prep
%setup -q -n samyak-fonts-%{version}

%build
mkdir -p TTFfiles/
./generate.pe */*.sfd

%install
rm -rf %{buildroot}
cp TTFfiles/Samyak-Devanagari.ttf TTFfiles/devanagari
cp TTFfiles/Samyak-Gujarati.ttf TTFfiles/gujarati
cp TTFfiles/Samyak-Malayalam.ttf TTFfiles/malayalam
cp TTFfiles/Samyak-Oriya.ttf TTFfiles/oriya
cp TTFfiles/Samyak-Tamil.ttf TTFfiles/tamil

for i in "%{langlist}"
do
install -m 0755 -d %{buildroot}%{_fontdir}/samyak-$i
install -m 0644 -p $i/*.ttf %{buildroot}%{_fontdir}/samyak-$i
done

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

# Repeat for every font family
install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-devanagari.conf

install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-tamil.conf

install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/68-samyak-malayalam.conf

install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-gujarati.conf

install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-oriya.conf


for fconf in %{fontconf}-devanagari.conf \
             %{fontconf}-tamil.conf \
             68-samyak-malayalam.conf \
             %{fontconf}-gujarati.conf \
             %{fontconf}-oriya.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -fr %{buildroot}


%files common
%defattr(-,root,root,-) 
%doc COPYING README AUTHORS
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-1m)
- sync with Fedora 13 (1.2.2-1)

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-5m)
- sync with Fedora 13 (1.2.1-11)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.1-1m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-1m)
- import from Fedora

* Thu Feb 28 2008 Pravin Satpute <psatpute@redhat.com> - 1.2.0-1
- update to samyak-fonts-1.2.0 from upstream cvs
- major bug fixes for devanagari and malayalam
- licence update to 'GNU Gplv3 or later with font exceptions'
- update spec file

* Fri Feb 08 2008 Pravin Satpute <psatpute@redhat.com> - 1.1.0-2
- added sub packaging support in spec file based on lohit-fonts.spec file 

* Fri Jan 18 2008 Pravin Satpute <psatpute@redhat.com> - 1.1.0-1
- initial packaging
