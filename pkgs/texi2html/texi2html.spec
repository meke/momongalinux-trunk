%global momorel 7

Name: texi2html
Version: 1.82
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
Summary: Open Source Texinfo to HTML Conversion Script
Source0: http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.nongnu.org/texi2html/
Requires(post): info
Requires(preun): info
Requires: perl
Requires: perl-Text-Unidecode
Requires: perl-Unicode-Normalize
Requires: texinfo
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
The basic purpose of texi2html is to convert Texinfo documents into HTML, 
and other formats.  Configuration files written in perl provide fine degree 
of control over the final output, allowing most every aspect of the final 
output not specified in the Texinfo input file to be specified.  

%prep
%setup -q

%build
./configure  --prefix=%{_prefix} \
             --exec-prefix=%{_exec_prefix} \
             --bindir=%{_bindir} \
             --sbindir=%{_sbindir} \
             --sysconfdir=%{_sysconfdir} \
             --datadir=%{_datadir} \
             --includedir=%{_includedir} \
             --libdir=%{_libdir} \
             --libexecdir=%{_libexecdir} \
             --localstatedir=%{_localstatedir} \
             --sharedstatedir=%{_sharedstatedir} \
             --mandir=%{_mandir} \
             --infodir=%{_infodir}

make clean
make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} 

rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/%{name}.info \
    %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO %{name}.init
%{_bindir}/%{name}
%{_datadir}/texinfo/html
%{_mandir}/man*/%{name}*
%{_infodir}/%{name}.info*

%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*.init
%{_datadir}/%{name}/*.texi
%dir %{_datadir}/%{name}/i18n/
%{_datadir}/%{name}/i18n/*
%dir %{_datadir}/%{name}/images/
%{_datadir}/%{name}/images/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.82-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.82-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.82-5m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.82-4m)
- Requires: perl-Text-Unidecode perl-Unicode-Normalize

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.82-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.82-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.82-1m)
- update to 1.82

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.78-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.78-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.78-2m)
- rebuild against perl-5.10.0-1m

* Sun Jan 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.78-1m)
- update to 1.78
- delete patch1 source1 (merged)
- add Reqires: texinfo (http://pc11.2ch.net/test/read.cgi/linux/1188293074/91)

* Thu Jun 15 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.76-2m)
- revised URL

* Wed Jun 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.76-1m)
- splited from tetex
- import spec from Fedora Core

* Sat Feb 25 2006 Jindrich Novy <jnovy@redhat.com> 1.76-3
- PreReq info (#182888)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Mar 08 2005 Jindrich Novy <jnovy@redhat.com> 1.76-2
- replace %configure with ./configure to prevent definition of
  target, build and host for noarch package

* Fri Feb 18 2005 Jindrich Novy <jnovy@redhat.com> 1.76-1
- we have separate texi2html package now (#121889)
- fix Source0
- BuildArchitectures -> BuildArch
- create backups for patches

* Thu Feb 10 2005 MATSUURA Takanori <t-matsuu@estyle.ne.jp> - 1.76-0
- updated to 1.76

* Mon Jan 10 2005 MATSUURA Takanori <t-matsuu@estyle.ne.jp> - 1.72-1.fc3
- initial build for Fedora Core 3 based on spec file in source tarball

* Mon Mar 23 2004 Patrice Dumas <pertusus@free.fr> 0:1.69-0.fdr.1
- Initial build.
