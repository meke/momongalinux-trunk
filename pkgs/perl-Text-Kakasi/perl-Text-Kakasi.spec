%global ver	2.04
%global momorel 28

Summary: kakasi library module for perl
Name: perl-Text-Kakasi
Version: %ver
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
Source0: http://www.cpan.org/modules/by-module/Text/Text-Kakasi-%{ver}.tar.gz
NoSource: 0
URL: http://www.cpan.org/modules/by-module/Text/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kakasi >= 2.3.1
BuildRequires: perl >= 5.8.5
BuildRequires: kakasi-devel >= 2.3.1
BuildRequires: kakasi-dict

%description
This module provides libkakasi interface for perl. libkakasi is a part
of KAKASI.
KAKASI is the language processing filter to convert Kanji characters
to Hiragana, Katakana or Romaji and may be helpful to read Japanese
documents.
More information about KAKASI is available at <http://kakasi.namazu.org/>.

%prep
%setup -n Text-Kakasi-%{ver} -q

%build
CFLAGS="%optflags" perl Makefile.PL INSTALLDIRS=vendor
make


%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm %{buildroot}%{perl_vendorarch}/auto/Text/Kakasi/.packlist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Change* COPYING README*
%{perl_vendorarch}/Text/Kakasi*
%{perl_vendorarch}/auto/Text/Kakasi
%{_mandir}/man3/Text::Kakasi*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-28m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-27m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-26m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-25m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-24m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-23m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-22m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-21m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-20m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-19m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-18m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-16m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-15m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.04-14m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-13m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-12m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-10m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.04-8m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.04-7m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.04-6m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@monoga-linux.org>
- (2.04-5m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.04-4m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.04-3m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (2.04-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.04-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.05-8m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.05-7m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.05-6m)
- add %%{momorel}

* Fri Nov 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.05-5m)
- rebuild against perl-5.8.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.05-4k)
- rebuild against for perl-5.6.1

* Mon Jul  2 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.05-2k)
- add documents

* Fri May 04 2001 Toru Hoshina <toru@df-usa.com>
- (1.04-8k)
- rebuild by gcc 2.95.3 :-P

* Wed Nov 29 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- use perl_archlib & perl_sitearch macro

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.600.

* Tue Apr 11 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- version 1.04.

* Fri Mar 11 2000 Motonobu Ichimura <famao@kondara.org>
- fix .packlist problem

* Thu Dec 30 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Kondara Adaptations.

* Fri Dec 03 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- version 1.01.

* Wed Dec 01 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- First build.
