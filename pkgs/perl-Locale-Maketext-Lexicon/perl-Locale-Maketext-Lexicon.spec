%global         momorel 2

Name:           perl-Locale-Maketext-Lexicon
Version:        1.00
Release:        %{momorel}m%{?dist}
Summary:        Use other catalog formats in Maketext
License:        MIT
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Locale-Maketext-Lexicon/
Source0:        http://www.cpan.org/authors/id/D/DR/DRTECH/Locale-Maketext-Lexicon-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.005
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTML-Parser >= 3.56
BuildRequires:  perl-Lingua-EN-Sentence >= 0.25
BuildRequires:  perl-Locale-Maketext >= 0.01
BuildRequires:  perl-PPI >= 1.203
BuildRequires:  perl-Template-Toolkit >= 2.20
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Text-Haml
BuildRequires:  perl-YAML >= 0.66
Requires:       perl-HTML-Parser >= 3.56
Requires:       perl-Lingua-EN-Sentence >= 0.25
Requires:       perl-Locale-Maketext >= 0.01
Requires:       perl-PPI >= 1.203
Requires:       perl-Template-Toolkit >= 2.20
Requires:       perl-Text-Haml
Requires:       perl-YAML >= 0.66
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides lexicon-handling modules to read from other
localization formats, such as Gettext, Msgcat, and so on.

%prep
%setup -q -n Locale-Maketext-Lexicon-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS Changes README
%{_bindir}/xgettext.pl
%{perl_vendorlib}/Locale/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-2m)
- rebuild against perl-5.18.1

* Sat Jun 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-2m)
- rebuild against perl-5.18.0

* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-2m)
- rebuild against perl-5.16.3

* Thu Dec  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-2m)
- rebuild against perl-5.14.2

* Sat Aug 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Fri Aug 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.87-1m)
- update to 0.87

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.86-2m)
- rebuild for new GCC 4.6

* Thu Feb 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Sat Feb 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Sat Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.82-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.82-5m)
- full rebuild for mo7 release

* Sun Jun  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.82-4m)
- add BuildRequires

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-2m)
- rebuild against perl-5.12.0

* Mon Apr 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Sat Apr 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81
- Specfile re-generated by cpanspec 1.78.

* Fri Apr  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-1m)
- update to 0.78

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.77-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.77-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- update to 0.77

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.75-1m)
- update to 0.75

* Sun Oct  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.65-2m)
- rebuild against gcc43

* Sun Dec 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-1m)
- update to 0.65

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- update to 0.64

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-3m)
- fix files dup

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.62-2m)
- use vendor

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.62-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
