%global momorel 4

Name:           squirrel
Version:        2.2.4
Release:        %{momorel}m%{?dist}
Summary:        High level imperative/OO programming language
Group:          Development/Tools
License:        "zlib"
URL:            http://squirrel-lang.org/
Source0:        http://dl.sourceforge.net/%{name}/%{name}_%{version}_stable.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0:         squirrel-autotools.patch
BuildRequires:  libtool

%description
Squirrel is a high level imperative/OO programming language, designed
to be a powerful scripting tool that fits in the size, memory bandwidth,
and real-time requirements of applications like games.

%package libs
Summary:        Libraries needed to run Squirrel scripts
Group:          System Environment/Libraries

%description libs
Libraries needed to run Squirrel scripts.

%package devel
Summary:        Development files needed to use Squirrel libraries
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}

%description devel
Development files needed to use Squirrel libraries.

%prep
%setup -q -c
# fix file permissions
find . -type f -exec chmod a-x {} \;

pushd SQUIRREL2
%patch0 -p1

# fix extension for autotools
mv sq/sq.c sq/sq.cpp

# fix EOL + preserve timestamps
for f in README HISTORY COPYRIGHT
do
    sed -i.orig 's/\r//g' $f
    touch -r $f.orig $f
done

sh autogen.sh
popd

%build
pushd SQUIRREL2
%configure --disable-static
make %{?_smp_mflags}
popd

%install
rm -rf %{buildroot}
pushd SQUIRREL2
make DESTDIR=%{buildroot} INSTALL="/usr/bin/install -p" install
popd

rm %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc SQUIRREL2/{README,HISTORY,COPYRIGHT}
%{_bindir}/sq

%files libs
%defattr(-,root,root,-)
%doc SQUIRREL2/COPYRIGHT
%{_libdir}/libsqstdlib-%{version}.so
%{_libdir}/libsquirrel-%{version}.so

%files devel
%defattr(-,root,root,-)
%doc SQUIRREL2/doc/*.pdf
%{_includedir}/squirrel
%{_libdir}/libsqstdlib.so
%{_libdir}/libsquirrel.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.4-2m)
- full rebuild for mo7 release

* Sun Jul  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.4-1m)
- import from Fedora devel

* Sun Dec 27 2009 Dan Horák <dan[at]danny.cz> 2.2.4-1
- update to upstream version 2.2.4

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jul  1 2009 Dan Horák <dan[at]danny.cz> 2.2.3-1
- update to upstream version 2.2.3

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Oct 13 2008 Dan Horák <dan[at]danny.cz> 2.2.2-1
- update to upstream version 2.2.2

* Sat May 31 2008 Dan Horak <dan[at]danny.cz> 2.2.1-1
- update to upstream version 2.2.1
- update URL of Source0
- really preserve timestamps on modified files

* Sun Apr 27 2008 Dan Horak <dan[at]danny.cz> 2.2-2
- enable parallel make
- add missing %%defattr for subpackages
- preserve timestamps

* Sun Apr 13 2008 Dan Horak <dan[at]danny.cz> 2.2-1
- initial version 2.2
