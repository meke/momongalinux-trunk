%global momorel 1

%global do_mem_tests 0
%global do_perf_tests 0

Name: mdds
Version: 0.9.0
Release: %{momorel}m%{?dist}
Summary: A collection of multi-dimensional data structures and indexing algorithms

Group: Development/Libraries
License: MIT
URL: http://code.google.com/p/multidimalgorithm/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://multidimalgorithm.googlecode.com/files/%{name}_%{version}.tar.bz2
NoSource: 0

BuildRequires: boost-devel
%if %{do_mem_tests}
BuildRequires: valgrind
%endif

BuildArch: noarch


%description
A collection of multi-dimensional data structures and indexing algorithms.
 
It implements the following data structures:
* segment tree
* flat segment tree 
* rectangle set
* point quad tree
* mixed type matrix


%package devel
Group: Development/Libraries
Summary: Headers for %{name}
Requires: boost-devel

%description devel
Headers for %{name}.


%prep
%setup -q -n %{name}_%{version}
# this is only used in tests
sed -i -e '/^CPPFLAGS/s/-Wall.*-std/%{optflags} -std/' Makefile.in


%build
%configure


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_includedir}
mkdir %{buildroot}/%{_includedir}/mdds
cp -pr include/mdds/* %{buildroot}/%{_includedir}/mdds


%check
make %{?_smp_mflags}
for t in fst pqt recset st; do
    make test.$t
done
%if %{do_perf_tests}
    for t in recset st; do
        make test.$t.perf
    done
    make test.stl
%endif
%if %{do_mem_tests}
    for t in fst pqt recset st; do
        make test.$t.mem
    done
%endif


%clean
rm -rf %{buildroot}


%files devel
%defattr(-,root,root,-)
%{_includedir}/mdds
%doc AUTHORS COPYING NEWS README


%changelog
* Fri Mar 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-1m)
- update 0.9.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.5.4-2m)
- rebuild for boost 1.50.0

* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.4-1m)
- update 0.5.4

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-2m)
- rebuild for boost-1.48.0

* Thu Aug 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-1m)
- Initial Commit Momonga Linux

* Thu Jul 14 2011 David Tardon <dtardon@redhat.com> - 0.5.3-1
- new version

* Wed Mar 30 2011 David Tardon <dtardon@redhat.com> - 0.5.2-2
- install license

* Tue Mar 29 2011 David Tardon <dtardon@redhat.com> - 0.5.2-1
- new version

* Thu Mar 24 2011 David Tardon <dtardon@redhat.com> - 0.5.1-3
- Resolves: rhbz#680766 fix a crash and two other bugs

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jan 08 2011 David Tardon <dtardon@redhat.com> - 0.5.1-1
- new version

* Tue Dec 21 2010 David Tardon <dtardon@redhat.com> - 0.4.0-1
- new version

* Tue Nov 16 2010 David Tardon <dtardon@redhat.com> - 0.3.1-1
- new version

* Wed Jul 07 2010 Caolán McNamara <caolanm@redhat.com> - 0.3.0-2
- rpmlint warnings

* Wed Jun 30 2010 David Tardon <dtardon@redhat.com> - 0.3.0-1
- initial import
