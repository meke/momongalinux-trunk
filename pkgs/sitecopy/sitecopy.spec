%global momorel 1 

Name:           sitecopy
Version:        0.16.6
Release:        %{momorel}m%{?dist}
Summary:        Tool for easily maintaining remote web sites
Group:          Applications/Internet
License:        GPLv2+
URL:            http://www.manyfish.co.uk/%{name}/
Source0:        http://www.manyfish.co.uk/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         configure-%{version}.patch
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires:  gettext, neon-devel

%description
sitecopy allows you to easily maintain remote Web sites.  The program
will upload files to the server which have changed locally, and delete
files from the server which have been removed locally, to keep the
remote site synchronized with the local site, with a single
command. sitecopy will also optionally try to spot files you move
locally, and move them remotely.  FTP and WebDAV servers are
supported.

%prep
%setup -q
%patch0 -p1 -b .configure

# Forcibly prevent use of bundled neon/expat/gettext sources.
rm -r lib/neon/*.[ch] intl/*.[ch]

%build
export CFLAGS="$RPM_OPT_FLAGS -fPIE"
export LDFLAGS="-pie"
%configure --with-neon
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install docdir=%{_datadir}/doc/sitecopy-%{version} DESTDIR=$RPM_BUILD_ROOT
%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING ChangeLog INSTALL NEWS README THANKS TODO
%{_bindir}/sitecopy
%{_mandir}/man1/*
%{_mandir}/*/man1/*
%{_datadir}/sitecopy

%changelog
* Sun May 1 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.16.6-1m)
- Import from fedora

* Wed Mar 30 2011 Joe Orton <jorton@redhat.com> - 0.16.6-7
- forcibly prevent use of bundled neon/expat/gettext sources

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16.6-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jan 16 2010 Rakesh Pandit <rakesh@fedoraproject.org> 0.16.6-5
- Patched configure for time being (to work with 0.29.X neon)

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Aug  3 2008 Joe Orton <jorton@redhat.com> 0.16.6-2
- really build as PIE

* Sun Aug  3 2008 Joe Orton <jorton@redhat.com> 0.16.6-1
- update to 0.16.6
- drop OS/2 README
- use system gettext
- build as PIE
- sanitize BuildRequires

* Wed Jul 16 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.16.5-1
 - Updated to latest release 0.16.5 which fixs neon 

* Wed Jul 16 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.16.3-3
 - license issue, corrected buildrequire tag

* Thu Jul 10 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.16.3-2
 - Rebuild on fedora 8 onwards, packaged with new guidelies
 - removed broken xsitecopy again
 - solved encoding warning, corrected group tag

* Sun Mar 12 2006 Dag Wieers <dag@wieers.com> 0.16.3-1
 - Updated to release 0.16.3 and older versions
 - mainted old package

* Sat Aug 13 2005 Joe Orton <joe@manyfish.co.uk>
 - removed xsitecopy subpackage, use find_lang, etc
 - maintained old packages
