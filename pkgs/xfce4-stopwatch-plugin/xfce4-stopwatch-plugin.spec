%global momorel 5

%global xfce4ver 4.8.0
%global major 0.2

Name:		xfce4-stopwatch-plugin
Version:	0.2.0
Release:	%{momorel}m%{?dist}
Summary:	Stopwatch plugin for the Xfce panel

Group:		User Interface/Desktops
License:	BSD
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, intltool, libxml2-devel
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
The xfce-stopwatch-plugin is a panel plugin to keep track of elapsed time. 
There are no ways to configure the plugin at this time, just add it to the 
panel. The time elapsed will be saved when your panel quits and restored next 
time it’s running. If time was ticking, it will not start ticking again 
automatically.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'


%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files 
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*


%changelog
* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-1m)
- import frome Fedora to Momonga

* Sat Aug 01 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-2
- Fix Source0 URL
- Fix two typos in spec file

* Tue Jul 28 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Initial Fedora package.
