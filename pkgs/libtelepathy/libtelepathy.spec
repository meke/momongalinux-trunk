%global momorel 10

Summary: framework for many different kinds of real-time communications
Name: libtelepathy
Version: 0.3.3
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://telepathy.freedesktop.org/wiki/
Source0: http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: telepathy-glib-devel >= 0.7.15
BuildRequires: dbus-glib-devel >= 0.76
BuildRequires: dbus-devel >= 1.2.3
BuildRequires: glib2-devel >= 2.18.1

%description
framework for many different kinds of real-time communications

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: telepathy-glib-devel
Requires: dbus-glib-devel
Requires: dbus-devel
Requires: glib2-devel

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure LIBS="-lglib-2.0 -ltelepathy-glib"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog NEWS
%{_libdir}/libtelepathy.so.*
%exclude %{_libdir}/libtelepathy.la

%files devel
%defattr(-, root, root)
%{_libdir}/libtelepathy.a
%{_libdir}/libtelepathy.so
%{_libdir}/pkgconfig/libtelepathy.pc
%{_includedir}/telepathy-1.0/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-10m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-7m)
- full rebuild for mo7 release

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-6m)
- build fix gcc-4.4.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-3m)
- rebuild against rpm-4.6

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-2m)
- fix %%files

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-1m)
- initial build
