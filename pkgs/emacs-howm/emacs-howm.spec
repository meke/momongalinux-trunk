%global momorel 10
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global lispname howm
#%%global src_date 061025
#%%global rcver 1

Summary: Howm is a note-taking tool on Emacs
Name: emacs-%{lispname}
Version: 1.3.8
#Release: 0.20%{src_date}.%{momorel}m%{?dist}
#Release: 0.%{rcver}.%{momorel}m%{?dist}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://howm.sourceforge.jp/
#Source0: http://howm.sourceforge.jp/a/%{lispname}-test.tar.gz
#Source0: http://howm.sourceforge.jp/a/%{lispname}-%{version}rc%{rcver}.tar.gz
Source0: http://howm.sourceforge.jp/a/%{lispname}-%{version}.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: howm-emacs
Obsoletes: howm-xemacs

Obsoletes: elisp-howm
Provides: elisp-howm

%description
Howm is a note-taking tool on Emacs. It is similar to emacs-wiki;
you can enjoy hyperlinks and full-text search easily. It is not
similar to emacs-wiki; it can be combined with any format.

%prep
%setup -q -n %{lispname}-%{version}
#%%setup -q -n %{lispname}-test%{src_date}
#%%setup -q -n %{lispname}-%{version}rc%{rcver}

%build
./configure --prefix=%{_prefix} \
  --with-emacs=%{_bindir}/emacs \
  --datadir=%{_datadir} \
  --mandir=%{_mandir}

make EMACS=emacs

%install
rm -rf %{buildroot}

make EMACS=emacs \
  prefix=%{buildroot}%{_prefix} \
  datadir=%{buildroot}%{_datadir} \
  lispdir=%{buildroot}%{e_sitedir} \
  docdir=%{buildroot}%{_docdir}/%{name}-%{version} \
  install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%doc doc/*.rd doc/*.png doc/*.html
%dir %{_datadir}/%{lispname}/ext
%{_datadir}/%{lispname}/ext/*
#%%dir %{_datadir}/%{lispname}/ja
#%%{_datadir}/%{lispname}/ja/0000-00-00-000000.howm
#%%dir %{_datadir}/%{lispname}/en
#%%{_datadir}/%{lispname}/en/0000-00-00-000000.howm
%dir %{e_sitedir}/%{lispname}
%{e_sitedir}/%{lispname}/*.el
%{e_sitedir}/%{lispname}/*.elc

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-10m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-9m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.8-8m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-7m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.8-6m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.8-4m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.8-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.8-2m)
- merge howm-emacs to elisp-howm
- kill howm-xemacs

* Sat Jan  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.7-9m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.7-8m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-7m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-6m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-5m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-4m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7
- License: GPLv2+

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-3m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-2m)
- rebuild against xemacs-21.5.28

* Wed Jun  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-1m)
- update to 1.3.6

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.5-2m)
- rebuild against gcc43

* Mon Dec 10 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5

* Thu Nov 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-0.1.1m)
- update to 1.3.5rc1
- License: GPLv2

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-8m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-7m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-6m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-5m)
- rebuild against emacs-22.0.95

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-4m)
- rebuild against emacs-22.0.94

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-4m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-3m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-2m)
- rebuild against emacs-22.0.92

* Fri Dec 22 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-0.20061025.4m)
- rebuild against emacs-22.0.91

* Sun Nov 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3-0.20061025.3m)
- update to latest test package (20061025)
- - now, "coloring" works with the latest emacs again

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-0.20061003.2m)
- rebuild against emacs-22.0.90

* Fri Oct  6 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-0.20061003.1m)
- update to test061003

* Fri Sep 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.2-0.20060912-1m)
- revised Version, Release num

* Thu Sep 14 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20060912-1m)
- update to test060912

* Thu May 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-2m)
- update to daily test tarball for build
  change lispdir
  from
   lispdir=%{buildroot}%{sitepdir}/%{lispname}
  to
   lispdir=%{buildroot}%{e_sitedir} 

* Tue Dec 20 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2-4m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2-2m)
- rebuild against emacs-21.3.50

* Sat Oct 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2-1m)
- initial import for momonga
