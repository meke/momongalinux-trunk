%global momorel 2

Summary: Mail filter and delivery agent
Name: maildrop
Version: 2.5.5
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Internet
URL: http://www.courier-mta.org/
Source: http://dl.sourceforge.net/sourceforge/courier/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: courier-authlib
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: courier-authlib-devel >= 0.62.1
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: libtool
BuildRequires: pcre-devel >= 8.31
Provides: maildrop-man
Obsoletes: maildrop-man

%package devel
Summary: development tools for handling E-mail messages
Group: Applications/Internet

%description
Maildrop is a combination mail filter/mail delivery agent.
Maildrop reads the message to be delivered to your mailbox,
optionally reads instructions from a file how filter incoming
mail, then based on these instructions may deliver mail to an
alternate mailbox, or forward it, instead of dropping the
message into your mailbox.

%description devel
The maildrop-devel package contains the libraries and header files
that can be useful in developing software that works with or processes
E-mail messages.

Install the maildrop-devel package if you want to develop applications
which use or process E-mail messages.

%prep
%setup -q

%build
%configure \
 --disable-dependency-tracking \
 --with-devel \
 --enable-userdb \
 --with-db=db \
 --enable-maildirquota \
 --enable-syslog=1 \
 --enable-trusted-users='root mail daemon postmaster qmaild mmdf nobody postfix postdrop' \
 --enable-restrict-trusted=0 \
 --enable-sendmail=/usr/sbin/sendmail \
 --with-default-maildrop=Maildir/

# make -j8 and make -j16 fail (maildrop-2.1.0-1m)
make

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot} MAILDROPUID='' MAILDROPGID=''

mkdir htmldoc
cp %{buildroot}%{_datadir}/maildrop/html/* htmldoc
rm -rf %{buildroot}%{_datadir}/maildrop/html
rm -f %{buildroot}/%{_libdir}/*.la

# give a higher priority to courier-imap than maildrop
rm -f %{buildroot}/%{_mandir}/man8/deliverquota.8
rm -f %{buildroot}/%{_mandir}/man1/maildirmake.1

%clean
rm -rf %{buildroot}

%files
%defattr(-, bin, bin)
%{_datadir}/maildrop

%doc htmldoc/*

%attr(755, root, mail) %{_bindir}/maildrop
%attr(755, root, mail) %{_bindir}/lockmail
%{_bindir}/makedat
%{_bindir}/makedatprog
%{_bindir}/mailbot
%{_bindir}/maildirmake
%{_bindir}/deliverquota
%{_bindir}/reformail
%{_bindir}/makemime
%{_bindir}/reformime
%{_mandir}/man[1578]/*

%doc maildir/README.maildirquota.html maildir/README.maildirquota.txt
%doc COPYING README README.postfix INSTALL NEWS UPGRADE ChangeLog maildroptips.txt

%files devel
%defattr(-, bin, bin)
%{_mandir}/man3/*
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/librfc2045.so
%{_libdir}/librfc2045.so.0
%{_libdir}/librfc2045.so.0.0.0
%{_libdir}/librfc822.so
%{_libdir}/librfc822.so.0
%{_libdir}/librfc822.so.0.0.0

%changelog
* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.5-2m)
- rebuild against pcre-8.31

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5
- rebuild against libdb-5.3.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.0-5m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.0-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Tue May 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Mon Mar  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.0-2m)
- rebuild against db-4.8.26

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- [SECURITY] CVE-2010-0301
- [SECURITY] patch from upstrem, http://courier.cvs.sourceforge.net/viewvc/courier/courier/maildrop/maildrop/main.C?r1=1.58&r2=1.59
- update to 2.3.0

* Sun Dec 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.0-3m)
- add autoreconf 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-3m)
- fix build

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-2m)
- disable parallel build

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0
- run autoreconf for libtool22
- License: GPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-4m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-3m)
- rebuild against courier-authlib-0.62.1
- License: GPLv2+

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.4-2m)
- rebuild against db4-4.7.25-1m

* Wed Jun 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-6m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-5m)
- rebuild against db4-4.6.21

* Sat Feb 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-4m)
- revise %%install

* Sat Feb 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.2-3m)
- delete deliverquota.8.bz2 maildirmake.1.bz2 for courier-imap 

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-2m)
- rebuild against db4-4.5.20

* Sun Jun 25 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-1m)
- initial import for momonga

* Thu Feb 23 2006 Michael Fleming <mfleming+rpm@enlartenment.com>
- 2.0.2

* Sat Oct 1 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- 2.0.1

* Sat Sep 17 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- 2.0
- Added PCRE support.

* Fri Jul 8 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- Merged -man into main package
- Don't strip the binaries (useless for debug otherwise)
- Be more explicit about which files get pulled into -devel (so the debug stuff
  doesn't get pulled in by accident)

* Thu Jul 7 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- URL and Group fixed.
- Summary fixed up too.
- Removed obvious crack from spec.

* Thu Jun 16 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- Rebuilt

* Mon May 16 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- 1.8.1

* Mon Jan 3 2005 Michael Fleming <mfleming+rpm@enlartenment.com>
- 1.8.0
- userdb tools are now in courier-authlib-userdb if you need them.

* Wed Dec 15 2004 Michael Fleming <mfleming+rpm@enlartenment.com>
- Built with spec tweaks (based on original packages)
- Added a changelog
