%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global momorel 1


Name:		Cython
Version:	0.17.1
Release:	%{momorel}m%{?dist}
Summary:	A language for writing Python extension modules

Group:		Development/Tools
License:	Apache
URL:		http://www.cython.org
Source:		http://www.cython.org/release/Cython-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	python-devel >= 2.7 python-setuptools
Requires:	python

%description
This is a development version of Pyrex, a language
for writing Python extension modules.

For more info, see:

    Doc/About.html for a description of the language
    INSTALL.txt	   for installation instructions
    USAGE.txt	   for usage instructions
    Demos	   for usage examples


%prep
%setup -q


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc *.txt Demos Doc Tools
%{_bindir}/cython
%{_bindir}/cygdb
%{python_sitearch}/Cython
%{python_sitearch}/cython.py*
%{python_sitearch}/pyximport
%{python_sitearch}/Cython*egg-info


%changelog
* Sat Sep 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17.1-1m)
- update to 0.17.1

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.1-1m)
- update 0.15.1

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15-1m)
- update 0.15

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.1-1m)
- update 0.14.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.1-2m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.1-1m)
- Initial Commit Momonga Linux

