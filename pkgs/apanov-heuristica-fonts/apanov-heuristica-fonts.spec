%global momorel 6

%global fontname apanov-heuristica
%global fontconf 61-%{fontname}.conf

%global archivename heuristica-src-%{version}
%global googlename  evristika

Name:    %{fontname}-fonts
Epoch:   1
Version: 0.2.2
Release: %{momorel}m%{?dist}
Summary: Heuristica font

Group:     User Interface/X
License:   OFL
URL:       http://code.google.com/p/%{googlename}/
Source0:   http://%{googlename}.googlecode.com/files/%{archivename}.tar.xz
NoSource:  0
Source1:   %{name}-fontconfig.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch
BuildRequires: fontforge, fonttools >= 2.2-3m, xgridfit >= 1.19-1m, xmlstarlet
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem

%description
Heuristica is a serif latin & cyrillic font, derived from the "Adobe Utopia"
font that was released to the TeX Users Group under a liberal license.


%prep
%setup -q -c

for txt in *.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done


%build

make %{?_smp_mflags}

rm *\.gen\.ttf

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *\.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc *.txt


%changelog
* Fri Apr 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.2.2-6m)
- version 0.2.2
- remove bolditalic.patch
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.2-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.2.0-2m)
- add epoch to %%changelog

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.2.0-1m)
- sync with Fedora 13 (1:0.2-5)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090125-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090125-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20090125-1m)
- import from Fedora

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 20090125-5
- Make sure F11 font packages have been built with F11 fontforge

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090125-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 22 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20090125-3
- fix url (thanks nirik)

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20090125-2
- prepare for F11 mass rebuild, new rpm and new fontpackages

*Fri Jan 30 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20090125-1

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081109-3
- rpm-fonts renamed to fontpackages

* Sun Nov 16 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081109-2
- Rebuild using new rpm-fonts
- 20081109-1
- Fedora import

* Fri Oct 10 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080825-1
- FE-LEGAL lifted - update package in review request

* Fri Jun 20 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080619-1
- Initial packaging
