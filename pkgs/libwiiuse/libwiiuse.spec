%global momorel 7

Name:           libwiiuse
Version:        0.12
Release:        %{momorel}m%{?dist}
Summary:        RWiiuse is a library written in C that connects with several Nintendo Wii remotes

Group:          System Environment/Libraries
License:        GPLv3+ or LGPLv3+
URL:            http://www.wiiuse.net/
Source0:        wiiuse_feesh2.tar.bz2
Patch0:		wiiuse_v0.12-install.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf
BuildRequires:  bluez-libs-devel

%description
Wiiuse is a library written in C that connects with several
Nintendo Wii remotes. Supports motion sensing, IR tracking, 
nunchuk, classic controller, and the Guitar Hero 3 controller. 

Single threaded and nonblocking makes a light weight and clean API.

%prep
%setup -q -n wiiuse_v%{version}

%patch0 -p1 -b .install

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_includedir}
mkdir -p $RPM_BUILD_ROOT/%{_libdir}

pushd src
make install DESTDIR=$RPM_BUILD_ROOT LIBDIR=%{_libdir}
find $RPM_BUILD_ROOT -name '*.la' | xargs rm -f
# boo static libraries.  hooray beer!
popd

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README CHANGELOG LICENSE LICENSE_noncommercial
%{_includedir}/*
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-2m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv3+ or LGPLv3+

* Mon Dec 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12-1m)
- initial commit Momonga Linux

