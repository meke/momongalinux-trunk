%global momorel 3


%define with_python %{?_without_python: 0} %{?!_without_python: 1}
%define with_php %{?_without_php: 0} %{?!_without_php: 1}
%define with_tcl %{?_without_tcl: 0} %{?!_without_tcl: 1}
%define with_ruby %{?_without_ruby: 0} %{?!_without_ruby: 1}
%define with_lua %{?_without_lua: 0} %{?!_without_lua: 1}
%define php_extdir %(php-config --extension-dir 2>/dev/null || echo %{_libdir}/php4)
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")
%define svnrev r1190
#define pretag 1.2.99908020600

Summary: Round Robin Database Tool to store and display time-series data
Name: rrdtool
Version: 1.4.8
Release: %{momorel}m%{?dist}
License: "GPLv2+ with exceptions"
Group: Applications/Databases
URL: http://oss.oetiker.ch/rrdtool/
Source0: http://oss.oetiker.ch/%{name}/pub/%{name}-%{version}.tar.gz
NoSource: 0
Source1: php4-%{svnrev}.tar.gz
Patch1: %{name}-1.4.4-php54.patch
# disable logo for php 5.5.
Patch3: %{name}-1.4.7-php55.patch
Patch4: %{name}-1.4.7-autoconf-fix.patch
# patch merged upstream, http://github.com/oetiker/rrdtool-1.x/pull/397
Patch6: %{name}-%{version}-imginfo-check.patch
# patch sent upstream
Patch7: %{name}-%{version}-doc-fix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dejavu-lgc-sans-fonts
Requires: dejavu-lgc-sans-mono-fonts
Requires: dejavu-lgc-serif-fonts
BuildRequires: gcc-c++, openssl-devel, freetype-devel
BuildRequires: libpng-devel, zlib-devel, cairo-devel, pango-devel
BuildRequires: libtool, groff
BuildRequires: gettext, libxml2-devel
BuildRequires: perl-ExtUtils-MakeMaker perl-devel
BuildRequires: perl-Pod-Html

%description
RRD is the Acronym for Round Robin Database. RRD is a system to store and
display time-series data (i.e. network bandwidth, machine-room temperature,
server load average). It stores the data in a very compact way that will not
expand over time, and it presents useful graphs by processing the data to
enforce a certain data density. It can be used either via simple wrapper
scripts (from shell or Perl) or via frontends that poll network devices and
put a friendly user interface on it.

%package devel
Summary: RRDtool libraries and header files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
RRD is the Acronym for Round Robin Database. RRD is a system to store and
display time-series data (i.e. network bandwidth, machine-room temperature,
server load average). This package allow you to use directly this library.

%package doc
Summary: RRDtool documentation
Group: Documentation

%description doc
RRD is the Acronym for Round Robin Database. RRD is a system to store and
display time-series data (i.e. network bandwidth, machine-room temperature,
server load average). This package contains documentation on using RRD.

%package perl
Summary: Perl RRDtool bindings
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes: perl-%{name} < %{version}-%{release}
Provides: perl-%{name} = %{version}-%{release}

%description perl
The Perl RRDtool bindings

%if %{with_python}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c 'from distutils import sysconfig; print sysconfig.get_python_lib(1)')}
# eval to 2.3 if python isn't yet present, workaround for no python in fc4 minimal buildroot
%{!?python_version: %define python_version %(%{__python} -c 'import sys; print sys.version.split(" ")[0]' || echo "2.3")}

%package python
Summary: Python RRDtool bindings
Group: Development/Languages
BuildRequires: python-devel >= 2.7
Requires: python >= %{python_version}
Requires: %{name} = %{version}-%{release}
Obsoletes: python-%{name} < %{version}-%{release}
Provides: python-%{name} = %{version}-%{release}

%description python
Python RRDtool bindings.
%endif

%ifarch ppc64
# php bits busted on ppc64 at the moment
%define with_php 0
%endif

%if %{with_php}
%package php
Summary: PHP RRDtool bindings
Group: Development/Languages
BuildRequires: php-devel >= 5.5.2
Requires: php >= 5.5.2
Requires: %{name} = %{version}-%{release}
%if 0%{?php_zend_api}
Requires: php(zend-abi) = %{php_zend_api}
Requires: php(api) = %{php_core_api}
%else
Requires: php-api = %{php_apiver}
%endif
Obsoletes: php-%{name} < %{version}-%{release}
Provides: php-%{name} = %{version}-%{release}
Provides: php-pecl(rrdtool)

%description php
The %{name}-php package includes a dynamic shared object (DSO) that adds
RRDtool bindings to the PHP HTML-embedded scripting language.
%endif

%if %{with_tcl}
%package tcl
Summary: Tcl RRDtool bindings
Group: Development/Languages
BuildRequires: tcl-devel >= 8.0
Requires: tcl >= 8.0
Requires: %{name} = %{version}-%{release}
Obsoletes: tcl-%{name} < %{version}-%{release}
Provides: tcl-%{name} = %{version}-%{release}

%description tcl
The %{name}-tcl package includes RRDtool bindings for Tcl.
%endif

%if %{with_ruby}
%{!?ruby_archdir: %define ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')}

%package ruby
Summary: Ruby RRDtool bindings
Group: Development/Languages
BuildRequires: ruby, ruby-devel
Requires: ruby(abi) = 1.9.1
Requires: %{name} = %{version}-%{release}

%description ruby
The %{name}-ruby package includes RRDtool bindings for Ruby.
%endif

%if %{with_lua}
%define luaver 5.1
%define lualibdir %{_libdir}/lua/%{luaver}
%define luapkgdir %{_datadir}/lua/%{luaver}

%package lua
Summary: Lua RRDtool bindings
Group: Development/Languages
BuildRequires: lua, lua-devel
Requires: lua = %{luaver}
Requires: %{name} = %{version}-%{release}

%description lua
The %{name}-lua package includes RRDtool bindings for Lua.
%endif

%prep
%setup -q -n %{name}-%{version} %{?with_php: -a 1}
%if %{with_php}
%patch1 -p1 -b .php54
%patch3 -p1 -b .php55
%endif
%patch4 -p1 -b .autoconf-fix
%patch6 -p1 -b .imginfo-check
%patch7 -p1 -b .doc-fix

autoreconf -vif

# Fix to find correct python dir on lib64
%{__perl} -pi -e 's|get_python_lib\(0,0,prefix|get_python_lib\(1,0,prefix|g' \
    configure

# Most edits shouldn't be necessary when using --libdir, but
# w/o, some introduce hardcoded rpaths where they shouldn't
%{__perl} -pi.orig -e 's|/lib\b|/%{_lib}|g' \
    configure Makefile.in php4/configure php4/ltconfig*

# Perl 5.10 seems to not like long version strings, hack around it
%{__perl} -pi.orig -e 's|1.299907080300|1.29990708|' \
    bindings/perl-shared/RRDs.pm bindings/perl-piped/RRDp.pm

#
# fix config files for php4 bindings
# workaround needed due to https://bugzilla.redhat.com/show_bug.cgi?id=211069
cp -p /usr/lib/rpm/config.{guess,sub} php4/

%build
%configure \
    --with-perl-options='INSTALLDIRS="vendor"' \
    --disable-rpath \
%if %{with_tcl}
    --enable-tcl-site \
    --with-tcllib=%{_libdir} \
%else
    --disable-tcl \
%endif
%if %{with_python}
    --enable-python \
%else
    --disable-python \
%endif
%if %{with_ruby}
    --enable-ruby \
    --enable-ruby-site-install \
%endif
    --disable-static \
    --with-pic

# Fix another rpath issue
%{__perl} -pi.orig -e 's|-Wl,--rpath -Wl,\$rp||g' \
    bindings/perl-shared/Makefile.PL

%if %{with_ruby}
# Remove Rpath from Ruby
%{__perl} -pi.orig -e 's|-Wl,--rpath -Wl,\$\(EPREFIX\)/lib||g' \
    bindings/ruby/extconf.rb
%endif

# Force RRDp bits where we want 'em, not sure yet why the
# --with-perl-options and --libdir don't take
pushd bindings/perl-piped/
%{__perl} Makefile.PL INSTALLDIRS=vendor
%{__perl} -pi.orig -e 's|/lib/perl|/%{_lib}/perl|g' Makefile
popd

#{__make} %{?_smp_mflags}
make

# Build the php module, the tmp install is required
%if %{with_php}
%define rrdtmp %{_tmppath}/%{name}-%{version}-tmpinstall
%{__make} install DESTDIR="%{rrdtmp}"
pushd php4/
%configure \
    --with-rrdtool="%{rrdtmp}%{_prefix}" \
    --disable-static
#{__make} %{?_smp_mflags}
make
popd
%{__rm} -rf %{rrdtmp}
%endif

# Fix @perl@ and @PERL@
find examples/ -type f \
    -exec %{__perl} -pi -e 's|^#! \@perl\@|#!%{__perl}|gi' {} \;
find examples/ -name "*.pl" \
    -exec %{__perl} -pi -e 's|\015||gi' {} \;

%install
rm -rf %{buildroot}
make DESTDIR="%{buildroot}" install transform='s,x,x,'

# Install the php module
%if %{with_php}
%{__install} -D -m0755 php4/modules/rrdtool.so \
    %{buildroot}%{php_extdir}/rrdtool.so
# Clean up the examples for inclusion as docs
%{__rm} -rf php4/examples/.svn
# Put the php config bit into place
%{__mkdir_p} %{buildroot}%{_sysconfdir}/php.d
%{__cat} << __EOF__ > %{buildroot}%{_sysconfdir}/php.d/rrdtool.ini
; Enable rrdtool extension module
extension=rrdtool.so
__EOF__
%endif

# Pesky RRDp.pm...
%{__mv} %{buildroot}%{perl_vendorarch}/../RRDp.pm %{buildroot}%{perl_vendorarch}/

# Dunno why this is getting installed here...
%{__rm} -f %{buildroot}%{perl_vendorarch}/../leaktest.pl

# We only want .txt and .html files for the main documentation
%{__mkdir_p} doc2/html doc2/txt
%{__cp} -a doc/*.txt doc2/txt/
%{__cp} -a doc/*.html doc2/html/

# Put perl docs in perl package
%{__mkdir_p} doc3/html
%{__mv} doc2/html/RRD*.html doc3/html/

# Clean up the examples
%{__rm} -f examples/Makefile* examples/*.in

# This is so rpm doesn't pick up perl module dependencies automatically
find examples/ -type f -exec chmod 0644 {} \;

# Clean up the buildroot
%{__rm} -rf %{buildroot}%{_docdir}/%{name}-* \
	%{buildroot}%{perl_vendorarch}/ntmake.pl \
	%{buildroot}%{perl_archlib}/perllocal.pod \
        %{buildroot}%{_datadir}/%{name}/examples \
        %{buildroot}%{perl_vendorarch}/auto/*/{.packlist,*.bs}

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_libdir}/*.so.*
%{_datadir}/%{name}
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%exclude %{_libdir}/*.la
%{_libdir}/*.so
%exclude %{_libdir}/tclrrd*.so
%{_libdir}/pkgconfig/librrd.pc

%files doc
%defattr(-,root,root,-)
%doc CONTRIBUTORS COPYING COPYRIGHT README TODO NEWS THREADS
%doc examples doc2/html doc2/txt

%files perl
%defattr(-,root,root,-)
%doc doc3/html
%{_mandir}/man3/*
%{perl_vendorarch}/*.pm
%attr(0755,root,root) %{perl_vendorarch}/auto/RRDs/

%if %{with_python}
%files python
%defattr(-,root,root,-)
%doc bindings/python/AUTHORS bindings/python/COPYING bindings/python/README
%{python_sitearch}/rrdtoolmodule.so
%{python_sitearch}/py_rrdtool-*.egg-info
%endif

%if %{with_php}
%files php
%defattr(-,root,root,0755)
%doc php4/examples php4/README
%config(noreplace) %{_sysconfdir}/php.d/rrdtool.ini
%{php_extdir}/rrdtool.so
%endif

%if %{with_tcl}
%files tcl
%defattr(-,root,root,-)
%doc bindings/tcl/README
%{_libdir}/tclrrd*.so
%{_libdir}/rrdtool/*.tcl
%endif

%if %{with_ruby}
%files ruby
%defattr(-,root,root,-)
%doc bindings/ruby/README
%{ruby_sitearch}/RRD.so
%endif

%if %{with_lua}
%files lua
%defattr(-,root,root,-)
%doc bindings/lua/README
%{lualibdir}/*
%exclude %{lualibdir}/*.la
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.8-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.8-2m)
- rebuild against perl-5.18.2

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8
- rebuild against php-5.5.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-2m)
- rebuild against perl-5.16.0

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-1m)
- update to 1.4.7
- rebuild against php-5.4.1

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-5m)
- add BuildRequires

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.5-4m)
- use RbConfig

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-2m)
- rebuild against perl-5.14.1

* Wed Jun  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-16m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.8-15m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.8-11m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.8-10m)
- Requires: ruby(abi)-1.9.1

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.8-9m)
- rebuild agaist ruby-1.9.2

* Tue Jun 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-8m)
- change Requires for new dejavu-fonts

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.8-7m)
- rebuild against php-5.3.2

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-6m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-5m)
- rebuild against perl-5.12.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-2m)
- rebuild against perl-5.10.1

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Wed May  6 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.6-2m)
- change Requires: from dejavu-lgc-fonts to dejavu-fonts-lgc-compat

* Mon Feb  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-1m)
- update to 1.3.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-2m)
- rebuild against python-2.6.1

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m)
- update 1.3.3

* Thu Sep 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-3m)
- correct filenames

* Fri Jul 25 2008 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (1.3.0-2m)
- revise %%files devel to avoid conflicting with package tcl

* Fri Jul 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-1m)
- sync Fedora

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.27-4m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.27-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.27-2m)
- rebuild against gcc43

* Fri Feb 22 2008 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (1.2.27-1m)
- up to 1.2.27
- ruby-rrd was added to main source
  remove ruby-rrd-1.1.tar.gz and ruby-rrd-1.1-rrdtool-1.2.patch

* Thu Feb 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.15-11m)
- use perl_vendorarch

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.15-10m)
- rebuild against perl-5.10.0

* Thu Nov  8 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.15-9m)
- no NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.15-8m)
- rebuild against ruby-1.8.6-4m

* Tue May  1 2007 zunda <zunda at freeshell.org>
- (1.2.15-7m)
- Updated URLs for the package, Source0, and Source2

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.15-6m)
- rebuild against php-5.2.1

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.15-5m)
- BuildRequires: freetype2-devel -> freetype-devel

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.15-4m)
- rebuild against python-2.5

* Wed Aug  9 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.15-3m)
- build fix for old env.

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.15-2m)
- merge rrdtool-ruby

* Mon Jul 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.15-1m)
- up to 1.2.15

* Wed Mar  8 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.49-1m)
- up to 1.0.49
- change place for config file of php-rrdtool

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.48-10m)
- rebuild against perl-5.8.8

* Sun Jul  3 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.48-9m)
- fixed build error gcc4. add rrdtool-1.0.49-fc4.patch

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.48-8m)
- rebuilt against perl-5.8.7

* Tue Apr  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.48-7m)
- rebuild against php-5.0.4

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.48-6m)
- phpextdir %%{_libdir}/php4 -> %%{_libdir}/php5
- enable x86_64.

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.48-5m)
- build against perl-5.8.5

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.48-4m)
- add php-rrdtool

* Wed Jul 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.48-3m)
- noexec autoconf-1.7
- add transform='s,x,x,'

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0.48-2m)
- remove Epoch from BuildPrereq

* Sat Jun 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.48-1m)
- verup

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.40-4m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.40-3m)
- rebuild against perl-5.8.2

* Sun Nov  9 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.40-2m)
- rebuild against perl-5.8.1

* Mon Nov 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.40-1m)
- rebuild against perl-5.8.0

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.38-5m)
- add Provides: /usr/bin/rrdcgi

* Thu Aug  8 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.38-4m)
- move site_perl directory [Momonga-devel.ja:00311] thanks Kyoichi Ozaki

* Wed Jul 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.38-3m)
- remove AutoReq: 0
- rrdtool-devel: Requires: %{name} = %{version}-%{release}
- rrdtool: Obsoletes: rrdtool-tcl

* Wed Jul 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.38-2m)
- use 'automake-old' instead of 'automake'

* Tue Jul 30 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.38-1m)
- version up 1.0.38
- modified specfile

* Sun Apr 28 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.0.37-2k)
- chobitto syuusei

* Sun Apr 28 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.0.37-1k)
- up to 1.0.37
- irewasure
- brush up spec

* Tue Mar 12 2002 Masahiro Takahata <takahata@kondara.org>
- (1.0.34-2k)
- upgrade to 1.0.34-2k
- replaced zlib 1.1.3 with 1.1.4

* Mon Nov 19 2001 Masahiro Takahata <takahata@kondara.org>
- (1.0.33-2k)
- the first release
