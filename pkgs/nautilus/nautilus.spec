%global momorel 1
%define glib2_version 2.31.9
%define gnome_desktop3_version 3.6.0
%define pango_version 1.28.3
%define gtk3_version 3.6.0
%define libxml2_version 2.7.8
%define libexif_version 0.6.20
%define exempi_version 2.1.0
%define gobject_introspection_version 0.9.5

Name:           nautilus
Summary:        File manager for GNOME
Version:        3.6.3
Release: %{momorel}m%{?dist}
License:        GPLv2+
Group:          User Interface/Desktops
Source:         http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

URL:            http://projects.gnome.org/nautilus/
# !!FIXME!!
#Requires:       redhat-menus
Requires:       gvfs
Requires:       gnome-icon-theme
Requires:       libexif >= %{libexif_version}
Requires:       gsettings-desktop-schemas >= 3.6.0

BuildRequires:  glib2-devel >= %{glib2_version}
BuildRequires:  pango-devel >= %{pango_version}
BuildRequires:  gtk3-devel >= %{gtk3_version}
BuildRequires:  libxml2-devel >= %{libxml2_version}
BuildRequires:  gnome-desktop3-devel >= %{gnome_desktop3_version}
BuildRequires:  intltool >= 0.40.6-2
BuildRequires:  libX11-devel
BuildRequires:  desktop-file-utils
BuildRequires:  libSM-devel
BuildRequires:  libtool
BuildRequires:  libexif-devel >= %{libexif_version}
BuildRequires:  exempi-devel >= %{exempi_version}
BuildRequires:  gettext
BuildRequires:  libselinux-devel
BuildRequires:  gobject-introspection-devel >= %{gobject_introspection_version}
BuildRequires:  gsettings-desktop-schemas-devel
BuildRequires:  libnotify-devel
BuildRequires:	tracker-devel >= 0.14.2

# the main binary links against libnautilus-extension.so
# don't depend on soname, rather on exact version
Requires:       nautilus-extensions = %{version}-%{release}

Obsoletes:      nautilus-extras
Obsoletes:      nautilus-suggested
Obsoletes:      nautilus-mozilla < 2.0
Obsoletes:      nautilus-media

Obsoletes:      gnome-volume-manager < 2.24.0-2
Provides:       gnome-volume-manager = 2.24.0-2
Obsoletes:      eel2 < 2.26.0-3
Provides:       eel2 = 2.26.0-3

# The selinux patch is here to not lose it, should go upstream and needs
# cleaning up to work with current nautilus git.
#Patch4:         nautilus-2.91.8-selinux.patch

%description
Nautilus is the file manager and graphical shell for the GNOME desktop
that makes it easy to manage your files and the rest of your system.
It allows to browse directories on local and remote filesystems, preview
files and launch applications associated with them.
It is also responsible for handling the icons on the GNOME desktop.

%package extensions
Summary: Nautilus extensions library
License: LGPLv2+
Group: Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description extensions
This package provides the libraries used by nautilus extensions.

%package devel
Summary: Support for developing nautilus extensions
License: LGPLv2+
Group: Development/Libraries
Requires:   %{name} = %{version}-%{release}
Obsoletes:      eel2-devel < 2.26.0-3
Provides:       eel2-devel = 2.26.0-3

%description devel
This package provides libraries and header files needed
for developing nautilus extensions.

%prep
%setup -q -n %{name}-%{version}

#%patch4 -p1 -b .selinux

%build
CFLAGS="$RPM_OPT_FLAGS -g -DNAUTILUS_OMIT_SELF_CHECK" %configure --disable-more-warnings --disable-update-mimedb

# drop unneeded direct library deps with --as-needed
# libtool doesn't make this easy, so we do it the hard way
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' libtool

export tagname=CC
LANG=en_US make %{?_smp_mflags} V=1

%install
export tagname=CC
LANG=en_US make install DESTDIR=$RPM_BUILD_ROOT LIBTOOL=/usr/bin/libtool

desktop-file-install --delete-original       \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications             \
  --add-only-show-in GNOME                                  \
  $RPM_BUILD_ROOT%{_datadir}/applications/*

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/nautilus/extensions-3.0/*.la

%find_lang %name

%post
/sbin/ldconfig
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null

%postun
/sbin/ldconfig

if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :

%post extensions -p /sbin/ldconfig

%postun extensions -p /sbin/ldconfig

%files  -f %{name}.lang
%doc AUTHORS COPYING COPYING-DOCS COPYING.LIB NEWS README
%{_datadir}/nautilus
%{_datadir}/applications/*
%{_datadir}/mime/packages/nautilus.xml
%{_bindir}/*
%{_datadir}/dbus-1/services/org.gnome.Nautilus.service
%{_datadir}/dbus-1/services/org.freedesktop.FileManager1.service
%{_datadir}/gnome-shell/search-providers/nautilus-search-provider.ini
%{_datadir}/dbus-1/services/org.gnome.Nautilus.SearchProvider.service
%{_mandir}/man1/nautilus-connect-server.1.bz2
%{_mandir}/man1/nautilus.1.bz2
%{_libexecdir}/nautilus-convert-metadata
%{_libexecdir}/nautilus-shell-search-provider
%{_datadir}/GConf/gsettings/nautilus.convert
%{_datadir}/glib-2.0/schemas/org.gnome.nautilus.gschema.xml
%dir %{_libdir}/nautilus/extensions-3.0
%{_libdir}/nautilus/extensions-3.0/libnautilus-sendto.so
%{_sysconfdir}/xdg/autostart/nautilus-autostart.desktop

%files extensions
%{_libdir}/libnautilus-extension.so.*
%{_libdir}/girepository-1.0/*.typelib
%dir %{_libdir}/nautilus

%files devel
%{_includedir}/nautilus
%{_libdir}/pkgconfig/*
%{_libdir}/*.so
%{_datadir}/gir-1.0/*.gir
%doc %{_datadir}/gtk-doc/html/libnautilus-extension/*

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.3-1m)
- update to 3.6.3

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Wed Oct 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-2m)
- update Requires:

* Tue Oct  9 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- revise requires
- rebuild for tracker 0.14.2

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-3m)
- fix build failure with glib 2.33+

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org>  
- (3.0.2-2m)
- revised br

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1.1-1m)
- update to 3.0.1.1
-- fix some crash

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.2.1-2m)
- rebuild for new GCC 4.6

* Wed Dec 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2.1-1m)
- update to 2.32.2.1

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-1m)
- update to 2.32.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- fix BR (good-bye beagle)

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.29.90-3m)
- add patch for gtk-2.20 by gengtk220patch

* Tue Feb 16 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.29.90-2m)
- remove eel2-devel from BuildPrereq

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.4-2m)
- delete __libtoolize hack

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.4-1m)
- update to 2.28.4

* Mon Dec 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-2m)
- define __libtoolize :

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Apr  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.93-1m)
- update to 2.25.93

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.4-1m)
- update to 2.25.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against rpm-4.6

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
- welcome beagle tracker
- good-bye empty-view

* Tue Aug 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.5.1-3m)
- add Requires: gvfs (gnome-vfs like tool)

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.5.1-2m)
- add %%pre
- change %%preun

* Sun Jul 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.5.1-1m)
- update to 2.22.5.1
- good bye tracker and beagle

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.4-1m)
- update to 2.22.4

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.2-2m)
- rebuild against librsvg2-2.22.2-3m

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against gcc43

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Dec 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-3m)
- delete patch0 (good-bye beagle)

* Sun Dec  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- rebuild against beagle-0.3.0 (add patch0)

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-1m)
- update to 2.18.0.1

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.16.3-3m)
- disable mono for ppc64, alpha, sparc64

* Tue Dec 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16.3-2m)
- add directories for extentions and revise %%files, thanks >> 837
- http://pc8.2ch.net/test/read.cgi/linux/1092539027/837

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.3-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Sun Apr 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-3m)
- revise %%post section
-- add execute update-mime-database

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-2m)
- rebuild against libgsf-1.14.0

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-3m)
- rebuild against libexif-0.6.12-1m

* Sun Nov 20  2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.1-2m)
- remove make check

* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop
- substitutes for patch1 from patch0

* Mon Dec 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.3-2m)
- add BuildPrereq: gnome-keyring-devel for use /usr/lib/libgnome-keyring.la

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.3-1m)
- version 2.6.3

* Sat Apr 24 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.1-2m)
- import patch to add keyboard shortcut (shift+backspace)

* Thu Apr 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.1-1m)
- version 2.6.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-1m)
- pretty spec file.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.8-1m)
- version 2.3.8

* Tue Jul 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.7-1m)
- version 2.3.7

* Fri Jul 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-2m)
- rebuild against for libcroco-0.3.0

* Sat Jun 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Sat Jun 28 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.3.5-2m)
- now requires gnome-vfs >= 2.3.5

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Tue Jun  3 2003 zunda <zunda at freeshell.org>
- (2.3.2-2m)
- now requires eel >= 2.3.2

* Tue May 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Fri May 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.1-2m)
- BuildPreReq: gnome-vfs-devel >= 2.3.1

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Sat Apr 12 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (2.2.3.1-2m)
- rebuild against for librsvg (libcroco).

* Wed Apr 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.3.1-1m)
- version 2.2.3.1

* Tue Apr 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.3-1m)
- version 2.2.3

* Tue Mar 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-2m)
- rebuild against for XFree86-4.3.0

* Tue Mar 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-1m)
- version 2.2.2

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.1-2m)
  rebuild against openssl 0.9.7a

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.2-1m)
- version 2.2.0.2

* Fri Jan 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.91-1m)
- version 2.1.91

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Sat Nov 23 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.1.2-2m)
- fix %files

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.7-1m)
- version 2.0.7

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Sat Aug 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-2m)
- rebuild against for gnome-desktop-2.0.4
- rebuild against for gnome-session-2.0.3

* Fri Jul 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-3m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- add BuildPrereq: gnome-desktop-devel

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-17m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-16k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-14k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-12k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-10k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.19-12k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.19-10k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.19-8k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.19-6k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.19-4k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.19-2k)
- version 1.1.19

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.18-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.18-2k)
- version 1.1.18

* Thu May 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.17-4k)
- add ui files

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.17-2k)
- version 1.1.17

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.16-2k)
- version 1.1.16

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.15-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.15-2k)
- version 1.1.15

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.14-2k)
- version 1.1.14

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.13-2k)
- version 1.1.13

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.12-6k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.12-4k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.12-2k)
- version 1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.11-6k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.11-4k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.11-2k)
- version 1.1.11

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.10-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.10-2k)
- version 1.1.10

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.9-4k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.9-2k)
- version 1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-8k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-6k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-4k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-2k)
- version 1.1.8

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.7-4k)
- just rebuild

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.7-2k)
- version 1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-6k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-4k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-2k)
- version 1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-26k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-24k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-22k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-20k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-18k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-16k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-14k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-12k)
- add kondara news patch
- add conv.patch

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-10k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-8k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-6k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-4k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-2k)
- version 1.1.5
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.4-6k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.4-4k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.4-2k)
- version 1.1.4
- rebuild against for gnome-vfs-1.9.6
- rebuild against for GConf-1.1.7
- rebuild against for eel-1.1.3
- rebuild against for libbonoboui-1.110.2
- rebuild against for librsvg-1.1.2
- rebuild against for ORBit2-2.3.104
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-8k)
- rebuild against for libglade-1.99.6

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-6k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-4k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.3-2k)
- version 1.1.3
- rebuild against for eel-1.1.2
- rebuild against for libgnomeui-1.110.0
- rebuild against for libgnomecanvas-1.110.0
- rebuild against for libgnome-1.110.0
- rebuild against for libbonoboui-1.110.0
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-4k)
- rebuild against for linc-0.1.15

* Mon Jan  7 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.5-0.02002010702k)
- cvs version 1.0.5
- gnome2 env

* Fri Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-34k)
- remove ammonite

* Fri Dec 21 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-32k)
- change gnome-help: handler

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-26k)
- add omfenc patch
- use scrollkeeper 0.3

* Thu Dec 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-24k)
- add japanese users-guide

* Thu Dec 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-22k)
- setlocale(CAT,"")...

* Tue Dec 11 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-20k)
- toc diet
- add Require gnome-doc-tools

* Fri Dec  7 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-18k)
- create converters package
- change nautilus-1.0.6-bzinfo.patch

* Thu Dec  6 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-16k)
- add nautilus-1.0.6-bzinfo.patch

* Tue Nov 27 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0.6-14k)
- rebuild against mozilla-0.9.6

* Fri Nov 16 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-12k)
- add nautilus-1.0.6-mozilla_lang.patch

* Thu Nov 15 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-10k)
- add notesb64 patch because libxml1 is broken..

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-8k)
- nigittenu

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-6k)
- add pclose patch

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-4k)
- add xsl patch

* Thu Oct 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.5-2k)
- version 1.0.5

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (1.0.4-12k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.0.4-10k)
- rebuild against libpng 1.2.0.

* Mon Sep 17 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.0.4-8k)
  rebuild against for mozilla-0.9.4

* Tue Aug 28 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.0.4-4k)
- rebuild against for gnome-print-0.29

* Thu Aug 22 2001 Shingo Akagaki <dora@kondara.org>
- revuild against for librsvg, eel 1.0.1
- Require mozilla = 0.9.3

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.3-22k)
- no more ifarch alpha.

* Fri Jun 15 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against for mozilla-0.9.1

* Wed May 23 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add nautilus-1.0.3-mozilla_title.patch
  based galeon-title-jp.patch

* Sat May 19 2001 Shingo Akagaki <dora@kondara.org>
- nautilus news i18n fix

* Thu May 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- use /usr/share/nautilus/linksets/desktop.xml

* Thu May 17 2001 Shingo Akagaki <dora@kondara.org>
- disable first time druid
- chenge eazel URIs because eazel was ... TT

* Wed May 16 2001 Shingo Akagaki <dora@kondara.org>
- add gmclink patch
- rdf file's encoding may be UTF-8 ...dasa...

* Tue May 15 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- fix kondara rdf file path

* Mon May 14 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add kondara channel

* Fri May 11 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- bug fix...

* Thu May 10 2001 Akira Higuchi <a@kodara.org>
- requires ammonite >= 1.0.2

* Tue May  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.3

* Tue Apr 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.2
- change rsvg
- nautilus_font_manager ... TT

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0.1.1

* Sat Apr  8 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.1-5k)
  don't forget automake!!

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.1-3k)
- version 1.0.1

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (1.0-5k)
- rebuild against audiofile-0.2.1.

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0
- K2K

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5-9k)
- modified spec file with macros about docdir

* Thu Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5-7k)
- build against audiofile-0.2.0

* Wed Dec 06 2000 Kenichi Matsubara <m@kondara.org>
- use egcs,egcs++.

* Wed Nov 15 2000 Shingo Akagaki <dora@kondara.org>
- now we can build this

* Wed Nov 08 2000 KUSUNOKI Masanori <nori@kondara.org>
- Kondarized ;-P
- Fontset patch ported from nautilus-0.1.0-kondara-20000913.patch.

* Tue Oct 10 2000 Robin Slomkowski <rslomkow@eazel.com>
- removed obsoletes from sub packages and added mozilla and trilobite
subpackages

* Wed Apr 26 2000 Ramiro Estrugo <ramiro@eazel.com>
- created this thing
