%global momorel 1

Summary: A C library for multiple-precision floating-point computations
Name: mpfr
Version: 3.1.2
Release: %{momorel}m%{?dist}
URL: http://www.mpfr.org/
Source0: http://www.mpfr.org/mpfr-%{version}/mpfr-%{version}.tar.xz
NoSource: 0
License: LGPLv2+ and GPLv2+ and GFDL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf libtool gmp-devel
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: gmp >= 4.2.1

%description
The MPFR library is a C library for multiple-precision floating-point
computations with "correct rounding". The MPFR is efficient and 
also has a well-defined semantics. It copies the good ideas from the 
ANSI/IEEE-754 standard for double-precision floating-point arithmetic 
(53-bit mantissa). MPFR is based on the GMP multiple-precision library.

%package devel
Summary: Development tools A C library for mpfr library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description devel
The static libraries, header files and documentation for using the MPFR 
multiple-precision floating-point library in applications.

If you want to develop applications which will use the MPFR library,
you'll need to install the mpfr-devel package.  You'll also need to
install the mpfr package.

%prep
%setup -q

%build
%configure --disable-assert
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_libdir}/libmpfr.la
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_libdir}/libmpfr.a
rm -rf %{buildroot}%{_datadir}/doc/mpfr

%check
make %{?_smp_mflags} check

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/mpfr.info %{_infodir}/dir || :

%preun devel
if [ "$1" = 0 ]; then
   /sbin/install-info --delete %{_infodir}/mpfr.info %{_infodir}/dir || :
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING COPYING.LESSER NEWS README
%{_libdir}/libmpfr.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libmpfr.so
%{_includedir}/*.h
%{_infodir}/mpfr.info*

%changelog
* Thu Mar 14 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2, bug fix release

* Sat Jul 21 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-1m)
- update to 3.1.1

* Tue Oct 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0
- add cumulative patch

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- fix four bugs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update 3.0.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-1m)
- update 3.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-2m)
- full rebuild for mo7 release

* Wed Dec 30 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-1m)
- [SECURITY] fix a buffer overflow
- update 2.4.2
- add cumulative patch to fix two known bugs

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-1m)
- [SECURITY] CVE-2009-0757
- update to 2.4.1
- %%define __libtoolize :
- License: LGPLv2+ and GPLv2+ and GFDL

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-2m)
- rebuild against rpm-4.6

* Thu Sep 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.2-1m)
- update 2.3.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3.1-1m)
- import to Momonga (from Fedora, 2.3.0-3)

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.3.0-3
- Autorebuild for GCC 4.3

* Fri Jan 18 2008 Ivana Varekova <varekova@redhat.com> 2.3.0-2
- rebuilt

* Thu Sep 20 2007 Ivana Varekova <varekova@redhat.com> 2.3.0-1
- update to 2.3.0
- fix license flag

* Mon Aug 20 2007 Ivana Varekova <varekova@redhat.com> 2.2.1-2
- spec file cleanup (#253440)

* Mon Jan 16 2007 Ivana Varekova <varekova@redhat.com> 2.2.1-1
- started

