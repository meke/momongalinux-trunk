%global momorel 1
%global geckover 1.9.1

Summary:   epiphany is a GNOME web browser
Name:      epiphany
Version:   3.6.0
Release:   %{momorel}m%{?dist}
License:   GPL
Group:     Applications/Internet
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

URL:       http://www.gnome.org/projects/epiphany/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: libcanberra-devel >= 0.11
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gtk3-devel
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: libxslt-devel >= 1.1.24
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: libglade2-devel >= 2.6.3
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: gnome-desktop-devel >= 3.0.0
BuildRequires: startup-notification-devel >= 0.9
BuildRequires: dbus-glib-devel >= 0.80
BuildRequires: webkitgtk-devel >= 1.6.1
BuildRequires: pygtk2-devel >= 2.14.1
BuildRequires: avahi-devel >= 0.6.24
BuildRequires: enchant-devel >= 1.4.2
BuildRequires: iso-codes
BuildRequires: gtk-doc
BuildRequires: libsoup >= 2.29.91
BuildRequires: seed-devel
BuildRequires: gobject-introspection-devel >= 0.9.10
Requires: dbus
Requires: pygtk2
Requires(pre): GConf2
Requires(pre): rarian
Provides: webclient
Requires(pre): hicolor-icon-theme gtk2

%description
Epiphany is a GNOME web browser based on the mozilla rendering engine. The name meaning: "An intuitive grasp of reality through something (as an event) usually simple and striking"

%package devel
Summary: files for developing epiphany components
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: nautilus-devel
Requires: gtk2-devel
Requires: libxml2-devel

%description devel
This package provides the necessary development libraries and include
files to allow you to develop epiphany components.

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --disable-static \
    --enable-gtk-doc \
    --disable-schemas-install \
    --enable-dbus \
    --enable-python \
    --disable-scrollkeeper \
    --enable-spell-checker \
    --enable-introspection=yes \
    --enable-network-manager \
    --enable-seed
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

# add extension dir
mkdir -p %{buildroot}%{_libdir}/epiphany/3.0/extensions

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog* HACKING NEWS MAINTAINERS README TODO
%{_bindir}/epiphany
%{_bindir}/ephy-profile-migrator
%{_datadir}/applications/epiphany.desktop
%{_datadir}/epiphany
%{_mandir}/man1/epiphany.1*
%{_libdir}/epiphany
%{_libdir}/girepository-1.0/Epiphany-3.6.typelib
%{_datadir}/GConf/gsettings/epiphany.convert
%{_datadir}/glib-2.0/schemas/org.gnome.Epiphany.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.epiphany.gschema.xml

%files devel
%defattr(-, root, root)
%{_includedir}/epiphany
%{_libdir}/pkgconfig/epiphany-3.6.pc
%{_datadir}/aclocal/epiphany.m4
%{_datadir}/dbus-1/services/org.gnome.Epiphany.service
%{_datadir}/gtk-doc/html/epiphany
%{_datadir}/gir-1.0/Epiphany-3.6.gir

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Tue Sep 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91.1-1m)
- update to 3.5.91.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91.1-2m)
- fix BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-1m)
- update to 3.1.91.1

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Tue Apr 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.6-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.6-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.6-1m)
- update to 2.30.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-4m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- version 2.30

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90.1-1m)
- update to 2.29.90.1
- delete patch0 merged

* Sat Feb 13 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-4m)
- add patch for gtk-2.20

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- delete __libtoolize hack

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- --disable-gtk-doc

* Wed Dec 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.0-2m)
- add BuildRequires: seed-devel

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3
- delete patch0

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.2-2m)
- rebuild against xulrunner-1.9.1

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-1m)
- update to 2.25.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2.1-2m)
- rebuild against python-2.6.1-2m

* Thu Nov 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2.1-1m)
- update to 2.24.2.1

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0.1-1m)
- update to 2.24.0.1

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3
- delete patch1 (merged)

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.2-1m)
- update 2.22.2
- add workaround patch to fix compilation error
- add Requires: gecko-libs

* Fri May  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-4m)
- own extension dir

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1.1-3m)
- rebuild against firefox-3

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.22.1.1-2m)
- rebuild against firefox-2.0.0.14

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-1m)
- update to 2.22.1.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.22.0-2m)
- rebuild against firefox-2.0.0.13

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- delete gcc43 patch (merged)

* Sun Feb 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.3-3m)
- add gcc43 patch

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.3-2m)
- rebuild against firefox-2.0.0.11

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.2-2m)
- rebuild against firefox-2.0.0.11

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-4m)
- rebuild against firefox-2.0.0.10

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-3m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.20.0-2m)
- rebuild against firefox-2.0.0.8

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Sep 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.3-4m)
- rebuild against firefox-2.0.0.7

* Mon Aug  6 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.18.3-3m)
- rebuild against firefox-2.0.0.6

* Sat Jul 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.3-2m)
- rebuild against firefox-2.0.0.5

* Mon Jul  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Sat Jun  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-2m)
- rebuild against firefox

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Sun Mar 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18.0-2m)
- rebuild against firefox-2.0.0.3-2m

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Fri Mar  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-2m)
- fix %%files (schemas)

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.3-2m)
- rebuild against python-2.5-9m
- delete configure.patch

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Nov 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2.1-3m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.2.1-2m)
- rebuild against expat-2.0.0-1m

*Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2.1-1m)
- update to 2.14.2.1

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-4m)
- rebuild against dbus-0.61

* Sat Apr 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.14.1-3m)
- rebuild against mozilla-1.7.13

* Sun Apr 16 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.14.1-2m)
- add configure patch for finding site-packages dir on lib64 env.
- add NetworkManager-devel to BuildPrereq

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Tue Apr 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.4.1-1m)
- update to 1.8.4.1

* Tue Jan  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-2m)
- rebuild against dbus-0.60-2m

* Sat Dec 2 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3
- GNOME 2.12.2 Desktop

* Mon Nov 28 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-3m)
- add %preun for schemas

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-2m)
- delete autoreconf and make check

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- version up
- GNOME 2.12.1 Desktop

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9-1m)
- update to 1.4.9 for mozilla-1.7.12

* Sun Jul 31 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.8-5m)
- rebuild against mozilla-1.7.11

* Mon Jul 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.8-4m)
- rebuild against mozilla-1.7.10

* Thu May 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.8-3m)
- rebuild against mozilla-1.7.8

* Mon Apr 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.8-2m)
- rebuild against mozilla-1.7.7

* Thu Mar 31 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.8-1m)
- update to 1.4.8 for mozilla-1.7.6

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4.7-1m)
- update for mozilla-1.7.5

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.6-5m)
- rebuild against gcc-c++-3.4.1

* Thu Sep 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.6-4m)
- rebuild against mozilla-1.7.3

* Sat Aug  7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.6-3m)
- rebuild against mozilla-1.7.2

* Tue Jul  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.6-2m)
- remove '%%requires_eq mozilla' and rebuild against mozilla-1.7-2m

* Tue Jun 29 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.6-1m)
- ver. up

* Sat Apr 24 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.3-1m)
- version 1.2.3

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (1.2.2-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.2-1m)
- version 1.2.2
- GNOME 2.6 Desktop
- adjustment BuildPreReq
- remove intltool-* file copy command

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.7-2m)
- revised spec for enabling rpm 4.2.

* Fri Mar 12 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.7-1m)
- update to 1.0.7
- use installed intltool-{extract,merge,update}.in instead of ones that included in an archive.

* Sat Dec 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.6-1m)
- version up to 1.0.6

* Mon Oct 27 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0.4-1m)
- version up to 1.0.4

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-1m)
- version 1.0.3

* Fri Oct 17 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0.2-2m)
- rebuild against mozilla-1.5

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Thu Oct 09 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0.1-1m)
- version 1.0.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0-1m)
- version 1.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.4-1m)
- version 0.8.4

* Tue Aug 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.2-1m)
- version 0.8.2

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.0-1m)
- version 0.8.0

* Wed Jul  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.7.3-2m)
- rebuild against for mozilla-1.4
- build with gcc3.2

* Mon Jun 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.3-1m)
- version 0.7.3

* Sat Jun 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.1-1m)
- version 0.7.1

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.0-1m)
- version 0.7.0

* Mon May 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-1m)
- version 0.6.1

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.0-1m)
- version 0.6.0

* Mon Apr 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-2m)
- modify starthere xmls and xsl.
- add encoding property to xmls.
- add lang property to section element (ephy-start-here.c).
- add lang property to html element (xsl).

* Mon Apr 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-1m)
- version 0.5.0

* Mon Apr  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.2-1m)
- version 0.4.2
