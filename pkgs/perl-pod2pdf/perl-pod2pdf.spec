%global         momorel 9

Name:           perl-pod2pdf
Version:        0.42
Release:        %{momorel}m%{?dist}
Summary:        pod2pdf Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/pod2pdf/
Source0:        http://www.cpan.org/authors/id/J/JO/JONALLEN/pod2pdf-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Getopt-ArgvFile
BuildRequires:  perl-PDF-API2 >= 0.6
BuildRequires:  perl-podlators
BuildRequires:  perl-Pod-Escapes
BuildRequires:  perl-Pod-Parser
Requires:       perl-Getopt-ArgvFile
Requires:       perl-PDF-API2 >= 0.6
Requires:       perl-podlators
Requires:       perl-Pod-Escapes
Requires:       perl-Pod-Parser
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
INSTALLATION  Unzip and untar the archive, cd to the distribution
directory and enter the following commands:

%prep
%setup -q -n pod2pdf-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc artistic-2_0.txt Changes README
%{_bindir}/pod2pdf
%{perl_vendorlib}/App/pod2pdf.pm
%{_mandir}/man1/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
