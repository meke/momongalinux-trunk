%global momorel 1

Name:		xdg-user-dirs
Version:	0.14
Release:	%{momorel}m%{?dist}
Summary:	Handles user special directories
Group:		User Interface/Desktops
License:	GPL
URL:		http://freedesktop.org/wiki/Software/xdg-user-dirs
Source0:	http://user-dirs.freedesktop.org/releases/%{name}-%{version}.tar.gz
NoSource:       0
Source1:	xdg-user-dirs.sh
Source2:	xdg-user-dirs.sysconfig

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gettext
Requires:	xorg-x11-xinit

%description
Contains xdg-user-dirs-update that updates folders in a users
homedirectory based on the defaults configured by the administrator.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d
install -p -m 755 %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -p -m 755 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/xdg-user-dirs

%find_lang %name

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc NEWS AUTHORS README ChangeLog COPYING
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/xdg/user-dirs.conf
%config(noreplace) %{_sysconfdir}/xdg/user-dirs.defaults
%config(noreplace) %{_sysconfdir}/sysconfig/xdg-user-dirs
%{_sysconfdir}/X11/xinit/xinitrc.d/*

%changelog
* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-2m)
- full rebuild for mo7 release

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-5m)
- rebuild against rpm-4.6

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-4m)
- delete patch0 fixed

* Sat May 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10-3m)
- default user dir LANG is C
- You can control by /etc/sysconfig/xdg-user-dirs.

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-2m)
- does not use $HOME/.config/user-dirs.*

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-3m)
- rebuild against gcc43

* Wed Jan  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-2m)
- modify Requires

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- import from Fedora devel

* Tue Aug 21 2007 Alexander Larsson <alexl@redhat.com> - 0.9-1
- Update to 0.9 (new translations)

* Tue May 29 2007 Matthias Clasen <mclasen@redhat.com> - 0.8-2
- Fix a possible crash.

* Wed May 16 2007  <alexl@redhat.com> - 0.8-1
- Update to 0.8, (only) fixing bug that always recreated deleted directories (#240139)

* Wed Apr 11 2007 Alexander Larsson <alexl@redhat.com> - 0.6-1
- Update to 0.6 (minor fixes)

* Mon Mar 26 2007 Alexander Larsson <alexl@redhat.com> - 0.5-1
- update to 0.5 (more translations)

* Wed Mar  7 2007 Alexander Larsson <alexl@redhat.com> - 0.4-1
- Update to 0.4

* Thu Mar  1 2007 Alexander Larsson <alexl@redhat.com> - 0.3-1
- Update to 0.3

* Fri Feb 23 2007 Alexander Larsson <alexl@redhat.com> - 0.2-1
- initial version
