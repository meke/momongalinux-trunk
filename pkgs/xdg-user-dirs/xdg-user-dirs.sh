#!/bin/sh
# Copyright (C) 2007 Red Hat, Inc. All rights reserved. This
# copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the
# GNU General Public License version 2.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
. /etc/sysconfig/xdg-user-dirs

if [ -x /usr/bin/xdg-user-dirs-update ]; then
  if [ ${USE_LANG_C} = "yes" ]; then
    LANG=C /usr/bin/xdg-user-dirs-update
  else
    /usr/bin/xdg-user-dirs-update
  fi
fi    
