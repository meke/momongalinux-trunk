%global momorel 2
%define realname xrdb

Summary: X.Org X11 X server utilities (%{realname})
Name: xorg-x11-%{realname}
Version: 1.0.9
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%define xorgurl http://xorg.freedesktop.org/releases/individual/app/
Source0: %{xorgurl}/%{realname}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel
BuildRequires: libXmu-devel
BuildRequires: libX11-devel

Requires: cpp

# FIXME: check if still needed for X11R7
Requires(pre): filesystem >= 2.4.6

%description
%{realname}

%prep
%setup -q -n %{realname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog AUTHORS COPYING README
%{_bindir}/%{realname}
%{_mandir}/man1/%{realname}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9
- [SECURITY] CVE-2011-0465

* Sat Feb  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-1m)
- update 1.0.5

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- separate from xorg-x11-server-utils
