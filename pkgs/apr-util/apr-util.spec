%global momorel 1

%define apuver 1

Summary: Apache Portable Runtime Utility library
Name: apr-util
Version: 1.5.1
Release: %{momorel}m%{?dist}
License: Apache
Group: System Environment/Libraries
URL: http://apr.apache.org/
Source0: http://archive.apache.org/dist/apr/%{name}-%{version}.tar.bz2
NoSource: 0
Patch1: apr-util-1.2.7-pkgconf.patch
Patch2: apr-util-1.3.7-nodbmdso.patch
# for autoconf-2.60
Patch10: apr-util-1.2.7-momonga.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, apr-devel >= 1.4.6
BuildRequires: libdb-devel >= 3.5.15, expat-devel >= 2.0.0-2m, e2fsprogs-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: nss-devel
BuildRequires: libuuid-devel 

%description
The mission of the Apache Portable Runtime (APR) is to provide a
free library of C data structures and routines.  This library
contains additional utility interfaces for APR; including support
for XML, LDAP, database interfaces, URI parsing and more.

%package devel
Group: Development/Libraries
Summary: APR utility library development kit
Requires: apr-util = %{version}-%{release}, apr-devel, pkgconfig
Requires: openldap-devel, libdb-devel, expat-devel

%description devel
This package provides the support files which can be used to 
build applications using the APR utility library.  The mission 
of the Apache Portable Runtime (APR) is to provide a free 
library of C data structures and routines.

%package pgsql
Group: Development/Libraries
Summary: APR utility library PostgreSQL DBD driver
BuildRequires: postgresql-devel
Requires: apr-util = %{version}-%{release}

%description pgsql
This package provides the PostgreSQL driver for the apr-util
DBD (database abstraction) interface.

%package mysql
Group: Development/Libraries
Summary: APR utility library MySQL DBD driver
BuildRequires: mysql-devel >= 5.5.10
Requires: apr-util = %{version}-%{release}

%description mysql
This package provides the MySQL driver for the apr-util DBD
(database abstraction) interface.

%package sqlite
Group: Development/Libraries
Summary: APR utility library SQLite DBD driver
BuildRequires: sqlite-devel >= 3.0.0
Requires: apr-util = %{version}-%{release}

%description sqlite
This package provides the SQLite driver for the apr-util DBD
(database abstraction) interface.

%package freetds
Group: Development/Libraries
Summary: APR utility library FreeTDS DBD driver
BuildRequires: freetds-devel
Requires: apr-util = %{version}-%{release}

%description freetds
This package provides the FreeTDS driver for the apr-util DBD
(database abstraction) interface.

%package odbc
Group: Development/Libraries
Summary: APR utility library ODBC DBD driver
BuildRequires: unixODBC-devel
Requires: apr-util = %{version}-%{release}

%description odbc
This package provides the ODBC driver for the apr-util DBD
(database abstraction) interface.

%package ldap
Group: Development/Libraries
Summary: APR utility library LDAP support
BuildRequires: openldap-devel >= 2.4
Requires: apr-util = %{version}-%{release}

%description ldap
This package provides the LDAP support for the apr-util.

%package openssl
Group: Development/Libraries
Summary: APR utility library OpenSSL crytpo support
BuildRequires: openssl-devel
Requires: apr-util = %{version}-%{release}

%description openssl
This package provides the OpenSSL crypto support for the apr-util.

%package nss
Group: Development/Libraries
Summary: APR utility library NSS crytpo support
BuildRequires: nss-devel
Requires: apr-util = %{version}-%{release}

%description nss
This package provides the NSS crypto support for the apr-util.

%prep
%setup -q
%patch1 -p1 -b .pkgconf
%patch2 -p1 -b .nodbmdso
%patch10 -p1 -b .layout

# Our apr.h file is arch specific
%ifarch %{ix86}
    sed -i -e 's,apr_h=\(.*\)/apr.h",apr_h=\1/apr-i386.h",' build/dso.m4
%else
    sed -i -e 's,apr_h=\(.*\)/apr.h",apr_h=\1/apr-%{_arch}.h",' build/dso.m4
%endif

%build
autoheader && autoconf
export LDFLAGS=`pkg-config sqlite3 --libs`
export CFLAGS=`pkg-config sqlite3 --cflags`
%configure --with-apr=%{_prefix} \
        --includedir=%{_includedir}/apr-%{apuver} \
        --with-ldap --without-gdbm \
        --with-sqlite3 --with-pgsql --with-mysql --with-freetds --with-odbc \
        --with-berkeley-db \
        --without-sqlite2 \
        --with-crypto --with-openssl --with-nss \
%if %{_lib} == "lib64"
        --enable-layout=Momonga64
%else
        --enable-layout=Momonga
%endif

make %{?_smp_mflags} LIBTOOL=%{_bindir}/libtool

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} LIBTOOL=%{_bindir}/libtool

mkdir -p %{buildroot}/%{_datadir}/aclocal
install -m 644 build/find_apu.m4 %{buildroot}/%{_datadir}/aclocal

# Unpackaged files; remove the static libaprutil
rm -f %{buildroot}%{_libdir}/aprutil.exp \
      %{buildroot}%{_libdir}/libapr*.a

# And remove the reference to the static libaprutil from the .la
# file.
sed -i '/^old_library/s,libapr.*\.a,,' \
      %{buildroot}%{_libdir}/libapr*.la

# Remove unnecessary exports from dependency_libs
sed -ri '/^dependency_libs/{s,-l(pq|sqlite[0-9]|rt|dl|uuid) ,,g}' \
      %{buildroot}%{_libdir}/libapr*.la

find %{buildroot} -name "*.la" -delete

# Trim libtool DSO cruft
rm -f %{buildroot}%{_libdir}/apr-util-%{apuver}/*.*a

%check
# Run the less verbose test suites
export MALLOC_CHECK_=2 MALLOC_PERTURB_=$(($RANDOM % 255 + 1))
cd test
make %{?_smp_mflags} testall LIBTOOL=%{_bindir}/libtool
# testall breaks with DBD DSO; ignore
export LD_LIBRARY_PATH="`echo "../dbm/.libs:../dbd/.libs:../ldap/.libs:$LD_LIBRARY_PATH" | sed -e 's/::*$//'`"
./testall -v -q || true
./testall testrmm
./testall testdbm

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE NOTICE
%{_libdir}/libaprutil-%{apuver}.so.*
%dir %{_libdir}/apr-util-%{apuver}

%files pgsql
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_dbd_pgsql*

%files mysql
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_dbd_mysql*

%files sqlite
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_dbd_sqlite*

%files freetds
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_dbd_freetds*

%files odbc
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_dbd_odbc*

%files ldap
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_ldap*

%files openssl
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_crypto_openssl*

%files nss
%defattr(-,root,root,-)
%{_libdir}/apr-util-%{apuver}/apr_crypto_nss*

%files devel
%defattr(-,root,root,-)
%{_bindir}/apu-%{apuver}-config
%{_libdir}/libaprutil-%{apuver}.so
%{_includedir}/apr-%{apuver}/*.h
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4

%changelog
* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-2m)
- rebuild against libdb-3.5.13

* Wed Mar 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.12-2m)
- rebuild against libdb

* Sun May 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-1m)
- update to 1.3.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.10-6m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.10-5m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.10-4m)
- rebuild for new GCC 4.5

* Fri Oct 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.10-3m)
- change BR uuid-devel to libuuid-devel

* Fri Oct 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.10-2m)
- add BR uuid-devel

* Sun Oct 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.10-1m)
- [SECURITY] CVE-2010-1623, CVE-2009-3560, CVE-2009-3720
- see http://www.apache.org/dist/apr/CHANGES-APR-UTIL-1.3

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.9-7m)
- build fix

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.9-6m)
- update CFLAGS and LDFLAGS for sqlite3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.9-5m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-4m)
- rebuild against openssl-1.0.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-3m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-1m)
- [SECURITY] CVE-2009-2412
- update to 1.3.9

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-3m)
- revert previous changes and fix build on i686

* Tue Jun 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.7-2m)
- fix build on i686 by Fedora's nodbmdso.patch
 +* Tue Jun  9 2009 Joe Orton <jorton@redhat.com> 1.3.7-4
 +- disable DBM-drivers-as-DSO support
 +- backport r783046 from upstream

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-1m)
- [SECURITY] CVE-2009-0023 CVE-2009-1955 CVE-2009-1956
- sync with Fedora 11 (1.3.7-1)

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-1m)
- sync with Fedora 11 (1.3.4-3)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-10m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-9m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-8m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.8-7m)
- rebuild against db4-4.7.25-1m

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.8-6m)
- rebuild against openssl-0.9.8h

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.8-5m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.8-4m)
- rebuild against openldap-2.4.8

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.8-3m)
- rebuild against db4-4.6.21

* Wed May 30 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.8-2m)
- sync with fc-devel (apr-util-1_2_8-7)
--* Thu Apr  5 2007 Joe Orton <jorton@redhat.com> 1.2.8-7
--- remove old Conflicts, doxygen BR (#225254)
--* Fri Mar 23 2007 Joe Orton <jorton@redhat.com> 1.2.8-6
--- add DBD DSO lifetime fix (r521327)
--* Thu Mar 22 2007 Joe Orton <jorton@redhat.com> 1.2.8-5
--- drop doxygen documentation (which caused multilib conflicts)
--* Wed Feb 28 2007 Joe Orton <jorton@redhat.com> 1.2.8-4
--- add mysql driver in -mysql subpackage (Bojan Smojver, #222237)
--* Tue Feb 27 2007 Joe Orton <jorton@redhat.com> 1.2.8-3
--- build DBD drivers as DSOs (w/Bojan Smojver, #192922)
--- split out pgsql driver into -pgsql subpackage

* Fri Feb 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.7-5m)
- rebuild against db-4.5

* Sun Nov  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.7-4m)
- revised configure for lib64

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.7-3m)
- use config.layout

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.7-2m)
- delete libtool library

* Tue Oct 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7
- add sed script (%%build)

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.6-3m)
- rebuild against expat-2.0.0-1m

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.6-2m)
- depend sqlite3 -> sqlite

* Wed May  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.6-1m)
- import from fc-devel

* Thu Apr  6 2006 Joe Orton <jorton@redhat.com> 1.2.6-2
- update to 1.2.6
- define LDAP_DEPRECATED in apr_ldap.h (r391985, #188073)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.2.2-4.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.2.2-4.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 30 2006 Joe Orton <jorton@redhat.com> 1.2.2-4
- rebuild to drop reference to libexpat.la

* Wed Jan 18 2006 Joe Orton <jorton@redhat.com> 1.2.2-3
- disable sqlite2 support
- BuildRequire e2fsprogs-devel
- enable malloc paranoia in %%check

* Tue Jan  3 2006 Jesse Keating <jkeating@redhat.com> 1.2.2-2.2
- rebuilt again

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Dec  6 2005 Joe Orton <jorton@redhat.com> 1.2.2-2
- trim exports from .la file/--libs output (#174924)

* Fri Nov 25 2005 Joe Orton <jorton@redhat.com> 1.2.2-1
- update to 1.2.2

* Thu Oct 20 2005 Joe Orton <jorton@redhat.com> 0.9.7-3
- fix epoch again

* Thu Oct 20 2005 Joe Orton <jorton@redhat.com> 0.9.7-2
- update to 0.9.7
- drop static libs (#170051)

* Tue Jul 26 2005 Joe Orton <jorton@redhat.com> 0.9.6-3
- add FILE bucket fix for truncated files (#159191)
- add epoch to dependencies

* Fri Mar  4 2005 Joe Orton <jorton@redhat.com> 0.9.6-2
- rebuild

* Wed Feb  9 2005 Joe Orton <jorton@redhat.com> 0.9.6-1
- update to 0.9.6

* Wed Jan 19 2005 Joe Orton <jorton@redhat.com> 0.9.5-3
- restore db-4.3 detection lost in 0.9.5 upgrade

* Wed Jan 19 2005 Joe Orton <jorton@redhat.com> 0.9.5-2
- rebuild

* Mon Nov 22 2004 Joe Orton <jorton@redhat.com> 0.9.5-1
- update to 0.9.5

* Thu Nov 11 2004 Jeff Johnson <jbj@jbj.org> 0.9.4-19
- actually explicitly check for and detect db-4.3.

* Thu Nov 11 2004 Jeff Johnson <jbj@jbj.org> 0.9.4-18
- rebuild against db-4.3.21.

* Fri Sep 17 2004 Joe Orton <jorton@redhat.com> 0.9.4-17
- add security fix for CAN-2004-0786

* Sat Jun 19 2004 Joe Orton <jorton@redhat.com> 0.9.4-16
- have -devel require matching release of apr-util

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Apr  1 2004 Joe Orton <jorton@redhat.com> 0.9.4-14
- fix use of SHA1 passwords (#119651)

* Tue Mar 30 2004 Joe Orton <jorton@redhat.com> 0.9.4-13
- remove fundamentally broken check_sbcs() from xlate code

* Fri Mar 19 2004 Joe Orton <jorton@redhat.com> 0.9.4-12
- tweak xlate fix

* Fri Mar 19 2004 Joe Orton <jorton@redhat.com> 0.9.4-11
- rebuild with xlate fixes and tests enabled

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com> 0.9.4-10.1
- rebuilt

* Tue Mar  2 2004 Joe Orton <jorton@redhat.com> 0.9.4-10
- rename sdbm_* symbols to apu__sdbm_*

* Mon Feb 16 2004 Joe Orton <jorton@redhat.com> 0.9.4-9
- fix sdbm apr_dbm_exists() on s390x/ppc64

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com> 0.9.4-8
- rebuilt

* Thu Feb  5 2004 Joe Orton <jorton@redhat.com> 0.9.4-7
- fix warnings from use of apr_optional*.h with gcc 3.4

* Thu Jan 29 2004 Joe Orton <jorton@redhat.com> 0.9.4-6
- drop gdbm support

* Thu Jan  8 2004 Joe Orton <jorton@redhat.com> 0.9.4-5
- fix DB library detection

* Sat Dec 13 2003 Jeff Johnson <jbj@jbj.org> 0.9.4-4
- rebuild against db-4.2.52.

* Mon Oct 13 2003 Jeff Johnson <jbj@jbj.org> 0.9.4-3
- rebuild against db-4.2.42.

* Mon Oct  6 2003 Joe Orton <jorton@redhat.com> 0.9.4-2
- fix 'apu-config --apu-la-file' output

* Mon Oct  6 2003 Joe Orton <jorton@redhat.com> 0.9.4-1
- update to 0.9.4.

* Tue Jul 22 2003 Nalin Dahyabhai <nalin@redhat.com> 0.9.3-10
- rebuild

* Mon Jul  7 2003 Joe Orton <jorton@redhat.com> 0.9.3-9
- rebuild
- don't run testuuid test because of #98677

* Thu Jul  3 2003 Joe Orton <jorton@redhat.com> 0.9.3-8
- rebuild

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May 20 2003 Joe Orton <jorton@redhat.com> 0.9.3-6
- fix to detect crypt_r correctly (CAN-2003-0195)

* Thu May 15 2003 Joe Orton <jorton@redhat.com> 0.9.3-5
- fix to try linking against -ldb first (#90917)
- depend on openldap, gdbm, db4, expat appropriately.

* Tue May 13 2003 Joe Orton <jorton@redhat.com> 0.9.3-4
- rebuild

* Wed May  7 2003 Joe Orton <jorton@redhat.com> 0.9.3-3
- make devel package conflict with old subversion-devel
- run the less crufty parts of the test suite

* Tue Apr 29 2003 Joe Orton <jorton@redhat.com> 0.9.3-2
- run ldconfig in post/postun

* Mon Apr 28 2003 Joe Orton <jorton@redhat.com> 0.9.3-1
- initial build
