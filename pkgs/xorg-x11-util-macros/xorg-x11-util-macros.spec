%global momorel 1

%define debug_package %{nil}
%global tarname util-macros

Summary: X.Org X11 Autotools macros
Name: xorg-x11-util-macros
Version: 1.19.0
Release: %{momorel}m%{?dist}
License: "The Open Group License"
Group: Development/System
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/util/%{tarname}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
X.Org X11 autotools macros required for building the various packages that
comprise the X Window System.

%prep
%setup -q -n %{tarname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_datadir}/aclocal/xorg-macros.m4
%{_datadir}/pkgconfig/xorg-macros.pc
%{_datadir}/util-macros/INSTALL

%changelog
* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.0-1m)
- update to 1.19.0

* Fri Mar 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17-1m)
- update to 1.17

* Thu Mar  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.16.2-1m)
- update to 1.16.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.16.1-1m)
- update to 1.16.1

* Mon Dec 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.16.0-1m)
- update to 1.16.0

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15.0-1m)
- update to 1.15.0

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14.0-1m)
- update to 1.14.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.0-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.0-1m)
- update to 1.13.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.0-1m)
- update to 1.11.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.0-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update 1.10.0

* Thu Jun 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0-1m)
- update 1.9.0

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0-1m)
- update 1.7.0

* Fri Apr  2 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-1m)
- update 1.6.1

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- update 1.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update 1.3.0

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Tue Mar  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.6-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-1m)
- update 1.1.6

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-2m)
- %%NoSource -> NoSource

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Thu Nov  9 2006 Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- delete duplicated dir

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 23 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Update to util-macros-1.0.1 from X11R7.

* Thu Dec 15 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update to util-macros-1.0.0 from the X11R7 RC4 release.

* Tue Dec  6 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Update to util-macros-0.99.2 from the X11R7 RC3 release.

* Wed Oct 19 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Update to util-macros-0.99.1 from the X11R7 RC1 release.
- Disable debuginfo package creation, as there are no ELF objects present.
- Add xorg-macros.m4 to file list.

* Wed Jul 13 2005 Mike A. Harris <mharris@redhat.com> 0.0.1-1
- Initial build
