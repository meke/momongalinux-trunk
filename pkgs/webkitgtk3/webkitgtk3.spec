%global		momorel 2
## NOTE: Lots of files in various subdirectories have the same name (such as
## "LICENSE") so this short macro allows us to distinguish them by using their
## directory names (from the source tree) as prefixes for the files.
%global         add_to_doc_files()      \
        mkdir -p %{buildroot}%{_docdir}/%{name}-%{version} ||: ; \
        cp -p %1  %{buildroot}%{_docdir}/%{name}-%{version}/$(echo '%1' | sed -e 's!/!.!g')

Name:           webkitgtk3
Version:        1.9.92
Release:        %{momorel}m%{?dist}
Summary:        GTK+ Web content engine library

Group:          Development/Libraries
License:        LGPLv2+ and BSD
URL:            http://www.webkitgtk.org/

Source0:        http://webkitgtk.org/releases/webkit-%{version}.tar.xz
NoSource:	0

Patch1:         webkit-1.3.4-no-execmem.patch
Patch2:         webkit-1.1.14-nspluginwrapper.patch
Patch3:         webkit-1.9.92-bison3.patch

BuildRequires:  bison
BuildRequires:  chrpath
BuildRequires:  enchant-devel
BuildRequires:  flex
BuildRequires:  geoclue-devel
BuildRequires:  gettext
BuildRequires:  gperf
BuildRequires:  gstreamer-devel
BuildRequires:  gstreamer-plugins-base-devel
BuildRequires:  gtk2-devel
BuildRequires:  gtk3-devel >= 3.0
BuildRequires:  gtk-doc
BuildRequires:  libsoup-devel >= 2.37.2.1
BuildRequires:  libicu-devel >= 52
BuildRequires:  libjpeg-devel
BuildRequires:  libpng-devel
BuildRequires:  libxslt-devel
BuildRequires:  libXt-devel
BuildRequires:  pcre-devel
BuildRequires:  sqlite-devel
BuildRequires:  gobject-introspection-devel
#BuildRequires:  perl-Switch
BuildRequires:  mesa-libGL-devel

## Conditional dependencies...
%if %{with pango}
BuildRequires:  pango-devel
%else
BuildRequires:  cairo-devel
BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
%endif

%description
WebKitGTK+ is the port of the portable web rendering engine WebKit to the
GTK+ platform.

This package contains WebKitGTK+ for GTK+ 3.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       gtk3-devel

%description    devel
The %{name}-devel package contains libraries, build data, and header
files for developing applications that use %{name}.

%package        doc
Summary:        Documentation files for %{name}
Group:          Documentation
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}

%description    doc
This package contains developer documentation for %{name}.

%prep
%setup -qn "webkit-%{version}"
# tbzatek - doesn't apply, is this fixed?
# %patch1 -p1 -b .no-execmem
%patch2 -p1 -b .nspluginwrapper
%patch3 -p1 -b .bison3

%build
%ifarch s390 %{arm} ppc
# Use linker flags to reduce memory consumption on low-mem architectures
%global optflags %{optflags} -Wl,--no-keep-memory -Wl,--reduce-memory-overheads
%endif
%ifarch s390
# Decrease debuginfo verbosity to reduce memory consumption even more
%global optflags %(echo %{optflags} | sed 's/-g/-g1/')
%endif

# explicitly disable JIT on ARM https://bugs.webkit.org/show_bug.cgi?id=85076
CFLAGS="%{optflags} -DLIBSOUP_I_HAVE_READ_BUG_594377_AND_KNOW_SOUP_PASSWORD_MANAGER_MIGHT_GO_AWAY" %configure                                                   \
                        --with-gtk=3.0                          \
%ifarch %{arm} s390 s390x ppc ppc64
                        --disable-jit                           \
%else
                        --enable-jit                            \
%endif
                        --enable-introspection

mkdir -p DerivedSources/webkit
mkdir -p DerivedSources/WebCore
mkdir -p DerivedSources/ANGLE
mkdir -p DerivedSources/WebKit2
mkdir -p DerivedSources/InjectedBundle

# no %%{_smp_mflags} because of http://bugs.webkit.org/show_bug.cgi?id=34846
make V=1

%install
make install DESTDIR=%{buildroot}

install -d -m 755 %{buildroot}%{_libexecdir}/%{name}
install -m 755 Programs/GtkLauncher %{buildroot}%{_libexecdir}/%{name}

# Remove lib64 rpaths
chrpath --delete %{buildroot}%{_bindir}/jsc-3
chrpath --delete %{buildroot}%{_libdir}/libwebkitgtk-3.0.so
chrpath --delete %{buildroot}%{_libdir}/libwebkit2gtk-3.0.so
chrpath --delete %{buildroot}%{_libexecdir}/%{name}/GtkLauncher
chrpath --delete %{buildroot}%{_libexecdir}/WebKitPluginProcess
chrpath --delete %{buildroot}%{_libexecdir}/WebKitWebProcess

# for some reason translations don't get installed in 1.3.7
%find_lang webkit-3.0

## Finally, copy over and rename the various files for %%doc inclusion.
%add_to_doc_files Source/WebKit/LICENSE
%add_to_doc_files Source/WebKit/gtk/po/README
%add_to_doc_files Source/WebKit/gtk/NEWS
%add_to_doc_files Source/WebCore/icu/LICENSE
%add_to_doc_files Source/WebCore/LICENSE-APPLE
%add_to_doc_files Source/WebCore/LICENSE-LGPL-2
%add_to_doc_files Source/WebCore/LICENSE-LGPL-2.1
%add_to_doc_files Source/JavaScriptCore/COPYING.LIB
%add_to_doc_files Source/JavaScriptCore/THANKS
%add_to_doc_files Source/JavaScriptCore/AUTHORS
%add_to_doc_files Source/JavaScriptCore/icu/README
%add_to_doc_files Source/JavaScriptCore/icu/LICENSE


%post
/sbin/ldconfig

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :


%files -f webkit-3.0.lang
%doc %{_docdir}/%{name}-%{version}/
%exclude %{_libdir}/*.la
%{_libdir}/libwebkitgtk-3.0.so.*
%{_libdir}/libwebkit2gtk-3.0.so.*
%{_libdir}/libjavascriptcoregtk-3.0.so.*
%{_libdir}/girepository-1.0/WebKit-3.0.typelib
%{_libdir}/girepository-1.0/JSCore-3.0.typelib
%{_libexecdir}/%{name}/
%{_libexecdir}/WebKitPluginProcess
%{_libexecdir}/WebKitWebProcess
%{_datadir}/webkitgtk-3.0

%files  devel
%{_bindir}/jsc-3
%{_includedir}/webkitgtk-3.0
%{_libdir}/libwebkitgtk-3.0.so
%{_libdir}/libwebkit2gtk-3.0.so
%{_libdir}/libjavascriptcoregtk-3.0.so
%{_libdir}/pkgconfig/webkitgtk-3.0.pc
%{_libdir}/pkgconfig/webkit2gtk-3.0.pc
%{_libdir}/pkgconfig/javascriptcoregtk-3.0.pc
%{_datadir}/gir-1.0/WebKit-3.0.gir
%{_datadir}/gir-1.0/JSCore-3.0.gir

%files doc
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/webkitgtk
%{_datadir}/gtk-doc/html/webkit2gtk


%changelog
* Wed Mar 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.92-2m)
- rebuild against icu-52

* Sun Sep 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.92-1m)
- update to 1.9.92

* Sat Sep  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.90-1m)
- update to 1.9.90

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.5-1m)
- update to 1.9.5

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-3m)
- fix file confliction between webkitgtk-doc and webkitgtk3-doc

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-2m)
- add Obsoletes: webkitgtk-doc

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-1m)
- import from fedora
