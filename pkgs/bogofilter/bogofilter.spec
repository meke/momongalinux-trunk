%global momorel 1

Summary: Fast anti-spam filtering by Bayesian statistical analysis
Name: bogofilter
Version: 1.2.4
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://bogofilter.sourceforge.net/
Group: Applications/Internet
Source0: http://dl.sourceforge.net/sourceforge/bogofilter/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gsl
Requires: sqlite
BuildRequires: gsl-devel
BuildRequires: sqlite-devel

%description
Bogofilter is a Bayesian spam filter.  In its normal mode of
operation, it takes an email message or other text on standard input,
does a statistical check against lists of "good" and "bad" words, and
returns a status code indicating whether or not the message is spam.
Bogofilter is designed with fast algorithms (including Berkeley DB system),
coded directly in C, and tuned for speed, so it can be used for production
by sites that process a lot of mail.

%prep
%setup -q

%build
CFLAGS="%{optflags} `pkg-config sqlite3 --cflags`" \
LDFLAGS="`pkg-config sqlite3 --libs`" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--mandir=%{_mandir} \
	--sysconfdir=%{_sysconfdir} \
	--with-database=sqlite3

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

mv %{buildroot}%{_sysconfdir}/bogofilter.cf.example %{buildroot}%{_sysconfdir}/bogofilter.cf

# install contrib
for d in contrib ; do
  install -d %{buildroot}%{_datadir}/%{name}/$d
  files=$(find "$d" -maxdepth 1 -type f -print)
  for f in $files ; do
    case $f in
      *.c|*.o|*.obj|*/Makefile*) continue ;;
      *.1)
	cp -p $f %{buildroot}%{_mandir}/man1 ;;
      *)
	cp -p $f %{buildroot}%{_datadir}/%{name}/$d ;;
    esac
  done
done

# for doc
for n in xml html ; do
  install -d .inst/$n
  install -m644 doc/*.$n .inst/$n
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING GETTING.STARTED INSTALL NEWS* README* RELEASE.NOTES TODO
%doc bogofilter.cf.example
%doc doc/bogofilter-SA* doc/integrating* doc/README.* doc/rpm.notes.BerkeleyDB
%doc .inst/html .inst/xml
%config(noreplace) %{_sysconfdir}/bogofilter.cf
%{_bindir}/bf_compact
%{_bindir}/bf_copy
%{_bindir}/bf_tar
%{_bindir}/bogofilter
%{_bindir}/bogolexer
%{_bindir}/bogotune
%{_bindir}/bogoupgrade
%{_bindir}/bogoutil
%{_datadir}/bogofilter
%{_mandir}/man1/bf_compact.1*
%{_mandir}/man1/bf_copy.1*
%{_mandir}/man1/bf_tar.1*
%{_mandir}/man1/bogofilter.1*
%{_mandir}/man1/bogolexer.1*
%{_mandir}/man1/bogotune.1*
%{_mandir}/man1/bogoupgrade.1*
%{_mandir}/man1/bogoutil.1*

%changelog
* Tue Oct 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-1m)
- version 1.2.4

* Sun Jan 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-1m)
- version 1.2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-4m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- update CFLAGS and LDFLAGS for sqlite3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-1m)
- [SECURITY] CVE-2010-2494
- update to 1.2.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Thu Apr  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-1m)
- version 1.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-2m)
- rebuild against rpm-4.6

* Wed May 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-1m)
- version 1.1.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.6-2m)
- rebuild against gcc43

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-1m)
- version 1.1.6

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.5-2m)
- rebuild against perl-5.10.0-1m

* Fri Jan 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-1m)
- version 1.1.5

* Thu Dec 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- version 1.1.3

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1

* Fri Jul 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-1m)
- version 1.0.3

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-2m)
- depend sqlite3 -> sqlite

* Sun Mar  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- version 1.0.2

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-2m)
- build for sqlite3

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- version 1.0.0

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.6-1m)
- version 0.96.6

* Tue Nov  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.5-1m)
- version 0.96.5

* Sat Nov  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.4-1m)
- version 0.96.4

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.3-1m)
- version 0.96.3

* Wed Sep 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.2-1m)
- version 0.96.2

* Tue Sep  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.1-1m)
- version 0.96.1

* Tue Aug 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.0-1m)
- version 0.96.0

* Fri Jul 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.95.2-1m)
- initial package for sylpheed-2.0.0
