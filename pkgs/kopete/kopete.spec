%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global sopranover 2.9.4
%global qimageblitzver 0.0.6

%global moz_pluginsdir %{_kde4_libdir}/mozilla/plugins

Name: kopete
Summary: Kopete, The KDE Instant Messenger
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
# support USE_SYSTEM_IRIS build option
Patch0: %{name}-4.11.95-system_iris.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
Requires: %{name}-libs = %{version}-%{release}
# kopete/yahoo
Requires(hint): jasper
Requires: qca-ossl
Requires: mozilla-filesystem
## kppp
Requires: ppp
## krdc
BuildRequires: boost-devel >= 1.50.0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: desktop-file-utils
BuildRequires: freenx-client-devel >= 1.0
BuildRequires: giflib-devel
BuildRequires: iris-devel
BuildRequires: jasper-devel
BuildRequires: jsoncpp-devel
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kdepimlibs-devel >= %{version}
BuildRequires: kde-workspace-devel >= %{version}
BuildRequires: kde-baseapps-devel
BuildRequires: libgadu-devel >= 1.8.0
BuildRequires: libidn-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libktorrent-devel >= 1.3
BuildRequires: libmsn-devel >= 4.1
BuildRequires: libotr-devel
BuildRequires: libv4l-devel
BuildRequires: libvncserver-devel >= 0.9.9
BuildRequires: libxslt-devel
BuildRequires: libxml2-devel
BuildRequires: linphone-devel >= 3.4.3
BuildRequires: meanwhile-devel
BuildRequires: mozilla-filesystem
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openslp-devel
BuildRequires: ortp-devel >= 0.15.0
BuildRequires: pcre-devel
BuildRequires: qca2-devel
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: speex-devel
BuildRequires: sqlite-devel
BuildRequires: telepathy-qt4-devel >= 0.1.8
BuildRequires: webkitpart-devel >= 0.0.4
BuildRequires: openssl-devel >= 1.0.1c
BuildRequires: libmms-devel
BuildRequires: libsrtp-devel

Obsoletes: kdenetwork-kopete < %{version}-%{release}
Conflicts: kdenetwork < 4.10.90

%description
Kopete is an instant messenger supporting AIM, ICQ, Windows Live Messenger, Yahoo, Jabber,
Gadu-Gadu, Novell GroupWise Messenger, and more.
It is designed to be a flexible and extensible multi-protocol system suitable for personal and enterprise use.

%package cryptography
Summary: Encrypts and signs messages in Kopete using the OpenPGP
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}
Conflicts: %{name}-cryptography <= 1.3.0

%description cryptography
%{summary}.

%package libs
Summary: Runtime libraries for %{name}
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: kdenetwork-kopete-libs < %{version}-%{release}

%description libs
%{summary}.

%package devel
Summary: Development files for Kopete
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel
Obsoletes: kdenetwork-kopete-devel < %{version}

%description devel
%{summary}.

%prep
%setup -q

%patch0 -p1 -b .system_iris

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
export CXXFLAGS=-DGLIB_COMPILATION
%{cmake_kde4} \
	-DMOZPLUGIN_INSTALL_DIR:PATH=%{_libdir}/mozilla/plugins \
	-DUSE_SYSTEM_IRIS:BOOL=ON \
	-DWITH_JINGLE:BOOL=TRUE \
	../
popd

make -C %{_target_platform}/doc
make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  update-desktop-database -q &> /dev/null ||:
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING* HACKING IDENTITY_REFACTORY INSTALL
%doc KABC_INTEG_NOTES PORTING_KDE4 README TODO
%{_kde4_bindir}/kopete
%{_kde4_bindir}/googletalk-call
%{_kde4_bindir}/winpopup*
%{_kde4_bindir}/kopete_latexconvert.sh
%{_libdir}/mozilla/plugins/skypebuttons.so
%{_kde4_libdir}/kde4/kopete_*.so
%{_kde4_libdir}/kde4/kcm_kopete_*.so
%{_kde4_libdir}/libqgroupwise.so
%{_kde4_libdir}/kde4/libchattexteditpart.so
%{_kde4_appsdir}/kopete*
%{_kde4_appsdir}/kconf_update/kopete-*
%{_kde4_configdir}/kopeterc
%{_datadir}/dbus-1/interfaces/org.kde.Kopete.xml
%{_datadir}/dbus-1/interfaces/org.kde.kopete.*.xml
%{_kde4_datadir}/applications/kde4/kopete.desktop
%{_kde4_datadir}/config.kcfg/kopete*
%{_kde4_datadir}/sounds/Kopete_*
%{_kde4_datadir}/kde4/services/aim.protocol
%{_kde4_datadir}/kde4/services/callto.protocol
%{_kde4_datadir}/kde4/services/chatwindow.desktop
%{_kde4_datadir}/kde4/services/emailwindow.desktop
%{_kde4_datadir}/kde4/services/kopete_*.desktop
%{_kde4_datadir}/kde4/services/kconfiguredialog
%{_kde4_datadir}/kde4/services/skype.protocol
%{_kde4_datadir}/kde4/services/tel.protocol
%{_kde4_datadir}/kde4/services/xmpp.protocol
%{_kde4_datadir}/kde4/servicetypes/kopete*.desktop
%{_kde4_datadir}/config.kcfg/history2config.kcfg
%{_kde4_datadir}/config.kcfg/historyconfig.kcfg
%{_kde4_datadir}/config.kcfg/latexconfig.kcfg
%{_kde4_datadir}/config.kcfg/nowlisteningconfig.kcfg
%{_kde4_datadir}/config.kcfg/translatorconfig.kcfg
%{_kde4_datadir}/config.kcfg/urlpicpreview.kcfg
%{_kde4_datadir}/config.kcfg/webpresenceconfig.kcfg
%{_kde4_iconsdir}/hicolor/*/apps/kopete*.*
%{_kde4_iconsdir}/oxygen/*/actions/*
%{_kde4_datadir}/doc/HTML/en/kopete
## kopete-cryptography
%exclude %{_kde4_libdir}/kde4/kcm_kopete_cryptography.so
%exclude %{_kde4_libdir}/kde4/kopete_cryptography.so
%exclude %{_kde4_appsdir}/kopete_cryptography
%exclude %{_kde4_datadir}/kde4/services/kopete_cryptography.desktop
%exclude %{_kde4_datadir}/kde4/services/kconfiguredialog/kopete_cryptography_config.desktop

%files cryptography
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/kcm_kopete_cryptography.so
%{_kde4_libdir}/kde4/kopete_cryptography.so
%{_kde4_appsdir}/kopete_cryptography
%{_kde4_datadir}/kde4/services/kconfiguredialog/kopete_cryptography_config.desktop
%{_kde4_datadir}/kde4/services/kopete_cryptography.desktop

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/plugins/accessible
%{_kde4_libdir}/libkopete*.so.*
%{_kde4_libdir}/libkyahoo.so*
%{_kde4_libdir}/liboscar.so*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/kopete
%{_kde4_libdir}/libkopete*.so

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sun Apr 20 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.13.0-2m)
- Installed (but unpackaged) file(s) found:
- /usr/bin/googletalk-call

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-2m)
- add subpackage kopete-cryptography

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- initial build for Momonga Linux

