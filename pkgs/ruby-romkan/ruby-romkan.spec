%global momorel 14

%global rbname romkan
Summary: Ruby/Romkan --  a Romaji <-> Kana conversion library for Ruby
Name: ruby-%{rbname}

Version: 0.4
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://namazu.org/~satoru/ruby-romkan/

Source0: http://namazu.org/~satoru/ruby-romkan/%{name}-%{version}.tar.gz 
NoSource: 0

Patch0: ruby-romkan-ruby19-magic-comment.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby >= 1.9.2

Requires: ruby

%description
Ruby/Romkan is a Romaji <-> Kana conversion library for Ruby. It can
convert a Japanese Romaji string to a Japanese Kana string or vice
versa.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q

%patch0 -p1 -b .ruby19

%build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
mkdir -p %{buildroot}%{ruby_libdir}
install -m 644 romkan.rb %{buildroot}%{ruby_libdir}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(- ,root,root)
%doc ChangeLog *.rd test.*
%{ruby_libdir}/romkan.rb

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4-14m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-11m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-10m)
- add Magic Comment. need ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-5m)
- %%NoSource -> NoSource

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4-4m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4-3m)
- rebuild against ruby-1.8.0.

* Thu Feb 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.4-2k)

* Fri Jul 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.3-2k)
