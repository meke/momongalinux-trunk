%global momorel 1
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: gEdit is a small but powerful text editor for GNOME.
Name:      gedit
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
Requires(pre): GConf
Requires(pre): rarian
Requires(pre): desktop-file-utils
BuildRequires: rarian
BuildRequires: pkgconfig
BuildRequires: enchant-devel >= 1.4.2
BuildRequires: iso-codes
BuildRequires: libSM-devel
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gtksourceview2-devel >= 2.9.7
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: pygobject-devel >= 2.16.1
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: pygtksourceview-devel >= 2.6.0
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: libpeas-devel >= 1.1.0

Requires:    gnome-python2-gtksourceview
Obsoletes: gedit2

%description
gEdit is a small but powerful text editor designed specifically for
the GNOME GUI desktop.  gEdit includes a plug-in API (which supports
extensibility while keeping the core binary small), support for
editing multiple documents using notebook tabs, and standard text
editor functions.

You'll need to have GNOME and GTK+ installed to use gEdit.
#'

%package devel
Summary: gEdit include files for plugins.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtksourceview2-devel

%description devel
gEdit plugins include files.

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --disable-schemas-install \
    --disable-scrollkeeper \
    --disable-gtk-doc \
    --enable-attr \
    --enable-pthon=yes \
    --enable-introspection=yes \
    --enable-gvfs-metadata \
    --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
update-desktop-database -q 2> /dev/null || :
rarian-sk-update

%postun
update-desktop-database -q 2> /dev/null || :
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc README COPYING ChangeLog NEWS AUTHORS BUGS
%{_bindir}/%{name}
%{_bindir}/gnome-text-editor
%{_libdir}/%{name}
%{python_sitearch}/gi/overrides/Gedit.p*
%{_libexecdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/dbus-1/services/org.gnome.gedit.service

%{_mandir}/man1/%{name}.1.*
%doc %{_datadir}/help/*/gedit

%{_datadir}/GConf/gsettings/gedit.convert
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.externaltools.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.filebrowser.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.filebrowser.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.pythonconsole.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.time.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.time.gschema.xml


%files devel
%defattr(-, root, root)
%{_includedir}/gedit-3.0
#%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/gtk-doc/html/gedit

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- update to 3.5.2

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-3m)
- update Requires

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-2m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.6-1m)
- update to 3.2.6

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.6-1m)
- update to 3.1.6

* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.5-3m)
- ommit python site-package (what is happen?)

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.5-2m)
- add BuildRequires

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.6-1m)
- update to 3.0.6

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-2m)
- use python_sitearch

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.4-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.4-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.4-1m)
- update to 2.30.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-3m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-2m)
- hide warning %%post

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.30.2-2m)
- use BuildRequires

* Wed Apr 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Thu Apr  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.29.9-1m)
- [CRASH FIX and BUILD FIX] version 2.29.9

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.7-1m)
- update to 2.29.7

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6
--disable-gtk-doc

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.8-1m)
- update to 2.25.8

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.7-1m)
- update to 2.25.7

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.6-1m)
- update to 2.25.6
- delete patch0 (remove file)

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-3m)
- [SECURITY] CVE-2009-0314
- import a security patch (Patch0) from Fedora 10 (1:2.24.3-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-2m

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Mon Nov  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.22.3-2m)
- modify Require gnome-python2-desktop-gtksourceview

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.4-2m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.4-1m)
- update to 2.20.4

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Sep 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Jul  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.6-1m)
- update to 2.17.6

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-1m)
- update to 2.17.5 (unstable)

* Tue Dec 26 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.2-2m)
- rebuild against python-2.5

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update 2.16.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.4-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-1m)
- update 2.14.4

* Mon Jul 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- add Requires: gnome-python-desktop-gtksourceview

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.12.1-3m)
- rebuild against openssl-0.9.8a

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- version up
- GNOME 2.12.1 Desktop

* Sun Jul 31 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Sun Jul 31 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.3-1m)
- version 2.8.3
- add Patch0 (gedit-2.8.3-formatstring-vulnerability.patch)
- MomoVTS:00123
  Filename Format String Vulnerability

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Sat Dec 11 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.2-2m)
- BuildPreReq: libtool >= 1.5.10 ("so jya nai" problem)

* Sat Aug 21 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.2-1m)
- ver up
- remove Patch0

* Mon Aug  2 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.0-2m)
- fix the filename encoding bug in SaveAs dialog (gedit-2.6.0-saveas_dialog-filename.patch).

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.1-2m)
- revised spec for enabling rpm 4.2.

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Tue Jul 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Fri May 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-2m)
- create devel package.

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Thu Mar 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Thu Mar 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0.1-3m)
  rebuild against openssl 0.9.7a

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-2m)
- rebuild against for aspell

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.91-1m)
- version 2.1.91

* Wed Jan 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.6-1m)
- version 2.1.6

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2.1-1m)
- version 2.1.2.1

* Fri Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0.1-1m)
- version 2.1.0.1

* Fri Sep  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Thu Aug 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-4m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-3m)
- rebuild against for libgnomeprint-1.116.0
- rebuild against for libgnomeprintui-1.116.0

* Fri Jul 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-2m)
- rebuild against for eel-2.0.2
- rebuild against for nautilus-2.0.2
- rebuild against for gnome-utils-2.0.1

* Thu Jul 25 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-3m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- add BuildPrereq: eel-devel

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-14k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-12k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-10k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-8k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-6k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.199.0-2k)
- version 1.199.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-14k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-12k)
- rebuild against for libgnomeprint-1.115.0
- rebuild against for libgnomeprintui-1.115.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-10k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-8k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-6k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-4k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.1-2k)
- version 1.121.1

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.121.0-2k)
- version 1.121.0

* Wed May 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.120.0-2k)
- version 1.120.0

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.119.0-4k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Wed May 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.119.0-2k)
- version 1.119.0

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.118.0-8k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.118.0-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.118.0-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.118.0-2k)
- version 1.118.0

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.0-2k)
- version 1.117.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-2k)
- version 1.116.0

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-4k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-2k)
- version 1.115.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-14k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-12k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-10k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-8k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-4k)
- change schema handring

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-2k)
- version 1.114.0

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-8k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-6k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-4k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-2k)
- version 1.113.0

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-8k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-6k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-4k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-2k)
- version 1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-6k)
- rebuild against for bonobo-activation-0.9.4

* Fri Feb 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-4k)
- add build pre requires

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-2k)
- version 1.111.0

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.1-2k)
- version 1.110.1

* Fri Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.107.0)
- gnome2 env

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.9.7-4k)
- rebuild against gettext 0.10.40.

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.9.7-2k)
- version 0.9.7

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.6-12k)
- rebuild against for gnome-print-0.29

* Wed May 30 2001 Akira Higuchi <a@kondara.org>
- (10k)
- use gdk_fontset_load() by default (gedit-fontset.patch)

* Thu May 10 2001 Akira Higuchi <a@kondara.org>
- remove /usr/share/mime-info/gedit.keys

* Tue May 2 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gnome-print-0.25

* Thu Mar 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.9.6-5k)
- fix BuildPreReq

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- (0.9.6-3k)
- up to 0.9.6

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.9.4-5k)
- modified Docdir with macros

* Wed Nov 29 2000 Shingo Akagaki <dora@kondara.org>
- version 0.9.4

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- change source URL

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- just rebuild for new gnome-print

* Tue Sep 26 2000 Toru Hoshina <t@kondara.org>
- fixed spec file, add infodir= when make install.

* Wed Sep 20 2000 Shingo Akagaki <dora@kondara.org>
- version 0.9.1

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.6.1-3).

* Sun Feb 20 2000 Shingo Akagaki <dora@kondara.org>
- remove gedit.keys

* Fri Feb 11 2000 Jonathan Blandford <jrb@redhat.com>
- removed "reverse search function as it doesn't work.
#"

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man pages

* Mon Jan 17 2000 Elliot Lee <sopwith@redhat.com>
- If I don't put in a log entry here, people will be very upset about not
  being able to find out that I am to blame for the 0.6.1 upgrade

* Tue Nov 30 1999 Shingo Akagaki <dora@kondara.org>
- version 0.6.1

* Sun Nov 28 1999 Shingo Akagaki <dora@kondara.org>
- version 0.6.0

* Mon Aug 16 1999 Michael Fulbright <drmike@redhat.com>
- version 0.5.4

* Sat Feb 06 1999 Michael Johnson <johnsonm@redhat.com>
- Cleaned up a bit for Red Hat use

* Thu Oct 22 1998 Alex Roberts <bse@dial.pipex.com>
- First try at an RPM
