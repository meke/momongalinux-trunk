%global momorel 15

Summary: libdv - DV software codec
Name: libdv
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: GPL
URL: http://libdv.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: %{name}-0.104-audio.patch
Patch2: libdv-0.104-no-exec-stack.patch
Patch5: libdv-1.0.0-gtk2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel >= 1.2.11-2m, glib2-devel gtk2-devel
BuildRequires: automake >= 1.5, autoconf >= 2.50
Requires(post): libselinux-utils

%description 
The Quasar DV codec (libdv) is a software codec for DV video.  DV is
the encoding format used by most digital camcorders, typically those
that support the IEEE 1394 (aka FireWire or i.Link) interface.  libdv
was developed according to the official standards for DV video, IEC
61834 and SMPTE 314M.  See http://libdv.sourceforge.net/ for more.

This is the dynamic link libraries.

%package devel
Summary: static link libraries, includes and more from libdv
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The Quasar DV codec (libdv) is a software codec for DV video.  DV is
the encoding format used by most digital camcorders, typically those
that support the IEEE 1394 (aka FireWire or i.Link) interface.  libdv
was developed according to the official standards for DV video, IEC
61834 and SMPTE 314M.  See http://libdv.sourceforge.net/ for more.

This is the static link libraries, include files and other resources
you can use to incorporate libdv into applications.

%package apps
Summary: utility programs from libdv
Group: Applications/Multimedia
Requires: glib SDL libdv

%description apps
The Quasar DV codec (libdv) is a software codec for DV video.  DV is
the encoding format used by most digital camcorders, typically those
that support the IEEE 1394 (aka FireWire or i.Link) interface.  libdv
was developed according to the official standards for DV video, IEC
61834 and SMPTE 314M.  See http://libdv.sourceforge.net/ for more.

This is the utility programs.

%prep
%setup -q
%patch0 -p1 -b .audio~
%patch2 -p0 -b .no-exec-stack
%patch5 -p1 -b .gtk2

%build
autoreconf -fi
CFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--mandir=%{_mandir} \
	--infodir=%{_infodir} \
	--enable-sdl \
	--without-debug \
	LIBS="-lX11 -lXext"

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
if test -f %{_sbindir}/getenforce ; then
  result=$(%{_sbindir}/getenforce)
  if test "x${result}" != "xDisabled" ; then
    chcon -t textrel_shlib_t %{_libdir}/libdv.so.4.0.3
  fi
fi

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc ChangeLog COPYING README README.encoder AUTHORS NEWS INSTALL TODO COPYRIGHT
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/libdv
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/pkgconfig/libdv.pc

%files apps
%defattr(-,root,root)
%{_mandir}/man1/*
%{_bindir}/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-15m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-12m)
- add Requires(post): libselinux-utils

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-11m)
- full rebuild for mo7 release

* Thu Aug 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-10m)
- add chcon hack
- [BTS 283]

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-9m)
- fix build

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-6m)
- add autoreconf option (-fi)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- delete libtool library

* Tue Dec 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.104-2m)
- rebuild against expat-2.0.0-1m

* Mon May 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.104-1m)
- update to 0.104
- renumber patches
- import patches from FC (include enable gtk+2 patch)

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.103-3m)
- import libdv.patch from opensuse
 +* Wed Oct 26 2005 - rguenther@suse.de
 +- put some meaningful constraints on the asms in mmx.h to
 +  allow compiling with gcc 4.1 on i386.

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.103-2m)
- rebuild against SDL-1.2.7-11m

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.103-1m)
- version up

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (0.102-3m)
- enable x86_64.

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.102-2m)
- rebuild against DirectFB-0.9.21

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.102-1m)
- update to 0.102
- revise Patch1 (please check!)

* Thu Oct 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.99-3m)
- update to cvs 20031016 (now supports 4ch audio)
- revive audio api extention patch (Patch1)

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.99-2m)
  rebuild against DirectFB 0.9.18

* Sat Feb 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.99-1m)
  update to 0.99

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98-1m)
- version up to 0.98

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-9m)
- fix duplicated /usr/lib/libdv.so in libdv and libdv-devel

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-8m)
- move /usr/lib/libdv.so from libdv-devel to libdv 

* Wed Aug 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.5-7m)
- rebuild against SDL-1.2.4-3m

* Fri May 11 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.9.5-6k)
- fiexed Source0 path typo.

* Fri May 10 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9.5-4k)
- apply patch from cvs-20020510
- revise libdv-audio.patch

* Wed Apr 10 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9.5-2k)
- update to 0.9.5
- add NoSource statement(Source0)

* Thu Apr 04 2002 Toru Hoshina <t@kondara.org>
- (0.9-0.0200200404004k)
- revised alpha patch.

* Thu Apr 04 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9-0.0200200404002k)
- update to cvs-20020404
- remove Require: glib
- add %{_libdir}/pkgconfig/libdv.pc to %files devel

* Fri Mar 28 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9-0.020020316010k)
- remove glibless patch(Patch4)
- change required automake version(1.4p2 -> 1.5)

* Thu Mar 27 2002 Toru Hoshina <t@kondara.org>
- (0.9-0.020020316008k)
- add alpha support.

* Tue Mar 26 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9-0.020020316006k)
- add audio decode API (gint dv_decode_full_audio_to_pack())
- fix glibless.patch(Patch4)

* Fri Mar 16 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9-0.020020316004k)
- add glibless patch

* Fri Mar 16 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9-0.020020316002k)
- kondarize
- fix memory leak
- move apps to libdv-apps- sub package
- port specfile to Kondara(%changelog was empty in original specfile)
