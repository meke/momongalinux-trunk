%global momorel 1

Summary: Meta package for GNOME desktop.
Name: gnome
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

## CORE
Requires:GConf
Requires:NetworkManager
Requires:atk
Requires:atkmm
Requires:brasero
Requires:cantarell-fonts
Requires:caribou
Requires:clutter
Requires:clutter-gtk
Requires:cogl
Requires:dconf
Requires:empathy
Requires:eog
Requires:epiphany
Requires:evince
Requires:evolution-data-server
Requires:folks
Requires:gcalctool
Requires:gdk-pixbuf2
Requires:gdm
Requires:gjs
Requires:glib2
Requires:glib-networking
Requires:glibmm
Requires:gnome-backgrounds
Requires:gnome-bluetooth
Requires:gnome-contacts
Requires:gnome-control-center
Requires:gnome-desktop3
Requires:gnome-disk-utility
Requires:gnome-doc-utils
Requires:gnome-icon-theme
Requires:gnome-icon-theme-extras
Requires:gnome-icon-theme-symbolic
Requires:gnome-keyring
Requires:gnome-menus
Requires:gnome-online-accounts
Requires:gnome-packagekit
Requires:gnome-panel
Requires:gnome-power-manager
Requires:gnome-screensaver
Requires:gnome-session
Requires:gnome-settings-daemon
Requires:gnome-shell
Requires:gnome-system-monitor
Requires:gnome-terminal
Requires:gnome-themes-standard
Requires:gnome-user-docs
Requires:gnome-user-share
#Requires:gnome-utils
Requires:gnome-video-effects
Requires:gobject-introspection
Requires:gsettings-desktop-schemas
Requires:gstreamer-plugins-base
Requires:gstreamer-plugins-good
Requires:gstreamer
Requires:gtk3
Requires:gtk2
Requires:gtk-doc
Requires:gtkmm
Requires:gucharmap
Requires:gvfs
Requires:libgnome-keyring
Requires:libgnomekbd
Requires:libgtop2
Requires:libgweather
Requires:libpeas
Requires:librsvg2
Requires:libsoup
Requires:libwnck3
Requires:metacity
Requires:mm-common
Requires:gnome-tweak-tool
Requires:mousetweaks
Requires:mutter
Requires:nautilus
# (included NetworkManager)
# Requires:network-manager-applet
Requires:notification-daemon
Requires:pango
Requires:pangomm
Requires:polkit-gnome
Requires:sushi
Requires:totem-pl-parser
Requires:vino
Requires:vte3
Requires:yelp
Requires:yelp-tools
Requires:yelp-xsl
Requires:zenity

## APPS
Requires:accerciser
Requires:aisleriot
Requires:anjuta
Requires:cheese
Requires:devhelp
Requires:evolution
Requires:file-roller
Requires:gedit
Requires:glade
Requires:gnome-color-manager
Requires:gnome-devel-docs
Requires:gnome-documents
Requires:gnome-games
Requires:gnome-nettool
Requires:hamster-applet
# (I do not need this)
# Requires:nautilus-sendto
Requires:nemiver
Requires:orca
Requires:rygel
Requires:seahorse
Requires:totem
Requires:vinagre

# Not listed in GNOME 3.1.92
#Requires:ekiga
#Requires:tomboy

%description
This is meta package for GNOME desktop. But Core and Apps Only.

%files

%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Mon Aug 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.30.1-2m)
- change Requires: libgdl

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
- good-bye eel2

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.91

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Fri Jan 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-3m)
- add %%files (for build)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Thu Nov 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Sun Oct 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-2m)
- fix spec

* Fri Oct 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Aug 27 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.3-3m)
- add gnome-keyring-pam

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.3-2m)
- change Requires: gst-plugins-base to gstreamer-plugins-base
- change Requires: gst-plugins-good to gstreamer-plugins-good

* Mon Jul 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3
- wasurete ita

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Fri Jan 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Sun Jun 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2 :-)

* Sun May  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- GNOME 2.16.1
-- epiphany and yelp was GNOME 2.14.3

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- GNOME 2.16.0

* Sat Aug 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- GNOME 2.14.3

* Sun Jun 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- GNOME 2.14.2
-- add require gtk+-common

* Sun May 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- GNOME 2.14.1

* Wed Jun 16 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-3m)
- add require gst-plugins-additional

* Sat Jun  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.0-2m)
- remove require openoffice.org

* Mon May 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- initial version
