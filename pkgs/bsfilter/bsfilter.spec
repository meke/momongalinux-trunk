%global momorel 5
%global reldir 39712
%global rcver 4
%global prever 4

Summary: a filter which ditinguishes spam and non-spam mail, written in Ruby
Name: bsfilter
Version: 1.0.17
Release: %{?prever:0.%{prever}.}%{momorel}m%{?dist}
Source0: http://osdn.dl.sourceforge.jp/bsfilter/%{reldir}/bsfilter-%{version}%{?rcver:.rc%{rcver}}.tgz 
NoSource: 0
Patch0: %{name}-%{version}.rc%{rcver}-ruby18.patch
License: GPL
URL: http://bsfilter.org/
Group: Applications/Internet

Requires: ruby18

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%prep
%setup -q -n %{name}-%{version}%{?rcver:.rc%{rcver}}
%patch0 -p1

%build

%install
[ %{buildroot} != "/" ] && %__rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
install -m 0755 bsfilter/bsfilter %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_datadir}/config-sample/bsfilter/mua/mew3
mkdir -p %{buildroot}%{_datadir}/config-sample/bsfilter/mua/mew4
mkdir -p %{buildroot}%{_datadir}/config-sample/bsfilter/mua/mew5
mkdir -p %{buildroot}%{_datadir}/config-sample/bsfilter/mda/maildrop
mkdir -p %{buildroot}%{_datadir}/config-sample/bsfilter/mda/procmail

install -m 0644 mua/mew3/* %{buildroot}%{_datadir}/config-sample/bsfilter/mua/mew3
install -m 0644 mua/mew4/* %{buildroot}%{_datadir}/config-sample/bsfilter/mua/mew4
install -m 0644 mua/mew5/* %{buildroot}%{_datadir}/config-sample/bsfilter/mua/mew5
install -m 0644 mda/maildrop/mailfilter.header  %{buildroot}%{_datadir}/config-sample/bsfilter/mda/maildrop
install -m 0644 mda/procmail/* %{buildroot}%{_datadir}/config-sample/bsfilter/mda/procmail

%clean
[ %{buildroot} != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING htdocs/*
%{_bindir}/bsfilter
%{_datadir}/config-sample/bsfilter

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.17-0.4.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.17-0.4.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.17-0.4.3m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.17-0.4.2m)
- use ruby18

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.17-0.4.1m)
- update to 1.0.17rc4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.16-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.16-2m)
- rebuild against rpm-4.6

* Sun Jul  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.16-1m)
- version up 1.0.16

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.15-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.15-2m)
- %%NoSource -> NoSource

* Fri Sep 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.15-1m)
- update to 1.0.15

* Sat Feb 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.10-2m)
- patch to config-sample/bsfilter/mua/mew.el for Mew of newer than 4.2.52
  (http://sourceforge.jp/forum/forum.php?thread_id=9051&forum_id=4204)

* Mon Dec 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.10-1m)
- bugfixes

* Tue Jul 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.9-1m)
- up to 1.0.9

* Mon Jun 20 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.7-1m)
- minor version up

* Mon Aug 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-1m)
- minor version up

* Wed Aug 11 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.3-1m)
- initial import

%description
a filter which distinguishes spam and non-spam(called "clean" in this page) mail
support mails written in japanese language
written in Ruby
support 3 methods for access
 * traditional unix-style filter. study and judge local files or pipe
 * IMAP. study and judge mails in an IMAP server
 * POP proxy. run between POP server and MUA
basic concepts come from A Plan for Spam, Better Bayesian Filtering, Spam Detection
distributed under GPL
