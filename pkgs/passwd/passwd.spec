%global momorel 3

%if %{?WITH_SELINUX:0}%{!?WITH_SELINUX:1}
%define WITH_SELINUX 1
%endif
%if %{?WITH_AUDIT:0}%{!?WITH_AUDIT:1}
%define WITH_AUDIT 1
%endif

Summary: An utility for setting or changing passwords using PAM
Name: passwd
Version: 0.78
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Base
URL: http://fedorahosted.org/passwd
Source: https://fedorahosted.org/releases/p/a/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pam >= 1.1.1
%if %{WITH_SELINUX}
BuildRequires: libselinux-devel
%endif
BuildRequires: glib2-devel, libuser-devel, pam-devel, libuser >= 0.53-1
BuildRequires: gettext, popt-devel
%if %{WITH_AUDIT}
BuildRequires: audit-libs-devel >= 2.0.4
Requires: audit-libs >= 2.0.4
%endif

%description
This package contains a system utility (passwd) which sets
or changes passwords, using PAM (Pluggable Authentication
Modules) library.

%prep
%setup -q -n %{name}-%{version}

%build
%configure \
%if %{WITH_SELINUX}
        --with-selinux \
%else
        --without-selinux \
%endif
%if %{WITH_AUDIT}
        --with-audit
%else
        --without-audit
%endif
make DEBUG= RPM_OPT_FLAGS="%{optflags}"

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} bindir=%{_bindir} mandir=%{_mandir}
install -m 755 -d %{buildroot}%{_sysconfdir}/pam.d/
install -m 644 passwd.pamd %{buildroot}%{_sysconfdir}/pam.d/passwd
%find_lang %{name}
for dir in $(ls -1d %{buildroot}%{_mandir}/{??,??_??}) ; do
    dir=$(echo $dir | sed -e "s|^%{buildroot}||")
    lang=$(basename $dir)
    echo "%%lang($lang) $dir/man*/*" >> %{name}.lang
done

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING AUTHORS ChangeLog
%config(noreplace) %{_sysconfdir}/pam.d/passwd
%attr(4755,root,root) %{_bindir}/passwd
%{_mandir}/man1/passwd.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.78-3m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.78-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.78-1m)
- update to 0.78

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.77-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.77-2m)
- full rebuild for mo7 release

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.77-1m)
- update 0.77

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.76-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.76-1m)
- sync with Fedora 11 (0.76-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.75-1m)
- sync with Fedora devel (0.75-2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.74-3m)
- rebuild against gcc43

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.74-2m)
- modify Requires

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.74-1m)
- rebuild against pam-0.99.7-1m
- update to 0.74

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (0.68-2m)
- revised spec for rpm 4.2.

* Tue Aug  5 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.68-1m)
- update to 0.68
- use %%{momorel}, %%make, %%makeinstall
- remove Requires: pwdb >= 0.58
- Source from RawHide (0.68-4)
- add %%doc

* Sat Nov 16 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.67-3m)
- add BuildPrereq glib-devel, libuser-devel, pam-devel

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (0.67-2k)
- ver up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.64.1-8k)

* Fri Jul  6 2001 Toru Hoshina <toru@df-usa.com>
- (0.64.1-7k)
- revised passwd.pamd.

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.64.1-1).

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- fix manpage links

* Fri Feb 04 2000 Nalin Dahyabhai <nalin@redhat.com>
- document --stdin in man page
- fix for gzipped man pages

* Sun Jan 16 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Apr 10 1999 Cristian Gafton <gafton@redhat.com>
- first build from the new source code base.
