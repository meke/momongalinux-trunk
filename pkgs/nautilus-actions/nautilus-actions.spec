%global momorel 1

Summary: Nautilus extension for customizing the context menu
Name:    nautilus-actions
Version: 3.2.2
Release: %{momorel}m%{?dist}
Group:   User Interface/Desktops
License: GPL
URL:     http://www.grumz.net/node/8
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeui-devel, libglade-devel, nautilus
BuildRequires: gtk2-devel
BuildRequires: nautilus-devel >= 3.5.90
BuildRequires: e2fsprogs-devel
BuildRequires: desktop-file-utils
Requires(pre): GConf
Requires(preun): GConf

%description
Nautilus actions is an extension for Nautilus, the GNOME file manager.
It provides an easy way to configure programs to be launch on files 
selected in Nautilus interface

%package devel
Summary: Development tools for the nautilus-actions
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains headers and shared libraries needed for development
with nautilus-actions.

%package docs
Summary:        Documentations for %{name}
Group:          Documentation
BuildArch:      noarch

%description docs
This package contains the documentation for %{name}


%prep
%setup -q

%build
%configure --disable-schemas-install --program-prefix="" --with-gtk=3 --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name}

rm -rf %{buildroot}%{_datadir}/applications/fedora-nact.desktop

desktop-file-install --vendor=""  --delete-original  \
    --dir %{buildroot}%{_datadir}/applications        \
    --add-only-show-in GNOME \
    --remove-category AdvancedSettings \
    --remove-category Application \
    %{buildroot}%{_datadir}/applications/nact.desktop

%clean
rm -rf --preserve-root %{buildroot}

%pre
if [ -s %{_sysconfdir}/gconf/schemas/config_newaction.schemas ]; then 
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-install-rule \
      %{_sysconfdir}/gconf/schemas/config_newaction.schemas >/dev/null || :
    killall -HUP gconfd-2 || :
fi
fi

%preun
if [ -s %{_sysconfdir}/gconf/schemas/config_newaction.schemas ]; then
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/config_newaction.schemas > /dev/null || :
    gconftool-2 --recursive-unset /apps/nautilus-actions
    gconftool-2 --recursive-unset /schemas/apps/nautilus-actions
    killall -HUP gconfd-2 || :
fi
fi

%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_bindir}/nautilus-actions-config-tool
%{_bindir}/nautilus-actions-new
%{_bindir}/nautilus-actions-run
%{_bindir}/nautilus-actions-print

%{_libdir}/%{name}/
%{_libexecdir}/%{name}/
%{_libdir}/nautilus/extensions-?.0/libnautilus-actions-menu.so
%{_libdir}/nautilus/extensions-?.0/libnautilus-actions-menu.la
%{_libdir}/nautilus/extensions-?.0/libnautilus-actions-tracker.so
%{_libdir}/nautilus/extensions-?.0/libnautilus-actions-tracker.la

%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/apps/nautilus-actions.*
%{_datadir}/applications/nact.desktop

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}

%files docs
%defattr(-,root,root,-)
%{_datadir}/gnome/help/%{name}-config-tool/
%{_datadir}/gtk-doc/html/%{name}-3/
%{_datadir}/omf/%{name}-config-tool/


%changelog
* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for nautilus-3.5.90

* Thu Apr  5 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Tue May 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-5m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-3m)
- rebuild against gcc43

* Wed Mar 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-2m)
- rebuild against nautilus-2.22.0

* Sat Jan  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-3m)
- remove category AdvancedSettings Application

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-1m)
- update 1.4

* Fri May 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-2m)
- show only GNOME menu

* Thu May  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- import from http://www.grumz.net/?q=taxonomy/term/6/9

* Mon May 01 2006 Deji Akingunola <dakingun@gmail.com> - 1.2-1
- Update to version 1.2
- Make sure fix for upgrade from ver. < 1.0 doesn't throw up errors
#'

* Mon Feb 13 2006 Deji Akingunola <dakingun@gmail.com> - 1.0-2
- Rebuild for Fedora Extras 5

* Wed Feb 8 2006 Deji Akingunola <dakingun@gmail.com> - 1.0-1
- New upstream version
- Do away with gconf schemas installation

* Mon Jan 2 2006 Deji Akingunola <dakingun@gmail.com> - 0.99-6
- Add nautilus extensions dir to configure arguments

* Sun Jan 1 2006 Deji Akingunola <dakingun@gmail.com> - 0.99-5
- Remove libXdmcp-devel as BR, nautilus now takkes care of it

* Sun Dec 18 2005 Deji Akingunola <dakingun@gmail.com> - 0.99-4
- Remove libSM-devel from build require as libgnomeui now build-requires
- Add libXdmcp-devel to the build requires

* Tue Nov 29 2005 Deji Akingunola <dakingun@gmail.com> - 0.99-2
- Remove unnecesary configure options
- Clean-up the desktop file installation
- Add e2fsprogs-devel buildrequire and remove explicit require on nautilus

* Fri Nov 25 2005 Deji Akingunola <dakingun@gmail.com> - 0.99-1
- initial Extras release
