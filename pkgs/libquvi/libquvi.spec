%global momorel 1
Name:           libquvi
Version:        0.4.1 
Release: %{momorel}m%{?dist}
Summary:        A cross-platform library for parsing flash media stream

Group:          Applications/Internet
License:        LGPLv2+
URL:            http://quvi.sourceforge.net/
Source0:        http://downloads.sourceforge.net/quvi/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires:  libquvi-scripts libcurl-devel lua-devel
Requires:       libquvi-scripts

%description
Libquvi is a cross-platform library for parsing flash media stream
URLs with C API.

%package devel
Summary: Files needed for building applications with libquvi
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: quvi-devel <= 0.2.19
Provides:  quvi-devel = %{version}-%{release}

%description devel
Files needed for building applications with libquvi


%prep
%setup -q

%build
%configure --enable-static=no

%make 

%install
make install DESTDIR=%{buildroot}
rm -f ${RPM_BUILD_ROOT}%{_libdir}/%{name}.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc ChangeLog COPYING README 
%{_libdir}/%{name}.so.*
%{_mandir}/man3/%{name}.3.*

%files devel
%doc examples/simple.c 
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/quvi/

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-1m)
- import from fedora

