%global momorel 2
Name:           gmime
Version:        2.6.10
Release: %{momorel}m%{?dist}
Summary:        Library for creating and parsing MIME messages

Group:          System Environment/Libraries
# Files in examples/, src/ and tests/ are GPLv2+
License:        LGPLv2+ and GPLv2+
URL:            http://spruce.sourceforge.net/gmime/
Source0:        http://download.gnome.org/sources/gmime/2.6/gmime-%{version}.tar.xz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= 2.18.0
BuildRequires:  gpgme-devel
BuildRequires:  libgpg-error-devel
BuildRequires:  zlib-devel >= 1.2.1.1
BuildRequires:  gettext-devel, gtk-doc
BuildRequires:  automake autoconf

# mono available only on selected architectures
%ifarch %ix86 x86_64 ia64 armv4l sparcv9 alpha s390x ppc ppc64
%define buildmono 1
%else
%define buildmono 0
%endif
%if 0%{?rhel} >= 6
%define buildmono 0
%endif

%if 0%buildmono
BuildRequires:  mono-devel gtk-sharp2-gapi
BuildRequires:  gtk-sharp2-devel >= 2.4.0
%endif

%description
The GMime suite provides a core library and set of utilities which may be
used for the creation and parsing of messages using the Multipurpose
Internet Mail Extension (MIME).


%package        devel
Summary:        Header files to develop libgmime applications
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       glib2-devel

%description    devel
The GMime suite provides a core library and set of utilities which may be
used for the creation and parsing of messages using the Multipurpose
Internet Mail Extension (MIME). The devel-package contains header files
to develop applications that use libgmime.

%if 0%buildmono
%package        sharp
Summary:        Mono bindings for gmime
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk-sharp2
Obsoletes:	gmime-sharp-devel

%description    sharp
The GMime suite provides a core library and set of utilities which may be
used for the creation and parsing of messages using the Multipurpose
Internet Mail Extension (MIME). The devel-package contains support 
for developing mono applications that use libgmime.
%endif

%prep
%setup -q

%build
autoreconf
MONO_ARGS="--enable-mono=no"
%if 0%buildmono
export MONO_SHARED_DIR=%{_builddir}/%{?buildsubdir}
MONO_ARGS="--enable-mono"
%endif
# Don't conflict with sharutils.
%configure $MONO_ARGS --program-prefix=%{name} --disable-static

# Deal with rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

%make 

%install
rm -rf %{buildroot}
export MONO_SHARED_DIR=%{_builddir}/%{?buildsubdir}
make install DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README TODO
#%{_bindir}/gmime*
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/gmime-2.6.pc
%{_includedir}/gmime-2.6
%{_datadir}/gtk-doc/html/gmime-2.6

%if 0%buildmono
%files sharp
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/gmime-sharp-2.6.pc
%{_prefix}/lib/mono/gac/gmime-sharp
%{_prefix}/lib/mono/gmime-sharp-2.6
%{_datadir}/gapi-2.0/gmime-api.xml
%endif

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.10-2m)
- fix build on x86_64

* Fri Sep  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.10-1m)
- update to 2.6.10

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-3m)
- rebuild for mono-2.10.9

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-2m)
- revise Obsoletes

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.25-2m)
- rebuild for glib 2.33.2

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.25-1m)
- update to 2.4.25

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.24-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.24-1m)
- update to 2.4.24

* Tue Mar  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.23-1m)
- update to 2.4.23

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.22-1m)
- update to 2.4.22

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.21-1m)
- update to 2.4.21

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.20-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.20-1m)
- update to 2.4.20

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.19-2m)
- rebuild against mono-2.8

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.19-1m)
- update to 2.4.19

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.17-2m)
- full rebuild for mo7 release

* Thu May 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.17-1m)
- update to 2.4.17

* Sun May 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.16-1m)
- update to 2.4.16

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.15-1m)
- [SECURITY] CVE-2010-0409
- update to 2.4.15

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.14-1m)
- update to 2.4.14

* Wed Jan 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.13-1m)
- update to 2.4.13

* Wed Jan 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.12-1m)
- update to 2.4.12

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.11-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11

* Mon Oct 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.10-1m)
- update to 2.4.10

* Fri Sep  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.9-1m)
- update to 2.4.9

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-2m)
- now we do not need lib64 patch

* Fri Aug 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.8-1m)
- update to 2.4.8
- add sharp-devel package

* Sat Aug 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.24-1m)
- update to 2.2.24

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.23-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.23-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.23-2m)
- back to 2.2.23

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Mon Sep 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.23-1m)
- update to 2.2.23

* Mon Jul 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.22-1m)
- update to 2.2.22

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.21-1m)
- update to 2.2.21

* Sun May 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.20-1m)
- update to 2.2.20

* Mon May  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.19-1m)
- update to 2.2.19

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.18-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.18-1m)
- update to 2.2.18

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.17-2m)
- rebuild against gtk-sharp2-2.12.0

* Mon Feb 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.17-1m)
- update to 2.2.17

* Sun Feb  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.16-1m)
- update to 2.2.16

* Thu Jan  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.15-1m)
- update to 2.2.15

* Sun Dec 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.12-1m)
- update to 2.2.12

* Fri Dec 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Fri Jul 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Tue May 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.9-1m)
- update to 2.2.9

* Tue May 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-5m)
- fix BuildPrereq

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.1-4m)
- disable mono for ppc64, alpha, sparc64

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-4m)
- rebuild against mono-1.1.17.1 gtk-sharp 2.10.0

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.1-3m)
- revised for multilibarch

* Tue May  9 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.2.1-2m)
- add lib64 patch
- revised %files section in spec file

* Sat May  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.1-1m)
- start.
