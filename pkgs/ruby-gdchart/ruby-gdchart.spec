Summary: Ruby gdchart
Name: ruby-gdchart

%global momorel 18

Version: 0.0.9
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Ruby and "GDChart"
URL: http://sourceforge.jp/projects/ruby-gdchart/

Source0: http://osdn.dl.sourceforge.jp/ruby-gdchart/1080/ruby-gdchart-0.0.9-beta.tar.gz 
NoSource: 0
Patch0: ruby-gdchart-0.0.9-beta-cflags.patch
Patch1: ruby-gdchart-0.0.9-beta-gcc4.patch
Patch2: ruby-gdchart-0.0.9-beta-ruby19.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: freetype-devel
BuildRequires: gd-devel
BuildRequires: libpng-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: zlib-devel

%description
This is an extension library to use the GDCHART C library written by
Bruce Verderaime (http://www.fred.net/brv/chart/).  The extension
provides a the module 'GDChart'.

%prep
%setup -q -n %{name}-%{version}-beta
%patch0 -p1 -b .cflags~
%patch1 -p1 -b .gcc4
%patch2 -p1 -b .ruby19

cp gdchart0.11.2dev/README gdchart0.11.2dev/README.gdchart

%build
CFLAGS="%{optflags}" ruby extconf.rb
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog README.en gdchart0.11.2dev/README.gdchart
%{ruby_sitearchdir}/GDChart.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.9-16m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.9-15m)
- rebuild against ruby-1.9.2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-14m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.9-12m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.9-10m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.9-9m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.9-8m)
- rebuild against ruby-1.8.6-4m

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.9-7m)
- BuildRequires: freetype2-devel -> freetype-devel

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.9-6m)
- add gcc4 patch
- Patch1: ruby-gdchart-0.0.9-beta-gcc4.patch

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.0.9-5m)
- /usr/lib/ruby

* Tue Feb 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.0.9-4m)
- rebuild without freetype 1.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.0.9-3m)
- enable x86_64.

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.9-2m)
- rebuild against ruby-1.8.2

* Wed May 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.0.9-1m)
- import to momonga

