%global momorel 9

Summary: EFI Boot Manager
Name: efibootmgr
Version: 0.5.4
Release: %{momorel}m%{?dist}
Group: System Environment/Base
License: GPLv2+
URL: http://linux.dell.com/%{name}/
BuildRequires: pciutils-devel, zlib-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# EFI/UEFI don't exist on PPC
ExclusiveArch: %{ix86} x86_64 ia64

# for RHEL / Fedora when efibootmgr was part of the elilo package
Conflicts: elilo <= 3.6-5
Obsoletes: elilo <= 3.6-5

Source0: http://linux.dell.com/%{name}/permalink/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: efibootmgr-0.5.4-default-to-grub-momonga.patch
Patch1: efibootmgr-0.5.4-support-4k-sectors.patch

%description
%{name} displays and allows the user to edit the Intel Extensible
Firmware Interface (EFI) Boot Manager variables.  Additional
information about EFI can be found at
http://developer.intel.com/technology/efi/efi.htm and http://uefi.org/.

%prep
%setup -q

%patch0 -p1
%patch1 -p1

%build
make %{?_smp_mflags} EXTRA_CFLAGS='%{optflags}'

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir} %{buildroot}%{_mandir}/man8
install -p --mode 755 src/%{name}/%{name} %{buildroot}%{_sbindir}
install -p --mode 644 src/man/man8/%{name}.8 %{buildroot}%{_mandir}/man8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_sbindir}/%{name}
%{_mandir}/man8/%{name}.8*
%doc README INSTALL COPYING
    
%changelog
* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-9m)
- Make \EFI\momonga\grub.efi the default bootloader

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-4m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-3m)
- rebuild against rpm-4.6

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-2m)
- modify ExclusiveArch

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.4-1m)
- import from Fedora

* Thu Apr 03 2008 Peter Jones <pjones@redhat.com> - 0.5.4-4
- Revert changes in -3, they weren't finalized and we don't need
  the feature at this time.

* Thu Mar 06 2008 Peter Jones <pjones@redhat.com> - 0.5.4-3
- Add support for setting driver related variables.

* Tue Feb  5 2008 Matt Domsch <Matt_Domsch@dell.com> 0.5.4-2
- rebuild with conflicts/obsoletes matching elilo

* Thu Jan  3 2008 Matt Domsch <Matt_Domsch@dell.com> 0.5.4-1
- split efibootmgr into its own RPM for Fedora/RHEL.

* Thu Aug 24 2004 Matt Domsch <Matt_Domsch@dell.com>
- new home linux.dell.com

* Fri May 18 2001 Matt Domsch <Matt_Domsch@dell.com>
- See doc/ChangeLog
