%global momorel 2

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global with_python3 1

Name:           python-mglob
Version:        0.4
Release:        %{momorel}m%{?dist}
Summary:        Enhanced file name globbing module

Group:          Development/Libraries
License:        MIT
URL:            http://pypi.python.org/pypi/mglob
Source0:        http://pypi.python.org/packages/source/m/mglob/mglob-%{version}.zip
NoSource:       0
# sent upstream via mail
Patch0:         mglob-0.4-built-in-set.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools

%if 0%{?with_python3}
BuildRequires:  python3-tools
BuildRequires:  python3-devel >= 3.4
BuildRequires:  python3-setuptools
%endif

%description
Usable as stand-alone utility (for xargs, backticks etc.), or as a globbing
library for own python programs.
Some enhanced features are recursion, exclusion, and directory omission.

%if 0%{?with_python3}
%package -n python3-mglob
Summary:        Enhanced file name globbing module
%description -n python3-mglob
Usable as stand-alone utility (for xargs, backticks etc.), or as a globbing
library for own python programs.
Some enhanced features are recursion, exclusion, and directory omission.
%endif

%prep
%setup -q -n mglob-%{version}
%patch0
sed -i -e '/^#!\//, 1d' mglob.py

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'

2to3 --write --nobackups %{py3dir}
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python}|'


%build
%if 0%{?with_python3}
pushd %{py3dir}
    %{__python3} setup.py build
popd
%endif # with_python3

%{__python} setup.py build


%install
rm -rf %{buildroot}
# Must do the python3 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python2 version
# to be the default for now).
%if 0%{?with_python3}
pushd %{py3dir}
    %{__python3} setup.py install -O1 --skip-build --root %{buildroot}
    mv %{buildroot}%{_bindir}/mglob %{buildroot}%{_bindir}/python3-mglob
popd
%endif # with_python3

%{__python} setup.py install -O1 --skip-build --root %{buildroot}

 
%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
# upstream has no docs
%doc
%{_bindir}/mglob
%{python_sitelib}/mglob.py*
%{python_sitelib}/mglob-%{version}-py?.?.egg-info

%if 0%{with_python3}
%files -n python3-mglob
%defattr(-,root,root,-)
# upstream has no docs
%doc
%{_bindir}/python3-mglob
%{python3_sitelib}/__pycache__/mglob.cpython*
%{python3_sitelib}/mglob.py*
%{python3_sitelib}/mglob-%{version}-py?.?.egg-info
%endif # with_python3

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-2m)
- rebuild against python-3.4

* Tue Sep 18 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.4-1m)
- imported to Momonga 8 from fc18 for ipython

* Sat Aug 04 2012 David Malcolm <dmalcolm@redhat.com>
- rebuild for https://fedoraproject.org/wiki/Features/Python_3.3

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 27 2012 Thomas Spura <tomspur@fedoraproject.org> - 0.4-5
- build python3 on f16 and up

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Jul 30 2010 Thomas Spura <tomspur@fedoraproject.org> - 0.4-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Sat Jun 19 2010 Thomas Spura <tomspur@fedoraproject.org - 0.4-1
- initial packaging
