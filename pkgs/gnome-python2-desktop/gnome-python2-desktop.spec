%global momorel 15
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

# Last updated for version 2.29.1
# The order here corresponds to that in configure.ac,
# for easier comparison.  Please do not alphabetize.
%define pygtk_version                   2.10.3
%define glib_version                    2.6.0
%define gtk_version                     2.4.0
%define gnome_python_version            2.10.0
%define gnome_panel_version             2.13.4
# Version not necessary for libgnomeprintui
%define gtksourceview_version           1.8.5
%define libwnck_version                 2.19.3
%define libgtop_version                 2.13.0
# Version not necessary for libnautilus_burn
%define brasero_version                 2.29
%define gnome_media_version             2.10.0
%define gconf2_version                  2.10.0
%define metacity_version                2.21.5
%define librsvg2_version                2.13.93
%define gnome_keyring_version           0.5.0
%define gnome_desktop_version           2.10.0
%define totem_version                   1.4.0
%define eds_version                     1.4.0
%define evince_version                  2.29

### Abstract ###

Name: gnome-python2-desktop
Version: 2.32.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Languages
Summary: The sources for additional PyGNOME Python extension modules
#VCS: git://git.gnome.org/gnome-python-desktop
Source: http://download.gnome.org/sources/gnome-python-desktop/2.32/gnome-python-desktop-%{version}.tar.bz2
NoSource: 0

# https://bugzilla.gnome.org/show_bug.cgi?id=616306
Patch0: gnome-python-desktop-2.30.0-wnck-flagsfix.patch

# https://bugzilla.gnome.org/show_bug.cgi?id=672016
Patch1: gnome-python-desktop-2.32.0-metacity-build.patch

### Dependencies ###

Requires: gnome-python2-canvas >= %{gnome_python_version}

### Build Dependencies ###

#BuildRequires: brasero-devel >= %{brasero_version}
#BuildRequires: evince-devel >= %{evince_version}
BuildRequires: evolution-data-server-devel >= %{eds_version}
BuildRequires: glib2-devel >= %{glib_version}
BuildRequires: GConf2-devel >= %{gconf2_version}
BuildRequires: gnome-desktop-devel >= %{gnome_desktop_version}
BuildRequires: gnome-keyring-devel >= %{gnome_keyring_version}
BuildRequires: gnome-panel-devel >= %{gnome_panel_version}
BuildRequires: gnome-python2-bonobo >= %{gnome_python_version}
BuildRequires: gnome-python2-canvas >= %{gnome_python_version}
BuildRequires: gnome-python2-devel >= %{gnome_python_version}
BuildRequires: gnome-python2-gconf >= %{gnome_python_version}
BuildRequires: gtk2-devel >= %{gtk_version}
BuildRequires: gtksourceview-devel >= %{gtksourceview_version}
BuildRequires: libgnomeui-devel
BuildRequires: libgnomeprintui22-devel
BuildRequires: libgtop2-devel >= %{libgtop_version}
BuildRequires: librsvg2-devel >= %{librsvg2_version}
BuildRequires: libwnck-devel >= %{libwnck_version}
BuildRequires: metacity-devel >= %{metacity_version}
BuildRequires: pygtk2-devel >= %{pygtk_version}
BuildRequires: python-devel
BuildRequires: totem-pl-parser-devel >= %{totem_version}
BuildRequires: autoconf, automake, libtool
%ifnarch s390 s390x
#BuildRequires: gnome-media-devel >= %{gnome_media_version}
%endif

Obsoletes: gnome-python2-bugbuddy

%description
The gnome-python-desktop package contains the source packages for additional 
Python bindings for GNOME. It should be used together with gnome-python.

#package -n gnome-python2-brasero
#Summary: Python bindings for interacting with brasero
#License: LGPLv2
#Group: Development/Languages
#Requires: %{name} = %{version}-%{release}
#Requires: brasero-libs >= %{brasero_version}

#description -n gnome-python2-brasero
#This module contains a wrapper that allows the use of brasero via Python.

#package -n gnome-python2-evince
#Summary: Python bindings for interacting with evince
#License: LGPLv2
#Group: Development/Languages
#Requires: %{name} = %{version}-%{release}
#Requires: evince-libs >= %{evince_version}

#description -n gnome-python2-evince
#This module contains a wrapper that allows the use of evince via Python.

%package -n gnome-python2-evolution
Summary: Python bindings for interacting with evolution-data-server
License: LGPLv2
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: evolution-data-server >= %{eds_version}
Provides: evolution-python = %{version}-%{release}
Obsoletes: evolution-python <= 0.0.4-3

%description -n gnome-python2-evolution
This module contains a wrapper that allows the use of evolution-data-server
via Python.

%package -n gnome-python2-gnomeprint
Summary: Python bindings for interacting with libgnomeprint
License: LGPLv2
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgnomeprint22
Requires: libgnomeprintui22
Requires: gnome-python2-canvas

%description -n gnome-python2-gnomeprint
This module contains a wrapper that allows the use of libgnomeprint via
Python.

%package -n gnome-python2-gtksourceview
Summary: Python bindings for interacting with the gtksourceview library 
License: GPLv2+
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gtksourceview >= %{gtksourceview_version}
Requires: gnome-python2-gnomeprint

%description -n gnome-python2-gtksourceview
This module contains a wrapper that allows the use of gtksourceview via
Python.

%package -n gnome-python2-libwnck
Summary: Python bindings for interacting with libwnck
License: LGPLv2
Group: Development/Languages
Requires: libwnck >= %{libwnck_version}

%description -n gnome-python2-libwnck
This module contains a wrapper that allows the use of libwnck via
Python.

%package -n gnome-python2-libgtop2
Summary: Python bindings for interacting with libgtop
License: GPLv2+
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgtop2 >= %{libgtop_version}
Obsoletes: gnome-python2-libgtop

%description -n gnome-python2-libgtop2
This module contains a wrapper that allows the use of libgtop via
Python.

%package -n gnome-python2-metacity
Summary: Python bindings for interacting with metacity
License: GPLv2
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: metacity >= %{metacity_version}

%description -n gnome-python2-metacity
This module contains a wrapper that allows the use of metacity
via Python.

%package -n gnome-python2-totem
Summary: Python bindings for interacting with totem
License: LGPLv2
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: totem-pl-parser >= %{totem_version}
Requires: gnome-python2-gconf

%description -n gnome-python2-totem
This module contains a wrapper that allows the use of totem
via Python.

%package -n gnome-python2-rsvg
Summary: Python bindings for interacting with librsvg
License: LGPLv2
Group: Development/Languages
Requires: librsvg2 >= %{librsvg2_version}

%description -n gnome-python2-rsvg
This module contains a wrapper that allows the use of librsvg
via Python.

%package -n gnome-python2-gnomedesktop
Summary: Python bindings for interacting with gnome-desktop
License: LGPLv2
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gnome-desktop >= %{gnome_desktop_version}

%description -n gnome-python2-gnomedesktop
This module contains a wrapper that allows the use of gnome-desktop
via Python.

%package -n gnome-python2-gnomekeyring
Summary: Python bindings for interacting with gnome-keyring
License: LGPLv2
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gnome-keyring >= %{gnome_keyring_version}

%description -n gnome-python2-gnomekeyring
This module contains a wrapper that allows the use of gnome-keyring
via Python.

%prep
%setup -q -n gnome-python-desktop-%{version}
%patch0 -p1 -b .flags
%patch1 -p1 -b .metacity-build

%build
# evince, brasero and mediaprofiles are disabled because these things have
# been ported to GTK+3. It's not practical to mix GTK+2 and GTK+3 bindings
# in gnome-python2-desktop, so for now we'll just have to disable the GTK+3
# stuff. - AdamW 2010/07

# evolution and evolution_ecal are disabled for a while 
%configure --enable-metacity --disable-evince --disable-braseromedia --disable-braseroburn --disable-mediaprofiles --disable-applet \
 --disable-evolution --disable-evolution_ecal
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -exec rm {} \;

rm -rf %{buildroot}/%{_libdir}/python*/site-packages/gtk-2.0/gksu
rm -rf %{buildroot}/%{_libdir}/python*/site-packages/gtk-2.0/bugbuddy.*

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS NEWS README COPYING COPYING.GPL COPYING.LGPL
%{_libdir}/pkgconfig/gnome-python-desktop-2.0.pc
%{_datadir}/pygtk

#files -n gnome-python2-brasero
#defattr(-,root,root,-)
#{python_sitearch}/gtk-2.0/braseroburn.so
#{python_sitearch}/gtk-2.0/braseromedia.so

#files -n gnome-python2-evince
#defattr(-,root,root,-)
#{python_sitearch}/gtk-2.0/evince.so

%files -n gnome-python2-evolution
%defattr(-,root,root,-)
#%{python_sitearch}/gtk-2.0/evolution/

%files -n gnome-python2-gnomeprint
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gnomeprint/
%{_datadir}/gtk-doc/html/pygnomeprint
%{_datadir}/gtk-doc/html/pygnomeprintui
%defattr(644,root,root,755)
%doc ../gnome-python-desktop-%{version}/examples/gnomeprint/*

%files -n gnome-python2-gtksourceview
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gtksourceview.so
%{_datadir}/gtk-doc/html/pygtksourceview
%defattr(644,root,root,755)
%doc ../gnome-python-desktop-%{version}/examples/gtksourceview/*

%files -n gnome-python2-libwnck
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/wnck.so

%files -n gnome-python2-libgtop2
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gtop.so

%files -n gnome-python2-metacity
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/metacity.so

%files -n gnome-python2-totem
%defattr(-,root,root,-)
%ifnarch s390 s390x
#{python_sitearch}/gtk-2.0/mediaprofiles.so
%endif
%{python_sitearch}/gtk-2.0/totem

%files -n gnome-python2-rsvg
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/rsvg.so

%files -n gnome-python2-gnomedesktop
%defattr(-,root,root,-)
#%{python_sitearch}/gtk-2.0/gnomedesktop

%files -n gnome-python2-gnomekeyring
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gnomekeyring.so

%changelog
* Tue Jul 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.0-15m)
- set Obsoletes: gnome-python2-libgtop

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-14m)
- reimport from fedora
- remove sub-package; gnome-python2-bugbuddy
- disable evolution sub-package

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-13m)
- rebuild for evolution-data-server-devel-3.5.3

* Sat Jun 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.0-12m)
- rebuild with bug-buddy-2.32.0-6m

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-11m)
- rebuild for glib 2.33.2

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-10m)
- rebuild against evolution-data-server-3.1

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-9m)
- welcome bug-buddy

* Wed May  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-8m)
- rebuild GNOME 3.0
- no file gnome-python2-applet gnome-python2-bugbuddy
-- gnome-python2-gnomedesktop gnome-python2-evince
-- gnome-python2-brasero

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.0-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-5m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-4m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-3m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- rebuild against evolution-2.30.2

* Sat Jun 12 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.1-2m)
- rebuild against totem-pl-parser-2.29.1

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.1-1m)
- update to 2.29.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
- Obsolete nautilus-cd-burner

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Feb 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.25.91-2m)
- fix BuildRequires

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.90-2m)
- add BuildRequires: evince >= 2.25.90

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.24.1-2m)
- rebuild agaisst python-2.6.1-1m

* Sat Dec 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.1-1m)
- update to 1.24.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- welcome evolution-data-server

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
- good-bye evolution

* Sat Aug  9 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-5m)
- Requires: gnome-python2-desktop-gnomeprint -> gnome-python2-gnomeprint

* Mon Jul 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.22.0-4m)
- seperate package
- rename sub package

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against totem-2.23.2-1m for firefox-3
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- add package evolution

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-3m)
- rebuild against libwnck-2.20.0

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- rebuild against totem-2.20.0

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0
-- comment out libwnck (until libwnck-2.20.0)

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.0-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.93-1m)
- update to 2.17.93

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-2m)
- add package bug-buddy

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.3-1m)
- update to 2.17.3 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-3m)
- rename gnome-python-desktop -> gnome-python2-desktop

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.0-2m)
- rebuild against python-2.5

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- updateo to 2.16.0

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- delete duplicated files

* Wed Apr 12 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-1m)
- version 2.14.0
- first import from Fedora Core 5
