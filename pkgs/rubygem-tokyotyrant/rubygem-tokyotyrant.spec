%global momorel 5
%global abi_ver 1.9.1

# Generated from tokyotyrant-1.10.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname tokyotyrant
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Tokyo Tyrant: network interface of Tokyo Cabinet
Name: rubygem-%{gemname}
Version: 1.13
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://fallabs.com/tokyotyrant/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Tokyo Tyrant is a package of network interface to the DBM called Tokyo
Cabinet.  Though the DBM has high performance, you might bother in case that
multiple processes share the same database, or remote processes access the
database.  Thus, Tokyo Tyrant is provided for concurrent and remote
connections to Tokyo Cabinet.  It is composed of the server process managing a
database and its access library for client applications.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13-5m)
- change URL
- fix newer ruby warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13-1m)
- update 1.13

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12-1m)
- update 1.12

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10-1m)
- Initial package for Momonga Linux
