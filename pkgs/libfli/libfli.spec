%global momorel 6

Summary:	Library for FLI CCD Camera & Filter Wheels.
Name:		libfli
Version:	1.71
Release:	%{momorel}m%{?dist}
License:	BSD
Group:		System Environment/Libraries
Url:		http://www.flicamera.com
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	cmake 

%description
Finger Lakes Instrument library is used by applications to control FLI CCDs and Filter wheels.

%package devel
License:      BSD
Group:        Development/Libraries
Summary:      Library for FLI CCDs and Filter wheels
Requires:     %{name} >= %{version}

%description devel
Apogee library is used by applications to control Apogee CCDs.
libfli-devel contain header files required for development.

%prep
%setup -q

%build
%{cmake} .
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}%{_libdir}/*.la
mkdir %{buildroot}%{_includedir}/fli
mv %{buildroot}%{_includedir}/*.h %{buildroot}%{_includedir}/fli

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root)
%doc LICENSE.BSD
%{_libdir}/*.so

%files devel
%defattr(-,root,root)
%{_includedir}/fli

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.71-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.71-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.71-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.71-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.71-2m)
- rebuild against rpm-4.6

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- import from OpenSuSE
