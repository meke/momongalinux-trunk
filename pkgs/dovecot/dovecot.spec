%global momorel 1
%global majorver 2.2

Summary: Secure imap and pop3 server
Name: dovecot
Epoch: 1
Version: 2.2.7
Release: %{momorel}m%{?dist}
#dovecot itself is MIT, a few sources are PD, pigeonhole is LGPLv2
License: MIT and LGPLv2
Group: System Environment/Daemons

%define pigeonhole_version 20100516

URL: http://www.dovecot.org/
Source: http://www.dovecot.org/releases/%{majorver}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: dovecot.init
Source2: dovecot.pam
%global pigeonholever 0.4.2
Source8: http://www.rename-it.nl/dovecot/%{majorver}/dovecot-%{majorver}-pigeonhole-%{pigeonholever}.tar.gz
Source9: dovecot.sysconfig
Source10: dovecot.tmpfilesd

#our own
Source14: dovecot.conf.5

# 3x Fedora/RHEL specific
Patch1: dovecot-2.0-defaultconfig.patch
Patch2: dovecot-1.0.beta2-mkcert-permissions.patch
Patch3: dovecot-1.0.rc7-mkcert-paths.patch

Patch4: dovecot-2.1.10-reload.patch
Patch5: dovecot-2.1-privatetmp.patch

#wait for network
Patch6: dovecot-2.1.10-waitonline.patch
Source15: prestartscript

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel, pam-devel, zlib-devel, bzip2-devel, libcap-devel
BuildRequires: libtool, autoconf, automake, pkgconfig
BuildRequires: sqlite-devel
BuildRequires: postgresql-devel
BuildRequires: mysql-devel
BuildRequires: openldap-devel
BuildRequires: krb5-devel

# gettext-devel is needed for running autoconf because of the
# presence of AM_ICONV
#BuildRequires: gettext-devel

# Explicit Runtime Requirements for executalbe
Requires: openssl >= 0.9.7f-4

# Package includes an initscript service file, needs to require initscripts package
Requires(pre): shadow-utils
Requires(post): chkconfig shadow-utils
Requires(preun): shadow-utils chkconfig initscripts
Requires: systemd
Requires(postun): systemd

%define ssldir %{_sysconfdir}/pki/%{name}

BuildRequires: libcurl-devel expat-devel

%description
Dovecot is an IMAP server for Linux/UNIX-like systems, written with security 
primarily in mind.  It also contains a small POP3 server.  It supports mail 
in either of maildir or mbox formats.

The SQL drivers and authentication plug-ins are in their subpackages.

%package pigeonhole
Requires: %{name} = %{epoch}:%{version}-%{release}
Obsoletes: dovecot-sieve < 1:1.2.10-3
Obsoletes: dovecot-managesieve < 1:1.2.10-3
Summary: Sieve and managesieve plug-in for dovecot
Group: System Environment/Daemons
License: MIT and LGPLv2

%description pigeonhole
This package provides sieve and managesieve plug-in for dovecot LDA.

%package pgsql
Requires: %{name} = %{epoch}:%{version}-%{release}
Summary: Postgres SQL back end for dovecot
Group: System Environment/Daemons
%description pgsql
This package provides the Postgres SQL back end for dovecot-auth etc.

%package mysql
Requires: %{name} = %{epoch}:%{version}-%{release}
Summary: MySQL back end for dovecot
Group: System Environment/Daemons
%description mysql
This package provides the MySQL back end for dovecot-auth etc.

%package devel
Requires: %{name} = %{epoch}:%{version}-%{release}
Summary: Development files for dovecot
Group: Development/Libraries
%description devel
This package provides the development files for dovecot.

%prep
%setup -q -a 8
%patch1 -p1 -b .default-settings
%patch2 -p1 -b .mkcert-permissions
%patch3 -p1 -b .mkcert-paths
%patch4 -p1 -b .reload
%patch5 -p1 -b .privatetmp
%patch6 -p1 -b .waitonline
sed -i '/DEFAULT_INCLUDES *=/s|$| '"$(pkg-config --cflags libclucene-core)|" src/plugins/fts-lucene/Makefile.in

%build
#required for fdpass.c line 125,190: dereferencing type-punned pointer will break strict-aliasing rules
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
%configure                       \
    INSTALL_DATA="install -c -p -m644" \
    --docdir=%{_docdir}/%{name}-%{version}     \
    --disable-static             \
    --disable-rpath              \
    --with-nss                   \
    --with-shadow                \
    --with-pam                   \
    --with-gssapi=plugin         \
    --with-ldap=plugin           \
    --with-sql=plugin            \
    --with-pgsql                 \
    --with-mysql                 \
    --with-sqlite                \
    --with-zlib                  \
    --with-libcap                \
    --with-ssl=openssl           \
    --with-ssldir=%{ssldir}      \
    --with-solr                  \
    --with-systemdsystemunitdir=/lib/systemd/system/  \
    --with-docs

sed -i 's|/etc/ssl|/etc/pki/dovecot|' doc/mkcert.sh doc/example-config/conf.d/10-ssl.conf

make %{?_smp_mflags}

#pigeonhole
pushd dovecot-%{majorver}-pigeonhole-%{pigeonholever}
#autoreconf -fiv
%configure                             \
    INSTALL_DATA="install -c -p -m644" \
    --disable-static                   \
    --with-dovecot=../                 \
    --without-unfinished-features

make %{?_smp_mflags}
popd

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

pushd dovecot-%{majorver}-pigeonhole-%{pigeonholever}
make install DESTDIR=$RPM_BUILD_ROOT
popd

#move doc dir back to build dir so doc macro in files section can use it
mv $RPM_BUILD_ROOT/%{_docdir}/%{name}-%{version} %{_builddir}/%{name}-%{version}/docinstall


#%%if %{?fedora}00%{?rhel} < 6
#sed -i 's|password-auth|system-auth|' %{SOURCE2}
#%%endif

install -p -D -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/dovecot

#install man pages
install -p -D -m 644 %{SOURCE14} $RPM_BUILD_ROOT%{_mandir}/man5/dovecot.conf.5

#install waitonline script
install -p -D -m 755 %{SOURCE15} $RPM_BUILD_ROOT%{_libexecdir}/dovecot/prestartscript

# generate ghost .pem files
mkdir -p $RPM_BUILD_ROOT%{ssldir}/certs
mkdir -p $RPM_BUILD_ROOT%{ssldir}/private
touch $RPM_BUILD_ROOT%{ssldir}/certs/dovecot.pem
chmod 600 $RPM_BUILD_ROOT%{ssldir}/certs/dovecot.pem
touch $RPM_BUILD_ROOT%{ssldir}/private/dovecot.pem
chmod 600 $RPM_BUILD_ROOT%{ssldir}/private/dovecot.pem

#%%if %{?fedora}0 > 140 || %{?rhel}0 > 60
install -p -D -m 644 %{SOURCE10} $RPM_BUILD_ROOT%{_sysconfdir}/tmpfiles.d/dovecot.conf
#%%else
#install -p -D -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initddir}/dovecot
#install -p -D -m 600 %{SOURCE9} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/dovecot
#%%endif

mkdir -p $RPM_BUILD_ROOT/var/run/dovecot/{login,empty}

# Install dovecot configuration and dovecot-openssl.cnf
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/dovecot/conf.d
install -p -m 644 docinstall/example-config/dovecot.conf $RPM_BUILD_ROOT%{_sysconfdir}/dovecot
install -p -m 644 docinstall/example-config/conf.d/*.conf $RPM_BUILD_ROOT%{_sysconfdir}/dovecot/conf.d
install -p -m 644 docinstall/example-config/conf.d/*.conf.ext $RPM_BUILD_ROOT%{_sysconfdir}/dovecot/conf.d
install -p -m 644 doc/dovecot-openssl.cnf $RPM_BUILD_ROOT%{ssldir}/dovecot-openssl.cnf

install -p -m755 doc/mkcert.sh $RPM_BUILD_ROOT%{_libexecdir}/%{name}/mkcert.sh

mkdir -p $RPM_BUILD_ROOT/var/lib/dovecot

#remove the libtool archives
find $RPM_BUILD_ROOT%{_libdir}/%{name}/ -name '*.la' | xargs rm -f

#remove what we don't want
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/dovecot/README
pushd docinstall
rm -f securecoding.txt thread-refs.txt
popd


%clean
rm -rf $RPM_BUILD_ROOT


%pre
#dovecot uig and gid are reserved, see /usr/share/doc/setup-*/uidgid 
getent group dovecot >/dev/null || groupadd -r --gid 97 dovecot
getent passwd dovecot >/dev/null || \
useradd -r --uid 97 -g dovecot -d /usr/libexec/dovecot -s /sbin/nologin -c "Dovecot IMAP server" dovecot

getent group dovenull >/dev/null || groupadd -r dovenull
getent passwd dovenull >/dev/null || \
useradd -r -g dovenull -d /usr/libexec/dovecot -s /sbin/nologin -c "Dovecot's unauthorized user" dovenull
exit 0

%post
if [ $1 -eq 1 ]
then
#%%if %{?fedora}0 > 140 || %{?rhel}0 > 60
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
#%%else
#  /sbin/chkconfig --add %{name}
#%%endif
fi

# generate the ssl certificates
if [ ! -f %{ssldir}/certs/%{name}.pem ]; then
    SSLDIR=%{ssldir} OPENSSLCONFIG=%{ssldir}/dovecot-openssl.cnf \
         %{_libexecdir}/%{name}/mkcert.sh &> /dev/null
fi

if [ ! -f /var/lib/dovecot/ssl-parameters.dat ]; then
    /usr/libexec/dovecot/ssl-params &>/dev/null
fi

install -d -m 0755 -g dovecot -d /var/run/dovecot
install -d -m 0755 -d /var/run/dovecot/empty
install -d -m 0750 -g dovenull /var/run/dovecot/login
[ -x /sbin/restorecon ] && /sbin/restorecon -R /var/run/dovecot

exit 0

%preun
if [ $1 = 0 ]; then
#%%if %{?fedora}0 > 140 || %{?rhel}0 > 60
        /bin/systemctl disable dovecot.service dovecot.socket >/dev/null 2>&1 || :
        /bin/systemctl stop dovecot.service dovecot.socket >/dev/null 2>&1 || :
#%%else
#    /sbin/service %{name} stop > /dev/null 2>&1
#    /sbin/chkconfig --del %{name}
#%%endif
    rm -rf /var/run/dovecot
fi

%postun
if [ "$1" -ge "1" ]; then
#%%if %{?fedora}0 > 140 || %{?rhel}0 > 60
    /bin/systemctl try-restart dovecot.service >/dev/null 2>&1 || :
#%%else
#    /sbin/service %{name} condrestart >/dev/null 2>&1 || :
#%%endif
fi

%check
make check
cd dovecot-%{majorver}-pigeonhole-%{pigeonholever}
make check

%files
%defattr(-,root,root,-)
%doc docinstall/* AUTHORS ChangeLog COPYING COPYING.LGPL COPYING.MIT NEWS README
%{_sbindir}/dovecot

%{_bindir}/doveadm
%{_bindir}/doveconf
%{_bindir}/dsync


#%%if %{?fedora}0 > 140 || %{?rhel}0 > 60
%config(noreplace) %{_sysconfdir}/tmpfiles.d/dovecot.conf
/lib/systemd/system/dovecot.service
/lib/systemd/system/dovecot.socket
#%%else
#%%{_initddir}/dovecot
#%%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/dovecot
#%%endif

%dir %{_sysconfdir}/dovecot
%dir %{_sysconfdir}/dovecot/conf.d
%config(noreplace) %{_sysconfdir}/dovecot/dovecot.conf
#list all so we'll be noticed if upstream changes anything
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/10-auth.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/10-director.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/10-logging.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/10-mail.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/10-master.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/10-ssl.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/15-lda.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/15-mailboxes.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/20-imap.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/20-lmtp.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/20-pop3.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/90-acl.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/90-quota.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/90-plugin.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/90-sieve-extprograms.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-checkpassword.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-deny.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-dict.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-ldap.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-master.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-passwdfile.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-sql.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-static.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-system.conf.ext
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/auth-vpopmail.conf.ext

%config(noreplace) %{_sysconfdir}/pam.d/dovecot
%config(noreplace) %{ssldir}/dovecot-openssl.cnf

%dir %{ssldir}
%dir %{ssldir}/certs
%dir %{ssldir}/private
%attr(0600,root,root) %ghost %config(missingok,noreplace) %verify(not md5 size mtime) %{ssldir}/certs/dovecot.pem
%attr(0600,root,root) %ghost %config(missingok,noreplace) %verify(not md5 size mtime) %{ssldir}/private/dovecot.pem

%dir %{_libdir}/dovecot
%dir %{_libdir}/dovecot/auth
%dir %{_libdir}/dovecot/dict
%{_libdir}/dovecot/doveadm
#these (*.so files) are plugins not a devel files
%{_libdir}/dovecot/*_plugin.so
%{_libdir}/dovecot/libssl_iostream_openssl.so
%{_libdir}/dovecot/*.so.*
%{_libdir}/dovecot/auth/libauthdb_imap.so
%{_libdir}/dovecot/auth/libauthdb_ldap.so
%{_libdir}/dovecot/auth/libmech_gssapi.so
%{_libdir}/dovecot/auth/libdriver_sqlite.so
%{_libdir}/dovecot/dict/libdriver_sqlite.so
%{_libdir}/dovecot/sieve/lib90_sieve_extprograms_plugin.so
%{_libdir}/dovecot/libdriver_sqlite.so

%{_libexecdir}/dovecot

%ghost /var/run/dovecot
%attr(0750,dovecot,dovecot) /var/lib/dovecot

%{_mandir}/man1/deliver.1*
%{_mandir}/man1/doveadm*.1*
%{_mandir}/man1/doveconf.1*
%{_mandir}/man1/dovecot*.1*
%{_mandir}/man1/dsync.1*
%{_mandir}/man5/dovecot.conf.5*
%{_mandir}/man7/doveadm-search-query.7*

%exclude %{_libexecdir}/%{name}/managesieve
%exclude %{_libexecdir}/%{name}/managesieve-login

%files devel
%defattr(-,root,root,-)
%{_includedir}/dovecot
%{_datadir}/aclocal/dovecot.m4
%{_libdir}/dovecot/libdovecot*.so
%{_libdir}/dovecot/dovecot-config

%files pigeonhole
%defattr(-,root,root,-)
%{_bindir}/sieve-dump
%{_bindir}/sieve-filter
%{_bindir}/sieve-test
%{_bindir}/sievec
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/90-sieve.conf
%config(noreplace) %{_sysconfdir}/dovecot/conf.d/20-managesieve.conf
%{_libexecdir}/%{name}/managesieve
%{_libexecdir}/%{name}/managesieve-login

%dir %{_libdir}/dovecot/settings
%{_libdir}/dovecot/settings/libmanagesieve_*.so

%{_mandir}/man1/sieve-test.1*
%{_mandir}/man1/sieve-dump.1*
%{_mandir}/man1/sieve-filter.1*
%{_mandir}/man1/sievec.1*
%{_mandir}/man1/sieved.1*
%{_mandir}/man7/pigeonhole.7*

%files mysql
%defattr(-,root,root,-)
%{_libdir}/%{name}/libdriver_mysql.so
%{_libdir}/%{name}/auth/libdriver_mysql.so
%{_libdir}/%{name}/dict/libdriver_mysql.so

%files pgsql
%defattr(-,root,root,-)
%{_libdir}/%{name}/libdriver_pgsql.so
%{_libdir}/%{name}/auth/libdriver_pgsql.so
%{_libdir}/%{name}/dict/libdriver_pgsql.so

%changelog
* Sat Dec 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.7-1m)
- [SECURITY] http://wiki2.dovecot.org/AuthDatabase/CheckPassword#Security
- update to 2.2.7

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.16-1m)
- update to 2.1.16

* Thu Jan 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.21-1m)
- version up 2.0.21

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.19-1m)
- version up 2.0.19

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.17-1m)
- version up 2.0.17

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.16-1m)
- version up 2.0.16

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.15-1m)
- version up 2.0.15
- support sysmted

* Mon Oct 17 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.13-2m)
- exclude managesieve and managesieve-login from main package

* Sun Oct 16 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.13-1m)
- version up 2.0.13

* Thu May 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.17-1m)
- [SECURITY] CVE-2011-1929 CVE-2011-2166 CVE-2011-2167
- update to 1.2.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.15-4m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15-3m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.15-2m)
- rebuild for new GCC 4.5

* Sun Oct 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15-1m)
- [SECURITY] CVE-2010-3706 CVE-2010-3707 CVE-2010-3779 CVE-2010-3780
- update to 1.2.15

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.13-3m)
- full rebuild for mo7 release

* Sat Aug 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.13-2m)
- explicitly link -lcrypt and -ldl for the moment

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.13-1m)
- [SECURITY] http://www.dovecot.org/list/dovecot-news/2010-July/000163.html
- sync with Fedora 13 (1:1.2.13-1)

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.11-3m)
- fix build with new gcc on i686

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.11-2m)
- rebuild against openssl-1.0.0

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.11-1m)
- [SECURITY] CVE-2010-0745
- update to 1.2.11

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-1m)
- [SECURITY] CVE-2009-3897
- update to 1.2.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- sync with Fedora 11 (1:1.2.1-1)
-- use dovecot-1.2-sieve and dovecot-1.2-managesieve

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.5.3m)
- remove duplicate files

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.5.2m)
- remove duplicate files

* Fri Jun 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.5.1m)
- update to 1.2.rc5
- enable ldap
- add managesieve based on Fedora 11 (1:1.2-0.rc5.2)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.4.2m)
- rebuild against openssl-0.9.8k

* Thu Mar 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-0.4.1m)
- update to 1.2.beta4

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.3.1m)
- update to 1.2.beta3
- rebuild against mysql-5.1.32 

* Sun Feb 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.2.1m)
- update to 1.2.beta1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.1.2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.1.1m)
- update to 1.2.alpha5
- temporarily set %%global build_ldap 0

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.15-1m)
- [SECURITY] CVE-2008-4577
-- import security patch from Fedora 9 updates (1:1.0.15-14)
- update to 1.0.15
- update sieve plugin to 1.0.3

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.13-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.13-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.13-1m)
- up to 1.0.13
- [SECURITY] CVE-2008-1199 CVE-2008-1218

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-4m) 
- fix sieverel

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-2m)
- %%NoSource -> NoSource

* Wed Jan  2 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.10-1m)
- up to 1.0.10
- [SECURITY] CVE-2007-6598
- [SECURITY] http://www.dovecot.org/list/dovecot-news/2007-December/000057.html

* Thu Dec 13 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.9-1m)
- up to 1.0.9

* Fri Nov 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.8-1m)
- up to 1.0.8

* Tue Oct 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.7-1m)
- up to 1.0.7

* Sun Oct 28 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.6-1m)
- up to 1.0.6

* Tue Sep 11 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.5-1m)
- up to 1.0.5
- update sieve plugin to 1.0.2

* Thu Aug  2 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-1m)
- up to 1.0.3

* Tue Jul 17 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.2-1m)
- up to 1.0.2

* Sat Jun 16 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.1-1m)
- up to 1.0.1

* Sat Apr 14 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.0-1m)
- up to 1.0.0

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.931-2m)
- increase release, yum can not handle same version same release (dovecot-sieve-1.0.1-1m)
- dovecot-sieve should be provided independent release number like kdebindings

* Mon Apr  9 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.931-1m)
- up to 1.0.rc31
- [SECURITY] zlib plugin allows opening any gziped mboxes
  <http://www.dovecot.org/list/dovecot-news/2007-March/000038.html>

* Sat Mar 24 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.928-1m)
- version up 1.0.rc28
- sync fedora devel (add dovecot-sieve package)

* Wed Mar 14 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.927-1m)
- up to 1.0.rc27

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.926-2m)
- rebuild against postgresql-8.2.3

* Wed Mar  7 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.926-1m)
- up to 1.0.rc26

* Fri Mar  2 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.925-1m)
- up to 1.0.rc25

* Fri Feb 23 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.924-1m)
- up to 1.0.rc24

* Tue Feb 20 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.923-1m)
- version up 1.0.rc23

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-0.922-2m)
- delete libtool library

* Wed Feb 07 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.922-1m)
- version up 1.0.rc22

* Fri Feb 02 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.921-1m)
- version up 1.0.rc21

* Fri Feb 02 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.920-1m)
- version up 1.0.rc20

* Wed Jan 24 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.919-1m)
- version up 1.0.rc19

* Tue Jan 23 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.918-1m)
- version up 1.0.rc18

* Sun Jan  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.917.1m)
- up to RC 17
- add BuildRequires: autoconf >= 2.61

* Sat Nov 25 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.915-1m)
- [SECURITY] CVE-2006-5973
- version up 1.0.rc15
- sync fc-6

* Thu Nov  8 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.912.2m)
- namespace hack for apple mail

* Thu Nov  7 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.912.1m)
- up to RC 12

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-0.96.2m)
- change autoreconf to each autotools

* Wed Aug  9 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.96.1m)
- up to 1.0rc6

* Tue Jun  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.8.2m)
- modify Prereq for new shadow-utils

* Fri May 19 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0-0.8.1m)
- sync with fc-devel (1.0-0.beta7.1)
- version up to 1.0.beta8

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.99.14-7m)
- rebuild against mysql 5.0.22-1m

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.14-6m)
- rebuild against openldap-2.3.19

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.14-5m)
- rebuild against openldap-2.3.11

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.99.14-4m)
- rebuild against postgresql-8.1.0

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.99.14-3m)
- rebuild against for MySQL-4.1.14

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (0.99.14-2m)
- rebuild against postgresql-8.0.2.

* Fri Mar 11 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.99.14-1m)
- several fixes

* Mon Jan 10 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.99.13-1m)
- update to 0.99.13

* Fri Nov 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.11-2m)
- more momonganize

* Fri Oct 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.99.11-1m)
- import to momonga

* Thu Sep 30 2004 John Dennis <jdennis@redhat.com> 0.99.11-1.FC3.3
- fix bug #124786, listen to ipv6 as well as ipv4

* Wed Sep  8 2004 John Dennis <jdennis@redhat.com> 0.99.11-1.FC3.1
- bring up to latest upstream,
  comments from Timo Sirainen <tss at iki.fi> on release v0.99.11 2004-09-04
  + 127.* and ::1 IP addresses are treated as secured with
    disable_plaintext_auth = yes
  + auth_debug setting for extra authentication debugging
  + Some documentation and error message updates
  + Create PID file in /var/run/dovecot/master.pid
  + home setting is now optional in static userdb
  + Added mail setting to static userdb
  - After APPENDing to selected mailbox Dovecot didn't always notice the
    new mail immediately which broke some clients
  - THREAD and SORT commands crashed with some mails
  - If APPENDed mail ended with CR character, Dovecot aborted the saving
  - Output streams sometimes sent data duplicated and lost part of it.
    This could have caused various strange problems, but looks like in
    practise it rarely caused real problems.

* Wed Aug  4 2004 John Dennis <jdennis@redhat.com>
- change release field separator from comma to dot, bump build number

* Mon Aug  2 2004 John Dennis <jdennis@redhat.com> 0.99.10.9-1,FC3,1
- bring up to date with latest upstream, fixes include:
- LDAP support compiles now with Solaris LDAP library
- IMAP BODY and BODYSTRUCTURE replies were wrong for MIME parts which
  didn't contain Content-Type header.
- MySQL and PostgreSQL auth didn't reconnect if connection was lost
  to SQL server
- Linking fixes for dovecot-auth with some systems
- Last fix for disconnecting client when downloading mail longer than
  30 seconds actually made it never disconnect client. Now it works
  properly: disconnect when client hasn't read _any_ data for 30
  seconds.
- MySQL compiling got broken in last release
- More PostgreSQL reconnection fixing


* Mon Jul 26 2004 John Dennis <jdennis@redhat.com> 0.99.10.7-1,FC3,1
- enable postgres and mySQL in build
- fix configure to look for mysql in alternate locations
- nuke configure script in tar file, recreate from configure.in using autoconf

- bring up to latest upstream, which included:
- Added outlook-pop3-no-nuls workaround to fix Outlook hang in mails with NULs.
- Config file lines can now contain quoted strings ("value ")
- If client didn't finish downloading a single mail in 30 seconds,
  Dovecot closed the connection. This was supposed to work so that
  if client hasn't read data at all in 30 seconds, it's disconnected.
- Maildir: LIST now doesn't skip symlinks


* Wed Jun 30 2004 John Dennis <jdennis@redhat.com>
- bump rev for build
- change rev for FC3 build

* Fri Jun 25 2004 John Dennis <jdennis@redhat.com> - 0.99.10.6-1
- bring up to date with upstream,
  recent change log comments from Timo Sirainen were:
  SHA1 password support using OpenSSL crypto library
  mail_extra_groups setting
  maildir_stat_dirs setting
  Added NAMESPACE capability and command
  Autocreate missing maildirs (instead of crashing)
  Fixed occational crash in maildir synchronization
  Fixed occational assertion crash in ioloop.c
  Fixed FreeBSD compiling issue
  Fixed issues with 64bit Solaris binary

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu May 27 2004 David Woodhouse <dwmw2@redhat.com> 0.99.10.5-1
- Update to 0.99.10.5 to fix maildir segfaults (#123022)

* Fri May 07 2004 Warren Togami <wtogami@redhat.com> 0.99.10.4-4
- default auth config that is actually usable
- Timo Sirainen (author) suggested functionality fixes
  maildir, imap-fetch-body-section, customflags-fix

* Mon Feb 23 2004 Tim Waugh <twaugh@redhat.com>
- Use ':' instead of '.' as separator for chown.

* Tue Feb 17 2004 Jeremy Katz <katzj@redhat.com> - 0.99.10.4-3
- restart properly if it dies (#115594)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Nov 24 2003 Jeremy Katz <katzj@redhat.com> 0.99.10.4-1
- update to 0.99.10.4

* Mon Oct  6 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-7
- another patch from upstream to fix returning invalid data on partial
  BODY[part] fetches
- patch to avoid confusion of draft/deleted in indexes

* Tue Sep 23 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-6
- add some patches from upstream (#104288)

* Thu Sep  4 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-5
- fix startup with 2.6 with patch from upstream (#103801)

* Tue Sep  2 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-4
- fix assert in search code (#103383)

* Tue Jul 22 2003 Nalin Dahyabhai <nalin@redhat.com> 0.99.10-3
- rebuild

* Thu Jul 17 2003 Bill Nottingham <notting@redhat.com> 0.99.10-2
- don't run by default

* Thu Jun 26 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-1
- 0.99.10

* Mon Jun 23 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-0.2
- 0.99.10-rc2 (includes ssl detection fix)
- a few tweaks from fedora
  - noreplace the config file
  - configure --with-ldap to get LDAP enabled

* Mon Jun 23 2003 Jeremy Katz <katzj@redhat.com> 0.99.10-0.1
- 0.99.10-rc1
- add fix for ssl detection
- add zlib-devel to BuildRequires
- change pam service name to dovecot
- include pam config

* Thu May  8 2003 Jeremy Katz <katzj@redhat.com> 0.99.9.1-1
- update to 0.99.9.1
- add patch from upstream to fix potential bug when fetching with
  CR+LF linefeeds
- tweak some things in the initscript and config file noticed by the
  fedora folks

* Sun Mar 16 2003 Jeremy Katz <katzj@redhat.com> 0.99.8.1-2
- fix ssl dir
- own /var/run/dovecot/login with the correct perms
- fix chmod/chown in post

* Fri Mar 14 2003 Jeremy Katz <katzj@redhat.com> 0.99.8.1-1
- update to 0.99.8.1

* Tue Mar 11 2003 Jeremy Katz <katzj@redhat.com> 0.99.8-2
- add a patch to fix quoting problem from CVS

* Mon Mar 10 2003 Jeremy Katz <katzj@redhat.com> 0.99.8-1
- 0.99.8
- add some buildrequires
- fixup to build with openssl 0.9.7
- now includes a pop3 daemon (off by default)
- clean up description and %%preun
- add dovecot user (uid/gid of 97)
- add some buildrequires
- move the ssl cert to %{_datadir}/ssl/certs
- create a dummy ssl cert in %post
- own /var/run/dovecot
- make the config file a source so we get default mbox locks of fcntl

* Sun Dec  1 2002 Seth Vidal <skvidal@phy.duke.edu>
- 0.99.4 and fix startup so it starts imap-master not vsftpd :)

* Tue Nov 26 2002 Seth Vidal <skvidal@phy.duke.edu>
- first build
