%global momorel 16
%define support_perl 1
%define perl_version %(eval "`%{__perl} -V:version`"; echo $version)

Summary: File transfer utilities between Linux and PalmPilots
Name: pilot-link
Version: 0.12.5
Release: %{momorel}m%{?dist}
Source0: http://downloads.pilot-link.org/%{name}-%{version}.tar.bz2 
NoSource: 0

Source3: blacklist-visor
Source4: README.fedora
Source5: 60-pilot.perms

ExcludeArch: s390 s390x
Patch4: pilot-link-0.12.1-var.patch 
Patch6: pilot-link-0.12.2-open.patch
Patch10: pilot-link-0.12.2-werror_194921.patch
Patch20: pilot-link-0.12.5-perl-5.14.patch

URL: http://www.pilot-link.org/
License: GPL and LGPL
Group: Applications/Communications
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libpng-devel >= 1.2.5-4m
BuildRequires: readline-devel >= 5.0, ncurses-devel
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: libusb-devel
BuildRequires: bluez-libs-devel >= 4.00

%if %{support_perl}
Requires:      perl(:MODULE_COMPAT_%{perl_version})
BuildRequires: perl-ExtUtils-MakeMaker
BuildRequires: perl-constant
%endif

%description
This suite of tools allows you to upload and download programs and
data files between a Linux/UNIX machine and the PalmPilot. It has a
few extra utilities that will allow for things like syncing the
PalmPilot's calendar app with Ical. Note that you might still need to
consult the sources for pilot-link if you would like the Python, Tcl,
or Perl bindings.

Install pilot-link if you want to synchronize your Palm with your
Momonga Linux system.

%package devel
Summary: PalmPilot development header files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libpng-devel
Requires: readline-devel

%description devel
This package contains the development headers that are used to build
the pilot-link package. It also includes the static libraries
necessary to build static pilot applications.

If you want to develop PalmPilot synchronizing applications, you'll
need to install pilot-link-devel.

%prep 
%setup -q
%patch4 -p1 -b .var
%patch6 -p1 -b .open
%patch10 -p1 -b .werror
%patch20 -p1 -b .perl-5.14

%build
autoreconf -is
CFLAGS="%{optflags}" %configure \
  --with-python=no \
  --with-itcl=no \
  --with-tk=no \
  --with-tcl=no \
  --with-java=no \
  --with-cpp=yes \
%if %{support_perl}
  --with-perl=yes \
%else
  --with-perl=no \
%endif
  --enable-conduits \
  --enable-compile-werror=no \
  --enable-libusb --program-prefix=""

make

%install
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} libdir=%{_libdir} 
make install -C doc/man DESTDIR=%{buildroot} libdir=%{_libdir}

%if %{support_perl}
if test -f bindings/Perl/Makefile.PL ; then
   cd bindings/Perl
   perl -pi -e 's|^\$libdir =.*|\$libdir = "%{buildroot}%{_libdir}";|g' Makefile.PL
   CFLAGS="%{optflags}" %{__perl} Makefile.PL INSTALLDIRS=vendor
   make -B || :
   make
   cd ../..
fi
%endif

%if %{support_perl}
  cd bindings/Perl
  make pure_install PERL_INSTALL_ROOT=%{buildroot}
  cd ../..
  # remove files and fix perms
  find %{buildroot}%{_libdir}/perl5/ -type f -name '.packlist' -exec rm -f {} \;
  find %{buildroot}%{_libdir}/perl5/ -type f -name '*.bs' -size 0 -exec rm -f {} \;
  find %{buildroot}%{_libdir}/perl5/ -type f -name '*.so' -exec chmod 0755 {} \;
  rm -f %{buildroot}%{_libdir}/perl5/*/*/perllocal.pod
  rm -f %{buildroot}%{_libdir}/perl5/*/*/*/PDA/dump.pl
%else
  rm -f %{buildroot}%{_mandir}/man1/ietf2datebook*
%endif

# remove files we don't want to include
rm -f %{buildroot}%{_libdir}/*.la

# remove static libraries
rm -f %{buildroot}%{_libdir}/*.a

# remove broken prog
rm -f %{buildroot}%{_bindir}/pilot-prc

# Put visor to blacklist
mkdir -p %{buildroot}%{_sysconfdir}/modprobe.d/
install -p -m644 %{SOURCE3} %{buildroot}%{_sysconfdir}/modprobe.d/blacklist-visor.conf

rm -f %{buildroot}%{perl_vendorarch}/auto/PDA/Pilot/.packlist

# put README.fedora into tree
cp %{SOURCE4} README.fedora

# install visor configs to share/udev
install -p -m644 %{SOURCE5} %{buildroot}%{_datadir}/pilot-link/udev

find %{buildroot} -name "*.la" -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README NEWS doc/README.usb doc/README.debugging doc/README.libusb README.fedora
%{_libdir}/*.so.*
%if %{support_perl}
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/PDA*
%endif
%{_bindir}/*
%{_datadir}/pilot-link
%{_mandir}/man?/*
%config(noreplace) %{_sysconfdir}/modprobe.d/blacklist-visor.conf

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/aclocal/*.m4

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-16m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.5-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.5-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.5-1m)
- update to 0.12.5

* Fri May 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.3-11m)
- add BuildRequires

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.3-10m)
- rebuild against perl-5.12.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.3-9m)
- rebuild against readline6

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.3-8m)
- rebuild against perl-5.12.0

* Wed Feb 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.3-7m)
- move blacklist-visor to blacklist-visor.conf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.3-5m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.3-4m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
-  (0.12.3-3m)
- rebuild against bluez-4.22

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.3-2m)
- import patch10 from Gentoo
- set --enable-compile-werror=no to fix build break

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.3-1m)
- update to 0.12.3

* Sun Jun  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-8m)
- remove "-Werror"
- remove CC=gcc from configure's option so that ccache can be used

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.1-7m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.1-6m)
- remove BPR libtermcap-devel

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.1-5m)
- %%NoSource -> NoSource

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-4m)
- rebuild against perl-5.10.0

* Sat Apr 21 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.12.1-3m)
- x86_64 build fix (remove -Werror).

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-2m)
- delete libtool library

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1
- delete patch1 (not needed)

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.11.8-10m)
- rebuild against readline-5.0

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.8-9m)
- rebuild against perl-5.8.8

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.8-8m)
- rebuilt against perl-5.8.7

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.8-7m)
- suppress AC_DEFUN warning.

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.11.8-6m)
- rebuild against libtermcap and ncurses

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.11.8-5m)
- rebuild against gcc-c++-3.4.1

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.8-4m)
- build against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.11.8-2m)
- remove Epoch from BuildPrereq

* Wed Apr  7 2004 Toru Hoshina <t@momonga-linux.org>
- (0.11.8-1m)
- termcap...

* Wed Nov 19 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.11.8-1m)
- updated to 0.11.8
- change URL: and Source: sites
- fix License: GPL and LGPL - see License notes in README
- add BuildPreReq: readline-devel, libtermcap-devel, ncurses-devel, gcc-c++
- append CC=gcc --program-prefix="" at %%configure

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.5-4m)
- rebuild against perl-5.8.2

* Sun Nov  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.11.5-3m)
- rebuild against perl-5.8.1

* Mon Aug 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.5-2m)
- use NoSource macro

* Mon Nov 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.11.5-1m)
- rebuild against perl-5.8.0

* Sat Sep 14 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.11.3-1m)
- My CLIE T650 requires newer version.

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9.5-8k)
- cancel gcc-3.1 autoconf-2.53

* Sat May 25 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.5-6k)
  applied gcc 3.1 patch

* Mon Oct 29 2001 Toru Hoshina <t@Kondara.org>
- (0.9.5-2k)
- version up.

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gcc 2.96.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul  4 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Mon Jun  5 2000 Matt Wilson <msw@redhat.com>
- defattr on devel package

* Sat Jun  3 2000 Matt Wilson <msw@redhat.com>
- rebuilt against tcl-8.3.1

* Wed May 31 2000 Matt Wilson <msw@redhat.com>
- fix building with egcs 2.96 and gcc 2.2, build against new libstdc++
- use _mandir macro

* Wed Apr  5 2000 Bill Nottingham <notting@redhat.com>
- rebuild against current ncurses/readline

* Sun Mar 26 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- call ldconfig directly from postun

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- gzip man pages

* Wed Oct 20 1999 Preston Brown <pbrown@redhat.com>
- upgrade to pilot-link 0.9.3, rewrite spec.

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- strip binaries

* Tue Mar 30 1999 Preston Brown <pbrown@redhat.com>
- added missing files from devel subpackage

* Fri Mar 26 1999 Preston Brown <pbrown@redhat.com>
- move /usr/lib/pix to /usr/lib/pilot-link (dumb, BAD name)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Thu Jan 21 1999 Bill Nottingham <notting@redhat.com>
- arm fix

* Fri Sep 24 1998 Michael Maher <mike@redhat.com>
- cleaned up spec file, updated package

* Tue May 19 1998 Michael Maher <mike@redhat.com>
- updated rpm

* Thu Jan 29 1998 Otto Hammersmith <otto@redhat.com>
- added changelog
- updated to 0.8.9
- removed explicit requires for /usr/bin/perl
