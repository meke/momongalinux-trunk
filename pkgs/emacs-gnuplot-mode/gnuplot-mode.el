;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Gnuplot-mode configuration sample by muraken <muraken2@nifty.com>
;;
;; Please copy & paste to .emacsen
;;

(autoload 'gnuplot-mode "gnuplot" "gnuplot major mode" t)
(autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot mode" t)

(setq auto-mode-alist (append '(("\\.gp$" . gnuplot-mode)) auto-mode-alist))

;; use toolbar
;;(setq gnuplot-use-toolbar 'default-toolbar)
(setq gnuplot-toolbar-use-toolbar 'left-toolbar)
;;(setq gnuplot-use-toolbar 'right-toolbar)
;;(setq gnuplot-use-toolbar 'top-toolbar)
;;(setq gnuplot-use-toolbar 'bottom-toolbar)

;; display certain help message automatically
(setq gnuplot-insertions-show-help-flag t)

;; open arguments pop-ups automatically
(setq gnuplot-gui-popup-flag nil)

(global-set-key [(f9)] 'gnuplot-make-buffer)
