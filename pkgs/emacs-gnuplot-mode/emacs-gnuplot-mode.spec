%global momorel 35
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Gnuplot mode for Emacs
Name: emacs-gnuplot-mode
Version: 0.6.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
URL: http://cars9.uchicago.edu/~ravel/software/gnuplot-mode.html
Source0: http://cars9.uchicago.edu/~ravel/software/gnuplot-mode/gnuplot-mode.%{version}.tar.gz
NoSource: 0
Source1: gnuplot-mode.el
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: gnuplot-mode-emacs
Obsoletes: gnuplot-mode-xemacs

Obsoletes: elisp-gnuplot-mode
Provides: elisp-gnuplot-mode

%description
This is inspired by the Gnuplot mode originally written by Gershon Elber
and initial versions were written in collaboration with Phil Type. All
of the code in the current version is new and shares only some concepts
with the original mode by Elber.

%prep
%setup -q -n gnuplot-mode.%{version}

%build
%configure
make EMACS=emacs
for i in *.elc; do
    mv $i $i.emacs
done

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{e_sitedir}/gnuplot-mode
install -m 644 g*.el %{buildroot}%{e_sitedir}/gnuplot-mode
for i in g*.elc.emacs; do
    install -m 644 $i %{buildroot}%{e_sitedir}/gnuplot-mode/`basename $i .emacs`
done

mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/config-sample/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog INSTALL README dot.el dotemacs gpelcard.*
%{_datadir}/config-sample/%{name}
%{e_sitedir}/gnuplot-mode

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-35m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-34m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.0-33m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-32m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-31m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-30m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-29m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-28m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-27m)
- merge gnuplot-mode-emacs to elisp-gnuplot-mode
- kill gnuplot-mode-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-25m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-24m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-23m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-22m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-21m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-20m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-19m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-18m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-17m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-16m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-15m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-14m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-13m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-12m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-11m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-10m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-9m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-8m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-7m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-6m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-5m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.0-4m)
- use %%{e_sitedir} %%{xe_sitedir}.
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6.0-2m)
- rebuild against emacs-21.3.50

* Thu Jun 26 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0
- thanks to Masayuki SANO [Momonga-devel.ja:01796]

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5q-12m)
- rebuild against emacs-21.3

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5q-11m)
- add BuildPreReq xemacs-sumo

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5q-10k)
- rebuild against emacs-21.2

* Sun Nov 18 2001 Kenta MURATA <muraken2@nifty.com>
- (0.5q-8k)
- add gnuplot-mode.el for config-sample.

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.5q-6k)
- modify BuildPreReq

* Sat Oct 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.5q-4k)
- requires common package

* Sat Oct 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (elisp-gnuplot-mode-0.5q-2k)
- do not be NoSource, since source file may be modified without changing
  name

* Sun Dec  3 2000 KIM Hyeong Cheol <kim@kondara.org>
- (0.5k-3k)
- add URL tag.
- use %{_prefix} macro.

* Wed Nov 22 2000 Yusuke Ono <ono@quake2.kuciv.kyoto-u.ac.jp>
- initial release.
