%global momorel 6
%global srcname %{name}-%{version}rc5

Summary: (a livecoding environment)
Name: fluxus
Version: 0.17
Release: 0.1.%{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPLv2
URL: http://www.pawfal.org/fluxus/
Source0: http://git.savannah.gnu.org/cgit/fluxus.git/snapshot/%{srcname}.tar.gz
#NoSource: 0
Patch1: fluxus-0.16rc3-link.patch
BuildRequires: plt-scheme >= 4.2.4-2m
BuildRequires: alsa-lib-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: fftw-devel
BuildRequires: freeglut-devel
BuildRequires: freetype-devel
BuildRequires: glibc-devel
BuildRequires: glew-devel >= 1.10.0
BuildRequires: jack-devel
BuildRequires: libX11-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: liblo-devel
BuildRequires: libpng-devel
BuildRequires: libsndfile-devel
BuildRequires: libstdc++-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: ode-devel
BuildRequires: zlib-devel
Requires: plt-scheme >= 4.2.4-2m

%description
A rapid prototyping, livecoding and playing/learning environment for
3D graphics, sound and games. Extends PLT Scheme with graphical
commands and can be used within it's own livecoding environment or
from within the DrScheme IDE.

%prep
%setup -q -n %{srcname}
%patch1 -p1 -b .link

%build
cd docs && ./makehelpmap.scm

%install
rm -rf %{buildroot}
scons install \
      DESTDIR=%{buildroot} \
      Prefix=%{_prefix} \
      PLTPrefix=%{_prefix} \
      PLTLib=%{_libdir}/plt

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGES COPYING LICENCE README
%{_bindir}/fluxa
%{_bindir}/fluxus
%{_prefix}/lib/fluxus-017
%{_datadir}/doc/fluxus-017
%{_datadir}/fluxus-017

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-0.1.6m)
- rebuild against ffmpeg

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.17-0.1.5m)
- rebuild against glew-1.10.0

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-0.1.4m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-0.1.3m)
- rebuild for glew-1.9.0

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-0.1.2m)
- rebuild against libtiff-4.0.1

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-0.1.1m)
- update to 0.17rc5
- rebuild against glew-1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-0.1.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-0.1.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-0.1.4m)
- full rebuild for mo7 release

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-0.1.3m)
- fix build with gcc-4.4.4

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-0.1.2m)
- rebuild against libjpeg-8a

* Thu Feb 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-0.1.1m)
- initial packaging
