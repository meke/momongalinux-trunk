%global momorel 1

Summary:        Command-line ACPI client
Name:           acpi
Version:        1.7
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/System
URL:		http://sourceforge.net/projects/acpiclient/
Source0:        http://grahame.angrygoats.net/source/%{name}/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Linux ACPI client is a small command-line program that attempts to
replicate the functionality of the 'old' apm command on ACPI systems.
It includes battery and thermal information.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%changelog
* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7-1m)
- update 1.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-1m)
- version 1.5

* Sun Apr 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-1m)
- version 1.4
- remove merged keep_CFLAGS.diff

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.09-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.09-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jul 14 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.09-4
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.09-3
- Autorebuild for GCC 4.3

* Wed Sep  6 2006 Patrice Dumas <pertusus@free.fr> 0.09-2
- rebuild for FC-6

* Mon Mar  6 2006 Patrice Dumas <pertusus@free.fr> 0.09-1
- based on pld. Updated, and adapted to fedora extras
