%global momorel 4
%global srcname BWidget

%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitelib: %define tcl_sitelib %{_datadir}/tcl%{tcl_version}}

Name:           bwidget
Version:        1.9.2
Release:        %{momorel}m%{?dist}
Summary:        Extended widget set for Tk

Group:          Development/Libraries
License:        "TCL"
URL:            http://tcllib.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/tcllib/%{srcname}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
Requires:       tcl(abi) = 8.5 tk
BuildRequires:  tcl

%description
An extended widget set for Tcl/Tk.

%prep
%setup -q -n %{srcname}-%{version}
%{__sed} -i 's/\r//' LICENSE.txt

%install
rm -rf $RPM_BUILD_ROOT
# Don't bother with the included configure script and Makefile.  They
# are missing a lot of pieces and won't work at all.  Installation is
# pretty simple, so we can just do it here manually.
mkdir -p $RPM_BUILD_ROOT/%{tcl_sitelib}/%{name}%{version}/
mkdir $RPM_BUILD_ROOT/%{tcl_sitelib}/%{name}%{version}/lang
mkdir $RPM_BUILD_ROOT/%{tcl_sitelib}/%{name}%{version}/images

install -m 0644 -pD *.tcl $RPM_BUILD_ROOT/%{tcl_sitelib}/%{name}%{version}/
install -m 0644 -pD lang/*.rc $RPM_BUILD_ROOT/%{tcl_sitelib}/%{name}%{version}/lang/
install -m 0644 -pD images/*.gif images/*.xbm $RPM_BUILD_ROOT/%{tcl_sitelib}/%{name}%{version}/images/


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{tcl_sitelib}/%{name}%{version}
%doc README.txt LICENSE.txt
%doc BWman/*.html

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2-1m)
- update to 1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-2m)
- rebuild against rpm-4.6

* Sun Apr 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.0-1m)
- import from Fedora

* Fri Jan  3 2008 Marcela Maslanova <mmaslano@redhat.com> 1.8.0-3
- rebuild with new tcl8.5, changed abi in spec

* Wed Aug 22 2007 Wart <wart at kobold.org> 1.8.0-2
- License tag clarification
- Move files to a tcl-specific directory for faster loading

* Thu Oct 19 2006 Wart <wart at kobold.org> 1.8.0-1
- Update to 1.8.0
- Remove patch that was accepted upstream

* Mon Aug 28 2006 Wart <wart at kobold.org> 1.7.0-4
- Rebuild for Fedora Extras

* Fri Aug 11 2006 Wart <wart at kobold.org> 1.7.0-3
- Add patch for adding a color selector to the font dialog

* Sat Dec 10 2005 Wart <wart at kobold.org> 1.7.0-2
- added dist tag to release tag.

* Sat Dec 10 2005 Wart <wart at kobold.org> 1.7.0-1
- Initial spec file.
