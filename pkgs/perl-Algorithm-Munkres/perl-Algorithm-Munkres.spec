%global         momorel 3

Name:           perl-Algorithm-Munkres
Version:        0.08
Release:        %{momorel}m%{?dist}
Summary:        Perl extension for Munkres' solution to
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Algorithm-Munkres/
Source0:        http://www.cpan.org/authors/id/T/TP/TPEDERSE/Algorithm-Munkres-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Assignment Problem: Given N jobs, N workers and the time taken by    each
worker to complete a job then how should the assignment of a    Worker to a
Job be done, so as to minimize the time taken.

%prep
%setup -q -n Algorithm-Munkres-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Algorithm/Munkres.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-2m)
- rebuild against perl-5.18.2

* Tue Sep 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
