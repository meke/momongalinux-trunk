%global momorel 6

Summary: A collection of CD/DVD utilities
Name: cdrkit
Version: 1.1.11
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://cdrkit.org/
Source0: http://cdrkit.org/releases/cdrkit-%{version}.tar.gz
NoSource: 0
Patch1: cdrkit-1.1.8-werror.patch
Patch2: cdrkit-1.1.9-efi-boot.patch
Patch4: cdrkit-1.1.9-no_mp3.patch
Patch5: cdrkit-1.1.9-buffer_overflow.patch
Patch6: cdrkit-1.1.10-build-fix.patch
Patch7: cdrkit-1.1.11-manpagefix.patch
Patch8: cdrkit-1.1.11-rootstat.patch
Patch9: cdrkit-1.1.11-usalinst.patch
Patch10: cdrkit-1.1.11-readsegfault.patch
Patch11: cdrkit-1.1.11-format.patch
Patch12: cdrkit-1.1.11-handler.patch

BuildRequires: cmake libcap-devel zlib-devel perl file-devel bzip2-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
cdrkit is a collection of CD/DVD utilities.

%package -n wodim
Summary: A command line CD/DVD recording program
Group: Applications/Archiving
Obsoletes: dvdrecord <= 0.1.5
Provides: dvdrecord = 0.1.5.1
Obsoletes: cdrecord < 2.01-12
Provides: cdrecord = 2.01-12
Obsoletes: cdrtools
Provides: cdrtools
Obsoletes: cdrtools-devel
Requires(preun): chkconfig coreutils
Requires(post): chkconfig coreutils

%description -n wodim
Wodim is an application for creating audio and data CDs. Wodim
works with many different brands of CD recorders, fully supports
multi-sessions and provides human-readable error messages.

%package -n genisoimage
Summary: Creates an image of an ISO9660 filesystem
Group: Applications/System
Obsoletes: mkisofs < 2.01-12
Provides: mkisofs = 2.01-12
Provides: mkhybrid
Obsoletes: mkhybrid
Requires: perl >= 4:5.8.1
Requires(preun): chkconfig coreutils
Requires(post): chkconfig coreutils

%description -n genisoimage
The genisoimage program is used as a pre-mastering program; i.e., it
generates the ISO9660 filesystem. Genisoimage takes a snapshot of
a given directory tree and generates a binary image of the tree
which will correspond to an ISO9660 filesystem when written to
a block device. Genisoimage is used for writing CD-ROMs, and includes
support for creating bootable El Torito CD-ROMs.

Install the genisoimage package if you need a program for writing
CD-ROMs.

%package -n icedax
Group: Applications/Multimedia
Summary: A utility for sampling/copying .wav files from digital audio CDs
Obsoletes: cdda2wav < 2.01-12
Provides: cdda2wav = 2.01-12
Requires(preun): chkconfig coreutils
Requires(post): chkconfig coreutils

%description -n icedax
Icedax is a sampling utility for CD-ROM drives that are capable of
providing a CD's audio data in digital form to your host. Audio data
read from the CD can be saved as .wav or .sun format sound files.
Recording formats include stereo/mono, 8/12/16 bits and different
rates. Icedax can also be used as a CD player.

%package -n libusal-devel
Summary: Development files for libusal
Group: Development/Libraries
Provides: libusal-static = %{version}-%{release}
Requires(preun): chkconfig coreutils
Requires(post): chkconfig coreutils

%description -n libusal-devel
The libusal-devel package contains libraries and header files
for developing applications that use libusal.

%prep
%setup -q 
%patch1 -p1 -b .werror
%patch2 -p1 -b .efi
#%patch4 -p1 -b .no_mp3
%patch5 -p1 -b .buffer_overflow
%patch6 -p1 -b .build-fix
%patch7 -p1 -b .manpagefix
%patch8 -p1 -b .rootstat
%patch9 -p1 -b .usalinst
%patch10 -p1 -b .readsegfault
%patch11 -p1 -b .format
%patch12 -p1 -b .handler

find . -type f -print0 | xargs -0 perl -pi -e 's#/usr/local/bin/perl#/usr/bin/perl#g'
find doc -type f -print0 | xargs -0 chmod a-x 

%build
mkdir build
pushd build

export CXXFLAGS="$CFLAGS"
export FFLAGS="$CFLAGS"

%cmake .. \
        -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
        -DBUILD_SHARED_LIBS:BOOL=ON \
        -DHAVE_SYS_CAPABILITY_H:INTERNAL=1

make VERBOSE=1 %{?_smp_mflags}
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
pushd build
make install DESTDIR=%{buildroot}

perl -pi -e 's#^require v5.8.1;##g' %{buildroot}%{_bindir}/dirsplit
ln -s genisoimage %{buildroot}%{_bindir}/mkisofs
ln -s genisoimage %{buildroot}%{_bindir}/mkhybrid
ln -s icedax %{buildroot}%{_bindir}/cdda2wav
ln -s wodim %{buildroot}%{_bindir}/cdrecord
ln -s wodim %{buildroot}%{_bindir}/dvdrecord

popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -n wodim
link=`readlink %{_bindir}/cdrecord`
if [ "$link" == "%{_bindir}/wodim" ]; then
        rm -f %{_bindir}/cdrecord
fi
link=`readlink %{_bindir}/dvdrecord`
if [ "$link" == "wodim" ]; then
        rm -f %{_bindir}/dvdrecord
fi

%{_sbindir}/alternatives --install %{_bindir}/cdrecord cdrecord \
               %{_bindir}/wodim 50 \
       --slave %{_mandir}/man1/cdrecord.1.bz2 cdrecord-cdrecordman \
               %{_mandir}/man1/wodim.1.bz2 \
       --slave %{_bindir}/dvdrecord cdrecord-dvdrecord %{_bindir}/wodim \
       --slave %{_mandir}/man1/dvdrecord.1.bz2 cdrecord-dvdrecordman \
               %{_mandir}/man1/wodim.1.bz2 \
       --slave %{_bindir}/readcd cdrecord-readcd %{_bindir}/readom \
       --slave %{_mandir}/man1/readcd.1.bz2 cdrecord-readcdman \
               %{_mandir}/man1/readom.1.bz2 

%preun -n wodim
if [ $1 = 0 ]; then
        %{_sbindir}/alternatives --remove cdrecord %{_bindir}/wodim
fi

%post -n genisoimage
link=`readlink %{_bindir}/mkisofs`
if [ "$link" == "genisoimage" ]; then
	rm -f %{_bindir}/mkisofs
fi

%{_sbindir}/alternatives --install %{_bindir}/mkisofs mkisofs \
               %{_bindir}/genisoimage 50 \
       --slave %{_mandir}/man1/mkisofs.1.bz2 mkisofs-mkisofsman \
               %{_mandir}/man1/genisoimage.1.bz2 \
       --slave %{_bindir}/mkhybrid mkisofs-mkhybrid %{_bindir}/genisoimage

%preun -n genisoimage
if [ $1 = 0 ]; then
        %{_sbindir}/alternatives --remove mkisofs %{_bindir}/genisoimage
fi

%post -n icedax
link=`readlink %{_bindir}/cdda2wav`
if [ "$link" == "icedax" ]; then
        rm -f %{_bindir}/cdda2wav
fi
%{_sbindir}/alternatives --install %{_bindir}/cdda2wav cdda2wav \
               %{_bindir}/icedax 50 \
       --slave %{_mandir}/man1/cdda2wav.1.bz2 cdda2wav-cdda2wavman \
               %{_mandir}/man1/icedax.1.bz2 

%preun -n icedax
if [ $1 = 0 ]; then
        %{_sbindir}/alternatives --remove cdda2wav %{_bindir}/icedax
fi

%files -n wodim
%defattr(-,root,root)
%doc Changelog COPYING FAQ FORK START
%doc doc/READMEs doc/wodim
%{_bindir}/devdump
%{_bindir}/wodim
%ghost %{_bindir}/cdrecord
%ghost %{_bindir}/dvdrecord
%{_bindir}/readom
%{_sbindir}/netscsid
%{_mandir}/man1/devdump.*
%{_mandir}/man1/wodim.*
%{_mandir}/man1/readom.*

%files -n icedax
%defattr(-,root,root)
%doc doc/icedax COPYING
%{_bindir}/icedax
%ghost %{_bindir}/cdda2wav
%{_bindir}/cdda2mp3
%{_bindir}/cdda2ogg
%{_mandir}/man1/icedax.*
%{_mandir}/man1/cdda2ogg.*
%{_mandir}/man1/list_audio_tracks.*

%files -n genisoimage
%defattr(-,root,root)
%doc doc/genisoimage COPYING
%{_bindir}/mkisofs
%{_bindir}/mkhybrid
%{_bindir}/genisoimage
%{_bindir}/isodebug
%{_bindir}/isodump
%{_bindir}/isoinfo
%{_bindir}/isovfy
%{_bindir}/dirsplit
%{_bindir}/pitchplay
%{_bindir}/readmult
%{_mandir}/man5/genisoimagerc.*
%{_mandir}/man1/genisoimage.*
%{_mandir}/man1/isodebug.*
%{_mandir}/man1/isodump.*
%{_mandir}/man1/isoinfo.*
%{_mandir}/man1/isovfy.*
%{_mandir}/man1/dirsplit.*
%{_mandir}/man1/pitchplay.*
%{_mandir}/man1/readmult.*

%files -n libusal-devel
%defattr(-,root,root)
%doc Changelog COPYING FAQ FORK START
%{_libdir}/libusal.a
%{_libdir}/librols.a
%{_includedir}/usal

%changelog
* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-6m)
- modify Requires of package libusal-devel

* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-5m)
- fix build on x86_64

* Sun Jul 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-4m)
- import fedora patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.11-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.11-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.11-1m)
- update 1.1.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.10-2m)
- full rebuild for mo7 release

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.10-1m)
- update 1.1.10

* Mon Nov 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-5m)
- [BUILD FIX] import cmake2.8-build.patch from cooker
 +* Thu Oct 08 2009 Frederic Crozat <fcrozat@mandriva.com> 1.1.9-3mdv2010.0
 ++ Revision: 456119
 +  + Helio Chissini de Castro <helio@mandriva.com>
 +    - Fix for new cmake 2.8 behavior

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-3m)
- apply glibc210 patch and so on from Rawhide (1.1.9-10)

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9-2m)
- fix build on x86_64

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-1m)
- update to 1.1.9
-- import Patch1,2 from Fedora 11 (1.1.9-4)

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-4m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-3m)
- rebuild against rpm-4.6

* Fri Jul  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-2m)
- fix broken alternatives

* Tue May 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-1m)
- udate 1.1.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.3-2m)
- rebuild against perl-5.10.0-1m

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- version down to 1.1.3 (rewind drive detection)
- apply opensuse's eltorito.patch

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-4m)
- fix Provides and Obsoletes

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-3m)
- add readcd and list_audio_tracks

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-2m)
- modify Provides and Obsoletes for smooth upgrading

* Fri Jun  8 2007 Masahiro Takahata <takahata@momogna-linux.org>
- (1.1.6-1m)
- import from Fedora

* Mon Apr 23 2007 Harald Hoyer <harald@redhat.com> - 1.1.2-4
- bump obsoletes/provides

* Tue Feb 27 2007 Harald Hoyer <harald@redhat.com> - 1.1.2-3
- applied specfile changes as in bug #224365

* Wed Jan 24 2007 Harald Hoyer <harald@redhat.com> - 1.1.2-1
- version 1.1.2
