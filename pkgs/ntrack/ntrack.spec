%global         momorel 1
%global         qtver 4.7.4

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           ntrack
Version:        016
Release:        %{momorel}m%{?dist}
License:        GPLv3 and LGPLv3
Summary:        Network status tracking library
Group:          System Environment/Libraries
Source0:        http://launchpad.net/ntrack/main/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:       0
#Patch0:         %{name}-lp-755608.patch
#Patch1:         %{name}-link.patch
URL:            http://launchpad.net/ntrack
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libnl3-devel
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  pygobject-devel
BuildRequires:  qt-devel >= %{qtver}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ntrack aims to be a lightweight and easy to use library for
application developers that want to get events on network online
status changes such as online, offline or route changes.

%package devel
Summary:        Header files for ntrack library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-qt = %{version}-%{release}

%description devel
Header files for ntrack library.

%package qt
Summary:        Qt4 bindings for ntrack library
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description qt
Qt4 bindings for ntrack library.

%package qt-devel
Summary:        Headers and libraries for ntrack-qt
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-qt = %{version}-%{release}

%description qt-devel
Headers and libraries for ntrack-qt

%package -n python-ntrack
Summary:        python bindings for ntrack library
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description -n python-ntrack
Python bindings for ntrack library.

%prep
%setup -q

#%patch0 -p0 -b .lp-755608
#%patch1 -p1 -b .link

# for Patch1
#mkdir -p m4
#./autogen.sh

%build
CFLAGS="%{optflags} -std=c99 -D_GNU_SOURCE=1"
%configure --disable-static

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

find %{buildroot} -type f -name "*.la" | xargs rm -f
rm -rf %{buildroot}%{_docdir}/%{name}

%check
make check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun	-p /sbin/ldconfig

%post qt -p /sbin/ldconfig

%postun	qt -p /sbin/ldconfig

%files
%doc AUTHORS COPYING COPYING.LESSER ChangeLog INSTALL NEWS README
%defattr(-,root,root,-)
%{_libdir}/libntrack.so.*
%{_libdir}/libntrack-glib.so.*
%{_libdir}/libntrack-gobject.so.*
%{_libdir}/%{name}

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/common
%{_includedir}/%{name}/glib
%{_includedir}/%{name}/gobject
%{_libdir}/libntrack.so
%{_libdir}/libntrack-glib.so
%{_libdir}/libntrack-gobject.so
%{_libdir}/pkgconfig/libntrack.pc
%{_libdir}/pkgconfig/libntrack-glib.pc
%{_libdir}/pkgconfig/libntrack-gobject.pc

%files qt
%defattr(-,root,root,-)
%{_libdir}/libntrack-qt4.so.*

%files qt-devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/qt4
%{_libdir}/libntrack-qt4.so
%{_libdir}/pkgconfig/libntrack-qt4.pc

%files -n python-ntrack
%defattr(-,root,root,-)
%{python_sitearch}/pyntrack.so

%changelog
* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (016-1m)
- update 016 for libnl3-3.2.7
-- see https://bugs.launchpad.net/ntrack/+bug/879141
- add %%check
- drop two patches, which seem to have been merged already

* Wed Nov  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (014-3m)
- import link.patch from ubuntu, thanks to pulsar

* Mon Nov  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (014-2m)
- add qt-devel subpackage

* Sun Nov  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (014-1m)
- initial build for Momonga Linux
