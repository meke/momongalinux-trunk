%global momorel 22
%global pythonver 2.7

%define python_sitelib %(%{__python} -c "from distutils import sysconfig; print sysconfig.get_python_lib()")
%define python_sitearch %(%{__python} -c 'from distutils import sysconfig; print sysconfig.get_python_lib(1)')

Summary: Tool to access devices via the OBEX protocol
Name: obexftp
Version: 0.23
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/File
URL: http://dev.zuckschwerdt.org/openobex/wiki/ObexFtp
Source0: http://dl.sourceforge.net/project/openobex/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-norpath.patch
Patch1: obexftp-0.23-ruby19.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: bluez-libs-devel >= 4.0
BuildRequires: gettext-devel
BuildRequires: libtool
BuildRequires: libusb-devel
BuildRequires: openobex-devel
BuildRequires: perl-devel >= 5.16.0
BuildRequires: pkgconfig
BuildRequires: python-devel >= %{pythonver}
BuildRequires: ruby-devel >= 1.9.2

%description
The overall goal of this project is to make mobile devices featuring the OBEX
protocol and adhering to the OBEX FTP standard accessible by an open source
implementation. The common usage for ObexFTP is to access your mobile phones
memory to store and retrieve e.g. your phonebook, logos, ringtones, music,
pictures and alike.

%package libs
Summary: Shared libraries for obexftp
Group: System Environment/Libraries

%description libs
The overall goal of this project is to make mobile devices featuring the OBEX
protocol and adhering to the OBEX FTP standard accessible by an open source
implementation. The common usage for ObexFTP is to access your mobile phones
memory to store and retrieve e.g. your phonebook, logos, ringtones, music,
pictures and alike.

%package devel
Summary: Header files and libraries for obexftp
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: openobex-devel
Requires: pkgconfig

%description devel
This package contains the header files, static libraries and development
documentation for obexftp. If you like to develop programs using %{name},
you will need to install obexftp-devel.

%package -n perl-%{name}
Summary: Perl library to access devices via the OBEX protocol
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description -n perl-%{name}
The overall goal of this project is to make mobile devices featuring the OBEX
protocol and adhering to the OBEX FTP standard accessible by an open source
implementation. The common usage for ObexFTP is to access your mobile phones
memory to store and retrieve e.g. your phonebook, logos, ringtones, music,
pictures and alike.

%package -n python-%{name}
Summary: Python library to access devices via the OBEX protocol
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description -n python-%{name}
The overall goal of this project is to make mobile devices featuring the OBEX
protocol and adhering to the OBEX FTP standard accessible by an open source
implementation. The common usage for ObexFTP is to access your mobile phones
memory to store and retrieve e.g. your phonebook, logos, ringtones, music,
pictures and alike.

%package -n ruby-%{name}
Summary: Ruby library to access devices via the OBEX protocol
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description -n ruby-%{name}
The overall goal of this project is to make mobile devices featuring the OBEX
protocol and adhering to the OBEX FTP standard accessible by an open source
implementation. The common usage for ObexFTP is to access your mobile phones
memory to store and retrieve e.g. your phonebook, logos, ringtones, music,
pictures and alike.

%prep
%setup -q 

%patch0 -p1 -b .norpath
%patch1 -p1 -b .ruby19

autoreconf -f -i

%build
%configure --disable-rpath
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALLDIRS="vendor"

# get rid of *.la files
find %{buildroot} -name "*.la" -delete

# fix permission
chmod 755 %{buildroot}%{perl_vendorarch}/*/OBEXFTP/OBEXFTP.so

# clean up
rm -f %{buildroot}%{perl_vendorarch}/*/OBEXFTP/{.packlist,OBEXFTP.bs}
rm -f %{buildroot}%{perl_archlib}/perllocal.pod

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog NEWS README THANKS TODO
%{_bindir}/%{name}
%{_bindir}/%{name}d
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}d.1*

%files libs
%defattr(-,root,root)
%{_libdir}/libbfb.so.*
%{_libdir}/libmulticobex.so.*
%{_libdir}/lib%{name}.so.*
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/bfb
%{_includedir}/multicobex
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/libbfb.a
%{_libdir}/libbfb.so
%{_libdir}/libmulticobex.a
%{_libdir}/libmulticobex.so
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%files -n perl-%{name}
%defattr(-,root,root)
%{perl_vendorarch}/OBEXFTP.pm
%{perl_vendorarch}/*/OBEXFTP

%files -n python-%{name}
%defattr(-,root,root)
%{python_sitearch}/%{name}
%{python_sitearch}/%{name}-*.egg-info

%files -n ruby-%{name}
%defattr(-,root,root)
%{ruby_sitearchdir}/%{name}.so

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@moonga-linuux.org>
- (0.23-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-12m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23-11m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-7m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-6m)
- rebuild against ruby-1.9.2

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.23-5m)
- split package libs

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-4m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-1m)
- update to 0.23
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-0.3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.23-0.2m)
- rebuild agaisst python-2.6.1-1m

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23-0.1m)
- update 0.23 alpha

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22-0.4.4m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-0.4.3m)
- add egg-info for new python

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22-0.4.2m)
- rebuild against gcc43

* Mon Feb  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-0.4.1m)
- update to 0.22-rc7
- update norpath.patch
- remove perl.patch
- License: GPLv2 and LGPL

* Mon Feb  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-0.3.4m)
- rebuild against perl-5.10.0

* Wed Jun 20 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.22-0.3.3m)
- rebuild against ruby-1.8.6-4m

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-0.3.2m)
- BuildRequires: libusb-devel

* Tue Jun 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-0.3.1m)
- initial package for kdebluetooth 1.0 Beta 3
- import norpath.patch and perl.patch from Fedora
- Summary and %%description are imported from Fedora
