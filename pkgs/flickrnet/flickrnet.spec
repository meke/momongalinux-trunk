%global momorel 5

%define         debug_package %{nil}

Name:           flickrnet
Version:        2.2
Release:	%{momorel}m%{?dist}
Summary:        .NET library to interact with the Flickr API
Group:          Development/Libraries
License:        LGPLv2
URL:            http://www.codeplex.com/FlickrNet
#http://flickrnet.codeplex.com/releases/6181/download/61452
Source0:        FlickrNet2.2-Src-48055.zip
Source1:        flickrnet.pc
Patch0:         assemblyinfo.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  mono-devel

ExclusiveArch: %ix86 x86_64 ppc ppc64 ia64 armv4l sparcv9 alpha s390 s390x

%description
The Flickr.Net API is a .NET Library for interacting with the Flickr API. It 
can be accessed from with any .NET language.

%package devel
Summary:        Development files for Flickr.Net
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
The Flickr.Net API is a .NET Library for interacting with the Flickr API. It 
can be accessed from any .NET language.

The %{name}-devel package contains development files for %{name}.

%prep
%setup -cn FlickrNet -q
cd FlickrNet
%patch0 -p1

sed -i 's|@LIBDIR@|%{_libdir}|g' %{SOURCE1}

%build
cd FlickrNet
gmcs -debug -target:library -out:FlickrNet.dll  -r:System.Web.dll -r:System.Drawing.dll *.cs

%install
rm -rf --preserve-root %{buildroot}
cd FlickrNet
gacutil -i FlickrNet.dll -package %{name} -root %{buildroot}%{_libdir}
install -m 0755 -d %{buildroot}%{_libdir}/pkgconfig
install -m 0644 -p %{SOURCE1} %{buildroot}%{_libdir}/pkgconfig/

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/mono/%{name}
%{_libdir}/mono/gac/FlickrNet

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-5m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-2m)
- full rebuild for mo7 release

* Sat Jun 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- import from fedora

* Mon Jun 07 2010 Paul Lange <palango@gmx.de> - 2.2-4
- Fix pkgconfig files.

* Mon Oct 26 2009 Dennis Gilmore <dennis@ausil.us> - 2.2-3
- enable sparcv9 s390 s390x

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jun 26 2009 Paul Lange <palango@gmx.de> - 2.2-1
- Update to upstream release 2.2.

* Thu Jun 25 2009 Juan Rodriguez <nushio@fedoraproject.org> - 2.1.5-3
- Fix libdir on x86_64

* Tue Jun 02 2009 Paul Lange <palango@gmx.de> - 2.1.5-2
enable ppc64 build

* Mon Jan 12 2009 Paul Lange <palango@gmx.de> 2.1.5-1
- Initial packaging (based on openSuse package)
