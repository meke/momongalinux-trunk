%global momorel 7
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m

Summary: A midi/karaoke player for KDE
Name: kmid
Version: 2.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://userbase.kde.org/KMid2
Source0: http://dl.sourceforge.net/project/%{name}2/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}-ja.po
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: alsa-lib-devel
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: gettext
BuildRequires: drumstick-devel
Obsoletes: %{name}2

%description
KMid is a MIDI/Karaoke player with KDE interface, based on the ALSA
sequencer.

%package devel
Summary: Development files for kmid
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files for kmid.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja~

# for Japanese
install -m 644 %{SOURCE1} po/ja.po

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING ChangeLog README TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/%{name}_alsa.so
%{_kde4_libdir}/kde4/%{name}_part.so
%{_kde4_libdir}/lib%{name}backend.so.*
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/%{name}_part
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.svgz
%{_kde4_datadir}/kde4/services/%{name}_alsa.desktop
%{_kde4_datadir}/kde4/services/%{name}_part.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_backend.desktop
%{_datadir}/dbus-1/interfaces/*
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/lib%{name}backend.so

%changelog
* Mon Jan 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-7m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-5m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-2m)
- rebuild against qt-4.6.3-1m

* Sun Jun 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- version 2.3.1
- update desktop.patch

* Tue Apr 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0
- revive devel package

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-1m)
- version 2.2.2
- merge kmid2
- use Japanese Translation from svn.kde.org/trunk/l10n-kde4/ for the moment
- add desktop.patch
- update %%description
- remove PEOPLE from %%doc and add TODO to %%doc
- remove merged docdir.patch
- remove and Obsoletes: kmid-devel
- Obsoletes: kmid2
- set NoSource: 0 again

* Tue Feb  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Thu Dec 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- initial build for Momonga Linux

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.20090629.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20090629.1m)
- update to 20090629 svn snapshot

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20090320.1m)
- update to 20090320 svn snapshot
- fix up documentation's directory
- remove merged cmake.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.20080516.2m)
- rebuild against rpm-4.6

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080516.1m)
- initial package for Momonga Linux
- import and modify Fedora's cmake.patch
