%global momorel 2

%global pyver 2.7
%global pysite %{_libdir}/python%{pyver}/site-packages
%global pyinc /usr/include/python%{pyver}

%global srcname Imaging

Summary: The Python Imaging Library
Name: python-imaging
Version: 1.1.7
Release: %{momorel}m%{?dist}
License: see "README"
Group: Development/Languages
Source0: http://effbot.org/downloads/%{srcname}-%{version}.tar.gz 
NoSource: 0
Patch3: Imaging-1.1.7-no-xv.patch
Patch4: %{name}-giftrans.patch
Patch10: %{name}-%{version}-freetype.patch
URL: http://www.pythonware.com/products/pil/
Requires: zlib, libjpeg 
Requires: python  >= %{pyver}
BuildRequires: tcl >= 8.3.0 tk >= 8.3.0
BuildRequires: python >= %{pyver}
BuildRequires: libjpeg >= 8a
BuildRequires: freetype-devel >= 2.5.3
Provides: Imaging
Obsoletes: Imaging
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Python Imaging Library adds image processing capabilities to your
Python interpreter.  This library provides extensive file format
support, an efficient internal representation, and fairly powerful
image processing capabilities.

%package devel
Summary: Development files for python-imaging.
Group: Development/Languages
Requires: %{name} = %{version}-%{release}, python-devel

%description devel
Development files for python-imaging.

%prep
%setup -q -n %{srcname}-%{version}
%patch3 -p1 -b .no-xv~
%patch4 -p1
%patch10 -p1 -b .freetype

rm -rf %{buildroot}
mkdir -p %{buildroot}

mkdir -p %{buildroot}%{pysite}/PIL
mkdir -p %{buildroot}%{pyinc}

%build
python setup.py build_ext -i

%install
python setup.py install --root %{buildroot}
cp libImaging/*.h %{buildroot}%{pyinc}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGES
%{pysite}/PIL.pth
%{pysite}/PIL
%{_bindir}/*

%files devel
%defattr (-,root,root)
%doc Docs Scripts Images Sane
%{pyinc}/*

%changelog
* Mon Apr 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-2m)
- enable to build with freetype-2.5.3

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.7-1m)
- update 1.1.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-11m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-10m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.6-8m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-7m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-6m)
- update Patch3 for fuzz=0

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.6-5m)
- rebuild agaisst python-2.6.1-1m

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.6-4m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-2m)
- %%NoSource -> NoSource

* Tue Apr 10 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-4m)
- rebuild against python-2.5

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-3m)
- rebuild against python-2.4.2

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.5-2m)
- build fix for multilib arch.

* Thu Jul 28 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5 and renamed to python-imaging (same as FC)
- split devel package
- import python-imaging-no-xv.patch and python-imaging-giftrans.patch from FC Extra (python-imaging-1.1.4-9.src.rpm)
- remove old patches (Imaging-1.1.4-include.patch, Imaging-1.1.4-setup.patch, Imaging-tcltk-ver.patch)
- cleanup spec

* Wed Dec 15 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1.4-2m)
- python 2.3

* Sat Jun 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.4-1m)
- add Patch1

* Mon Nov 24 2003 Dai OKUYAMA <dai@ouchi.nahi.to>
- (kossori)
- change Source0 URL

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.3-3m)
- accept python not only 2.2 but 2.2.*

* Thu Oct 23 2003 zunda <zunda at freeshell.org>
- (1.1.3-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Mar 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.3-1m)
- version 1.1.3

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-4k)
- rebuild against for python 2.2

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- update to 1.1.2

* Sun Oct 14 2001 Masaru Sato <masachan@kondara.org>
- Modify BuildRoot

* Sun Jan 30 2000 KUSUNOKI Masanori <masanori@linux.or.jp>
- version 1.0

* Sun Mar  7 1999 MATSUMOTO Shoji <vine@flatout.org>
- make spec

