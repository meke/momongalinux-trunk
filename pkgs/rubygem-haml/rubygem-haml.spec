# Generated from haml-3.1.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname haml

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: An elegant, structured XHTML/XML templating engine
Name: rubygem-%{gemname}
Version: 3.1.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://haml-lang.com/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Haml (HTML Abstraction Markup Language) is a layer on top of XHTML or
XML
that's designed to express the structure of XHTML or XML documents
in a non-repetitive, elegant, easy way,
using indentation rather than closing tags
and allowing Ruby to be embedded with ease.
It was originally envisioned as a plugin for Ruby on Rails,
but it can function as a stand-alone templating engine.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/haml
%{_bindir}/html2haml
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.4-1m)
- update 3.1.4

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.3-1m)
- update 3.1.3

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.2-1m)
- update 3.1.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.23-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.23-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.23-1m)
- update 3.0.23

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.22-1m)
- update 3.0.22

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.16-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.16-1m)
- update 3.0.16

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.15-1m)
- Initial package for Momonga Linux
