%global momorel 1

%global xfce4ver 4.11.0
%global major 1.2

Summary: 	XFce4 clipboard history plugin
Name: 		xfce4-clipman-plugin
Version: 	1.2.6
Release:	%{momorel}m%{?dist}

Group: 		User Interface/Desktops
License:	GPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/xfce4-clipman-plugin
Source0:        http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
BuildRequires:	libXt-devel, gettext, perl-XML-Parser
BuildRequires:  libxml2-devel
#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  pkgconfig
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description 
A a clipboard history plugin for the panel of XFce4.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'
find %{buildroot} -name "*.la" -delete

desktop-file-install --vendor ""                          \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
  --add-only-show-in=XFCE 				  \
  --delete-original                                       \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/xfce4-clipman.desktop

desktop-file-install --vendor ""                          \
  --dir ${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart     \
  --add-only-show-in=XFCE 				  \
  --delete-original                                       \
  ${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart/%{name}-autostart.desktop

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
##%{_libexecdir}/xfce4/panel-plugins/%{name}
%config %{_sysconfdir}/xdg/autostart/%{name}-autostart.desktop
%config(noreplace) %{_sysconfdir}/xdg/xfce4/panel/xfce4-clipman-actions.xml
%{_bindir}/xfce4-clipman
%{_bindir}/xfce4-clipman-settings
%{_bindir}/xfce4-popup-clipman
%{_datadir}/appdata/xfce4-clipman.appdata.xml
%{_datadir}/applications/xfce4-clipman.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/xfce4/panel/plugins/%{name}.desktop
%{_libdir}/xfce4/panel/plugins/libclipman.so
%{_datadir}/locale/*/*/*


%changelog
* Tue Jun  3 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5
- build against xfce4-4.11.0

* Sat Sep  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-6m)
- rebuild against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-1m)
- update
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- add only show in XFCE

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0
- License: GPLv2+

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-1m)
- update
- rebuild against xfce4-4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-4m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-7m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-6m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-5m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-4m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.1-3m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-2m)
- rebuild against xfce4 4.1.90

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.1-1m)

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.0-4m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.4.0-3m)
- rebuild against for libxml2-2.6.8

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against xfce4 4.0.3

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0
- rebuild against xfce4 4.0.2

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-1m)
- update
- rebuild against xfce4 4.0.1

* Wed Oct 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.1-2m)
- revise License:

* Tue Oct 14 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.1-1m)
- first import to Momonga
