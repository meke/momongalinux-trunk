%global momorel 4
%global prever 2
%global betaver 7

Summary: HTML to PostScript converter
Name: html2ps
Version: 1.0
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPLv2+
URL: http://user.it.uu.se/~jan/html2ps.html
Group: Applications/Publishing
Source0: http://user.it.uu.se/~jan/%{name}-%{version}b%{betaver}.tar.gz
NoSource: 0
Source1: xhtml2ps.desktop
Source10: hi16-apps-xhtml2ps.png
Source11: hi22-apps-xhtml2ps.png
Source12: hi32-apps-xhtml2ps.png
Source13: hi48-apps-xhtml2ps.png
Source14: hi64-apps-xhtml2ps.png
Source15: hi128-apps-xhtml2ps.png
Patch0: http://ftp.de.debian.org/debian/pool/main/h/%{name}/%{name}_%{version}b5-5.diff.gz
# use xdg-open in xhtml2ps
Patch1: %{name}-%{version}b5-xdg-open.patch
# patch config file from debian to use dvips, avoid using weblint 
# don't set letter as default page type, paperconf will set the default
Patch2: %{name}-%{version}b5-config.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: ghostscript
Requires: libpaper
Requires: perl-libwww-perl
Requires: tetex
Requires: tetex-dvipsk
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: sed

%description
An HTML to PostScript converter written in Perl.
* Many possibilities to control the appearance. 
* Support for processing multiple documents.
* A table of contents can be generated.
* Configurable page headers/footers.
* Automatic hyphenation and text justification can be selected.

%package -n xhtml2ps
Summary: GUI frontend for html2ps
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: xdg-utils

%description -n xhtml2ps
X-html2ps is freely-available GUI frontend for html2ps, a HTML-to-PostScript
converter.

%prep
%setup -q -n %{name}-%{version}b%{betaver}

%patch0 -p1
%patch1 -p1 -b .xdg-open
%patch2 -p1 -b .config

# convert README to utf8
iconv -f latin1 -t utf8 < README > README.utf8
touch -c -r README README.utf8
mv README.utf8 README

patch -p1 < debian/patches/01_manpages.dpatch
# ugly
sed -i -e "s|$globrc='/it/sw/share/www/lib/html2ps/html2psrc';|$globrc='/opt/misc/lib/html2ps/html2psrc';|" html2ps
sed -i -e "s|$ug='/it/sw/share/www/lib/html2ps/html2ps.html';|$ug='/opt/misc/lib/html2ps/html2ps.html';|" html2ps
patch -p1 < debian/patches/03_html2ps.dpatch

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing...
mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
mkdir -p %{buildroot}%{_mandir}/man{1,5}
mkdir -p %{buildroot}%{_datadir}/pixmaps

# install html2ps
sed -e 's;/etc/html2psrc;%{_sysconfdir}/html2psrc;' \
    -e 's;/usr/share/doc/html2ps;%{_docdir}/%{name}-%{version};' \
        html2ps > %{buildroot}%{_bindir}/html2ps

# set permission
chmod 755 %{buildroot}%{_bindir}/html2ps

# install ma files
install -m 644 html2ps.1 %{buildroot}%{_mandir}/man1/
install -m 644 html2psrc.5 %{buildroot}%{_mandir}/man5/

# install html2psrc
sed -e 's;/usr/bin;%{_bindir};' \
    -e 's;/usr/share/texmf-texlive;%{_datadir}/texmf;' \
    debian/config/html2psrc > %{buildroot}%{_sysconfdir}/html2psrc

# install xhtml2ps
install -m 755 contrib/xhtml2ps/xhtml2ps %{buildroot}%{_bindir}/

# install desktop file
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icons
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/xhtml2ps.png
install -m 644 %{SOURCE11} %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/xhtml2ps.png
install -m 644 %{SOURCE12} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/xhtml2ps.png
install -m 644 %{SOURCE13} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/xhtml2ps.png
install -m 644 %{SOURCE14} %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/xhtml2ps.png
install -m 644 %{SOURCE15} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/xhtml2ps.png

# link icon
ln -s ../icons/hicolor/48x48/apps/xhtml2ps.png %{buildroot}%{_datadir}/pixmaps/xhtml2ps.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README sample html2ps.html
%config(noreplace) %{_sysconfdir}/html2psrc
%{_bindir}/html2ps
%{_mandir}/man1/html2ps.1*
%{_mandir}/man5/html2psrc.5*

%files -n xhtml2ps
%doc contrib/xhtml2ps/README contrib/xhtml2ps/LICENSE
%{_bindir}/xhtml2ps
%{_datadir}/applications/*xhtml2ps.desktop
%{_datadir}/icons/hicolor/*/apps/xhtml2ps.png
%{_datadir}/pixmaps/xhtml2ps.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.2.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.2.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.2.2m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.2.1m)
- [SECURITY] arbitrary file disclosure in SSI directives (fixed in 1.0b6)
- update to 1.0b7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.3m)
- add icons for menu

* Sat Sep 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.1.2m)
- use package name 

* Sat Sep 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.1m)
- initial package for kmymoney-1.0.1
- import 3 patches from Debian and Fedora
