%global momorel 1

Summary: machine readable format
Name: gobject-introspection
Version: 1.40.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Libraries
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.40/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.28.0
BuildRequires: gtk-doc >= 1.19
BuildRequires: dbus-glib-devel
BuildRequires: libffi-devel

Obsoletes: gir-repository

%description
There's an XML format called GIR used by GObjectIntrospection.  The
purpose of it is to provide a common structure to access the complete
available API that a library or other unit of code exports.  It is
meant to be language agnostic using namespaces to separate core,
language or library specific functionality.  There are currently only
C based tools that work on the format, but it's meant to be usable to
use in other situations, for instance to/from another set of
languages.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --enable-silent-rules \
	--enable-gtk-doc \
	--disable-static 
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%check
make check

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS CONTRIBUTORS COPYING COPYING.GPL COPYING.LGPL ChangeLog NEWS README TODO
%{_bindir}/g-ir-compiler
%{_bindir}/g-ir-doc-tool
%{_bindir}/g-ir-generate
%{_bindir}/g-ir-scanner
%{_bindir}/g-ir-annotation-tool
#%{_bindir}/g-ir-doc-tool
%{_libdir}/libgirepository-1.0.so.*
%exclude %{_libdir}/*.la

%{_libdir}/girepository-1.0
%{_libdir}/gobject-introspection
%{_datadir}/gir-1.0
%{_mandir}/man1/g-ir-*.1.*

%files devel
%defattr(-, root, root)
%{_includedir}/gobject-introspection-1.0
%{_libdir}/libgirepository-1.0.so
%{_libdir}/pkgconfig/gobject-introspection-1.0.pc
%{_libdir}/pkgconfig/gobject-introspection-no-export-1.0.pc
%{_datadir}/aclocal/introspection.m4
%{_datadir}/gobject-introspection-1.0/Makefile.introspection
%{_datadir}/gtk-doc/html/gi

# test for devel package
%{_datadir}/gobject-introspection-1.0/tests/annotation.c
%{_datadir}/gobject-introspection-1.0/tests/annotation.h
%{_datadir}/gobject-introspection-1.0/tests/drawable.c
%{_datadir}/gobject-introspection-1.0/tests/drawable.h
%{_datadir}/gobject-introspection-1.0/tests/everything.c
%{_datadir}/gobject-introspection-1.0/tests/everything.h
%{_datadir}/gobject-introspection-1.0/tests/foo.c
%{_datadir}/gobject-introspection-1.0/tests/foo.h
%{_datadir}/gobject-introspection-1.0/tests/gimarshallingtests.c
%{_datadir}/gobject-introspection-1.0/tests/gimarshallingtests.h
%{_datadir}/gobject-introspection-1.0/tests/regress.c
%{_datadir}/gobject-introspection-1.0/tests/regress.h
%{_datadir}/gobject-introspection-1.0/tests/utility.c
%{_datadir}/gobject-introspection-1.0/tests/utility.h
%{_datadir}/gobject-introspection-1.0/tests/warnlib.c
%{_datadir}/gobject-introspection-1.0/tests/warnlib.h
%{_datadir}/gobject-introspection-1.0/gdump.c

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.40.0-1m)
- update to 1.40.0

* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.34.2-1m)
- update to 1.34.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.34.1-1m)
- update to 1.34.1

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34.0-1m)
- update to 1.34.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.14-1m)
- update to 1.33.14

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.10-1m)
- update to 1.33.10

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.9-1m)
- update to 1.33.9

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.4-1m)
- update to 1.33.4

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33.2-1m)
- update to 1.33.2

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.30.0-1m)
- update to 1.30.0

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.17-1m)
- update to 1.29.17

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.7-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7

* Sat Apr  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Thu Feb 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Wed Feb  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.12-2m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.12-1m)
- update to 0.9.12

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.10-1m)
- update to 0.9.10

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6
-- disable-gtk-doc (for a while)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-2m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.14-1m)
- update to 0.6.14

* Sun May 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.11-2m)
- add BuildRequires

* Thu May 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.11-1m)
- update to 0.6.11

* Wed Apr 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.10-1m)
- update to 0.6.10

* Fri Mar 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.9-1m)
- update to 0.6.9

* Mon Mar 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.8-1m)
- update to 0.6.8

* Sat Dec 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.7-4m)
- add BuildPrereq: dbus-glib-devel

* Fri Dec 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.7-3m)
- add BuildPrereq: gtk-doc >= 1.12

* Tue Dec 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.7-2m)
- enable-gtk-doc

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.7-1m)
- update to 0.6.7

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5
-- disable-gtk-doc (need gtk-doc 1.2)

* Wed Sep  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-2m)
- to main

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- initial build
-- does not support libffi (gcc-libffi)
-- http://sourceware.org/libffi/
-- disable-gtk-doc (need gtk-doc 1.2)
