%global momorel 3

#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# 
# Since this spec file supports multiple distributions, ensure we
# use the correct group for each.
#

# SSLeay (required by ldirectord)
%global SSLeay perl-Net-SSLeay

# determine the ras-set to process based on configure invokation
%bcond_without rgmanager
%bcond_without linuxha

Name:		resource-agents
Summary:	Open Source HA Reusable Cluster Resource Scripts
Version:	3.9.2
Release:	%{momorel}m%{?dist}
License:	GPLv2+ and LGPLv2+
URL:		http://to.be.defined.com/
Group:		System Environment/Base
Source0:	%{name}-%{version}%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:-%{alphatag}}%{?dirty:-%{dirty}}.tar.bz2
Obsoletes:	heartbeat-resources <= %{version}
Provides:	heartbeat-resources = %{version}

## Setup/build bits
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

# Build dependencies
BuildRequires: automake autoconf pkgconfig
BuildRequires: perl python-devel
BuildRequires: libxslt glib2-devel
BuildRequires: which

BuildRequires: cluster-glue-libs-devel
BuildRequires: docbook-style-xsl docbook-dtds

## Runtime deps
## These apply to rgmanager agents only to guarantee agents
## are functional
%if %{with rgmanager}
# system tools shared by several agents
Requires: /bin/bash /bin/grep /bin/sed /bin/gawk
Requires: /bin/ps /usr/bin/pkill /bin/hostname
Requires: /sbin/fuser
Requires: /sbin/findfs /bin/mount

# fs.sh
Requires: /sbin/quotaon /sbin/quotacheck
Requires: /sbin/fsck
Requires: /usr/sbin/fsck.ext2 /usr/sbin/fsck.ext3 /usr/sbin/fsck.ext4
Requires: /usr/sbin/fsck.xfs

# ip.sh
Requires: /sbin/ip /usr/sbin/ethtool
Requires: /sbin/rdisc /usr/sbin/arping /bin/ping /bin/ping6

# lvm.sh
Requires: /usr/sbin/lvm

# netfs.sh
Requires: /sbin/mount.nfs /sbin/mount.nfs4 /usr/sbin/mount.cifs
Requires: /usr/sbin/rpc.nfsd /sbin/rpc.statd /usr/sbin/rpc.mountd
%endif

%description
A set of scripts to interface with several services to operate in a
High Availability environment for both Pacemaker and rgmanager
service managers.

%if %{with linuxha}
%package -n ldirectord
License:	GPLv2+
Summary:	A Monitoring Daemon for Maintaining High Availability Resources
Group:		System Environment/Daemons
Obsoletes:	heartbeat-ldirectord <= %{version}
Provides:	heartbeat-ldirectord = %{version}
Requires:       %{SSLeay} perl-libwww-perl perl-MailTools
Requires:       ipvsadm logrotate
Requires:	perl-Net-IMAP-Simple-SSL
Requires(post):	/sbin/chkconfig
Requires(preun):/sbin/chkconfig

%description -n ldirectord
The Linux Director Daemon (ldirectord) was written by Jacob Rief.
<jacob.rief@tiscover.com>

ldirectord is a stand alone daemon for monitoring the services on real
servers. Currently, HTTP, HTTPS, and FTP services are supported.
lditrecord is simple to install and works with the heartbeat code
(http://www.linux-ha.org/).

See 'ldirectord -h' and linux-ha/doc/ldirectord for more information.
%endif

%prep
%setup -q -n %{name}-%{version}%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:-%{alphatag}}%{?dirty:-%{dirty}}

%build
if [ ! -f configure ]; then
	./autogen.sh
fi

CFLAGS="$(echo '%{optflags}')"
%global conf_opt_rsctmpdir "--with-rsctmpdir=%{_var}/run/heartbeat/rsctmp"
%global conf_opt_fatal "--enable-fatal-warnings=no"

%if %{with rgmanager}
%global rasset rgmanager
%endif
%if %{with linuxha}
%global rasset linux-ha
%endif
%if %{with rgmanager} && %{with linuxha}
%global rasset all
%endif

export CFLAGS

%configure \
	%{?conf_opt_rsctmpdir:%conf_opt_rsctmpdir} \
	%{conf_opt_fatal} \
	--with-pkg-name=%{name} \
	--with-ras-set=%{rasset}

%if %{defined jobs}
JFLAGS="$(echo '-j%{jobs}')"
%else
JFLAGS="$(echo '%{_smp_mflags}')"
%endif

make $JFLAGS

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%if %{with linuxha}
%if 0%{?suse_version}
test -d %{buildroot}/sbin || mkdir %{buildroot}/sbin
(
  cd %{buildroot}/sbin
  ln -sf /%{_sysconfdir}/init.d/ldirectord rcldirectord 
) || true
%endif
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.GPLv3 ChangeLog
%if %{with linuxha}
%doc doc/README.webapps
%doc %{_datadir}/%{name}/ra-api-1.dtd
%endif

%if %{with rgmanager}
%{_datadir}/cluster
%{_sbindir}/rhev-check.sh
%endif

%if %{with linuxha}
%dir /usr/lib/ocf
%dir /usr/lib/ocf/resource.d
%dir /usr/lib/ocf/lib

/usr/lib/ocf/lib/heartbeat

/usr/lib/ocf/resource.d/heartbeat
%exclude /usr/lib/ocf/resource.d/heartbeat/ldirectord
%if %{with rgmanager}
/usr/lib/ocf/resource.d/redhat
%endif

%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/ocft
%{_datadir}/%{name}/ocft/configs
%{_datadir}/%{name}/ocft/caselib
%{_datadir}/%{name}/ocft/README
%{_datadir}/%{name}/ocft/README.zh_CN

%{_sbindir}/ocf-tester
%{_sbindir}/ocft
%{_sbindir}/sfex_init
%{_sbindir}/sfex_stat

%{_includedir}/heartbeat

%dir %{_var}/run/heartbeat/rsctmp

%{_mandir}/man7/*.7*
%{_mandir}/man8/ocf-tester.8*
%{_mandir}/man8/sfex_init.8*

# For compatability with pre-existing agents
%{_sysconfdir}/ha.d/shellfuncs

%{_libdir}/heartbeat

%if %{with rgmanager}
%post -n resource-agents
ccs_update_schema > /dev/null 2>&1 ||:
%endif

%preun -n ldirectord
/sbin/chkconfig --del ldirectord
%postun -n ldirectord -p /sbin/ldconfig
%post -n ldirectord
/sbin/chkconfig --add ldirectord

%files -n ldirectord
%defattr(-,root,root)
%{_sbindir}/ldirectord
%doc ldirectord/ldirectord.cf COPYING
%{_mandir}/man8/ldirectord.8*
%config(noreplace) %{_sysconfdir}/logrotate.d/ldirectord
%dir %{_sysconfdir}/ha.d/resource.d
%{_sysconfdir}/ha.d/resource.d/ldirectord
%{_sysconfdir}/init.d/ldirectord
/usr/lib/ocf/resource.d/heartbeat/ldirectord
%endif

%changelog
* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.2-3m)
- update 3.9.2
- support UserMove env

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.2-2m)
- rebuild for glib 2.33.2

* Sat Oct 29 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.9.2-1m)
- version up 3.9.2

* Fri Oct 28 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.3-8m)
- remove Requires: mount

* Thu Apr 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.3-7m)
- set --enable-libnet=no to Cluster-Resource-Agents for the moment
- http://bugs.gentoo.org/348783
- this package should be updated ASAP
- and do not forget remove --enable-libnet=no

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-4m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.3-3m)
- stop auto starting service at initial system startup

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-1m)
- sync with Rawhide
-- 
-- * Wed Sep 23 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.3-1
-- - New rgmanager resource agents upstream release
-- 
-- * Thu Aug 20 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.1-1
-- - New rgmanager resource agents upstream release
-- 
-- * Tue Aug 18 2009 Andrew Beekhof <andrew@beekhof.net> - 3.0.0-16
-- - Create an ldirectord package
-- - Update Pacameker agents to upstream version: 2198dc90bec4
--   + Build: Import ldirectord.
--   + Ensure HA_VARRUNDIR has a value to substitute
--   + High: Add findif tool (mandatory for IPaddr/IPaddr2)
--   + High: IPv6addr: new nic and cidr_netmask parameters
--   + High: postfix: new resource agent
--   + Include license information
--   + Low (LF 2159): Squid: make the regexp match more precisely output of netstat
--   + Low: configure: Fix package name.
--   + Low: ldirectord: add dependency on $remote_fs.
--   + Low: ldirectord: add mandatory required header to init script.
--   + Medium (LF 2165): IPaddr2: remove all colons from the mac address before passing it to send_arp
--   + Medium: VirtualDomain: destroy domain shortly before timeout expiry
--   + Medium: shellfuncs: Make the mktemp wrappers work.
--   + Remove references to Echo function
--   + Remove references to heartbeat shellfuncs.
--   + Remove useless path lookups
--   + findif: actually include the right header. Simplify configure.
--   + ldirectord: Remove superfluous configure artifact.
--   + ocf-tester: Fix package reference and path to DTD.
-- 
-- * Tue Aug 11 2009 Ville Skytta <ville.skytta@iki.fi> - 3.0.0-15
-- - Use bzipped upstream hg tarball.
-- 
-- * Wed Jul 29 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-14
-- - Merge Pacemaker cluster resource agents:
--   * Add Source1.
--   * Drop noarch. We have real binaries now.
--   * Update BuildRequires.
--   * Update all relevant prep/build/install/files/description sections.
-- 
-- * Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.0.0-13
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Wed Jul  8 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-12
-- - spec file updates:
--   * Update copyright header
--   * final release.. undefine alphatag
-- 
-- * Thu Jul  2 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-11.rc4
-- - New upstream release.
-- 
-- * Sat Jun 20 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-10.rc3
-- - New upstream release.
-- 
-- * Wed Jun 10 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-9.rc2
-- - New upstream release + git94df30ca63e49afb1e8aeede65df8a3e5bcd0970

* Tue May 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-0.1.1m)
- import from Fedora 11

* Tue Mar 24 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-8.rc1
- New upstream release.
- Update BuildRoot usage to preferred versions/names

* Mon Mar  9 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-7.beta1
- New upstream release.

* Fri Mar  6 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-6.alpha7
- New upstream release.

* Tue Mar  3 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-5.alpha6
- New upstream release.

* Tue Feb 24 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-4.alpha5
- Drop Conflicts with rgmanager.

* Mon Feb 23 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-3.alpha5
- New upstream release.

* Thu Feb 19 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-2.alpha4
- Add comments on how to build this package.

* Thu Feb  5 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-1.alpha4
- New upstream release.
- Fix datadir/cluster directory ownership.

* Tue Jan 27 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.0.0-1.alpha3
  - Initial packaging
