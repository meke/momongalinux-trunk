%global momorel 1

# first two digits of version
%define release_version %(echo %{version} | awk -F. '{print $1"."$2}')

Name:           grilo
Version:        0.2.4
Release:        %{momorel}m%{?dist}
Summary:        Content discovery framework

Group:          Applications/Multimedia
License:        LGPLv2+
Source0:        http://ftp.gnome.org/pub/GNOME/sources/grilo/%{release_version}/grilo-%{version}.tar.xz
Url:            http://live.gnome.org/Grilo
NoSource:	0

BuildRequires:  chrpath
BuildRequires:  gnome-common
BuildRequires:  vala-devel >= 0.7.2
BuildRequires:  vala-tools >= 0.7.2
BuildRequires:  gtk-doc
BuildRequires:  gobject-introspection-devel >= 0.9.0
BuildRequires:  libxml2-devel
BuildRequires:  libsoup-devel
# For the test UI
BuildRequires:  gtk3-devel

Requires:       gobject-introspection

%description
Grilo is a framework that provides access to different sources of
multimedia content, using a pluggable system.
This package contains the core library and elements.

%package devel
Summary:        Libraries/include files for Grilo framework
Group:          Development/Libraries

Requires:       %{name} = %{version}-%{release}
Requires:       glib2-devel gobject-introspection-devel

%description devel
Grilo is a framework that provides access to different sources of
multimedia content, using a pluggable system.
This package contains the core library and elements, as well as
general and API documentation.

%package vala
Summary:        Vala language bindings for Grilo framework
Group:          Development/Libraries

Requires:       %{name}-devel = %{version}-%{release}
Requires:       vala >= 0.7.2

%description vala
Grilo is a framework that provides access to different sources of
multimedia content, using a pluggable system.
This package contains the Vala language bindings.

%prep
%setup -q

# Fix vala detection for version 0.16
sed -i.vala 's/libvala-0.14/libvala-0.16/g' configure*

%build
%configure                      \
        --enable-vala           \
        --enable-gtk-doc        \
        --enable-introspection  \
        --enable-grl-net        \
        --disable-tests

make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/grilo-%{release_version}/
mkdir -p $RPM_BUILD_ROOT%{_datadir}/grilo-%{release_version}/plugins/

# Remove rpath
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/grl-inspect-%{release_version}
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/grilo-test-ui-%{release_version}
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/libgrlnet-%{release_version}.so.*

# Remove files that will not be packaged
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
rm -f $RPM_BUILD_ROOT%{_bindir}/grilo-simple-playlist

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*.typelib
%{_bindir}/grl-inspect-%{release_version}
%{_bindir}/grilo-test-ui-%{release_version}
%{_libdir}/grilo-%{release_version}/
%{_datadir}/grilo-%{release_version}/plugins/
%{_mandir}/man1/grl-inspect.1.*

%files devel
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO
%{_datadir}/gtk-doc/html/%{name}
%{_includedir}/%{name}-%{release_version}/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir

%files vala
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO
%{_datadir}/vala/vapi/*

%changelog
* Mon Jan 28 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.4-1m)
- update 0.2.4

* Mon Nov 12 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.2-1m)
- import from fc18

