%global         momorel 1
%global         xapian_ver 1.2.18

Name:           perl-Search-Xapian
Version:        %{xapian_ver}.0
Release:        %{momorel}m%{?dist}
Summary:        Perl XS frontend to the Xapian C++ search library
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Search-Xapian/
Source0:        http://www.cpan.org/authors/id/O/OL/OLLY/Search-Xapian-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  xapian-core-devel >= %{xapian_ver}
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module wraps most methods of most Xapian classes. The missing classes
and methods should be added in the future. It also provides a simplified,
more 'perlish' interface to some common operations, as demonstrated above.

%prep
%setup -q -n Search-Xapian-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes makehtmldocs perlobject.map README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Search*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.18.0-1m)
- rebuild against perl-5.20.0
- update to 1.2.18.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.17.0-1m)
- update to 1.2.17.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.16.0-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.16.0-1m)
- update to 1.2.16.0

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15.0-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15.0-2m)
- rebuild against perl-5.18.0

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.15.0-1m)
- update to 1.2.15.0

* Sun Mar 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.14.0-1m)
- update to 1.2.14.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.13.0-1m)
- update to 1.2.13.0
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.10.0-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.10.0-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.10.0-1m)
- update to 1.2.10.0
- rebuild against perl-5.16.0

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.9.0-1m)
- update to 1.2.9.0

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8.0-1m)
- update to 1.2.8.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7.0-2m)
- rebuild against perl-5.14.2

* Fri Aug 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7.0-1m)
- update to 1.2.7.0

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6.0-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6.0-1m)
- update to 1.2.6.0

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4.0-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4.0-2m)
- rebuild for new GCC 4.6

* Tue Dec 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4.0-1m)
- update to 1.2.4.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3.0-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3.0-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3.0-1m)
- update to 1.2.3.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2.0-2m)
- full rebuild for mo7 release

* Wed Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2.0-1m)
- update to 1.2.2.0

* Tue Jun 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.21.0-1m)
- update to 1.0.21.0

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.20.0-2m)
- rebuild against perl-5.12.1

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.20.0-1m)
- update to 1.0.20.0

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.19.0-1m)
- update to 1.0.19.0

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.18.0-2m)
- rebuild against perl-5.12.0

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.18.0-1m)
- update to 1.0.18.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.17.0-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.17.0-1m)
- update to 1.0.17.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.16.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.16.0-1m)
- update to 1.0.16.0

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.14.0-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.14.0-1m)
- update to 1.0.14.0

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.13.1-1m)
- update to 1.0.13.1

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.12.0-1m)
- update to 1.0.12.0

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.10.0-1m)
- update to 1.0.10.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7.0-2m)
- rebuild against rpm-4.6

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7.0-1m)
- update to 1.0.7.0

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6.0-2m)
- change BuildRequires from perl module name to package name

* Fri Jul 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6.0-1m)
- Specfile autogenerated by cpanspec 1.75 for Momonga Linux.
- add BuildRequires:  xapian-core-devel for xapian-config

