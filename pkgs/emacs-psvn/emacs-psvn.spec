%global momorel 10
%global pkg psvn
%global pkgname psvn

Summary: Subversion Interface for Emacs
Name: emacs-%{pkg}
Version: 0.40299
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source: http://www.xsteve.at/prg/emacs/psvn.el
URL: http://www.xsteve.at/prg/emacs/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
Requires: emacs(bin) >= %{_emacs_version}
Obsoletes: psvn-emacs
Obsoletes: psvn-xemacs
Obsoletes: elisp-psvn
Provides: elisp-psvn

%description
This mode provides an interface to svn - similar to pcl-cvs and CVS.

%package el
Summary:        Elisp source files for %{pkgname} under GNU Emacs
Group:          Applications/Text
Requires:       %{name} = %{version}-%{release}

%description el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.


%prep
%setup -q -c -T 0
cp -p %{SOURCE0} .

%build
%{_emacs_bytecompile} psvn.el

cat > %{pkg}-init.el <<"EOF"
(require 'psvn)
EOF

%install
rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -m 644 psvn.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -m 644 psvn.elc %{buildroot}%{_emacs_sitelispdir}/%{pkg}

%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m 644 %{pkg}-init.el %{buildroot}%{_emacs_sitestartdir}/%{pkg}-init.el

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40299-10m)
- improve subversion 1.7 support

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40299-9m)
- rebuild for emacs-24.1

* Mon Oct 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40299-8m)
- add subversion 1.7 support

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40299-7m)
- rename the package name
- add psvn-init.el

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.40299-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40299-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.40299-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40299-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.40299-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40299-1m)
- update to revision 40299

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37869-7m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37869-6m)
- merge psvn-emacs to elisp-psvn
- kill psvn-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37869-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.37869-4m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.37869-3m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37869-2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37869-1m)
- update to revision 37869

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37448-2m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.37448-1m)
- update to revision 37448

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33557-5m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33557-4m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33557-3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33557-2m)
- rebuild against rpm-4.6

* Thu Nov  6 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33557-1m)
- update to revision 33557

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32295-2m)
- rebuild against emacs-22.3

* Sat Jul 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32295-1m)
- update to revision 32295

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.29253-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.29253-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.29253-2m)
- rebuild against gcc43

* Tue Feb 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.29253-1m)
- update to revision 29253

* Fri Nov 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.27834-1m)
- update to revision 27834
- License: GPLv2

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25407-2m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25407-1m)
- update to revision 25407

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21235-2m)
- rebuild against emacs-22.0.90

* Thu Aug 24 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21235-1m)
- update to revision 21235

* Tue Jul  5 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.15026-1m)
- initial import to Momonga
