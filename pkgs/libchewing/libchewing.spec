%global momorel 1

Summary: Intelligent phonetic input method library for Traditional Chinese
Name: libchewing
Version: 0.3.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://chewing.csie.net/
Source0: https://github.com/downloads/chewing/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}.multilibConflict.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: pkgconfig

%description
libchewing is an intelligent phonetic input method library for Chinese.

It provides the core algorithm and logic that can be used by various
input methods.  The Chewing input method is a smart bopomofo phonetics
input method that is useful for inputting Mandarin Chinese.

%package devel
Summary: Header files and static libraries from libchewing
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libchewing.

%prep
%setup -q

%patch0 -p0 -b .multilibConflict

# for patches
autoreconf -fi

%build
export CFLAGS=-DLIBINSTDIR='\"%{_libdir}\" -g'

%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_libdir}/chewing
%{_libdir}/%{name}.so.*
%{_datadir}/chewing

%files devel
%defattr(-,root,root)
%{_includedir}/chewing
%{_libdir}/pkgconfig/chewing.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-7m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-6m)
- import 5 patches from Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- version 0.3.2
- remove all patches

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-2m)
- %%NoSource -> NoSource

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- initial package for scim-chewing-0.3.1
- import 5 patches from Fedora
