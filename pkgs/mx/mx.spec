%global momorel 6
%global pythonver 2.7

%define pybasever %{nil}
%define without_pybasever 1
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary:  A collection of Python software tools
Name: mx%{pybasever}
Version: 3.1.1
Release: %{momorel}m%{?dist}
URL: http://www.lemburg.com/files/python/eGenix-mx-Extensions.html
Source0: http://www.lemburg.com/python/egenix-mx-base-%{version}.tar.gz
Patch1: mx-3.1.1-longyear.patch
Patch2: mx-3.1.1-lib64.patch
License: "Python"
Group: Development/Libraries
BuildRequires: Distutils
BuildRequires: python >= %{pythonver}
BuildRequires: python-devel >= %{pythonver}
%if %{?without_pybasever}
Provides: mx2 = %{version}-%{release}
Obsoletes: mx2
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The mx extensions for Python are a collection of Python software tools
which enhance Python's usability in many areas.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files for %{name}

%prep
%setup -q -n egenix-mx-base-%{version}
%patch1 -p1 -b .longyear
%patch2 -p1 -b .lib64

%build
# alter /usr/local/bin/python
for file in mx/DateTime/Examples/Y2000.py mx/Misc/OrderedMapping.py \
    mx/TextTools/Examples/pytag.py mx/TextTools/Examples/Loop.py \
    mx/TextTools/Examples/Python.py mx/DateTime/Examples/AtomicClock.py \
    mx/TextTools/Examples/mysplit.py mx/TextTools/Examples/HTML.py \
    mx/DateTime/Examples/alarm.py mx/TextTools/Examples/RegExp.py \
    mx/TextTools/Examples/altRTF.py mx/TextTools/Examples/Words.py \
    mx/TextTools/Examples/RTF.py mx/Misc/FileLock.py ; do
    sed -i -e 's!/usr/local/bin/python!%{_bindir}/python!' ${file}
done

# These just have test cases and aren't meant to be run
for file in mx/Log.py mx/BeeBase/FileLock.py mx/Misc/OrderedMapping.py \
    mx/Misc/FileLock.py ; do
    sed -i -e '/^#!.*python\b/d' ${file}
done

CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --skip-build --root=%{buildroot} 

pushd %{buildroot}%{python_sitearch}
for I in `find . -name '*.h'`; do
    mkdir -p %{buildroot}%{_includedir}/`dirname $I`
    mv $I %{buildroot}%{_includedir}/`dirname $I`
done
popd

# Examples, tests, benchmarks
BASEDIR=%{buildroot}%{python_sitearch}
mkdir examples
mv ${BASEDIR}/mx/TextTools/mxTextTools/testkj.py examples/
mv ${BASEDIR}/mx/Stack/stackbench.py examples/
mv ${BASEDIR}/mx/Queue/queuebench.py examples/
mv ${BASEDIR}/mx/DateTime/mxDateTime/test.py examples/
# This is a utility.  If it's deemed useful to the general public it should
# be installed in %{_bindir} instead of examples
mv ${BASEDIR}/mx/BeeBase/showBeeDict.py examples/

# These files are documentation, and are in a bad location
mkdir docs
mv -f ${BASEDIR}/mx/{LICENSE,COPYRIGHT} docs/
rm -rf ${BASEDIR}/mx/Doc
DESTDIR=`pwd`/docs
pushd ${BASEDIR}/mx
cp -r --parents */Doc/* ${DESTDIR}/
cp -r --parents */Examples/* ${DESTDIR}/
popd
rm -rf ${BASEDIR}/mx/*/Doc/
rm -rf ${BASEDIR}/mx/*/Examples
rm -rf docs/*/Examples/*.pyc
rm -rf docs/*/Examples/*.pyo

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc README docs/*
%{python_sitearch}/mx
%{python_sitearch}/egenix_mx_base*.egg-info

%files devel
%defattr(-,root,root,-)
%{_includedir}/mx/

%changelog
* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-1m)
- sync with Fedora 11 (3.1.1-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-7m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.6-6m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.6-5m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.6-4m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.6-3m)
- rebuild against gcc43

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.6-2m)
- rebuild against python-2.5

* Sun Jul 16 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.6-1m)
- import from fc

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2.0.6-2.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.0.6-2.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.0.6-2.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Mar 14 2005 Mihai Ibanescu <misa@redhat.com> 2.0.6-2
- Rebuilt

* Wed Feb 02 2005 Elliot Lee <sopwith@redhat.com> 2.0.6-1
- Rebuild with python 2.4

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sun Nov 23 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 2.0.5
- recompile with python 2.3

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 2.0.3-7
- lib64'ize

* Tue Aug 06 2002 Elliot Lee <sopwith@redhat.com> 2.0.3-6
- Provide mx2 dep

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed May 29 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.0.3-4
- Make it require python >= 2.2, < 2.3

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.0.3-2
- Move to python 2.2

* Wed Mar 13 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.0.3-1
- 2.0.3

* Wed Feb 27 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.0.2-5
- Rebuild (and no, it wasn't broken. It just used /usr/bin/python
  as the version to build for)

* Mon Jan 21 2002 Elliot Lee <sopwith@redhat.com> 2.0.2-4
- Remove pyver autodetection (it's broken!) and install header files

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Oct  1 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.0.2-2
- detect python version when building
- 64bit fix mxDateTime

* Fri Sep 14 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.0.2-1
- 2.0.2
- Build for Python 2.2

* Tue Jun 19 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Initial build. Needed for python DB API


