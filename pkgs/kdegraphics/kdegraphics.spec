%global momorel 1
%global kdever 4.13.1

Summary: K Desktop Environment - Graphics Applications
Name: kdegraphics
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.kde.org/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gwenview = %{version}
Requires: kamera = %{version}
Requires: kcolorchooser = %{version}
Requires: kdegraphics-strigi-analyzer = %{version}
Requires: kdegraphics-thumbnailers = %{version}
Requires: kgamma = %{version}
Requires: kolourpaint = %{version}
Requires: kruler = %{version}
Requires: ksaneplugin = %{version}
Requires: ksnapshot = %{version}
Requires: kdegraphics-mobipocket = %{version}
Requires: okular = %{version}
Requires: okular-active = %{version}
Requires: qmobipocket = %{version}
Requires: svgpart = %{version}

Obsoletes: %{name}3

%description
Kdegraphics metapackage

%package libs
Summary: Runtime libraries for %{name}
Group: System Environment/Libraries

Requires: gwenview-libs = %{version}
Requires: kio_msits = %{version}
Requires: kolourpaint-libs = %{version}
Requires: libkipi = %{version}
Requires: libkdcraw = %{version}
Requires: libkexiv2 = %{version}
Requires: libksane = %{version}
Requires: okular-libs = %{version}
Requires: okular-part = %{version}

%description libs
%{summary}.

%package devel
Group: Development/Libraries
Summary: Developer files for %{name}

Requires: %{name}-libs = %{version}-%{release}
Requires: libkipi-devel = %{version}
Requires: libkdcraw-devel = %{version}
Requires: libkexiv2-devel = %{version}
Requires: libksane-devel = %{version}
Requires: okular-devel = %{version}
Requires: qmobipocket-devel = %{version}

%description devel
%{summary}.

%prep

%build

%install

%files

%files libs

%files devel

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-2m)
- add Requires: mobipocket

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Thu Sep 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.1-2m)
- set Obsoletes: %%{name}3

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- rebuild against exiv2-0.21.1

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-3m)
- update KolourPaint patch
- patch from http://mail.kde.gr.jp/pipermail/kdeveloper/2010-October/000267.html

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- fix kdebug #194821
-- https://bugs.kde.org/show_bug.cgi?id=194821
-- http://twitter.com/#!/phanect/status/26508282551
-- patch from Kdeveloper, thanks Tasuku Suzuki

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sat Sep 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.1-2m)
- fix up okular.desktop again

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-3m)
- full rebuild for mo7 release

* Thu Aug 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- [SECURITY] CVE-2010-2575
- "Okular PDB Processing Memory Corruption Vulnerability"
- http://www.kde.org/info/security/advisory-20100825-1.txt

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- remove Graphics from Categories of okular.desktop

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sun Jul 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-2m)
- update libkexiv2, libraw, libkdcraw and libksane for digikam and kipi-plugins-1.3.0
- use cooker's sync-libkdcraw-trunk.patch

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-2m)
- rebuild against exiv2-0.20

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-2m)
- rebuild against libjpeg-8a

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-2m)
- rebuild against exiv2-0.19

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.1-3m)
- rebuild against libjpeg-7

* Sat Sep 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.3.1-2m)
- rebuild against poppler-0.12.0

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Wed May 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.3-2m)
- rebuild against djvulibre-3.5.22

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-3m)
- modify for new digikam
- move libkdcraw.so, libkexiv2.so and libkipi.so to %%{_libdir}/
- add libkdcraw.pc, libkexiv2.pc and libkipi.pc
- Provides: and Obsoletes: libkdcraw, libkexiv2 and libkipi

* Thu Dec 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-2m)
- rebuild against exiv2-0.18

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.62

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Thu Jul 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-2m)
- add BuildRequires: ebook-tools-devel for Okular.

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up KDE 4.1 beta 2

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-3m)
- revive kipi-plugins_logo.png for new libkipi

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-2m)
- move and remove files conflicting with
  libkdcraw-devel, libkexiv2-devel, libkipi-devel and libkipi
- there are no applications using these files of kdegraphics-devel

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Mon Jun  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-3m)
- rebuild against exiv2-0.17

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- release %%{_kde4_datadir}/kde4/services/ServiceMenus, it's provided by kdelibs

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-2m)
- rebuild against poppler-0.8.0

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1
- import some patches from Fedora devel

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Fri Nov  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-2m)
- [SECURITY] CVE-2007-4352 CVE-2007-5392 CVE-2007-5393
- import upstream patch for these issue

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sat Sep  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.7-4m)
- rebuild against poppler-0.6

* Tue Jul 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-3m)
- [SECURITY] CVE-2007-3387
- import upstream patch (post-3.5.7-kdegraphics-CVE-2007-3387.diff)

* Sat Jul 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-2m)
- do not support gift any more

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against kdelibs etc.

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches and security patches

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-8m)
- [SECURITY] MOAB-06-01-2007
-   Multiple Vendor PDF Document Catalog Handling Vulnerability

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-7m)
- import upstream patch to fix kpdf problem
-  see ftp://ftp.foolabs.com/pub/xpdf/xpdf-3.01pl2.patch

* Wed Dec  6 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-6m)
- [SECURITY] JPEG-EXIF Meta Information DoS vulnerability
- add post-3.5.5-kdegraphics.diff from www.kde.org
- see http://www.kde.org/info/security/advisory-20061129-1.txt

* Fri Nov 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-5m)
- import upstream patch to fix following problem
  #136590, problem with cyrillic's filenames: cannot open file

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-4m)
- rebuild against OpenEXR-1.4.0

* Tue Nov  7 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-3m)
- import upstream patch to fix following problem
  #111517, check 0-pointer to avoid crashes

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.5-2m)
- rebuild against expat-2.0.0-2m

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.4-4m)
- rebuild against freeglut

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-3m)
- import 2 upstream patches from Fedora Core devel
 +* Thu Sep 07 2006 Than Ngo <than@redhat.com> 7:3.5.4-2
 +- apply upstream patches
 +   fix #113635, kpdf crash

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3

* Mon Apr 10 2006 Nishio Futoshi <futoshi@momonga-lixux.org>
- (3.5.2-2m)
- rebuild against poppler-0.5.1

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2
- update kpdf-xft.patch
- remove post-3.5.1-kdegraphics-CVE-2006-0301.diff

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-3m)
- import kdegraphics-3.5.1-warning.patch from Fedora Core devel
 +* Tue Feb 07 2006 Than Ngo <than@redhat.com> 7:3.5.1-2
 +- apply patch to fix gcc warning (#169189)
- import kdegraphics-3.5.1-kpdf-xft.patch from Fedora Core devel
 +* Thu Feb 16 2006 Than Ngo <than@redhat.com> 7:3.5.1-3
 +- apply patch to fix kpdf build with modular-X again #173836
 +* Mon Dec 12 2005 Than Ngo <than@redhat.com> 7:3.5.0-2
 +- apply patch to fix modula-X problem

* Fri Feb  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- [SECURITY] CVE-2006-0301
  post-3.5.1-kdegraphics-CVE-2006-0301.diff for "kpdf/xpdf heap based buffer overflow"
  http://www.kde.org/info/security/advisory-20060202-1.txt

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- remove post-3.5.0-kdegraphics-CAN-2005-3193.diff

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-4m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Thu Dec  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- [SECURITY] post-3.5.0-kdegraphics-CAN-2005-3193.diff for "kpdf/xpdf multiple integer overflows"
  http://www.kde.org/info/security/advisory-20051207-1.txt

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- rebuild against libexif-0.6.12-1m

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- revise %%files section
- disable hidden visibility

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- BuildPreReq: libgphoto2-devel
- add %%post and %%postun

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdegraphics-3.4.2-uic.patch
- remove kpdf patches

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- import kdegraphics-3.4.2-uic.patch from Fedora Core devel
 +* Wed Sep 21 2005 Than Ngo <than@redhat.com> 7:3.4.2-5
 +- fix uic build problem

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure
- import kpdf.patches from Fedora Core devel
 +* Mon Sep 05 2005 Than Ngo <than@redhat.com> 7:3.4.2-3
 +- backport CVS patch to fix kpdf crash when trying to
 +  expand sub-bookmarks in the bookmark tree #167390
 +* Wed Aug 31 2005 Than Ngo <than@redhat.com> 7:3.4.2-2
 +- backport CVS patch to fix rendering problem in kpdf
 +- backport CVS patch to fix bug #kde110171
 +- backport CVS patch to fix bug #kde110034, #kde110000
 +- backport CVS patch to fix crash in kpdf
- BuildPreReq: libexif-devel

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- remove nokdelibsuff.patch

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- modify %%files section

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- remove old patches
- BuildPreReq: kdebase

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- [SECURITY] post-3.3.2-kdegraphics.diff for "kpdf Buffer Overflow Vulnerability"
  http://kde.org/info/security/advisory-20041223-1.txt
- [SECURITY] post-3.3.2-kdegraphics-3.diff for "kpdf Buffer Overflow Vulnerability"
  http://kde.org/info/security/advisory-20050119-1.txt
- import patches from Fedora Core
 - kdegraphics-3.3.1-xorg.patch
  +* Fri Nov 19 2004 Than Ngo <than@redhat.com> 7:3.3.1-4
  +- fix xf86gammacfg to work with xorg.conf
 - kdegraphics-3.3.2-immodule.patch
  +* Sat Feb 12 2005 Than Ngo <than@redhat.com> 7:3.3.2-0.4
  +- backport from CVS for working with qt-immodule

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Fri Oct 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-2m)
- security fix
- - KDE Security Advisory: kpdf integer overflows
    (http://www.kde.org/info/security/advisory-20041021-1.txt)

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)
- modify CXXFLAGS

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Wed Mar 24 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.1-2m)
- revised spec for rpm 4.2

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- modified %%files (many new files)
- add --xinerama to ./configure

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

 * Sat Dec 27 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-4m)
- rebuild against for qt-3.2.3

* Tue Dec 4 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- buildprereq gphoto2

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- rebuild against for qt-3.2.3

* Thu Sep 25 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Wed Aug 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Wed Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (3.1.2-3m)
- rebuild against libexif-0.5.12

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-2m)
- remove --disable-warnings from configure

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     kghostview: Better handling of half-broken Postscript and PDF files [#44855]
     kghostview: Fix the opening of files on the command line, which was not working if the paths contained non-alphanumeric characters or were absolute paths [#56169 and #51533]
     kghostview: Work around -dMaxBitmap bug in gs version 6.5x [#37287]
     kghostview: Reset orientation and paper size selectors after opening a new document [#51014]
     kghostview: Security fix for #56808. The security patch which was present in version 3.1.1a caused problems for some users and has been corrected [#57441]
     kghostview: ghostscript version 8 is now supported [#53343]
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m

* Tue May 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-2m)
- add --disable-warnings to configure
- %%{?smp_mflags} -> %%{?_smp_mflags}

* Fri Apr 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-1m)
  update to 3.1.1a

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-3m)
- remove requires: qt-Xt

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    KIconEdit: Fix the ellipse/circle tool not to leave any "holes" in the drawings
    Kooka: Some UI crashes fixed
    KViewShell: Default paper size is fixed
    KGhostView: Fixed wheel-mouse scrolling

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-3m)
- add URL tag

* Wed Feb 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-2m)
- add REMOVE.PLEASE

* Sun Feb  5 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.

* Sun Jun  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Fri Apr  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Thu Nov  8 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-10k)
- nigirisugi.
- revised spec file.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised spec file.

* Fri Oct 26 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- rebuild against sane-backends-devel 1.0.5

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alpha2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Fri Mar 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.1.1-3k)

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.1-2k)

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-12k)
- rebuild against QT-2.3.0.

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-10k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-8k]
- backport 2.0.1-9k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- rebuild against.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild against new environment glibc-2.2 egcs++

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Sat Nov 04 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- add GIF Support switch.
- release.

* Fri Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- modified Requires,BuildPrereq.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.

* Tue Sep 26 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Tue Jun 20 2000 Kenichi Matsubara <m@kondara.org>
- initial release Kondara MNU/Linux.
