%global momorel 2

Summary: PolicyKit integration for the GNOME desktop
Name: polkit-gnome
Version: 0.105
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.freedesktop.org/wiki/Software/PolicyKit
Group: Applications/System
#Source0: http://hal.freedesktop.org/releases/%{name}-%{version}.tar.bz2
#NoSource: 0
Source0: %{name}-%{version}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: gtk3-devel
BuildRequires: polkit-devel >= 0.107
BuildRequires: desktop-file-utils
BuildRequires: intltool
BuildRequires: dbus-glib-devel
BuildRequires: gobject-introspection-devel

Requires: polkit >= 0.107

Obsoletes: PolicyKit-gnome <= 0.10
Provides: PolicyKit-gnome = 0.11
Obsoletes: PolicyKit-gnome-libs <= 0.10
Provides: PolicyKit-gnome-libs = 0.11
Obsoletes: PolicyKit-gnome-demo <= 0.10
Provides: PolicyKit-gnome-demo = 0.11
Obsoletes: PolicyKit-devel <= 0.10
Provides: PolicyKit-devel = 0.11
Obsoletes: PolicyKit-gnome-devel <= 0.10
Provides: PolicyKit-gnome-devel = 0.11
Obsoletes: polkit-gnome-devel < 0.103
Provides: polkit-gnome-devel = 0.103
Obsoletes: polkit-gnome-docs < 0.103
Provides: polkit-gnome-docs = 0.103
Obsoletes: polkit-gnome-devel < 0.103
Provides: polkit-gnome-devel = 0.103

%description
polkit-gnome provides an authentication agent for PolicyKit
that matches the look and feel of the GNOME desktop.

%prep
%setup -q

%build
%configure --disable-static
make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

rm -f %{buildroot}/%{_libdir}/*.la

%find_lang %{name}-1

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}-1.lang
%defattr(-,root,root,-)
%doc COPYING AUTHORS README
%{_libexecdir}/*

%changelog
* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.105-2m)
- use polkit >= 0.107

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.105-1m)
- update 0.105

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.103-1m)
- update 0.103
- Drop subpackage

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.102-2m)
- add configure option

* Mon Aug 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.102-1m)
- update 0.102

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.101-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.101-1m)
- update to 0.101

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (0.99-1m)
- update to 0.99

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96-2m)
- full rebuild for mo7 release

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.96-1m)
- update to 0.96

* Thu Dec 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.94-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.94-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.94-1m)
- import from Fedora (but version 0.94)

* Mon Sep 14 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.5
- add Provides: PolicyKit-authentication-agent to satify what PolicyKit-gnome
  also provided

* Mon Sep 14 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.4
- Refine how Obsolete: is used and also add Provides: (thanks Jesse
  Keating and nim-nim)

* Mon Sep 14 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.3
- Obsolete old PolicyKit-gnome packages

* Sun Sep 13 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.2
- Update BR

* Sun Sep 13 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913.1
- Update BR

* Sun Sep 13 2009 David Zeuthen <davidz@redhat.com> - 0.95-0.git20090913
- Update to git snapshot
- Turn on GObject introspection

* Wed Sep  2 2009 Matthias Clasen <mclasen@redhat.com> - 0.94-4
- Just remove the OnlyShowIn, it turns out everybody wants this

* Sat Aug 29 2009 Matthias Clasen <mclasen@redhat.com> - 0.94-3
- Require a new enough polkit (#517479)

* Sat Aug 29 2009 Matthias Clasen <mclasen@redhat.com> - 0.94-2
- Show in KDE, too (#519674)

* Wed Aug 12 2009 David Zeuthen <davidz@redhat.com> - 0.94-1
- Update to upstream release 0.94

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.93-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 20 2009 David Zeuthen <davidz@redhat.com> - 0.93-2
- Rebuild

* Mon Jul 20 2009 David Zeuthen <davidz@redhat.com> - 0.93-1
- Update to 0.93

* Tue Jun  9 2009 Matthias Clasen <mclasen@redhat.com> - 0.9.2-3
- Fix BuildRequires

* Tue Jun 09 2009 David Zeuthen <davidz@redhat.com> - 0.92-2
- Change license to LGPLv2+
- Remove Requires: gnome-session

* Mon Jun 08 2009 David Zeuthen <davidz@redhat.com> - 0.92-1
- Update to 0.92 release

* Wed May 27 2009 David Zeuthen <davidz@redhat.com> - 0.92-0.git20090527
- Update to 0.92 snapshot

* Mon Feb  9 2009 David Zeuthen <davidz@redhat.com> - 0.91-1
- Initial spec file.
