%global momorel 1

Name:           kbd
Version:        2.0.1
Release:        %{momorel}m%{?dist}
Summary:        Tools for configuring the console (keyboard, virtual terminals, etc.)
Group:          System Environment/Base
License:        GPLv2+
URL:            http://ftp.altlinux.org/pub/people/legion/kbd
Source0:        http://ftp.altlinux.org/pub/people/legion/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
Source2:        kbd-latsun-fonts.tar.bz2
Source3:        kbd-latarcyrheb-16-fixed.tar.bz2
Source4:        fr-dvorak.tar.bz2
Source5:        ro_maps.tar.bz2
Source10:       kbd-jp+k.tar.gz
Source11:       jp106_Ctrl_Ctrl.map.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  bison, flex, gettext
Conflicts:      util-linux < 2.11r-9
Requires:       initscripts >= 5.86-1
Requires:       %{name}-misc = %{version}-%{release}

%description
The %{name} package contains tools for managing a Linux
system's console's behavior, including the keyboard, the screen
fonts, the virtual terminals and font files.

%package misc
Summary:        Data for kbd package
BuildArch:      noarch
 
%description misc
The %{name}-misc package contains data for kbd package - console fonts,
keymaps etc. Please note that %{name}-misc is not helpful without kbd.

%prep
%setup -q -a 2 -a 3 -a 4 -a 10

# 7-bit maps are obsolete; so are non-euro maps
pushd data/keymaps/i386
mv qwerty/fi.map qwerty/fi-old.map
cp qwerty/fi-latin9.map qwerty/fi.map
cp qwerty/pt-latin9.map qwerty/pt.map
cp qwerty/sv-latin1.map qwerty/se-latin1.map

mv azerty/fr.map azerty/fr-old.map
cp azerty/fr-latin9.map azerty/fr.map

cp azerty/fr-latin9.map azerty/fr-latin0.map # legacy alias

# Rename conflicting keymaps
mv dvorak/no.map dvorak/no-dvorak.map
mv fgGIod/trf.map fgGIod/trf-fgGIod.map
mv olpc/es.map olpc/es-olpc.map
mv olpc/pt.map olpc/pt-olpc.map
mv qwerty/cz.map qwerty/cz-qwerty.map
popd

# remove obsolete "gr" translation
pushd po
rm -f gr.po gr.gmo
popd

# Convert to utf-8
iconv -f iso-8859-1 -t utf-8 < "ChangeLog" > "ChangeLog_"
mv "ChangeLog_" "ChangeLog"

%build
%configure --prefix=%{_prefix} --datadir=/lib/kbd --mandir=%{_mandir} --localedir=%{_datadir}/locale --enable-nls
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# ro_win.map.gz is useless
rm -f %{buildroot}/lib/kbd/keymaps/i386/qwerty/ro_win.map.gz

# Create additional name for Serbian latin keyboard
ln -s sr-cy.map.gz %{buildroot}/lib/kbd/keymaps/i386/qwerty/sr-latin.map.gz

# The rhpl keyboard layout table is indexed by kbd layout names, so we need a
# Korean keyboard
ln -s us.map.gz %{buildroot}/lib/kbd/keymaps/i386/qwerty/ko.map.gz

# Move binaries which we use before /usr is mounted from %{_bindir} to /bin.
mkdir -p %{buildroot}/bin
for binary in setfont dumpkeys kbd_mode unicode_start unicode_stop loadkeys ; do
  mv %{buildroot}%{_bindir}/$binary %{buildroot}/bin/
done

# Momonga hack
install -m644 kbd-jp/us.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/us.map.gz
install -m644 kbd-jp/jp106.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/jp106.map.gz
install -m644 kbd-jp/jp106_Ctrl_CAPS.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/jp106_Ctrl_CAPS.map.gz
install -m644 kbd-jp/us_Ctrl_CAPS.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/us_Ctrl_CAPS.map.gz
install -m644 kbd-jp/jp106_Ctrl_CAPS_jed.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/jp106_Ctrl_CAPS_jed.map.gz
install -m644 kbd-jp/us_Ctrl_CAPS_jed.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/us_Ctrl_CAPS_jed.map.gz
install -m644 kbd-jp/us_jed.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/us_jed.map.gz
install -m644 kbd-jp/us_Ctrl_Ctrl.kmap.gz \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/us_Ctrl_Ctrl.map.gz
install -m644 %{SOURCE11} \
        %{buildroot}/lib/kbd/keymaps/i386/qwerty/jp106_Ctrl_Ctrl.map.gz

# Some microoptimization
sed -i -e 's,\<kbd_mode\>,/bin/kbd_mode,g;s,\<setfont\>,/bin/setfont,g' \
        %{buildroot}/bin/unicode_start

# Link open to openvt
ln -s openvt %{buildroot}%{_bindir}/open

#%%find_lang %{name}

%clean
rm -rf %{buildroot}

#%%files -f %{name}.lang
%files
%defattr(-,root,root,-)
%doc ChangeLog AUTHORS README COPYING
/bin/*
%{_bindir}/*
%{_mandir}/*/*

%files misc
%defattr(-,root,root,-)
/lib/kbd

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.2-2m)
- change Source0 URI and remove Source1

* Sat Oct 29 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.15.2-1m)
- version up 1.15.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.15-4m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.15-3m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-1m)
- update to 1.15 based on Fedora 11 (1.15-7)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-5m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-4m)
- revive Source10: kbd-jp+k.tar.gz
- revive Source11: jp106_Ctrl_Ctrl.map.gz

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-3m)
- sync with Rawhide (1.12-32)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-2m)
- rebuild against gcc43

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (1.08-4m)
- revise spec

* Tue Sep  9 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.08-3m)
- a typo fix, s/us_Ctrl_Ctrl\.g/us_Ctrl_Ctrl.gz/

* Fri May 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- /etc/rc.d/init.d/keytable -> /etc/init.d/keytable

* Fri Mar 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.08-1m)
- import

* Thu Jan 30 2003 Bill Nottingham <notting@redhat.com> 1.08-4
- remove condrestart from initscript

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Dec  6 2002 Nalin Dahyabhai <nalin@redhat.com> 1.08-2
- only output terminal unicode init sequence if both stdout and stderr are
  connected to terminals, so that it doesn't show up when script outputs
  get piped to files

* Fri Nov 22 2002 Nalin Dahyabhai <nalin@redhat.com> 1.08-1
- update to 1.08
- drop updates which went mainline

* Mon Nov 11 2002 Nalin Dahyabhai <nalin@redhat.com> 1.06-27
- add detached signature
- remove directory names from PAM configuration so that the same config file
  can be used for any arch on multilib systems

* Wed Sep  4 2002 Bill Nottingham <notting@redhat.com> 1.06-26
- don't munge /etc/sysconfig/i18n

* Tue Sep  3 2002 Bill Nottingham <notting@redhat.com> 1.06-25
- don't run setsysfont in upgrade trigger on console-tools

* Thu Aug 29 2002 Jakub Jelinek <jakub@redhat.com> 1.06-24
- use cyr-sun16 cyrillic chars in latarcyrheb-sun16 font
  instead of old LatArCyrHeb-16 chars
- add Euro character to latarcyrheb-sun16
- use latarcyrheb-sun16 by default in unicode_start script

* Tue Aug 27 2002 Jakub Jelinek <jakub@redhat.com> 1.06-23
- add back lat[02]-sun16 fonts plus latarcyrheb-sun16 font

* Thu Aug 22 2002 Karsten Hopp <karsten@redhat.de>
- needs to conflict with older util-linux packages 
  (kbdrate moved between packages)

* Tue Aug 13 2002 Bill Nottingham <notting@redhat.com> 1.06-21
- remove Evil Hack in favor of slightly-less-evil-hack in initscripts

* Tue Jul  9 2002 Bill Nottingham <notting@redhat.com> 1.06-20
- fix speakup keymap names

* Tue Jul 09 2002 Phil Knirsch <pknirsch@redhat.com> 1.06-19
- Evil hack to make setfont work correctly on all consoles (#68018)

* Thu Jun 27 2002 Bill Nottingham <notting@redhat.com> 1.06-18
- move unicode_stop to /bin too
- fix path to loadkeys in keytable.init
- add in speakup keymaps

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Jun 11 2002 Nalin Dahyabhai <nalin@redhat.com> 1.06-16
- fix incorrect path in console.apps configuration file

* Thu May 30 2002 Bill Nottingham <notting@redhat.com> 1.06-14
- move some more stuff to /bin (unicode_start and dependencies)

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Feb 25 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-12
- Rebuild in new environment

* Wed Jan 30 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-11
- Oops, actually list the pam files in %files

* Tue Jan 29 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-10
- Add and consolehelper'ify kbdrate

* Tue Jan 29 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-9
- Re-remove kbdrate

* Thu Jan 24 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-7
- Fix build in current environment
- Get rid of kbdrate, it's in util-linux these days

* Wed Jul 18 2001 Matt Wilson <msw@redhat.com>
- added a patch (Patch4) that allows --tty= in setfont
- modified patch not to break translations

* Tue Jul  3 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-4
- Add cyrillic patches from leon@geon.donetsk.ua (#47144)

* Tue Jun 26 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-3
- Fix "Alt+AltGr=Compose" in qwertz-keyboards

* Mon Jun 25 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.06-2
- Fix "make install" and init script (#45327)

* Sat Jun 16 2001 Than Ngo <than@redhat.com>
- update to 1.0.6
- use %%{_tmppath}
- use find_lang
- support new gettext
- remove some patch files, which are included in 1.0.6
- fix to use RPM_OPT_FLAGS

* Thu May  3 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.05-3
- Fix up resizecons

* Wed May  2 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.05-2
- Build everything, obsoletes console-tools
- s/Copyright:/License:/
- License is GPL, not just distributable
- Add our compose mappings from old console-tools
- Add triggerpostun -- console-tools magic to get sane fonts and mappings

* Tue Apr 17 2001 Erik Troan <ewt@redhat.com>
- initial packaging for kbdrate
