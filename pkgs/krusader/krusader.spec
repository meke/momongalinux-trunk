%global momorel 1
%global prever 2
%global betaver 3
%global qtver 4.8.4
%global kdever 4.9.95
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: An advanced twin-panel (commander-style) file-manager for KDE
Name: krusader
Version: 2.4.0
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPLv2
Group: Applications/File
URL: http://krusader.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}-beta%{betaver}/%{name}-%{version}-beta%{betaver}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: krename
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebindings-devel >= %{kdever}
BuildRequires: desktop-file-utils
BuildRequires: gamin-devel
BuildRequires: gettext
BuildRequires: libpng-devel
BuildRequires: libxml2

%description
Krusader is an advanced twin-panel (commander-style) file-manager for KDE
(similar to Midnight or Total Commander) but with many extras.
It provides all the file-management features you could possibly want.
Plus: extensive archive handling, mounted filesystem support, FTP, advanced
search module, viewer/editor, directory synchronisation, file content
comparisons, powerful batch renaming and much much more.
It supports the following archive formats: tar, zip, bzip2, gzip, rar, ace,
arj and rpm and can handle other KIOSlaves such as smb:// or fish://
It is (almost) completely customizable, very user friendly, fast and looks
great on your desktop! :-)

You should give it a try.

%prep
%setup -q -n %{name}-%{version}-beta%{betaver}

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-category Utility \
  --add-category System \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --add-category X-KDE-More \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}_root-mode.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING CREDITS ChangeLog FAQ INSTALL README SVNNEWS TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/kio_iso.so
%{_kde4_libdir}/kde4/kio_krarc.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/%{name}_root-mode.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_configdir}/kio_isorc
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/*/*/*/%{name}*.png
%{_kde4_datadir}/kde4/services/iso.protocol
%{_kde4_datadir}/kde4/services/krarc.protocol
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Sat Dec 22 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-0.2.1m)
- update to version 2.4.0-beta3 "Single Step"
- update desktop.patch

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-0.1.1m)
- update to version 2.4.0-beta1 "Migration"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-0.1.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-0.1.5m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-0.1.4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-0.1.3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-0.1.2m)
- rebuild against qt-4.6.3-1m

* Sun May  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-0.1.1m)
- update to version 2.2.0-beta1 "DeKade"
- update desktop.patch

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-0.1.2m)
- touch up spec file

* Wed Jan 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-0.1.1m)
- update to version 2.1.0-beta1 "Rusty Clutch"
- update desktop.patch
- remove merged gcc44.patch
- add CREDITS to %%doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-3m)
- [STABLE RELEASE] version 2.0.0 "Mars Pathfinder"
- remove make-doc-ru.patch

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-2.2.4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-2.2.3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-2.2.2m)
- update Patch0 for fuzz=0
- drop Patch1, not needed

* Mon Dec 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-2.2.1m)
- update to version 2.0.0 beta 2
- License: GPLv2

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.20081005.1m)
- update to 20081004 svn snapshot
- - Full msgmerge

* Mon Sep 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.20080915.1m)
- update to 20080915 svn snapshot

* Fri Sep 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.20080912.1m)
- update to 20080912 svn snapshot

* Wed Aug 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.20080827.1m)
- update to 20080827 svn snapshot

* Sun Aug 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.20080824.1m)
- update to 20080824 svn snapshot

* Thu Jul 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.20080731.1m)
- update to 20080731 svn snapshot

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1.1.1m)
- update to version 2.0.0 beta 1

* Sun Jun 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080629.1m)
- update to 20080629 svn snapshot (to fix crashing at initial start up)
- remove list.patch

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080408.3m)
- revise %%{_docdir}/HTML/*/krusader/common

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.20080408.2m)
- rebuild against qt-4.4.0-1m

* Tue Apr  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080408.1m)
- update to 20080408 svn snapshot for gcc43
- add list.patch to enable build

* Sat Apr  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080405.1m)
- update to 20080405 svn snapshot

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-0.20080314.2m)
- rebuild against gcc43

* Fri Mar 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080314.1m)
- update to 20080314 svn snapshot for KDE4

* Mon Feb 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.80.0-4m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.80.0-3m)
- rebuild without kdebindings
- modify BPR for KDE4

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.80.0-2m)
- %%NoSource -> NoSource

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.80.0-1m)
- version 1.80.0

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.80.0-0.2.1m)
- update to version 1.80.0-beta2
- remove merged gcc42.patch

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.70.1-3m)
- remove Application from "Categories" of krusader.desktop

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.70.1-2m)
- add gcc-4.2 patch. (krusader-1.70.1-gcc42.patch)

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.70.1-1m)
- initial package for Momonga Linux
