%global momorel 1

Name:		unetbootin
Version:	581
Release:	%{momorel}m%{?dist}
Summary:	Create bootable Live USB drives for a variety of Linux distributions
Group:		System Environment/Base
License:	GPLv2+
URL:		http://unetbootin.sourceforge.net/
Source0:	http://downloads.sourceforge.net/%{name}/%{name}-source-%{version}.tar.gz
Nosource: 	0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
# Syslinux is only available on x86 architectures
ExclusiveArch:	%{ix86} x86_64

BuildRequires:	desktop-file-utils
BuildRequires:	qt-devel
# Not picked up automatically, required for operation
Requires:	p7zip
Requires:	syslinux

%description
UNetbootin allows you to create bootable Live USB drives for a variety of
Linux distributions from Windows or Linux, without requiring you to burn a CD.
You can either let it download one of the many distributions supported
out-of-the-box for you, or supply your own Linux .iso file if you've already
downloaded one or your preferred distribution isn't on the list.

%prep
%setup -q -c 
# Fix EOL encoding
for file in README.TXT; do
 sed "s|\r||g" $file > $file.new && \
 touch -r $file $file.new && \
 mv $file.new $file
done
# Fix desktop file
sed -i '/^Version/d' unetbootin.desktop
sed -i '/\[en_US\]/d' unetbootin.desktop
sed -i 's|/usr/bin/unetbootin|unetbootin|g' unetbootin.desktop

%build

# Generate .qm files
lrelease-qt4 unetbootin.pro
qmake-qt4

make %{?_smp_mflags}

%install
rm -rf %{buildroot} 
install -D -p -m 755 unetbootin %{buildroot}%{_bindir}/unetbootin
# Install desktop file
desktop-file-install --vendor="" --remove-category=Application --dir=%{buildroot}%{_datadir}/applications unetbootin.desktop
# Install localization files
install -d %{buildroot}%{_datadir}/unetbootin
install -c -p -m 644 unetbootin_*.qm %{buildroot}%{_datadir}/unetbootin/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.TXT
%{_bindir}/unetbootin
%{_datadir}/unetbootin/
%{_datadir}/applications/unetbootin.desktop

%changelog
* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (581-1m)
- update to revision 581
- build against syslinux-5.00-1m

* Wed Feb  8 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (568-1m)
- Update to revision 568.

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (485-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (485-2m)
- rebuild for new GCC 4.5

* Tue Sep 14 2010 McLellan Daniel <daniel.mclellan@gmail.com>
- initial momonga release

* Tue Jun 22 2010 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-8.485bzr
- Update to revision 485.

* Tue Jun 22 2010 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-8.471bzr
- Update to revision 471.

* Mon Mar 29 2010 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-8.419bzr
- Update to revision 419.

* Wed Feb 03 2010 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-8.393bzr
- Update to revision 393.

* Sun Dec 06 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-7.377bzr
- Update to revision 377.

* Fri Oct 23 2009 Milos Jakubicek <xjakub@fi.muni.cz> 0-6.358bzr
- Fix FTBFS: bump release to be able to tag in new branch and sync source name

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-6.357bzr
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jul 16 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-6.356bzr
- Add ExclusiveArch to prevent build on architectures lacking syslinux.

* Fri Jul 10 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-5.356bzr
- Fix EPEL install.

* Fri Jul 10 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-4.356bzr
- Fix EPEL build.

* Fri Jul 10 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-3.356bzr
- Added localizations.

* Fri Jul 10 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-2.356bzr
- Fixed source URL.
- Changed Req: p7zip to p7zip-plugins.
- Use included desktop file.

* Fri Jul 10 2009 Jussi Lehtola <jussilehtola@fedoraproject.org> - 0-1.356bzr
- First release.
