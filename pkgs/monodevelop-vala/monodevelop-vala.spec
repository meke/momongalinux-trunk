%global momorel 2

Summary: monodevelop vala plugin
Name: monodevelop-vala
Version: 2.6.0.1
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: "BSDL like"
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk-sharp2-devel >= 2.12.9
BuildRequires: webkitgtk-devel >= 1.1.15.4
BuildRequires: mono-devel >= 2.6.7
BuildRequires: monodevelop >= 2.6.0.1
Requires: mono-core
Requires: gtk2
Requires: gtk-sharp2
Requires: monodevelop >= 2.4

%description
ValaBinding is a Vala language binding for MonoDevelop.

%prep
%setup -q

%build
%configure --program-prefix=""
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_prefix}/lib/monodevelop/AddIns/BackendBindings/MonoDevelop.ValaBinding.dll

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0.1-2m)
- rebuild for mono-2.10.9

* Fri Sep 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0.1-1m)
- update to 2.6.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- rebuild for new GCC 4.5

* Mon Oct  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-3m)
- use BuildRequires

* Thu Dec 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-2m)
- fix build on x86_64

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- initial build
