%global momorel 6
%global wbxml2ver 0.10.7

Summary: Library providing support for the SyncML protocol
Name: libsyncml
Version: 0.5.1
Release: %{momorel}m%{?dist}
Epoch: 1
License: LGPLv2+
URL: http://libsyncml.opensync.org/
Group: System Environment/Libraries
Source0: https://libsyncml.opensync.org/download/releases/%{version}/%{name}-%{version}.tar.bz2
# NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake
BuildRequires: libsoup-devel
BuildRequires: libtool
BuildRequires: libusb-devel
BuildRequires: libxml2-devel
BuildRequires: openobex-devel >= 1.4
BuildRequires: pkgconfig
BuildRequires: wbxml2-devel >= %{wbxml2ver}

%description
Libsyncml is a implementation of the SyncML protocol. It allows
programs like OpenSync the synchronization of SyncML-enabled devices,
such as the SonyEricsson P800, as well as remote OpenSync to OpenSync
synchronization over the internet. You need to install the
libopensync-* packages and msynctool for that, too.

%package devel
Summary: Header files and static libraries from libsyncml
Group: Development/Libraries
Requires: %{name} = %{epoch}:%{version}-%{release}
Requires: libsoup-devel
Requires: openobex-devel
Requires: pkgconfig
Requires: wbxml2-devel >= %{wbxml2ver}

%description devel
Libraries and includes files for developing programs based on libsyncml.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
	-DCMAKE_SKIP_RPATH=YES ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL LICENSE README
%{_bindir}/syncml-ds-tool
%{_libdir}/libsyncml.so.*

%files devel
%defattr(-,root,root)
%doc CODING RELEASE
%{_includedir}/libsyncml-1.0
%{_libdir}/pkgconfig/libsyncml-1.0.pc
%{_libdir}/libsyncml.so

%changelog
* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.5.1-6m)
- use libsoup-devel instead of libsoup-2.2-devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.5.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.5.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.5.1-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.5.1-2m)
- add epoch to %%changelog

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.5.1-1m)
- version 0.5.1
- add LICENSE to %%doc
- add CODING and RELEASE to %%doc of devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.5.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Mar 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.5.0-2m)
- add Epoch: 1 for upgrading from STABLE_5
- License: LGPLv2+

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0
- we cannot build libopensync-plugin-syncml with libsyncml-0.5.1...

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20081004.3m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.20081004.2m)
- rebuild against bluez and openobex

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20081004.1m)
- update to 1.0 svn snapshot

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-4m)
- rebuild against gnutls-2.4.1 (libsoup-2.2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.6-3m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-2m)
- rebuild against libsoup-2.2.105
- BPR libsoup-2.2

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- version 0.4.6

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-2m)
- BuildRequires: libusb-devel

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-1m)
- initial package for libopensync-plugin-sunbird
- Summary and %%description are imported from opensuse
