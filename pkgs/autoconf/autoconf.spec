%global momorel 1

Summary: A GNU tool for automatically configuring source code
Name: autoconf
%{?include_specopt}
%{?!do_test: %global do_test 1}
Version: 2.69
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Source: http://ftp.gnu.org/gnu/autoconf/autoconf-%{version}.tar.xz
NoSource: 0
Source1:    config.site
Patch0: autoconf-2.64-nocheck-pallarel.patch

URL: http://www.gnu.org/software/autoconf/
BuildRequires: sed, m4, emacs >= %{_emacs_version}
Requires(post): info
Requires(preun): info
Requires: gawk, m4, mktemp, perl, textutils
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GNU's Autoconf is a tool for configuring source code and Makefiles.
Using Autoconf, programmers can create portable and configurable
packages, since the person building the package is allowed to 
specify various configuration options.
#'

You should install Autoconf if you are developing software and
would like to create shell scripts that configure your source code
packages. If you are installing Autoconf, you will also need to
install the GNU m4 package.

Note that the Autoconf package is not required for the end-user who
may be configuring software with an Autoconf-generated script;
Autoconf is only required for the generation of the scripts, not
their use.

%package -n emacs-autoconf
Summary: Autoconf code editing commands for Emacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
Obsoletes: elisp-autoconf

%description -n emacs-autoconf
A major mode for editing autoconf input (like configure.in).

%prep
%setup -q -n autoconf-%{version}
%patch0 -p1 -b .pallarel

%build
%configure
make

%if %{do_test}
%check
make check || make check VERBOSE=yes
%endif

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
mkdir -p %{buildroot}/share
install -m 0644 %{SOURCE1} %{buildroot}%{_datadir}

rm -f %{buildroot}%{_infodir}/dir

%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/autoconf
mv %{buildroot}%{_emacs_sitelispdir}/auto*el* %{buildroot}%{_emacs_sitelispdir}/autoconf

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/autoconf.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --del %{_infodir}/autoconf.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%{_bindir}/*
%{_infodir}/autoconf.info*
# don't include standards.info, because it comes from binutils...
%exclude %{_infodir}/standards*
%{_datadir}/autoconf
%config %{_datadir}/config.site
%{_mandir}/man1/*
%doc AUTHORS COPYING ChangeLog NEWS README THANKS TODO

%files -n emacs-autoconf
%defattr(-,root,root)
%dir %{_emacs_sitelispdir}/autoconf
%{_emacs_sitelispdir}/autoconf/autoconf-mode.el*
%{_emacs_sitelispdir}/autoconf/autotest-mode.el*

%changelog
* Tue Jul 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.69-1m)
- update 2.69
-- can not build software of recent

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.68-5m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.68-4m)
- rename elisp-autoconf to emacs-autoconf

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.68-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.68-2m)
- rebuild for new GCC 4.5

* Thu Sep 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.68-1m)
- update to 2.68 (merge from TP4R)
--
--* Tue Aug  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
--- (2.67-1m)
--- update to 2.67
--
--* Sun Jul  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
--- (2.66-2m)
--- add patch1 (from Eric Blake)
--
--* Sat Jul  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
--- (2.66-1m)
--- update to 2.66

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.65-3m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.65-2m)
- rebuild against emacs-23.2
- split out elisp-autoconf

* Sat May 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.65-1m)
- update to 2.65
- import from trunk
---* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
---- (2.65-2m)
---- use Requires:
---* Sun Nov 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
---- (2.65-1m)
---- update to 2.65

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.63-11m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.63-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.63-9m)
- back to 2.63

* Sun Aug  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.64-1m)
- update to 2.64
- add patch0 (no check)

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.63-8m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.63-7m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.63-6m)
- rebuild against emacs-23.0.95

* Tue May 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.63-1m)
- add patch1 (for build fix only with libtool-2.2.6a)

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.63-4m)
- rebuild against emacs-23.0.94
- fix install-info

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.63-3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.63-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.63-1m)
- update 2.63

* Sat Apr  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.61-5m)
- add patch0 (not check gfortran)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.61-4m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.61-3m)
- rebuild against perl-5.10.0-1m

* Sat Nov 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.61-2m)
- use autoconf.specopt condition for %%check

* Sat Nov 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.61-1m)
- update 2.61

* Mon Oct 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.60-1m)
- update 2.60

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.59-3m)
- delete duplicated dir

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.59-2m)
- seperate automake213 and automake
- obsolete automake-2.53

* Sat Aug  7 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.59-1m)
- update to 2.59

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.58-2m)
- revised spec for enabling rpm 4.2.

* Tue Dec  9 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.58-1m)
- update to 2.58

* Wed Sep 24 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.57-1m)
- version 2.57

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.56-7m)
- add URL

* Fri Jan 31 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.56-6m)
- revice %%post, %%preun

* Thu Jan 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.56-5m)
- check if $1 is 0 when deleting autoconf.info* from dir in %%preun
  thanks Blend <blend @ manabi.gr.jp>

* Mon Dec  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.56-4m)
- fix error around m4

* Sun Dec  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.56-3m)
- add autoconf-2.53
- divide autoconf-suppress-cpp-warning.patch into each version
- add autoconf-2.53-autoheader-warn.patch for autoconf-2.53 (from RedHat-8.0)
- change data directory from /usr/share/autoconf to /usr/share/autoconf-2.56
- replace embedded build directory names to "/usr/src/momonga"

* Sat Nov 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.56-2m)
- add BuildRequires: libtool, but this make dependency loop with libtool?

* Sun Nov 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.56-1m)
- update to 2.56
- add redhat compatible old commands(autoconf-2.13, autoheader-2.13, ...)
- import patches for 2.13 from redhat-8.0(autoconf213-4). 
  autoconf-throw.patch is replaced with autoconf-2.13-c++exit.patch.
  patches are:
   autoconf-2.13-c++exit.patch
   autoconf-2.13-headers.patch
   autoconf-2.13-autoscan.patch
   autoconf-2.13-exit.patch
   autoconf-2.13-wait3test.patch
   autoconf-2.13-make-defs-62361.patch

* Sun May 26 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.53-6m)
- add BuildRequires

* Sat May 25 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.53-4k)
- suppress cpp warning(Patch2)

* Wed May 22 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (2.53-2k)
- removed mawk patch (no longer needed)

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.52-10k)
- /sbin/install-info -> info in PreReq.

* Fri Oct 12 2001 Motonobu Ichimura <famao@kondara.org>
- (2.52-9k)
- up to 2.52d and other stuff

* Fri Aug 31 2001 Motonobu Ichimura <famao@kondara.org>
- merging autoconf 2.13 and autoconf 2.52

* Tue Apr  3 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- applied throw patch

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Sun Jul  2 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.6.0

* Sun Feb 27 2000 Kenichi Matsubara <m@kondara.org>
- modified %files section.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Mar 26 1999 Cristian Gafton <gafton@redhat.com>
- add patch to help autoconf clean after itself and not leave /tmp clobbered
  with acin.* and acout.* files (can you say annoying?)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)
- use gawk, not mawk

* Thu Mar 18 1999 Preston Brown <pbrown@redhat.com>
- moved /usr/lib/autoconf to /usr/share/autoconf (with automake)

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Tue Jan 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.13.

* Fri Dec 18 1998 Cristian Gafton <gafton@redhat.com>
- build against glibc 2.1

* Mon Oct 05 1998 Cristian Gafton <gafton@redhat.com>
- requires perl

* Thu Aug 27 1998 Cristian Gafton <gafton@redhat.com>
- patch for fixing /tmp race conditions

* Sun Oct 19 1997 Erik Troan <ewt@redhat.com>
- spec file cleanups
- made a noarch package
- uses autoconf
- uses install-info

* Thu Jul 17 1997 Erik Troan <ewt@redhat.com>
- built with glibc

