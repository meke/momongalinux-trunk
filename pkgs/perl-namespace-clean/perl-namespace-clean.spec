%global         momorel 2

Name:           perl-namespace-clean
Version:        0.25
Release:        %{momorel}m%{?dist}
Summary:        Keep imports and functions out of your namespace
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/namespace-clean/
Source0:        http://www.cpan.org/authors/id/R/RI/RIBASUSHI/namespace-clean-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-B-Hooks-EndOfScope >= 0.07
BuildRequires:  perl-constant
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Package-Stash >= 0.18
BuildRequires:  perl-Sub-Identify >= 0.04
BuildRequires:  perl-Sub-Name >= 0.04
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-B-Hooks-EndOfScope >= 0.07
Requires:       perl-Package-Stash >= 0.18
Requires:       perl-Sub-Identify >= 0.04
Requires:       perl-Sub-Name >= 0.04
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Keeping packages clean

%prep
%setup -q -n namespace-clean-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/namespace/
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-2m)
- rebuild against perl-5.16.3

* Wed Dec  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.16.0

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Tue Dec 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.14.2

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-2m)
- rebuild for new GCC 4.6

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-2m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sun Jun  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sun Jan 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Thu Jan 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.10.1

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.75 for Momonga Linux.
