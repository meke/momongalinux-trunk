%global momorel 21
%global php_ver 5.4.1

%global pkg_name chasen

Summary: chasen module for PHP
Name: php-%{pkg_name}
Version: 0.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Languages
URL: http://pecl.php.net/package/chasen
#Source0: http://pecl.php.net/get/%{pkg_name}-%{version}.tgz
#NoSource: 0
Source0:   %{pkg_name}-%{version}.tar.bz2
Source1:   %{pkg_name}.ini
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: chasen
BuildRequires: chasen-devel
Requires: php >= %{php_ver}
BuildRequires: php-devel >= %{php_ver}

Patch1: chasen-empty_string.patch
Patch2: %{name}-%{version}-php54.patch

%description
chasen module for PHP

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}

%patch1 -p1 -b .empty_string
%patch2 -p1 -b .php54

%{_bindir}/phpize
%configure

%build
# Build main module
%make

%install
rm -rf %{buildroot}
%makeinstall INSTALL_ROOT=%{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/php.d
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/php.d/%{pkg_name}.ini

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc  CREDITS README.ja
%config(noreplace) %{_sysconfdir}/php.d/%{pkg_name}.ini
%{_libdir}/php/modules/%{pkg_name}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-21m)
- rebuild against php-5.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-18m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-17m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-15m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-14m)
- rebuild against php 5.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-13m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-12m)
- rebuild against chasen-2.4.2-1m

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-11m)
- rebuild against php-5.2.1

* Sat May 20 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-10m)
- add chasen.ini
- clean up spec

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-9m)
- rebuild against PHP 5.1.4 

* Fri Jan 13 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-8m)
- rebuild against PHP 5.1.2 

* Thu Dec 29 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-7m)
- revise patch 

* Sat Dec 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-6m)
- fix compile error

* Sat Dec 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-5m)
- rebuild against php-5.1.1-1m

* Sat Nov  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.1-4m)
- rebuild against php-5.0.5-8m

* Tue Apr  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.1-3m)
- rebuild against php-5.0.4

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-2m)
- rebuild against php-5.0.3

* Tue Oct 12 2004 Yasuo Ohgaki <yoghaki@ohgaki.net>
- (0.1-1m)
- Initial version
