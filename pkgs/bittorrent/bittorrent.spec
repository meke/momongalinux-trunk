%global momorel 2
%global pythonver 2.7.1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           bittorrent
Version:        5.2.2
Release:	%{momorel}m%{?dist}
Summary:        BitTorrent swarming network file transfer tool

Group:          Applications/Internet
License:        "see LICENSE.txt (BitTorrent Open Source License)"
URL:            http://www.bittorrent.com/
Source0:        http://download.bittorrent.com/dl/archive/BitTorrent-%{version}.tar.gz
Source1:        bittorrent.desktop
Source2:        bittorrent.png
Source3:	btseed.init
Source4:	bttrack.init
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  desktop-file-utils gettext
Requires:       python-crypto
Requires:       python >= %{pythonver}
# FC4 and later auto-generate python version dependency
#%%if %{!?fedora:0}%{?fedora} < 4 || %{!?rhel:0}%{?rhel}
#%Requires:       python-abi = %(%{__python} -c "import sys ; print sys.version[:3]")
#%%endif

Provides:	python-khashmir = %{version}-%{release}
Obsoletes:	python-khashmir < %{version}-%{release}

Requires(pre):  shadow-utils
# /usr/sbin/useradd
Requires(post): chkconfig
#/sbin/chkconfig
Requires(post): initscripts
#/sbin/service
Requires(preun):chkconfig
#/sbin/chkconfig
Requires(preun): initscripts
#/sbin/service
Requires(postun): initscripts
#/sbin/service

%description
BitTorrent is a tool for distributing files. It's extremely easy to use -
downloads are started by clicking on hyperlinks. Whenever more than one person
is downloading at once they send pieces of the file(s) to each other, thus
relieving the central server's bandwidth burden. Even with many simultaneous
downloads, the upload burden on the central server remains quite small, since
each new downloader introduces new upload capacity.

%package        gui
Summary:        GUI versions of the BitTorrent file transfer tool
Group:          Applications/Internet
Requires:       pygtk >= 2.6.0
Requires:       %{name} = %{version}-%{release}
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Obsoletes:	BitTorrent <= %{version}
Provides:	BitTorrent = %{version}-%{release}

%description    gui
This package contains the GUI versions of the BitTorrent file transfer
tool.

%define bt_dir      /srv/bittorrent/data
%define bt_statedir /srv/bittorrent/state

%define pkidir %{_sysconfdir}/pki

%prep
%setup -q -n BitTorrent-%{version}

# Our package is "bittorrent", not "BitTorrent"
%{__sed} -i "s#appdir = .*#appdir = '%{name}-%{version}'#" \
	BitTorrent/platform.py

# Put public key in standard place
%{__sed} -i "s#public_key_file = .*#public_key_file = open('%{pkidir}/bittorrent/public.key', 'rb')#" \
	BitTorrent/NewVersion.py

# Remove useless shellbangs
%{__sed} -i -e '/^#! *\/usr\/bin\/env python/d' BitTorrent/launchmanycore.py BitTorrent/makemetafile.py

# Create options files for initscripts
%{__cat} <<EOF >bittorrent.sysconfig
SEEDDIR=%{bt_dir}
SEEDOPTS="--max_upload_rate 350 --display_interval 300"
SEEDLOG=%{_localstatedir}/log/bittorrent/btseed.log
TRACKPORT=6969
TRACKDIR=%{bt_dir}
TRACKSTATEFILE=%{bt_statedir}/bttrack
TRACKLOG=%{_localstatedir}/log/bittorrent/bttrack.log
TRACKOPTS="--min_time_between_log_flushes 4.0 --show_names 1 --hupmonitor 1"
EOF

# Have the services' log files rotated
%{__cat} <<EOF >bittorrent.logrotate
%{_localstatedir}/log/bittorrent/btseed.log {
	notifempty
	missingok
	postrotate
		/sbin/service btseed condrestart 2>/dev/null >/dev/null || :
	endscript
}

%{_localstatedir}/log/bittorrent/bttrack.log {
	notifempty
	missingok
	postrotate
		/sbin/service bttrack condrestart 2>/dev/null >/dev/null || :
	endscript
}
EOF

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

/usr/bin/desktop-file-install --vendor=  \
	--dir %{buildroot}%{_datadir}/applications \
	%{SOURCE1}
# --add-category X-Fedora \
#
%{__install} -d %{buildroot}%{bt_dir}
%{__install} -d %{buildroot}%{bt_statedir}
%{__install} -d %{buildroot}%{_localstatedir}/{run,log/bittorrent}
%{__install} -d %{buildroot}%{pkidir}/bittorrent
%{__install} -m 0644 public.key              %{buildroot}%{pkidir}/bittorrent
%{__install} -m 0644 -D %{SOURCE2}           %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/bittorrent.png
%{__install} -m 0755 -D %{SOURCE3}           %{buildroot}%{_initscriptdir}/btseed
%{__install} -m 0755 -D %{SOURCE4}           %{buildroot}%{_initscriptdir}/bttrack
%{__install} -m 0644 -D bittorrent.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/bittorrent
%{__install} -m 0644 -D bittorrent.logrotate %{buildroot}%{_sysconfdir}/logrotate.d/bittorrent
# pidof doesn't find scripts with hyphenated names, so make some convenience links for initscripts
%{__ln_s} bittorrent-tracker %{buildroot}%{_bindir}/bttrack
%{__ln_s} launchmany-console %{buildroot}%{_bindir}/btseed
# ghost the tracker pidfile so it goes on package removal
: > %{buildroot}%{_localstatedir}/run/bittorrent-tracker.pid
%{find_lang} bittorrent

%clean
%{__rm} -rf %{buildroot}

%pre
/usr/sbin/useradd -r -s /sbin/nologin -d %{_localstatedir}/spool/bittorrent \
	-c "BitTorrent Seed/Tracker" torrent &>/dev/null || :

%post
/sbin/chkconfig --add btseed  || :
/sbin/chkconfig --add bttrack || :

%preun
if [ $1 -eq 0 ]; then
	/sbin/service btseed  stop &>/dev/null || :
	/sbin/service bttrack stop &>/dev/null || :
	/sbin/chkconfig --del btseed  || :
	/sbin/chkconfig --del bttrack || :
fi

%postun
if [ $1 -gt 0 ]; then
	/sbin/service btseed  condrestart &>/dev/null || :
	/sbin/service bttrack condrestart &>/dev/null || :
fi

%post gui
/usr/bin/update-desktop-database %{_datadir}/applications &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor || :
[ -x /usr/bin/gtk-update-icon-cache ] && /usr/bin/gtk-update-icon-cache -q %{_datadir}/icons/hicolor || :

%postun gui
/usr/bin/update-desktop-database %{_datadir}/applications &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor || :
[ -x /usr/bin/gtk-update-icon-cache ] && /usr/bin/gtk-update-icon-cache -q %{_datadir}/icons/hicolor || :

%files -f bittorrent.lang
%defattr(-,root,root,0755)
%doc README.txt credits.txt LICENSE.txt TRACKERLESS.txt
%{_bindir}/bittorrent-console
%{_bindir}/bittorrent-curses
%{_bindir}/bittorrent-tracker
%{_bindir}/changetracker-console
%{_bindir}/launchmany-console
%{_bindir}/launchmany-curses
%{_bindir}/maketorrent-console
%{_bindir}/torrentinfo-console
%{_bindir}/bttrack
%{_bindir}/btseed
%{pkidir}/bittorrent
%{python_sitelib}/Zeroconf*
%{python_sitelib}/BitTorrent/
%{python_sitelib}/BTL/
%{python_sitelib}/khashmir/
%{python_sitelib}/BitTorrent-*.egg-info
%attr(-,torrent,torrent) %dir %{bt_dir}
%attr(-,torrent,torrent) %dir %{bt_statedir}
%attr(-,torrent,torrent) %dir %{_localstatedir}/log/bittorrent
%ghost %{_localstatedir}/run/bittorrent-tracker.pid
%{_initscriptdir}/btseed
%{_initscriptdir}/bttrack
%config(noreplace) %{_sysconfdir}/logrotate.d/bittorrent
%config(noreplace) %{_sysconfdir}/sysconfig/bittorrent

%files gui
%defattr(-,root,root,0755)
%{_bindir}/bittorrent
%{_bindir}/maketorrent
%{_datadir}/pixmaps/bittorrent-%{version}
%{_datadir}/icons/hicolor/48x48/apps/bittorrent.png
%{_datadir}/applications/bittorrent.desktop

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.2-2m)
- add source

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.5.2-1m)
- update 5.5.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.0-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.0-8m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.4.0-7m)
- rebuild against python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-6m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4.0-5m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-4m)
- rebuild against gcc43

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.0-3m)
- rebuild against python-2.5

* Wed Nov  8 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-2m)
- use %%{_initscriptdir}

* Tue Aug 29 2006 Paul Howarth <paul@city-fan.org> 4.4.0-2
- Don't need python-devel buildreq
- Don't %%ghost .pyo files

* Tue Apr 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- nosrc
- s/pygtk2/pygtk/g
- add -q at setup
- TODO: add for Requires: python-crypto

* Thu Feb  2 2006 Paul Howarth <paul@city-fan.org> 4.4.0-1
- Update to 4.4.0
- Hardcode /usr/sbin in useradd dependency path
- Update %%description
- GUI subpackage now requires pygtk2 >= 2.6.0 so no FC3 or RHEL4 build
- Specify explicit path for runuser in initscripts
- Merge /etc/sysconfig/{btseed,bttrack} into /etc/sysconfig/bittorrent
- Merge /etc/logrotate.d/{btseed,bttrack} into /etc/logrotate.d/bittorrent
- Fix initscript for bttrack to allow for pidfile creation
- Ghost /var/run/bittorrent-tracker.pid so it gets deleted on package removal
- Add comments in %%prep
- Remove shellbangs from python files to reduce rpmlint output
- Replace $RPM_BUILD_ROOT with %%{buildroot} (packager's personal preference)

* Tue Dec 20 2005 Paul Howarth <paul@city-fan.org> 4.2.2-2
- Add patch to prevent phoning home for new version checks

* Mon Dec 19 2005 Paul Howarth <paul@city-fan.org> 4.2.2-1
- Update to 4.2.2

* Tue Dec  6 2005 Paul Howarth <paul@city-fan.org> 4.2.1-1
- Update to 4.2.1
- Source now at www.bittorrent.com rather than sourceforge

* Wed Nov 23 2005 Paul Howarth <paul@city-fan.org> 4.2.0-1
- update to 4.2.0

* Wed Nov 16 2005 Paul Howarth <paul@city-fan.org> 4.1.8-1
- update to 4.1.8

* Thu Nov  3 2005 Paul Howarth <paul@city-fan.org> 4.1.7-1
- update to 4.1.7
- don't use macros in build-time and scriptlet command paths

* Thu Oct 13 2005 Paul Howarth <paul@city-fan.org> 4.1.6-2
- append to seed log file instead of overwriting it

* Thu Oct 13 2005 Paul Howarth <paul@city-fan.org> 4.1.6-1
- update to 4.1.6

* Thu Aug 18 2005 Paul Howarth <paul@city-fan.org> 4.1.4-1
- update to 4.1.4
- all "binaries" renamed; initscripts & desktop entry changed accordingly
- don't need to edit out "env python2"... stuff
- appdir now set in platform.py instead of __init__.py
- add credits-l10n.txt as %%doc
- add python-crypto dependency
- add public.key in appropriate place
- tweak Fedora/RHEL conditional build selection

* Fri Jul 15 2005 Paul Howarth <paul@city-fan.org> 4.1.3-1
- update to 4.1.3

* Fri Jul 15 2005 Paul Howarth <paul@city-fan.org> 4.1.2-6
- provides should be version-release tagged too

* Tue Jul 11 2005 Paul Howarth <paul@city-fan.org> 4.1.2-5
- move icon to %{_datadir}/icons/hicolor/48x48/apps instead of
  %{_datadir}/pixmaps and update icon cache on installation if
  necessary (#162465)
- no need to set CFLAGS in %%build for noarch package
- don't offer build option not to delete usr/group on uninstall
- don't generate redundant python-abi dependency for FC >= 4

* Thu Jul  7 2005 Paul Howarth <paul@city-fan.org> 4.1.2-4
- bump and rebuild

* Tue Jul  5 2005 Paul Howarth <paul@city-fan.org> 4.1.2-3
- obsolete python-khashmir based on version-release, not just version

* Mon Jul  4 2005 Paul Howarth <paul@city-fan.org> 4.1.2-2
- add Provides/Obsoletes for python-khashmir for clean upgrades from split
  bittorrent packages

* Mon Jun 13 2005 Paul Howarth <paul@city-fan.org> 4.1.2-1
- update to 4.1.2
- add gettext build requirement for translations

* Mon Jun  6 2005 Paul Howarth <paul@city-fan.org> 4.1.1-3
- don't delete user/group torrent on uninstall
  (see https://www.redhat.com/archives/fedora-extras-commits/2005-June/msg00271.html)

* Thu Jun  2 2005 Paul Howarth <paul@city-fan.org> 4.1.1-2
- tidy up description text
- improved initscripts (#158273)
- for versions on Fedora Core >= 4 or RHEL, put server data under /srv rather
  than %{_localstatedir}
- separate logs/logrotate scripts for tracker and seeder
- have the gui subpackage obsolete the official BitTorrent RPM package, which
  is called BitTorrent, includes the GUI in the same package and is built
  using python's dist tools and hence doesn't include all dependencies,
  provide initscripts etc.; this provides a clean upgrade path to Extras

* Tue May 31 2005 Paul Howarth <paul@city-fan.org> 4.1.1-1
- update to 4.1.1
- add "khashmir" python library
- add TRACKERLESS.txt as %%doc
- add translations

* Tue May 24 2005 Paul Howarth <paul@city-fan.org> 4.0.2-1
- update to 4.0.2

* Mon May 23 2005 Paul Howarth <paul@city-fan.org> 4.0.1-2
- use macros consistently throughout
- add initscripts so users can easily set up trackers and seeders
  on their own (#158273)
- add user torrent for tracker/seeder
- add logrotate script for tracker

* Wed May 18 2005 Paul Howarth <paul@city-fan.org> 4.0.1-1
- new upstream release 4.0.1 (#157632)
- add dist tag
- source now found on sourceforge.net
- move post/postun scriptlets to -gui subpackage
- bttest.py removed upstream

* Tue Mar 15 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 4.0.0-2
- add desktop file, desktop menu icon, post/postun scriptlets

* Mon Mar 14 2005 David Hill <djh[at]ii.net> - 4.0.0-1
- update to 4.0.0, no longer requires wxPythonGTK

* Wed Dec 22 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:3.4.1-3.a
- x86_64: Fix python dependency, replace with python-abi dependency.

* Mon Mar 29 2004 Marius L. Johndal <mariuslj at ifi.uio.no> - 0:3.4.1-0.fdr.2.a
- Changed Requires wxPythonGTK to wxPython (bug 1416).
- Converted spec file to UTF-8.

* Thu Mar 25 2004 Marius L. Johndal <mariuslj at ifi.uio.no> - 0:3.4.1-0.fdr.1.a
- Updated to 3.4.1a.
- Brought in line with the Python spec template.
- Fixed group and missing epoch in requires for gui subpackage.
- Eliminated multiple copies of README.txt.
- Eliminated perl BuildReq.

* Wed Jul 16 2003 Seth Vidal <skvidal@phy.duke.edu>
- rebuild under 7.3
- some fixes

* Wed Jul 16 2003 Warren Togami <warren@togami.com> 3.2.1-0.fdr.2.b
- Fedoraize

* Wed Jul 16 2003 Seth Vidal <skvidal@phy.duke.edu>
- rebuilt on rhl 9 - testing

* Fri May 30 2003 Peter Hanecak <hanecak@megaloman.sk> 3.2.1b-1
- adapted to Doors Linux

* Sun Mar 30 2003 Gotz Waschk <waschk@linux-mandrake.com> 3.2.1b-1mdk
- split out gui tools to remove wxPythonGTK dep from the main package
- new version

* Fri Mar 28 2003 Frederic Lepied <flepied@mandrakesoft.com> 3.2-1mdk
- 3.2

* Wed Mar 26 2003 Frederic Lepied <flepied@mandrakesoft.com> 3.1-1mdk
- initial packaging
