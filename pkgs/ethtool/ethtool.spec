%global momorel 1

Summary: Ethernet settings tool for PCI ethernet cards
Name: ethtool
Version: 3.13
Release: %{momorel}m%{?dist}
Epoch: 1
License: GPL
Group: Applications/System
URL: http://ftp.kernel.org/pub/software/network/%{name}/
Source0: http://ftp.kernel.org/pub/software/network/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This utility allows querying and changing of ethernet
card settings, such as speed, port, autonegotiation,
and PCI locations.

%prep
%setup -q
autoreconf -ivf

%build
%configure
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} INSTALL='install -p' install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING INSTALL NEWS README
%{_sbindir}/%{name}
%{_mandir}/man8/ethtool.*

%changelog
* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13-1m)
- update 3.13

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.8-1m)
- update to 3.8

* Sun Feb  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.7-1m)
- update to 3.7
- change URL

* Mon Dec 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:3.1-1m)
- update to 3.1

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1:2.6.39-1m)
- update to 2.6.39

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.6.36-2m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.6.36-1m)
- update to 2.6.36

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.6.35-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.6.35-1m)
- update to 2.6.35

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:2.6.34-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.6.34-2m)
- add epoch to %%changelog

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.6.34-1m)
- version renumbering by upstream
- add Epoch: 1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6-5m)
- update 20090323

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6-1m)
- update to 6

* Sun Sep  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5-1m)
- update to 5

* Sun Jul 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4-1m)
- update to 4
- change Source0 URI to dl.sourceforge.net

* Thu Dec 15 2005 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3-1m)
- update to 3
- change Source0 URI to jaist
- use momorel and NoSource macro

* Wed Dec  8 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2-1m)
  update to 2
  
* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.7-1m)
- version 1.7

* Wed May 15 2002 Toru Hoshina <t@kondara.org>
- (1.5-2k)

* Mon Mar  4 2002 Bill Nottingham <notting@redhat.com> 1.5-1
- 1.5

* Thu Feb 21 2002 Bill Nottingham <notting@redhat.com>
- rebuild

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Dec  4 2001 Bill Nottingham <notting@redhat.com>
- update to 1.4

* Fri Aug  3 2001 Bill Nottingham <notting@redhat.com>
- return of ethtool! (#50475)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- rebuilt for next release
- use FHS man path

* Tue Feb 22 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Wed Apr 14 1999 Bill Nottingham <notting@redhat.com>
- run through with new s/d

* Tue Apr 13 1999 Jakub Jelinek <jj@ultra.linux.cz>
- initial package.
