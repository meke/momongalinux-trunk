%global momorel 10

%ifarch x86_64 ppc64 sparc64
%global archdep 1
%endif
%global jdk_path /usr/lib/jvm/java-1.6.0-openjdk%{?archdep:.%{_arch}}

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:           checkstyle
Version:        4.1
Release:        4jpp.3.%{momorel}m%{?dist}
#Epoch:          0
Summary:        Java source code checker
License:        LGPL
Group:          Development/Tools
Source0:        http://download.sf.net/checkstyle/checkstyle-src-4.1.tar.gz
Source1:        %{name}-%{version}-script
Source2:        %{name}-%{version}.catalog

Patch0:         %{name}-%{version}-build.patch
#Patch1:         %{name}-%{version}-javadoc-crosslink.patch

Patch2:         %{name}-%{version}-checks-AllTests.patch
Patch3:         %{name}-%{version}-checks-blocks-AllTests.patch
Patch4:         %{name}-%{version}-checks-coding-AllTests.patch
Patch5:         %{name}-%{version}-checks-design-AllTests.patch
Patch6:         %{name}-%{version}-checks-imports-AllTests.patch
Patch7:         %{name}-%{version}-checks-indentation-AllTests.patch
Patch8:         %{name}-%{version}-checks-javadoc-AllTests.patch
Patch9:         %{name}-%{version}-checks-metrics-AllTests.patch
Patch10:        %{name}-%{version}-checks-modifier-AllTests.patch
Patch11:        %{name}-%{version}-checks-naming-AllTests.patch
Patch12:        %{name}-%{version}-checks-sizes-AllTests.patch
Patch13:        %{name}-%{version}-checks-whitespace-AllTests.patch
Patch14:        %{name}-%{version}-grammars-AllTests.patch

URL:            http://checkstyle.sourceforge.net/
Requires:       ant >= 0:1.6
Requires:       antlr >= 0:2.7.1, regexp >= 0:1.2, jakarta-commons-logging
Requires:       jakarta-commons-cli, jakarta-commons-beanutils
Requires:       jakarta-commons-collections, jpackage-utils >= 0:1.5
Requires:       jaxp_parser_impl
BuildRequires:  ant >= 0:1.6, ant-nodeps >= 0:1.6
BuildRequires:  ant-junit >= 0:1.6, junit, antlr >= 0:2.7.1
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  jakarta-commons-beanutils
BuildRequires:  jakarta-commons-cli, xalan-j2, jpackage-utils >= 0:1.5
BuildRequires:  jakarta-commons-logging, jakarta-commons-collections, xerces-j2
BuildRequires:  antlr-javadoc, xml-commons-apis-javadoc
BuildRequires:  jakarta-commons-beanutils-javadoc, ant-javadoc, /usr/bin/perl
BuildRequires:  jdom
BuildRequires:  velocity
BuildRequires:  java-1.6.0-openjdk-devel
Requires:       antlr >= 0:2.7.1
Requires:       java
Requires:       jpackage-utils
Requires:       jakarta-commons-beanutils
Requires:       jakarta-commons-cli, xalan-j2, jpackage-utils >= 0:1.5
Requires:       jakarta-commons-logging, jakarta-commons-collections, xerces-j2
Requires:       antlr-javadoc, xml-commons-apis-javadoc
Requires:       jakarta-commons-beanutils-javadoc, ant-javadoc, /usr/bin/perl
Requires:       jdom
Requires:       velocity
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A tool for checking Java source code for adherence to a set of rules.

%package        demo
Group:          Development/Tools
Summary:        Demos for %{name}
Requires:       %{name} = %{version}

%description    demo
Demonstrations and samples for %{name}.

%package        javadoc
Group:          Documentation
Summary:        Javadoc for %{name}

%description    javadoc
Javadoc for %{name}.

%package        manual
Group:          Documentation
Summary:        Manual for %{name}

%description    manual
Manual for %{name}.

%package        optional
Group:          Development/Tools
Summary:        Optional functionality for %{name}
Requires:       %{name} = %{version}
Requires(post):   sed
Requires(postun): sed

%description    optional
Optional functionality for %{name}.

%prep
%setup -q -n %{name}-src-%{version}
%patch0 -b .sav
#%%patch1 -p0
%patch2 -b .sav
%patch3 -b .sav
%patch4 -b .sav
%patch5 -b .sav
%patch6 -b .sav
%patch7 -b .sav
%patch8 -b .sav
%patch9 -b .sav
%patch10 -b .sav
%patch11 -b .sav
%patch12 -b .sav
%patch13 -b .sav
%patch14 -b .sav
%__perl -p -i -e 's|\./{\@docRoot}/\.\./index\.html|%{_docdir}/%{name}-manual-%{version}/index.html|' build.xml
# remove all binary libs
find . -name "*.jar" -exec %__rm -f {} \;

%build
export JAVA_HOME=%{jdk_path}
export OPT_JAR_LIST="ant/ant-junit junit ant/ant-nodeps jdom velocity"
export CLASSPATH=$(build-classpath commons-collections)

pushd lib
ln -sf $(build-classpath antlr) .
ln -sf $(build-classpath commons-beanutils-core) .
ln -sf $(build-classpath commons-collections) .
ln -sf $(build-classpath commons-cli) .
ln -sf $(build-classpath commons-logging) .

# FIXME: re-enable when Fedora has emma
#ln -sf $(build-classpath emma) .
#ln -sf $(build-classpath emma_ant) .
popd

# FIXME: target run.tests disabled because it will need emma which Fedora does 
# not have yet

ant \
  -Dbuild.sysclasspath=first \
  -Dant.javadoc=%{_javadocdir}/ant \
  -Dantlr.javadoc=%{_javadocdir}/antlr \
  -Djaxp.javadoc=%{_javadocdir}/xml-commons-apis \
  -Dbeanutils.javadoc=%{_javadocdir}/jakarta-commons-beanutils \
  build.bindist

%install
%__rm -rf %{buildroot}

# jar
%__mkdir_p %{buildroot}%{_javadir}
%__cp -pa target/dist/%{name}-%{version}/%{name}-%{version}.jar \
  %{buildroot}%{_javadir}
%__cp -pa target/dist/%{name}-%{version}/%{name}-optional-%{version}.jar \
  %{buildroot}%{_javadir}
(cd %{buildroot}%{_javadir} && 
    for jar in *-%{version}.jar; 
        do %__ln_s ${jar} `echo $jar| %__sed "s|-%{version}||g"`; 
    done
)

# script
%__mkdir_p %{buildroot}%{_bindir}
%__cp -pa %{SOURCE1} %{buildroot}%{_bindir}/%{name}

# dtds
%__mkdir_p %{buildroot}%{_datadir}/xml/%{name}
%__cp -pa %{SOURCE2} %{buildroot}%{_datadir}/xml/%{name}/catalog
%__cp -pa src/checkstyle/com/puppycrawl/tools/checkstyle/*.dtd \
  %{buildroot}%{_datadir}/xml/%{name}

# javadoc
%__mkdir_p %{buildroot}%{_javadocdir}/%{name}-%{version}
%__cp -par target/dist/%{name}-%{version}/docs/api/* \
  %{buildroot}%{_javadocdir}/%{name}-%{version}
%__ln_s %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}

# demo
%__mkdir_p %{buildroot}%{_datadir}/%{name}
%__cp -par target/dist/%{name}-%{version}/contrib/* \
  %{buildroot}%{_datadir}/%{name}

# ant.d
%__mkdir_p  %{buildroot}%{_sysconfdir}/ant.d
%__cat > %{buildroot}%{_sysconfdir}/ant.d/%{name} << EOF
checkstyle antlr regexp jakarta-commons-beanutils jakarta-commons-cli jakarta-commons-logging jakarta-commons-collections jaxp_parser_impl
EOF

# fix encoding issues in docs
for i in LICENSE LICENSE.apache README RIGHTS.antlr \
         build.xml checkstyle_checks.xml java.header sun_checks.xml suppressions.xml \
         target/dist/%{name}-%{version}/docs/css/*; do
    tr -d \\r < $i > temp_file; mv temp_file $i
done

%clean
%__rm -rf %{buildroot}

%post
# remove broken entries in %{_sysconfdir}/sgml/catalog
touch %{_sysconfdir}/sgml/catalog
sed --in-place=.rpmsave -e '/^CATALOG.*\%{_sysconfdir}\/sgml\/%{name}-[0-9].*/d' %{_sysconfdir}/sgml/catalog

%{_bindir}/xmlcatalog --sgml --noout --add \
    %{_sysconfdir}/sgml/%{name}-%{version}-%{release}.cat \
    %{_datadir}/xml/%{name}/catalog >/dev/null || :

%preun
%{_bindir}/xmlcatalog --sgml --noout --del \
    %{_sysconfdir}/sgml/%{name}-%{version}-%{release}.cat \
    %{_datadir}/xml/%{name}/catalog >/dev/null || :

%post optional
%__grep -q checkstyle-optional %{_sysconfdir}/ant.d/%{name} || \
%__sed -i -e 's|checkstyle|checkstyle checkstyle-optional|' %{_sysconfdir}/ant.d/%{name} || :

%postun optional
%__grep -q checkstyle-optional %{_sysconfdir}/ant.d/%{name} && \
%__sed -i -e 's|checkstyle-optional ||' %{_sysconfdir}/ant.d/%{name} || :

%files
%defattr(0644,root,root,0755)
%doc LICENSE LICENSE.apache README RIGHTS.antlr
%doc build.xml checkstyle_checks.xml java.header sun_checks.xml suppressions.xml

%{_javadir}/%{name}.jar
%{_javadir}/%{name}-%{version}.jar
%{_datadir}/xml/%{name}
%attr(0755,root,root) %{_bindir}/*
%config(noreplace) %{_sysconfdir}/ant.d/%{name}

%files demo
%defattr(0644,root,root,0755)
%{_datadir}/%{name}

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/*

%files manual
%defattr(0644,root,root,0755)
%doc target/dist/%{name}-%{version}/docs/*

%files optional
%defattr(0644,root,root,0755)
%{_javadir}/%{name}-optional.jar
%{_javadir}/%{name}-optional-%{version}.jar

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-4jpp.3.10m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1-4jpp.3.9m)
- fix up %%post and %%preun

* Thu Dec  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-4jpp.3.8m)
- revise /etc/sgml/catalog handling
- add workaround to fix broken /etc/sgml/catalog

* Wed Dec  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-4jpp.3.7m)
- fix bug in handling of /etc/sgml/catalogs

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-4jpp.3.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-4jpp.3.5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-4jpp.3.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-4jpp.3.3m)
- rebuild against rpm-4.6

* Thu Jul 17 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.1-4jpp.3.2m)
- force use openjdk

* Thu Jul 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-4jpp.3.1m)
- fix rel version

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-4jpp.1m)
- import from Fedora to Momonga

* Mon Apr 07 2008 Deepak Bhole <dbhole@redhat.com> - 0:4.1-4jpp.3
- Require java-devel >= 1.6 for javadocs (bug in sinjdoc prevents building)

* Fri Apr 04 2008 Deepak Bhole <dbhole@redhat.com> - 0:4.1-4jpp.2
- Remove < 1.5 JVM requirement, and keep tests that need 1.5

* Thu Feb 24 2007 Deepak Bhole <dbhole@redhat.com> - 0:4.1-4jpp.1
- Update per Fedora spec
- Removed emma and excalibur-avalon-logkit dependencies

* Thu Mar 30 2006 Ralph Apel <r.apel@r-apel.de> 0:4.1-3jpp
- replace avalon-logkit by excalibur-avalon-logkit as BR

* Wed Feb 22 2006 Ralph Apel <r.apel@r-apel.de> 0:4.1-2jpp
- add exclude to javadoc task iot build with java-1.4.2-bea

* Wed Feb 15 2006 Ralph Apel <r.apel@r-apel.de> 0:4.1-1jpp
- update to 4.1 for JPP-1.7
- reduce dependencies

* Wed Feb 15 2006 Ralph Apel <r.apel@r-apel.de> 0:3.5-2jpp
- set locale iot avoid failure of GeneratedJava14LexerTest

* Mon Feb 21 2005 David Walluck <david@jpackage.org> 0:3.5-1jpp
- 0.3.5
- fix ant task with new ant
- add more files to %%doc

* Fri Aug 20 2004 Ralph Apel <r.apel at r-apel.de> - 0:3.4-4jpp
- Build with ant-1.6.2
- Runtime Req ant >= 0:1.6.2

* Fri Aug 06 2004 Ralph Apel <r.apel at r-apel.de> - 0:3.4-3jpp
- Void change

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 0:3.4-2jpp
- Upgrade to Ant 1.6.X

* Mon Apr 12 2004 Ville Skytta <ville.skytta at iki.fi> - 0:3.4-1jpp
- Update to 3.4.
- Make -optional depend on the main package.
- Update DTD catalog, move DTDs to %%{_datadir}/xml/%%{name}.
- New style versionless javadoc dir symlinking.
- Add -optional jar to classpath in startup script if available.

* Tue Jan 20 2004 David Walluck <david@anti-microsoft.org> 0:3.3-1jpp
- 3.3
- rediff patches
- add `optional' subpackage

* Fri Jul 11 2003 Ville Skytta <ville.skytta at iki.fi> - 0:3.1-2jpp
- Install DTDs into %%{_datadir}/sgml/%%{name}.
- Include catalog for DTDs, and install it if %%{_bindir}/install-catalog
  is available.
- Javadoc crosslinking.

* Wed Jun  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:3.1-1jpp
- Update to 3.1.
- Non-versioned javadoc symlinking.

* Fri Apr  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:3.0-2jpp
- Rebuild for JPackage 1.5.

* Sat Mar  1 2003 Ville Skytta <ville.skytta at iki.fi> - 3.0-1jpp
- Update to 3.0.
- Run unit tests during build.
- Separate manual package.

* Sat Sep 14 2002 Ville Skytta <ville.skytta at iki.fi> 2.4-1jpp
- 2.4.
- No RPM macros in source URL.
- Use (patched) ant build.bindist task to fix docs.

* Thu Jul 11 2002 Ville Skytta <ville.skytta at iki.fi> 2.3-2jpp
- Unbreak build.
- Add shell script.

* Tue Jul  9 2002 Ville Skytta <ville.skytta at iki.fi> 2.3-1jpp
- Updated to 2.3.
- Use sed instead of bash 2 extension when symlinking jars during build.
- BuildRequires ant-optional.

* Fri May 10 2002 Ville Skytta <ville.skytta at iki.fi> 2.2-1jpp
- Updated to 2.2.
- Added versioned requirements.
- Fixed Distribution and Group tags.
- Added demo package.

* Sun Mar 03 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 2.1-1jpp
- first jpp release
