%global momorel 1
# first two digits of version
%define release_version %(echo %{version} | awk -F. '{print $1"."$2}')

Name:           ghex
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        Binary editor for GNOME

Group:          Applications/Editors
License:        GPLv2+
URL:            http://ftp.gnome.org/pub/GNOME/sources/ghex/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/ghex/%{release_version}/ghex-%{version}.tar.xz
NoSource: 0

BuildRequires:  gtk3-devel
BuildRequires:  gettext
BuildRequires:  desktop-file-utils
BuildRequires:  perl-XML-Parser
BuildRequires:  gnome-doc-utils
BuildRequires:  intltool
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description
GHex can load raw data from binary files and display them for editing in the
traditional hex editor view. The display is split in two columns, with
hexadecimal values in one column and the ASCII representation in the other.
A useful tool for working with raw data.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure --disable-scrollkeeper
%make 


%install
make install DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

desktop-file-validate %{buildroot}%{_datadir}/applications/ghex.desktop

%find_lang %{name}-3.0 --all-name --with-gnome


%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}-3.0.lang
%doc AUTHORS COPYING COPYING-DOCS HACKING NEWS README
%{_bindir}/*
%{_datadir}/applications/ghex.desktop
%{_datadir}/GConf/gsettings/ghex.convert
%{_datadir}/glib-2.0/schemas/org.gnome.GHex.gschema.xml
%{_datadir}/icons/hicolor/*/apps/*
%{_libdir}/*.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc


%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Mon Sep  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- reimport from fedora
- update to 3.5.2

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90.2-1m)
- update to 2.90.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.0-5m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24.0-4m)
- fix build

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24.0-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.1-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.1-7m)
- %%NoSource -> NoSource

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-6m)
- delete libtool library

* Tue Sep 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.1-5m)
- rebuild against gail-1.9.2

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.1-4m)
- rebuild against dbus-0.92

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.1-3m)
- rebuild against openssl-0.9.8a

* Wed Nov 23 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-2m)
- rebuild against

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1

* Sat May  8 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.5.0-3m)
- rebuild against for libxml2-2.6.8

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.0-2m)
- revised spec for enabling rpm 4.2.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.0-1m)
- version 2.5.0

* Sun Apr 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-3m)
- rebuild against for XFree86-4.3.0

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0-2m)
  rebuild against openssl 0.9.7a

* Tue Jan 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.90-1m)
- version 1.99.90

* Mon Oct 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.2-1m)
- version 1.99.2

* Wed Sep  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.1-1m)
- version 1.99.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-17m)
- remove .omf, gnome/help

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.1-14k)
- add ghex-1.2.1-omfenc.patch

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.1-12k)
- move zh_CN.GB2312 => zh_CN
- move zh_TW.Big5 => zh_TW

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.2.1-6k)
- rebuild against gettext 0.10.40.

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.1-4k)
- rebuild against for gnome-print-0.29

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- verison 1.2.1

* Tue May 2 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gnome-print-0.25

* Wed Mar 14 2001 Shingo Akagaki <dora@kondara.org>
- (1.2-3k)
- version 1.2

* Thu Nov 16 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.4

* Tue Nov 07 2000 Shingo Akagaki <dora@kondara.org>
- added gnome-print-0.24 patch

* Wed Oct 18 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against gnome-print-0.24.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Jun 22 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.3

* Mon Mar 27 2000 Shingo Akagaki <dora@kondara.org>
- first release
