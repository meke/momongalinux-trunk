%global momorel 1
%global __perl_requires %{SOURCE98}

Summary: The Squid proxy caching server
Name: squid
Version: 3.4.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://www.squid-cache.org/
Source: http://www.squid-cache.org/Versions/v3/3.4/squid-%{version}.tar.xz
NoSource: 0

Source2:  squid.logrotate
Source3:  squid.sysconfig
Source4:  squid.pam
Source5:  squid.nm
Source6:  squid.service
Source7:  cache_swap.sh
Source98: perl-requires-squid.sh

# Upstream patches

# Local patches
# Applying upstream patches first makes it less likely that local patches
# will break upstream ones.
Patch201: squid-3.1.0.9-config.patch
Patch202: squid-3.1.0.9-location.patch
Patch203: squid-3.0.STABLE1-perlpath.patch
Patch204: squid-3.2.0.9-fpic.patch
Patch205: squid-3.1.9-ltdl.patch
Patch206: active-ftp.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# squid_ldap_auth and other LDAP helpers require OpenLDAP
BuildRequires: openldap-devel >= 2.4.0
# squid_pam_auth requires PAM development libs
BuildRequires: pam-devel
# SSL support requires OpenSSL
BuildRequires: openssl-devel >= 1.0.1e
# squid_kerb_aut requires Kerberos development libs
BuildRequires: krb5-devel
# squid_session_auth requires DB4
BuildRequires: libdb-devel >= 5.3.15
# ESI support requires Expat & libxml2
BuildRequires: expat-devel libxml2-devel
# TPROXY requires libcap, and also increases security somewhat
BuildRequires: libcap-devel
BuildRequires: libbind-devel >= 6.0
#ip_user helper requires
BuildRequires: glibc-headers
# 
BuildRequires: libtool libtool-ltdl-devel
# For test suite
BuildRequires: cppunit-devel

Requires: bash >= 2.0
Requires(pre): shadow-utils
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

Obsoletes: %{name}-sysvinit

%description
Squid is a high-performance proxy caching server for Web clients,
supporting FTP, gopher, and HTTP data objects. Unlike traditional
caching software, Squid handles all requests in a single,
non-blocking, I/O-driven process. Squid keeps meta data and especially
hot objects cached in RAM, caches DNS lookups, supports non-blocking
DNS lookups, and implements negative caching of failed requests.

Squid consists of a main server program squid, a Domain Name System
lookup program (dnsserver), a program for retrieving FTP data
(ftpget), and some management and client tools.

%prep
%setup -q

%patch201 -p1 -b .config
%patch202 -p1 -b .location
%patch203 -p1 -b .perlpath
%patch204 -p1 -b .fpic
%patch205 -p1 -b .ltdl
%patch206 -p1 -b .active-ftp

%build

CXXFLAGS="$RPM_OPT_FLAGS -fpie" \
CFLAGS="$RPM_OPT_FLAGS -fpie" \
LDFLAGS="$RPM_LD_FLAGS -pie -Wl,-z,relro -Wl,-z,now"

%configure \
   --exec_prefix=/usr \
   --libexecdir=%{_libdir}/squid \
   --localstatedir=/var \
   --datadir=%{_datadir}/squid \
   --sysconfdir=/etc/squid \
   --with-logdir='$(localstatedir)/log/squid' \
   --with-pidfile='$(localstatedir)/run/squid.pid' \
   --disable-dependency-tracking \
   --enable-arp-acl \
   --enable-eui \
   --enable-follow-x-forwarded-for \
   --enable-auth \
   --enable-auth-basic="DB,LDAP,MSNT,MSNT-multi-domain,NCSA,NIS,PAM,POP3,RADIUS,SASL,SMB,getpwnam" \
   --enable-auth-ntlm="smb_lm,fake" \
   --enable-auth-digest="file,LDAP,eDirectory" \
   --enable-auth-negotiate="kerberos" \
   --enable-external-acl-helpers="LDAP_group,time_quota,session,unix_group,wbinfo_group" \
   --enable-storeid-rewrite-helpers="file" \
   --enable-cache-digests \
   --enable-cachemgr-hostname=localhost \
   --enable-delay-pools \
   --enable-epoll \
   --enable-icap-client \
   --enable-ident-lookups \
   %ifnarch ppc64 ia64 x86_64 s390x
   --with-large-files \
   %endif
   --enable-linux-netfilter \
   --enable-referer-log \
   --enable-removal-policies="heap,lru" \
   --enable-snmp \
   --enable-ssl \
   --enable-ssl-crtd \
   --enable-storeio="aufs,diskd,ufs,rock" \
   --enable-diskio \
   --enable-useragent-log \
   --enable-wccpv2 \
   --enable-esi \
   --with-aio \
   --with-default-user="squid" \
   --with-filedescriptors=16384 \
   --with-dl \
   --with-openssl \
   --with-pthreads

make \
	DEFAULT_SWAP_DIR='$(localstatedir)/spool/squid' \
	%{?_smp_mflags}

%check
make check
	
%install
rm -rf %{buildroot}
make \
	DESTDIR=%{buildroot} \
	install
echo "
#
# This is /etc/httpd/conf.d/squid.conf
#

ScriptAlias /Squid/cgi-bin/cachemgr.cgi %{_libdir}/squid/cachemgr.cgi

# Only allow access from localhost by default
<Location /Squid/cgi-bin/cachemgr.cgi>
 order allow,deny
 allow from localhost.localdomain
 # Add additional allowed hosts as needed
 # allow from .example.com
</Location>" > %{buildroot}/squid.httpd.tmp


mkdir -p %{buildroot}/etc/init.d
mkdir -p %{buildroot}/etc/logrotate.d
mkdir -p %{buildroot}/etc/sysconfig
mkdir -p %{buildroot}/etc/pam.d
mkdir -p %{buildroot}/etc/httpd/conf.d/
mkdir -p %{buildroot}/etc/NetworkManager/dispatcher.d
mkdir -p %{buildroot}/%{_unitdir}
mkdir -p %{buildroot}/usr/libexec/squid
install -m 644 %{SOURCE2} %{buildroot}/etc/logrotate.d/squid
install -m 644 %{SOURCE3} %{buildroot}/etc/sysconfig/squid
install -m 644 %{SOURCE4} %{buildroot}/etc/pam.d/squid
install -m 644 %{SOURCE6} %{buildroot}/%{_unitdir}
install -m 755 %{SOURCE7} %{buildroot}/usr/libexec/squid
install -m 644 %{buildroot}/squid.httpd.tmp %{buildroot}/etc/httpd/conf.d/squid.conf.dist
install -m 644 %{SOURCE5} %{buildroot}/etc/NetworkManager/dispatcher.d/20-squid
mkdir -p %{buildroot}/var/run/squid
mkdir -p %{buildroot}/var/log/squid
mkdir -p %{buildroot}/var/spool/squid
chmod 644 contrib/url-normalizer.pl contrib/rredir.* contrib/user-agents.pl
iconv -f ISO88591 -t UTF8 ChangeLog -o ChangeLog.tmp
mv -f ChangeLog.tmp ChangeLog

# Move the MIB definition to the proper place (and name)
mkdir -p %{buildroot}/usr/share/snmp/mibs
mv %{buildroot}/usr/share/squid/mib.txt %{buildroot}/usr/share/snmp/mibs/SQUID-MIB.txt

# squid.conf.documented is documentation. We ship that in doc/
rm -f %{buildroot}/etc/squid/squid.conf.documented

# remove unpackaged files from the buildroot
rm -f %{buildroot}%{_bindir}/{RunAccel,RunCache}
rm -f %{buildroot}/squid.httpd.tmp

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING COPYRIGHT README ChangeLog QUICKSTART src/squid.conf.documented
%doc contrib/url-normalizer.pl contrib/rredir.* contrib/user-agents.pl

%{_unitdir}/squid.service
%attr(755,root,root) %dir %{_libexecdir}/squid
%attr(755,root,root) %{_libexecdir}/squid/cache_swap.sh
%attr(755,root,root) %dir %{_sysconfdir}/squid
%attr(755,root,root) %dir %{_libdir}/squid
%attr(750,squid,squid) %dir /var/log/squid
%attr(750,squid,squid) %dir /var/spool/squid
%attr(750,squid,squid) %dir /var/run/squid

%config(noreplace) %attr(644,root,root) %{_sysconfdir}/httpd/conf.d/squid.conf.dist
%config(noreplace) %attr(640,root,squid) %{_sysconfdir}/squid/squid.conf
%config(noreplace) %attr(644,root,squid) %{_sysconfdir}/squid/cachemgr.conf
%config(noreplace) %{_sysconfdir}/squid/mime.conf
%config(noreplace) %{_sysconfdir}/squid/errorpage.css
%config(noreplace) %{_sysconfdir}/sysconfig/squid
%config(noreplace) %{_sysconfdir}/squid/msntauth.conf
# These are not noreplace because they are just sample config files
%config %{_sysconfdir}/squid/msntauth.conf.default
%config %{_sysconfdir}/squid/squid.conf.default
%config %{_sysconfdir}/squid/mime.conf.default
%config %{_sysconfdir}/squid/errorpage.css.default
%config %{_sysconfdir}/squid/cachemgr.conf.default
%config(noreplace) %{_sysconfdir}/pam.d/squid
%config(noreplace) %{_sysconfdir}/logrotate.d/squid

%dir %{_datadir}/squid
%attr(-,root,root) %{_datadir}/squid/errors
%attr(755,root,root) %{_sysconfdir}/NetworkManager/dispatcher.d/20-squid
%{_datadir}/squid/icons
%{_sbindir}/squid
%{_bindir}/purge
%{_bindir}/squidclient
%{_mandir}/man8/*
%{_mandir}/man1/*
%{_libdir}/squid/*
%{_datadir}/snmp/mibs/SQUID-MIB.txt

%pre
if ! getent group squid >/dev/null 2>&1; then
  /usr/sbin/groupadd -g 23 squid
fi

if ! getent passwd squid >/dev/null 2>&1 ; then
  /usr/sbin/useradd -g 23 -u 23 -d /var/spool/squid -r -s /sbin/nologin squid >/dev/null 2>&1 || exit 1 
fi

for i in /var/log/squid /var/spool/squid ; do
        if [ -d $i ] ; then
                for adir in `find $i -maxdepth 0 \! -user squid`; do
                        chown -R squid:squid $adir
                done
        fi
done

exit 0

%post
%systemd_post squid.service

%preun
%systemd_preun squid.service

%postun
%systemd_postun_with_restart squid.service

%triggerin -- samba-common
if ! getent group wbpriv >/dev/null 2>&1 ; then
  /usr/sbin/groupadd -g 88 wbpriv >/dev/null 2>&1 || :
fi
/usr/sbin/usermod -a -G wbpriv squid >/dev/null 2>&1 || \
    chgrp squid /var/cache/samba/winbindd_privileged >/dev/null 2>&1 || :


%changelog
* Fri May 30 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.4.5-1m)
- update to 3.4.5
- Obsoletes sysvinit subpackage

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.7-1m)
- [SECURITY] CVE-2013-0189
- update to 3.2.7

* Sat Dec 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-1m)
- [SECURITY] CVE-2012-5643
- update to 3.2.4

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0.14-2m)
- rebuild against libdb-5.3.15

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0.14-1m)
- update 3.2.0.14

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0.13-2m)
- fix initscript file

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0.13-1m)
- update 3.2.0.13
- support systemd

* Sat Sep 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.15-1m)
- [SECURITY] CVE-2011-3205
- update to 3.1.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.4-5m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.4-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4 based on Fedora 13 (7:3.1.4-2)
-- we use /var/cache/squid as DEFAULT_SWAP_DIR

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.24-2m)
- rebuild against openssl-1.0.0

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.24-1m)
- [SECURITY] CVE-2010-0308 CVE-2010-0639
- update to 3.0.STABLE24

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.20-1m)
- update to 3.0.STABLE20
- install nm script
- use __perl_requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.18-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.18-2m)
- fix a bug that daemon doesn't start
- delete unused patch

* Wed Aug  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.18-1m)
- [SECURITY] CVE-2009-2621 CVE-2009-2622
- update to 3.0.STABLE18

* Wed Jul 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.17-1m)
- update to 3.0.STABLE17

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.16-3m)
- rebuild against libbind-6.0

* Tue Jun 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.16-2m)
- fix build on x86_64 by upstream patch
- http://www.mail-archive.com/squid-users@squid-cache.org/msg64896.html
- fix "File listed twice"

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.16-1m)
- update to 3.0.STABLE16

* Sun May 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.15-2m)
- re-update to 3.0.STABLE15

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.15-1m)
- update to 3.0.STABLE15
- add autoreconf
-- need libtool-2.2.x

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.14-1m)
- update to 3.0.STABLE14
- import the following Rawhide changes
-- * Tue Apr 28 2009 Jiri Skala <jskala@redhat.com> - 3.0.STABLE14-3
-- - fixed ambiguous condition in the init script (exit 4)
-- 
-- * Thu Aug 14 2008 Jiri Skala <jskala@redhat.com> - 7:3.0.STABLE7-2
-- - used ncsa_auth.8 from man-pages. there will be this file removed due to conflict

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.13-2m)
- rebuild against openssl-0.9.8k

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.13-1m)
- [SECURITY] CVE-2009-0478
- [SECURITY] http://www.squid-cache.org/Advisories/SQUID-2009_1.txt
- update to 3.0.STABLE13
- build with -fno-strict-aliasing and -Wno-uninitialized when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.11-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.11-1m)
- update to 3.0.11
-- disable coss support, not supported in 3.0
-- update Patch201,203,400 for fuzz=0

* Fri Jul 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.8-1m)
- sync with Fedora devel (3.0.STABLE7-1)
- update to 3.0.STABLE8

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.18-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.18-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.17-1m)
- update 2.6.STABLE18

* Thu Dec  6 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.17-1m)
- [SECURITY] CVE-2007-6239
- update 2.6.STABLE17

* Sun Jun  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.13-1m)
- [SECURITY] CVE-2007-1560 CVE-2007-0247 CVE-2007-0248
- update 2.6.STABLE13
- sync FC-devel (squid-2.6.STABLE12-1)

* Sat May 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5.14-1m)
- update 2.5.STABLE14

* Mon Mar 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5.13-1m)
- update 2.5.STABLE13

* Thu Jan 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.12-1m)
- update 2.5.STABLE12
- rebuild against openldap-2.3.11

* Wed May 18 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.10-1m)
- update 2.5.STABLE10

* Mon Feb 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.9-1m)
- update 2.5.STABLE9

* Tue Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.8-1m)
- update 2.5.STABLE8

* Wed Jan 19 2005 TAKAHASHI Tamotsu <tamo>
- (2.5.7-4m)
- fix multiple security problems:
 squid-2.5.STABLE7-fakeauth_auth.patch
 squid-2.5.STABLE7-gopher_html_parsing.patch
 squid-2.5.STABLE7-wccp_denial_of_service.patch
 squid-2.5.STABLE7-ldap_spaces.patch

* Mon Dec 27 2004 TAKAHASHI Tamotsu <tamo>
- (2.5.7-3m)
- fix "Confusing results on empty acl declarations"
  http://www.squid-cache.org/Versions/v2/2.5/bugs/#squid-2.5.STABLE7-empty_acls

* Fri Dec 10 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.5.7-2m)
- [SECURITY] Squid Malformed Host Name Error Message Information Leakage
  http://secunia.com/advisories/13408/

* Wed Nov 17 2004 TAKAHASHI Tamotsu <tamo>
- (2.5.7-1m)
- [SECURITY] update to 2.5-STABLE7
http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0918
- remove patch2 (squid-2.5.STABLE6-ntlm_fetch_string.patch)

* Mon Sep  6 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (2.5.6-1m)
- update to 2.5-STABLE6
- remove libntlmssp.c.patch (merged to original distribution)
- [SECURITY FIX] apply 'squid-2.5.STABLE6-ntlm_fetch_string.patch'

* Thu Jun 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.5-2m)
- [SECURITY FIX] apply 'libntlmssp.c.patch'

* Mon Mar 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.5.5-1m)
- update to 2.5-STABLE5
- this release includes security fix

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.4-4m)
- revised spec for rpm 4.2.

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.4-3m)
- accept gdbm-1.8.0 or newer

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.4-2m)
- rebuild against gdbm-1.8.0

* Thu Oct 30 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5-STABLE4

* Thu Oct 30 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.3-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft).

* Wed Jun 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.5-3.1m)
  update to 2.5-STABLE3

* Wed Mar 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.5-2.1m)
  update to 2.5-STABLE2

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.5-1m)
  update to 2.5-STABLE1

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4-29m)
- rebuild against for gdbm

* Tue Nov 26 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4-28m)
- enable nullfs

* Mon Jul  8 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.4-27m)
- 2.4.STABLE7
- major security fixes

* Wed Mar 27 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.4-24k)
- 2.4.STABLE6

* Wed Feb 27 2002 Masahiro Takahata <takahata@kondara.org>
- (2.4-22k)
- filename squid-2.4-perl-path.patch typo

* Wed Feb 27 2002 Masahiro Takahata <takahata@kondara.org>
- (2.4-20k)
- cleanup specfile
- add sysconfig/squid

* Sat Feb 23 2002 WATABE Toyokazu <toy2@kondara.org>
- (2.4-18k)
- update to 2.4-STABLE4 to fix security holes.
  http://www.squid-cache.org/Advisories/SQUID-2002_1.txt
- delete old patches.

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (2.4-16k)
- No docs could be excutable :-p

* Thu Nov 22 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4-14k)
- fix following patches (see http://www.squid-cache.org/Versions/v2/2.4/bugs/)
  squid-2.4.STABLE2-CONNECT_miss_access_core.patch
  squid-2.4.STABLE2-swap_meta.patch
  squid-2.4.STABLE2-ldap_auth_password_spaces.patch
  squid-2.4.STABLE2-ftp_create_directory.patch
  squid-2.4.STABLE2-statHistDump_prototype.patch
  squid-2.4.STABLE2-snmpwalk_coredump.patch
  squid-2.4.STABLE2-aufs_fd_leak.patch
- add "--enable-linux-netfilter"
- convert gif to png

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.4-12k)
- nigittenu

* Tue Sep 11 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4-10k)
- update to squid 2.4 STABLE2
- removed all official STABLE1 patches
- update FAQ.sgml from http://www.squid-cache.org/Doc/FAQ/FAQ.sgml

* Mon Apr 23 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4-8k)
- merge official patches for squid-2.4.STABLE1, see
-  http://www.squid-cache.org/Versions/v2/2.4/bugs/
-  squid-2.4.stable1-diskd_fixed_path.patch
-  squid-2.4.stable1-htcp_assertion_fix.patch
- change cache_swap directory /var/spool/squid to /var/cache/squid

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.4-6k)
- /etc/rc.d/init.d -> /etc/init.d

* Sun Apr  1 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4-5k)
- merge official patches for squid-2.4.STABLE1, see
-  http://www.squid-cache.org/Versions/v2/2.4/bugs/
-  squid-2.4.stable1-wrong_sign_on_timestamp_check.patch
-  squid-2.4.stable1-high_cpu_with_peers.patch
-  squid-2.4.stable1-force_valid_blksize.patch
-  squid-2.4.stable1-kill_parent_on_child_sigkill.patch

* Sun Mar 25 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4-3k)
- update to 2.4.STABLE1
- release-no to %{rel} and %{subrel}
- rework patches against squid-2.4.STABLE1
  (squid-2.1-make.patch to squid-2.4-make.patch,
   squid-2.3-config.patch to squid-2.4-config.patch,
   squid-2.3.stable4-perl-path-kondara.patch to squid-2.4-...)
- removed official patches for 2.3.STABLE4
- removed configure option "--enable-heap-replacement" (2.4 obsoletes)
- update FAQ.sgml 1.1.1.1 to 1.9
- enable "diskd"

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3-7k)
- rebuild against rpm-3.0.5-39k

* Wed Nov 29 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}

* Tue Aug 15 2000 Kenji Yamaguchi <yamk@sophiaworks.com>
- (2.3.stable4-1k)
- update to 2.3.stable4
- added 2.3.stable4 patches.
- deleted 2.3.stable1 patches.
- update FAQ.sgml from www.squid-cache.org.
- Kondarize. (SPEC)
- fixed scripts "#! /usr/local/bin/perl" to "#! /usr/bin/perl"
  [squid-2.3.stable4-perl-path-kondara.patch]

* Mon Feb 14 2000 Bill Nottingham <notting@redhat.com>
- Yet More Bugfix Patches

* Tue Feb  8 2000 Bill Nottingham <notting@redhat.com>
- add more bugfix patches
- --enable-heap-replacement

* Mon Jan 31 2000 Cristian Gafton <gafton@redhat.com>
- rebuild to fix dependencies

* Fri Jan 28 2000 Bill Nottingham <notting@redhat.com>
- grab some bugfix patches

* Mon Jan 10 2000 Bill Nottingham <notting@redhat.com>
- 2.3.STABLE1 (whee, another serial number)

* Tue Dec 21 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix compliance with ftp RFCs
  (http://www.wu-ftpd.org/broken-clients.html)
- Work around a bug in some versions of autoconf
- BuildPrereq sgml-tools - we're using sgml2html

* Mon Oct 18 1999 Bill Nottingham <notting@redhat.com>
- add a couple of bugfix patches

* Wed Oct 13 1999 Bill Nottingham <notting@redhat.com>
- update to 2.2.STABLE5.
- update FAQ, fix URLs.

* Sat Sep 11 1999 Cristian Gafton <gafton@redhat.com>
- transform restart in reload and add restart to the init script

* Tue Aug 31 1999 Bill Nottingham <notting@redhat.com>
- add squid user as user 23.

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging
- fix conflict between logrotate & squid -k (#4562)

* Wed Jul 28 1999 Bill Nottingham <notting@redhat.com>
- put cachemgr.cgi back in /usr/lib/squid

* Wed Jul 14 1999 Bill Nottingham <notting@redhat.com>
- add webdav bugfix patch (#4027)

* Mon Jul 12 1999 Bill Nottingham <notting@redhat.com>
- fix path to config in squid.init (confuses linuxconf)

* Wed Jul  7 1999 Bill Nottingham <notting@redhat.com>
- 2.2.STABLE4

* Wed Jun 9 1999 Dale Lovelace <dale@redhat.com>
- logrotate changes
- errors from find when /var/spool/squid or
- /var/log/squid didn't exist

* Thu May 20 1999 Bill Nottingham <notting@redhat.com>
- 2.2.STABLE3

* Thu Apr 22 1999 Bill Nottingham <notting@redhat.com>
- update to 2.2.STABLE.2

* Sun Apr 18 1999 Bill Nottingham <notting@redhat.com>
- update to 2.2.STABLE1

* Thu Apr 15 1999 Bill Nottingham <notting@redhat.com>
- don't need to run groupdel on remove
- fix useradd

* Mon Apr 12 1999 Bill Nottingham <notting@redhat.com>
- fix effective_user (bug #2124)

* Mon Apr  5 1999 Bill Nottingham <notting@redhat.com>
- strip binaries

* Thu Apr  1 1999 Bill Nottingham <notting@redhat.com>
- duh. adduser does require a user name.
- add a serial number

* Tue Mar 30 1999 Bill Nottingham <notting@redhat.com>
- add an adduser in %pre, too

* Thu Mar 25 1999 Bill Nottingham <notting@redhat.com>
- oog. chkconfig must be in %preun, not %postun

* Wed Mar 24 1999 Bill Nottingham <notting@redhat.com>
- switch to using group squid
- turn off icmp (insecure)
- update to 2.2.DEVEL3
- build FAQ docs from source

* Tue Mar 23 1999 Bill Nottingham <notting@redhat.com>
- logrotate changes

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Wed Feb 10 1999 Bill Nottingham <notting@redhat.com>
- update to 2.2.PRE2

* Wed Dec 30 1998 Bill Nottingham <notting@redhat.com>
- cache & log dirs shouldn't be world readable
- remove preun script (leave logs & cache @ uninstall)

* Tue Dec 29 1998 Bill Nottingham <notting@redhat.com>
- fix initscript to get cache_dir correct

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- update to 2.1.PATCH2
- merge in some changes from RHCN version

* Sat Oct 10 1998 Cristian Gafton <gafton@redhat.com>
- strip binaries
- version 1.1.22

* Sun May 10 1998 Cristian Gafton <gafton@redhat.com>
- don't make packages conflict with each other...

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- added a proxy auth patch from Alex deVries <adevries@engsoc.carleton.ca>
- fixed initscripts

* Thu Apr 09 1998 Cristian Gafton <gafton@redhat.com>
- rebuilt for Manhattan

* Fri Mar 20 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.1.21/1.NOVM.21

* Mon Mar 02 1998 Cristian Gafton <gafton@redhat.com>
- updated the init script to use reconfigure option to restart squid instead
  of shutdown/restart (both safer and quicker)

* Sat Feb 07 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.1.20
- added the NOVM package and tryied to reduce the mess in the spec file

* Wed Jan 7 1998 Cristian Gafton <gafton@redhat.com>
- first build against glibc
- patched out the use of setresuid(), which is available only on kernels
  2.1.44 and later

