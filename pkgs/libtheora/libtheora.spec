%global momorel 5

Summary: Theora Video Compression Codec
Name: libtheora
Version: 1.1.1
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://www.theora.org/
Source0: http://downloads.xiph.org/releases/theora/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel
BuildRequires: doxygen
BuildRequires: libogg-devel >= 1.1.3
BuildRequires: libpng-devel
BuildRequires: libvorbis-devel >= 1.2.0
BuildRequires: pkgconfig
BuildRequires: tetex-latex
BuildRequires: transfig

%description
Theora is Xiph.Org's first publicly released video codec, intended
for use within the Ogg's project's Ogg multimedia streaming system.
Theora is derived directly from On2's VP3 codec; Currently the two are
nearly identical, varying only in encapsulating decoder tables in the
bitstream headers, but Theora will make use of this extra freedom
in the future to improve over what is possible with VP3.

%package devel
Summary: Development tools for Theora applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libogg-devel >= 1.1
Requires: pkgconfig

%description devel
The libtheora-devel package contains the header files and documentation 
needed to develop applications with libtheora.

%package -n theora-tools
Summary: Command line tools for Theora videos
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description -n theora-tools
The theora-tools package contains simple command line tools for use 
with theora bitstreams.

%prep
%setup -q

%build
%configure --enable-shared=yes --enable-static=yes
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

mkdir -p %{buildroot}%{_bindir}
install -m 755 examples/.libs/dump_video %{buildroot}%{_bindir}/theora_dump_video
install -m 755 examples/.libs/encoder_example %{buildroot}%{_bindir}/theora_encode
install -m 755 examples/.libs/player_example %{buildroot}%{_bindir}/theora_player
install -m 755 examples/.libs/png2theora %{buildroot}%{_bindir}/png2theora

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/libtheora*.la

# move doc
mv %{buildroot}%{_docdir}/%{name}-%{version} %{buildroot}%{_docdir}/%{name}-%{version}-devel

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGES COPYING LICENSE README
%{_libdir}/libtheora*.so.*

%files devel
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}-devel
%{_includedir}/theora
%{_libdir}/pkgconfig/theora.pc
%{_libdir}/pkgconfig/theoradec.pc
%{_libdir}/pkgconfig/theoraenc.pc
%{_libdir}/libtheora*.a
%{_libdir}/libtheora*.so

%files -n theora-tools
%defattr(-,root,root,-)
%{_bindir}/png2theora
%{_bindir}/theora_dump_video
%{_bindir}/theora_encode
%{_bindir}/theora_player

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1

* Sat Sep 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- [SECURITY] CVE-2009-3389
- version 1.1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Thu Nov  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0

* Fri Oct 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.9.1m)
- update to version 1.0 RC2
- remove all patches and additional sources

* Mon Jun 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.8.1m)
- update to version 1.0 beta3
- import textreloc.patch from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.7.4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.7.3m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.7.2m)
- rebuild against libvorbis-1.2.0-1m

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.7.1m)
- update to 1.0alpha7

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0-0.5.1m)
- update to 1.0alpha5

* Mon Mar 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-0.4.1m)
- 1.0alpha4 import from FC

* Wed Mar 02 2005 John (J5) Palmieri <johnp@redhar.com> - 1.0alpha4-2
- rebuild with gcc 4.0

* Mon Jan 03 2005 Colin Walters <walters@redhat.com> - 1.0alpha4-1
- New upstream version 1.0alpha4 
- Remove upstreamed patch libtheora-1.0alpha3-include.patch 
- Use Theora_I_spec.pdf for spec
- Add in .pc file (yay! another library sees the light)

* Tue Oct 05 2004 Colin Walters <walters@redhat.com> - 1.0alpha3-5
- Add BuildRequires on libvorbis-devel (134664)

* Sat Jul 17 2004 Warren Togami <wtogami@redhat.com> - 1.0alpha3-4
- Add Epoch dependencies for future Epoch increment safety measure

* Thu Jul 15 2004 Colin Walters <walters@redhat.com> - 1.0alpha3-3
- Apply patch to fix include path, thanks to Thomas Vander Stichele

* Tue Jul 13 2004 Jeremy Katz <katzj@redhat.com> - 1.0alpha3-2
- rebuild

* Mon Jun 21 2004 Jeremy Katz <katzj@redhat.com> - 1.0alpha3-1
- Initial build
