
%global momorel 1
Name:           gnome-screenshot
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        A screenshot utility for GNOME

Group:          Applications/System
License:        GPLv2+
URL:            http://www.gnome.org
Source:  	http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires: gtk3-devel
BuildRequires: libcanberra-devel
BuildRequires: intltool
BuildRequires: desktop-file-utils

Obsoletes: gnome-utils < 3.3
Obsoletes: gnome-utils-devel < 3.3
Obsoletes: gnome-utils-libs < 3.3

%description
gnome-screenshot lets you take pictures of your screen.

%prep
%setup -q


%build
%configure
%make 


%install
make install DESTDIR=%{buildroot}

# the desktop file contains Canonical 'enhancements' which don't validate :-(
# desktop-file-validate %{buildroot}%%{_datadir}/applications/gnome-screenshot.desktop

%find_lang %{name}

%postun
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi


%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f %{name}.lang
%doc COPYING
%{_bindir}/gnome-screenshot
%{_datadir}/GConf/gsettings/gnome-screenshot.convert
%{_datadir}/applications/gnome-screenshot.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-screenshot.gschema.xml
%doc %{_mandir}/man1/gnome-screenshot.1.*

%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Thu Aug 02 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- import from fedora

