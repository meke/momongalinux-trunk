%global momorel 10

Summary: .NET language binding for webkit
Name: webkit-sharp
Version: 0.3
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: "BSDL like"
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-x86_64.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk-sharp2-devel >= 2.12.9
BuildRequires: webkitgtk-devel >= 1.1.15.4
BuildRequires: mono-devel >= 2.8
Requires: mono-core
Requires: gtk2
Requires: gtk-sharp2

%description
webkit for C#

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%patch0 -p1 -b .x86_64

%build
%configure --program-prefix=""
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_prefix}/lib/mono/gac/%{name}
%dir %{_prefix}/lib/mono/webkit-sharp
%{_prefix}/lib/mono/webkit-sharp/webkit-sharp.dll

%files devel
%defattr(-,root,root)
%{_prefix}/lib/monodoc/sources/*
%{_libdir}/pkgconfig/webkit-sharp-1.0.pc

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-10m)
- change Source0 URI

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-9m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-7m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-6m)
- rebuild against mono-2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-5m)
- full rebuild for mo7 release

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-4m)
- add %%dir

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-3m)
- use BuildRequires

* Thu Dec 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-2m)
- apply x86_64.patch for mono-tools-2.6.1

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-1m)
- initial build
