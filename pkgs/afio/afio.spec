%global momorel 10

Summary: cpio compatible archiver
Name: afio

Version: 2.5
Release: %{momorel}m%{?dist}
Source: http://www.ibiblio.org/pub/linux/system/backup/afio-%{version}.tgz
Patch0: afio.makefile.patch
NoSource: 0
License: GPL and LGPL and Artistic
URL: http://freshmeat.net/projects/afio/
Group: Applications/Archiving
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Afio makes cpio-format archives.  It deals somewhat gracefully with
input data corruption.  Supports multi-volume archives during
interactive operation.  Afio can make compressed archives that are
much safer than compressed tar or cpio archives.  Afio is best used as
an `archive engine' in a backup script.

%prep
%setup -q
%patch0 -p0 -b .makefile

mv Makefile Makefile.opt
sed "s/^\(CFLAGS1 = \).*$/\1%{optflags}/" Makefile.opt > Makefile

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
make DESTDIR=%{buildroot} bindir=%{_bindir} mandir=%{_mandir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/bin/afio
%{_mandir}/man1/afio.1*
%doc HISTORY README SCRIPTS perl.artistic.license
%doc script1 script2 script3 script4 script5

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-5m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPL and LGPL and Artistic

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-4m)
- rebuild against gcc43

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-3m)
- fix %%changelog section

* Tue Oct 12 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.5-2m)
- modify afio.makefile.patch not to remove ${LARGEFILEFLAGS} from ${CFLAGS1}
  (http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=78)

* Fri May 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5-1m)
- verup

* Fri Nov 14 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (2.4.7-3m)
- momonga release up

* Sun Oct 14 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.4.7-2k)

* Tue Nov  7 2000 Yasuhide OOMORI <dasen@icntv.ne.jp>
- (2.4.6-5k)
- use $(DESTDIR) in Makefile
- use %{optflags}
- create build root in %%install section

* Mon Jul 10 2000 AYUHANA Tomonori <l@kondara.org>
- (2.4.6-2k)
- add -b at %patch
- add * at man pages

* Sat Jun 30 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- First release for Kondara.
