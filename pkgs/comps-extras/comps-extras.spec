%global momorel 5

Summary: Images for components included in Momonga
Name: comps-extras
Version: 20
Release: %{momorel}m%{?dist}
URL: http://git.fedorahosted.org/git/?p=comps-extras.git;a=summary
Source0: http://fedorahosted.org/releases/c/o/comps-extras/%{name}-%{version}.tar.gz
NoSource: 0
# while GPL isn't normal for images, it is the case here
# No version specified.
# KDE logo is LGPL
# LXDE logo is GPLv2+
# Haskell logo is a variation on MIT/X11
# Sugar and Ruby logos are CC-BY-SA
# See COPYING for more details
License: GPL+ and LGPL+ and GPLv2+ and "CC-BY-SA" and MIT
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
This package contains images for the components included in Momonga.

%prep
%setup -q

%build
# nothing to do

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc comps.dtd comps-cleanup.xsl
%dir %{_datadir}/pixmaps/comps
%{_datadir}/pixmaps/comps/*

%changelog
* Fri Mar 23 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (20-5m)
- delete patch0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20-1m)
- update to 20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (16-1m)
- update to 16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (13-2m)
- rebuild against rpm-4.6

* Tue Jul 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (13-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (12-2m)
- rebuild against gcc43

* Mon May 28 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (12-1m)
- update to 12

* Thu Apr 27 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.1-1m)
- update to 11.1
- delete %%{_datadir}/comps-extras from %%files

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (10.1-2m)
- build against python-2.4.2

* Mon Feb 14 2005 Toru Hoshina <t@momonga-linux.org>
- (10.1-1m)
  update to 10.1

* Tue Sep 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (9.92-1m)
  update to 9.92

* Sat Jul 24 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (9.0.3-3m)
- add icon images (ruby.png,window-manager.png,xfce4.png)

* Fri Apr 16 2004 Toru Hoshina <t@momonga-linux.org>
- (9.0.3-2m)
- import from FC1.

* Tue May 27 2003 Jeremy Katz <katzj@redhat.com> 9.0.3-1
- getfullcomps.py can go away now that anaconda does dep resolution in 
  realtime

* Fri Apr 11 2003 Jeremy Katz <katzj@redhat.com> 9.0.2-1
- update getfullcomps.py to not prefer devel packages

* Tue Apr  8 2003 Tim Powers <timp@redhat.com> 9.0.1-2
- made getfullcomps.py importable

* Tue Mar  4 2003 Jeremy Katz <katzj@redhat.com> 9.0.1-1
- add /usr/share/comps-extras/whichcd.py to find out which cd a given 
  package is on (#85343)

* Tue Dec 17 2002 Jeremy Katz <katzj@redhat.com>
- improve getfullcomps.py handling of multiple provides

* Wed Sep 04 2002 Jeremy Katz <katzj@redhat.com>
- update images again

* Wed Sep 04 2002 Michael Fulbright <msf@redhat.com>
- update images

* Thu Aug 29 2002 Jeremy Katz <katzj@redhat.com>
- update images

* Tue Jul 23 2002 Jeremy Katz <katzj@redhat.com>
- Initial build.


