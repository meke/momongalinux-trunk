%global momorel 1

%bcond_with	mpeg
%bcond_without	flac

%global plugindir	%{_libdir}/%{name}

Summary: Simple library for keyword extraction
Name: libextractor
Version: 1.1
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://gnunet.org/libextractor/  
Group: System Environment/Libraries
Source0: ftp://ftp.gnu.org/pub/gnu/libextractor/libextractor-%{version}.tar.gz
NoSource: 0
Source10: README.fedora
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: gtk2-devel >= 2.0.6 glib2-devel >= 2.0.0
BuildRequires: exiv2-devel >= 0.23 libgsf-devel
BuildRequires: mpeg2dec-devel libvorbis-devel
BuildRequires: libtool-ltdl-devel >= 2.2.6
BuildRequires: gettext
BuildRequires: grep
BuildRequires: bzip2-devel zlib-devel
BuildRequires: qt-devel >= 4.8.3
Requires(post):	info
Requires(preun): info

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig

%package plugins
Summary:	Plugins for libextractor
Group:		System Environment/Libraries
BuildArch:	noarch
Obsoletes:      %{name}-plugins-pdf < %{version}
Obsoletes:      %{name}-plugins-thumbnailqt < %{version}
Requires:	%{name} = %{version}-%{release}
Requires:	%name-plugins-base
Requires:	%name-plugins-exiv2
Requires:	%name-plugins-ogg
Requires:	%name-plugins-ole2
Requires:	%name-plugins-thumbnailgtk
#Requires:	%name-plugins-rpm
Requires:	%name-plugins-tiff
Requires:	%name-plugins-gif
Requires:	%name-plugins-mime
%{?with_flac:Requires:	%name-plugins-flac}
%{?with_mpeg:Requires:	%name-plugins-mpeg}

%global pluginpkg(B:R:P:u)	\
%package plugins-%1	\
Summary:	The '%1' libextractor plugin\
Group:		System Environment/Libraries		\
Provides:	plugin(%{name}) = %1 %%{-P*}		\
%%{-u:Requires(post):	chkconfig}                      \
%%{-u:Requires(preun):	chkconfig}                      \
%%{-B:BuildRequires:	%%{-B*}}			\
Requires:	%{name} = %{version}-%{release} %%{-R*}	\
	\
%description plugins-%1	\
libextractor is a simple library for keyword extraction.  libextractor\
does not support all formats but supports a simple plugging mechanism\
such that you can quickly add extractors for additional formats, even\
without recompiling libextractor.\
\
This package ships the '%1' plugin.\
\
%files plugins-%1			\
%defattr(-,root,root,-)			\
%{plugindir}/libextractor_%1.so*		\
%nil

%package plugins-base
Summary:	Base plugins for libextractor
Group:		System Environment/Libraries
Requires:	%{name} = %{version}-%{release}

%{?with_mpeg:%pluginpkg mpeg -B mpeg2dec-devel}
%{?with_flac:%pluginpkg flac -B flac-devel}
%pluginpkg exiv2 -B exiv2-devel
%pluginpkg ogg -B libvorbis-devel
%pluginpkg ole2 -B libgsf-devel,glib2-devel
#pluginpkg rpm  -B rpm-devel
%pluginpkg tiff -B libtiff-devel
%pluginpkg gif  -B giflib-devel
%pluginpkg mime -B file-devel
%pluginpkg thumbnailgtk -B gtk2-devel,file-devel


%description
libextractor is a simple library for keyword extraction.  libextractor
does not support all formats but supports a simple plugging mechanism
such that you can quickly add extractors for additional formats, even
without recompiling libextractor.  libextractor typically ships with a
dozen helper-libraries that can be used to obtain keywords from common
file-types.

libextractor is a part of the GNU project (http://www.gnu.org/).


%description plugins
libextractor is a simple library for keyword extraction.  libextractor
does not support all formats but supports a simple plugging mechanism
such that you can quickly add extractors for additional formats, even
without recompiling libextractor.

This is a metapackage which requires all supported plugins for
libextractor.

%description plugins-base
libextractor is a simple library for keyword extraction.  libextractor
does not support all formats but supports a simple plugging mechanism
such that you can quickly add extractors for additional formats, even
without recompiling libextractor.

This package contains all plugins for libextractor which do not
introduce additional dependencies.


%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

#install -pm644 %{SOURCE10} .
rm -f README.debian

sed -i 's!\(-L\(/usr\|\$with_qt\)/lib\|-I/usr/include\) !!g' configure

%build
%{!?with_mpeg:export ac_cv_lib_mpeg2_mpeg2_init=no}
export ac_cv_lib_rpm_rpmReadPackageFile=no

%configure --disable-static	\
	--disable-rpath		\
	--disable-xpdf		\
	--with-qt=/usr

# build with --as-needed and disable rpath
sed -i \
	-e 's! -shared ! -Wl,--as-needed\0!g' 					\
	-e '/sys_lib_dlsearch_path_spec=\"\/lib \/usr\/lib /s!\"\/lib \/usr\/lib !/\"/%{_lib} /usr/%{_lib} !g'	\
	libtool

# not SMP safe
make # %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

rm -f $RPM_BUILD_ROOT%{plugindir}/libextractor_thumbnail.so

echo '%defattr(-,root,root,-)' > filelists.base

for i in $RPM_BUILD_ROOT%{plugindir}/*.so; do
	readelf -a "$i" | \
	sed '/(NEEDED)/s!.*\[\(.*\)\].*!\1!p;d' | {
		target=base
		fname=${i##$RPM_BUILD_ROOT}
		while read lib; do
			lib=${lib%%.so*}
			case $lib in
				(libz|libdl)				;;
				(libextractor|libextractor_common)	;;
				(libc|libm|libpthread)	;;
				(*)
					target=other
					echo "$fname -> $lib"
					;;
			esac
		done

		case $target in
			(base)	echo "$fname" >> filelists.base;;
		esac
	}
done

ln -s dummy $RPM_BUILD_ROOT%{plugindir}/libextractor-thumbnail.so
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

mv $RPM_BUILD_ROOT%{_bindir}/{,libextractor-}extract
mv $RPM_BUILD_ROOT%{_mandir}/man1/{,libextractor-}extract.1

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
/sbin/install-info --info-dir %{_infodir} %{_infodir}/extractor.info || :

%preun
test $1 != 0 || /sbin/install-info --info-dir %{_infodir} --delete %{_infodir}/extractor.info || :

%postun -p /sbin/ldconfig


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README* TODO
%{_bindir}/*
%{_libdir}/lib*.so.*
%dir %{_libdir}/%{name}
%exclude %{_libdir}/%{name}/*
# move to plugins-base
#
%{_mandir}/man1/*
%{_infodir}/*
#%%dir %{plugindir}
# this is same as %%dir %%{_libdir}/%%{name}

%files plugins
#dummy

%files plugins-base -f filelists.base

%files devel
%defattr(-, root, root,-)
%{_includedir}/*
%{_libdir}/lib*.so
#%%{_libdir}/lib*.a
#%%{_libdir}/%{name}/*.a
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man3/*

%changelog
* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1m)
- update 1.1

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- rebuild against poppler-0.20.5 (remove BR: poppler-devel)
- set Obsoletes pdf and thumbnailqt plugins
- new plugins has come (sync Fedora)

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-5m)
- rebuild against exiv2-0.23

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-4m)
- rebuild against poppler-0.20.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-3m)
- rebuild for glib 2.33.2

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-2m)
- rebuild against poppler-0.18.2

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.3-1m)
- update 0.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.22-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.22-8m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.22-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.22-6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.22-5m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.22-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.22-3m)
- modify spec to enable build against bash-4.0

* Mon Jun  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.22-2m)
- modify spec to avoid conflicting

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.22-1m)
- update to 0.5.22 based on Fedora 11 (0.5.22-1)

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.20-4m)
- fix install-info

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.20-3m)
- rebuild against libtool-ltdl-2.2.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.20-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.20-1m)
- update to 0.5.20b (sync Fedora)
- fix %%files for avoid conflicts

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.18-6m)
- rebuild against gcc43

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.18-5m)
- libtool-1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.18-4m)
- rebuild against libtool-2.2

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.18-3m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Sep  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.18-2m)
- delete info dir

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.18-1m)
- update to 0.5.18a
- rebuild against libvorbis-1.2.0-1m

* Tue Aug  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.16-2m)
- [SECURITY] CVE-2007-3387
- add patch0 to fix CVE-2007-3387

* Fri Dec 15 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5.16-1m)
- initial packaging for Momonga
