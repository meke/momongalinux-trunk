%global momorel 5

Summary: Library for dealing with ITSS/CHM format files
Name: chmlib
Version: 0.40
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.jedrea.com/chmlib/
Group: System Environment/Libraries
Source0: http://www.jedrea.com/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-chm_lib_c-ppc-patch.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
CHMLIB is a library for dealing with ITSS/CHM format files. Right now, it is 
a very simple library, but sufficient for dealing with all of the .chm files 
I've come across. Due to the fairly well-designed indexing built into this 
particular file format, even a small library is able to gain reasonably good 
performance indexing into ITSS archives.

%package devel
Summary: Header files and static libraries from CHMLIB
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on CHMLIB.

%prep
%setup -q
%patch0 -p0 -b .powerpc

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall pkgdocdir=`pwd`/installed-docs

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/libchm.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/chm_lib.h
%{_includedir}/lzx.h
%{_libdir}/libchm.a
%{_libdir}/libchm.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.40-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.40-1m)
- version 0.40

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.39-5m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.39-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.39-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-2m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-1m)
- initial package for kchmviewer
- import chmlib-chm_lib_c-ppc-patch.diff from Fedora
