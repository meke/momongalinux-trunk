%global momorel 7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define packagename prioritized_methods

Name:           python-prioritized-methods
Version:        0.2.1
Release:        %{momorel}m%{?dist}
Summary:        An extension to PEAK-Rules to prioritize methods in order

Group:          Development/Languages
License:        MIT
URL:            http://pypi.python.org/pypi/prioritized_methods
Source0:        http://pypi.python.org/packages/source/p/%{packagename}/%{packagename}-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel

Requires:       python-peak-rules


%description
This module provides four decorators: `prioritized_when`, `prioritized_around`,
`prioritized_before`, and `prioritized_after`.  These behave like their
`peak.rules` counterparts except that they accept an optional `prio`
argument which can be used to provide a comparable object (usually an integer)
that will be used to disambiguate situations in which more than rule applies to
the given arguments and no rule is more specific than another. That is,
situations in which an `peak.rules.AmbiguousMethods` would have been raised.

This is useful for libraries which want to be extensible via generic functions
but want their users to easily override a method without figuring out how to
write a more specific rule or when it is not feasible.


%prep
%setup -q -n %{packagename}-%{version}

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-1m)
- import from Rawhide

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.2.1-3
- Rebuild for Python 2.6

* Wed Oct 15 2008 Luke Macken <lmacken@redhat.com> - 0.2.1-2
- Fix our python-peak-rules dependency

* Tue Sep 16 2008 Luke Macken <lmacken@redhat.com> - 0.2.1-1
- Initial package for Fedora
