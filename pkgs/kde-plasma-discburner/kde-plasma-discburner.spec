%global momorel 1
%global kdever 1.10.1
%global qtver 4.8.4
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global src_name discburner
%global src_ver a13
%global content_id 157336
%global prever 3

Name:           kde-plasma-discburner
Version:        0.0
Release:        %{?prever:0.%{prever}.}%{momorel}m%{?dist}
Summary:        DiscBurner Widget
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://kde-apps.org/content/show.php/discburner?content=157336
Source0:        http://kde-apps.org/CONTENT/content-files/%{content_id}-%{src_name}_%{src_ver}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  mpg321
BuildRequires:  lame-devel
BuildRequires:  flac-devel
BuildRequires:  taglib-devel
BuildRequires:  cdrdao
BuildRequires:  wodim

%description
it burns flipn discs. isos, audio, data, etc. first alpha. there is a lot left to do. help??

this is just a frontend to various burning/conversion/encoding softwares.
(lame, wodim, sox, cdda2wav/mp3/ogg, icedax, cdrdao, ffmpeg, etc, etc)

%prep
%setup -q -n %{src_name}_%{src_ver}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README TODO
%{_kde4_libdir}/kde4/plasma_applet_discburner.so
%{_kde4_datadir}/kde4/services/plasma-applet-discburner.desktop
%{_kde4_appsdir}/desktoptheme/default/discburner

%changelog
* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.3.1m)
- update to 13a

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-0.2.7m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.2.6m)
- fix build error

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-0.2.5m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.2.4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0-0.2.3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.2.2m)
- rebuild against qt-4.6.3-1m

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.2.1m)
- update to discburner-a12

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.1.1m)
- initial build for Momonga Linux
