%global momorel 16

Name:           perl-mime-construct
Version:        1.11
Release:        %{momorel}m%{?dist}
Summary:        Construct/send MIME messages from the command line 

Group:          Development/Libraries
License:        GPLv2+
URL:            http://search.cpan.org/~rosch/mime-construct-%{version}
Source0:        http://search.cpan.org/CPAN/authors/id/R/RO/ROSCH/mime-construct-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MIME-Types
BuildRequires:  perl-Proc-WaitStat

Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%{?perl_default_filter}

%description
mime-construct constructs and (by default) mails MIME messages.  
It is entirely driven from the command line, it is designed to be used 
by other programs, or people who act like programs.


%prep
%setup -q -n mime-construct-%{version}


%build
%{__perl} Makefile.PL INSTALLDIRS=vendor 
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null ';'
%{_fixperms} %{buildroot}/*


%check
make test


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README debian/changelog debian/copyright
%{_bindir}/*
%{_mandir}/man?/*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-16m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-3m)
- rebuild against perl-5.12.2

* Mon Sep 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-2m)
- use package name for BuildRequires
- use %%{buildroot} macro

* Sun Sep 12 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11-1m)
- import from Fedora

* Tue Jun 29 2010 Matthias Runge <mrunge@matthias-runge.de> 1.11-2
- fix up dependencies

* Thu Jun 22 2010 Matthias Runge <mrunge@matthias-runge.de> 1.11-1
- version updated to 1.11

* Tue Jun 22 2010 Matthias Runge <mrunge@matthias-runge.de> 1.10-2
- SPEC-file fixes (URL, include changelog)

* Tue Jun 8 2010 Matthias Runge <mrunge@matthias-runge.de> 1.10-1
- initial version
