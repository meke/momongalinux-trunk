%global         momorel 1

Name:           perl-Catalyst-Action-REST
Version:        1.15
Release:        %{momorel}m%{?dist}
Summary:        Automated REST Method Dispatching
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Action-REST/
Source0:        http://www.cpan.org/authors/id/F/FR/FREW/Catalyst-Action-REST-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Catalyst-Runtime >= 5.80030
BuildRequires:  perl-Class-Inspector >= 1.13
BuildRequires:  perl-Config-General
BuildRequires:  perl-Data-Serializer >= 0.36
BuildRequires:  perl-Data-Taxi
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-FreezeThaw
BuildRequires:  perl-JSON >= 2.12
BuildRequires:  perl-JSON-XS >= 2.2222
BuildRequires:  perl-libwww-perl >= 2.033
BuildRequires:  perl-Module-Pluggable
BuildRequires:  perl-Moose >= 1.03
BuildRequires:  perl-MooseX-Role-WithOverloading >= 0.09
BuildRequires:  perl-MRO-Compat >= 0.10
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Params-Validate >= 0.76
BuildRequires:  perl-PHP-Serialization
BuildRequires:  perl-URI-Find
BuildRequires:  perl-XML-Simple
BuildRequires:  perl-YAML-Syck >= 0.67
Requires:       perl-Catalyst-Runtime >= 5.80030
Requires:       perl-Class-Inspector >= 1.13
Requires:       perl-Config-General
Requires:       perl-Data-Serializer >= 0.36
Requires:       perl-Data-Taxi
Requires:       perl-FreezeThaw
Requires:       perl-JSON >= 2.12
Requires:       perl-JSON-XS >= 2.2222
Requires:       perl-libwww-perl >= 2.033
Requires:       perl-Module-Pluggable
Requires:       perl-Moose >= 1.03
Requires:       perl-MooseX-Role-WithOverloading >= 0.09
Requires:       perl-MRO-Compat >= 0.10
Requires:       perl-namespace-autoclean
Requires:       perl-Params-Validate >= 0.76
Requires:       perl-PHP-Serialization
Requires:       perl-URI-Find
Requires:       perl-XML-Simple
Requires:       perl-YAML-Syck >= 0.67
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This Action handles doing automatic method dispatching for REST requests.
It takes a normal Catalyst action, and changes the dispatch to append an
underscore and method name. First it will try dispatching to an action
with the generated name, and failing that it will try to dispatch to a
regular method.

%prep
%setup -q -n Catalyst-Action-REST-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%{perl_vendorlib}/Catalyst/Action/Deserialize.pm
%{perl_vendorlib}/Catalyst/Action/Deserialize
%{perl_vendorlib}/Catalyst/Action/DeserializeMultiPart.pm
%{perl_vendorlib}/Catalyst/Action/REST.pm
%{perl_vendorlib}/Catalyst/Action/REST
%{perl_vendorlib}/Catalyst/Action/Serialize.pm
%{perl_vendorlib}/Catalyst/Action/Serialize
%{perl_vendorlib}/Catalyst/Action/SerializeBase.pm
%{perl_vendorlib}/Catalyst/Controller/REST.pm
%{perl_vendorlib}/Catalyst/Request/REST.pm
%{perl_vendorlib}/Catalyst/Request/REST
%{perl_vendorlib}/Catalyst/TraitFor/Request/REST.pm
%{perl_vendorlib}/Catalyst/TraitFor/Request/REST
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- rebuild against perl-5.20.0
- update to 1.15

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-2m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-2m)
- rebuild against perl-5.18.0

* Tue Apr 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sat Apr 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Tue Apr 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Fri Apr 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-2m)
- rebuild against perl-5.16.3

* Wed Dec 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-2m)
- rebuild against perl-5.14.2

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.90-2m)
- rebuild for new GCC 4.6

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-1m)
- update to 0.89

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.87-2m)
- rebuild for new GCC 4.5

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.87-1m)
- update to 0.87

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
