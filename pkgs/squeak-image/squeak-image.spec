%global momorel 5

%define image_major 3.10
%define image_ver %{image_major}.2
%define image_rel 7179

Name:           squeak-image
Version:        %{image_ver}.%{image_rel}
Release:        %{momorel}m%{?dist}
Summary:        The image files for Squeak

Group:          Development/Languages
License:        MIT
URL:            http://www.squeak.org
Source0:        http://ftp.squeak.org/%{image_major}/Squeak%{image_ver}-%{image_rel}-basic.zip
NoSource:       0
Source1:        http://ftp.squeak.org/sources_files/SqueakV39.sources.gz
NoSource:       1
Source2:        squeak-image-doc.html

Requires:       squeak-vm >= 3.7

BuildArch:      noarch


%description
This is the standard Squeak image as distributed by sqeak.org.
The Squeak image is split into three interdependent parts, 
the .image file, the .changes file, and the .sources file.

%prep
%setup -q -c %{name}-%{version}
cp -p %SOURCE2 .

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/squeak
cp Squeak%{image_ver}-%{image_rel}-basic.image %{buildroot}%{_datadir}/squeak
cp Squeak%{image_ver}-%{image_rel}-basic.changes %{buildroot}%{_datadir}/squeak
zcat %{SOURCE1} >%{buildroot}%{_datadir}/squeak/SqueakV39.sources
cd %{buildroot}%{_datadir}/squeak
gzip Squeak%{image_ver}-%{image_rel}-basic.image
gzip Squeak%{image_ver}-%{image_rel}-basic.changes
ln -sf Squeak%{image_ver}-%{image_rel}-basic.image.gz squeak.image.gz
ln -sf Squeak%{image_ver}-%{image_rel}-basic.changes.gz squeak.changes.gz

# inisqueak is looking for SqueakV3.sources (not V39), create this for it
cd %{buildroot}%{_datadir}/squeak/
ln -s SqueakV39.sources SqueakV3.sources


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc squeak-image-doc.html
%{_datadir}/squeak/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.2.7179-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.2.7179-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.2.7179-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.2.7179-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.2-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.10.2.7179-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Nov 25 2008 Gavin Romig-Koch <gavin@redhat.com> - 3.10.2.7179-1
- Moved SqueakV39.sources into this rpm.
- Add a minimal doc page

* Sat Oct 22 2005 Gerard Milmeister <gemi@bluewin.ch> - 3.8.6665-1
- First Fedora release

