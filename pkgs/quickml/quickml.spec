%global momorel 16

Summary: A Very-Easy-To-Use Mailing List System
Name: quickml
Version: 0.7
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Daemons
URL: http://quickml.com/quickml/
Source0: http://quickml.com/%{name}/%{name}-%{version}.tar.gz
Source1: %{name}.init
Source2: %{name}.logrotate
Patch1: %{name}-0.6-rc.patch
Patch2: %{name}-0.7-Makefile.patch
Patch3: %{name}-0.7-rubydir.patch
Patch4: %{name}-0.7-aclocal.patch
Patch5: quickml-0.7-ruby18.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): grep
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(preun): shadow-utils
Requires: ruby18
BuildRequires: ruby18
BuildRequires: ruby-rdtool
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildArch: noarch

%description
quickml server is a very-easy-to-use mailing list system.
quickml server provides very-easy-to-use mailing list service.

* You can create mailing lists of any names you like.
* You can create mailing lists with any subdomains you like.
* quickml server runs as a stand-alone SMTP server.
* quickml server delegates mail delivery to another mail server.
* quickml server is simply written in Ruby.

%prep
%setup -q -n %{name}-%{version}
%patch1 -p1 -b .rc
%patch2 -p1 -b .bak
%patch3 -p1 -b .rubydir
%patch4 -p1 -b .aclocal
%patch5 -p1 -b .ruby18

%build
autoreconf -fi
export RUBY=/usr/bin/ruby18
%configure \
    --with-rubydir=%{_datadir}/%{name}/lib \
    --datadir=%{_datadir}/%{name} \
    --with-user=quickml \
    --with-group=quickml
make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__mkdir -p %{buildroot}/var/log
%__mkdir -p %{buildroot}%{_localstatedir}/%{name}
%__mkdir_p %{buildroot}%{_datadir}/%{name}/lib
make DESTDIR=%{buildroot} \
  quickmlstatedir=%{buildroot}%{_localstatedir}/%{name} \
  install
%__mv %{buildroot}%{_sysconfdir}/%{name}rc.sample %{buildroot}%{_sysconfdir}/%{name}rc
%__mkdir_p %{buildroot}%{_initscriptdir}
%__mkdir_p %{buildroot}%{_sysconfdir}/logrotate.d
%__install -c -m644 messages.* %{buildroot}%{_datadir}/%{name}/
%__install -c -m755 %{SOURCE1} %{buildroot}%{_initscriptdir}/%{name}
%__install -c -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/quickml
touch %{buildroot}/var/log/quickml-log

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%pre
QUICKML_USER=%{name}
QUICKML_GROUP=%{name}

if ! grep -q "^$QUICKML_GROUP:" /etc/group; then
    /usr/sbin/groupadd -r $QUICKML_GROUP
fi

if ! grep -q "^$QUICKML_USER:" /etc/passwd; then
    /usr/sbin/useradd -r -s /bin/false -g $QUICKML_GROUP -c "QuickML operator" $QUICKML_USER || :
fi

if [ -f %{_var}/lock/subsys/%{name} ]; then
    %{_initscriptdir}/%{name} stop
fi

exit 0

%post
umask 022

if [ -d %{_localstatedir}/lib/%{name} ]; then
	%__mv %{_localstatedir}/lib/%{name}/* %{_localstatedir}/%{name}
fi

/sbin/chkconfig --add %{name}

exit 0

%preun
QUICKML_USER=%{name}
QUICKML_GROUP=%{name}

umask 022

if [ "$1" = 0 ]; then
    /sbin/service %{name} stop &>/dev/null
    /sbin/chkconfig --del %{name}
#    userdel $QUICKML_USER
# userdel also deletes quickml group.
#    groupdel $QUICKML_GROUP 
fi

exit 0

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README *.rd
%{_sbindir}/*
%{_bindir}/*
%{_datadir}/%{name}
%dir %attr(-,quickml,quickml) %{_var}/%{name}
%{_initscriptdir}/%{name}
%attr(-,quickml,quickml) %config(noreplace) %{_sysconfdir}/%{name}rc
#%%config(noreplace) %{_sysconfdir}/logrotate.d/quickml
# force replace 0.6-2m only
%config %{_sysconfdir}/logrotate.d/quickml
%config(noreplace) %attr(0640,quickml,quickml) /var/log/quickml-log

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-16m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-13m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-12m)
- build with ruby18

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-11m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-8m)
- rebuild against gcc43

* Sat Jul 29 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (0.7-7m)
- move quickml specific library files to /usr/share/quickml/lib.

* Thu Mar 18 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (0.7-6m)
- don't use initlog -c, use daemon instead.

* Thu Nov 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.7-5m)
- revise init script

* Tue Nov  2 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.7-4m)
- chown quickml:quickml data directory
- now default log file name is 'quickml.log'

* Sat Oct 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.7-3m)
- canceled 2m
- stop deleting user and group on uninstallation

* Sat Oct 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.7-2m)
- fixed the name of log file (quickml-log to quickml.log)

* Sat Sep  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.7-1m)
- configure with '--with-user=quickml --with-group=quickml'

* Sat Sep 04 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-2m)
- stop quickml kicked by logrotate
- add condrestart to quickml.init(%{_initscriptdir}/%{name})

* Thu Feb 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-1m)

* Wed Nov 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.5-7m)
- bug fix ([Momonga-devel.ja:02292])

* Thu Sep 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.5-6m)
- cvs snapshot

* Tue Aug  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.5-5m)
- rebuild against ruby-1.8

* Wed Jun 04 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (0.5-4m)
- add logrotate configuration

* Wed Mar 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-3m)
- fix URL

* Wed Nov 20 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.5-2m)
- don't start service on any level by default.
- chagne statedir (/var/lib/quickml -> /var/quickml).

* Mon Sep 23 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.5-1m)
- initial import to Momonga.
