%global momorel 1
Name:           zathura-djvu
Version:        0.2.3
Release: %{momorel}m%{?dist}
Summary:        DjVu support for zathura

Group:          Applications/Publishing
License:        "zlib"
URL:            http://pwmt.org/projects/zathura/plugins/%{name}
Source0:        http://pwmt.org/projects/zathura/plugins/download/%{name}-%{version}.tar.gz
NoSource: 0
Requires:       zathura
BuildRequires:  djvulibre-devel
BuildRequires:  zathura-devel >= 0.2.3

%description
The zathura-djvu plugin adds DjVu support to zathura by
using the djvulibre library.

%prep
%setup -q
# Don't rebuild during install phase
sed -i 's/^install:\s*all/install:/' Makefile

%build
CFLAGS='%{optflags}' make %{?_smp_mflags} LIBDIR=%{_libdir} debug
mv djvu-debug.so djvu.so

%install
make DESTDIR=%{buildroot} LIBDIR=%{_libdir} install

%files
%doc AUTHORS LICENSE
%{_libdir}/zathura/djvu.so
%{_datadir}/applications/%{name}.desktop

%changelog
* Wed Feb 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-1m)
- import from fedora

