%global momorel 17
%global cipecfgver 1.0

Summary: Encrypted IP over UDP tunneling
Name: cipe
Version: 1.6.0
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
URL: http://sites.inka.de/~bigred/devel/cipe.html
BuildRequires: glibc-kernheaders, sed, gawk, newt-devel >= 0.52.2
BuildRequires: openssl-devel >= 1.0.0
Requires(post): info fileutils sh-utils
Requires(preun): info

Source0: http://sites.inka.de/~bigred/sw/cipe-%{version}.tar.gz
Source1: http://dl.sourceforge.net/sourceforge/cipecfg/cipecfg-%{cipecfgver}.tar.gz
Patch0: cipe-1.6.0.patch
Patch4: cipe-1.6.0-install.patch
Patch5: cipe-1.6.0-lib64.patch
Patch6: cipe-1.6.0-kernel.patch
Patch7: cipe-1.6.0-gcc43.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0
NoSource: 1

%ifarch i386 i486 i586 i686 athlon
%define cipecpu i386
%else
%ifarch alpha alphaev5
%define cipecpu alpha
%else
%define cipecpu %{_target_cpu}
%endif
%endif

%description
A network device that does encrypted IP-in-UDP tunneling.
Useful for building virtual private networks, etc.
The package consists of a kernel module and driver program.

%prep
%setup -q -a 1
%patch0 -p0
%patch4 -p0
%if %{_lib} == "lib64"
%patch5 -p1 -b .lib64~
%endif
%patch6 -p1 -b .kernel26
%patch7 -p1 -b .kernel26
sed -i "s/KVERS=XXX/KVERS=`uname -r`/g" configure

%build
%configure --enable-protocol=4 --with-linux=/usr
make all cipe.info

cd cipecfg-%{cipecfgver}
%configure
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/{usr/sbin,etc/cipe/samples,%{_infodir}}
#header_version=`grep UTS_RELEASE /usr/include/linux/version.h | awk '{print $3}' | sed -e 's/"//g'`
header_version=`uname -r`
if [ -d $header_version-%{cipecpu}-db ] ; then
  cipedir=$header_version-%{cipecpu}-db
elif [ -d $header_version-%{cipecpu}-SMP-db ] ; then
  cipedir=$header_version-%{cipecpu}-SMP-db
else
  exit -1
fi

make install BINDIR=%{buildroot}%{_sbindir} -C $cipedir
%makeinstall DESTDIR=%{buildroot} -C pkcipe
install -m 644 cipe.info %{buildroot}%{_infodir}
install -m 640 samples/options %{buildroot}/etc/cipe/samples
install -m 750 samples/{ip-up,ip-down} %{buildroot}/etc/cipe/samples

cd cipecfg-%{cipecfgver}
%makeinstall

%post
if [ ! -f /etc/cipe/identity.priv ] ; then
	echo "*** Generating new identity (host) key ***"
	/usr/bin/rsa-keygen /etc/cipe/identity
	chmod 600 /etc/cipe/identity
fi
/sbin/install-info --info-file %{_infodir}/cipe.info --dir-file=%{_infodir}/dir --entry="* CIPE: (cipe).                 encrypting IP tunnel device"

%preun
/sbin/install-info --delete --info-file=%{_infodir}/cipe.info --dir-file=%{_infodir}/dir --entry="* CIPE: (cipe).                 encrypting IP tunnel device"

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING CHANGES README README.key-bug tcpdump.patch samples
%dir /etc/cipe
%dir /etc/cipe/pk
%dir /etc/cipe/samples
%dir /var/run/cipe
/etc/cipe/samples/*
%{_sbindir}/ciped-db
%{_sbindir}/cipecfg
%{_sbindir}/pkcipe
%{_bindir}/rsa-keygen
%{_infodir}/cipe.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-15m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-14m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-12m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-11m)
- rebuild against rpm-4.6

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-10m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-9m)
- rebuild against gcc43

* Mon Mar 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-8m)
- support gcc43

* Sat May 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.6.0-7m)
- use kernel detail version

* Sat May 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.6.0-6m)
- fix UTS_RELEASE lost problem(2.6.20, linux/version.h) 
- add Patch6: cipe-1.6.0-kernel.patch

* Thu May 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-5m)
- rebuild against newt-0.52.2

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.0-4m)
- rebuild against openssl-0.9.8a

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.0-3m)
- telia.dl.sf.net -> jaist.dl.sf.net

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6.0-2m)
- enable x86_64.

* Tue Nov 30 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.6.0-1m)
- version up for gcc34
- update Patch0,4
- remove Patch1,2,3

* Fri Mar  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.5.2-13m)
- rebuild against newt-0.51.6-1m

* Fri Dec 19 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.2-12m)
- rebuild against newt

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.5.2-11m)
  rebuild against openssl 0.9.7a

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.5.2-10k)
- /sbin/install-info -> info in Prereq.

* Thu Nov  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.2-8k)
- use protocol number 4
- include pkcipe and rsa-keygen for protocol 4

* Wed Oct 31 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.2-6k)
- modify to work correctly

* Thu Oct 25 2001 MATSUDA, Daiki <dyky@df-uas.com>
- (1.5.2-4k)
- include cipecfg

* Thu Oct 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.2-2k)
- update to 1.5.2

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (1.5.1-10k)
- add alphaev5 support.

* Sat Jun 23 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.1-8k)
- add cipe-1.5.1-kernel-2.4.4.patch for kernel-2.4.4

* Wed May  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.1-6k)
- modified spec file for enabling to build on raichu

* Thu Apr 26 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.1-4k)
- add cipe-1.5.1.configure.patch, forgive me, dora!

* Fri Apr 13 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.1-2k)
- update to 1.5.1
- temporaly removed Patch1

* Sun Mar  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4.6-2k)
- update to 1.4.6

* Sun Dec 17 2000 KIM Hyeong Cheol <kim@kondara.org>
- fixed %preun

* Tue Dec 12 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4.5-3k)
- imported to Jirai
- update to 1.4.5

* Sat Sep  9 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4.1-0k)
- first release
