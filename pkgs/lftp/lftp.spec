%global         momorel 1

Summary:        A sophisticated file transfer program
Name:           lftp
Version:        4.5.2
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Source0:        http://lftp.yar.ru/ftp/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         lftp-3.7.7-momonga.patch
Patch1:         lftp-4.0.9-date_fmt.patch
Group:          Applications/Internet
URL:            http://lftp.yar.ru/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  readline-devel >= 5.0
BuildRequires:  glibc-devel
BuildRequires:  expat-devel
BuildRequires:  libgcrypt-devel
BuildRequires:  gnutls-devel >= 3.2.0
BuildRequires:  libgpg-error-devel
BuildRequires:  readline-devel
BuildRequires:  ncurses-devel
BuildRequires:  zlib-devel
BuildRequires:  libstdc++-static

%description
LFTP is a sophisticated ftp/http file transfer program. Like bash, it has job
control and uses the readline library for input. It has bookmarks, built-in
mirroring, and can transfer several files in parallel. It is designed with
reliability in mind.

%package scripts
Summary:        Scripts for lftp
Group:          Applications/Internet
Requires:       lftp >= %{version}-%{release}
BuildArch:      noarch

%description scripts
Utility scripts for use with lftp.

%prep

%setup -q
%patch0 -p1 -b .momonga~
%patch1 -p1 -b .date_fmt

#sed -i.rpath -e '/lftp_cv_openssl/s|-R.*lib||' configure
sed -i.norpath -e \
	'/sys_lib_dlsearch_path_spec/s|/usr/lib |/usr/lib /usr/lib64 /lib64 |' \
	configure

%build
%configure --with-modules --disable-static --with-gnutls --without-openssl --with-debug
make %{?_smp_mflags}
 

%install
export tagname=CC
make DESTDIR=%{buildroot} INSTALL='install -p' install
chmod 0755 %{buildroot}%{_libdir}/lftp/*
chmod 0755 %{buildroot}%{_libdir}/lftp/%{version}/*.so
iconv -f ISO88591 -t UTF8 NEWS -o NEWS.tmp
touch -c -r NEWS NEWS.tmp
mv NEWS.tmp NEWS
# Remove files from $RPM_BUILD_ROOT that we aren't shipping.
#rm $RPM_BUILD_ROOT%{_libdir}/lftp/%{version}/*.la
rm %{buildroot}%{_libdir}/liblftp-jobs.la
rm %{buildroot}%{_libdir}/liblftp-tasks.la
rm %{buildroot}%{_libdir}/liblftp-jobs.so
rm %{buildroot}%{_libdir}/liblftp-tasks.so


%find_lang %{name}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%doc BUGS COPYING ChangeLog FAQ FEATURES README* NEWS THANKS TODO
%config(noreplace) %{_sysconfdir}/lftp.conf
%{_bindir}/*
%{_mandir}/*/*
%dir %{_libdir}/lftp
%dir %{_libdir}/lftp/%{version}
%{_libdir}/lftp/%{version}/cmd-torrent.so
%{_libdir}/lftp/%{version}/cmd-mirror.so
%{_libdir}/lftp/%{version}/cmd-sleep.so
%{_libdir}/lftp/%{version}/liblftp-network.so
%{_libdir}/lftp/%{version}/liblftp-pty.so
%{_libdir}/lftp/%{version}/proto-file.so
%{_libdir}/lftp/%{version}/proto-fish.so
%{_libdir}/lftp/%{version}/proto-ftp.so
%{_libdir}/lftp/%{version}/proto-http.so
%{_libdir}/lftp/%{version}/proto-sftp.so
%{_libdir}/liblftp-jobs.so.*
%{_libdir}/liblftp-tasks.so.*

%files scripts
%defattr(-,root,root,-)
%{_datadir}/lftp

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.2-1m)
- update 4.5.2

* Wed Dec 18 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.4-1m)
- update to 4.4.4

* Fri Nov 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.11-1m)
- update to 4.4.11

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux,org>
- (4.4.8-2m)
- rebuild against gnutls-3.2.0

* Thu May 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.4.8-1m)
- update to 4.4.8

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to 4.4.5

* Tue Jan 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1

* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.8-1m)
- update to 4.3.8

* Sat Dec 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.4-1m)
- update to 4.3.4

* Sun Nov 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.3-1m)
- update to 4.3.3

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to 4.3.2

* Wed Jun 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to 4.2.3

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2

* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-3m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.1-1m)
- update to 4.2.1

* Thu Mar 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- update to 4.1.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1.1-1m)
- update to 4.1.1

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.10-1m)
- update to 4.0.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.9-2m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.9-1m)
- update to 4.0.9

* Tue May 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.8-1m)
- update to 4.0.8

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.0.7-1m)
- [SECURITY] CVE-2010-2251
- update to 4.0.7

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.5-2m)
- rebuild against readline6

* Mon Jan  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.5-1m)
- update to 4.0.5

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Wed Sep 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2
-- added BitTorrent client

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.15-1m)
- update to 3.7.15

* Fri May 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.14-1m)
- update to 3.7.14

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.13-1m)
- update to 3.7.13

* Sat Apr 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.11-1m)
- update to 3.7.11

* Wed Mar 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.9-1m)
- update to 3.7.9

* Sun Feb  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.8-1m)
- update to 3.7.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.7-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.7-2m)
- update Patch0 for fuzz=0
- License: GPLv3+

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.7-1m)
- update to 3.7.7
- License changed to GNU GPL3

* Thu Dec  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.6-1m)
- update 3.7.6
-- drop Patch1, merged upstream

* Tue Nov 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.5-1m)
- update to 3.7.5

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.4-1m)
- update to 3.7.4

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.3-2m)
- rebuild against gnutls-2.4.1

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.3-1m)
- update to 3.7.3

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.1-1m)
- update to 3.7.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.0-2m)
- rebuild against gcc43

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.0-1m)
- update to 3.7.0

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.3-1m)
- update to 3.6.3

* Tue Jan  8 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2
- remove `BuildRequires: openssl-devel >= 0.9.7d', not used as default

* Thu Nov 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1
- License: GPLv2
- add some BuildRequires

* Sun Sep  2 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.14-1m)
- update to 3.5.14

* Sat Jul 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.12-1m)
- update to 3.5.12

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.11-1m)
- update to 3.5.11

* Sat Apr  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-1m)
- update to 3.5.10

* Sun Jan 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-1m)
- [SECURITY] CVE-2007-2348
- update to 3.5.9

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- delete libtool library

* Wed Oct 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.5.1-3m)
- rebuild against gnutls-1.4.4-1m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.5.1-2m)
- rebuild against expat-2.0.0-1m

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.1-1m)
- version up
- rebuild against readline-5.0

* Thu May  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.4.6-1m)
- version up

* Tue Apr 11 2006 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.3-2m)
- revise lftp.conf patch for recent lftp variables

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.4.3-1m)
- update to 3.4.3

* Sun Dec 11 2005 Koichi Narita <pulsar@sea.plala.or.jp>
- (3.3.5-2m)
- Require: perl-String-CRC32 added

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.3.5-1m)
- update to 3.3.5

* Mon Dec 27 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.13-1m)
- update to 3.0.13

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.12-2m)
- add lftp-3.0.12-nocoredump.patch
  from http://www.mail-archive.com/lftp%40uniyar.ac.ru/msg01819.html

* Tue Dec  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.12-1m)
- minor bugfixes

* Sat Nov  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.11-1m)
- version up

* Wed Sep 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.9-1m)
- update to 3.0.9

* Wed Sep 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.8-1m)
- minor bugfixes

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.0.7-1m)
  update to 3.0.7

* Sun Jun 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.6-1m)
- minor bugfixes

* Tue Jun  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.5-1m)
- minor feature enhancements

* Wed May 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.4-1m)
- minor feature enhancements

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.3-2m)
- rebuild against ncurses 5.3.

* Tue Apr 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.3-1m)
- minor bugfixes

* Fri Apr 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.2-1m)
- several bugs fixed
- some minor features added

* Wed Apr  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.1-1m)
- minor bugfixes

* Sat Apr 03 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Jan 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.11-1m)
- update to 2.6.11

* Sun Dec 14 2003 TAKAHASHI Tamotsu <tamo>
- (2.6.10-1m)
- From NEWS:
	"security fixes in html parsing code."
	"fixed a rare bug with access to freed memory in ftp."
- you may want to "URL_ALIAS ^ftp://lftp\.yar\.ru/lftp/
 ftp://ftp.st.ryukoku.ac.jp/pub/network/ftp/lftp/" in your
 ~/.OmoiKondara

* Mon Nov 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.9-1m)
- update to 2.6.9

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.8-1m)
- update to 2.6.8

* Thu Jun 5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.6-1m)
- update to 2.6.6

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.5-2m)
  rebuild against openssl 0.9.7a

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.5-1m)
  update to 2.6.5

* Mon Feb 10 2003 TAKAHASHI Tamotsu <tamo>
- (2.6.4-2m)
- Patch1: lftp-2.6.4-continue.patch
 this should fix the "lftpget -c" problem

* Sat Jan 18 2003 TAKAHASHI Tamotsu
- (2.6.4-1m)
- Some new features added and some bugs fixed

* Thu Nov  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.3-1m)
- some bugfixes

* Wed Sep 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.2-1m)

* Sun Aug 18 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1-1m)
- up to 2.6.1

* Tue Aug  6 2002 Junichiro Kita <kita@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Tue Jun 11 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.5.4-2k)
  update to 2.5.4

* Tue May 21 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.5.2-2k)
  update to 2.5.2

* Wed Apr 17 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.5.1-2k)
- update to 2.5.1
- change Source0: URI http to ftp

  * Tue Mar 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.5.0a-2k)
  update to 2.5.0a

* Tue Feb  5 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.4.9-2k)
- change URL tag and Source0 URI

* Thu Nov  8 2001 Tsutomu Yasuda <tom@kondara.org>
- update to 2.4.6

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.4.5-4k)
- rebuild against gettext 0.10.40.

* Thu Oct 18 2001 Toshiro HIKITA <toshi@sodan.org>
- update to 2.4.5

* Wed Sep 19 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.4.4

* Fri Sep  9 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.4.3

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against openssl 0.9.6.

* Thu Mar 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.3.9

* Mon Feb  5 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.3.7

* Mon Nov 27 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.3.5
- without ssl
- add default setting net:timeout & dns:fatal-timeout

* Tue Oct 24 2000 Kenichi Matsubara <m@kondara.org>
- [2.3.4-3k]
- set max-retries 1.

* Sun Oct 22 2000 Kenichi Matsubara <m@kondara.org>
- [2.3.4-2k]
- add lftp-2.3.4-maxretries.patch. [ set retries 5 ]

* Fri Oct 13 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- build on Jirai
- static link /usr/lib/lftp/*.so

* Thu Oct 12 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.3.3

* Wed May  3 2000 AYUHANA Tomonori <l@kondara.org>
- version up 2.2.1a -> 2.2.2

* Thu Apr 27 2000 Takaaki Tabuchi <tab@kondara.org>
- add -q at %setup.
- be able to rebuild non-root user. (non-root.patch)

* Thu Apr 27 2000 AYUHANA Tomonori <l@kondara.org>
- version up 2.1.10 -> 2.2.1a

* Mon Mar 13 2000 AYUHANA Tomonori <l@kondara.org>
- version up 2.1.9 -> 2.1.10

* Tue Feb 22 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 2.1.9-2mdk
- Include src/ChangeLog not ChangeLog (outdated).
- 2.1.9.

* Sun Feb 20 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 2.1.8-1mdk
- 2.1.8.

* Wed Feb 16 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 2.1.7-1mdk
- 2.1.7.

* Tue Jan 18 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 2.1.6-1mdk
- 2.1.6.
- use %configure.

* Sun Nov 21 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- Last cvs version.
- Fix completion on directory.
- more alias.

* Wed Nov 10 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- 2.1.4.

* Tue Oct 19 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- 2.1.3.

* Wed Oct 13 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- 2.1.2

* Wed Sep 29 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- 2.1.0

* Wed Sep 22 1999 Daouda Lo <daouda@mandrakesoft.com>
- 2.0.5

* Wed Aug 18 1999 Thierry Vignaud <tvignaud@mandrakesoft.com>
- 2.0.4
- add the url
- fix a bug in spec that preventing from including locales
- fix permissions (now this could be builded as non root)

* Tue Jul 27 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- 2.0.3 finale.

* Thu Jul 22 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- pre2.0.3-990722 from CVS.
- enable --with-modules.
- Relifting of Spec files.

* Wed Jun 30 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- Set passive by default (special Pixel ;-)).
- 2.0.1.

* Mon Jun 21 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- 2.0.0
