%global momorel 4

Name:           lxtask
Version:        0.1.3
Release:        %{momorel}m%{?dist}
Summary:        Lightweight and desktop independent task manager

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-0.1.3-invalid-desktop-file.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.6, gettext, intltool, desktop-file-utils

%description
LXTask is a lightweight task manager derived from xfce4 task manager with all
xfce4 dependencies removed, some bugs fixed, and some improvement of UI. 
Although being part of LXDE, the Lightweight X11 Desktop Environment, it's 
totally desktop independent and only requires pure gtk+.
#'

%prep
%setup -q
%patch0 -p1 -b .invalid

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install --vendor=                             \
  --delete-original                                        \
  --add-category=Monitor                                   \
  --add-category=GTK                                       \
  --remove-category=Application                            \
  --remove-category=Utility                                \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications          \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-1m)
- update to 0.1.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1-2m)
- modified patch0 (add NotShowIn=GNOME;)

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-1m)
- import from Rawhide

* Mon Feb 23 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-2
- Fix categories in desktop file

* Sun May 04 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-1
- Initial Fedora RPM
