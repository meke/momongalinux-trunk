%global momorel 7
%global srcrel 117906
%global srcname myKrandr
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m

Summary: A multiple monitors management tool for KDE
Name: mykrandr
Version: 0.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kde-apps.org/content/show.php/MyKrandr?content=117906
Group: Applications/System
Source0: http://kde-apps.org/CONTENT/content-files/%{srcrel}-%{srcname}.tar.gz
Source1: %{name}.desktop
Patch0: %{name}-%{version}-fix-build.patch
Patch1: %{name}-%{version}-fix-typo.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdebase-libs >= %{kdever}-%{kdelibsrel}
Requires: oxygen-icons
Requires: xorg-x11-xrandr
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: coreutils

%description
A little GUI to manage multiple video outputs, 
e.g. if you use an external monitor with a laptop.

%prep
%setup -q -c

%patch0 -p1 -b .fix-build
%patch1 -p1 -b .fix-typo

%build
%{cmake_kde4} .

make %{?_smp_mflags} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
mkdir -p %{buildroot}%{_kde4_datadir}/applications/kde4
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_datadir}/applications/kde4/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc INSTALL README
%{_kde4_bindir}/krandr
%{_kde4_datadir}/applications/kde4/%{name}.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-6m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-5m)
-rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-3m)
- rebuild against qt-4.6.3-1m

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-2m)
- use BuildRequires

* Sat Jan 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-1m)
- initial package for Momonga Linux
