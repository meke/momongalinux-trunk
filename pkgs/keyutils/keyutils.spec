%global momorel 1

Summary: Linux Key Management Utilities
Name: keyutils
Version: 1.5.9
Release: %{momorel}m%{?dist}
License: LGPL and GPL
Group: System Environment/Base
Url: http://people.redhat.com/~dhowells/keyutils/

Source0: http://people.redhat.com/~dhowells/keyutils/keyutils-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glibc-kernheaders >= 2.5

%description
Utilities to control the kernel key management facility and to provide
a mechanism by which the kernel call back to userspace to get a key
instantiated.

%package libs
Summary: Key utilities library
Group: System Environment/Base

%description libs
This package provides a wrapper library for the key management facility system
calls.

%package libs-devel
Summary: Development package for building linux key management utilities
Group: System Environment/Base
Requires: keyutils-libs == %{version}-%{release}

%description libs-devel
This package provides headers and libraries for building key utilities.

%prep
%setup -q

%build
make \
	NO_ARLIB=1 \
	LIBDIR=/%{_lib} \
	USRLIBDIR=%{_libdir} \
	RELEASE=.%{release} \
	NO_GLIBC_KEYERR=1 \
	CFLAGS="-Wall $RPM_OPT_FLAGS -Werror"

%install
rm -rf --preserve-root %{buildroot}
make \
	NO_ARLIB=1 \
	DESTDIR=$RPM_BUILD_ROOT \
	LIBDIR=/%{_lib} \
	USRLIBDIR=%{_libdir} \
	install

%clean
rm -rf --preserve-root %{buildroot}

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README LICENCE.GPL
/bin/*
/sbin/*
%{_datadir}/keyutils
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man7/*
%{_mandir}/man8/*
%config(noreplace) /etc/*

%files libs
%defattr(-,root,root,-)
%doc LICENCE.LGPL
/%{_lib}/libkeyutils.so.*

%files libs-devel
%defattr(-,root,root,-)
%{_libdir}/libkeyutils.so
%{_includedir}/*
%{_mandir}/man3/*

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.9-1m)
- update 1.5.9

* Sun Jul 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-2m)
- rebuild against gcc43

* Wed May 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-1m)
- Initial Commit Momonga Linux

