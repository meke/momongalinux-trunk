%global momorel 1

%global contentdir  /var/www
# API/ABI check
%global apiver      20121113
%global zendver     20121212
%global pdover      20080721
# Extension version
%global fileinfover 1.0.5
%global pharver     2.0.2
%global zipver      1.11.0
%global jsonver     1.2.1

%global httpd_mmn %(cat %{_includedir}/httpd/.mmn || echo missing-httpd-devel)
%global mysql_sock %(mysql_config --socket || echo /var/lib/mysql/mysql.sock)

# Regression tests take a long time, you can skip 'em with this
%{!?runselftest: %{expand: %%global runselftest 1}}

# Use the arch-specific mysql_config binary to avoid mismatch with the
# arch detection heuristic used by bindir/mysql_config.
%global mysql_config %{_libdir}/mysql/mysql_config
%global with_zip     1
%global with_libzip  1
%global zipmod       zip

Summary: PHP scripting language for creating dynamic web sites
Name: php
Version: 5.5.12
Release: %{momorel}m%{?dist}
License: "The PHP License v3.01"
Group: Development/Languages
URL: http://www.php.net/

Source0: http://www.php.net/distributions/php-%{version}.tar.bz2
NoSource: 0
Source1: php.conf
Source2: php.ini
Source3: macros.php
Source4: php-fpm.conf
Source5: php-fpm-www.conf
Source6: php-fpm.service
Source7: php-fpm.logrotate
Source8: php-fpm.sysconfig
Source9: php.modconf
Source10: php.ztsmodconf
# Configuration files for some extensions
Source50: opcache.ini
Source51: opcache-default.blacklist

# Build fixes
Patch5: php-5.2.0-includedir.patch
Patch6: php-5.2.4-embed.patch
Patch7: php-5.3.0-recode.patch
Patch8: php-5.4.7-libdb.patch

# Fixes for extension modules
# https://bugs.php.net/63171 no odbc call during timeout
Patch21: php-5.4.7-odbctimer.patch

# Functional changes
Patch40: php-5.4.0-dlopen.patch
Patch42: php-5.3.1-systzdata-v8.patch
# See http://bugs.php.net/53436
Patch43: php-5.4.0-phpize.patch
# Use system libzip instead of bundled one
Patch44: php-5.5.2-system-libzip.patch
# Use -lldap_r for OpenLDAP
Patch45: php-5.4.8-ldap_r.patch
# Make php_config.h constant across builds
Patch46: php-5.4.9-fixheader.patch
# drop "Configure command" from phpinfo output
Patch47: php-5.4.9-phpinfo.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: bzip2-devel, curl-devel >= 7.9, db4-devel, gmp-devel >= 5.0.0
BuildRequires: httpd-devel >= 2.2.22, pam-devel
BuildRequires: libstdc++-devel, openssl-devel, sqlite-devel >= 3.6.0
BuildRequires: zlib-devel, pcre-devel >= 8.31, smtpdaemon, libedit-devel
BuildRequires: bzip2, perl, libtool >= 1.4.3, gcc-c++
%if %{with_libzip}
BuildRequires: libzip-devel >= 0.10.1-2m
%endif
Obsoletes: php-dbg, php3, phpfi, stronghold-php
# php-5.3.2
Obsoletes: php-mhash php-ncurses
Provides: php-mhash = %{version} php-ncurses = %{version}
Requires: httpd-mmn = %{httpd_mmn}
Provides: mod_php = %{version}-%{release}
Requires: php-common = %{version}-%{release}
# php-5.3.7
Obsoletes: php-zts
Provides: php-zts = %{version}
%if %{with_zip}
Provides: php-zip
Provides: php-pecl-zip = %{zipver}
Provides: php-pecl(zip) = %{zipver}
Obsoletes: php-pecl-zip
%endif
# For backwards-compatibility, require php-cli for the time being:
Requires: php-cli = %{version}-%{release}
# To ensure correct /var/lib/php/session ownership:
Requires(pre): httpd

%description
PHP is an HTML-embedded scripting language. PHP attempts to make it
easy for developers to write dynamically generated webpages. PHP also
offers built-in database integration for several commercial and
non-commercial database management systems, so writing a
database-enabled webpage with PHP is fairly simple. The most common
use of PHP coding is probably as a replacement for CGI scripts. 

The php package contains the module which adds support for the PHP
language to Apache HTTP Server.

%package cli
Group: Development/Languages
Summary: Command-line interface for PHP
Requires: php-common = %{version}-%{release}
Provides: php-cgi = %{version}-%{release}
Provides: php-pcntl, php-readline
Provides: %{_bindir}/php

%description cli
The php-cli package contains the command-line interface 
executing PHP scripts, /usr/bin/php, and the CGI interface.

%package fpm
Group: Development/Languages
Summary: PHP FastCGI Process Manager
Requires: php-common = %{version}-%{release}
BuildRequires: libevent-devel >= 1.4.11
BuildRequires: systemd-units
Requires: systemd-units
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
# This is actually needed for the %%triggerun script but Requires(triggerun)
# is not valid.  We can use %%post because this particular %%triggerun script
# should fire just after this package is installed.
Requires(post): systemd-sysv

%description fpm
PHP-FPM (FastCGI Process Manager) is an alternative PHP FastCGI
implementation with some additional features useful for sites of
any size, especially busier sites.

%package common
Group: Development/Languages
Summary: Common files for PHP
Provides: php-api = %{apiver}, php-zend-abi = %{zendver}
Provides: php(api) = %{apiver}, php(zend-abi) = %{zendver}
# Provides for all builtin modules:
Provides: php-bz2, php-calendar, php-ctype, php-curl, php-date, php-exif
Provides: php-ftp, php-gettext, php-gmp, php-hash, php-iconv, php-libxml
Provides: php-reflection, php-session, php-shmop, php-simplexml, php-sockets
Provides: php-spl, php-tokenizer, php-openssl, php-pcre
Provides: php-zlib, php-json, php-zip, php-fileinfo
Obsoletes: php-openssl, php-pecl-zip, php-pecl-json, php-json, php-pecl-phar, php-pecl-Fileinfo, php-suhosin
# For obsoleted pecl extension
Provides: php-pecl-json = %{jsonver}, php-pecl(json) = %{jsonver}
Provides: php-pecl-zip = %{zipver}, php-pecl(zip) = %{zipver}
Provides: php-pecl-phar = %{pharver}, php-pecl(phar) = %{pharver}
Provides: php-pecl-Fileinfo = %{fileinfover}, php-pecl(Fileinfo) = %{fileinfover}

%description common
The php-common package contains files used by both the php
package and the php-cli package.

%package devel
Group: Development/Libraries
Summary: Files needed for building PHP extensions
Requires: php = %{version}-%{release}, autoconf, automake
Obsoletes: php-pecl-pdo-devel

%description devel
The php-devel package contains the files needed for building PHP
extensions. If you need to compile your own PHP extensions, you will
need to install this package.

%package opcache
Summary:   The Zend OPcache
Group:     Development/Languages
License:   PHP
Requires:  php-common = %{version}-%{release}
Obsoletes: php-pecl-apc

%description opcache
The Zend OPcache provides faster PHP execution through opcode caching and
optimization. It improves PHP performance by storing precompiled script
bytecode in the shared memory. This eliminates the stages of reading code from
the disk and compiling it on future access. In addition, it applies a few
bytecode optimization patterns that make code execution faster.

%package imap
Summary: A module for PHP applications that use IMAP
Group: Development/Languages
Requires: php-common = %{version}-%{release}
Obsoletes: mod_php3-imap, stronghold-php-imap
BuildRequires: krb5-devel, openssl-devel, libc-client-devel

%description imap
The php-imap package contains a dynamic shared object (DSO) for the
Apache Web server. When compiled into Apache, the php-imap module will
add IMAP (Internet Message Access Protocol) support to PHP. IMAP is a
protocol for retrieving and uploading e-mail messages on mail
servers. PHP is an HTML-embedded scripting language. If you need IMAP
support for PHP applications, you will need to install this package
and the php package.

%package ldap
Summary: A module for PHP applications that use LDAP
Group: Development/Languages
Requires: php-common = %{version}-%{release}
Obsoletes: mod_php3-ldap, stronghold-php-ldap
BuildRequires: cyrus-sasl-devel, openldap-devel, openssl-devel

%description ldap
The php-ldap package is a dynamic shared object (DSO) for the Apache
Web server that adds Lightweight Directory Access Protocol (LDAP)
support to PHP. LDAP is a set of protocols for accessing directory
services over the Internet. PHP is an HTML-embedded scripting
language. If you need LDAP support for PHP applications, you will
need to install this package in addition to the php package.

%package pdo
Summary: A database access abstraction module for PHP applications
Group: Development/Languages
Requires: php-common = %{version}-%{release}
Obsoletes: php-pecl-pdo-sqlite, php-pecl-pdo
Provides: php-pdo-abi = %{pdover}
Provides: php-sqlite3, php-pdo_sqlite

%description pdo
The php-pdo package contains a dynamic shared object that will add
a database access abstraction layer to PHP.  This module provides
a common interface for accessing MySQL, PostgreSQL or other 
databases.

%package mysql
Summary: A module for PHP applications that use MySQL databases
Group: Development/Languages
Requires: php-common = %{version}-%{release}, php-pdo
Provides: php_database, php-mysqli, php-pdo_mysql
Obsoletes: mod_php3-mysql, stronghold-php-mysql
BuildRequires: mysql-devel >= 5.5.10

%description mysql
The php-mysql package contains a dynamic shared object that will add
MySQL database support to PHP. MySQL is an object-relational database
management system. PHP is an HTML-embeddable scripting language. If
you need MySQL support for PHP applications, you will need to install
this package and the php package.

%package mysqlnd
Summary: A module for PHP applications that use MySQL databases
Group: Development/Languages
Requires: php-pdo = %{version}-%{release}
Provides: php_database
Provides: php-mysql = %{version}-%{release}
Provides: php-mysql = %{version}-%{release}
Provides: php-mysqli = %{version}-%{release}
Provides: php-mysqli = %{version}-%{release}
Provides: php-pdo_mysql, php-pdo_mysql

%description mysqlnd
The php-mysqlnd package contains a dynamic shared object that will add
MySQL database support to PHP. MySQL is an object-relational database
management system. PHP is an HTML-embeddable scripting language. If
you need MySQL support for PHP applications, you will need to install
this package and the php package.

This package use the MySQL Native Driver

%package pgsql
Summary: A PostgreSQL database module for PHP
Group: Development/Languages
Requires: php-common = %{version}-%{release}, php-pdo
Provides: php_database, php-pdo_pgsql
Obsoletes: mod_php3-pgsql, stronghold-php-pgsql
BuildRequires: krb5-devel, openssl-devel, postgresql-devel

%description pgsql
The php-pgsql package includes a dynamic shared object (DSO) that can
be compiled in to the Apache Web server to add PostgreSQL database
support to PHP. PostgreSQL is an object-relational database management
system that supports almost all SQL constructs. PHP is an
HTML-embedded scripting language. If you need back-end support for
PostgreSQL, you should install this package in addition to the main
php package.

%package process
Summary: Modules for PHP script using system process interfaces
Group: Development/Languages
Requires: php-common = %{version}-%{release}
Provides: php-posix, php-sysvsem, php-sysvshm, php-sysvmsg

%description process
The php-process package contains dynamic shared objects which add
support to PHP using system interfaces for inter-process
communication.

%package odbc
Group: Development/Languages
Requires: php-common = %{version}-%{release}, php-pdo
Summary: A module for PHP applications that use ODBC databases
Provides: php_database, php-pdo_odbc
Obsoletes: stronghold-php-odbc
BuildRequires: unixODBC-devel

%description odbc
The php-odbc package contains a dynamic shared object that will add
database support through ODBC to PHP. ODBC is an open specification
which provides a consistent API for developers to use for accessing
data sources (which are often, but not always, databases). PHP is an
HTML-embeddable scripting language. If you need ODBC support for PHP
applications, you will need to install this package and the php
package.

%package soap
Group: Development/Languages
Requires: php-common = %{version}-%{release}
Summary: A module for PHP applications that use the SOAP protocol
BuildRequires: libxml2-devel

%description soap
The php-soap package contains a dynamic shared object that will add
support to PHP for using the SOAP web services protocol.

%package interbase
Summary: 	A module for PHP applications that use Interbase/Firebird databases
Group: 		Development/Languages
BuildRequires:  firebird-devel >= 2.1.3.18185.0-7m
Requires: 	php-common = %{version}-%{release}, php-pdo
Provides: 	php_database, php-firebird, php-pdo_firebird

%description interbase
The php-interbase package contains a dynamic shared object that will add
database support through Interbase/Firebird to PHP.

InterBase is the name of the closed-source variant of this RDBMS that was
developed by Borland/Inprise. 

Firebird is a commercially independent project of C and C++ programmers, 
technical advisors and supporters developing and enhancing a multi-platform 
relational database management system based on the source code released by 
Inprise Corp (now known as Borland Software Corp) under the InterBase Public
License.

%package snmp
Summary: A module for PHP applications that query SNMP-managed devices
Group: Development/Languages
Requires: php-common = %{version}-%{release}, net-snmp
BuildRequires: net-snmp-devel >= 5.7

%description snmp
The php-snmp package contains a dynamic shared object that will add
support for querying SNMP devices to PHP.  PHP is an HTML-embeddable
scripting language. If you need SNMP support for PHP applications, you
will need to install this package and the php package.

%package xml
Summary: A module for PHP applications which use XML
Group: Development/Languages
Requires: php-common = %{version}-%{release}
Obsoletes: php-domxml, php-dom
Provides: php-dom, php-xsl, php-domxml, php-wddx
BuildRequires: libxslt-devel >= 1.0.18-1, libxml2-devel >= 2.4.14-1

%description xml
The php-xml package contains dynamic shared objects which add support
to PHP for manipulating XML documents using the DOM tree,
and performing XSL transformations on XML documents.

%package xmlrpc
Summary: A module for PHP applications which use the XML-RPC protocol
Group: Development/Languages
Requires: php-common = %{version}-%{release}

%description xmlrpc
The php-xmlrpc package contains a dynamic shared object that will add
support for the XML-RPC protocol to PHP.

%package mbstring
Summary: A module for PHP applications which need multi-byte string handling
Group: Development/Languages
Requires: php-common = %{version}-%{release}

%description mbstring
The php-mbstring package contains a dynamic shared object that will add
support for multi-byte string handling to PHP.

%package gd
Summary: A module for PHP applications for using the gd graphics library
Group: Development/Languages
Requires: php-common = %{version}-%{release}
# Required to build the bundled GD library
BuildRequires: libXpm-devel, libjpeg-devel, libpng-devel, freetype-devel, t1lib-devel

%description gd
The php-gd package contains a dynamic shared object that will add
support for using the gd graphics library to PHP.

%package bcmath
Summary: A module for PHP applications for using the bcmath library
Group: Development/Languages
Requires: php-common = %{version}-%{release}

%description bcmath
The php-bcmath package contains a dynamic shared object that will add
support for using the bcmath library to PHP.

%package dba
Summary: A database abstraction layer module for PHP applications
Group: Development/Languages
Requires: php-common = %{version}-%{release}

%description dba
The php-dba package contains a dynamic shared object that will add
support for using the DBA database abstraction layer to PHP.

%package mcrypt
Summary: Standard PHP module provides mcrypt library support
Group: Development/Languages
Requires: php-common = %{version}-%{release}
BuildRequires: libmcrypt-devel

%description mcrypt
The php-mcrypt package contains a dynamic shared object that will add
support for using the mcrypt library to PHP.

%package tidy
Summary: Standard PHP module provides tidy library support
Group: Development/Languages
Requires: php-common = %{version}-%{release}
BuildRequires: libtidy-devel

%description tidy
The php-tidy package contains a dynamic shared object that will add
support for using the tidy library to PHP.

%package mssql
Summary: MSSQL database module for PHP
Group: Development/Languages
Requires: php-common = %{version}-%{release}, php-pdo
BuildRequires: freetds-devel
Provides: php-pdo_dblib

%description mssql
The php-mssql package contains a dynamic shared object that will
add MSSQL database support to PHP.  It uses the TDS (Tabular
DataStream) protocol through the freetds library, hence any
database server which supports TDS can be accessed.

%package embedded
Summary: PHP library for embedding in applications
Group: System Environment/Libraries
Requires: php-common = %{version}-%{release}
# doing a real -devel package for just the .so symlink is a bit overkill
Provides: php-embedded-devel = %{version}-%{release}

%description embedded
The php-embedded package contains a library which can be embedded
into applications to provide PHP scripting language support.

%package pspell
Summary: A module for PHP applications for using pspell interfaces
Group: System Environment/Libraries
Requires: php-common = %{version}-%{release}
BuildRequires: aspell-devel >= 0.50.0

%description pspell
The php-pspell package contains a dynamic shared object that will add
support for using the pspell library to PHP.

%package recode
Summary: A module for PHP applications for using the recode library
Group: System Environment/Libraries
Requires: php-common = %{version}-%{release}
BuildRequires: recode-devel

%description recode
The php-recode package contains a dynamic shared object that will add
support for using the recode library to PHP.

%package intl
Summary: Internationalization extension for PHP applications
Group: System Environment/Libraries
Requires: php-common = %{version}-%{release}
BuildRequires: libicu-devel >= 52

%description intl
The php-intl package contains a dynamic shared object that will add
support for using the ICU library to PHP.

%package enchant
Summary: Human Language and Character Encoding Support
Group: System Environment/Libraries
Requires: php-common = %{version}-%{release}
BuildRequires: enchant-devel >= 1.2.4

%description enchant
The php-intl package contains a dynamic shared object that will add
support for using the enchant library to PHP.


%prep
%setup -q
%patch5 -p1 -b .includedir
%patch6 -p1 -b .embed
%patch7 -p1 -b .recode

%patch21 -p1 -b .odbctimer

%patch40 -p1 -b .dlopen
%patch42 -p1 -b .systzdata
%patch43 -p1 -b .headers
%if %{with_libzip}
%patch44 -p1 -b .systzip
%endif
%patch45 -p1 -b .ldap_r
%patch46 -p1 -b .fixheader
%patch47 -p1 -b .phpinfo

# Prevent %%doc confusion over LICENSE files
cp Zend/LICENSE Zend/ZEND_LICENSE
cp TSRM/LICENSE TSRM_LICENSE
cp ext/ereg/regex/COPYRIGHT regex_COPYRIGHT
cp ext/gd/libgd/README gd_README

# Multiple builds for multiple SAPIs
mkdir build-cgi build-apache build-embedded build-zts build-ztscli build-fpm

# Remove bogus test; position of read position after fopen(, "a+")
# is not defined by C standard, so don't presume anything.
rm -f ext/standard/tests/file/bug21131.phpt
# php_egg_logo_guid() removed by patch41
rm -f tests/basic/php_egg_logo_guid.phpt

# Tests that fail.
rm -f ext/standard/tests/file/bug22414.phpt \
      ext/iconv/tests/bug16069.phpt

# Safety check for API version change.
vapi=`sed -n '/#define PHP_API_VERSION/{s/.* //;p}' main/php.h`
if test "x${vapi}" != "x%{apiver}"; then
   : Error: Upstream API version is now ${vapi}, expecting %{apiver}.
   : Update the apiver macro and rebuild.
   exit 1
fi

vzend=`sed -n '/#define ZEND_MODULE_API_NO/{s/^[^0-9]*//;p;}' Zend/zend_modules.h`
if test "x${vzend}" != "x%{zendver}"; then
   : Error: Upstream Zend ABI version is now ${vzend}, expecting %{zendver}.
   : Update the zendver macro and rebuild.
   exit 1
fi

# Safety check for PDO ABI version change
vpdo=`sed -n '/#define PDO_DRIVER_API/{s/.*[ 	]//;p}' ext/pdo/php_pdo_driver.h`
if test "x${vpdo}" != "x%{pdover}"; then
   : Error: Upstream PDO ABI version is now ${vpdo}, expecting %{pdover}.
   : Update the pdover macro and rebuild.
   exit 1
fi

# Check for some extension version
ver=$(sed -n '/#define PHP_FILEINFO_VERSION /{s/.* "//;s/".*$//;p}' ext/fileinfo/php_fileinfo.h)
if test "$ver" != "%{fileinfover}"; then
   : Error: Upstream FILEINFO version is now ${ver}, expecting %{fileinfover}.
   : Update the fileinfover macro and rebuild.
   exit 1
fi
ver=$(sed -n '/#define PHP_PHAR_VERSION /{s/.* "//;s/".*$//;p}' ext/phar/php_phar.h)
if test "$ver" != "%{pharver}"; then
   : Error: Upstream PHAR version is now ${ver}, expecting %{pharver}.
   : Update the pharver macro and rebuild.
   exit 1
fi
ver=$(sed -n '/#define PHP_ZIP_VERSION_STRING /{s/.* "//;s/".*$//;p}' ext/zip/php_zip.h)
if test "$ver" != "%{zipver}"; then
   : Error: Upstream ZIP version is now ${ver}, expecting %{zipver}.
   : Update the zipver macro and rebuild.
   exit 1
fi
ver=$(sed -n '/#define PHP_JSON_VERSION /{s/.* "//;s/".*$//;p}' ext/json/php_json.h)
if test "$ver" != "%{jsonver}"; then
   : Error: Upstream JSON version is now ${ver}, expecting %{jsonver}.
   : Update the jsonver macro and rebuild.
   exit 1
fi

# Fix some bogus permissions
find . -name \*.[ch] -exec chmod 644 {} \;
chmod 644 README.*

# php-fpm configuration files for tmpfiles.d
echo "d %{_localstatedir}/run/php-fpm 755 root root" >php-fpm.tmpfiles

# Some extensions have their own configuration file
cp %{SOURCE50} .

%build
# aclocal workaround - to be improved
cat `aclocal --print-ac-dir`/{libtool,ltoptions,ltsugar,ltversion,lt~obsolete}.m4 >>aclocal.m4

# Force use of system libtool:
libtoolize --force --copy
cat `aclocal --print-ac-dir`/{libtool,ltoptions,ltsugar,ltversion,lt~obsolete}.m4 >build/libtool.m4

# Regenerate configure scripts (patches change config.m4's)
touch configure.in
./buildconf --force

CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -Wno-pointer-sign"
export CFLAGS

# Install extension modules in %{_libdir}/php/modules.
EXTENSION_DIR=%{_libdir}/php/modules; export EXTENSION_DIR

# Set PEAR_INSTALLDIR to ensure that the hard-coded include_path
# includes the PEAR directory even though pear is packaged
# separately.
PEAR_INSTALLDIR=%{_datadir}/pear; export PEAR_INSTALLDIR

# Shell function to configure and build a PHP tree.
build() {
# bison-1.875-2 seems to produce a broken parser; workaround.
# mkdir Zend && cp ../Zend/zend_{language,ini}_{parser,scanner}.[ch] Zend
ln -sf ../configure
%configure \
	--cache-file=../config.cache \
        --with-libdir=%{_lib} \
	--with-config-file-path=%{_sysconfdir} \
	--with-config-file-scan-dir=%{_sysconfdir}/php.d \
	--disable-debug \
	--with-pic \
	--disable-rpath \
	--without-pear \
	--with-bz2 \
	--with-exec-dir=%{_bindir} \
	--with-freetype-dir=%{_prefix} \
	--with-png-dir=%{_prefix} \
	--with-xpm-dir=%{_prefix} \
	--enable-gd-native-ttf \
	--with-t1lib=%{_prefix} \
	--without-gdbm \
	--with-gettext \
	--with-gmp \
	--with-iconv \
	--with-jpeg-dir=%{_prefix} \
	--with-openssl \
        --with-pcre-regex=%{_prefix} \
	--with-zlib \
	--with-layout=GNU \
	--enable-exif \
	--enable-ftp \
	--enable-magic-quotes \
	--enable-sockets \
	--with-kerberos \
	--enable-ucd-snmp-hack \
	--enable-shmop \
	--enable-calendar \
        --with-libxml-dir=%{_prefix} \
	--enable-xml \
        --with-system-tzdata \
	--with-mhash \
	$* 
if test $? != 0; then 
  tail -500 config.log
  : configure failed
  exit 1
fi

make %{?_smp_mflags}
}

# Build /usr/bin/php-cgi with the CGI SAPI, and all the shared extensions
pushd build-cgi
build --enable-force-cgi-redirect \
      --libdir=%{_libdir}/php \
      --enable-pcntl \
      --with-imap=shared --with-imap-ssl \
      --enable-mbstring=shared \
      --enable-mbregex \
      --with-gd=shared \
      --enable-bcmath=shared \
      --enable-dba=shared --with-db4=%{_prefix} \
      --with-xmlrpc=shared \
      --with-ldap=shared --with-ldap-sasl \
      --enable-mysqlnd=shared \
      --with-mysql=shared,mysqlnd \
      --with-mysqli=shared,mysqlnd \
      --with-mysql-sock=%{mysql_sock} \
      --with-interbase=shared,%{_libdir}/firebird \
      --with-pdo-firebird=shared,%{_libdir}/firebird \
      --enable-dom=shared \
      --with-pgsql=shared \
      --enable-wddx=shared \
      --with-snmp=shared,%{_prefix} \
      --enable-soap=shared \
      --with-xsl=shared,%{_prefix} \
      --enable-xmlreader=shared --enable-xmlwriter=shared \
      --with-curl=shared,%{_prefix} \
      --enable-fastcgi \
      --enable-pdo=shared \
      --with-pdo-odbc=shared,unixODBC,%{_prefix} \
      --with-pdo-mysql=shared,mysqlnd \
      --with-pdo-pgsql=shared,%{_prefix} \
      --with-pdo-sqlite=shared,%{_prefix} \
      --with-pdo-dblib=shared,%{_prefix} \
      --with-sqlite3=shared,%{_prefix} \
      --enable-json=shared \
%if %{with_zip}
      --enable-zip=shared \
%endif
%if %{with_libzip}
      --with-libzip \
%endif
      --without-readline \
      --with-libedit \
      --with-pspell=shared \
      --enable-phar=shared \
      --with-mcrypt=shared,%{_prefix} \
      --with-tidy=shared,%{_prefix} \
      --with-mssql=shared,%{_prefix} \
      --enable-sysvmsg=shared --enable-sysvshm=shared --enable-sysvsem=shared \
      --enable-posix=shared \
      --with-unixODBC=shared,%{_prefix} \
      --enable-fileinfo=shared \
      --enable-intl=shared \
      --with-icu-dir=%{_prefix} \
      --with-enchant=shared,%{_prefix} \
      --with-recode=shared,%{_prefix}
popd

without_shared="--without-gd \
      --disable-dom --disable-dba --without-unixODBC \
      --disable-xmlreader --disable-xmlwriter \
      --without-sqlite3 --disable-phar --disable-fileinfo \
      --disable-json --without-pspell --disable-wddx \
      --without-curl --disable-posix \
      --disable-sysvmsg --disable-sysvshm --disable-sysvsem"

# Build Apache module, and the CLI SAPI, /usr/bin/php
pushd build-apache
build --with-apxs2=%{_httpd_apxs} \
      --libdir=%{_libdir}/php \
      --enable-pdo=shared \
      --with-mysql=shared,%{_prefix} \
      --with-mysqli=shared,%{mysql_config} \
      --with-pdo-mysql=shared,%{mysql_config} \
      --with-pdo-sqlite=shared,%{_prefix} \
      ${without_shared}
popd

# Build php-fpm
pushd build-fpm
build --enable-fpm \
      --libdir=%{_libdir}/php \
      --without-mysql --disable-pdo \
      ${without_shared}
popd

# Build for inclusion as embedded script language into applications,
# /usr/lib[64]/libphp5.so
pushd build-embedded
build --enable-embed \
      --without-mysql --disable-pdo \
      ${without_shared}
popd

# Build a special thread-safe (mainly for modules)
pushd build-ztscli

EXTENSION_DIR=%{_libdir}/php-zts/modules
build --enable-force-cgi-redirect \
      --includedir=%{_includedir}/php-zts \
      --libdir=%{_libdir}/php-zts \
      --enable-maintainer-zts \
      --with-config-file-scan-dir=%{_sysconfdir}/php-zts.d \
      --enable-pcntl \
      --with-imap=shared --with-imap-ssl \
      --enable-mbstring=shared \
      --enable-mbregex \
      --with-gd=shared \
      --enable-bcmath=shared \
      --enable-dba=shared --with-db4=%{_prefix} \
      --with-xmlrpc=shared \
      --with-ldap=shared --with-ldap-sasl \
      --enable-mysqlnd=shared \
      --with-mysql=shared,mysqlnd \
      --with-mysqli=shared,mysqlnd \
      --with-mysql-sock=%{mysql_sock} \
      --enable-mysqlnd-threading \
      --with-interbase=shared,%{_libdir}/firebird \
      --with-pdo-firebird=shared,%{_libdir}/firebird \
      --enable-dom=shared \
      --with-pgsql=shared \
      --enable-wddx=shared \
      --with-snmp=shared,%{_prefix} \
      --enable-soap=shared \
      --with-xsl=shared,%{_prefix} \
      --enable-xmlreader=shared --enable-xmlwriter=shared \
      --with-curl=shared,%{_prefix} \
      --enable-fastcgi \
      --enable-pdo=shared \
      --with-pdo-odbc=shared,unixODBC,%{_prefix} \
      --with-pdo-mysql=shared,mysqlnd \
      --with-pdo-pgsql=shared,%{_prefix} \
      --with-pdo-sqlite=shared,%{_prefix} \
      --with-pdo-dblib=shared,%{_prefix} \
      --with-sqlite3=shared,%{_prefix} \
      --enable-json=shared \
%if %{with_zip}
      --enable-zip=shared \
%endif
%if %{with_libzip}
      --with-libzip \
%endif
      --without-readline \
      --with-libedit \
      --with-pspell=shared \
      --enable-phar=shared \
      --with-mcrypt=shared,%{_prefix} \
      --with-tidy=shared,%{_prefix} \
      --with-mssql=shared,%{_prefix} \
      --enable-sysvmsg=shared --enable-sysvshm=shared --enable-sysvsem=shared \
      --enable-posix=shared \
      --with-unixODBC=shared,%{_prefix} \
      --enable-fileinfo=shared \
      --enable-intl=shared \
      --with-icu-dir=%{_prefix} \
      --with-enchant=shared,%{_prefix} \
      --with-recode=shared,%{_prefix}
popd

# Build a special thread-safe Apache SAPI
pushd build-zts
build --with-apxs2=%{_httpd_apxs} \
      --includedir=%{_includedir}/php-zts \
      --libdir=%{_libdir}/php-zts \
      --enable-maintainer-zts \
      --with-config-file-scan-dir=%{_sysconfdir}/php-zts.d \
      --enable-pdo=shared \
      --with-mysql=shared,%{_prefix} \
      --with-mysqli=shared,%{mysql_config} \
      --with-pdo-mysql=shared,%{mysql_config} \
      --with-pdo-sqlite=shared,%{_prefix} \
      ${without_shared}
popd

### NOTE!!! EXTENSION_DIR was changed for the -zts build, so it must remain
### the last SAPI to be built.

%check
%if %runselftest
cd build-apache
# Run tests, using the CLI SAPI
export NO_INTERACTION=1 REPORT_EXIT_STATUS=1 MALLOC_CHECK_=2
unset TZ LANG LC_ALL
if ! make test; then
  set +x
  for f in `find .. -name \*.diff -type f -print`; do
    echo "TEST FAILURE: $f --"
    cat "$f"
    echo "-- $f result ends."
  done
  set -x
  #exit 1
fi
unset NO_INTERACTION REPORT_EXIT_STATUS MALLOC_CHECK_
%endif

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

# Install the extensions for the ZTS version
make -C build-ztscli install \
     INSTALL_ROOT=$RPM_BUILD_ROOT

# rename extensions build with mysqlnd
mv $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/mysql.so \
   $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/mysqlnd_mysql.so
mv $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/mysqli.so \
   $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/mysqlnd_mysqli.so
mv $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/pdo_mysql.so \
   $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/pdo_mysqlnd.so

# Install the extensions for the ZTS version modules for libmysql
make -C build-zts install-modules \
     INSTALL_ROOT=$RPM_BUILD_ROOT

# rename ZTS binary
mv $RPM_BUILD_ROOT%{_bindir}/php        $RPM_BUILD_ROOT%{_bindir}/zts-php
mv $RPM_BUILD_ROOT%{_bindir}/phpize     $RPM_BUILD_ROOT%{_bindir}/zts-phpize
mv $RPM_BUILD_ROOT%{_bindir}/php-config $RPM_BUILD_ROOT%{_bindir}/zts-php-config

# Install the version for embedded script language in applications + php_embed.h
make -C build-embedded install-sapi install-headers \
     INSTALL_ROOT=$RPM_BUILD_ROOT

# Install the php-fpm binary
make -C build-fpm install-fpm \
     INSTALL_ROOT=$RPM_BUILD_ROOT

# Install everything from the CGI SAPI build
make -C build-cgi install \
     INSTALL_ROOT=$RPM_BUILD_ROOT

# rename extensions build with mysqlnd
mv $RPM_BUILD_ROOT%{_libdir}/php/modules/mysql.so \
   $RPM_BUILD_ROOT%{_libdir}/php/modules/mysqlnd_mysql.so
mv $RPM_BUILD_ROOT%{_libdir}/php/modules/mysqli.so \
   $RPM_BUILD_ROOT%{_libdir}/php/modules/mysqlnd_mysqli.so
mv $RPM_BUILD_ROOT%{_libdir}/php/modules/pdo_mysql.so \
   $RPM_BUILD_ROOT%{_libdir}/php/modules/pdo_mysqlnd.so

# Install the mysql extension build with libmysql
make -C build-apache install-modules \
     INSTALL_ROOT=$RPM_BUILD_ROOT

# Install the default configuration file and icons
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/php.ini
install -m 755 -d $RPM_BUILD_ROOT%{contentdir}/icons
install -m 644 php.gif $RPM_BUILD_ROOT%{contentdir}/icons/php.gif

# For third-party packaging:
install -m 755 -d $RPM_BUILD_ROOT%{_datadir}/php

# install the DSO
install -m 755 -d $RPM_BUILD_ROOT%{_libdir}/httpd/modules
install -m 755 build-apache/libs/libphp5.so $RPM_BUILD_ROOT%{_libdir}/httpd/modules

# install the ZTS DSO
install -m 755 build-zts/libs/libphp5.so $RPM_BUILD_ROOT%{_libdir}/httpd/modules/libphp5-zts.so

# install the ZTS DSO
install -m 755 build-zts/libs/libphp5.so $RPM_BUILD_ROOT%{_httpd_moddir}/libphp5-zts.so

# Apache config fragment
%if "%{_httpd_modconfdir}" == "%{_httpd_confdir}"
# Single config file with httpd < 2.4
install -D -m 644 %{SOURCE9} $RPM_BUILD_ROOT%{_httpd_confdir}/php.conf
cat %{SOURCE10} >>$RPM_BUILD_ROOT%{_httpd_confdir}/php.conf
cat %{SOURCE1} >>$RPM_BUILD_ROOT%{_httpd_confdir}/php.conf
%else
# Dual config file with httpd >= 2.4
install -D -m 644 %{SOURCE9} $RPM_BUILD_ROOT%{_httpd_modconfdir}/10-php.conf
install -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_httpd_confdir}/php.conf
%endif

install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/php.d
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d
install -m 755 -d $RPM_BUILD_ROOT%{_localstatedir}/lib/php
install -m 700 -d $RPM_BUILD_ROOT%{_localstatedir}/lib/php/session

# PHP-FPM stuff
# Log
install -m 755 -d $RPM_BUILD_ROOT%{_localstatedir}/log/php-fpm
install -m 755 -d $RPM_BUILD_ROOT%{_localstatedir}/run/php-fpm
# Config
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.d
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.conf
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.d/www.conf
mv $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.conf.default .
# tmpfiles.d
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/tmpfiles.d
install -m 644 php-fpm.tmpfiles $RPM_BUILD_ROOT%{_sysconfdir}/tmpfiles.d/php-fpm.conf
# install systemd unit files and scripts for handling server startup
install -m 755 -d $RPM_BUILD_ROOT%{_unitdir}
install -m 644 %{SOURCE6} $RPM_BUILD_ROOT%{_unitdir}/
# LogRotate
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -m 644 %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/php-fpm
# Environment file
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -m 644 %{SOURCE8} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/php-fpm
# Fix the link
(cd $RPM_BUILD_ROOT%{_bindir}; ln -sfn phar.phar phar)

# Generate files lists and stub .ini files for each subpackage
for mod in pgsql mysql mysqli odbc ldap snmp xmlrpc imap \
    mysqlnd mysqlnd_mysql mysqlnd_mysqli pdo_mysqlnd \
    mbstring gd dom xsl soap bcmath dba xmlreader xmlwriter \
    pdo pdo_mysql pdo_pgsql pdo_odbc pdo_sqlite json %{zipmod} \
    sqlite3  interbase pdo_firebird \
    enchant phar fileinfo intl opcache \
    mcrypt tidy pdo_dblib mssql pspell curl wddx \
    posix sysvshm sysvsem sysvmsg recode; do
    cat > $RPM_BUILD_ROOT%{_sysconfdir}/php.d/${mod}.ini <<EOF
; Enable ${mod} extension module
extension=${mod}.so
EOF
    cat > $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/${mod}.ini <<EOF
; Enable ${mod} extension module
extension=${mod}.so
EOF
    cat > files.${mod} <<EOF
%attr(755,root,root) %{_libdir}/php/modules/${mod}.so
%attr(755,root,root) %{_libdir}/php-zts/modules/${mod}.so
%config(noreplace) %attr(644,root,root) %{_sysconfdir}/php.d/${mod}.ini
%config(noreplace) %attr(644,root,root) %{_sysconfdir}/php-zts.d/${mod}.ini
EOF
done

# The dom, xsl and xml* modules are all packaged in php-xml
cat files.dom files.xsl files.xml{reader,writer} files.wddx > files.xml

# The mysql and mysqli modules are both packaged in php-mysql
cat files.mysqli >> files.mysql
# mysqlnd
cat files.mysqlnd_mysql \
    files.mysqlnd_mysqli \
    files.pdo_mysqlnd \
    >> files.mysqlnd

# Split out the PDO modules
cat files.pdo_dblib >> files.mssql
cat files.pdo_mysql >> files.mysql
cat files.pdo_pgsql >> files.pgsql
cat files.pdo_odbc >> files.odbc
cat files.pdo_firebird >> files.interbase

# sysv* and posix in packaged in php-process
cat files.sysv* files.posix > files.process

# Package sqlite3 and pdo_sqlite with pdo; isolating the sqlite dependency
# isn't useful at this time since rpm itself requires sqlite.
cat files.pdo_sqlite >> files.pdo
cat files.sqlite3 >> files.pdo

# Package json, zip, curl, phar and fileinfo in -common.
cat files.json files.curl files.phar files.fileinfo > files.common
%if %{with_zip}
cat files.zip >> files.common
%endif

# The default Zend OPcache blacklist file
install -m 644 %{SOURCE51} $RPM_BUILD_ROOT%{_sysconfdir}/php.d/opcache-default.blacklist
install -m 644 %{SOURCE51} $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/opcache-default.blacklist
sed -e '/blacklist_filename/s/php.d/php-zts.d/' \
    -i $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/opcache.ini

# Install the macros file:
install -d $RPM_BUILD_ROOT%{_sysconfdir}/rpm
sed -e "s/@PHP_APIVER@/%{apiver}/" \
    -e "s/@PHP_ZENDVER@/%{zendver}/" \
    -e "s/@PHP_PDOVER@/%{pdover}/" \
    < %{SOURCE3} > macros.php
install -m 644 -c macros.php \
           $RPM_BUILD_ROOT%{_sysconfdir}/rpm/macros.php

# Remove unpackaged files
rm -rf $RPM_BUILD_ROOT%{_libdir}/php/modules/*.a \
       $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/*.a \
       $RPM_BUILD_ROOT%{_bindir}/{phptar} \
       $RPM_BUILD_ROOT%{_datadir}/pear \
       $RPM_BUILD_ROOT%{_libdir}/libphp5.la

# Remove irrelevant docs
rm -f README.{Zeus,QNX,CVS-RULES}

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
rm files.* macros.php

%post fpm
if [ $1 = 1 ]; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun fpm
if [ $1 = 0 ]; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable php-fpm.service >/dev/null 2>&1 || :
    /bin/systemctl stop php-fpm.service >/dev/null 2>&1 || :
fi

%postun fpm
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart php-fpm.service >/dev/null 2>&1 || :
fi

# Handle upgrading from SysV initscript to native systemd unit.
# We can tell if a SysV version of php-fpm was previously installed by
# checking to see if the initscript is present.
%triggerun fpm -- php-fpm
if [ -f /etc/rc.d/init.d/php-fpm ]; then
    # Save the current service runlevel info
    # User must manually run systemd-sysv-convert --apply php-fpm
    # to migrate them to systemd targets
    /usr/bin/systemd-sysv-convert --save php-fpm >/dev/null 2>&1 || :

    # Run these because the SysV package being removed won't do them
    /sbin/chkconfig --del php-fpm >/dev/null 2>&1 || :
    /bin/systemctl try-restart php-fpm.service >/dev/null 2>&1 || :
fi

%post embedded -p /sbin/ldconfig
%postun embedded -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/httpd/modules/libphp5.so
%{_libdir}/httpd/modules/libphp5-zts.so
%attr(0770,root,apache) %dir %{_localstatedir}/lib/php/session
%config(noreplace) %{_httpd_confdir}/php.conf
%if "%{_httpd_modconfdir}" != "%{_httpd_confdir}"
%config(noreplace) %{_httpd_modconfdir}/10-php.conf
%endif
%{contentdir}/icons/php.gif

%files common -f files.common
%defattr(-,root,root)
%doc CODING_STANDARDS CREDITS EXTENSIONS LICENSE NEWS README*
%doc Zend/ZEND_* TSRM_LICENSE regex_COPYRIGHT
%doc php.ini-*
%config(noreplace) %{_sysconfdir}/php.ini
%dir %{_sysconfdir}/php.d
%dir %{_sysconfdir}/php-zts.d
%dir %{_libdir}/php
%dir %{_libdir}/php/modules
%dir %{_libdir}/php-zts
%dir %{_libdir}/php-zts/modules
%dir %{_localstatedir}/lib/php
%dir %{_datadir}/php

%files cli
%defattr(-,root,root)
%{_bindir}/php
%{_bindir}/php-cgi
%{_bindir}/phar.phar
%{_bindir}/phar
# provides phpize here (not in -devel) for pecl command
%{_bindir}/phpize
%{_mandir}/man1/phar.1*
%{_mandir}/man1/phar.phar.1*
%{_mandir}/man1/php.1*
%{_mandir}/man1/php-cgi.1*
%{_mandir}/man1/phpize.1*
%doc sapi/cgi/README* sapi/cli/README

%files fpm
%defattr(-,root,root)
%doc php-fpm.conf.default
%config(noreplace) %{_sysconfdir}/php-fpm.conf
%config(noreplace) %{_sysconfdir}/php-fpm.d/www.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/php-fpm
%config(noreplace) %{_sysconfdir}/sysconfig/php-fpm
%config(noreplace) %{_sysconfdir}/tmpfiles.d/php-fpm.conf
%{_unitdir}/php-fpm.service
%{_sbindir}/php-fpm
%dir %{_sysconfdir}/php-fpm.d
# log owned by apache for log
%attr(770,apache,root) %dir %{_localstatedir}/log/php-fpm
%dir %{_localstatedir}/run/php-fpm
%{_mandir}/man8/php-fpm.8*
%{_datadir}/fpm/status.html

%files devel
%defattr(-,root,root)
%{_bindir}/php-config
%{_bindir}/zts-php-config
%{_bindir}/zts-phpize
# usefull only to test other module during build
%{_bindir}/zts-php
%{_includedir}/php
%{_includedir}/php-zts
%{_libdir}/php/build
%{_libdir}/php-zts/build
%{_mandir}/man1/php-config.1*
%config %{_sysconfdir}/rpm/macros.php

%files embedded
%defattr(-,root,root,-)
%{_libdir}/libphp5.so
%{_libdir}/libphp5-%{version}.so

%files pgsql -f files.pgsql

%files mysql -f files.mysql

%files odbc -f files.odbc

%files imap -f files.imap

%files ldap -f files.ldap

%files snmp -f files.snmp

%files xml -f files.xml

%files xmlrpc -f files.xmlrpc

%files mbstring -f files.mbstring

%files gd -f files.gd
%defattr(-,root,root,-)
%doc gd_README

%files soap -f files.soap

%files bcmath -f files.bcmath

%files dba -f files.dba

%files pdo -f files.pdo

%files mcrypt -f files.mcrypt

%files tidy -f files.tidy

%files mssql -f files.mssql

%files pspell -f files.pspell

%files intl -f files.intl

%files process -f files.process

%files recode -f files.recode

%files interbase -f files.interbase

%files enchant -f files.enchant

%files mysqlnd -f files.mysqlnd

%files opcache -f files.opcache
%config(noreplace) %{_sysconfdir}/php.d/opcache-default.blacklist
%config(noreplace) %{_sysconfdir}/php-zts.d/opcache-default.blacklist

%changelog
* Wed May 21 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.5.12-1m)
- update to 5.5.12
- [SECURITY] CVE-2014-0185

* Tue Apr  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.11-1m)
- [SECURITY] CVE-2013-7345
- update to 5.5.11

* Mon Mar 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.5.10-2m)
- rebuild against icu-52

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.10-1m)
- [SECURITY] CVE-2013-7327 CVE-2014-1943 CVE-2014-2270
- update to 5.5.10

* Sat Dec 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.7-1m)
- [SECURITY] CVE-2013-6420
- update to 5.5.7

* Sun Sep 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.4-1m)
- update to 5.5.4

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.2-1m)
- [SECURITY] CVE-2013-4248 CVE-2013-4718
- update to 5.5.2

* Tue Jun 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.16-1m)
- [SECURITY] CVE-2013-2110
- update to 5.4.16

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.14-1m)
- [SECURITY] CVE-2012-1635 CVE-2013-1643 CVE-2013-1824 (all issues are fixed in 5.4.13)
- update to 5.4.14

* Thu Jan 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.11-1m)
- update to 5.4.11

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.10-1m)
- update to 5.4.10
- rebuild against httpd-2.4.3

* Wed Nov 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.9-1m)
- [SECURITY] Fixed bug #63447 (max_input_vars doesn't filter variables when mbstring.encoding_translation = On)
- update to 5.4.9

* Sat Oct 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.8-1m)
- see Changelog - http://www.php.net/ChangeLog-5.php
- update to 5.4.8

* Thu Oct 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.7-1m)
- see Changelog - http://www.php.net/ChangeLog-5.php
- update to 5.4.7

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.5-2m)
- rebuild against pcre-8.31

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.5-1m)
- [SECURITY] CVE-2012-2688
- update to 5.4.5

* Fri Jun 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.4-1m)
- [SECURITY] CVE-2012-2143 CVE-2012-2386
- update to 5.4.4

* Sat Jun  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.3-2m)
- [SECURITY] CVE-2012-2143

* Thu May 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.3-1m)
- [SECURITY] CVE-2012-2311 CVE-2012-2329
- update to 5.4.3

* Mon May  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- [SECURITY] CVE-2012-1823
- but, still remain CVE-2012-2311
- update to 4.5.2

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.1-1m)
- [SECURITY] CVE-2012-1172
- update to 5.4.1

* Sun Feb 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.10-2m)
- [SECURITY] CVE-2012-0831

* Fri Feb  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.10-1m)
- [SECURITY] CVE-2012-0830
- update to 5.3.10

* Wed Jan 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.9-1m)
- [SECURITY] CVE-2011-4566 CVE-2011-4885
- update to 5.3.9

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.3.8-2m)
- rebuild against net-snmp-5.7.1

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.8-1m)
- fixed serious crypt() bug (#55439)
- update to 5.3.8

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linix.org>
- (5.3.7-1m)
- [SECURITY] CVE-2011-1148 CVE-2011-1938 CVE-2011-2202 CVE-2011-2483
- update to 5.3.7

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.3.6-6m)
- rebuild against icu-4.6

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.3.6-5m)
- rebuild against net-snmp-5.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.3.6-4m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.6-3m)
- rebuild against net-snmp-5.6.1

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.6-2m)
- rebuild against mysql-5.5.10

* Fri Mar 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.6-1m)
- [SECURITY] CVE-2011-0421 CVE-2011-0708 CVE-2011-1092 CVE-2011-1153
- [SECURITY] CVE-2011-1464 CVE-2011-1466 CVE-2011-1467 CVE-2011-1468
- [SECURITY] CVE-2011-1469 CVE-2011-1470 CVE-2011-1471
- update to 5.3.6

* Sat Jan  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.5-1m)
- [SECURITY] CVE-2010-4645
- update to 5.3.5

* Sat Dec 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.4-1m)
- [SECURITY] CVE-2006-7243 CVE-2010-2950 CVE-2010-3436 CVE-2010-3709
- [SECURITY] CVE-2010-3710 CVE-2010-4150
- [SECURITY] http://www.php.net/archive/2010.php#id2010-12-10-1
- update to 5.3.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.3.3-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.3.3-3m)
- rebuild against gmp-5.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.3.3-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3.3-1m)
- [SECURITY] CVE-2010-2531 CVE-2010-0397 CVE-2010-2225
- [SECURITY] see http://www.php.net/archive/2010.php#id2010-07-22-2
- and many other security and bug fixes
- update to 5.3.3

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.3.2-1m)
- [SECURITY] CVE-2010-2093 CVE-2010-2094
- update to 5.3.2 (sync Fedora)

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.13-4m)
- rebuild against readline6

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.13-3m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.13-2m)
- rebuild against openssl-1.0.0

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.13-1m)
- [SECURITY] CVE-2010-1128 CVE-2010-1129 CVE-2010-1130
- [SECURITY] CVE-2010-0397
- update to 5.2.13
- import a security patch from Debian lenny (5.2.6.dfsg.1-1+lenny8)

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.12-3m)
- rebuild against net-snmp-5.5

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.12-2m)
- rebuild against db-4.8.26

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.12-1m)
- [SECURITY] CVE-2009-3557 CVE-2009-3558 CVE-2009-4017
- [SECURITY] CVE-2009-4143 CVE-2009-4142
- drop Patch100, already aplied to upstream

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.11-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.11-2m)
- [SECURITY] CVE-2009-3546
- import a security patch from Mandriva (3:5.2.9-6.3mdv2009.1)

* Sun Jun 28 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.2.11-1m)
- upgrade to 5.2.11
- [SECURITY] CVE-2009-2626 CVE-2009-4018
- [SECURITY] CVE-2008-3291 CVE-2009-3292 CVE-2009-3293 CVE-2009-3294

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.10-2m)
- exclude strict-session patch for 5.2.10. it seems broken.

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.10-1m)
- [SECURITY] CVE-2009-2687
- update to 5.2.10

* Mon Jun  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.9-4m)
- modify spec for phpize using new libtool

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.9-3m)
- drop odbc patch

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.9-2m)
- rebuild against unixODBC-2.2.14
- import aclocal workaround from Fedora 11 (5.2.9-2)

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.9-1m)
- [SECURITY] CVE-2008-5814 CVE-2009-0754 CVE-2009-1271 CVE-2009-1272
- update to 5.2.9

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.8-7m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.8-6m)
- rebuild against mysql-5.1.32

* Wed Feb 4 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.2.8-5m)
- add strict session patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.8-4m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.8-3m)
- [SECURITY] CVE-2008-5498
- apply Patch100 from upstream

* Fri Jan 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.8-2m)
- rebuild against uw-imap-2007e

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.8-1m)
- [SECURITY] CVE-2008-2829 CVE-2008-5557 CVE-2008-5624
- [SECURITY] CVE-2008-5625 CVE-2008-5658 CVE-2008-5844
- [SECURITY] CVE-2009-0754
- update to 5.2.8
-- update Patch1,2,5,50 for fuzz=0

* Mon Nov 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.6-4m)
- [SECURITY] CVE-2008-2371 CVE-2008-2665 CVE-2008-2666
- [SECURITY] CVE-2008-3658 CVE-2008-3659 CVE-2008-3660
- import security patches (Patch100-104) from Gentoo
  http://home.hoffie.info/php-patchset-5.2.6-r8.tar.bz2

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.2.6-3m)
- rebuild against db4-4.7.25-1m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.6-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May 05 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.2.6-1m)
- [SECURITY] 
- update to 5.2.6
- Fixed possible stack buffer overflow in the FastCGI SAPI identified by Andrei Nigmatulin (CVE-2008-2050).
- Fixed integer overflow in printf() identified by Maksymilian Aciemowicz (CVE-2008-1384).
- Fixed security issue detailed in CVE-2008-0599 identified by Ryan Permeh.
- Fixed a safe_mode bypass in cURL identified by Maksymilian Arciemowicz.
- Properly address incomplete multibyte chars inside escapeshellcmd() identified by Stefan Esser (CVE-2008-2051).
- Upgraded bundled PCRE to version 7.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.5-5m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2.5-4m)
- rebuild against openlda-2.4.8

* Mon Dec  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.5-3m)
- php-cli Provides: %%{_bindir}/php

* Tue Nov 20 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.2.5-2m)
- fix php-cgi binay. it was CLI binary.

* Tue Nov 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.5-1m)
- [SECURITY] CVE-2007-4887 CVE-2007-4783 CVE-2007-4840 CVE-2008-2107 CVE-2008-2108
- update to 5.2.5
- and many other bugs are fixed, see http://www.php.net/ChangeLog-5.php#5.2.5
- remove php-5.2.2-pdosym.patch

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.2.4-2m)
- rebuild against db4-4.6.21

* Sun Sep 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.4-1m)
- [SECURITY] CVE-2007-3378 CVE-2007-3996 CVE-2007-3997 CVE-2007-3998 CVE-2007-4652
- [SECURITY] CVE-2007-4657 CVE-2007-4658 CVE-2007-4659 CVE-2007-4662 CVE-2007-4670
- http://www.php.net/releases/5_2_4.php
- import Patch23 and Patch24 from Fedora devel
- sync Patch1, Patch3, Patch21, Patch31, Patch50 with Fedora devel

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.2.3-2m)
- rebuild against net-snmp

* Fri Jun  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.2.3-1m)
- [SECURITY] http://www.php.net/releases/5_2_2.php
- [SECURITY] http://www.php.net/releases/5_2_3.php
- sync fc-devel php-5.0.4-tests-dashn.patch, php.ini
- remove Patch9 php-5.2.1-strreplace.patch

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.1-2m)
- move Obsoletes for yum upgrade

* Mon Apr  9 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2.1-1m)
- sync fc7

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.1.6-4m)
- rebuild against postgresql-8.2.3

* Sat Nov 18 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.1.6-3m)
- rebuild against db-4.5

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.1.6-2m)
- rebuild against curl-7.16.0
- - import php-5.1.6-curl716.patch from Fedora Core

* Fri Sep  1 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.6-1m)
- version up 5.1.6

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.1.4-12m)
- rebuild against expat-2.0.0-1m

* Wed Aug  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.4-11m)
- rebuild against net-snmp 5.3.1.0

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.4-10m)
- rebuild against readline-5.0

* Sun Jun 25 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.1.4-9m)
- add BuildPrereq: sqlite2-devel
- revised spec

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.4-8m)
- rebuild against sqlite-3.3.5

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.4-7m)
- delete duplicate file %%{_mandir}/man1/php.1*
- delete duplicate dir %%dir %%{_sysconfdir}/php

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.4-6m)
- rebuild against mm-1.4.0

* Tue May 23 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.4-5m)
- rebuild against rpm-4.4.2

* Mon May 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.1.4-4m)
- rebuild against mm-1.4.0-1m

* Wed May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.4-3m)
- rebuild against mysql 5.0.22-1m
- enable mysqli module

* Wed May 17 2006 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (5.1.4-2m)
- change  md5sum_src0 only

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.4-1m)
- Update to 5.1.4

* Thu May 04 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (5.1.2-7m)
- rebuild against httpd-2.2.2

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.2-6m)
- rebuild against openssl-0.9.8a

* Wed Feb 2 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.2-5m)
- apply strict session patches

* Fri Jan 27 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.2-4m)
- apply cvs patches

* Fri Jan 27 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.2-3m)
- add patch that adds allow_url_include php.ini option.

* Thu Jan 26 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.2-2m)
- more secure php.ini-web settings

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.2-2m)
- rebuild against openldap-2.3.11

* Fri Jan 13 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.2-1m)
- SECURITY FIX
- # HTTP Response Splitting has been addressed in ext/session and in the header() function.
- # Fixed format string vulnerability in ext/mysqli.
- # Fixed possible cross-site scripting problems in certain error conditions.

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.1-5m)
- rebuild against db4.3

* Sun Dec 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.1.1-4m)
- enable lib64 arch
- remove phpize patch

* Sat Dec 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.1-3m)
- move phpize patch (Patch11) into lib64 build section for resolve ix86 build problem

* Sat Dec 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.1-2m)
- add lib64, phpize patch

* Fri Dec 9 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.1.1-1m)
- update to PHP 5.1.1
- fam, yp, dbx modules are removed
- patches will be added later
- beware, this is jirai version

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (5.0.5-11m)
- rebuild against postgresql-8.1.0

* Mon Nov 7 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (5.0.5-10m)
- use system sqlite lib.
- apply CVS diff to Zend.

* Mon Nov 7 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (5.0.5-9m)
- use system gd lib.

* Wed Nov 2 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (5.0.5-8m)
- SECURITY: add patch protect $GLOBALS (CRITICAL)
- other minor issues such as missing open_basedir is not addressed

* Mon Oct 24 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (5.0.5-7m)
- import patches from fedora devel

* Sun Oct 23 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (5.0.5-6m)
- disable mmap support. mmap support is broken.

* Thu Oct 20 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.0.5-5m)
- build dir fix is not lib64 specific..

* Thu Oct 6 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.5-3m)
- increase memory limit for CLI. kossori

* Tue Sep 27 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.0.5-3m)
- lib64 build dir fix.

* Thu Sep 15 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.5-2m)
- fix build file path
- remove xmlrpc-epi dependency

* Mon Sep 12 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.5-1m)
- bugfix release (XML_RPC security fix was done in 5.0.4-6m)

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (5.0.4-9m)
- rebuild against for MySQL-4.1.14

* Wed Jul 20 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.4-8m)
- change web sapi error from dir from /var/log/ to /var/log/httpd
- for selinux

* Wed Jul 20 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.4-7m)
- fix php.ini file extension_dir for x86_64
- fix php-snmp initialization

* Tue Jul 19 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.4-6m)
- security fix. PEAR XML_PRC verup to 1.3.3
- http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-1921
- http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-2106
- http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-2116

* Thu Jul 14 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.4-5m)
- move session state file to /var/lib/php/session to make session
- work with selinux

* Fri May 20 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (5.0.4-4m)
- add patch (mbstring-jpr-25298-20050414.patch.diff.gz)
- fix mojibake.

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (5.0.4-3m)
- rebuild against postgresql-8.0.2.

* Tue Apr  5 2005 Toru Hoshina <t@momonga-linux.org>
- (5.0.4-2m)
- revised lib64 patch.

* Tue Apr  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.0.4-1m)
- up to 5.0.4

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (5.0.3-8m)
- rebuild against libtermcap and ncurses

* Wed Feb  14 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.3-7m)
- revise php-cgi.conf so that module/cgi version may co-exist

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (5.0.3-6m)
- large file support. rebuild against httpd 2.0.53.

* Mon Feb 7 2005 Yasuo OHgaki<yohgaki@momonga-linux.org>
- (5.0.3-5m)
- fix missing icon
- revise php.conf and php-cgi.conf

* Mon Feb 7 2005 Toru Hoshina <t@momonga-linux.org>
- (5.0.3-4m)
- enable x86_64.

* Sun Feb 6 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.3-3m)
- add patch that stablizes encoding detection


* Tue Jan 18 2005 zunda <zunda at freeshell.org>
- (5.0.3-2m)
- add Requires: tmpwatch for /etc/cron.daily/php-tmpwatch

* Fri Dec 17 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.0.3-1m)
- up to 5.0.3
- [SECURITY] http://www.hardened-php.net/advisories/012004.txt

* Sun Oct 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.2-4m)
- rebuild against curl-7.12.2

* Tue Oct 12 2004 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (5.0.2-3m)
- fix php-cgi.ini and php-cli.ini

* Tue Oct 12 2004 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (5.0.2-2m)
- add missing php-cgi.conf %file

* Tue Oct 12 2004 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (5.0.2-2m)
- make always require php-cli package to satisfy module dependency among
- php, php-cil, php-cgi installations.
- install php-cgi.conf.dist for php-cgi.

* Tue Oct 12 2004 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (5.0.2-1m)
- Update to PHP5
- Update manuals.
- Configuration notice
  Configuration is based on php.ini-recommended.
  Script error will not displayed but logged to /var/log/php-error.log
  PHP4 compatibility mode if off by default.
     zend.ze1_compatibility_mode = Off
  Call time pass by reference is off by default.
  Short open tag (<?) is disabled by default. Use <?php instead.
     short_open_tag = Off
  All PECL modules (e.g. kakashi, chasen) are disabled for now.
  ming support is disabled for now. (Need newer jaming(ming) but it's not released yet)
  Session module uses SHA1 instead of MD5.
  Session data is stored to /var/state/php.
  Error reporting E_ALL by default.
  DOM support is built in. (php-domxml is obsolete)
  Added ini file support for dba module.
  Added fam, tidy module support.
  SQLite support is built in. UTF-8 support is enabled.
  MySQLi module is not included. (MySQL 4.1 is required for mysqli)
  Crack module is removed. (It may be added as PECL module)
  Enabled GD JIS support
- List of unsupported modules
   FrontBase
   FDF
   Informix
   Ingres II
   InterBase
   IRCG
   MCVE
   msession
   msql
   mssql
   mysqli
   Oracle-OCI7
   Oracle-OCI8
   Adabas D
   SAP DB
   Solid
   IBM DB2
   Empress
   Birdstep
   Easysoft OOB
   DBMaker
   Ovrimos SQL
   Verisign Payflow Pro
   Hyperwave

* Mon Sep 27 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.3.9-1m)
- update to 4.3.9 - bugfix release

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.8-2m)
- stop service

* Wed Jul 14 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.8-1m)
- security and bugfix release

* Thu Jul 8 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.7-2m)
- exec_dir security fix.

* Mon Jul 5 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.7-1m)
- update to 4.3.7 - bug fix release
- update html manual

* Sun Jun 27 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.6-4m)
- rebuild against db4

* Thu May  21 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.6-3m)
- work around httpd sig HUP problem by loading php4 module in early stage.
- made cli binay be /usr/bin/php always.
- create pear package.
- added man php page.
- enable pcntl for module and CLI build.

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (4.3.6-2m)
- rebuild against ncurses 5.3.

* Wed Apr 16 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.6-1m)
- bug fix release

* Wed Apr 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.3.5-3m)
- rebuild against namazu-2.0.13

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.3.5-2m)
- rebuild against for libxml2-2.6.8

* Fri Mar 26 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.5-1m)
- update to 4.3.5

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (4.3.4-6m)
- revised spec for rpm 4.2.

* Sun Dec 28 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.3.4-5m)
- rebuild against libxslt

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.3.4-4m)
- rebuild against postgresql-7.4.1

* Wed Nov 19 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.3.4-3m)
- append "mime_magic" sub package.

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.4-2m)
- rebuild against openldap

* Tue Nov  4 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.3.4-1m)
- update to 4.3.4
- append new package. (php-ming)
- add php-apache2hander.ini for apache2hander.
- apache2hander is default. (modify php.conf)
- php-cgi.ini was changed from symlink to regular file.

* Thu Oct 9 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.1-6m)
- add missing build files

* Tue Oct 7 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- fix license. remove unneeded patches.

* Mon Apr 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.1-5m)
- rebuild against t1lib 5.0.0

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.1-4m)
- rebuild against zlib 1.1.4-5m

* Mon Mar 24 2003 Yasuo Ohgaki <yohgaki@momonga.linux>
- (4.3.1-3m)
- fix php.ini files
- install php.ini-dist and php.ini-recommended comes with distribution

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.3.1-2m)
  rebuild against openssl 0.9.7a

* Sat Feb 22 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (4.3.1-1m)
- serious security vulnerability in the CGI SAPI of PHP version 4.3.0.
  see. http://www.php.net/release_4_3_1.php

* Fri Feb 21 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.0-4m)
- modified php.ini
- without ndbm support

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.3.0-3m)
- remove Require: pspell

* Fri Feb 14 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.0-2m)
- merge to HEAD

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.3-8m)
- rebuild against for aspell

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.2.3-7m)
  rebuild against libgdbm

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.2.3-6m)
- rebuild against for gd

* Sun Jan 5 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.3.0-1m)
- build with Apache2. Apache1.3 is still able to used by changing
  %define use_apache_1_3_x 0 to 1. php.ini will not be modified
  with Apache1.3
- configuration files are located like rawhide except php
  files are stored under /etc/php directory
- moved php module loading settings to /etc/php/php.d
- cleanup php.spec and removed most debug options
- devided sysv modules into sysvsem and sysvshm module
- separated bcmath module to php-bcmath*.rpm
- added shmop module. php-shmop*.rpm
- added iconv module. php-iconv*.rpm
- added SJIS and other multibyte encoding support
- added CLI(Command Line Interface)
- added --disable-rpath for CGI/CLI
- added process control module (pcntl) to CGI/CLI
- added dio module by defualt.
- use alternatives for CLI and CGI selection
- removed --enable-safe-mode for CGI
- removed --enable-mcal. It was not built anyway.
  (--with-mcal is the right option)
- removed libphp4.a install
- TODO:
    fix broken shared modules - namazu and session_pgsql
    fix alternative usage
    remove modules that are not used much e.g. wddx
    add some useful modules from PECL. e.g. apd, apc, mailparse
    add embed (both static and shared) support
    improve and test Apache1.3 build. it should at least be able to built with it
    require PostgreSQL 7.3.0 >=
    add more patches e.g. apahce2 304 header patch, etc

* Sun Oct 6 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.2.3-5m)
- fix gcc3 build problem(php-gcc3.patch)

* Sat Sep 7 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.3-4m)
- fix POST/GET/COOKIE var handling finally

* Sat Sep 7 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.3-3m)
- fix POST/GET/COOKIE var handling actually

* Sat Sep 7 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.3-2m)
- fix POST/GET/COOKIE var handling

* Sat Sep 7 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.3-1m)
- update to 4.2.3

* Wed Aug 21 2002 Kikutani makoto <poo@momonga-linux.org>
- (4.2.2-20m)
- php-4.2.1-snmp.patch

* Sat Aug 10 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-19m)
- actually enable php-soap extension.

* Sat Aug 10 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-18m)
- add php-soap extension that provide easy to use SOAP
  client/server API for PHP.

* Sat Aug 10 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-17m)
- remove --enable-magic-quote. It's off by default in php.ini
  for a long time.

* Fri Aug 2 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-16m)
- enable mhash and mcrypt module
- xmlrpc module is enabled as built-in module. xmlrpc-epi package
  is required for PHP from now on.
- fix session_pgsql rpm
- set session entropy file and unset session trans sid flag for
  security reason

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-15m)
- disable mhash also. mhash, mcrypt, xmlrpc will be enabled
  when I finish testing...

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-14m)
- disable xmlrpc for now. (forgot that I was testing this still)

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-13m)
- add mhash and xmlrpc module.

* Mon Jul 29 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.2.2-12m)
  wu-imap included.

* Sun Jul 28 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (4.2.2-11m)
- php-tmpwatch comes back from the Attic.

* Sat Jul 27 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-10m)
- add session_pgsql
- add NoSource rquired
- enable mm session save handler only for apache sapi

* Thu Jul 23 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.2.2-9m)
- update to 4.2.2

* Wed May 22 2002 Masaru Sato <masachan@kondara.org>
- (4.2.1-8k)
- enalbe t1lib support in gd extention, again
- enable bcmath, exif and cracklib support
- enable ncurses support, only /usr/bin/php
- add enable-discard-path option, only /usr/bin/php
- update manual (en=2002-05-17,ja=2002-05-06)
- fix requires bzip2 tag

* Mon May 20 2002 Toru Hoshina <t@kondara.org>
- (4.2.1-6k)
- iconv template was different.

* Thu May 16 2002 Masaru Sato <masachan@kondara.org>
- (4.2.1-4k)
- rebuild against postgresql-7.2.1

* Thu May 16 2002 Masaru Sato <masachan@kondara.org>
- (4.2.1-2k)
- up to 4.2.1
- move PEAR directory from "/usr/share/php" to "/usr/share/php/pear"

* Tue May 14 2002 Masaru Sato <masachan@kondara.org>
- (4.1.2-10k)
- enable FreeType2 support, again
- use gcc 2.96, again
- Sablotron(XSLT) support, default "On"
- enable mm support for session storage
- fixed configure option enable-inline-optimization,disable-debug
- delete BuildPrereq t1lib-devel
- delete Requires libjepg,libpng from php-standalone

* Tue Apr 30 2002 Toru Hoshina <t@kondara.org>
- (4.1.2-8k)
- fixed Provides tag.

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (4.1.2-6k)
- rebuild against libpng 1.2.2.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (4.1.2-4k)
- rebuild against new environment.

* Mon Mar 18 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (4.1.2-2k)
- update to 4.1.2

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (4.1.1-18k)
- rebuild against for db3,4

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (4.1.1-16k)
- rebuild against for ucd-snmp 4.2.3

* Wed Feb  6 2002 Masaru Sato <masachan@kondara.org>
- (4.1.1-14k)
- merge from STABLE_2_1
-   modify php.ini (extension=imap.so and extension=ldap.so)
-   rebuild against imap-2000c-14k
-   see [Kondara-devel.ja:06145]
- gd extension to static link and remove gd extension truetype font support

* Tue Jan 29 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (4.1.1-10k)
- php-gd subpackage was revived.
- A configure option "--with-gd" was changed to "--with-gd=shared"
- This release built by gcc_2_95_3. ("%define use_gcc_2_95_3" is turned on)

* Wed Jan 23 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (4.1.1-8k)
  "/etc/cron.daily/php-tmpwatch" permition set to 0755.

* Fri Jan 18 2002 Masaru Sato <masachan@kondara.org>
- (4.1.1-6k)
- php.ini based php.ini-dist
- make imap.so, move to compile() function
- rebuild against gcc-2.96

* Fri Jan 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.1.1-4k)
  The "Obsoletes tag(php-misc, php-readline, php-gd)" was erased sub packages.
  It is because upgrade installation goes wrong.

* Mon Jan  7 2002 Masaru Sato <masachan@kondara.org>
- (4.1.1-2k)
- up to 4.1.1
- enable calendar, chasen, curl, gmp, kakasi, namazu and snmp extension
- bz2 extension to shared library
- gd and readline extension to static link
- php.ini based php.ini-recommended
- clean spec file

* Wed Nov 21 2001 Masaru Sato <masachan@kondara.org>
- (4.0.6-32k)
- modify use libtool in build_ext() function
- add ZVAL patch from rawhide php-4.0.6-9
- add PostgreSQL support patch from php-users ML, fix pg_last_notice() bug,
-   see http://ns1.php.gr.jp/pipermail/php-users/2001-October/003170.html

* Wed Nov 14 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-30k)
- rebuild against revised pspell.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (4.0.6-28k)
- nigittenu

* Wed Oct 31 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-24k)
- external MySQL configuration. [Kondara-devel.ja:05947]

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-22k)
- rebuild against postgresql 7.1.3-4k.
- add BuildPreReq.

* Sat Oct 27 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-20k)
- rebuild against postgresql 7.1.3.

* Mon Oct 22 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-18k)
- added Sablot support, it could be switched by 'sabloton' option.
- default setting is 'OFF'.

* Mon Oct 22 2001 Masaru Sato <masachan@kondara.org>
- (4.0.6-16k)
- rebuild against unixODBC 2.0.7
- fix build_ext() function, "-I/usr/include/freetype"
-   to "-I/usr/include/freetype2/freetype"
- add configure option "--with-bz2"
- add configure option "--with-xpm-dir"
- add configure option "--enable-mbstr-enc-trans"
- add configure option "--with-iconv=shared", make subpackage "php-iconv"
- add multibyte regex module and configure option "--enable-mbregex"
- add memlimit patch, This patch is php official
- add /var/state/php for session management tmp dir
- add tmpwatch config file for session management
- move safe_mode_exec_dir /usr/bin to /usr/lib/php4/bin
- spilt php-misc package to php-readline, php-gettext and php-sysv

* Fri Oct 19 2001 Masaru Sato <masachan@kondara.org>
- (4.0.6-14k)
- add sub packages dba, domxml, ftp, sockets and yp
- clean up spec file, fix bug

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-12k)
- rebuild against libpng 1.2.0.

* Wed Oct  3 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (4.0.6-10k)
- add --enable-versioning (for with php3)

* Fri Sep 14 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (4.0.6-8k)
- rebuild against for libxml2-2.4.4

* Thu Sep 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (4.0.6-6k)
- remove unixODBC support because httpd will not start with php-odbc.

* Wed Sep 12 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-4k)
- mysql, unixODBC support is turned on.

* Sat Aug 26 2001 Toru Hoshina <t@kondara.org>
- (4.0.6-2k)
- version up.
- merge from rawhide

* Fri Aug 10 2001 Tim Powers <timp@redhat.com>
- only english in php-manuals, space constraints :P

* Thu Aug  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- include %{_libdir}/%{name}/build instead of %{_libdir}/%{name}4/build (#51141)

* Mon Aug  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- add build deps on pam-devel, pspell-devel, gdbm-devel (#49878)
- add some conditional logic if %%{oracle} is defined (from Antony Nguyen)

* Mon Jul  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- don't obsolete subpackages we ended up not merging

* Mon Jul  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- cleanups
- add manuals in multiple languages (using ko instead of kr for Korean)
- merge all of the manuals into a single -manual subpackage
- use libtool to install binary files which libtool builds
- don't strip any binaries; let the buildroot policies take care of it

* Thu Jun 28 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.6 (preliminary)

* Mon Jun 25 2001 Nalin Dahyabhai <nalin@redhat.com>
- enable ttf in the build because the gd support needs it
- add -lfreetype to the LIBS for the same reason

* Wed Jun  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Wed May 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- actually use two source trees to build things
- add %%post and %%postun scriptlets to run ldconfig

* Tue May 15 2001 Nalin Dahyabhai <nalin@redhat.com>
- quote part of the AC_ADD_LIBRARY macro to make newer autoconf happy

* Mon May 14 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix error in %%install
- depend on the imap-devel which supplies linkage.c
- modify trigger to disable php versions less than 4.0.0 instead of 3.0.15
- enable DOM support via libxml2 (suggested by Sylvain Berg?)
- build the OpenSSL extension again

* Mon May  7 2001 Nalin Dahyabhai <nalin@redhat.com>
- enable pspell extensions
- update to 4.0.5

* Mon Apr 30 2001 Nalin Dahyabhai <nalin@redhat.com>
- build the ODBC extension

* Mon Apr 30 2001 Bill Nottingham <notting@redhat.com>
- build on ia64

* Fri Mar  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Fri Feb 23 2001 Nalin Dahyabhai <nalin@redhat.com>
- obsolete the old phpfi (PHP 2.x) package

* Thu Feb  8 2001 Nalin Dahyabhai <nalin@redhat.com>
- add a commented-out curl extension to the config file (part of #24933)
- fix the PEAR-installation-directory-not-being-eval'ed problem (#24938)
- find the right starting point for multipart form data (#24933)

* Tue Jan 30 2001 Nalin Dahyabhai <nalin@redhat.com>
- aaarrgh, the fix breaks something else, aaarrgh; revert it (#24933)
- terminate variable names at the right place (#24933)

* Sat Jan 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- tweak the fix some more

* Thu Jan 18 2001 Nalin Dahyabhai <nalin@redhat.com>
- extract stas's fix for quoting problems from CVS for testing
- tweak the fix, ask the PHP folks about the tweak
- tweak the fix some more

* Wed Jan 17 2001 Nalin Dahyabhai <nalin@redhat.com>
- merge mod_php into the main php package (#22906)

* Fri Dec 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- try to fix a quoting problem

* Wed Dec 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.4 to get a raft of bug fixes
- enable sockets
- enable wddx

* Fri Nov  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in updated environment

* Thu Nov  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- add more commented-out modules to the default config file (#19276)

* Wed Nov  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix not-using-gd problem (#20137)

* Tue Oct 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.3pl1 to get some bug fixes

* Sat Oct 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- build for errata

* Wed Oct 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.3 to get security fixes integrated
- patch around problems configuring without Oracle support
- add TSRM to include path when building individual modules

* Fri Sep  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment
- enable OpenSSL support

* Wed Sep  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.2, and move the peardir settings to configure (#17171)
- require %%{version}-%%{release} for subpackages
- add db2-devel and db3-devel prereqs (#17168)

* Wed Aug 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment (new imap-devel)

* Wed Aug 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix summary and descriptions to match the specspo package

* Wed Aug  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- hard-code the path to apxs in build_ext() (#15799)

* Tue Aug  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- add "." to the include path again, which is the default

* Wed Jul 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- enable PEAR and add it to the include path
- add the beginnings of a -devel subpackage

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jul  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweaks to post and postun from Bill Peck

* Thu Jul  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- fixes from Nils for building the MySQL client
- change back to requiring %{version} instead of %{version}-%{release}

* Sat Jul  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.1pl2
- enable MySQL client
- move the php.ini file to %{_sysconfdir}

* Fri Jun 30 2000 Nils Philippsen <nils@redhat.de>
- build_ext defines HAVE_PGSQL so pgsql.so in fact contains symbols
- post/un scripts tweak php.ini correctly now

* Thu Jun 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.0.1
- refresh manual

* Tue Jun 26 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild against new krb5 package

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild against new db3 package

* Sat Jun 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- Fix syntax error in post and preun scripts.
- Disable IMAP, LDAP, PgSql in the standalone version because it picks up
  the extensions.

* Fri Jun 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- Unexclude the Sparc arch.
- Exclude the ia64 arch until we get a working Postgres build.
- Stop stripping extensions as aggressively.
- Start linking the IMAP module to libpam again.
- Work around extension loading problems.
- Reintroduce file-editing post and preun scripts for the mod_php extensions
  until we come up with a better way to do it.

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- ExcludeArch: sparc for now

* Sun Jun  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- add Obsoletes: phpfi, because their content handler names are the same
- add standalone binary, rename module packages to mod_php
- FHS fixes

* Tue May 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- change license from "GPL" to "PHP"
- add URL: tag
- disable mysql support by default (license not specified)

* Mon May 22 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to PHP 4.0.0
- nuke the -mysql subpackage (php comes with a bundled mysql client lib now)

* Tue May 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- link IMAP module against GSS-API and PAM to get dependencies right
- change most of the Requires to Prereqs, because the post edits config files
- move the PHP *Apache* module back to the right directory
- fix broken postun trigger that broke the post
- change most of the postuns to preuns in case php gets removed before subpkgs

* Thu May 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuilt against new postgres libraries

* Tue May 09 2000 Preston Brown <pbrown@redhat.com>
- php3 .so modules moved to /usr/lib/php3 from /usr/lib/apache (was incorrect)

* Mon Apr 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- make subpackages require php = %{version} (bug #10671)

* Thu Apr 06 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.0.16

* Fri Mar 03 2000 Cristian Gafton <gafton@redhat.com>
- fixed the post script to work when upgrading a package
- add triggere to fix the older packages

* Tue Feb 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.0.15
- add build-time dependency for openldap-devel
- enable db,ftp,shm,sem support to fix bug #9648

* Fri Feb 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- add dependency for imap subpackage
- rebuild against Apache 1.3.12

* Thu Feb 24 2000 Preston Brown <pbrown@redhat.com>
- don't include old, outdated manual.  package one from the php distribution.

* Tue Feb 01 2000 Cristian Gafton <gafton@redhat.com>
- rebuild to fix dependency problem

* Fri Jan 14 2000 Preston Brown <pbrown@redhat.com>
- added commented out mysql module, thanks to Jason Duerstock
  (jason@sdi.cluephone.com). Uncomment to build if you have mysql installed.

* Thu Jan 13 2000 Preston Brown <pbrown@redhat.com>
- rely on imap-devel, don't include imap in src.rpm (#5099).
- xml enabled (#5393)

* Tue Nov 02 1999 Preston Brown <pborwn@redhat.com>
- added post/postun sections to modify httpd.conf (#5259)
- removed old obsolete faq and gif (#5260)
- updated manual.tar.gz package (#5261)

* Thu Oct 07 1999 Matt Wilson <msw@redhat.com>
- rebuilt for sparc glibc brokenness

* Fri Sep 24 1999 Preston Brown <pbrown@redhat.com>
- --with-apxs --> --with-apxs=/usr/sbin/apxs (# 5094)
- ldap support (# 5097)

* Thu Sep 23 1999 Preston Brown <pbrown@redhat.com>
- fix cmdtuples for postgresql, I had it slightly wrong

* Tue Aug 31 1999 Bill Nottingham <notting@redhat.com>
- subpackages must obsolete old stuff...

* Sun Aug 29 1999 Preston Brown <pbrown@redhat.com>
- added -DHAVE_PGCMDTUPLES for postgresql module (bug # 4767)

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- name change to php to follow real name of package
- fix up references to php3 to refer to php
- upgrade to 3.0.12
- fixed typo in pgsql postun script (bug # 4686)

* Mon Jun 14 1999 Preston Brown <pbrown@redhat.com>
- upgraded to 3.0.9
- fixed postgresql module and made separate package
- separated manual into separate documentation package

* Mon May 24 1999 Preston Brown <pbrown@redhat.com>
- upgraded to 3.0.8, which fixes problems with glibc 2.1.
- took some ideas grom Gomez's RPM.

* Tue May 04 1999 Preston Brown <pbrown@redhat.com>
- hacked in imap support in an ugly way until imap gets an official
  shared library implementation

* Fri Apr 16 1999 Preston Brown <pbrown@redhat.com>
- pick up php3.ini

* Wed Mar 24 1999 Preston Brown <pbrown@redhat.com>
- build against apache 1.3.6

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 2)

* Mon Mar 08 1999 Preston Brown <pbrown@redhat.com>
- upgraded to 3.0.7.

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Sun Feb 07 1999 Preston Brown <pbrown@redhat.com>
- upgrade to php 3.0.6, built against apache 1.3.4

* Mon Oct 12 1998 Cristian Gafton <gafton@redhat.com>
- rebuild for apache 1.3.3

* Thu Oct 08 1998 Preston Brown <pbrown@redhat.com>
- updated to 3.0.5, fixes nasty bugs in 3.0.4.

* Sun Sep 27 1998 Cristian Gafton <gafton@redhat.com>
- updated to 3.0.4 and recompiled for apache 1.3.2

* Thu Sep 03 1998 Preston Brown <pbrown@redhat.com>
- improvements; builds with apache-devel package installed.

* Tue Sep 01 1998 Preston Brown <pbrown@redhat.com>
- Made initial cut for PHP3.
