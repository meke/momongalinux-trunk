%global momorel 4

Name:           cal10n
Version:        0.7.2
Release:        %{momorel}m%{?dist}
Summary:        Compiler assisted localization library (CAL10N)

Group:          Development/Libraries
License:        MIT
URL:            http://cal10n.qos.ch/
Source0:        http://cal10n.qos.ch/dist/cal10n-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch

BuildRequires: junit4
BuildRequires: java-devel >= 1.6.0
BuildRequires: maven2
BuildRequires: maven2-plugin-assembly
BuildRequires: maven2-plugin-compiler
BuildRequires: maven2-plugin-install
BuildRequires: maven2-plugin-jar
BuildRequires: maven2-plugin-javadoc
BuildRequires: maven2-plugin-plugin
BuildRequires: maven2-plugin-resources
BuildRequires: maven2-plugin-source
BuildRequires: maven-doxia-sitetools
BuildRequires: maven-surefire-maven-plugin
BuildRequires: maven-surefire-provider-junit4

Requires(post):   jpackage-utils >= 1.7.3
Requires(postun): jpackage-utils >= 1.7.3

%description
Compiler Assisted Localization, abbreviated as CAL10N (pronounced as "calion") 
is a java library for writing localized (internationalized) messages.
Features:
    * java compiler verifies message keys used in source code
    * tooling to detect errors in message keys
    * native2ascii tool made superfluous, as you can directly encode bundles 
      in the most convenient charset, per locale.
    * good performance (300 nanoseconds per key look-up)
    * automatic reloading of resource bundles upon change


%package javadoc
Group:          Documentation
Summary:        Javadoc for %{name}

%description javadoc
API documentation for %{name}.

%package -n maven-cal10n-plugin
Summary: CAL10N maven plugin
Group: Development/Tools
Requires: maven2

%description -n maven-cal10n-plugin
Maven plugin verifying that the codes defined in
an enum type match those in the corresponding resource bundles. 

%prep
%setup -q 
find . -name "*.jar" | xargs rm

%build
mvn-jpp \
        -e \
        -Dmaven2.jpp.mode=true \
        -Dmaven.test.failure.ignore=true \
        install javadoc:javadoc

%install
rm -rf %{buildroot}

# jars
install -d -m 0755 %{buildroot}%{_javadir}/%{name}
install -m 644 cal10n-api/target/cal10n-api-%{version}.jar \
        %{buildroot}%{_javadir}/%{name}/cal10n-api-%{version}.jar
install -m 644 maven-cal10n-plugin/target/maven-cal10n-plugin-%{version}.jar \
        %{buildroot}%{_javadir}/%{name}/maven-cal10n-plugin-%{version}.jar

(cd %{buildroot}%{_javadir}/%{name} && for jar in *-%{version}*; \
    do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

%add_to_maven_depmap ch.qos.cal10n cal10n-parent %{version} JPP/%{name} cal10n-parent
%add_to_maven_depmap ch.qos.cal10n cal10n-api %{version} JPP/%{name} cal10n-api
%add_to_maven_depmap ch.qos.cal10n maven-cal10n-plugin %{version} JPP/%{name} maven-cal10n-plugin

# poms
install -d -m 755 %{buildroot}%{_mavenpomdir}
install -pm 644 pom.xml \
    %{buildroot}%{_mavenpomdir}/JPP.%{name}-parent.pom
install -pm 644 cal10n-api/pom.xml \
    %{buildroot}%{_mavenpomdir}/JPP.%{name}-api.pom
install -pm 644 maven-cal10n-plugin/pom.xml \
    %{buildroot}%{_mavenpomdir}/JPP.%{name}-maven-cal10n-plugin.pom

# javadoc
install -d -m 0755 %{buildroot}%{_javadocdir}/%{name}-%{version}
cp -pr target/site/api*/* %{buildroot}%{_javadocdir}/%{name}-%{version}/
ln -s %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}
rm -rf target/site/api*

%post
%update_maven_depmap

%postun
%update_maven_depmap

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_javadir}/%{name}
%{_javadir}/%{name}/%{name}*.jar
%{_datadir}/maven2/poms/JPP.%{name}-parent*
%{_datadir}/maven2/poms/JPP.%{name}-api*
%{_mavendepmapfragdir}/*

%files -n maven-cal10n-plugin
%defattr(-,root,root,-)
%{_javadir}/%{name}/maven*.jar
%{_datadir}/maven2/poms/JPP.%{name}-maven*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-2m)
- full rebuild for mo7 release

* Sat Mar 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-1m)
- import from Fedora 13

* Wed Feb 3 2010 Alexander Kurtakov <akurtako@redhat.com> 0.7.2-2
- Removed not needed external repo definitions.
- Use _mavenpomdir.

* Wed Feb 3 2010 Alexander Kurtakov <akurtako@redhat.com> 0.7.2-1
- Initial package

