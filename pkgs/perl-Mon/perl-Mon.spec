%global momorel 36
Summary: perl-Mon
Name: perl-Mon
Version: 0.11
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Languages
Source0: http://www.cpan.org/modules/by-module/Mon/Mon-%{version}.tar.gz
NoSource: 0
URL: http://www.cpan.org/modules/by-module/Mon/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.8.5
BuildArch: noarch

%description
The Mon::Client Mon::Protocol Mon::SNMP module

%prep
rm -rf %{buildroot}

%setup -q -n Mon-%{version} 

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make


%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/Mon/.packlist

%clean
rm -rf %{buildroot}

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-36m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-35m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-34m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-33m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-32m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-31m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-30m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-29m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-28m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-27m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-26m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-24m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-23m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-22m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-21m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-20m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-18m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-17m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-16m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.11-15m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11-14m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki
- (0.11-13m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11-12m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.11-11m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.11-10m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-9m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.11-8m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.11-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.11-6m)
- rebuild against perl-5.8.0

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.11-5m)
- remove BuildRequires: gcc2.95.3

* Fri Mar  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.11-4k)
- noarch

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.11-2k)
- create

%files
%defattr(-,root,root)
%doc CHANGES COPYING COPYRIGHT README VERSION
%{_mandir}/man?/*
%{perl_vendorlib}/Mon
