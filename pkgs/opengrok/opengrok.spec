%global momorel 4

# Webapp is disabled, since Fedora does not have the Java Webapp guidelines
# finished yet: http://fedoraproject.org/wiki/PackagingDrafts/JavaWebApps
%bcond_with webapp

Name:           opengrok
Version:        0.8.1
Release:        %{momorel}m%{?dist}
Summary:        Source browser and indexer

Group:          Development/Tools
License:        "CDDL"
URL:            http://hub.opensolaris.org/bin/view/Project+opengrok/
Source0:        http://hub.opensolaris.org/bin/download/Project+opengrok/files/%{name}-%{version}-src.tar.gz
NoSource:       0
Source1:        opengrok
Source2:        configuration.xml
Source3:        opengrok-README.Fedora.webapp
Source4:        opengrok-README.Fedora.nowebapp
Patch0:         opengrok-0.5-jrcs-import.patch
Patch1:         opengrok-0.7-nocplib.patch
Patch3:         opengrok-0.8.1-manifest-classpath.patch
Patch4:         opengrok-0.6-nooverview.patch
Patch5:         opengrok-0.6-nochangeset.patch
Patch6:         opengrok-0.7-jflex.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

%define common_reqs jakarta-oro ant bcel servlet lucene > 2 lucene-contrib > 2 swing-layout jpackage-utils javacc
Requires:       %{common_reqs}
Requires:       ctags
Requires:       java
BuildRequires:  %{common_reqs}
BuildRequires:  jflex >= 1.4
BuildRequires:  java_cup
BuildRequires:  ant-nodeps
# FIXME: As of 0.6-hg275 this should build with java-1.5 again.
# This is just to prevent GCJ from attempting to build this.
# ant scripts from both jrcs and opengrok need to be fixed somehow
BuildRequires:  java-devel >= 1.6
BuildRequires:  unzip
BuildRequires:  junit4
BuildRequires:  ant-junit
BuildRequires:  ctags
BuildRequires:  docbook2X

%description
OpenGrok is a fast and usable source code search and cross reference
engine, written in Java. It helps you search, cross-reference and navigate
your source tree. It can understand various program file formats and
version control histories like SCCS, RCS, CVS, Subversion and Mercurial.


%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Requires:       jpackage-utils

%description javadoc
%{summary}.


%if %with webapp
%package tomcat5
Summary:        Source browser web application
Group:          Development/Tools
Requires:       %{name} tomcat5

%description tomcat5
OpenGrok web application
%endif


%prep
%setup -q -n %{name}-%{version}-src
%{__unzip} -q ext/jrcs.zip
%patch0 -p1 -b .jrcs-import
%patch1 -p1 -b .nocplib
%patch3 -p1 -b .manifest-classpath
%patch4 -p1 -b .nooverview
%patch5 -p1 -b .nochangeset
%patch6 -p1 -b .jflex

# This is not strictly needed, but to nuke prebuilt stuff
# makes us feel warmer while building
find -name '*.jar' -o -name '*.class' -o -name '*.war' -exec rm -f '{}' \;

# jrcs' javacc directory
sed '
        s,\(property name="javacc.lib.dir" value="\)[^"]*,\1%{_javadir},;
        s,\(javacchome="\)[^"]*,\1${javacc.lib.dir},;
' -i jrcs/build.xml

# Default war configuration
sed 's,/opengrok/configuration.xml,%{_sysconfdir}/%{name}/configuration.xml,' \
        -i conf/web.xml

# README.Fedora
%if %with webapp
cp %{SOURCE3} README.Fedora
%else
cp %{SOURCE4} README.Fedora
%endif


%build
pushd jrcs
CLASSPATH=$(build-classpath oro) %{ant} -v all

popd
CLASSPATH=$(build-classpath jflex java_cup) %{ant} -v jar javadoc                               \
        -Dfile.reference.org.apache.commons.jrcs.diff.jar=jrcs/lib/org.apache.commons.jrcs.diff.jar \
        -Dfile.reference.org.apache.commons.jrcs.rcs.jar=jrcs/lib/org.apache.commons.jrcs.rcs.jar \
        -Dfile.reference.lucene-core-2.2.0.jar=$(build-classpath lucene)                        \
        -Dfile.reference.lucene-spellchecker-2.2.0.jar=$(build-classpath lucene-contrib/lucene-spellchecker) \
        -Dfile.reference.ant.jar=$(build-classpath ant)                                         \
        -Dfile.reference.bcel-5.1.jar=$(build-classpath bcel)                                   \
        -Dfile.reference.jakarta-oro-2.0.8.jar=$(build-classpath jakarta-oro)                   \
        -Dfile.reference.servlet-api.jar=$(build-classpath servlet)                             \
        -Dfile.reference.swing-layout-0.9.jar=$(build-classpath swing-layout)

# SolBook is more-or-less DocBook subset, so this can be done safely
# FIXME: db2x_docbook2man output is not as nice as it should be
sed '
        s,^<!DOCTYPE.*,<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "docbookx.dtd">,
        s,^<?Pub Inc>,,
' dist/opengrok.1 |db2x_docbook2man -


%check
pushd jrcs
CLASSPATH=$(build-classpath junit4) %{ant} test

popd
#CLASSPATH=$(build-classpath jflex junit4) %{ant} test


%install
rm -rf $RPM_BUILD_ROOT

# directories

%if %with webapp
%define webappdir %{_localstatedir}/lib/tomcat5/webapps/source
install -d $RPM_BUILD_ROOT%{webappdir}/WEB-INF/lib
%endif

install -d $RPM_BUILD_ROOT%{_javadir}
install -d $RPM_BUILD_ROOT%{_javadocdir}/%{name}
install -d $RPM_BUILD_ROOT%{_javadocdir}/%{name}-jrcs
install -d $RPM_BUILD_ROOT%{_bindir}
install -d $RPM_BUILD_ROOT%{_mandir}/man1
install -d $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -d $RPM_BUILD_ROOT%{_localstatedir}/lib/%{name}/data
install -d $RPM_BUILD_ROOT%{_localstatedir}/lib/%{name}/src
install -d $RPM_BUILD_ROOT%{_datadir}/pixmaps

# jar
install -p -m 644 dist/opengrok.jar $RPM_BUILD_ROOT%{_javadir}/opengrok-%{version}.jar
ln -sf opengrok-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/opengrok.jar

# jrcs
install -d $RPM_BUILD_ROOT%{_javadir}/opengrok-jrcs

install -p -m 644 jrcs/lib/org.apache.commons.jrcs.rcs.jar \
        $RPM_BUILD_ROOT%{_javadir}/opengrok-jrcs/org.apache.commons.jrcs.rcs-%{version}.jar
ln -sf org.apache.commons.jrcs.rcs-%{version}.jar \
        $RPM_BUILD_ROOT%{_javadir}/opengrok-jrcs/org.apache.commons.jrcs.rcs.jar

install -p -m 644 jrcs/lib/org.apache.commons.jrcs.diff.jar \
        $RPM_BUILD_ROOT%{_javadir}/opengrok-jrcs/org.apache.commons.jrcs.diff-%{version}.jar
ln -sf org.apache.commons.jrcs.diff-%{version}.jar \
        $RPM_BUILD_ROOT%{_javadir}/opengrok-jrcs/org.apache.commons.jrcs.diff.jar

# bin
install -p -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}

# man
install -p -m 644 opengrok.1 $RPM_BUILD_ROOT%{_mandir}/man1

# javadoc
cp -pR dist/javadoc/. $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -pR jrcs/doc/api/. $RPM_BUILD_ROOT%{_javadocdir}/%{name}-jrcs

# Configuration file configuration.xml
install -p -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}

%if %with webapp
# Make love, not war!
unzip -q dist/source.war -d $RPM_BUILD_ROOT%{webappdir}
(IFS=:; for file in $(build-classpath                   \
        bcel jakarta-oro swing-layout                   \
        lucene lucene-contrib/lucene-spellchecker)      \
        %{_javadir}/opengrok.jar                        \
        %{_javadir}/opengrok-jrcs/org.apache.commons.jrcs.diff.jar \
        %{_javadir}/opengrok-jrcs/org.apache.commons.jrcs.rcs.jar
do
        ln -sf $file $RPM_BUILD_ROOT%{webappdir}/WEB-INF/lib
done)
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_javadir}/*
%{_bindir}/opengrok
%{_mandir}/man1/opengrok.1*
%{_localstatedir}/lib/%{name}
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/configuration.xml
%doc CHANGES.txt LICENSE.txt README.txt doc/EXAMPLE.txt README.Fedora


%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*


%if %with webapp
%files tomcat5
%defattr(-,root,root,-)
%{webappdir}
%config(noreplace) %{webappdir}/WEB-INF/web.xml
%config(noreplace) %{webappdir}/index_body.html
%endif


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-1m)
- import from Fedora to Momonga

* Wed Feb 17 2010 Lubomir Rintel <lkundrak@v3.sk> - 0.8.1-1
- Fix build
- Update to later upstream release

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8-0.2.20090712hg
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Jul 12 2009 Lubomir Rintel <lkundrak@v3.sk> - 0.8-0.1.20090712hg
- Update to latest Mercurial snapshot
- bconds are nice, use them

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7-0.3.20081016hg
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Oct 16 2008 Lubomir Rintel <lkundrak@v3.sk> - 0.7-0.2-20081016hg
- Fix servlet classpath
- Do not depend on subversion-javahl now that svn tool is used
- Rediff jflex patch for zero fuzz

* Thu Oct 16 2008 Lubomir Rintel <lkundrak@v3.sk> - 0.7-0.1-20081016hg
- Update to post-0.7rc1
- Disable JUnit tests
- Remove GUI

* Sun Aug 10 2008 Lubomir Rintel <lkundrak@v3.sk> - 0.6.1-3.20080810hg
- Update to a Mercurial snapshot to adress excessive memory usage bug
- Specify fuzz for patches explicitely for now (see #458577)

* Thu May 22 2008 Lubomir Rintel <lkundrak@v3.sk> 0.6.1-2
- Tolerate svn-javahl not being in correct directory, in RHEL5
- Replace sed-mungled configuration with hardcoded, so that stamps don't change

* Mon May 19 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6.1-1
- New upstream release, dropping snapshot patch

* Fri Apr 18 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-9.hg275
- Review from Deepak Bhole and Andrew Overholt: (#433312)
- Remove GCJ bits
- Preserve timestamps wherever it makes sense
- Fix dependencies of -javadoc subpackage
- Remove webapp subpackage for now
- Do not try to include nonexistent overview file in jrcs javadoc
- Do not call hg

* Wed Apr 09 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-8.hg275
- Fix the Junit tests

* Wed Apr 09 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-7.hg275
- Javadoc
- Return forgotten patch to nuke classpath from manifest

* Wed Apr 09 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-6.hg275
- Newer tip with bugfixes
- %%check with junit tests

* Thu Apr 03 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-5.hg260
- GCJ

* Thu Mar 27 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-4.hg260
- Convert the manpage to roff from SolBook

* Thu Mar 27 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-3.hg260
- Install manual
- Don't warn if subversion jar is not available
- Correct java options variable name

* Thu Mar 27 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-2.hg260
- Current tip

* Thu Mar 27 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.6-1
- New upstream release

* Tue Mar 18 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-9.hg206
- Patch from Trend Norbye to close file handles manually
- Possibly to work around the VM issue

* Thu Feb 21 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-8.hg206
- Go back to revision 206; where project used a shared lucene index
- Will be cherry-picking important commits until searching multiple project works again

* Thu Feb 21 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-7.e3806d642190
- Subversion finally fixed
- README.Fedora

* Wed Feb 20 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-6.e3806d642190
- trunk
- Webapp
- Desktop entry

* Tue Feb 19 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-5.c4dea0135445
- swing-layout from jpackage 1.6
- We don't actually need ant-tools

* Tue Feb 19 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-4.c4dea0135445
- Use our lucene, once we have version 2

* Mon Feb 18 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-3.c4dea0135445
- Fix the script
- Use Tomcat servlet api
- Use internal JRCS

* Mon Feb 18 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-2.c4dea0135445
- Use mercurial snapshot
- Attempt to use no prebuilt stuff

* Fri Jan 25 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.5-1
- Initial packaging attempt
