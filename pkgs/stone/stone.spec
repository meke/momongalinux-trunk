%global momorel 10
%global sub_ver 2.3.2.7

Name: stone
Version: 2.3d
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source0: http://www.gcd.org/sengoku/%{name}/%{name}-%{version}-%{sub_ver}.tar.gz 
NoSource: 0
Patch0: stone-2.3b-man.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: TCP/IP packet repeater in the application layer
Url: http://www.gcd.org/sengoku/stone/Welcome.ja.html
BuildRequires: openssl-devel >= 1.0.0
Requires: openssl

%description
Stone is a TCP/IP packet repeater in the application layer. It repeats
TCP and UDP packets from inside to outside of a firewall, or from
 outside to inside.

Stone has following features:
                        
1. Stone supports Win32.
    Formerly, UNIX machines are used as firewalls, but recently
    WindowsNT machines are used, too. You can easily run Stone on
    WindowsNT/2000 and Windows95/98/ME. Of course, available on Linux,
    FreeBSD, BSD/OS, SunOS, Solaris, HP-UX and so on.
2. Simple.
    Stone's source code is only 4000 lines long (written in C language),
    so you can minimize the risk of security holes.
3. Stone supports SSL.
    Using OpenSSL, stone can encrypt/decrypt packets.
4. Stone is a http proxy.
    Stone can also be a tiny http proxy.
5. POP -> APOP conversion.
    With stone and a mailer that does not support APOP, you can access
    to an APOP server. The MD5 program is needed.

%prep
%setup -q -n %{name}-%{version}-%{sub_ver}

%patch0 -p1

%build
make linux-ssl SSL_FLAGS='-DUSE_SSL' SSL_LIBS='-lssl -lcrypto' CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/ja/man1
install stone %{buildroot}%{_bindir}
install stone.1 %{buildroot}%{_mandir}/man1
install stone.1.ja %{buildroot}%{_mandir}/ja/man1/stone.1
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
  iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/ja/man1/*
%doc GPL.txt README.en.txt README.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3d-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3d-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3d-8m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3d-7m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3d-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3d-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3d-4m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3d-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3d-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3d-1m)
- update 2.3d

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3c-3m)
- %%NoSource -> NoSource

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3c-2m)
- convert ja.man(UTF-8)

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3c-1m)
- update to 2.3c

* Tue Jul 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.3b-1m)
- update to 2.3b

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2e-2m)
- rebuild against openssl-0.9.8a

* Wed May 18 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2e-1m)
- update

* Fri Jan 21 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2d-1m)
- update to 2.2d

* Thu May 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2c-1m)
- update to 2.2c

* Mon Jan 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2b-1m)
- update to 2.2b

* Mon Dec 15 2003 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2a-1m)
- update to 2.2a
- use OpenSSL

* Tue Oct 15 2002 Junichiro Kita <kita@momonga-linux.org>
- update to 2.1w

* Mon Jul 29 2002 Junichiro Kita <kita@momonga-linux.org>
- initial build
