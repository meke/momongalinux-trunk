%global momorel 11

Summary: Userspace control program for the arptables network filter.
Name: arptables_jf
Version: 0.0.8
Release: %{momorel}m%{?dist}
Source: %{name}-%{version}.tbz
#Source1: Makefile
#Source2: arptables.h
#Source3: arptables.8
#Source4: libarptc.c
#Source5: libarptc.h
#Source6: arptables.init
Patch1: arptables_jf-0.0.8-2.6-kernel.patch
Patch2: arptables_jf-0.0.8-man.patch
Patch3: arptables_jf-0.0.8-warnings.patch
Patch4: arptables_jf-0.0.8-header.patch
Patch5: arptables_jf-0.0.8-init.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: System Environment/Base
License: GPL
BuildRequires: perl
# Conflicts: kernel < 2.4.0
Requires(post,postun): chkconfig
BuildRequires: glibc-kernheaders

%description
The arptables_jf utility controls the arpfilter network packet filtering
code in the Linux kernel.  You do not need this program for normal
network firewalling.  If you need to manually control which arp
requests and/or replies this machine accepts and sends, you should
install this package.

%prep
rm -rf %{buildroot}
%setup -q
%patch1 -p1 -b .2.6-kernel
%patch2 -p1 -b .man
%patch3 -p1 -b .warnings
%patch4 -p1 -b .header
%patch5 -p1 -b .init

%build
make all LIBDIR=/%{_lib} RCDIR=%{_initscriptdir}

%install
make install DESTDIR=%{buildroot} LIBDIR=/%{_lib} RCDIR=%{_initscriptdir}

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/chkconfig --add arptables_jf

%preun
if [ "$1" = 0 ]; then
        /sbin/chkconfig --del arptables_jf
fi

%files
%defattr(-,root,root,0755)
%config %{_initscriptdir}/arptables_jf
/sbin/arptables*
%{_mandir}/*/arptables*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.8-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.8-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.8-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.8-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.8-5m)
- rebuild against gcc43

* Sun Jun 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.8-4m)
- move /etc/rc.d/init.d -> /etc/init.d

* Wed Jun 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.8-3m)
- disable service at initial start up

* Fri Oct 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.8-2m)
- merge FC 0.0.8-8
- * Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:0.0.8-8
- - rebuild
- - Add patch to not include linux/compiler.h
- - Remove br on glibc-kernheaders, part of the build-env.

* Sat Jun 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.8-1m)
- import to Momonga

* Fri May 26 2006 Jay Fenlason <fenlason@redhat.com> 0:0.0.8-7
- Add warnings patch to close
  bz#191688 arptables_jf fails to build in mock

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0:0.0.8-6.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0:0.0.8-6.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Aug 18 2005 Florian La Roche <laroche@redhat.com>
- change the requires into a conflicts for "kernel"

* Thu Jun 9 2005 Jay Fenlason <fenlason@redhat.com> 0.0.8-5
- add -man patch to correct the names of the default tables.
  bz#123089 aptables man pages is not correct: built in chain name are wrong.

* Tue Mar 8 2005 Jay Fenlason <fenlason@redhat.com> 0.0.8-4
- rebuilt with gcc4

* Fri Nov 26 2004 Florian La Roche <laroche@redhat.com>
- add a %%clean target into .spec

* Tue Aug 31 2004 Jay Fenlason <fenlason@redhat.com> 0.0.8-2
- backport latest version from 3E branch.
- Add 2.6-kernel patch, since glibc_kernheaders has incorrect
  arptables headers for the 2.6 kernel.

* Mon Jul 7 2003 Jay Fenlason <fenlason@redhat.com> 0.0.2-0
- first attempt at a packaged version of arptables_jf, for
  cambridge.
