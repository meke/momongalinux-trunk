%global momorel 8
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Outline-based notes management and organizer for Emacs
Name: emacs-org-mode
Version: 7.01g
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Editors
Source0: http://orgmode.org/org-%{version}.tar.gz
URL: http://orgmode.org/
BuildArch: noarch
BuildRequires: emacs >= %{emacsver}
BuildRequires: info
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: org-mode-emacs

Obsoletes: elisp-org-mode
Provides: elisp-org-mode

%description
Org-mode is for keeping notes, maintaining ToDo lists, doing project
planning, and authoring with a fast and effective plain-text system.

%prep
%setup -q -n org-%{version}
rm -f contrib/lisp/.DS_Store

%build
make EMACS=emacs \
     lispdir=%{e_sitedir}/org-mode

%install
rm -rf %{buildroot}

make EMACS=emacs \
     lispdir=%{buildroot}%{e_sitedir}/org-mode \
     install

mkdir -p %{buildroot}%{_infodir}
install -m 644 doc/org %{buildroot}%{_infodir}/org-mode

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/org-mode %{_infodir}/dir \
    --entry="* Org Mode: (org-mode).      Outline-based notes management and organizer" --section="Emacs"

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/org-mode %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc Changes.org README request-assign-future.txt
%doc doc/org.pdf doc/orgcard.pdf
%doc contrib xemacs
%{e_sitedir}/org-mode
%{_infodir}/org-mode*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.01g-8m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.01g-7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.01g-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.01g-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.01g-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.01g-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.01g-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.01g-1m)
- update to 7.01g

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.33f-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.33f-2m)
- merge org-mode-emacs to elisp-org-mode

* Thu Dec 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.33f-1m)
- update to 6.33f

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.28e-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.28e-3m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.28e-2m)
- rebuild against emacs 23.0.96

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.28e-1m)
- update to 6.28e

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.27a-2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.27a-1m)
- update to 6.27a

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.27-2m)
- rebuild against emacs-23.0.94
 
* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.27-1m)
- update to 6.27

* Thu May 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.26d-1m)
- update to 6.26d

* Wed Apr 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.25f-1m)
- initial packaging
