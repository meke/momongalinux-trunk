%global         momorel 11

Name:           perl-Makefile-Parser
Version:        0.215
Release:        %{momorel}m%{?dist}
Summary:        Simple parser for Makefiles
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Makefile-Parser/
Source0:        http://www.cpan.org/authors/id/A/AG/AGENT/Makefile-Parser-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-test.patch
Patch1:         %{name}-%{version}-pod.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.1
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-Class-Trigger >= 0.13
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Slurp
BuildRequires:  perl-Filter-Util-Call
BuildRequires:  perl-IPC-Run3 >= 0.036
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-List-Util
BuildRequires:  perl-Makefile-DOM >= 0.005
BuildRequires:  perl-Text-Balanced
BuildRequires:  perl-Time-HiRes
Requires:       perl-Class-Accessor
Requires:       perl-Class-Trigger >= 0.13
Requires:       perl-Cwd
Requires:       perl-File-Slurp
Requires:       perl-Filter-Util-Call
Requires:       perl-List-MoreUtils
Requires:       perl-List-Util
Requires:       perl-Makefile-DOM >= 0.005
Requires:       perl-Text-Balanced
Requires:       perl-Time-HiRes
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is a simple parser for Makefiles. At this very early stage, the parser
only supports a limited set of features, so it may not recognize most of
the advanced features provided by certain make tools like GNU make. Its
initial purpose is to provide basic support for another module named
Makefile::GraphViz, which is aimed to render the building process specified
by a Makefile using the amazing GraphViz library. The Make module is not
satisfactory for this purpose, so I decided to build one of my own.

%prep
%setup -q -n Makefile-Parser-%{version}
%patch0 -p1
%patch1 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Makefile/AST*
%{perl_vendorlib}/Makefile/Parser*
%{_mandir}/man3/*
%{_mandir}/man1/*
%{_bindir}/makesimple
%{_bindir}/pgmake-db
%{_bindir}/plmake

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-11m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-10m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-9m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-8m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-7m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-4m)
- rebuild against perl-5.16.0

* Tue Jan 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-3m)
- enable test

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.215-1m)
- update to 0.215
- disbale test for now (FIX ME)

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.211-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.211-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.211-10m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.211-9m)
- no check (fix me)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.211-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.211-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.211-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.211-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.211-4m)
- rebuild against perl-5.12.0

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.211-3m)
- add LANG=C

* Thu Jan 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.211-2m)
- remove duplicate directories

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.211-1m)
- import from Rawhide for publican-1.3

* Mon Dec  7 2009 Stepan Kasal <skasal@redhat.com> - 0.211-2
- rebuild against perl 5.10.1

* Mon Sep 07 2009 Scott Radvan <sradvan@redhat.com> 0.211-1
- Specfile autogenerated by cpanspec 1.78.
