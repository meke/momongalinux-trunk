%global momorel 5

%define	fontname	hanazono
%define archivename	%{fontname}-%{version}
%define	priority	65-1
%define fontconf	%{priority}-%{fontname}.conf

Name:		%{fontname}-fonts
Version:	20100222
Release:	%{momorel}m%{?dist}
Summary:	Japanese Mincho-typeface TrueType font

Group:		User Interface/X
License:	"Copyright only"
URL:		http://fonts.jp/hanazono/
Source0:	http://fonts.jp/hanazono/%{archivename}.zip
NoSource:	0
Source1:	%{name}-fontconfig.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:	noarch
BuildRequires:	fontpackages-devel
Requires:	fontpackages-filesystem

%description
Hanazono Mincho typeface is a Japanese TrueType font that developed with
a support of Grant-in-Aid for Publication of Scientific Research Results from
Japan Society for the Promotion of Science and the International Research
Institute for Zen Buddhism (IRIZ), Hanazono University. also with volunteers
who work together on glyphwiki.org.

This font supports:
 - 6359 characters in JIS X 0208:1997
 - 3695 characters in JIS X 0213:2004
 - 22 characters in ISO/IEC 10646:2003/Amd.1:2005 (U+9FA6~U+9FBB)
 - 8 characters in Unicode 5.1 (U+9FBC~U+9FC3)
 - 16 characters in ISO/IEC 10646:2003/Amd.1:2005 (U+31C0~U+31CF)
 - 20 characters in ISO/IEC 10646:2003/Amd.3:2008 (U+31D0~U+31E3)
 - 84 characters in IBM extensions

%prep
%setup -q -T -c -a 0


%build


%install
rm -rf $RPM_BUILD_ROOT

install -dm 0755 $RPM_BUILD_ROOT%{_fontdir}
install -pm 0644 hanazono.ttf $RPM_BUILD_ROOT%{_fontdir}
install -dm 0755 $RPM_BUILD_ROOT%{_fontconfig_templatedir} \
		 $RPM_BUILD_ROOT%{_fontconfig_confdir}
install -pm 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} $RPM_BUILD_ROOT%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf $RPM_BUILD_ROOT


%_font_pkg -f %{fontconf} hanazono.ttf

%doc LICENSE.txt README.txt THANKS.txt


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2010022-5m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100222-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100222-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100222-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100222-1m)
- update to 20100222

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090506-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090506-1m)
- update to 20090506
- no src

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081012-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20081012-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20081012-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Dec 24 2008 Akira TAGOH <tagoh@redhat.com> - 20081012-6
- Update the spec file to fit into new guideline. (#477395)

* Fri Nov 14 2008 Akira TAGOH <tagoh@redhat.com> - 20081012-5
- Fix a typo in fontconfig config again.

* Thu Nov 13 2008 Akira TAGOH <tagoh@redhat.com> - 20081012-4
- Try to test the language with the exact match in fontconfig config.

* Wed Nov 12 2008 Akira TAGOH <tagoh@redhat.com> - 20081012-3
- Fix a typo in fontconfig config.

* Mon Nov 10 2008 Akira TAGOH <tagoh@redhat.com>
- Drop -f from fc-cache.
- Improve fontconfig config.

* Mon Nov 10 2008 Akira TAGOH <tagoh@redhat.com> - 20081012-2
- Improve a bit in the spec file.

* Tue Oct 28 2008 Akira TAGOH <tagoh@redhat.com> - 20081012-1
- Initial packaging.

