%global momorel 4

Name: hunspell-sk
Summary: Slovak hunspell dictionaries
%define upstreamid 20091213
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://www.sk-spell.sk.cx/file_download/75/%{name}-%{upstreamid}.zip
Group: Applications/Text
URL: http://www.sk-spell.sk.cx/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2 or GPLv2 or MPLv1.1
BuildArch: noarch

Requires: hunspell

%description
Slovak hunspell dictionaries.

%prep
%setup -q -n %{name}-%{upstreamid}

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc doc/*
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20091213-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20091213-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20091213-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20091213-1m)
- sync with Fedora 13 (1:0.20091213-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.6-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.6-1m)
- import from Fedora to Momonga

* Wed Jul 11 2007 Caolan McNamara <caolanm@redhat.com> - 1:0.5.6-1
- canonical upstream version

* Wed Feb 14 2007 Caolan McNamara <caolanm@redhat.com> - 0.20050911-1
- upstream id

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20050228-1
- initial version
