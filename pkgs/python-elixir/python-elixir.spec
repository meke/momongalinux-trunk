%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-elixir
Version:        0.7.1
Release:        %{momorel}m%{?dist}
Summary:        A declarative mapper for SQLAlchemy

Group:          Development/Languages
License:        MIT
URL:            http://elixir.ematia.de/
Source0:        http://pypi.python.org/packages/source/E/Elixir/Elixir-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7, python-setuptools >= 0.6c9-2m

Requires:       python-sqlalchemy


%description
Elixir is a declarative layer on top of SQLAlchemy. It is a fairly thin
wrapper, which provides the ability to define model objects following the
Active Record design pattern, and using a DSL syntax similar to that of the
Ruby on Rails ActiveRecord system.

Elixir does not intend to replace SQLAlchemy's core features, but instead
focuses on providing a simpler syntax for defining model objects when you do
not need the full expressiveness of SQLAlchemy's manual mapper definitions.


%prep
%setup -q -n Elixir-%{version}


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
rm -rf build/lib/{tests,examples}/
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc PKG-INFO README
# For noarch packages: sitelib
%{python_sitelib}/*


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linnux.org>
- (0.7.1-1m)
- update to 0.7.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.2-1m)
- import from Fedora

* Sat Apr 05 2008 James Bowes <jbowes@redhat.com> 0.5.2-1
- Update to 0.5.2

* Mon Feb 11 2008 James Bowes <jbowes@redhat.com> 0.5.1-1
- Update to 0.5.1

* Thu Dec 13 2007 James Bowes <jbowes@redhat.com> - 0.5.0-1
- Update to 0.5.0

* Wed Nov 14 2007 Steve 'Ashcrow' Milner <me@stevemilner.org> - 0.4.0-1
- Updated for upstream 0.4.0.

* Sun Jun 24 2007 James Bowes <jbowes@redhat.com> - 0.3.0-1
- Initial packaging for Fedora.
