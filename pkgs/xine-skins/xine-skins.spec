%global momorel 10
%define	skindir	%{_datadir}/xine/skins

Summary: A collection of skins for XINE.
Name: xine-skins
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
# xinetic is included in xine-ui
Source0: http://xinehq.de/index.php/force-download/skins/CelomaGold.tar.gz
Source1: http://xinehq.de/index.php/force-download/skins/CelomaChrome.tar.gz
Source2: http://xinehq.de/index.php/force-download/skins/CelomaMdk.tar.gz
Source3: http://xinehq.de/index.php/force-download/skins/Centori.tar.gz
Source4: http://xinehq.de/index.php/force-download/skins/Crystal.tar.gz
Source5: http://xinehq.de/index.php/force-download/skins/Keramic.tar.gz
Source6: http://xinehq.de/index.php/force-download/skins/cloudy.tar.gz
Source7: http://xinehq.de/index.php/force-download/skins/concept.tar.gz
Source8: http://xinehq.de/index.php/force-download/skins/lcd.tar.gz
Source9: http://xinehq.de/index.php/force-download/skins/mp2k.tar.gz
Source10: http://xinehq.de/index.php/force-download/skins/pitt.tar.gz
Source11: http://xinehq.de/index.php/force-download/skins/mplayer.tar.gz
Source12: http://xinehq.de/index.php/force-download/skins/KeramicRH8.tar.gz
Source13: http://xinehq.de/index.php/force-download/skins/OMS_legacy.tar.gz
#Source14: http://xinehq.de/index.php/force-download/skins/xinetic.tar.gz
Source15: http://www.xinehq.de/index.php/force-download/skins/Sunset.tar.gz
Source16: http://www.xinehq.de/index.php/force-download/skins/Galaxy.tar.gz
Source17: http://xinehq.de/index.php/force-download/skins/gudgreen.tar.gz

URL: http://www.xinehq.de/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xine-ui
BuildArch: noarch

%description
This package contains a collection of additional skins for the GUI version
of XINE, the movie player for Linux. Install this package if you wish to
change the appeareance of XINE.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{skindir}
skins="%{SOURCE0} %{SOURCE1} %{SOURCE2} \
%{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} %{SOURCE7} %{SOURCE8} \
%{SOURCE9} %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE15} %{SOURCE16} %{SOURCE17}"
pushd %{buildroot}%{skindir}
for skin in $skins; do
    tar -zxf $skin
done
popd

%clean
rm -rf %{buildroot}

%files
%defattr(0644, root, root, 0755)
%{skindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc43

* Sat Apr 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-4m)
- add CelomaChrome and cloudy since these skins are not included in
  xine-ui-0.99.1

* Thu Jan 22 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-3m)
- add gudgreen

* Sat Dec 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-2m)
- add Sunset and Galaxy

* Thu Jul 17 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-1m)
- initial package

