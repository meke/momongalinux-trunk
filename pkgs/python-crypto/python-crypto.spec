%global momorel 4
%global real_name pycrypto
%global pythonver 2.7

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Collection of cryptographic algorithms and protocols for python
Name: python-crypto
Version: 2.3
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Libraries
URL: http://www.dlitz.net/software/pycrypto/

Source0: http://ftp.dlitz.net/pub/dlitz/crypto/%{real_name}/%{real_name}-%{version}.tar.gz
NoSource: 0
Patch0: python-crypto-2.3-optflags.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python-devel >= %{pythonver}
BuildRequires: python >= %{pythonver}
BuildRequires: gmp >= 5.0
Requires: python >= %{pythonver}
Obsoletes: pycrypto <= %{version}
Provides: pycrypto <= %{version}

%description
pycrypto is a collection of cryptographic algorithms and protocols,
implemented for use from Python. Among the contents of the package:

    * Hash functions: MD2, MD4, RIPEMD.
    * Block encryption algorithms: AES, ARC2, Blowfish, CAST, DES, Triple-DES, IDEA, RC5.
    * Stream encryption algorithms: ARC4, simple XOR.
    * Public-key algorithms: RSA, DSA, ElGamal, qNEW.
    * Protocols: All-or-nothing transforms, chaffing/winnowing.
    * Miscellaneous: RFC1751 module for converting 128-key keys into a set of English words, primality testing.

%prep
%setup -q -n %{real_name}-%{version}
%patch0 -p1

%build
python2 setup.py build

%install
%{__rm} -rf %{buildroot}
python2 setup.py install \
	--root="%{buildroot}"

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc ACKS ChangeLog COPYRIGHT README TODO Doc/ LEGAL/
##%%{python_sitelib}/Crypto/
%{_libdir}/python*/site-packages/Crypto/
##%%{python_sitelib}/%{real_name}-*.egg-info
%{_libdir}/python*/site-packages/%{real_name}-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-1m)
- update to 2.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsarr@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Feb 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-9m)
- [SECURITY] CVE-2009-0544
-- import upstream patches
- License: Public Domain

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-8m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.1-7m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-6m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.1-5m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-3m)
- %%NoSource -> NoSource

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- rebuild against python-2.5

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Apr 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-1m)
- import to Momonga for bittorrent
- delete out # Packager: Dag Wieers <dag@wieers.com>
- delete out # Vendor: Dag Apt Repository, http://dag.wieers.com/apt/
- nosrc 0
- add -q at setup

* Mon Dec 20 2004 Dag Wieers <dag@wieers.com> - 2.0-1
- Updated to release 2.0.

* Sat Jan 31 2004 Dag Wieers <dag@wieers.com> - 1.9-0.a6
- Initial package. (using DAR)
