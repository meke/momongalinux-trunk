%global momorel 4

Summary: The Mail Transport Agent Switcher
Name: system-switch-mail
Version: 1.0.1
Release: %{momorel}m%{?dist} 
Url: http://than.fedorapeople.org/
Source0: http://than.fedorapeople.org/%{name}-%{version}.tar.bz2
License: GPLv2+
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch

Requires: newt-python
Requires: chkconfig
Requires: python
Requires: usermode

BuildRequires: python-devel
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: desktop-file-utils

%description
The system-switch-mail is the Mail Transport Agent Switcher.
It enables users to easily switch between various Mail Transport Agent
that they have installed.

%package gnome
Summary: A GUI interface for Mail Transport Agent Switcher
Group: Applications/System
Requires: %{name} = %{version}-%{release}
Requires: libglade2
Requires: pygtk2-libglade
Requires: usermode-gtk

%description gnome
The system-switch-mail-gnome package contains a GNOME interface for the
Mail Transport Agent Switcher.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} mandir=%{_mandir} sysconfdir=%{_sysconfdir} install
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING AUTHORS
%{_bindir}/*
%{_sbindir}/*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/functions.py*
%{_datadir}/%{name}/switchmail_tui.py*
%{_mandir}/man1/*
%config(noreplace) %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/%{name}*

%files gnome
%defattr(-,root,root,-)
%{_datadir}/applications/*
%{_datadir}/pixmaps/*
%{_datadir}/%{name}/switchmail_gui.py*
%{_datadir}/%{name}/pixmaps
%{_datadir}/%{name}/switchmail.glade

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-1m)
- sync with Rawhide (1.0.1-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.26-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.26-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.26-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 01 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.5.26-4
- Rebuild for Python 2.6

* Sat Sep  6 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.5.26-3
- fix license tag

* Tue Mar 11 2008 Than Ngo <than@redhat.com> 0.5.26-2
- fix permission of po files (bz#436888)
- update po files

* Fri Mar 07 2008 Than Ngo <than@redhat.com> 0.5.26-1
- 0.5.26 release
   - use chkconfig to configure selected MTA to start automatically (#436359)
   - add missing file in POTFILES.in (#433456)
   - use correct python version (#427571)
   - chkconfig off/on after mta is switched (#246092)
   - Require newt-python (#251363)
   - remove obsolete translation (#332461)
   - add man page (#427570)

* Tue Apr 10 2007 Than Ngo <than@redhat.com> - 0.5.25-13
- add support for ssmtp and esmtp

* Mon Dec 18 2006 Phil Knirsch <pknirsch@redhat.com> - 0.5.25-12
- Resolves: bz#216602, one more update for kn and si po files

* Mon Dec 04 2006 Than Ngo <than@redhat.com> - 0.5.25-11
- Resolved: bz#216602, update po files

* Mon Jul 17 2006 Than Ngo <than@redhat.com> 0.5.25-10
- update po files

* Tue Mar 07 2006 Than Ngo <than@redhat.com> 0.5.25-8
- fix deprecated functions in gtk #159155

* Mon Feb 27 2006 Than Ngo <than@redhat.com> 0.5.25-7
- fix consolehelper config #160931

* Sat Dec 17 2005 Than Ngo <than@redhat.com> 0.5.25-6
- update po files

* Wed Oct 26 2005 Than Ngo <than@redhat.com> 0.5.25-5
- add new common pam configuration file #170650 
- update po files

* Wed May 11 2005 Than Ngo <than@redhat.com> 0.5.25-4
- fix location for menu item #157173

* Fri Oct 01 2004 Than Ngo <than@redhat.com> 0.5.25-3
- update translation

* Tue Sep 21 2004 Than Ngo <than@redhat.com> 0.5.25-2
- rebuilt

* Mon Apr 05 2004 Than Ngo <than@redhat.com> 0.5.25-1
- 0.5.25 release

* Thu Feb 19 2004 Than Ngo <than@redhat.com> 0.5.24-1
- 0.5.24 release

* Sun Feb 15 2004 Than Ngo <than@redhat.com> 0.5.23-1
- 0.5.23 release

* Tue Nov 25 2003 Than Ngo <than@redhat.com> 0.5.22-1
- 0.5.22: renamed to system-switch-mail, support Exim MTA    

* Mon Sep 29 2003 Than Ngo <than@redhat.com> 0.5.21-1
- 0.5.21, fixed Categories

* Thu Aug 14 2003 Than Ngo <than@redhat.com> 0.5.20-1
- 0.5.20, use intltool instead pygettext,
  thanks to Miloslav Trmac (bug #82319, #83464)

* Thu Jul 17 2003 Than Ngo <than@redhat.com> 0.5.19-1
- 0.5.19
- exim support
- UTF8 issue in PO files

* Wed Feb  5 2003 Than Ngo <than@redhat.com> 0.5.18-1
- 0.5.18

* Mon Feb  3 2003 Than Ngo <than@redhat.com> 0.5.17-1
- 0.5.17

* Tue Dec 10 2002 Than Ngo <than@redhat.com> 0.5.16-1
- rename to redhat-switch-mail
- start service after successfully switched

* Sat Nov  9 2002 Than Ngo <than@redhat.com> 0.5.15-1
- updated po files
- remove some unpackaged files

* Tue Sep  3 2002 Than Ngo <than@redhat.com> 0.5.14-1
- Updated po files

* Mon Aug 26 2002 Than Ngo <than@redhat.com> 0.5.13-1
- Fix some inconsistent interface (bug #72443)

* Mon Aug 12 2002 Karsten Hopp <karsten@redhat.de>
- fix german translation. String output was completely broken
- use SystemSetup keyword

* Wed Aug  7 2002 Than Ngo <than@redhat.com> 0.5.11-1
- move to X-Red-Hat-Extra (bug #70992)
- Updated po files

* Tue Aug  6 2002 Than Ngo <than@redhat.com> 0.5.10-1
- Fixed a bug in warningDialog (bug #70828)
- Updated po files

* Fri Jul 26 2002 Than Ngo <than@redhat.com> 0.5.9-1
- Use pam_timestamp.so

* Thu Jul 25 2002 Than Ngo <than@redhat.com> 0.5.8-1
- new desktop file
- update po files

* Wed Jun 12 2002 Harald Hoyer <harald@redhat.de>
- fallback for C locale

* Wed Jun 12 2002 Than Ngo <than@redhat.com> 0.5.6-1
- fixed traceback bug (bug #66465)

* Mon Jun  3 2002 Harald Hoyer <harald@redhat.de>
- dropped gnome2, fixed automake Req

* Wed May 29 2002 Phil Knirsch <pknirsch@redhat.com> 0.5.3-1
- Included all fixes for gnome2 by Harald Hoyer <harald@redhat.com>

* Wed May 29 2002 Phil Knirsch <pknirsch@redhat.com> 0.5.2-2
- Fixed to build in new environment.

* Wed Apr 24 2002 Than Ngo <than@redhat.com> 0.5.2-1
- fixed a bug in runing in text mode in X

* Wed Apr 10 2002 Than Ngo <than@redhat.com> 0.5.1-1
- release 0.5.1
- update translations

* Mon Apr  8 2002 Than Ngo <than@redhat.com> 0.5.0-1
- release 0.5.0

* Sun Mar 31 2002 Than Ngo <than@redhat.com> 0.4.0-1
- release 0.4.0

* Sat Mar 02 2002 Than Ngo <than@redhat.com> 0.3.0-1
- update to 0.3.0 (bug #60590, #60591)

* Tue Feb 26 2002 Than Ngo <than@redhat.com> 0.2.0-1
- update to 0.2.0

* Thu Jan 31 2002 Bill Nottingham <notting@redhat.com> 0.1.1-1
- %%defattr in -gnome package

* Mon Jan 28 2002 Than Ngo <than@redhat.com> 0.1.0-1
- initial packaging
