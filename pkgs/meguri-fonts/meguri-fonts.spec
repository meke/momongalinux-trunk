%global momorel 3

%define priority	65-2
%define fontname	meguri
%define	archivename	meguri-fonts
%define	fontconf	%{priority}-%{fontname}
%define	common_desc	\
Meguri fonts are modified IPA and M+ fonts for Japanese.

Name:		%{fontname}-fonts
Version:	20101113
Release:	%{momorel}m%{?dist}
Summary:	Modified IPA and M+ fonts

License:	"IPA" and "mplus"
Group:		User Interface/X
URL:		http://www.geocities.jp/ep3797/modified_fonts_01.html
# Source0:	http://www.geocities.jp/ep3797/snapshot/modified_fonts/%{archivename}-%{version}a.tar.lzma
Source0:	http://downloads.sourceforge.net/project/mdk-ut/30-source/source/%{archivename}-%{version}a.tar.lzma
NoSource:       0
Source1:        %{fontname}-fontconfig-gothic.conf
Source2:        %{fontname}-fontconfig-pgothic.conf
Source11:       revert-backslash.pe
Source12:       revert-backslash-p.pe
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	fontpackages-devel
BuildRequires:	fontforge

Requires:	%{name}-common = %{version}-%{release}
%description
%common_desc

This package contains %{fontname} fonts.

%package	common
Summary:	Common files of meguri
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description	common
%common_desc

This package consists of files used by other %{name} packages.

%package -n	%{fontname}-p-fonts
Summary:	Meguri proportional font
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-p-fonts
%common_desc

This package contains %{fontname} proportional fonts.

%prep
%setup -q -n %{archivename}-%{version}a

%build
fontforge %{SOURCE11}
fontforge %{SOURCE12}

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 modified-meguri.ttf %{buildroot}%{_fontdir}/meguri.ttf
install -m 0644 modified-meguri-p.ttf %{buildroot}%{_fontdir}/meguri-p.ttf

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir}
install -m 0755 -d %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-gothic.conf
install -m 0644 -p %{SOURCE2} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pgothic.conf

for fconf in %{fontconf}-gothic.conf %{fontconf}-pgothic.conf; do
    ln -s %{_fontconfig_templatedir}/$fconf %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -rf %{buildroot}

%files common
%defattr(-,root,root,-)
%doc ChangeLog README
%doc docs-ipa/
%doc docs-mplus/
%dir %{_fontdir}

%_font_pkg -f %{fontconf}-gothic.conf meguri.ttf

%_font_pkg -n p -f %{fontconf}-pgothic.conf meguri-p.ttf

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20101113-3m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20101113-2m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20101113-1m)
- update 20101113a

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100114-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100114-5m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100114-4m)
- the priority is 65-2

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100114-3m)
- add ja to fontconfig files

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100114-2m)
- priority is 68 (temporal)
- remove binding="same" from fontconfig files

* Fri Jan 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100114-1m)
- update to 20100114

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090604-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug  6 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (20090604-3m)
- Move half-width yen to correct position
- Generate backslash from slash

* Sun Jul 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090604-2m)
- revert backslash

* Fri Jul 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090604-1m)
- initial packaging
