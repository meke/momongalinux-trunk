%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:		obmenu
Version:	1.0
Release:	%{momorel}m%{?dist}
Summary:	A graphical menu editor for Openbox
Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://obmenu.sourceforge.net/

Source0:	http://dl.sourceforge.net/sourceforge/obmenu/%{name}-%{version}.tar.gz
NoSource: 0
Source2:	%{name}.desktop
Patch0:		%{name}-copy-default-xdg-menu.patch 

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

Requires:	pygtk2-libglade

BuildRequires:	desktop-file-utils
BuildRequires:	python-devel >= 2.7

%description
obmenu is a graphical menu editor for the Openbox window manager. Openbox uses
XML to store its menu preferences, and editing these by hand can quickly become
tedious; and even moreso when generating an entire menu for oneself! However,
this utility provides a convenient method of editing the menu in a graphical
interface, while not losing the powerful features of Openbox such as its
pipe menus. 

This also provides a Python module named obxml that can be used to further
script Openbox's menu system. 


%prep
%setup -q
%patch0 -p0


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
chmod +x %{buildroot}%{python_sitelib}/obxml.py
desktop-file-install --vendor= 	\
	--dir %{buildroot}%{_datadir}/applications	\
	%{SOURCE2}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/%{name}
%{_bindir}/obm-*
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{python_sitelib}/obxml.py
%{python_sitelib}/obxml.pyc
%{python_sitelib}/obxml.pyo
%{python_sitelib}/*.egg-info


%changelog
* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- import from Rawhide

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.0-7
- Rebuild for Python 2.6

* Mon Jan 07 2008 Miroslav Lichvar <mlichvar@redhat.com> - 1.0-6
- Include egg-info file

* Wed Aug 22 2007 Miroslav Lichvar <mlichvar@redhat.com> - 1.0-5
- Update license tag

* Fri Dec 08 2006 Peter Gordon <peter@thecodergeek.com> - 1.0-4
- Rebuild for Python 2.5 upgrade
- Fix Categories entry and remove X-Fedora category addition in installed
  .desktop file

* Sun Oct 15 2006 Peter Gordon <peter@thecodergeek.com> - 1.0-3
- Some minor aesthetic spec cleanups
- Add a patch from upstream to copy the default /etx/xdg menu stuff if one
  does not exist on the first run:
  + copy-default-xdg-menu.patch
- Drop unneeded README.Fedora file:
  - README.Fedora


* Fri Sep 01 2006 Peter Gordon <peter@thecodergeek.com> - 1.0-2
- Don't %%ghost the .pyo file(s) to comply with the new Extras Python
  packaging guidelines
- Package a README.Fedora file and a .desktop file:
  + README.Fedora
  + %{name}.desktop

* Sun Jun 14 2006 Peter Gordon <peter@thecodergeek.com> - 1.0-1
- Initial packaging
