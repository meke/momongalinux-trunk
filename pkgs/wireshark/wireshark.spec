%global momorel 1

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
#%%global pre_ver pre1-26829
#%%global pre 1

Summary:	Network traffic analyzer
Name:		wireshark
Version:	1.10.8
Release:	%{momorel}m%{?dist}
#Release:	0.%{pre}.%{momorel}m%{?dist}
License:	GPL+
Group:		Applications/System
URL:		http://www.wireshark.org/
Source0:	http://www.wireshark.org/download/src/%{name}-%{version}.tar.bz2
#Source0:	http://www.wireshark.org/download/prerelease/%{name}-%{version}%{pre_ver}.tar.gz
NoSource:	0
Source1:	wireshark.pam
Source2:	wireshark.console
Source3:	wireshark.desktop
Patch1:		wireshark-libtool-pie.patch
Patch3:		wireshark-1.6.0-nfsv4-opts.patch
Patch4:         wireshark-1.10.0-enable-lua.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	net-snmp-devel >= 5.7.1
BuildRequires:	gtk2-devel
BuildRequires:	flex
BuildRequires:	openssl-devel >= 1.0.0
BuildRequires:	libpcap-devel >= 1.4.0
BuildRequires:	krb5-devel
BuildRequires:	automake >= 1.5-15m
BuildRequires:	elfutils-libelf
BuildRequires:	elfutils-devel
BuildRequires:	atk-devel >= 1.4.1-3m
BuildRequires:	momonga-rpmmacros >= 20040310-6m
BuildRequires:	libgcrypt-devel >= 1.2.3-2m
BuildRequires:	libgpg-error-devel >= 1.5-2m
BuildRequires:	gnutls-devel >= 3.2.0
BuildRequires:	portaudio-devel >= 19
BuildRequires:	python >= 2.7
Requires:	libpcap >= 1.4.0
Conflicts:	ethereal
Obsoletes:	ethereal

%package gnome
Summary:	Gnome desktop integration for wireshark and wireshark-usermode
Group:		Applications/Internet
Requires:	gtk2
Requires:	usermode >= 1.37
Requires:	%{name} = %{version}-%{release}
Requires:	net-snmp >= 5.7.1
Requires:	net-snmp-libs >= 5.7.1
Obsoletes:	ethereal-gnome
Provides:	ethereal-gnome

%description
Wireshark is a network traffic analyzer for Unix-ish operating systems.

This package lays base for libpcap, a packet capture and filtering 
library, contains command-line utilities, contains plugins and 
documentation for wireshark. A graphical user interface is packaged 
separately to GTK+ package.

%description gnome
Contains wireshark for Gnome 2 and desktop integration file

%prep
%setup -q -n %{name}-%{version}
#%%setup -q -n %{name}-%{version}%{pre_ver}
%patch1 -p1 -b .pie
##%%patch3 -p1 
%patch4 -p1 -b .enable-lua

%build

%configure --sysconfdir=%{_sysconfdir}/wireshark \
   --bindir=%{_sbindir} \
   --enable-zlib \
   --enable-ipv6 \
   --with-net-snmp \
   --with-gnu-ld \
   --disable-static \
   --disable-usr-local \
   --enable-gtk2 \
   --enable-threads \
   --with-pic \
   --with-ssl \
   --disable-warnings-as-errors \
   --with-plugindir=%{_libdir}/%{name}/plugins/%{version}

find -name "Makefile" | xargs perl -p -i -e \
     's,-I%{_includedir} ,,;s,-I/usr/local/include ,,'

time make %{?_smp_mflags}

%install
%{__rm} -rf --preserve-root %{buildroot}

%{__install} -m 0755 -d %{buildroot}%{_datadir}/applications
%{__install} -m 0644 %{SOURCE3} %{buildroot}%{_datadir}/applications

# The evil plugins hack
perl -pi -e 's|-L../../epan|-L../../epan/.libs|' plugins/*/*.la

make DESTDIR=%{buildroot} install transform='s,x,x,'

#symlink tshark to tethereal
ln -s tshark %{buildroot}%{_sbindir}/tethereal

#empty?!
rm -f %{buildroot}%{_sbindir}/idl2wrs

# install support files for usermode, gnome and kde
mkdir -p %{buildroot}/%{_sysconfdir}/pam.d
install -m 644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/pam.d/wireshark
mkdir -p %{buildroot}/%{_sysconfdir}/security/console.apps
install -m 644 %{SOURCE2} %{buildroot}/%{_sysconfdir}/security/console.apps/wireshark
mkdir -p %{buildroot}/%{_bindir}
ln -s consolehelper %{buildroot}/%{_bindir}/wireshark

# fullpath for desktop file
sed -i -e 's@TryExec=wireshark@TryExec=/usr/bin/wireshark@' \
    -e 's@Exec=wireshark@Exec=/usr/bin/wireshark@' \
    %{buildroot}%{_datadir}/applications/wireshark.desktop

# Install python stuff.
mkdir -p %{buildroot}%{python_sitelib}
install -m 644 tools/wireshark_be.py tools/wireshark_gen.py  %{buildroot}%{python_sitelib}

mkdir -p %{buildroot}/%{_datadir}/pixmaps
install -m 644 image/wsicon48.png %{buildroot}/%{_datadir}/pixmaps/wireshark.png

%{__chmod} -x doc/*.pl

find %{buildroot} -name "*.la" -delete

%clean
%{__rm} -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README* 
%{_sbindir}/editcap
#%{_sbindir}/idl2wrs
%{_sbindir}/tshark
%{_sbindir}/mergecap
%{_sbindir}/text2pcap
%{_sbindir}/dftest
%{_sbindir}/capinfos
%{_sbindir}/randpkt
%{_sbindir}/dumpcap
%{_sbindir}/tethereal
%{_sbindir}/rawshark
%{_sbindir}/reordercap
%{python_sitelib}/*
%{_libdir}/lib*
%{_mandir}/man1/editcap.*
%{_mandir}/man1/tshark.*
#%{_mandir}/man1/idl2wrs.*
%{_mandir}/man1/mergecap.*
%{_mandir}/man1/text2pcap.*
%{_mandir}/man1/capinfos.*
%{_mandir}/man1/dumpcap.*
%{_mandir}/man1/rawshark.*
%{_mandir}/man1/dftest.*
%{_mandir}/man1/randpkt.*
%{_mandir}/man1/reordercap.*
%{_mandir}/man4/wireshark-filter.*
%{_libdir}/wireshark
%config(noreplace) %{_sysconfdir}/pam.d/wireshark
%config(noreplace) %{_sysconfdir}/security/console.apps/wireshark
%{_datadir}/wireshark

%files gnome
%defattr(-,root,root)
%{_datadir}/applications/wireshark.desktop
%{_datadir}/pixmaps/wireshark.png
%{_bindir}/wireshark
%{_sbindir}/wireshark
%{_mandir}/man1/wireshark.*

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.8-1m)
- update 1.10.8

* Sat Apr 26 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.7-1m)
- [SECURITY] CVE-2014-2907
- update to 1.10.7

* Wed Mar 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.6-1m)
- [SECURITY] CVE-2014-2281 CVE-2014-2282 CVE-2014-2283 CVE-2014-2299
- update to 1.10.6

* Sat Dec 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.4-1m)
- [SECURITY] CVE-2013-7112 CVE-2013-7113 CVE-2013-7114
- update to 1.10.4

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.3-1m)
- [SECURITY] CVE-2013-6336 CVE-2013-6337 CVE-2013-6338 CVE-2013-6339
- [SECURITY] CVE-2013-6340
- update to 1.10.3

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-1m)
- [SECURITY] CVE-2013-4933 CVE-2013-5717 CVE-2013-5718 CVE-2013-5719
- [SECURITY] CVE-2013-5720 cVE-2013-5721 CVE-2013-5722
- update to 1.10.2

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.1-1m)
- [SECURITY] CVE-2013-4083 CVE-2013-4920 CVE-2013-4921 CVE-2013-4922
- [SECURITY] CVE-2013-4923 CVE-2013-4924 CVE-2013-4925 CVE-2013-4926
- [SECURITY] CVE-2013-4927 CVE-2013-4928 CVE-2013-4929 CVE-2013-4930
- [SECURITY] CVE-2013-4931 CVE-2013-4932 CVE-2013-4933 CVE-2013-4934
- [SECURITY] CVE-2013-4935 CVE-2013-4936
- update to 1.10.1

* Tue Jun 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.0-2m)
- fix flow graph crash
- rebuild for new libpcap-1.4.0 (now wireshark works fine on x86_64)

* Tue Jun 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.7-2m)
- rebuild against gnutls-3.2.0

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.7-1m)
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-23.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-24.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-25.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-26.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-27.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-28.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-29.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-30.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-31.html
- update to 1.8.7

* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6-1m)
- [SECURITY] CVE-2013-2475 CVE-2013-2476 CVE-2013-2477 CVE-2013-2478
- [SECURITY] CVE-2013-2479 CVE-2013-2480 CVE-2013-2481 CVE-2013-2482
- [SECURITY] CVE-2013-2483 CVE-2013-2484 CVE-2013-2485 CVE-2013-2486
- [SECURITY] CVE-2013-2487 CVE-2013-2488
- update to 1.8.6

* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-1m)
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-01.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-02.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-03.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-04.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-05.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-06.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-07.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-08.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2013-09.html
- update to 1.8.5

* Fri Nov 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.4-1m)
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-30.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-31.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-32.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-33.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-34.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-35.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-36.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-37.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-38.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-39.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2012-40.html
- update to 1.8.4

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3-1m)
- [SECURITY] CVE-2012-5237 CVE-2012-5238 CVE-2012-5239 CVE-2012-5240
- update to 1.8.3

* Fri Aug 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-1m)
- [SECURITY] CVE-2012-4285 CVE-2012-4286 CVE-2012-4287 CVE-2012-4288
- [SECURITY] CVE-2012-4289 CVE-2012-4290 CVE-2012-4291 CVE-2012-4292
- [SECURITY] CVE-2012-4293 CVE-2012-4294 CVE-2012-4295 CVE-2012-4296
- [SECURITY] CVE-2012-4297 CVE-2012-4298
- update to 1.8.2

* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- [SECURITY] CVE-2012-4048 CVE-2012-4049
- update to 1.8.1

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-2m)
- fix link error for new glib2

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update 1.8.0

* Fri May 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-2m)
- [SECURITY] CVE-2012-2392 CVE-2012-2393 CVE-2012-2394

* Thu Apr 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- [SECURITY] CVE-2012-0041 CVE-2012-0042 CVE-2012-0043 CVE-2012-0066
- [SECURITY] CVE-2012-0067 CVE-2012-0068 CVE-2012-1593 CVE-2012-1594
- [SECURITY] CVE-2012-1595 CVE-2012-1596
- update to 1.7.1

* Wed Jan  4 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.0-1m)
- update 1.7.0
- remove Patch3: wireshark-1.6.0-nfsv4-opts.patch

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update 1.6.4

* Thu Nov  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-17.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-18.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-19.html
- update to 1.6.3

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.2-2m)
- rebuild against net-snmp 5.7.1

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-12.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-13.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-14.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-15.html
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-16.html
- update to 1.6.2

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- [SECURITY] CVE-2011-2597 CVE-2011-2698
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-11.html
- update to 1.6.1

* Sun Jun 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0
- we do not need "-O0" any more...

* Sat Jun  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-1m)
- [SECURITY] CVE-2011-1957 CVE-2011-1958 CVE-2011-1959 CVE-2011-2174
- [SECURITY] CVE-2011-2175
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-08.html
- update to 1.4.7

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.6-3m)
- rebuild against python-2.7

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-2m)
- use -O1 in CFLAGS for new gcc-4.6

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Wed Apr 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2011-06.html
- update to 1.4.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-2m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- [SECURITY] CVE-2011-0538 CVE-2011-0713
- update to 1.4.4

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- [SECURITY] CVE-2010-4538 CVE-2011-0444 CVE-2011-0445
- [SECURITY] patch for CVE-2010-4538 was applied to new upstream release
- update to 1.4.3

* Mon Jan 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-3m)
- [SECURITY] CVE-2010-4538
- apply upstream patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-1m)
- [SECURITY] CVE-2010-4300 CVE-2010-4301
- [SECURITY] wnpa-sec-2010-14
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2010-14.html
- update to 1.4.2

* Mon Oct 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-2m)
- enable lua

* Tue Oct 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- [SECURITY] CVE-2010-3445
- [SECURITY] wnpa-sec-2010-12 http://www.wireshark.org/security/wnpa-sec-2010-12.html
- update to 1.4.1

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- [SECURITY] CVE-2010-3133 (perhaps, windows only)
- update to 1.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-1m)
- [SECURITY] CVE-2010-2284 CVE-2010-2287
- update to 1.2.10

* Fri Jun 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.9-1m)
- [SECURITY] CVE-2010-2283 CVE-2010-2284 CVE-2010-2285 CVE-2010-2286
- [SECURITY] CVE-2010-2287
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2010-06.html
- update to 1.2.9

* Fri May  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8-1m)
- [SECURITY] CVE-2010-1455
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2010-04.html
- update to 1.2.8

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-3m)
- rebuild against libpcap-1.1.1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-2m)
- rebuild against openssl-1.0.0

* Sat Apr  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6-1m)
- [SECURITY] CVE-2009-2563 CVE-2009-4377 CVE-2010-0304
- [SECURITY] see http://www.wireshark.org/security/wnpa-sec-2010-02.html

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-2m)
- delete __libtoolize hack

* Mon Dec 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- [SECURITY] CVE-2009-4376 CVE-2009-4377 CVE-2009-4378
- [SECURITY] The Daintree SNA file parser could overflow a buffer.
- [SECURITY] The SMB and SMB2 dissectors could crash.
- [SECURITY] The IPMI dissector could crash on Windows.
- [SECURITY] see http://www.wireshark.org/security/wnpa-sec-2009-09.html

* Sat Nov 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- [SECURITY] CVE-2009-3549 CVE-2009-3550 CVE-2009-3551
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2009-07.html
- update to 1.2.3

* Mon Sep 21 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.2-1m)
- [SECURITY] CVE-2009-3243 CVE-2009-3242 CVE-2009-3241
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2009-06.html
- update to 1.2.2

* Wed Jul 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- [SECURITY] CVE-2009-2559 CVE-2009-2560 CVE-2009-2561 CVE-2009-2562 CVE-2009-2563
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2009-04.html
- update to 1.2.1

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Wed May 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- [SECURITY] CVE-2009-1829
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2009-03.html
- update to 1.0.8

* Sun Apr 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-1m)
- [SECURITY] CVE-2009-1210 CVE-2009-1266 CVE-2009-1267 CVE-2009-1268 CVE-2009-1269
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2009-02.html
- update to 1.0.7

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against openssl-0.9.8k

* Mon Feb  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-1m)
- [SECURITY] CVE-2009-0599 CVE-2009-0600 CVE-2009-0601
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2009-01.html
- update to 1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-4m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-3m)
- update Patch1,3 for fuzz=0

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.5-2m)
- rebuild against python-2.6.1-2m

* Fri Dec 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5
-- this version includes CVE-2008-5285 fix

* Mon Nov 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-0.1.1m)
- [SECURITY] CVE-2008-5285 CVE-2008-6472
- update to 1.0.5pre1-26829

* Wed Oct 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- [SECURITY] Wireshark Multiple Denial of Service Vulnerabilities
- [SECURITY] CVE-2008-4680 CVE-2008-4681 CVE-2008-4682
- [SECURITY] CVE-2008-4683 CVE-2008-4684 CVE-2008-4685
- see http://www.wireshark.org/security/wnpa-sec-2008-06.html for more details
- update to 1.0.4

* Fri Sep  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- [SECURITY] CVE-2008-3146 CVE-2008-3932 CVE-2008-3933 CVE-2008-3934
- update to 1.0.3

* Thu Jul 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-3m)
- rebuild against portaudio-19

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gnutls-2.4.1

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- [SECURITY] CVE-2008-3145
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2008-04.html
- update to 1.0.2

* Wed Jul  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- [SECURITY] CVE-2008-3137 CVE-2008-3138 CVE-2008-3139 CVE-2008-3140 CVE-2008-3141
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2008-03.html
- update to 1.0.1

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc43

* Sun Mar 30 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.0-1m)
- up to 1.0.0
- [SECURITY] CVE-2008-1561 CVE-2008-1562 CVE-2008-1563
- [SECURITY] http://www.wireshark.org/security/wnpa-sec-2008-02.html

* Fri Feb 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-1m)
- [SECURITY] Wireshark Multiple Denial of Service Vulnerabilities   	
- [SECURITY] CVE-2008-1070 CVE-2008-1071 CVE-2008-1072
- update to 0.99.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.7-2m)
- %%NoSource -> NoSource

* Sat Dec 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.7-1m)
- [SECURITY] CVE-2007-6438 CVE-2007-6439 CVE-2007-6440 
- [SECURITY] CVE-2007-6441 CVE-2007-6442 CVE-2007-6443 
- [SECURITY] CVE-2007-6444 CVE-2007-6445 CVE-2007-6446 
- [SECURITY] CVE-2007-6447 CVE-2007-6448 CVE-2007-6449 
- [SECURITY] CVE-2007-6450 CVE-2007-6451
- update to 0.99.7

* Tue Aug 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.6-2m)
- add configure option --disable-warnings-as-errors

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.6-1m)
- update to 0.99.6

* Sun Jul 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.5-8m)
- modify desktop file (full path)

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-7m)
- update wireshark.desktop
  remove Categories X-Red-Hat-Extra and Application
  re-add Name[ja]

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.5-6m)
- separate gnome subpackage
- add 
  Source1: wireshark.pam
  Source2: wireshark.console
  Source3: wireshark.desktop
  Patch1: wireshark-0.99.5-pie.patch
  Patch3: wireshark-nfsv4-opts.patch

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.5-5m)
- rebuild against net-snmp

* Thu Jun 07 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.5-4m)
- rebuild against libpcap-0.9.5 (restrict by version)

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.5-3m)
- rebuild against libpcap-0.9.5

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.5-2m)
- delete libtool library

* Mon Feb 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.5-1m)
- update to 0.99.5
- [SECURITY] Multiple problems are fixed in this version
  CVE-2007-0459 CVE-2007-0458 CVE-2007-0457 CVE-2007-0456
  see http://www.wireshark.org/security/wnpa-sec-2007-01.html

* Wed Nov  1 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.4-1m)
- update to 0.99.4
- [SECURITY] fix following Multiple Denial of Service Vulnerabilities
  CVE-2006-5468 CVE-2006-5740 CVE-2006-4805 CVE-2006-5469 CVE-2006-4574 CVE-2006-5595

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.99.3a-2m)
- rebuild against gnutls-1.4.4-1m

* Sun Sep  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.99.3a-1m)
- update to 0.99.3a
- [SECURITY] CVE-2006-4330 CVE-2006-4333 CVE-2006-4331 CVE-2006-4332
- http://www.wireshark.org/security/wnpa-sec-2006-02.html

* Wed Aug  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.2-3m)
- rebuild against net-snmp 5.3.1.0

* Sun Jul 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.2-2m)
- add Obsoletes: ethereal for upgrade from ethereal

* Sat Jul 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.99.2-1m)
- The Ethereal network protocol analyzer has changed its name to Wireshark.
- [SECURITY] CVE-2006-3627 CVE-2006-3628 CVE-2006-3629 CVE-2006-3630
             CVE-2006-3631 CVE-2006-3632

* Tue May 23 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.99.0-2m)
- rebuild against rpm 4.4.2

* Tue Apr 25 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.99.0-1m)
- [SECURITY] CVE-2006-1932 CVE-2006-1933 CVE-2006-1934 CVE-2006-1935
             CVE-2006-1936 CVE-2006-1937 CVE-2006-1938 CVE-2006-1939
             CVE-2006-1940 

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.14-2m)
- rebuild against openssl-0.9.8a

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.10.14-1m)
- update to 0.10.14
- drop patch0 : fixed in upstream

* Tue Dec 13 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.10.13-1m)
- update to 0.10.13
- import ospf patch from CVS (CVE-2005-3651)

* Mon Aug  1 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.10.12-1m)
- up to 0.10.12
- [SECUIRTY] CAN-2005-2361 - CAN-2005-2367

* Sun May  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.11m)
- major security fixes (CAN-2005-1456 - CAN-2005-1470)

* Thu Mar 14 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.10.10-1m)
- version up. includes
- The COPS dissector could go into an infinite loop (CAN-2005-0006).
- The DLSw dissector could cause an assertion, making Ethereal exit
  prematurely (CAN-2005-0007).
- The DNP dissector could cause memory corruption (CAN-2005-0008).
- The Gnutella dissector could cause an assertion, making Ethereal
  exit prematurely (CAN-2005-0009).
- The MMSE dissector could free statically-allocated memory
  (CAN-2005-0010).
- The X11 dissector is vulnerable to a string buffer overflow
  (CAN-2005-0084).


* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.9-4m)
- move ethereal.desktop to %%{_datadir}/applications/
- update Source3: ethereal.desktop
- import Source1: ethereal.png from Fedora Core

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.10.9-3m)
- enable x86_64.

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (0.10.9-2m)
- applied patch for ltmain.sh to find dependency out from itself.

* Fri Jan 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.9-1m)
- major security fixes

* Thu Dec 16 2004 Mitsuru Shimaura <smbd@momonga-linux.org>
- (0.10.8-1m)
- up to 0.10.8
- [SECURITY] Ethereal Multiple Vulnerabilities
  http://secunia.com/advisories/13468/

* Thu Aug 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.5-2m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Thu Jul  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.5-1m)
- minor security fixes

* Fri May 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.4-1m)
- fixes security-related bugs in the AIM, MMSE, SIP, and SPNEGO
  dissectors

* Sat Mar 27 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.3-1m)
- security fix http://www.ethereal.com/appnotes/enpa-sa-00013.html
- revise specfile

* Wed Feb 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.2-1m)
- minor feature enhancements

* Tue Dec 16 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0a
  Security problems in Ethereal 0.9.16
  http://www.ethereal.com/appnotes/enpa-sa-00012.html

* Thu Nov 06 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.16-2m)
- use --disable-static instead of delete --enable-static at %%configure
- we can exec ethereal, but ethereal said:
  The plugin asn1.so, version 0.5.0 is an old-style plugin;
  Those are no longer supported.
  sigh!!

* Wed Nov 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.16-1m)
- update to 0.9.16
- security fix http://www.ethereal.com/appnotes/enpa-sa-00011.html
- add kludge Patch0: plugins-asn1-packet-asn1.c.patch
- use %%global
- add BuildPreReq: elfutils-libelf and elfutils-devel
  for link libelf.so.*
- add BuildPreReq: atk-devel >= 1.4.1-3m
  for link libatk-1.0.so.*
-
- !!! CAUTION !!!
- this release was broken, please do not use
- compile message:
- epan/libethereal.a(filesystem.o)(.text+0x1e6): In function
- get_persconffile_dir':
- : warning: Using 'getpwuid' in statically linked applications requires at
- runtime the shared libraries from the glibc version used for linking
-
- message when exec ethereal
- $ sudo ethereal &
- sudo: unable to exec /usr/bin/ethereal: No such file or directory

* Tue Jul 29 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (0.9.14-1m)
- update to 0.9.14

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.13-2m)
- rebuild against glibc-2.3.2

* Thu Jun 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.13-1m)
  update to 0.9.13

* Fri May  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-1m)
- minor security fixes

* Sat Mar 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.11-1m)
- update to 0.9.11

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.10-3m)
- rebuild against for XFree86-4.3.0

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.10-2m)
- rebuild against openssl 0.9.7a

* Sun Mar  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.10-1m)
- minor security fixes
- use gtk2

* Fri Feb 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.9-1m)
- version 0.9.9

* Sun Dec  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.8-1m)
- update to 0.9.8 which includes security fix
- add more documents

* Sun Nov 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.7-3m)
- use automake-1.7

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.7-2m)
- rebuild against gcc-3.2 with autoconf-2.53

* Sun Oct  6 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.7-1m)
- upgrade to 0.9.7

* Thu Aug 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.6-2m)
- rebuild against net-snmp-5.0.3-3m

* Wed Aug 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.6-1m)
- minor security fixes

* Sat Aug 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.5-2m)
- rebuild against net-snmp

* Tue Jul  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.5-1m)
- update to 0.9.5

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.9.4-6k)
- cancel gcc-3.1 autoconf-2.53

* Fri May 24 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.4-4k)
  for gcc 3.1

* Mon May 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.9.4-2k)
- enable snmp (I don't know why snmp had been disabled...)

* Sun Mar 31 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.9.3-2k)

* Sat Mar 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.9.2-2k)

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.1-2k)
- update to 0.9.1

* Wed Jan  2 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.0-2k)
- version 0.9.0

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (0.8.20-4k)
- No docs could be excutable :-p

* Tue Oct 16 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.8.20-2k)

* Thu Mar 22 2001 Motonobu Ichimura <famao@kondara.org>
- (0.8.14-7k)
- rebuild against glibc-2.2.2

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.8.14-5k)
- changable IPv6 function with %{_ipv6} macro

* Wed Dec 13 2000 AYUHANA Tomonori <l@kondara.org>
- (0.8.14-2k)
- backport 0.8.14-3k for 2.0

* Thu Dec  7 2000 AYUHANA Tomonori <l@kondara.org>
- (0.8.14-3k)
- version 0.8.14
- add BuildRequires: flex, glib1-devel

* Wed Oct 11 2000 Motonobu Ichimura <famao@kondara.org>
- Version up tarball. (0.8.7 -> 0.8.12)

* Tue Apr 25 2000 Chiaki Nakata <hanibi@kondara.org>
- Version up tarball. (0.8.3 -> 0.8.7)

* Sun Feb 13 2000 Toru Hoshina <t@kondara.org>
- Version up tarball. (0.8.0 -> 0.8.3)

* Sat Jan  8 2000 Kazuaki Yoshida <kazuaki@nifty.co.jp>
- Version up tarball. (0.7.9 -> 0.8.0)

* Mon Dec  6 1999 Kazuaki Yoshida <kazuaki@nifty.co.jp>
- Version up tarball. (0.7.1 -> 0.7.9)
- Add "ethereal.png", "ethereal.desktop".
- change "Group".
- change to nosrc.rpm.
- Adjust to the reglation of "Kondara MNU/Linux".

* Sat Aug  7 1999 Yoshida <kazuaki@nifty.co.jp>
- 1st Release for Kondara MNU/Linux Users (glibc2.1).


