%global momorel 6

Summary: momonga nonfrees package build tool
Name: momonga-nonfree-package-builder
Version: 0.1.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.momonga-linux.org/
Group: Development/Tools
Source0: %{name}-%{version}.tar.bz2
Source1: %{name}.consolehelper
Source2: %{name}.pam
Source3: %{name}.desktop
Source5: nonfree.csv
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: createrepo
Requires: gawk
Requires: redhat-lsb
Requires: rpm
Requires: ruby
Requires: ruby-rpm
Requires: subversion
Requires: usermode
Requires: wget
Requires: yum
Requires: zenity

Obsoletes: momonga-nonfrees-packager

%description
%{name}
It's a test release...
Build Nonfree package and put it in /root ;-p

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# add consolehelper script
mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps/
cp %{SOURCE1} %{buildroot}%{_sysconfdir}/security/console.apps/%{name}
mkdir -p %{buildroot}%{_bindir}
(cd %{buildroot}%{_bindir}; ln -sf consolehelper %{name})
touch %{buildroot}%{_sysconfdir}/security/console.apps/%{name}

mkdir -p %{buildroot}%{_sysconfdir}/pam.d/
cp %{SOURCE2} %{buildroot}%{_sysconfdir}/pam.d/%{name}

# add to menu
mkdir -p %{buildroot}%{_datadir}/applications
cp %{SOURCE3} %{buildroot}%{_datadir}/applications

install -p -m 644 %{SOURCE5} %{buildroot}%{_datadir}/%{name}/nonfree.csv

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_sysconfdir}/pam.d/%{name}
%{_sysconfdir}/security/console.apps/%{name}
%{_bindir}/%{name}
%{_sbindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop

%changelog
* Sun Feb 12 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.0-6m)
- modify desktop and nonfree.csv
-- NotShowIn=GNOME; use mnpb :-D

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-4m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-3m)
- enable flashplayer-plugin on x86_64

* Mon Sep 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-2m)
- fix typo

* Sun Sep 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-1m)
- update to 0.1.0
-- fix MO_SVN for STABLE_7

* Wed Sep  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.19-1m)
- update to 0.0.19
-- save proxy settings

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.18-2m)
- full rebuild for mo7 release

* Sun Aug 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.18-1m)
- update to 0.0.18

* Sat Aug 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.17-1m)
- update to 0.0.17
-- much improve messages
- License: GPLv2+
- i'd like to release next version as 0.1.0

* Fri Aug 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.16-1m)
- update to 0.0.16

* Fri Aug 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.15-1m)
- update to 0.0.15
-- correct yum installation
-- use nonfree.csv

* Thu Aug 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14-1m)
- update to 0.0.14
-- momonga-nonfree-package-builder: add actions for ruby script
-- momonga-nonfree-package-builder: delete ping script
-- momonga-nonfree-package-builder.rb: bug fix for yum

* Wed Aug 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.13-1m)
- update to 0.0.13
-- momonga-nonfree-package-builder.rb checks nonfree.csv

* Wed Aug 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.12-1m)
- update to 0.0.12
-- momonga-nonfree-package-builder:
--- do not remove generated binary packages and build log
-- momonga-nonfree-package-builder.rb:
--- check network status
--- recognize build host arch
--- always run yum
--- check checksums of downloaded files
--- support proxy host and port (initial implementation)
- drop some packages from nonfree.list
-- ghostscript-epson intel-cc java-activation java-java3d
-- java-javamail java-jmx realplayer8 truecrypt truetype-fonts-bakoma
-- wnn6-support xmascot

* Tue Aug 24 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.11-1m)
- update to 0.0.11
- add error check (ruby script exit status)
- add network check (use ping -q -c)

* Tue Aug 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.10-2m)
- add Requires: ruby-rpm

* Mon Aug 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.10-1m)
- update to 0.0.10
-- define error exit statuses

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-1m)
- update to 0.0.9
-- drop Requires: build-momonga
-- instead, build-momonga will be installed at runtime

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.8-1m)
- update to 0.0.8
-- fallback to trunk when the target branch does not exist
- BuildArch: noarch

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.7-3m)
- add Source4: nonfree.list and add xvid to it

* Sun Aug 22 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.7-2m)
- add some Req's

* Wed Aug 18 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.7-1m)
- update to 0.0.7

* Wed Aug  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-1m)
- update to 0.0.6
-- rename ruby script file name and add license terms to it
-- support Momonga Linux 7 (STABLE_7)

* Wed Aug  4 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.5-1m)
- rename package name 
-- from momonga-nonfrees-packager
-- to momonga-nonfree-package-builder

* Wed Aug  4 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.4-1m)
- update to 0.0.4 (fix filename)

* Wed Aug  4 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.3-1m)
- update to 0.0.3 (sukosi masi release)

* Tue Aug  3 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.2-1m)
- update to 0.0.2 (mada mada dame release)

* Mon Aug  2 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.1-2m)
- add Requires: build-momonga

* Mon Aug  2 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.0.1-1m)
- initial build... mada mada
- Build Nonfree package and put it in /root ;-p
