%global         momorel 1

Summary:        A Tool for manipulating BibTeX data bases
Name:           BibTool
Version:        2.55
Release:        %{momorel}m%{?dist}
Group:          Applications/Publishing
Source:         ftp://ftp.dante.de/tex-archive/biblio/bibtex/utils/bibtool/BibTool-%{version}.tar.gz
NoSource:	0
Patch0:         BibTool-2.53-regex_DESTDIR.patch
Patch1:         BibTool-2.55-cwdPATH.patch
URL:            http://www.gerd-neugebauer.de/software/TeX/BibTool.en.html
License:        GPL+
BuildRequires:  kpathsea
BuildRequires:  tetex-latex
BuildRequires:  texlive-web2c
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
BibTeX provides an easy to use means to integrate citations and
bibliographies into LaTeX documents. But the user is left alone with
the management of the BibTeX files. The program BibTool is intended to
fill this gap. BibTool allows the manipulation of BibTeX files which
goes beyond the possibilities --- and intentions --- of BibTeX.

%prep
%setup -q -n %{name}
%patch0 -p1 -b .regex_DESTDIR
%patch1 -p1 -b .cwdPATH
sed -i -e 's%^#!/usr/local/bin/tclsh%#! %{_bindir}/tclsh%' Tcl/bibtool.tcl
sed -i -e 's%^#!/usr/local/bin/perl%#! %{_bindir}/perl%' Perl/bibtool.pl
# configure will recreate the directory, but only with config.h within
rm -rf regex-0.12

%build
%configure --libdir=%{_datadir}
sed -i -e 's#@kpathsea_lib_static@##' makefile
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"
make doc

%install
rm -rf $RPM_BUILD_ROOT
make install install-man DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes.tex COPYING INSTALL README THANKS 
%doc Doc/bibtool.pdf Doc/ref_card.pdf
%doc Perl/ Tcl/
%{_bindir}/bibtool
%{_datadir}/BibTool/
%{_mandir}/man1/bibtool.1*

%changelog
* Mon Aug 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.55-1m)
- update to 2.55

* Tue Dec  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-1m)
- update to 2.53
- add BuildRequires: kpathsea and texlive-web2c

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.48-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.48-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.48-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.48-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.48-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 14 2008 Patrice Dumas <pertusus@free.fr> 2.48-7
- add #define __USE_GNU since in regex GNU extensions are used
- keep timestamps and fix man page permissions

* Mon Jan 29 2007 Patrice Dumas <pertusus@free.fr> 2.48-6
- use system regex (#225108)
- honor optflags (#225108)
- merge honor_DESTDIR diff with regex changes in regex_DESTDIR
- don't ship c_lib.dvi

* Sun Sep 10 2006 Patrice Dumas <pertusus@free.fr> 2.48-5
- rebuild for FC6

* Thu Feb 16 2006 Patrice Dumas <pertusus@free.fr> 2.48-4
- rebuild for fc5

* Fri Sep  2 2005 Patrice Dumas <pertusus@free.fr> 2.48-3
- change shebangs in example scripts

* Thu Jul 14 2005 Patrice Dumas <pertusus@free.fr> 2.48-2
- update to 2.48
- use fedora template 

* Mon Sep 10 2001 Lenny Cartier <lenny@mandrakesoft.com> 2.44-1mdk
- added in contribs by Guillaume Rousse <g.rousse@linux-mandrake.com>
- first mdk release
