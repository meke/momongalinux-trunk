%global momorel 34
%global binutilsver 2.21.90
%global binutilsrel 4m%{?dist}
%global qtver 3.3.7
%global qtrel 17m
%global qtdir %{_libdir}/qt3
%global real_ver 3_0_14_0196-Public

Summary:       CodeAnalyst is a Performance Analysis Suite for AMD-based System
Name:          CodeAnalyst-gui
Version:       2.9.18
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         Development/System
URL:           http://developer.amd.com/cpu/CodeAnalyst/codeanalystlinux
#Source0:       http://developer.amd.com/Downloads/CodeAnalyst-%{real_ver}.tar.gz
Source0:       http://developer.amd.com/Downloads/codeanalyst-%{version}.tar.gz
Source1:       CodeAnalyst-gui.desktop
Source2:       DiffAnalyst-gui.desktop

# Use oprofile default cpu/watershed/event-buffer size
# since using stock oprofile daemon/driver
Patch0:        ca-use-oprofile-default-buffersize.patch

# This patch allows us to use the DESTDIR variable in install section.
Patch1:        ca-destdir.patch

# This patch allows to succesfully build with --disable-dwarf
Patch2:        ca-disable-dwarf.patch

# This patch allows to CA to be configured with any libdwarf
Patch3:        ca-configure-libdwarf.patch

# This patch fix the splash screen
Patch4:        ca-fix-splash.patch

# initscripts patch
Patch5:        CodeAnalyst-gui-init.patch

Requires:      popt
Requires:      binutils = %{binutilsver}-%{binutilsrel}
Requires:      elfutils-libelf
Requires:      qt3
Requires:      oprofile >= 0.9.4
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig 
# This is for /sbin/service
Requires(preun): initscripts
Requires(postun): initscripts

BuildRequires: automake
BuildRequires: libtool
BuildRequires: popt-devel
BuildRequires: binutils-devel = %{binutilsver}-%{binutilsrel}
BuildRequires: elfutils-libelf-devel
BuildRequires: qt3-devel >= 3.3
BuildRequires: qt3-designer >= 3.3
BuildRequires: oprofile >= 0.9.4
BuildRequires: desktop-file-utils
BuildRequires: libdwarf-devel >= 0.20100629

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

# Unsupported architecture list due to non-AMD based system.
ExcludeArch: ppc ppc64 s390 s390x alpha alphaev6 sparcv9 sparc64


%description
CodeAnalyst is a performance analysis suite. It provids graphical
utilitys for running Oprofile, and analyzing result on AMD-based systems.
CodeAnalyst includes several features to aid profile analysis such as
inline-function analysis, code-block analysis, and a utility for
profile comparison, DiffAnalayst.


%prep
# %setup -q -n CodeAnalyst-%{real_ver}
%setup -q -n codeanalyst-%{version}
#%patch0 -p1 -b .ca-use-oprofile-default-buffersize
#%patch1 -p0 -b .ca-destdir
#%patch2 -p0 -b .ca-disable-dwarf
#%patch3 -p1 -b .ca-configure-libdwarf
#%patch4 -p1 -b .ca-fix-splash
%patch5 -p1 -b .stop-daemon

# remove "--no-undefined" from LDFLAGS
find -name Makefile.am | xargs sed --in-place=.no-undefined~ -e 's,--no-undefined,,g'

%build
export QTDIR=%{qtdir} QTLIB=%{qtdir}/lib

./autogen.sh
%configure \
  --with-oprofile=%{_prefix} \
  --disable-oprofile-lib \
  --disable-static \
  --with-libdwarf-includes=%{_includedir}/libdwarf \
  --with-libdwarf-libraries=%{_libdir} \
  --with-qt-dir=%{qtdir} \
  --with-qt-libraries=%{qtdir}/lib

make %{?_smp_mflags} all \
  CFLAGS="${RPM_OPT_FLAGS}" \
  CXXFLAGS="${RPM_OPT_FLAGS}"


%install
rm -rf ${RPM_BUILD_ROOT}

make -C src/ca/libs    install DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p"
make -C src/ca/gui     install DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p"
make -C src/ca/diffgui install DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p"
make -C src/ca/utils   install DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p"
make -C src/ca/scripts install DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p"

# These are help documents and images which
# the GUIs is using for the "Help" on toolbar.
# GUI will not run correctly if these are not available
make -C doc install DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p"

install -pD -m 755 src/ca/scripts/codeanalyst ${RPM_BUILD_ROOT}%{_initscriptdir}/codeanalyst

install -pD -m 755 careport.sh ${RPM_BUILD_ROOT}%{_bindir}/careport.sh

# Remove these unnecessary files from the installation
rm -rf ${RPM_BUILD_ROOT}%{_libdir}/lib*.{la,so}

# Install CodeAnalyst-gui.desktop file
desktop-file-install --vendor="" --dir=${RPM_BUILD_ROOT}%{_datadir}/applications %{SOURCE1}

# Install DiffAnalyst-gui.desktop file
desktop-file-install --vendor="" --dir=${RPM_BUILD_ROOT}%{_datadir}/applications %{SOURCE2}


%clean
rm -rf ${RPM_BUILD_ROOT}


%pre
# Adding "amdca" user group
getent group amdca >/dev/null || /usr/sbin/groupadd -r amdca
exit 0


%post
/sbin/ldconfig

# Install init script
/sbin/chkconfig --add codeanalyst


%preun
# Deinit 
if [ $1 = 0 ] ; then
    /sbin/service codeanalyst stop >/dev/null 2>&1
    /sbin/chkconfig --del codeanalyst
fi
exit 0


%postun
/sbin/ldconfig
if [ "$1" -ge "1" ] ; then
    /sbin/service codeanalyst condrestart >/dev/null 2>&1 || :
fi


%files
%defattr(-,root,root,-)
%doc README COPYING INSTALLATION samples
%{_bindir}/CodeAnalyst
%{_bindir}/DiffAnalyst
%{_bindir}/careport.sh
%{_bindir}/capackage.sh
%{_sbindir}/ca_user_manager
%{_sbindir}/ca_oprofile_controller
%{_libdir}/lib*.so.*
%dir %{_datadir}/codeanalyst
%{_datadir}/codeanalyst/*
%{_datadir}/applications/CodeAnalyst-gui.desktop
%{_datadir}/applications/DiffAnalyst-gui.desktop
%{_initscriptdir}/codeanalyst


%changelog
* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-34m)
- rebuild against binutils-2.21.90-4m

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-33m)
- rebuild against binutils-2.21.90-3m

* Thu Oct 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-32m)
- fix build error

* Thu Oct 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-31m)
- rebuild against binutils-2.21.90-2m

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-30m)
- rebuild against binutils-2.21.53.0.2-1m

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-29m)
- rebuild against binutils-2.21.53.0.1-2m
- delete "BuildRequires: binutils-static"

* Tue Jul 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-28m)
- rebuild against binutils-2.21.53.0.1-1m

* Thu Jul 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-27m)
- rebuild against binutils-2.21.52.0.2-3m

* Thu Jun 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-26m)
- rebuild against binutils-2.21.52.0.2-1m

* Thu Jun  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-25m)
- rebuild against binutils-2.21.52.0.1-1m

* Sat Jun  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-24m)
- rebuild against binutils-2.21.51.0.8-2m

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-23m)
- rebuild against binutils-2.21.51.0.9-1m

* Mon Apr 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-22m)
- rebuild against binutils-2.21.51.0.8-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-21m)
- fix build failure with GCC 4.6

* Mon Apr 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-20m)
- rebuild against binutils-2.21.51.0.7-2m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-19m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-18m)
- rebuild aginst binutils-2.21.51.0.7

* Fri Mar 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-17m)
- rebuild aginst binutils-2.21.51.0.6-2m

* Mon Mar  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-16m)
- rebuild against binutils-2.21.51.0.6-1m

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-15m)
- rebuild against binutils-2.21.51.0.2-2m

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-14m)
- rebuild against binutils-2.21.51.0.4-1m

* Fri Dec 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-13m)
- rebuild against binutils-2.21.51.0.2-1m

* Sun Nov 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-12m)
- rebuild against binutils-2.21.51.0.1-3m

* Sun Nov 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-11m)
- rebuild against binutils-2.21.51.0.1-2m

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-10m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-8m)
- rebuild against binutils-2.21.51.0.1-1m

* Thu Sep 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-8m)
- rebuild against binutils-2.20.51.0.11-1m

* Wed Sep 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.18-7m)
- rebuild against binutils-2.20.51.0.2-9m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.18-6m)
- full rebuild for mo7 release

* Sun Aug 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-5m)
- apply stop daemon patch

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.18-4m)
- rebuild against libdwarf-0.20100808

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.18-3m)
- rebuild against libdwarf-0.20100629

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-2m)
- rebuild against binutils-2.20.51.0.2-8m

* Sun May 16 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.18-1m)
- update to 2.9.18

* Mon May  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.38-14m)
- add "BuildRequires: binutils-static"

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-13m)
- rebuild against binutils-2.20.51.0.2-7m

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.38-12m)
- rebuild against binutils-2.20.51.0.2-6m

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-11m)
- rebuild against binutils-2.20.51.0.2-3m

* Mon Dec 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.38-10m)
- rebuild against binutils-2.19.51.0.14-6m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-8m)
- rebuild against  binutils-2.19.51.0.14-4m

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-7m)
- rebuild against binutils-2.19.51.0.14-3m

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-6m)
- rebuild against binutils-2.19.51.0.14-2m

* Thu Aug 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.38-5m)
- rebuild against binutils-2.19.51.0.14

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.38-4m)
- stop daemon

* Sat Jul  4 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.38-3m)
- add binutils version for install problem

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.38-2m)
- we must specify qt3 libraries

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.38-1m)
- import from Fedora

* Mon Apr 6 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-11
- Remove --disable-dwarf from configuration
- Add patch ca-configure-libdwarf.patch
- Add patch ca-fix-splash.patch
- configure to build with libdwarf package

* Tue Mar 31 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-10
- Rebuild with new libbfd-2.19.51.0.2-16.fc11.so

* Thu Mar 12 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-9
- Disable dwarf
- Add patch2

* Wed Mar 11 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-8
- Add "INSTALL=install -p" in install section 

* Tue Mar 10 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-7
- Update Source0 download location.
- Add sample application.

* Mon Mar 9 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-6
- Update Source0 download location.

* Thu Mar 2 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-5
- Usign "install -p"
- Using /sbin/service in preun and postun

* Thu Mar 2 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-4
- Use "configure" macro instead of ./configure
- Add patch1:ca-destdir.patch and make use of DESTDIR variable.

* Thu Mar 2 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-3
- Clean up and reorganize the spec file.
- Update "install -D -m 755"
- Explicitely declare /usr/share/codeanalyst directory in files section.
- Remove the echo in install section.

* Thu Feb 18 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-2
- Use upstream URL for source

* Thu Feb 12 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-1
- Using new buildroot string
- Remove #### partitioning lines
- Fixed the build flag (using $RPM_OPT_FLAGS)
- Change service name from codeanalyst_init to codeanalyst
- Add /var/lock/subsys/codeanalyst lock file for service start/stop
- Add version number to shared libraries (.so files)

* Thu Feb 05 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.38-0
- Add patch0: ca-use-oprofile-default-buffersize.patch
- Clean up to meet Fedora Packaging Guideline
- Bump the version due to changes in the CodeAnalyst.

* Tue Jan 27 2009 - Suravee Suthikulpanit <suravee.suthikulpanit@amd.com>
- 2.8.37-1
- Initial revision
