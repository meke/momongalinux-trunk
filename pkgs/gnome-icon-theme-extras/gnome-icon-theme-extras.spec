%global momorel 2
Summary: Additional GNOME icons
Name: gnome-icon-theme-extras
Version: 3.4.0
Release: %{momorel}m%{?dist}
#VCS: git:git://git.gnome.org/gnome-icon-theme-extras
Source0: http://download.gnome.org/sources/gnome-icon-theme-extras/3.4/%{name}-%{version}.tar.xz
NoSource: 0
License: CC-BY-SA
BuildArch: noarch
Group: User Interface/Desktops
BuildRequires: icon-naming-utils >= 0.8.7
Requires: gnome-icon-theme

%description
This package contains extra device and mime-type icons for use by
the GNOME desktop.

%prep
%setup -q

%build
%configure

%install
make install DESTDIR=%{buildroot}

%post
touch --no-create %{_datadir}/icons/gnome &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/gnome &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :

%files
%defattr(-,root,root)
%doc COPYING AUTHORS
%{_datadir}/icons/gnome/*


%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-2m)
- reimport from fedora

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-2m)
- full rebuild for mo7 release

* Wed Apr 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.11m)
- initial build

