%global momorel 1

%global    _default_patch_fuzz 2
%global    mVersion   2.23
%global    lVersion   05
%global    use_xhtml_out 0

Summary:   The Webalizer - A web server log file analysis thingie
Name:      webalizer
Version:   %{mVersion}.%{lVersion}
Release:   %{momorel}m%{?dist}
License:   GPLv2+
Group:     Applications/Internet
URL:       http://webalizer.fadlan.com/
Source0:   ftp://ftp.mrunix.net/pub/webalizer/%{name}-%{mVersion}-%{lVersion}-src.tar.bz2
##NoSource:  0
Source1: webalizer.crontab
Source2: webalizer.conf
Patch4: webalizer-2.21-02-underrun.patch
Patch6: webalizer-2.23-05-confuser.patch
Patch9: webalizer-2.23-05-groupvisit.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gd-devel >= 1.8.4
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool

%description
The Webalizer is a web server log file analysis program which produces
usage statistics in HTML format for viewing with a browser.  The results
are presented in both columnar and graphical format, which facilitates
interpretation.  Yearly, monthly, daily and hourly usage statistics are
presented, along with the ability to display usage by site, URL, referrer,
user agent (browser) and country (user agent and referrer are only
available if your web server produces combined log format files).

%prep
%setup -q -n %{name}-%{mVersion}-%{lVersion}
%patch4 -p1 -b .underrun
%patch6 -p1 -b .confuser
%patch9 -p1 -b .groupvisit

%build
CFLAGS="$RPM_OPT_FLAGS $CPPFLAGS -D_GNU_SOURCE" ; export CFLAGS
%configure --with-gd --with-language=japanese --enable-dns \
           --with-dblib=/lib --enable-bz2
%make

%install
rm -rf --preserve-root %{buildroot}
install -d %{buildroot}%{_localstatedir}/www/usage
install -d %{buildroot}%{_localstatedir}/lib/webalizer
install -d %{buildroot}%{_sysconfdir}/cron.daily

make DESTDIR=%{buildroot} install

install -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/webalizer.conf
install -p -m644 *.png %{buildroot}%{_localstatedir}/www/usage
install -m755 %{SOURCE1} %{buildroot}%{_sysconfdir}/cron.daily/00webalizer

rm -f %{buildroot}%{_sysconfdir}/webalizer.conf.sample

%clean
rm -rf --preserve-root %{buildroot}

%pre
/usr/sbin/adduser -c "Webalizer" -u 67 \
    -s /sbin/nologin -r -d %{_localstatedir}/www/usage webalizer 2>/dev/null || :

%files
%defattr(-,root,root)
%doc CHANGES DNS.README COPYING README* country-codes.txt lang/*
%config(noreplace) %{_sysconfdir}/webalizer.conf
%config(noreplace) %{_sysconfdir}/cron.daily/00webalizer
%{_bindir}/*
%{_mandir}/man1/*.1*
%attr(-, webalizer, root) %dir %{_localstatedir}/www/usage
%attr(-, webalizer, root) %dir %{_localstatedir}/lib/webalizer
%attr(-, webalizer, root) %{_localstatedir}/www/usage/*.png

%changelog
* Tue Mar 27 2012 NARITA Koichi <puuulsar@momonga-linux.org>
- (2.23.05-1m)
- update to 2.23.05
- rebuild against libdb-5.3.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.21.02-7m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.02-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.02-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.21.02-4m)
- full rebuild for mo7 release

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.21.02-3m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.21.02-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.21.02-1m)
- update to 2.21.02

* Sat Jun 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.01.10-25m)
- stop cron

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.01.10-24m)
- fix build with new libtool

* Tue Mar 31 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.01.10-23m)
- add BuildRequires: autoconf

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01.10-22m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01.10-21m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01.10-20m)
- %%global _default_patch_fuzz 2

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01.10-19m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.01.10-18m)
- rebuild against db4-4.7.25-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.01.10-17m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.01.10-16m)
- rebuild against db4-4.6.21

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.01.10-15m)
- rebuild against db4.5

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.01.10-14m)
- rebuild against db4.3

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.01.10-13m)
- enable x86_64.

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.01.10-12m)
- rebuild against db4.2

* Sun Dec 07 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.01.10-11m)
- fix to enable DNS lookup
- modify OutputDir in webalizer.conf
- add BuildRequires: db4-devel
- use %%{momorel} macro
- use %%global

* Mon Oct 27 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.01.10-10m)
- It corrects so that it may not be dependent on db1.

* Mon Feb 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.01.10-9m)
- rebuild against httpd-2.0.43

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.01.10-8m)
- rebuild against for gd

* Mon Aug 12 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.01.10-7m)
- remake patch (%patch6)

* Sun Aug 11 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.01.10-6m)
- remake patch (%patch6)

* Sun Aug 11 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.01.10-5m)
- add patch xhtml output patch.(but, this is not default)

* Tue May 14 2002 Toru Hoshina <t@kondara.org>
- (2.01.10-4k)
- rebuild against HEAD.

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.01.10-2k)
- ver up.

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.01.09-6k)
- BuildRequires: gd-devel >= 1.8.4-6k

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.01.09-4k)
- rebuild against libpng 1.2.2.

* Mon Jan 07 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.01.09-2k)
- update to 2.01-09
- rework webalizer-2.01-09-jp.patch
- rework webalizer-2.01-09-multilingual-logs.patch
- merge *-hourly.patch to *-multilingual-logs.patch
  (fixed by t@kondara.org in 2.01.06-28k)
- add /etc/crontab.daily/webalizer

* Tue Dec 25 2001 Toru Hoshina <t@kondara.org>
- (2.01.06-28k)
- added hourly patch.

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (2.01.06-26k)
- move config file to config-sample
- make webalizer-cron.sh not install automatically
- enable dns

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.01.06-22k)
- rebuild against libpng 1.2.0.
- merge from Jirai, so rebuild against gd 1.8.4.

* Sun Oct 8 2001 Hiroki Ata <bxh05251@nifty.com>
- fix not to be created default files(index.html and
- usage_Xxx.html)

* Wed Aug 29 2001 Hiroki Ata <bxh05251@nifty.com>
- (2.01.06-20k)
- upgrade to 2.01-06
- modified and replaced patch files.
- added httpd-webalizer.conf for apache httpd.conf

* Mon May 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.30.04-18k)
- modified contentdir to /home/httpd

* Fri Mar 16 2001 Daiki Matsuda <dyky@df-usa.com>
- (1.30.04-17k)
- modified webalizer.conf.patch to use /var/www/html/usage (#859)

* Mon Dec 25 2000 Daiki Matsuda <dyky@df-usa.com>
- (1.30.04-15k)
- fixed for apache-1.3.14 about /var/www (FHS?)

* Mon Jul 17 2000 Hiroki Ata <hata@kondara.org>
- (1.30.04-7k)
- modify Makefile and shell script to create language libraries
- for gcc-2.96.
- modified create_lang_libs.sh, webalizer_lang.shared.c and Makefile
- in directory "lang".

* Mon Jul 10 2000 Hiroki Ata <hata@kondara.org>
- add the feature to report multilingual logs.

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Mon Sep 13 1999 Peter Hanecak <hanecak@megaloman.sk>
  [1.30.04-4]
  rebuild against gd-1.6.3 (now PNG images are used)

* Sun Aug 29 1999 Peter Hanecak <hanecak@megaloman.sk>
  [1.30.04-3]
  source changed to .bz2

* Fri Aug 27 1999 Henri Gomez <gomez@slib.fr>
  [1.30.04-2]
  crontab activated each Sunday at 4:01 AM (just before apache rotate its log)

* Thu Aug 26 1999 Henri Gomez <gomez@slib.fr>
  [1.30-04]
  Big rework of RPM (at least configure support)
  Adapted spec to RPM 3 naming conventions,
  Changed URL
  Added icon
  Corrected -lz
  Adapted /etc/webalizer.conf to Redhat
  New directory usage in /home/httpd/html/usage
  Add month cron in /etc/crontab

* Fri Jan 08 1999 Simon Liddington <sjl96v@ecs.soton.ac.uk>
  [1.20-11]
  Installs manpage
  Config file installed as config

* Mon Jul 27 1998 Arne Coucheron <arneco@online.no>
  [1.20-07]

* Sun Jul 26 1998 Arne Coucheron <arneco@online.no>
  [1.12-10]

* Sun Jun 14 1998 Arne Coucheron <arneco@online.no>
  [1.12-09]

* Sun Jun 07 1998 Arne Coucheron <arneco@online.no>
  [1.12-08]

* Fri Jun 05 1998 Arne Coucheron <arneco@online.no>
  [1.00-05]

* Wed May 27 1998 Arne Coucheron <arneco@online.no>
  [1.00-04]
- using predefined %%{name} and %%{version} macros
- using %defattr macro in file list, this requires rpm 2.5 to build
- installing msfree.gif to /home/httpd/html/usage
- added -q parameter to %setup
- added CHANGES and TODO to %doc

* Fri May 08 1998 Arne Coucheron <arneco@online.no>
  [0.99.06-1]

* Sat Jan 31 1998 Arne Coucheron <arneco@online.no>
- First release
