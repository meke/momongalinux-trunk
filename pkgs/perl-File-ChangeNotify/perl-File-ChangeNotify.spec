%global         momorel 3

Name:           perl-File-ChangeNotify
Version:        0.24
Release:        %{momorel}m%{?dist}
Summary:        Watch for changes to files, cross-platform style
License:        "Artistic 2.0"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-ChangeNotify/
Source0:        http://www.cpan.org/authors/id/D/DR/DROLSKY/File-ChangeNotify-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-File-Path
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Pluggable
BuildRequires:  perl-Moose >= 2.0001
BuildRequires:  perl-MooseX-Params-Validate >= 0.08
BuildRequires:  perl-MooseX-SemiAffordanceAccessor
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-PathTools
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Time-HiRes
Requires:       perl-Module-Pluggable
Requires:       perl-Moose >= 2.0001
Requires:       perl-MooseX-Params-Validate >= 0.08
Requires:       perl-MooseX-SemiAffordanceAccessor
Requires:       perl-namespace-autoclean
Requires:       perl-PathTools
Requires:       perl-Time-HiRes
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides an API for creating a File::ChangeNotify::Watcher
subclass that will work on your platform.

%prep
%setup -q -n File-ChangeNotify-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(IO::KQueue)/d'

EOF
%define __perl_requires %{_builddir}/File-ChangeNotify-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/File/ChangeNotify
%{perl_vendorlib}/File/ChangeNotify.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.16.3

* Sun Jan 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- rebuild against perl-Moose-2.0001
- updateto 0.20

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-2m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Tue Oct  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-2m)
- rebuild against perl-5.10.1

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
