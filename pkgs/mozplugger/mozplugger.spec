%global        momorel 1

Summary:       A generic plug-in for mozilla
Name:          mozplugger
Version:       2.1.3
Release:       %{momorel}m%{?dist}
License:       GPL
Group:         Applications/Internet
URL:           http://mozplugger.mozdev.org/
Source0:       http://mozplugger.mozdev.org/files/%{name}-%{version}.tar.gz
NoSource:      0
Patch0:        %{name}-%{version}-Makefile.patch
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:      mikmod
Requires:      xloadimage
Requires:      sox
BuildRequires: libX11-devel
BuildRequires: libXt-devel
BuildRequires: gcc-c++

Requires(pre): mozplugger-common = %{version}-%{release}
Obsoletes:     plugger
Obsoletes:     firefox-plugin-mozplugger

%description
generic plug-ins for mozilla

%package common
Summary:  common files for mozplugger and firefox-plugin-mozplugger
Group:    Applications/Internet

%description common
MozPlugger is a generic Mozilla plug-in that allows the use of standard Linux
programs as plug-ins for media types on the Internet.

%prep
%setup -q
%patch0 -p1

%build
%configure PLUGINDIRS=%{_libdir}/mozilla/plugins/
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install root=%{buildroot} libprefix=/%{_lib} PLUGINDIRS=%{_libdir}/mozilla/plugins/

%clean
rm -rf %{buildroot}

%files common
%defattr(-,root,root)
%doc README COPYING ChangeLog
%{_bindir}/mozplugger*
%config(noreplace) %{_sysconfdir}/mozpluggerrc
%{_mandir}/man7/mozplugger.7*

%files
%defattr(-,root,root)
%{_libdir}/mozilla/plugins/mozplugger.so

%changelog
* Wed Oct 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Tue Jan  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.3-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.3-1m)
- update to 1.14.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.2-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.2-1m)
- update to 1.14.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.0-2m)
- full rebuild for mo7 release

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.0-1m)
- update to 1.14.0

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13.3-1m)
- update to 1.13.3

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13.2-1m)
- update to 1.13.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13.0-1m)
- update to 1.13.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.0-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.2-2m)
- rebuild against gcc43

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-1m)
- update to 1.10.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3-3m)
- %%NoSource -> NoSource

* Sun Nov 12 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3-2m)
- removed firefox-plugin-mozplugger
- copy from trunk (r12675)

* Sun May 14 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.7.3-1m)
- update to 1.7.3
- revise lib64 patch

* Tue Mar 24 2005 Toru Hoshina <t@momonga-linux.org>
- (1.7.1-4m)
- rebuild against mozilla 1.7.6.
- rebuild against firefox 1.0.1.
- install to {_libdir}/firefox/plugins and {_libdir}/mozilla/plugins.

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (1.7.1-3m)
- rebuild against firefox 1.0.1.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7.1-2m)
- enable x86_64.

* Thu Jan  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.1-1m)
- minor bugfixes

* Sun Dec 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.0-1m)
- major bugfixes

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6.1-4m)
- rebuild against mozilla-1.7.5

* Mon Sep 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-3m)
- add firefox-plugin-mozplugger

* Thu Sep 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6.1-2m)
- rebuild against mozilla-1.7.3

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-1m)
- minor feature enhancements and bugfixes

* Sat Aug  7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5.2-5m)
- rebuild against mozilla-1.7.2

* Tue Jul  6 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5.2-4m)
- rebuild against mozilla-1.7-2m

* Mon Jun 28 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5.2-3m)
- rebuild against mozilla-1.7

* Sat May 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.2-2m)
- modify spec and require

* Tue Apr  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.2-1m)
- does not pop up the sound controller anymore

* Sun Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.5.1-2m)
- Epoch, the company that is known as "the ballpark" game vendor.

* Wed Feb 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.1-1m)

* Sat Jan 17 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5.0-2m)
- rebuild against mozilla-1.6

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.0-1m)

* Mon Dec  8 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.3.2-3m)
- put unix-sdk-3.0b5.tar.Z into repository

* Fri Oct 17 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.2-2m)
- rebuild against mozilla-1.5

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.3.2-1m)
- version 1.3.2

* Sun Aug 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.3.1-1m)
- code cleanup

* Thu Jul 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.0-1m)
  update to 1.3.0

* Wed Jul  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.1-2m)
  rebuild against for mozilla-1.4
  build with gcc3.2

* Tue Jun 17 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.1-1m)
  update to 1.2.1

* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.0-1m)
  update to 1.2.0

* Fri May 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1.3-2m)
- rebuild against for mozilla-1.3.1

* Sun Apr 27 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.3-1m)
  update to 1.1.3

* Sat Mar 15 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.2-1m)
  update to 1.1.2

* Fri Mar 14 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (1.1-3m)
- rebuild against for mozilla-1.3

* Fri Mar  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1-2m)
- rebuild against for mozilla-1.3b

* Sun Feb 16 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1-1m)
  update to 1.1

* Wed Feb 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-1m)
  imported.
