%global momorel 1
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: GNU GLOBAL source code tag system
Name: global
Version: 6.2.12
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: GPLv3+ and LGPLv3+ and Modified BSD and Public Domain
URL: http://www.gnu.org/software/global/
Source0: ftp://ftp.gnu.org/pub/gnu/global/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: perl
Requires(post): info
Requires(preun): info
Obsoletes: gtags-mode-xemacs

%description
GNU GLOBAL is a source code tag system that works the same way across diverse
environments. It supports C, C++, Yacc and Java source code.

%package -n emacs-gtags-mode
Summary: GNU GLOBAL source code tag system for Emacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{emacsver}
Obsoletes: gtags-mode-emacs
Obsoletes: elisp-gtags-mode

%description -n emacs-gtags-mode
GNU GLOBAL is a source code tag system that works the same way across diverse
environments. It supports C, C++, Yacc and Java source code.

%prep
%setup -q

%build
%{configure} --with-lispdir=%{e_sitedir}
%{make}

%install
rm -rf %{buildroot}
%{makeinstall}
%{__mkdir_p} %{buildroot}%{_sysconfdir}
%{__mkdir_p} %{buildroot}%{_datadir}/%{name}
%{__install} -m 644 gtags.conf %{buildroot}%{_sysconfdir}
%{__install} -m 644 globash/globash.rc %{buildroot}%{_datadir}/%{name}
%{__install} -m 644 gtags.pl %{buildroot}%{_datadir}/%{name}

%{__mkdir_p} %{buildroot}%{e_sitedir}
%{__install} -m 644 gtags.el %{buildroot}%{e_sitedir}
pushd %{buildroot}%{e_sitedir}
emacs -q --batch -no-site-file -f batch-byte-compile gtags.el
popd

rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_libdir}/gtags/exuberant-ctags.a
rm -f %{buildroot}%{_libdir}/gtags/exuberant-ctags.la
rm -f %{buildroot}%{_libdir}/gtags/user-custom.a
rm -f %{buildroot}%{_libdir}/gtags/user-custom.la

%post
/sbin/install-info %{_infodir}/global.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/global.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/gtags.conf
%{_bindir}/htags
%{_bindir}/global
%{_bindir}/globash
%{_bindir}/gozilla
%{_bindir}/gtags
%{_bindir}/gtags-cscope
%{_libdir}/gtags/exuberant-ctags.so
%{_libdir}/gtags/user-custom.so
%{_datadir}/global
%{_datadir}/gtags
%{_mandir}/man1/global.1*
%{_mandir}/man1/htags.1*
%{_mandir}/man1/globash.1*
%{_mandir}/man1/gozilla.1*
%{_mandir}/man1/gtags.1*
%{_mandir}/man1/gtags-cscope.1*
%{_infodir}/global.info*
%doc AUTHORS BOKIN_MODEL* COPYING* ChangeLog DONORS 
%doc FAQ INSTALL LICENSE NEWS README THANKS
%doc htags/global.cgi.tmpl htags/ghtml.cgi.tmpl htags/bless.sh.tmpl
%doc globash/globash.rc gtags.el gtags.pl gtags-cscope.vim gtags.vim gtags.conf

%files -n emacs-gtags-mode
%defattr(-,root,root,-)
%{e_sitedir}/gtags.el*

%changelog
* Tue Apr 08 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.12-1m)
- update 6.2.12

* Mon Mar 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.11-1m)
- update 6.2.11

* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.9-1m)
- update 6.2.9

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.2.7-1m)
- update 6.2.7

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.2.5-1m)
- update 6.2.5

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.2.4-2m)
- rebuild for emacs-24.1

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.2.4-1m)
- update 6.2.4

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.1-1m)
- update 6.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.7-2m)
- update for new emacs packaging

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9.7-1m)
- update 5.9.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.4-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.4-1m)
- update to 5.9.4

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.3-1m)
- update to 5.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9.1-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9.1-1m)
- update to 5.9.1

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9-2m)
- rebuild against emacs-23.2

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9-1m)
- update to 5.9

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.8.2-1m)
- update to 5.8.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.8.1-1m)
- update to 5.8.1
- rename gtags-mode-emacs to elisp-gtags-mode
- kill gtags-mode-xemacs

* Mon Jan  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.7-1m)
- update to 5.7.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.6-1m)
- update to 5.7.6

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7.5-7m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7.5-6m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.5-5m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.5-4m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.5-3m)
- rebuild against xemacs-21.5.29

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.5-2m)
- rebuild against emacs-23.0.92

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.5-1m)
- update to 5.7.5
- License: GPLv3+ and LGPLv3+ and Modified BSD and Public Domain

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.4-2m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.4-1m)
- update to 5.7.4
- License: GPLv3+ or LGPLv3+ or Modified BSD or Public Domain
- modify %%doc

* Sun Oct 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.2-1m)
- update to 5.7.2

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.1-2m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Tue May  6 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.1-1m)
- update to 5.7.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.6.2-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6.2-3m)
- %%NoSource -> NoSource
 
* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.6.2-2m)
- rebuild against perl-5.10.0-1m

* Wed Oct 31 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.6.2-1m)
- update to 5.6.2

* Mon Aug 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.6.1-1m)
- update to 5.6.1

* Fri Jul 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.6-1m)
- update to 5.6
-- switch license to GPLv3
- set xemacsver to 21.4.20
- set xemacssumover to 2007.04.27

* Mon Jun  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5-1m)
- update to 5.5
- set emacsver to 22.1

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1-1m)
- update to 5.4.1

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4-1m)
- update to 5.4

* Fri Nov 24 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.3-1m)
- update to 5.3
- rebuild against emacs-22.0.91

* Wed Sep 20 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-1m)
- update to 5.2

* Sun Jul 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.1-1m)
- update to 5.1

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.0-1m)
- update to 5.0

* Wed May 25 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.8.6-1m)
  update to 4.8.6
  
* Wed Mar 23 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.8.4-1m)
  update to 4.8.4

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.8.2-2m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Nov 23 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.8.2-1m)
- update to 4.8.2

* Sun Nov  7 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.8.1-1m)
  update to 4.8.1

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (4.5.2-2m)
- revised spec for enabling rpm 4.2.

* Thu May 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.5.2-1m)
- update to 4.5.2
- change URL
- set emacsver 21.3

* Fri Jan 10 2003 Kenta MURATA <muraken@momonga-linux.org>.
- (4.5.1-1m)
- version up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>.
- (4.4-4k)
- /sbin/install-info -> info in PreReq.

* Wed Apr 3 2002 Junichiro Kita<kita@kondara.org>
- kondarize
